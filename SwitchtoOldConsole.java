package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;

public class SwitchtoOldConsole extends Initialization{

	@Test(priority=1,description="Verify Switch to Old Console") 
	public void VerifySwitchtoOldConsoleTC_ST_390() throws Throwable {
		Reporter.log("\n1.Verify Switch to Old Console",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonTrynewconsole();
		accountsettingspage.SwitchtoOldConsole();		
	}
	
	
	
	
}
