package RightClick;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//Sure video should not be installed in device
public class RightClick_Surevideo extends Initialization
{
     
	@Test(priority='1',description="1.To Verify Install Option SureVideo RightClick")
	public void VerifyInstallOptionSureLock() throws Exception {
		Reporter.log("\n1.To Verify Install Option SureVideo RightClick",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();;
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureVideo();
		rightclickDevicegrid.InstallOptionSureVideo();
	}
	
	@Test(priority='2',description="2.To verify SureVideo Activate Licence,Launch SureFox  and Upgrade Option")
	public void VerifyActivateLicenseAndLaunchSureFoxOption() throws IOException, InterruptedException {
		Reporter.log("\n2.To verify SureVideo Activate Licence,Launch SureFox  and Upgrade Option",true);
		rightclickDevicegrid.InstallSureVideoLower();
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnDynamicRefresh();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureVideo();
		rightclickDevicegrid.VerifyUpgradeActvateLicenseAndLaunchSureVideoOption();
		rightclickDevicegrid.ClickOnRefreshDeviceGrid();
	}
	
	@Test(priority='3',description="3.To verify SureVideo Activate Functionality")
	public void VerifyActivateSureVideo() throws InterruptedException {
		Reporter.log("\n3.To verify SureVideo Activate Functionality",true);
		rightclickDevicegrid.ClickOnrefreshButtonDeviceInfo();
		rightclickDevicegrid.ClickOnRefreshDeviceGrid();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureVideo();
		rightclickDevicegrid.ActivateSureVideo(Config.SVActivationCode);
	}
	
	@Test(priority='4',description="4.To verify SureVideo Deactivate Functionality")
	public void VerifyDeactivateSureVideo() throws InterruptedException {
		Reporter.log("\n4.To verify SureVideo Deactivate Functionality",true);
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnDynamicRefresh();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureVideo();
		rightclickDevicegrid.DeactivateSureVideo();
	}
}
