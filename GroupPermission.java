package UserManagement_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class GroupPermission extends Initialization {

	@Test(priority = '1', description = "1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException {
		Reporter.log("\n1.To Verify roles option is present", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
	}

	@Test(priority = '2', description = "2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException {
		Reporter.log("\n2.To Verify default super user", true);
		usermanagement.toVerifySuperUsertemplete();
	}

	@Test(priority = '3', description = "3.To create a roll with gropup permisssion device action permission managenet permission ")
	public void RollWithGroupActionManagement() throws InterruptedException {
		Reporter.log("\n3.To create a roll with gropup permisssion device action permission managenet permission ",
				true);
		usermanagement.CreateRoleForGroupPermissions();
		usermanagement.CreateUserForPermissions("AutoGroupPermission", "42Gears@123", "AutoGroupPermission",
				"Group Permissions");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoGroupPermission", "42Gears@123");

	}

	@Test(priority = '4', description = "4.To create a roll with gropup permisssion device action permission managenet permission ")
	public void LoginAsUSERA()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n4.To create a roll with gropup permisssion device action permission managenet permission ",
				true);
		usermanagement.GroupPermissionsUSERAallowed();
		Thread.sleep(3000);
	}

	@Test(priority = '5', description = "5.To allow only new group,rename,allow home group")
	public void allowFewGroupPermissions() throws InterruptedException {
		Reporter.log("\n5.To allow only new group,rename,allow home group", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		Thread.sleep(6000);
	}

	@Test(priority = '6', description = "6.To allow only new group,rename,allow home group")
	public void AdminFewGroupPermissions() throws InterruptedException {
		Reporter.log("\n6.To allow only new group,rename,allow home group", true);
		usermanagement.InAdminEnableOnlyNewGroupRenameAllowHomeGroup();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoGroupPermission", "42Gears@123");
		// usermanagement.RefreshCurrentPage();
	}

	@Test(priority = '7', description = "7.To allow only new group,rename,allow home group")
	public void AccesForFewGroupPermissions() throws InterruptedException {
		Reporter.log("\n7.To allow only new group,rename,allow home group", true);
		usermanagement.checkAccessNewGroupRenameAllowHomeGroup();

	}
}
