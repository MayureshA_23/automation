package DynamicJobs_TestScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class DynamicSureFox extends Initialization{
	
	
	@Test(priority='1',description="1.Verify surefox install window")
	public void VerifySureFoxInstallWindow() throws InterruptedException, Throwable{
		Reporter.log("\n1.Verify surefox install window",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.VerifySureFoxInstallationWindow();
        dynamicjobspage.ClickOnSurefoxSettingsCloseBtn();

		Reporter.log("PASS>> Verify surefox install window",true);
	}

	@Test(priority='2',description="2.Verify Installation of SureFox")
	public void VerifyInstallationOfSureFox() throws InterruptedException, Throwable{
		Reporter.log("\n2.Verify Installation of SureFox",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
        commonmethdpage.SearchDeviceInconsole();
    	dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.ClickOnInstallButtonSurefoxSetup();
    	dynamicjobspage.Allowedwebsite("www.google.co.in","Google");
		dynamicjobspage.Allowedwebsite("www.amazon.com","Amazon");
		dynamicjobspage.AllowedwebsiteHttps("www.facebook.com", "FaceBook");
		dynamicjobspage.ClickonNextBtnSureFoxSetup();
		dynamicjobspage.EnterActivationKey(Config.ActivationKey);
		dynamicjobspage.ClickOnNextBtnSurefoxpopup();
		dynamicjobspage.VerifySuccessfullyInstalledMsg();
		dynamicjobspage.ClickOnSurefoxSettingsCloseBtn();
		commonmethdpage.ClickOnDynamicRefresh();
	}

	
	@Test(priority='3',description="3.To Verify Verify Edit surefox settings- Apply Settings ")
	public void VerifySurefoxSettingsApplySettings() throws InterruptedException, Throwable{
		Reporter.log("\n3.To Verify Verify Edit surefox settings- Apply Settings",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
        dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.Allowedwebsite("www.irctc.co.in","IRCTC");
	    dynamicjobspage.ClickOnApplyButtonSureFox();
	    dynamicjobspage.ClickOnApplyButtonYesBtn();
	    dynamicjobspage.ConfirmationMessageApplySettings();
	    dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		
	}
	

	@Test(priority='4',description="4.Verify Edit surefox settings and save as job")
	public void VerifySurefoxSettingsSaveAsJob() throws InterruptedException, Throwable{
		Reporter.log("\n4.Verify Edit surefox settings and save as job",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();


		dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.Allowedwebsite("www.google.co.in","Google");
		dynamicjobspage.ClickOnSaveAsJobButton_SureFox();
		dynamicjobspage.VerifyOfJobDetailsPopup_SureFox();
		dynamicjobspage.EnterJobName(Config.SFSaveAsJob);
		dynamicjobspage.ClickOnSaveButtonOfJobDetails();
		dynamicjobspage.SaveAsJob_ConfirmationMessage();
		dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		jobsOnAndroidpage.ClickOnJobs();
		jobsOnAndroidpage.ClearSearch();
		jobsOnAndroidpage.SearchJob(Config.SFSaveAsJob);
		dynamicjobspage.VerifyOfSureFoxSaveAsJob_Save();
		commonmethdpage.ClickOnHomePage();
		Reporter.log("PASS>> Verify Edit surefox settings and save as job",true);
	}
	
	@Test(priority='5',description="5.Verify Edit surefox settings and Edit XML")
	public void VerifyEditSureFoxSaveSource() throws InterruptedException, Throwable{
		Reporter.log("\n5.Verify Edit surefox settings and Edit XML",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();


		dynamicjobspage.ClickOnSurefoxSettings();
	    dynamicjobspage.ClickOnEditXMLButtonSureFox();
	    dynamicjobspage.VerifyOfEditXMLSureFox();
	    dynamicjobspage.ClickOnSaveSourceButton();
	    dynamicjobspage.SaveAsSource_ConfirmationMessage();
	    dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
	 
		Reporter.log("PASS>> Verification of Edit XML Popup of Dynamic SureFox Settings Job",true);
	}
	
	@Test(priority='6',description="6.To Verify Edit XML button of Dynamic SureFox Settings Job when XML text area is empty")
	public void VerifyEditXMLAreaEmpty() throws InterruptedException, Throwable{
		Reporter.log("\n6.To Verify Edit XML button of Dynamic SureFox Settings Job when XML text area is empty",true);
	
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();


		dynamicjobspage.ClickOnSurefoxSettings();
	    dynamicjobspage.ClickOnEditXMLButtonSureFox();
	    dynamicjobspage.ClearEditXMLTextArea();
	    dynamicjobspage.ClickOnEditXMLSaveBtn();
	    dynamicjobspage.EditXMLSaveBtnSureFox_Error();
	    dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		Reporter.log("PASS>> Verification of Edit XML button of Dynamic SureFox Settings Job when XML text area is empty",true);
	}
	
	@Test(priority='7',description="7.To Verify Edit XML button of Dynamic SureFox Settings Job when XML file is invalid ")
	public void VerifyEditXMLInvalidText() throws InterruptedException, Throwable{
		Reporter.log("\n7.To Verify Edit XML button of Dynamic SureFox Settings Job when XML file is invalid",true);
	
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();


		dynamicjobspage.ClickOnSurefoxSettings();
	    dynamicjobspage.ClickOnEditXMLButtonSureFox();
	    dynamicjobspage.ClearEditXMLTextArea();
	 
	    dynamicjobspage.ClickOnEditXMLSaveBtn();
	    dynamicjobspage.EditXMLSaveBtnSureFox_Error();
	    dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		Reporter.log("PASS>> Verification of Edit XML button of Dynamic SureFox Settings Job when XML file is invalid",true);
	}
	
	@Test(priority='8',description="8.To Verify Save As Job Button when Clicked on Save button of Job Details with JobName Empty")
	public void VerifySaveAsJobEmpty() throws InterruptedException, Throwable{
		Reporter.log("\n8.To Verify Save As Job Button when Clicked on Save button of Job Details with JobName Empty",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
    	dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.Allowedwebsite("www.google.co.in","Google");
		dynamicjobspage.ClickOnSaveAsJobButton_SureFox();
		dynamicjobspage.VerifyOfJobDetailsPopup_SureFox();
		dynamicjobspage.ClickOnSaveButtonOfJobDetails();
		dynamicjobspage.SaveAsJobErrorMessage();
		dynamicjobspage.ClickOnJobDetailsCloseBtn_SureFox();
		dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		Reporter.log("PASS>> Verification of Save As Job Button when Clicked on Save button of Job Details with JobName Empty",true);
	}
	
	@Test(priority='9',description="9.To Verify Save As Job Button when Clicked on Close button of Job Details")
	public void VerifySaveAsJobCloseButton() throws InterruptedException, Throwable{
		Reporter.log("\n9.To Verify Save As Job Button when Clicked on Close button of Job Details",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
        dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.Allowedwebsite("www.google.co.in","Google");
		dynamicjobspage.ClickOnSaveAsJobButton_SureFox();
		dynamicjobspage.VerifyOfJobDetailsPopup_SureFox();
		dynamicjobspage.EnterJobName(Config.SFSaveAsJob);
		dynamicjobspage.ClickOnJobDetailsCloseBtn_SureFox();
		dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		jobsOnAndroidpage.ClickOnJobs();
		jobsOnAndroidpage.ClearSearch();
		jobsOnAndroidpage.SearchJob(Config.SFSaveAsJob);
		dynamicjobspage.VerifyOfSureFoxSaveAsJob_Cancel();
		commonmethdpage.ClickOnHomePage();
		Reporter.log("PASS>> Verification of Save As Job Button when Clicked on Close button of Job Details",true);
	}
	
	@Test(priority='A',description="10.To Verify Apply button of Dynamic SureFox Settings Job when clicked on No Button of Apply Settings")
	public void VerifyApplyButtonClickedNo() throws InterruptedException, Throwable{
		Reporter.log("\n10.To Verify Apply button of Dynamic SureFox Settings Job when clicked on No Button of Apply Settings",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
        dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.Allowedwebsite("www.irctc.co.in","IRCTC");
	    dynamicjobspage.ClickOnApplyButtonSureFox();
	    dynamicjobspage.VerifyOfApplySettingsPopup_SureFox();
	    dynamicjobspage.ClickOnApplyButtonNoBtn_SureFox();
//	    dynamicjobspage.ConfirmationMessageApplySettingsVerify_False();
	    dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		Reporter.log("PASS>> Verification of Apply button of Dynamic SureFox Settings Job when clicked on No Button of Apply Settings",true);
	}
	

	@Test(priority='B',description="11. Verify Edit surefox settings and save as file")
	public void VerifySurefoxSettingsSaveAsFile() throws InterruptedException, Throwable{
		Reporter.log("\n11.To Verify Save As File Button Of SureFox Settings Job",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
        dynamicjobspage.ClickOnSurefoxSettings();
		dynamicjobspage.Allowedwebsite("www.42Gears","42Gears");
		androidJOB.clickOnSaveAsFile();
		androidJOB.isSureLockFileDownloaded(Config.FileDownloaded,"surefox");

//		dynamicjobspage.ClickOnSaveAsFileButton_SureFox();
		dynamicjobspage.ClickOnSureFoxHomeSettingsCloseButton();
		Reporter.log("PASS>> Verify Edit surefox settings and save as file",true);
	}
	
	
	
	
	
	
}
