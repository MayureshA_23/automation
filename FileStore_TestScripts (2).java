package SanityTestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class FileStore_TestScripts extends Initialization{
	
	
	@Test(priority=0,description="File Store-Creating New folder")
	public void CreateNewFolder() throws Throwable
	{
		Reporter.log("=====1.File Store-Creating New folder=====",true);
		filestorepage.ClickOnFileStore();
		filestorepage.NamingFolderWhileCreation("File123");
		filestorepage.ClickOnDelete("File123");
		filestorepage.ClickOnDeleteYesButton();
	}
	@Test(priority=1,description="File Store-Delete")
	public void DeleteFolder() throws Throwable
	{	Reporter.log("\n=====2.File Store-Delete=====",true);
		filestorepage.ClickOnFileStore();
		filestorepage.NamingFolderWhileCreation("AutoFile");
		filestorepage.ClickOnDelete("AutoFile");
		filestorepage.ClickOnDeleteYesButton();
	}
	@Test(priority=1,description="Verify Filestore Android")
	public void VerifyOfFileStoreAndroid() throws Throwable
	{
		Reporter.log("\n=====3.Verify Filestore Android=====",true);
		filestorepage.ClickOnFileStore();
		filestorepage.DisableFileStore();
		filestorepage.EnableFileStore();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.NamingFolderWhileCreation("FileSharing12345");
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("FileSharing12345");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FileSharingProfile");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FileSharingProfile");
		commonmethdpage.Selectjob("FileSharingProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
        androidJOB.CheckStatusOfappliedInstalledJob("FileSharingProfile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		profilesAndroid.verifyFolderOnDevice("FileSharing1234");
		Reporter.log("PASS>>File Store-File Sharing policy _Android", true);
		
		
	}
	
}
