package NewUI_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Profiles_TestScripts extends Initialization{
	@Test(priority=0,description="Verify modifying a profile under Android profiles section.")
	public void ModifyAndroidProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====1.Verify modifying a profile under Android profiles section.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("0Modify Profile");
		profilesAndroid.clickOnSave();
		profilesAndroid.SearchProfileInProfilePage("0Modify Profile");
		profilesAndroid.SelectProfile("0Modify Profile");
		profilesAndroid.clickOnEditInProfilePage();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.DeSelectParametersofSystemSettings();
		profilesAndroid.clickOnSave();
		profilesAndroid.NotificationOnProfileUpdate();
	}
	@Test(priority=1,description="Verify deleting a profile from the Android profile section.")
	public void DeleteAndroidProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====2.Verify deleting a profile from the Android profile section.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("Delete 0Profile");
		profilesAndroid.clickOnSave();
		profilesAndroid.SearchProfileInProfilePage("Delete 0Profile");
		profilesAndroid.SelectProfile("Delete 0Profile");
		profilesAndroid.clickOnDeleteBtn();
		profilesAndroid.clickOnDeleteYESBtn();
		profilesAndroid.NotificationOnProfileDelete();
		profilesAndroid.SearchProfileInProfilePage("Delete 0Profile");
		profilesAndroid.VerifyOfDeleteFolderOrProfile("Delete 0Profile");
	}
	@Test(priority=2,description="Verify copy option under Android profile section.")
	public void CopyAndroidProfile() throws InterruptedException
	{	Reporter.log("=====3.Verify copy option under Android profile section.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnWifiConfigurationPolicy();
		profilesAndroid.ClickOnWifiConfigurationPolicyConfigButton();
		profilesAndroid.EnterSSID_PasswordValues_WifiConfigurationWAP2();
		profilesAndroid.EnableAutoConnect();
		profilesAndroid.EnterWifiConfigurationProfileName("0Copy Profile");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		profilesAndroid.SearchProfileInProfilePage("0Copy Profile");
		profilesAndroid.SelectProfile("0Copy Profile");
		profilesAndroid.clickOnCopyBtn();
		profilesAndroid.NotificationOnProfileCopied();
		profilesAndroid.SearchProfileInProfilePage("copy");
		profilesAndroid.VerifyOfCopiedProfileName("0Copy Profile");	
	}
	@Test(priority=3,description="Verify refresh option under Android profile section.")
	public void RefreshAndroidProfilePage() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====4.Verify refresh option under Android profile section.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("Profile to Check 0RefreshBtn");
		profilesAndroid.clickOnSave();
		profilesAndroid.clickOnRefreshBtnInProfilePAge();
		profilesAndroid.SearchProfileInProfilePage("Profile to Check 0RefreshBtn");
		profilesAndroid.VerifyOfExpectedProfileName("Profile to Check 0RefreshBtn");
	}
	@Test(priority=4,description="Verify Set as default option under Android Profiles section.")
	public void DefaultInAndroidProfilePage() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====5.Verify Set as default option under Android Profiles section.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnWifiConfigurationPolicy();
		profilesAndroid.ClickOnWifiConfigurationPolicyConfigButton();
		profilesAndroid.EnterSSID_PasswordValues_WifiConfigurationWAP2();
		profilesAndroid.EnableAutoConnect();
		profilesAndroid.EnterWifiConfigurationProfileName("Make 0Default Profile");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		profilesAndroid.SearchProfileInProfilePage("Make 0Default Profile");
		profilesAndroid.SelectProfile("Make 0Default Profile");
		profilesAndroid.clickOnDefaultBtn();
		newUIScriptsPage.NotificationOnProfileDefualt("Make 0Default Profile");
		profilesAndroid.SearchProfileInProfilePage("Default");
		newUIScriptsPage.VerifyOfDefaultProfile("Make 0Default Profile");
		
	}
	@Test(priority=5,description="Verify Add folder option under Android profiles.")
	public void AddFolderInAndroidProfilePage() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====6.Verify Add folder option under Android profiles.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.clickOnAddFolder();
		profilesAndroid.EnterFolderNameInProfile("AutoProfile 0Folder");
		profilesAndroid.NotificationOnCreatedFolderInProfile();
		profilesAndroid.SearchProfileInProfilePage("AutoProfile 0Folder");
		profilesAndroid.VerifyOfExpectedProfileName("AutoProfile 0Folder");
		profilesAndroid.SelectProfile("AutoProfile 0Folder");
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("0Pofileinside Folder");
		profilesAndroid.clickOnSave();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("0Pofileinside Folder");
		newUIScriptsPage.ReadingConsoleMessageForJobDeployment("0Pofileinside Folder",Config.DeviceName);
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.SearchProfileInProfilePage("AutoProfile 0Folder");
		profilesAndroid.SelectProfile("AutoProfile 0Folder");
		profilesAndroid.clickOnDeleteBtn();
		profilesAndroid.clickOnDeleteYESBtn();
		profilesAndroid.NotificationOnFolderDeleteInProfile();
		profilesAndroid.SearchProfileInProfilePage("AutoProfile 0Folder");
		profilesAndroid.VerifyOfDeleteFolderOrProfile("AutoProfile 0Folder");
		profilesAndroid.SearchProfileInProfilePage("0Pofileinside Folder");
		profilesAndroid.VerifyOfDeleteFolderOrProfile("0Pofileinside Folder");
	}
	@Test(priority=6,description="Verify search option in Android profiles..")
	public void SearchOptionInAndroidProfilePage() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====7.Verify search option in Android profiles.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles1");
		profilesAndroid.clickOnSave();
		profilesAndroid.DeleteCreatedProfile("SystemSettings AndroidProfiles2");
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles2");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles3");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles4");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles5");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles6");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles7");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles8");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles9");
		profilesAndroid.clickOnSave();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("SystemSettings AndroidProfiles10");
		profilesAndroid.clickOnSave();
		profilesAndroid.DeleteCreatedProfile("q");
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnSystemSettingsPolicy();
		profilesAndroid.ClickOnSystemSettingPolicyConfigButton();
		profilesAndroid.SelectParametersofSystemSettings();
		profilesAndroid.EnterSystemSettingsProfileName("q");
		profilesAndroid.clickOnSave();
		profilesAndroid.SearchProfileInProfilePage("SystemSettings AndroidProfiles");
		profilesAndroid.VerifyOfExpectedProfileName("SystemSettings AndroidProfiles");
		profilesAndroid.SearchProfileInProfilePage("q");
		newUIScriptsPage.VerifyOfSearchOptionWithSingleChar("q");
	}

}
