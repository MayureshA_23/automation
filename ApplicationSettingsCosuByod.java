package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//PRE-CONDITION- DEVICE SHOULD BE IN BYOD AND COSU

//BYOD
public class ApplicationSettingsCosuByod extends Initialization {
	
	
	
	@Test(priority=1, description="\n Verify Hide Apps in Application Settings Job")
	public void ApplicationSettingsHideApps() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n Verify Hide Apps in Application Settings Job",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("HideApps");
		androidJOB.SearchApplication("Evernote");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.ShowAppsJobType("Hide Apps");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("HideApps");
		androidJOB.SelectJob("HideApps");
		androidJOB.ClickOnApplyButton();
		androidJOB.CheckStatusOfappliedInstalledJob("HideApps",360);
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.sec.android.app.launcher", "com.sec.android.app.launcher.activities.LauncherActivity");
		androidJOB.SwipeDevice();
		androidJOB.AdbCommandsForEvernoteApp();
		commonmethdpage.commonScrollMethod("VIEW MORE IN GALAXY APPS");
		androidJOB.VerifyIconEverNotewhenHidden();

		
    
	}
	
	
	@Test(priority=2, description="\n Verify Show Apps in Application Settings job")
	public void ApplicationSettingsShowApps() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n Show Apps in Application Settings job",true);

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("ShowApps");
		androidJOB.SearchApplication("Evernote");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.ShowAppsJobType("Show Apps");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ShowApps");
		androidJOB.SelectJob("ShowApps");
		androidJOB.ClickOnApplyButton();
		androidJOB.CheckStatusOfappliedInstalledJob("ShowApps",360);
		commonmethdpage.AppiumConfigurationCommonMethod("com.sec.android.app.launcher", "com.sec.android.app.launcher.activities.LauncherActivity");
		androidJOB.SwipeDevice();
		androidJOB.AdbCommandsForEvernoteApp();
		commonmethdpage.commonScrollMethod("APPLICATIONS");
        androidJOB.VerifyEvernoteWhenShowApps();
	}
	//In COSU MODE

/*	@Test(priority=3, description="\n Verify Hide Apps in Application Settings Job in COSU")
	public void ApplicationSettingsHideAppsInCosu() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n Verify Hide Apps in Application Settings Job",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("HideApps");
		androidJOB.SearchApplication("Evernote");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.ShowAppsJobType("Hide Apps");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("HideApps");
		androidJOB.SelectJob("HideApps");
		androidJOB.ClickOnApplyButton();
		androidJOB.CheckStatusOfappliedInstalledJob("HideApps",360);
		commonmethdpage.AppiumConfigurationCommonMethod("com.sec.android.app.launcher","com.sec.android.app.launcher.activities.LauncherActivity");
		androidJOB.SwipeDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.android.vending", "com.android.vending.AssetBrowserActivity");
		androidJOB.searchAppPLaystore("Evernote");
		androidJOB.VerifyWhetherInstallOptionIsDispalyed();
}
	
	@Test(priority=4, description="\n Verify Show Apps in Application Settings job in Cosu")
	public void ApplicationSettingsShowAppsInCosu() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n Show Apps in Application Settings job",true);

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("ShowApps");
		androidJOB.SearchApplication("Evernote");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.ShowAppsJobType("Show Apps");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ShowApps");
		androidJOB.SelectJob("ShowApps");
		androidJOB.ClickOnApplyButton();
		androidJOB.CheckStatusOfappliedInstalledJob("ShowApps",360);
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.sec.android.app.launcher","com.sec.android.app.launcher.activities.LauncherActivity");
		androidJOB.SwipeDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.android.vending", "com.android.vending.AssetBrowserActivity");
		androidJOB.searchAppPLaystore("Evernote");
		androidJOB.VerifyWhetherUnInstallOptionIsDispalyed();
}*/
}
