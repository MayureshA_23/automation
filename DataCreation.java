package CustomReportsScripts;

import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DataCreation extends Initialization {

	
	@Test(priority='0',description="Data Creation")
	public void CustomReportCase_1() throws Throwable
	{
		// Group creation
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		
    	//Enable CallLog
    	commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_On();
	    dynamicjobspage.VerifyOfUpdateInterval();
	    dynamicjobspage.ClickOnRefreshInCallLogTab();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
    	
	      //Enable SMSLog
	    commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_On();
	    dynamicjobspage.VerifyOfSMSUpdateInterval();
	    dynamicjobspage.ClickOnRefreshInCallLogTab();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
    	
	    //Enable DataUsage
		commonmethdpage.ClickOnAllDevicesButton();
	    commonmethdpage.SearchDevice(Config.DeviceName);
	    dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnDataUsage_UM();
		dynamicjobspage.ClickOnDataUsageRefreshButton();
		dynamicjobspage.ClickOnDataUsageCloseBtn();
    	
 
		//Enable SureLock Analytics
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnDataAnalyticsTab();
		accountsettingspage.EnableDataAnalyticsCheckBox();
		accountsettingspage.DisableSureLockAnalyticsCheckBox();
		accountsettingspage.ClickOnDataAnalyticsApplyButton();
		accountsettingspage.EnablingSureLockAnalyticsChckBox();
		accountsettingspage.ClickOnDataAnalyticsApplyButton();
		accountsettingspage.VerifySureLockAnalyticsEnablePopUp();
		accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
		
		//Enable SureVideo Analytics
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnDataAnalyticsTab();	
		accountsettingspage.EnableDataAnalyticsCheckBox();
		accountsettingspage.ClickOnAddAnalyticsButton();
		accountsettingspage.SendingDataAnalyticsName("SureVieo Analytics");
		accountsettingspage.SendingTagName("File Name");
		accountsettingspage.ClickOnAddFiledButton();
		accountsettingspage.SendingFieldName("Type");
		accountsettingspage.SelectingFieldType("string");
		accountsettingspage.ClickingonAddFieldAddButton();
		accountsettingspage.ClickOnAddFiledButton();
		accountsettingspage.SendingFieldName("Playlist Name");
		accountsettingspage.SelectingFieldType("string");
		accountsettingspage.ClickingonAddFieldAddButton();
		accountsettingspage.ClickOnAddFiledButton();
		accountsettingspage.SendingFieldName("Start At");
		accountsettingspage.SelectingFieldType("string");
		accountsettingspage.ClickingonAddFieldAddButton();
		accountsettingspage.ClickOnAddFiledButton();
		accountsettingspage.SendingFieldName("End At");
		accountsettingspage.SelectingFieldType("string");
		accountsettingspage.ClickingonAddFieldAddButton();
		accountsettingspage.ClickOnAddFiledButton();
		accountsettingspage.SendingFieldName("Total Time in Seconds");
		accountsettingspage.SelectingFieldType("string");
		accountsettingspage.ClickingonAddFieldAddButton();
		accountsettingspage.ClickonAnalyticsSaveButton();
		accountsettingspage.VerifyNewlyAddedAnalyticsSecretKey();
		accountsettingspage.ClickOnDataAnalyticsApplyButton();
    	
	}
}
