package Settings_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class FirmWareUpdatesEfota extends Initialization{
	
	@Test(priority=1,description="Verify license details of Efota in Account settings")
	public void VerifylicensedetailsofEfotainAccountsettings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("1.Verify license details of Efota in Account settings",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnFirmwareUpdates();
		accountsettingspage.VerifyAllFeaturesOption();
	}
	
	@Test(priority=2,description="Verify giving Firmware updates as empty and click on save")
	public void VerifygivingFirmwareupdatesasemptyandclickonsave() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n2.Verify giving Firmware updates as empty and click on save",true);
		accountsettingspage.ClearAllFieldsOfFirmware();
		accountsettingspage.ClickOnSaveFirmware();
		accountsettingspage.VerifyConfirmationMsgWhenAllFieldsBlank();
	}
	
	@Test(priority=3,description="Verify giving License text field as empty in firmware updates and click on save")
	public void VerifygivingLicensetextfieldasemptyinfirmwareupdatesandclickonsave() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n3.Verify giving License text field as empty in firmware updates and click on save",true);
		accountsettingspage.EnterAllFieldsWithoutLicenseID();
		accountsettingspage.ClickOnSaveFirmware();
		accountsettingspage.VerifyConfirmationMsgWhenAllFieldsBlank();
	}
	
	@Test(priority=4,description="Verify giving Customer ID text field as empty in firmware updates and click on save")
	public void VerifygivingCustomerIDtextfieldasemptyinfirmwareupdatesandclickonsave() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n4.Verify giving Customer ID text field as empty in firmware updates and click on save",true);
		accountsettingspage.EnterAllFieldsWithoutCustomerID();
		accountsettingspage.ClickOnSaveFirmware();
		accountsettingspage.VerifyConfirmationMsgWhenCustomerIDFieldBlank();
	}
	
	@Test(priority=5,description="Verify giving Client ID text field as empty in firmware updates and click on save")
	public void VerifygivingCleintIDtextfieldasemptyinfirmwareupdatesandclickonsave() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n5.Verify giving Client ID text field as empty in firmware updates and click on save",true);
		accountsettingspage.EnterAllFieldsWithoutClientID();
		accountsettingspage.ClickOnSaveFirmware();
		accountsettingspage.VerifyConfirmationMsgWhenCleintIDFieldBlank();
	}
	
	@Test(priority=6,description="Verify giving Secret Client ID text field as empty in firmware updates and click on save")
	public void VerifygivingSecretCleintIDtextfieldasemptyinfirmwareupdatesandclickonsave() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n6.Verify giving Secret Client ID text field as empty in firmware updates and click on save",true);
		accountsettingspage.EnterAllFieldsWithoutSecretClientID();
		accountsettingspage.ClickOnSaveFirmware();
		accountsettingspage.VerifyConfirmationMsgWhenSecretCleintIDFieldBlank();
	}
	@Test(priority=7,description="Verify copy to clipboard button in firmware updates.")
	public void VerifyCopyToClipBoardInZebraLifeguardOTA() throws InterruptedException{
		Reporter.log("\n7Verify copy to clipboard button in firmware updates..",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnFirmwareUpdates();
		accountsettingspage.clickOnZebraLifeGuardOTA();
		accountsettingspage.verifycopyToClipBoard();
		commonmethdpage.ClickOnHomePage();
		//accountsettingspage.verifyExportsetting();
	}

	//vinimanoj 
	
	@Test(priority = 8, description = "Verify Copy to clipboard Button in Firmware Updates TC_ST_515")
	public void VerifyCopytoclipboardButtoninFirmwareUpdatesTC_ST_515() throws InterruptedException {
		Reporter.log("\n8.Verify Copy to clipboard Button in Firmware Updates",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.AccountSettings();
		accountsettingspage.FirmwareUpdates();
		accountsettingspage.ZebraLifeGuardOTA();
		accountsettingspage.CopyToClipboardButton();
		accountsettingspage.clickHome();
	}
	
	
	
	
	
	
	
	
	
	
	
}
