package Settings_TestScripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class SAMLSingleSignOn extends Initialization {

	
	@Test(priority=1,description="Verify SAML Single Sign-On") 
	public void VerifySAMLSingleSignOnTC_ST_256() throws Throwable {
		Reporter.log("\n1.Verify SAML Single Sign-On UI",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();	
		accountsettingspage.EnableSingleSignOnCheckboxVisible();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.SingleSignOnTypeOptionVisible();
		accountsettingspage.ServiceIdentifierOptionVisible();
		accountsettingspage.SignOnServiceUrlOptionVisible();
		accountsettingspage.LogoutServiceUrlOptionVisible();
		accountsettingspage.RolesOptionVisible();
		accountsettingspage.DeviceGroupSetOptionVisible();
		accountsettingspage.JobsProfilesFolderSetOptionVisible();
		accountsettingspage.DeleteCertificateButtonVisible();
		accountsettingspage.DownloadCertificateButtonVisible();
		accountsettingspage.DoneButtonVisible();
		accountsettingspage.SingleSignOnTypeDropDownValue();
}
	
	@Test(priority=2,description="Verify on selecting SSO type as Generic in SAML Single Sign On") 
	public void VerifyonselectingSSOtypeasGenericinSAMLSingleSignOnTC_ST_474() throws Throwable {
		Reporter.log("\n2.Verify on selecting SSO type as Generic in SAML Single Sign On",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();	
		accountsettingspage.EnableSingleSignOnCheckboxVisible();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.SelectGenericOption();	
		accountsettingspage.GenericExampleUrlVisible();
}
	                                
	@Test(priority=3,description="Verify Generic option in SAML Single Sign-On SSO Type") 
	public void VerifyGenericoptioninSAMLSingleSignOnSSOTypeTC_ST_473() throws Throwable {
		Reporter.log("\n3.Verify Generic option in SAML Single Sign-On SSO Type",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();	
		accountsettingspage.EnableSingleSignOnCheckboxVisible();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.SelectGenericOption();	
		accountsettingspage.GenericOptionVisible();		
	}	
		
	@Test(priority=4,description="Verify Upload Certificate UI in SAML Single Sign-On") 
	public void VerifyUploadCertificateUIinSAMLSingleSignOnTC_ST_259() throws Throwable {
		Reporter.log("\n4.Verify Upload Certificate UI in SAML Single Sign-On",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();			
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.DeleteVisible();
		accountsettingspage.UploadCertficateVisible();
		accountsettingspage.CertficateOptionVisible();
		accountsettingspage.PasswordOptionVisible();
		accountsettingspage.BrowserOptionVisible();
		accountsettingspage.CloseCertificateWindow();					
	}
		
	@Test(priority=5,description="Verify Upload Certificate in SAML Single Sign-On.") 
	public void VerifyUploadCertificateinSAMLSingleSignOnTC_ST_260() throws Throwable {
		Reporter.log("\n5.Verify Upload Certificate in SAML Single Sign-On.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();			
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.DeleteVisible();
		accountsettingspage.UploadCertficateVisible();
		accountsettingspage.ClickedonBrowserFile();
		accountsettingspage.browseFileSAMLSingleSignOn("./Uploads/UplaodFiles/SAMLCertificate/\\adfs.exe");			
	}
	
	@Test(priority=6,description="Verify Enable SAML Single Sign-On ") 
	public void VerifyEnableSAMLSingleSignOnTC_ST_257() throws Throwable {
		Reporter.log("\n6.Verify Enable SAML Single Sign-On ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.AdfsSelected();
		accountsettingspage.ServiceIdentiferADFS();
		accountsettingspage.SignOnServiceUrlADFS();
		accountsettingspage.LogoutServiceUrlADFS();		
	}
	
	@Test(priority=7,description="Verify Login URL for Single Sign-On users in SAML Single Sign-On.") 
	public void VerifyLoginURLforSingleSignOnusersinSAMLSingleSignOnTC_ST_261() throws Throwable {
		Reporter.log("\n7.Verify Login URL for Single Sign-On users in SAML Single Sign-On.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.AdfsSelected();
		accountsettingspage.ServiceIdentiferADFS();
		accountsettingspage.SignOnServiceUrlADFS();
		accountsettingspage.LogoutServiceUrlADFS();	
		accountsettingspage.LoginURLforSSOUser();		
	}
	
	@Test(priority=8,description="Verify Logout from URL for Single Sign-On users in SAML Single Sign-On") 
	public void VerifyLogoutfromURLforSingleSignOnusersinSAMLSingleSignOnTC_ST_262() throws Throwable {
		Reporter.log("\n8.Verify Logout from URL for Single Sign-On users in SAML Single Sign-On",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.AdfsSelected();
		accountsettingspage.ServiceIdentiferADFS();
		accountsettingspage.SignOnServiceUrlADFS();
		accountsettingspage.LogoutServiceUrlADFS();	
		accountsettingspage.LoginLogoutforSSOUser();		
	}
	
	@Test(priority=9,description="Verify Configuration of other SSO Types in SAML Single-On") 
	public void VerifyConfigurationofotherSSOTypesinSAMLSingleOnTC_ST_263() throws Throwable {
		Reporter.log("\n9.Verify Configuration of other SSO Types in SAML Single-On",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickedonUserManagement();
		accountsettingspage.ClickonSuperuser();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.AdfsSelected();
		accountsettingspage.ServiceIdentiferADFS();
		accountsettingspage.SignOnServiceUrlADFS();
		accountsettingspage.LogoutServiceUrlADFS();	
		accountsettingspage.ValidatingSuperUser();		
	}
	
	@Test(priority=10,description="Verify Modification of user Permission for SSO User") 
	public void VerifyModificationofuserPermissionforSSOUserTC_ST_264() throws Throwable {
		Reporter.log("\n10.Verify Modification of user Permission for SSO User",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickedonUserManagement();		
		accountsettingspage.ModifiedRoles();
		accountsettingspage.SelectingAllPermissions("Group/Tag/Filter Permissions");
		accountsettingspage.SelectingAllPermissions("Device Action Permissions");
		accountsettingspage.SelectingAllPermissions("Device Management Permissions");
		accountsettingspage.SelectingAllPermissions("Application Settings Permissions");
		accountsettingspage.ClickedonRolesSave();		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();
		accountsettingspage.selectEnableSSOcheckbox();
		accountsettingspage.AdfsSelected();
		accountsettingspage.ServiceIdentiferADFS();
		accountsettingspage.SignOnServiceUrlADFS();
		accountsettingspage.LogoutServiceUrlADFS();	
		accountsettingspage.LoginLogoutforSSOuserVerifyRoles();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickedonUserManagement();
		accountsettingspage.ModifiedRoles();
		accountsettingspage.VerifyOptionForSSOuser(accountsettingspage.GroupTagFilterPermissions,"Group/Tag/Filter Permissions");
		accountsettingspage.VerifyOptionForSSOuser(accountsettingspage.DeviceActionPermissions,"Device Action Permissions");
		accountsettingspage.VerifyOptionForSSOuser(accountsettingspage.DeviceManagementPermissions,"Device Management Permissions");
		accountsettingspage.VerifyOptionForSSOuser(accountsettingspage.ApplicationSettingsPermissions,"Application Settings Permissions");
		accountsettingspage.SelectingAllPermissions("Group/Tag/Filter Permissions");
		accountsettingspage.SelectingAllPermissions("Device Action Permissions");
		accountsettingspage.SelectingAllPermissions("Device Management Permissions");
		accountsettingspage.SelectingAllPermissions("Application Settings Permissions");
		accountsettingspage.ClickedonRolesSave();
		accountsettingspage.Return();
	}
		
	@Test(priority=11,description="Verify Disable SAML Single Sign-On in SSO Login") 
	public void VerifyDisableSAMLSingleSignOninSSOLoginTC_ST_265() throws Throwable {
		Reporter.log("\n11.Verify Disable SAML Single Sign-On in SSO Login",true);		
		commonmethdpage.ClickOnSettings();		
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonSAMLsinglebutton();
		accountsettingspage.SelectDisbaleSSOcheckbox();
		accountsettingspage.SaveDisbaleSSOcheckbox();
		
	}
	
	
	
	
	
	

	
}
