package PageObjectRepository;

import java.io.BufferedReader;
import java.io.File;
	import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
	import java.net.URL;
	import java.sql.Driver;
	import java.text.DateFormat;
	import java.text.SimpleDateFormat;
	import java.time.LocalDate;
	import java.time.format.DateTimeFormatter;
	import java.util.Calendar;
	import java.util.Date;
	import java.util.List;
	import java.util.concurrent.TimeUnit;

	import javax.xml.xpath.XPath;

	import org.apache.poi.EncryptedDocumentException;
	import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
	import org.openqa.selenium.By;
	import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.remote.BrowserType;
	import org.openqa.selenium.remote.DesiredCapabilities;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.Select;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.Assert;
	import org.testng.Reporter;
	import org.testng.annotations.Parameters;
	import org.testng.asserts.SoftAssert;

	import Common.Initialization;
	import Library.AssertLib;
	import Library.Config;
	import Library.ExcelLib;
	import Library.WebDriverCommonLib;
import io.appium.java_client.android.AndroidDriver;
	import io.appium.java_client.remote.MobileCapabilityType;

	public class DynamicJobs_POM extends WebDriverCommonLib
	{
		public static int FirstIntervalCount;
		public static int SecondIntervalCount;
		public static int FirstIntervalSMSCount;
		public static int SecondIntervalSMSCount;

		AssertLib ALib=new AssertLib();
		ExcelLib ELib=new ExcelLib();
		JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
		CommonMethods_POM common=new CommonMethods_POM();

		@FindBy(id="appBtn2")
		private WebElement MoreBtn;

		public void ClickOnMoreOption() throws InterruptedException
		{
			MoreBtn.click();
			waitForidPresent("callLog");
			sleep(7);
		}

		@FindBy(id="remoteButton")
		private WebElement RemoteBtn;

		//Dynamic Message
		@FindBy(id="messageDeviceBtn")
		private WebElement MessageBtn;

		@FindBy(xpath="//h4[text()='Send Message']/preceding-sibling::button")
		private WebElement MessageCloseBtn;

		@FindBy(xpath=".//*[@id='message_modal']/div/div/div[1]/h4")
		private WebElement MessageHeader;

		@FindBy(id="deviceMessageSubject")
		private WebElement SubjectTextField;

		@FindBy(id="deviceMessageBody")
		private WebElement BodyTextField;

		@FindBy(id="deviceMessageReadNotification")
		private WebElement GetReadNotificationRadioBtn;

		@FindBy(id="deviceMessageForceRead")
		private WebElement ForceReadMessageRadioBtn;

		@FindBy(xpath="//input[@value='Send']")
		private WebElement SendBtn;

//		@FindBy(xpath="//span[contains (text(),'Dynamic message')]")
//		private WebElement ConfirmationMessage;

		@FindBy(xpath="//span[contains(text(),'The dynamic message job will arrive on the device the next time it comes online. If it is currently online, the message has been sent.')]")
		private WebElement ConfirmationMessage;

		@FindBy(xpath="//span[text()='Cannot send message without Subject and Message.']")
		private WebElement ErrorMessageBody;

		@FindBy(xpath="//span[text()='Cannot send message without Subject and Message.']")
		private WebElement ErrorMessageSubject;

//		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job TEXT_MESSAGE on device("+Config.DeviceName+").')]")
//		private List<WebElement> MessageDeviceActivityLogs;
		@FindBy(xpath="//p[contains(text(),'has initiated a dynamic job known as \"TEXT_MESSAGE\" on device named "+"\""+Config.DeviceName+"\".')]")
		private WebElement MessageDeviceActivityLogs;

		public void ClickOnMessage() throws InterruptedException
		{
			MessageBtn.click();
			waitForidPresent("deviceMessageSubject");
			sleep(2);
			Reporter.log("Clicked on Dynamic Message Job, Navigates successfully to Dynamic Message Popup",true);
		}

		public void ClickOnMessageCloseBtn() throws InterruptedException
		{
			MessageCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(2);
			Reporter.log("Clicked on Close button of Dynamic Message Job, Navigates successfully to Home Page",true);
		}

		public void ClickOnMessage_UM()
		{
			MessageBtn.click();
		}

		public void VerifyOfSendMessageWindow()
		{
			String ActualValue=MessageHeader.getText();
			String ExpectedValue="Send Message";
			String PassStatement = "PASS >> "+ActualValue+" is displayed successfully when Clicked on Dynamic Message Job";
			String FailStatement = "Fail >> "+ActualValue+" is not displayed when Clicked on Dynamic Message Job";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value = SubjectTextField.isDisplayed();
			PassStatement = "PASS >> Subject Textbox is displayed successfully when Clicked on Dynamic Message Job";
			FailStatement = "Fail >> Subject Textbox is not displayed even when Clicked on Dynamic Message Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value = BodyTextField.isDisplayed();
			PassStatement = "PASS >> Body Textbox is displayed successfully when Clicked on Dynamic Message Job";
			FailStatement = "Fail >> Body Textbox is not displayed even when Clicked on Dynamic Message Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value = GetReadNotificationRadioBtn.isEnabled();
			PassStatement = "PASS >> Get Read Notification radiobutton is displayed successfully when Clicked on Dynamic Message Job";
			FailStatement = "Fail >> Get Read Notification radiobutton is not displayed even when Clicked on Dynamic Message Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value = ForceReadMessageRadioBtn.isEnabled();
			PassStatement = "PASS >> Force Read Message radiobutton is displayed successfully when Clicked on Dynamic Message Job";
			FailStatement = "Fail >> Force Read Message radiobutton is not displayed even when Clicked on Dynamic Message Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void EnterMessageSubject() throws EncryptedDocumentException, InvalidFormatException, IOException{
			SubjectTextField.sendKeys("Suremdm");
		}

		public void EnterMessageBody() throws EncryptedDocumentException, InvalidFormatException, IOException{
			BodyTextField.sendKeys("Suremdm");
		}

		public void ClickOnForceReadMessage()
		{
			ForceReadMessageRadioBtn.click();
			Reporter.log("Clicked on Force Read Message Radio button of Dynamic Message Job",true);
		}

		public void ClickOnGetReadNotification()
		{
			GetReadNotificationRadioBtn.click();
			Reporter.log("Clicked on Force Read Message Radio button of Dynamic Message Job",true);
		}

		public void ClickOnSend()
		{
			SendBtn.click();
			Reporter.log("Clicked on Send button of Dynamic Message Job",true);

		}

/*		public void ConfirmationMessageVerify() throws InterruptedException{
			waitForXpathPresent("//span[contains (text(),'Dynamic message')]");
			String PassStatement="PASS >> 'Successfully delivered Message to device.' Message is displayed successfully";
			String FailStatement="FAIL >> 'Successfully delivered Message to device.' Message is not displayed";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessage, true, PassStatement, FailStatement,5);
		}
*/		public void ConfirmationMessageVerify() throws InterruptedException{
			waitForXpathPresent("//span[contains(text(),'The dynamic message job will arrive on the device the next time it comes online. If it is currently online, the message has been sent.')]");
			String PassStatement="PASS >> 'The dynamic message job will arrive on the device the next time it comes online. If it is currently online, the message has been sent.' Message is displayed successfully";
			String FailStatement="FAIL >> 'The dynamic message job will arrive on the device the next time it comes online. If it is currently online, the message has been sent.' Message is not displayed";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void ErrorMessageBodyVerify() throws InterruptedException{
			String PassStatement="PASS >> Alert Message 'Cannot send message without body.' Message is displayed successfully when Body field is empty";
			String FailStatement="FAIL >> Alert Message 'Cannot send message without body.' Message is not displayed even when Body field is empty";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageBody, true, PassStatement, FailStatement,5);
		}

		public void ErrorMessageSubjectVerify() throws InterruptedException{
			String PassStatement="PASS >> Alert Message 'Cannot send message without subject.' Message is displayed successfully when Body field is empty";
			String FailStatement="FAIL >> Alert Message 'Cannot send message without subject.' Message is not displayed even when Body field is empty";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageSubject, true, PassStatement, FailStatement,5);
		}

		public void VerifyOfMessageActivityLogs(String DeviceName)throws InterruptedException
		{
			boolean value=MessageDeviceActivityLogs.isDisplayed();
			String PassStatement="PASS >> Activity Logs for Message is displayed successfully when clicked on Dynamic Message Job as ";
			String FailStatement="FAIL >> Activity Logs for Message is not displayed even when clicked on Message"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}	

		//Reboot Device
		@FindBy(id="rebootDevice")
		private WebElement RebootDeviceBtn;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='No']")
		private WebElement RebootLockWipeBuzzDeviceNoBtn;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='Yes']")
		private WebElement RebootLockWipeBuzzDeviceYesBtn;

		@FindBy(xpath="//span[text()='Device Reboot initiated.']")
        private WebElement RebootDeviceConfirmationMessage;

		@FindBy(xpath="//span[text()='Device Reboot initiated.']")
		private WebElement RebootDeviceConfirmationMessage_Knox;

		@FindBy(xpath="//span[contains(text(),'Device Reboot initiated.')]")
		private WebElement RebootDeviceActivityLogs;

		@FindBy(xpath="//div[@id='jobQueueButton']")
		private WebElement JobQueueButton;

		@FindBy(xpath="//table[@id='jobQueueDataDynamicGrid']/tbody/tr[1]/td[3]")
		private WebElement JobStatus;

		@FindBy(xpath="(//p[@id='jobComments_undefined'])[1]")
		private WebElement StatusMessage;

		public void ClickOnRebootDevice_UM()
		{
			RebootDeviceBtn.click();
		}

		public void ClickOnRebootDevice() throws InterruptedException
		{
			RebootDeviceBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]");
			sleep(3);
			Reporter.log("Clicked on Reboot Dynamic Job, Reboot Job popup is opened",true);
		}

		public void ClickOnRebootLockWipeBuzzDeviceNoBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceNoBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(4);
		}

		public void VerifyOfRebootDevicePopup()
		{
			boolean flag;
			String ActualValue=LockDevicePopup.getText();
			if(LockDevicePopup.isDisplayed())
				flag=true;
			else
				flag=false;
			String PassStatement="PASS >> "+ActualValue+" Message is displayed successfully when clicked on Reboot";
			String FailStatement="FAIL >> "+ActualValue+" Message is not displayed even when clicked on Reboot"; 	
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
			boolean value=RebootLockWipeBuzzDeviceNoBtn.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on Reboot";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on Reboot"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=RebootLockWipeBuzzDeviceYesBtn.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Reboot";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Reboot"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnRebootDeviceNoBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceNoBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(4);
			Reporter.log("Clicked on No button of Reboot Job popup",true);
		}

		public void ClickOnRebootDeviceYesBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceYesBtn.click();
			Reporter.log("Clicked on Yes button of Reboot Job popup",true);
			waitForXpathPresent("//span[contains(text(),'Device Reboot initiated.')]");
		}	
		public void ConfirmationMessageRebootDeviceVerify_Knox() throws InterruptedException
		{

			String PassStatement="PASS >> 'Initiating reboot on the device' Message is displayed successfully when clicked on yes button of Dynamic Reboot Job";
			String FailStatement="FAIL >> 'Initiating reboot on the device' Message is not displayed even when clicked on yes button of Dynamic Reboot Job";
			Initialization.commonmethdpage.ConfirmationMessageVerify(RebootDeviceConfirmationMessage_Knox, true, PassStatement, FailStatement,5);
		}

		public void ConfirmationMessageRebootDeviceVerify() throws InterruptedException{
            
            boolean message = RebootDeviceConfirmationMessage.isDisplayed();
            String pass = "PASS >> Device Reboot initiated. message is displayed";
            String fail = "FAIL >> Device Reboot initialted. messafe is Not Displayed";
            ALib.AssertTrueMethod(message, pass, fail);
             
    } 
		
		public void VerifyingJobStatusInJobQueue() throws InterruptedException
		{
			boolean flag;
			while(Initialization.driver.findElement(By.xpath("//div[@class='lds-ring']/div[4]")).isDisplayed())
			{
			}
			sleep(7);
			JobQueueButton.click();
			waitForXpathPresent("//div[@id='jobQueueRefreshDynamicBtn']");
			sleep(4);
			Initialization.driver.findElement(By.xpath("//li[@id='dynamicJObqueueSection']")).click();	    
			waitForXpathPresent("//div[@id='jobQueueRefreshDynamicBtn']");
			sleep(3);
			String jobStatus = JobStatus.getText();
			if(jobStatus.equals("Error"))
			{
				flag=true;
				String JobStatusMessage = StatusMessage.getText();
				Reporter.log(JobStatusMessage,true);	
			}
			else 
			{
				flag=false;
			}

			String PassStatement="PASS >> 'Enrolled Device Is Not An Rooted Or Knox Device";
			String FailStatement="FAIL >> 'Enrolled Device Is An Rooted Or Knox Device";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
			Initialization.driver.findElement(By.xpath("//div[@id='device_jobQ_modal']//div//button[@class='close']")).click();
		}

/*		public void ConfirmationMessageRebootDeviceVerify_False() throws InterruptedException{
			String PassStatement="PASS >> 'Device reboot is supported only on rooted Android devices.' Message is not displayed when clicked on No button of Dynamic Reboot Job";
			String FailStatement="FAIL >> 'Device reboot is supported only on rooted Android devices.' Message is displayed even when clicked on No button of Dynamic Reboot Job";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(RebootDeviceConfirmationMessage, false, PassStatement, FailStatement,5);
		}
		
*/	
		public void ConfirmationMessageRebootDeviceVerify_False() throws InterruptedException{

		boolean flag = false;
		try 
		{
			if(RebootDeviceConfirmationMessage.isDisplayed())
			{
				flag=true;
			}

		}
		catch(Exception e) 
		{
			flag=false;
		}
		String PassStatement="PASS >> No message is NOT displayed";
		String FailStatement="FAIL >>  message is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
		}
		
		
		public void VerifyOfRebootActivityLogs()
		{
			try {
				RebootDeviceActivityLogs.isDisplayed();
				Reporter.log("PASS >> 'Device Reboot initiated' is displayed successfully when clicked on Dynamic Reboot",true);
			} catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> 'Device Reboot initiated' isn't displayed successfully when clicked on Dynamic Reboot");
			}
		}




		//Lock Device
		@FindBy(id="lockDeviceBtn")
		private WebElement LockDeviceBtn;

		@FindBy(xpath="//*[@id='deviceConfirmationDialog']/div/div/div[1]/p")
		private WebElement LockDevicePopup;

//		@FindBy(xpath="//span[text()='Device lock initiated.']")
//		private WebElement LockDeviceConfirmationMessage;

		@FindBy(xpath="//span[text()='The device is now locked.']")
		private WebElement LockDeviceConfirmationMessage;

//		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job LOCK on device("+Config.DeviceName+").')]")
//		private List<WebElement> LockDeviceActivityLogs;

		@FindBy(xpath="//p[contains(text(),'has initiated a dynamic job known as \"LOCK\" on device named "+"\""+Config.DeviceName+"\".')]")
		private List<WebElement> LockDeviceActivityLogs;

		public void ClickOnLockDevice_UM()
		{
			LockDeviceBtn.click();
		}

		public void ClickOnLockDevice() throws InterruptedException
		{
			LockDeviceBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]");
			sleep(3);
			Reporter.log("Clicked on Lock Dynamic Job, Lock Job popup is opened",true);
		}

		public void VerifyOfLockDevicePopup()
		{
			String ActualValue=LockDevicePopup.getText();
			String ExpectedValue="Selected device will be locked. Do you wish to continue?";
			String PassStatement="PASS >> "+ActualValue+" Message is displayed successfully when clicked on Lock";
			String FailStatement="FAIL >> "+ActualValue+" Message is not displayed even when clicked on Lock"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=RebootLockWipeBuzzDeviceNoBtn.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on Lock";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on Lock"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=RebootLockWipeBuzzDeviceYesBtn.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Lock";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Lock"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnLockDeviceNoBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceNoBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(3);
			Reporter.log("Clicked on No button of Lock Job popup",true);
		}

		public void ClickOnLockDeviceYesBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceYesBtn.click();
			Reporter.log("Clicked on Yes button of Lock Job popup",true);
		}

		public void ConfirmationMessageLockDeviceVerify() throws InterruptedException{//  //Device locked.
			String PassStatement="PASS >> 'The device is now locked.' Message is displayed successfully when clicked on yes button of Dynamic Lock Job";
			String FailStatement="FAIL >> 'The device is now locked.' Message is not displayed even when clicked on yes button of Dynamic Lock Job";
			Initialization.commonmethdpage.ConfirmationMessageVerify(LockDeviceConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void ConfirmationMessageLockDeviceVerify_False() throws InterruptedException{
		     try {
					if(LockDeviceConfirmationMessage.isDisplayed()) {

						ALib.AssertFailMethod("FAIL >> 'Device locked.' Message is displayed even when clicked on No button of Dynamic Lock Job");
					}
					}catch(Exception e) {
						Reporter.log("PASS >> 'Device locked.' Message is not displayed when clicked on No button of Dynamic Lock Job", true);
					}
		}

		public void VerifyOfLockActivityLogs()
		{
			boolean value=LockDeviceActivityLogs.get(0).isDisplayed();
			String ActualValue=LockDeviceActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Lock is displayed successfully when clicked on Dynamic Lock Job as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Lock is not displayed even when clicked on Lock"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}





		//Wipe Device
		@FindBy(id="wipeDevice")
		private WebElement WipeDeviceBtn;

		@FindBy(xpath="//span[text()='Device wiped successfully.']")
		private WebElement WipeDeviceConfirmationMessage;

		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job WIPE on device("+Config.DeviceName+").')]")
		private List<WebElement> WipeDeviceActivityLogs;

		public void ClickOnWipeDevice_UM()
		{
			WipeDeviceBtn.click();
		}

		public void ClickOnWipeDevice() throws InterruptedException
		{
			WipeDeviceBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]");
			sleep(3);
		}

		public void VerifyOfWipeDevicePopup()
		{
			String ActualValue=LockDevicePopup.getText();
			String ExpectedValue="Selected device will be wiped. Do you wish to continue?";
			String PassStatement="PASS >> "+ActualValue+" Message is displayed successfully when clicked on Wipe";
			String FailStatement="FAIL >> "+ActualValue+" Message is not displayed even when clicked on Wipe"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=RebootLockWipeBuzzDeviceNoBtn.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on Wipe";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on Wipe"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=RebootLockWipeBuzzDeviceYesBtn.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Wipe";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Wipe"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnWipeDeviceNoBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceNoBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(4);
			Reporter.log("Clicked on No button of Wipe Job popup",true);
		}

		public void ClickOnWipeDeviceYesBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceYesBtn.click();
			Reporter.log("Clicked on Yes button of Wipe Job popup",true);
		}

		public void ConfirmationMessageWipeDeviceVerify() throws InterruptedException{
			String PassStatement="PASS >> 'Device wiped successfully.' Message is displayed successfully when clicked on yes button of Dynamic Wipe Job";
			String FailStatement="FAIL >> 'Device wiped successfully.' Message is not displayed even when clicked on yes button of Dynamic Wipe Job";
			Initialization.commonmethdpage.ConfirmationMessageVerify(LockDeviceConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void ConfirmationMessageWipeDeviceVerify_False() throws InterruptedException{
			String PassStatement="PASS >> 'Device wiped successfully.' Message is not displayed when clicked on No button of Dynamic Wipe Job";
			String FailStatement="FAIL >> 'Device wiped successfully.' Message is displayed even when clicked on No button of Dynamic Wipe Job";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(LockDeviceConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void VerifyOfWipeActivityLogs()
		{
			boolean value=WipeDeviceActivityLogs.get(0).isDisplayed();
			String ActualValue=WipeDeviceActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Wipe is displayed successfully when clicked on Dynamic Wipe Job as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Wipe is not displayed even when clicked on Wipe"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}




		//Call Log
		@FindBy(id="callLog")
		private WebElement CallLogBtn;

		@FindBy(id="callLogModalTitle")
		private WebElement CallLogHeader;

		@FindBy(id="all_call")
		private WebElement CallLogAllTab;

		@FindBy(id="incoming_call")
		private WebElement CallLogIncomingTab;

		@FindBy(id="outgoing_call")
		private WebElement CallLogOutgoingTab;

		@FindBy(id="missed_call")
		private WebElement CallLogMissedTab;

		@FindBy(id="rejected_call")
		private WebElement CallLogRejectedTab;

		@FindBy(id="call_logs_refresh")
		private WebElement CallLogRefresh;

		@FindBy(id="call_logs_export")
		private WebElement CallLogExport;

		@FindBy(xpath="//h4[@id='callLogModalTitle']/preceding-sibling::button")
		private WebElement CallLogCloseBtn;

		@FindBy(xpath="//div[@id='call_Log_panel']/div/div/div/table[@id='devicecalllogtable']/thead/tr/th/div[1]")
		private List<WebElement> CallLogColumn;

		@FindBy(id="callloginterval")
		private WebElement CallLogInterval;

		@FindBy(id="calllogstatus")
		private WebElement CallLogStatus;

		@FindBy(xpath="//input[@class='btn okbutton addbutton buttonwidth']")
		private WebElement CallLogOkBtn;

		@FindBy(xpath="//input[@class='btn smdm_btns smdm_red_clr deletebutton buttonwidth']")
		private WebElement CallLogCancelBtn;

		@FindBy(xpath="//span[text()='Interval should be greater than 14.']")
		private WebElement CallLogErrorMessage;

//		@FindBy(xpath="//span[text()='Call logs tracking updated successfully.']")
//		private WebElement CallLogConfirmationMessage;

		@FindBy(xpath="//span[text()='Call log tracking settings updated successfully.']")
		private WebElement CallLogConfirmationMessage;

		@FindBy(xpath="//div[contains(text(),'No Data available.')]")
		private WebElement GridMessage;

		@FindBy(xpath="//table[@id='devicecalllogtable']/tbody/tr[1]")
		private WebElement CallLogUpdate;

		@FindBy(xpath="//table[@id='devicecalllogtable']/tbody/tr/td[1]/img")
		private List<WebElement> ImageCallLogUpdate;

//		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job APPLY_CALL_TRACKING on device("+Config.DeviceName+").')]")
//		private List<WebElement> CallLogsDeviceActivityLogs;
		
		@FindBy(xpath="//p[contains(text(),'has initiated a dynamic job known as \"APPLY_CALL_TRACKING\" on device named "+"\""+Config.DeviceName+"\".')]")
		private List<WebElement> CallLogsDeviceActivityLogs;

		public void ClickOnCallLog_UM()
		{
			CallLogBtn.click();
		}

		public void ClickOnCallLog() throws InterruptedException
		{
			CallLogBtn.click();
			waitForidPresent("calllogstatus");
			sleep(5);
			Reporter.log("Clicked on Call Log, Naviagted successfully to Call Log Popup",true);
		}

		public void RebootDevice() throws IOException, InterruptedException
		{
			Runtime.getRuntime().exec("adb reboot");
			Reporter.log("Device Reboot initiated");
			sleep(70);
		}
		
		@FindBy(xpath="//*[@id='applistDetailsAndroid']/div[1]/button")
		private WebElement closeDynamicApps;
		public void clickOnCloseDynamicApps() {
			closeDynamicApps.click();
		}
		
		@FindBy(xpath="(//*[@id='applock_com.android.vending'])[1]")
		private WebElement LockCheckBox;
		@FindBy(xpath="(//*[@id='applock_com.android.chrome'])[1]")
		private WebElement LockCheckBoxChrome;
		public void ClickOnLockCheckBox(String AppID) throws InterruptedException
		{ 
			sleep(2);
			waitForXpathPresent("(//*[@id='"+AppID+"'])[1]");
			LockCheckBox.click();
				sleep(2);
			}

		public void UnlockChrome(String AppID) throws InterruptedException {
			sleep(2);
			waitForXpathPresent("(//*[@id='"+AppID+"'])[1]");
			LockCheckBoxChrome.click();
			sleep(2);
		}
		public void verifyClearData() throws IOException, InterruptedException
		{
			try {
				
			Initialization.driverAppium.findElementById("com.estrongs.android.pop:id/txt_grant").isDisplayed();}
			catch(Exception e ) {
				ALib.AssertFailMethod("Clear Data Job is not working properly");
			}
		}

		public void VerifyClearData(String FirstButtonName)
		{
			boolean agreeButton = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+FirstButtonName+"']").isDisplayed();
			String pass ="PASS>> First Button is displayed on Launch: Clear Data is successful";
			String fail ="FAIL>> First Button is NOT displyed: Clear Data is failed";
			ALib.AssertTrueMethod(agreeButton, pass, fail);
		}
		
		public void GetTopActivity() throws IOException, InterruptedException
		{
			try {
				Process process = Runtime.getRuntime().exec("adb shell dumpsys \"activity activities | grep mFocusedActivity\"");
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(process.getInputStream()));

				StringBuilder result=new StringBuilder();
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					result.append(line);
				}
				int indexOfSlash = result.toString().indexOf("/");
				if(indexOfSlash != -1)
				{
					String newResult = result.toString().substring(0, indexOfSlash);
					String actualResult= newResult.substring(newResult.lastIndexOf(" "));
					System.out.println("actual result: " + actualResult);
					String expectedResult= " com.estrongs.android.pop";
					String pass = "PASS >> On Focus Package name is matched: ES File Explorer Run At StartUp Works";
					String fail = "FAIL >> On Focus Package name is Note matched: ES File Explorer Run At StartUp does not Work";
					ALib.AssertEqualsMethod(expectedResult, actualResult, pass, fail);
				}
			} 
			catch (IOException e) {}
		}

		
		public void ClickOnCallLogCloseBtn() throws InterruptedException
		{
			CallLogCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(3);
			Reporter.log("Clicked on Close button of Call Log, popup is closed successfully",true);
		}

		public void ClickOnCallLogOkBtn() throws InterruptedException
		{
			CallLogOkBtn.click();
			Reporter.log("Clicked on Ok button of Call Log, popup is closed successfully",true);
			sleep(2);
		}

		public void ClickOnCallLogCancelBtn() throws InterruptedException
		{
			CallLogCancelBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(1);
			Reporter.log("Clicked on Cancel button of Call Log, popup is closed successfully",true);
		}

		public void SelectCallLogStatus_On() throws InterruptedException
		{
			Select selCallLogstatus = new Select(CallLogStatus);
			selCallLogstatus.selectByVisibleText("On");
			sleep(2);
		}

		public void SelectCallLogStatus_Off()
		{
			Select selCallLogstatus = new Select(CallLogStatus);
			selCallLogstatus.selectByVisibleText("Off");
		}

		public void EnterCallLogInterval(String CallLogintervalNum)
		{
			CallLogInterval.sendKeys(CallLogintervalNum);
		}
		public void EnterCallLogInterval2()
		{
			CallLogInterval.sendKeys("15");
		}

		public void ClearCallLogInterval() throws InterruptedException
		{
			CallLogInterval.clear();
			sleep(2);
		}

		public void CallLogErrorMessage() throws InterruptedException
		{
			boolean flag;
			if(Initialization.driver.findElement(By.xpath("//span[text()='Interval should be greater than 14.']")).isDisplayed())
			{
				flag=true;
			}
			else
			{
				flag=false;
			}
			String PassStatement="PASS >> Alert Message : 'Interval should be greater than 14.' is displayed successfully when Call Log interval is less than 14";
			String FailStatement="Fail >> Alert Message : 'Interval should be greater than 14.' is not displayed even when Call Log interval is less than 14";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

		}

/*		public void CallLogConfirmationMessage() throws InterruptedException
		{

			String PassStatement="PASS >> Confirmation Message : 'Call logs tracking updated successfully.' is displayed successfully when Call Log interval is greater than 14";
			String FailStatement="Fail >> Confirmation Message : 'Call logs tracking updated successfully 14.' is not displayed even when Call Log interval is greater than 14";
			Initialization.commonmethdpage.ConfirmationMessageVerify(CallLogConfirmationMessage, true, PassStatement, FailStatement,7);
			sleep(5);
		}
*/		public void CallLogConfirmationMessage() throws InterruptedException
		{

			String PassStatement="PASS >> Confirmation Message : 'Call log tracking settings updated successfully.' is displayed successfully when Call Log interval is greater than 14";
			String FailStatement="Fail >> Confirmation Message : 'Call log tracking settings updated successfully.' is not displayed even when Call Log interval is greater than 14";
			Initialization.commonmethdpage.ConfirmationMessageVerify(CallLogConfirmationMessage, true, PassStatement, FailStatement,7);
			sleep(5);
		}


		public void VerifyOfCallLogWindow()
		{
			String ActualValue=CallLogHeader.getText();
			String ExpectedValue="Call Logs - "+Config.DeviceName;
			String PassStatement = "PASS >> Call Log Header with Device name is displayed successfully when Clicked on Dynamic Call Log Job as "+ActualValue;
			String FailStatement = "Fail >> Call Log Header with Device name is not displayed when Clicked on Dynamic Call Log Job as "+ActualValue;
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			WebElement[] CallLogWebelment={CallLogAllTab,CallLogIncomingTab,CallLogOutgoingTab,CallLogMissedTab,CallLogRejectedTab};
			String[] Text={"All","Incoming","Outgoing","Missed","Rejected"};
			for(int i=0;i<CallLogWebelment.length-1;i++)
			{
				boolean value = CallLogWebelment[i].isDisplayed();
				PassStatement = "PASS >> "+Text[i]+" CallLog Tab is displayed successfully when Clicked on Dynamic Call Log Job";
				FailStatement = "Fail >> "+Text[i]+" CallLog Tab is not displayed even when Clicked on Dynamic Call Log Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] CallLogbuton={CallLogRefresh,CallLogExport,CallLogOkBtn,CallLogCancelBtn};
			String[] Text1={"Refresh","Export","Ok","Cancel"};
			for(int i=0;i<CallLogbuton.length-1;i++)
			{
				boolean value = CallLogbuton[i].isDisplayed();
				PassStatement = "PASS >> "+Text1[i]+" Button is displayed successfully when Clicked on Dynamic Call Log Job";
				FailStatement = "Fail >> "+Text1[i]+" Button is not displayed even when Clicked on Dynamic Call Log Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			String[] Text2={"Type","Name","Number","Time","Duration (seconds)"};
			for(int i=0;i<CallLogColumn.size()-1;i++)
			{
				ActualValue = CallLogColumn.get(i).getAttribute("innerHTML");
				boolean value=ActualValue.equals(Text2[i]);
				PassStatement = "PASS >> "+ActualValue+" Column is displayed successfully when Clicked on Dynamic Call Log Job";
				FailStatement = "Fail >> "+ActualValue+" Column is not displayed even when Clicked on Dynamic Call Log Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			boolean value = CallLogInterval.isEnabled();
			PassStatement = "PASS >> Call Log On/Off Dropdown is displayed successfully when Clicked on Dynamic Call Log Job";
			FailStatement = "Fail >> Call Log On/Off Dropdown is not displayed even when Clicked on Dynamic Call Log Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value = CallLogStatus.isEnabled();
			PassStatement = "PASS >> Update interval textbox is displayed successfully when Clicked on Dynamic Call Log Job";
			FailStatement = "Fail >> Update interval textbox is not displayed even when Clicked on Dynamic Call Log Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfGridMessage()
		{
			boolean value=GridMessage.isDisplayed();
			String PassStatement = "PASS >> Grid Message 'No data avialable' is displayed successfully when Clicked on Dynamic Call Log Job";
			String FailStatement = "Fail >> Grid Message 'No data avialable' is not displayed even when Clicked on Dynamic Call Log Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		@FindBy(xpath="//div[@id='datausage_modal']//button[@aria-label='Close']")
        private WebElement DataUsageCloseBtn;
		
		public void VerifyOfUpdateIntervalGrayedOut()
		{
			String ActualValue = CallLogInterval.getAttribute("class");
			boolean value = ActualValue.contains("disabled");
			String PassStatement = "PASS >> Update Interval is grayed out successfully when Call Log is Off";
			String FailStatement = "Fail >> Update Interval is not grayed out even when Call Log is Off";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnRefreshInCallLogTab() throws InterruptedException
        {
                Initialization.driver.findElement(By.xpath("//span[@class='tit actionbuttontext'][contains(normalize-space(),'Refresh')]")).click();
                sleep(5);
        }
		public void VerifyOfUpdateInterval()
		{
			String ActualValue = CallLogInterval.getAttribute("class");
			boolean value = ActualValue.contains("disabled");
			String PassStatement = "PASS >> Update Interval is Enabled successfully when Call Log is On";
			String FailStatement = "Fail >> Update Interval is not Enabled even when Call Log is On";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfCallLogCancelButton(String CallLogIntervalNum)
		{
			String ActualValue = CallLogStatus.getAttribute("value");
			if(ActualValue.equals("1"))
				ActualValue="On";
			else
				ActualValue="Off";
			boolean value = ActualValue.equals("On");
			String PassStatement = "PASS >> Call Log is not saved when Call Log is On and clicked on Cancel Button";
			String FailStatement = "Fail >> Call Log is saved even when Call Log is On and clicked on Cancel Button";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			ActualValue = CallLogInterval.getAttribute("value");
			value = ActualValue.equals(CallLogIntervalNum);
			PassStatement = "PASS >> Update Interval is not saved when Call Log is On and clicked on Cancel Button";
			FailStatement = "Fail >> Update Interval is saved even when Call Log is On and clicked on Cancel Button";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfCallLogOkButton(String CallLogIntervalNum)
		{
			String ActualValue = CallLogStatus.getAttribute("value");
			if(ActualValue.equals("1"))
				ActualValue="On";
			else
				ActualValue="Off";
			boolean value = ActualValue.equals("On");
			String PassStatement = "PASS >> Call Log is not saved when Call Log is On and clicked on Cancel Button";
			String FailStatement = "Fail >> Call Log is saved even when Call Log is On and clicked on Cancel Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ActualValue = CallLogInterval.getAttribute("value");
			value = ActualValue.equals(CallLogIntervalNum);
			PassStatement = "PASS >> Update Interval is not saved when Call Log is On and clicked on Cancel Button";
			FailStatement = "Fail >> Update Interval is saved even when Call Log is On and clicked on Cancel Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void CallLogUpdate()
		{
			String ActualValue = CallLogUpdate.getAttribute("class");
			System.out.println(ActualValue);
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> Call Log is updated succesfully when Call Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> Call Log is not updated even when Call Log is On and clicked on Ok Button";
			SoftAssert softAssertion= new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
		}

		public void ClickOnExport() throws Throwable
		{
			String downloadPath = Config.downloadpath;
			File dir = new File(downloadPath);
			File[] dir_contents = dir.listFiles();
			int initial = 0;
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().startsWith("Call Logs") && dir_contents[i].getName().endsWith(".csv")) {
					initial++;
				}
			}

			CallLogExport.click();
			sleep(10);

			dir = new File(downloadPath);
			dir_contents = dir.listFiles();
			int result=0;
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().startsWith("Call Logs") && dir_contents[i].getName().endsWith(".csv")) {
					result++;
				}
			}
			boolean value=result>initial;
			String PassStatement="PASS >> Call Logs file is Exported successfully in csv Format";
			String FailStatement="Fail >> Call Logs file is not Exported in csv Format";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnIncomingTab() throws Throwable {
			CallLogIncomingTab.click();
			Reporter.log("Clicked on Incoming Tab, Navigated successfully to Incoming tab section", true);
			sleep(2);
		}

		public void VerifyOfIncomingCallLogUpdate()
		{
			String ActualValue = CallLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> Call Log is updated succesfully when Call Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> Call Log is not updated even when Call Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			if (value==true)
			{
				for(int i=1 ; i<ImageCallLogUpdate.size();i++)
				{
					ActualValue= ImageCallLogUpdate.get(i).getAttribute("src");
					value=ActualValue.contains("incoming.png");
					if(value==false)
					{
						PassStatement = "PASS >> Only Incoming Call Log is updated succesfully when Call Log is On and clicked on Incoming Tab section";
						FailStatement = "Fail >> Only Incoming Call is not updated even when Call Log is On and clicked on Incoming Tab section";
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}

				PassStatement = "PASS >> Only Incoming Call Log is updated succesfully when Call Log is On and clicked on Incoming Tab section";
				FailStatement = "Fail >> Only Incoming Call is not updated even when Call Log is On and clicked on Incoming Tab section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void ClickOnOutgoingTab() throws Throwable {
			CallLogOutgoingTab.click();
			Reporter.log("Clicked on Incoming Tab, Navigated successfully to Outgoing tab section", true);
			sleep(2);
		}

		public void VerifyOfOutgoingCallLogUpdate()
		{
			String ActualValue = CallLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> Call Log is updated succesfully when Call Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> Call Log is not updated even when Call Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			if (value==true)
			{
				for(int i=1 ; i<ImageCallLogUpdate.size();i++)
				{
					ActualValue= ImageCallLogUpdate.get(i).getAttribute("src");
					value=ActualValue.contains("outgoing.png");
					if(value==false)
					{
						PassStatement = "PASS >> Only Outgoing Call Log is updated succesfully when Call Log is On and clicked on Outgoing Tab section";
						FailStatement = "Fail >> Only Outgoing is not updated even when Call Log is On and clicked on Outgoing Tab section";
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}

				PassStatement = "PASS >> Only Outgoing Call Log is updated succesfully when Call Log is On and clicked on Outgoing Tab section";
				FailStatement = "Fail >> Only Outgoing is not updated even when Call Log is On and clicked on Outgoing Tab section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void ClickOnMissedTab() throws Throwable 
		{
			CallLogMissedTab.click();
			Reporter.log("Clicked on Incoming Tab, Navigated successfully to Missed tab section", true);
			sleep(2);
		}

		public void VerifyOfMissedCallLogUpdate()
		{
			String ActualValue = CallLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> Call Log is updated succesfully when Call Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> Call Log is not updated even when Call Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			if (value==true)
			{
				for(int i=1 ; i<ImageCallLogUpdate.size();i++)
				{
					ActualValue= ImageCallLogUpdate.get(i).getAttribute("src");
					value=ActualValue.contains("missed.png");
					if(value==false)
					{
						PassStatement = "PASS >> Only Missed Call Log is updated succesfully when Call Log is On and clicked on Missed Tab section";
						FailStatement = "Fail >> Only Missed is not updated even when Call Log is On and clicked on Missed Tab section";
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}

				PassStatement = "PASS >> Only Missed Call Log is updated succesfully when Call Log is On and clicked on Missed Tab section";
				FailStatement = "Fail >> Only Missed is not updated even when Call Log is On and clicked on Missed Tab section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void ClickOnRejectedTab() throws Throwable {
			CallLogRejectedTab.click();
			Reporter.log("Clicked on Rejected Tab, Navigated successfully to Rejected tab section", true);
			sleep(2);
		}

		public void VerifyOfRejectedCallLogUpdate()
		{
			String ActualValue = CallLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> Call Log is updated succesfully when Call Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> Call Log is not updated even when Call Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			if (value==true)
			{
				for(int i=1 ; i<ImageCallLogUpdate.size();i++)
				{
					ActualValue= ImageCallLogUpdate.get(i).getAttribute("src");
					value=ActualValue.contains("rejected.png");
					if(value==false)
					{
						PassStatement = "PASS >> Only Rejected Call Log is updated succesfully when Call Log is On and clicked on Rejected Tab section";
						FailStatement = "Fail >> Only Rejected is not updated even when Call Log is On and clicked on Rejected Tab section";
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}
				PassStatement = "PASS >> Only Rejected Call Log is updated succesfully when Call Log is On and clicked on Rejected Tab section";
				FailStatement = "Fail >> Only Rejected is not updated even when Call Log is On and clicked on Rejected Tab section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}

		}

		public void VerifyOfCallLogsActivityLogs()
		{
			boolean value=CallLogsDeviceActivityLogs.get(0).isDisplayed();
			String ActualValue=CallLogsDeviceActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for CallLogs is displayed successfully when clicked on Dynamic CallLogs Job as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for CallLogs is not displayed even when clicked on CallLogs"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}







		//SMS Logs
		@FindBy(id="smsLog")
		private WebElement SMSLogBtn;

		@FindBy(xpath="//h4[@id='smsLogModalTitle']/preceding-sibling::button")
		private WebElement SMSLogCloseBtn;

		@FindBy(id="smsLogModalTitle")
		private WebElement SMSLogHeader;

		@FindBy(id="all_sms")
		private WebElement SMSLogAllTab;

		@FindBy(id="inbox")
		private WebElement SMSLogInboxTab;

		@FindBy(id="sent")
		private WebElement SMSLogSentTab;

		@FindBy(id="draft")
		private WebElement SMSLogDraftTab;

		@FindBy(id="outbox")
		private WebElement SMSLogOutboxTab;

		@FindBy(id="sms_logs_refresh")
		private WebElement SMSLogRefresh;

		@FindBy(id="sms_logs_export")
		private WebElement SMSLogExport;

		@FindBy(xpath="//table[@id='devicesmslogtable']/thead/tr/th/div[1]")
		private List<WebElement> SMSLogColumn;

		@FindBy(id="smsloginterval")
		private WebElement SMSLogInterval;

		@FindBy(id="smslogstatus")
		private WebElement SMSLogStatus;

		@FindBy(xpath="//input[@class='btn smdm_btns smdm_grn_clr okbutton addbutton buttonwidth']")
		private WebElement SMSLogOkBtn;

		@FindBy(xpath="//input[@class='btn smdm_btns smdm_red_clr deletebutton buttonwidth']")
		private WebElement SMSLogCancelBtn;

		@FindBy(xpath="//span[text()='Interval should be greater than 14.']")
		private WebElement SMSLogErrorMessage;

		@FindBy(xpath="//span[text()='SMS log tracking settings updated successfully.']")
		private WebElement SMSLogConfirmationMessage;

		@FindBy(xpath="//table[@id='devicesmslogtable']/tbody/tr[1]")
		private WebElement SMSLogUpdate;

		@FindBy(xpath="//table[@id='devicesmslogtable']/tbody/tr/td[1]/img")
		private List<WebElement> ImageSMSLogUpdate;

//		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job APPLY_SMS_TRACKING on device("+Config.DeviceName+").')]")
//		private List<WebElement> SMSLogsDeviceActivityLogs;
		
		@FindBy(xpath="//p[contains(text(),'has initiated a dynamic job known as \"APPLY_SMS_TRACKING\" on device named "+"\""+Config.DeviceName+"\".')]")
		private List<WebElement> SMSLogsDeviceActivityLogs;


		public void ClickOnSMSLog_UM()
		{
			SMSLogBtn.click();
		}

		public void ClickOnSMSLog() throws InterruptedException
		{
			SMSLogBtn.click();
			waitForidPresent("smslogstatus");
			sleep(4);
			Reporter.log("Clicked on SMS Log, Naviagted successfully to SMS Log Popup",true);
		}

		public void ClickOnSMSLogCloseBtn() throws InterruptedException
		{
			SMSLogCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Reporter.log("Clicked on Close button of SMS Log, popup is closed successfully",true);
			sleep(2);
		}

		public void ClickOnSMSLogOkBtn() throws InterruptedException
		{
			SMSLogOkBtn.click();
			Reporter.log("Clicked on Ok button of SMS Log, popup is closed successfully",true);
		}

		public void ClickOnSMSLogCancelBtn() throws InterruptedException
		{
			SMSLogCancelBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(1);
			Reporter.log("Clicked on Cancel button of SMS Log, popup is closed successfully",true);
		}

		public void SelectSMSLogStatus_On()
		{
			Select selSMSLogstatus = new Select(SMSLogStatus);
			selSMSLogstatus.selectByVisibleText("On");
		}

		public void SelectSMSLogStatus_Off() throws InterruptedException
		{
			Select selSMSLogstatus = new Select(SMSLogStatus);
			selSMSLogstatus.selectByVisibleText("Off");
			sleep(3);
		}
		public void TurningOffSMSLogs() throws InterruptedException
		{
			ClickOnMoreOption();
			ClickOnSMSLog();
			SelectSMSLogStatus_Off();
			ClickOnSMSLogOkBtn();
			sleep(3);

		}

		public void EnterSMSLogInterval(String SMSLogintervalNum) throws InterruptedException
		{
			SMSLogInterval.sendKeys(SMSLogintervalNum);
			sleep(3);
		}

		public void ClearSMSLogInterval()
		{
			SMSLogInterval.clear();
		}

		public void SMSLogErrorMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Alert Message : 'Interval should be greater than 14.' is displayed successfully when SMS Log interval is less than 14";
			String FailStatement="Fail >> Alert Message : 'Interval should be greater than 14.' is not displayed even when SMS Log interval is less than 14";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SMSLogErrorMessage, true, PassStatement, FailStatement,2);
		}

		public void SMSLogConfirmationMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'SMS logs tracking updated settings successfully.' is displayed successfully when SMS Log interval is greater than 14";
			String FailStatement="Fail >> Confirmation Message : 'SMS logs tracking updated settings successfully.' is not displayed even when SMS Log interval is greater than 14";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SMSLogConfirmationMessage, true, PassStatement, FailStatement,7);
		}

		public void VerifyOfSMSLogWindow()
		{
			String ActualValue=SMSLogHeader.getText();
			String ExpectedValue="SMS Logs - "+Config.DeviceName;
			String PassStatement = "PASS >> SMS Log Header with Device name is displayed successfully when Clicked on Dynamic SMS Log Job as "+ActualValue;
			String FailStatement = "Fail >> SMS Log Header with Device name is not displayed when Clicked on Dynamic SMS Log Job as "+ActualValue;
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			WebElement[] SMSLogWebelment={SMSLogAllTab,SMSLogInboxTab,SMSLogSentTab,SMSLogDraftTab,SMSLogOutboxTab};
			String[] Text={"All","Incoming","Outgoing","Missed","Rejected"};
			for(int i=0;i<SMSLogWebelment.length-1;i++)
			{
				boolean value = SMSLogWebelment[i].isDisplayed();
				PassStatement = "PASS >> "+Text[i]+" SMSLog Tab is displayed successfully when Clicked on Dynamic SMS Log Job";
				FailStatement = "Fail >> "+Text[i]+" SMSLog Tab is not displayed even when Clicked on Dynamic SMS Log Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] SMSLogbuton={SMSLogRefresh,SMSLogExport,SMSLogOkBtn,SMSLogCancelBtn};
			String[] Text1={"Refresh","Export","Ok","Cancel"};
			for(int i=0;i<SMSLogbuton.length-1;i++)
			{
				boolean value = SMSLogbuton[i].isDisplayed();
				PassStatement = "PASS >> "+Text1[i]+" Button is displayed successfully when Clicked on Dynamic SMS Log Job";
				FailStatement = "Fail >> "+Text1[i]+" Button is not displayed even when Clicked on Dynamic SMS Log Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			String[] Text2={"Type","Name","Number","Time","Message"};
			for(int i=0;i<SMSLogColumn.size()-1;i++)
			{
				ActualValue = SMSLogColumn.get(i).getAttribute("innerHTML");
				boolean value=ActualValue.equals(Text2[i]);
				PassStatement = "PASS >> "+ActualValue+" Column is displayed successfully when Clicked on Dynamic SMS Log Job";
				FailStatement = "Fail >> "+ActualValue+" Column is not displayed even when Clicked on Dynamic SMS Log Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			boolean value = SMSLogInterval.isEnabled();
			PassStatement = "PASS >> SMS Log On/Off Dropdown is displayed successfully when Clicked on Dynamic SMS Log Job";
			FailStatement = "Fail >> SMS Log On/Off Dropdown is not displayed even when Clicked on Dynamic SMS Log Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value = SMSLogStatus.isEnabled();
			PassStatement = "PASS >> Update interval textbox is displayed successfully when Clicked on Dynamic SMS Log Job";
			FailStatement = "Fail >> Update interval textbox is not displayed even when Clicked on Dynamic SMS Log Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSMSUpdateIntervalGrayedOut() throws InterruptedException
		{
			String ActualValue = SMSLogInterval.getAttribute("class");
			boolean value = ActualValue.contains("disabled");
			String PassStatement = "PASS >> Update Interval is grayed out successfully when SMS Log is Off";
			String FailStatement = "Fail >> Update Interval is not grayed out even when SMS Log is Off";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			sleep(3);
		}

		public void VerifyOfSMSUpdateInterval()
		{
			String ActualValue = SMSLogInterval.getAttribute("class");
			boolean value = ActualValue.contains("disabled");
			String PassStatement = "PASS >> Update Interval is Enabled successfully when SMS Log is On";
			String FailStatement = "Fail >> Update Interval is not Enabled even when SMS Log is On";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSMSLogCancelButton(String SMSLogIntervalNum) throws InterruptedException
		{
			String ActualValue = SMSLogStatus.getAttribute("value");
			System.out.println(ActualValue);
			if(ActualValue.equals("0"))
				ActualValue="Off";
			else
				ActualValue="On";
			boolean value = ActualValue.equals("On");
			String PassStatement = "PASS >> SMS Log is not saved when SMS Log is On and clicked on Cancel Button";
			String FailStatement = "Fail >> SMS Log is saved even when SMS Log is On and clicked on Cancel Button";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			ActualValue = SMSLogInterval.getAttribute("value");
			value = ActualValue.equals(SMSLogIntervalNum);
			PassStatement = "PASS >> Update Interval is not saved when SMS Log is On and clicked on Cancel Button";
			FailStatement = "Fail >> Update Interval is saved even when SMS Log is On and clicked on Cancel Button";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			sleep(4);
		}

		public void VerifyOfSMSLogOkButton(String SMSLogIntervalNum)
		{
			String ActualValue = SMSLogStatus.getAttribute("value");
			if(ActualValue.equals("1"))
				ActualValue="On";
			else
				ActualValue="Off";
			boolean value = ActualValue.equals("On");
			String PassStatement = "PASS >> SMS Log is not saved when SMS Log is On and clicked on Cancel Button";
			String FailStatement = "Fail >> SMS Log is saved even when SMS Log is On and clicked on Cancel Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ActualValue = SMSLogInterval.getAttribute("value");
			value = ActualValue.equals(SMSLogIntervalNum);
			PassStatement = "PASS >> Update Interval is not saved when SMS Log is On and clicked on Cancel Button";
			FailStatement = "Fail >> Update Interval is saved even when SMS Log is On and clicked on Cancel Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SMSLogUpdate()
		{
			String ActualValue = SMSLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> SMS Log is updated succesfully when SMS Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> SMS Log is not updated even when SMS Log is On and clicked on Ok Button";
			SoftAssert softAssertion= new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
		}

		public void ClickOnSMSExport() throws Throwable
		{
			String downloadPath = ELib.getDatafromExcel("Sheet1", 19, 1);
			File dir = new File(downloadPath);
			File[] dir_contents = dir.listFiles();
			int initial = 0;
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().startsWith("SMS Logs") && dir_contents[i].getName().endsWith(".csv")) {
					initial++;
				}
			}

			SMSLogExport.click();
			sleep(10);

			dir = new File(downloadPath);
			dir_contents = dir.listFiles();
			int result=0;
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().startsWith("SMS Logs") && dir_contents[i].getName().endsWith(".csv")) {
					result++;
				}
			}
			boolean value=result>initial;
			String PassStatement="PASS >> SMS Logs file is Exported successfully in csv Format";
			String FailStatement="Fail >> SMS Logs file is not Exported in csv Format";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnInboxTab() throws Throwable 
		{
			SMSLogInboxTab.click();
			Reporter.log("Clicked on Inbox Tab, Navigated successfully to Inbox tab section", true);
			sleep(2);
		}

		public void VerifyOfInboxSMSLogUpdate()
		{
			String ActualValue = SMSLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> SMS Log is updated succesfully when SMS Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> SMS Log is not updated even when SMS Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			PassStatement = "PASS >> Only Inbox SMS Log is updated succesfully when SMS Log is On and clicked on Inbox Tab section";
			FailStatement = "Fail >> Only Inbox SMS is not updated even when SMS Log is On and clicked on Inbox Tab section";
			if (value==true)
			{
				for(int i=1 ; i<ImageSMSLogUpdate.size();i++)
				{
					ActualValue= ImageSMSLogUpdate.get(i).getAttribute("class");
					value=ActualValue.contains("inbox");
					if(value==false)
					{
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void ClickOnSentTab() throws Throwable 
		{
			SMSLogSentTab.click();
			Reporter.log("Clicked on Sent Tab, Navigated successfully to Sent tab section", true);
			sleep(2);
		}

		public void VerifyOfSentSMSLogUpdate()
		{
			String ActualValue = SMSLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> SMS Log is updated succesfully when SMS Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> SMS Log is not updated even when SMS Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			PassStatement = "PASS >> Only Sent SMS Log is updated succesfully when SMS Log is On and clicked on Sent Tab section";
			FailStatement = "Fail >> Only Sent is not updated even when SMS Log is On and clicked on Sent Tab section";
			if (value==true)
			{
				for(int i=1 ; i<ImageSMSLogUpdate.size();i++)
				{
					ActualValue= ImageSMSLogUpdate.get(i).getAttribute("class");
					value=ActualValue.contains("paper-plane");
					if(value==false)
					{
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void ClickOnDraftTab() throws Throwable {
			SMSLogDraftTab.click();
			Reporter.log("Clicked on Draft Tab, Navigated successfully to Draft tab section", true);
			sleep(2);
		}

		public void VerifyOfDraftSMSLogUpdate()
		{
			String ActualValue = SMSLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> SMS Log is updated succesfully when SMS Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> SMS Log is not updated even when SMS Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			PassStatement = "PASS >> Only Draft SMS Log is updated succesfully when SMS Log is On and clicked on Draft Tab section";
			FailStatement = "Fail >> Only Draft is not updated even when SMS Log is On and clicked on Draft Tab section";

			if (value==true)
			{
				for(int i=1 ; i<ImageSMSLogUpdate.size();i++)
				{
					ActualValue= ImageSMSLogUpdate.get(i).getAttribute("class");
					value=ActualValue.contains("pencil-square");
					if(value==false)
					{
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void ClickOnOutboxTab() throws Throwable {
			SMSLogOutboxTab.click();
			Reporter.log("Clicked on Outbox Tab, Navigated successfully to Outbox tab section", true);
			sleep(2);
		}

		public void VerifyOfOutboxSMSLogUpdate()
		{
			String ActualValue = SMSLogUpdate.getAttribute("class");
			String ExpectedValue="no-records-found";
			boolean value=ActualValue.equals(ExpectedValue);
			String PassStatement = "PASS >> SMS Log is updated succesfully when SMS Log is On and clicked on Ok Button";
			String FailStatement = "Fail >> SMS Log is not updated even when SMS Log is On and clicked on Ok Button";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(value);
			if(value==true)
			{
				System.err.println(PassStatement);
				Reporter.log(PassStatement);
			}
			else{
				System.err.println(FailStatement);
				Reporter.log(FailStatement);
			}
			PassStatement = "PASS >> Only Outbox SMS Log is updated succesfully when SMS Log is On and clicked on Outbox Tab section";
			FailStatement = "Fail >> Only Outbox is not updated even when SMS Log is On and clicked on Outbox Tab section";
			if (value==true)
			{
				for(int i=1 ; i<ImageSMSLogUpdate.size();i++)
				{
					ActualValue= ImageSMSLogUpdate.get(i).getAttribute("class");
					value=ActualValue.contains("external-link");
					if(value==false)
					{
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}

		}

		public void VerifyOfSMSLogsActivityLogs() throws InterruptedException
		{
			boolean value=SMSLogsDeviceActivityLogs.get(0).isDisplayed();
			String ActualValue=SMSLogsDeviceActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for SMSLogs is displayed successfully when clicked on Dynamic SMSLogs Job as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for SMSLogs is not displayed even when clicked on SMSLogs"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			waitForXpathPresent("//h4[@id='smsLogModalTitle']/preceding-sibling::button");
			sleep(10);

		}





		//Data Usage
		@FindBy(id="dataUsage")
		private WebElement DataUsageBtn;

		@FindBy(xpath="//div[@id='datausage_modal']/div/h4")
		private WebElement DataUsageHeader;

		@FindBy(id="datausage_devicename")
		private WebElement DataUsageDeviceName;

		@FindBy(id="datausage_useddata")
		private WebElement DataUsageData;

//		@FindBy(id="data_usage_refresh")                       //      //*[@id="data_usage_refresh"]
//		private WebElement DataUsageRefreshButton;

		@FindBy(xpath="//*[@id='data_usage_refresh']")                       //      //*[@id="data_usage_refresh"]
		private WebElement DataUsageRefreshButton;

		@FindBy(id="dataUsage_chart_canvas")
		private WebElement DataUsageGraph;

		@FindBy(xpath="//a[text()='Mobile Data']")
		private WebElement DataUsageMobileDataSection;

		@FindBy(xpath="//a[text()='Wi-Fi Data']")
		private WebElement DataUsageWifiDataSection;

		@FindBy(xpath="//a[text()='All Data ']")
		private WebElement DataUsageAllDataSection;

		@FindBy(id="select_billingcycle")
		private WebElement DataUsagePeriod;

		@FindBy(id="startDate-picker")
		public WebElement DataUsageStartDate;

		@FindBy(id="endDate-picker")
		public WebElement DataUsageEndDate;

//		@FindBy(id="updateDate")
//		private WebElement DataUsageUpdateButton;      //   //*[@id="updateDate"]

		@FindBy(xpath="//*[@id='updateDate']")
		private WebElement DataUsageUpdateButton;      //   

		@FindBy(id="application_list")
		private WebElement DataUsageApplicationList;

		@FindBy(id="dataUsageEditBtn")
		private WebElement TelecomManagementEditButton;

		@FindBy(xpath="//span[text()='Enable Telecom Management']/preceding-sibling::span/input")
		private WebElement EnableTelecomManagementButton;

		@FindBy(xpath="//span[text()='Enable Telecom Management']")
		private WebElement EnableTelecomManagement;

		@FindBy(id="okbtn")
		private WebElement TelecomManagementOkButton;

//		@FindBy(xpath="//span[text()='Data usage tracking updated successfully.']")
//		private WebElement TelecomManagementConfirmationMessage;

		@FindBy(xpath="//span[text()='Data usage tracking settings updated successfully.']")
		private WebElement TelecomManagementConfirmationMessage;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/p[@class='main_txt_line']")
		private WebElement DataUsagePopup;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='No']")
		private WebElement DataUsageNoButton;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='Yes']")
		private WebElement DataUsageYesButton;

		@FindBy(xpath="//h4[contains(text(),'Telecom Management')]/preceding-sibling::button")
		private WebElement TelecomManagementCloseBtn;

		@FindBy(id="MobileMonthlyStartDate")
		private WebElement TelecomManagementMonthlyStartButton;

		@FindBy(id="MobileDataUsageWarningLimit")
		private WebElement TelecomManagementWarningLimit;

		@FindBy(id="MobileDataUsageWarningData")
		private WebElement TelecomManagementWarningDropdown;

		@FindBy(id="SendDeviceAlretWarning")
		private WebElement TelecomManagementDeviceAlert;

		@FindBy(id="SendMDMAlertWarning")
		private WebElement TelecomManagementMDMAlert;

		@FindBy(id="confidBillCycle")
		private WebElement TelecomManagementBillingCycle;

		@FindBy(id="MobileCustomNoOfDays")
		private WebElement TelecomManagementCustom;

		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job GET_DATA_USAGE on device("+Config.DeviceName+").')]")
		private List<WebElement> DataUsageRefreshActivityLogs;

		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job DATA_USAGE_TRACKING on device("+Config.DeviceName+").')]")
		private List<WebElement> TelecomManagemnetActivityLogs;
		
		@FindBy(id="backToMain")
	    private WebElement DataUsageBackButton;
		
		@FindBy(xpath="//span[.='Show Legacy Data Usage']")
		private WebElement ShowLegacyDataButton;
		
		public void ClickOnDataUsage_UM() throws InterruptedException
		{
			
			sleep(20);
			DataUsageBtn.click();
			sleep(3);
		}

		public void ClickOnDataUsage() throws InterruptedException
		{
			DataUsageBtn.click();
			waitForidPresent("select_billingcycle");
			sleep(4);
			Reporter.log("Clicked on Data Usage,Navigates successfully to Data Usage popup",true);
		}

		public void ClickOnDataUsageCloseBtn() throws InterruptedException
		{
			DataUsageCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(9);
			Reporter.log("Clicked on Close Button of Data Usage",true);
		}
		
		public void enterJobNameNixUpgrade(String name) throws InterruptedException{
			waitForXpathPresent("//*[@id='jobName']");
			sleep(1);
			JobNameNixUpgrade.clear();
			JobNameNixUpgrade.sendKeys(name);
			Reporter.log("Entered Job Name", true);
			sleep(2);
		}
	public void enterNixAgentLink(String nixLate) {
			nixAgentLink.clear();
			nixAgentLink.sendKeys(nixLate);
		}
	public void nixUpgradeOkButton() throws InterruptedException 
		{
			nixupgradejobokbtn.click();
			sleep(2);
			//waitForXpathPresent("//span[text()='Job created successfully.']");
			//sleep(4);
		}
	
	public void ClickOnShowlegacyDataUsage() throws InterruptedException
	{
		ShowLegacyDataButton.click();
		waitForidPresent("select_billingcycle");
		sleep(4);	
	}
	
	
	@FindBy(id="nix_agent_link")
		private WebElement nixAgentLink;
	@FindBy(id="nixupgradejobokbtn")
		private WebElement nixupgradejobokbtn;
	@FindBy(xpath="//*[@id='jobName']")
	private WebElement JobNameNixUpgrade;
	

		
		public void ClickOnWifiDataUsageSection() throws InterruptedException
		{
			DataUsageWifiDataSection.click();
			waitForidPresent("select_billingcycle");
			sleep(2);
			Reporter.log("Clicked on wifi Data Usage Section,Navigates successfully to Data Usage Section",true);
		}
		
		public void ClickONAllDataUsageSection() throws InterruptedException
		{
			DataUsageAllDataSection.click();
			waitForidPresent("select_billingcycle");
			sleep(2);
			Reporter.log("Clicked on wifi Data Usage Section,Navigates successfully to Data Usage Section",true);		
		}
		
		public void ClickOnMobileDataUsageSection() throws InterruptedException
		{
			DataUsageMobileDataSection.click();
			waitForidPresent("select_billingcycle");
			sleep(2);
			Reporter.log("Clicked on Mobile Data Usage Section,Navigates successfully to Mobile Data Usage Section");
		}

		public void VerifyOfDataUsage() throws Throwable
		{
			String ActualValue=DataUsageHeader.getText();
			String ExpectedValue="Data Usage";
			String PassStatement = "PASS >> 'Data Usage' Header is displayed successfully when clicked on Data Usage";
			String FailStatement = "Fail >> 'Data Usage' Header is not displayed even when clicked on Data Usage";	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			ActualValue=DataUsageDeviceName.getText();
			ExpectedValue=Config.DeviceName;
			PassStatement = "PASS >> Device Name is displayed successfully when clicked on Data Usage";
			FailStatement = "Fail >> Device Name is is not displayed even when clicked on Data Usage";	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			ClickOnDataUsageRefreshButton();
			WebElement[] DataUsageElement={DataUsageRefreshButton,DataUsageData,DataUsageGraph,DataUsageApplicationList,DataUsagePeriod
					,DataUsageStartDate,DataUsageEndDate,DataUsageUpdateButton};
			String[] text={"Data Usage Refresh Button","Usage Data","Data Usage Graph","Data Usage List of Apps","Billing cycle",
					"Billing cycle Start Date","Billing cycle End Date","Billing Cycle Update Button"};
			for(int i=0;i<DataUsageElement.length;i++)
			{
				boolean value=DataUsageElement[i].isDisplayed();
				PassStatement = "PASS >> "+text[i]+" is displayed successfully when clicked on Data Usage";
				FailStatement = "Fail >> "+text[i]+" is is not displayed even when clicked on Data Usage";	
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			DataUsageMobileDataSection.click();
			boolean value=DataUsageMobileDataSection.isDisplayed();
			ActualValue=DataUsageData.getText();
			PassStatement = "PASS >> Mobile Data section is displayed successfully when clicked on Mobile Data Graph of Data Usage as "+ActualValue;
			FailStatement = "Fail >> Mobile Data section is is not displayed even when clicked on Mobile Data Graph of Data Usage as "+ActualValue;	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			DataUsageWifiDataSection.click();
			value=DataUsageWifiDataSection.isDisplayed();
			ActualValue=DataUsageData.getText();
			PassStatement = "PASS >> Wifi Data section is displayed successfully when clicked on Wifi Data Graph of Data Usage as "+ActualValue;
			FailStatement = "Fail >> Wifi Data section is is not displayed even when clicked on Wifi Data Graph of Data Usage as "+ActualValue;	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			DataUsageAllDataSection.click();
			value=DataUsageAllDataSection.isDisplayed();
			ActualValue=DataUsageData.getText();
			PassStatement = "PASS >> All Data section is displayed successfully when clicked on All Data Graph of Data Usage as "+ActualValue;
			FailStatement = "Fail >> All Data section is is not displayed even when clicked on All Data Graph of Data Usage as "+ActualValue;	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnDataUsageRefreshButton() throws Throwable
		{
			
			sleep(60);
			DataUsageRefreshButton.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
			{}
			waitForidClickable("select_billingcycle");
			sleep(30);
			Reporter.log("Clicked on Refresh button of Data Usage",true);
		}

		public void ClickOnTelecomManagementEditButton() throws Throwable
		{
			TelecomManagementEditButton.click();
			waitForXpathPresent("//span[text()='Enable Telecom Management']/preceding-sibling::span/input");
			sleep(7);
			Reporter.log("Clicked on Edit button of Telecom Management,Navigates successfully to Telecom Management popup",true);
		}

		public void DisableTelecomManagement()
		{
			boolean value=EnableTelecomManagementButton.isSelected();
			if(value==true)
				EnableTelecomManagementButton.click();
			Reporter.log("Telecom Management is disabled successfully",true);
		}

		public void EnableTelecomManagement()
		{
			boolean value=EnableTelecomManagementButton.isSelected();
			if(value==false)
				EnableTelecomManagementButton.click();
			Reporter.log("Telecom Management is enabled successfully",true);
		}
		
		public void ClickOnTelecomManagementOkButton() throws Throwable
		{
			TelecomManagementOkButton.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(3);
			Reporter.log("Clicked on Ok button of Telecom Management",true);
		}
		public void ClickOnTelecomManagementJobOkButton() throws Throwable
		{
			TelecomManagementOkButton.click();
			sleep(3);
			Reporter.log("Clicked on Ok button of Telecom Management",true);
		}

		public void ConfirmationMessageTelecomManagement() throws InterruptedException{
			String PassStatement="PASS >> 'Data usage tracking updated succesfully.' Message is displayed successfully when clicked on Ok button of Telecom Management Job";
			String FailStatement="FAIL >> 'Data usage tracking updated succesfully.' Message is not displayed even when clicked on Ok button of Telecom Management Job";
			Initialization.commonmethdpage.ConfirmationMessageVerify(TelecomManagementConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void VerifyofDataUsagePopup()
		{
			String ActualValue=DataUsagePopup.getAttribute("innerHTML");
			String ExpectedValue="Telecom Management is turned off for device";
			boolean value=ActualValue.contains(ExpectedValue);
			String PassStatement="PASS >> Alert Message is displayed successfully as "+ExpectedValue+" when clicked on Data Usage with Telecom Management turned OFF";
			String FailStatement="FAIL >> Alert Message is displayed successfully as "+ExpectedValue+" is not displayed even when clicked on Data Usage with Telecom Management turned OFF";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ExpectedValue="Do you want to turn it ON?";
			value=ActualValue.contains(ExpectedValue);
			PassStatement="PASS >> Alert Message is displayed successfully as "+ExpectedValue+" when clicked on Data Usage with Telecom Management turned OFF";
			FailStatement="FAIL >> Alert Message is displayed successfully as "+ExpectedValue+" is not displayed even when clicked on Data Usage with Telecom Management turned OFF";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnDataUsage_Popup() throws InterruptedException
		{
			DataUsageBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='No']");
			sleep(2);
		}

		public void ClickOnDataUsageNoButton() throws Throwable
		{
			DataUsageNoButton.click();
			waitForidPresent("select_billingcycle");
			sleep(2);
			Reporter.log("Clicked on No button of Data Usage to turn on Telecom Management,Navigates successfully to Data Usage popup", true);
		}

		public void ClickOnDataUsageYesButton() throws Throwable
		{
			DataUsageYesButton.click();
			waitForXpathPresent("//span[text()='Enable Telecom Management']/preceding-sibling::span/input");
			sleep(8);
			Reporter.log("Clicked on Yes button of Data Usage to turn on Telecom Management", true);
		}

		public void VerifyOfTelecomManagement() throws Throwable
		{
			boolean value=EnableTelecomManagement.isDisplayed();
			String PassStatement="PASS >> Telecom Management popup is displayed successfully when clicked on Yes Button of Data usage popup with Telecom Management turned OFF";
			String FailStatement="FAIL >> Telecom Management popup is not displayed even when clicked on Yes Button of Data usage popup with Telecom Management turned OFF";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnTelecomManagementCloseBtn() throws InterruptedException
		{
			TelecomManagementCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Reporter.log("Clicked on Close Button of Telecom Management",true);
		}

		public void ClickOnStartDate_Monthly() throws Throwable
		{
			Select sel=new Select(TelecomManagementBillingCycle);
			sel.selectByVisibleText("Monthly");
			sleep(2);
			TelecomManagementMonthlyStartButton.click();
			String[] Day=Config.StartDate_Monthly.split("/");
			String Daynumber = Day[0];
			String finalDate=Day[0];
			if(Daynumber.startsWith("0"))
			{
				finalDate=Daynumber.replace("0", "");
				int rrr = Integer.parseInt(finalDate);
				rrr = rrr-1;
				finalDate=Integer.toString(rrr);
			}
			Initialization.driver.findElement(By.xpath("//li[text()='"+finalDate+"']")).click();
		}

		public void EnableofAlert()
		{
			TelecomManagementWarningLimit.clear();
			TelecomManagementWarningLimit.sendKeys("12");
			TelecomManagementWarningDropdown.click();
			TelecomManagementDeviceAlert.click();
			TelecomManagementMDMAlert.click();

		}

		public void MonthlyBiilingCycle_DataUsage()
		{
			String ActualValue=DataUsageStartDate.getAttribute("value");
			String ExpectedValue=Config.StartDate_Monthly;
			boolean value=ExpectedValue.equals(ActualValue);
			String PassStatement="PASS >> Start Date of Billing Cycle is updated succesfully when telecom management is enabled with monthly billing cycle";
			String FailStatement="FAIL >> Start Date of Billing Cycle is not updated even when telecom management is enabled with monthly billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ActualValue=DataUsageEndDate.getAttribute("value");
			ExpectedValue=Config.EndDate_Monthly;
			value=ExpectedValue.equals(ActualValue);
			PassStatement="PASS >> End Date of Billing Cycle is updated succesfully when telecom management is enabled with monthly billing cycle";
			FailStatement="FAIL >> End Date of Billing Cycle is not updated even when telecom management is enabled with monthly billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnStartDate_Weekly() throws Throwable
		{
			Select sel=new Select(TelecomManagementBillingCycle);
			sel.selectByVisibleText("Weekly");
			sleep(2);
			Initialization.driver.findElement(By.id("day_0")).click();
		}

		public void WeeklyBiilingCycle_DataUsage()
		{
			String ActualValue=DataUsageStartDate.getAttribute("value");
			String ExpectedValue=Config.StartDate_Weekly;
			boolean value=ExpectedValue.equals(ActualValue);
			String PassStatement="PASS >> Start Date of Billing Cycle is updated succesfully when telecom management is enabled with Weekly billing cycle";
			String FailStatement="FAIL >> Start Date of Billing Cycle is not updated even when telecom management is enabled with Weekly billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ActualValue=DataUsageEndDate.getAttribute("value");
			ExpectedValue=Config.EndDate_Weekly;
			value=ExpectedValue.equals(ActualValue);
			PassStatement="PASS >> End Date of Billing Cycle is updated succesfully when telecom management is enabled with Weekly billing cycle";
			FailStatement="FAIL >> End Date of Billing Cycle is not updated even when telecom management is enabled with Weekly billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnStartDate_Daily() throws Throwable
		{
			Select sel=new Select(TelecomManagementBillingCycle);
			sel.selectByVisibleText("Daily");
			sleep(2);
		}

		public void DailyBiilingCycle_DataUsage()
		{
			String ActualValue=DataUsageStartDate.getAttribute("value");
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
			Date date = new Date();
			String Expected_StartDate = dateFormat.format(date);
			String ExpectedValue=Expected_StartDate;
			boolean value=ExpectedValue.equals(ActualValue);
			String PassStatement="PASS >> Start Date of Billing Cycle is updated succesfully when telecom management is enabled with Daily billing cycle";
			String FailStatement="FAIL >> Start Date of Billing Cycle is not updated even when telecom management is enabled with Daily billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ActualValue=DataUsageEndDate.getAttribute("value");
			System.out.println(ActualValue);
			value=ExpectedValue.equals(ActualValue);
			PassStatement="PASS >> End Date of Billing Cycle is updated succesfully when telecom management is enabled with Daily billing cycle";
			FailStatement="FAIL >> End Date of Billing Cycle is not updated even when telecom management is enabled with Daily billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnStartDate_Custom() throws InterruptedException
		{
			Select sel=new Select(TelecomManagementBillingCycle);
			sel.selectByVisibleText("Custom");
			sleep(2);
			TelecomManagementCustom.sendKeys("2");
		}

		public void CustomBiilingCycle_DataUsage()
		{
			String ActualValue=DataUsageStartDate.getAttribute("value");
			String ExpectedValue=Config.StartDate_Custom;
			boolean value=ExpectedValue.equals(ActualValue);
			String PassStatement="PASS >> Start Date of Billing Cycle is updated succesfully when telecom management is enabled with Custom billing cycle";
			String FailStatement="FAIL >> Start Date of Billing Cycle is not updated even when telecom management is enabled with Custom billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ActualValue=DataUsageEndDate.getAttribute("value");
			ExpectedValue=Config.EndDate_Custom;
			value=ExpectedValue.equals(ActualValue);
			PassStatement="PASS >> End Date of Billing Cycle is updated succesfully when telecom management is enabled with Custom billing cycle";
			FailStatement="FAIL >> End Date of Billing Cycle is not updated even when telecom management is enabled with Custom billing cycle";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		
		public void SelectCustomBillingCycle()
		{
			Select sel = new Select(DataUsagePeriod);
			sel.selectByVisibleText("Custom");
			DataUsageStartDate.click();
			for (int i = 0; i < 2; i++) {
				List<WebElement> lst1 = Initialization.driver
						.findElements(By.xpath("//th[@class='next available']/preceding-sibling::th[2]"));
				lst1.get(0).click();
			}
			List<WebElement> lst2 = Initialization.driver
					.findElements(By.xpath("//td[text()='" + Config.CustomDate + "']"));
			lst2.get(0).click();

			DataUsageEndDate.click();
			List<WebElement> lst3 = Initialization.driver
					.findElements(By.xpath("//td[contains(@class,'active')]"));
			lst3.get(1).click();
		}

		public void ClickOnUpdateButton()
		{
			DataUsageUpdateButton.click();
			Reporter.log("Clicked on Update Button of Billing Cycle of Data Usage");
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
			{}
		}

		public void VerifyCustombillingFutureDate(WebElement ele)
		{
			DataUsageEndDate.click();
			LocalDate localDate = LocalDate.now();
			String date=DateTimeFormatter.ofPattern("dd").format(localDate);
			 int date1=Integer.parseInt(date);
			 int FutureDate=date1+1;
	        String status = Initialization.driver.findElement(By.xpath("//td[text()='" +FutureDate+ "']")).getAttribute("class");
			if(!status.equals("off disabled")) 
				ALib.AssertFailMethod("PASS>>> User Is Not Able to Edit Custom Billing Future Date");
			else
				Reporter.log("FAIL>>> User Is Able to Edit Custom Billing Future Date",true);
		}

		public void VerifyOfDataUsageRefreshActivityLogs()
		{
			boolean value=DataUsageRefreshActivityLogs.get(0).isDisplayed();
			String ActualValue=DataUsageRefreshActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Refresh of Data Usage is displayed successfully when clicked on Refresh Button of Data Usage as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Refresh of Data Usage is not displayed even when clicked on Refresh Button of Data Usage"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);	
		}

		public void VerifyOfTelecomManagemnetActivityLogs()
		{
			boolean value=TelecomManagemnetActivityLogs.get(0).isDisplayed();
			String ActualValue=TelecomManagemnetActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Telecom Management is displayed successfully when Telecom Management Policy is updated as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Telecom Management is not displayed even when Telecom Management Policy is updated"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);	
		}

		public void VerifyWifiDataIsShownAfterRefreshing() throws Throwable
		{
			ClickOnWifiDataUsageSection();
			try
			{
			//	Initialization.driver.findElement(By.id("dataUsageappList_0")).isDisplayed();//
				Initialization.driver.findElement(By.xpath("//*[@id='dataUsage_chart_canvas']")).isDisplayed();
				Reporter.log("PASS>>> Wifi Data Is Shown Successfully On Refreshing",true);
			}
			catch(Exception e)
			{
				ClickOnTelecomManagementCloseBtn();
				ALib.AssertFailMethod("FAIL>>> Wifi Data Is Not Updated On Refreshing");
			}
		}
		
		public void VerifyMobileDataIsNotShownAfterRefreshing() throws InterruptedException
		{
			try
			{
				Initialization.driver.findElement(By.id("dataUsageappList_0")).isDisplayed();
				ClickOnTelecomManagementCloseBtn();
				ALib.AssertFailMethod("FAIL>>> Mobile Data Usage Is Updated On Refreshing, though Telecom Management Is Off");
			}
			catch (Exception e) {
				Reporter.log("PASS>>> Mobile Data Usage Is Not Shown On Refreshing",true);
			}
			
		}
		
		public void VerifyNoDataMessageIsShown()
		{
			try
			{
				Initialization.driver.findElement(By.xpath("//span[text()='No data available. Press Refresh ( ']")).isDisplayed();
				Reporter.log("No data available please click on refresh to update Message Is Displayed Successfully",true);
			}
			
			catch (Exception e) {
				ALib.AssertFailMethod("No data available please click on refresh to update Message Is Not Displayed");
			}
		}
		
		
		  String PeriodOption ;
		public void VerifyDataUsagePeriod() throws InterruptedException
		{
			Select sel = new Select(DataUsagePeriod);
			List<WebElement> PeriodOptions = sel.getOptions();
			 PeriodOption = PeriodOptions.get(0).getText();
			if(PeriodOptions.size()==1 && PeriodOption.contains("(CBC)"))
			{
				Reporter.log("PASS>>> only One Option Is Shown In Billing Cycle And The Billing Is Current Billing Cycle",true);
			}
			else
			{
				ClickOnDataUsageCloseBtn();
				ALib.AssertFailMethod("FAIL>>> Is Billing Is Not Current Billing Cycle");
			}
		}
		
		public void VerifyStartDateAndEndDateOFBillingCycle() throws InterruptedException
		{
			  Date date = new Date();
			  Calendar cal=Calendar.getInstance();
			  int maxDat = cal.getActualMaximum(Calendar.DATE);
			  SimpleDateFormat DateFor = new SimpleDateFormat("MMM yyyy");
			  String Month = DateFor.format(date);
			  String MaxDateInBillingCycle = maxDat +" "+Month;
			  String MinDateInBillingCycle="01"+" "+Month;
			  if(PeriodOption.contains(MinDateInBillingCycle)&&PeriodOption.contains(MaxDateInBillingCycle))
			  {
				  Reporter.log("PASS>>> Start Date And End Date Is Displayed Correctly",true);
			  }
			  else
			  {
				  ClickOnDataUsageCloseBtn();
				  ALib.AssertFailMethod("FAIL>>> Start Date And End Date Is Not Displayed Correctly");
			  }
		}
		
		public void VerifyDataUsageGraphIsDisplayed()
		{
			try
			{
				DataUsageGraph.isDisplayed();
				Reporter.log("Data Graphs Are Displayed Successfully",true);
			}
			
			catch (Exception e) {
				ALib.AssertFailMethod("Data Graphs Are Not Displyed");
			}
		}
		
		public void VerifyEditingInCurrentBillingCycleDataUsage()
		{
			try
			{
				DataUsageUpdateButton.click();
				ALib.AssertFailMethod("FAIL>>> User Can Edit Current Billing Cycle");
			}
			
			catch (Exception e) {
				Reporter.log("PASS>>> User Is Not Allowed To Edit Current Billing Cycle",true);
			}
		}
		 
		public void ClickOnapplicationnameInDataUsagePage(String ApplicationName) throws InterruptedException
		{
			WebElement ele = Initialization.driver.findElement(By.xpath("//span[.='"+ApplicationName+"']"));
			((JavascriptExecutor) Initialization.driver).executeScript(
					"arguments[0].scrollIntoView();", ele);
			ele.click();
			sleep(3);
		}
		public void VerifyParticularApplicationConsumingDataInListOfApps(String ApplicationName) throws InterruptedException
		{
			ClickOnapplicationnameInDataUsagePage(ApplicationName);
			try
			{
				DataUsageBackButton.isDisplayed();
				Reporter.log("Data Usage Of A Particular Application Is Displayed",true);
				DataUsageGraph.isDisplayed();
				Reporter.log("Graph Is Displayed Successfully For Data Usage Of Particular Application",true);
			}
			catch (Exception e) 
			{
				ClickOnDataUsageCloseBtn();
				ALib.AssertFailMethod("Graph Is Not Displayed For Data Usage Of an application");
			}
		}
		
		public void ClickOnDatausageBackArrowButton() throws InterruptedException
		{
			DataUsageBackButton.click();
			sleep(3);
			try
			{
				DataUsageBackButton.click();
				ClickOnDataUsageCloseBtn();
				ALib.AssertFailMethod("FAIL>>>Unable To Navigate into Main Page Of Data Usage");
			}
			catch (Exception e) {
				Reporter.log("PASS>>> Successfully Navigated Into Main Page Of Data Usage",true);
			}
			
		}
		
		public void NixAdminSettings()throws InterruptedException, IOException{
			WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 60);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Get Started']").click();
			sleep(5);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']").click();
			sleep(1);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']").click();
			sleep(1);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']").click();
			sleep(1);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']").click();
			sleep(1);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow all the time']").click();
			sleep(1);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']").click();
			sleep(1);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']").click();
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("android:id/button1")));
				Initialization.driverAppium.findElementById("android:id/button1").click();
				sleep(2);
			}catch(Exception e){
				Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='ALLOW']").click();
			}
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Activate']")));
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Activate']").click();
			try {
				sleep(10);
				
				Initialization.driverAppium.findElementById("com.samsung.klmsagent:id/checkBox1").click();
				Initialization.driverAppium.findElementById("com.samsung.klmsagent:id/btn_confirm_inner").click();
				sleep(3);
			}catch(Exception e) {
				Reporter.log("Radio button is not present",true);
			}
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='OK']")));
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK']").click();
			common.commonScrollMethod("SureMDM Nix");
	        common.ClickOnAppiumText("SureMDM Nix");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Switch[@text='Off']")));
			Initialization.driverAppium.findElementByXPath("//android.widget.Switch[@text='Off']").click();
			sleep(3);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Allow']").click();
			sleep(3);
			Runtime.getRuntime().exec("adb shell input keyevent 4");
			sleep(3);
			Initialization.driverAppium.findElementById("com.nix:id/button_continue_done").click();
		}
		
		public void EnterSureMDMAccountID(String AccId)throws InterruptedException
		{
			Initialization.driverAppium.findElementById("com.nix:id/editTextCustID").sendKeys(AccId);
			sleep(2);
		}
		
		DeviceEnrollmentHonor8_ScanQR_POM DeviceEnrollment_SCanQR=new DeviceEnrollmentHonor8_ScanQR_POM();
	public void EnterRegisterServerPath(String ServerPath)throws InterruptedException, IOException{
			sleep(8);
			try {
				if(Initialization.driverAppium.findElementById("com.nix:id/serverPath").isDisplayed()) {
					Initialization.driverAppium.findElementById("com.nix:id/serverPath").sendKeys(ServerPath);

					Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Set Server Path']").click();
					sleep(3);
					Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Continue...']").click();
				}}catch(Exception e) {
					Reporter.log("Server path is not displayed",true);	
			    	 DeviceEnrollment_SCanQR.SendingDeviceName(Config.Enrolled_DeviceName);
				}
		}

	public void SelectingBillingCycle(String Option)
	{
		Select sel = new Select(DataUsagePeriod);
		sel.selectByVisibleText(Option);
	}



		//Remote Buzz
		@FindBy(id="remoteBuzz")
		private WebElement RemoteBuzzBtn;

		@FindBy(xpath="//span[text()='Device buzz initiated successfully.']")
		private WebElement RemoteBuzzDeviceConfirmationMessage;

		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job REMOTE_BUZZ on device("+Config.DeviceName+").')]")
		private List<WebElement> RemoteBuzzDeviceActivityLogs;

		public void ClickOnRemoteBuzz_UM()
		{
			RemoteBuzzBtn.click();
		}

		public void ClickOnRemoteBuzz() throws InterruptedException
		{
			RemoteBuzzBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]");
			sleep(3);
			Reporter.log("Clicked on Remote Buzz Dynamic Job, Remote Buzz Job popup is opened",true);
		}

		public void VerifyOfRemoteBuzzDevicePopup()
		{
			String ActualValue=LockDevicePopup.getText();
			String ExpectedValue="Selected device will be buzzed. Do you wish to continue?";
			String PassStatement="PASS >> "+ActualValue+" Message is displayed successfully when clicked on RemoteBuzz";
			String FailStatement="FAIL >> "+ActualValue+" Message is not displayed even when clicked on RemoteBuzz"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=RebootLockWipeBuzzDeviceNoBtn.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on RemoteBuzz";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on RemoteBuzz"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=RebootLockWipeBuzzDeviceYesBtn.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on RemoteBuzz";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on RemoteBuzz"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnRemoteBuzzDeviceNoBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceNoBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(4);
			Reporter.log("Clicked on No button of Remote Buzz Job popup",true);
		}

		public void ClickOnRemoteBuzzDeviceYesBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceYesBtn.click();
			Reporter.log("Clicked on Yes button of Remote Buzz Job popup",true);
		}

		public void ConfirmationMessageRemoteBuzzDeviceVerify() throws InterruptedException{
			String PassStatement="PASS >> 'Device buzz initiated successfully.' Message is displayed successfully when clicked on yes button of Dynamic Lock Job";
			String FailStatement="FAIL >> 'Device buzz initiated successfully.' Message is not displayed even when clicked on yes button of Dynamic Lock Job";
			Initialization.commonmethdpage.ConfirmationMessageVerify(RemoteBuzzDeviceConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void ConfirmationMessageRemoteBuzzDeviceVerify_False() throws InterruptedException{
			String PassStatement="PASS >> 'Device buzz initiated successfully.' Message is not displayed when clicked on No button of Dynamic Remote Buzz Job";
			String FailStatement="FAIL >> 'Device buzz initiated successfully.' Message is displayed even when clicked on No button of Dynamic Remote Buzz Job";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(RemoteBuzzDeviceConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void VerifyOfRemoteBuzzActivityLogs()
		{
			boolean value=RemoteBuzzDeviceActivityLogs.get(0).isDisplayed();
			String ActualValue=RemoteBuzzDeviceActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Remote Buzz is displayed successfully when clicked on Dynamic Remote Buzz Job as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Remote Buzz is not displayed even when clicked on Remote Buzz"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}












		//Dynamics Apps
		@FindBy(id="appListBtn")
		private WebElement AppsBtn;

		@FindBy(id="uninstallAppList")
		private WebElement UninstallBtn;

		@FindBy(id="clearDataAppList")
		private WebElement ClearDataBtn;

		@FindBy(id="refreshAppList")
		private WebElement AppsRefreshBtn;

		@FindBy(id="appDetailModalTitle")
		private WebElement DynamicAppsHeader;

		@FindBy(xpath="//h4[@id='appDetailModalTitle']/preceding-sibling::button")
		private WebElement DynamicAppsCloseBtn;

		@FindBy(id="downloaded_apps")
		private WebElement DownloadAppsTabSection;

		@FindBy(id="sytem_apps")
		private WebElement SystemTabSection;

		@FindBy(id="all_apps")
		private WebElement AllAppsTabSection;

		@FindBy(xpath="//table[@id='downlodedAppListTable']/thead[@id='apptableHeader']/tr/th/div[@class='th-inner sortable']")
		private List<WebElement> DownloadAppsColumnName;

		@FindBy(xpath="//table[@id='downlodedAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Lock']")
		private WebElement DownloadAppsLockColumnName;

		@FindBy(xpath="//table[@id='downlodedAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Run at startup']")
		private WebElement DownloadAppsRunAtStartupColumnName;

		@FindBy(xpath="//table[@id='downlodedAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Startup Delay In Secs']")
		private WebElement DownloadAppsStartupDelayColumnName;

		@FindBy(xpath="//table[@id='deviceAppListTable']/thead[@id='apptableHeader']/tr/th/div[@class='th-inner sortable']")
		private List<WebElement> AllAppsColumnName;

		@FindBy(xpath="//table[@id='deviceAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Lock']")
		private WebElement AllAppsLockColumnName;

		@FindBy(xpath="//table[@id='deviceAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Run at startup']")
		private WebElement AllAppsRunAtStartupColumnName;

		@FindBy(xpath="//table[@id='deviceAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Startup Delay In Secs']")
		private WebElement AllAppsStartupDelayColumnName;

		@FindBy(xpath="//table[@id='systemAppListTable']/thead[@id='apptableHeader']/tr/th/div[@class='th-inner sortable']")
		private List<WebElement> SystemAppsColumnName;

		@FindBy(xpath="//table[@id='systemAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Lock']")
		private WebElement SystemAppsLockColumnName;

		@FindBy(xpath="//table[@id='systemAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Run at startup']")
		private WebElement SystemAppsRunAtStartupColumnName;

		@FindBy(xpath="//table[@id='systemAppListTable']/thead[@id='apptableHeader']/tr/th/div[text()='Startup Delay In Secs']")
		private WebElement SystemAppsStartupDelayColumnName;

		@FindBy(id="permissionsAppList")
		private WebElement PermissionList;

		@FindBy(xpath="//div[@id='appList_panel3']/div/div/div/input[@placeholder='Search']")
		private WebElement DownloadAppsSearch;

		@FindBy(xpath="//div[@id='appList_panel2']/div/div/div/input[@placeholder='Search']")
		private WebElement SystemAppsSearch;

		@FindBy(xpath="//div[@id='appList_panel1']/div/div/div/input[@placeholder='Search']")
		private WebElement AllAppsSearch;

		@FindBy(id="advancedAppList")
		private WebElement AdvancedSettingsButton;

		@FindBy(xpath="//div[@id='appListConfiramtionDialog']/div/div/div/h4")
		private WebElement AdvancedSettingsHeader;

		@FindBy(xpath="//h4[text()='Advanced Settings']/preceding-sibling::button")
		private WebElement AdvancedSettingsClosebutton;

		@FindBy(xpath="//div[@id='appListConfiramtionDialog']/div/div/div/p[@class='main_txt_line']")
		private WebElement AdvancedSettingsSummary;

		@FindBy(id="applistunlockpin")
		private WebElement AdvancedSettingsPinTextBox;

		@FindBy(xpath="//div[@id='appListConfiramtionDialog']/div/div/div/button[text()='OK']")
		private WebElement AdvancedSettingsOkButton;

		@FindBy(xpath="//div[@id='appListConfiramtionDialog']/div/div/div/button[text()='Cancel']")
		private WebElement AdvancedSettingsCancelbutton;

		@FindBy(id="applyAppList")
		private WebElement ApplyChangesButton;

		@FindBy(xpath="//table[@id='downlodedAppListTable']/tbody/tr/td[4]")
		private List<WebElement> DownloadedAppsType;

		@FindBy(xpath="//div[@id='appList_panel3']/div[1]/div[2]/div[1]/table/thead/tr/th/div/span/input")
		private WebElement DownloadedAppsMasterCheckbox;

		@FindBy(xpath="//table[@id='downlodedAppListTable']/tbody/tr/td[1]/span/input")
		private List<WebElement> DownloadedAppsCheckoxes;

		@FindBy(xpath="//table[@id='systemAppListTable']/tbody/tr/td[4]")
		private List<WebElement> SystemAppsType;

		@FindBy(xpath="//div[@id='appList_panel2']/div[1]/div[2]/div[1]/table/thead/tr/th/div/span/input")
		private WebElement SystemAppsMasterCheckbox;

		@FindBy(xpath="//table[@id='systemAppListTable']/tbody/tr/td[1]/span/input")
		private List<WebElement> SystemAppsCheckoxes;

		@FindBy(xpath="//table[@id='deviceAppListTable']/tbody/tr/td[4]")
		private List<WebElement> AllAppsType;

		@FindBy(xpath="//div[@id='appList_panel1']/div[1]/div[2]/div[1]/table/thead/tr/th/div/span/input")
		private WebElement AllAppsMasterCheckbox;

		@FindBy(xpath="//table[@id='deviceAppListTable']/tbody/tr/td[1]/span/input")
		private List<WebElement> AllAppsCheckoxes;

		@FindBy(xpath="//span[text()='Please select the applications to be uninstalled.']")
		private WebElement AppsUninstallErrorMessage;

		@FindBy(xpath="//span[text()='System apps cannot be uninstalled. Please unselect system apps.']")
		private WebElement AllAppsUninstallErrorMessage;

		@FindBy(xpath="//span[text()='Initiated application uninstall.']")
		private WebElement AppsUninstallConfirmationMessage;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/p")
		private WebElement AppsUninstallpopup;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='No']")
		private WebElement AppsUninstallNoButton;

		@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='Yes']")
		private WebElement AppsUninstallYesButton;

		@FindBy(xpath="//span[text()='Please select the applications to clear data.']")
		private WebElement ClearDataErrorMessage;

		@FindBy(xpath="//span[text()='Application data cleared.']")
		private WebElement ClearDataConfirmationMessage;

		@FindBy(xpath="//span[text()='Pin changed successfully.']")
		private WebElement PinChangeConfirmationMessage;

		@FindBy(xpath="//span[text()='Settings applied successfully.']")
		private WebElement ApplyChangesConfirmationMessage;

		@FindBy(xpath="//span[text()='No changes in application settings detected.']")
		private WebElement NoChangesErrorMessage;

		@FindBy(xpath="//span[text()='No matching result found.']")
		private WebElement SearchErrorMeesage;

		@FindBy(xpath="//div[@id='appPermissionDialog']/div/div/div/button[text()='OK']")
		private WebElement AppPermissionOKButton;

		@FindBy(xpath="//div[@id='appPermissionDialog']/div/div/div/h4")
		private WebElement AppPermissionHeader;

		@FindBy(xpath="//h4[text()='Application Permissions']/preceding-sibling::button")
		private WebElement AppPermissionCloseButton;

		@FindBy(xpath="//table[@id='appsAppPermissionTable']/tbody/tr/td")
		private List<WebElement> AppPermissionList;

		public void ClickOnApps() throws InterruptedException
		{
			AppsBtn.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
			{}
			waitForidPresent("uninstallAppList");
			waitForidPresent("uninstallAppList");
			waitForidPresent("uninstallAppList");
			sleep(10);
			Reporter.log("Clicked on Apps Dynamic Job, Application Settings popup is opened",true);
		}

		public boolean VerifyOfLockApplicationEnabled()
		{
			boolean value;
			try{
				String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td[p[text()='SureMDM Nix']]/following-sibling::td[2]/span/input")).getAttribute("disabled");
				value=ActualValue.contains("");
			}catch(Exception e)
			{
				value=false;
			}
			return value;
		}

		public boolean VerifyOfRunAtStartupApplicationEnabled()
		{
			boolean value;
			try{
				String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td[p[text()='SureMDM Nix']]/following-sibling::td[3]/span/input")).getAttribute("disabled");
				value=ActualValue.contains("");
			}catch(Exception e)
			{
				value=false;
			}
			return value;
		}

		public void ClickOnApplication() throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td[p[text()='SureMDM Nix']]/preceding-sibling::td[2]/span/input")).click();
			sleep(5);
		}

		public void ClickOnUninstall_UM()
		{
			UninstallBtn.click();
		}

		public void ClickOnClearData_UM()
		{
			ClearDataBtn.click();
		}

		public void ClickOnUninstall() throws InterruptedException
		{
			UninstallBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]");
			sleep(3);
			Reporter.log("Click on Uninstall of Apps, Navigates successfully to Uninstall of Apps", true);
		}

		public void ClickOnClearData() throws InterruptedException
		{
			ClearDataBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]");
			sleep(3);
			Reporter.log("Click on Clear Data of Apps, Navigates successfully to Clear Data of Apps", true);
		}

		public void ClickOnCloseClearData() throws InterruptedException
		{
			try{
				RebootLockWipeBuzzDeviceNoBtn.click();
				waitForidPresent("deleteDeviceBtn");
				sleep(4);
			}catch(Exception e)
			{

			}
		}

		public void ClickOnDynamicAppsCloseBtn() throws InterruptedException
		{
			DynamicAppsCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			waitForidPresent("deleteDeviceBtn");
			sleep(9);
			Reporter.log("Clicked on Close button of Apps Dynamic Job, Application Settings popup is closed",true);
		}

		public void VerifyOfDynamicApps() throws Throwable
		{
			String ActualValue=DynamicAppsHeader.getText();
			String ExpectedValue="Application List - "+Config.DeviceName;
			String PassStatement="PASS >> "+ActualValue+" Header is displayed successfully when clicked on Apps Dynamic Job";
			String FailStatement="FAIL >> "+ActualValue+" Header is not displayed even when clicked on Apps Dynamic Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			WebElement[] Appsbutton={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] Text={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<Appsbutton.length-1;i++)
			{
				boolean value = Appsbutton[i].isDisplayed();
				PassStatement = "PASS >> "+Text[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job";
				FailStatement = "Fail >> "+Text[i]+" Button is not displayed even when Clicked on Dynamic Apps Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] AppsTab={PermissionList,UninstallBtn,ClearDataBtn};
			String[] Text2={"Permissions","Uninstall","Clear Data"};
			for(int i=0;i<AppsTab.length-1;i++)
			{
				boolean value = AppsTab[i].isDisplayed();
				PassStatement = "PASS >> "+Text2[i]+" Tab is displayed successfully when Clicked on Dynamic Apps Job";
				FailStatement = "Fail >> "+Text2[i]+" Tab is not displayed even when Clicked on Dynamic Apps Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			String[] DownloadAppsColumn1 ={DownloadAppsColumnName.get(0).getAttribute("innerHTML"),DownloadAppsColumnName.get(1).getAttribute("innerHTML"),DownloadAppsColumnName.get(2).getAttribute("innerHTML"),DownloadAppsColumnName.get(3).getAttribute("innerHTML")};
			String[] DownloadAppsText1={"Application Name","Type","Package","Version"};
			for(int i=0;i<DownloadAppsColumn1.length-1;i++)
			{
				boolean value = DownloadAppsText1[i].equals(DownloadAppsColumn1[i]);
				PassStatement = "PASS >> "+DownloadAppsText1[i]+" Column is displayed successfully when Clicked on Dynamic Apps Job under Download section";
				FailStatement = "Fail >> "+DownloadAppsText1[i]+" Column is not displayed even when Clicked on Dynamic Apps Job under Download section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] DownloadAppsColumn2={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] DownloadAppsText2={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<DownloadAppsColumn2.length-1;i++)
			{
				boolean value = DownloadAppsColumn2[i].isDisplayed();
				PassStatement = "PASS >> "+DownloadAppsText2[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job under Download section";
				FailStatement = "Fail >> "+DownloadAppsText2[i]+" Button is not displayed even when Clicked on Dynamic Apps Job under Download section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			ClickOnSystemAppsTab();
			String[] SystemAppsColumn1 ={SystemAppsColumnName.get(0).getAttribute("innerHTML"),SystemAppsColumnName.get(1).getAttribute("innerHTML"),SystemAppsColumnName.get(2).getAttribute("innerHTML"),SystemAppsColumnName.get(3).getAttribute("innerHTML")};
			String[] SystemAppsText1={"Application Name","Type","Package","Version"};
			for(int i=0;i<SystemAppsColumn1.length-1;i++)
			{
				boolean value = SystemAppsText1[i].equals(SystemAppsColumn1[i]);
				PassStatement = "PASS >> "+SystemAppsText1[i]+" Column is displayed successfully when Clicked on Dynamic Apps Job under System section";
				FailStatement = "Fail >> "+SystemAppsText1[i]+" Column is not displayed even when Clicked on Dynamic Apps Job under System section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] SystemAppsColumn2={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] SystemAppsText2={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<SystemAppsColumn2.length-1;i++)
			{
				boolean value = SystemAppsColumn2[i].isDisplayed();
				PassStatement = "PASS >> "+SystemAppsText2[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job under System section";
				FailStatement = "Fail >> "+SystemAppsText2[i]+" Button is not displayed even when Clicked on Dynamic Apps Job under System section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			ClickOnAllAppsTab();
			String[] AllAppsColumn1 ={AllAppsColumnName.get(0).getAttribute("innerHTML"),AllAppsColumnName.get(1).getAttribute("innerHTML"),AllAppsColumnName.get(2).getAttribute("innerHTML"),AllAppsColumnName.get(3).getAttribute("innerHTML")};
			String[] AllAppsText1={"Application Name","Type","Package","Version"};
			for(int i=0;i<AllAppsColumn1.length-1;i++)
			{
				boolean value = AllAppsText1[i].equals(AllAppsColumn1[i]);
				PassStatement = "PASS >> "+AllAppsText1[i]+" Column is displayed successfully when Clicked on Dynamic Apps Job under All Apps section";
				FailStatement = "Fail >> "+AllAppsText1[i]+" Column is not displayed even when Clicked on Dynamic Apps Job under All Apps section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] AllAppsColumn2={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] AllAppsText2={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<AllAppsColumn2.length-1;i++)
			{
				boolean value = AllAppsColumn2[i].isDisplayed();
				PassStatement = "PASS >> "+AllAppsText2[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job under All Apps section";
				FailStatement = "Fail >> "+AllAppsText2[i]+" Button is not displayed even when Clicked on Dynamic Apps Job under All Apps section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void ClickOnSystemAppsTab() throws Throwable
		{
			SystemTabSection.click();
			waitForidPresent("uninstallAppList");
			sleep(6);
			Reporter.log("Clicked on System Apps Tab",true);
		}

		public void ClickOnAllAppsTab() throws Throwable
		{
			AllAppsTabSection.click();
			waitForidPresent("uninstallAppList");
			sleep(6);
			Reporter.log("Clicked on All Apps Tab", true);
		}

		@FindBy(xpath=".//*[@id='downlodedAppListTable']/tbody/tr/td/p[text()='"+Config.DownloadedApp+"']")
		private WebElement DownloadedApps;
		
		public void VerifyDownloadedApps()
		{
			boolean isDownloadedAppPresent = DownloadedApps.isDisplayed();
		    String pass = "PASS >> 'ES file Manager' Application is present";
			String fail = "FAIL >> 'ES file Manager' Application is Not present";
			ALib.AssertTrueMethod(isDownloadedAppPresent, pass, fail);
		}

		public void VerifyOfDownloadApps_type()
		{
			boolean value = false;
			String PassStatement = "PASS >> App Present in Downloaded Apps List contains with Type 'Installed'";
			String FailStatement = "Fail >> App Present in Downloaded Apps List does not contains with Type 'Installed'";
			for (int i = 0; i < DownloadedAppsType.size(); i++) {
				String ActualValue = DownloadedAppsType.get(i).getAttribute("innerHTML");
				value = ActualValue.equals("Installed");
				if (value == false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfDownloadApps_MasterCheckbox()
		{
			DownloadedAppsMasterCheckbox.click();
			String PassStatement = "PASS >>Apps Checkboxes Present in Downloaded Apps List is checked when master checkbox is enabled";
			String FailStatement = "Fail >>Apps Checkboxes Present in Downloaded Apps List is not checked even when master checkbox is enabled";
			boolean value =true;;
			for (int i = 1; i < DownloadedAppsCheckoxes.size(); i++) {
				value = DownloadedAppsCheckoxes.get(i).isSelected();
				if (value==false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			String PermissionListValue=PermissionList.getAttribute("class");	
			String AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			String ApplyChangesValue=ApplyChangesButton.getAttribute("class");
			value= PermissionListValue.contains("disabled")&&AdvancedSettingsValue.contains("disabled")&&ApplyChangesValue.contains("disabled");
			PassStatement = "PASS >> Permission List,Advanced Settings,Apply Changes Settings is grayed out when master checkbox is enabled";
			FailStatement = "Fail >> Permission List,Advanced Settings,Apply Changes Settings is not grayed out even when master checkbox is enabled";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

			DownloadedAppsMasterCheckbox.click();
			PassStatement = "PASS >> Apps Checkboxes Present in Downloaded Apps List is unchecked when master checkbox is unchecked";
			FailStatement = "Fail >> Apps Checkboxes Present in Downloaded Apps List is unchecked when master checkbox is unchecked";
			for (int i = 1; i < DownloadedAppsCheckoxes.size(); i++) {
				value = DownloadedAppsCheckoxes.get(i).isSelected();
				if (value==true) {
					ALib.AssertFalseMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			PermissionListValue=PermissionList.getAttribute("class");	
			AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			ApplyChangesValue=ApplyChangesButton.getAttribute("class");
			value= PermissionListValue.contains("disabled")&&AdvancedSettingsValue.contains("disabled")&&ApplyChangesValue.contains("disabled");
			PassStatement = "PASS >> Permission List,Advanced Settings,Apply Changes Settings is enabled when master checkbox is unchecked";
			FailStatement = "Fail >> Permission List,Advanced Settings,Apply Changes Settings is not enabled even when master checkbox is unchecked";	
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSystemApps_type()
		{
			boolean value = false;
			String PassStatement = "PASS >> App Present in System Apps List contains with Type 'System'";
			String FailStatement = "Fail >> App Present in System Apps List does not contains with Type 'System'";
			for (int i = 0; i < SystemAppsType.size(); i++) {
				String ActualValue = SystemAppsType.get(i).getAttribute("innerHTML");
				value = ActualValue.equals("System");
				if (value == false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSystemApps_MasterCheckbox()
		{
			SystemAppsMasterCheckbox.click();
			String PassStatement = "PASS >>Apps Checkboxes Present in System Apps List is checked when master checkbox is enabled";
			String FailStatement = "Fail >>Apps Checkboxes Present in System Apps List is not checked even when master checkbox is enabled";
			boolean value =true;;
			for (int i = 1; i < SystemAppsCheckoxes.size(); i++) {
				value = SystemAppsCheckoxes.get(i).isSelected();
				if (value==false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			String PermissionListValue=PermissionList.getAttribute("class");	
			String AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			String ApplyChangesValue=ApplyChangesButton.getAttribute("class");
			value= PermissionListValue.contains("disabled")&&AdvancedSettingsValue.contains("disabled")&&ApplyChangesValue.contains("disabled");
			PassStatement = "PASS >> Permission List,Advanced Settings,Apply Changes Settings is grayed out when master checkbox is enabled";
			FailStatement = "Fail >> Permission List,Advanced Settings,Apply Changes Settings is not grayed out even when master checkbox is enabled";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

			SystemAppsMasterCheckbox.click();
			PassStatement = "PASS >> Apps Checkboxes Present in System Apps List is unchecked when master checkbox is unchecked";
			FailStatement = "Fail >> Apps Checkboxes Present in System Apps List is unchecked when master checkbox is unchecked";
			for (int i = 1; i < SystemAppsCheckoxes.size(); i++) {
				value = SystemAppsCheckoxes.get(i).isSelected();
				if (value==true) {
					ALib.AssertFalseMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			PermissionListValue=PermissionList.getAttribute("class");	
			AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			ApplyChangesValue=ApplyChangesButton.getAttribute("class");
			value= PermissionListValue.contains("disabled")&&AdvancedSettingsValue.contains("disabled")&&ApplyChangesValue.contains("disabled");
			PassStatement = "PASS >> Permission List,Advanced Settings,Apply Changes Settings is enabled when master checkbox is unchecked";
			FailStatement = "Fail >> Permission List,Advanced Settings,Apply Changes Settings is not enabled even when master checkbox is unchecked";	
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfAllApps_type()
		{
			boolean value = false;
			String PassStatement = "PASS >> App Present in All Apps List contains with Type 'Installed' or 'System'";
			String FailStatement = "Fail >> App Present in All Apps List does not contains with Type 'Installed' or 'System'";
			for (int i = 0; i < AllAppsType.size(); i++) {
				String ActualValue = AllAppsType.get(i).getAttribute("innerHTML");
				value = ActualValue.equals("Installed")||ActualValue.equals("System");
				if (value == false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfAllApps_MasterCheckbox()
		{
			AllAppsMasterCheckbox.click();
			String PassStatement = "PASS >>Apps Checkboxes Present in All Apps List is checked when master checkbox is enabled";
			String FailStatement = "Fail >>Apps Checkboxes Present in All Apps List is not checked even when master checkbox is enabled";
			boolean value =true;;
			for (int i = 1; i < AllAppsCheckoxes.size(); i++) {
				value = AllAppsCheckoxes.get(i).isSelected();
				if (value==false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			String PermissionListValue=PermissionList.getAttribute("class");	
			String AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			String ApplyChangesValue=ApplyChangesButton.getAttribute("class");
			value= PermissionListValue.contains("disabled")&&AdvancedSettingsValue.contains("disabled")&&ApplyChangesValue.contains("disabled");
			PassStatement = "PASS >> Permission List,Advanced Settings,Apply Changes Settings is grayed out when master checkbox is enabled";
			FailStatement = "Fail >> Permission List,Advanced Settings,Apply Changes Settings is not grayed out even when master checkbox is enabled";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

			AllAppsMasterCheckbox.click();
			PassStatement = "PASS >> Apps Checkboxes Present in All Apps List is unchecked when master checkbox is unchecked";
			FailStatement = "Fail >> Apps Checkboxes Present in All Apps List is unchecked when master checkbox is unchecked";
			for (int i = 1; i < AllAppsCheckoxes.size(); i++) {
				value = AllAppsCheckoxes.get(i).isSelected();
				if (value==true) {
					ALib.AssertFalseMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			PermissionListValue=PermissionList.getAttribute("class");	
			AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			ApplyChangesValue=ApplyChangesButton.getAttribute("class");
			value= PermissionListValue.contains("disabled")&&AdvancedSettingsValue.contains("disabled")&&ApplyChangesValue.contains("disabled");
			PassStatement = "PASS >> Permission List,Advanced Settings,Apply Changes Settings is enabled when master checkbox is unchecked";
			FailStatement = "Fail >> Permission List,Advanced Settings,Apply Changes Settings is not enabled even when master checkbox is unchecked";	
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void AppsUninstallErrorMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Alert Message : 'Please select the applications to be uninstalled.' is displayed successfully when clicked on uninstall without selecting apps";
			String FailStatement="Fail >> Alert Message : 'Please select the applications to be uninstalled.' is not displayed even when clicked on uninstall without selecting apps";
			Initialization.commonmethdpage.ConfirmationMessageVerify(AppsUninstallErrorMessage, true, PassStatement, FailStatement,2);
		}

		public void SystemAppsUninstallErrorMessage() throws InterruptedException
		{
			String ActualValue=UninstallBtn.getAttribute("class");
			boolean value= ActualValue.contains("disabled");
			String PassStatement = "PASS >> Uninstall Apps is disabled when clicked on uninstall without selecting apps";
			String FailStatement = "Fail >> Uninstall Apps is disabled even when clicked on uninstall without selecting apps";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SystemAppsUninstallErrorMessage_AppsSelected() throws InterruptedException
		{
			String ActualValue=UninstallBtn.getAttribute("class");
			boolean value= ActualValue.contains("disabled");
			String PassStatement = "PASS >> Uninstall Apps is disabled when clicked on uninstall with selecting apps";
			String FailStatement = "Fail >> Uninstall Apps is disabled even when clicked on uninstall with selecting apps";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void AllSystemAppsUninstallErrorMessage_AppsSelected() throws InterruptedException
		{
			String ActualValue=UninstallBtn.getAttribute("class");
			boolean value= ActualValue.contains("disabled");
			String PassStatement = "PASS >> Uninstall Apps is disabled when clicked on uninstall with selecting system apps";
			String FailStatement = "Fail >> Uninstall Apps is not disabled even when clicked on uninstall with selecting system apps";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnApplication_DownloadedApps(String ApplicationName) throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/preceding-sibling::td[2]/span/input")).click();
			sleep(5);
		}

		public void ClickOnApplication_SystemApps(String ApplicationName) throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/preceding-sibling::td[2]/span/input")).click();
			sleep(5);
		}

		public void ClickOnApplication_AllApps(String ApplicationName) throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/preceding-sibling::td[2]/span/input")).click();
			sleep(5);
		}

		public void VerifyOfUninstall()
		{
			String ActualValue=AppsUninstallpopup.getText();
			String ExpectedValue="Do you want to uninstall "+Config.Uninstall_DownloadedApps+"?";
			String PassStatement="PASS >> "+ActualValue+" Summary is displayed successfully when clicked on Uninstall Button of Dynamic Apps";
			String FailStatement="FAIL >> "+ActualValue+" Summary is not displayed even when clicked on Uninstall Button of Dynamic Apps"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=AppsUninstallNoButton.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on Uninstall Button of Dynamic Apps";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on Uninstall Button of Dynamic Apps"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=AppsUninstallYesButton.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Uninstall Button of Dynamic Apps";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Uninstall Button of Dynamic Apps"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnUninstallAppsNoButton() throws Throwable
		{
			AppsUninstallNoButton.click();
			waitForidPresent("uninstallAppList");
			sleep(2);
			Reporter.log("Clicked on No button of Uninstall popup of Dynamic Apps", true);
		}

		public void ClickOnUninstallAppsYesButton() throws Throwable
		{
			AppsUninstallYesButton.click();
			Reporter.log("Clicked on Yes button of Uninstall popup of Dynamic Apps", true);
		}

		public void AllAppsUninstallAlertMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Alert Message : 'System apps cannot be uninstalled. Please unselect system apps.' is displayed successfully when clicked on Yes button of Uninstall of Dynamic Apps";
			String FailStatement="Fail >> Alert Message : 'System apps cannot be uninstalled. Please unselect system apps.' is not displayed even when clicked on Yes button of Uninstall of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerify(AllAppsUninstallErrorMessage, true, PassStatement, FailStatement,2);
		}

		public void AppsUninstallConfirmationMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Initiated application uninstall.' is displayed successfully when clicked on Yes button of Uninstall of Dynamic Apps";
			String FailStatement="Fail >> Confirmation Message : 'Initiated application uninstall.' is not displayed even when clicked on Yes button of Uninstall of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerify(AppsUninstallConfirmationMessage, true, PassStatement, FailStatement,2);
		}

		public void AppsUninstallConfirmationMessage_False() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Initiated application uninstall.' is not displayed when clicked on No button of Uninstall of Dynamic Apps";
			String FailStatement="Fail >> Confirmation Message : 'Initiated application uninstall.' is displayed even when clicked on No button of Uninstall of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(AppsUninstallConfirmationMessage, false, PassStatement, FailStatement,2);
		}

		public void ClearDataErrorMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Alert Message : 'Please select the applications to clear data.' is displayed successfully when clicked on Clear Data without selecting apps on  Device";
			String FailStatement="Fail >> Alert Message : 'Please select the applications to clear data.' is not displayed even when clicked on Clear Data without selecting apps on Device";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ClearDataErrorMessage, true, PassStatement, FailStatement,2);
		}

		public void VerifyOfClearData()
		{
			String ActualValue=AppsUninstallpopup.getText();
			String ExpectedValue="Do you want to clear data of "+Config.ClearData_DownloadedApps+"?";
			String PassStatement="PASS >> "+ActualValue+" Summary is displayed successfully when clicked on Clear Data Button of Dynamic Apps for Knox/rooted device";
			String FailStatement="FAIL >> "+ActualValue+" Summary is not displayed even when clicked on Clear Data Button of Dynamic Apps for Knox/rooted device"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=AppsUninstallNoButton.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on Clear Data Button of Dynamic Apps for Knox/rooted device";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on Clear Data Button of Dynamic Apps for Knox/rooted device"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=AppsUninstallYesButton.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Clear Data Button of Dynamic Apps for Knox/rooted device";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Clear Data Button of Dynamic Apps for Knox/rooted device"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnClearDataAppsNoButton() throws Throwable
		{
			AppsUninstallNoButton.click();
			waitForidPresent("uninstallAppList");
			sleep(2);
			Reporter.log("Clicked on No button of Clear Data popup of Dynamic Apps", true);
		}

		public void ClickOnClearDataAppsYesButton() throws Throwable
		{
			AppsUninstallYesButton.click();
			Reporter.log("Clicked on Yes button of Clear Data popup of Dynamic Apps", true);
			//		waitForXpathPresent("//span[text()='Application data cleared.");
			//		sleep(2);

		}

		public void ClearDataConfirmationMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Application data cleared.' is displayed successfully when clicked on Yes button of Uninstall of Dynamic Apps for Knox/rooted device";
			String FailStatement="Fail >> Confirmation Message : 'Application data cleared.' is not displayed even when clicked on Yes button of Uninstall of Dynamic Apps for Knox/rooted device";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ClearDataConfirmationMessage, true, PassStatement, FailStatement,2);
			sleep(5);
		}

		public void ClearDataConfirmationMessage_False() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Application data cleared.' is not displayed when clicked on No button of Uninstall of Dynamic Apps for Knox/rooted device";
			String FailStatement="Fail >> Confirmation Message : 'Application data cleared.' is displayed even when clicked on No button of Uninstall of Dynamic Apps for Knox/rooted device";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(ClearDataConfirmationMessage, false, PassStatement, FailStatement,2);
		}

		public void ClickOnApplicationLock_DownloadApps(String ApplicationName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/following-sibling::td[2]/span/input")).click();
			sleep(5);
		}

		public void ClickOnApplicationLock_SystemApps(String ApplicationName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/following-sibling::td[2]/span/input")).click();
			sleep(5);
		}

		public void ClickOnApplicationLock_AllApps(String ApplicationName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/following-sibling::td[2]/span/input")).click();
			sleep(5);
		}

		public void VerifyOfLock_disabled()
		{
			String AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			boolean value= AdvancedSettingsValue.contains("disabled");
			String PassStatement = "PASS >> Advanced Settings is disabled when lock is not enabled for any application";
			String FailStatement = "Fail >> Advanced Settings is enabled even when lock is not enabled for any application";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfLock_enabled()
		{
			String AdvancedSettingsValue=AdvancedSettingsButton.getAttribute("class");
			boolean value= AdvancedSettingsValue.contains("disabled");
			String PassStatement = "PASS >> Advanced Settings is enabled when lock is enabled for any application";
			String FailStatement = "Fail >> Advanced Settings is disabled even when lock is enabled for any application";	
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnAdvancedSettings() throws Throwable
		{
			AdvancedSettingsButton.click();
			waitForidPresent("applistunlockpin");
			sleep(6);
			Reporter.log("Clicked On Advanced Settings, Navigates successfully to Advanced settings popup",true);
		}

		public void VerifyOfAdvancedSettings_Cancel() throws Throwable
		{
			sleep(5);
			String ActualValue=AdvancedSettingsPinTextBox.getAttribute("value");
			String ExpectedValue="0000";
			boolean value=ExpectedValue.equals(ActualValue);
			String PassStatement = "PASS >> Default Pin value as "+ExpectedValue+" is not changed successfully when clicked on Cancel of Advanced Settings";
			String FailStatement = "Fail >> Default Pin value as "+ExpectedValue+" is changed even when clicked on Cancel of Advanced Settings";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfAdvancedSettings_Ok() throws Throwable
		{
			sleep(5);
			String ActualValue=AdvancedSettingsPinTextBox.getAttribute("value");
			String ExpectedValue="1111";
			boolean value=ExpectedValue.equals(ActualValue);
			String PassStatement = "PASS >> Default Pin value as "+ExpectedValue+" is changed successfully when clicked on Ok of Advanced Settings";
			String FailStatement = "Fail >> Default Pin value as "+ExpectedValue+" is not changed even when clicked on Ok of Advanced Settings";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfAdvancedSettings()
		{
			boolean value=AdvancedSettingsHeader.isDisplayed();
			String PassStatement = "PASS >> 'Advanced Settings' Header is displayed successfully when clicked on Advanced Settings";
			String FailStatement = "Fail >> 'Advanced Settings' Header is not displayed even when clicked on Advanced Settings";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			String ActualValue=AdvancedSettingsSummary.getText();
			String ExpectedValue="Pin that will unlock locked applications on device";
			PassStatement = "PASS >> 'Advanced Settings' Summary as "+ActualValue+" is displayed successfully when clicked on Advanced Settings";
			FailStatement = "Fail >> 'Advanced Settings' Summary as "+ActualValue+" is not displayed even when clicked on Advanced Settings";	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			value=AdvancedSettingsPinTextBox.isDisplayed();
			PassStatement = "PASS >> Textbox to enter Pin is displayed successfully when clicked on Advanced Settings";
			FailStatement = "Fail >> Textbox to enter Pin is not displayed even when clicked on Advanced Settings";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			ActualValue=AdvancedSettingsPinTextBox.getAttribute("value");
			ExpectedValue="0000";
			PassStatement = "PASS >> Default Pin value as "+ExpectedValue+" is displayed successfully when clicked on Advanced Settings";
			FailStatement = "Fail >> Default Pin value as "+ExpectedValue+" is not displayed even when clicked on Advanced Settings";	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			value=AdvancedSettingsOkButton.isDisplayed();
			PassStatement = "PASS >> 'Ok' button is displayed successfully when clicked on Advanced Settings";
			FailStatement = "Fail >> 'Ok' button as is not displayed even when clicked on Advanced Settings";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=AdvancedSettingsCancelbutton.isDisplayed();
			PassStatement = "PASS >> 'Cancel' button is displayed successfully when clicked on Advanced Settings";
			FailStatement = "Fail >> 'Cancel' button is not displayed even when clicked on Advanced Settings";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnCloseOfAdvancedSettings() throws Throwable
		{
			AdvancedSettingsClosebutton.click();
			waitForidPresent("uninstallAppList");
			sleep(3);
			Reporter.log("Clicked On Close of Advanced Settings, Navigates successfully to Dynamic Apps popup",true);
		}

		public void ClearPin()
		{
			AdvancedSettingsPinTextBox.clear();
		}

		public void EnterPin()
		{
			AdvancedSettingsPinTextBox.sendKeys("1111");
		}

		public void ClickOnAdvancedSettingsCancelButton() throws Throwable
		{
			AdvancedSettingsCancelbutton.click();
			waitForidPresent("uninstallAppList");
			sleep(2);
			Reporter.log("Clicked on Cancel button of Advanced Settings popup of Dynamic Apps", true);
		}

		public void ClickOnAdvancedSettingsOkButton() throws Throwable
		{
			AdvancedSettingsOkButton.click();
			Reporter.log("Clicked on Ok button of Advanced Settings popup of Dynamic Apps", true);
		}

		public void PinChangeConfirmationMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Pin changed successfully.' is displayed successfully when clicked on Ok button of Advanced Settings of Dynamic Apps";
			String FailStatement="Fail >> Confirmation Message : 'Pin changed successfully.' is not displayed even when clicked on Ok button of Advanced Settings of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerify(PinChangeConfirmationMessage, true, PassStatement, FailStatement,2);
		}

		public void PinChangeConfirmationMessage_False() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Pin changed successfully.' is not displayed successfully when clicked on Cancel button of Advanced Settings of Dynamic Apps";
			String FailStatement="Fail >> Confirmation Message : 'Pin changed successfully.' is displayed even when clicked on Cancel button of Advanced Settings of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(PinChangeConfirmationMessage, false, PassStatement, FailStatement,2);
		}

		public void ClickOnApplyChanges() throws Throwable
		{
			ApplyChangesButton.click();
			sleep(6);
			Reporter.log("Clicked on Apply Changes of Dynamic Apps", true);
		}

		public void ClickOnApplyChanges_Error() throws Throwable
		{
			ApplyChangesButton.click();
			Reporter.log("Clicked on Apply Changes of Dynamic Apps", true);
		}

		public void VerifyOfApplyChanges()
		{
			String ActualValue=AdvancedSettingsSummary.getText();
			String ExpectedValue="Changes will be applied on application. Do you wish to continue ?";
			String PassStatement = "PASS >> 'Apply Changes' Summary as "+ActualValue+" is displayed successfully when clicked on Apply Changes";
			String FailStatement = "Fail >> 'Advanced Settings' Summary as "+ActualValue+" is not displayed even when clicked on Apply Changes";	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=AdvancedSettingsOkButton.isDisplayed();
			PassStatement = "PASS >> 'Ok' button is displayed successfully when clicked on Apply changes";
			FailStatement = "Fail >> 'Ok' button as "+ActualValue+" is not displayed even when clicked on Apply changes";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=AdvancedSettingsCancelbutton.isDisplayed();
			PassStatement = "PASS >> 'Cancel' button is displayed successfully when clicked on Apply changes";
			FailStatement = "Fail >> 'Cancel' button is not displayed even when clicked on Apply changes";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnApplyChangesCancelButton() throws Throwable
		{
			AdvancedSettingsCancelbutton.click();
			waitForidPresent("uninstallAppList");
			sleep(2);
			Reporter.log("Clicked on Cancel button of Apply Changes popup of Dynamic Apps", true);
		}

		public void ClickOnApplyChangesOkButton() throws Throwable
		{
			AdvancedSettingsOkButton.click();
			Reporter.log("Clicked on Ok button of Apply Changes popup of Dynamic Apps", true);
		}

		public void ApplyChangesConfirmationMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Settings applied successfully.' is displayed successfully when clicked on Ok button of Apply Changes of Dynamic Apps";
			String FailStatement="Fail >> Confirmation Message : 'Settings applied successfully.' is not displayed even when clicked on Ok button of Apply Changes of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ApplyChangesConfirmationMessage, true, PassStatement, FailStatement,2);
		}

		public void ApplyChangesConfirmationMessage_False() throws InterruptedException
		{
			String PassStatement="PASS >> Confirmation Message : 'Settings applied successfully.' is not displayed successfully when clicked on Cancel button of Apply Changes of Dynamic Apps";
			String FailStatement="Fail >> Confirmation Message : 'Settings applied successfully.' is displayed even when clicked on Cancel button of Apply Changes of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(ApplyChangesConfirmationMessage, false, PassStatement, FailStatement,2);
		}

		public void ClickOnApplicationRunAtStartup_DownloadApps(String ApplicationName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/following-sibling::td[3]/span/input")).click();
			sleep(7);
		}

		public void ClickOnApplicationRunAtStartup_SystemApps(String ApplicationName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/following-sibling::td[3]/span/input")).click();
			sleep(7);
		}

		public void ClickOnApplicationRunAtStartup_AllApps(String ApplicationName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td[p[text()='"+ApplicationName+"']]/following-sibling::td[3]/span/input")).click();
			sleep(7);
		}

		public void VerifyOfRunAtStartupDownload_disabled(String ApplicationPackageName)
		{
			String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).getAttribute("disabled");
			boolean value= ActualValue.contains("true");
			String PassStatement = "PASS >> Start Up Delay in seconds is disabled when RunAtStartup is not enabled for any application";
			String FailStatement = "Fail >> Start Up Delay in seconds is enabled even when RunAtStartup is not enabled for any application";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfRunAtStartupSystem_disabled(String ApplicationPackageName)
		{
			String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).getAttribute("disabled");
			boolean value= ActualValue.contains("true");
			String PassStatement = "PASS >> Start Up Delay in seconds is disabled when RunAtStartup is not enabled for any application";
			String FailStatement = "Fail >> Start Up Delay in seconds is enabled even when RunAtStartup is not enabled for any application";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfRunAtStartupAll_disabled(String ApplicationPackageName)
		{
			String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).getAttribute("disabled");
			boolean value= ActualValue.contains("true");
			String PassStatement = "PASS >> Start Up Delay in seconds is disabled when RunAtStartup is not enabled for any application";
			String FailStatement = "Fail >> Start Up Delay in seconds is enabled even when RunAtStartup is not enabled for any application";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfRunAtStartupDownload_enabled(String ApplicationPackageName)
		{
			String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).getAttribute("disabled");
			System.out.println(ActualValue);
			boolean value=false;
			if(ActualValue==null || ActualValue.isEmpty() || ActualValue=="")
				value=true;
			else
				value=false;
			String PassStatement = "PASS >> Start Up Delay in seconds is enabled when RunAtStartup is enabled for any application";
			String FailStatement = "Fail >> Start Up Delay in seconds is disabled even when RunAtStartup is enabled for any application";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfRunAtStartupSystem_enabled(String ApplicationPackageName)
		{
			String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).getAttribute("disabled");
			boolean value=false;
			if(ActualValue==null || ActualValue.isEmpty()|| ActualValue=="")
				value=true;
			else
				value=false;
			String PassStatement = "PASS >> Start Up Delay in seconds is enabled when RunAtStartup is enabled for any application";
			String FailStatement = "Fail >> Start Up Delay in seconds is disabled even when RunAtStartup is enabled for any application";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfRunAtStartupAll_enabled(String ApplicationPackageName)
		{
			String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).getAttribute("disabled");
			boolean value=false;
			if(ActualValue==null || ActualValue.isEmpty()|| ActualValue=="")
				value=true;
			else
				value=false;
			String PassStatement = "PASS >> Start Up Delay in seconds is enabled when RunAtStartup is enabled for any application";
			String FailStatement = "Fail >> Start Up Delay in seconds is disabled even when RunAtStartup is enabled for any application";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void EnterStartUpDelay_Download(String ApplicationPackageName)
		{
			Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).sendKeys("10");
		}

		public void EnterStartUpDelay_System(String ApplicationPackageName)
		{
			Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).sendKeys("10");
		}

		public void EnterStartUpDelay_All(String ApplicationPackageName)
		{
			Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).sendKeys("10");
		}

		public void ClearStartUpDelay_Download(String ApplicationPackageName)
		{
			Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).clear();
		}

		public void ClearStartUpDelay_System(String ApplicationPackageName)
		{
			Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).clear();
		}

		public void ClearStartUpDelay_All(String ApplicationPackageName)
		{
			Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td/input[@id='startupdelay_"+ApplicationPackageName+"']")).clear();
		}

		public void NoChangesErrorMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Alert Message : 'No changes in application settings detected.' is displayed successfully when clicked on Apply Changes of Dynamic Apps";
			String FailStatement="Fail >> Alert Message : 'No changes in application settings detected.' is not displayed even when clicked on Apply Changes of Dynamic Apps";
			Initialization.commonmethdpage.ConfirmationMessageVerify(NoChangesErrorMessage, true, PassStatement, FailStatement,5);
		}

		public void EnterSearchOfDownloadedApps(String AppName) throws InterruptedException
        {
                DownloadAppsSearch.clear();
                DownloadAppsSearch.sendKeys(AppName);
                sleep(2);
        }

		public void EnterSearchOfSystemApps(String AppName)
		{
			SystemAppsSearch.sendKeys(AppName);
		}

		public void EnterSearchOfAllApps(String AppName)
		{
			AllAppsSearch.sendKeys(AppName);
		}

		public void ClearSearchOfDownloadedApps() throws InterruptedException
        {
                DownloadAppsSearch.clear();
                sleep(5);
                DownloadAppsSearch.sendKeys(Keys.ENTER);
        }

		public void ClearSearchOfSystemApps()
		{
			SystemAppsSearch.clear();
		}

		public void ClearSearchOfAllApps()
		{
			AllAppsSearch.clear();
		}

		public void SearchOfDownloadApps(String AppName)
		{
			boolean value;
			try {
				Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td/p[text()='"+AppName+"']")).isDisplayed();
				value = true;
			} catch (Exception e) {
				value = false;
			}
			String PassStatement="PASS >> "+AppName+" application is Searched succesfully in Downloaded App Section";
			String FailStatement="FAIL >> "+AppName+" application is not Searched in Downloaded App Section";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SearchOfDownloadApps_Package(String PackageName)
		{
			boolean value;
			try {
				Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td/p[text()='"+PackageName+"']")).isDisplayed();
				value = true;
			} catch (Exception e) {
				value = false;
			}
			String PassStatement="PASS >> "+PackageName+" package is Searched succesfully in Downloaded App Section";
			String FailStatement="FAIL >> "+PackageName+" package is not Searched in Downloaded App Section";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SearchOfSystemApps(String AppName)
		{
			boolean value;
			try {
				Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td/p[text()='"+AppName+"']")).isDisplayed();
				value = true;
			} catch (Exception e) {
				value = false;
			}
			String PassStatement="PASS >> "+AppName+" application is Searched succesfully in System App Section";
			String FailStatement="FAIL >> "+AppName+" application is not Searched in System App Section";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SearchOfSystemApps_Package(String PackageName)
		{
			boolean value;
			try {
				Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td/p[text()='"+PackageName+"']")).isDisplayed();
				value = true;
			} catch (Exception e) {
				value = false;
			}
			String PassStatement="PASS >> "+PackageName+" package is Searched succesfully in System App Section";
			String FailStatement="FAIL >> "+PackageName+" package is not Searched in System App Section";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SearchOfAllApps(String AppName)
		{
			boolean value;
			try {
				Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td/p[text()='"+AppName+"']")).isDisplayed();
				value = true;
			} catch (Exception e) {
				value = false;
			}
			String PassStatement="PASS >> "+AppName+" application is Searched succesfully in All App Section";
			String FailStatement="FAIL >> "+AppName+" application is not Searched in All App Section";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SearchOfAllApps_Package(String PackageName)
		{
			boolean value;
			try {
				Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td/p[text()='"+PackageName+"']")).isDisplayed();
				value = true;
			} catch (Exception e) {
				value = false;
			}
			String PassStatement="PASS >> "+PackageName+" package is Searched succesfully in All App Section";
			String FailStatement="FAIL >> "+PackageName+" package is not Searched in All App Section";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SearchErrorMessage_DownloadedApps() throws InterruptedException
		{
			String PassStatement="PASS >> Error Message : 'No matching result found.' is displayed successfully when Searched with system apps in Downloaded Apps Section";
			String FailStatement="Fail >> Error Message : 'No matching result found.' is not displayed when Searched with system apps in Downloaded Apps Section";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SearchErrorMeesage, true, PassStatement, FailStatement,2);
		}

		public void SearchErrorMessage_SystemApps() throws InterruptedException
		{
			String PassStatement="PASS >> Error Message : 'No matching result found.' is displayed successfully when Searched with installed apps in System Apps Section";
			String FailStatement="Fail >> Error Message : 'No matching result found.' is not displayed when Searched with system apps in System Apps Section";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SearchErrorMeesage, true, PassStatement, FailStatement,2);
		}

		public void SearchErrorMessage() throws InterruptedException
		{
			String PassStatement="PASS >> Error Message : 'No matching result found.' is displayed successfully when Searched with invalid app name";
			String FailStatement="Fail >> Error Message : 'No matching result found.' is not displayed even when Searched with invalid app name";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SearchErrorMeesage, true, PassStatement, FailStatement,2);
		}

		public void ClickOnPermission() throws Throwable
		{

			PermissionList.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed()){}
			waitForXpathPresent("//div[@id='appPermissionDialog']/div/div/div/button[text()='OK']");
			waitForXpathPresent("//div[@id='appPermissionDialog']/div/div/div/button[text()='OK']");
			sleep(10);
		}

		public void ClickOnPermission_DownloadedApps(String AppName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='downlodedAppListTable']/tbody/tr/td/p[text()='"+AppName+"']")).click();
			sleep(5);
			ClickOnPermission();
		}

		public void VerifyOfDynamicAppsUI(String device) throws Throwable
		{
			try {
			String ActualValue=DynamicAppsHeader.getText();
			String ExpectedValue="Application List - "+device;
			String PassStatement="PASS >> "+ActualValue+" Header is displayed successfully when clicked on Apps Dynamic Job";
			String FailStatement="FAIL >> "+ActualValue+" Header is not displayed even when clicked on Apps Dynamic Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			WebElement[] Appsbutton={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] Text={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<Appsbutton.length-1;i++)
			{
				boolean value = Appsbutton[i].isDisplayed();
				PassStatement = "PASS >> "+Text[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job";
				FailStatement = "Fail >> "+Text[i]+" Button is not displayed even when Clicked on Dynamic Apps Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] AppsTab={PermissionList,UninstallBtn,ClearDataBtn};
			String[] Text2={"Permissions","Uninstall","Clear Data"};
			for(int i=0;i<AppsTab.length-1;i++)
			{
				boolean value = AppsTab[i].isDisplayed();
				PassStatement = "PASS >> "+Text2[i]+" Tab is displayed successfully when Clicked on Dynamic Apps Job";
				FailStatement = "Fail >> "+Text2[i]+" Tab is not displayed even when Clicked on Dynamic Apps Job";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			String[] DownloadAppsColumn1 ={DownloadAppsColumnName.get(0).getAttribute("innerHTML"),DownloadAppsColumnName.get(1).getAttribute("innerHTML"),DownloadAppsColumnName.get(2).getAttribute("innerHTML"),DownloadAppsColumnName.get(3).getAttribute("innerHTML")};
			String[] DownloadAppsText1={"Application Name","Type","Package","Version"};
			for(int i=0;i<DownloadAppsColumn1.length-1;i++)
			{
				boolean value = DownloadAppsText1[i].equals(DownloadAppsColumn1[i]);
				PassStatement = "PASS >> "+DownloadAppsText1[i]+" Column is displayed successfully when Clicked on Dynamic Apps Job under Download section";
				FailStatement = "Fail >> "+DownloadAppsText1[i]+" Column is not displayed even when Clicked on Dynamic Apps Job under Download section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] DownloadAppsColumn2={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] DownloadAppsText2={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<DownloadAppsColumn2.length-1;i++)
			{
				boolean value = DownloadAppsColumn2[i].isDisplayed();
				PassStatement = "PASS >> "+DownloadAppsText2[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job under Download section";
				FailStatement = "Fail >> "+DownloadAppsText2[i]+" Button is not displayed even when Clicked on Dynamic Apps Job under Download section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			ClickOnSystemAppsTab();
			String[] SystemAppsColumn1 ={SystemAppsColumnName.get(0).getAttribute("innerHTML"),SystemAppsColumnName.get(1).getAttribute("innerHTML"),SystemAppsColumnName.get(2).getAttribute("innerHTML"),SystemAppsColumnName.get(3).getAttribute("innerHTML")};
			String[] SystemAppsText1={"Application Name","Type","Package","Version"};
			for(int i=0;i<SystemAppsColumn1.length-1;i++)
			{
				boolean value = SystemAppsText1[i].equals(SystemAppsColumn1[i]);
				PassStatement = "PASS >> "+SystemAppsText1[i]+" Column is displayed successfully when Clicked on Dynamic Apps Job under System section";
				FailStatement = "Fail >> "+SystemAppsText1[i]+" Column is not displayed even when Clicked on Dynamic Apps Job under System section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] SystemAppsColumn2={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] SystemAppsText2={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<SystemAppsColumn2.length-1;i++)
			{
				boolean value = SystemAppsColumn2[i].isDisplayed();
				PassStatement = "PASS >> "+SystemAppsText2[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job under System section";
				FailStatement = "Fail >> "+SystemAppsText2[i]+" Button is not displayed even when Clicked on Dynamic Apps Job under System section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			ClickOnAllAppsTab();
			String[] AllAppsColumn1 ={AllAppsColumnName.get(0).getAttribute("innerHTML"),AllAppsColumnName.get(1).getAttribute("innerHTML"),AllAppsColumnName.get(2).getAttribute("innerHTML"),AllAppsColumnName.get(3).getAttribute("innerHTML")};
			String[] AllAppsText1={"Application Name","Type","Package","Version"};
			for(int i=0;i<AllAppsColumn1.length-1;i++)
			{
				boolean value = AllAppsText1[i].equals(AllAppsColumn1[i]);
				PassStatement = "PASS >> "+AllAppsText1[i]+" Column is displayed successfully when Clicked on Dynamic Apps Job under All Apps section";
				FailStatement = "Fail >> "+AllAppsText1[i]+" Column is not displayed even when Clicked on Dynamic Apps Job under All Apps section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			WebElement[] AllAppsColumn2={PermissionList,UninstallBtn,ClearDataBtn,AppsRefreshBtn,AdvancedSettingsButton,ApplyChangesButton};
			String[] AllAppsText2={"Permissions","Uninstall","Clear Data","Refresh","Advanced Settings","Apply Changes"};
			for(int i=0;i<AllAppsColumn2.length-1;i++)
			{
				boolean value = AllAppsColumn2[i].isDisplayed();
				PassStatement = "PASS >> "+AllAppsText2[i]+" Button is displayed successfully when Clicked on Dynamic Apps Job under All Apps section";
				FailStatement = "Fail >> "+AllAppsText2[i]+" Button is not displayed even when Clicked on Dynamic Apps Job under All Apps section";
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
			}
			catch (Exception e) {
				Initialization.driver.findElement(By.xpath("//div[@id='applistDetailsAndroid']//button[@aria-label='Close']")).click();
				ALib.AssertFailMethod("Expected values are not present");
			}
			Initialization.driver.findElement(By.xpath("//div[@id='applistDetailsAndroid']//button[@aria-label='Close']")).click();
			sleep(2);
		}
		
		@FindBy(xpath="//*[@id='url_https']/span[1]/input")
		private WebElement HttpsRadioBtn;
		public void ClickOnHttpsRadioBtnAllowedUrl() throws InterruptedException {
			HttpsRadioBtn.click();
			sleep(2);
		}
		
		@FindBy(xpath="//div[text()='Connection timed out']")
		private WebElement ConnectionTimedOutError;
		
		@FindBy(xpath="(//input[@value='Next'])[2]")
		private WebElement NextBtn2SurefoxPopUp;
		
		@FindBy(xpath="(//div[text()='Loader'])[1]")
		private WebElement LoaderIndicator;
		
		@FindBy(xpath="//a[@class='retryFailed']")
		private WebElement ConnectionTimedOutErrorRetry;
		
		@FindBy(xpath="(//h5[text()='Success'])[1]")
		private WebElement SuccessMsgSurefox;

		public void VerifySuccessfullyInstalledMsg() throws InterruptedException {
			while(ApplyingSureLockSetingsText.isDisplayed() && ApplyingSureLockSettingsLoadingIcon.isDisplayed()) {
			} 
			try {
				if(AnerrorOccurredMsg.isDisplayed()) {
					Reporter.log("Clicking on retry",true);
					ApplyingsurelockSettingRetryButton.click();
					while(ApplyingSureLockSetingsText.isDisplayed() && ApplyingSureLockSettingsLoadingIcon.isDisplayed()) {}
					try {
						if(AnerrorOccurredMsg.isDisplayed()) {
							Reporter.log("Clicking on retry",true);
							ApplyingsurelockSettingRetryButton.click();
							while(ApplyingSureLockSetingsText.isDisplayed() && ApplyingSureLockSettingsLoadingIcon.isDisplayed()){}
						}}catch (Exception e) {}
				}}catch (Exception e) {}
			boolean status=false;
			try {
				sleep(60);
				Initialization.driver.findElement(By.xpath("//span[text()='Settings applied successfully.'")).isDisplayed();
			}catch (Exception e) {
				status=true;
			}
			ALib.AssertTrueMethod(status, "PASS >> Settings applied successfully message displayed.", "FAIL >> Settings applied successfully message not displayed.");
			waitForXpathPresent("(//h5[text()='Success'])[1]");
			boolean bln =SuccessMsgSurefox.isDisplayed();
			String pass="PASS >> 'Success' message icon is displayed After Installation ";
			String fail="FAIL >>> 'Success' message icon  is not displayed After Installation";
			ALib.AssertTrueMethod(bln, pass, fail);
			sleep(15);
		}

		public void ClickOnNextBtnSurefoxpopup() throws InterruptedException {

			NextBtn2SurefoxPopUp.click();
			while(LoaderIndicator.isDisplayed() && LoaderIndicatorMsg.isDisplayed()) {}
			try{
				if(Initialization.driver.findElement(By.xpath("//div[text()='Connection timed out']")).isDisplayed()) 
				{
					Reporter.log("1.Clicking on retry",true);
					ConnectionTimedOutErrorRetry.click(); 
					sleep(4);
					while(Initialization.driver.findElement(By.xpath("//div[text()='Trying to connect to device...']")).isDisplayed()){}
					while(LoaderIndicator.isDisplayed() && LoaderIndicatorMsg.isDisplayed()){}
					try{
						if(ConnectionTimedOutError.isDisplayed()) 
						{
							Reporter.log("2.Clicking on retry",true);
							ConnectionTimedOutErrorRetry.click(); 
							sleep(4);
							while(Initialization.driver.findElement(By.xpath("//div[text()='Trying to connect to device...']")).isDisplayed()) {}
							while(LoaderIndicator.isDisplayed() && LoaderIndicatorMsg.isDisplayed()){}
							try{
								if(ConnectionTimedOutError.isDisplayed()) 
								{
									Reporter.log("3.Clicking on retry",true);
									ConnectionTimedOutErrorRetry.click(); 
									sleep(4);
									while(Initialization.driver.findElement(By.xpath("//div[text()='Trying to connect to device...']")).isDisplayed()){}
									while(LoaderIndicator.isDisplayed() && LoaderIndicatorMsg.isDisplayed()){}
								}}catch (Exception e) {}
						}}catch (Exception e) {}
				}}catch (Exception e) {}
			while(InstallingLoader.isDisplayed() && InstallingMsg.isDisplayed()){}
			waitForXpathPresent("(//strong[text()='Please follow instruction provided in device to install.'])[1]");
			sleep(12);
		}
		
		@FindBy(xpath="(//div[text()='Downloading'])[1]")
		private WebElement LoaderIndicatorMsg;

		@FindBy(xpath="//div[text()='Installing...']")
		private WebElement InstallingMsg;

		@FindBy(id="activationCode")
		private WebElement AcivatioonKeyTFsurefox;
		public void EnterActivationKey(String ActivationKey) throws InterruptedException {
			Reporter.log("Entering Activation Key",true);
			AcivatioonKeyTFsurefox.sendKeys(ActivationKey);
			sleep(3);
		}

		@FindBy(xpath="(//input[@value='Next'])[1]")
		private WebElement NextBtnSurefoxPopUp;
		public void ClickonNextBtnSureFoxSetup() throws InterruptedException {
			NextBtnSurefoxPopUp.click();
			waitForXpathPresent("(//input[@value='Next'])[2]");
			sleep(2);
			Reporter.log("Clicked on Next Button 'surefox AllowedWebsite page'",true);
		}
		
		@FindBy(xpath = "//span[text()='Settings Saved Succesfully']")
		private WebElement SaveAsSourceConfirmationMessage;
		public void SaveAsSource_ConfirmationMessage() throws Throwable
		{
			boolean a = SaveAsSourceConfirmationMessage.isDisplayed();
			String pass = "PASS >> Setting Saved Succesfully message is displayed";
			String fail = "FAIL >> Setting Saved Succesfully message is not displayed";
			ALib.AssertTrueMethod(a, pass, fail);
			sleep(4);
		}
		public void ConfirmationMessageApplySettings() throws InterruptedException{
			
			boolean a =  ApplySettingsConfirmationMessage.isDisplayed();
			String pass = "PASS >> Settings applied message is displayed";
			String fail = "FAIL >> Settings applied message is not displayed";
			ALib.AssertTrueMethod(a, pass, fail);
			sleep(5);
		}
		
		@FindBy(xpath="//input[@value='Install Now']")
		private WebElement InstallNowButtonSurefoxSetUp;
		
		@FindBy(xpath="//h5[text()='Welcome to SureFox Setup.']")
		private WebElement WelcomeSurefoxSetUp;
		public void ClickOnInstallButtonSurefoxSetup() throws InterruptedException {
			
			sleep(4);
			boolean bln=WelcomeSurefoxSetUp.isDisplayed();
			String pass="PASS >> 'Welcome to surefox setup' is displayed";
			String fail="FAIL >> 'Welcome to surefox setup' is not displayed";
			ALib.AssertTrueMethod(bln, pass, fail);
			waitForXpathPresent("//input[@value='Install Now']");
			sleep(2);
			InstallNowButtonSurefoxSetUp.click();
			waitForidPresent("surefoxAllowedWebsite");
			sleep(2);
			Reporter.log("Clicked on Next Button 'Welcome to surefox setup'",true);
			
		}
		
		
		public void AllowedwebsiteHttps(String URL,String WebsiteName) throws Throwable
		{
			ClickOnAllowedWebsite();
			AddURL.click();
			waitForidPresent("AllowSubDomain");
			sleep(2);
			Reporter.log("Clicked on Add URL of Allowed Websites",true);
			ClickOnHttpsRadioBtnAllowedUrl();
			EnterURL.sendKeys(URL);
			EnterName.sendKeys(WebsiteName);
			AddURLDone.click();
			waitForidPresent("doneButton");
			AllowedWebsiteDoneBtn.click();
			waitForidPresent("surefoxAllowedWebsite");
			sleep(2);
			Reporter.log("Allowed Website with https "+WebsiteName+"",true);
		}

		@FindBy(xpath="//*[@id='modal_title_startup']")
		private WebElement SureFoxSettingsHeader;
		
		@FindBy(xpath="//*[@id='xslt_install_prompt']")
		private WebElement XSLTInstallPromptMessageWhenNotInstalled;

		@FindBy(xpath="//*[@id='insatall_permission']/input")
		private WebElement InstallNowButton;
		public void VerifySureFoxInstallationWindow()
		{
			String ActualValue=SureFoxSettingsHeader.getText();
			System.out.println(ActualValue);
			String ExpectedValue="SureFox Settings -"+Config.DeviceName+"";
			String PassStatement="PASS >> SureFox Settings Header is displayed successfully when clicked on SureFox Settings Job as "+ActualValue;
			String FailStatement="FAIL >> SureFox Settings Header is not displayed when clicked on SureFox Settings Job as "+ActualValue;
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

			String ActualWelcomeMessage = XSLTInstallPromptMessageWhenNotInstalled.getText();
			String ExpectedWelcomeMessage = "Welcome to SureFox Setup.";
			String pass = "PASS >> Welcome message of XSLT installation is correct";
			String fail = "FAIL >> Welcome message of XSLT installation is NOT correct";
			ALib.AssertEqualsMethod(ExpectedWelcomeMessage, ActualWelcomeMessage, pass, fail);

			boolean a = InstallNowButton.isDisplayed();
			String pass1 = "PASS >> Install Now button is displayed";
			String fail1 = "FAIL >> Install Now button is Not Displayed";
			ALib.AssertTrueMethod(a, pass1, fail1);	
		}

		public void ClickOnDownloadedAppsTab() throws Throwable
		{
			DownloadAppsTabSection.click();
			waitForidPresent("uninstallAppList");
			sleep(10);
			Reporter.log("Clicked on Downloaded Apps Tab",true);
		}
		public void ClickOnPermission_SystemApps(String AppName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='systemAppListTable']/tbody/tr/td/p[text()='"+AppName+"']")).click();
			sleep(5);
			ClickOnPermission();
		}

		public void ClickOnPermission_AllApps(String AppName) throws Throwable
		{
			Initialization.driver.findElement(By.xpath("//table[@id='deviceAppListTable']/tbody/tr/td/p[text()='"+AppName+"']")).click();
			sleep(5);
			ClickOnPermission();
		}

		public void VerifyOfPermissionList()
		{
			String ActualValue=AppPermissionHeader.getText();
			String ExpectedValue="Application Permissions";
			String PassStatement="PASS >> 'Application Permissions' Header is displayed successfully when clicked on Permissions Button Of Dynamic Apps";
			String FailStatement="FAIL >> 'Application Permissions' Header is not displayed when clicked on Permissions Button Of Dynamic Apps";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			for(int i=0;i<AppPermissionList.size();i++)
			{
				Reporter.log(AppPermissionList.get(i).getText(),true);
			}
			Reporter.log("PASS >> Application Permission is listed successfully",true);
			boolean value=AppPermissionOKButton.isDisplayed();
			PassStatement = "PASS >> 'Ok' button is displayed successfully when clicked on Permissions Button Of Dynamic Apps";
			FailStatement = "Fail >> 'Ok' button is not displayed even when clicked on Permissions Button Of Dynamic Apps";	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnAppsPermissionOkButton() throws Throwable
		{
			AppPermissionOKButton.click();
			waitForidPresent("permissionsAppList");
			sleep(8);
			Reporter.log("Clicked on Ok button of Application Permissions popup of Dynamic Apps", true);
		}

		public void ClickOnAppsPermissionCloseButton() throws Throwable
		{
			AppPermissionCloseButton.click();
			waitForidPresent("permissionsAppList");
			sleep(8);
			Reporter.log("Clicked on close button of Application Permissions popup of Dynamic Apps", true);
		}

		public void ClickOnAppsRefresh() throws Throwable
		{
			AppsRefreshBtn.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
			{}
			waitForidPresent("uninstallAppList");
			sleep(8);
			Reporter.log("Clicked on Refresh Button of Dynamic Apps", true);
		}








		//ResetDevice

		@FindBy(id="resetDevice")
		private WebElement Reset;

		@FindBy(xpath="//div[@id='resetPinDialogContent']/div/h4")
		private WebElement ResetHeader;

		@FindBy(id="resetPassword")
		private WebElement ResetPasswordtextbox;

		@FindBy(xpath="//div[@id='resetPinDialogContent']/div/button[text()='Cancel']")
		private WebElement ResetCancelButton;

		@FindBy(xpath="//div[@id='resetPinDialogContent']/div/button[text()='OK']")
		private WebElement ResetOkButton;

		@FindBy(xpath="//div[@id='resetPinDialogContent']/div/div[@class='close']")
		private WebElement ResetCloseButton;

		@FindBy(xpath="//span[text()='Password reset initiated.']")
		private WebElement ResetPasswordConfirmationMessage;

//		@FindBy(xpath="//p[contains(text(),'has initiated Dynamic job CHANGE_PIN on device("+Config.DeviceName+").')]")
//		private List<WebElement> ResetPasswordActivityLogs;
		
		@FindBy(xpath="//p[contains(text(),'has initiated a dynamic job known as \"CHANGE_PIN\" on device named "+"\""+Config.DeviceName+"\".')]")
		private List<WebElement> ResetPasswordActivityLogs;


		public void ClickOnResetDevice() throws Throwable
		{
			Reset.click();
			waitForidPresent("resetPassword");
			sleep(2);
			Reporter.log("Clicked on Reset Dynamic Job, Change Password popup is opened",true);	
		}

		public void VerifyOfReset()
		{
			String ActualValue=ResetHeader.getText();
			String ExpectedValue="Change Password";
			String PassStatement="PASS >> "+ActualValue+" Header is displayed successfully when clicked on Reset Dynamic Job";
			String FailStatement="FAIL >> "+ActualValue+" Header is not displayed even when clicked on Reset Dynamic Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=ResetCancelButton.isDisplayed();
			PassStatement="PASS >> 'Cancel' button is displayed successfully when clicked on Reset Dynamic Job";
			FailStatement="FAIL >> 'Cancel' button is not displayed even when clicked on Reset Dynamic Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=ResetOkButton.isDisplayed();
			PassStatement="PASS >> 'Ok' button is displayed successfully when clicked on Reset Dynamic Job";
			FailStatement="FAIL >> 'Ok' button is not displayed even when clicked on Reset Dynamic Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=ResetPasswordtextbox.isDisplayed();
			PassStatement="PASS >> 'Password' textbox is displayed successfully when clicked on Reset Dynamic Job";
			FailStatement="FAIL >> 'Password' textbox is not displayed even when clicked on Reset Dynamic Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnResetClose() throws Throwable
		{
			ResetCloseButton.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(2);
			Reporter.log("Clicked on Close button of Reset Dynamic Job",true);	
		}

		public void ClickOnResetCancel() throws Throwable {
			ResetCancelButton.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(2);
			Reporter.log("Clicked on Cancel button of Reset Dynamic Job", true);
		}

		public void EnterResetPassword(String ResetPwd) {
			ResetPasswordtextbox.sendKeys(ResetPwd);
		}

		public void ClickOnResetOk() throws Throwable
		{
			ResetOkButton.click();
			Reporter.log("Clicked on Ok button of Reset Dynamic Job",true);	
		}

		public void ConfirmationMessageResetDeviceVerify() throws InterruptedException{
			String PassStatement="PASS >> 'Password reset initiated.' Message is displayed successfully when clicked on yes button of Dynamic Reset Job";
			String FailStatement="FAIL >> 'Password reset initiated.' Message is not displayed even when clicked on yes button of Dynamic Reset Job";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ResetPasswordConfirmationMessage, true, PassStatement, FailStatement,5);
		}

/*		public void ConfirmationMessageResetDeviceVerify_False() throws InterruptedException{
			String PassStatement="PASS >> 'Password reset successfully.' Message is not displayed when clicked on No button of Dynamic Reset Job";
			String FailStatement="FAIL >> 'Password reset successfully.' Message is displayed even when clicked on No button of Dynamic Reset Job";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(ResetPasswordConfirmationMessage, true, PassStatement, FailStatement,5);
		}
		
		
*/	
		public void VerifyNoConfirmationMessageResetDevice() 
		{
		try 
		{
			if(ResetPasswordConfirmationMessage.isDisplayed())
			{
				ALib.AssertFailMethod("FAIL >>'Password reset initiated.' Message is displayed even when clicked on No button of Dynamic Lock Job");
			}
		}
			
		catch(Exception e) {
			Reporter.log("PASS >> 'Password reset initiated.' Message is not displayed when clicked on No button of Dynamic Lock Job", true);
		}
}


		public void ConfirmationMessageResetDeviceVerify_False() throws InterruptedException{
			String PassStatement="PASS >> 'Password reset initiated.' Message is not displayed when clicked on No button of Dynamic Reset Job";
			String FailStatement="FAIL >> 'Password reset initiated.' Message is displayed even when clicked on No button of Dynamic Reset Job";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(ResetPasswordConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void VerifyOfResetActivityLogs()
		{
			boolean value=ResetPasswordActivityLogs.get(0).isDisplayed();
			String ActualValue=ResetPasswordActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Reset is displayed successfully when clicked on Dynamic Reset Job as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Reset is not displayed even when clicked on Reset"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		////Refresh

		@FindBy(id="refreshButton")
		private WebElement RefreshButton;

		@FindBy(xpath="//div[@id='dis-card']/div[text()='Last Device Time']/following-sibling::div[1]")
		private WebElement LastDeviceTime;

		@FindBy(xpath="//p[contains(text(),'Device("+Config.DeviceName+") updated its info.')]")
		private List<WebElement> RefreshPasswordActivityLogs;

		public void ClickOnRefreshButton() throws Throwable
		{
			RefreshButton.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
			{}
			waitForidPresent("deleteDeviceBtn");
			sleep(2);
			Reporter.log("Clicked on Refresh Button",true);
		}

		public String VerifyLastDeviceTime()
		{
			String value=LastDeviceTime.getText();
			return value;
		}

		public void VerifyOfRefresh() throws Throwable
		{
			String initialvalue=VerifyLastDeviceTime();
			System.out.println(initialvalue);
			ClickOnRefreshButton();
			String finalvalue=VerifyLastDeviceTime();
			System.out.println(finalvalue);
			boolean number=finalvalue.equals(initialvalue);
			String PassStatement="PASS >> Last Device Time is updated successfully when clicked on refresh button";
			String FailStatement="FAIL >> Last Device Time is not updated even when clicked on refresh button"; 	
			ALib.AssertFalseMethod(number, PassStatement, FailStatement);
		}

		public void VerifyOfRefreshActivityLogs()
		{
			boolean value=RefreshPasswordActivityLogs.get(0).isDisplayed();
			String ActualValue=RefreshPasswordActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Refresh is displayed successfully when clicked on Refresh button as "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Refresh is not displayed even when clicked on Refresh button"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}


		public void ClickOnRemote_UM()
		{
			RemoteBtn.click();
		}


		///SureLock Settings

		@FindBy(id="surelockSettings")
		private WebElement SurelockSettingsBtn;

		@FindBy(id="AllowedApplications")
		private WebElement AllowedApplicationsBtn;

		@FindBy(xpath="//li[@id='AllowedApplications']/div/div/p[@class='tit_line']")
		private WebElement AllowedApplicationsHeading;

		@FindBy(xpath="//li[@id='AllowedApplications']/div/div/p[@class='desc_line']")
		private WebElement AllowedApplicationsSummary;

		@FindBy(xpath="//li[@id='SureLockSettings']/div/div/p[@class='tit_line']")
		private WebElement SureLockSettingsHeading;

		@FindBy(xpath="//li[@id='SureLockSettings']/div/div/p[@class='desc_line']")
		private WebElement SureLockSettingsSummary;

		@FindBy(xpath="//li[@id='AllowedWidgets']/div/div/p[@class='tit_line']")
		private WebElement AllowedWidgetsHeading;

		@FindBy(xpath="//li[@id='AllowedWidgets']/div/div/p[@class='desc_line']")
		private WebElement AllowedWidgetsSummary;

		@FindBy(xpath="//li[@id='ManageShortcuts']/div/div/p[@class='tit_line']")
		private WebElement ManageShortcutsHeading;

		@FindBy(xpath="//li[@id='ManageShortcuts']/div/div/p[@class='desc_line']")
		private WebElement ManageShortcutsSummary;

		@FindBy(xpath="//li[@id='PhoneSettings']/div/div/p[@class='tit_line']")
		private WebElement PhoneSettingsHeading;

		@FindBy(xpath="//li[@id='PhoneSettings']/div/div/p[@class='desc_line']")
		private WebElement PhoneSettingsSummary;

		@FindBy(xpath="//li[@id='MultiUserProfileSett']/div/div/p[@class='tit_line']")
		private WebElement MultiUserProfileSettHeading;

		@FindBy(xpath="//li[@id='MultiUserProfileSett']/div/div/p[@class='desc_line']")
		private WebElement MultiUserProfileSettSummary;

		@FindBy(xpath="//li[@id='ImportExportSetting']/div/div/p[@class='tit_line']")
		private WebElement ImportExportSettingHeading;

		@FindBy(xpath="//li[@id='ImportExportSetting']/div/div/p[@class='desc_line']")
		private WebElement ImportExportSettingSummary;

		@FindBy(xpath="//li[@id='AboutSureLock']/div/div/p[@class='tit_line']")
		private WebElement AboutSureLockHeading;

		@FindBy(xpath="//li[@id='AboutSureLock']/div/div/p[@class='desc_line']")
		private WebElement AboutSureLockSummary;

		@FindBy(id="add_app")
		private WebElement AddApp;

		@FindBy(xpath="//div[@id='ava_app_list']/footer/div[2]/button[1]")
		private WebElement AddAppDoneBtn;

		@FindBy(id="add_folder")
		private WebElement AddFolder;

		@FindBy(id="newFolderName")
		private WebElement FolderNameTextBox;

//		@FindBy(xpath="//div[@id='new_folder_popup']/div/div/div/button[text()='Ok']")
//		private WebElement AddFolderDoneBtn;

		@FindBy(xpath="//*[@id='new_folder_popup']/div/div/div[3]/button[2][text()='OK']")
		private WebElement AddFolderDoneBtn;

		@FindBy(id="done_btn")
		private WebElement AllowedApplicationsDoneBtn;

		@FindBy(id="ApplyBtn")
		private WebElement ApplyBtn;

		@FindBy(id="SaveAsBtn")
		private WebElement SaveAsJobBtn;

		@FindBy(id="xslt_submodal_title")
		private WebElement SaveAsJobHeader;

		@FindBy(id="job_name")
		private WebElement SaveAsJobJobNameTextBox;

		@FindBy(id="job_passwordBox")
		private WebElement SaveAsJobPasswordTextBox;

		@FindBy(xpath="//h4[@id='xslt_submodal_title']/preceding-sibling::button")
		private WebElement SaveAsJobCloseBtn;

		@FindBy(xpath="//div[@id='saveAsJobModal']/div/div/div/button[text()='Save']")
		private WebElement SaveAsJobSaveBtn;

		@FindBy(xpath = "//span[text()='Job created successfully.']")
		private WebElement SaveAsJobConfirmationMessage;

		@FindBy(xpath = "//div[@id='input_jobname']/span/ul/li")
		private WebElement SaveAsJobErrorMessage;

		@FindBy(id="DownloadXML")
		private WebElement SaveAsFileBtn;

		@FindBy(xpath = "//*[@id='EditXML']")
		private WebElement EditXMLBtn;

		@FindBy(id="ThirdPartySettingsXML")
		private WebElement EditXMLTextArea;

		@FindBy(id="SaveBtn")
		private WebElement EditXMLSaveButton;

		@FindBy(xpath="//div[@id='content3p']/div/h4")
		private WebElement EditXMLHeader;

		@FindBy(xpath="//h4[text()='SureLock Source']/preceding-sibling::button")
		private WebElement EditXMLCloseBtn;

		@FindBy(xpath = "//span[text()='Invalid SureLock Settings.']")
		private WebElement EditXMLErrorMessage;

		@FindBy(id="RefreshSetng")
		private WebElement RefreshSettingsBtn;

		@FindBy(id="RemoveBtn")
		private WebElement RemoveApplicationBtn;

		@FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div/button[text()='Yes']")
		private WebElement RemoveApplicationYesBtn;

		@FindBy(xpath="//div[@id='commonModalLargeXSLT']/div/div/div/h4")
		private WebElement SureLockSettingsHeader;

//		@FindBy(xpath="//*[@id='XSLTStartUpPage_header']/button")               ////*[@id="sureLock_homePage_header"]/button
//       private WebElement SureLockSettingsCloseBtn;

		@FindBy(xpath="//*[@id='sureLock_homePage_header']/button")               //
        private WebElement SureLockSettingsCloseBtn;

		@FindBy(xpath = "//div[@id='deviceConfirmationDialog']/div/div/div/p")
		private WebElement ApplySettingsPopup;

		@FindBy(xpath = "//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='No']")
		private WebElement ApplySettingsNoBtn;

		@FindBy(xpath = "//div[@id='deviceConfirmationDialog']/div/div/div/button[text()='Yes']")
		private WebElement ApplySettingsYesBtn;

		@FindBy(xpath = "//span[text()='Settings applied successfully.']")
		private WebElement ApplySettingsConfirmationMessage;

		public void ClickOnSurelockSettings_UM()
		{
			SurelockSettingsBtn.click();
		}

		public void ClickOnSurelockSettings() throws InterruptedException
		{
			SurelockSettingsBtn.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
			{}
			waitForidPresent("AllowedApplications");
			sleep(2);
			Reporter.log("Clicked on SureLock Settings Job , Navigate successfully to surelock settings popup",true);
		}

		public void ClickOnSurelockSettingsCloseBtn() throws InterruptedException
		{
			SureLockSettingsCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(10);
			Reporter.log("Clicked on close button of SureLock Settings Job",true);
		}

		public void VerifyOfSureLockSettings()
		{
			WebElement[] Header= { AllowedApplicationsHeading,SureLockSettingsHeading,AllowedWidgetsHeading,ManageShortcutsHeading,PhoneSettingsHeading,
					ImportExportSettingHeading,AboutSureLockHeading};//MultiUserProfileSettHeading,
			String[] Text1={"Allowed Applications","SureLock Settings","Allowed Widgets","Manage Shortcuts","Phone Settings",
					"Import / Export Settings","About SureLock"};//"Multi-User Profile Settings",
			WebElement[] Summary= { AllowedApplicationsSummary,SureLockSettingsSummary,AllowedWidgetsSummary,ManageShortcutsSummary,PhoneSettingsSummary,
					ImportExportSettingSummary,AboutSureLockSummary};//MultiUserProfileSettSummary,
			String[] Text2={"Select the applications to be allowed in Lockdown Mode","Configure SureLock","Select the widgets to be allowed in Lockdown Mode","Add/delete shortcuts on home screen","Configure Phone Settings",
					"Backup and share settings across devices","Licensed version"};//"Configure Multiple Profiles and Users",
			for(int i=0;i<Header.length;i++)
			{
				String ActualValue=Header[i].getText();
				String ExpectedValue=Text1[i];
				String PassStatement="PASS >> "+Text1[i]+" Header is displayed successfully when clicked on Surelock Settings Job";
				String FailStatement="PASS >> "+Text1[i]+" Header is not displayed even when clicked on Surelock Settings Job";
				ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
				ActualValue=Summary[i].getText();
				ExpectedValue=Text2[i];
				PassStatement="PASS >> "+Text2[i]+" Summary is displayed successfully when clicked on Surelock Settings Job";
				FailStatement="PASS >> "+Text2[i]+" Summary is not displayed even when clicked on Surelock Settings Job";
				ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			}
			String ActualValue=SureLockSettingsHeader.getText();
			String ExpectedValue="SureLock Settings ("+Config.DeviceName+")";
			String PassStatement="PASS >> SureLock Settings Header is displayed successfully when clicked on Surelock Settings Job as "+ActualValue;
			String FailStatement="PASS >> SureLock Settings Header is not displayed even when clicked on Surelock Settings Job as "+ActualValue;
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			WebElement[] SLButton={ApplyBtn,SaveAsJobBtn,SaveAsFileBtn,EditXMLBtn,RefreshSettingsBtn};
			String[] Text3={"Apply Button","Save As Job Button","Save As File Button","EditXML Button","Refresh Settings Button"};
			for(int i=0;i<SLButton.length;i++)
			{
				ActualValue=SLButton[i].getText();
				ExpectedValue=Text3[i];
				PassStatement="PASS >> "+Text3[i]+" is displayed successfully when clicked on Surelock Settings Job";
				FailStatement="PASS >> "+Text3[i]+" is not displayed even when clicked on Surelock Settings Job";
			}

		}

		public void AllowedApplication(String ApplicationName) throws Throwable
		{
			ClickOnAllowedApplication();
			AddApp.click();
			waitForXpathPresent("//span[contains(@class,'search_btn')]");
			sleep(2);
			Reporter.log("Clicked on Add app of Allowed Applications");
			WebElement element = Initialization.driver.findElement(By.xpath("//div[p[text()='"+ApplicationName+"']]/following-sibling::div/span/input"));
			((JavascriptExecutor) Initialization.driver).executeScript(
					"arguments[0].scrollIntoView();", element);
			element.click();
			AddAppDoneBtn.click();
			waitForidPresent("add_app");
			AddFolder.click();
			FolderNameTextBox.sendKeys("Test");
			AddFolderDoneBtn.click();
			AllowedApplicationsDoneBtn.click();
		}

		public void ClickOnAllowedApplication() throws Throwable
		{
			AllowedApplicationsBtn.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
			{}
			waitForidPresent("add_app");
			sleep(2);
			Reporter.log("Clicked on Allowed Application of SureLock Settings Job");
		}

		public void ClickOnApplyButton() throws Throwable
		{
			ApplyBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div/p");
			sleep(4);
			Reporter.log("Clicked on Apply button of SureLock Settings Job",true);
		}

		public void VerifyOfApplySettingsPopup()
		{
			String ActualValue=ApplySettingsPopup.getText();
			String ExpectedValue="Would you like to apply the SureLock changes you have indicated?";                             //"Would you like to apply the changes?";
			String PassStatement="PASS >> "+ActualValue+" Message is displayed successfully when clicked on Apply Button of SureLock Settings Job";
			String FailStatement="FAIL >> "+ActualValue+" Message is not displayed even when clicked on Apply Button of SureLock Settings Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=ApplySettingsNoBtn.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on Apply Button of SureLock Settings Job";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on Apply Button of SureLock Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=ApplySettingsYesBtn.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Apply Button of SureLock Settings Job";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Apply Button of SureLock Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnApplyButtonNoBtn() throws InterruptedException
		{
			ApplySettingsNoBtn.click();
			waitForidPresent("AllowedApplications");
			sleep(4);
			Reporter.log("Clicked on No button of Apply Settings popup",true);
		}

		public void ClickOnApplyButtonYesBtn() throws InterruptedException
        {
                ApplySettingsYesBtn.click();
                waitForXpathPresent("//span[text()='Settings applied successfully.']");
                Reporter.log("Clicked on Yes button of Apply Settings popup",true);
                sleep(2);
        }

		public void ConfirmationMessageApplySettingsVerify() throws InterruptedException{
			String PassStatement="PASS >> 'Settings applied successfully.' Message is displayed successfully when clicked on yes button of Apply Settings";
			String FailStatement="FAIL >> 'Settings applied successfully.' Message is not displayed even when clicked on yes button of Apply Settings";
			Initialization.commonmethdpage.ConfirmationMessageVerify(ApplySettingsConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		@FindBy(xpath="//*[@id='sureFox_header_id']/button")
		private WebElement SurefoxSettingHomePageCloseButton;
		public void ClickOnSureFoxHomeSettingsCloseButton() throws InterruptedException
		{
			SurefoxSettingHomePageCloseButton.click();
			sleep(4);
		}
		public void ConfirmationMessageApplySettingsVerify_False() throws InterruptedException{
			String PassStatement="PASS >> 'Settings applied successfully.' Message is not displayed when clicked on No button of Apply Settings";
			String FailStatement="FAIL >> 'Settings applied successfully.' Message is displayed even when clicked on No button of Apply Settings";
			Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(ApplySettingsConfirmationMessage, false, PassStatement, FailStatement,5);
		}

		public void ClickOnEditXMLButton() throws Throwable
		{
			EditXMLBtn.click();
			waitForidPresent("ThirdPartySettingsXML");
			sleep(4);
			Reporter.log("Clicked on EditXML button of SureLock Settings Job",true);
		}

		public void ClearEditXMLTextArea()
		{
			EditXMLTextArea.clear();
		}

		public void EnterEditXMLTextArea(String FilePath) throws Throwable
		{
			Initialization.commonmethdpage.ReadfromFile(FilePath, EditXMLTextArea);
		}

		public void VerifyOfEditXML()
		{
			String ActualValue=EditXMLHeader.getText();
			String ExpectedValue="SureLock Source";
			String PassStatement="PASS >> "+ActualValue+" Header is displayed successfully when clicked on Edit XML Button of SureLock Settings Job";
			String FailStatement="FAIL >> "+ActualValue+" Header is not displayed even when clicked on Edit XML Button of SureLock Settings Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=EditXMLSaveButton.isDisplayed();
			PassStatement="PASS >> 'Save Source' button is displayed successfully when clicked on Edit XML Button of SureLock Settings Job";
			FailStatement="FAIL >> 'Save Source' button is not displayed even when clicked on Edit XML Button of SureLock Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=EditXMLTextArea.isDisplayed();
			PassStatement="PASS >> XML TextArea is displayed successfully when clicked on Edit XML Button of SureLock Settings Job";
			FailStatement="FAIL >> XML TextArea button is not displayed even when clicked on Edit XML Button of SureLock Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnEditXMLSaveBtn() throws Throwable
		{
			EditXMLSaveButton.click();
		}

		public void EditXMLSaveBtn_Error() throws Throwable
		{
			String PassStatement="PASS >> 'Invalid SureLock Settings.' Message is displayed successfully when XML File is wrong and clicked on Save Source of Edit XML";
			String FailStatement="FAIL >> 'Invalid SureLock Settings.' Message is displayed even when XML File is wrong and clicked on Save Source of Edit XML";
			Initialization.commonmethdpage.ConfirmationMessageVerify(EditXMLErrorMessage, true, PassStatement, FailStatement,5);
		}

		public void ClickOnEditXMLCloseBtn() throws InterruptedException
		{
			EditXMLCloseBtn.click();
			waitForidPresent("AllowedApplications");
			sleep(1);
			Reporter.log("Clicked on close button of EditXML popup",true);
		}

		public void ClickOnSaveAsJobButton() throws Throwable
		{
			SaveAsJobBtn.click();
			waitForidPresent("job_name");
			sleep(2);
			Reporter.log("Clicked on Save As Job button of SureLock Settings Job, Navigates successfully to Job Details popup",true);
		}

		public void VerifyOfJobDetailsPopup()
		{
			String ActualValue=SaveAsJobHeader.getText();
			String ExpectedValue="Job Details";
			String PassStatement="PASS >> "+ActualValue+" Header is displayed successfully when clicked on Save As Job Button of SureLock Settings Job";
			String FailStatement="FAIL >> "+ActualValue+" Header is not displayed even when clicked on Save As Job Button of SureLock Settings Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=SaveAsJobJobNameTextBox.isDisplayed();
			PassStatement="PASS >> 'Job Name' textbox is displayed successfully when clicked on Save As Job Button of SureLock Settings Job";
			FailStatement="FAIL >> 'Save Source' textbox is not displayed even when clicked on Save As Job Button of SureLock Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			/*value=SaveAsJobPasswordTextBox.isDisplayed();
			PassStatement="PASS >> 'Password' textbox is displayed successfully when clicked on Save As Job Button of SureLock Settings Job";
			FailStatement="FAIL >> 'Password' textbox is not displayed even when clicked on Save As Job Button of SureLock Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);*/
			value=SaveAsJobSaveBtn.isDisplayed();
			PassStatement="PASS >> 'Save' button is displayed successfully when clicked on Save As Job Button of SureLock Settings Job";
			FailStatement="FAIL >> 'Save' button is not displayed even when clicked on Save As Job Button of SureLock Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnSaveButtonOfJobDetails() throws Throwable
		{
			SaveAsJobSaveBtn.click();
			Reporter.log("Clicked on Save button of Job details popup",true);
		}

		public void SaveAsJobErrorMessage()
		{
			boolean value1=SaveAsJobErrorMessage.isDisplayed();
			String ActualValue=SaveAsJobErrorMessage.getText();
			String ExpectedValue="Please enter a name for the job.";                                  //"Enter Job Name";
			boolean value2=ExpectedValue.equals(ActualValue);
			boolean value=value1 && value2;
			String PassStatement="PASS >> "+ActualValue+" is displayed successfully when clicked on Save Button of Job Details with Job name empty";
			String FailStatement="FAIL >> "+ActualValue+" is not displayed even when clicked on Save Button of Job Details with Job name empty"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}

		public void ClickOnJobDetailsCloseBtn() throws InterruptedException
		{
			SaveAsJobCloseBtn.click();
			waitForidPresent("AllowedApplications");
			sleep(1);
			Reporter.log("Clicked on Close button of Job Details popup",true);
		}

		public void EnterJobName(String JobName)
		{
			SaveAsJobJobNameTextBox.sendKeys(JobName);
		}

		public void EnterPassword()
		{
			SaveAsJobPasswordTextBox.sendKeys("0000");
		}

		public void VerifyOfSureLockSaveAsJob_Cancel()
		{
			boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(Config.SLSaveAsJob);
			String PassStatement = "PASS >> Job is not created when Clicked on close button of Job Details of SureLock Settings";
			String FailStatement = "Fail >> Job is created even when Clicked on close button of Job Details of SureLock Settings";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSureLockSaveAsJob_Save()
		{
			boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(Config.SLSaveAsJob);
			String PassStatement = "PASS >> Job is created successfully when Clicked on Save button of Job Details of SureLock Settings";
			String FailStatement = "Fail >> Job is not created even when Clicked on Save button of Job Details of SureLock Settings";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void SaveAsJob_ConfirmationMessage() throws Throwable
		{
			String PassStatement="PASS >> 'Job created successfully.' Message is displayed successfully when clicked on Save Button of Save As Job";
			String FailStatement="FAIL >> 'Job created successfully.' Message is displayed even when clicked on Save Button of Save As Job";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SaveAsJobConfirmationMessage, true, PassStatement, FailStatement,5);
		}

				public void RemoveApplication(String ApplicationName) throws Throwable
		{
			ClickOnAllowedApplication();
			sleep(2);
			Initialization.driver.findElement(By.xpath("//p[text()='"+ApplicationName+"']")).click();
			waitForidClickable("RemoveBtn");
			sleep(3);
			RemoveApplicationBtn.click();
			waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/button[text()='Yes']");
			sleep(3);
			RemoveApplicationYesBtn.click();
			waitForidPresent("add_app");
			sleep(2);
			boolean value=true;
			try{
				Initialization.driver.findElement(By.xpath("//p[text()='"+ApplicationName+"']")).isDisplayed();
			}catch(Exception e)
			{
				value=false;
			}
			String PassStatement="PASS >> Application is removed successfuly from Allowed Application";
			String FailStatement="Fail >> Application is not removed from Allowed Application";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			AllowedApplicationsDoneBtn.click();
		}
				
				public void ClickOnSaveAsFileButton() throws Throwable
		        {
		                SaveAsFileBtn.click();
		                sleep(10);
		        
		        }

		public void CheckApplicationAfterRefresh(String ApplicationName) throws Throwable {
			ClickOnAllowedApplication();
			boolean value = true;
			try {
				Initialization.driver.findElement(By.xpath("//p[text()='"+ApplicationName+"']")).isDisplayed();
			} catch (Exception e) {
				value = false;
			}
			String PassStatement = "PASS >> Application is present in Allowed Application after refresh of SureLock Settings Job";
			String FailStatement = "Fail >> Application is not present in Allowed Application even after refresh of SureLock Settings Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}




		// SureFox Settings
		@FindBy(id = "surefoxSettings")
		private WebElement SurefoxSettingsBtn;

		@FindBy(xpath = "//*[@id='XSLTStartUpPage_header']/button")
        private WebElement SureFoxSettingsCloseBtn;

		@FindBy(xpath="//li[@id='surefoxAllowedWebsite']/div/div/p[@class='tit_line']")
		private WebElement SurefoxAllowedWebsiteHeading;

		@FindBy(xpath="//li[@id='surefoxAllowedWebsite']/div/div/p[@class='desc_line']")
		private WebElement SurefoxAllowedWebsiteSummary;

		@FindBy(xpath="//li[@id='surefoxBlockedWebsite']/div/div/p[@class='tit_line']")
		private WebElement SurefoxBlockedWebsiteHeading;

		@FindBy(xpath="//li[@id='surefoxBlockedWebsite']/div/div/p[@class='desc_line']")
		private WebElement SurefoxBlockedWebsiteSummary;

		@FindBy(xpath="//li[@id='manageCategories']/div/div/p[@class='tit_line']")
		private WebElement ManageCategoriesHeading;

		@FindBy(xpath="//li[@id='manageCategories']/div/div/p[@class='desc_line']")
		private WebElement ManageCategoriesSummary;

		@FindBy(xpath="//li[@id='browserPreferences']/div/div/p[@class='tit_line']")
		private WebElement BrowserPreferencesHeading;

		@FindBy(xpath="//li[@id='browserPreferences']/div/div/p[@class='desc_line']")
		private WebElement BrowserPreferencesSummary;

		@FindBy(xpath="//li[@id='surefoxProSettings']/div/div/p[@class='tit_line']")
		private WebElement SurefoxProSettingsHeading;

		@FindBy(xpath="//li[@id='surefoxProSettings']/div/div/p[@class='desc_line']")
		private WebElement SurefoxProSettingsSummary;

		@FindBy(xpath="//li[@id='displaySettings']/div/div/p[@class='tit_line']")
		private WebElement DisplaySettingsHeading;

		@FindBy(xpath="//li[@id='displaySettings']/div/div/p[@class='desc_line']")
		private WebElement DisplaySettingsSummary;

		@FindBy(xpath="//li[@id='sureFoxImportExportSettings']/div/div/p[@class='tit_line']")
		private WebElement ImportExportSettingSureFoxHeading;

		@FindBy(xpath="//li[@id='sureFoxImportExportSettings']/div/div/p[@class='desc_line']")
		private WebElement ImportExportSettingSureFoxSummary;

		@FindBy(xpath="//li[@id='aboutSureFox']/div/div/p[@class='tit_line']")
		private WebElement AboutSureFoxHeading;

		@FindBy(xpath="//li[@id='aboutSureFox']/div/div/p[@class='desc_line']")
		private WebElement AboutSureFoxSummary;

		@FindBy(xpath = "//span[text()='Invalid SureFox Settings.']")
		private WebElement EditXMLErrorMessageSureFox;

		@FindBy(id = "surefoxAllowedWebsite")
		private WebElement SureFoxAllowedWebsite;

		@FindBy(xpath = "//button[text()='Add URL']")
		private WebElement AddURL;

		@FindBy(xpath = "//div[@id='URL']/input")
		private WebElement EnterURL;

		@FindBy(xpath = "//div[@id='Name']/input")
		private WebElement EnterName;

		@FindBy(xpath = "//button[text()='Done']")
		private WebElement AddURLDone;

		@FindBy(id = "doneButton")
		private WebElement AllowedWebsiteDoneBtn;

		@FindBy(xpath = "//div[@id='site_details_pg']/footer/div/button[text()='Delete']")
		private WebElement RemoveWebsite;

		@FindBy(xpath="//h4[text()='SureFox Source']/preceding-sibling::button")
		private WebElement EditXMLCloseBtn_SureFox;

		public void ClickOnSurefoxSettings_UM() {
			SurefoxSettingsBtn.click();
		}

		public void ClickOnSurefoxSettings() throws InterruptedException {
            SurefoxSettingsBtn.click();
            while (Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed()) {
                    sleep(6);
            }
            
            Reporter.log("Clicked on SureFox Settings Job , Navigate successfully to surefox settings popup", true);
    }

		public void ClickOnSurefoxSettingsCloseBtn() throws InterruptedException {
            SureFoxSettingsCloseBtn.click();
            sleep(5);
            Reporter.log("Clicked on close button of SureLock Settings Job",true);
    }

		public void VerifyOfSureFoxSettings()
		{
			WebElement[] Header= { SurefoxAllowedWebsiteHeading,SurefoxBlockedWebsiteHeading,ManageCategoriesHeading,BrowserPreferencesHeading,SurefoxProSettingsHeading,
					DisplaySettingsHeading,ImportExportSettingSureFoxHeading,AboutSureFoxHeading};
			String[] Text1={"Allowed Website","Blocked Website","Manage Categories","Browser Preferences","SureFox Pro Settings",
					"Display Settings","Import / Export Settings","About SureFox"};
			WebElement[] Summary= { SurefoxAllowedWebsiteSummary,SurefoxBlockedWebsiteSummary,ManageCategoriesSummary,BrowserPreferencesSummary,SurefoxProSettingsSummary,
					DisplaySettingsSummary,ImportExportSettingSureFoxSummary,AboutSureFoxSummary};
			String[] Text2={"Add / Edit / Remove Whitelisted URLs","Add / Remove Blocked URLs","Add / Edit / Remove Categories","Security Settings and Advanced Browser Settings","SureFox Pro Version Settings",
					"Manage the look and feel of SureFox","Backup and share settings across devices","Licensed version"};
			for(int i=0;i<Header.length;i++)
			{
				String ActualValue=Header[i].getText();
				String ExpectedValue=Text1[i];
				String PassStatement="PASS >> "+Text1[i]+" Header is displayed successfully when clicked on Surelock Settings Job";
				String FailStatement="PASS >> "+Text1[i]+" Header is not displayed even when clicked on Surelock Settings Job";
				ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
				ActualValue=Summary[i].getText();
				ExpectedValue=Text2[i];
				PassStatement="PASS >> "+Text2[i]+" Summary is displayed successfully when clicked on SureFox Settings Job";
				FailStatement="PASS >> "+Text2[i]+" Summary is not displayed even when clicked on SureFox Settings Job";
				ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			}
			String ActualValue=SureLockSettingsHeader.getText();
			String ExpectedValue="SureFox Settings ("+Config.DeviceName+")";
			String PassStatement="PASS >> SureFox Settings Header is displayed successfully when clicked on SureFox Settings Job as "+ActualValue;
			String FailStatement="PASS >> SureFox Settings Header is not displayed even when clicked on SureFox Settings Job as "+ActualValue;
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			WebElement[] SLButton={ApplyBtn,SaveAsJobBtn,SaveAsFileBtn,EditXMLBtn,RefreshSettingsBtn};
			String[] Text3={"Apply Button","Save As Job Button","Save As File Button","EditXML Button","Refresh Settings Button"};
			for(int i=0;i<SLButton.length;i++)
			{
				ActualValue=SLButton[i].getText();
				ExpectedValue=Text3[i];
				PassStatement="PASS >> "+Text3[i]+" is displayed successfully when clicked on SureFox Settings Job";
				FailStatement="PASS >> "+Text3[i]+" is not displayed even when clicked on SureFox Settings Job";
			}

		}

		public void ClickOnEditXMLButtonSureFox() throws Throwable
		{
			EditXMLBtn.click();
			waitForidPresent("ThirdPartySettingsXML");
			sleep(4);
			Reporter.log("Clicked on EditXML button of SureFox Settings Job",true);
		}

		@FindBy(xpath="//*[@id='SaveBtn']")
		private WebElement SaveSourceButton;
		public void ClickOnSaveSourceButton() throws InterruptedException
		{
			SaveSourceButton.click();
			sleep(3);
		}
		
		public void VerifyOfEditXMLSureFox()
		{
			String ActualValue=EditXMLHeader.getText();
			String ExpectedValue="SureFox Source";
			String PassStatement="PASS >> "+ActualValue+" Header is displayed successfully when clicked on Edit XML Button of SureFox Settings Job";
			String FailStatement="FAIL >> "+ActualValue+" Header is not displayed even when clicked on Edit XML Button of SureFox Settings Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=EditXMLSaveButton.isDisplayed();
			PassStatement="PASS >> 'Save Source' button is displayed successfully when clicked on Edit XML Button of SureFox Settings Job";
			FailStatement="FAIL >> 'Save Source' button is not displayed even when clicked on Edit XML Button of SureFox Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=EditXMLTextArea.isDisplayed();
			PassStatement="PASS >> XML TextArea is displayed successfully when clicked on Edit XML Button of SureFox Settings Job";
			FailStatement="FAIL >> XML TextArea button is not displayed even when clicked on Edit XML Button of SureFox Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnEditXMLCloseBtnSureFox() throws InterruptedException
		{
			EditXMLCloseBtn_SureFox.click();
			waitForidPresent("surefoxAllowedWebsite");
			sleep(1);
			Reporter.log("Clicked on close button of EditXML popup",true);
		}

		public void EditXMLSaveBtnSureFox_Error() throws Throwable
		{
			String PassStatement="PASS >> 'Invalid SureFox Settings.' Message is displayed successfully when XML File is wrong and clicked on Save Source of Edit XML";
			String FailStatement="FAIL >> 'Invalid SureFox Settings.' Message is displayed even when XML File is wrong and clicked on Save Source of Edit XML";
			Initialization.commonmethdpage.ConfirmationMessageVerify(EditXMLErrorMessageSureFox, true, PassStatement, FailStatement,5);
		}

		public void ClickOnApplyButtonSureFox() throws Throwable
		{
			ApplyBtn.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div/p");
			sleep(4);
			Reporter.log("Clicked on Apply button of SureFox Settings Job",true);
		}

		public void Allowedwebsite(String URL,String WebsiteName) throws Throwable
		{
			ClickOnAllowedWebsite();
			AddURL.click();
			waitForidPresent("AllowSubDomain");
			sleep(2);
			Reporter.log("Clicked on Add URL of Allowed Websites");
			EnterURL.sendKeys(URL);
			EnterName.sendKeys(WebsiteName);
			AddURLDone.click();
			waitForidPresent("doneButton");
			AllowedWebsiteDoneBtn.click();
		}

		public void ClickOnAllowedWebsite() throws Throwable
		{
			SureFoxAllowedWebsite.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
			{}
			waitForidPresent("doneButton");
			sleep(2);
			Reporter.log("Clicked on Allowed Website of SureFox Settings Job");
		}

		public void ClickOnSaveAsJobButton_SureFox() throws Throwable
		{
			SaveAsJobBtn.click();
			waitForidPresent("job_name");
			sleep(2);
			Reporter.log("Clicked on Save As Job button of SureFox Settings Job, Navigates successfully to Job Details popup",true);
		}

		public void VerifyOfJobDetailsPopup_SureFox()
		{
			String ActualValue=SaveAsJobHeader.getText();
			String ExpectedValue="Job Details";
			String PassStatement="PASS >> "+ActualValue+" Header is displayed successfully when clicked on Save As Job Button of SureFox Settings Job";
			String FailStatement="FAIL >> "+ActualValue+" Header is not displayed even when clicked on Save As Job Button of SureFox Settings Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=SaveAsJobJobNameTextBox.isDisplayed();
			PassStatement="PASS >> 'Job Name' textbox is displayed successfully when clicked on Save As Job Button of SureFox Settings Job";
			FailStatement="FAIL >> 'Save Source' textbox is not displayed even when clicked on Save As Job Button of SureFox Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			/*value=SaveAsJobPasswordTextBox.isDisplayed();
			PassStatement="PASS >> 'Password' textbox is displayed successfully when clicked on Save As Job Button of SureFox Settings Job";
			FailStatement="FAIL >> 'Password' textbox is not displayed even when clicked on Save As Job Button of SureFox Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);*/
			value=SaveAsJobSaveBtn.isDisplayed();
			PassStatement="PASS >> 'Save' button is displayed successfully when clicked on Save As Job Button of SureFox Settings Job";
			FailStatement="FAIL >> 'Save' button is not displayed even when clicked on Save As Job Button of SureFox Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnSaveAsFileButton_SureFox() throws Throwable
		{
			String downloadPath = ELib.getDatafromExcel("Sheet1", 19, 1);
			String fileName = "surefox.settings";
			String dd = fileName.replace(".", "  ");
			String[] text = dd.split("  ");
			File dir = new File(downloadPath);
			File[] dir_contents = dir.listFiles();
			int initial = 0;
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

					initial++;
				}
			}

			SaveAsFileBtn.click();
			sleep(10);

			dir = new File(downloadPath);
			dir_contents = dir.listFiles();
			int result=0;
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
					result++;
				}
			}
			boolean value=result>initial;
			String PassStatement="PASS >> SureFox Settings Downloaded successfully";
			String FailStatement="Fail >> SureFox Settings is not Downloaded";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSureFoxSaveAsJob_Cancel()
		{
			boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(Config.SLSaveAsJob);
			String PassStatement = "PASS >> Job is not created when Clicked on close button of Job Details of SureFox Settings";
			String FailStatement = "Fail >> Job is created even when Clicked on close button of Job Details of SureFox Settings";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSureFoxSaveAsJob_Save()
		{
			boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(Config.SFSaveAsJob);
			String PassStatement = "PASS >> Job is created successfully when Clicked on Save button of Job Details of SureFox Settings";
			String FailStatement = "Fail >> Job is not created even when Clicked on Save button of Job Details of SureFox Settings";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfApplySettingsPopup_SureFox()
		{
			String ActualValue=ApplySettingsPopup.getText();
			String ExpectedValue="Would you like to apply the SureFox changes you have indicated?";
			String PassStatement="PASS >> "+ActualValue+" Message is displayed successfully when clicked on Apply Button of SureFox Settings Job";
			String FailStatement="FAIL >> "+ActualValue+" Message is not displayed even when clicked on Apply Button of SureFox Settings Job"; 	
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=ApplySettingsNoBtn.isDisplayed();
			PassStatement="PASS >> 'No' button is displayed successfully when clicked on Apply Button of SureFox Settings Job";
			FailStatement="FAIL >> 'No' button is not displayed even when clicked on Apply Button of SureFox Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=ApplySettingsYesBtn.isDisplayed();
			PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Apply Button of SureFox Settings Job";
			FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Apply Button of SureFox Settings Job"; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnApplyButtonNoBtn_SureFox() throws InterruptedException
		{
			ApplySettingsNoBtn.click();
			waitForidPresent("surefoxAllowedWebsite");
			sleep(4);
			Reporter.log("Clicked on No button of Apply Settings popup",true);
		}

		public void RemoveWebsite(String WebsiteName) throws Throwable
		{
			ClickOnAllowedWebsite();
			Initialization.driver.findElement(By.xpath("//p[text()='"+WebsiteName+"']")).click();
			waitForXpathPresent("//div[@id='site_details_pg']/footer/div/button[text()='Delete']");
			sleep(3);
			RemoveWebsite.click();
			waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/button[text()='Yes']");
			sleep(3);
			RemoveApplicationYesBtn.click();
			waitForXpathPresent("//div[@id='allowed_sites_list']/footer/div/button[text()='Add URL']");
			sleep(2);
			boolean value=true;
			try{
				Initialization.driver.findElement(By.xpath("//p[text()='"+WebsiteName+"']")).isDisplayed();
			}catch(Exception e)
			{
				value=false;
			}
			String PassStatement="PASS >> Website is removed successfuly from Allowed Website";
			String FailStatement="Fail >> Website is not removed from Allowed Website";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			AllowedWebsiteDoneBtn.click();
		}

		public void CheckWebsiteAfterRefresh(String WebsiteName) throws Throwable {
			ClickOnAllowedWebsite();
			boolean value = true;
			try {
				Initialization.driver.findElement(By.xpath("//p[text()='" + WebsiteName + "']")).isDisplayed();
			} catch (Exception e) {
				value = false;
			}
			String PassStatement = "PASS >> Website is present in Allowed Website after refresh of SureFox Settings Job";
			String FailStatement = "Fail >> Website is not present in Allowed Website even after refresh of SureFox Settings Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void ClickOnJobDetailsCloseBtn_SureFox() throws InterruptedException
		{
			SaveAsJobCloseBtn.click();
			waitForidPresent("surefoxAllowedWebsite");
			sleep(1);
			Reporter.log("Clicked on Close button of Job Details popup",true);
		}

		// SureVideo Settings
		@FindBy(id = "surevideoSettings")
		private WebElement SurevideoSettingsBtn;

		@FindBy(xpath = "//*[@id='commonModalLargeXSLT']/div/div/div/div[1]/button")
        private WebElement SureVideoSettingsCloseBtn;

		@FindBy(xpath="//*[@id='commonModalLargeXSLT']/div/div/div/div[1]/h4")
        private WebElement SureVideoSettingsHeader;

		@FindBy(xpath="//*[@id='ThirdPartySettingsXML']")
        private WebElement SureVideoEditXMLArea;

		@FindBy(id="SaveAsBtn")
        private WebElement SureVideoSaveAsJobButton;

		@FindBy(id="ApplyBtn")
        private WebElement SureVideoApplyButton;

		@FindBy(xpath = "//div[@id='commonModalLargeSureVedio']/div/div/div/button[text()='Cancel']")
		private WebElement SureVideoCancelButton;

		@FindBy(id="RefreshSetng")
        private WebElement SureVideoRefreshButton;

		@FindBy(xpath="//div[@id='passwordModal']/div/div/div/h4[@id='xslt_submodal_title']")
		private WebElement SureVideoApplySettingsHeader;

		@FindBy(id="passwordBox")
		private WebElement SureVideoApplySettingsPasswordBox;

		@FindBy(xpath = "//div[@id='passwordModal']/div/div/div/button[text()='Apply']")
		private WebElement SureVideoApplySettingsButton;

		@FindBy(xpath = "//h4[text()='Enter Password']/preceding-sibling::button")
		private WebElement SureVideoApplySettingsCloseButton;

		@FindBy(xpath = "//span[text()='Settings applied successfully.']")
		private WebElement SureVideoApplyConfirmationMessage;

		@FindBy(xpath = "//span[text()='Entered password does not match with  surevideo password.']")
		private WebElement SureVideoApplyErrorMessage;

		@FindBy(xpath = "//div[@id='password_div']/span/ul/li")
		private WebElement SaveAsJobPasswordErrorMessage;

		public void ClickOnSurevideoSettings_UM() {
			SurevideoSettingsBtn.click();
		}

		public void ClickOnSurevideoSettings() throws InterruptedException {
            SurevideoSettingsBtn.click();
            while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
            {}
            sleep(8);
            Reporter.log("Clicked on SureVideo Settings Job , Navigate successfully to surevideo settings popup",true);
    }

		public void ClickOnSurevideoSettingsCloseBtn() throws InterruptedException {
			SureVideoSettingsCloseBtn.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(3);
			Reporter.log("Clicked on close button of Surevideo Settings Job",true);
		}

		public void VerifyOfSureVideoSettings()
        {
                String ActualValue=SureVideoSettingsHeader.getText();
                System.out.println(ActualValue);
                String ExpectedValue="SureVideo Settings ("+Config.DeviceName+")";
                String PassStatement="PASS >> SureVideo Settings Header is displayed successfully when clicked on SureVideo Settings Job as "+ActualValue;
                String FailStatement="FAIL >> SureVideo Settings Header is not displayed when clicked on SureVideo Settings Job as "+ActualValue;
                ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
                
                WebElement[] SVButton={SureVideoSaveAsJobButton,SaveAsFile,EditXMLBtn,SureVideoRefreshButton,SureVideoApplyButton,};
                String[] Text3={"Save As Job Button","Save As File Button","Edit XML","Refresh Button","Apply Button"};
                for(int i=0;i<SVButton.length;i++)
                {
                        ActualValue=SVButton[i].getText();
                        ExpectedValue=Text3[i];
                        PassStatement="PASS >> "+Text3[i]+" is displayed successfully when clicked on SureVideo Settings Job";
                        FailStatement="PASS >> "+Text3[i]+" is not displayed even when clicked on SureVideo Settings Job";
                }
        }
		
		public void ClickOnRefreshBtn() throws InterruptedException
        {
                RefreshSettingsBtn.click();
                sleep(35);
        }
		
		@FindBy(id="DownloadXML")
        private WebElement SaveAsFile;
		
		public void ClearSureVideoEditXMLArea()
		{
			SureVideoEditXMLArea.clear();
		}

		public void ClickOnSureVideoApplyButton() throws Throwable
		{
			SureVideoApplyButton.click();
			waitForidPresent("passwordBox");
			sleep(2);
			Reporter.log("Clicked on Apply Button of Surevideo Settings",true);
		}

		public void VerifyOfSureVideoApplySettingsPopup()
		{
			String ActualValue=SureVideoApplySettingsHeader.getText();
			String ExpectedValue="Enter Password";
			String PassStatement="PASS >> Apply Settings Header is displayed successfully when clicked on Apply button of SureVideo Settings Job as "+ActualValue;
			String FailStatement="PASS >> Apply Settings Header is not displayed even when clicked on Apply button of SureVideo Settings Job as "+ActualValue;
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=SureVideoApplySettingsPasswordBox.isDisplayed();
			PassStatement="PASS >> Password TextBox is displayed successfully when clicked on SureVideo Settings Job as "+ActualValue;
			FailStatement="PASS >> Password TextBox is not displayed even when clicked on SureVideo Settings Job as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=SureVideoApplySettingsButton.isDisplayed();
			PassStatement="PASS >> Apply Button is displayed successfully when clicked on SureVideo Settings Job as "+ActualValue;
			FailStatement="PASS >> Apply Button is not displayed even when clicked on SureVideo Settings Job as "+ActualValue;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}

		public void ClickOnSureVideoApplySettingsCloseButton() throws Throwable
		{
			SureVideoApplySettingsCloseButton.click();
			waitForidPresent("surevedioxsltxmlContent");
			sleep(2);
			Reporter.log("Clicked on Apply Button of Surevideo Settings",true);
		}

		public void EnterPasswordSureVideo()
		{
			SureVideoApplySettingsPasswordBox.sendKeys("0000");
		}

		public void ClickOnSureVideoApplySettingsButton() throws Throwable
		{
			SureVideoApplySettingsButton.click();
		}

		public void SureVideoApplyConfirmationMessage() throws Throwable
		{
			String PassStatement="PASS >> 'Settings applied successfully.' Message is displayed successfully when clicked on Apply Button of SureVideo Settings";
			String FailStatement="FAIL >> 'Settings applied successfully.' Message is displayed even when clicked on Apply Button of SureVideo Settings";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SureVideoApplyConfirmationMessage, true, PassStatement, FailStatement,5);
		}

		public void SureVideoApplyErrorMessage() throws Throwable
		{
			String PassStatement="PASS >> 'Entered password does not match with  surevideo password.' Message is displayed successfully when clicked on Apply Button of SureVideo Settings";
			String FailStatement="FAIL >> 'Entered password does not match with  surevideo password.' Message is displayed even when clicked on Apply Button of SureVideo Settings";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SureVideoApplyErrorMessage, true, PassStatement, FailStatement,5);
		}

		public void EnterSureVideoEditXMLTextArea(String FilePath) throws Throwable
		{
			Initialization.commonmethdpage.ReadfromFile(FilePath, SureVideoEditXMLArea);
		}

		public void ClickOnSureVideoSaveAsJob() throws Throwable
		{
			SureVideoSaveAsJobButton.click();
			waitForidPresent("job_name");
			sleep(2);
			Reporter.log("Clicked on Save As Job Button of Surevideo Settings",true);
		}

		
		public void ClickOnJobDetailsCloseBtn_SureVideo() throws InterruptedException
		{
			SaveAsJobCloseBtn.click();
			waitForLogXpathPresent("(//*[@id='surevideoSettings']/div/div/p[text()='SureVideo Settings'])[1]");
			sleep(4);
			Reporter.log("Clicked on Close button of Job Details popup",true);
		}

		public void SaveAsJobErrorMessageSureVideo() throws InterruptedException
        {
                boolean value1=SaveAsJobErrorMessage.isDisplayed();
                String ActualValue=SaveAsJobErrorMessage.getText();
                String ExpectedValue= "Please enter a name for the job.";         //"Enter Job Name";
                boolean value2=ExpectedValue.equals(ActualValue);
                boolean value=value1 && value2;
                String PassStatement="PASS >> "+ActualValue+" is displayed successfully when clicked on Save Button of Job Details with Job name empty";
                String FailStatement="FAIL >> "+ActualValue+" is not displayed even when clicked on Save Button of Job Details with Job name empty";         
                ALib.AssertTrueMethod(value, PassStatement, FailStatement);
                sleep(4);
                
        }
		
		public void VerifyOfSureVideoSaveAsJob_Cancel()
		{
			boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(Config.SVSaveAsJob);
			String PassStatement = "PASS >> Job is not created when Clicked on close button of Job Details of SureVideo Settings";
			String FailStatement = "Fail >> Job is created even when Clicked on close button of Job Details of SureVideo Settings";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}

		public void VerifyOfSureVideoSaveAsJob_Save()
		{
			boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(Config.SVSaveAsJob);
			String PassStatement = "PASS >> Job is created successfully when Clicked on save button of Job Details of SureVideo Settings";
			String FailStatement = "Fail >> Job is not created even when Clicked on save button of Job Details of SureVideo Settings";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void UnlockDeviceAndVAlidate() throws IOException, InterruptedException
		{
			Runtime.getRuntime().exec("adb shell input keyevent 26"); 
			sleep(1);
			Runtime.getRuntime().exec("adb shell input keyevent 82");
			sleep(1);
			/*Initialization.driverAppium.findElementById("com.android.systemui:id/key0").click();
			Initialization.driverAppium.findElementById("com.android.systemui:id/key0").click();
			Initialization.driverAppium.findElementById("com.android.systemui:id/key0").click();
			Initialization.driverAppium.findElementById("com.android.systemui:id/key0").click();*/
			Runtime.getRuntime().exec("adb shell input text 000000");
			sleep(1);
			Runtime.getRuntime().exec("adb shell input keyevent 66"); 
		}
		public void EnterSubjectSMS(String subject ) throws InterruptedException
		{
			SubjectTextField.sendKeys(subject);
			sleep(1);
		}
		public void EnterBodySMS(String messageBody)
		{
			BodyTextField.sendKeys(messageBody);
		}
		public void clickOnTextMessageInMailBox() throws InterruptedException
        {
                Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Subject: Dynamic')]").click();
                sleep(3);
                boolean MessageInMaliBox = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'meetat6pm')]").isDisplayed();
                String pass = "PASS >> Meeting at 6pm is displayed";
                String fail = "FAIL >> meeting at 6pm is not displayed";
                ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
                sleep(10);

        }
		
		public void clickOnTextMessageInMailBoxReadNotify() throws InterruptedException
        {
                Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Subject: DynamicRN')]").click();
                sleep(3);
                boolean MessageInMaliBox = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Wssup??')]").isDisplayed();
                String pass = "PASS >> wssup?? text is displayed";
                String fail = "FAIL >> Wssup?? text is not displayed";
                ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
                sleep(10);

        }
		
		 public void SMSForceReadMessage()
         {
                 boolean MessageInMaliBox = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Have a good day')]").isDisplayed();
                 String pass = "PASS >> Force read is displayed";
                 String fail = "FAIL >> Force read is not displayed";
                 ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
         }
		 
		public void ClearDataDownLoadedApp() throws IOException, InterruptedException
		{
			Runtime.getRuntime().exec("adb shell am start -n com.alc.filemanager/com.alc.filemanager.activities.MainActivity");
			sleep(2);
			boolean AllowButton = Initialization.driverAppium.findElementById("com.android.packageinstaller:id/permission_allow_button").isDisplayed();
			String pass = "PASS >> data cleared for downloaded App";
			String fail = "FAIL >> Data Not Cleared for downloded App";
			ALib.AssertTrueMethod(AllowButton, pass, fail);
			sleep(3);
		}
		public void SystemAppCleardata() throws InterruptedException
		{	

			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Contacts']").click();
			sleep(4);
			boolean Startbutton = Initialization.driverAppium.findElementById("com.samsung.android.contacts:id/ftu_start_text").isDisplayed();
			String pass1 = "PASS >> data cleared for system App";
			String fail1 = "FAIL >> Data Not Cleared for system App";
			ALib.AssertTrueMethod(Startbutton, pass1, fail1);
			sleep(3);
		}
		public void disableMailBoxDeviceSide() throws InterruptedException
		{
			Initialization.driverAppium.findElementById("com.nix:id/button2").click();
			sleep(3);
		    common.commonScrollMethod("Uncheck to disable mailbox");
			sleep(2);

		}
		@FindBy(xpath = "//span[text()='Reason: Mailbox disabled on device']")
		private WebElement MailBoxDisabledErrorMessage;
		public void ErrorMessageWhenMailBoxIsDisabled()
		{
			boolean ErrorMessageMailBox = MailBoxDisabledErrorMessage.isDisplayed();
			String pass = "PASS >> 'Error delivering message Reason: Mailbox disabled on device' is displayed";
			String fail = "FAIL >> 'Error delivering message Reason: Mailbox disabled on device' is not displayed";
			ALib.AssertTrueMethod(ErrorMessageMailBox, pass, fail);
		}
		public void LaunchContactsSystemApp() throws IOException
		{
			Runtime.getRuntime().exec("adb shell am start -n com.android.vending/com.android.vending.AssetBrowserActivity");

		}

		 public void UnLockingDeviceUsingADB() throws IOException, InterruptedException
         {
                 Runtime.getRuntime().exec("adb shell input keyevent 26");
                 sleep(1);
                 Runtime.getRuntime().exec("adb shell input keyevent 82");
                 sleep(1);
                 Runtime.getRuntime().exec("adb shell input text 000000");
                 sleep(1);
                 Runtime.getRuntime().exec("adb shell input keyevent 66");
                 sleep(1);
         }
		 
		public void LaunchESfileExplorer() throws IOException, InterruptedException
		{
			Runtime.getRuntime().exec("adb shell am start -n com.estrongs.android.pop/com.estrongs.android.pop.view.FileExplorerActivity");
			sleep(5);

		}
		public void UnlockApplicationByEnteringPassword() throws InterruptedException
		{
			Initialization.driverAppium.findElementByClassName("android.widget.EditText").sendKeys("1111");
			sleep(2);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Unlock']").click();
			sleep(7);
		}
		public void LaunchUberApplication() throws IOException, InterruptedException
		{
			Runtime.getRuntime().exec("adb shell am start -n com.ubercab/com.ubercab.presidio.app.core.root.RootActivity");
			sleep(5);

		}

		
		@FindBy(xpath="//table[@id='devicecalllogtable']/tbody/tr[1]/td[3]")
		private WebElement PhoneNumber;

		@FindBy(xpath="//table[@id='devicecalllogtable']/tbody/tr[1]/td[2]")
		private WebElement CallerName;

		@FindBy(xpath="//table[@id='devicecalllogtable']/tbody/tr[1]/td[4]")
		private WebElement CallTime;

		@FindBy(xpath="//table[@id='devicecalllogtable']/tbody/tr[1]/td[5]")
		private WebElement CallDuration;





		public void ValidateCallLogsCallerName()
		{  
			boolean flag;
			String CallerName_Device =Config.CallLogs_CallerName;
			String CallerName_Console=CallerName.getText();
			if(CallerName_Console.equals(Config.CallLogs_CallerName))
			{
				flag=true;
			}
			else
				flag=false;
			String pass = "PASS >> 'Caller Name In Device And Console Are Matching"+" : "+CallerName_Console;
			String fail = "FAIL >> 'Caller Name In Device And Console Are Not Matching"+" : "+CallerName_Console;
			ALib.AssertTrueMethod(flag, pass, fail);

		}
		public void ValidateCallLogsCallerNumber()
		{
			boolean flag;
			String ActualCallerNumber=PhoneNumber.getText();
			String ExpectedCallerNumber=Config.CallLogs_PhoneNumber;
			if(ActualCallerNumber.equals(ExpectedCallerNumber))
			{
				flag=true;
			}
			else
				flag=false;
			String pass = "PASS >> 'Expeted And Actual Phone Numbers Are Matching"+" : "+ActualCallerNumber;
			String fail = "FAIL >> 'Expeted And Actual Phone Numbers Are Not Matching"+" : "+ActualCallerNumber;
			ALib.AssertTrueMethod(flag, pass, fail);
		}
		public void ValidateCallLogsTime()
		{
			boolean flag;
			String CallLogTime=CallTime.getText();
			if(CallLogTime.equals("N/A"))
			{
				flag=false;
			}
			else
				flag=true;
			String pass = "PASS >> 'Call Time is"+" : "+CallLogTime;
			String fail = "FAIL >> 'Call Time is"+" : "+CallLogTime;
			ALib.AssertTrueMethod(flag, pass, fail);
		}
		public void ValidateCallLogDurationForIncomingCalls()
		{
			boolean flag;
			String Callduration = CallDuration.getText();
			int callduration=Integer.parseInt(Callduration);
			if(callduration > 0)
			{
				flag=true;
			}
			else
				flag=false;
			String pass = "PASS >> Incoming Call Duration is"+" : "+callduration;
			String fail = "FAIL >> Incoming Call Duration is"+" : "+callduration;
			ALib.AssertTrueMethod(flag, pass, fail);	 
		}
		public void ValidateCallLogDurationForOutgoingCalls() 
		{
			boolean flag;
			String Callduration = CallDuration.getText();
			int callduration=Integer.parseInt(Callduration);
			if(callduration >= 0)
			{
				flag=true;
			}
			else
				flag=false;
			String pass = "PASS >> Outgoing Call Duration is"+" : "+callduration;
			String fail = "FAIL >> Outgoing Call Duration is"+" : "+callduration;
			ALib.AssertTrueMethod(flag, pass, fail);

		}
		public void ValidateCallLogDurationForMissedCalls_RejectedCalls()
		{
			boolean flag;
			String Callduration = CallDuration.getText();
			int callduration=Integer.parseInt(Callduration);
			if(callduration == 0)
			{
				flag=true;
			}
			else
				flag=false;
			String pass = "PASS >> Outgoing Call Duration is"+" : "+callduration;
			String fail = "FAIL >> Outgoing Call Duration is"+" : "+callduration;
			ALib.AssertTrueMethod(flag, pass, fail);

		}

		@FindBy(xpath="//table[@id='devicesmslogtable']/tbody/tr/td[2]")
		private WebElement SMSLogSenderName;

		@FindBy(xpath="//table[@id='devicesmslogtable']/tbody/tr[1]/td[3]")
		private WebElement SMSLogSenderNumber;

		@FindBy(xpath="//table[@id='devicesmslogtable']/tbody/tr[1]/td[4]")
		private WebElement SMSLogTime;

		@FindBy(xpath="//table[@id='devicesmslogtable']/tbody/tr[1]/td[5]")
		private WebElement SMSLogMessage;

		@FindBy(xpath="//i[@class='fa fa-inbox']")
		private WebElement InboxSymbol;

		@FindBy(xpath="//i[@class='fa fa-paper-plane']")
		private WebElement SentSymbol;

		public void ValidateSMSLogSenderName()
		{
			boolean flag;
			String ExpectedSenderName=Config.SMSLogs_SenderName;
			String ActualSenderName=SMSLogSenderName.getText();
			if(ActualSenderName.equals(ExpectedSenderName))
				flag=true;
			else
				flag=false;
			String pass = "PASS >> Actual and Expected Sender Names Are Matching"+" : "+ActualSenderName;
			String fail = "FAIL >> Actual and Expected Sender Names Are Not Matching"+" : "+ActualSenderName;
			ALib.AssertTrueMethod(flag, pass, fail);

		}
		public void ValidateSMSLogSenderNumber()
		{
			boolean flag;
			String ExpectedSenderNumber=Config.SMSLOgs_SenderNumber;
			String ActualSenderNumber=SMSLogSenderNumber.getText();
			if(ActualSenderNumber.equals(ExpectedSenderNumber))
				flag=true;
			else
				flag=false;
			String pass = "PASS >> Actual and Expected Sender Numbers Are Matching"+" : "+ActualSenderNumber;
			String fail = "FAIL >> Actual and Expected Sender Numbers Are Not Matching"+" : "+ActualSenderNumber;
			ALib.AssertTrueMethod(flag, pass, fail);
		}
		public void ValidateSMSLogTime() 
		{
			boolean flag;
			String SMSLogtime=SMSLogTime.getText();
			if(SMSLogtime.equals("N/A"))
				flag=false;
			else
				flag=true;
			String pass = "PASS >> 'SMS Time is"+" : "+SMSLogtime;
			String fail = "FAIL >> 'SMS Time is"+" : "+SMSLogtime;
			ALib.AssertTrueMethod(flag, pass, fail);
		}
		public void ValidteSMSStatusInInboxSection()
		{
			boolean flag;
			if(InboxSymbol.isDisplayed())
				flag=true;
			else
				flag=false;
			String pass = "PASS >> 'SMS Is Inside Inbox Section";
			String fail = "FAIL >> 'SMS Isn't Inside Inbox Section";
			ALib.AssertTrueMethod(flag, pass, fail); 
		}
		public void ValidateSMSStatusInSentSection()
		{
			boolean flag;
			if(SentSymbol.isDisplayed())
				flag=true;
			else
				flag=false;
			String pass = "PASS >> 'SMS Is Inside Sent Section";
			String fail = "FAIL >> 'SMS Isn't Inside Sent Section";
			ALib.AssertTrueMethod(flag, pass, fail);


		}
		public void ValidateSMSContent()
		{
			boolean flag;
			String SMSContent = SMSLogMessage.getText();
			if(SMSContent.equals("N/A"))
				flag=false;
			else
				flag=true;
			String pass = "PASS >> 'SMS Content is"+" : "+SMSContent;
			String fail = "FAIL >> 'SMS Content is"+" : "+SMSContent;
			ALib.AssertTrueMethod(flag, pass, fail);	
		}

		@FindBy(xpath="//div[@id='call_tracking_job']")
		private WebElement StaticJobCallLogTrackingJob;

		@FindBy(id="job_calllog_name_input")
		private WebElement StaticJobCallLogTrackingJobNameTextBox;

		@FindBy(id="CalltrackTrackingOn")
		private WebElement StaticJobCallLogTrackingStatusBox;

		@FindBy(id="call_tracking_interval")
		private WebElement StaticJobCallLogTrackingPeriodicityTextBox;

		@FindBy(xpath="//*[@id='CallTrackingModal']/div/div/div[3]/button")
		private WebElement StaticJobCallLogTrackingOKButton;

		@FindBy(id="applyJobButton")
		private WebElement ApplyJobButton;

//		@FindBy(xpath="//div[@id='JobGridContainer']/div[2]/div[1]/div[1]/div[2]/input")
//		private WebElement SearchJobTextField;
		
		@FindBy(xpath="//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
		private WebElement SearchJobTextField;


		@FindBy(xpath="//a[text()='Call Log Tracking']")
		private WebElement TelecomManagementCallLogTrackingButton;

		@FindBy(id="CalltrackTrackingOn")
		private WebElement TelecomManagementCallLogStatus;

		@FindBy(xpath="//*[@id='telecom_mgnment_modal']/div/div/div[1]/button")
		private WebElement TelecomManagementCallLogTrackingCancelButton;

		@FindBy(xpath="//span[text()='Data usage tracking updated successfully.']")
		private WebElement TeelcomManagementConfirmationmessage;


		public void ClickOnStaticJobCallLogTrackingJob() throws InterruptedException
		{
			StaticJobCallLogTrackingJob.click();
			waitForXpathPresent("//*[@id='CallTrackingModal']/div/div/div[3]/button");
			sleep(4);
		}
		public void  CreatingStaticJobCalllLogTracking_ON(String Jobname) throws InterruptedException
		{
			StaticJobCallLogTrackingJobNameTextBox.sendKeys(Jobname);
			Select sel=new Select(StaticJobCallLogTrackingStatusBox);
			sel.selectByValue("1");
			StaticJobCallLogTrackingPeriodicityTextBox.sendKeys("15");
			StaticJobCallLogTrackingOKButton.click();
			waitForidPresent("job_new_job");
			sleep(4);
		}

		public void SelectingCallLogsJobStatus(String Value) throws InterruptedException
		{
			Select sel=new Select(StaticJobCallLogTrackingStatusBox);
			sel.selectByValue(Value);
			StaticJobCallLogTrackingPeriodicityTextBox.sendKeys("15");
			StaticJobCallLogTrackingOKButton.click();
			waitForidPresent("job_new_job");
			sleep(4);
		}
		public void SelectingCallLogsJobStatusTeleCom(String Periodicity) throws InterruptedException
		{
			StaticJobCallLogTrackingPeriodicityTextBox.clear();
			sleep(2);
			StaticJobCallLogTrackingPeriodicityTextBox.sendKeys(Periodicity);
			sleep(4);
		}

		public void  CreatingStaticJobCalllLogTracking_OFF(String Jobname) throws InterruptedException
		{
			StaticJobCallLogTrackingJobNameTextBox.sendKeys(Jobname);
			Select sel=new Select(StaticJobCallLogTrackingStatusBox);
			sel.selectByValue("0");
			StaticJobCallLogTrackingOKButton.click();	
			waitForidPresent("job_new_job");
			sleep(4);
		}
		public void CountingFirstIntervalCalls()
		{
			List<WebElement> FirstIntervalCallCount=Initialization.driver.findElements(By.xpath("//table[@id='devicecalllogtable']/tbody/tr/td[2]"));
			FirstIntervalCount=FirstIntervalCallCount.size();
			System.out.println(FirstIntervalCount);
			Reporter.log("Total Number Of Calls In First Interval is"+" : "+FirstIntervalCount,true);
		}
		public void CountingSecondIntervalCalls()
		{
			List<WebElement> SecondIntervalCallCount = Initialization.driver.findElements(By.xpath("//table[@id='devicecalllogtable']/tbody/tr/td[2]"));
			SecondIntervalCount=SecondIntervalCallCount.size();
			System.out.println(SecondIntervalCount);
			Reporter.log("Total Number Of Calls In Second Interval is"+" : "+SecondIntervalCount,true);
		}

		public void ClickOnApplyJobButton() throws InterruptedException
		{
			ApplyJobButton.click();
			waitForidPresent("applyJob_okbtn");
			sleep(8);
		}

		@FindBy(xpath="//div[@class='pull-right search showClearBtn']")
		private WebElement CancelButton;


		public void Applying_StaticCallLogTracking_OnJob(String Jobname) throws InterruptedException
		{
			SearchJobTextField.sendKeys(Jobname);
			waitForXpathPresent("//p[text()='"+Jobname+"']");
			sleep(4);
			Initialization.driver.findElement(By.xpath("//*[@id='applyJobDataGrid']/tbody/tr/td[2]")).click();
		}
/*		public void CheckStatusOfappliedCallLogTrackingJob() throws InterruptedException
		{
			WebDriverWait wait3 = new WebDriverWait(Initialization.driver,1200); 
			try{
				wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']"))).isDisplayed();
				System.out.println("PASS>>> Job is deployed successfully to device");
			}
			catch(Throwable e){
				System.out.println("job in queue refresh device");
				Initialization.driver.findElement(By.id("refreshButton")).click();
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);  
				sleep(4);
				try{
					wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']"))).isDisplayed();
					System.out.println("PASS>>> Job is deployed successfully to device after device refresh");

				}
				catch(Throwable e1){
					System.out.println("FAIL>>> job not deployed to device");
				}	  
			}

		}
*/	
		@FindBy(xpath="//p[contains(text(),'The job named \"CallLogTracking_ON\" was successfully deployed on the device named "+"\""+Config.DeviceName+"\".')]")
		private List<WebElement> CallLogActivityLogs;

		public void VerifyOfCallLogsActivityLogsDeployed() throws InterruptedException
		{
			boolean value=CallLogActivityLogs.get(0).isDisplayed();
			String ActualValue=CallLogActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Call is displayed successfully  "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Call is not displayed "; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			sleep(2);

		}	
		@FindBy(xpath="//p[contains(text(),'The job named \"SMSLogTracking_ON\" was successfully deployed on the device named "+"\""+Config.DeviceName+"\".')]")
		private List<WebElement> SmsLogActivityLogs;

		public void VerifyOfSmsLogsActivityLogsDeployed()
		{
			boolean value=SmsLogActivityLogs.get(0).isDisplayed();
			String ActualValue=SmsLogActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for Call is displayed successfully  "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for Call is not displayed "; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}	

		public void CheckStatusOfappliedCallLogTrackingJob() throws InterruptedException
		{
			WebDriverWait wait3 = new WebDriverWait(Initialization.driver,1200); 
			try{
				wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("The job named \"CallLogTracking_ON\" was successfully deployed on the device named "+"\""+Config.DeviceName+"\"."))).isDisplayed();
				System.out.println("PASS>>> Job is deployed successfully to device");
			}
			catch(Throwable e){
				System.out.println("job in queue refresh device");
				Initialization.driver.findElement(By.id("refreshButton")).click();
				Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);  
				sleep(4);
				try{
					wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("The job named \"CallLogTracking_ON\" was successfully deployed on the device named "+"\""+Config.DeviceName+"\"."))).isDisplayed();
					System.out.println("PASS>>> Job is deployed successfully to device after device refresh");

				}
				catch(Throwable e1){
					System.out.println("FAIL>>> job not deployed to device");
				}	  
			}

		}

		public void ToVerifyDynamicJobStatus_ON()
		{
			boolean flag;
			Select sel=new Select(CallLogStatus);
			String FirstSelectedOption = sel.getFirstSelectedOption().getText();
			if(FirstSelectedOption.equals("On"))
			{
				flag=true;
			}
			else
				flag=false;
			String PassStatement="PASS >> Dynamic Call Log Tracking Job Status Is ON When Static CallLog Tracking_ON Job Is Applied On Device";
			String FailStatement="PASS >> Dynamic Call Log Tracking Job Status Is Not ON When Static CallLog Tracking_ON Job Is Applied On Device";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		public void ClickOnTelecomManagementCallLogTrackingButton() throws InterruptedException
		{
			TelecomManagementCallLogTrackingButton.click();
			waitForXpathPresent("//*[@id='okbtn']");
			sleep(2);
		}
		public void ToVerifyTelecomManagementCallLogStatus_ON() throws InterruptedException
		{
			boolean flag;
			Select sel=new Select(TelecomManagementCallLogStatus);
			String TelecomManagementCallLogFirstSelectedOption = sel.getFirstSelectedOption().getText();
			if(TelecomManagementCallLogFirstSelectedOption.equals("On"))
				flag=true;
			else
				flag=false;
			String PassStatement="PASS >> Telecom Management Call Log Tracking Job Status Is ON When Static CallLog Tracking_ON Job Is Applied On Device";
			String FailStatement="PASS >> Telecom Management Call Log Tracking Job Status Is Not ON When Static CallLog Tracking_ON Job Is Applied On Device";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);		
			TelecomManagementCallLogTrackingCancelButton.click();
			sleep(10);
		}
		public void Turning_OFFTelecomManagementCallLogTracking() throws InterruptedException
		{
			boolean flag;
			Select sel=new Select(TelecomManagementCallLogStatus);
			sel.selectByValue("0");
			Initialization.driver.findElement(By.xpath("//*[@id='okbtn']")).click();
			waitForXpathPresent("//span[text()='Data usage tracking settings updated successfully.']");
			sleep(4);
			if(TelecomManagementConfirmationMessage.isDisplayed())
			{
				flag=true;
			}
			else
				flag=false;
			String PassStatement="PASS >> Telecom Management Call Log Tracking Updated Successfully";
			String FailStatement="FAIL >> Telecom Management Call Log Tracking Not Updated Successfully";
		}
		public void Turning_ONTelecomManagementCallLogTracking() throws InterruptedException
		{
			boolean flag;
			Select sel=new Select(TelecomManagementCallLogStatus);
			sel.selectByValue("1");
			Initialization.driver.findElement(By.xpath("//*[@id='okbtn']")).click();
			waitForXpathPresent("//span[text()='Data usage tracking settings updated successfully.']");
			sleep(4);
			if(TelecomManagementConfirmationMessage.isDisplayed())
			{
				flag=true;
			}
			else
				flag=false;
			String PassStatement="PASS >> Telecom Management Call Log Tracking Updated Successfully";
			String FailStatement="FAIL >> Telecom Management Call Log Tracking Not Updated Successfully";
		}
		public void Turning_ONTelecomManagementCallLogTrackingJobs(String Status) throws InterruptedException
		{
			boolean flag;
			Select sel=new Select(TelecomManagementCallLogStatus);
			sel.selectByValue(Status);
			sleep(4);
		}
		public void ToVerifyDynamicJobStatus_OFF()
		{
			Select sel=new Select(CallLogStatus);
			String FirstSelectedOption = sel.getFirstSelectedOption().getText();
			System.out.println(FirstSelectedOption);
			if(FirstSelectedOption.equalsIgnoreCase("Off"))
			{
				Reporter.log("PASS >> Dynamic Call Log Tracking Job Status Is OFF When Telecom Management Call Tracking Is OFF");
			}
			else
			{
				ALib.AssertFailMethod("PASS >> Dynamic Call Log Tracking Job Status Is Not OFF When Telecom Management Call Tracking Is OFF");

			}
		}
		@FindBy(xpath="(//div[text()='Telecom Management']/following-sibling::div/span)[1]")
		private WebElement TeleComMagagementStatus;
		
		public void ToVerifyTeleCom_OFF()
		{
			String VerifyTeleComStatus=TeleComMagagementStatus.getText();
			System.out.println(VerifyTeleComStatus);
			if(VerifyTeleComStatus.equals("OFF"))
			{
				Reporter.log("PASS >> TeleCom Job Status Is OFF When Telecom Management Call Tracking Is OFF");
			}
			else
			{
				ALib.AssertFailMethod("PASS >> TeleCom Job Status Is Not OFF When Telecom Management Call Tracking Is OFF");

			}
		}

		public void WaitForInterval() throws InterruptedException, IOException
		{
			Reporter.log("Waiting For 15 Mins To Call/SMS Log Updation",true);
			Reporter.log("Perform Call and SMS Actions",true);
			Runtime.getRuntime().exec("adb shell am start -a android.intent.action.CALL -d tel:9380377695");
			sleep(300);
		}
		public void ToVerifyCallLogsForSecondInterval() 
		{
			boolean flag;
			System.out.println(SecondIntervalCount);
			System.out.println(FirstIntervalCount);
			if(SecondIntervalCount > FirstIntervalCount)
				flag=true;
			else
				flag=false;
			String PassStatement="PASS >> Call Logs Are Updating In Second interval";
			String FailStatement="PASS >> Call Logs Are Not Updating In Second interval";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

		}	

		@FindBy(id="sms_tracking_job")
		private WebElement StaticJobSMSLogTrackingJob;

		@FindBy(id="job_smslog_name_input")
		private WebElement StaticJobSMSLogTrackingJobNameTextBox;

		@FindBy(xpath="//input[@id='sms_tracking_interval']")
		private WebElement StaticJobSMSLogTrackingPeriodicityTextBox;
		
		@FindBy(xpath="(//input[@id='sms_tracking_interval'])[1]")
		private WebElement StaticJobTeleComSMSLogTrackingPeriodicityTextBox;


		@FindBy(xpath="//*[@id='SmsTrackingModal']/div/div/div[3]/button")
		private WebElement StaticJobSMSLogTrackingOKButton;

		@FindBy(xpath="//select[@id='SMStrackTrackingOn']")
		private WebElement StaticJobSMSLogTrackingStatusBox;

		@FindBy(xpath="//a[text()='SMS Log Tracking']")
		private WebElement TelecomManagementSMSLogTrackingButton;

		@FindBy(xpath="//select[@id='SMStrackTrackingOn']")
		private WebElement TelecomManagementSMSLogStatus;

		public void ClickOnStaticJobSMSLogTrackingJob() throws InterruptedException
		{
			StaticJobSMSLogTrackingJob.click();
			waitForXpathPresent("//*[@id='SmsTrackingModal']/div/div/div[3]/button");
			sleep(4);
		}
		public void  CreatingStaticJobSMSLogTracking_ON(String Jobname) throws InterruptedException
		{
			StaticJobSMSLogTrackingJobNameTextBox.sendKeys(Jobname);
			Select sel=new Select(StaticJobSMSLogTrackingStatusBox);
			sel.selectByValue("1");
			StaticJobSMSLogTrackingPeriodicityTextBox.sendKeys("15");
			StaticJobSMSLogTrackingOKButton.click();
			waitForidPresent("job_new_job");
			sleep(10);
		}
		
		public void SendingSMSLogTrackingJobName(String JobName)
		{
			StaticJobSMSLogTrackingJobNameTextBox.sendKeys(JobName);
		}
		public void SelectingSMSLogTrackingJobStatus(String Val)
		{
			Select sel=new Select(StaticJobSMSLogTrackingStatusBox);
			sel.selectByValue(Val);
		}
		
		public void SendingSMSLogTrackingPeriodicity(String Periodicity)
		{
			StaticJobSMSLogTrackingPeriodicityTextBox.sendKeys(Periodicity);
		}
		public void SendingSMSLogTrackingPeriodicityTeleCom(String Periodicity) throws InterruptedException
		{
			StaticJobTeleComSMSLogTrackingPeriodicityTextBox.clear();
			sleep(2);
			StaticJobTeleComSMSLogTrackingPeriodicityTextBox.sendKeys(Periodicity);
			sleep(2);
		}

		public void ClickOnSMSLogtrackingJObOKButton() throws InterruptedException
		{
			StaticJobSMSLogTrackingOKButton.click();
			waitForidPresent("job_new_job");
			sleep(5);
		}
		public void CountingFirstIntervalSMS()
		{
			List<WebElement> FirstIntervalCallCount=Initialization.driver.findElements(By.xpath("//table[@id='devicesmslogtable']/tbody/tr/td[2]"));
			FirstIntervalSMSCount=FirstIntervalCallCount.size();
			System.out.println(FirstIntervalSMSCount);
			Reporter.log("Total Number Of SMS In First Interval is"+" : "+FirstIntervalSMSCount,true);
		}
		public void CountingSecondIntervalSMS()
		{
			List<WebElement> SecondIntervalCallCount = Initialization.driver.findElements(By.xpath("//table[@id='devicesmslogtable']/tbody/tr/td[2]"));
			SecondIntervalSMSCount=SecondIntervalCallCount.size();
			System.out.println(SecondIntervalSMSCount);
			Reporter.log("Total Number Of SMS In Second Interval is"+" : "+SecondIntervalSMSCount,true);
		}
		public void Applying_StaticSMSLogTracking_OnJob(String Jobname) throws InterruptedException
		{
			SearchJobTextField.sendKeys(Jobname);
			waitForXpathPresent("//p[text()='"+Jobname+"']");
			sleep(4);
			Initialization.driver.findElement(By.xpath("//*[@id='applyJobDataGrid']/tbody/tr/td[2]")).click();
		}
		public void ToVerifyDynamicSMSTrackingJobStatus_ON()
		{
			boolean flag;
			Select sel=new Select(SMSLogStatus);
			String FirstSelectedOption = sel.getFirstSelectedOption().getText();
			if(FirstSelectedOption.equals("On"))
			{
				flag=true;
			}
			else
				flag=false;
			String PassStatement="PASS >> Dynamic SMS Log Tracking Job Status Is ON When Static SMS Tracking_ON Job Is Applied On Device";
			String FailStatement="PASS >> Dynamic SMS Log Tracking Job Status Is Not ON When Static SMS Log Tracking_ON Job Is Applied On Device";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		public void ClickOnTelecomManagementSMSLogTrackingButton() throws InterruptedException
		{
			TelecomManagementSMSLogTrackingButton.click();
			waitForXpathPresent("//*[@id='okbtn']");
			sleep(2);
		}
		public void ToVerifyTelecomManagementSMSLogStatus_ON() throws InterruptedException
		{
			boolean flag;
			Select sel=new Select(TelecomManagementSMSLogStatus);
			String TelecomManagementCallLogFirstSelectedOption = sel.getFirstSelectedOption().getText();
			if(TelecomManagementCallLogFirstSelectedOption.equals("On"))
				flag=true;
			else
				flag=false;
			String PassStatement="PASS >> Telecom Management SMS Log Tracking Job Status Is ON ";
			String FailStatement="PASS >> Telecom Management SMS Log Tracking Job Status Is OFF";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);		
			TelecomManagementCallLogTrackingCancelButton.click();
			sleep(10);
		}
		public void Turning_OFFTelecomManagementSMSLogTracking() throws InterruptedException
		{
			Select sel=new Select(TelecomManagementSMSLogStatus);
			sel.selectByValue("0");
			Initialization.driver.findElement(By.xpath("//*[@id='okbtn']")).click();
			waitForXpathPresent("//span[text()='Data usage tracking settings updated successfully.']");
			sleep(4); 
			Reporter.log("Telecom ManageMent SMS Log Status Is Turned Off",true);
		}
		public void Turning_ONTelecomManagementSMSLogTracking() throws InterruptedException
		{
			Select sel=new Select(TelecomManagementSMSLogStatus);
			sel.selectByValue("1");
			Initialization.driver.findElement(By.xpath("//*[@id='okbtn']")).click();
			waitForXpathPresent("//span[text()='Data usage tracking settings updated successfully.']");
			sleep(4); 
			Reporter.log("Telecom ManageMent SMS Log Status Is Turned Off",true);
		}
		public void Turning_ONTelecomManagementSMSLogTrackingJobs() throws InterruptedException
		{
			Select sel=new Select(TelecomManagementSMSLogStatus);
			sel.selectByValue("1");
			sleep(4); 
			Reporter.log("Telecom ManageMent SMS Log Status Is Turned ON",true);
		}

		public void ToVerifyDynamicJobSMSStatus_OFF()
		{
			boolean flag;
			Select sel=new Select(SMSLogStatus);
			String FirstSelectedOption = sel.getFirstSelectedOption().getText();
			if(FirstSelectedOption.equals("Off"))
			{
				flag=true;
			}
			else
				flag=false;
			String PassStatement="PASS >> Dynamic SMS Log Tracking Job Status Is OFF When Telecom Management SMS Tracking Is OFF";
			String FailStatement="PASS >> Dynamic SMS Log Tracking Job Status Is Not OFF When Telecom Management SMS Tracking Is OFF";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		public void ToVerifySMSLogsForSeconfInterval()
		{
			boolean flag;
			System.out.println(FirstIntervalSMSCount);
			System.out.println(SecondIntervalSMSCount);
			if(SecondIntervalSMSCount>FirstIntervalSMSCount)
				flag=true;
			else 
				flag=false;

			String PassStatement="PASS >> SMS Logs Are Updating In Second interval";
			String FailStatement="PASS >> SMS Logs Are Not Updating In Second interval";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}



		@FindBy(xpath="//div[@id='surelockSettings']")
		private WebElement SureLockDynamicJob;

		@FindBy(xpath="//input[@type='button' and @value='Install Now']")
		private WebElement InstallNowbtnSureLockSettingsPopup; 

		@FindBy(xpath="(//input[@value='Next'])[1]")
		private WebElement NextButtonSurelockSettingsPopUp;

		@FindBy(id="activationCode")
		private WebElement ActivationCodeTextField;


		@FindBy(xpath="(//input[@value='Next'])[2]")
		private WebElement NextActivationKeyPage;

		@FindBy(xpath="//*[@id='downlaoding_loader']/div/div/span/div")
		private WebElement DownloadingProgress;

		@FindBy(xpath="//*[@id='downlaoding_loader']/div/div/div[text()='Downloading']")
		private WebElement Downloading;

		@FindBy(xpath="//div[text()='Trying to connect to device...']")
		private WebElement TryingToconnect;

		@FindBy(xpath="//div[text()='Loader']")
		private WebElement InstallingLoader;

		@FindBy(id="skip_launch_default_home")
		private WebElement SkipBtn;


		@FindBy(xpath = "//*[@id='appplying_setings_to_dv']/div/div/div[2]")
		private WebElement ApplyingSureLockSetingsText;

		@FindBy(xpath = "//*[@id='appplying_setings_to_dv']/div/div/div[1]")
		private WebElement ApplyingSureLockSettingsLoadingIcon;


		@FindBy(xpath="//div[text()='An error occurred while applying settings.']")
		private WebElement AnerrorOccurredMsg;

		@FindBy(xpath = "//input[@value='Retry']")
		private WebElement ApplyingsurelockSettingRetryButton;

		@FindBy(xpath="(//h5[text()='Success'])[1]")
		private WebElement Successmsg;

		@FindBy(id="refreshButton")
		private WebElement refreshdeviceinfo;

		@FindBy(id="RefreshSetng")
		private WebElement RefreshBtnSettings;

		@FindBy(id="hidden")
		private WebElement HideApplicationIconCheckBox;

		@FindBy(id="startup")
		private WebElement RunatStartUpCheckBox;

		@FindBy(id="freshlaunch")
		private WebElement LauchAppAfterRefreshCheckBox;

		@FindBy(id="DoneBtn")
		private WebElement DoneBtnRemoveApp;

		@FindBy(xpath="//input[@class='ct-selbox-in wallcheck']")
		private WebElement UseSystemWallpaperCheckBox;

		@FindBy(xpath="//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement BackgroundColorCheckBox;

		@FindBy(xpath=".//*[@id='use_classic_clac']/div/div[2]/span/input")
		private WebElement useclassicCalculationCheckBox;

		@FindBy(xpath="//li[@id='icon_relocation_settings']//input[@class='ct-selbox-in']")
		private WebElement AllowIconRelocationCheckBox;

		@FindBy(xpath="//li[@id='detect_network_settings']//input[@class='ct-selbox-in']")
		private WebElement DetectNetworkSettingsCheckBox;


		@FindBy(xpath="//li[@id='full_screen_mode_settings']//input[@class='ct-selbox-in']")
		private WebElement full_screen_mode_settings_checkbox;

		@FindBy(xpath="//li[@id='notification_badge_settings']//input[@class='ct-selbox-in']")
		private WebElement notification_badge_settingscheckBox;

		@FindBy(xpath="//li[@id='hide_apptitle_settings']//input[@class='ct-selbox-in']")
		private WebElement hide_apptitle_settingsCheckBox;

		@FindBy(xpath="//li[@id='multi_user_mode']//input[@class='ct-selbox-in']")
		private WebElement multi_user_modeCheckBox;

		@FindBy(xpath="//li[@id='disable_bottombar_setting']//input[@class='ct-selbox-in']")
		private WebElement disable_bottombar_settingCheckBox;

		@FindBy(xpath="//li[@id='hide_bottom_bar']//input[@class='ct-selbox-in']")
		private WebElement hide_bottom_barCheckBox;

		@FindBy(xpath="//li[@id='prefer_mobileData']//input[@class='ct-selbox-in']")
		private WebElement prefer_mobileDataCheckBox;

		@FindBy(xpath="//li[@id='volume_settings']//input[@class='ct-selbox-in']")
		private WebElement volume_settingsCheckBox;

		@FindBy(xpath="//li[@id='enable_toast_message']//input[@class='ct-selbox-in']")
		private WebElement enable_toast_messageCheckBox;

		@FindBy(xpath="//li[@id='watch_dog_service']//input[@class='ct-selbox-in']")
		private WebElement watch_dog_serviceCheckBox;

		@FindBy(xpath=" //li[@id='kill_unallowed_apps']//input[@class='ct-selbox-in']")
		private WebElement kill_unallowed_appsCheckBox;

		@FindBy(xpath="//li[@id='lock_safemode_setting']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement lock_safemode_settingCheckBox;

		@FindBy(xpath="//li[@id='usb_state_home']//input[@class='ct-selbox-in']")
		private WebElement usb_state_homeCheckBox;

		@FindBy(xpath="//li[@id='clear_data_onhomeload']//input[@class='ct-selbox-in']")
		private WebElement clear_data_onhomeloadCheckBox;

		@FindBy(xpath="//*[@id='suppress_notification_panel']/div/div[2]/span/input")
		private WebElement suppress_notification_panelCheckBox;

		@FindBy(xpath="//li[@id='block_notification']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement block_notificationCheckBox;

		@FindBy(xpath="//li[@id='disable_status_bar']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement disable_status_barCheckBox;

		@FindBy(xpath="//li[@id='disable_factory_reset']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement disable_factory_resetCheckBox;

		@FindBy(xpath="//li[@id='disable_usb']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement disable_usbCheckBox;

		@FindBy(xpath="//li[@id='disable_external_card']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement disable_external_cardCheckBox;

		@FindBy(xpath="//li[@id='suppresss_power_button']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement suppresss_power_buttonCheckBox;

		@FindBy(xpath="//li[@id='enable_soft_keyboard']//input[@class='ct-selbox-in sc-chkBox']")
		private WebElement enable_soft_keyboardCheckBox;

		@FindBy(xpath="//div[text()='Connection timed out']")
		private WebElement connectiontimedout;

		@FindBy(xpath="//a[text()='Retry']")
		private WebElement RetryButton;

		@FindBy(xpath="(//button[text()='Done'])[1]")
		private WebElement DoneButtonSureLockSettings;

		@FindBy(xpath="//p[text()='Allowed Widgets']")
		private WebElement AllowedWidgetsOption;

		@FindBy(xpath="(//button[text()='Ok'])[25]")
		private WebElement OkDisableBottomBarBtn;

		@FindBy(xpath="(//button[text()='Ok'])[1]")
		private WebElement OkBtnLockSafePopUp;

		@FindBy(xpath="//button[@id='add_app_shortcut']")
		private WebElement AddAppShortcutButton;

		@FindBy(xpath="//button[@id='add_widget']")
		private WebElement AddWidgetButton;


		@FindBy(xpath=".//*[@id='volume_settings']/div/div[2]/span/input")
		private WebElement VolumeSettingsCheckBox;

		@FindBy(xpath=".//*[@id='prefer_mobileData']/div/div[2]/span/input")
		private WebElement PrefereMObdataCheckBox;

		@FindBy(xpath="(//button[text()='OK'])[1]")
		private WebElement OkBtnDisableBottombarPopUp;

		@FindBy(xpath="//*[@id='lock_safemode_setting']/div/div[2]/span/input")
		private WebElement LockSafeCheckBox;

		@FindBy(xpath=".//*[@id='usb_state_home']/div/div[2]/span/input")
		private WebElement UsbStateHomeCheckBox;

		@FindBy(xpath="//*[@id='block_notification']/div/div[2]/span/input")
		private WebElement BlockNotifcationCheckBox;

		@FindBy(xpath="//*[@id='suppresss_power_button']/div/div[2]/span/input")
		private WebElement SuppressPowerCheckBox;

		@FindBy(xpath="//*[@id='enable_soft_keyboard']/div/div[2]/span/input")
		private WebElement EnableKeboardCheckBox;

		@FindBy(xpath="//p[text()='Manage Shortcuts']")
		private WebElement ManageShortcutOption;

		@FindBy(xpath="//span[text()='Accept new shortcuts']")
		private WebElement AcceptnewshortcutsOption;

		@FindBy(xpath="//span[text()='Remove shortcuts on app uninstall']")
		private WebElement RemoveshortcutsOption;

		@FindBy(xpath="//input[@id='accept_shortcuts']")
		private WebElement accept_shortcutsCheckBox;

		@FindBy(xpath="//input[@id='remove_shortcuts']")
		private WebElement remove_shortcutsCheckBox;

		@FindBy(xpath="//li[@id='showCallInScreen']//input[@class='ct-selbox-in']")
		private WebElement ShowCallInscreenCheckBox;

		@FindBy(xpath="//li[@id='blockIncomingCalls']//input[@class='ct-selbox-in']")
		private WebElement blockIncomingCallsCheckBox;

		@FindBy(xpath="//li[@id='blockOutgoingCalls']//input[@class='ct-selbox-in']")
		private WebElement blockOutgoingCallsCheckBox;

		@FindBy(xpath="//li[@id='whitelistNewContacts']//input[@class='ct-selbox-in']")
		private WebElement whitelistNewContactsCheckBox;

		@FindBy(xpath="//li[@id='blockIncomingSMS']//input[@class='ct-selbox-in']")
		private WebElement blockIncomingSMSCheckBox;

		@FindBy(xpath="//li[@id='blockOutgoingSMS']//input[@class='ct-selbox-in']")
		private WebElement blockOutgoingSMSCheckBox;

		@FindBy(xpath="//li[@id='blockIncomingMMS']//input[@class='ct-selbox-in']")
		private WebElement blockIncomingMMSCheckBox;

		@FindBy(xpath="//li[@id='blockOutgoingMMS']//input[@class='ct-selbox-in']")
		private WebElement blockOutgoingMMSCheckBox;

		@FindBy(xpath="//li[@id='enablesmscommand']//input[@class='ct-selbox-in']")
		private WebElement enablesmscommandCheckBox;



		//APPIUM PART

		public void ClickOnSureLockJob() throws InterruptedException {
			SureLockDynamicJob.click();
			waitForXpathPresent("//*[@id='sureLock_homePage_header']/h4[contains(text(),'SureLock Settings')]");
			sleep(40);
			Reporter.log("SureLock Settings is displayed", true);
		}
		public void ClickOnInsallNowButtonSureLockSettingPopUp() throws InterruptedException {
			InstallNowbtnSureLockSettingsPopup.click();
			waitForidPresent("AllowedApplications");
			Reporter.log("SureLock Settings. page displayed", true);
			sleep(2);
		}
		public void AllowedApplicationAddApp(String ApplicationName) throws Throwable
		{
			ClickOnAllowedApplication();
			AddApp.click();
			waitForXpathPresent("//span[contains(@class,'search_btn')]");
			sleep(1);
			Reporter.log("Clicked on Add app of Allowed Applications");
			WebElement element = Initialization.driver.findElement(By.xpath("//div[p[text()='"+ApplicationName+"']]/following-sibling::div/span/input"));
			((JavascriptExecutor) Initialization.driver).executeScript(
					"arguments[0].scrollIntoView();", element);
			element.click();
			AddAppDoneBtn.click();
			waitForidPresent("add_app");
			sleep(1);
		
		}
		
		public void SearchApp(String AppName) throws InterruptedException
		{
			Initialization.driver.findElement(By.xpath("//*[@id='ava_app_list']/header/span[4]")).click();
			sleep(3);
			Initialization.driver.findElement(By.xpath("//*[@id='surelock_app']")).sendKeys(AppName);
			sleep(5);
		}
		
		 public void VerifyOfEnablingAnySettings() throws InterruptedException {
		    	
			 Initialization.driver.findElement(By.xpath("//*[@id='surelock_settings_pg']/div/ul[1]/li[4]/div/div[2]/span/input")).click();
			 sleep(4);
		 }
		 
		public void ClickOnDoneButtonSureLockSettings() throws InterruptedException
		 {
			 Initialization.driver.findElement(By.xpath("//*[@id='surelock_settings_pg']/footer/div[2]/button")).click();
			 sleep(4);
		 }
		
		@FindBy(xpath=".//*[@id='logContent']/ul/li[1]/p")
		private WebElement FirstLog;
		public void CheckSureVideoInstallationLogs() throws InterruptedException
		{
			String LogContain = FirstLog.getText();
			System.out.println(LogContain);
			sleep(2);
			//boolean value = LogContain.contains("Job("+Config.SureVideoXSLTjob+") applied to device "+Config.DeviceName+"");
			boolean value = LogContain.contains("has been applied to the device known as "+"\""+Config.DeviceName+"\"");//applied to the device known as  //applied to device
			String PassStatement3 = "PASS >> Install SureVideo job applied on the device";
		    String FailStatement3 = "FAIL >> Install SureVideo job not applied on the device";
		    ALib.AssertTrueMethod(value, PassStatement3, FailStatement3);
		    sleep(60);
		}
		@FindBy(xpath="//p[contains(text(),'The job named \"https://mars.42gears.com/support/inout/surevideo.apk\" was successfully deployed on the device named "+"\""+Config.DeviceName+"\".')]")
		private List<WebElement> SureVideoActivityLogs;


		public void VerifySureVideoActivityLogsDeployed()
		{
			WebDriverWait wait=new WebDriverWait(Initialization.driver,120);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//p[contains(text(),'The job named \"https://mars.42gears.com/support/inout/surevideo.apk\" was successfully deployed on the device named "+"\""+Config.DeviceName+"\".')]")));

			boolean value=SureVideoActivityLogs.get(0).isDisplayed();
			String ActualValue=SureVideoActivityLogs.get(0).getText();
			String PassStatement="PASS >> Activity Logs for SureVideo is displayed successfully  "+ActualValue;
			String FailStatement="FAIL >> Activity Logs for SureVideo is not displayed "; 	
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		}	
		
		
		
		
		
		
		
		
		
		@FindBy(xpath="//*[@id='sureVideoRunAtStartUp']/div/div[2]/span/input")
		private WebElement RunAtStartUp;
		public void SureVideoRunAtStartUp() throws InterruptedException
		{
			RunAtStartUp.click();
			sleep(2);
		}
		
		public void ClickOnSureVideoSettingsMenu() throws InterruptedException
		{
			
			Initialization.driver.findElement(By.xpath("//*[@id='surevideoSettings']/div/div[2]")).click();
			sleep(10);
		}
		@FindBy(xpath = "//span[text()='Invalid SureVideo Settings.']")
		private WebElement SureVideoInvalideSettingsErrorMessage;
		public void SureVideoInvalidSettingsErrorMessage() throws Throwable
		{
			String PassStatement="PASS >> 'Invalid SureVideo Settings.' Message is displayed successfully when clicked on Save Source of SureVideo Settings";
			String FailStatement="FAIL >> 'Invalid SureVideo Settings.' Message is displayed when clicked on Save Source of SureVideo Settings";
			Initialization.commonmethdpage.ConfirmationMessageVerify(SureVideoInvalideSettingsErrorMessage, true, PassStatement, FailStatement,5);
		}
		
		@FindBy(xpath="//*[@id='passwordModal']/div/div/div[3]/button[text()='Apply']")
		private WebElement ApplyButtonSureVideoPasswordPrompt;
		public void ApplyButtonSureVidePasswordPrompt() throws InterruptedException
		{
			ApplyButtonSureVideoPasswordPrompt.click();
			sleep(3);
		}
		
		@FindBy(xpath="//*[@id='saveAsJobModal']/div/div/div[3]/button[text()='Save']")
		private WebElement SaveButtonJobDetails;
		public void ClickOnSaveButton() throws InterruptedException
		{
			SaveButtonJobDetails.click();
			sleep(8);
		}
		
		@FindBy(xpath="//*[@id='SaveBtn']")
		private WebElement SaveSource;
		
		public void ClickOnSaveSource() throws InterruptedException
		{
			SaveSource.click();
		}
		
		public void VerifyOfSureVideoSaveAsJob()
		{
			boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(Config.SVSaveAsJob);
			String PassStatement = "PASS >> Job is created when Clicked on close button of Job Details of SureVideo Settings";
			String FailStatement = "Fail >> Job is not created when Clicked on close button of Job Details of SureVideo Settings";
		    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		
		@FindBy(xpath="//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
		private WebElement YesButton;
		public void ClickOnYesButtonApplySureVideoSettings() throws InterruptedException
		{
			YesButton.click();
			sleep(2);
		}
		
		@FindBy(xpath="//*[@id='passwordBox']")
		private WebElement password;
		public void EnterSureVideoPassword() throws InterruptedException
		{
			password.sendKeys("0000");
			sleep(2);
		}
		public void ClickOnSureVideoHomeSettingsCloseButton() throws InterruptedException
		{
			sleep(5);
			Initialization.driver.findElement(By.xpath("//*[@id='commonModalLargeXSLT']/div/div/div/div[1]/button")).click();
			sleep(4);
		}
		public void CheckRunAtStartUpAfterRefresh() throws Throwable {
			
			boolean value = true;
			try {
				if(RunAtStartUp.isEnabled());
			} catch (Exception e) {
				value = false;
			}
			String PassStatement = "PASS >> Run At Start Up is Enabled after refresh of SurVideo Settings Job";
			String FailStatement = "Fail >> Run At Start Up is Enabled after refresh of SurVideo Settings Job";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			sleep(2);
		}
		
		@FindBy(xpath="//*[@id='sureVideoRunAtStartUp']/div/div[2]/span/input")
		private WebElement sureVideoRunAtStartUp;
		public void verifyEnablesureVideoRunAtStartUp(String FontSizeText) throws IOException, InterruptedException {

			sleep(15);
		if (!sureVideoRunAtStartUp.isSelected()) {

			Reporter.log(FontSizeText + "CehckBox Is Already disabled", true);
		} else {
			ALib.AssertFailMethod(FontSizeText + "FAIL TO DISABLE ");
		}
	}

		
		
		
		
		
		
		@FindBy(xpath="//*[@id='surevideo_sett_pg']/footer/div[2]/button")
		private WebElement DoneButtonSureVideoSettings;
		public void DoneButtonSureVideoSettings() throws InterruptedException
		{
			DoneButtonSureVideoSettings.click();
			sleep(4);
		}

		@FindBy(xpath="//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
		private WebElement YesButtonSureVideoInstallation;
		public void ClickOnYesButtonSureVideoInstallation() throws InterruptedException
		{
			YesButtonSureVideoInstallation.click();
			sleep(15);
		}
		
		@FindBy(xpath="//*[@id='ConfirmationDialog']/div/div/div[1]/p")
		private WebElement MessageOnSureVideoInstallation;
		public void MessageDuringInstallationWhenSureVideoNotInstalled()
		{
			String ActualMessage = MessageOnSureVideoInstallation.getText();
			String ExpectedMessage = "SureVideo is not installed on the highlighted device(s). Do you want to install SureVideo?";
			String pass = "PASS >> Message is displayed when trying to install SureVideo" +ActualMessage;
			String fail = "FAIL >> Message is not displayed when trying to install SureVideo";
			ALib.AssertEqualsMethod(ExpectedMessage, ActualMessage, pass, fail);
		}
		
		@FindBy(xpath="//*[@id='sureLock_homePage_header']/button")
		private WebElement SureLockSettingsHomePageCloseButton;
		public void CloseSureLockSettingsHomePageButton() throws InterruptedException
		{
			SureLockSettingsHomePageCloseButton.click();
			sleep(4);
		}
		 public void ClickOnAddAppButton() throws InterruptedException
		    {
		    	AddApp.click();
				waitForXpathPresent("//span[contains(@class,'search_btn')]");
				sleep(4);
				Reporter.log("Clicked on Add app of Allowed Applications");
		    }
		 public void AllowedApplicationAddAppTo(String ApplicationName) throws Throwable
	        {
	                
	                WebElement element = Initialization.driver.findElement(By.xpath("//div[p[text()='"+ApplicationName+"']]/following-sibling::div/span/input"));
	                 ((JavascriptExecutor) Initialization.driver).executeScript(
	                 "arguments[0].scrollIntoView();", element);
	                 element.click();
	                 AddAppDoneBtn.click();
	                 waitForidPresent("add_app");
	                 sleep(2);
	                 
	        }
		 
		public void ClickondoneBttonAllowedapp() throws InterruptedException {
			AllowedApplicationsDoneBtn.click();
			WebDriverWait wait=new WebDriverWait(Initialization.driver,120);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='AllowedApplications']/div/div/p[@class='tit_line']")));
			sleep(2);
		}
		public void ClickOnNextButtonSurelockSettings() throws InterruptedException {
            NextButtonSurelockSettingsPopUp.click();
            waitForXpathPresent("//h5[text()='Please enter activation code.']");
            sleep(3);
    }

		public void EnterActivationKeySL(String ActivationCodeSL) throws InterruptedException {
			ActivationCodeTextField.sendKeys(ActivationCodeSL);
			sleep(4);
		}
		public void ClickOnNextActivationKeyPage() throws InterruptedException {
			NextActivationKeyPage.click();
			while(DownloadingProgress.isDisplayed() && Downloading.isDisplayed()) 
			{}
			try {
				if(connectiontimedout.isDisplayed()) 
				{
					RetryButton.click();
					sleep(4);
					while(TryingToconnect.isDisplayed()) 
					{}
					if(connectiontimedout.isDisplayed()) 
					{
						RetryButton.click();
						sleep(4);
						while(TryingToconnect.isDisplayed()) 
						{}}
					else{

					}
				}
			}catch (Exception e) {}
			while(InstallingLoader.isDisplayed() && InstallingMsg.isDisplayed()){}
			waitForXpathPresent("(//strong[text()='Please follow instruction provided in device to install.'])[1]");
			sleep(2);
		}
		public void ClickOnNextOndeviceside() throws InterruptedException {
			Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
			sleep(2);
			Initialization.driverAppium.findElement(By.id("com.android.packageinstaller:id/ok_button")).click();
		}
		public void ClickOnskip() throws InterruptedException {

			WebDriverWait wait=new WebDriverWait(Initialization.driver,500);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("skip_launch_default_home")));
			sleep(23);
			SkipBtn.click();
			while(ApplyingSureLockSetingsText.isDisplayed() && ApplyingSureLockSettingsLoadingIcon.isDisplayed()) {}
			try {
				if(AnerrorOccurredMsg.isDisplayed()) {
					ApplyingsurelockSettingRetryButton.click();
					while(ApplyingSureLockSetingsText.isDisplayed() && ApplyingSureLockSettingsLoadingIcon.isDisplayed()){}
					if(AnerrorOccurredMsg.isDisplayed()) {
						ApplyingsurelockSettingRetryButton.click();
						while(ApplyingSureLockSetingsText.isDisplayed() && ApplyingSureLockSettingsLoadingIcon.isDisplayed()){}
					}else {}
				}}catch (Exception e){}
			boolean bln=false;
			try {
				Initialization.driver.findElement(By.xpath("//span[text()='Settings applied successfully.']")).isDisplayed();
			}catch (Exception e) {
				bln=true;
			}
			ALib.AssertTrueMethod(bln, "PASS >> Settings applied successfully. is displayed",  "FAIL >> Settings applied successfully. is not displayed");
			waitForXpathPresent("(//h5[text()='Success'])[1]");
			sleep(4);
			boolean Actual=Successmsg.isDisplayed();
			String Pass="Sucess Message is displayed after Installation";
			String Fail="Sucess Message is not displayed after Installation";
			ALib.AssertTrueMethod (Actual, Pass, Fail);	
		}
		public void VerifyAllowingSureLockSettingPermission() throws InterruptedException {

			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Set SureLock As Default Launcher']")).click();
			sleep(2);
			Initialization.driverAppium.findElement(By.id("com.gears42.surelock:id/nextButton")).click();
			sleep(6);
			Initialization.driverAppium.findElement(By.id("android:id/button_always")).click();
			sleep(2);
			Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Continue']")).click();
			sleep(2);
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Configure Runtime Permissions']")).click();
			sleep(2);
			Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Continue']")).click();
			sleep(2);

		}
		public void LaunchSureLock() throws IOException, InterruptedException {
			Reporter.log("Launching SureLock On device",true);
			Runtime.getRuntime().exec("adb shell am start -n com.gears42.surelock/com.gears42.surelock.ClearDefaultsActivity");
			sleep(4);
		}

		public void VerifyAllowedAppsOnDevice() throws InterruptedException {

			boolean bln=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Camera' and @index='1']")).isDisplayed();
			String Pass="PASS >> 'Allowed app 'Camera' is present '";
			String fail="FAIL >> Allowe app is 'Camera' not present";
			ALib.AssertTrueMethod(bln, Pass, fail);

			boolean bln1=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Chrome' and @index='1']")).isDisplayed();
			String Pass1="PASS >> 'Allowed app 'Chrome' is present '";
			String fail1="FAIL >> Allowe app 'Chrome' is not present";
			ALib.AssertTrueMethod(bln1, Pass1, fail1);

			boolean bln2=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Contacts' and @index='1']")).isDisplayed();
			String Pass2="PASS >> 'Allowed app 'Contacts' is present '";
			String fail2="FAIL >> Allowe app 'Contacts' is not present";
			ALib.AssertTrueMethod(bln2, Pass2, fail2);

			boolean bln3=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Calculator' and @index='1']")).isDisplayed();
			String Pass3="PASS >> 'Allowed app 'Calculator' is present '";
			String fail3="FAIL >> Allowe app 'Calculator' is not present";
			ALib.AssertTrueMethod(bln3, Pass3, fail3);

		}
		public void ClickOnrefreshButtonDeviceInfo() throws InterruptedException {
			refreshdeviceinfo.click();
			sleep(2);
			while(Initialization.driver.findElement(By.xpath("(//div[@class='lds-ring1'])[1]")).isDisplayed()) {};
			sleep(2);
		}
		public void ClickOnSurelockSettings1() throws InterruptedException
		{
			SurelockSettingsBtn.click();
			while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
			{}
			waitForidPresent("AllowedApplications");
			sleep(8);
			Reporter.log("Clicked on SureLock Settings Job , Navigate successfully to surelock settings popup",true);
		}
		public void ClickOnrefreshSurelockSettings() throws InterruptedException {
			RefreshBtnSettings.click();
			sleep(4);
			while(Initialization.driver.findElement(By.xpath("//div[@class='load-bar']")).isDisplayed())
			{}
			waitForidPresent("AllowedApplications");
			sleep(8);

		}
		public void VerifyAppGotremovedAfterRefresh() throws Throwable {

			ClickOnAllowedApplication();
			boolean bln=false;
			try {
				Initialization.driver.findElement(By.xpath("//p[text()='SureMDM Nix']")).isDisplayed();
			}catch (Exception e) {
				bln=true;
			}
			ALib.AssertTrueMethod(bln, "Pass >> Allowed app is Removed After Refresh", "FAIL >> Allowed app is Not Removed After Refresh");

			boolean bln1=false;
			try {
				Initialization.driver.findElement(By.xpath("//p[@class='tit_line' and text()='vinod']")).isDisplayed();
			}catch (Exception e) {
				bln1=true;
			}
			ALib.AssertTrueMethod(bln1, "Pass >> Allowed Folder is Removed After Refresh", "FAIL >> Allowed Folder is Not Removed After Refresh");
			sleep(4);
			DoneButtonSureLockSettings.click();
			waitForidPresent("AllowedApplications");
			sleep(4);

		}
		public void VerifySuccefullyAppliedSettingsMsg() throws InterruptedException {
			boolean bln=true;
			try {
				Initialization.driver.findElement(By.xpath("//span[text()='Settings applied successfully.']")).isDisplayed();
			}catch (Exception e) {
				bln=false;
			}
			ALib.AssertTrueMethod(bln, "PASS >> Settings applied successfully.", "FAIL >> Settings applied successfully NOT displayed");
			sleep(5);
		}
		public void VerifyAppsAfterApplyingSettings() throws InterruptedException {

			boolean bln2=true;
			try {
				Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Chrome' and @index='1']").isDisplayed();
			}catch (Exception e) {
				bln2=false;
			}
			Assert.assertFalse(bln2, "PASS >>Allowed app Chrome is removed ");

			boolean bln3=true;
			try {
				Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Camera' and @index='1']").isDisplayed();
			}catch (Exception e) {
				bln3=false;
			}
			Assert.assertFalse(bln3, "PASS >>Allowed app Camera is removed ");

			boolean bln4=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Contacts' and @index='1']")).isDisplayed();
			String Pass2="PASS >> 'Allowed app 'Contacts' is present '";
			String fail2="FAIL >> Allowe app 'Contacts' is not present";
			ALib.AssertTrueMethod(bln4, Pass2, fail2);

			boolean bln5=Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Calculator' and @index='1']")).isDisplayed();
			String Pass3="PASS >> 'Allowed app 'Calculator' is present '";
			String fail3="FAIL >> Allowe app 'Calculator' is not present";
			ALib.AssertTrueMethod(bln5, Pass3, fail3);
		}
		public void  EnableHideIconRunAtStartUpAndRestartApp(String ApplicationName) throws Throwable
		{
			ClickOnAllowedApplication();
			Initialization.driver.findElement(By.xpath("//p[text()='"+ApplicationName+"']")).click();
			waitForidClickable("RemoveBtn");
			sleep(2);
			HideApplicationIconCheckBox.click();
			RunatStartUpCheckBox.click();
			LauchAppAfterRefreshCheckBox.click();
			DoneBtnRemoveApp.click();
			sleep(4);
			AllowedApplicationsDoneBtn.click();
		}
		public void SurelockSettingChanges() throws InterruptedException {
			SureLockSettingsHeading.click();
			waitForXpathPresent("//p[text()='Home Screen Settings']");
			sleep(2);
		}
		public void VerifyingSystemSettingPopUIandEnableAllCheckBoxes() throws InterruptedException {

			List<WebElement> ls=Initialization.driver.findElements(By.xpath("//*[@id='surelock_settings_pg']/div/ul/li/div/div[1]/p[1]"));
			List<WebElement> ls2=Initialization.driver.findElements(By.xpath("//*[@id='surelock_settings_pg']/div/ul/li/div/div[1]/p[2]"));

			JavascriptExecutor js=(JavascriptExecutor) Initialization.driver;
			for(int i=0;i<=ls.size()-1;i++) {

				if(i==0) {
					String Actual=ls.get(i).getText();
					String Expected="Wallpaper";
					String Pass="PASS >> 'wallpaper' is present";
					String fail="FAIL >> 'wallpaper' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls2.get(i).getText();
					String Expected1="Tap to configure wallpaper";
					String Pass1="PASS >> 'Tap to configure wallpaper' is present";
					String fail1="FAIL >> 'Tap to configure wallpaper' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
				}if(i==1) {
					String Actual=ls.get(i).getText();
					String Expected="Wallpaper Position";
					String Pass="PASS >> 'wallpaper Position' is present";
					String fail="FAIL >> 'wallpaper Position' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls2.get(i).getText();
					String Expected1="Center";
					String Pass1="PASS >> 'Center' is present";
					String fail1="FAIL >> 'Center' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
				}if(i==2) {
					String Actual=ls.get(i).getText();
					String Expected="Use System Wallpaper";
					String Pass="PASS >> 'Use System wallpaper' is present";
					String fail="FAIL >> 'Use System wallpaper' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls2.get(i).getText();
					String Expected1="Uses system wallpaper as SureLock wallpaper";
					String Pass1="PASS >> 'Uses system wallpaper as SureLock wallpaper' is present";
					String fail1="FAIL >> 'Uses system wallpaper as SureLock wallpaper' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
					boolean status=UseSystemWallpaperCheckBox.isSelected();
					String Pass2="PASS >> 'Uses system wallpaper as SureLock wallpaper' CheckBox is disabled";
					String fail2="FAIL >> 'Uses system wallpaper as SureLock wallpaper' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					UseSystemWallpaperCheckBox.click();

				}if(i==3) {
					String Actual=ls.get(i).getText();
					String Expected="Background Color";
					String Pass="PASS >> 'Background Color' is present";
					String fail="FAIL >> 'Background Color' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls2.get(i).getText();
					String Expected1="Use System Wallpaper should be disabled";
					String Pass1="PASS >> 'Use System Wallpaper should be disabled' is present";
					String fail1="FAIL >> 'Use System Wallpaper should be disabled' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
					boolean status=BackgroundColorCheckBox.isSelected();
					String Pass2="PASS >> 'BackgroundColorCheckBox' CheckBox is disabled";
					String fail2="FAIL >> 'BackgroundColorCheckBox' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);

				}if(i==4) {
					String Actual=ls.get(i).getText();
					String Expected="Row and Column Size";
					String Pass="PASS >> 'Row and Column Size' is present";
					String fail="FAIL >> 'Row and Column Size' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);

				}if(i==5) {
					String Actual=ls.get(i).getText();
					String Expected="Icon Size";
					String Pass="PASS >> 'Icon Size' is present";
					String fail="FAIL >> 'Icon Size' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls2.get(i).getText();
					String Expected1="Application's Icon Size : Medium (100%)";
					String Pass1="PASS >> 'Application's Icon Size : Medium (100%)' is present";
					String fail1="FAIL >> 'Application's Icon Size : Medium (100%)' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);	
				}if(i==6) {
					String Actual=ls.get(i).getText();
					String Expected="Font Size";
					String Pass="PASS >> 'Font Size' is present";
					String fail="FAIL >> 'Font Size' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls2.get(i).getText();
					String Expected1="Application's Font Size : Same as Icon Size";
					String Pass1="PASS >> 'Application's Font Size : Same as Icon Size' is present";
					String fail1="FAIL >> 'Application's Font Size : Same as Icon Size' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(7));

				}if(i==7) {
					String Actual=ls.get(i).getText();
					String Expected="Use Classic Calculation";
					String Pass="PASS >> 'Use Classic Calculation' is present";
					String fail="FAIL >> 'Use Classic Calculation' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=useclassicCalculationCheckBox.isSelected();
					String Pass2="PASS >> 'Uses classic CalculationCheckBox'  is disabled";
					String fail2="FAIL >> 'Uses classic CalculationCheckBox'  is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					useclassicCalculationCheckBox.click();
				}if(i==8) {
					String Actual=ls.get(i).getText();
					String Expected="Spacing between icons";
					String Pass="PASS >> 'Spacing between icons' is present";
					String fail="FAIL >> 'Spacing between icons' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==9) {
					String Actual=ls.get(i).getText();
					String Expected="Text color";
					String Pass="PASS >> 'Text color' is present";
					String fail="FAIL >> 'Text color' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==10) {
					String Actual=ls.get(i).getText();
					String Expected="Apps Order";
					String Pass="PASS >> 'Apps Order' is present";
					String fail="FAIL >> 'Apps Order' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==11) {
					String Actual=ls.get(i).getText();
					String Expected="Allow Icon Relocation";
					String Pass="PASS >> 'Allow Icon Relocation' is present";
					String fail="FAIL >> 'Allow Icon Relocation' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=AllowIconRelocationCheckBox.isSelected();
					String Pass2="PASS >> 'AllowIconRelocation CheckBox' is disabled";
					String fail2="FAIL >> 'AllowIconRelocation CheckBox' is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					AllowIconRelocationCheckBox.click();
				}if(i==12) {
					String Actual=ls.get(i).getText();
					String Expected="Detect Network Connection";
					String Pass="PASS >> 'Detect Network Connection' is present";
					String fail="FAIL >> 'Detect Network Connection' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=DetectNetworkSettingsCheckBox.isSelected();
					String Pass2="PASS >> 'DetectNetworkSettings CheckBox'  is disabled";
					String fail2="FAIL >> 'DetectNetworkSettings CheckBox'  is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					DetectNetworkSettingsCheckBox.click();
				}if(i==13) {
					String Actual=ls.get(i).getText();
					String Expected="Full Screen Mode";
					String Pass="PASS >> 'Full Screen Mode' is present";
					String fail="FAIL >> 'Full Screen Mode' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=full_screen_mode_settings_checkbox.isSelected();
					String Pass2="PASS >> 'full_screen_mode_settings CheckBox'  is disabled";
					String fail2="FAIL >> 'full_screen_mode_settings CheckBox' is not disabled";
					ALib.AssertTrueMethod(status, Pass2, fail2);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(13));
				}if(i==14) {
					String Actual=ls.get(i).getText();
					String Expected="Notification Badge";
					String Pass="PASS >> 'Notification Badge' is present";
					String fail="FAIL >> 'Notification Badge' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=notification_badge_settingscheckBox.isSelected();
					String Pass2="PASS >> 'notification_badge_settings CheckBox' CheckBox is disabled";
					String fail2="FAIL >> 'notification_badge_settings CeckBox' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					notification_badge_settingscheckBox.click();
				}if(i==15) {
					String Actual=ls.get(i).getText();
					String Expected="Hide Application Title";
					String Pass="PASS >> 'Hide Application Title' is present";
					String fail="FAIL >> 'Hide Application Title' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=hide_apptitle_settingsCheckBox.isSelected();
					String Pass2="PASS >> 'hide_apptitle_settings' CheckBox is disabled";
					String fail2="FAIL >> 'hide_apptitle_settings' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					hide_apptitle_settingsCheckBox.click();
				}if(i==16) {
					String Actual=ls.get(i).getText();
					String Expected="Floating Buttons Settings";
					String Pass="PASS >> 'Floating Buttons Settings' is present";
					String fail="FAIL >> 'Floating Buttons Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);

				}if(i==17) {
					String Actual=ls.get(i).getText();
					String Expected="Single Application Mode";
					String Pass="PASS >> 'Single Application Mode' is present";
					String fail="FAIL >> 'Single Application Mode' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls.get(i).getText();
					String Expected1="Single Application Mode";
					String Pass1="PASS >> 'Single Application Mode' is present";
					String fail1="FAIL >> 'Single Application Mode' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
				}if(i==18) {
					String Actual=ls.get(i).getText();
					String Expected="Change Password";
					String Pass="PASS >> 'Change Password' is present";
					String fail="FAIL >> 'Change Password' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(18));
				}if(i==19) {
					String Actual=ls.get(i).getText();
					String Expected="Admin Users";
					String Pass="PASS >> 'Admin Users' is present";
					String fail="FAIL >> 'Admin Users' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==20) {
					String Actual=ls.get(i).getText();
					String Expected="Multi-User Mode";
					String Pass="PASS >> 'Multi-User Mode' is present";
					String fail="FAIL >> 'Multi-User Mode' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=multi_user_modeCheckBox.isSelected();
					String Pass2="PASS >> 'multi_user_mode' CheckBox is disabled";
					String fail2="FAIL >> 'multi_user_mode' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
				}if(i==21) {
					String Actual=ls.get(i).getText();
					String Expected="Disable Bottom Bar";
					String Pass="PASS >> 'Disable Bottom Bar' is present";
					String fail="FAIL >> 'Disable Bottom Bar' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=disable_bottombar_settingCheckBox.isSelected();
					String Pass2="PASS >> 'disable_bottombar_setting' CheckBox is disabled";
					String fail2="FAIL >> 'disable_bottombar_setting' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					disable_bottombar_settingCheckBox.click();
					sleep(1);
					OkBtnDisableBottombarPopUp.click();
					sleep(1);
				}if(i==22) {
					String Actual=ls.get(i).getText();
					String Expected="Hide Bottom Bar";
					String Pass="PASS >> 'Hide Bottom Bar' is present";
					String fail="FAIL >> 'Hide Bottom Bar' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=hide_bottom_barCheckBox.isSelected();
					String Pass2="PASS >> 'hide_bottom_bar_setting' CheckBox is disabled";
					String fail2="FAIL >> 'hide_bottom_bar_setting' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
				}if(i==23) {
					String Actual=ls.get(i).getText();
					String Expected="Disable Hardware Keys";
					String Pass="PASS >> 'Disable Hardware Keys' is present";
					String fail="FAIL >> 'Disable Hardware Keys' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==24) {
					String Actual=ls.get(i).getText();
					String Expected="Disable Soft Navigation Keys";
					String Pass="PASS >> 'Disable Soft Navigation Keys' is present";
					String fail="FAIL >> 'Disable Soft Navigation Keys' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(24));
				}if(i==25) {
					String Actual=ls.get(i).getText();
					String Expected="Camera Settings";
					String Pass="PASS >> 'Camera Settings' is present";
					String fail="FAIL >> 'Camera Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);	
				}if(i==26) {
					String Actual=ls.get(i).getText();
					String Expected="Wifi Settings";
					String Pass="PASS >> 'Wifi Settings' is present";
					String fail="FAIL >> 'Wifi Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==27) {
					String Actual=ls.get(i).getText();
					String Expected="Mobile Data Settings";
					String Pass="PASS >> 'Mobile Data Settings' is present";
					String fail="FAIL >> 'Mobile Data Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(27));
				}if(i==28) {
					String Actual=ls.get(i).getText();
					String Expected="Prefer Mobile Data";
					String Pass="PASS >> 'Prefer Mobile Data' is present";
					String fail="FAIL >> 'Prefer Mobile Data' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					PrefereMObdataCheckBox.click();
				}if(i==29) {
					String Actual=ls.get(i).getText();
					String Expected="Wi-Fi Hotspot Settings";
					String Pass="PASS >> 'Wi-Fi Hotspot Settings' is present";
					String fail="FAIL >> 'Wi-Fi Hotspot Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==30) {
					String Actual=ls.get(i).getText();
					String Expected="GPS Settings";
					String Pass="PASS >> 'GPS Settings' is present";
					String fail="FAIL >> 'GPS Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==31) {
					String Actual=ls.get(i).getText();
					String Expected="Bluetooth Settings";
					String Pass="PASS >> 'Bluetooth Settings' is present";
					String fail="FAIL >> 'Bluetooth Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(31));
				}if(i==32) {
					String Actual=ls.get(i).getText();
					String Expected="Sound Settings";
					String Pass="PASS >> 'Sound Settings' is present";
					String fail="FAIL >> 'Sound Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==33) {
					String Actual=ls.get(i).getText();
					String Expected="Loudspeaker Settings";
					String Pass="PASS >> 'Loudspeaker Settings' is present";
					String fail="FAIL >> 'Loudspeaker Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==34) {
					String Actual=ls.get(i).getText();
					String Expected="Volume Settings";
					String Pass="PASS >> 'Volume Settings' is present";
					String fail="FAIL >> 'Volume Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					VolumeSettingsCheckBox.click();
					sleep(3);
					OkDisableBottomBarBtn.click();
					sleep(1);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(34));
				}if(i==35) {
					String Actual=ls.get(i).getText();
					String Expected="Airplane Mode Settings";
					String Pass="PASS >> 'Airplane Mode Settings' is present";
					String fail="FAIL >> 'Airplane Mode Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==36) {
					String Actual=ls.get(i).getText();
					String Expected="SureLock Home Screen Orientation";
					String Pass="PASS >> 'SureLock Home Screen Orientation' is present";
					String fail="FAIL >> 'SureLock Home Screen Orientation' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==37) {
					String Actual=ls.get(i).getText();
					String Expected="Rotation Settings";
					String Pass="PASS >> 'Rotation Settings' is present";
					String fail="FAIL >> 'Rotation Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==38) {
					String Actual=ls.get(i).getText();
					String Expected="Brightness Settings";
					String Pass="PASS >> 'Brightness Settings' is present";
					String fail="FAIL >> 'Brightness Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(38));
				}if(i==39) {
					String Actual=ls.get(i).getText();
					String Expected="Set Custom Toast Message";
					String Pass="PASS >> 'Set Custom Toast Message' is present";
					String fail="FAIL >> 'Set Custom Toast Message' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==40) {
					String Actual=ls.get(i).getText();
					String Expected="Enable Toast Message";
					String Pass="PASS >> 'Enable Toast Message' is present";
					String fail="FAIL >> 'Enable Toast Message' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==41) {
					String Actual=ls.get(i).getText();
					String Expected="Set Custom 'Access Denied Message'";
					String Pass="PASS >> 'Set Custom 'Access Denied Message'' is present";
					String fail="FAIL >> 'Set Custom 'Access Denied Message'' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==42) {
					String Actual=ls.get(i).getText();
					String Expected="Watchdog Service";
					String Pass="PASS >> 'Watchdog Service' is present";
					String fail="FAIL >> 'Watchdog Service' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(42));
				}if(i==43) {
					String Actual=ls.get(i).getText();
					String Expected="Kill Unallowed Application";
					String Pass="PASS >> 'Kill Unallowed Application' is present";
					String fail="FAIL >> 'Kill Unallowed Application' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==44) {
					String Actual=ls.get(i).getText();
					String Expected="Lock Safe Mode";
					String Pass="PASS >> 'Lock Safe Mode' is present";
					String fail="FAIL >> 'Lock Safe Mode' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==45) {
					String Actual=ls.get(i).getText();
					String Expected="On Launch of Unallowed Application";
					String Pass="PASS >> 'On Launch of Unallowed Application' is present";
					String fail="FAIL >> 'On Launch of Unallowed Application' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(44));
				}if(i==46) {
					String Actual=ls.get(i).getText();
					String Expected="On USB state change go to Home";
					String Pass="PASS >> 'On USB state change go to Home' is present";
					String fail="FAIL >> 'On USB state change go to Home' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					UsbStateHomeCheckBox.click();

				}if(i==47) {
					String Actual=ls.get(i).getText();
					String Expected="Clear Data On Home Screen Load";
					String Pass="PASS >> 'Clear Data On Home Screen Load' is present";
					String fail="FAIL >> 'Clear Data On Home Screen Load' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==48) {
					String Actual=ls.get(i).getText();
					String Expected="Suppress Notification Panel";
					String Pass="PASS >> 'Suppress Notification Panel' is present";
					String fail="FAIL >> 'Suppress Notification Panel' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					//suppress_notification_panelCheckBox.click();
					js.executeScript("arguments[0].scrollIntoView();",ls.get(48));

				}if(i==49) {
					String Actual=ls.get(i).getText();
					String Expected="Hide Quick Settings Tiles";
					String Pass="PASS >> 'Hide Quick Settings Tiles' is present";
					String fail="FAIL >> 'Hide Quick Settings Tiles' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==50) {
					String Actual=ls.get(i).getText();
					String Expected="Block Notifications";
					String Pass="PASS >> 'Block Notifications' is present";
					String fail="FAIL >> 'Block Notifications' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					BlockNotifcationCheckBox.click();
				}if(i==51) {
					String Actual=ls.get(i).getText();
					String Expected="Disable Status Bar";
					String Pass="PASS >> 'Disable Status Bar' is present";
					String fail="FAIL >> 'Disable Status Bar' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);

				}if(i==52) {
					String Actual=ls.get(i).getText();
					String Expected="Status Bar Action";
					String Pass="PASS >> 'Status Bar Action' is present";
					String fail="FAIL >> 'Status Bar Action' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==53) {
					String Actual=ls.get(i).getText();
					String Expected="Disable Factory Reset";
					String Pass="PASS >> 'Disable Factory Reset' is present";
					String fail="FAIL >> 'Disable Factory Reset' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==54) {
					String Actual=ls.get(i).getText();
					String Expected="Disable USB";
					String Pass="PASS >> 'Disable USB' is present";
					String fail="FAIL >> 'Disable USB' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(53));
				}if(i==55) {
					String Actual=ls.get(i).getText();
					String Expected="Disable OTG/External SD Card";
					String Pass="PASS >> 'Disable OTG/External SD Card' is present";
					String fail="FAIL >> 'Disable OTG/External SD Card' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==56) {
					String Actual=ls.get(i).getText();
					String Expected="Suppress Power Button/Keyboard";
					String Pass="PASS >> 'Suppress Power Button/Keyboard' is present";
					String fail="FAIL >> 'Suppress Power Button/Keyboard' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					SuppressPowerCheckBox.click();
				}if(i==57) {
					String Actual=ls.get(i).getText();
					String Expected="Keyboard Settings";
					String Pass="PASS >> 'Keyboard Settings' is present";
					String fail="FAIL >> 'Keyboard Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					EnableKeboardCheckBox.click();
				}if(i==58) {
					String Actual=ls.get(i).getText();
					String Expected="Driver Safety";
					String Pass="PASS >> ' Driver Safety' is present";
					String fail="FAIL >> ' Driver Safety' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==59) {
					String Actual=ls.get(i).getText();
					String Expected="Bootup Delay";
					String Pass="PASS >> 'Bootup Delay' is present";
					String fail="FAIL >> Bootup Delay' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==60) {
					String Actual=ls.get(i).getText();
					String Expected="Timeout Settings";
					String Pass="PASS >> 'Timeout Settings' is present";
					String fail="FAIL >> 'Timeout Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(59));
				}if(i==61) {
					String Actual=ls.get(i).getText();
					String Expected="Schedule Reboot";
					String Pass="PASS >> 'Schedule Reboot' is present";
					String fail="FAIL >> 'Schedule Reboot' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==62) {
					String Actual=ls.get(i).getText();
					String Expected="Widget Settings";
					String Pass="PASS >> 'Widget Settings' is present";
					String fail="FAIL >> 'Widget Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==63) {
					String Actual=ls.get(i).getText();
					String Expected="Title Bar Settings";
					String Pass="PASS >> 'Title Bar Settings' is present";
					String fail="FAIL >> 'Title Bar Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==64) {
					String Actual=ls.get(i).getText();
					String Expected="Status Bar Color";
					String Pass="PASS >> 'Status Bar Color' is present";
					String fail="FAIL >> 'Status Bar Color' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==65) {
					String Actual=ls.get(i).getText();
					String Expected="SureLock Analytics";
					String Pass="PASS >> 'SureLock Analytics' is present";
					String fail="FAIL >> 'SureLock Analytics' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==66) {
					String Actual=ls.get(i).getText();
					String Expected="Power Saving Settings";
					String Pass="PASS >> 'Power Saving Settings' is present";
					String fail="FAIL >> 'Power Saving Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(64));
				}if(i==67) {
					String Actual=ls.get(i).getText();
					String Expected="Battery Popup Notification";
					String Pass="PASS >> 'Battery Popup Notification' is present";
					String fail="FAIL >> 'Battery Popup Notification' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==68) {
					String Actual=ls.get(i).getText();
					String Expected="Miscellaneous Settings";
					String Pass="PASS >> 'Miscellaneous Settings' is present";
					String fail="FAIL >> 'Miscellaneous Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==69) {
					String Actual=ls.get(i).getText();
					String Expected="Memory Settings";
					String Pass="PASS >> 'Memory Settings' is present";
					String fail="FAIL >> 'Memory Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}if(i==70) {
					String Actual=ls.get(i).getText();
					String Expected="Screensaver Settings";
					String Pass="PASS >> 'Screensaver Settings' is present";
					String fail="FAIL >> 'Screensaver Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
				}
			}

			DoneButtonSureLockSettings.click();
			waitForXpathPresent("//li[@id='AllowedApplications']/div/div/p[@class='tit_line']");
			sleep(2);

		}
		public void VerifyOfPhoneSettingsUIAndEnableAllCheckBox() throws InterruptedException {
			PhoneSettingsHeading.click();
			System.out.println("Clicked On Phone Settings SureLockSettings");
			waitForXpathPresent("//span[text()='Configure Phone Settings']");
			sleep(1);
			List<WebElement> ls=Initialization.driver.findElements(By.xpath("//p[@class='tit_line']"));
			List<WebElement> ls1=Initialization.driver.findElements(By.xpath("//p[@class='desc_line']"));
			JavascriptExecutor js=(JavascriptExecutor) Initialization.driver;
			for(int i=0;i<=ls.size();i++) {

				if(i==0) {
					String Actual=ls.get(i).getText();
					String Expected= "Call Settings";
					String Pass="PASS >> 'Call Settings' is present";
					String fail="FAIL >> 'Call Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Bring call progress on phone launch";
					String Pass1="PASS >> 'Bring call progress on phone launch' is present";
					String fail1="FAIL >> 'Bring call progress on phone launch' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);

				}if(i==1) {
					String Actual=ls.get(i).getText();
					String Expected="Call Progress Screen";
					String Pass="PASS >> 'Call Progress Screen' is present";
					String fail="FAIL >> 'Call Progress Screen' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Check to block all incoming call";
					String Pass1="PASS >> 'Check to block all incoming call' is present";
					String fail1="FAIL >> 'Check to block all incoming call' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
					boolean status=ShowCallInscreenCheckBox.isSelected();
					String Pass2="PASS >> 'ShowCallInscreen' CheckBox is disabled";
					String fail2="FAIL >> 'ShowCallInscreen' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					ShowCallInscreenCheckBox.click();
				}if(i==2) {
					String Actual=ls.get(i).getText();
					String Expected="Block All Incoming Call";
					String Pass="PASS >> 'Block All Incoming Call' is present";
					String fail="FAIL >> 'Block All Incoming Call' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Check to block all outgoing call";
					String Pass1="PASS >> 'Check to block all outgoing call' is present";
					String fail1="FAIL >> 'Check to block all outgoing call' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
					boolean status=blockIncomingCallsCheckBox.isSelected();
					String Pass2="PASS >> 'blockIncomingCalls' CheckBox is disabled";
					String fail2="FAIL >> 'blockIncomingCalls' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					blockIncomingCallsCheckBox.click();
				}if(i==3) {
					String Actual=ls.get(i).getText();
					String Expected="Block All Outgoing Call";
					String Pass="PASS >> 'Block All Outgoing Call' is present";
					String fail="FAIL >> 'Block All Outgoing Call' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Configure Blacklisted Phone Numbers";
					String Pass1="PASS >> 'Configure Blacklisted Phone Numbers' is present";
					String fail1="FAIL >> 'Configure Blacklisted Phone Numbers' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
					boolean status=whitelistNewContactsCheckBox.isSelected();
					String Pass2="PASS >> 'whitelistNewContacts' CheckBox is disabled";
					String fail2="FAIL >> 'whitelistNewContacts' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					blockOutgoingCallsCheckBox.click();

				}if(i==4) {
					String Actual=ls.get(i).getText();
					String Expected="Automatically whitelist new contact numbers";
					String Pass="PASS >> 'Automatically whitelist new contact numbers' is present";
					String fail="FAIL >> 'Automatically whitelist new contact numbers' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Check to block all incoming SMS";
					String Pass1="PASS >> 'Check to block all incoming SMS' is present";
					String fail1="FAIL >> 'Check to block all incoming SMS' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);

				}if(i==5) {
					String Actual=ls.get(i).getText();
					String Expected="Blacklisted Phone Numbers";
					String Pass="PASS >> 'Blacklisted Phone Numbers' is present";
					String fail="FAIL >> Blacklisted Phone Numbers' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					js.executeScript("arguments[0].scrollIntoView();",ls.get(5));
				}if(i==6) {
					String Actual=ls.get(i).getText();
					String Expected="Whitelisted Phone Numbers";
					String Pass="PASS >> 'Whitelisted Phone Numbers' is present";
					String fail="FAIL >> 'Whitelisted Phone Numbers' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Check to block all incoming MMS";
					String Pass1="PASS >> 'Check to block all incoming MMS' is present";
					String fail1="FAIL >> 'Check to block all incoming MMS' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);

				}if(i==7) {
					String Actual=ls.get(i).getText();
					String Expected="SMS Settings";
					String Pass="PASS >> 'SMS Settings' is present";
					String fail="FAIL >> 'SMS Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Check to block all outgoing MMS";
					String Pass1="PASS >> 'Check to block all outgoing MMS' is present";
					String fail1="FAIL >> 'Check to block all outgoing MMS' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);

				}if(i==8) {
					String Actual=ls.get(i).getText();
					String Expected="Block All Incoming SMS";
					String Pass="PASS >> 'Block All Incoming SMS' is present";
					String fail="FAIL >> 'Block All Incoming SMS' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					String Actual1=ls1.get(i).getText();
					String Expected1="Check to enable SureLock specific SMS commands";
					String Pass1="PASS >> 'Check to enable SureLock specific SMS commands' is present";
					String fail1="FAIL >> 'Check to enable SureLock specific SMS commands' is not present";
					ALib.AssertEqualsMethod(Expected1, Actual1, Pass1, fail1);
					boolean status=blockIncomingSMSCheckBox.isSelected();
					String Pass2="PASS >> 'blockIncomingSMS' CheckBox is disabled";
					String fail2="FAIL >> 'blockIncomingSMS' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					blockIncomingSMSCheckBox.click();
				}if(i==9) {
					String Actual=ls.get(i).getText();
					String Expected="Block All Outgoing SMS";
					String Pass="PASS >> 'Block All Outgoing SMS' is present";
					String fail="FAIL >> 'Block All Outgoing SMS' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=blockOutgoingSMSCheckBox.isSelected();
					String Pass2="PASS >> 'blockOutgoingSMS' CheckBox is disabled";
					String fail2="FAIL >> 'blockOutgoingSMS' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					blockOutgoingSMSCheckBox.click();
				}if(i==10) {
					String Actual=ls.get(i).getText();
					String Expected="Block All Incoming MMS";
					String Pass="PASS >> 'Block All Incoming MMS' is present";
					String fail="FAIL >> 'Block All Incoming MMS' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=blockIncomingMMSCheckBox.isSelected();
					String Pass2="PASS >> 'blockIncomingMMS' CheckBox is disabled";
					String fail2="FAIL >> 'blockIncomingMMS' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					blockIncomingMMSCheckBox.click();
				}if(i==11) {
					String Actual=ls.get(i).getText();
					String Expected="Block All Outgoing MMS";
					String Pass="PASS >> 'Block All Outgoing MMS' is present";
					String fail="FAIL >> 'Block All Outgoing MMS' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=blockOutgoingMMSCheckBox.isSelected();
					String Pass2="PASS >> 'blockOutgoingMMS' CheckBox is disabled";
					String fail2="FAIL >> 'blockOutgoingMMS' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					blockOutgoingMMSCheckBox.click();

				}if(i==12) {
					String Actual=ls.get(i).getText();
					String Expected="SMS Command Settings";
					String Pass="PASS >> 'SMS Command Settings' is present";
					String fail="FAIL >> 'SMS Command Settings' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);

				}if(i==13) {
					String Actual=ls.get(i).getText();
					String Expected="Enable SMS Command";
					String Pass="PASS >> 'Enable SMS Command' is present";
					String fail="FAIL >> 'Enable SMS Command' is not present";
					ALib.AssertEqualsMethod(Expected, Actual, Pass, fail);
					boolean status=enablesmscommandCheckBox.isSelected();
					String Pass2="PASS >> 'enablesmscommand' CheckBox is disabled";
					String fail2="FAIL >> 'enablesmscommand' CheckBox is not disabled";
					ALib.AssertFalseMethod(status, Pass2, fail2);
					enablesmscommandCheckBox.click();
				}}
			DoneButtonSureLockSettings.click();
			waitForidPresent("AllowedApplications");
			sleep(2);
		}

		public void VerifyAllowedSettingsOnDevice() throws InterruptedException {
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='SureLock Settings' and @index='0']")).click();
			sleep(1);
			List<WebElement> ls=Initialization.driverAppium.findElements(By.id("android:id/checkbox"));
			boolean bln=ls.get(0).isEnabled();
			String pass="PASS >> 'Use System Wallpaper' is Enabled";
			String fail="FAIL >> 'Use System Wallpaper' is not Enabled";
			ALib.AssertTrueMethod(bln, pass, fail);
			common.commonScrollMethod("Use Classic Calculation");
			boolean bln1=ls.get(0).isEnabled();
			String pass1="PASS >> 'Use Classic Calculation' is Enabled";
			String fail1="FAIL >> 'Use Classic Calculation' is not Enabled";
			ALib.AssertTrueMethod(bln1, pass1, fail1);
			boolean bln2=ls.get(0).isEnabled();
			String pass2="PASS >> 'Allow Icon Relocation' is Enabled";
			String fail2="FAIL >> 'Allow Icon Relocation' is not Enabled";
			ALib.AssertTrueMethod(bln2, pass2, fail2);
		}

		public void OpenSureLockSettings() throws IOException, InterruptedException {
			Runtime.getRuntime().exec("adb shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e \\\"command\\\" \\\"open_admin_settings\\\" -e \\\"password\\\" \\\"0000\\\"");
			sleep(2);
		}
		public void VeifyAllowedAppSettingsOnSureLock() throws InterruptedException, IOException {
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Allowed Applications' and @index='0']")).click();
			sleep(1);
			Initialization.driverAppium.findElement(By.id("com.gears42.surelock:id/instruction_txt")).click();
			sleep(1);
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Calculator' and @index='0']")).click();
			sleep(1);
			Runtime.getRuntime().exec("adb shell input keyevent 4");
			sleep(2);
			boolean bln=Initialization.driverAppium.findElement(By.id("com.gears42.surelock:id/cbHideIcon")).isDisplayed();
			String Pass3="PASS >> 'Hide Application Icon' is Enabled";
			String fail3="FAIL >> 'Hide Application Icon' is not Enabled";
			ALib.AssertTrueMethod(bln, Pass3, fail3);
			boolean bln1=Initialization.driverAppium.findElement(By.id("com.gears42.surelock:id/cbLaunchAtBoot")).isDisplayed();
			String Pass2="PASS >> 'Launch at reboot' is Enabled";
			String fail2="FAIL >> 'Launch at reboot' is not Enabled";
			ALib.AssertTrueMethod(bln1, Pass2, fail2);
			boolean bln3=Initialization.driverAppium.findElement(By.id("com.gears42.surelock:id/cbFreshLaunch")).isDisplayed();
			String Pass4="PASS >> 'FreshLaunch' is Enabled";
			String fail4="FAIL >> 'FreshLaunch' is not Enabled";
			ALib.AssertTrueMethod(bln3, Pass4, fail4);
			sleep(1);
			Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Done' and @index='0']")).click();
			sleep(1);
			Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Done' and @index='2']")).click();
			sleep(1);
		}
		public void ClickOnDoneBtn() throws InterruptedException {
			Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Done' and @index='1']")).click();
			sleep(1);
		}
		public void VerifyDetectNetworkConnectionFullScreenModeAndHideAppTitleareEnabled() throws Exception {
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='SureLock Settings' and @index='0']")).click();
			sleep(1);
			common.commonScrollMethod("Detect Network Connection");
			List<WebElement> ls=Initialization.driverAppium.findElements(By.id("android:id/checkbox"));
			boolean bln=ls.get(1).isEnabled();
			String pass="PASS >> 'Detect Network Connection' is Enabled";
			String fail="FAIL >> 'Detect Network Connection' is not Enabled";
			ALib.AssertTrueMethod(bln, pass, fail);
			boolean bln1=ls.get(2).isEnabled();
			String pass1="PASS >> 'Full Screen Mode' is Enabled";
			String fail1="FAIL >> 'Full Screen Mode' is not Enabled";
			ALib.AssertTrueMethod(bln1, pass1, fail1);
			boolean bln2=ls.get(4).isEnabled();
			String pass2="PASS >> 'Hide App Title' is Enabled";
			String fail2="FAIL >> 'Hide App Title' is not Enabled";
			ALib.AssertTrueMethod(bln2, pass2, fail2); 
		}

		public void VerifyPreferMobDataAndVolumeSettingareEnabled() throws InterruptedException {
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='SureLock Settings' and @index='0']")).click();
			sleep(1);
			common.commonScrollMethod("WiFi Hotspot Settings");
			List<WebElement> ls=Initialization.driverAppium.findElements(By.id("android:id/checkbox"));
			boolean bln=ls.get(0).isEnabled();
			String pass="PASS >> 'Prefer Mobile Data' is Enabled";
			String fail="FAIL >> 'Prefer Mobile Data' is not Enabled";
			ALib.AssertTrueMethod(bln, pass, fail);
			common.commonScrollMethod("Volume Settings");
			boolean bln1=ls.get(0).isEnabled();
			String pass1="PASS >> 'Volume Settings' is Enabled";
			String fail1="FAIL >> 'Volume Settings' is not Enabled";
			ALib.AssertTrueMethod(bln1, pass1, fail1);

		}
		public void VerifyOnUSBstateandSuppressNotificationPanelEnabled() throws Exception {
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='SureLock Settings' and @index='0']")).click();
			sleep(1);
			common.commonScrollMethod("On USB State Change go to Home");
			List<WebElement> ls=Initialization.driverAppium.findElements(By.id("android:id/checkbox"));
			boolean bln=ls.get(2).isEnabled();
			String pass="PASS >> 'On USB State Change go to Home' is Enabled";
			String fail="FAIL >> 'On USB State Change go to Home' is not Enabled";
			ALib.AssertTrueMethod(bln, pass, fail);
			boolean bln1=ls.get(4).isEnabled();
			String pass1="PASS >> 'Suppress Notification Panel' is Enabled";
			String fail1="FAIL >> 'Suppress Notification Panel' is not Enabled";
			ALib.AssertTrueMethod(bln1, pass1, fail1);
		}

		public void ClickOnChangeDeviceNameInNix() throws InterruptedException
		{
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Change Device Name']").click();
			sleep(3);
		}

		boolean val;
		public void VerifyNameChangeInConsole()
		{

			String DeviceNameInConsole = Initialization.driver.findElement(By.xpath("//td[@class='DeviceName']")).getText();
			System.out.println(DeviceNameInConsole);
			if(DeviceNameInConsole.equals(Config.Enrolled_DeviceName))
			{
				val=true;
			}
			else if(DeviceNameInConsole.equals(Config.DeviceName))
			{
				Reporter.log("DeviceName Has Not Changed After Changing From Nix",true);
				val=false;
			}
			String Pass="Device Name Reflected Correctly After Changing From Nix";
			String Fail="Device Naem Is Not Reflected After Changing From Nix";
			ALib.AssertTrueMethod(val, Pass, Fail);
		}
		
		
		@FindBy(xpath="//div[@id='MTP_systemScan']")
		private WebElement SystemScanButton;
		public void ClickOnSystemScanButton() throws InterruptedException
		{
			SystemScanButton.click();
			sleep(2);
		}
		@FindBy(xpath="//div[@id='MTP_system_scan']/div[1]/button")
		private WebElement CloseSystemScan;
		public void CloseSystemScanWindow() throws InterruptedException
		{
			CloseSystemScan.click();
			sleep(2);
		}
		
		@FindBy(xpath="(//span[text()='Edit'])[2]")
		private WebElement EditAllowedApplication;
		public void ClickOnEdit() throws InterruptedException {
			
			EditAllowedApplication.click();
			Reporter.log("Clicked On Edit Btn",true);
			sleep(2);
		}

		
		@FindBy(xpath="//*[@id='folder_settings_pg']/footer/div[2]/button[2][contains(text(),'Remove')]")
		private WebElement RemoveBtn;
					public void RemoveFolder(String FolderName) throws Throwable
					{
						ClickOnAllowedApplication();
						
						ClickOnEdit();
						RemoveBtn.click();
						waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/button[text()='Yes']");
						sleep(3);
						RemoveApplicationYesBtn.click();
						waitForidPresent("add_app");
						sleep(2);
						boolean value=true;
						try{
							Initialization.driver.findElement(By.xpath("//p[text()='"+FolderName+"']")).isDisplayed();
						}catch(Exception e)
						{
							value=false;
						}
						String PassStatement="PASS >> FolderName is removed successfuly from Allowed Application";
						String FailStatement="Fail >> FolderName is not removed from Allowed Application";
						ALib.AssertFalseMethod(value, PassStatement, FailStatement);
						AllowedApplicationsDoneBtn.click();
					}

					public void ClickOnMessageOnSureVideoInstallation() throws InterruptedException
					{
						try {
							waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[1]/p");
							sleep(4);
							ClickOnYesButtonSureVideoInstallation();
							sleep(2);
							Reporter.log("PASS>> Clicking On YesButtonSureVideoInstallation", true);
						} catch (Exception e) {
		
							Reporter.log("PASS >> Navigated To SV Settings", true);
						}
					}
					
					
					@FindBy(xpath="//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
					private WebElement SearchJobField;

					public void SearchJobInSearchField(String JobToBeSearched) throws InterruptedException
					{
						SearchJobField.clear();
						sleep(2);
						SearchJobField.sendKeys(JobToBeSearched);
						waitForXpathPresent("//p[text()='"+JobToBeSearched+"']");
						sleep(3);
						Reporter.log("Job Searched Successfully",true);
					}
				
					
	@FindBy(xpath="//*[@id='confidBillCycle']")		
	private WebElement confidBillCycle;
	public void BillingCycle() throws InterruptedException {
		
		Select selCallLogstatus = new Select(confidBillCycle);
		selCallLogstatus.selectByVisibleText("Daily");
		sleep(2);

		
	}
					
					
					
					
	public void VerifyLowerVersionOfApp(String Version) throws InterruptedException {
		
		String GetVersionOfApp=Initialization.driver.findElement(By.xpath("//*[@id='downlodedAppListTable']/tbody/tr/td[contains(text(),'"+Version+"')]")).getText();
		System.out.println(GetVersionOfApp);
		String ExpectedVersion=Version;
		String pass="PASS>> Cersion "+ExpectedVersion+" is correct";
		String fail="FAIL>> Cersion "+ExpectedVersion+" is NOT correct";
		ALib.AssertEqualsMethod(GetVersionOfApp, ExpectedVersion, pass, fail);
		sleep(2);

		
	}
	public void CloseDynamicApp() throws InterruptedException {
		
		Initialization.driver.findElement(By.xpath("//*[@id='applistDetailsAndroid']/div[1]/button")).click();
		sleep(2);
		
	}

	public void VerifyUpgradedVersionOfApp(String LowerVersion,String UpgradedVersion) throws InterruptedException {
		String GetHigherVersionOfApp=Initialization.driver.findElement(By.xpath("//*[@id='downlodedAppListTable']/tbody/tr/td[contains(text(),'"+UpgradedVersion+"')]")).getText();
		System.out.println(GetHigherVersionOfApp);

		if(!GetHigherVersionOfApp.equals(LowerVersion)) 
		{
			
			Reporter.log("Upgrade is done",true);
			
			
			}
		else
		{
			
			ALib.AssertFailMethod("Upgrade is Not done");
			
		}
//		String ExpectedVersion=Version;
//		String pass="PASS>> Cersion "+ExpectedVersion+" is correct";
//		String fail="FAIL>> Cersion "+ExpectedVersion+" is NOT correct";
//		ALib.AssertEqualsMethod(GetVersionOfApp, ExpectedVersion, pass, fail);
//		sleep(2);

		
	}
			
					
	 public void UnLockingDeviceUsingADBSecurityPolicyJob() throws IOException, InterruptedException
     {
		 sleep(30);
             Runtime.getRuntime().exec("adb shell input keyevent 26");
             sleep(1);
             Runtime.getRuntime().exec("adb shell input keyevent 82");
             sleep(1);
             Runtime.getRuntime().exec("adb shell input text 0000");
             sleep(1);
             Runtime.getRuntime().exec("adb shell input keyevent 66");
             sleep(1);
     }
		
	//Madhu
	 public void ClickOnDeleteDeviceYesBtn() throws InterruptedException
		{
			RebootLockWipeBuzzDeviceYesBtn.click();
			Reporter.log("Clicked on Yes button of Reboot Job popup",true);
			waitForXpathPresent("//span[contains(text(),'Device(s) deleted successfully.')]");
		}	
   }


