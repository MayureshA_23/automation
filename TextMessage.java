package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class TextMessage extends Initialization  {
	
	@Test(priority=0, description="Verify error message while creating Text Message Job with all fields are empty.")
	public void CreateTextMessageJobWithAllFieldsEmpty() throws InterruptedException, IOException{

	    androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.ClickOnOkButton();
		androidJOB.JobNameCannotBeEmptyMessage();
		androidJOB.ClickOnOkButton();
		androidJOB.VerifyMessageBodyEmpty();
		
	}
	@Test(priority=1, description="Creating text message job when none of the fields are empty")
	public void createtextMessageJobWithoutSelectingAnyRadioButton() throws InterruptedException, IOException{

		androidJOB.textMessageJobWhenNoFieldIsEmpty("0TextMessage", "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
		
	@Test(priority=2, description="Text Message - Verify creating text message job")
	public void VerifyTextMessageJob() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("TextVerifyJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("TextVerifyJob"); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob("TextVerifyJob",360); 
		deviceinfopanelpage.ClickOnBackButton();
    	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxNix();
	   Reporter.log("Pass >>Text Message - Verify creating text message job",true); 
	}

	@Test(priority=3, description="Text Message - Verify Get Read Notification")
	public void VerifyTextJobReadNotification() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.textMessageJobWhenNoFieldIsEmpty("TextJobReadNotification","TextJobReadNotification","notify Text");
		androidJOB.ClickOnGetReadNotification();
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("TextJobReadNotification"); 
		androidJOB.CheckStatusOfappliedInstalledJob("TextJobReadNotification",360); 
		androidJOB.ClickOnMailBoxNix();
		androidJOB.verifyReadMessage("Subject: TextJobReadNotification");

//		androidJOB.ReadNotification("TextJobReadNotification");
//		androidJOB.ClickOnInbox();
//		androidJOB.verifyReadNotificationInConsole("TextJobReadNotification");
		Reporter.log("Pass >>Text Message - Verify Get Read Notification",true); 
	}


	@Test(priority=4, description="Text Message - Verify Force read message")
	public void VerifyTextJobForceRead() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("ForceRead","ForceRead","notify Text");
		androidJOB.ClickOnForceReadMessage();
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ForceRead"); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob("ForceRead",360); 
		androidJOB.verifyForceReadMessage("ForceRead");
		Reporter.log("Pass >>Text Message - Verify Force read message",true); 
	}

	@Test(priority=5, description="Text Message - verify Force read message by enabling Buzz.")
	public void VerifyTextJobBuzz() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.ClickOnForceReadMessage();
		androidJOB.ClickOnEnableBuzz("5");
		androidJOB.textMessageJobWhenNoFieldIsEmpty("ForceBuzz","ForceBuzz","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ForceBuzz"); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob("ForceBuzz",360); 
		androidJOB.verifyForceReadMessage("ForceBuzz");
		Reporter.log("Pass >>Text Message - verify Force read message by enabling Buzz.",true); 
	}

	@Test(priority=6, description="Text Message - Verify Force read message by enabling Close message Pop-up.")
	public void VerifyTextJobBuzzClosePopUp() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("ForceBuzzClosePOPUp","ClosePopUp","notify Text");
		androidJOB.ClickOnForceReadMessage();
		androidJOB.ClickOnEnableBuzz("60");
		androidJOB.ClickOnEnableCloseMessage("60");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ForceBuzzClosePOPUp"); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob("ForceBuzzClosePOPUp",360); 
		androidJOB.verifyForceReadMessage("ClosePopUp");
		androidJOB.verifyPopupisClosed("ClosePopUp");
		Reporter.log("Pass >>Text Message - Verify Force read message by enabling Close message Pop-up.",true); 
	}
	
}
