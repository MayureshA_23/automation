package Settings_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class SASS extends Initialization{

	@Test(priority=1,description="Verify Admin Center is Visible") 
	public void VerifyAdminCenterisVisibleTC_SS_001() throws Throwable {
		Reporter.log("\n1.Verify Admin Center is Visible",true);
		AvaylerScript.AdminCenter();		
		accountsettingspage.AdminCenterTrials();
				
}
	
	@Test(priority=2,description="Verify Login Button is Visible") 
	public void VerifyAdminCenterisClickableTC_SS_002() throws Throwable {
		Reporter.log("\n2.Verify Login Button is Visible",true);
		AvaylerScript.ClickonLogin();		
	//	accountsettingspage.AdminCenterTrials();
				
}
	
	@Test(priority=3,description="Verify All the Tab Engine is Visible") 
	public void VerifyDynamicSchedulineEngineisVisibleTC_SS_003() throws Throwable {
		Reporter.log("\n3.Verify All the Tab Engine is Visible",true);
		AvaylerScript.DynamicPricingEngineVisible();
		AvaylerScript.DynamicSchedulingEngineVisible();
		AvaylerScript.TechnicianPortalVisible();
	//	accountsettingspage.AdminCenterTrials();
	}
	
	@Test(priority=4,description="Verify Technician Portal is Visible") 
	public void VerifyTechnicianPortalisVisibleTC_SS_004() throws Throwable {
		Reporter.log("\n4.Verify Technician Portal is Visible",true);
		AvaylerScript.TechnicianPortalVisible();
		AvaylerScript.ClickonTechnicianPortal();
		
				
}

	@Test(priority=5,description="Verify All the option of Avayler is Visible") 
	public void VerifyAvayleroptionisVisibleTC_SS_005() throws Throwable {
		Reporter.log("\n5.Verify Avayler option is Visible",true);
		AvaylerScript.AvaylertextVisible();
		AvaylerScript.TechnicianPortaltextVisible();
//		AvaylerScript.PricingProfilestextVisible();
//		AvaylerScript.WorkflowstextVisible();
//		AvaylerScript.AmazingModuletextVisible();
//		AvaylerScript.FantasticModuletextVisible();
//		AvaylerScript.UsersManagementtextVisible();
	
	}	
	@Test(priority=6,description="Verify All the option of Workflow Setup is Visible") 
	public void VerifyWorkflowSetupoptionisVisibleTC_SS_006() throws Throwable {
		Reporter.log("\n6.Verify Workflow Setup is Visible",true);
		AvaylerScript.WorkflowSetuptextVisible();
		AvaylerScript.StartofDaytextVisible();
		AvaylerScript.BreaktextVisible();
		AvaylerScript.EndofDaytextVisible();
		AvaylerScript.JobDisclaimerstextVisible();
		AvaylerScript.StartyourjobtextVisible();
		AvaylerScript.unabletostartjobtextVisible();
		
		
		AvaylerScript.JobCantFulfiltextVisible();
		AvaylerScript.JobDisclaimers2textVisible();
		AvaylerScript.YourWorkflowtextVisible();
		AvaylerScript.UnabletocompletetextVisible();
		AvaylerScript.CustomerReviewtextVisible();
		AvaylerScript.FinishYourJobtextVisible();
		AvaylerScript.CompleteyourjobtextVisible();
	
	}
	@Test(priority=7,description="Verify All the option Present on technician Portal is Visible") 
	public void VerifyTechnicianPortaloptionisVisibleTC_SS_007() throws Throwable {
		Reporter.log("\n7.Verify All the option of Technician Portal is Visible",true);
		AvaylerScript.UserNametextVisible();
		AvaylerScript.ConsoletextVisible();
		AvaylerScript.MyAccounttextVisible();
		AvaylerScript.LogouttextVisible();
	}
	
	@Test(priority=8,description="Verify Start of Day is Visible & Clickable") 
	public void VerifyStartofDayisVisibleandclickableTC_SS_008() throws Throwable {
		Reporter.log("\n8.Verify Start of Day is Visible & CLickable",true);
		AvaylerScript.StartofDaytextVisible();
		AvaylerScript.ClickonStartofDay();
		
	}
	
	@Test(priority=9,description="Verify All the option Present on Start of Day is Visible") 
	public void VerifyStartofDayoptionisVisibleTC_SS_009() throws Throwable {
		Reporter.log("\n9.Verify All the option of Start of Day is Visible",true);
		AvaylerScript.StartofDayVisible();
		AvaylerScript.NowServingVisible();
		AvaylerScript.VersionVisible();
		AvaylerScript.PublishedVisible();
		AvaylerScript.DisableWorkflowVisible();
		AvaylerScript.ViewWorkflowVisible();
		AvaylerScript.DraftChangesetVisible();
		AvaylerScript.ResetDraftVisible();
		AvaylerScript.PublishDraftVisible();
		AvaylerScript.EditDraftVisible();
		AvaylerScript.ArchivedWorkflowsVisible();
		
}
	
	@Test(priority=10,description="Verify DISABLE WORKFLOW is Visible & Clickable") 
	public void VerifyDISABLEWORKFLOWisVisibleandclickableTC_SS_010() throws Throwable {
		Reporter.log("\n10.Verify DISABLE WORKFLOW is Visible & CLickable",true);
		AvaylerScript.DisableWorkflowVisible();
		AvaylerScript.ClickonDisableWorkflow();
		AvaylerScript.WarningMessageVisible();
		AvaylerScript.CancelVisible();
		AvaylerScript.DisableSurveyVariationVisible();
		AvaylerScript.ClickonCancel();
//		AvaylerScript.ClickontheDisableWorkflow();
//		AvaylerScript.ClickontheDisableSurveyVariation(); 
	
	}
	
	@Test(priority=11,description="Verify VIEW WORKFLOW is Visible & Clickable") 
	public void VerifyVIEWWORKFLOWisVisibleandclickableTC_SS_011() throws Throwable {
		Reporter.log("\n11.Verify VIEW WORKFLOW is Visible & CLickable",true);
		AvaylerScript.ViewWorkflowisVisible();
		AvaylerScript.ClickontheViewWorkflow();
		AvaylerScript.StartofdaysurveyVisible();
		AvaylerScript.MuiAlertmessageVisible();
//		AvaylerScript.CheckInVisible();
//		AvaylerScript.ToolCheckVisible();
//		AvaylerScript.StockCheckVisible();
//		AvaylerScript.TestIndexPageVisible();
//		AvaylerScript.TestPageOneVisible();
//		AvaylerScript.TestPageTwoVisible(); 
//		AvaylerScript.PPEVisible();
		AvaylerScript.BACKTOWORKFLOWSETUPtextisdisplayedofviewworkflowstartofday();
		AvaylerScript.ClickonBACKTOWORKFLOWSETUPofviewworkflow();
		
		
		
	}
	
	
	@Test(priority=12,description="Verify RESET DRAFT is Visible & Clickable") 
	public void VerifyRESETDRAFTisVisibleandclickableTC_SS_012() throws Throwable {
		Reporter.log("\n12.Verify RESET DRAFT is Visible & CLickable",true);
		AvaylerScript.ResetDraftVisible();
		AvaylerScript.ClickontheResetDraft();
		AvaylerScript.WarningMessageVisible();
		AvaylerScript.CancelVisible();
//		AvaylerScript.ClickontheCancelbutton();
//		AvaylerScript.ClickontheResetDraft();
		AvaylerScript.ResetChangesetisVisible();
		AvaylerScript.ClickontheCancelbutton();
		
		
		
		
		
	}
	
	
	@Test(priority=13,description="Verify Publish Draft is Visible & Clickable") 
	public void VerifyPublishDraftisVisibleandclickableTC_SS_013() throws Throwable {
		Reporter.log("\n13.Verify Publish Draft is Visible & CLickable",true);
		AvaylerScript.PublishDraftisVisible();
		AvaylerScript.ClickonthePublishDraft();
		AvaylerScript.MessageisbeenVisible();
		AvaylerScript.CancelbuttonVisible();
//		AvaylerScript.ClickontheCancelButton();
//		AvaylerScript.ClickonthePublishDraft();
		AvaylerScript.PublishDraftChangesetVisible();
		AvaylerScript.ClickontheCancelButton();
		
		
	}		
	
	@Test(priority=14,description="Verify EDIT DRAFT is Visible & Clickable") 
	public void VerifyEDITDRAFTisVisibleandclickableTC_SS_014() throws Throwable {
		Reporter.log("\n14.Verify EDIT DRAFTis Visible & CLickable",true);
		
		AvaylerScript.EditDraftisVisible();
		AvaylerScript.ClickontheEditDraftButton();
		AvaylerScript.NewPageisVisible();
//		AvaylerScript.CheckInisVisible();
//		AvaylerScript.ToolCheckisVisible();
//		AvaylerScript.StockCheckisVisible();
//		AvaylerScript.TestIndexPageisVisible();
//		AvaylerScript.TestPageOneisVisible();
//		AvaylerScript.TestPageTwoisVisible();
//		AvaylerScript.PPEisVisible();
		AvaylerScript.STARTOFDAYtextisdisplayedofEDITDRAFTstartofday();
		AvaylerScript.BacktoWorkflowSetuptextisdisplayedofEDITDRAFTstartofday();
		AvaylerScript.VECHICLEtextisdisplayedofEDITDRAFTstartofday();
		AvaylerScript.TOOLCHECKtextisdisplayedofEDITDRAFTstartofday();
		AvaylerScript.STOCKCHECKtextisdisplayedofEDITDRAFTstartofday();
		
		
	}
	
	
	@Test(priority=15,description="Verify New Page is Visible & Clickable") 
	public void VerifyNewPageisVisibleandclickableTC_SS_015() throws Throwable {
		Reporter.log("\n15.Verify New Page is Visible & CLickable",true);
		
		AvaylerScript.NewPageisVisible();
		AvaylerScript.ClickontheNewPageButton();
		AvaylerScript.PageElementisVisible();
		AvaylerScript.NewPagetitleisVisible();
		AvaylerScript.DescriptionfieldisVisible();
//		AvaylerScript.ButtonTextfieldisVisible();
		AvaylerScript.CancelButtonfieldisVisible();
//		AvaylerScript.ClickontheCancelButtonfield();
	
		AvaylerScript.abcd();

		AvaylerScript.ClickonSAVEBUTTONOFEDITDRAFTOFSTARTOFDAY();
		AvaylerScript.enterNEWPAGE("TEST NEW PAGE");
		AvaylerScript.PressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY();
		AvaylerScript.abcde();
		AvaylerScript.enterDescription("Trial Description");
		AvaylerScript.enteronSAVEBUTTONofEDITDRAFTOFSTARTOFDAY();
		AvaylerScript.enterNEWPAGE("TEST NEW PAGE");
		AvaylerScript.ClickorpressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY();
		AvaylerScript.NEWSECTIONTABisdisplayedofEDITDRAFTstartofday();
		AvaylerScript.NEWQUESTIONTABisdisplayedofEDITDRAFTstartofday();
//		AvaylerScript.ClickorpressonRemoveBUTTONofEDITDRAFTOFSTARTOFDAY();
		
		
		
	}	
	@Test(priority=16,description="Verify New Section Visible & Clickable") 
	public void VerifyNewSectionisVisibleandclickableTC_SS_016() throws Throwable {
		Reporter.log("\n16.Verify New Section Visible & CLickable",true);
//		AvaylerScript.CheckInisVisible();
//		AvaylerScript.ClickontheCheckInButtonfield();
//		AvaylerScript.PageElementfieldofCheckInisVisible();
//		AvaylerScript.fieldofCheckInVehicleisVisible();
//		AvaylerScript.fieldofCheckInContinueisVisible();
//		AvaylerScript.fieldofCheckInRemoveisVisible();
//		AvaylerScript.fieldofCheckInCancelisVisible();
//		AvaylerScript.fieldofCheckInSaveisVisible();
//		AvaylerScript.ClickontheCancelofCheckInButtonfield();
		AvaylerScript.NEWSECTIONTABisdisplayedofEDITDRAFTstartofday();
		AvaylerScript.ClickorpressonNEWSECTIONTABofEDITDRAFTOFSTARTOFDAY();
		AvaylerScript.Cleartitlenewsection();
	//	AvaylerScript.enterNewsectiontitle("TEST NEW SECTION");
		AvaylerScript.ClickorpressonSaveofNEWSECTIONOFSTARTOFDAY();
		AvaylerScript.enterNewsectiontitle("TEST NEW SECTION");
		AvaylerScript.enterDescriptionNEWSECTION("DESCRIPTION");
		AvaylerScript.ClickorpressonSaveofNEWSECTION();
		AvaylerScript.ClickorpressonREMOVEofNEWSECTION();
		
	
	
	}
	
	@Test(priority=17,description="Verify New QUESTION Text Visible & Clickable") 
	public void VerifyNewquestiontextisVisibleandclickableTC_SS_017() throws Throwable {
		Reporter.log("\n17.Verify New QUESTION Text Visible & CLickable",true);
		AvaylerScript.Clickorpressontestnewpageofnewpage();
		AvaylerScript.ClickorpressonNewQuestionoftestnewpage();
		AvaylerScript.Clickorpressontextofnewquestion();
		AvaylerScript.Cleartitleofnewquestion();
		AvaylerScript.ClickorpressonSaveofnewquestion();
		AvaylerScript.enterNEWQUESTION("TEST NEW QUESTION");
		AvaylerScript.ClickorpressonSaveBUTTONQUESTION();
		AvaylerScript.enterQUESTIONNAME("test question name");
		AvaylerScript.enterValidationExpression("test validation expression");
		AvaylerScript.ClickorpressonMultilineTextbox();
		AvaylerScript.ClickorpressonOptionalTextbox();
		AvaylerScript.ClickorpressonRemovetabofquestionname();
		
	
	
	}	
	
	@Test(priority=18,description="Verify New QUESTION Number Visible & Clickable") 
	public void VerifyNewquestionNumberisVisibleandclickableTC_SS_018() throws Throwable {
		Reporter.log("\n18.Verify New QUESTION Number Visible & CLickable",true);
		AvaylerScript.ClickTESTNEWPAGEAGAIN();
		AvaylerScript.ClickDiscardChanges();
		AvaylerScript.ClickorpressonNewQuestionoftestnewpage();
		AvaylerScript.ClickonNumber();
		AvaylerScript.ClearNumberofnewquestion();
		AvaylerScript.ClickonsaveofNumber();
		AvaylerScript.enterQUESTIONTEXTOFNUMBER("TEST NEW QUESTION");
		AvaylerScript.enterQUESTIONNAMEOFNUMBER("TEST QUESTION NAME");
		AvaylerScript.ClickonsaveofNumber();
		AvaylerScript.ClickonDECIMALofNumber();
		AvaylerScript.ClickonsaveofNumber();
		AvaylerScript.EnterMinDigit();
		AvaylerScript.ClickonsaveofNumber();
		AvaylerScript.EnterMaxDigit();
		AvaylerScript.ClickonIntegerofNumber();
		AvaylerScript.ClickonDECIMALofNumber();
		AvaylerScript.ClickSAVEBUTTONOFNUMBER();
		AvaylerScript.RemovetabclickonNumber();
	
	
	}
	
	@Test(priority=19,description="Verify New QUESTION Select Visible & Clickable") 
	public void VerifyNewquestionSelectisVisibleandclickableTC_SS_019() throws Throwable {
		Reporter.log("\n19.Verify New QUESTION Select Visible & CLickable",true);
		
		AvaylerScript.TESTNEWPAGEISclick();
		AvaylerScript.NEWQUESTIONISclickED();
		AvaylerScript.SELECTISclickED();
		AvaylerScript.ClearSELECTofnewquestion();
		AvaylerScript.CLICKONSAVEOFSELECT();
		AvaylerScript.enterQUESTIONTEXTOFSELECT("TEST QUESTION TEXT");
		AvaylerScript.enterQUESTIONNAMEOFSELECT("TEST QUESTION NAME");
		AvaylerScript.CLICKONSAVEOFSELECT();
		AvaylerScript.CLICKONRADIOCHECKBOXOFSELECT();
		AvaylerScript.CLICKONSAVEOFSELECT();
		AvaylerScript.CLICKONDROPDOWNCHECKBOXOFSELECT();
		AvaylerScript.CLICKONSAVEOFSELECT();
		AvaylerScript.CLICKONRADIOCHECKBOXOFSELECT();
		AvaylerScript.enterSETTEHTEXTPLEASEOFSELECT("TEST SET THE TEXT PLEASE");
		AvaylerScript.CLICKONSAVEOFSELECT();
		AvaylerScript.enterSETTEHTEXTPLEASE2OFSELECT("TEST SET THE TEXT PLEASE 2");
		AvaylerScript.CLICKONADDANSWEROPTIONOFSELECT();
		AvaylerScript.enterSETTEHTEXTPLEASE3OFSELECT("test set the text 3");
		AvaylerScript.CLICKONoptionalOFSELECT();
		AvaylerScript.CLICKONsavebuttonOFSELECT();
		AvaylerScript.CLICKONremovebuttonOFSELECT();
		
		
	}
	
	@Test(priority=20,description="Verify New QUESTION Multi-Select Visible & Clickable") 
	public void VerifyNewquestionMultiSelectisVisibleandclickableTC_SS_020() throws Throwable {
		Reporter.log("\n20.Verify New QUESTION Multi-Select Visible & CLickable",true);
	
		AvaylerScript.TESTNEWPAGEISclick();
		AvaylerScript.NEWQUESTIONISclickED();
		AvaylerScript.CLICKONMultiSelect();
		AvaylerScript.ClearMultiSELECTofnewquestion();
		AvaylerScript.CLICKONSAVEOFMultiSELECT();
		AvaylerScript.entermultiSELECTquestiontext("test question text");
		AvaylerScript.entermultiSELECTquestionname("test question name");
		AvaylerScript.CLICKONSAVEOFMultiSELECT();
		AvaylerScript.CLICKONheadlessquestion();
		AvaylerScript.CLICKONoptionalofmultiselect();
		AvaylerScript.CLICKONheadlessquestion();
		AvaylerScript.entermultiSELECTquestiontext("test question text");
		AvaylerScript.CLICKONSAVEOFMultiSELECT();
		AvaylerScript.enterAnsweroptionmultiSELECT("Test Answer Option");
		AvaylerScript.CLICKONPreselectedofmultiselect();
		AvaylerScript.CLICKONPreselectedofmultiselect();
		
		AvaylerScript.CLICKONADDANSWEROPTIONofmultiselect();
		AvaylerScript.CLICKDeleteoptionofmultiselect();
		AvaylerScript.CLICKSAVEOPTIONBUTTONofmultiselect();
		AvaylerScript.CLICKREMOVEOPTIONBUTTONofmultiselect();
	
	}	
	
	
	@Test(priority=21,description="Verify New QUESTION Instruction Visible & Clickable") 
	public void VerifyNewquestionInstructionisVisibleandclickableTC_SS_021() throws Throwable {
		Reporter.log("\n21.Verify New QUESTION Instruction Visible & CLickable",true);
		
		AvaylerScript.TESTNEWPAGEISclick();
		AvaylerScript.NEWQUESTIONISclickED();
		AvaylerScript.CLICKInstruction();
		AvaylerScript.Clearinstructionofnewquestion();
		AvaylerScript.CLICKInstructiononSAVEBUTTON();
		AvaylerScript.enterinstructionquestiontext("test question text");
		AvaylerScript.CLICKInstructiononSAVEBUTTON();
		AvaylerScript.enterinstructionquestionName("test question name");
		AvaylerScript.CLICKInstructiononSAVEofBUTTON();
		AvaylerScript.CLICKInstructiononRemoveofBUTTON();
	}
	
	
	@Test(priority=22,description="Verify New QUESTION Button Visible & Clickable") 
	public void VerifyNewquestionButtonisVisibleandclickableTC_SS_022() throws Throwable {
		Reporter.log("\n22.Verify New QUESTION Button Visible & CLickable",true);
		
		AvaylerScript.TESTNEWPAGEISclick();
		AvaylerScript.NEWQUESTIONISclickED();
		AvaylerScript.CLICKButton();
		AvaylerScript.CLICKsaveofButtonmethod();
		AvaylerScript.enterButtonquestionText("Test Question Text");
		AvaylerScript.CLICKNavigationEnabledofButtonmethod();
		AvaylerScript.CLICKsaveofButtonmethod();
		AvaylerScript.CLICKNavigationtypeofButtonmethod();
		AvaylerScript.CLICKscreenorpageofNavigationtype();
		AvaylerScript.CLICKNavigationtypeofButtonmethod();
		AvaylerScript.CLICKWorkflowofNavigationtype();
		AvaylerScript.CLICKNavigationtypeofButtonmethod();
		AvaylerScript.CLICKWorkflowpageofNavigationtype();
		AvaylerScript.CLICKsaveofButtonmethod();
		AvaylerScript.CLICKSelectpagetypeofNavigationtype();
		AvaylerScript.CLICKVehicleofselectpagetype();
		AvaylerScript.CLICKSelectpagetypeofNavigationtype();
		AvaylerScript.CLICKtoolcheckofselectpagetype();
		AvaylerScript.CLICKSelectpagetypeofNavigationtype();
		AvaylerScript.CLICKStockCheckofselectpagetype();
	//	AvaylerScript.CLICKSelectpagetypeofNavigationtype();
	//	AvaylerScript.CLICKTestNewPageofselectpagetype();
		AvaylerScript.CLICKActionenabledofButtonmethod();
		AvaylerScript.CLICKsaveofButtonmethod();
		AvaylerScript.CLICKActionenabledoptiontext();
		AvaylerScript.CLICKsetjobtodoneoptiontext();
		AvaylerScript.CLICKActionenabledoptiontext();
		AvaylerScript.CLICKsetjobitemtodoneoptiontext();
		AvaylerScript.CLICKActionenabledoptiontext();
		AvaylerScript.CLICKsetbreaktodoneoptiontext();
		AvaylerScript.CLICKAddActionOptiontab();
		AvaylerScript.CLICKdeleteboxOptiontab();
		AvaylerScript.CLICKoptionalboxOptiontab();
		AvaylerScript.CLICKsaveboxOptiontab();
		AvaylerScript.CLICKRemoveboxOptiontab();
		
	}	
	
	@Test(priority=23,description="Verify New QUESTION Photo Visible & Clickable") 
	public void VerifyNewquestionPhotoisVisibleandclickableTC_SS_023() throws Throwable {
		Reporter.log("\n23.Verify New QUESTION Photo Visible & CLickable",true);
	
		AvaylerScript.TESTNEWPAGEISclick();
		AvaylerScript.NEWQUESTIONISclickED();
		AvaylerScript.CLICKPhotoOptiontab();
		AvaylerScript.ClearPhotoofnewquestion();
		AvaylerScript.CLICKSaveofPhotoOptiontab();
		AvaylerScript.enterPhotoquestiontext("Test New Question");
		AvaylerScript.enterPhotoquestionName("test question name");
		AvaylerScript.CLICKOptionalPhotoOptiontab();
		AvaylerScript.CLICKtabsavePhotoOptiontab();
		AvaylerScript.CLICKtabRemovePhotoOptiontab();
	
	}
	
	
	@Test(priority=24,description="Verify New QUESTION Signature Visible & Clickable") 
	public void VerifyNewquestionSignatureisVisibleandclickableTC_SS_024() throws Throwable {
		Reporter.log("\n24.Verify New QUESTION Signature Visible & CLickable",true);
	
		AvaylerScript.TESTNEWPAGEISclick();
		AvaylerScript.NEWQUESTIONISclickED();
		AvaylerScript.CLICKSignatureOptiontab();
		AvaylerScript.ClearSignatureofnewquestion();
		AvaylerScript.CLICKSaveofSignatureOptiontab();
		AvaylerScript.enterSignaturequestiontext("Test question text");
		AvaylerScript.enterSignaturequestionName("Test question name");
		AvaylerScript.CLICKoptionalofSignatureOptiontab();
		AvaylerScript.CLICKSavetabofSignatureOption();
		AvaylerScript.CLICKRemovetabofSignatureOption();
	}
}