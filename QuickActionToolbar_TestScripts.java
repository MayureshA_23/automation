package NewUI_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class QuickActionToolbar_TestScripts extends Initialization{
	@Test(priority=0,description="Verify locate in Quick Action Tool Bar.")
	public void VerifyOfLocate() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====1.Verify locate in Quick Action Tool Bar.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		newUIScriptsPage.CheckingLocationTrackingStatus();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.ClickingOnTurnOnButton();
    	quickactiontoolbarpage.TurningOnLocationTracking("2");
    	quickactiontoolbarpage.WaitingForUpdate(180);	
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.VerifyingDeviceNameInRealTimeLocationTracking(Config.DeviceName);
    	quickactiontoolbarpage.VerifyingDeviceAddressInRealTimeLocationTracking(Config.LoctionTrackingAddress,Config.PinCode);
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();
	}
	@Test(priority=1,description="Verify Blocklist multiple devices.")
	public void BlockListMultipleDevices() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====2.Verify Blocklist multiple devices.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		commonmethdpage.SearchMultipleDevice(Config.MultipleDeviceName);
		rightclickDevicegrid.SelectMultipleDevice();
		blacklistedpage.ClickOnBlacklistButton();
		newUIScriptsPage.ClickOnYesButtonWarningDialogBlacklist();
		blacklistedpage.ClickOnBlacklisted();
		blacklistedpage.IstheBlacklistedDevicePresent(Config.DeviceName);
		blacklistedpage.IstheBlacklistedDevicePresent(Config.MultipleDevices1);
		newUIScriptsPage.SelectMultipleDevicesToWhitelist();
		blacklistedpage.ClickOnWhitelistButton();
		newUIScriptsPage.ClickOnYesButtonWarningDialogWhitelist();
		
	}
	@Test(priority=2,description="Verify deleting the Android devices.")
	public void DeleteDeviceFromConsole() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====3.Verify deleting the Android devices.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName1);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName1);
		rightclickDevicegrid.ClickOnDelete();
		newUIScriptsPage.WarningMessageOnDeviceDelete();
		//dynamicjobspage.ClickOnRebootDeviceYesBtn();
		//rightclickDevicegrid.clickOnPendingDelete();
		//rightclickDevicegrid.VerifyOfDeletedDeviceInPendingDelete(Config.DeviceName1);
	}

}
