package NewUI_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class FileStore_TestScripts extends Initialization{
	@Test(priority=0,description="File Store-Creating New folder")
	public void CreateNewFolder() throws Throwable
	{
		Reporter.log("=====1.File Store-Creating New folder=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		filestorepage.ClickOnFileStore();
		filestorepage.NamingFolderWhileCreation("File123");
		filestorepage.ClickOnDelete("File123");
		filestorepage.ClickOnDeleteYesButton();
	}
	@Test(priority=1,description="File Store-Delete")
	public void DeleteFolder() throws Throwable
	{	Reporter.log("=====2.File Store-Delete=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		filestorepage.ClickOnFileStore();
		filestorepage.NamingFolderWhileCreation("AutoFile");
		filestorepage.ClickOnDelete("AutoFile");
		filestorepage.ClickOnDeleteYesButton();
	}
}
