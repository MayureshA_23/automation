package Profiles_Windows_Testscripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class AppLocker extends Initialization{
	
	@Test(priority='1',description="To verify clicking on App Locker Policy") 
    public void VerifyClickingOnAppLockerPolicy() throws InterruptedException
	{
        Reporter.log("1.To verify clicking on App Locker Policy",true);
        profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
        profilesWindows.ClickOnAppLocker();
    }
	
	@Test(priority='2',description="To verify summary of AppLocker")
	public void VerifySummaryofAppLocker() throws InterruptedException
	{
		 Reporter.log("\n2.To verify summary of AppLocker",true);
		profilesWindows.VerifySummaryof_AppLocker();
	}
	
	@Test(priority='3',description="To Verify warning message Saving App Locker profile without profile Name")
	public void VerifyWarningMessageOnSavingAppLockerProfileWithoutName() throws InterruptedException{
		Reporter.log("\n3.To Verify warning message Saving App Locker profile without profile Name",true);
		profilesWindows.ClickOnConfigureButton_AppLockerProfile();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='4',description="To Verify warning message Saving App locker Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingApplockerProfileWihtoutName() throws InterruptedException{
		Reporter.log("\n4.To Verify warning message Saving a App Locker Profile without entering all the fields",true);
		profilesWindows.EnterAppLockerPolicyProfileName();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	@Test(priority='5',description="To Verify clicking on Add button of the app locker profile")
	public void VerifyClickingOnAddButtonOfAppLockerProfile() throws InterruptedException{
		Reporter.log("\n5.To Verify clicking on Add button of the app locker profile",true);
		profilesWindows.ClickOnAddButton_AppLocker();
	}
	
	@Test(priority='6',description="To Verify the parameters of App locker Window")
	public void VerifyParamentersOfAppLockerWindow() throws InterruptedException{
		Reporter.log("\n6.To Verify the parameters of App locker Window",true);
		profilesWindows.VerifyAlltheParametersOfAppLockerWindow();
	}
	
	@Test(priority='7',description="To Verify warning message while clicking Add Button of Applocker Profile without entering all the fields")
	public void VerifyWarningMessageOnClickingOnAddButtonOfAppLockerWindowWithoutAllFields() throws InterruptedException{
		Reporter.log("\n7.To Verify warning message while clicking Add Button of Applocker Profile without entering all the fields",true);
		profilesWindows.AddButton_AppLockerWindow();
		profilesWindows.WarningMessageWithoutOutgoing_MailConfiguration();//common
	}
	
	@Test(priority='8',description="To Verify cancelling the AppLocker Window")
	public void VerifyCancellingTheAppLockerWindow() throws InterruptedException{
		Reporter.log("\n8.To Verify cancelling the AppLocker Window",true);
		profilesWindows.ClickOnCancelButton_AppLockerWindow();
	}
	
	@Test(priority='9',description="To Verify adding Allow Type")
	public void VerifyAddingAllowTypeAppLockerProfile() throws InterruptedException{
		Reporter.log("\n9.To Verify adding allow type",true);
		profilesWindows.ClickOnAddButton_AppLocker();
		profilesWindows.EnterPublisherAndPackageName_AllowType();
		profilesWindows.AddButton_AppLockerWindow();
		profilesWindows.VerifyPublisherPackageNameActionAfterAdding_Allow();
	}
	
	@Test(priority='A',description="To Verify adding Deny Type")
	public void VerifyAddingDenyTypeAppLockerProfile() throws InterruptedException{
		Reporter.log("\n10.To Verify adding allow type",true);
		profilesWindows.ClickOnAddButton_AppLocker();
		profilesWindows.ClickOnDenyRadioButton();
		profilesWindows.EnterPublisherAndPackageName_DenyType();
		profilesWindows.AddButton_AppLockerWindow();
		profilesWindows.VerifyPublisherPackageNameActionAfterAdding_Deny();
		
	}
	
	@Test(priority='B',description="To Verify Editing the Name of the Publisher of the 1st row")
	public void VerifyEditingTheNameofThePublisherOfTheFirstRow() throws InterruptedException{
		Reporter.log("\n11.To Verify Editing the Name of the Publisher of the 1st row",true);
		profilesWindows.ClickOnFirstRowDataOfAppLocker();
		profilesWindows.ClickOnEditButtonAppLocker();
		profilesWindows.RenamePublisher_AppLocker();
		profilesWindows.SaveButton_AppLockerWindow(); //button is 'Save' after Edit
		profilesWindows.isRenamedPublisherNameSaved();
		
	}
	
	@Test(priority='C',description="To Verify the warning dialog on Deleting the App Locker publisher")
	public void VerifyWarningMessageOnDeletingAppLockerPublisher() throws InterruptedException{
		Reporter.log("\n12.To Verify the warning message on Deleting the App Locker publisher",true);
		profilesWindows.ClickOnFirstRowDataOfAppLocker();
		profilesWindows.ClickOnDeleteButton_AppLocker();
		profilesiOS.WarningMessageOnApplicationDelete(); //common for this as well
		
	}
	@Test(priority='D',description="1.To Verify cancelling the warning dialog")
	public void VerifyCancellingTheWarningDialog() throws InterruptedException{
		Reporter.log("\n13.To Verify cancelling the warning dialog",true);
		profilesWindows.ClickOnNoButtonWarningDialog();
		
	}
	
	@Test(priority='E',description="To Verify Deleting the 1st row")
	public void VerifyDeletingTheFirstRow() throws InterruptedException{
		Reporter.log("\n14.To Verify Deleting the 1st row",true);
		profilesWindows.ClickOnDeleteButton_AppLocker();
		profilesWindows.ClickOnYesButtonWarningDialog();
		
	}
	@Test(priority='F',description="To Verify Saving AppLocker profile")
	public void VerifySavingAppLockerProfile() throws InterruptedException{
		Reporter.log("\n15.To Verify Saving AppLocker profile",true);
	    profilesWindows.ClickOnSaveButton();
		profilesWindows.NotificationOnProfileCreated();
	}
	
	
}
