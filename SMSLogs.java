package DynamicJobs_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Utility;

public class SMSLogs extends Initialization{

	@Test(priority='1',description="1.To Verify Dynamic SMS Log Job Window's UI")
	public void DynamicSMSLogJob_TC1() throws InterruptedException, Throwable{
		Reporter.log("\n1.To Verify Dynamic SMS Log Job Window's UI",true);
		commonmethdpage.ClickOnMDMTrailAccountWarning();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.TurningOffSMSLogs();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.VerifyOfSMSLogWindow();
	    dynamicjobspage.VerifyOfGridMessage();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window's UI",true);
	}
	
	@Test(priority='2',description="2.To Verify Dynamic SMS Log Job Window when SMS log is OFF, Update interval textbox must be grayed out")
	public void DynamicSMSLogJob_TC2() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Dynamic SMS Log Job Window when SMS log is OFF, Update interval textbox must be grayed out",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_Off();
	    dynamicjobspage.VerifyOfSMSUpdateIntervalGrayedOut();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when SMS log is OFF, Update interval textbox must be grayed out",true);
	}
	
	@Test(priority='3',description="3.To Verify Dynamic SMS Log Job Window when SMS log is ON, Update interval textbox must be Enabled")
	public void DynamicSMSLogJob_TC3() throws InterruptedException, Throwable{
		Reporter.log("\n3.To Verify Dynamic SMS Log Job Window when SMS log is ON, Update interval textbox must be Enabled",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_On();
	    dynamicjobspage.VerifyOfSMSUpdateInterval();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when SMS log is ON, Update interval textbox must be Enabled",true);
	}
	
	@Test(priority='4',description="4.To Verify Dynamic SMS Log Job Window when Update interval is less than fourteen")
	public void DynamicSMSLogJob_TC4() throws InterruptedException, Throwable{
		Reporter.log("\n4.To Verify Dynamic SMS Log Job Window when Update interval is less than fourteen",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnMDMTrailAccountWarning();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_On();
	    dynamicjobspage.ClearSMSLogInterval();
	    dynamicjobspage.EnterSMSLogInterval("10");
	    dynamicjobspage.ClickOnSMSLogOkBtn();
	    dynamicjobspage.SMSLogErrorMessage();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when Update interval is less than fourteen",true);
	}
	
	@Test(priority='5',description="5.To Verify Dynamic SMS Log Job Window when Clicked on Cancel button")
	public void DynamicSMSLogJob_TC5() throws InterruptedException, Throwable{
		Reporter.log("\n5.To Verify Dynamic SMS Log Job Window when Clicked on Cancel button",true);
		String SMSLogInterval="20";
		commonmethdpage.ClickOnMDMTrailAccountWarning();
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_On();
	    dynamicjobspage.ClearSMSLogInterval();
	    dynamicjobspage.EnterSMSLogInterval(SMSLogInterval);
	    dynamicjobspage.ClickOnSMSLogCancelBtn();
	    dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.VerifyOfSMSLogCancelButton(SMSLogInterval);
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when Clicked on Cancel button",true);
	}
	
	@Test(priority='6',description="6.To Verify Dynamic SMS Log Job Window when SMS Log is On and Clicked On Ok button")
	public void DynamicSMSLogJob_TC6() throws InterruptedException, Throwable{
		Reporter.log("\n6.To Verify Dynamic SMS Log Job Window when SMS Log is On and Clicked On Ok button",true);
		String SMSLogInterval="16";
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_On();
	    dynamicjobspage.ClearSMSLogInterval();
	    dynamicjobspage.EnterSMSLogInterval(SMSLogInterval);
	    dynamicjobspage.ClickOnSMSLogOkBtn();
	    dynamicjobspage.SMSLogConfirmationMessage();
	    dynamicjobspage.VerifyOfSMSLogsActivityLogs();
	//    dynamicjobspage.ClickOnSMSExport();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when SMS Log is On and Clicked On Ok button",true);
	}
	
	@Test(priority='7',description="7.To Verify Dynamic SMS Log Job Window when Clicked on Inbox Tab Section")
	public void DynamicSMSLogJob_TC7() throws InterruptedException, Throwable{
		Reporter.log("\n7.To Verify Dynamic SMS Log Job Window when Clicked on Inbox Tab Section",true);
		commonmethdpage.ClickOnMDMTrailAccountWarning();
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.ClickOnInboxTab();
	    dynamicjobspage.VerifyOfInboxSMSLogUpdate();
	//    dynamicjobspage.ClickOnSMSExport();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when Clicked on Inbox Tab Section",true);
	}
	
		@Test(priority='8',description="8.To Verify Dynamic SMS Log Job Window when Clicked on Sent Tab Section")
	public void DynamicSMSLogJob_TC8() throws InterruptedException, Throwable{
		Reporter.log("\n8.To Verify Dynamic SMS Log Job Window when Clicked on Sent Tab Section",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.ClickOnSentTab();
	    dynamicjobspage.VerifyOfSentSMSLogUpdate();
	//    dynamicjobspage.ClickOnSMSExport();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when Clicked on Sent Tab Section",true);
	}
	
    @Test(priority='9',description="9.To Verify Dynamic SMS Log Job Window when Clicked on Draft Tab Section")
	public void DynamicSMSLogJob_TC9() throws InterruptedException, Throwable{
		Reporter.log("\n9.To Verify Dynamic SMS Log Job Window when Clicked on Draft Tab Section",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.ClickOnDraftTab();
	    dynamicjobspage.VerifyOfDraftSMSLogUpdate();
	//    dynamicjobspage.ClickOnSMSExport();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when Clicked on Draft Tab Section",true);
	}
	
	@Test(priority='A',description="10.To Verify Dynamic SMS Log Job Window when Clicked on Outbox Tab Section")
	public void DynamicSMSLogJob_TCA() throws InterruptedException, Throwable{
		Reporter.log("\n10.To Verify Dynamic SMS Log Job Window when Clicked on Outbox Tab Section",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.ClickOnOutboxTab();
	    dynamicjobspage.VerifyOfOutboxSMSLogUpdate();
//	    dynamicjobspage.ClickOnSMSExport();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic SMS Log Job Window when Clicked on Outbox Tab Section",true);
	}
	
	@Test(priority='B',description="11.To Create Static Jobs Call Logs tracking Jobs_ON&OFF")
	public void CreatingStaticJobsSMSTracking() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		dynamicjobspage.ClickOnStaticJobSMSLogTrackingJob();
		dynamicjobspage.CreatingStaticJobSMSLogTracking_ON("SMSLogTracking_ON");
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority='C',description="12.To Verify SMS Log Deatils In Dynamic Job SMS Tracking For First Interval")
	public void VerifyingSMSDetails_AllSectionfirstInterval() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.CountingFirstIntervalSMS();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
	    dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_Off();
	    dynamicjobspage.ClickOnSMSLogOkBtn();	
	    dynamicjobspage.SMSLogConfirmationMessage();
	}
	@Test(priority='D',description="To Verify Dynamic Job Status And Telecom Policy  Status While Apply Static CallLog Tracking_ON Job On Device")
	public void ToVerifyDynamicJobStatusAndTelcomPolicyStatus() throws Throwable
	{
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnApplyJobButton();
		dynamicjobspage.Applying_StaticSMSLogTracking_OnJob("SMSLogTracking_ON");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		dynamicjobspage.VerifyOfSmsLogsActivityLogsDeployed();
	//	dynamicjobspage.CheckStatusOfappliedCallLogTrackingJob();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSMSLog();
		dynamicjobspage.ToVerifyDynamicSMSTrackingJobStatus_ON();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
	    dynamicjobspage.ClickOnTelecomManagementEditButton();
	    dynamicjobspage.ClickOnTelecomManagementSMSLogTrackingButton();
		dynamicjobspage.ToVerifyTelecomManagementSMSLogStatus_ON();
	}
	@Test(priority='E',description="To Verify Dynamic SMS Log Tracking status When SMS Log Tracking Status Turned Off In Telecom Management")
	public void VerifyDynamicSMSLogTrackingStatus() throws Throwable
	{
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnTelecomManagementEditButton();
		dynamicjobspage.ClickOnTelecomManagementSMSLogTrackingButton();
		dynamicjobspage.Turning_OFFTelecomManagementSMSLogTracking();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSMSLog();
		dynamicjobspage.ToVerifyDynamicJobSMSStatus_OFF();
		dynamicjobspage.ClickOnSMSLogCloseBtn();
	}
	@Test(priority='F',description="To Verify Telecom Management Call Log Tracking Status When Dynamic Call Log Tracking Is On")
	public void VerifyTelecomManagementSMSLogTrackingStatus() throws Throwable
	{
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSMSLog();
		dynamicjobspage.SelectSMSLogStatus_On();
		dynamicjobspage.ClickOnSMSLogOkBtn();	
	    dynamicjobspage.SMSLogConfirmationMessage();
	    dynamicjobspage.ClickOnTelecomManagementEditButton();
	    dynamicjobspage.ClickOnTelecomManagementSMSLogTrackingButton();
	    dynamicjobspage.ToVerifyTelecomManagementSMSLogStatus_ON();
	}
	@Test(priority='G',description="To Verify SMS Log Deatils In Dynamic Job SMS Tracking For Second Interval")
	public void VerifyingCallDetails_AllSectionSecondInterval() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.WaitForInterval();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSMSLog();
    	dynamicjobspage.CountingSecondIntervalSMS();
    	dynamicjobspage.ToVerifySMSLogsForSeconfInterval();
    	dynamicjobspage.ClickOnSMSLogCloseBtn();
	}
	 
	@AfterMethod
	public void aftermethod(ITestResult result) throws InterruptedException, Throwable
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			Utility.captureScreenshot(driver, result.getName());
		}
	}
}
