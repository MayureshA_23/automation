package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class LockScreen_Knox extends Initialization{
	@Test(priority=1, description="create a runscript to create App shortcut on home screen")//add last
	public void ChangeLockScreenWallpaper_TC_AJ_172() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.ClickOnChangeLockScreenWallPaper();
		runScript.ClickOnValidateButton();
		runScript.ErrorMessageToFillPackageName();
		runScript.sendPackageNameORpath(Config.ImageForLockScreen);
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("ChangeLockWallpaper");
		commonmethdpage.ClickOnHomePage();
		androidJOB.clickOnAllDevices();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ChangeLockWallpaper");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.RunScriptChangeLockWallpaper,Config.TimeToDeployRunscriptJob);
		Reporter.log("1.To Verify change Lock screen wallpaper ",true);
		// check manully if the wallpaper is changed.
	}
	@Test(priority=2, description="create a runscript to create App shortcut on home screen") //putLast verify priority
	public void ToResetLockScreenWallpaperOnTheDeviceScreen_TC_AJ_173() throws InterruptedException, IOException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.clickOnResetLockWallpaper();
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("ResetLockScreenWallPaper");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ResetLockScreenWallPaper");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.RunScriptResetLockScreenWallPaper,Config.TimeToDeployRunscriptJob);
	}	

}
