package Profiles_Windows_Testscripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class ExchangeActiveSync  extends Initialization{
	
	@Test(priority='1',description="To verify clicking on Exchange ActiveSync") 
    public void VerifyClickingOnExchangeActiveSync() throws InterruptedException
	{
        Reporter.log("1.To verify clicking on Exchange ActiveSync",true);
        profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
        profilesWindows.ClickOnExchangeActiveSyncPolicy();
    }
	@Test(priority='2',description="To Verify warning message Saving Exchange ActiveSync profile without profile Name")
	public void VerifyWarningMessageOnSavingExchangeActiveSyncProfileWithoutName() throws InterruptedException{
		Reporter.log("\n2.To Verify warning message Saving Exchange ActiveSync profile without profile Name",true);
		profilesWindows.ClickOnConfigureButton_ExchangeActiveSyncProfile();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='3',description="To Verify warning message Saving Exchange Active Sync Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingExchangeActiveSyncProfileWithoutAllFielfs() throws InterruptedException{
		Reporter.log("\n3.To Verify warning message Saving Exchange Active Sync Profile without entering all the fields",true);
		profilesWindows.EnterExchangeActiveSyncProfileName();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	@Test(priority='4',description="To Verify launching Active Sync Window")
	public void VerifyLaunchingActiveSyncWindow() throws InterruptedException
	{
		Reporter.log("\n4.To Verify launching Active Sync Window",true);
		profilesWindows.ClickOnAddButton_ExchangeActiveSync();
	}
	
	@Test(priority='5',description="To Verify the parameters of Exchange Active Sync Window")
	public void VerifyParamentersOfExchangeActiveSyncWindow() throws InterruptedException{
		Reporter.log("\n5.To Verify the parameters of Exchange Active Sync Window",true);
	    profilesWindows.VerifyAlltheParametersOfExchangeActiveSyncWindow();
	}
	@Test(priority='6',description="To Verify the warning message and warning toast of the parameters without thier values")
	public void VerifyWarningMessageAndWarningToastsOfParamentersOfExchangeActiveSyncWindow() throws InterruptedException{
		Reporter.log("\n6.To Verify the warning message and warning toast of the parameters without thier values",true);
        profilesWindows.ClickOnAddButton_ExchangeActiveSyncWindow();
        profilesWindows.WarningMessageWithoutOutgoing_MailConfiguration();// common 
		profilesWindows.VerifyAlltheWarningToastsOfTheParametersWithoutTheirValue();
		
	}
	@Test(priority='7',description="To Verify the default values of 'Sync Schedule' and 'Days To Sync'")
	public void VerifyDefaultValuesOfSyncScheduleAndDaysToSync() throws InterruptedException{
		Reporter.log("\n7To Verify the default values of 'Sync Schedule' and 'Days To Sync'",true);
		profilesWindows.verifyDefaultValuesofSyncScheduleAndDaysToSync();
	}
	
	@Test(priority='8',description="1.To Verify if the 'use SSL' is enabled by default in Exchange ActiveSync Window")
	public void VerifyifUseSSLCheckBoxisEnabledinExchangeActiveSyncWindow() throws InterruptedException{
		Reporter.log("\n8.To Verify if the 'use SSL' is enabled by default in Exchange ActiveSync Window",true);
		profilesWindows.VerifyIfUseSSLEnabledByDefault();
	}
	
	@Test(priority='9',description="1.To Verify cancelling the Exchange ActiveSync Window")
	public void VerifyCancellingExchangeActiveSyncWindow() throws InterruptedException{
		Reporter.log("\n9.To Verify cancelling the Exchange ActiveSync Window",true);
		profilesWindows.ClickOnCancelButton_ExchangeActiveSync();
	}

	
	@Test(priority='A',description="1.To Verify Adding Exchange ActiveSync profile")
	public void VerifyAddingExchangeActiveSyncProfile() throws InterruptedException{
		Reporter.log("\n10.To Verify Adding Exchange ActiveSync profile",true);
		profilesWindows.ClickOnAddButton_ExchangeActiveSync();
		profilesWindows.EnterValidEmailAddressExchangeActiveSync();
		profilesWindows.EnterExchangeActiveSyncDetails();
		profilesWindows.ClickOnAddButton_ExchangeActiveSyncWindow();
	}
	@Test(priority='B',description="1.To Verify Saving ExchangeActiveSync profile")
	public void VerifySavingExchangeActiveSyncWindowsProfile() throws InterruptedException{
		Reporter.log("\n11.To Verify Saving ExchangeActiveSync profile",true);
	    profilesWindows.ClickOnSaveButton();
		profilesWindows.NotificationOnProfileCreated();
	}
	
	// edit, delete pending
	
	

}
