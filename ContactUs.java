package Settings_TestScripts;


import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class ContactUs extends Initialization {

	@Test(priority=1,description="1.To Verify Contact Us window  UI")
	public void VerifyContactusPage() throws InterruptedException{
		Reporter.log("\n1.To Verify Contact Us window Open in New Tab",true);
		commonmethdpage.ClickOnSettings(); // generally it should be commneted if before suite
		contactUsPage.ClickOnContactus();
		
	}
}
