package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class AnyOs_Job extends Initialization {
	
//	@Test(priority=1,description="Move Device to Group - Verify moving device to any group which has a default job by applying move device to group job")
//    public void CreateGroup() throws InterruptedException, IOException
//    {
//		groupspage.ClickOnNewGroup();
//		groupspage.EnterGroupName(Config.AnyOsJobGrpName);
//		groupspage.ClickOnNewGroupOkBtn();
//		groupspage.ClickOnHomeGrup();
//}
//	@Test(priority=2,description="Move Device to Group - Verify moving device to any group which has a default job by applying move device to group job")
//    public void AddDefaultJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.clickOnFileTransferJob();
//		androidJOB.enterJobName(Config.JobNew);
//		androidJOB.clickOnAddInstallApplication();
//		androidJOB.browseFileTRansferJobTAGS("./Uploads/UplaodFiles/FileStore/\\Image.exe");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//
//    
//		groupspage.ClickOnGroup(Config.AnyOsJobGrpName);
//
//		groupspage.ClickOnGrpProperties();
//		groupspage.ClickOnAddPropertiesBtn();
//		groupspage.SearchJob(Config.JobNew);
//		groupspage.DefaultJobSelect2();
//		groupspage.OkBtnDefaultJob_Click3();
//		groupspage.grouppropertiesokbtnClick();
//
//		groupspage.ClickOnHomeGrup();   
//   }
//	@Test(priority=3,description="Move Device to Group - Verify moving device to any group which has a default job by applying move device to group job")
//    public void CreateMoveToJob()throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//
//        androidJOB.clickOnJobs();
//        androidJOB.clickNewJob();
//        androidJOB.ClickOnAnyOs();
//        androidJOB.ClickOnMoveDeviceJob();
//        androidJOB.SendingJobName("MoveDeviceToGroup");
//        androidJOB.Entermovedevicegroupname();
//        androidJOB.SearchGrp(Config.AnyOsJobGrpName);
//        androidJOB.SelectGroupFromGroupList();              
//        androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("MoveDeviceToGroup");
//		androidJOB.JobInitiatedMessage();
//		androidJOB.CheckingJobStatusUsingConsoleLogs("MoveDeviceToGroup");
//		
//
//		groupspage.ClickOnGroup(Config.AnyOsJobGrpName);
//		androidJOB.ClearSearchFieldConsole();
//        androidJOB.SearchDeviceInconsole(Config.DeviceName);
//        deviceinfopanelpage.VerifyDeviceNameAndroid();
//		androidJOB.CheckStatusOfappliedInstalledJob(Config.JobNew,360);
//       
////		deviceinfopanelpage.refesh();
////		groupspage.ClickOnGroup(Config.AnyOsJobGrpName);
////		androidJOB.ClearSearchFieldConsole();
////        androidJOB.SearchDeviceInconsole(Config.DeviceName);
////
////		groupspage.ClickOnDeleteGroup();
////		androidJOB.DeleteGroup();  
//}

	//Mithilesh
	@Test(priority=4,description="Verify deploying compliance job for both online and Offline devices.") 
	public void VerifydeployingcompliancejobforbothonlineandOfflinedevicesTC_JO_849() throws Throwable {
		Reporter.log("\n4.Verify deploying compliance job for both online and Offline devices.",true);		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();		
		androidJOB.clickOnANYOS();
		androidJOB.clickOnCompliancejobAnyOS();
		androidJOB.CompliancejobAnyOSJobName("Trials OS Version Job");
		androidJOB.CompliancejobAnyOSCreateJob();
		androidJOB.ComplianceActionSendMessage("Send Message");
		androidJOB.JobCreatedMessage();	
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.WindowComplianceJob);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Trials OS Version Job"); 
		androidJOB.WindowsCopyGeniusActivityLog();			
}





}