package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class UsermangemengtPermissions extends Initialization {
	@Test(priority='1',description="1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException{
	Reporter.log("\n1.To Verify roles option is present",true);
	commonmethdpage.ClickOnSettings();
	usermanagement.ClickOnUserManagementOption();
	
	
	}
	@Test(priority='2',description="2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException{
	Reporter.log("\n2.To Verify default super user",true);

	usermanagement.toVerifySuperUsertemplete();
	}
	@Test(priority='3',description="3.To create a roll for UserManagemnet Permission")
	public void CreateRollForUsermanagement() throws InterruptedException{
		Reporter.log("\n3.To create a roll for UserManagemnet Permission",true);
	
		usermanagement.CreateRoleUsermanagemnetPermission();
		Thread.sleep(7000);
	}
	@Test(priority='4',description="4.To create User for Device Permissions")
	public void CreateUserForUsermanagement() throws InterruptedException{
		
		Reporter.log("\n4.To create User for Device Permissions",true);
	
		usermanagement.CreateUserForPermissions("AutoUsermanagementPermission", "42Gears@123", "AutoUsermanagementPermission", "Usermanagement permission");  // uncomment remove later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoUsermanagementPermission", "42Gears@123");


		
	}
	@Test(priority='5',description="5.To create a roll for UserManagemnet Permission")
	public void AddUserEnabled() throws InterruptedException{
		Reporter.log("\n5.Add user Accessed allowed",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.UsermanagementAccess();
		usermanagement.CreateUserForPermissions("bently20", "42Gears@123", "bently20", "Usermanagement permission");
		commonmethdpage.ClickOnSettings();
		usermanagement.UsermanagementAccess();
		Thread.sleep(7000);
	}
	
	@Test(priority='6',description="3.To create a role for UserManagemnet Permission")
	public void EditUserEnabled() throws InterruptedException{
		Reporter.log("\n6.Edit user Accessed allowed",true);
		usermanagement.EditOptionEnabled();
		Thread.sleep(7000);
	}
	@Test(priority='7',description="7.To create a roll for UserManagemnet Permission")
	public void DeleteUserEnabled() throws InterruptedException{
		Reporter.log("\n7.delete user Accessed allowed",true);	
		usermanagement.DeleteEnable();
		Thread.sleep(7000);
	}
	@Test(priority='8',description="8.To create a roll for UserManagemnet Permission")
	public void ChangePasswordEnabled() throws InterruptedException{
		Reporter.log("\n8.Edit user Accessed allowed",true);
		
		usermanagement.ChangePasswordUserDisabled();
		Thread.sleep(7000);
	}
	@Test(priority='9',description="9.To create a roll for UserManagemnet Permission")
	public void UserEnableverifyUsermanagent() throws InterruptedException{
		Reporter.log("\n9.Edit user Accessed allowed",true);
		usermanagement.EnabaleDisableUser();
		usermanagement.UserEnable();
		Thread.sleep(7000);
	}
	@Test(priority='A',description="10.To create a roll for UserManagemnet Permission")
	public void DisabledUserPermissions() throws InterruptedException{
		Reporter.log("\n10.Edit user Accessed allowed",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID ,Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.DisablingUserManagemnetPermission();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoUsermanagementPermission", "42Gears@123");
		commonmethdpage.ClickOnSettings();
		usermanagement.UserManagementOptionPresentOrNot();
		
	}
	
}
