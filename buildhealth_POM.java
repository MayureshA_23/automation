package PageObjectRepository;


import java.sql.Driver;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class buildhealth_POM extends WebDriverCommonLib {
  
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	WebDriverWait wait1=new WebDriverWait(Initialization.driver,30);
	
	@FindBy(id="remoteButton")
	private WebElement RemoteSupportBtn;
	public void ClickOnRemoteSupport() throws InterruptedException{
		RemoteSupportBtn.click();
		sleep(5);
	}
	String parentwinID;
	String childwinID;
	String URL;
	WebDriverWait wait = new WebDriverWait(Initialization.driver,30);
	public void windowhandles()
	{
		Set<String> set=Initialization.driver.getWindowHandles();
	    Iterator<String> id=set.iterator();
	    parentwinID =id.next();
	    childwinID =id.next();
	}
	public void SwitchToRemoteWindow(){
		Initialization.driver.switchTo().window(childwinID);
		URL=Initialization.driver.getCurrentUrl();
	}
	
	@FindBy(xpath="//div[@id='fountainG']")
	private WebElement LoadingBarOfRemoteSupport;
	
	@FindBy(xpath="//label/input")
	private WebElement FolderNameTitle;
	public void VerifyOfRemoteSupportLaunched() throws InterruptedException
	{
		try
		{
		while(LoadingBarOfRemoteSupport.isDisplayed())
		{
			
		}
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='rexplorer']/div[1]/ul/li[4]")));
	//	waitForXpathPresent("//div[@id='rexplorer']/div[1]/ul/li[4]");
		sleep(8);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		Reporter.log("Remote support launched "+dateFormat.format(date),true);
		}
		catch(Exception e)
		{
			WebElement ErrorMessage = Initialization.driver.findElement(By.xpath("//p[@id='errorMessage']"));
			if(ErrorMessage.isDisplayed())
			{
				String FailMessage = "Device went offline. Cannot establish remote connection. Please retry when device comes online.";
				ALib.AssertFailMethod(FailMessage);
			}
		}
	}
public void DeviceNameVerification(String deviceName )
	{
		boolean devicenamevalue = Initialization.driver.findElement(By.xpath("//div[text()='Device: "+deviceName+"']")).isDisplayed();
		
			String PassStatement1 = "PASS >> device name is displayed as expected ";
			String FailStatement1 = "FAIL >> device name is not displayed as expected";
			ALib.AssertTrueMethod(devicenamevalue, PassStatement1, FailStatement1);

	}

public void DeviceNameVerification1(String deviceName )
{
	boolean devicenamevalue = Initialization.driver.findElement(By.xpath("//div[text()='Device: "+deviceName+"']")).isDisplayed();
	
		String PassStatement1 = "PASS >> device name is displayed as expected ";
		String FailStatement1 = "FAIL >> device name is not displayed as expected";
		ALib.AssertTrueMethod(devicenamevalue, PassStatement1, FailStatement1);

}
	@FindBy(xpath="//li[@title='New folder']")
	private WebElement CreateFolder;
	
	@FindBy(xpath="//li[@title='Reload']")
	private WebElement Reload;
	
	public void resumeButtonDisplayed()
	{
		boolean value=true;
		try{
		Initialization.driver.findElement(By.xpath(".//*[@id='dimiss_pause']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		Assert.assertTrue(value,"FAIL: 'Resume button not displayed'");
		  Reporter.log("PASS>> 'Resume button is displayed",true );	
	}
	public void CreateFolder(){
		
		 try{
			 for(int i=0;i<7;i++)
			    {
			   CreateFolder.click();//Click on New Folder
			   sleep(2);
			   Reporter.log("New Folder created successfully",true);
			   waitForXpathPresent("//div[@id='rexplorer']/div[1]/ul/li[4]");
			    }
		
        }catch(Exception e)
		 { 
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage")));
				String reconnecting = wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage"))).getText();
				DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				final String start = dateFormat1.format(new Date());
				Reporter.log(start, true);
				Reporter.log("Reconnecting issue : " + reconnecting, true);
				try {
					while (Initialization.driver.findElement(By.id("reconnectingMessage")).isDisplayed()) {
					}
				} catch (Exception e1) {
					String end = dateFormat1.format(new Date());
					Reporter.log(end, true);
					Reporter.log("Reconnecting stopped ", true);
				}
			} catch (Exception e1) {
				
			}
		}
	System.out.println();
}
	@FindBy(xpath="//li[@title='Back']")
	private WebElement Back;
	public void OpenFolder(){
		
		 try{
			 for(int i=0;i<1;i++)
			    {
				WebElement wb = Initialization.driver.findElement(
						By.xpath("//div[label[text()='" + ELib.getDatafromExcel("Sheet1",4, 4) + "']]"));
				Actions action = new Actions(Initialization.driver);
				action.moveToElement(wb).doubleClick().build().perform();
				sleep(3);
				System.out.println("Folder opened successfully");
				sleep(1);
				Back.click();
			    }
		
       }catch(Exception e)
		 {
       	try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage")));
				String reconnecting = wait
						.until(ExpectedConditions.presenceOfElementLocated(By.id("reconnectingMessage"))).getText();
				DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				final String start = dateFormat1.format(new Date());
				Reporter.log(start, true);
				Reporter.log("Reconnecting issue : " + reconnecting, true);
				try {
					while (Initialization.driver.findElement(By.id("reconnectingMessage")).isDisplayed()) {
					}
				} catch (Exception e1) {
					String end = dateFormat1.format(new Date());
					Reporter.log(end, true);
					Reporter.log("Reconnecting stopped ", true);
				}
			} catch (Exception e1) {
				
			}
  	}
	}
	public void SwitchToMainWindow() throws InterruptedException{
		Initialization.driver.close();
		Initialization.driver.switchTo().window(parentwinID);
		sleep(5);
	}
}
