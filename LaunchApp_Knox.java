package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class LaunchApp_Knox extends Initialization{
	
	@Test(priority=1, description="Launch Application On Device By PackageName TC_AJ_189") 
	public void LaunchApplicationOnDeviceByPackageName_TC_AJ_189() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnLaunchApplictionByPackageOrAppName();
		runScript.sendPackageNameORpath(Config.Launch_App_PackageNanme);
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("LaunchByPackage");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.alc.filemanager","com.alc.filemanager.activities.MainActivity");
		androidJOB.ClickOnHomeButtonDeviceSide();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("LaunchByPackage");
        androidJOB.CheckStatusOfappliedInstalledJob("LaunchByPackage",60);
        dynamicjobspage.ClearDataDownLoadedApp();
		androidJOB.ClickOnHomeButtonDeviceSide();
		
	}
	@Test(priority='2', description="Launch Application On Device By App Name_TC_AJ_190") 
	public void LaunchApplicationOnDeviceByAppName_TC_AJ_190() throws InterruptedException, IOException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnLaunchApplictionByPackageOrAppName();
		runScript.sendPackageNameORpath(Config.Launch_App_AppName);
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("LaunchByAppName");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.alc.filemanager","com.alc.filemanager.activities.MainActivity");
		androidJOB.ClickOnHomeButtonDeviceSide();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("LaunchByAppName");
	    androidJOB.CheckStatusOfappliedInstalledJob("LaunchByAppName",60);
	    dynamicjobspage.ClearDataDownLoadedApp();
		androidJOB.ClickOnHomeButtonDeviceSide();
	}	
	
}
