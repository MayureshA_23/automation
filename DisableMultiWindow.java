package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DisableMultiWindow extends Initialization{
	
	@Test(priority=1, description="On deploying job to the device Multi-Window option should be disabled on device.")
	public void ToDisableMultiWindowOption_TC_AJ_224() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
 		runScript.clickOnMultiWindowMode();
 		runScript.sendPackageNameORpath("false");
 		runScript.ClickOnValidateButton();
 		runScript.ScriptValidatedMessage();
 		runScript.ClickOnInsertButton();
    	runScript.RunScriptName("DisableMultiWindow");
 		commonmethdpage.ClickOnHomePage();
 		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableMultiWindow");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("DisableMultiWindow",60);
		runScript.OpenSettingsPage();
		runScript.clickOnAdvancedSettings();
		runScript.CheckMultiWindowIsDisabled();
		
	}
	@Test(priority=2, description="On deploying job to the device Multi-Window option should be enabled on device")
	public void ToEnableMultiwindowOption_TC_AJ_225() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
 		runScript.clickOnMultiWindowMode();
 		runScript.sendPackageNameORpath("True");
 		runScript.ClickOnValidateButton();
 		runScript.ScriptValidatedMessage();
 		runScript.ClickOnInsertButton();
    	runScript.RunScriptName("EnableMultiWindow");
 		commonmethdpage.ClickOnHomePage();
 		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableMultiWindow");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("EnableMultiWindow",60);
		runScript.OpenSettingsPage();
		runScript.clickOnAdvancedSettings();
		runScript.CheckMultiWindowEnable();
		
		
	}
}
