package Groups_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import PageObjectRepository.Groups_POM;

public class Groups extends Initialization {

	@Test(priority = 1, description = "1.To Verify Creating a new group")
	public void Creatsingnewgroup_TC1() throws InterruptedException {
		Reporter.log("1.To Verify CreatingNewgroup", true);
		groupspage.ClickOnNewGroup();
		groupspage.Verifynewgroupheading();
		groupspage.EnterGroupName("test group");
		groupspage.ClickOnNewGroupOkBtn();
		Reporter.log("PASS >> Verification of Creation of new group", true);

	}

	@Test(priority = 2, description = "2.To Verify Creating a new group with special characters")
	public void Creatinggroupwithspecialcharacters_TC2() throws InterruptedException {
		Reporter.log("2.To Verify CreatingNewgroup with special characters", true);
		// groupspage.ClickOnHomeGrup();
		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName("group!@#$%%^^&&*");
		groupspage.ClickOnNewGroupOkBtn();
		Reporter.log("PASS >> Verification of Creation of new group with special characters", true);

	}

	@Test(priority = 3, description = "3.To Verify Creating a new group with group name empty")
	public void Creatinggroupwithgroupnameempty_TC3() throws InterruptedException {
		Reporter.log("3.To Verify Creating New group with groupname empty", true);
		groupspage.ClickOnNewGroup();
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.verifyGroupErrorMessage();
		groupspage.ClickOnNewGroupCloseBtn();
		Reporter.log("PASS >> Verification of Creation of new group with group name empty", true);
	}

	@Test(priority = 4, description = "4.To Verify Renaming existing group and verify the heading")
	public void Renaminggroup_TC4() throws InterruptedException {
		Reporter.log("4.To Verify Renaming group", true);
		groupspage.ClickOnGroup("test group");
		groupspage.ClickOnRenameGroup();
		groupspage.VerifyRenamegroupheading();
		groupspage.EnterGroupName("Renamed test group");
		groupspage.ClickOnNewGroupOkBtn();
		Reporter.log("PASS >> Renaming group", true);
	}

	@Test(priority = 5, description = "5.To Verify Renaming existing group with group name empty")
	public void Renaminggroupwithgroupnameempty_TC5() throws InterruptedException {
		Reporter.log("5.To Verify Renaming group with groupname empty", true);
		groupspage.ClickOnRenameGroup();
		groupspage.EnterGroupName("");
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.verifyGroupErrorMessage();
		groupspage.ClickOnRenameGroupCloseBtn();
		Reporter.log("PASS >> Verification of Renaming group with group name empty", true);
	}

	@Test(priority = 6, description = "6.To Verify creating sub groups")
	public void creatingsubgroups_TC6() throws InterruptedException {
		Reporter.log("6.To Verify CreatingSubgroup", true);
		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName("sub group");
		groupspage.ClickOnNewGroupOkBtn();
		Reporter.log("PASS >> Verification of creation of subgroup", true);

	}

	@Test(priority = 7, description = "7.To Verify Renaming sub groups")
	public void Renamingsubgroups_TC7() throws InterruptedException {
		Reporter.log("7.To Verify RenamingSubgroup", true);
		groupspage.ClickOnRenameGroup();
		groupspage.EnterGroupName("Renamed sub group");
		groupspage.ClickOnNewGroupOkBtn();
		Reporter.log("PASS >> Verification of Renaming of subgroup", true);

	}
	
	@Test(priority = 8, description = "8.To Verify Deleting subgroup")
	public void Deletingsubgroup_TC8() throws InterruptedException {
		Reporter.log("8.To Verify deleting a group", true);
		// groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup("Renamed test group");
		groupspage.ClickOnGroup("Renamed sub group");
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
		Reporter.log("PASS >> Verification of deleting sub group", true);
	}

	@Test(priority = 9, description = "9.To Verify Deleting main groups")
	public void Deletingmaingroups_TC9() throws InterruptedException {
		Reporter.log("9.To Verify deleting a main group", true);
		groupspage.ClickOnGroup("Renamed test group");
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
		Reporter.log("PASS >> Verification of deleting main group", true);
	}

	// ************************NEW CASES **********************************

	@Test(priority = 10, description = "Verify selecting a home group and disabled options")
	public void Groups_TC_GR_007() throws InterruptedException {

		groupspage.VerifyRenameByrightclickforhomegroup();
		groupspage.VerifySelectingHomeGroupAndDisabledOptionsForRenameOption();
		groupspage.VerifySelectingHomeGroupAndDisabledOptionsForDeleteOption();

	}

	@Test(priority = 11, description = "verify applying job to sub group")
	public void GroupsTC_GR_026()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.FileTransferJobName);
		androidJOB.clickOnAdd();

		androidJOB.browseFile("./Uploads/UplaodFiles/Job On Android/FileTransferSingle/\\File1.exe");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.verifyFileTransferJobCreation();

		commonmethdpage.ClickOnHomePage();

		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName(Config.SourceGroupName);
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.ClickOnHomeGrup();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		accountsettingspage.SelectMultipleDevice();
		accountsettingspage.RightClickOndeviceInsideGroups(Config.DeviceName);
		accountsettingspage.MoveToGroupMultiple1();
		groupspage.ClickOnApplyGroups();
		groupspage.SearchTextField(Config.FileTransferJobName);

		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.FileTransferJobName, 360);
		groupspage.clickOnGrpJobQeue();
		groupspage.VerifyJobCompletedJobSectionGroupRtClick();
		groupspage.CloseBtnRtClickJobHistory();
	}


	@Test(priority = 12, description = "verify success column in the group job queue")
	public void GroupsTC_GR_030()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {
		commonmethdpage.ClickOnHomePage();

		accountsettingspage.RightClickOndeviceInsideGroups1(Config.SourceGroupName);
		groupspage.ClickOnJobHistoryOptnRtClickOnGroup();
		groupspage.VerifyJobCompletedJobSectionGroupRtClick();
		groupspage.CloseBtnRtClickJobHistory();
	}

	@Test(priority = 13, description = "Verify Failed column in the Group job queue")
	public void GroupsTC_GR_031()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		commonmethdpage.ClickOnHomePage();

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		quickactiontoolbarpage.CreatingSureFoxSettingsJob(Config.FailJob);
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		groupspage.ClickOnApplyGroups();
		groupspage.SearchTextFieldFailJob(Config.FailJob);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error", Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobErrorStateSymbol);
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();

		groupspage.clickOnGrpJobQeue();
		groupspage.VerifyJobFailedJobSectionGroupRtClick();
		groupspage.VerifyInsideJobQeueFailSection();
		groupspage.CloseBtnRtClickJobHistory();

	}

	@Test(priority = 14, description = "verify Retry button in the Failed column ")
	public void GroupsTC_GR_032()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		commonmethdpage.ClickOnHomePage();

		accountsettingspage.RightClickOndeviceInsideGroups1(Config.SourceGroupName);
		groupspage.ClickOnJobHistoryOptnRtClickOnGroup();
		groupspage.VerifyJobFailedJobSectionGroupRtClick();
		groupspage.SelectFailJob();
		groupspage.ClickOnRetryBtn();
		groupspage.CloseBtnRtClickJobHistory();
	}

	@Test(priority = 15, description = "Verify Applying job to the parent group when subgroups are present.")
	public void GroupsTC_GR_033()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		commonmethdpage.ClickOnHomePage();
		deviceinfopanelpage.refesh();

		groupspage.ClickOnGroup(Config.SourceGroupName);

		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName(Config.SubGroupDevice);
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.ClickOnHomeGrup();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsoleGroups(Config.MultipleDevices1);
		rightclickDevicegrid.RightClickOndevice(Config.MultipleDevices1);
		accountsettingspage.MoveToGroupMultipleSubGroup();
		groupspage.ClickOnGroup(Config.SourceGroupName);
		groupspage.ClickOnApplyGroups();
		groupspage.SearchTextField(Config.FileTransferJobName);
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.FileTransferJobName, 360);
		groupspage.clickOnGrpJobQeue();
		groupspage.VerifyJobCompletedJobSectionGroupRtClick();
		groupspage.CloseBtnRtClickJobHistory();
		groupspage.ClickOnGroup(Config.SubGroupDevice); // Device should be present inside the subgroup
		groupspage.clickOnGrpJobQeueSubGroup();
		groupspage.VerifyJobCompletedJobSectionGroupRtClick();
		groupspage.CloseBtnRtClickJobHistory();
	}

	@Test(priority = 16, description = "Verify default job when device get enrolled to Sub group")
	public void GroupsTC_GR_039()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		commonmethdpage.refesh();

		commonmethdpage.ClickOnHomePage();

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.JobNew);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileTRansferJobTAGS("./Uploads/UplaodFiles/FileStore/\\Image.exe");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();

		groupspage.ClickOnGroup(Config.SourceGroupName);
		groupspage.ClickOnDropDownGroups(Config.SubGroupDevice);
		groupspage.ClickOnGroup(Config.SubGroupDevice);
		groupspage.ClickOnGrpProperties();
		groupspage.ClickOnAddPropertiesBtn();
		groupspage.SearchJob(Config.JobNew);
		groupspage.DefaultJobSelect2();
		groupspage.OkBtnDefaultJob_Click2();
		groupspage.grouppropertiesokbtnClick();

		groupspage.ClickOnHomeGrup();
		androidJOB.ClearSearchFieldConsole();

		commonmethdpage.SearchDeviceInconsole();
		devicegridpage.RefreshDeviceGrid();

		accountsettingspage.SelectMultipleDevice();
		accountsettingspage.RightClickOndeviceInsideGroups(Config.DeviceName);
		accountsettingspage.MoveToGroupMultipleSubGroup();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.JobNew, 360);
		groupspage.clickOnGrpJobQeueSubGroup();
		groupspage.VerifyJobCompletedJobSectionGroupRtClick2();
		groupspage.CloseBtnRtClickJobHistory();
		groupspage.ClickOnHomeGrup();

	}

	@Test(priority = 17, description = "Verify job apply on group.")
	public void verifyGroupJbQueue_TC_GR_043() throws InterruptedException, IOException {
		Reporter.log("Verify job apply on group", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("0TextMessage", "0text", "Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		commonmethdpage.ClickOnAllDevicesButton();
		rightclickDevicegrid.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.AutoGroup);
		rightclickDevicegrid.VerifyDeviceMoved();
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField(Config.TextMessageJobname);
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.VerifyJobInsideTab(Config.TextMessageJobname, "Pending");
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,
				Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		androidJOB.verifyReadMessage("Subject: 0text");
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("Completed", 12);
		quickactiontoolbarpage.VerifyJobInsideTab(Config.TextMessageJobname, "Completed");
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnDeleteGrpBtn_MoveDeviceToHome();
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField(Config.TextMessageJobname);
		androidJOB.ClickOnOnJob(Config.TextMessageJobname);
		androidJOB.deleteCreatedJob();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 18, description = "Verify Success message on the console for 'Move to Group'.")
	public void verifySuccessMsgMoveToGroup() throws InterruptedException {
		Reporter.log("Verify Success message on the console for 'Move to Group'.", true);
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup1);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnMoreButton();
		groupspage.clickOnMoveToGroupSuccessMsg(Config.AutoGroup1);
		// Delete grp
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup1);
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
	}

	@Test(priority = 19, description = "Verify Failure message on the console for 'Move to Group'.")
	public void verifyFailureMsgMoveToGroup() throws InterruptedException {
		Reporter.log("Verify Failure message on the console for 'Move to Group'.", true);
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnMoreButton();
		groupspage.clickOnMoveToGroupFailureMsg(Config.homeGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
	}

	@Test(priority = 20, description = "Verify by click No on job apply pop up.")
	public void verifyNoOnJobApply() throws InterruptedException {
		Reporter.log("Verify by click No on job apply pop up.", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("0TextMessage", "0text", "Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		commonmethdpage.ClickOnAllDevicesButton();
		rightclickDevicegrid.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.AutoGroup);
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField(Config.TextMessageJobname);
		quickactiontoolbarpage.ClickOnApplyJobNoButton_Group();
		quickactiontoolbarpage.VerifyJobNoPresentInsideTab(Config.TextMessageJobname);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnDeleteGrpBtn_MoveDeviceToHome();
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField(Config.TextMessageJobname);
		androidJOB.ClickOnOnJob(Config.TextMessageJobname);
		androidJOB.deleteCreatedJob();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 21, description = "Verify the Functionality of \"Move to Group\" in Groups")
	public void verifyMotoToGroupFunc() throws InterruptedException {
		Reporter.log("Verify the Functionality of \"Move to Group\" in Groups", true);
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup1);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnMoreButton();
		groupspage.clickOnMoveToGroupSuccessMsg(Config.AutoGroup1);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup1);
		groupspage.verifyMoveToGroupFunc(Config.AutoGroup, Config.AutoGroup1);

		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup1);
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
	}

	@Test(priority = 22, description = "Verify the Functionality of \"Move to Group\" in Groups through Right Click")
	public void verifyMoveToGroup_RightClick() throws InterruptedException {
		Reporter.log("Verify the Functionality of \"Move to Group\" in Groups through Right Click", true);
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup1);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.rightClickOnGroup(Config.AutoGroup);
		groupspage.clickOnMoveToGrp_RightClick();

		rightclickDevicegrid.moveGrpToGrp_RightClick(Config.AutoGroup1);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup1);
		groupspage.verifyMoveToGroupFunc(Config.AutoGroup, Config.AutoGroup1);

		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup1);
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
	}

	@Test(priority = 23, description = "Verify apply job popup")
	public void VerifyOfApplyJobPopup_TC_GR_042() throws InterruptedException {
		Reporter.log("\n3.Verify apply job popup", true);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnApplyGroups();
		groupspage.verifyOfApplyJobGroupToGroup();
	}

	@Test(priority = 24, description = "Verify \"Move to Group\" UI in Groups")
	public void VerifyMoveToGroupUI() throws InterruptedException {

		Reporter.log("\n4. Verify Move to Group", true);
		groupspage.ClickOnHomeGrup();
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnMoreButton();
		groupspage.VerifyMoveToGroupUIFunctionality();
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnDeleteGrpBtn_MoveDeviceToHome();
	}

	@Test(priority = 25, description = "Verify Move to Group through right Click in Groups")
	public void VerifyMoveToGroupUIByRightClick() throws InterruptedException {

		Reporter.log("\n4. Verify Move to Group By RIght Click ", true);
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.VerifyMoveToGroupUIByRightClick(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnDeleteGrpBtn_MoveDeviceToHome();

	}

	@Test(priority = 26, description = "Verify the Note in Group list by CLicking More")
	public void VerifyNoteDisplayedOnClickingMore() throws InterruptedException {
		Reporter.log("\n5.Verify the Note in Group list", true);
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnMoreButton();
		groupspage.VerifyNoteDisplayedOnClickingMore();
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnDeleteGrpBtn_MoveDeviceToHome();
	}

	@Test(priority = 27, description = "Verify the Note in Group list Through Right Click")
	public void VerifyNoteDisplayedOnRightClick() throws InterruptedException {
		Reporter.log("\n5.Verify the Note in Group list", true);
		rightclickDevicegrid.VerifyGroupPresence(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.VerifyNoteDisplayedOnRightClick(Config.AutoGroup);
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGroup(Config.AutoGroup);
		groupspage.clickOnDeleteGrpBtn_MoveDeviceToHome();
	}

}
