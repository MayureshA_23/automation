package SanityTestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//TC2.Device Grid_Right click for Android-Remote------>pass any of the knox device name which is there in console
//TC6.Device Grid_Right click for Android-Reboot------>pass any of the knox device name which is there in console 
//TC7.Device Grid_Right click for Android-Delete------>pass any of the device name which is in offline state 

public class DeviceGrid_Rightclick_Android extends Initialization {

	@Test(priority = '0', description = "Device Grid_Right click for Android-Refresh")
	public void RefreshRightClick_Android() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("=====1.Device Grid_Right click for Android-Refresh=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnRefresh();
	}

	@Test(priority = '1', description = "Device Grid_Right click for Android-Remote")
	public void RemoteRightClick_Android() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("=====2.Device Grid_Right click for Android-Remote=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.VerifyClickOnRemote();
		rightclickDevicegrid.windowhandles();
		rightclickDevicegrid.SwitchToRemoteWindow();
		rightclickDevicegrid.VerifyOfRemoteSupportLaunched();
		rightclickDevicegrid.DeviceNameVerification(Config.DeviceName);
		rightclickDevicegrid.resumeButtonDisplayed();
		rightclickDevicegrid.SwitchToPrentwindow();
	}
	@Test(priority = '2', description = "Device Grid_Right click for Android-Move To Group")
	public void MoveToGroupRightClick_Android() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("=====3.Device Grid_Right click for Android-Move To Group=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		rightclickDevicegrid.clickOnGroup();
		rightclickDevicegrid.ClickonNewGroup();
		rightclickDevicegrid.CreateNewGroup(Config.GroupName);
		commonmethdpage.SearchDevice(Config.DeviceName);		
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName);
		rightclickDevicegrid.NotificationOnMoveDevice(Config.GroupName);
	}	
	@Test(priority = '5', description = "Device Grid_Right click for Android-Reboot")
	public void RebootRightClick_Android() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("=====4.Device Grid_Right click for Android-Reboot=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName1);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName1);
		rightclickDevicegrid.ClickOnReboot();
		dynamicjobspage.ClickOnRebootDeviceYesBtn();
		dynamicjobspage.VerifyOfRebootActivityLogs();
	}
	
	@Test(priority = '6', description = "Device Grid_Right click for Android-Push File")
	public void VerifyOfRightClickPushFile() throws Exception,InterruptedException, EncryptedDocumentException,InvalidFormatException, IOException, ParseException
	{
		Reporter.log("\n=====6.Device Grid_Right click for Android-Push File=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
	    rightclickDevicegrid.ClickOnPushFile();
	    rightclickDevicegrid.UploadFile(Config.FileStore_audiofile, Config.FileStore_AudioName);
		rightclickDevicegrid.ClickOnNextBtnPushFilePopup();
		rightclickDevicegrid.VerifyingSuccessMsg();
		rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
		commonmethdpage.AppiumConfigurationCommonMethod("com.mauriciotogneri.fileexplorer","com.mauriciotogneri.fileexplorer.app.MainActivity");
		commonmethdpage.commonScrollMethod("Audio.mp3");
		rightclickDevicegrid.ClickOnHomeButtonDevice();
		rightclickDevicegrid.CloseRecentApp();
		rightclickDevicegrid.ClickOnHomeButtonDevice();
	}
	@Test(priority = '7', description = "Device Grid_Right click for Android-Push Application")
	public void VerifyOfRightClickPushApplication() throws Exception,InterruptedException, EncryptedDocumentException,InvalidFormatException, IOException, ParseException
	{
		Reporter.log("\n=====7.Device Grid_Right click for Android-Push Application=====", true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickonPushApplication();
		rightclickDevicegrid.UploadAPK(Config.Pushapk1,Config.apkname1);
		rightclickDevicegrid.ClickOnNextBtnPushApplicationPopup();
	    rightclickDevicegrid.VerifyingSuccessMsg();
	    rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	    commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
	    accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.apkname1,Config.DeviceName);
		androidJOB.CheckApplicationIsInstalledDeviceSide(Config.apkpackage1);
	}
	
	@Test(priority = '9', description = "Device Grid_Right click for Android-Delete")
	public void DeleteDeviceRightClick_Android() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n=====5.Device Grid_Right click for Android-Delete=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnDelete();
		rightclickDevicegrid.WarningMessageOnDeviceDelete(Config.DeviceName);
		dynamicjobspage.ClickOnDeleteDeviceYesBtn();
		rightclickDevicegrid.clickOnPendingDelete();
		rightclickDevicegrid.VerifyOfDeletedDeviceInPendingDelete(Config.DeviceName);
	}
}
