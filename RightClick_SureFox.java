package RightClick;
import java.io.IOException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

// Make sure SureFox is not installed on the device

public class RightClick_SureFox extends Initialization {

	@Test(priority='1',description="1.To Verify SureFox Install Option when Surefox Not Installed")
	public void VerifySureFoxIsnstallOption() throws Exception {
	
		Reporter.log("\n1.To Verify SureFox Install Option when Surefox Not Installed",true);
		commonmethdpage.SearchDeviceInconsole();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureFox();
		rightclickDevicegrid.VerifyInstallOptionWhenSFnotInstalled();
		rightclickDevicegrid.ClickOnrefreshButtonDeviceInfo();
	}
	
	
	@Test(priority='2',description="2.To verify Surefox Activate Licence,Launch SureFoxOption")
	public void VerifyActivateLicenseAndLaunchSureFoxOption() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n2.To verify Surefox Activate Licence,Launch SureFoxOption",true);
		rightclickDevicegrid.InstallationSureFoxOlder();
		commonmethdpage.ClickOnDynamicRefresh();
		commonmethdpage.clickOnGridrefreshbutton();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureFox();
		rightclickDevicegrid.VerifyActivateLicenseLaunchSureFoxAndUpgradeOption();

	}
    	
	@Test(priority='3',description="3.To verify Activation Of SureFox Functionality")
	public void VerifyActivationSureFox() throws Throwable{
		Reporter.log("\n3.To verify Activation Of SureFox Functionality",true);
		rightclickDevicegrid.ClickOnRefreshDeviceGrid();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureFox();
		rightclickDevicegrid.ActivationOfSureFoxRightClick(Config.SFActivationCode);
		
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnDynamicRefresh();
	}
	@Test(priority='4',description="4.To verify Deactivate Option Surefox Settings Right Click")
	public void VerifyDeactivateOptionSureFox() throws Exception {
		
		Reporter.log("\n4.To verify Deactivate Option Surefox Settings Right Click",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureFox();
		rightclickDevicegrid.VerifyDeactivateSureFoxRightClick();
		commonmethdpage.clickOnGridrefreshbutton();
	}
	
	@Test(priority='5',description="5. To Verify Deactivation SureFox Functionality")
	public void VerifyDeactivationRightClick() throws InterruptedException {
		Reporter.log("\n5.To Verify Deactivation SureFox Functionality",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureFox();
		rightclickDevicegrid.DeactivateSureFoxRightClick();
		rightclickDevicegrid.ClickOnRefreshDeviceGrid();
	}
	
}
