package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;

public class ApplicationPermission_CustomReport extends Initialization{
	@Test(priority='0',description="Verify SureMDM Nix Permission checklist added in Custom reports.") 
	public void VerifyApplicationPermission_TC_RE_178() throws InterruptedException
		{Reporter.log("\nVerify SureMDM Nix Permission checklist added in Custom reports.",true);
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.ClickOnColumnInTable("Application Permission");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.VerifyApplicationProgram();
			customReports.ClickOnCancelButton();
		}
	@Test(priority='1',description="Verify creating and generating Custom report for SureMDM Nix Permission checklist") 
	public void VerifyApplicationPermission_TC_RE_179() throws InterruptedException
		{Reporter.log("\nVerify creating and generating Custom report for SureMDM Nix Permission checklist ",true);
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("ApplicationPermission CR with filters","test");
			customReports.ClickOnColumnInTable("Application Permission");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("ApplicationPermission CR with filters");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("ApplicationPermission CR with filters");
			customReports.VerifyingReportInViewReportSection("ApplicationPermission CR with filters");
		}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
