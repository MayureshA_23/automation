package UserManagement_TestScripts;

// Demouser must be created
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

// Account Settings role needs to be deleted

public class AccountSettingsPermission extends Initialization {

	@Test(priority = '1', description = "1.Verify expanding Account settings permissions in roles.")
	public void VerifyAvailibilityOfAccoutSettingsOptions() throws InterruptedException {
		Reporter.log("\n1.Verify expanding Account settings permissions in roles.", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.ClickOnExpandButtonAccountSettingsRoles();
		usermanagement.AvailibityOfAllFeaturesOfAccountSettings();
		usermanagement.ClickOnCloseRoleWindow();

	}

	@Test(priority = '2', description = "2.Verify selecting Account settings permissions in Roles.")
	public void VerifyAccountSettingsPermissionRoles() throws InterruptedException {
		Reporter.log("\n2.Verify selecting Account settings permissions in Roles.", true);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.ClickOnAccountSettingsPermission();
		usermanagement.EnterRolesName(Config.RoleName);
		usermanagement.EnterRoleDescriptio(Config.RolesDescription);
		usermanagement.ClickOnSaveButtonRoles();
		usermanagement.RolesCreatedSuccessfullyMessage();
		usermanagement.ClickOnUserOption();
		usermanagement.SelectDemoUser();
		usermanagement.ClickOnEditUser();
		usermanagement.SelectRolesInUser();
		usermanagement.ClickOnCreateButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.user, Config.pswd);
		commonmethdpage.ClickOnSettings();

	}

}
