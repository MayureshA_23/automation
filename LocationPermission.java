package UserManagement_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class LocationPermission extends Initialization {

	@Test(priority = '1', description = "1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException {
		Reporter.log("\n1.To Verify roles option is present", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
	}
	@Test(priority = '2', description = "2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException {
		Reporter.log("\n2.To Verify super user is present", true);
		usermanagement.toVerifySuperUsertemplete();
	}

	@Test(priority = '3', description = "3.To create roll for other permission")
	public void createRollForOtherPermission() throws InterruptedException {
		Reporter.log("\n3.To create a roll for Other permission", true);
		usermanagement.RoleWithLocationPermissions();
	}

	@Test(priority = '4', description = "4.To create User for Device Permissions")
	public void CreateUserForLocationPermission() throws InterruptedException {
		Reporter.log("\n4. User with other Permissions", true);
		usermanagement.CreateUserForPermissions("AutoLocationPermission", "42Gears@123", "AutoLocationPermission",
				"Location Permission"); // uncomment remove later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoLocationPermission", "42Gears@123");
		Thread.sleep(2000);
	}

	@Test(priority = '5', description = "3.To create role for other permission")
	public void clickOnLocate()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n5.To create a roll for Other permission", true);
		commonmethdpage.SearchDeviceInconsole();
		usermanagement.ClickOnLocateDevice();
	}

	@Test(priority = '6', description = "3.Disabling Location Permission")
	public void DisablingLocationPermission() throws InterruptedException {
		Reporter.log("\n6.To create a roll for Other permission", true);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.DisablingLocationPermission();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoLocationPermission", "42Gears@123");

	}

	@Test(priority = '7', description = "7.To create role for other permission")
	public void DisablesHistoryMOdify()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n7.To create a roll for Other permission", true);
		commonmethdpage.SearchDeviceInconsole();
		usermanagement.AccessDeniedLocationPermission();
	}

}
