package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class AlertTemplate extends Initialization{

	@Test(priority=1,description="Verify new tags are added in right side panel") 
	public void VerifynewtagsareaddedinrightsidepanelTC_ST_880() throws Throwable {
		Reporter.log("\n1.Verify new tags are added in right side panel",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.OfflineOnlineTagVisble();
}
	
	@Test(priority=2,description="Verify alert template feature in Account Settings") 
	public void VerifyalerttemplatefeatureinAccountSettingsTC_ST_266() throws Throwable {
		Reporter.log("\n2.Verify alert template feature in Account Settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.BatteryPolicyVisible();
		accountsettingspage.ConnectionPolicyVisible();
		accountsettingspage.DataUsagePolicyVisible();
		accountsettingspage.NotifywhendevicecomesonlineVisible();
		accountsettingspage.NotifywhenSIMischangedVisible();
		accountsettingspage.NotifywhendeviceisrootedorNixhasbeengrantVisible();
		accountsettingspage.InviteUsersVisible();
		accountsettingspage.ScheduleReportVisible();
		
}
	
	@Test(priority=3,description="Verify alert template feature by customizing the notification for battery policy") 
	public void VerifyalerttemplatefeaturebycustomizingthenotificationforbatterypolicyTC_ST_267() throws Throwable {
		Reporter.log("\n3.Verify alert template feature by customizing the notification for battery policy",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();			
		accountsettingspage.BatteryPolicyVisible();
		accountsettingspage.EditTextBatteryPolicy();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyDetails();
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Alert Template Battery Policy");
		//accountsettingspage.ApplyJobonDevice();
		accountsettingspage.JobApplyActivityLogs();
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectBatteryJob();		
}
	
	@Test(priority=4,description="Verify alert template feature by giving user defined notification for battery policy") 
	public void VerifyalerttemplatefeaturebygivinguserdefinednotificationforbatterypolicyTC_ST_268() throws Throwable {
		Reporter.log("\n4.Verify alert template feature by giving user defined notification for battery policy",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();			
		accountsettingspage.BatteryPolicyVisible();
		accountsettingspage.CustomizeTextBatteryPolicy();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyCustomizeDetails();
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Customize Alert Template Battery Policy");
		
		//accountsettingspage.ApplyCustomizeJobonDevice();
		accountsettingspage.JobApplyActivityLogs();
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectCustomizeBatteryJob();		
}
	
	@Test(priority=5,description="Verify alert template feature by customizing the notification for data usage policy") 
	public void VerifyalerttemplatefeaturebycustomizingthenotificationfordatausagepolicyTC_ST_269() throws Throwable {
		Reporter.log("\n5.Verify alert template feature by customizing the notification for data usage policy",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesDataUsagePolicy();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyDataUsageDetails();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Data Usage Policy");
		
		
		
		//accountsettingspage.ApplyDataUsageJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsDatausageJob();		//
}
	
	
	@Test(priority=6,description="Verify alert template feature by providing userdefined notification for data usage policy") 
	public void VerifyalerttemplatefeaturebyprovidinguserdefinednotificationfordatausagepolicyTC_ST_270() throws Throwable {
		Reporter.log("\n6.Verify alert template feature by providing userdefined notification for data usage policy",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesDataUsagePolicyUserdefined();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyDataUsageUserdefined();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Userdefined Data Usage Policy");
		
		
		
		
		//accountsettingspage.DataUsageJobonDeviceUserdefined();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsUserdefinedDatausageJob();		//
}
	
	
	@Test(priority=7,description="Verify alert template feature by customizing the notification for connection policy") 
	public void VerifyalerttemplatefeaturebycustomizingthenotificationforconnectionpolicyTC_ST_271() throws Throwable {
		Reporter.log("\n7.Verify alert template feature by customizing the notification for connection policy",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesConnectionPolicy();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyConnectionDetails();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Connection Policy");
		
		
		
		
		
		//accountsettingspage.ApplyConnectionPolicyJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsConnectionPolicyJob();		//
}
	
	
	@Test(priority=8,description="Verify alert template feature by the user defined notification for connection policy") 
	public void VerifyalerttemplatefeaturebytheuserdefinednotificationforconnectionpolicyTC_ST_272() throws Throwable {
		Reporter.log("\n8.Verify alert template feature by the user defined notification for connection policy",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesConnectionPolicyUserdefined();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyConnectionDetailsUserdefined();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Userdefined Connection Policy");
		
		
		
		
		//accountsettingspage.ApplyConnectionPolicyUserdefinedJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsConnectionPolicyJobUserdefined();	//
}
	
	@Test(priority=9,description="Verify alert template feature by customizing the notify when device comes online") 
	public void VerifyalerttemplatefeaturebycustomizingthenotifywhendevicecomesonlineTC_ST_273() throws Throwable {
		Reporter.log("\n9.Verify alert template feature by customizing the notify when device comes online",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesNotifywhendevicecomesonline();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyNotifywhenDeviceComesonline();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Notify when device comes online");
		
		
		
		//accountsettingspage.ApplyNotifywhenDevicecomesonlineJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsNotifywhendevicecomesonlineJob();	//
}
	
	@Test(priority=10,description="Verify alert template feature user defined the notify when device comes online") 
	public void VerifyalerttemplatefeatureuserdefinedthenotifywhendevicecomesonlineTC_ST_274() throws Throwable {
		Reporter.log("\n10.Verify alert template feature user defined the notify when device comes online",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesNotifywhendevicecomesonlineUserdefined();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyNotifywhenDeviceComesonlineUserdefined();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Userdefined Notify when device comes online");
		
				
		//accountsettingspage.ApplyNotifywhendevicecomesonlineUserdefinedJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsNotifywhendevicecomesonlineJobUserdefined();	//
}
	
	@Test(priority=11,description="Verify alert template feature by customizing notification when sim is changed") 
	public void VerifyalerttemplatefeaturebycustomizingnotificationwhensimischangedTC_ST_275() throws Throwable {
		Reporter.log("\n11.Verify alert template feature by customizing notification when sim is changed",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesNotifywhenSIMischanged();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyNotifywhenSIMischanged();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Notify when SIM is changed");
				
		//accountsettingspage.ApplyNotifywhenSIMischangedJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsNotifywhenSIMischanged();	//
}
	
	@Test(priority=12,description="Verify alert template feature by enerting user defined notification when sim is changed") 
	public void VerifyalerttemplatefeaturebyenertinguserdefinednotificationwhensimischangedTC_ST_276() throws Throwable {
		Reporter.log("\n12.Verify alert template feature by enerting user defined notification when sim is changed",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesNotifywhenSIMischangedUserdefined();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyNotifywhenSIMischangedUserdefined();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Userdefined Notify when SIM is changed");
		
			
		//accountsettingspage.ApplyNotifywhenSIMischangedUserdefinedJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsNotifywhenSIMischangedUserdefined();	//
}
	
	
	@Test(priority=13,description="Verify alert template feature by customizing notification Notify when device is rooted or Nix has been granted with root permission") 
	public void VerifyalerttemplatefeaturebycustomizingnotificationNotifywhendeviceisrootedorNixhasbeengrantedwithrootpermissionTC_ST_277() throws Throwable {
		Reporter.log("\n13.Verify alert template feature by customizing notification Notify when device is rooted or Nix has been granted with root permission",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesNotifywhendeviceisrooted();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyNotifywhendeviceisrooted();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Notify when device is rooted");
		
		
		
		
		//accountsettingspage.ApplyNotifywhendeviceisrootedJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsNotifywhendeviceisrooted();	//
}
	
	
	@Test(priority=14,description="Verify alert template feature by enerting user defined notification Notify when device is rooted or Nix has been granted with root permission") 
	public void VerifyalerttemplatefeaturebyenertinguserdefinednotificationNotifywhendeviceisrootedTC_ST_278() throws Throwable {
		Reporter.log("\n14.Verify alert template feature by enerting user defined notification Notify when device is rooted or Nix has been granted with root permission",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAlertTemplate();	
		accountsettingspage.AlertTemplatesNotifywhendeviceisrootedUserdefined();//
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		accountsettingspage.ClickonNotificationPolicy();
		accountsettingspage.NotificationPolicyNotifywhendeviceisrootedUserdefined();//
		accountsettingspage.SendAlertToMDM();
		accountsettingspage.SendAlertToDevice();
		accountsettingspage.SendAlertToEmail();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.SearchDeviceInconsole(Config.SearchDeviceName);//
		commonmethdpage.ClickOnApplyButton();
		accountsettingspage.SearchField("Trials Userdefined Notify when device is rooted");
				
		//accountsettingspage.ApplyNotifywhendeviceisrootedUserdefinedJobonDevice();	//	
		accountsettingspage.JobApplyActivityLogs();		
		//androidJOB.clickOnJobs();
		//accountsettingspage.SelectTrialsNotifywhendeviceisrootedUserdefined();	//
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
