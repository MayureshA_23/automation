package Settings_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;


import Common.Initialization;

public class Webhooks extends Initialization {

	
	@Test(priority=1,description="Verify Webhooks option is disabled by default.") 
	public void VerifyWebhooksoptionisdisabledbydefaultTC_ST_668() throws Throwable {
		Reporter.log("\n1.Verify Webhooks option is disabled by default",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonWebhooksButton();
		accountsettingspage.WebhookEnablebyDefault();						
}
	
//	@Test(priority=2,description="Verify Webhook Subscription option in Webhook") 
//	public void VerifyWebhookSubscriptionoptioninWebhookTC_ST_788() throws Throwable {
//		Reporter.log("\n2.Verify Webhook Subscription option in Webhook",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonWebhooksButton();
//		accountsettingspage.EnableWebhookCheckbox();
//		accountsettingspage.ClickonAddNewEndpoint();
//		accountsettingspage.WebhookName1();
//		accountsettingspage.WebhookEndpoint1();
//		accountsettingspage.WebhookSubscriptionoptions();		
//}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
