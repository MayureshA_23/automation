package OnDemandReports_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class DeviceHealthReport extends Initialization {

	@Test(priority = 1, description = "Generate And View the 'Device Health Report'  for Selected 'Home' group")
	public void VefifyDeviceHealthReportHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("1.Generate And View the 'Device Health Report'  for Selected 'Home' group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyDeviceHealthReportColumns();
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 2, description = "Generate And View the 'Device Health Report'  for Selected 'Home' group for Today")
	public void VefifyDeviceHealthReportHomeGroup_Today() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n2.Generate And View the 'Device Health Report'  for Selected 'Home' group for Today", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_DeviceConnectedReport();
		reportsPage.VerifyDeviceHealthReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 3, description = "Generate And View the 'Device Health Report'  for Selected 'Sub' group for Today")
	public void VefifyDeviceHealthReportSubGroup_Today() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n3.Generate And View the 'Device Health Report'  for Selected 'Sub' group for Today", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_DeviceConnectedReport();
		reportsPage.VerifyDeviceHealthReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 4, description = "Generate And View the 'Device Health Report'  for Selected 'Home' group for Yesterday")
	public void VefifyDeviceHealthReportHomeGroup_Yesterday() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n4.Generate And View the 'Device Health Report'  for Selected 'Home' group for Yesterday", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedYesterdayDate_DeviceConnected();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 5, description = "Generate And View the 'Device Health Report'  for Selected 'Home' group for Last 1 Week")
	public void VefifyDeviceHealthReportHomeGroup_Last1week() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n5.Generate And View the 'Device Health Report'  for Selected 'Home' group for Last 1 Week", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week_DeviceConnectedReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 6, description = "Generate And View the 'Device Health Report'  for Selected 'Sub' group for Last 1 Week")
	public void VefifyDeviceHealthReportSubGroup_Last1week() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n6.Generate And View the 'Device Health Report'  for Selected 'Home' group for Last 1 Week", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week_DeviceConnectedReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 7, description = "Generate And View the 'Device Health Report'  for Selected 'Home' group for Last 30 Days")
	public void VefifyDeviceHealthReportHomeGroup_Last30Days() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n7.Generate And View the 'Device Health Report'  for Selected 'Home' group for Last 1 Week", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30DaysInDeviceHealthReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 8, description = "Generate And View the 'Device Health Report'  for Selected 'Sub' group for Last 30 Days")
	public void VefifyDeviceHealthReportSubGroup_Last30Days() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n8.Generate And View the 'Device Health Report'  for Selected 'Sub' group for Last 1 Week", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30DaysInDeviceHealthReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 9, description = "Generate And View the 'Device Health Report'  for Selected 'Home' group for This Month")
	public void VefifyDeviceHealthReportHomeGroup_ThisMonth() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n9.Generate And View the 'Device Health Report'  for Selected 'Home' group for This Month",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth_DeviceHealthReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 10, description = "Generate And View the 'Device Health Report'  for Selected 'Sub' group for This Month")
	public void VefifyDeviceHealthReportSubGroup_ThisMonth() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n10.Generate And View the 'Device Health Report'  for Selected 'Sub' group for This Month",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth_DeviceHealthReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 11, description = "Generate And View the 'Device Health Report'  for Selected 'Home' group for Custom Range")
	public void VefifyDeviceHealthReportHomeGroup_CustomRange() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n11.Generate And View the 'Device Health Report'  for Selected 'Home' group for Custom Range",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange_InstalledJob();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange_DeviceHealthReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 12, description = "Generate And View the 'Device Health Report'  for Selected 'Sub' group for Custom Range")
	public void VefifyDeviceHealthReportSubGroup_CustomRange() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n12.Generate And View the 'Device Health Report'  for Selected 'Sub' group for Custom Range",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange_InstalledJob();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange_DeviceHealthReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 13, description = "Generate And View the 'Device Health Report'  for Selected 'Sub' group")
	public void VefifyDeviceHealthReportSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n13.Generate And View the 'Device Health Report'  for Selected 'Sub' group", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.SelectGroupDeviceHealthReport();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyDeviceHealthReportColumns();
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.BatteryPercentColumnVerificationDeviceHealthReport();
		reportsPage.AvailableStorageColumnVerificationDeviceHealthReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 14, description = "Download and Verify the data for Device Health Report for 'Home' Group")
	public void VerifyDownloadingDeviceActivityReportHomeGroup() throws InterruptedException {
		Reporter.log("\n14.Download and Verify the data for Device Health Report for 'Home' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@Test(priority = 15, description = "Download and Verify the data for Device Health Report for 'Sub' Group")
	public void VerifyDownloadingDeviceActivityReportSubGroup() throws InterruptedException {
		Reporter.log("\n15.Download and Verify the data for Device Health Report for 'Sub' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@Test(priority = 16, description = "Verify of Last Connected Column in Device Health Report")
	public void VerifyOfLastConnected() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log("\n16.Verify of Last Connected Column in Device Health Report", true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.GetDeviceLastConnectedDate();
		reportsPage.getdatetimeFormatAsexpected(reportsPage.lastDate);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHealthReport();
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.EnterStorageSpace();
		reportsPage.EnterPhysicalMemory();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.getLastConnectedDatetime_DeviceHealthReport(2);
		reportsPage.splitDateAndTimeAsExpected(reportsPage.lastconnecteddatefromreport);
		reportsPage.compareLastConnectedDate();
	}

	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();

		}
	}
}