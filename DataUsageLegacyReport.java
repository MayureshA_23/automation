package OnDemandReports_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

// Some data should consumed from wifi 
public class DataUsageLegacyReport extends Initialization{   ////////
	
//	   @Test(priority=0,description="Precondition-Creating Job")
//	   public void JobCreation() throws Throwable
//	   {
//			androidJOB.clickOnJobs();
//			androidJOB.clickNewJob();
//			androidJOB.clickOnAndroidOS();
//			androidJOB.clickOnFileTransferJob();
//			androidJOB.enterJobName("NixSingleFileTransfer");
//			androidJOB.clickOnAdd();
//			androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe");
//			androidJOB.EnterDevicePath("/sdcard/Download");
//			androidJOB.clickOkBtnOnBrowseFileWindow();
//			androidJOB.clickOkBtnOnAndroidJob();
//			androidJOB.JobCreatedMessage(); 
//			commonmethdpage.ClickOnHomePage();
//			androidJOB.SearchDeviceInconsole(Config.DeviceName);
//			commonmethdpage.ClickOnApplyButton();
//			androidJOB.SearchField("NixSingleFileTransfer"); 
//			androidJOB.JobInitiatedOnDevice(); 
//			androidJOB.CheckStatusOfappliedInstalledJob("NixSingleFileTransfer",200);
//	   }
//	   @Test(priority=1,description="Precondition-Verify Telecom management turn On Pop up validate Yes button")
//	   public void VerifyTelecomManagementPopUpYesButton() throws Throwable
//	   {
//		   commonmethdpage.SearchDevice(Config.DeviceName);
//		   dynamicjobspage.ClickOnTelecomManagementEditButton();
//		   dynamicjobspage.DisableTelecomManagement();
//		   dynamicjobspage.ClickOnTelecomManagementOkButton();
//		   dynamicjobspage.ClickOnMoreOption();
//		   dynamicjobspage.ClickOnDataUsage_UM();
//		   dynamicjobspage.ClickOnDataUsageYesButton();
//		   dynamicjobspage.EnableTelecomManagement();
//		   dynamicjobspage.ClickOnTelecomManagementOkButton();
//		   dynamicjobspage.ConfirmationMessageTelecomManagement();
//		   
//		   dynamicjobspage.ClickOnMoreOption();
//		   dynamicjobspage.ClickOnDataUsage_UM();
//		   dynamicjobspage.ClickOnDataUsageRefreshButton();
//		   dynamicjobspage.VerifyWifiDataIsShownAfterRefreshing();
//		   dynamicjobspage.ClickOnDataUsageCloseBtn();
//
//
//		   
//	   }
	   
	@Test(priority = 2, description = "1.Generate And View the 'Data Usage Legacy Report' when Suremdm Nix v <=14.26 is used'")
	public void GenerateDataUsageLegacyReportHomeGroup_MobileData() throws InterruptedException, NumberFormatException,EncryptedDocumentException, InvalidFormatException, IOException
	{
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDataUsageLegacyReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Data Usage Legacy");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReport(Config.DeviceName);
		reportsPage.VerifyDataUsageLegacyReportColumns();
		reportsPage.WifiDataUsageColumnVerificationDataUsageLegacyReport();
		reportsPage.MobileDataUsageColumnVerificationDataUsageLegacyReport();
		reportsPage.SwitchBackWindow();




	}
	@Test(priority = 3, description = "2.Generate And View the 'Data Usage Legacy Report' when Suremdm Nix v <=14.26 is used for Sub folder'")
	public void GenerateDataUsageLegacyReportSubGroup() throws InterruptedException, NumberFormatException,EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDataUsageLegacyReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();

       reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReport(Config.DeviceName);
		reportsPage.VerifyDataUsageLegacyReportColumns();
		reportsPage.WifiDataUsageColumnVerificationDataUsageLegacyReport();
       reportsPage.MobileDataUsageColumnVerificationDataUsageLegacyReport();





	}

}

