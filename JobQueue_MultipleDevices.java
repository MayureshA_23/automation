package QuickActionToolBar_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class JobQueue_MultipleDevices extends Initialization
{
	@Test(priority=0,description="To verify  the Tabs present in Job Queue for Multiple devices.")
	public void VerifyJobQueueTabsForMultipleDevices() throws InterruptedException, AWTException
	{
		Reporter.log("1.To verify  the Tabs present in Job Queue for Multiple devices.",true);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.VerifyJobHistoryForMultileDevices();
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
	}
	
	@Test(priority=1,description="To verify functionality of job queue for multiple devices for pending jobs.")
	public void VerifyPendingJobsTabForMultipleDevices() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException, AWTException
	{
		Reporter.log("\n2.To verify functionality of job queue for multiple devices for pending jobs.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TextMessageJobname);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.VerifyJobsTabForMultipleDevices(Config.TextMessageJobname,"TextMessage","1");
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,Config.DeviceName);
	}
	
	/*@Test(priority=2,description="To verify functionality of job queue for multiple devices for In-Progress jobs.")
	public void VerifyInProgressJobsTabForMultipleDevices() throws AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n3.To verify functionality of job queue for multiple devices for In-Progress jobs.",true);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.MultipleDevices1);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("In Progress");
		quickactiontoolbarpage.VerifyJobsTabForMultipleDevices(Config.InstallJObName,"Install","2");
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.MultipleDevices1);
	}*/
	
	@Test(priority=3,description="To verify functionality of job queue for multiple devices for successfully deployed jobs.")
	public void VerifyCompletedJobsTabForMultipleDevices() throws AWTException, InterruptedException
	{
		Reporter.log("\n4.To verify functionality of job queue for multiple devices for successfully deployed jobs.",true);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Completed");
		quickactiontoolbarpage.VerifyCompletedJobsTabForMultipleDevices(Config.InstallJObName,"Install","2");
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();		
	}
	
	@Test(priority=4,description="To verify functionality of job queue for multiple devices for Failed jobs.")
	public void VerifyErrorJobsTabForMultipleDevices() throws AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n5.To verify functionality of job queue for multiple devices for Failed jobs.",true);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.MultipleDevices1);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Failed");
		quickactiontoolbarpage.VerifyJobsTabForMultipleDevices(quickactiontoolbarpage.ErrorjobName,"SureFox Settings","2");
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
	}
	
	@Test(priority=5,description="To verify functionality of Remove button in job queue for multiple devices.")
	public void VerifyRemoveButtonInJobQueueForMultipleDevices() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException
	{
		Reporter.log("\n6.To verify functionality of Remove button in job queue for multiple devices.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.SelectingJobInsideJobQueue(Config.InstallJObName);
		quickactiontoolbarpage.ClickonJobRemoveButton();
		quickactiontoolbarpage.VerifyJobsDeletionInMultipleDevicesJobQueue();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("In Progress");
		quickactiontoolbarpage.SelectingJobInsideJobQueue(Config.InstallJObName);
		quickactiontoolbarpage.ClickonJobRemoveButton();
		quickactiontoolbarpage.VerifyJobsDeletionInMultipleDevicesJobQueue();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Failed");
		quickactiontoolbarpage.SelectingJobInsideJobQueue(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.ClickonJobRemoveButton();
		quickactiontoolbarpage.VerifyJobsDeletionInMultipleDevicesJobQueue();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Completed");
		quickactiontoolbarpage.SelectingDeployedJObInsideCompletedJobsTab(Config.InstallJObName);
		quickactiontoolbarpage.VerifyRemoveButtonIsGrayedOutWhenTriedToDeleteCompletedJobs();
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
	}
	
	@Test(priority=6,description="To verify functionality of Retry button in job queue for multiple devices.")
	public void VerifyFunctionalityOfRetryButton_PendingJobs() throws InterruptedException, AWTException
	{
		Reporter.log("\n7.To verify functionality of Retry button in job queue for multiple devices.",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.SelectingJobInsideJobQueue(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnJobRetryButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
	}
	
	@Test(priority=7,description="To verify functionality of Retry button in job queue for multiple devices.",dependsOnMethods="VerifyFunctionalityOfRetryButton_PendingJobs")
	public void VerifyFunctionalityOfRetryButton_InprogressJobs() throws AWTException, InterruptedException, IOException
	{
		Reporter.log("\n8.To verify functionality of Retry button in job queue for multiple devices.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		quickactiontoolbarpage.WaitForMultipleDevice_JobInprogressStatus();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("In Progress");
		quickactiontoolbarpage.SelectingJobInsideJobQueue(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnJobRetryButton();
		quickactiontoolbarpage.VerifyJobsDeletionInMultipleDevicesJobQueue();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Pending");
		//quickactiontoolbarpage.VerifyJobsTabForMultipleDevices(Config.InstallJObName,"Install","2");
		//quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
	}
	
	@Test(priority=8,description="To verify functionality of Retry button in job queue for multiple devices.")
	public void VerifyFunctionalityOfRetryButton_Failedjobs() throws InterruptedException, AWTException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n9.To verify functionality of Retry button in job queue for multiple devices.",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.MultipleDevices1);
		quickactiontoolbarpage.ClearingConsoleLogs();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Failed");
		quickactiontoolbarpage.SelectingJobInsideJobQueue(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.ClickOnJobRetryButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
		quickactiontoolbarpage.VerifyRetryOfFailedJobs();
	}
	
	@Test(priority=9,description="Verify that user is able to view pending jobs from job queue of multiple devices")
	public void VerifyUserIsAbleToViewPendingJobsInMultipleDevicesQueue() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException
	{
		Reporter.log("\n10.Verify that user is able to view pending jobs from job queue of multiple devices",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TextMessageJobname);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();	
		quickactiontoolbarpage.VerifyPendingJobForMultipleDevice(Config.TextMessageJobname);
	}
	
	@Test(priority='A',description="Verify that user is able to view failed job and success jobs from job queue of multiple devices")
	public void VerifyUserIsAbleToViewFailedJobAndSuccessJobsInMultipleDevicesQueue() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n11.Verify that user is able to view failed job and success jobs from job queue of multiple devices",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.MultipleDevices1);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Failed");
		quickactiontoolbarpage.VerifyJobsTabForMultipleDevices(quickactiontoolbarpage.ErrorjobName,"SureFox Settings","2");
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Completed");
		quickactiontoolbarpage.VerifyCompletedJobsTabForMultipleDevices(Config.InstallJObName,"Install","2");
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();
	}
	
	@Test(priority='B',description="Verify that user is able to view pending job queue of combination of multiple platform devices")
	public void VerifyUserIsAbleToViewJobQueueofCombinationOfMultiplePlatformDevices() throws InterruptedException, IOException, AWTException
	{
		Reporter.log("\n12.Verify that user is able to view pending job queue of combination of multiple platform devices",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.Windows_DeviceName1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TextMessageJobname);
		androidJOB.ClickOnjobQueueForMultipleDevices();
		quickactiontoolbarpage.VerifyPendingJobForMultipleDevice(Config.TextMessageJobname);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
	}
	
	@Test(priority='C',description="Verify that user is able to view success jobs of multiple devices(android/IOS/Windows).")
	public void VerifyUserIsabletoViewsuccessjobsOfMultipleDevices() throws AWTException, InterruptedException
	{
		Reporter.log("\n13.Verify that user is able to view success jobs of multiple devices(android/IOS/Windows).",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		androidJOB.ClickOnjobQueueForMultipleDevices();	
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForMultipleDevices("Completed");
		quickactiontoolbarpage.VerifyCompletedJobsTabForMultipleDevices(Config.InstallJObName,"Install","2");
		quickactiontoolbarpage.ClickOnJobQueueCloseButton_MultipleDevices();	
	}
	
	
	
	
	
	
}
