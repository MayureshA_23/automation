package RemoteSupport_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class RemoteSupport_Android_MasterTestCases extends Initialization
{
	@Test(priority=0,description="PreCondtions For Android Remote Support")
	public void PreConditions() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
 		accountsettingspage.ClickOnMiscellaneousSettings();
 		remotesupportpage.UncheckingZipAllDowlaodCheckBox();
 		remotesupportpage.UncheckingDontPauseScreenCheckBox();
 		remotesupportpage.ClickingOnAccSettingsApply();
 		commonmethdpage.ClickOnHomePage();

	}

	@Test(priority='1',description="Remote support - Launch, Device is online, create folder")
	public void RemoteSupportLaunchAndCreateFolder() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfRemoteSupportLaunched();
		System.out.println("");		
		System.out.println("b. Remote support - Creating Folders");
		remotesupportpage.CreateFolder();
		System.out.println("");
		remotesupportpage.SwitchToMainWindow();

	}

	@Test(priority='2',description="Remote support - uploading file")
	public void UploadFile() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		System.out.println("2. Uploading File in Remote Support");
		remotesupportpage.ClickingOnUploadButtonInRemoteSupport();
		remotesupportpage.ClickingOnFileBrowseButton();
		remotesupportpage.UploadingFileInRemoteSupport("./Uploads/UplaodFiles/Remote Support/File1.exe");
		remotesupportpage.VerifyUploadingFile(Config.DownloadFile1Name);
		remotesupportpage.SwitchToMainWindow();
	}	

	@Test(priority='3',description="3.To Check Remote Support Window UI")
	public void RemoteSupportUI() throws Throwable
	{
		Reporter.log("\n3.To Check Remote Support Window UI",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfRemoteSupportwindowTest();
		remotesupportpage.SwitchToMainWindow();
	}

	@Test(priority='4',description="4.To Verify Remote support - Download Files")
	public void DownloadSingleFile() throws Throwable{
		Reporter.log("\n4.To Verify Remote support - Download Files",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnFile(Config.DownloadFile1Name);
		remotesupportpage.CheckDownloads(Config.DownloadFile1Name,Config.downloadpath);
		remotesupportpage.SwitchToMainWindow();
		Reporter.log("PASS >> Verification of Remote support - Download Files",true);
	}

	@Test(priority='5',description="5.To Verify Remote support - Delete Popup when clicked on Cancel Button")
	public void DeletePopUpCancel() throws Throwable{
		Reporter.log("\n5.To Verify Remote support - Delete Popup when clicked on Cancel Button",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnFile(Config.DownloadFile1Name);
		remotesupportpage.ClickOnDelete();
		remotesupportpage.VerifyOfDeleteFileHeader();
		remotesupportpage.VerifyOfCancelButton_DeleteFile();
		remotesupportpage.VerifyOfOkButton_DeleteFile();
		remotesupportpage.VerifyOfDeleteSummary();
		remotesupportpage.ClickOnCancelButton_DeleteFile();
		remotesupportpage.VerifyOfDeleteFile_True(Config.DownloadFile1Name);
		remotesupportpage.SwitchToMainWindow();
		Reporter.log("PASS >> Verification of Remote support - Delete Popup when clicked on Cancel Button",true);
	}

	@Test(priority='6',description="6.To Verify Remote support - Delete File")
	public void DeleteFile() throws Throwable{
		Reporter.log("\n6.To Verify Remote support - Delete File",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnFile(Config.DownloadFile1Name);
		remotesupportpage.ClickOnDelete();
		remotesupportpage.VerifyOfDeleteFileHeader();
		remotesupportpage.VerifyOfCancelButton_DeleteFile();
		remotesupportpage.VerifyOfOkButton_DeleteFile();
		remotesupportpage.VerifyOfDeleteSummary();
		remotesupportpage.ClickOnOkButton_DeleteFile();
		remotesupportpage.VerifyOfDeleteFile_False(Config.DownloadFile1Name);
		remotesupportpage.SwitchToMainWindow();
		Reporter.log("PASS >> Verification of Remote support - Delete File",true);
	}


	@Test(priority='7',description="7.To Verify Remote support - Rename of New folder")
	public void RenameNewFolder() throws Throwable{
		Reporter.log("\n7.To Verify Remote support - Rename of New folder",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		String FolderName="TestFolder";
		remotesupportpage.RenameNewFolder(FolderName);
		Reporter.log("PASS >> Verification of Remote support - Rename of New folder",true);
		remotesupportpage.SwitchToMainWindow();
	}


	@Test(priority='8',description="8.To Verify Remote support - Delete Folder")
	public void DeleteFolder() throws Throwable{
		Reporter.log("\n8.To Verify Remote support - Delete Folder",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnReload();
		remotesupportpage.ClickOnFile("TestFolder");
		remotesupportpage.ClickOnDelete();
		remotesupportpage.VerifyOfDeleteFileHeader();
		remotesupportpage.VerifyOfCancelButton_DeleteFile();
		remotesupportpage.VerifyOfOkButton_DeleteFile();
		remotesupportpage.VerifyOfDeleteSummary();
		remotesupportpage.ClickOnOkButton_DeleteFile();
		remotesupportpage.VerifyOfDeleteFile_False("TestFolder");
		Reporter.log("PASS >> Verification of Remote support - Delete Folder",true);
		remotesupportpage.SwitchToMainWindow();
	}

	@Test(priority='9',description="9.To Verify Remote support - View as List")
	public void ViewAsList() throws Throwable
	{
		Reporter.log("\n9.To Verify Remote support - View as List",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnReload();
		remotesupportpage.ClickOnListView();
		remotesupportpage.VerifyOfListViewColumn();
		remotesupportpage.SwitchToMainWindow();
		Reporter.log("PASS >> Verification of Remote support - View as List",true);
	}

	@Test(priority='A',description="10.To Verify Remote support - View as Icon")
	public void ViewAsIcon() throws Throwable{
		Reporter.log("\n10.To Verify Remote support - View as Icon",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnIconView();
		remotesupportpage.VerifyOfIconViewColumn();
		remotesupportpage.SwitchToMainWindow();
		Reporter.log("PASS >> Verification of Remote support - View as Icon",true);
	}

	@Test(priority='B',description="11.To Verify Remote support - Back,Delete,Download button grayed out when no file selected")
	public void BackDeleteDownoloadGreyedOutWhenNoFileSelected() throws Throwable{
		Reporter.log("\n11.To Verify Remote support - Back,Delete,Download button grayed out when no file selected",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfGrayedOut();
		Reporter.log("PASS >> Verification of Remote support - Back,Delete,Download button grayed out when no file selected",true);
		remotesupportpage.SwitchToMainWindow();
	}


	// Appium Server Should Be On and Knox feature should be enabled in the device.


	@Test(priority='C',description="Verify data usage in remote screen.")
	public void VerifyDataUsageInRemoteScreen() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyDataUsageInRemoteSupport();
		remotesupportpage.SwitchToMainWindow();

	}

	@Test(priority='D',description="Verify Lock in remote screen for android device .")
	public void VerifyLockInRemoteScreen() throws Exception
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnPowerButtonInRemoteScreen();
		remotesupportpage.SwitchToMainWindow();		
	}

	@Test(priority='E',description="Verify UnLock in remote screen for android device .")
	public void VerifyUnlockInRemoteSupportScreen() throws Exception
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnPowerButtonInRemoteScreen();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		remotesupportpage.VerifydeviceUnlocked();
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='F',description="Verify functionality of Play button on remote screen for android device .")
	public void VerifyFunctionalityOfPlayButton() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnPlayPauseButton();
		remotesupportpage.VerifyScreenAfterClickingPauseButton();
		remotesupportpage.ClickingOnPlayPauseButton();
		remotesupportpage.VerifyPlayPauseSymbol();
		remotesupportpage.VerifyRemoteScreenAfterClickingOnPlayButton();
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='G',description="Verify cancel button while uploading file")
	public void  VerifyCancelButtonWhileUploadingFile() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnUploadButtonInRemoteSupport();
		remotesupportpage.ClickingOnFileBrowseButton();
		remotesupportpage.UploadingFileInRemoteSupport("./Uploads/UplaodFiles/Remote Support/File1.exe");
		remotesupportpage.ClickingOnUploadCancelButton();
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='H',description="Verify functionality of view button under the files tab on remote support for android device .")
	public void VerifyFunctionalityOfViewButtonInRemoteSupport() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnViewAsIconButton();
		remotesupportpage.VerifyViewAsIconButtonInRemoteSupport();
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='I',description="Verify task manager tab for android device .")
	public void VerifyTaskManagerTabInRemoteSupport() throws Throwable
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnTaskManagerTab();
		remotesupportpage.VerifyTasksInsideTaskManager();
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='J',description="Verify the Remote device screen capture by closing the remote support tab and validate capture screen for android device .")
	public void VerifyRemoteSupportScreenByClosingRemoteSupport() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.CheckingDontPauseScreenCheckBox();
		remotesupportpage.ClickingOnResumeScreenCaptureButton();
		remotesupportpage.SwitchToMainWindow();		
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='K',description="Verify downloaded screenshot should be on .webp format")
	public void VerifyDownloadedScreenShotInRemoteSupport() throws Throwable
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.CheckScreenshots("screenshot.webp",Config.downloadpath);
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='L',description="Verify the Remote device screen capture by pause with ideal time out for android device && Verify the Remote device screen capture by resume from ideal time out for android device .")
	public void VerifyRemoteeScreenCaptureByPauseWithIdealTimeOutAndClikcingOnResume() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.ClickingOnResumeScreenCaptureButton();
		remotesupportpage.SwitchToMainWindow();		

	}

	@Test(priority='M',description="Verify the Remote device screen capture by play from ideal time out for android device .")
	public void VerifyRemoteDeviceCaptureByPlayFromIdealTimeOut() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.CheckingDontPauseScreenCheckBox();
		remotesupportpage.ClickingOnResumeScreenCaptureButton();
		remotesupportpage.WaitingForScreenPauseMessageAferClickingOnResumeButton();
		remotesupportpage.SwitchToMainWindow();		
	}

	@Test(priority='N',description="Verify functionality of Refresh in task manager for android device .")
	public void VerifyRefreshInTaskManager() throws Throwable
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnTaskManagerTab();
		remotesupportpage.VerifyClickOnTaskManagerRefreshButton();
		remotesupportpage.SwitchToMainWindow();
	}
	/*@Test(priority='O',description="Verify enabling Zip All Downloads for remote support for android device.")
	public void VerifyEnablingZipAllDownloadsForRemoteSupport() throws Throwable
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
 		accountsettingspage.ClickOnMiscellaneousSettings();
 		remotesupportpage.CheckingZipAllDownloadCheckBox();
 		remotesupportpage.ClickingOnAccSettingsApply();
 		commonmethdpage.ClickOnHomePage();
 		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
        remotesupportpage.SelectingSingleFile("NixSettings.xm�l");
        remotesupportpage.ClickOnDownloadButton();
        remotesupportpage.SwitchToMainWindow();

	}

	@Test(priority='P',description="Verify by downloading multiple files from remote support for android device.")
	public void VerifyDownloadingMultipeFilesInRemoteSupport() throws Throwable
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.SelectingMultipleFilesInRemoteSupport("SureMDM Nix","NixSettings.xm�l");      
		remotesupportpage.SwitchToMainWindow();

	}*/

	@Test(priority='Q',description="Verify functionality of Read from Device Clipboard under the clipboard tab on remote support for android device .")
	public void VerifyFunctionalityOfReadFromDeviceClipBoard() throws Throwable
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		remotesupportpage.ReadingTextFromDevice(Config.DeviceText);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnClipBoardTab();
		remotesupportpage.ClickOnReadfromClipboard();
		remotesupportpage.VerifyOfReadFromDeviceClipBoard(Config.DeviceText);
		remotesupportpage.SwitchToMainWindow();
	}

	@Test(priority='R',description="Verify functionality of Send to device Clipboard under the clipboard tab on remote support for android device .")
	public void VerifyFunctionalityOfSendToDeviceClipBoard() throws Throwable
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnClipBoardTab();
		remotesupportpage.SendigTextToDeviceClipBoard(Config.SendtoDeviceClipBoardText);
		remotesupportpage.VerifySendToDeviceClipBoard(Config.SendtoDeviceClipBoardText);
		remotesupportpage.SwitchToMainWindow();
	}

	@Test(priority='S',description="Verify functionality of Clear Clipboard under the clipboard tab on remote support for android device .")
	public void VeirfyFunctionalityOfClearClipBoard() throws Throwable
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickOnClipBoardTab();
		remotesupportpage.ClickOnClearFromDeviceClipboard();
		remotesupportpage.VerifyOfClearFromDeviceClipBoard();
		remotesupportpage.SwitchToMainWindow();

	}



	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(remotesupportpage.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
						remotesupportpage.SwitchToMainWindow();
				}
			} 
			catch (Exception e) 
			{

			}
		}
	}

}
