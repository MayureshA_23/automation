package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Roles extends Initialization {
	@Test(priority = 1, description = "1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException {
		Reporter.log("\n1.To Verify roles option is present", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
	}

	@Test(priority = 2, description = "2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException {
		Reporter.log("\n2.To Verify super user is present", true);

		usermanagement.toVerifySuperUsertemplete();

	}

	@Test(priority = 3, description = "3.To Verify options in group permissions")
	public void OptionsGroupPermission() throws InterruptedException {
		Reporter.log("\n3.To Verify options in group permission is present", true);

		usermanagement.clickOnAddButtonUserManagement();
		usermanagement.clickOnGroupPermissions();
	}

	@Test(priority = 4, description = "4.To Verify options in Device Action permissions")
	public void OptionsDeviceActionPermission() throws InterruptedException {
		Reporter.log("\n4.To Verify options in Device Action permission is present", true);

		usermanagement.DeviceAction();
		usermanagement.VerifyDeviceActionPermission();
	}

	@Test(priority = 5, description = "5.To Verify options in Device management permissions")
	public void OptionsDeviceManagementPermission() throws InterruptedException {
		Reporter.log("\n5.To Verify options in Device Action permission is present", true);

		usermanagement.clickDeviceManagementPermission();
		usermanagement.verifyDeviceManagementPermission();
	}

	@Test(priority = 6, description = "6.To Verify options in Application Settings permissions")
	public void OptionsApplicationSettings() throws InterruptedException {
		Reporter.log("\n6.To Verify options in Device Action permission is present", true);

		usermanagement.clickApplicationSettingsPermissions();
		usermanagement.verifyApplicationSettingsPermission();
	}

	@Test(priority = 7, description = "7.To Verify options in Job permissions")
	public void OptionsJobPermissions() throws InterruptedException {
		Reporter.log("\n7.To Verify options in job permission is present", true);

		usermanagement.clickonJobPermission();
		usermanagement.verifyJobPermessionOptions();
	}

	@Test(priority = 8, description = "8.To Verify options in Report permissions")
	public void OptionsReportPermissions() throws InterruptedException {
		Reporter.log("\n8.To Verify options in Report permission is present", true);

		usermanagement.clickOnReportPermission();
		usermanagement.verifyReportPermissions();
	}

	@Test(priority = 9, description = "9.To Verify options in user mansgement permissions")
	public void OptionsUserManagementPermissions() throws InterruptedException {
		Reporter.log("\n9.To Verify options in user managemnet permission is present", true);

		usermanagement.clickonUserManagenemtPermission();
		usermanagement.verifyUserManagementPermission();
	}

	@Test(priority = 10, description = "10.To Verify options in user Dashboard permissions")
	public void OptionsDashboardPermissions() throws InterruptedException {
		Reporter.log("\n10.To Verify options in user managemnet permission is present", true);

		usermanagement.clickonDashboardPermission();
		usermanagement.verifyDashboardOptions();
	}

	@Test(priority = 11, description = "11.To Verify options in user File Store permissions")
	public void OptionsFileStorePermissions() throws InterruptedException {
		Reporter.log("\n11.To Verify options in user filestore permission is present", true);

		usermanagement.clickonFileStorePermission();
		usermanagement.verifyFileStorePermission();
	}

	@Test(priority = 12, description = "12.To Verify options in user Profile permissions")
	public void OptionsProfilePermissions() throws InterruptedException {
		Reporter.log("\n12.To Verify options in Profile permission is present", true);

		usermanagement.clickonProfilePermission();
		usermanagement.verifyProfilePermissionOptions();
	}

	@Test(priority = 13, description = "13.To Verify options in user AppStore permissions")
	public void OptionsAppStore() throws InterruptedException {
		Reporter.log("\n13.To Verify options in AppStoret permission is present", true);

		usermanagement.clickOnAppStorePermission();
		usermanagement.verifyAppStorePermission();
	}

	@Test(priority = 14, description = "14.To Verify options in Other permissions")
	public void OptionsOtherPermission() throws InterruptedException {
		Reporter.log("\n14.To Verify options in other permission is present", true);

		usermanagement.clickonOtherPermission();
		usermanagement.verifyOtherPermissions();
	}

	@Test(priority = 15, description = "15.To Verify options in Location permissions")
	public void OptionsLocationPermission() throws InterruptedException {
		Reporter.log("\n15.To Verify options in Location permissions is present", true);

		usermanagement.clickLocationPermission();
		usermanagement.verifyLoctaionPermission();
	}

	@Test(priority = 16, description = "17.To Verify error Message no permissions")
	public void VerifyErrorMessabeNoPermissionSelected() throws InterruptedException {
		Reporter.log("\n16.To Verify options in Location permissions is present", true);

		usermanagement.errorMessageNoPermissionSlected();
	}

	@Test(priority = 17, description = "18.To verify clone options present when u select super user")
	public void cloneOptionIsEnabledForSuperUser() throws InterruptedException {
		Reporter.log("\n17.To Verify options present for super user", true);
		usermanagement.ClosePremissionPopup();
		// usermanagement.SelectUser();
	}

	@Test(priority = 18, description = "20.to search the role in search text field")
	public void SearchTextField() throws InterruptedException {
		Reporter.log("\n18.To Verify options in Location permissions is present", true);
		usermanagement.searchRefreshOptions();
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}