package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
//Pass the defualt launcher of the device which your using for testing (TC_RE_145)
public class SureLockChecklist_CustomReport extends Initialization {
	@Test(priority = '0', description = "Verify SureLock Checklist  Custom reports")
	public void VerifyingSureLockChecklist_TC_RE_86() throws InterruptedException {
		Reporter.log("\nVerify SureLock Checklist Custom reports", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("SureLock CheckList");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SureLockChecklist  Customreport Without Filter",
				"test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SureLockChecklist  Customreport Without Filter");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SureLockChecklist  Customreport Without Filter");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SureLockChecklist  Customreport Without Filter");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep(Config.SureLockDefaultLauncher);
		customReports.VerifySureLockDefaultLauncher(Config.SureLockDefaultLauncher);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '1', description = "Verify SureLock Checklist  Custom reports for sub group ")
	public void VerifyingSureLockChecklistForSubGroup_TC_RE_145() throws InterruptedException {
		Reporter.log("\nVerify SureLock Checklist  Custom reports for sub group", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("SureLock CheckList");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SureLockChecklist  Customreport for sub group",
				"test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SureLockChecklist  Customreport for sub group");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SureLockChecklist  Customreport for sub group");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SureLockChecklist  Customreport for sub group");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep("NA");
		customReports.VerifySureLockDefaultLauncher("NA");
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '2', description = "Verify Sort by and Group by for Surelock Checklist ")
	public void VerifyingSureLockChecklistWithFilter_TC_RE_146() throws InterruptedException {
		Reporter.log("\nVerify Sort by and Group by for Surelock Checklist", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("SureLock CheckList");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(
				"SurelockChecklist CustomReport for SortBy And GroupBy", "test");
		customReports.Selectvaluefromsortbydropdown(Config.Select_Defualtluncher);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SurelockChecklist CustomReport for SortBy And GroupBy");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SurelockChecklist CustomReport for SortBy And GroupBy");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SurelockChecklist CustomReport for SortBy And GroupBy");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SurelockChecklist CustomReport for SortBy And GroupBy");
		reportsPage.WindowHandle();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '3', description = "To verify SureLock permissions in Custom Reports")
	public void VerifyingCustomReport_TC_RE_28() throws InterruptedException {
		Reporter.log("\nTo verify SureLock permissions in Custom Reports", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("SureLock CheckList");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyColumnsInSureLockCheckListTable();
		Reporter.log("All the options are present in SureLock permissions Column", true);
		customReports.ClickOnCancelButton();
	}

	@Test(priority = '4', description = "Generate SureLock Checklist Report and verify all the Columns")
	public void VerifyingCustomReport_TC_RE_29() throws InterruptedException {
		Reporter.log("\nGenerate SureLock Checklist Report and verify all the Columns", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("SureLock CheckList");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(
				"SureLockChecklist CustomReport ToCheckColumns in report", "test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SureLockChecklist CustomReport ToCheckColumns in report");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SureLockChecklist CustomReport ToCheckColumns in report");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SureLockChecklist CustomReport ToCheckColumns in report");
		reportsPage.WindowHandle();
		deviceGrid.ScrollToColumn("Display Over other Apps");
		customReports.VerifyColumnsInSureLockCheckListReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '5', description = "Modify a previously saved SureLock Checklist Report Custom Report,generate the Report and verify all the Columns.")
	public void VerifyingCustomReport_TC_RE_30() throws InterruptedException {
		Reporter.log("\nModify a previously saved SureLock Checklist Report Custom Report,generate the Report and verify all the Columns.",
				true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.SearchingReportInCustomReportsList("SureLockChecklist  Customreport Without Filter");
		customReports.ClickOnModifyButton();
		customReports.ClickOnAddMoreButton();
		customReports.Selectvaluefromdropdown(Config.Select_SureLockCheckList);
		customReports.SelectValueFromColumn(Config.Select_Defualtluncher);
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.SureLockDefaultLauncher);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SureLockChecklist  Customreport Without Filter");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SureLockChecklist  Customreport Without Filter");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SureLockChecklist  Customreport Without Filter");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SureLockChecklist  Customreport Without Filter");
		reportsPage.WindowHandle();
		customReports.VerifyDefaultLauncherColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		customReports.ClickOnDownloadReportLinkButton();
	}

	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				String FailedWindow = Initialization.driver.getWindowHandle();
				if (FailedWindow.equals(customReports.PARENTWINDOW)) {
					commonmethdpage.ClickOnHomePage();
				} else {
					reportsPage.SwitchBackWindow();
				}
			} catch (Exception e) {

			}
		}
	}
}
