package PageObjectRepository;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.PassScreenshot;
import Library.WebDriverCommonLib;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class RunScript_POM extends WebDriverCommonLib {

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	WebDriverWait wait;
	CommonMethods_POM common=new CommonMethods_POM();

	@FindBy(xpath=".//*[@id='run_script']")
	private WebElement RunScriptButton;

	public void ClickOnRunscript() throws InterruptedException
	{
		RunScriptButton.click();
		waitForidPresent("runscriptmodal");
		sleep(1);
	}
	
	public void clickOnAdvancedSettings() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Advanced features']").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Multi window']").click();
		sleep(2);
		Reporter.log("Clicked On Advanced Setteings and navigated to Multi Window", true);
	}
	
	public void CheckMultiWindowIsDisabled()
	{
		boolean value=true;
		try{
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Show more than one app at the same time.']").isEnabled();

		}catch(Exception e)
		{
			value=false;

		}
		Assert.assertTrue(value,"PASS >>Multi Window is disabled");

		Reporter.log("Multi Window is disabled through runscript", true);

	}
	
	public void clickOnConnection() throws InterruptedException
	{
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Connections']").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='NFC and payment']").click();
		sleep(2);
	}
	
	public boolean DisabledPowerSaving()
	{
		boolean value=true;
		try{
			Initialization.driverAppium.findElementById("com.samsung.android.lool:id/psm_mid_btn").isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	
	public void CheckEnablePowerSavingMode() throws InterruptedException
	{
		boolean searchicon =Initialization.driverAppium.findElementById("com.samsung.android.lool:id/psm_mid_btn").isDisplayed();
		String pass4 = "PASS >>Power saving Option is enabled through RunScript";
		String fail4 = "FAIL >>Power saving Option is Not enabled through Runscript";
		ALib.AssertTrueMethod(searchicon, pass4, fail4);
		sleep(3);
		Reporter.log("Power Saving mode is Enabled through RunScript", true);
	}
	public void Powersaving_Disabled() throws InterruptedException
	{
		List<WebElement> ls = Initialization.driverAppium.findElementsById("com.android.systemui:id/tile_label");
		ls.get(7).click();
		sleep(2);
		Initialization.driverAppium.findElementById("android:id/button2").click();
		sleep(2);
			}

	@FindBy(xpath=".//*[@id='knox']/ul/li/span[text()='Run script to control power saver mode android settings of the device']")
	private WebElement PowerSavingSection;
	public void clickOnPowerSavingModeOption() throws InterruptedException
	{
		PowerSavingSection.click();
		sleep(2);
	}
	
	public void DontcareNFCcondition()
	{
		String Actual = Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").getText();
		System.out.println(Actual);
		String Expected = "ON";
		String pass = "Dont care NFC functionality is pass ";
		String fail = "Dont care NFC functionality is fail ";
		ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
	}
	@FindBy(xpath=".//*[@id='all']/ul/li/span[text()='To launch app by package name or app name']")
	private WebElement LaunchAppByAppNamePackageSection;
	public void clickOnLaunchApplictionByPackageOrAppName() throws InterruptedException
	{
		LaunchAppByAppNamePackageSection.click();
		sleep(3);
	}
	@FindBy(xpath="//span[contains(text(),'Disable Logger')]")
	private WebElement DisableLogger;
	public void clickOnDisableLogger() throws InterruptedException
	{
		DisableLogger.click();
		sleep(2);
		Reporter.log("Clicked on Disable Loggger", true);

	}
	
	public void CheckMultiWindowEnable() throws InterruptedException
	{
		boolean searchicon =Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Pop-up view action']").isDisplayed();
		String pass4 = "PASS >> Multi Window option is Enabled";
		String fail4 = "FAIL >> Multi Window option is Disabled";
		ALib.AssertTrueMethod(searchicon, pass4, fail4);
		sleep(3);
	}

	@FindBy(xpath="//span[contains(text(),'Enable/Disable Multi Window Mode')]")
	private WebElement clickonMultiWindowOption;
	public void clickOnMultiWindowMode() throws InterruptedException
	{
		clickonMultiWindowOption.click();
		sleep(3);
		Reporter.log("Clicked On MultiWindow Enable/Disable Tab", true);
	}
	@FindBy(xpath=".//*[@id='runscriptmodal']/div/div/div[2]/ul/li[2]/a[text()='Knox']")
	private WebElement Knox;

	public void clickOnKnoxSection() throws InterruptedException
	{
		Knox.click();
		sleep(2);
	}

	@FindBy(xpath=".//*[@id='knox']/ul/li/span[text()=' To deactivate nix admin on Device']")
	private WebElement DeactivatAdminOnDevice;

	public void ClickOnDeactivateDeviceAdmin() throws InterruptedException
	{
		DeactivatAdminOnDevice.click();
		waitForidPresent("scriptmodalpop");
		sleep(1);
	}
	@FindBy(xpath=".//*[@id='ValidateBtn']")
	private WebElement ValidateButton;

	@FindBy(xpath="//span[text()='The script has been validated successfully.']")
	private WebElement ScriptValidationMessage;

	@FindBy(xpath=".//*[@id='ScriptOkBtn']")
	private WebElement InsertButton;

	@FindBy(xpath=".//*[@id='okbtn']")
	private WebElement SaveRunScript;


	public void ClickOnValidateButton() throws InterruptedException
	{
		ValidateButton.click();
		sleep(2);
		
	}

	JavascriptExecutor js=(JavascriptExecutor)Initialization.driverAppium;
	public void clickOnApps() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Apps']").click();
		Reporter.log("clicked on App on device side", true);
		sleep(6);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='9Apps']").click();
		Reporter.log("clicked on uninstall disabled APP info", true);
		sleep(2);
	}
	
	public void checkUninstallDisabled()
	{
		boolean value=true;
		try{
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='UNINSTALL']").isEnabled();

		}catch(Exception e)
		{
			value=false;

		}
		Assert.assertTrue(value,"PASS >>Uninstall button is disabled");

		Reporter.log("Unstiall button is disabled through runscript", true);
	}

	public void checkEnabledUninstall()
	{
		boolean searchicon =Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='UNINSTALL']").isDisplayed();
		String pass4 = "PASS >>Uninstall Option is enabled through RunScript";
		String fail4 = "FAIL >>Uninstall Option is Not enabled through Runscript";
		ALib.AssertTrueMethod(searchicon, pass4, fail4);
	}
	
	@FindBy(xpath="//span[contains(text(),'To enable uninstall')]")
	private WebElement clickOnEnableUninstall;

	public void ClickOnToEnableUninstall() throws InterruptedException
	{
		clickOnEnableUninstall.click();
		sleep(3);
	}
	public void OpenSettingsPage() throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb shell am start -n com.android.settings/com.android.settings.Settings");
		sleep(4);
	}
	
	@FindBy(xpath=" //span[contains(text(),'To disable uninstall')]")
	private WebElement ClickOnDisableUninstall;
	public void ClickDisableUninstall() throws InterruptedException
	{
		ClickOnDisableUninstall.click();
		sleep(3);
	}
	
	public void EnterModifiedRunScriptName(String JobName) throws InterruptedException
	{
		RuncriptName.clear();
		RuncriptName.sendKeys(JobName);
		SaveRunScript.click();
		sleep(10);}
	public void ScriptValidatedMessage() throws InterruptedException {
		boolean isdisplayed=true;

		try{
			ScriptValidationMessage.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}
		Assert.assertTrue(isdisplayed,"Fail: Script Validated messsage is not displayed");
		Reporter.log("PASS >> Script validation message is displayed successfully",true );		
		sleep(2);
	}
	public void ClickOnInsertButton() throws InterruptedException
	{
		waitForidPresent("ScriptOkBtn");
		sleep(1);
		InsertButton.click();
		sleep(2);
	}
	@FindBy(xpath=".//*[@id='job_name_input']")
	private WebElement RuncriptName;

	@FindBy(xpath="//span[text()='Job created successfully.']")
	private WebElement RoleCreatedMessage;
	public void RunScriptName(String JobName) throws InterruptedException
	{
		RuncriptName.sendKeys(JobName);
		sleep(1);
		SaveRunScript.click();
		sleep(2);
		boolean isdisplayed=true;
		try{
			RoleCreatedMessage.isDisplayed();
		}
		catch(Exception e)
		{
			isdisplayed=false;
		}
		Assert.assertTrue(isdisplayed,"Fail: Role created messsage is not displayed");
		Reporter.log("PASS >> Role created message is displayed successfully",true );		
		sleep(5);
	}

	public void ClickOnSettingsDeviceSide() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Settings']").click();
		sleep(2);
	}
	public void CheckAdminDeactivatedOnDevice() throws InterruptedException
	{
		common.commonScrollMethod("Enable Admin");
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Enable Admin']").click();
		sleep(2);		try {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='PROCEED']").click();
		sleep(2);}catch(Exception e) {}
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='ACTIVATE']").click();
		sleep(1);
	}
	
	
	@FindBy(xpath=".//*[@id='knox']/ul/li/span[text()='Android-script to create App shortcut in homescreen.']")
	private WebElement AppShprtCutInHomescreen;
	
	public void clickOnAppShortcutSection() throws InterruptedException
	{
		AppShprtCutInHomescreen.click();
		waitForidPresent("scriptmodalpop");
		sleep(2);
	}
	@FindBy(xpath=".//*[@id='scriptmodalpop']/div/div/div[2]/div/div/p/input")
	private WebElement PackageNameActivity;
	
	public void sendPackageNameORpath(String PackageName) throws InterruptedException
	{
		PackageNameActivity.sendKeys(PackageName);
		sleep(2);
	}
	@FindBy(xpath="//span[text()='Please complete all required input fields.']")
	private WebElement PackageNameAndActivity;
	public void ErrorMessageToFillPackageName() throws InterruptedException
	{
		boolean isdisplayed=true;

		try{
			PackageNameAndActivity.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}
		Assert.assertTrue(isdisplayed,"Fail: messsage to fill the field is not displayed");
		Reporter.log("PASS >> messsage to fill the field is displayed successfully",true );		
		sleep(2);
	}
	public void homepageclick() throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb shell am start -n com.sec.android.app.launcher/com.sec.android.app.launcher.activities.LauncherActivity");
		sleep(1);
		
	}
	public void VErifyOnHomeScreen() throws InterruptedException
	{
		boolean AppDisplsyedOnHomeScreen = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Uber']").isDisplayed();
		String pass4 = "PASS >> App is Displayed On home screen ";
		String fail4 = "FAIL >> App is not Displayed On home screen";
		ALib.AssertTrueMethod(AppDisplsyedOnHomeScreen, pass4, fail4);
		sleep(3);
	}
	
	@FindBy(xpath=".//*[@id='knox']/ul/li/span[text()='Change Lock Screen Wallpaper']")
	private WebElement ChangeLockWallpaperSection;
	public void ClickOnChangeLockScreenWallPaper() throws InterruptedException
	{
		ChangeLockWallpaperSection.click();
		sleep(2);
		Reporter.log("\n clicked on change Lock Screen Wallerpaper",true );	
	}
	
	@FindBy(xpath=".//*[@id='knox']/ul/li/span[text()='RunScript to Reset Lock Screen Wallpaper on the device screen.']")
	private WebElement ResetLockScreenWallpaper;
	
	public void clickOnResetLockWallpaper() throws InterruptedException
	{
		ResetLockScreenWallpaper.click();
		sleep(2);
		Reporter.log("\n clicked on Reset Lock Screen Wallerpaper",true );	
	}
	@FindBy(xpath=".//*[@id='knox']/ul/li/span[text()='Clear App Data']")
	private WebElement ClearAppDataSection;
	
	public void ClickOnClearData() throws InterruptedException
	{
		ClearAppDataSection.click();
		sleep(2);
		Reporter.log("\n clicked on clear Data section",true );	
	}
	@FindBy(xpath=".//*[@id='knox']/ul/li/span[text()='Turn GPS OFF']")
	private WebElement TurnOFF_Gps;
	public void clickOnTurnOFF_GPS() throws InterruptedException
	{
		TurnOFF_Gps.click();
		sleep(2);
	}
	public void ScrollStatusbarOnDeviceSide() throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb shell cmd statusbar expand-settings");
		sleep(2);
		
	}
	@FindBy(xpath=".//*[@id='all']/ul/li/span[text()='Open SureLock settings from MDM Without having to enter password']")
	private WebElement clickOnOpenSureLockSettingsFromMDMWithoutHavingToEnterPasswordButton;
	
	public void clickOnOpenSureLockSettingsFromMDMWithoutHavingToEnterPassword() throws InterruptedException
	{
		clickOnOpenSureLockSettingsFromMDMWithoutHavingToEnterPasswordButton.click();
		sleep(3);
	}
	
	public void LaunchSureLock() throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb shell am start -n com.gears42.surelock/com.gears42.surelock.ClearDefaultsActivity");
		sleep(3);
		
	}
	public void clickOnSkipButton() throws InterruptedException
	{
		sleep(4);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/skipButton").click();
		sleep(2);
		Reporter.log("\n sureLock settings Page is launched successfully with runscript",true);
		sleep(2);
	}
	public void ExitSurelock() throws InterruptedException 
	{
		common.commonScrollMethod("Exit SureLock");
		sleep(1);
		Reporter.log("\n Exit surelock settings page",true);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Exit SureLock']").click();
		sleep(2);
		Reporter.log("\n sureLock settings Page is launched successfully with runscript",true);
	}
	public void checkGpsIsTurnedOff() throws InterruptedException
	{
		
		List<WebElement> ls = Initialization.driverAppium.findElementsById("com.android.systemui:id/tile_label");
		
	    ls.get(1).click();
		
		
		sleep(2);
		String Actual = Initialization.driverAppium.findElementById("android:id/toggle").getText();
		System.out.println(Actual);
		String Expected = "OFF";
		String pass = "location is turned OFF using runscript";
		String fail = "location is not turned OFF using runscript";
		ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
		
	}
	
	@FindBy(xpath=".//*[@id='all']/ul/li/span[text()='Turn GPS ON']")
	private WebElement GPSturnON;
	
	public void ClickOnTurnOnGPS() throws InterruptedException
	{
		GPSturnON.click();
		sleep(2);
	}
	
    public void checkGPSisTurnedON() throws InterruptedException
    {
    	List<WebElement> ls = Initialization.driverAppium.findElementsById("com.android.systemui:id/tile_label");
		
	    ls.get(1).click();
		
		
		sleep(2);
		String Actual = Initialization.driverAppium.findElementById("android:id/toggle").getText();
		System.out.println(Actual);
		String Expected = "ON";
		String pass = "location is turned ON using runscript";
		String fail = "location is not turned ON using runscript";
		ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
    }
    
    @FindBy(xpath=".//*[@id='knox']/ul/li/span[text()='Turn NFC ON/OFF']")
    private WebElement TrunONOFFNfc;
    public void clickOnTurnON_OFF_NFC() throws InterruptedException
    {
    	TrunONOFFNfc.click();
    	sleep(2);
    }
    
    public void ClickOnSearchedRunScript(String RunscriptName) throws InterruptedException
    {
    	Initialization.driver.findElement(By.xpath("//span[contains(text(),'"+RunscriptName+"')]")).click();
    	sleep(3);
    }
    
    @FindBy(xpath="//div[@id='scriptmodalpop']//input")
    private WebElement RunScriptPassswordTextField;
    
    public void SendindPasswordToRunScript(String text) throws InterruptedException
    {
    	RunScriptPassswordTextField.sendKeys(text);
    	sleep(3);
    }
    
    public void ResettingSureLockAdminAccess() throws IOException, InterruptedException
    {
    	Runtime.getRuntime().exec("adb shell am start -S com.android.settings/.Settings\\$DeviceAdminSettingsActivity");
    	sleep(3);
    	Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureLock']").click();
    	sleep(3);
    	Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Deactivate']").click();
    	sleep(3);
    	Reporter.log("Device Admin Is Disabled",true);
    	
    }
    
	@FindBy(xpath=".//*[@id='runscriptmodal']/div/div/div/ul/li/a[text()='Generic']")
	private WebElement Generic;

	public void clickOnGenericSection() throws InterruptedException
	{
		Generic.click();
		sleep(2);
	}
	@FindBy(xpath=".//span[text()='To navigate SureLock Admin settings on the device']")
	private WebElement SurelockAdmin;

	public void ClickOnNavigateSurelockAdminSettings() throws InterruptedException
	{
		SurelockAdmin.click();
		sleep(2);
		Reporter.log("\n clicked on SurelockAdmin section",true );	

	}

	@FindBy(xpath="//*[@id='scriptmodalpop']/div/div/div[2]/div/div/p/input")
	private WebElement scriptmodalpopInput;
	
	
	public void ClickOnscriptmodalpopInput(String EnterSLpwd) throws InterruptedException
	{
		scriptmodalpopInput.sendKeys(EnterSLpwd);
		sleep(2);
		Reporter.log("\n Input on scriptmodalpopInput section",true );	

	}

    
    
    
    
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
