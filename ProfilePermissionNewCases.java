package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ProfilePermissionNewCases extends Initialization {
	
	@Test(priority='1',description="1.Verify Enabling/disabling 'Select Unapproved Android Enterprise Applications' permission for Sub-User for AFW ")
	public void TC_ST_450() throws InterruptedException{		
	Reporter.log("\n1.Verify Enabling/disabling 'Select Unapproved Android Enterprise Applications' permission for Sub-User for AFW ",true);
	commonmethdpage.ClickOnSettings();
	usermanagement.ClickOnUserManagementOption();
	usermanagement.VerifyUserManagementIsDisplayed();
	usermanagement.InputRolesDescription(Config.InputProfilePermissionRoles, "FOR USER F");
	usermanagement.CreateRoleProfilePermission_SelectUnapprovedAndroidEnterpriseApplications();
	usermanagement.CreateDemoUser1(Config.InputUser3,Config.pswd,Config.pswd,Config.InputProfilePermissionRoles);
	usermanagement.UsermanagementWindowsSwitch();
	commonmethdpage.ClickOnSettings();
    usermanagement.logoutAsAdmin();
	loginPage.ClickOnLoginAgainLink();
	usermanagement.LoginAsNewUser(Config.InputUser3,Config.pswd);
	commonmethdpage.ClickOnSettings();
	usermanagement.ClickOnUserManagementOption();
	usermanagement.VerifyUserManagementIsDisplayed();
	usermanagement.ClickOnRoles();
	usermanagement.SearchRole(Config.InputProfilePermissionRoles);
	usermanagement.selectRole();
	usermanagement.ClickOnEdit();
	usermanagement.VerifyEnable_SelectUnapprovedAndroidEnterpriseApplications();
	usermanagement.SwitchToMainWindow();
    commonmethdpage.ClickOnSettings();
    usermanagement.ClickOnUserManagementOption();
	usermanagement.VerifyUserManagementIsDisplayed();
	usermanagement.ClickOnRoles();
	usermanagement.SearchRole(Config.InputProfilePermissionRoles);
	usermanagement.selectRole();
	usermanagement.ClickOnEdit();
	usermanagement.Disable_SelectUnapprovedAndroidEnterpriseApplications();
	usermanagement.SwitchToChildWindow();
	commonmethdpage.ClickOnSettings();
    usermanagement.logoutAsAdmin();
	loginPage.ClickOnLoginAgainLink();
	usermanagement.LoginAsNewUser(Config.InputUser3,Config.pswd);
	commonmethdpage.ClickOnSettings();
	usermanagement.ClickOnUserManagementOption();
	usermanagement.VerifyUserManagementIsDisplayed();
	usermanagement.ClickOnRoles();
	usermanagement.SearchRole(Config.InputProfilePermissionRoles);
	usermanagement.selectRole();
	usermanagement.ClickOnEdit();
	usermanagement.VerifyDisable_SelectUnapprovedAndroidEnterpriseApplications();
    usermanagement.CloseBrowsers();
	usermanagement.SwitchToMainWindow();
	}	
	@Test(priority='2',description="1.Permission in roles to move folder Android folder")
	public void TC_ST_627() throws InterruptedException{
   Reporter.log("\n2.Permission in roles to move folder Android folder",true);
   commonmethdpage.ClickOnSettings();
   usermanagement.logoutAsAdmin();
   loginPage.ClickOnLoginAgainLink();
   usermanagement.LoginAsNewUser(Config.userID, Config.password);
   commonmethdpage.ClickOnSettings();
   usermanagement.ClickOnUserManagementOption();
   usermanagement.VerifyUserManagementIsDisplayed();
   usermanagement.VerifyProfilePermissionrolesto_move_folder_Android();
	}
	

	@Test(priority='3',description="1.Permission in roles to move folder iOS folder")
	public void TC_ST_628() throws InterruptedException{
		
   Reporter.log("\n3.Permission in roles to move folder iOS folder",true);
//   commonmethdpage.ClickOnSettings();
//   usermanagement.ClickOnUserManagementOption();
//   usermanagement.VerifyUserManagementIsDisplayed();
   usermanagement.VerifyProfilePermissionrolesto_move_folder_IOS();

	}
	
	
	@Test(priority='4',description="1.Permission in roles to move folder Windows folder")
	public void TC_ST_629() throws InterruptedException{
		
   Reporter.log("\n4.Permission in roles to move folder Windows folder",true);
//   commonmethdpage.ClickOnSettings();
//   usermanagement.ClickOnUserManagementOption();
//   usermanagement.VerifyUserManagementIsDisplayed();
   usermanagement.VerifyProfilePermissionrolesto_move_folder_Window();
}	
	@Test(priority='5',description="1.Permission in roles to move folder macOS folder")
	public void TC_ST_630() throws InterruptedException{
		
   Reporter.log("\n5.Permission in roles to move folder macOS folder",true);
//   commonmethdpage.ClickOnSettings();
//   usermanagement.ClickOnUserManagementOption();
//   usermanagement.VerifyUserManagementIsDisplayed();
   usermanagement.VerifyProfilePermissionrolesto_move_folder_MacOs();
	}				
}	
		
		

