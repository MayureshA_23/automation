package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

/*Precondition Give your device name in config*/

public class ClearData_Knox extends Initialization {
	
	@Test(priority=1, description="create a runscript to create App shortcut on home screen") 
	public void ClearAppData() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
	   	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.ClickOnClearData();
		runScript.ClickOnValidateButton();
		runScript.ErrorMessageToFillPackageName();
		runScript.sendPackageNameORpath(Config.ClearData_PackageName);
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("ClearDataRS");
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ClearDataRS");
		androidJOB.CheckStatusOfappliedInstalledJob("ClearDataRS",60);
		commonmethdpage.AppiumConfigurationCommonMethod("com.alc.filemanager","com.alc.filemanager.activities.MainActivity");
		dynamicjobspage.ClearDataDownLoadedApp();
		androidJOB.ClickOnHomeButtonDeviceSide();
	}
	
	@Test(priority=2, description="Runscript - Verify modifying the runscript job") 
	public void ClearAppDataModify() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.SearchJobInSearchField("ClearDataRS");
		androidJOB.ClickOnOnJob("ClearDataRS");
		androidJOB.clickOnModifyJob();
		runScript.EnterModifiedRunScriptName("ClearData");
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ClearData");
		androidJOB.CheckStatusOfappliedInstalledJob("ClearData",100);
		commonmethdpage.AppiumConfigurationCommonMethod("com.alc.filemanager","com.alc.filemanager.activities.MainActivity");
		dynamicjobspage.ClearDataDownLoadedApp();
		androidJOB.ClickOnHomeButtonDeviceSide();
	}

}
