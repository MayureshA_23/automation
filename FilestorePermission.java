package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import PageObjectRepository.FileStore_POM;

public class FilestorePermission extends Initialization {

	@Test(priority = '1', description = "1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException {
		Reporter.log("\n1.To Verify roles option is present", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
      
	}

	@Test(priority = '2', description = "2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException {
		Reporter.log("\n2.To Verify default super user", true);
		usermanagement.toVerifySuperUsertemplete();
	}

	@Test(priority = '3', description = "3.To create a roll for Filestore Permission")
	public void CreateRollForFileStore() throws InterruptedException {
		Reporter.log("\n3.To create a roll for Filestore Permission", true);
		usermanagement.FileStorePermissionRole();
	}

	@Test(priority = '4', description = "4.To create User for Device Permissions")
	public void CreateUserForFilePermission() throws InterruptedException {
		Reporter.log("\n4.To create User for Device Permissions", true);
		usermanagement.CreateUserForPermissions("AutoFileStorePermission", "42Gears@123","AutoFileStorePermission", "Filestore Permission"); // uncomment// remov// later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoFileStorePermission", "42Gears@123");
		Thread.sleep(5000);
	}

	@Test(priority = '5', description = "5 click on file store")
	public void ClickOnFileStore() throws InterruptedException {
		Reporter.log("\n5 click on file store", true);
		filestorepage.ClickOnFileStore();
		usermanagement.clickOnfilestore();
		Thread.sleep(3000);
	}

	@Test(priority = '6', description = "6.To create a roll for Filestore Permission")
	public void NewFolderEnable() throws Throwable {
		Reporter.log("\n6.To create a roll for Filestore Permission", true);
		usermanagement.NewFolderDisabled();
	}

	@Test(priority = '7', description = "7.To create a roll for Filestore Permission")
	public void FileUploadEnable() throws Throwable {
		Reporter.log("\n7.To create a roll for Filestore Permission", true);
		usermanagement.verifyFileuploadbutton();
	}

	@Test(priority = '8', description = "8.To Disable Filestore Permission")
	public void DisablingPermission() throws Throwable {
		Reporter.log("\n8.To Disable Filestore Permission", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.DisablingAllPermissons();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoFileStorePermission", "42Gears@123");
		usermanagement.RefreshCurrentPage();
		usermanagement.clickOnfilestore();
	}

	@Test(priority = '9', description = "9.New Folder is disabled")
	public void newFolderDisabled() throws Throwable {
		Reporter.log("\n9.New Folder is disabled", true);
		usermanagement.NewFolderDisabled();
	}

	@Test(priority = 'A', description = "10.Delete is disabled")
	public void DeleteDisabled() throws Throwable {
		Reporter.log("\n10.Delete is disabled", true);
		usermanagement.deleteButtonDisabled();
	}

	@Test(priority = 'B', description = "11.File upload is disabled")
	public void FileUploadDisabled() throws Throwable {
		Reporter.log("\n11.File upload is disabled", true);
		usermanagement.FileUploadButtonDisabled();
	}

}
