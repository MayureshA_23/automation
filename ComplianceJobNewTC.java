package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ComplianceJobNewTC extends Initialization  {
	
	//PasswordPolicyJob
	//"passwordPolicyProfileAutomation" job name should be deployed in device
	//"JTouch Job"  Job should be present in console 
	
	
	@Test(priority=1,description="\n Deploy one passwordpolicy job from profile")
	public void EnforcePasswordPolicyProfile() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("passwordPolicyProfileAutomation"); 
		androidJOB.JobInitiatedMessage(); 
		androidJOB.CheckStatusOfappliedInstalledJob("passwordPolicyProfileAutomation",360);
	}
	
	//Remove comment
	
	@Test(priority=2, description="\n verify creating compliance job with Password policy and verify all out of Compliance actions")
	public void ComplianceJobSendMessage() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n verify creating compliance job with Password policy and verify all out of Compliance actions",true);
		
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnPasswordPolicyComplianceJob();
		androidJOB.ClickOnConfigureButton("passcode_BLK");
		androidJOB.outOfComplianceActions("Send Message",2);
		androidJOB.EnterJobNameTextField("PasswordPolicySendMessage");
		androidJOB.ClickOnSaveButtonComplianceJob();
		commonmethdpage.ClickOnHomePage();

		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PasswordPolicySendMessage"); 
		androidJOB.JobInitiatedMessage(); 
		androidJOB.CheckStatusOfappliedInstalledJob("PasswordPolicySendMessage",360);
		androidJOB.ClickOnInbox();
	    androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
	
		
		
		
		}

	//Remove comment
	
	@Test(priority=3, description="\n verify creating compliance job with Password policy and verify all out of Compliance actions")
	public void ComplianceJobMoveToBlacklist() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n verify creating compliance job with Password policy and verify all out of Compliance actions",true);
		
	
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnPasswordPolicyComplianceJob();
		androidJOB.ClickOnConfigureButton("passcode_BLK");
		androidJOB.outOfComplianceActions("Move to Blocklist",2);
		androidJOB.EnterJobNameTextField("PasswordPolicyMoveToBlackList");
		androidJOB.ClickOnSaveButtonComplianceJob();
		commonmethdpage.ClickOnHomePage();
	
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PasswordPolicyMoveToBlackList"); 
		androidJOB.JobInitiatedMessage(); 
		androidJOB.CheckStatusOfappliedInstalledJob("PasswordPolicyMoveToBlackList",360);
		Thread.sleep(2000);

		blacklistedpage.ClickOnBlacklisted();
		blacklistedpage.ClickOnRefreshButton();
		blacklistedpage.ClickOnDeviceToWhiteList();
		blacklistedpage.ClickOnWhitelistButton();
		blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
		blacklistedpage.ClickOnHomeGroup();

		
//		androidJOB. ClickOnBlackListButton();
//		androidJOB.VerifyDevicePresentInBlackList();
//		androidJOB.VerifyIfDevicePresentThenMoveToWhiteList();
//		androidJOB.AllDeviceOption();
//		androidJOB.ClearSearchFieldConsole();
//		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		
		}
	
	
	
	@Test(priority=4, description="\n verify creating compliance job with Password policy and verify all out of Compliance actions")
	public void ComplianceJobApplyJob() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n verify creating compliance job with Password policy and verify all out of Compliance actions",true);
		
	
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnPasswordPolicyComplianceJob();
		androidJOB.ClickOnConfigureButton("passcode_BLK");
		androidJOB.outOfComplianceActions("Apply Job",2);
		androidJOB.AddButtonClick("JTouch Job");
		androidJOB.EnterJobNameTextField("PasswordPolicyApplyJob");
		androidJOB.ClickOnSaveButtonComplianceJob();
		commonmethdpage.ClickOnHomePage();
	
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PasswordPolicyApplyJob"); 
		androidJOB.JobInitiatedMessage(); 

		androidJOB.CheckStatusOfappliedInstalledJob("PasswordPolicyApplyJob",360);
		
		}
	
	
	
	
	@Test(priority=5, description="\n verify creating compliance job with Password policy and verify all out of Compliance actions")
	public void ComplianceJobSendSms() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n verify creating compliance job with Password policy and verify all out of Compliance actions",true);
		
	
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnPasswordPolicyComplianceJob();
		androidJOB.ClickOnConfigureButton("passcode_BLK");
		androidJOB.outOfComplianceActions("Send Sms",2);
		androidJOB.EnterJobNameTextField("PasswordPolicySendSMS");
		androidJOB.ClickOnSaveButtonComplianceJob();
		commonmethdpage.ClickOnHomePage();
	
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PasswordPolicySendSMS"); 
		androidJOB.JobInitiatedMessage(); 
		androidJOB.CheckStatusOfappliedInstalledJob("PasswordPolicySendSMS",360);
		
//	    androidJOB.AppiumConfigurationforNIX();
//	    androidJOB.VerifyComplanceSendSMS();
	
	
	}
	
	
	

	@Test(priority=6, description="\n verify creating compliance job with Password policy and verify all out of Compliance actions")
	public void ComplianceJobEmailNotification() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n verify creating compliance job with Password policy and verify all out of Compliance actions",true);
		
	
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnPasswordPolicyComplianceJob();
		androidJOB.ClickOnConfigureButton("passcode_BLK");
		androidJOB.outOfComplianceActions("E-Mail Notification",2);
		androidJOB.EnterJobNameTextField("PasswordPolicyE-MailNotification");
		androidJOB.ClickOnSaveButtonComplianceJob();
		commonmethdpage.ClickOnHomePage();
	
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PasswordPolicyE-MailNotification"); 
		androidJOB.JobInitiatedMessage(); 

		androidJOB.CheckStatusOfappliedInstalledJob("PasswordPolicyE-MailNotification",360);

        androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");
	
	}
	
	@Test(priority=7, description="\n verify creating compliance job with Password policy and verify all out of Compliance actions")
	public void ComplianceJobLockDevice() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("\n verify creating compliance job with Password policy and verify all out of Compliance actions",true);
		
	
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnPasswordPolicyComplianceJob();
		androidJOB.ClickOnConfigureButton("passcode_BLK");
		androidJOB.outOfComplianceActions("Lock Device (Android / iOS/iPadOS)",2);
		androidJOB.EnterJobNameTextField("PasswordPolicyLockDevice");
		androidJOB.ClickOnSaveButtonComplianceJob();
		commonmethdpage.ClickOnHomePage();
	
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PasswordPolicyLockDevice"); 
		androidJOB.JobInitiatedMessage(); 
		androidJOB.CheckStatusOfappliedInstalledJob("PasswordPolicyLockDevice",360);
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		
		androidJOB.VerifyIsDeviceLocked();
		dynamicjobspage.UnLockingDeviceUsingADB();

		
	
}
	
		
}
