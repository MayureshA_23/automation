package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class ApplicationPolicy extends Initialization {

/* Application Policy */
	@Test(priority='0',description="Precondition to add Application in Appstore") 
	public void Precondition() throws Throwable {
		Reporter.log("Precondition to add Application in Appstore");
		appstorepage.ClickOnAppStore();
		appstorepage.ClickOniOSAppStore();
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnUploadIPAiOS();
		appstorepage.ClickOnBrowseFileiOS();
		appstorepage.EnterCategoryiOS();
		appstorepage.EnterDescriptioniOS();
		appstorepage.ClickOnUploadipaPopupAddButton();
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnUploadIPAiOS();			
		appstorepage.ClickOnBrowseFileiOS("./Uploads/UplaodFiles/AppStoe-iOS/\\File4.exe"); 
		appstorepage.EnterCategoryiOS("123Movies");
		appstorepage.EnterDescriptioniOS("123Movies");
		appstorepage.ClickOnUploadipaPopupAddButton();


	}


	@Test(priority='1',description="1.To verify clicking on Application Policy") 
	public void VerifyClickingOnApplicationPolicy() throws InterruptedException {
		Reporter.log("To verify clicking on Application Policy");
		profilesAndroid.ClickOnProfile();
		profilesiOS.clickOniOSOption();
		profilesiOS.AddProfile();
		profilesiOS.ClickOnApplicationPolicy();
	}

	@Test(priority='2',description="1.To verify summary of Application Policy Profile")
	public void VerifySummaryofRestrictionProfile() throws InterruptedException
	{
		profilesiOS.VerifySummaryofApplicationPolicyProfile();
	}

	@Test(priority='3',description="1.To Verify warning message Saving a Application Policy Profile without Name")
	public void VerifyWarningMessageOnSavingAApplicationPolicyProfile() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Application Policy Profile without Name=========");
		profilesiOS.ClickOnConfigureButtonApplicationPolicyProfile();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();          
	}

	@Test(priority='4',description="1.To Verify warning message Saving a Application Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingApplicationProfileWihtoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Application Profile without entering all the fields=========");
		profilesiOS.EnterApplicationPolicyProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();

	}
	@Test(priority='5',description="1.To Verify cancelling Add App window ")
	public void VerifyCancellingAddAppWindow() throws InterruptedException{
		Reporter.log("=======To Verify cancelling Add App window=========");
		profilesiOS.ClickOnAddButon_ApplicationPolicy();
		profilesiOS.ClicOnCancelButtonAddAppWindow();
		profilesiOS.isColumnNamePresent();
	}


	/*
	@Test(priority='5',description="1.To Verify warning message without adding Application Name in Application Policy")
	public void VerifyWarningMessageWithoutApplicationInAddApp() throws InterruptedException{
		Reporter.log("=======To Verify warning message without adding Application Name in Application Policy=========");
		//profilesiOS.ClickOnAddButon_ApplicationPolicy(); bug exits so commented
	}*/
	@Test(priority='6',description="1.To Verify choosing 1st Application")
	public void VerifyChoosing1stApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======1.To Verify choosing Application=========");
		profilesiOS.ClickOnAddButon_ApplicationPolicy();
		profilesiOS.ChooseAppApplicationPolicyProfile();
		profilesiOS.SelectingApplicationmFromtheAddAppDropdown();
		profilesiOS.ClickOnInstallSilently();
	}

	@Test(priority='7',description="1.To Verify saving the selected Application")
	public void VerifySavingTheSelectedApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify saving the selected Application=========");
		profilesiOS.ClickOnAddButtonAddAppWindow();
		profilesiOS.isAddedApp1DisplayedinTheList();
	}

	@Test(priority='8',description="1.To Verify warning message while choosing the same application and saving it")
	public void VerifyWarningMessageOneChoosingAndAddingTheSameApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify choosing 2nd application saving the selected Application=========");
		profilesiOS.ClickOnAddButon_ApplicationPolicy();
		profilesiOS.ChooseAppApplicationPolicyProfile();
		profilesiOS.SelectingApplicationmFromtheAddAppDropdown();//selecting the same application facebook
		profilesiOS.ClickOnInstallSilently();
		profilesiOS.ClickOnAddButtonAddAppWindow();
		profilesiOS.WarningMessageOnAddingTheSameApplcation();
	}

	@Test(priority='9',description="1.To Verify choosing different app and saving it")
	public void VerifyChoosing2ndApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify choosing different app and saving it=========");
		profilesiOS.ChooseAppApplicationPolicyProfile();
		profilesiOS.SelectingDifferentApplicationmFromtheAddAppDropdown();
		profilesiOS.ClickOnAddButtonAddAppWindow();
		profilesiOS.isAddedApp2DisplayedinTheList();
	}

	@Test(priority='A',description="1.To Verify Config of Application Policy")
	public void VerifyConfigOfApplicationPolicy() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify Config of Application Policy=========");
		profilesiOS.VerifyConfig_ApplicationPolicy();
	}

	@Test(priority='B',description="1.To Verify Editing of Application Policy")
	public void VerifyEditOfApplicationPolicy() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify Editing of Application Policy=========");
		profilesiOS.ClickOnEditButton();
		profilesiOS.isEditWindowDisplayed();//just verifying if the edit window is launched.
		profilesiOS.ClicOnCancelButtonAddAppWindow();//just Canceling the edit window.
	}
	@Test(priority='C',description="1.To Verify warning message while Deleting of an application")
	public void VerifyWarningMessageWhileDeletingAnApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify Deleting of an application=========");
		profilesiOS.ClickOnMyntraApp();
		profilesiOS.ClickOnDeleteButtonApplicationPolicy();
		profilesiOS.WarningMessageOnApplicationDelete_ApplicationPolicy();
	}

	@Test(priority='D',description="1.To Verify Deleting the application")
	public void VerifyDeletingApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify Deleting the application=========");
		profilesiOS.ClickOnYesButton_DeleteWarning();
	}

	@Test(priority='E',description="1.To Verify Saving the Application Policy Profile")
	public void VerifySavingApplicationPolicyProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify saving application policy profile=========");
		profilesiOS.ClickOnSavebutton();
		profilesAndroid.NotificationOnProfileCreated();

	}










}
