package DynamicRefresh_JobQueue_QuickAction;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class MiscellaneousGridPerformance extends Initialization
{
	@Test(priority='1',description="To Verify Grid Performance Settings")
    public void VerifyMiscellaneousGridPerformance() throws InterruptedException
    {
    	commonmethdpage.ClickOnSettings();
 		accountsettingspage.ClickOnAccountSettings();
 		accountsettingspage.ClickOnMiscellaneousSettings();
 		MiscellaneousGridPerformance.verifyMiscellaneousGridPerformance();
 		
    }
	@Test(priority='2',description="To Verify Temperature Unit Default Value")
	public void VerifyMiscellaneousTemperatureUnitDefaultValue() throws InterruptedException
	{
 		MiscellaneousGridPerformance.VerifyTemperatureUnitDefaultValueInMicellaneousSettings();
	}
	@Test(priority='3',description="To Verify Changing Celsius To Fahrenheit")
	public void VerifyTemperatureChangeFromCelsiusToFahrenheit() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		MiscellaneousGridPerformance.ChangingCelciusToFahrenheit();
		MiscellaneousGridPerformance.ClickingOnApplyButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		MiscellaneousGridPerformance.VerifyingUnitChangeInConsoleFromCelsiusToFahrenheit();	
	}
	@Test(priority='4',description="To Verify Temperature Unit Change In Console Side")
	public void VerifyTemperaturehangeFromFahrenheit() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnSettings();
 		accountsettingspage.ClickOnAccountSettings();
 		accountsettingspage.ClickOnMiscellaneousSettings();
 		MiscellaneousGridPerformance.ChangingFahrenheitToCelcius();
 		MiscellaneousGridPerformance.ClickingOnApplyButton();
 		commonmethdpage.ClickOnHomePage();
 		commonmethdpage.SearchDevice(Config.DeviceName);
		MiscellaneousGridPerformance.VerifyingUnitChangeInConsoleFormFahrenheitToCelsius(); 		
	}
	
	@Test(priority='5',description="To Verify EULA Disclaimer Policy UI For Default Status")
	public void VerifuEULADisClaimerUIForDefaultCondition() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
 		accountsettingspage.ClickOnAccountSettings();
 		accountsettingspage.ClickOnMiscellaneousSettings();
 		MiscellaneousGridPerformance.ClickingOnEulaDisclaimerPolicy();
 		MiscellaneousGridPerformance.VerifyEulaDisclaimerDefaultStatus();
 		//MiscellaneousGridPerformance.VerifyEulaDisclaimerTextBoxWhenCheckBoxIsDisabled();
	}

	@Test(priority='5',description="To Verify EULA Disclaimer Policy In Account Settings")
	public void VerifyEULADisclaimerPolicy() throws InterruptedException
	{
 		MiscellaneousGridPerformance.VerifyEulaDisclaimerTextBoxWhenCheckBoxISEnabled();
 		MiscellaneousGridPerformance.ClickingOnEulaDisclaimerSaveButton();
 		MiscellaneousGridPerformance.ClickingOnEulaDisclaimerSaveButton();
	}
	
}
