package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DataUsage_CustomReport extends Initialization{
@Test(priority='0',description="Verify Sort by and Group by for Datausage")
public void VerifyDataUsageReport_TC_RE_110() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for Datausage",true);
		reportsPage.ClickOnReports_Button();	
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnDataUsageCol();
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDataUsage);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Datausage CustomReport for SortBy And GroupBy","test");	
		customReports.Selectvaluefromsortbydropdown(Config.SelectMobileData);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);	
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("Datausage CustomReport for SortBy And GroupBy");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Datausage CustomReport for SortBy And GroupBy");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Datausage CustomReport for SortBy And GroupBy");
		reportsPage.WindowHandle();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();		
	}
@Test(priority='1',description="Verify generating Data Usage Custom report without filters")
public void VerifyingDatausageForBothWifiAndMobData() throws InterruptedException
	{Reporter.log("\nVerify generating Data Usage Custom report without filters",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnDataUsageCol();
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDataUsage);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DataUsage CustomRep without filters","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("DataUsage CustomRep without filters");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("DataUsage CustomRep without filters");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DataUsage CustomRep without filters");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DataUsage CustomRep without filters");
		reportsPage.WindowHandle();
		customReports.VerifyingDataUsage();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='2',description="Verify generating DataUsage Customreport with filters for 'Current Date' & Home group")
public void VerifyingDataUsageForMobDataWrtFilter() throws InterruptedException
	{Reporter.log("\nVerify generating DataUsage Customreport with filters for 'Current Date' & Home group",true);
		commonmethdpage.ClickOnHomePage();
	    reportsPage.ClickOnReports_Button();
	    customReports.ClickOnCustomReports();
	    customReports.ClickOnAddButtonCustomReport();
	    customReports.ClickOnDataUsageCol();
	    customReports.ClickOnAddButtonInCustomReport();
	    customReports.VerifyTableInSelectedTableList(Config.SelectDataUsage);
	    customReports.Selectvaluefromdropdown(Config.SelectDataUsage);
		customReports.SelectValueFromColumn(Config.SelectMobileData);
		customReports.SelectOperatorFromDropDown(Config.SelectGreaterThanOrEqualtoOperator);
		customReports.SendValueToTextfield(Config.Data_usage1);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DataUsage CustomRep with filters for Current Date and Homegroup","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DataUsage CustomRep with filters for Current Date and Homegroup");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("DataUsage CustomRep with filters for Current Date and Homegroup");
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DataUsage CustomRep with filters for Current Date and Homegroup");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DataUsage CustomRep with filters for Current Date and Homegroup");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerificationOfMobileDataUsageColumn();
		customReports.VarifyingOneWeekDateInDataUsageViewReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='3',description="Verify generating Data Usage Custom report with filters for 'Current Date' & Sub group")
public void VerifyingDatausageForMobData() throws InterruptedException
	{Reporter.log("\nVerify generating DataUsage Customreport with filters for 'Current Date' & Sub group",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnDataUsageCol();
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDataUsage);
		customReports.Selectvaluefromdropdown(Config.SelectDataUsage);
		customReports.SelectValueFromColumn(Config.SelectMobileData);
		customReports.SelectOperatorFromDropDown(Config.SelectLessThanOrEqualtoOperator);
		customReports.SendValueToTextfield(Config.SendDataUsage);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DataUsage CustomRep with filters for Current Date and Subgroup","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DataUsage CustomRep with filters for Current Date and Subgroup");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("DataUsage CustomRep with filters for Current Date and Subgroup");
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DataUsage CustomRep with filters for Current Date and Subgroup");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DataUsage CustomRep with filters for Current Date and Subgroup");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerifyingMobileDataUsageInDataUsageRep();
		customReports.VarifyingOneWeekDateInDataUsageViewReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='4',description="Verify generating Data Usage Custom report with filters for '1week Date'")
public void VerifyingDatausageForWifi() throws InterruptedException
	{Reporter.log("\nVerify generating Data Usage Custom report with filters for '1week Date'",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnDataUsageCol();
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDataUsage);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DataUsage CustomRep with filters for one week Date","test");
		customReports.Selectvaluefromdropdown(Config.SelectDataUsage);
		customReports.SelectValueFromColumn(Config.SelectDateTime);
		customReports.SelectOneWeekDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DataUsage CustomRep with filters for one week Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("DataUsage CustomRep with filters for one week Date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DataUsage CustomRep with filters for one week Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DataUsage CustomRep with filters for one week Date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingOneWeekDateInDataUsageViewReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
@Test(priority='5',description="Verify generating Data usage Custom report with filters for 'Yesterday' date")
public void VerifyingDatausageAfterSorting() throws InterruptedException
	{Reporter.log("\nVerify generating Datausage Customreport with filters for 'Yesterday' date",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnDataUsageCol();
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDataUsage);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Datausage Customreport with filters for Yesterday date","test");
		customReports.Selectvaluefromdropdown(Config.SelectDataUsage);
		customReports.SelectValueFromColumn(Config.SelectWifiData);
		customReports.SelectOperatorFromDropDown(Config.SelectNotEqualtoOperator);
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Datausage Customreport with filters for Yesterday date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Datausage Customreport with filters for Yesterday date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Datausage Customreport with filters for Yesterday date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Datausage Customreport with filters for Yesterday date");
		reportsPage.WindowHandle();	
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingYestDateInDataUsageRep();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
      }
}
}
