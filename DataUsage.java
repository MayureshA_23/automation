package DataUsage;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DataUsage extends Initialization 
{

	@Test(priority=1,description="Reading Data Usage In Device")
    public void ReadingNixDataUsage() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.CheckingLocationTrackingStatus();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.ClickingOnTurnOnButton();
    	quickactiontoolbarpage.TurningOnLocationTracking("2");
    	//Reading OverAll Data Usage
    	androidJOB.AppiumConfigurationForMultipleDevices("com.android.settings","com.android.settings.Settings",Config.SerialNumber_Samsung);
		deviceGrid.SearchingInsideSettings("Data usage");
		nixdatausage.ClickOnElementsInSettings("Data usage");
		nixdatausage.ClickOnElementsInSettings("Wi-Fi data usage");
		nixdatausage.ReadingOverAllDataUsage();
		
		//Reading Nix Data Usage
		androidJOB.AppiumConfigurationForMultipleDevices("com.android.settings","com.android.settings.Settings",Config.SerialNumber_Samsung);
		deviceGrid.SearchingInsideSettings("SureMDM Nix");
		nixdatausage.ReadingDataUsageOfNixApplication();
		
		//Reading Google Play Service Data Usage
		androidJOB.AppiumConfigurationForMultipleDevices("com.android.settings","com.android.settings.Settings",Config.SerialNumber_Samsung);
		deviceGrid.SearchingInsideSettings("Google Play services");
		nixdatausage.ReadingDataUsageOfGooglePlayServiceApplication();
		
		//Waiting for 15 mins
		quickactiontoolbarpage.WaitingForUpdate(900);
		
		//Reading Over all Data Usage After 15 Mins
		androidJOB.AppiumConfigurationForMultipleDevices("com.android.settings","com.android.settings.Settings",Config.SerialNumber_Samsung);
		deviceGrid.SearchingInsideSettings("Data usage");
		nixdatausage.ClickOnElementsInSettings("Data usage");
		nixdatausage.ClickOnElementsInSettings("Wi-Fi data usage");
		nixdatausage.ReadingOverAllDataUsageAfter30Mins();
		
		//Reading Nix Data Usage After 15 Mins
		androidJOB.AppiumConfigurationForMultipleDevices("com.android.settings","com.android.settings.Settings",Config.SerialNumber_Samsung);
		deviceGrid.SearchingInsideSettings("SureMDM Nix");
		nixdatausage.ReadingDataUsageOfNixApplicationAfter30Mins();
		
		//Reading Google Play serviceData Usage After 15 Mins
		androidJOB.AppiumConfigurationForMultipleDevices("com.android.settings","com.android.settings.Settings",Config.SerialNumber_Samsung);
		deviceGrid.SearchingInsideSettings("Google Play services");
		nixdatausage.ReadingDataUsageOfGooglePlayServiceApplicationAfter30Mins();
		
		//Finding Difference of Data Usage
		nixdatausage.FindingOverAllDataDifference();
		nixdatausage.FindingNixDataUsageDifference();
		nixdatausage.FindingGooglePlayServiceDataUsageDifference();
    }
}
