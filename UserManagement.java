package Settings_TestScripts;
import java.io.IOException;
import javax.security.auth.login.AccountNotFoundException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class UserManagement extends Initialization{

	@Test(priority=1,description="Verify Editing an old Sub User which has Super user roles") 
	public void VerifyEditinganoldSubUserwhichhasSuperuserrolesTC_ST_862() throws Throwable {
		Reporter.log("\n1.Verify Editing an old Sub User which has Super user roles",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickedonUserManagement();
		accountsettingspage.SelectSuperUser();
		accountsettingspage.SuperuserisVisible();		
}
		
	//set pre condition in config 
	//String url = "https://licensedindia.in.suremdm.io/console/"; String userID ="Mithilesh_Superuser"; String password = "Mithilesh@123";
//	@Test(priority=2,description="Verify Editing an old Sub User  which has Super user roles by logging in with sub user creds") 
//	public void VerifyEditinganoldSubUserwhichhasSuperuserrolesbylogginginwithsubusercredsTC_ST_863() throws Throwable {
//		Reporter.log("\n2.Verify Editing an old Sub User  which has Super user roles by logging in with sub user creds",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickedonUserManagement();
//		accountsettingspage.SelectSuperUser();
//		accountsettingspage.SuperuserisVisible();		
//}
	
	
	
	
	
	
	
	
	
}
