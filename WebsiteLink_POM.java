package PageObjectRepository;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.WebDriverCommonLib;

public class WebsiteLink_POM extends WebDriverCommonLib {
	
	@FindBy(xpath=".//*[@id='login_page_body']/article/footer/p/a")
	private WebElement websiteLink;
	
	
	
	public void VerifyClickingOn42GearsWebsiteLink() throws InterruptedException{
		String originalHandle = Initialization.driver.getWindowHandle();
		websiteLink.click();
		Initialization.driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
	    String ActualValue = Initialization.driver.getTitle();
	    System.out.println(ActualValue);
	     ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
	     Initialization.driver.switchTo().window(tabs.get(1));
	     
	     for(String handle : Initialization.driver.getWindowHandles()) {
		        if (!handle.equals(originalHandle)) {
		        	Initialization.driver.switchTo().window(handle);
		        	Initialization.driver.close();
		        }
		    }

		  Initialization.driver.switchTo().window(originalHandle);
	      sleep(3);
		
	}

}
