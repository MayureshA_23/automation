package UserManagement_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//PRE-CONDITIONS which should not be present in console 

//Group-"@SourceDonotTouch" group,
//Tag-"#Auto0001"
//Role and Description for Device Group Set-"EnableGroup_@SourceDonotTouch","Automation1"
//Role and Description for Device Group Set-"DisableGroup_@SourceDonotTouch","Automation"
//Role and Description for ROLES-  "ALLRolespermissions","Roles" 
//Role and Description for ROLES- "ALLRolespermissions1","Roles"
// User -"DemouserDeviceGroupSetEnabled"

public class DeviceGroupSetNewCases extends Initialization {

	@Test(priority = '1', description = "Verify devices in tags if particular groups are disabled for the sub User.")
	public void GroupsTC_ST_480()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		Reporter.log("\n1.Verify devices in tags if particular groups are disabled for the sub User.", true);
		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName(Config.SourceGroupName);
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.ClickOnHomeGrup();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		accountsettingspage.SelectMultipleDevice();
		accountsettingspage.RightClickOndeviceInsideGroups(Config.DeviceName);
		accountsettingspage.MoveToGroupMultiple1();

		// Adding Tags to device
		Tags.ClickOnTags1();
		Tags.CreatTag("#Auto0001");
		Tags.ClickOnGroup();
		commonmethdpage.SearchDevice();
		Tags.RightClickTag(Config.DeviceName);
		Tags.taglistpopupSearchTextField2("#Auto0001");
		Tags.EnableTheSearchedTagNameFromTheList3();
		Tags.ClickOnSaveTagButton();

	}

	@Test(priority = '2', description = "Verify devices in tags if particular groups are disabled for the sub User.")
	public void GroupsTC_ST_480Continue()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {
		Reporter.log("\n2.Verify devices in tags if particular groups are disabled for the sub User.", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnDeviceGroupSet();
		usermanagement.ClickAddDeviceGroupSet();
		usermanagement.InputRolesDescriptionDeviceGroupSet(Config.InputRoles_Desc_DeviceGroupSet, "Automation1 user");
		usermanagement.DeviceGroupSet_SaveAllEnabledPermission(); // SaveDeviceGroupSet Permission
		usermanagement.RoleWithAllPermissions(Config.PermissionForAllRolesDeviceGrpSet1, "UserA"); // ROLES
		usermanagement.CreateUserForPermissionsAndDeviceGroupSet(Config.InputDeviceGrpSetUser, "42Gears@123", "UserA",
				Config.PermissionForAllRolesDeviceGrpSet1, Config.InputRoles_Desc_DeviceGroupSet); // User
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.InputDeviceGrpSetUser, "42Gears@123");
		Tags.ClickOnTags1();
		Tags.ScrollToCreatedTagName();
		Tags.VerifyDevicePresentInsideTags_UM();
	}

	@Test(priority = '3', description = "Verify devices in tags if particular groups are disabled for the sub User.")
	public void GroupsTC_ST_480Continue3()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {
		Reporter.log("\n3.Verify devices in tags if particular groups are disabled for the sub User.", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnDeviceGroupSet();
		usermanagement.ClickAddDeviceGroupSet();
		usermanagement.InputRolesDescriptionDeviceGroupSet(Config.InputRolesDescDeviceGrpSet_Disable,
				"Automation user");
		usermanagement.DisableDeviceGroupSet_SourceDonotTouch();

		usermanagement.RoleWithAllPermissions(Config.PermissionForAllRolesDeviceGrpSet2, "User Roles"); // ROLES
		usermanagement.ClickOnUserTab();
		usermanagement.SearchUserTextField1(Config.InputDeviceGrpSetUser);
		usermanagement.SelectUser(Config.InputDeviceGrpSetUser);
		usermanagement.ClickOnEditUser();
		usermanagement.EditUserPage(Config.PermissionForAllRolesDeviceGrpSet2,
				Config.InputRolesDescDeviceGrpSet_Disable);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.InputDeviceGrpSetUser, "42Gears@123");

		Tags.ClickOnTags1();
		Tags.ScrollToCreatedTagName();
		Tags.VerifyDeviceNotPresentInsideTags_UM();

	}

	@Test(priority = '4', description = "Delete Created Tag")
	public void Deletetag()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {
		Reporter.log("\4.Delete Created Tag",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);
		Tags.ClickOnTags1();
		Tags.DeleteTag("#Auto0001");

	}
}