package PageObjectRepository;

import java.awt.AWTException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class Filters_POM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();
	ExcelLib ELib = new ExcelLib();

	@FindBy(xpath = "(//span[text()='Filter'])[2]")
	private WebElement filter;

	@FindBy(xpath = "//span[text()='Update Filter']")
	private WebElement UpdateButton;

	@FindBy(xpath = ".//*[@id='newGroup']")
	private WebElement plusButton;

	@FindBy(xpath = "//*[@id='deleteGroup']")
	private WebElement MinusButton;

	@FindBy(xpath = "//span[text()='Save Filter']")
	private WebElement SaveButton;

	@FindBy(xpath = ".//*[@id='statusCol_searchinput']")
	private WebElement Status;

	@FindBy(xpath = "(//span[text()='Online Status'])[2]")
	private WebElement OnlineStatus;

	@FindBy(xpath = "//span[text()='Online']")
	private WebElement Online;

	@FindBy(xpath = "//span[text()='Online']")
	private WebElement onlineStatus;

	@FindBy(xpath = ".//*[@id='filterForm']/div/input")
	private WebElement NameFilter;

	@FindBy(id = "filterokbtn")
	private WebElement Ok;

	@FindBy(id = "all_ava_devices")
	private WebElement AllDevices;

	@FindBy(xpath = "//div [@id='groupstree']/ul/li[text()='Home']")
	private WebElement ClickOnHomeGroup;

	@FindBy(id = "deletegroupbtn")
	private WebElement DeleteFilter;

	@FindBy(xpath = "//span[text()='Groups']")
	private WebElement Groups;

	@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[1]/div[1]/div/button")
	private WebElement ColumnMenuButton;

	static int OnlineDeviceCountInGrid;
	static int OfflineDeviceCountInGrid;
	public String deviceName;

	public void ClickOnFilter() throws InterruptedException {
		filter.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(10);
		System.out.println("PASS >> Clicked on Filter, Filter option available");

	}

	public void ClickOnColumnMenuButton() throws InterruptedException {
		ColumnMenuButton.click();
		sleep(10);
	}

	public void ClickOnGroups() throws InterruptedException {
		Groups.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
	}

	public void DisableColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(
				By.xpath("//*[@id='tableContainer']/div[4]/div[1]/div[1]/div/div/ul/li/label/span/input"));
		ls.get(1).click();
		sleep(5);
	}

	public void ClickOnHomeGroup() throws InterruptedException {
		ClickOnHomeGroup.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(7);
	}

	@FindBy(xpath = "//span[text()='Offline']")
	private WebElement Offline;

	@FindBy(xpath = "//span[text()='Offline']")
	private WebElement OfflineStatus;

	public void SelectOfflineStatus() throws InterruptedException {

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(OnlineStatus).perform();
		sleep(1);
		act.moveToElement(Offline).perform();
		sleep(2);
		Offline.click();
		System.out.println("Selected Offline Status in Filter");

		WebDriverWait wait = new WebDriverWait(Initialization.driver, 30);

		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("deleteDeviceBtn")));

		sleep(6);

	}

	public void OnlineDevicesCountInGrid() {
		List<WebElement> OnlineDevices = Initialization.driver.findElements(By.xpath("//span[text()='Online']"));
		OnlineDeviceCountInGrid = OnlineDevices.size() - 1; // -1 is given because in the index for the first device
		System.out.println(">> Online Devices count in All Groups in device grid: " + OnlineDeviceCountInGrid);

	}

	public void OfflineDevicesCountInGrid() {
		List<WebElement> OfflineDevices = Initialization.driver.findElements(By.xpath("//span[text()='Offline']"));
		OfflineDeviceCountInGrid = OfflineDevices.size() - 1; // -1 is given because in the index for the first device
		System.out.println(">> Offline Devices count in All Groups in device grid: " + OfflineDeviceCountInGrid);
		System.out.println("");
	}

	public void VerifyFilterDisplayed() {
		boolean a = UpdateButton.isDisplayed();
		String pass = "PASS >> Update Filter Button is displayed, filter page is launched successfully";
		String fail = "FAIL>> Filter page is not launched";
		ALib.AssertTrueMethod(a, pass, fail);
	}

	public void ClickOnPlusButton() {
		plusButton.click();
	}

	public void ClickOnCreatedFilter(String xpath) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//li[text()='"+xpath+"']")).click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 30);

		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("deleteDeviceBtn")));

		sleep(8);
	}

	public void SelectOnlineStatus() throws InterruptedException {

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(OnlineStatus).perform();
		sleep(1);
		act.moveToElement(Online).perform();
		sleep(2);
		Online.click();
		System.out.println("Selected Online Status in Filter");

		WebDriverWait wait = new WebDriverWait(Initialization.driver, 30);

		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("deleteDeviceBtn")));

		sleep(6);

	}

	public void ClickOnUpdateFilter() throws InterruptedException {
		UpdateButton.click();
		sleep(4);
	}
	public void clickOnOkBtnInUpdate(String FilterName) throws InterruptedException
	{
		Ok.click();
		waitForXpathPresent("//span[contains(text(),'Filter [" + FilterName + "] updated successfully.')]");
	}
	public void ClickOnSaveFilterButton() throws InterruptedException {
		SaveButton.click();
		waitForXpathPresent("//h4[text()='Device Filter']");
		sleep(3);
	}

	public void ClickOnStatus() throws InterruptedException {
		Status.click();
		sleep(1);
	}

	public void ClickOnAllDevices() throws InterruptedException {
		AllDevices.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
	}

	public void VerifySaveFilterButton() throws InterruptedException {

		boolean value1 = SaveButton.isEnabled();
		String PassStatement1 = "PASS >> 'Save Filter Button' is displayed  when Clicked on plus Button";
		String FailStatement1 = "Fail >> 'Save Filter Button' is not displayed  when Clicked on plus Button";
		ALib.AssertTrueMethod(value1, PassStatement1, FailStatement1);
		sleep(2);
		System.out.println("");
	}

	public void EnterFilterName(String FilterName) throws InterruptedException {
		NameFilter.sendKeys(FilterName);
		sleep(2);
	}

	public void ClickOKButtonFilter(String FilterName) throws InterruptedException, AWTException {
		JavascriptExecutor executor = (JavascriptExecutor) Initialization.driver;
		executor.executeScript("arguments[0].click();", Ok);
		// Ok.click();
		System.out.println(Ok.isEnabled());
		waitForXpathPresent("//span[text()='Filter [" + FilterName + "] saved successfully.']");
		sleep(5);
		System.out.println("Filter Created Succesfully");
		sleep(2);

	}

	public void TakeLogs() {

		DesiredCapabilities caps = DesiredCapabilities.chrome();
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.BROWSER, Level.ALL);
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		// Initialization.driver = new ChromeDriver(caps);
	}

	public void analyzeLog() {
		LogEntries logEntries = Initialization.driver.manage().logs().get(LogType.BROWSER);
		for (LogEntry entry : logEntries) {
			System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());

		}
	}

	public void OnlineDeviceCountMatchFilterandGrid() {
		List<WebElement> OnlineDevices = Initialization.driver.findElements(By.xpath("//span[text()='Online']"));
		int OnlineDeviceCountInFilter = OnlineDevices.size() - 1;
		int Expected = OnlineDeviceCountInGrid;
		String pass = "PASS >> Online Devices in Filter: " + OnlineDeviceCountInFilter
				+ ", Online Device Count in Filter and Grid Matching";
		String fail = "FAIL >> Online Devices in Device Grid and Filters are not matching";

		if (OnlineDeviceCountInFilter != Expected) {
			String FailMessage = "FAIL >> Online Devices in Filter: " + OnlineDeviceCountInFilter
					+ ", Online Device Count in Filter and Grid are not Matching";
			ALib.AssertFailMethod(FailMessage);
		} else {

			ALib.AssertEqualsMethodInt(Expected, OnlineDeviceCountInFilter, pass, fail);
		}

	}
	
	@FindBy(xpath = "//span[text()='Filter Updated Successfully.']")
	private WebElement FilterUpdateMessage;

	@FindBy(xpath = "//span[text()='Filter deleted Successfully.']")
	private WebElement FilterDeleteMessage;

	public void UpdatedFilterName(String FilterName) throws InterruptedException {
		NameFilter.clear();
		NameFilter.sendKeys(FilterName);
		Ok.click();
		sleep(4);
	}

	public void MessageOnFilterUpdate(String FilterName) throws InterruptedException {
		try {
			Initialization.driver.findElement(By.xpath("//span[contains(text(),'Filter ["+FilterName+"] updated successfully.')]")).isDisplayed();
			System.out.println("PASS >> Filter Updated Message displayed successfully");
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Filter Updated Message not displayed");
		}
	}

	public void ClickOnMinusButton() throws InterruptedException {
		MinusButton.click();
		sleep(4);
	}

	public void OffineDeviceCountMatchFilterandGrid() {
		List<WebElement> OfflineDevices = Initialization.driver.findElements(By.xpath("//span[text()='Offline']"));
		int OfflineDeviceCountInFilter = OfflineDevices.size() - 1;
		int Expected = OfflineDeviceCountInGrid;
		String pass = "PASS >> Offline Devices in Filter: " + OfflineDeviceCountInFilter
				+ ", Offline Device Count in Filter and Grid Matching";
		String fail = "FAIL >> Offline Devices in Device Grid and Filters are not matching";

		if (OfflineDeviceCountInFilter != Expected) {
			String FailMessage = "FAIL >> Offline Devices in Filter: " + OfflineDeviceCountInFilter
					+ ", Offline Device Count in Filter and Grid are not Matching";
			ALib.AssertFailMethod(FailMessage);
		} else {

			ALib.AssertEqualsMethodInt(Expected, OfflineDeviceCountInFilter, pass, fail);
		}

	}

	@FindBy(xpath = "//*[@id='deleteGroupConf']/div/div/div[1]/p")
	private WebElement WarningMessage;

	public void WarningMessageOnDeleteFilter(String FilterName) throws InterruptedException {
		String ActualWarningMessage = WarningMessage.getText();
		String ExpectedWarningMesage = "Are you sure you want to delete the filter [" + FilterName + "]?";
		String pass = "PASS >> Warning message on deleting Filter is displayed " + ActualWarningMessage + " ";
		String fail = "FAIL >>Warning message on deleting Filter is wrong " + ActualWarningMessage + " ";
		ALib.AssertEqualsMethod(ExpectedWarningMesage, ActualWarningMessage, pass, fail);
		sleep(5);
	}

	public void ClickOnDeleteFilter() throws InterruptedException {
		DeleteFilter.click();
		sleep(3);
	}

	public void MessageOnFilterDelete(String Status) throws InterruptedException {
		try {
			waitForXpathPresent("//span[text()='The filter was successfully deleted.']");
			Reporter.log("PASS >> Filter Delete Message displayed successfully", true);
		} catch (Exception e) {
			Initialization.driver
					.findElement(By.xpath("//p[contains(text(),'" + Status + " Devices deleted successfully by')]"))
					.isDisplayed();
			Reporter.log("PASS >> Filter Delete Message displayed successfully", true);
		}
	}

	@FindBy(xpath = "//span[text()='Filter query cannot be empty.']")
	private WebElement WarningMessageEmptyFilter;

	public void WarningMessageOnSavingEmptyFilter() {
		boolean a = WarningMessageEmptyFilter.isDisplayed();
		String pass = "PASS >> Warning message on saving empty Filter is displayed";
		String fail = "FAIL >>Warning message on saving empty Filter is NOT displayed";
		ALib.AssertTrueMethod(a, pass, fail);
		;
	}

	@FindBy(xpath = "//*[@id='deviceFilterConfirmationDialog']/div/div/div[1]/p")
	private WebElement WarningMessageDisableColumnsAccessingFilter;

	public void WarningMessageOnAccessingFilterWhenColumnsDisabled() {
		String ActualMessage = WarningMessageDisableColumnsAccessingFilter.getText();
		String ExpectedMessage = "This filter requires few columns that are not visible in your Device list. Would you like to enable those columns?";
		String pass = "PASS >> Warning message on accessing filter when column is disabled";
		String fail = "FAIL >>Warning message on accessing filter when column is NOT disabled";
		ALib.AssertEqualsMethod(ExpectedMessage, ActualMessage, pass, fail);
	}

	public void CheckDeviceInFilterBeforeDelete() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='dataGrid']/tbody/tr/td[2]"));
		deviceName = ls.get(0).getText();
		System.out.println("Device Name checking before deleting the filter:" + deviceName);
	}

	@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")

	private WebElement SearchDeviceTextField;

	public void DeviceSearchAfterDeleteFilter() throws InterruptedException {
		SearchDeviceTextField.clear();
		SearchDeviceTextField.sendKeys(deviceName);
		sleep(15);
	}

	@FindBy(xpath = "//*[@id='dataGrid']/tbody/tr/td[2]")
	private WebElement SearchedDevice;

	public void IfDevicePresentAfterFilterDelete() {
		String ActualDeviceName = SearchedDevice.getText();
		String ExpectedDeviceName = deviceName;
		String pass = "PASS >> Device is found after deleting the filter";
		String fail = "FAIL Device is not found after deleting the filter";
		ALib.AssertEqualsMethod(ExpectedDeviceName, ActualDeviceName, pass, fail);
	}

	@FindBy(xpath = "(//span[@class='page-list']//span[@class='page-size'])[1]")
	private WebElement DevicesPerPageButton;

	public void ClickOnDevicesPerPagebutton() {
		DevicesPerPageButton.click();

	}

	@FindBy(xpath = "//a[text()='500']")
	private WebElement Select500;

	public void chooseDevicesPerPage500() throws InterruptedException {
		Select500.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		System.out
				.println(">> Total number of devices being shown when 500 per page is selected:" + totalDevices + " ");
		waitForidPresent("deleteDeviceBtn");
		sleep(6);
	}

	@FindBy(xpath = "(//button[contains(@aria-expanded,'true')]/following-sibling::ul/li)[last()]")
	private WebElement MaxiMumNoOFDevices;

	public void chooseMaxDevicesPerPage() throws InterruptedException {
		MaxiMumNoOFDevices.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		System.out
				.println(">> Total number of devices being shown when 100 per page is selected:" + totalDevices + " ");
		waitForidPresent("deleteDeviceBtn");
		sleep(6);
	}

	@FindBy(xpath = "//a[text()='20']")
	private WebElement Select20;

	public void chooseDevicesPerPage20() throws InterruptedException {
		Select20.click();
		sleep(7);
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		int expectedDeviceCount = 20;
		String PassStatement2 = "PASS >> Total device count for page when 20 per is selected is: " + expectedDeviceCount
				+ "  ";
		String FailStatement2 = "FAIL >> Total device count for page when 20 per is selected is Wrong";
		ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
		waitForidPresent("deleteDeviceBtn");
		sleep(6);

	}
	
	// Madhu


		public void deleteSuccessMsg() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("//span[text()='The filter was successfully deleted.']"));
			Reporter.log("PASS >> Filter Delete Message displayed successfully", true);
			sleep(3);
		}
		
		public void deleteExistingFilter(String colunmName) {
			try {
				ClickOnFilter();
				Initialization.driver.findElement(By.xpath("//li[text()='"+colunmName+"']")).isDisplayed();
				Reporter.log("Selected filter Is Present In Grid",true);
				Initialization.driver.findElement(By.xpath("//li[text()='"+colunmName+"']")).click();
				ClickOnMinusButton();
				WarningMessageOnDeleteFilter(colunmName);
				ClickOnDeleteFilter();
				deleteSuccessMsg();
				sleep(3);
			}
			catch(Exception e) {
				Reporter.log("Filter is not Present In Grid",true);
			}
			
		}
		
		public void verifyCustomColumnFilter(String customColumn) {
			List<WebElement> customColValue = Initialization.driver
					.findElements(By.xpath("//*[@id='dataGrid']/tbody/tr/td[@class='"+customColumn+"']"));
			for (WebElement customColVal : customColValue) {
				String CustomFilterValue = customColVal.getText();
				if (CustomFilterValue.equals("N/A")) {
					Reporter.log("PASS >> Custom column filter values are displaying N/A", true);
				} else {
					ALib.AssertFailMethod("FAIL >> Custom column filter values are not displaying N/A");
				}
			}

		}

		
		public void verifySureLockVerColumnFilter() {
			List<WebElement> sureLockVerValue = Initialization.driver
					.findElements(By.xpath("//*[@id='dataGrid']/tbody/tr/td[@class='SureLockVersion']"));
			for (WebElement sureLockVersion : sureLockVerValue) {
				String sureLockVerFilter = sureLockVersion.getText();
				if (sureLockVerFilter.equals("Not Installed")) {
					Reporter.log("PASS >> SureLock version filter values are displaying 'Not Installed'.", true);
				} else {
					ALib.AssertFailMethod("FAIL >> SureLock version filter values are not displaying 'Not Installed'.");
				}
			}

		}

}
