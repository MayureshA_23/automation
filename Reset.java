package DynamicJobs_TestScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class Reset extends Initialization{

	@Test(priority='1',description="1.To Verify Dynamic Reset Job Popup UI")
	public void DynamicResetJob_TC1() throws InterruptedException, Throwable{
		Reporter.log("\n1.To Verify Dynamic Reset Job Popup UI",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnResetDevice();
	    dynamicjobspage.VerifyOfReset();
	    dynamicjobspage.ClickOnResetClose();
	    dynamicjobspage.VerifyNoConfirmationMessageResetDevice();
//	    dynamicjobspage.ConfirmationMessageResetDeviceVerify_False();
		Reporter.log("PASS>> Verification of Dynamic Reset Job Popup UI",true);
	}
	
	@Test(priority='2',description="2.To Verify Dynamic Reset Job Popup UI when clicked on Cancel Button")
	public void DynamicResetJob_TC2() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Dynamic Reset Job Popup UI when clicked on Cancel Button",true);
	
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnResetDevice();
	    dynamicjobspage.VerifyOfReset();
	    dynamicjobspage.EnterResetPassword(Config.Pwd);
	    dynamicjobspage.ClickOnResetCancel();
	    dynamicjobspage.VerifyNoConfirmationMessageResetDevice();

	//    dynamicjobspage.ConfirmationMessageResetDeviceVerify_False();
		Reporter.log("PASS>> Verification of Dynamic Reset Job Popup UI when clicked on Cancel Button",true);
	}
	
	@Test(priority='3',description="3.To Verify Dynamic Reset Job Popup UI when clicked on Ok Button")
	public void DynamicResetJob_TC3() throws InterruptedException, Throwable{
		Reporter.log("\n3.To Verify Dynamic Reset Job Popup UI when clicked on Ok Button",true);
	
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnResetDevice();
	    dynamicjobspage.VerifyOfReset();
	    dynamicjobspage.EnterResetPassword(Config.Pwd);
	    dynamicjobspage.ClickOnResetOk();
	    dynamicjobspage.ConfirmationMessageResetDeviceVerify();
	    dynamicjobspage.VerifyOfResetActivityLogs();
		Reporter.log("PASS>> Verification of Dynamic Reset Job Popup UI when clicked on Ok Button",true);
	}
	
	@AfterMethod
	public void aftermethod(ITestResult result) throws InterruptedException, Throwable
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			Utility.captureScreenshot(driver, result.getName());
			commonmethdpage.refreshpage();
			commonmethdpage.SearchDevice();
		}
	}
}
