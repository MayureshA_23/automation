package SanityTestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceInfoPanel extends Initialization{
	@Test(priority='0',description="1.Verify Device Details for Android device in device info panel.")
	public void DeviceDetailsOf_AndroidDevice() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("=====1.Verify Device Details for Android device in device info panel.=====",true);
		
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyDeviceModelName_Windows(Config.AndroidDeviceModelName);
		deviceinfopanelpage.VerifyOperatingSystem_Windows(Config.Android_OS);
		deviceinfopanelpage.VerifyDeviceNameWindows(Config.DeviceName);
		deviceinfopanelpage.VerifyAgentVersionForAndroidDevice(Config.Android_AgentVersion);
		//deviceinfopanelpage.VerifyLastDeviceTimeWindows(Config.Android_LastDeviceTime);
		deviceinfopanelpage.VerifyAddNotesForWindows(Config.Android_AddNotes);
		deviceinfopanelpage.VerifyLocTrackingForWindows(Config.Android_LocTracking);
		deviceinfopanelpage.VerifyTelecomManagementForWindows(Config.Android_Telecom);
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelWindows(Config.Android_CustomCol);
	}
	@Test(priority='1',description="2.Verify Device Details for Windows device in device info panel.")
	public void DeviceDetailsOf_WindowsDevice() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("=====2.Verify Device Details for Windows device in device info panel.=====",true);
	
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchWindowsDeviceName();
		deviceinfopanelpage.VerifyDeviceModelName_Windows(Config.WindowsDeviceModelName);
		deviceinfopanelpage.VerifyOperatingSystem_Windows(Config.Windows_OS);
		deviceinfopanelpage.VerifyDeviceNameWindows(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyAgentVersionWindows(Config.Windows_AgentVersion);
		//deviceinfopanelpage.VerifyLastDeviceTimeWindows(Config.Windows_LastDeviceTime);
		deviceinfopanelpage.VerifyAddNotesForWindows(Config.Windows_AddNotes);
		deviceinfopanelpage.VerifyLocTrackingForWindows(Config.Windows_LocTracking);
		deviceinfopanelpage.VerifyTelecomManagementForWindows(Config.Windows_Telecom);
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelWindows(Config.Windows_CustomCol);
	}
	@Test(priority='2',description="3.Verify Device Details for iOS device in device info panel.")
	public void DeviceDetailsOf_iOSDevice() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("=====3.Verify Device Details for iOS device in device info panel.=====",true);
	
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchIOSDevice();
		deviceinfopanelpage.VerifyDeviceModelName_Windows(Config.IOS_DeviceModel);
		deviceinfopanelpage.VerifyOperatingSystem_MAC(Config.iOS_OS);
		deviceinfopanelpage.VerifyDeviceNameWindows(Config.iOS_DeviceName);
		//deviceinfopanelpage.VerifyLastDeviceTimeIOS(Config.iOS_LastDeviceTime);
		deviceinfopanelpage.VerifyAddNotesForIOS(Config.iOS_AddNotes);
		deviceinfopanelpage.VerifyLocTrackingForIOS(Config.iOS_LocTracking);         
		deviceinfopanelpage.VerifyTelecomManagementForIOS(Config.iOS_Telecom);
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelWindows(Config.iOS_CustomCol);
	}
}
