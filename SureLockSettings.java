package JobsOnAndroid;

import org.testng.annotations.Test;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import Common.Initialization;
import Library.Config;
///SureLock
//	dynamicjobspage.RemoveFolder("NewFolder"); method added in pom
//Precondition in this method VerifyScreenSaverIamge_Url() is an image should be present in the device and path of same image should be passed inside the given method VerifyScreenSaverIamge_Url()


public class SureLockSettings extends Initialization {
	@Test(priority=1,description="SureLock Settings job - Verify creating SureLock settings job.")
	public void VerifyModifydeployInstall() throws InterruptedException, IOException{

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.verifySurelockPrompt();
		Reporter.log("Pass >>SureLock Settings job - Verify creating SureLock settings job.",true); }
	
	@Test(priority=2,description="SureLock Settings job - Verify creating SureLock settings job by configuring single Application mode.")
	public void sureLockSettingSingleApplications() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
//		androidJOB.LaunchSureLock();
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob(); 
//		androidJOB.clickOnAndroidOS();
//		androidJOB.clickOnSureLockSettings();

		androidJOB.clickOnAllowedApplications();
		androidJOB.clickOnAddAppsButton();
		androidJOB.ClickOnSearch();
		androidJOB.EnterApplicationName("Chrome");
		androidJOB.clickOnDoneButtonAddApplication();
		androidJOB.clickOnDoneAllowedApplication();
		androidJOB.aboutSureLock();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("SureLockSettingSingleApplication");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureLockSettingSingleApplication");
		androidJOB.CheckStatusOfappliedInstalledJob("SureLockSettingSingleApplication",360);
		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		androidJOB.verifyAppSureLockSetting("Chrome");
		Reporter.log("Pass >>SureLock Settings job - Verify creating SureLock settings job by configuring single Application mode.",true); }

	@Test(priority=3,description="SureLock Settings job - Verify creating SureLock settings job by adding multiple Applications."+"SureLock Settings Job - Verify modifying SureLock Settings job")
	public void sureLockSettingMultipleApplications() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("SureLockSettingSingleApplication"); 
		androidJOB.ClickOnOnJob("SureLockSettingSingleApplication");
		androidJOB.clickOnModifyOption(); 
		androidJOB.clickOnAllowedApplications();
		androidJOB.clickOnAddAppsButton();
		androidJOB.ClickOnSearch();
		androidJOB.EnterApplicationName("Drive");
		androidJOB.clickOnDoneButtonAddApplication();
		androidJOB.clickOnDoneAllowedApplication();
		androidJOB.clickOnsureLockSetting();
		androidJOB.changeApplicationIconSize();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("SureLockModifiedMultiApplicationStng");
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureLockModifiedMultiApplicationStng");
		androidJOB.CheckStatusOfappliedInstalledJob("SureLockModifiedMultiApplicationStng",360);
		androidJOB.verifyMultiAppSureLockSetting("Chrome,Drive");
		Reporter.log("Pass >>SureLock Settings job - Verify creating SureLock settings job by adding multiple Applications."+"SureLock Settings Job - Verify modifying SureLock Settings job",true); }

	@Test(priority=4,description="SureLock Settings job - Verify SureLock Settings job with Save AS File.")
	public void sureLockSettingSaveas() throws InterruptedException, IOException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnSaveAsFile();
		androidJOB.isSureLockFileDownloaded(Config.FileDownloaded,"surelock");
		Reporter.log("SureLock Settings job - Verify SureLock Settings job with Save AS File.",true); }
	
	@Test(priority=5,description="SureLock Settings Job - Verify SureLock settings job with Edit XML Option.")
	public void sureLockEditXML() throws Throwable{
		androidJOB.clickOnEditXML();
		androidJOB.ClearEditXMLTextArea();
		androidJOB.EnterEditXMLTextArea("./Uploads/UplaodFiles/SureLock Settings/\\EditXMLOptionSureLock.txt");
		androidJOB.ClickOnEditXMLSaveBtn();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("SureLockEditXML");
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureLockEditXML");
		androidJOB.CheckStatusOfappliedInstalledJob("SureLockEditXML",360);
		androidJOB.SureLockAdminDeviceSide();
		androidJOB.clickOnSureLockSetting();
		androidJOB.verifyDisableUseSysWallpaper();
		Reporter.log("SureLock Settings Job - Verify SureLock settings job with Edit XML Option.",true); }
	
	@Test(priority=6,description="SureLock Settings Job - Verify SureLock Settings job with Advanced option.")
	public void sureLockAdvanceOption() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnAdvanceOption();
		androidJOB.enableAdvanceOption();
		androidJOB.EnterActivationode();
		androidJOB.ClickOnSaveEnableAdvncSureLckFox();
		androidJOB.clickOnsureLockSetting();
		androidJOB.enablebackGroundColor();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("SureLockSettingAdvanceOption");
		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureLockSettingAdvanceOption");
		androidJOB.CheckStatusOfappliedInstalledJob("SureLockSettingAdvanceOption",360);
		androidJOB.SureLockAdminDeviceSide();
		androidJOB.clickOnSureLockSetting();
		androidJOB.verifyEnableBackGroundColour();
		Reporter.log("SureLock Settings Job - Verify SureLock settings job with Edit XML Option.",true); }
	
	@Test(priority=7,description="SureLock Settings Job - Verify UI of static SureLock Job")
	public void SureLockStaticUI() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.ClickOnAboutSL();
		androidJOB.VerifyUIofSL_NoteAboutSL();
		androidJOB.ClickONImportExportSetting();
		androidJOB.VerifyUIofSL_ImportExportSL();
		androidJOB.clickOnAdvanceOption();
		androidJOB.VerifyUIOfNotesAdvancedOptnSL();
		androidJOB.CloseSLStaticJob();

		commonmethdpage.refesh();
		
		
		
	}

//*****************************************SureLock Testcases************************************
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&77
	

	
	@Test(priority = 'B', description = "11.Verify 'Add Application' from 'Allowed Application' settings.")
	public void AllowedApplicationSureLockSettings() throws InterruptedException, Throwable {
	
		Reporter.log("\n11.Verify 'Add Application' from 'Allowed Application' settings.", true);
		
	androidJOB.clickOnJobs();
	dynamicjobspage.SearchJobInSearchField("SureLockSettingAdvanceOption"); 
	androidJOB.ClickOnOnJob("SureLockSettingAdvanceOption");
	androidJOB.clickOnModifyOption(); 
	androidJOB.clickOnsureLockSetting();
	androidJOB.DisablebackGroundColor();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("DisableBackGroudColor");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("DisableBackGroudColor");
	androidJOB.CheckStatusOfappliedInstalledJob("DisableBackGroudColor",360);


		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnAllowedApplications();
		androidJOB.clickOnAddAppsButton();
		androidJOB.ClickOnSearch();
		androidJOB.EnterApplicationName("Chrome");
		androidJOB.EnterApplicationName("SureMDM Nix");
		androidJOB.clickOnDoneButtonAddApplication();
		androidJOB.clickOnDoneAllowedApplication();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("Allowed Application");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Allowed Application");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("Allowed Application",360);
       commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		androidJOB.verifyAppSureLockSetting("Chrome");
		androidJOB.verifyAppSureLockSetting("SureMDM Nix");
		Reporter.log("Pass >>SureLock Settings job - Verify 'Add Application' from 'Allowed Application' settings.",true); 



	}



@Test(priority = 'C', description = "12.Verify that 'Remove Application' should remove the selected Application from SureLock Home screen.")
public void RemoveApplicationInAllowedApplicationSureLockSettings() throws InterruptedException, Throwable {

	Reporter.log("\n12.Verify that 'Remove Application' should remove the selected Application from SureLock Home screen.", true);
	androidJOB.clickOnJobs();
	dynamicjobspage.SearchJobInSearchField("Allowed Application"); 
	androidJOB.ClickOnOnJob("Allowed Application");
	androidJOB.clickOnModifyOption(); 
	dynamicjobspage.RemoveApplication("Chrome");
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("Edited Allowed Application");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("Edited Allowed Application");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("Edited Allowed Application",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Chrome");
	Reporter.log("Pass >>SureLock Settings job - Verify that 'Remove Application' should remove the selected Application from SureLock Home screen.",true); 


}
/*@Test(priority = 'D', description = "13.Verify creating folder from 'Add Folder' option from 'Allowed Applications' settings.")
public void CreatingFolderAllowedApplicationSureLockSettings() throws InterruptedException, Throwable {
	Reporter.log("\n13.Verify creating folder from 'Add Folder' option from 'Allowed Applications' settings.", true);
	androidJOB.clickOnJobs();
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.clickOnAllowedApplications();
	androidJOB.ClickOnAddFolder();
	androidJOB.enterFolderName("NewFolder");
	androidJOB.tapOkBtnOnFolder();
	androidJOB.ClickOnCreatedFolderAllowedApplicationSureLock();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("Chrome");
	androidJOB.clickOnDoneButtonAddApplication();
   androidJOB.clickOnDoneAllowedApplication();
   androidJOB.ClickOnDoneBtn();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("CreatedFolder Allowed Application");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("CreatedFolder Allowed Application");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("CreatedFolder Allowed Application",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.verifyFolderInSureLockSettingHomeScreen("NewFolder");
	androidJOB.verifyAppSureLockSetting("Chrome");
	androidJOB.goToSLAdminSettings();
	androidJOB.tapOnAllowedApplications();        
	androidJOB.verifyAndTapAllowedAppinstructionsText();
  androidJOB.verifyFolderInSureLockSettingHomeScreen("NewFolder");
	androidJOB.verifyAppSureLockSetting("Chrome");
	androidJOB.tapOnDoneBtn();




}
	@Test(priority = 'E', description = "14.Verify Removing Folder from Edit Folder settings.")
	public void RemovingFolderAllowedApplicationSureLockSettings() throws InterruptedException, Throwable {
		Reporter.log("\n14.Verify Removing Folder from Edit Folder settings.", true);
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("CreatedFolder Allowed Application"); 
		androidJOB.ClickOnOnJob("CreatedFolder Allowed Application");                   //Changed xapth index
		androidJOB.clickOnModifyOption(); 
		dynamicjobspage.RemoveFolder("NewFolder");
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("Removed CreatedFolder Allowed Application");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Removed CreatedFolder Allowed Application");          //In com.42 Project replace common with androidjob pom
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("Removed CreatedFolder Allowed Application",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("NewFolder");
		androidJOB.goToSLAdminSettings();
		androidJOB.tapOnAllowedApplications();        
		androidJOB.verifyAndTapAllowedAppinstructionsText();
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("NewFolder");


}
*/	
	@Test(priority = 'F', description = "15.Verify Wallpaper Settings and choose Use System Wallpaper.")
	public void SureLockAdminSettings() throws InterruptedException, Throwable {
		Reporter.log("\n15.Verify Wallpaper Settings and choose Use System Wallpaper.", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.VerifyWallpaper();
		androidJOB.enableUseSystemWallpaper();
		androidJOB.VerifyWhenUseSysWallpaperIsDisabled();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("EnableUseSystemWallpaper SureLockAdminSettings");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableUseSystemWallpaper SureLockAdminSettings");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("EnableUseSystemWallpaper SureLockAdminSettings",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.verifyEnableUseSysWallpaper("Enable SysWallpaper",0);



}
/*	@Test(priority = 'G', description = "16.Verify Row and Column Size and in the prompt enter portrait and landscape values")
	public void VerifyDisplaySettingsRowColumnSize() throws InterruptedException, Throwable {
		Reporter.log("\n16.Verify Row and Column Size and in the prompt enter portrait and landscape values", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Row and Column Size");
        androidJOB.VerifyInRowColumnSizeWindow();
		androidJOB.Inputportrait_rowAndportrait_column("2","2");
		androidJOB.Inputlandscap_rowAndlandscap_column("4","3");
		androidJOB.ClickOnOkBtn();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("RowAndColSize DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("RowAndColSize DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("RowAndColSize DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideClickOnRowAndCol();             //Change Values inside if needed
		
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("RowAndColSize DisplaySettings SL"); 
		androidJOB.ClickOnOnJob("RowAndColSize DisplaySettings SL");                   //Changed xapth index
		androidJOB.clickOnModifyOption(); 
		androidJOB.clickOnsureLockSetting();
		androidJOB.VerifyInRowColumnSizeWindow();
		androidJOB.DisableRowColSize();
		androidJOB.ClickOnOkBtn();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("DisableRowAndColSize");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableRowAndColSize");          //In com.42 Project replace common with androidjob pom
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("DisableRowAndColSize",360);

	
	
	
}	
*/	
	@Test(priority = 'H', description = "17.Verify Icon Size and choose Small option")
	public void VerifyDisplaySettingsIconSize_SmallOption() throws InterruptedException, Throwable {
		Reporter.log("\n17.Verify Icon Size and choose Small option", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnIconSize();
		androidJOB.Select_small_size_icon();
		androidJOB.ClickOnDoneBtnIconSize();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("IconSizeSMALL DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("IconSizeSMALL DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("IconSizeSMALL DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Icon Size");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Icon Size");
	    androidJOB.VerifyDeviceSideWifiSL("Small (50 %)",0);


	

	
}
	@Test(priority = 'I', description = "18.Verify Icon Size and choose Medium option")
	public void VerifyDisplaySettingsIconSize_MediumOption() throws InterruptedException, Throwable {
		Reporter.log("\n18.Verify Icon Size and choose Medium option", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnIconSize();
		androidJOB.Select_medium_size_icon();
		androidJOB.ClickOnDoneBtnIconSize();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("IconSizeMEDIUM DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("IconSizeMEDIUM DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("IconSizeMEDIUM DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Icon Size");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Icon Size");

	    androidJOB.VerifyDeviceSideWifiSL("Medium (100 %)",1);

	

	
}
	@Test(priority = 'J', description = "19.Verify Icon Size and choose Large option")
	public void VerifyDisplaySettingsIconSize_LargeOption() throws InterruptedException, Throwable {
		Reporter.log("\n19.Verify Icon Size and choose Large option", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnIconSize();
		androidJOB.Select_large_size_icon();
		androidJOB.ClickOnDoneBtnIconSize();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("IconSizeLARGE DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("IconSizeLARGE DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("IconSizeLARGE DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Icon Size");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Icon Size");

	    androidJOB.VerifyDeviceSideWifiSL("Large (200 %)",2);

	

	
}
	@Test(priority = 'K', description = "20.Verify Text Color")
	public void VerifyDisplaySettingsIconSize_TextColor() throws InterruptedException, Throwable {
		Reporter.log("\n20.Verify Text Color", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Text Color");
		androidJOB.SelectTextColor();
		androidJOB.SelectTextColor_BLUE();
		androidJOB.ClickOnDoneBtnTextColor();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("TextColor DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("TextColor DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("TextColor DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Text Color");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Text Color");
		androidJOB.DeviceSideClickOnTextColor("Blue",1);  //blue

	
	}
	@Test(priority = 'L', description = "21.Verify Text Size choosing large option")
	public void VerifyDisplaySettingsIconSize_TextSize() throws InterruptedException, Throwable {
		Reporter.log("\n21.Verify Text Size choosing large option", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnFontSize();
		androidJOB.SelectLargeFontSize();
		androidJOB.ClickOnfont_size_popupDoneBtn();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("FonSizeLARGE DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("FonSizeLARGE DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("FonSizeLARGE DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Font Size");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Font Size");
		androidJOB.DeviceSideClickOnFontSize("Large (100 %)");


	
	}
	@Test(priority = 'M', description = "22.Verify Allow Icon Relocation")
	public void VerifyDisplaySettingsIconSize_IconRelocation() throws InterruptedException, Throwable {
		Reporter.log("\n22.Verify Allow Icon Relocation", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Allow Icon Relocation");
		androidJOB.Enable_icon_relocation_settings();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("Enable Allow Icon Relocation DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Enable Allow Icon Relocation DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("Enable Allow Icon Relocation DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Allow Icon Relocation");
		androidJOB.ClickOnAllowIconRelocationAfterSearchingOnDeviceSide();
		androidJOB.verifyEnableUseSysWallpaper("Allow Icon Relocation",0);
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("Enable Allow Icon Relocation DisplaySettings SL"); 
		androidJOB.ClickOnOnJob("Enable Allow Icon Relocation DisplaySettings SL");                   //Changed xapth index
		androidJOB.clickOnModifyOption(); 
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Allow Icon Relocation");
		androidJOB.Enable_icon_relocation_settings();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("Disable Allow Icon Relocation DisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Disable Allow Icon Relocation DisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("Disable Allow Icon Relocation DisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Allow Icon Relocation");
		androidJOB.OneTimeClickAfterSearchSLsettings("Allow Icon Relocation");
//		androidJOB.ClickOnAllowIconRelocationAfterSearchingOnDeviceSide();
      androidJOB.VerifyDeviceSideDisable_icon_relocation_settings("Allow Icon Relocation",0);

		
	}
	
	@Test(priority = 'N', description = "23.Verify Enable Hide App Title")
	public void VerifyDisplaySettingsEnable_Hide_App_Title() throws InterruptedException, Throwable {
		Reporter.log("\n23.Verify Enable Hide App Title", true);
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnAllowedApplications();
		androidJOB.clickOnAddAppsButton();
		androidJOB.ClickOnSearch();
		androidJOB.EnterApplicationName("Chrome");
		androidJOB.EnterApplicationName("com.gears42.surelock");
		androidJOB.clickOnDoneButtonAddApplication();
		androidJOB.clickOnDoneAllowedApplication();
      androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Hide Application Title");
		androidJOB.Enablehide_apptitle_settings_CheckBox();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("EnableHideApplicationTitleDisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableHideApplicationTitleDisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("EnableHideApplicationTitleDisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Chrome");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Hide App Title");
		androidJOB.OneTimeClickAfterSearchSLsettings("Hide App Title");
		androidJOB.verifyEnableUseSysWallpaper("Hide App Title",0);



}
	@Test(priority = 'O', description = "24.Verify Disable Hide App Title")
	public void VerifyDisplaySettingsDisable_Hide_App_Title() throws InterruptedException, Throwable {
		Reporter.log("\n24.Verify Disable Hide App Title", true);
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("EnableHideApplicationTitleDisplaySettings SL"); 
		androidJOB.ClickOnOnJob("EnableHideApplicationTitleDisplaySettings SL");                   //Changed xapth index
		androidJOB.clickOnModifyOption(); 
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Hide Application Title");
		androidJOB.Enablehide_apptitle_settings_CheckBox();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("DisableHideApplicationTitleDisplaySettings SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableHideApplicationTitleDisplaySettings SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("DisableHideApplicationTitleDisplaySettings SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
		androidJOB.verifyAppSureLockSetting("Chrome");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Hide App Title");
		androidJOB.OneTimeClickAfterSearchSLsettings("Hide App Title");
      androidJOB.VerifyDeviceSideDisable_icon_relocation_settings("Hide App Title",0);




}	
	@Test(priority = 'P', description = "25.Verify 'Single App Mode' and keep only one application in Allowed Application")
	public void VerifySingleApplicationMode_Enable() throws InterruptedException, Throwable {
		Reporter.log("\n25.Verify 'Single App Mode' and keep only one application in Allowed Application", true);
		
		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
		androidJOB.clickOnAllowedApplications();
		androidJOB.clickOnAddAppsButton();
		androidJOB.ClickOnSearch();
		androidJOB.EnterApplicationName("Chrome");
		androidJOB.clickOnDoneButtonAddApplication();
		androidJOB.clickOnDoneAllowedApplication();
      androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Single Application Mode");
		androidJOB.ClickOnSingleApplicationMode();
		androidJOB.Enablesingle_application_mode();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("EnableSingleApplicationMode SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableSingleApplicationMode SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("EnableSingleApplicationMode SL",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.VerifyOnDeviceSidesingle_application_modeEnable();
		
		
	}	
	@Test(priority = 'Q', description = "26.Verify that 'Single Application Mode' should get disabled when Multiple Applications are added on home screen.")
	public void VerifySingleApplicationMode_MultileApp() throws InterruptedException, Throwable {
		Reporter.log("\n26.Verify that 'Single Application Mode' should get disabled when Multiple Applications are added on home screen.", true);
		
		androidJOB.rebootDevice();
		androidJOB.LaunchSureLock();
        androidJOB.clickOnJobs();
        dynamicjobspage.SearchJobInSearchField("EnableSingleApplicationMode SL"); 
		androidJOB.ClickOnOnJob("EnableSingleApplicationMode SL");
		androidJOB.clickOnModifyOption(); 
		androidJOB.clickOnAllowedApplications();
		androidJOB.clickOnAddAppsButton();
		androidJOB.ClickOnSearch();
//		androidJOB.ClickOnNixApp("Nix");
		androidJOB.EnterApplicationName("SureMDM Nix");
		androidJOB.clickOnDoneButtonAddApplication();
		androidJOB.clickOnDoneAllowedApplication();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("SingleModeMultipleApp");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SingleModeMultipleApp");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("SingleModeMultipleApp",360);
       commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Single Application Mode");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Single Application Mode");
       androidJOB.VerifyDeviceSideDisable_icon_relocation_settings("Single Application Mode",0);


}	
		
	@Test(priority = 'R', description = "27.Verify Change Password")
	public void VerifyChangePassword() throws InterruptedException, Throwable {
	Reporter.log("\n27.Verify Change Password", true);
	
		
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Change Password");
	androidJOB.password_warning_popup();
	androidJOB.EnterNewSLpwd("00000");
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("ChangePassword SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("ChangePassword SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("ChangePassword SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.VerifyAdminSettingsChangePwd();
  androidJOB.goToSLAdminSettingsNewPwd();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("Change Password");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Change Password");
	androidJOB.ClickOnWarningPopUpInChangePwdSLsetting("00000","0000");



}	
	@Test(priority = 'S', description = "28.Verify Add Admin User")
	public void VerifyAdminUsers() throws InterruptedException, Throwable {
	Reporter.log("\n28.Verify Add Admin User", true);
	
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Admin Users");
	androidJOB.ClickOnadmin_users_settings();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("AdminUsers SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("AdminUsers SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("AdminUsers SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("Admin Users");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Admin Users");
  androidJOB.VerifyDeviceSideAdminUser();

	}
	@Test(priority = 'T', description = "29.Verify Remove Admin User")
	public void VerifyRemoveAdminUsers() throws InterruptedException, Throwable {
	Reporter.log("\n29.Verify Remove Admin User", true);
	
	
	androidJOB.clickOnJobs();
	dynamicjobspage.SearchJobInSearchField("AdminUsers SL"); 
	androidJOB.ClickOnOnJob("AdminUsers SL");                   //Changed xapth index
	androidJOB.clickOnModifyOption(); 
	androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Admin Users");
	androidJOB.VerifyRemoveAdminUser();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("RemoveAdminUsers SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("RemoveAdminUsers SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("RemoveAdminUsers SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("Admin Users");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Admin Users");
	androidJOB.verifyDeviceSideRemoveAppSureLockSetting("42Gears");

}
	
/*	@Test(priority = 'U', description = "30.Verify enabling 'Wi-Fi Settings' as 'Don't Care'")
	public void VerifyPeripheralSettings_enablingWiFiSettings_as_DontCare() throws InterruptedException, Throwable {
	Reporter.log("\n30.Verify enabling 'Wi-Fi Settings' as 'Don't Care", true);
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
    androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("WiFi Settings");
	androidJOB.ClickOnwifi_mode_setting();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("WifiSettingsDonTCare SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("WifiSettingsDonTCare SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("WifiSettingsDonTCare SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("WiFi Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("WiFi Settings");
	androidJOB.DeviceSideClickOnTextColor("Don't Care",0);
  

	
}
	
	@Test(priority = 'V', description = "31.Verify enabling 'Wi-Fi Settings' as 'Always ON'")
	public void VerifyPeripheralSettings_enablingWiFiSettings_as_AlwaysOn() throws InterruptedException, Throwable {
	Reporter.log("\n31.Verify enabling 'Wi-Fi Settings' as 'Always ON'", true);
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
    androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("WiFi Hotspot Settings");
	androidJOB.ClickOnwifi_always_on();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("WifiSettingsalways_on SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("WifiSettingsalways_on SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("WifiSettingsalways_on SL",360);
    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
    androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("WiFi Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("WiFi Settings");
	androidJOB.VerifyDeviceSideWifiSL("Always On",1);
	
	
	}
*/

	@Test(priority = 'W', description = "32.Verify enabling 'GPS Settings' as 'Don't Care'")
	public void VerifyPeripheralSettings_enablingGPSSettings_as_DontCare() throws InterruptedException, Throwable {
	Reporter.log("\n32.Verify enabling 'GPS Settings' as 'Don't Care'", true);

	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("GPS Settings");
	androidJOB.ClickOngps_settings();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("GPSSettingsDonTCare SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("GPSSettingsDonTCare SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("GPSSettingsDonTCare SL",360);
    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
   androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("GPS Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("GPS Settings");
	androidJOB.DeviceSideClickOnTextColor("Don't Care",0);
  

	
	
	}
	@Test(priority = 'X', description = "33.Verify enabling 'GPS Settings' as 'Always ON'")
	public void VerifyPeripheralSettings_enablingGPSSettings_as_AlwaysOn() throws InterruptedException, Throwable {
	Reporter.log("\n33.Verify enabling 'GPS Settings' as 'Always ON'", true);
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("GPS Settings");
	androidJOB.ClickOngps_always_on();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("GPSSettingsAlwaysOn SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("GPSSettingsAlwaysOn SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("GPSSettingsAlwaysOn SL",360);
commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("GPS Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("GPS Settings");
androidJOB.VerifyDeviceSideWifiSL("Always On",1);
	
	
	}
	@Test(priority = 'Y', description = "34.Verify enabling 'GPS Settings' as 'Always OFF'")
	public void VerifyPeripheralSettings_enablingGPSSettings_as_AlwaysOff() throws InterruptedException, Throwable {
	Reporter.log("\n34.Verify enabling 'GPS Settings' as 'Always OFF'", true);
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("GPS Settings");
	androidJOB.ClickOngps_always_off();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("GPSSettingsAlwaysOff SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("GPSSettingsAlwaysOff SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("GPSSettingsAlwaysOff SL",360);
   commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
   androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("GPS Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("GPS Settings");
	androidJOB.VerifyDeviceSideWifiSL("Always Off",2);

	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("GPSSettingsAlwaysOn SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("GPSSettingsAlwaysOn SL",360);
commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
 androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("GPS Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("GPS Settings");
androidJOB.VerifyDeviceSideWifiSL("Always On",1);

	
	}
	
	
	@Test(priority = 'a', description = "35.Verify SureLock home screen Orientation.")
	public void VerifyPeripheralSettings_enablingSureLock_home_screen_OrientationDontCare() throws InterruptedException, Throwable {
	Reporter.log("\n35.Verify SureLock home screen Orientation.", true);
	
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("SureLock Home Screen Orientation");
	androidJOB.ClickOnhome_screenOrientation_setting();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("SureLockHomeScreenOrientationDonTCare SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("SureLockHomeScreenOrientationDonTCare SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("SureLockHomeScreenOrientationDonTCare SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("SureLock Home Screen Orientation");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("SureLock Home Screen Orientation");
	androidJOB.DeviceSideClickOnTextColor("Don't Care",0);

	
	}
	@Test(priority = 'b', description = "36.Verify enabling 'SureLock home screen Orientation' as 'Landscape mode'.")
	public void VerifyPeripheralSettings_enablingSureLock_home_screen_OrientationLandscape_mode() throws InterruptedException, Throwable {
	Reporter.log("\n36.Verify enabling 'SureLock home screen Orientation' as 'Landscape mode'.", true);
	
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
   androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("SureLock Home Screen Orientation");
	androidJOB.ClickOnlandscape_orientation();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("SureLockHomeScreenOrientationLandsacpeMode SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("SureLockHomeScreenOrientationLandsacpeMode SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("SureLockHomeScreenOrientationLandsacpeMode SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("SureLock Home Screen Orientation");
	androidJOB.ClickOnBackButtonInDeviceSide();
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("SureLock Home Screen Orientation");
	androidJOB.VerifyDeviceSideSLHomeScreenOreientation();
	 androidJOB.VerifyDeviceSideWifiSL("Landscape",1);

	
	}
	@Test(priority = 'c', description = "37.Verify enabling 'SureLock home screen Orientation' as 'Portrait mode'.")
	public void VerifyPeripheralSettings_enablingSureLock_home_screen_OrientationPortrait_mode() throws InterruptedException, Throwable {
	Reporter.log("\n37.Verify enabling 'SureLock home screen Orientation' as 'Portrait mode'.", true);
	
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("SureLock Home Screen Orientation");
	androidJOB.ClickOnportrait_orientation();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("SureLockHomeScreenOrientationPortraitMode SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("SureLockHomeScreenOrientationPortraitMode SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("SureLockHomeScreenOrientationPortraitMode SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("SureLock Home Screen Orientation");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("SureLock Home Screen Orientation");
	androidJOB.VerifyDeviceSideSLHomeScreenOreientation();
  androidJOB.VerifyDeviceSideWifiSL("Portrait",2);
	}
	
	
	@Test(priority = 'd', description = "38.Verify enabling 'Rotation Settings' as 'Don't Care'.")
	public void VerifyPeripheralSettings_RotationSettingsDontCare() throws InterruptedException, Throwable {
	Reporter.log("\n38.Verify enabling 'Rotation Settings' as 'Don't Care'.", true);

	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Rotation Settings");
	androidJOB.ClickOnrotation_settingDontCare();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("rotation_dont_care SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("rotation_dont_care SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("rotation_dont_care SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("Rotation Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Rotation Settings");
	androidJOB.DeviceSideClickOnTextColor("Don't Care",0);


	}
	
	
	@Test(priority = 'e', description = "39.Verify enabling 'Rotation Settings' as 'Always ON'.")
	public void VerifyPeripheralSettings_RotationSettingsAlwaysON() throws InterruptedException, Throwable {
	Reporter.log("\n39.Verify enabling 'Rotation Settings' as 'Always ON'.", true);

	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Rotation Settings");
	androidJOB.ClickOnrotation_always_on();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("rotation_always_on SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("rotation_always_on SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("rotation_always_on SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("Rotation Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Rotation Settings");
	androidJOB.VerifyDeviceSideWifiSL("Always On",1);

	}
	
	@Test(priority = 'f', description = "40.Verify enabling 'Rotation Settings' as 'Always OFF'.")
	public void VerifyPeripheralSettings_RotationSettingsAlwaysOFF() throws InterruptedException, Throwable {
	Reporter.log("\n40.Verify enabling 'Rotation Settings' as 'Always ON'.", true);

	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Rotation Settings");
	androidJOB.ClickOnrotation_always_off();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("rotation_always_off SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("rotation_always_off SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("rotation_always_off SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("Rotation Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Rotation Settings");
	androidJOB.VerifyDeviceSideWifiSL("Always Off",2);

	}


	@Test(priority = 'g', description = "41.Verify 'On launch of Unallowed Application' and choose 'Go To Home screen'.")
	public void VerifyPeripheralSettings_GoToHomeScreen() throws InterruptedException, Throwable {
	Reporter.log("\n41.Verify 'On launch of Unallowed Application' and choose 'Go To Home screen'.", true);

	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.clickOnAllowedApplications();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("Play Store");
	androidJOB.clickOnDoneButtonAddApplication();
	androidJOB.clickOnDoneAllowedApplication();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.ClickOngo_to_screen();
	androidJOB.VerifyHomeScreenOptn();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("goto_home_screen SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("goto_home_screen SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("goto_home_screen SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("On Launch of Unallowed Application");
	androidJOB.DeviceSideClickOnTextColor("Home Screen",0);
	androidJOB.ClickOnCancel();
	androidJOB.tapOnDoneBtn2();
	androidJOB.tapOnDoneBtn2();
	androidJOB.tapOnDoneBtn2();
//	androidJOB.LaunchPlayStoreSL();
	androidJOB.PlayStore("com.google.android.gm");
	androidJOB.VerifyDeviceSideClickOnOPEN();
	androidJOB.verifyAppSureLockSetting("Play Store");





	}
	@Test(priority = 'h', description = "42.Verify 'On launch of Unallowed Application' and choose 'Resume Previous Application'.")
	public void VerifyPeripheralSettings_ResumePreviousApplication() throws InterruptedException, Throwable {
	Reporter.log("\n42.Verify 'On launch of Unallowed Application' and choose 'Resume Previous Application'.", true);

	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.clickOnAllowedApplications();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("Play Store");
	androidJOB.clickOnDoneButtonAddApplication();
	androidJOB.clickOnDoneAllowedApplication();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.ClickOngo_to_screen();
	androidJOB.Verifyresume_prev_app();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("goto_home_screen SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("goto_home_screen SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("goto_home_screen SL",360);
  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("On Launch of Unallowed Application");
	androidJOB.VerifyDeviceSideWifiSL("Resume Previous Application",1);
	androidJOB.ClickOnCancel();
	androidJOB.tapOnDoneBtn2();
	androidJOB.tapOnDoneBtn2();
	androidJOB.tapOnDoneBtn2();
//	androidJOB.LaunchPlayStoreSL();
	androidJOB.PlayStore("com.google.android.gm");
	androidJOB.VerifyDeviceSideClickOnOPEN();
	androidJOB.Verifyresume_prev_appDeviceSide();



	}

	@Test(priority = 'i', description = "43.Verify 'On launch of Unallowed Application' and choose 'Relaunch Previous Application'.")
	public void VerifyPeripheralSettings_RelaunchPreviousApplication() throws InterruptedException, Throwable {
	Reporter.log("\n43.Verify 'On launch of Unallowed Application' and choose 'Relaunch Previous Application'.", true);

	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.clickOnAllowedApplications();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("Play Store");
	androidJOB.clickOnDoneButtonAddApplication();
	androidJOB.clickOnDoneAllowedApplication();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.ClickOngo_to_screen();
	androidJOB.Verifyrestart_prev_app();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("goto_home_screen SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("goto_home_screen SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("goto_home_screen SL",360);
 commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
  androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("On Launch of Unallowed Application");
	androidJOB.VerifyDeviceSideWifiSL("Relaunch Previous Application",2);
	androidJOB.ClickOnCancel();
	androidJOB.tapOnDoneBtn2();
	androidJOB.tapOnDoneBtn2();
	androidJOB.tapOnDoneBtn2();
//	androidJOB.LaunchPlayStoreSL();
	androidJOB.PlayStore("com.google.android.gm");
	androidJOB.VerifyDeviceSideClickOnOPEN();
	androidJOB.Verifyrestart_prev_appDeviceSide();
	}
	
	@Test(priority = 'j', description = "44.Verify 'On launch of Unallowed Application' and choose 'Relaunch Previous Application'.")
	public void VerifyPeripheralSettings_EnableHomeScreenOptn() throws InterruptedException, Throwable {
	Reporter.log("\n44.Verify 'On launch of Unallowed Application' and choose 'Relaunch Previous Application'.", true);

	androidJOB.clickOnJobs();
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.clickOnAllowedApplications();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("Play Store");
	androidJOB.clickOnDoneButtonAddApplication();
	androidJOB.clickOnDoneAllowedApplication();
  androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.ClickOngo_to_screen();
	androidJOB.EnableHomeScreen();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("Enable_home_screen SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("Enable_home_screen SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("Enable_home_screen SL",360);
    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
    androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("On Launch of Unallowed Application");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("On Launch of Unallowed Application");
	androidJOB.DeviceSideClickOnTextColor("Home Screen",0);




	}

/*	@Test(priority = 'k', description = "46.Verify 'Add Shortcut' option from Manage Shortcut Settings.")
	public void VerifyManageShortcuts_AddShortcut() throws InterruptedException, Throwable {
	Reporter.log("\n46.Verify 'Add Shortcut' option from Manage Shortcut Settings.", true);
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.ClickOnManageShortcuts();
	androidJOB.ClickOnAddShortCut("Android Bluetooth Settings","android.settings.BLUETOOTH_SETTINGS");
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("ManageShortcuts_AddShortcut SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	commonmethdpage.SearchField("ManageShortcuts_AddShortcut SL");
//	androidJOB.ClickOnApplyJobOkButton();
	commonmethdpage.CheckStatusOfappliedInstalledJob("ManageShortcuts_AddShortcut SL",360);
	}


*/	
  

	@Test(priority = 'k', description = "45.Verify Enabling 'Enable Multi-User Profile'.+ 'Verify Activate feature by tapping on Menu button for Profiles'.+'Verify Activate Profile and add few applications and configure settings for that Profile'.+'Verify Adding Multiple Profiles in the Profile Management.'+'Verify 'ADD' User in User Management.'+'Verify 'SELECT ALL' and 'UNSELECT ALL' in the User Management.'")
	public void VerifyEnableMultiUserProfile() throws InterruptedException, Throwable {
	Reporter.log("\n45.Verify Enabling 'Enable Multi-User Profile'.", true);
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.ClickOnMultiUserProfileSett();  //UserA(Default)
	androidJOB.clickOnAllowedApplications();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("Gmail");
	androidJOB.EnterApplicationName("SureMDM Nix");
	androidJOB.EnterApplicationName("Play Store");
	androidJOB.clickOnDoneButtonAddApplication();
	androidJOB.clickOnDoneAllowedApplication();
    androidJOB.clickOnsureLockSetting();
	androidJOB.VerifyWallpaper();
	androidJOB.enableUseSystemWallpaper();
	androidJOB.clickOndoneButtonSureLockSetting();
	
	
	androidJOB.ClickOnmup_profiles_selectProfileA();  //UserB
	androidJOB.clickOnAllowedApplications();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("Chrome");
	androidJOB.EnterApplicationName("Maps");
	androidJOB.EnterApplicationName("Drive");
	androidJOB.clickOnDoneButtonAddApplication();
	androidJOB.clickOnDoneAllowedApplication();
	androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnsurelock_bgColor("#3b1fe0");
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.CREATE_CLONE();                         //UserD

	androidJOB.ClickOnmup_profiles_selectProfileB();  //UserC
	androidJOB.clickOnAllowedApplications();
	androidJOB.clickOnAddAppsButton();
	androidJOB.ClickOnSearch();
	androidJOB.EnterApplicationName("YouTube");
	androidJOB.EnterApplicationName("Google");
  androidJOB.clickOnDoneButtonAddApplication();
	androidJOB.clickOnDoneAllowedApplication();
	androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnsurelock_bgColor("#92e01f");
	androidJOB.clickOndoneButtonSureLockSetting();
	
	androidJOB.CreateUsers();

	androidJOB.ClickOnProfiles();

	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("EnableMultiUserProfile SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("EnableMultiUserProfile SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("EnableMultiUserProfile SL",360);
	
 commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");

    androidJOB.VerifyDeviceDEFAULT_UserA("UserA","0");
	androidJOB.ClickOnBackButtonInDeviceSide();

	androidJOB.verifyAppSureLockSetting("SureMDM Nix");
	androidJOB.verifyAppSureLockSetting("Play Store");
	androidJOB.verifyAppSureLockSetting("Gmail");
    androidJOB.goToSLAdminSettings();
   commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
   androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");
   androidJOB.verifyEnableUseSysWallpaper("Enable Multi-User Profile",0);
	androidJOB.OneTimeClickAfterSearchSLsettings("Profile Management");
	androidJOB.VerifyActiveStatus();
	androidJOB.ClickOnBackButtonInDeviceSide();
	androidJOB.ClickOnBackButtonInDeviceSide();
	commonmethdpage.commonScrollMethod("SureLock Settings");
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.verifyEnableUseSysWallpaper("Enable SysWallpaper",0);
	androidJOB.ClickOnBackButtonInDeviceSide();
	androidJOB.ClickOnBackButtonInDeviceSide();





	}
	@Test(priority = 'l', description = "46.Verify UserB'")
	public void VerifyUserB() throws InterruptedException, Throwable {
	Reporter.log("\n46.Verify UserB", true);
	
	
	   commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");

	
	   androidJOB.VerifyDeviceDEFAULT_UserA("UserB","0");
	   androidJOB.VerifyWaitTillSLscreenISdisplayed("Chrome");
		androidJOB.verifyAppSureLockSetting("Chrome");
		androidJOB.verifyAppSureLockSetting("Maps");
		androidJOB.verifyAppSureLockSetting("Drive");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("SureMDM Nix");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Play Store");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Gmail");


	    androidJOB.goToSLAdminSettings();
	   commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
	   androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");
	   androidJOB.verifyEnableUseSysWallpaper("Enable Multi-User Profile",0);
		androidJOB.OneTimeClickAfterSearchSLsettings("User Management");
		androidJOB.VerifyUsers();
		androidJOB.ClickOnBackButtonInDeviceSide();
		androidJOB.ClickOnBackButtonInDeviceSide();
		commonmethdpage.commonScrollMethod("SureLock Settings");
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.verifyEnableUseSysWallpaper("Check to pick SureLock background color",1);


	
}
	@Test(priority = 'm', description = "47.Verify 'CLONE' feature in the Profile Management.+'Verify Login with the valid credentials for Users.'")
	public void VerifyCLONEfeatureProfileManagment() throws InterruptedException, Throwable {
	Reporter.log("\n47.Verify 'CLONE' feature in the Profile Management.", true);
	
	
	   commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");

	   androidJOB.VerifyDeviceDEFAULT_UserA("UserD","0");
	   androidJOB.VerifyWaitTillSLscreenISdisplayed("Chrome");
		androidJOB.verifyAppSureLockSetting("Chrome");
		androidJOB.verifyAppSureLockSetting("Maps");
		androidJOB.verifyAppSureLockSetting("Drive");
	    androidJOB.goToSLAdminSettings();
	   commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
	   androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");
	   androidJOB.verifyEnableUseSysWallpaper("Enable Multi-User Profile",0);
		androidJOB.OneTimeClickAfterSearchSLsettings("Profile Management");
		androidJOB.VerifyActiveStatus();
		androidJOB.ClickOnBackButtonInDeviceSide();
		androidJOB.ClickOnBackButtonInDeviceSide();
		commonmethdpage.commonScrollMethod("SureLock Settings");
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.verifyEnableUseSysWallpaper("Check to pick SureLock background color",1);
		androidJOB.ClickOnBackButtonInDeviceSide();
		androidJOB.ClickOnBackButtonInDeviceSide();


	
	
	
}
	@Test(priority = 'n', description = "48.Verify 'ADD' User in User Management.And Verify Login with the valid credentials for Users.")
	public void VerifyADDuser() throws InterruptedException, Throwable {
	Reporter.log("\n48.Verify 'ADD' User in User Management.", true);

	
	
	   commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	   androidJOB.VerifyDeviceDEFAULT_UserA("UserC","0");
	   androidJOB.VerifyWaitTillSLscreenISdisplayed("YouTube");
		androidJOB.verifyAppSureLockSetting("Google");
		androidJOB.verifyAppSureLockSetting("YouTube");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("SureMDM Nix");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Play Store");
		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Gmail");


	    androidJOB.goToSLAdminSettings();
	   commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
	   androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");
	   androidJOB.verifyEnableUseSysWallpaper("Enable Multi-User Profile",0);
		androidJOB.OneTimeClickAfterSearchSLsettings("User Management");
		androidJOB.VerifyUsers();
		androidJOB.ClickOnBackButtonInDeviceSide();
		androidJOB.ClickOnBackButtonInDeviceSide();
		commonmethdpage.commonScrollMethod("SureLock Settings");
		androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.verifyEnableUseSysWallpaper("Check to pick SureLock background color",1);
//		androidJOB.ClickOnBackButtonInDeviceSide();
//		androidJOB.ClickOnBackButtonInDeviceSide();


	 

	
	
	
}
	@Test(priority = 'o', description = "49.Verify Logout prompt and after logout again Login page should be displayed.+ Verify Login with invalid Credentials for Users.")
	public void VerifyLoginLogout() throws InterruptedException, Throwable {
	Reporter.log("\n49.Verify 'ADD' User in User Management.", true);
	
	   commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	   androidJOB.VerifyDeviceDEFAULT_UserA("UserC","0");
	   androidJOB.VerifyWaitTillSLscreenISdisplayed("YouTube");
		androidJOB.ClickOnBackButtonInDeviceSide();
		androidJOB.WaitForLogOutPrompt();


	
}
	@Test(priority = 'p', description = "50.Verify Edit and Delete feature by tapping on Menu button for Profiles,UserManagement'")
	public void VerifyEditProfile() throws InterruptedException, Throwable {
	Reporter.log("\n50.Verify Edit feature by tapping on Menu button for Profiles.", true);


	androidJOB.clickOnJobs();
	dynamicjobspage.SearchJobInSearchField("EnableMultiUserProfile SL"); 
	androidJOB.ClickOnOnJob("EnableMultiUserProfile SL");
	androidJOB.clickOnModifyOption(); 
	androidJOB.EditandDeleteProfile();
	androidJOB.EditandDeleteUserManagement();
	androidJOB.ClickOnServerDetails();
	androidJOB.ClickOnmup_profiles_selectEditedProfileB();  //UserC
    androidJOB.clickOnAllowedApplications();
	androidJOB.VerifyAPPSforEditedProfile("Google");
	androidJOB.VerifyAPPSforEditedProfile("YouTube");
//	androidJOB.VerifyAPPSforEditedProfile("Drive");
	androidJOB.clickOnDoneAllowedApplication();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("EditAndDeleteProfileUserMgr SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("EditAndDeleteProfileUserMgr SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("EditAndDeleteProfileUserMgr SL",360);

	   commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");

		   androidJOB.VerifyDeviceDEFAULT_UserA("EditedUserC","0");
		   androidJOB.VerifyWaitTillSLscreenISdisplayed("YouTube");
			androidJOB.verifyAppSureLockSetting("Google");
			androidJOB.verifyAppSureLockSetting("YouTube");
//			androidJOB.verifyAppSureLockSetting("Drive");
			androidJOB.verifyDeviceSideRemoveAppSureLockSetting("SureMDM Nix");
			androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Play Store");
			androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Gmail");


		    androidJOB.goToSLAdminSettings();
		   commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
		   androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");
		   androidJOB.verifyEnableUseSysWallpaper("Enable Multi-User Profile",0);
			androidJOB.OneTimeClickAfterSearchSLsettings("Profile Management");
			androidJOB.VerifyEditedProfileUserMgr("EditedProfileB");
	//		androidJOB.verifyDeviceSideRemoveAppSureLockSetting("ProfileA");
			androidJOB.verifyDeviceSideRemoveAppSureLockSetting("ProfileAA");
	 		androidJOB.ClickOnBackButtonInDeviceSide();

           androidJOB.OneTimeClickAfterSearchSLsettings("User Management");
			androidJOB.VerifyEditedProfileUserMgr("EditedUserC");
        	androidJOB.verifyDeviceSideRemoveAppSureLockSetting("UserB");
			androidJOB.verifyDeviceSideRemoveAppSureLockSetting("UserA");
	 		androidJOB.ClickOnBackButtonInDeviceSide();
			androidJOB.ClickOnBackButtonInDeviceSide();
			commonmethdpage.commonScrollMethod("SureLock Settings");
			androidJOB.DeviceSideClickOnSureLockSettings();
			androidJOB.verifyEnableUseSysWallpaper("Check to pick SureLock background color",1);
	 		androidJOB.ClickOnBackButtonInDeviceSide();







		}

	@Test(priority = 'q', description = "51.'Verify Validation of server details with Correct credentials.'And'Verify Validation of server details with Incorrect credentials.'")
	public void VerifyServerConfig() throws InterruptedException, Throwable {
	Reporter.log("\n51.Verify Validation of server details with Correct credentials.", true);

	 commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	 androidJOB.WaitForUN_PWD();
	 androidJOB.goToSLAdminSettings();

	 commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
	 androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");
	 androidJOB.verifyEnableUseSysWallpaper("Enable Multi-User Profile",0);

	androidJOB.OneTimeClickAfterSearchSLsettings("Server Configuration");
	androidJOB.VerifyServerConfigDevice("52.88.140.72");
	androidJOB.VerifyServerConfigDevice("389");
	androidJOB.VerifyServerConfigDevice("cn=<username>,ou=People,dc=us-west-2,dc=compute,dc=internal");
	androidJOB.VerifyServerConfigDevice("cn");
	androidJOB.ClickOnBackButtonInDeviceSide();
	androidJOB.ClickOnValidate("28051900002","000000");	


	}
	@Test(priority = 'r', description = "52.'Verify Validation of server details with Incorrect credentials.'")
	public void VerifyInvalidServerConfig() throws InterruptedException, Throwable {
	Reporter.log("\n52.Verify Validation of server details with Incorrect credentials.", true);

	 commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	 androidJOB.WaitForUN_PWD();
	 androidJOB.goToSLAdminSettings();

	 commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
	 androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");
	 androidJOB.verifyEnableUseSysWallpaper("Enable Multi-User Profile",0);

	androidJOB.OneTimeClickAfterSearchSLsettings("Server Configuration");
	androidJOB.VerifyServerConfigDevice("52.88.140.72");
	androidJOB.VerifyServerConfigDevice("389");
	androidJOB.VerifyServerConfigDevice("cn=<username>,ou=People,dc=us-west-2,dc=compute,dc=internal");
	androidJOB.VerifyServerConfigDevice("cn");
	androidJOB.ClickOnBackButtonInDeviceSide();
	androidJOB.InvalidUn_PwdServerConfig("2805190000","000000");

	}
	
	@Test(priority = 's', description = "53. 'Verify disabling Multi-User Profile Mode.'")
	public void DisablingMultiUserProfileMode() throws InterruptedException, Throwable {
	Reporter.log("\n53.Verify disabling Multi-User Profile Mode.", true);
	
	androidJOB.clickOnJobs();
	dynamicjobspage.SearchJobInSearchField("EditAndDeleteProfileUserMgr SL"); 
	androidJOB.ClickOnOnJob("EditAndDeleteProfileUserMgr SL");
	androidJOB.clickOnModifyOption(); 
	androidJOB.DisableMultiUserProfile();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("DisableMultiUserProfile SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("DisableMultiUserProfile SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("DisableMultiUserProfile SL",360);

	commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	androidJOB.goToSLAdminSettings();
  commonmethdpage.commonScrollMethod("Multi-User Profile Settings");
	androidJOB.TapOnMultiUserProfileSettings("Multi-User Profile Settings");

	 androidJOB.VerifyDeviceSideDisable_icon_relocation_settings("Enable Multi-User Profile",0);
	
	
	
	}
/*	@Test(priority = 't', description = "54.Verify activation of SureLock with different 'Preferred Activation ID'.")
	public void VerifyPreferredActivationID() throws InterruptedException, Throwable {
	Reporter.log("\n54.Verify activation of SureLock with different 'Preferred Activation ID'.", true);
	
	 commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	 androidJOB.goToSLAdminSettings();
	 commonmethdpage.commonScrollMethod("About SureLock");
	 androidJOB.TapOnMultiUserProfileSettings("About SureLock");
	 commonmethdpage.commonScrollMethod("Deactivate");
	 androidJOB.TapOnMultiUserProfileSettings("Deactivate");
     androidJOB.DeactivateSLDevice();


	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
	androidJOB.VerifyAboutSL_ConsoleSide_IMEI("8242235894");
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("PreferredActivationID_IMEI SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("PreferredActivationID_IMEI SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("PreferredActivationID_IMEI SL",360);
	 androidJOB.goToSLAdminSettings();
	 commonmethdpage.commonScrollMethod("About SureLock");
	 androidJOB.TapOnMultiUserProfileSettings("About SureLock");
	 commonmethdpage.commonScrollMethod("Preferred Activation ID");
	 androidJOB.VerifyDeviceSideActivationSL("IMEI1");
     commonmethdpage.commonScrollMethod("Rate App on Play Store");
     androidJOB.VerifySLactivated_OR_not();


	 androidJOB.TapOnMultiUserProfileSettings("Deactivate");
   androidJOB.DeactivateSLDevice();
	 androidJOB.clickOnJobs();	
	 dynamicjobspage.SearchJobInSearchField("PreferredActivationID_IMEI SL"); 
	androidJOB.ClickOnOnJob("PreferredActivationID_IMEI SL");
	androidJOB.clickOnModifyOption(); 
	androidJOB.VerifyAboutSL_ConsoleSide_WIFI_MAC("8242235894");
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("PreferredActivationID_WIFIMAC SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("PreferredActivationID_WIFIMAC SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("PreferredActivationID_WIFIMAC SL",360);
	 androidJOB.goToSLAdminSettings();
	 commonmethdpage.commonScrollMethod("About SureLock");
	 androidJOB.TapOnMultiUserProfileSettings("About SureLock");
	 commonmethdpage.commonScrollMethod("Preferred Activation ID");
	 androidJOB.VerifyDeviceSideActivationSL("WIFI MAC");
	 commonmethdpage.commonScrollMethod("Rate App on Play Store");
   androidJOB.VerifySLactivated_OR_not();

	 androidJOB.TapOnMultiUserProfileSettings("Deactivate");
   androidJOB.DeactivateSLDevice();
	 androidJOB.clickOnJobs();	
	 dynamicjobspage.SearchJobInSearchField("PreferredActivationID_WIFIMAC SL"); 
	androidJOB.ClickOnOnJob("PreferredActivationID_WIFIMAC SL");
	androidJOB.clickOnModifyOption(); 
	androidJOB.VerifyAboutSL_ConsoleSide_BLUETOOTH_MAC("8242235894");
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("PreferredActivationID_BLUETOOTH_MAC SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("PreferredActivationID_BLUETOOTH_MAC SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("PreferredActivationID_BLUETOOTH_MAC SL",360);
	 androidJOB.goToSLAdminSettings();
	 commonmethdpage.commonScrollMethod("About SureLock");
	 androidJOB.TapOnMultiUserProfileSettings("About SureLock");
	 commonmethdpage.commonScrollMethod("Preferred Activation ID");
	 androidJOB.VerifyDeviceSideActivationSL("BLUETOOTH MAC");
	 commonmethdpage.commonScrollMethod("Rate App on Play Store");
     androidJOB.VerifySLactivated_OR_not();

	 androidJOB.TapOnMultiUserProfileSettings("Deactivate");
   androidJOB.DeactivateSLDevice();
	 androidJOB.clickOnJobs();	
	 dynamicjobspage.SearchJobInSearchField("PreferredActivationID_BLUETOOTH_MAC SL"); 
	androidJOB.ClickOnOnJob("PreferredActivationID_BLUETOOTH_MAC SL");
	androidJOB.clickOnModifyOption(); 
	androidJOB.VerifyAboutSL_ConsoleSide_GUID("8242235894");
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("PreferredActivationID_GUID SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("PreferredActivationID_GUID SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("PreferredActivationID_GUID SL",360);
	 androidJOB.goToSLAdminSettings();
	 commonmethdpage.commonScrollMethod("About SureLock");
	 androidJOB.TapOnMultiUserProfileSettings("About SureLock");
	 commonmethdpage.commonScrollMethod("Preferred Activation ID");
	 androidJOB.VerifyDeviceSideActivationSL("GUID");
	 commonmethdpage.commonScrollMethod("Rate App on Play Store");
  androidJOB.VerifySLactivated_OR_not();
   
	 androidJOB.TapOnMultiUserProfileSettings("Deactivate");
   androidJOB.DeactivateSLDevice();
	 androidJOB.clickOnJobs();	
	 dynamicjobspage.SearchJobInSearchField("PreferredActivationID_GUID SL"); 
	androidJOB.ClickOnOnJob("PreferredActivationID_GUID SL");
	androidJOB.clickOnModifyOption(); 
	androidJOB.VerifyAboutSL_ConsoleSide_NONE("8242235894");
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("PreferredActivationID_NONE SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("PreferredActivationID_NONE SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("PreferredActivationID_NONE SL",360);
	 androidJOB.goToSLAdminSettings();
	 commonmethdpage.commonScrollMethod("About SureLock");
	 androidJOB.TapOnMultiUserProfileSettings("About SureLock");
	 commonmethdpage.commonScrollMethod("Preferred Activation ID");
	 androidJOB.VerifyDeviceSideActivationSL("NONE");
	 commonmethdpage.commonScrollMethod("Rate App on Play Store");
	 androidJOB.VerifySLactivated_OR_not();


}*/
	
	@Test(priority = 'u', description = "55.Verify Screensaver and choose option as Use System Wallpaper")
	public void VerifyScreenSaverSysWallpaper() throws InterruptedException, Throwable {
	Reporter.log("\n55.Verify Screensaver and choose option as Use System Wallpaper", true);

  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");

	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
   androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Screensaver Settings");
	androidJOB.ClickOnscreenSaver_sett();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("ScreensaverUsesys_wallpaper SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("ScreensaverUsesys_wallpaper SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("ScreensaverUsesys_wallpaper SL",360);
	androidJOB.WaitTenSecsForScreenSaverToDisplay();
	androidJOB.verifyDeviceSideRemoveAppSureLockSetting("Tap the screen 5 times");

}
	
	@Test(priority = 'v', description = "56.Verify Screensaver and select any image or URL as screensaver")
	public void VerifyScreenSaverIamge_Url() throws InterruptedException, Throwable {
	Reporter.log("\n56.Verify Screensaver and select any image or URL as screensaver", true);

	
	  commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");

		androidJOB.clickOnJobs();	
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureLockSettings();
	   androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Screensaver Settings");
		androidJOB.ClickOnscreenSaver_settImage_Url(Config.DeviceImagePath);  //Pass in config
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("Screensaver_use_MediaFile SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Screensaver_use_MediaFile SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("Screensaver_use_MediaFile SL",360);
		androidJOB.VerifyScreenSaverImageView();
		
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("Screensaver_use_MediaFile SL"); 
		androidJOB.ClickOnOnJob("Screensaver_use_MediaFile SL");
		androidJOB.clickOnModifyOption(); 
		androidJOB.clickOnsureLockSetting();
		androidJOB.ClickOnSearchBar("Screensaver Settings");
		androidJOB.ClickOnDisablescreenSaver_sett();
		androidJOB.clickOndoneButtonSureLockSetting();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("DisableScreensaver SL");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableScreensaver SL");
//		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob("DisableScreensaver SL",360);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");

	    androidJOB.goToSLAdminSettings();
        androidJOB.DeviceSideClickOnSureLockSettings();
		androidJOB.DeviceSideSLSettingClickOnSearchBar("Screensaver Settings");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Screensaver Settings");
	    androidJOB.VerifyDeviceSideDisable_icon_relocation_settings("Enable Screensaver",0);
		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
	    androidJOB.goToSLAdminSettings();
		 commonmethdpage.commonScrollMethod("About SureLock");
		accountsettingspage.ClickOnExitSureLockButton("Ends Lockdown Mode");
		accountsettingspage.ClickOnExitPopUpButtonInSureLock("Exit");

}
	//********* Its Putting the WIFI in OFF mode ,Hence Jobs will not deploy on time.So all will fail.**********
	
/*	@Test(priority = 'w', description = "57.Verify enabling 'Wi-Fi Settings' as 'Always Off'")
	public void VerifyPeripheralSettings_enablingWiFiSettings_as_AlwaysOff() throws InterruptedException, Throwable {
	Reporter.log("\n57.Verify enabling 'Wi-Fi Settings' as 'Always Off'", true);
	
	androidJOB.clickOnJobs();	
	androidJOB.clickNewJob(); 
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnSureLockSettings();
   androidJOB.clickOnsureLockSetting();
	androidJOB.ClickOnSearchBar("Wifi Settings");
	androidJOB.ClickOnwifi_always_off();
	androidJOB.clickOndoneButtonSureLockSetting();
	androidJOB.clickOnsaveSureLockSetting();
	androidJOB.SurelockSettingName("WifiSettingsalways_off SL");
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("WifiSettingsalways_off SL");
//	androidJOB.ClickOnApplyJobOkButton();
	androidJOB.CheckStatusOfappliedInstalledJob("WifiSettingsalways_off SL",360);
   commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");
   androidJOB.goToSLAdminSettings();
	androidJOB.DeviceSideClickOnSureLockSettings();
	androidJOB.DeviceSideSLSettingClickOnSearchBar("WiFi Settings");
	androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("WiFi Settings");
	androidJOB.VerifyDeviceSideWifiSL("Always Off",2);
	androidJOB.ClickOnCancelWifiSettings("WiFi Settings");
	
	commonmethdpage.ClickOnAppiumButtons("Done");
	commonmethdpage.ClickOnAppiumButtons("Done");
	accountsettingspage.ClickOnExitSureLockButton("Ends Lockdown Mode");
	accountsettingspage.ClickOnExitPopUpButtonInSureLock("Exit");



	}

	
	
*/	
	}
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



