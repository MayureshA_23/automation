package DeviceGrid_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import bsh.ParseException;

//EMM device should be there in console
public class DeviceGrid_NewTestCases extends Initialization {

	@Test(priority = 0, description = "Verify UI for Device name advance search in Device grid")
	public void VerifyOfDeviceNameSearchOptions_TC_DG_01() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n1.Verify UI for Device name advance search in Device grid", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyOfDeviceNameSearchOptions();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 1, description = "Verify Device name  advance search with 'contains' option")
	public void VerifyOfDeviceNameSearchOp_Contains_TC_DG_02() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n2.Verify Device name  advance search with 'contains' option", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameContains);
		deviceGrid.verifyOfDevNameSearchWithContains(Config.DeviceNameContains);
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 2, description = "Verify Device name  advance search with 'starts with' option")
	public void VerifyOfDeviceNameSearchOp_StartsWith_TC_DG_02() throws InterruptedException {
		Reporter.log("\n3.Verify Device name  advance search with 'starts with' option", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameStartsWith);
		deviceGrid.verifyOfDevNameSearchStartWith(Config.DeviceNameStartsWith);
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 3, description = "Verify Device name  advance search with 'ends with' option")
	public void VerifyOfDeviceNameSearchOp_EndsWith_TC_DG_02() throws InterruptedException {
		Reporter.log("\n4.Verify Device name  advance search with 'ends with' option", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameEndsWith);
		deviceGrid.verifyOfDevNameSearchEndsWith(Config.DeviceNameEndsWith);
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 4, description = "Verify saving the searched values as filter")
	public void VerifyOfCreatingFilterInDevCol_Contains_TC_DG_06() throws InterruptedException, AWTException {
		Reporter.log("\n5.Verify saving the searched values as filter", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameContains);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp1);
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName(Config.ContainsFilterDeviceName);
		Filter.ClickOKButtonFilter(Config.ContainsFilterDeviceName);
		Filter.ClickOnFilter();
		Filter.ClickOnCreatedFilter(Config.ContainsFilterDeviceName);
		deviceGrid.verifyValueInDevNameColFilter_Contains();
		Filter.ClickOnGroups();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 5, description = "Verify saving the searched values as filter")
	public void VerifyOfCreatingFilterInDevCol_Startswith_TC_DG_06() throws InterruptedException, AWTException {
		Reporter.log("\n6.Verify saving the searched values as filter", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameStartsWith);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp2);
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName(Config.StartsWithFilterDeviceName);
		Filter.ClickOKButtonFilter(Config.StartsWithFilterDeviceName);
		Filter.ClickOnFilter();
		Filter.ClickOnCreatedFilter(Config.StartsWithFilterDeviceName);
		deviceGrid.verifyValueInDevNameColFilter_StartsWith();
		Filter.ClickOnGroups();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 6, description = "Verify saving the searched values as filter")
	public void VerifyOfCreatingFilterInDevCol_EndsWith_TC_DG_06() throws InterruptedException, AWTException {
		Reporter.log("\n7.Verify saving the searched values as filter", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameEndsWith);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp3);
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName(Config.EndsWithFilterDeviceName);
		Filter.ClickOKButtonFilter(Config.EndsWithFilterDeviceName);
		Filter.ClickOnFilter();
		Filter.ClickOnCreatedFilter(Config.EndsWithFilterDeviceName);
		deviceGrid.verifyValueInDevNameColFilter_EndsWith();
		Filter.ClickOnGroups();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = '7', description = "Verify modifying the above created filter")
	public void VerifyOfModifyCreatedFilter_Contains_TC_DG_07() throws InterruptedException, AWTException {
		Reporter.log("\n8.Verify modifying the above created filter", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameContains);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp1);
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName("DeviceNameFilter_Contains");
		Filter.ClickOKButtonFilter("DeviceNameFilter_Contains");
		Filter.ClickOnFilter();
		Filter.ClickOnCreatedFilter("DeviceNameFilter_Contains");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName",Config.DeviceNameContainsFilter_Update);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp1);	
		Filter.ClickOnUpdateFilter();
		Filter.clickOnOkBtnInUpdate("DeviceNameFilter_Contains");
		Filter.MessageOnFilterUpdate("DeviceNameFilter_Contains");
		deviceGrid.verifyFilterAfterUpdate_Contains(Config.DeviceNameContainsFilter_Update);
		Filter.ClickOnGroups();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = '8', description = "Verify modifying the above created filter")
	public void VerifyOfModifyCreatedFilter_Start_TC_DG_07() throws InterruptedException, AWTException {
		Reporter.log("\n9.Verify modifying the above created filter", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameStartsWith);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp2);
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName("DeviceNameFilter_Starts");
		Filter.ClickOKButtonFilter("DeviceNameFilter_Starts");
		Filter.ClickOnFilter();
		Filter.ClickOnCreatedFilter("DeviceNameFilter_Starts");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameStartsFilter_Update);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp2);
		Filter.ClickOnUpdateFilter();
		Filter.clickOnOkBtnInUpdate("DeviceNameFilter_Starts");
		Filter.MessageOnFilterUpdate("DeviceNameFilter_Starts");
		deviceGrid.verifyFilterAfterUpdate_StartsWith(Config.DeviceNameStartsFilter_Update);
		Filter.ClickOnGroups();
		deviceGrid.SelectSearchOption("Basic Search");

	}

	@Test(priority = '9', description = "Verify modifying the above created filter")
	public void VerifyOfModifyCreatedFilter_End_TC_DG_07() throws InterruptedException, AWTException {
		Reporter.log("\n10.Verify modifying the above created filter", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameEndsWith);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp3);
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName("DeviceNameFilter_Ends");
		Filter.ClickOKButtonFilter("DeviceNameFilter_Ends");
		Filter.ClickOnFilter();
		Filter.ClickOnCreatedFilter("DeviceNameFilter_Ends");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceNameendsFilter_Update);
		deviceGrid.ReadingValueFromConsole(Config.DeviceNameSearchOp3);
		Filter.ClickOnUpdateFilter();
		Filter.clickOnOkBtnInUpdate("DeviceNameFilter_Ends");
		Filter.MessageOnFilterUpdate("DeviceNameFilter_Ends");
		deviceGrid.verifyFilterAfterUpdate_EndsWith(Config.DeviceNameendsFilter_Update);
		Filter.ClickOnGroups();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 'A', description = "Verify Agent version in device grid column is displayed as x.y.z format")
	public void VerifyOfAngentVersion_TC_DG_001() throws InterruptedException, IOException, InvalidFormatException {
		Reporter.log("\n11.Verify Agent version in device grid column is displayed as x.y.z format", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Agent Version");
		deviceGrid.Enable_DisbleColumn("Agent Version", "Enable");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Agent Version");
		androidJOB.VerifyOfNixVFromColumn();
	}

	@Test(priority = 'B', description = "Verify SL/SF/SV version columns in device grid is displayed as x.y.z format")
	public void VerifyOfSLVersionInSLVersionCol() throws InterruptedException, IOException, InvalidFormatException {
		Reporter.log("\n12.Verify SL/SF/SV version columns in device grid is displayed as x.y.z format", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("SureLock Version");
		deviceGrid.Enable_DisbleColumn("SureLock Version", "Enable");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("SureLock Version");
		androidJOB.VerifyOfSLFromtColumn();
	}

	@Test(priority = 'C', description = "Verify if default columns are enabled in newly created DNS accounts.")
	public void VerifyOfCustomColumnsInNewDNS_TC_DG_001() throws InterruptedException {
		// run only in new Dns
		Reporter.log("\n13.Verify if default columns are enabled in newly created DNS accounts.", true);
		deviceGrid.verifyOfDefaultColInNewDns1();
	}

	@Test(priority = 'D', description = "Verify Device Name in device info panel")
	public void VerifyOfDeviceNameInDevInfoPanel() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n14.Verify Device Name in device info panel", true);
		commonmethdpage.SearchDevice("Client9146");
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName("RegressionDevice");
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		deviceGrid.verifyOfDeviceNameChangedMsg();
		Reporter.log("Android device remaned.", true);
		commonmethdpage.SearchDevice("RegressionDevice");
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName("Client9146");
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		Reporter.log("Android device remaned back to same name.", true);
	}

	@Test(priority = 'E', description = "Verify device search by searching more than one device at a time.")
	public void VerifyOfDeviceNameWithMultipleVal() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n15.Verify device search by searching more than one device at a time.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.MultipleDeviceName);
		deviceGrid.clickOnExpectedSearchOpInDeviceNameCol("0");
		deviceGrid.verifyEnrolledDeviceForMultiSearch(Config.Device1, Config.Device2);
		deviceGrid.clickOnExpectedSearchOpInDeviceNameCol("1");
		deviceGrid.verifyEnrolledDeviceForMultiSearch(Config.Device1, Config.Device2);
		deviceGrid.clickOnExpectedSearchOpInDeviceNameCol("2");
		deviceGrid.verifyEnrolledDeviceForMultiSearch(Config.Device1, Config.Device2);
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 'F', description = "Verify the device group path by changing the path multiple times")
	public void VerifyOfGrpPathByChangingMultipleTimes() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n16.Verify the device group path by changing the path multiple times", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Group Path");
		deviceGrid.Enable_DisbleColumn("Group Path", "Enable");
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Group Path");
		deviceGrid.ReadingGroupPathofDeviceGrp();
		rightclickDevicegrid.VerifyGroupPresence("dontouch1");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch1");
		deviceGrid.VerifyOfGroupPathofDeviceSubGrp();
	}

	@Test(priority = 'G', description = "Verify Right Click 'Install' option.",enabled=false)
	public void VerifyOfInstallOpForUEMnix() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n17.Verify Right Click 'Install' option.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceWithStandAloneSL);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceWithStandAloneSL);
		rightclickDevicegrid.ClickOnSureLock();
		rightclickDevicegrid.VerifyInstallOptionForUEMnix();
		rightclickDevicegrid.ClickOnrefreshButtonDeviceInfo();
	}

	/*@Test(priority = 'H', description = "Verify Free Storage Memory in Device Grid for Windows devices enrolled through EMM")
	public void VerifyOfFreeMemoryManForEMMWindow_TC_DG_252() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		// EMM device should be there in console
		Reporter.log("\n18.Verify Free Storage Memory in Device Grid for Windows devices enrolled through EMM", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Free Storage Memory");
		deviceGrid.Enable_DisbleColumn("Free Storage Memory", "Enable");
		commonmethdpage.SearchDevice(Config.EMMEnrolledWindows_DeviceName);
		deviceGrid.verifyOfFreeStorageMemoryCol_EMMWindowDevice();
	}*/

	@Test(priority = 'I', description = "Verify Roaming data option is removed from Console.")
	public void VerifyOfRoamingDataOption_DeviceInfoPanel()
			throws InterruptedException, InvalidFormatException, IOException {
		Reporter.log("\n19.Verify Roaming data option is removed from Console.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		androidJOB.clickOnTeleComMangeMentEditBtn();
		androidJOB.verifyOfRoamingDataOp();
		androidJOB.closeTelecomManagementJob();
	}

	@Test(priority = 'J', description = "Verify Roaming data option is removed from Console.")
	public void VerifyOfRoamingDataOption_Android() throws InterruptedException, InvalidFormatException, IOException {
		Reporter.log("\n20.Verify Roaming data option is removed from Console.", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTeleComManagementJob();
		androidJOB.verifyOfRoamingDataOp();
		androidJOB.closeTelecomManagementJob();
		androidJOB.clickOnCancelBtn_AndroidJobsTab();
	}

	@Test(priority = 'K', description = "Verify Roaming data option is removed from Console.")
	public void VerifyOfRoamingDataOption_Windows() throws InterruptedException, InvalidFormatException, IOException {
		Reporter.log("\n21.Verify Roaming data option is removed from Console.", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsOS();
		androidJOB.clickOnTeleComManagementJob();
		androidJOB.verifyOfRoamingDataOp();
		deviceGrid.RefreshGrid();
		/*androidJOB.closeTelecomManagementJob();
		androidJOB.clickOnCancelBtn_AndroidJobsTab();*/
	}

	//Madhu
	@Test(priority = 'L', description = "Verify the custom column icon.")
	public void verifyOfCustomColumnIcon() throws InterruptedException {
		Reporter.log("22.Verify the custom column icon.", true);
		commonmethdpage.ClickOnHomePage();
		deviceGrid.clickOnGridColumn();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.verifyCustomColumnAddButton();
	}

	// Run Only in new DNS
	//	@Test(priority = 'M', description = "Verify the custom column default message.")
	//	public void verifyCustomColnmDefaultMessage() throws InterruptedException {
	//		Reporter.log("23.Verify the custom column default message", true);
	//		commonmethdpage.ClickOnAllDevicesButton();
	//		deviceGrid.clickOnGridColumn();
	//		deviceGrid.ClickOncustomColumnList();
	//		deviceGrid.customColumnDefaultMessage();
	//	}

	@Test(priority = 'N', description = "Change the android device name when device is offline.")
	public void verifyOfOfflineDevRename_Android()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("24.Change the android device name when device is offline.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.Offline_Android);
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName("Renamed_Android");
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		deviceGrid.verifyOfDeviceNameChangedMsg();
		Reporter.log("Android device remaned.", true);

		commonmethdpage.SearchDevice("Renamed_Android");
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName(Config.Offline_Android);
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		Reporter.log("Android device remaned back to same name.", true);
	}

	@Test(priority = 'O', description = "Change the iOS device name when device is offline.")
	public void verifyOfOfflineDevRename_iOS()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("25.Change the iOS device name when device is offline.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.Offline_iOS);
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName("Renamed_iOS");
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		deviceGrid.verifyOfDeviceNameChangedMsg();
		Reporter.log("iOS device remaned.", true);

		commonmethdpage.SearchDevice("Renamed_iOS");
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName(Config.Offline_iOS);
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		Reporter.log("iOS device remaned back to same name.", true);
	}
/*
	@Test(priority = 'P', description = "Change the MacOS device name when device is offline ")
	public void verifyOfOfflineDevRename_MacOS()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("26.Change the MacOs device name when device is offline.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.Offline_MacOS);
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName("Renamed_MacOS");
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		deviceGrid.verifyOfDeviceNameChangedMsg();
		Reporter.log("MasOS device remaned.", true);

		commonmethdpage.SearchDevice("Renamed_MacOS");
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName(Config.Offline_MacOS);
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		Reporter.log("MacOs device remaned back to same name.", true);
	}*/

	@Test(priority = 'Q', description = "Change the windows device name when device is offline ")
	public void verifyOfOfflineDevRename_Windows()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("27.Change the windows device name when device is offline.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.Offline_Windows);
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName("Renamed_Windows");
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		deviceGrid.verifyOfDeviceNameChangedMsg();
		Reporter.log("Windows device remaned.", true);

		commonmethdpage.SearchDevice("Renamed_Windows");
		deviceGrid.ClickOnEditDevName();
		deviceGrid.EnterDeviceName(Config.Offline_Windows);
		deviceGrid.clickOnSaveBtnInEditDeviceName();
		Reporter.log("Windows device remaned back to same name.", true);
	}
	
	@Test(priority = 'R', description = "Verify Device Grid Pagination is not changing after navigating from one tab to another.")
	public void verifyPaginationNavigatingToAnotherTab() throws InterruptedException {
		Reporter.log("28.Verify Device Grid Pagination is not changing after navigating from one tab to another.", true);
//		deviceGrid.verifyPagination("10");
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.verifyPagination("20");
		deviceGrid.readDeviceCount();
		inboxpage.ClickOnInbox();
		commonmethdpage.ClickOnHomePage();
		deviceGrid.verifyDeviceCountAfterPagination();
	}

	@Test(priority = 'S', description = "Verify Device Grid Pagination is not changing after Logging out of the console.")
	public void verifyPaginationAfterLoggingOutAndLoginAgain() throws InterruptedException, IOException {
		Reporter.log("29.Verify Device Grid Pagination is not changing after Logging out of the console.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.verifyPagination("50");
		deviceGrid.readDeviceCount();
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickOnLogout();
		accountRegistrationPage.enterUsereName(Config.userID);
		accountRegistrationPage.enterPassword(Config.password);
		accountRegistrationPage.pressEnterToLogin();
		accountRegistrationPage.verifyloginIsSuccessful();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.verifyDeviceCountAfterPagination();
	}

	@Test(priority = 'T', description = "Verify Device Grid Pagination is not changing after Refreshing the console.")
	public void verifyPaginationByClickingOnRefresh() throws InterruptedException, IOException {
		Reporter.log("30.Verify Device Grid Pagination is not changing after Refreshing the console.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.verifyPagination("100");
		deviceGrid.readDeviceCount();
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnHomePage();
		deviceGrid.verifyDeviceCountAfterPagination();
		deviceGrid.verifyPagination("20");
	}

	@Test(priority = 'U', description = "Verify columns status by enabling columns after logout and login with same user.")
	public void verifyEnablingColumns() throws InterruptedException, IOException {
		Reporter.log("31.Verify columns status by enabling columns after logout and login with same user.", true);
		commonmethdpage.ClickOnHomePage();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Hostname");
		deviceGrid.Enable_DisbleColumn("Hostname", "Enable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device Locale");
		deviceGrid.Enable_DisbleColumn("Device Locale", "Enable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Cell Signal Strength");
		deviceGrid.Enable_DisbleColumn("Cell Signal Strength", "Enable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Serial Number");
		deviceGrid.Enable_DisbleColumn("Serial Number", "Enable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Bluetooth status");
		deviceGrid.Enable_DisbleColumn("Bluetooth status", "Enable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("File Threat Detected");
		deviceGrid.Enable_DisbleColumn("File Threat Detected", "Enable");
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickOnLogout();
		accountRegistrationPage.enterUsereName(Config.userID);
		accountRegistrationPage.enterPassword(Config.password);
		accountRegistrationPage.pressEnterToLogin();
		accountRegistrationPage.verifyloginIsSuccessful();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Hostname");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Device Locale");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Cell Signal Strength");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth status");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("File Threat Detected");
	}

	@Test(priority = 'V', description = "Verify columns status by disabled columns after logout and login with same user.")
	public void verifyDisablingColumns() throws InterruptedException, IOException {
		Reporter.log("32.Verify columns status by disabling columns after logout and login with same user.", true);
		commonmethdpage.ClickOnHomePage();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Hostname");
		deviceGrid.Enable_DisbleColumn("Hostname", "Disable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device Locale");
		deviceGrid.Enable_DisbleColumn("Device Locale", "Disable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Cell Signal Strength");
		deviceGrid.Enable_DisbleColumn("Cell Signal Strength", "Disable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Serial Number");
		deviceGrid.Enable_DisbleColumn("Serial Number", "Disable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Bluetooth status");
		deviceGrid.Enable_DisbleColumn("Bluetooth status", "Disable");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("File Threat Detected");
		deviceGrid.Enable_DisbleColumn("File Threat Detected", "Disable");
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickOnLogout();
		accountRegistrationPage.enterUsereName(Config.userID);
		accountRegistrationPage.enterPassword(Config.password);
		accountRegistrationPage.pressEnterToLogin();
		accountRegistrationPage.verifyloginIsSuccessful();
		deviceGrid.VerifyColumnIsNotPresentOnGridAndScroll("Hostname");
		deviceGrid.VerifyColumnIsNotPresentOnGridAndScroll("Device Locale");
		deviceGrid.VerifyColumnIsNotPresentOnGridAndScroll("Cell Signal Strength");
		deviceGrid.VerifyColumnIsNotPresentOnGridAndScroll("Bluetooth status");
		deviceGrid.VerifyColumnIsNotPresentOnGridAndScroll("File Threat Detected");
	}

	

	@Test(priority = 'W', description = "Verify device HashCode grid column value in Ondemand Reports.")
	public void verifyHashCodeColumnInOnDemandReport() throws InterruptedException {
		Reporter.log("33.Verify device HashCode grid column value in Ondemand Reports.", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnAssetTrackingReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Hash Code");
		reportsPage.SwitchBackWindow();
		deviceGrid.ClickOnHomePage();
	}

	@Test(priority = 'X', description = "Check for the error Message in the Installed application column in device grid")
	public void verifySearchInInstalledApplicationCol() throws InterruptedException {
		Reporter.log("\n34.Check for the error Message in the Installed application column in device grid", true);
		deviceGrid.DisablingAllInstalledApplicationColumns();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOnInstalledApplicationVersionList();
		deviceGrid.SearchingForApplicationInstalledAppVersionList("com.android.chrome");
		deviceGrid.SelectingApplicationIninstalledAppVersionList();
		deviceGrid.ClickOnSearchOption();
		deviceGrid.ClickOnAdvanceSearchButton();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Chrome ( com.android.chrome  ) ");

		deviceGrid.EnterValueInAdvanceSearchColumn("Application1", "<");
		deviceGrid.verifyErrMsgInInstallAppCol();
		deviceGrid.EnterValueInAdvanceSearchColumn("Application1", ">");
		deviceGrid.verifyErrMsgInInstallAppCol();
		deviceGrid.EnterValueInAdvanceSearchColumn("Application1", "<=");
		deviceGrid.verifyErrMsgInInstallAppCol();
		deviceGrid.EnterValueInAdvanceSearchColumn("Application1", ">=");
		deviceGrid.verifyErrMsgInInstallAppCol();
		deviceGrid.EnterValueInAdvanceSearchColumn("Application1", "<>");
		deviceGrid.verifyErrMsgInInstallAppCol();
		deviceGrid.EnterValueInAdvanceSearchColumn("Application1", "=<");
		deviceGrid.verifyErrMsgInInstallAppCol();
		deviceGrid.RefreshGrid();
	}
	
	@Test(priority = 'Y', description = "Verify Swipe to Unlock in remote screen for android device.")
	public void verifySwipeToUnlockOfAndroid() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n35.Verify Swipe to Unlock in remote screen for android device.",true);
		deviceGrid.SelectSearchOption("Basic Search");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.KnoxDevice);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfRemoteSupportLaunched();
		deviceGrid.verifySwipeToUnlockInAndroidRemoteSupport();
		remotesupportpage.SwitchToMainWindow();
		deviceGrid.RefreshGrid();
	}
	
	@Test(priority = 'Z',description="Verify the functionality of Swipe to Unlock in remote screen for android device.")
	public void verifyDeviceUnlock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n36.Verify the functionality of Swipe to Unlock in remote screen for android device.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		deviceGrid.verifyDeviceUnlock();
		commonmethdpage.SearchDevice(Config.KnoxDevice);
		rightclickDevicegrid.RightClickOndevice(Config.KnoxDevice);
		rightclickDevicegrid.VerifyClickOnRemote();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfRemoteSupportLaunched();
		deviceGrid.clickOnSwipeToUnlock();
		deviceGrid.verifyLockAndUnlockStatusOfDevice();
		remotesupportpage.SwitchToMainWindow();
		
	}

	/*@Test(priority = 'a', description="Verify Network Panel for macOS device.")
	public void verifyDevInfoPanelOfMacOS_TC_DG_122() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n37.Verify Network Panel for macOS device.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.Macos_DeviceName);
		deviceGrid.verifyMacAddressOfMacOS(Config.MacOS_DeviceMacAddress);
		deviceGrid.verifyIPAddressOfMacOS(Config.MacOS_IPAddress);
		deviceGrid.verifyConnectionOfMacOS(Config.MacOS_Connection);
		deviceGrid.verifySerialNumOfMacOS(Config.MacOS_DeviceSerialNumber);
	}*/
	
@Test(priority = 'b', description = "Verify Right click by selecting single iOS device.")
	public void verifySureProductsOnSingleiOSDevice() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n38.Verify Right click by selecting single iOS device.",true);
		commonmethdpage.SearchDevice(Config.iOS_DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.iOS_DeviceName);
		deviceGrid.readingOptionsOnRightClick(deviceGrid.AndroidOptions);
	}
	
/*@Test(priority = 'c', description = "Verify Right click by selecting multiple iOS devices.")
	public void verifySureProductsOnMultipleiOSDevice() throws AWTException, InterruptedException {
		Reporter.log("\n39.Verify Right click by selecting multiple iOS devices.", true);
		commonmethdpage.SearchForMultipleDevice(Config.iOS_DeviceName,Config.iOS_MultipleDevice);
		quickactiontoolbarpage.SelectingMultipleDevices();
		rightclickDevicegrid.RightClickOndevice(Config.iOS_DeviceName);
		deviceGrid.readingOptionsOnRightClick(deviceGrid.AndroidOptions);
	}
	
	@Test(priority = 'd', description = "Verify Right click by selecting multiple macOS devices.")
	public void verifySureProductsOnSingleMacOSDevice() throws AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n40.Verify Right click by selecting single iOS device.",true);
		commonmethdpage.SearchDevice(Config.Macos_DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.Macos_DeviceName);
		deviceGrid.readingOptionsOnRightClick(deviceGrid.AndroidOptions);
		deviceGrid.readingOptionsOnRightClick(deviceGrid.AndroidOptions);
	}
	
	@Test(priority = 'e', description = "Verify Right click by selecting multiple macOS devices.")
	public void verifySureProductsOnMultipleMacOSDevice() throws AWTException, InterruptedException {
		Reporter.log("\n41.Verify Right click by selecting multiple macOS devices.", true);
		commonmethdpage.SearchForMultipleDevice(Config.Macos_DeviceName,Config.MacOS_MultipleDeviceName);
		quickactiontoolbarpage.SelectingMultipleDevices();
		rightclickDevicegrid.RightClickOndevice(Config.Macos_DeviceName);
		deviceGrid.readingOptionsOnRightClick(deviceGrid.AndroidOptions);
	}*/
	@Test(priority = 'f',description = "Verify Job status in Device grid.")
	public void verifyJobStatusInDeviceGrid_OrangeDot_BlackDot_GreenDot() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n42.Verify Job status in Device grid.",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		androidJOB.verifyPendingJobStatusColor();
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		commonmethdpage.clickOnGridrefreshbutton();
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobSuccessStateSymbol);
	}
	@Test(priority = 'g',description = "Verify Job status in Device grid.")
	public void verifyJobStatusInDeviceGrid_RedDot() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n43.Verify Job status in Device grid.",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		quickactiontoolbarpage.CreatingSureFoxSettingsJob(quickactiontoolbarpage.ErrorjobName);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		//androidJOB.ClickOnApplyJobOkButton();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);	
	}
	//Harpinder
	@Test(priority = 'h', description = "verify Notes In Device Info Panel")
	public void verifyNotesInDeviceInfoPanel_TC_DG_149() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		System.out.println("\n44.*****verify Notes In Device Info Panel******");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnNotesEdit();
		deviceGrid.clearAddNotesField();
		deviceGrid.enterNotefordevice(Config.EnterNotes);
		deviceGrid.clickOnNoteSaveButton();
		deviceGrid.verifyNotesAdded(Config.ExpectedNotes);
	}

	@Test(priority = 'i', description = "Verify Device locale column in device grid")
	public void verifyDeviceLocaleColumnInDeviceGrid_TC_001() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{System.out.println("\n45.*****Verify Device locale column in device grid******");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnGridColumnSearch();
		deviceGrid.enterColumnName("Device locale");
		deviceGrid.verifyGridColumnIsEnabledAndGetFinalValue();
	}

	@Test(priority = 'j', description = "Verify Network Panel for iOS device.")
	public void verifyNetworkPanelForiOSDevice_TC_DG_121() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		System.out.println("\n46.*****Verify Network Panel for iOS device.******");
		commonmethdpage.SearchDevice(Config.SearchiOSdevice);
		System.out.println("Network Panel For iOS Device");
		deviceGrid.iOSNetworkPanelElements();
		Reporter.log("verified Network Panel For iOSD evice", true);

	}

	@Test(priority = 'k', description = "Verify Installed Application Version column in Device Grid")
	public void verifyInstalledApplicationVersioncolumnInDeviceGrid_TC_DG_03()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		System.out.println("\n47.*****Verify Installed Application Version column in Device Grid******");
		deviceGrid.DisablingAllInstalledApplicationColumns();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnInstalledApplicationVersion();
		deviceGrid.clickOnCustomAppListSearchBox();
		deviceGrid.enterAppNameForSearch(Config.SearchInstalledAppForCustomColumn);
		deviceGrid.installedAppValue();
	}

	@Test(priority = 'l', description = "Verify Right Click options across android platform.")
	public void VerifyRightClickOptionsAcrossAndroidplatform_TC_DG_074() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		System.out.println("\n48.*****Verify Right Click options across android platform.******");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.VerifyRightClickOptionsAcrossAndroidplatform();

	}

	@Test(priority = 'm', description = "Verify Right click for the iOS platform device which is enrolled newly/enrolled recently ")
	public void VerifyRightClickForTheiOSPlatformDevicesWhichEnrolledNewlyOREnrolledRecently_TC_DG_03()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		System.out.println("\n49.*****Verify Right click for the iOS platform device which is enrolled newly/enrolled recently******");
		commonmethdpage.SearchDevice(Config.SearchiOSdevice);
		deviceGrid.VerifyRightClickForTheiOSPlatformDeviceWhichEnrolledNewlyOREnrolledRecently();

	}

	@Test(priority = 'n', description = "Verify Right click for the Windows platform device which is enrolled newly/enrolled recently ")
	public void VerifyRightClickForTheWindowsPlatformDeviceWhichEnrolledNewlyOREnrolledRecently_TC_DG_03()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		System.out.println("\n50*****Verify Right click for the Windows platform device which is enrolled newly/enrolled recently******");
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		deviceGrid.VerifyRightClickForTheWindowsPlatformDeviceWhichEnrolledNewlyOREnrolledRecently();
		
	}

	@Test(priority = 'o', description = "Verify Right click for the macOS platform device which is enrolled newly/enrolled recently")
	public void VerifyRightClickForTheMacOSPlatformDeviceWhichEnrolledNewlyOREnrolledRecently_TC_DG_03() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		System.out.println("\n51.*****Verify Right click for the macOS platform device which is enrolled newly/enrolled recently******");
		commonmethdpage.SearchDevice(Config.Macos_DeviceName);
		deviceGrid.VerifyRightClickForTheMacOSPlatformDeviceWhichEnrolledNewlyOREnrolledRecently();
		// Reporter.log("Verify Right click for the macOS platform device which is
		// enrolled newly/enrolled recently " ,true);

	}

	@Test(priority = 'p', description = "Verify Launch and Exit SureLock Option in right click Menu")
	public void VerifyLaunchAndExitSureLockOptioninRightClickMenu() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		System.out.println("\n52.*****Verify Launch and Exit SureLock Option in right click Menu******");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.LaunchSurelock();
		deviceGrid.verifySurelockLaunchedJob();
		deviceGrid.refreshDevice();
		deviceGrid.waitForGreenShieldAndVerifySurelockIslaunched();
		deviceGrid.ExitSurelock();
		deviceGrid.verifySurelockExitJob();
		deviceGrid.refreshDevice();
		deviceGrid.waitForGrayShieldAndVerifySurelockIsExited();
	}

	@Test(priority = 'q', description = "Verify colour of shield icon when UEM Nix is installed, but Integrated SureLock is in exit state (disabled).")
	public void verifyColourOfShieldIconWhenUEMNixIsInstalledButIntegratedSureLockisInExitState() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		System.out.println("\n53.*****Verify colour of shield icon when UEM Nix is installed, but Integrated SureLock is in exit state (disabled)******");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.verifyGrayColorShield();
		Reporter.log("Verified Colour Of Shield incon is Gray When Integrated SureLock is in Exit State", true);
	}

	@Test(priority = 'r', description = "Verify colour of shield icon when UEM Nix is installed and Integrated SureLock is in foreground.")
	public void verifyColourOfShieldIconWhenUEMNixIsInstalledAndIntegratedSureLockisInForegound() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		System.out.println("\n54.*****Verify colour of shield icon when UEM Nix is installed and Integrated SureLock is in foreground.******");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.verifyGreenColorShield();
		Reporter.log("Verify colour of shield icon when UEM Nix is installed and Integrated SureLock is in foreground.",true);
	}

	@Test(priority = 's', description = "Verify colour of shield icon when Suremdm nix,surelock is installed and surelock is running in device")
	public void verifyColourOfShieldIconWhenstandAloneSLIsLaunchedIndevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		System.out.println("\n55.*****Verify colour of shield icon when Suremdm nix,surelock is installed and surelock is running in device******");
		commonmethdpage.SearchDevice(Config.DeviceWithStandAloneSL);
		deviceGrid.verifyGreenShieldForStandalonSurelock();
	}

	@Test(priority = 't', description = "Verify colour of shield icon when Suremdm nix,surelock is installed and surelock is exited in device.")
	public void verifyColourOfShieldIconWhenstandAloneSLIsExitIndevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		System.out.println("\n56.*****Verify colour of shield icon when Suremdm nix,surelock is installed and surelock is exited in device******");
		commonmethdpage.SearchDevice(Config.DeviceWithStandAloneSL);
		deviceGrid.verifyRedShieldForStandalonSurelock();

	}

	@Test(priority = 'u', description = "Verify Location Tracking in device info panel")
	public void VerifyLocationTrackingInDeviceInfoPanel_TC_DG_150() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n57.Verify Location Tracking in device info panel",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.CheckingLocationTrackingStatus();
		quickactiontoolbarpage.ClickOnLocate_UM();
		quickactiontoolbarpage.ClickingOnTurnOnButton();
		quickactiontoolbarpage.TurningOnLocationTracking("1");
		deviceGrid.refreshDevice();
		deviceGrid.clickLocationTracking();
		deviceGrid.verifyLocationTrackingIn();
		deviceGrid.changeToPreviousTab();
	}

	@Test(priority = 'v', description = "Verify Call log and SMS log tracking in telecom management policy job")
	public void verifyCalllogAndSMSLogTracking_TC_JO_239()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		System.out.println("\n58.*****Verify Call log and SMS log tracking in telecom management policy job******");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceGrid.clickOnTelecomManagementPolicyJob();
		deviceGrid.enterJobName("CallAndSmsLogTracking");
		deviceGrid.uncheckDataUsage();
		deviceGrid.clickCallLogTracking();
		deviceGrid.selectOnOROfForCallTracking("On");
		deviceGrid.EnterTrackingPeriodicityForCall("15");
		deviceGrid.clickSmsLogTracking();
		deviceGrid.selectOnOROfForSmsTracking("On");
		deviceGrid.EnterTrackingPeriodicityForSms("15");
		deviceGrid.clickOnButton();
		androidJOB.JobCreatedMessage();
		deviceGrid.verifyJobIsCreated();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("CallAndSmsLogTracking");
		androidJOB.JobInitiatedOnDevice();
		deviceGrid.verifyTelecommanagementJob();
		deviceGrid.clickdataUsageEditBtn();
		deviceGrid.clickCallLogTracking();
		deviceGrid.jobVerifyCallLogOn();
		deviceGrid.clickSmsLogTracking();
		deviceGrid.jobVerifySmsLogOn();
		deviceGrid.closeTelecomScreen();
		deviceGrid.clickOnDeviceGridBody();
	}
	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException {
			if(ITestResult.FAILURE==result.getStatus())
			{
				deviceGrid.RefreshGrid();
			}
		}

}
