package Inbox_TestScripts;


import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class Inbox extends Initialization{
	@Test(priority='0',description="Sending mails from Nix")
	public void PreCondition() throws InterruptedException, IOException{
		Reporter.log("====1.Sending mails from Nix Inbox=====",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		commonmethdpage.enterSubInComposeMessage(6,"suremdm", "test");
	}
	@Test(priority='1',description="To Verify Clicking on Inbox section")
	public void ClickOnInbox() throws InterruptedException{
		Reporter.log("=====2.To Verify clicking on inbox=====",true);
		inboxpage.ClickOnInbox();

	}

	@Test(priority='2',description="To Verify if the first message if selected by default")
	public void VerifyifFirstMessageisSelectedByDefault() throws InterruptedException{
		Reporter.log("=====3.To Verify if the first message if selected by default=====",true);
		inboxpage.isTheFirstMessageSelectedByDefault();
	}

	@Test(priority='3',description="To Verify reply to the mail")
	public void VerifyReplyMail() throws InterruptedException{
		Reporter.log("=====4.To Verify reply to the mail=====",true);
		inboxpage.ClickOnMailReplyButton();
	}

	@Test(priority='4',description="To Verify warning message when no message body")
	public void VerifyWarningMessageWithoutMessageBody() throws InterruptedException{
		Reporter.log("=====5.To Verify warning message when no message body=====",true);

		inboxpage.ClickOnSendButton();
		inboxpage.warningWhenNoMessageBody();
	}

	@Test(priority='5',description="version support message for rich text")
	public void VerifyVersionMessageForRichText() throws InterruptedException{
		Reporter.log("=====6.To verify version support message for rich text=====",true);

		inboxpage.verifyRichTextFields();
		inboxpage.VerifyVersionSupportMessage();
		inboxpage.clikOnPlainText();

	}

	@Test(priority='6',description="To verify sending mail with Get read notification")
	public void VerifySendMessageWithGetReadNotification() throws InterruptedException{
		Reporter.log("=====7.To verify sending mail with Get read notification=====",true);
		inboxpage.EnterMessageBody("Hi");
		inboxpage.GetReadNotificaiton();
		inboxpage.ClickOnSendButton();
		inboxpage.NotificationMessageSent();
	} 

	@Test(priority='7',description="To verify sending mail with Force read message ")
	public void VerifySendMessageWithForceReadMessage() throws InterruptedException{
		Reporter.log("=====8.To verify sending mail with Force read message=====",true);
		//inboxpage.ClickOnInbox();//TO REMOVED
		inboxpage.ClickOnMailReplyButton();
		inboxpage.EnterMessageBody("Hi");
		inboxpage.ForceReadMessage();
		inboxpage.VerifyBuzzSupportVersion();
		inboxpage.ClickOnSendButton();
		inboxpage.NotificationMessageSent();
	}

	@Test(priority='8',description="To verify Cancel mark as unread")
	public void VerifyCancelMarkAsUnread() throws InterruptedException{
		Reporter.log("=====9.To verify Cancel mark as unread=====",true);
		inboxpage.ClickOnMarkAsUnread();
		inboxpage.Cancelling_MarkAsUnread();

	}

	@Test(priority='9',description="To verify mark as unread and warning message")
	public void VerifyMarkAsUnread() throws InterruptedException{
		Reporter.log("=====10.To verify mark as unread=====",true);
		inboxpage.ClickOnMarkAsUnread();
		inboxpage.WarningPopUpWhenMarkAsUnread();
		inboxpage.Yes_MarkAsUnread();
		inboxpage.ConfirmationNotificationOnMarkAsUnread();

	}

	@Test(priority='A',description="To verify the warning message while deleting a message when no message is selected")
	public void VerifyWarningNotificationOnDeleteWithoutSelectingAMessage() throws InterruptedException{
		Reporter.log("=====11.To verify the warning message while deleting a message when no message is selected====",true);
		inboxpage.ClickOnDeleteButton();  
		inboxpage.DeleteMessageWithoutSelectingAnyMessage();
	}

	@Test(priority='B',description="To verify the warning message and Yes button on the warning dialog of delete")
	public void Verify_Warning_Message_And_Yes_ButtonOntheWarningDialog() throws InterruptedException{
		Reporter.log("=====12.To verify the warning message and Yes button on the warning dialog of delete====",true);
		inboxpage.SelectFirstMessage();
		inboxpage.VerifySingleMessageDeleteWarningMessageOnThePopUp();   
	}

	@Test(priority='C',description="To verify Cancelling the warning dialog of delete")
	public void Verify_CancellingWarningDialogOfDelete() throws InterruptedException{
		Reporter.log("=====13.To verify Cancelling the warning dialog of delete=====",true);
		inboxpage.ClickOnNoButonOnDeleteWarningPopUp();
	}

	@Test(priority='D',description="To verify Deleting single message")
	public void Verify_DeletingSingleMessage() throws InterruptedException{
		Reporter.log("=====14.To verify Deleting single message====",true);
		inboxpage.ClickOnDeleteButton(); 
		inboxpage.ClickOnYesDeleteMessage();
	}


	@Test(priority='E',description="To the warning message on the pop up while deleting multiple messages")
	public void Verify_WarningMessageWhileDeletingMultipleMessages() throws InterruptedException{
		Reporter.log("=====15.To the warning message on the pop up while deleting multiple messages=====",true);
		inboxpage.SelectThirdMessage();
		inboxpage.VerifyMultipleMessagesDeleteWarningMessageOnThePopUp();
	}


	@Test(priority='F',description="To verify confirming multiple delete of messages")
	public void Verify_MultipleMessageDelete() throws InterruptedException{
		Reporter.log("=====16.To verify confirming multiple delete of messages=====",true);
		inboxpage.ClickOnYesDeleteMessage();
	}

	
}
