package QuickActionToolBar_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceLogs_MasterTestCase extends Initialization
{
       @Test(priority=1,description="Verify Logs in Quick Action Tool Bar.")
       public void VerifyDeviceLogsUI() throws Throwable
       {
    	   Reporter.log("1.Verify Logs in Quick Action Tool Bar.",true);
    	   commonmethdpage.SearchDevice(Config.DeviceName);
    	   quickactiontoolbarpage.ClickOnDeviceLogs();
    	   quickactiontoolbarpage.VerifyOfDeviceLogPage();
    	   quickactiontoolbarpage.ClickOnDeviceLogCloseBtn();
       }
       
       @Test(priority=2,description="Verify online /offline/device info updated logs should not be displayed in device activity logs.")
       public void VerifyOnlineOfflineLogsInDeviceActivityLogs() throws Throwable
       {
    	   Reporter.log("\n2.Verify online /offline/device info updated logs should not be displayed in device activity logs.",true);
    	   quickactiontoolbarpage.ClickOnDeviceLogs();
    	   quickactiontoolbarpage.VerifyAbsenceOfOnlineOfflineStatusInDeviceLogsTab();
    	   quickactiontoolbarpage.ClickOnDeviceLogCloseBtn();
       }
       
       @Test(priority=3,description="Verify checking logs of multiple devices.")
       public void VerifyLogsOfMultipleDevices() throws InterruptedException, AWTException
       {
    	   Reporter.log("\n3.Verify checking logs of multiple devices.",true);
    	   commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
    	   quickactiontoolbarpage.SelectingMultipleDevices();
    	   quickactiontoolbarpage.ClickOnMultipleDevicesLogs();
       }
}
