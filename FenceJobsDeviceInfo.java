package DeviceInfoPanel;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class FenceJobsDeviceInfo extends Initialization{
	
	@Test(priority =1, description = "1.Verify Creating time fence job by selecting Device Time.")
	public void CreateTimeFenceWindowsJob()throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException {

		
		
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TimeFenceEnterJob,"ENTER","EnterTimeFence");
		androidJOB.ClickOnGetReadNotification();
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TimeFenceExitJob,"EXIT","ExitTimeFence");
		androidJOB.ClickOnGetReadNotification();
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();

		
		
		
		}
	
	@Test(priority =2, description = "2.Verify Creating time fence job by selecting Device Time.")
	public void TC_10TimeFenceEditButtonWindows()throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException {
		Reporter.log("\n9.Verify Creating time fence job by selecting Device Time.",true);
		// WINDOWS

		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.WindowsDevice_Name);
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTimeWindows();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.SelectOptionalTime();
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1");
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2");
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();

		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.TimeFenceEnterJob);
		deviceinfopanelpage.SelectFenceJob(Config.TimeFenceEnterJob);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceEntered(false, Config.EmailForAlert);

		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob(Config.TimeFenceExitJob);
		deviceinfopanelpage.SelectFenceJob(Config.TimeFenceExitJob);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceExited(false, Config.EmailForAlert);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();

		commonmethdpage.ClickOnHomePage();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyTimeFenceStatus("ON");

		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchWindowsDeviceName();
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTimeWindows();
		deviceinfopanelpage.ClickOnEnableTimeFence(true);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		commonmethdpage.ClickOnDynamicRefresh();
		deviceinfopanelpage.UpdatedTimeFence();
	}
	
	@Test(priority = 3, description = "Verify Network Fence Status in device info panel for Android.")
	public void NetworkFenceCreateJob()throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n10.Verify Network Fence Status in device info panel for Android.",true);
		androidJOB.clickOnJobs();
		deviceinfopanelpage.ClickOnNewFolder();
		deviceinfopanelpage.EnterAndSaveNewFolder("AutomationDontDeleteJobFolder");
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnNotificationPolicyJob();
		deviceinfopanelpage.CheckDisableNotificationPolicy(false);
		deviceinfopanelpage.EnterAndSaveNotificationPolicyJob("AutomationDontDeleteNotificationPolicy");
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.InstallJob);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileInstallApplication();
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
		androidJOB.clickNewJob();
		accountsettingspage.clickOnAnyOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("AutomationDontDeleteTextMessage", Config.Customize_Sub,Config.Customize_Message);
		androidJOB.ClickOnGetReadNotification();
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.enterJobName(Config.JobNameToApplyMiscSettings);
		androidJOB.clickOnAdd();
		androidJOB.browseFile();
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 4, description = "Verify Network Fence Status in device info panel for Android.")
	public void NetworkFenceStatusShouldBeON()throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n10.Verify Network Fence Status in device info panel for Android.",true);

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnNetworkFence();
		deviceinfopanelpage.ClickOnEnableNetwork(false);
		deviceinfopanelpage.EnterWifiSSID(Config.WifiSSID);
		deviceinfopanelpage.ClickOnNetworkFenceEnteredTab();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceEntered(false, Config.EmailForAlert);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.EnterJobnameNetworkFenceOkButton("NetworkFenceJob");

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("NetworkFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJob("NetworkFenceJob", Config.DeploymentTime);
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice();
		deviceinfopanelpage.VerifyNetworkFenceStatus("ON");

		deviceinfopanelpage.clickOnEditNetworkFenceDeviceInfo();
		deviceinfopanelpage.ClickOnEnableNetwork(true);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.VerifyNetworkFenceStatus("OFF");
		deviceinfopanelpage.SleepFor40Seconds();

		
	}

	@Test(priority = 5, description = "Verify Network Fence job with Fence entered and Fence exited from device info panel.")
	public void NetworkFenceEnterExit()throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n11.Verify Network Fence job with Fence entered and Fence exited from device info panel.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");

     	commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.clickOnEditNetworkFenceDeviceInfo();
		deviceinfopanelpage.ClickOnEnableNetwork(false);
		deviceinfopanelpage.EnterWifiSSID(Config.WifiSSID);

		deviceinfopanelpage.ClickOnNetworkFenceEnteredTab();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceEntered(false,Config.NotifiedEmail_Misc);

		deviceinfopanelpage.ClickOnNetworkFenceExiedTab();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceExited(false,Config.NotifiedEmail_Misc);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();

		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyNetworkFenceStatus("ON");

		deviceinfopanelpage.VerifyNetworkFenceEntryAlertForDevice(Config.DeviceName, "Network fence", 600);
		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.ClickOnInbox();
		deviceinfopanelpage.InboxEntryMessageNetworkFence();
        deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Network fence");
        deviceinfopanelpage.VerifyFenceAlertFromEmail(Config.DeviceName, "entry", "Network fence");
        
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
//		deviceinfopanelpage.SelectDifferentNetwork(Config.WifiSSID,Config.WifiSSID2); 
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
//		deviceinfopanelpage.VerifyNetworkFenceExitAlertForDevice(Config.DeviceName, "Network fence", 600);
//		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
//		commonmethdpage.ClickOnHomePage();
//		accountsettingspage.ClickOnInbox();
//		deviceinfopanelpage.InboxExitMessageNetworkFence();
//       deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Network fence");
//		androidJOB.VerifyEmailNotification("https://www.mailinator.com",Config.NotifiedEmail_Misc);
        commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.clickOnEditNetworkFenceDeviceInfo();
		deviceinfopanelpage.ClickOnEnableNetwork(true);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.VerifyNetworkFenceStatus("OFF");
		deviceinfopanelpage.SleepFor40Seconds();


	}

	@Test(priority =6, description = "Verify Network fence job by adding multiple SSIDs.")
	public void NetworkFenceMultipleSSIDS()throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n12.Verify Network fence job by adding multiple SSIDs.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.clickOnEditNetworkFenceDeviceInfo();
		deviceinfopanelpage.ClickOnEnableNetwork(false);
		deviceinfopanelpage.EnterWifiSSID(Config.WifiSSID);
		deviceinfopanelpage.ClickOnNetWorkAddFence();
		deviceinfopanelpage.EnterWifiSSID2(Config.WifiSSID2);

		deviceinfopanelpage.ClickOnNetworkFenceEnteredTab();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceEntered(false, Config.NotifiedEmail_Misc);

		deviceinfopanelpage.ClickOnNetworkFenceExiedTab();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceExited(false, Config.NotifiedEmail_Misc);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();

		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyNetworkFenceStatus("ON");

		deviceinfopanelpage.VerifyNetworkFenceExitAlertForDevice(Config.DeviceName, "Network fence", 60);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessageNetworkFence();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Network fence");
		deviceinfopanelpage.VerifyFenceAlertFromEmail(Config.DeviceName, "exit", "Network fence");


		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.clickOnEditNetworkFenceDeviceInfo();
		deviceinfopanelpage.ClickOnEnableNetwork(true);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.VerifyNetworkFenceStatus("OFF");
		deviceinfopanelpage.SleepFor40Seconds();

	}

	@Test(priority =7, description = "Network Fence - Verify creating Network fence job adding delay in Fence entered and fence exited.")
	public void NetworkFenceVerifycreatingNetworkfencejobaddingdelayinFenceenteredandfenceexited()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		Reporter.log("\n13.Network Fence - Verify creating Network fence job adding delay in Fence entered and fence exited.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");

		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.clickOnEditNetworkFenceDeviceInfo();
		deviceinfopanelpage.ClickOnEnableNetwork(false);
		deviceinfopanelpage.EnterWifiSSID(Config.WifiSSID);

		deviceinfopanelpage.ClickOnNetworkFenceEnteredTab();
		deviceinfopanelpage.EnterJobInDelay();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();

		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceEntered(false, Config.NotifiedEmail_Misc);

		deviceinfopanelpage.ClickOnNetworkFenceExiedTab();
		deviceinfopanelpage.EnterJobOutDelay();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage"); // Config.InstallJob
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceExited(false, Config.NotifiedEmail_Misc);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyNetworkFenceStatus("ON");

		deviceinfopanelpage.VerifyNetworkFenceExitAlertForDevice(Config.DeviceName, "Network fence", 60);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessageNetworkFence();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Network fence");
		deviceinfopanelpage.VerifyFenceAlertFromEmail(Config.DeviceName, "exit", "Network fence");

		commonmethdpage.ClickOnHomePage();

	}

	@Test(priority =8, description = "Network Fence - verify disable enable Network fencing")
	public void NetworkFencverifydisableenableNetworkfencing()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		Reporter.log("\n14.Network Fence - verify disable enable Network fencing",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.clickOnEditNetworkFenceDeviceInfo();
		deviceinfopanelpage.ClickOnEnableNetwork(true);
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.VerifyNetworkFenceStatus("OFF");
		deviceinfopanelpage.SleepFor40Seconds();
	}
	@Test(priority =9, description = "Verify Creating time fence job by selecting Device Time.")
	public void TC_9TimeFenceEditButtonAndroid()throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException {
		Reporter.log("\n30.Verify Creating time fence job by selecting Device Time.",true);
		// ANDROID

		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTime();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.SelectOptionalTime();
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1");
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2");
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();

		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceEntered(false, Config.EmailForAlert);

		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceExited(false, Config.EmailForAlert);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");

		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyTimeFenceStatus("ON");
		deviceinfopanelpage.VerifyFenceExitAlertForDevice(Config.DeviceName, "Time fence", 500);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Time fence");

		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTimeWindows();
		deviceinfopanelpage.ClickOnEnableTimeFence(true);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		commonmethdpage.ClickOnDynamicRefresh();
		deviceinfopanelpage.AndroidUpdatedTimeFence();
	}
	// Android and Windows

	@Test(priority = 'A', description = "Time Fence - Verify Delete fence in Time fence job")
	public void TimeFenceVerifyDeletefenceinTimefencejob()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		Reporter.log("\n31.Time Fence - Verify Delete fence in Time fence job",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTime();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);

		deviceinfopanelpage.ClickOnFenceDeleteButton();
		deviceinfopanelpage.ClickOnFenceDeleteConfirmationYesButton();
		deviceinfopanelpage.VerifyDeletedFence();
		deviceinfopanelpage.ClickOnCloseButtonOfTimeFence();

		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTimeWindows();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);

		deviceinfopanelpage.ClickOnFenceDeleteButton();
		deviceinfopanelpage.ClickOnFenceDeleteConfirmationYesButton();
		deviceinfopanelpage.VerifyDeletedFence();
		deviceinfopanelpage.ClickOnCloseButtonOfTimeFence();
	}

	@Test(priority = 'B', description = "Verify Disable enable Time Fence")
	public void TimeFenceVerifyDisableenableTimeFence()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		Reporter.log("\n32.Verify Disable enable Time Fence",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTime();
		deviceinfopanelpage.ClickOnEnableTimeFence(true);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.VerifyTimeFenceStatus("OFF");

		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyCreatingTimeFenceJob_selectingDeviceTimeWindows();
		deviceinfopanelpage.ClickOnEnableTimeFence(true);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.VerifyTimeFenceStatus("OFF");

	}

}
