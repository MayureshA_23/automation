package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class SingleAppModeProfile extends Initialization{
	


	@Test(priority='1',description="1.To verify clicking on the Single App Mode Profile option")
	public void VerifyClickingOnWebContentFilter() throws InterruptedException{
		
	    Reporter.log("To verify clicking on the Single App Mode Profile option");
		profilesAndroid.ClickOnProfile();
		profilesiOS.clickOniOSOption();
		profilesiOS.AddProfile();
		profilesiOS.ClicOnSingleAppModeProfile();
		} 
	/*
	@Test(priority='2',description="1.To verify summary of Lock Down Application")
	public void VerifySummaryofLockDownApplication() throws InterruptedException
	{    
		Reporter.log("To verify summary of Lock Down Application");
		profilesiOS.LockDownApplicationSummary();
	}
	*/
	@Test(priority='3',description="1.To Verify warning message Saving SAM profile without name")
	public void VerifyWarningMessageOnSavingSAMProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving SAM profile without name=========");
		profilesiOS.ClickOnConfigureButtonSAMProfile();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='4',description="1.To Verify Saving SAM profile ")
	public void VerifySavingSAMProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======VerifyWarningMessageOnSavingABrandingProfileEnteringRequiredFields=========");
		profilesiOS.EnterSAMProfileName();
		profilesiOS.VerifyChoosingLockDownApplication();
		//profilesiOS.ClickOnSingleAppMode(); // in case want to use periodic launch-below code
		profilesiOS.ClickOnLaunchPeriodically();
		profilesiOS.ClearLaunchPeriodTime();
		profilesiOS.EnterLaunchPeriodTime();
		profilesiOS.ClickOnSavebutton();
	    profilesAndroid.NotificationOnProfileCreated();

}
}
