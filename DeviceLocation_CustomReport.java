package CustomReportsScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceLocation_CustomReport extends Initialization {
	@Test(priority = '0', description = "Enable Device Location")
	public void EnablingDeviceLocation()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException { // Enable
																											// Device
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.CheckingLocationTrackingStatus();
		quickactiontoolbarpage.ClickOnLocate_UM();
		quickactiontoolbarpage.ClickingOnTurnOnButton();
		quickactiontoolbarpage.TurningOnLocationTracking("2");
		quickactiontoolbarpage.WaitingForUpdate(180);
	}

	@Test(priority = '1', description = "Verify generating Device Location Custom report with filters for 'Current Date' & Home group")
	public void VerifyingDeviceLocationReport_TC_RE_123() throws InterruptedException {
		Reporter.log("\nVerify generating Device Location Custom report with filters for 'Current Date' & Home group",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Location");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
		customReports.EnterCustomReportsNameDescription_DeviceDetails(
				"DevLocation CRep with filter for greater than or equal to opeartor", "test");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceLocation);
		customReports.SelectValueFromColumn(Config.SelectLongitude);
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield(Config.EnterSomeLongitudeValue);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevLocation CRep with filter for greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection(
				"DevLocation CRep with filter for greater than or equal to opeartor");
		customReports.SelectTodayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevLocation CRep with filter for greater than or equal to opeartor");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevLocation CRep with filter for greater than or equal to opeartor");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingCurrentDateInDeviceLocationReport();
		customReports.VarifyingLongitudeValueInDeviceLocationReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '2', description = "Verify generating Device Location Custom report without filters")
	public void VerifyingDeviceLocationReport_TC_RE_124() throws InterruptedException {
		Reporter.log("\nVerify generating Device Location Custom report without filters", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Location");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevLocation CRep without filter", "test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevLocation CRep without filter");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevLocation CRep without filter");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevLocation CRep without filter");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.VarifyingCurrentDateInDeviceLocationReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '3', description = "Verify generating Device Location Custom report with filters for '1week Date'")
	public void VerifyingDeviceLocation_TC_RE_125() throws InterruptedException {
		Reporter.log("\nVerify generating Device Location Custom report with filters for '1week Date'", true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Location");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevLocation CRep with filter for one week Date",
				"test");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceLocation);
		customReports.SelectValueFromColumn("Datetime");
		customReports.SelectOneWeekDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevLocation CRep with filter for one week Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevLocation CRep with filter for one week Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevLocation CRep with filter for one week Date");
		reportsPage.WindowHandle();
		/* customReports.CheckingDeviceName(); */
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingOneWeekDateInDevLocationReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '4', description = "Verify generating Device Location Custom report with filters for 'Yesterday' date  wrt specified address")
	public void VerifyingDeviceLocationReport_TC_RE_126() throws InterruptedException {
		Reporter.log(
				"\nVerify generating Device Location Custom report with filters for 'Yesterday' date  wrt specified address",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Location");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
		customReports.EnterCustomReportsNameDescription_DeviceDetails(
				"DevLocation CRep with filter for Yesterday and like operator", "test");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceLocation);
		customReports.SelectValueFromColumn(Config.SelectAddress);
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.EnterLocationAsAMRtechPark);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevLocation CRep with filter for Yesterday and like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevLocation CRep with filter for Yesterday and like operator");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevLocation CRep with filter for Yesterday and like operator");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingYesterDateInViewReport_DeviceLocation();
		customReports.VarifyingAddressInDeviceLocationReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '5', description = "Verify Sort by and Group by for Device Location")
	public void VerifyingDeviceLocationReport_TC_RE_127() throws InterruptedException {
		Reporter.log("\nVerify Sort by and Group by for Device Location", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Location");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevLocation CRep based on group by condition",
				"test");
		customReports.Selectvaluefromsortbydropdown(Config.SelectAddress);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevLocation CRep based on group by condition");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("DevLocation CRep based on group by condition");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevLocation CRep based on group by condition");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevLocation CRep based on group by condition");
		reportsPage.WindowHandle();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				String FailedWindow = Initialization.driver.getWindowHandle();
				if (FailedWindow.equals(customReports.PARENTWINDOW)) {
					commonmethdpage.ClickOnHomePage();
				} else {
					reportsPage.SwitchBackWindow();
				}
			} catch (Exception e) {

			}
		}
	}
}
