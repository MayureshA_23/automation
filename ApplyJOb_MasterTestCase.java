package QuickActionToolBar_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ApplyJOb_MasterTestCase extends Initialization
{
	//Multiple Devices Should be Enrolled
    @Test(priority=1,description="Verify Apply on the device.")
    public void VerifyApplyJobPage_UI() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	Reporter.log("1.Verify Apply on the device.",true);
    	commonmethdpage.SearchDevice(Config.MultipleDevices1);
		quickactiontoolbarpage.ClickOnApplyJob();
		quickactiontoolbarpage.VerifyOfApplyJobPage();
		quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
	
    }
    
    @Test(priority=2,invocationCount=2,description="Creating Jobs")
    public void CreatingJobs() throws InterruptedException
    {
    	Reporter.log("\n2.Creating Jobs",true);
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
    }
   @Test(priority=2,description="Verify applying Jobs/Profiles on the device And Verify after applying job on the device job get deployed on the device. And Verify apply Job/Profile to single device. ")
    public void VerifyApplyingJobOnDevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
	   Reporter.log("\n3.Verify applying Jobs/Profiles on the device And Verify after applying job on the device job get deployed on the device. And Verify apply Job/Profile to single device. ",true);
	   commonmethdpage.ClickOnHomePage();
   	   commonmethdpage.SearchDevice(Config.MultipleDevices1);
		quickactiontoolbarpage.ClickOnApplyJob();
		androidJOB.SearchField(Config.TextMessageJobname);
		androidJOB.clickonJobQueue();
		accountsettingspage.ReadingConsoleMessageForJobDeployment(Config.TextMessageJobname,Config.MultipleDevices1);		
		androidJOB.ClickOnJobQueueCloseButton();
    }
   @Test(priority=3,description="Verify applying multiple jobs to single device.")
   public void VerifyApplyingMultipleJobsOnSingleDevice() throws InterruptedException, AWTException, EncryptedDocumentException, InvalidFormatException, IOException
   {
	   Reporter.log("\n4.Verify applying multiple jobs to single device.",true);
	   commonmethdpage.SearchDevice(Config.MultipleDevices1);   //should be removed
	   quickactiontoolbarpage.ClickOnApplyJob();
	   androidJOB.SearchJobsInApplyJob(Config.TextMessageJobname);
	   androidJOB.SelectingMultipleJobs(Config.TextMessageJobname);
	   androidJOB.CheckStatusOfappliedInstalledJob(Config.TextMessageJobname,120);
	   androidJOB.ClickOnJobQueueCloseButton();
	 
   }
   @Test(priority=4,description="Verify apply Job/Profile to multiple device.")
   public void VerifyApplyingJobToMultipleDevices() throws InterruptedException, AWTException, EncryptedDocumentException, InvalidFormatException, IOException
   {
	   Reporter.log("\n5.Verify apply Job/Profile to multiple device.",true);
	   commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
	   quickactiontoolbarpage.SelectingMultipleDevices();
	   quickactiontoolbarpage.ClickOnApplyJob();
	   androidJOB.SearchField(Config.TextMessageJobname);
	   /*commonmethdpage.SearchDevice(Config.MultipleDevices1);
	   androidJOB.CheckStatusOfappliedInstalledJob(Config.TextMessageJobname,120);
	   androidJOB.ClickOnJobQueueCloseButton(); 
	   commonmethdpage.SearchDevice(Config.MultipleDevices1);
	   androidJOB.CheckStatusOfappliedInstalledJob(Config.TextMessageJobname,120);
	   androidJOB.ClickOnJobQueueCloseButton(); */
	  
   }

    @Test(priority=5,description="Verify applying multiple jobs to multiple devices.")
    public void VerifyApplyingMultipleJobsToMultileDevices() throws AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	Reporter.log("\n6.Verify applying multiple jobs to multiple devices.",true);
       commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
 	   quickactiontoolbarpage.SelectingMultipleDevices();
 	   quickactiontoolbarpage.ClickOnApplyJob();
 	   androidJOB.SearchJobsInApplyJob(Config.TextMessageJobname);
 	   androidJOB.SelectingMultipleJobs(Config.TextMessageJobname);
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   androidJOB.CheckStatusOfappliedInstalledJob(Config.TextMessageJobname,120);
	   androidJOB.ClickOnJobQueueCloseButton();
	  commonmethdpage.SearchDevice(Config.MultipleDevices1);
	  androidJOB.CheckStatusOfappliedInstalledJob(Config.TextMessageJobname,120);
	  androidJOB.ClickOnJobQueueCloseButton();
	  accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,Config.DeviceName);
    }
    
    @Test(priority=6,description="Verify applying Install/File transfer job on the device only when device is connected to Wifi")
    public void  VerifyApplyFileTransferJob_Wifi() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	Reporter.log("\n7.Verify applying Install/File transfer job on the device only when device is connected to Wifi",true);
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.FileTransferJobName);
		androidJOB.clickOnAdd();
		androidJOB.verifyFileTransferPrompt();
		androidJOB.browseFile();
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.verifyFileTransferJobCreation("0FileTransfer");
		androidJOB.UploadcompleteStatus();
		commonmethdpage.ClickOnHomePage();
    	commonmethdpage.SearchDevice(Config.DeviceName);
    	quickactiontoolbarpage.ClickOnApplyJob();
    	androidJOB.SearchJobsInApplyJob(Config.FileTransferJobName);
    	androidJOB.SelectingNetworkType();
		androidJOB.ClickOnApplyJobOkButton();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
		}
	
	@Test(priority=7,description="Verify apply Job/Profile on the device if Job/Profile present in folder.")
	public void VerifyApplyJobPresentInsideFolder() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n8.Verify apply Job/Profile on the device if Job/Profile present in folder.",true);
		androidJOB.clickOnJobs();
		androidJOB.ClickOnNewFolder();
		androidJOB.NamingFolder(Config.JobFolderName);
		androidJOB.SearchingJobFolder(Config.JobFolderName);
		androidJOB.DoubleClickOnFolder("job_new_job",Config.JobFolderName);
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.BackFromFolder();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		quickactiontoolbarpage.ClickOnApplyJob();
		 androidJOB.SearchJobsInApplyJob(Config.JobFolderName);
		androidJOB.DoubleClickOnFolder("scheduleJob",Config.JobFolderName);
		androidJOB.SearchField(Config.TextMessageJobname);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,Config.DeviceName);
	}
	
	@Test(priority=8,description="Verify Changing status UI in job apply page")
	public void VerifyChargingStatusUIInJobApplyPage() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n9.Verify Changing status UI in job apply page",true);
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
    	quickactiontoolbarpage.ClickOnApplyJob();
    	androidJOB.SearchJobsInApplyJob(Config.FileTransferJobName);
		quickactiontoolbarpage.VerifyChargingUIInJobApplyJob();
		quickactiontoolbarpage.VerifyChargingStateDropDownOptions();
		quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
	}
}
