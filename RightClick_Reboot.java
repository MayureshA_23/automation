package RightClick;
import java.io.IOException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;


public class RightClick_Reboot extends Initialization {

	@Test(priority='0',description="1.To Verify right reboot of the device")
	public void VerifyRightClickReboot() throws Exception {
		Reporter.log("1.To Verify right reboot of the device",true);
		commonmethdpage.SearchDeviceInconsole();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnReboot();
		dynamicjobspage.ClickOnRebootDeviceYesBtn();
		dynamicjobspage.VerifyOfRebootActivityLogs();
	}
}
