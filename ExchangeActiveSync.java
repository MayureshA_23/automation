package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class ExchangeActiveSync extends Initialization{
	
	@Test(priority='1',description="1.To verify clicking on Exchange Active Sync") 
    public void VerifyClickingOnExchangeActiveSync() throws InterruptedException {
    Reporter.log("To verify clicking on VPN");
    profilesAndroid.ClickOnProfile();
	profilesiOS.clickOniOSOption();
	profilesiOS.AddProfile();
	profilesiOS.ClickOnExchangeActiveSync();
}
	@Test(priority='2',description="1.To Verify warning message Saving Exchange ActiveSynce without Profile Name")
	public void VerifyWarningMessageOnSavingExchangeActiveSyncWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving Exchange ActiveSynce without Profile Name========");
		profilesiOS.ClickOnConfigureButtonExchangeActiveSyncProfile();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='3',description="1.To Verify warning message Saving a Exchange Active Sync Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingExchangeActiveSyncProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Exchange Active Sync Profile without entering all the fields=========");
		profilesiOS.EnterExchangeActiveSyncProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	@Test(priority='4',description="1.To Verify parameters of ExchangeActiveSync profile")
	public void VerifyParametersfExchangeActiveSynceProfile() throws InterruptedException{
		Reporter.log("=======To Verify parameters of ExchangeActiveSync profile=========");
		profilesiOS.VerifyExchangeActiveSyncParameters();
	}
	

	@Test(priority='5',description="1.To Verify warning message without uploading certificate and password")
	public void VerifyErrorOnSavingWhenCertificateIsNotUploaded() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify warning message without uploading certificate and password=========");
		profilesiOS.ClickOnPlusButton_ChooseAuthenticationCertificate();
		profilesAndroid.ClickOnAddButtonOnCertWindow();
	    profilesiOS.CertificateErrorWhenCertificateIsNotUploaded();
	    profilesiOS.CloseCertificatePopUp();
	}
	
	
	@Test(priority='6',description="To verify saving the Exchange Active Sync Profile")
	public void Verify_SavingExchangeActiveSyncProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("To verify saving the Exchange Active Sync Profile",true);
		profilesiOS.EnterExchangeactiveSyncDetails();
		profilesiOS.ClickOnSavebutton();
	    profilesAndroid.NotificationOnProfileCreated();
	}
}
	
	
	
	
	


