package QuickActionToolBar_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class JobQueue_Groups extends Initialization
{
    @Test(priority=0,description="verify Group job Queue")
	public void VerifyGroupJbQueue() throws InterruptedException
	{
    	Reporter.log("1.verify Group job Queue",true);
		MacOSDeviceInfo.ClickOnNewGroup();
		MacOSDeviceInfo.SendingGroupName(Config.GroupName);
		quickactiontoolbarpage.ClickOnGroups("Home");
		quickactiontoolbarpage.ClickOnGroups(Config.GroupName);
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.VerifyGroupJobQueueUI();
		quickactiontoolbarpage.ClickOnGroupJobQueueCloseButton();
	}

	@Test(priority=1,description="verify pending column in the Group job queue")
	public void VerifyPendingColumnInGroupJobQueue() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{		
		Reporter.log("\n2.verify pending column in the Group job queue",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.InstallJObName);   
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileInstallApplication("./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\File1.exe");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		MacOSDeviceInfo.ClickOnAllDeviceTab();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.ClickingOnMoveToGroup(quickactiontoolbarpage.DeviceName);
		quickactiontoolbarpage.MovingDeviceToGroup(Config.GroupName,Config.DeviceName);
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.VerifyJobInsideTab(Config.InstallJObName,"Pending");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("Completed",12);
		quickactiontoolbarpage.VerifyJobInsideTab(Config.InstallJObName,"Completed");
	}
	
	@Test(priority=2,description="verify Retry button in the pending column ")
	public void VerifyRetryButton_GroupJobQueue() throws InterruptedException, IOException
	{
		Reporter.log("\n3.verify Retry button in the pending column ",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.VerifyJobInsideTab(Config.InstallJObName,"Pending");
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.ClickOnRetryButtonInGroupJobQueue();
		quickactiontoolbarpage.ClickOnRemoveButton_GroupJobQueue();
		quickactiontoolbarpage.ClickOnGroupJobQueueCloseButton();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
	}
	
	@Test(priority=3,description="verify success column in the group job queue ")
	public void VerifySuccessColumnInGroupJobQueue() throws InterruptedException
	{
		Reporter.log("\n4verify success column in the group job queue ",true);
		quickactiontoolbarpage.RightClickOnGroup(Config.GroupName);
		quickactiontoolbarpage.ClickOnGroupJobHistory_RightClick();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("Completed",12);
		quickactiontoolbarpage.VerifyJobInsideTab(Config.InstallJObName,"Completed");
	}
	
	@Test(priority=4,description="Verify Failed column in the Group job queue")
	public void VerifyFailedColumnInGroupjobQueue() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n5.Verify Failed column in the Group job queue",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		quickactiontoolbarpage.CreatingSureFoxSettingsJob(quickactiontoolbarpage.ErrorjobName);
		commonmethdpage.ClickOnHomePage();
		MacOSDeviceInfo.ClickOnAllDeviceTab();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.ClickingOnMoveToGroup(quickactiontoolbarpage.DeviceName);
		quickactiontoolbarpage.MovingDeviceToGroup(Config.GroupName,Config.DeviceName);
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		quickactiontoolbarpage.RightClickOnGroup(Config.GroupName);
		quickactiontoolbarpage.ClickOnGroupJobHistory_RightClick();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("Failed",5);
		quickactiontoolbarpage.VerifyJobInsideTab(quickactiontoolbarpage.ErrorjobName,"Failed");
	}
	
	@Test(priority=5,description="verify Retry button in the Failed column ")
	public void verifyRetrybuttonInFailedInGroupJobQueue() throws InterruptedException, IOException
	{
		Reporter.log("\n6.verify Retry button in the Failed column ",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		quickactiontoolbarpage.ClearingConsoleLogs();
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("Failed",5);
		quickactiontoolbarpage.ClickOnRetryButtonInGroupJobQueue();
		quickactiontoolbarpage.ClickOnGroupJobQueueCloseButton();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("Pending",3);
		quickactiontoolbarpage.VerifyJobInsideTab(quickactiontoolbarpage.ErrorjobName,"Pending");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		commonmethdpage.ClickOnAllDevicesButton();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		
	}
	
	@Test(priority=7,description="Verify Applying job to the parent group when subgroups are present.")
	public void VerifyapplyingJobToParentGroupWhenSubGroupsArePresent() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n7.Verify Applying job to the parent group when subgroups are present.",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		groupspage.ClickOnGroup(Config.GroupName);
		MacOSDeviceInfo.ClickOnNewGroup();
		MacOSDeviceInfo.SendingGroupName(Config.SubGroupName);
		MacOSDeviceInfo.ClickOnAllDeviceTab();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.ClickingOnMoveToGroup(quickactiontoolbarpage.DeviceName);
		quickactiontoolbarpage.MovingDeviceToGroup(Config.SubGroupName,Config.DeviceName);
		groupspage.ClickOnGroup(Config.GroupName);
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();
		quickactiontoolbarpage.ClickOnGroups(Config.SubGroupName);
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.VerifyAppliedJobInsideSubGroupjobQueue(Config.InstallJObName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
	}
	
	@Test(priority='8',description="Verify Remove Job in the Group Job Queue in pending,InProgress, Failed Status tabs.")
	public void RemoveButtonInGroupJobQueue() throws InterruptedException
	{
		Reporter.log("\n8.Verify Remove Job in the Group Job Queue in pending,InProgress, Failed Status tabs.",true);
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		quickactiontoolbarpage.ClickOnGroupJobQueueButton();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("In Progress",5);
		quickactiontoolbarpage.ClickOnRemoveButton_GroupJobQueue();
		quickactiontoolbarpage.ClickOnJobTabsInsidejobHistoryForGroups("Failed",5);
		quickactiontoolbarpage.ClickOnRemoveButton_GroupJobQueue();
		quickactiontoolbarpage.ClickOnGroupJobQueueCloseButton();
		
	}
	
	
	
}
