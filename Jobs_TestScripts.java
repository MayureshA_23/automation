package NewUI_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Jobs_TestScripts extends Initialization{
	
	@Test(priority=0,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnEachPlatform() throws InterruptedException
	{
	  //android job
		Reporter.log("=====1.Android TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Android TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
		@Test(priority=1,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOniOSPlattform() throws InterruptedException
	{
		//iOS job
		Reporter.log("=====2.iOS TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOniOSOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("iOS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=2,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
		public void CreateOneJobOnWindowsOSPlatform() throws InterruptedException
		{
		//WindowsOS job
		Reporter.log("=====3.Windows TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Windows TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		}
	@Test(priority=3,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnWindowsSEPlatform() throws InterruptedException
	{
		//WindowsSE OS Job
		Reporter.log("=====4.WindowsSE TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsSEOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("WindowsSE TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=4,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnWindowsMobilePlatform() throws InterruptedException
	{
		//WindowsMobile OS Job
		Reporter.log("=====5.WindowsMobile TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsMobileOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("WindowsMobile TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=5,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnAnyOSPlatform() throws InterruptedException
	{
		//AnyOS Job
		Reporter.log("=====6.AnyOS TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAnyOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("AnyOS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}	
	@Test(priority=6,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnAndroidwearPlatform() throws InterruptedException
	{
		//Android wear OS Job
		Reporter.log("=====7.Android wear OS TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidWearOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Android wear OS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=7,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnLinuxPlatform() throws InterruptedException
	{
		//Linux OS Job
		Reporter.log("=====8.LinuxOS TextMessageJob=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnLinuxOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("LinuxOS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	
	@Test(priority=8,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnAndroidVRPlatform() throws InterruptedException
	{
		//Android VR OS
		Reporter.log("=====9.Android VR OS Install Application job=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidVR();
		androidJOB.ClickOnInstallApplication();
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterJobName(Config.EnterJobWithApkUrl);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL(Config.EnterApkUrl);
		androidJOB.EnterDevicePath("Home");
		newUIScriptsPage.clickOkBtnOnAndroidVRInstallJob();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
		
	}
	@Test(priority=9,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnmacOSPlatform() throws InterruptedException
	{
		//macOS 
		Reporter.log("=====10.macOS Reboot Job=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnmacOSR();
		androidJOB.clickOnReboot();
		androidJOB.enterRebootJobName("macOS Reboot Job");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority='A',description="Jobs - Verify Pagination in Jobs Page")
	public void VerifyOfPaginationInJobsPage() throws InterruptedException
	{
		Reporter.log("=====11. Verify Pagination in Jobs Page=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.VerifyOfPagination();
	}
	@Test(priority='B',description="Jobs - verify Search option in jobs page.")
	public void VerifyOfSeachOptionInJobsPAge() throws InterruptedException
	{
		Reporter.log("=====12.verify Search option in jobs page.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Text12345","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		newUIScriptsPage.SearchJobInList("Text12345");
		androidJOB.VerifyOfSearchOptionInJobsPage("Text12345");
	}
	@Test(priority='C',description="Jobs - Verify creating New Folder in jobs.")
	public void VerifyOfCreatingNewFolderInJobs() throws InterruptedException
	{
		Reporter.log("=====13.Verify creating New Folder in jobs.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.ClickOnNewFolder();
		androidJOB.NamingFolder("@Folder");
		androidJOB.VerifyOfFolderCreatedPopup();
		newUIScriptsPage.SearchJobInList("@Folder");
		androidJOB.VerifyOfSearchOptionInJobsPage("@Folder");
	}
	@Test(priority='D',description="Jobs - Verify Move to Folder in Jobs page.")
	public void VerifyOfMoveToFolderInJobs() throws InterruptedException
	{
		Reporter.log("=====14.Verify Move to Folder in Jobs page.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname,"TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.ClickOnNewFolder();
		androidJOB.NamingFolder("0MoveToFolder");
		newUIScriptsPage.SearchJobInList(Config.TextMessageJobname);
		androidJOB.clickOnMoveToFolder();
		newUIScriptsPage.SelectFolderInMoveTo("0MoveToFolder");
		newUIScriptsPage.VerifyOfMoveJobToFolderPopUp(Config.TextMessageJobname,"0MoveToFolder");
	}
	@Test(priority='E',description="Jobs - verify Copy in jobs page.")
	public void VerifyOfCopyInJobsPage() throws InterruptedException
	{
		Reporter.log("=====15.verify Copy in jobs page.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Copy @TextMessage","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		newUIScriptsPage.SearchJobInList("Copy @TextMessage");
		androidJOB.clickOnCopyBtn();
		androidJOB.SelectFolderInCopyJobPage("0MoveToFolder");
		androidJOB.VerifyOfCopiedJobPopUp();
		newUIScriptsPage.SearchJobInList("copy");
		androidJOB.VerifyOnCopiedJob("Copy @TextMessage");
	}
	@Test(priority='F',description="Jobs - Verify applying copied job to device")
	public void ApplyingCopiedJobToDevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====16.Verify applying copied job to device=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("@Message","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		newUIScriptsPage.SearchJobInList("@Message");
		androidJOB.clickOnCopyBtn();
		androidJOB.SelectFolderInCopyJobPage("0MoveToFolder");
		androidJOB.VerifyOfCopiedJobPopUp();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("@Message_copy");
		newUIScriptsPage.JobInitiatedOnDevice();
		newUIScriptsPage.ReadingConsoleMessageForJobDeployment("@Message_copy",Config.DeviceName);
	}
	@Test(priority='G',description="Jobs - Verify delete in jobs page")
	public void VerifyOfDeleteJobInJobsPage() throws InterruptedException
	{
		Reporter.log("=====17. Verify delete in jobs page=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("@TextMsg","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		newUIScriptsPage.SearchJobInList("@TextMsg");
		androidJOB.deleteCreatedJob();
		androidJOB.JobDeletedMessage();
		newUIScriptsPage.SearchJobInList("@Folder");
		androidJOB.VerifyOfSearchOptionInJobsPage("@Folder");
	}
}
