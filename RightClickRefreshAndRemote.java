package RightClick;


import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class RightClickRefreshAndRemote extends Initialization {

	@Test(priority='0',description="1.To verify RefreshJob Right click")
    public void VerifyRefreshJobRightClick() throws Exception {
		Reporter.log("\n1.To verify RefreshJob Right click",true);
		commonmethdpage.SearchDevice();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnRefresh();
	//	rightclickDevicegrid.VerifyRefresJobDeployedSuccesfully();
	}
	
	@Test(priority='1',description="2.To Verify RemoteSupport of Device")
	public void VerifyClickingRemoteSupport() throws Exception {
		Reporter.log("\n2.2.To Verify RemoteSupport of Device",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.VerifyClickOnRemote();
		rightclickDevicegrid.windowhandles();
		rightclickDevicegrid.SwitchToRemoteWindow();
	}
	
	@Test(priority='2',description="3.To verify RemoteLaunched")
	public void VerifyRemoteLaunched() throws InterruptedException {
		Reporter.log("\n3.To verify RemoteLaunched",true);
		rightclickDevicegrid.VerifyOfRemoteSupportLaunched();
		rightclickDevicegrid.DeviceNameVerification(Config.DeviceName);
		rightclickDevicegrid.resumeButtonDisplayed();
		rightclickDevicegrid.SwitchToPrentwindow();
	}
	
	@Test(priority='4',description="4.To Verify Deleting device Rightclick")
	public void VerifyDeletingDevice() throws Exception {
		Reporter.log("\n4.To Verify Deleting device Rightclick",true);
		rightclickDevicegrid.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnDelete();
		rightclickDevicegrid.WarningMessageOnDeviceDelete(Config.DeviceName);
		rightclickDevicegrid.DeleteDeviceFromRightClick();
		rightclickDevicegrid.VerifyAfterDeletingDevice();
	}
	
	@Test(priority='5',description="5.To Verify device Deleted device side")
	public void VerifyDeviceDeletedOnDevce() throws Exception {
		Reporter.log("\n5.To Verify device Deleted device side",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		rightclickDevicegrid.LaunchNix();
		rightclickDevicegrid.VerifyDeviceStopped();
	}
 
	
	
	
	
	
	
}
