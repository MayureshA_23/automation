package DynamicJobs_TestScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class Apps extends Initialization{


	@Test(priority='1',description="1.To Verify Dynamic Apps Job Popup UI")
	public void VerifyDynamicAppsJobUI() throws InterruptedException, Throwable{
		Reporter.log("\n1.To Verify Dynamic Apps Job Popup UI",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnApps();
	    dynamicjobspage.VerifyOfDynamicAppsUI(Config.DeviceName);
	   // dynamicjobspage.ClickOnDownloadedAppsTab();
	    
	    System.out.println("");
	}
	

	@Test(priority='2',description="2. To Verify Apps present in Downloaded Apps Section is with type 'Installed'")
	public void VerifyTheDownloadedApps_TC_DJ_43() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Apps present in Downloaded Apps Section is with type 'Installed'",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnApps();
	    dynamicjobspage.ClickOnDownloadedAppsTab();
    	dynamicjobspage.VerifyDownloadedApps();
		dynamicjobspage.VerifyOfDownloadApps_type();
		System.out.println("");
	}
	
	
	@Test(priority='3',description="3.To Verify Downloaded Apps Section when master checkbox is checked/unchecked")
	public void DownloadAppsMasterCheckboxCheckedUnchecked_TC_DJ_45_TC_DJ_46() throws InterruptedException, Throwable{
		Reporter.log("\n3.To Verify Downloaded Apps Section when master checkbox is checked/unchecked",true);
	
	    dynamicjobspage.ClickOnDownloadedAppsTab();
        dynamicjobspage.VerifyOfDownloadApps_MasterCheckbox();
		 System.out.println("");
	}
	
	
	@Test(priority='4',description="4.To Verify Apps present in System Apps Section is with type 'System'")
	public void VerifyAppsPresentSystemAppsSectionTypeSystem() throws InterruptedException, Throwable{
		Reporter.log("\n4.To Verify Apps present in System Apps Section is with type 'System'",true);
		
		dynamicjobspage.ClickOnSystemAppsTab();
		dynamicjobspage.VerifyOfSystemApps_type();
		 System.out.println("");
	}
	
	
	
	
	@Test(priority='5',description="5.To Verify Apps present in All Apps Section is with type 'Installed' or 'System'")
	public void VerifyAppsInAllAppsTypeInstalledAndSystem() throws InterruptedException, Throwable{
		Reporter.log("\n5.To Verify Apps present in All Apps Section is with type 'Installed' or 'System'",true);
		dynamicjobspage.ClickOnAllAppsTab();
		dynamicjobspage.VerifyOfAllApps_type();
		 System.out.println("");
	}
	
	@Test(priority='6',description="6.To Verify Search Of Installed Apps by Application name and package in Downloaded Apps section")
	public void VerifySearchOfInstalledAppsByApplicationNamePackageNameInDownloadedAppsSectio() throws InterruptedException, Throwable{
		Reporter.log("\n6.To Verify Search Of Installed Apps by Application name and package in Downloaded Apps section",true);
		dynamicjobspage.ClickOnDownloadedAppsTab();
		dynamicjobspage.EnterSearchOfDownloadedApps(Config.RunAtStartup_DownloadedApps);
		dynamicjobspage.SearchOfDownloadApps(Config.RunAtStartup_DownloadedApps);
		dynamicjobspage.ClearSearchOfDownloadedApps();
		dynamicjobspage.EnterSearchOfDownloadedApps(Config.RunAtStartup_DownloadedAppsPackage);
		dynamicjobspage.SearchOfDownloadApps_Package(Config.RunAtStartup_DownloadedAppsPackage);
		dynamicjobspage.ClearSearchOfDownloadedApps();
		 System.out.println("");
	}
	
	@Test(priority='7',description="7.To Verify Search Of System Apps in Downloaded App Section")
	public void VerifySearchOfSystemAppsInDownloadedAppSection() throws InterruptedException, Throwable{
		Reporter.log("\n7.To Verify Search Of System Apps in Downloaded Section",true);
	
		dynamicjobspage.ClickOnDownloadedAppsTab();

		dynamicjobspage.EnterSearchOfDownloadedApps(Config.RunAtStartup_SystemApps);
		dynamicjobspage.SearchErrorMessage_DownloadedApps();
		dynamicjobspage.ClearSearchOfDownloadedApps();
		 System.out.println("");
	}
	
	
	@Test(priority='8',description="8.To Verify Search Of System Apps by Application name and Package name in System Apps Section")
	public void VerifySearchOfSystemAppsInSystemAppsTab() throws InterruptedException, Throwable{
		Reporter.log("\n8.To Verify Search Of System Apps by Application namd and Package name in System Apps Section",true);
		
		
		dynamicjobspage.ClickOnSystemAppsTab();
		dynamicjobspage.EnterSearchOfSystemApps(Config.RunAtStartup_SystemApps);
		dynamicjobspage.SearchOfSystemApps(Config.RunAtStartup_SystemApps);
		dynamicjobspage.ClearSearchOfSystemApps();
		dynamicjobspage.EnterSearchOfSystemApps(Config.RunAtStartup_SystemAppsPackage);
		dynamicjobspage.SearchOfSystemApps_Package(Config.RunAtStartup_SystemAppsPackage);
		dynamicjobspage.ClearSearchOfSystemApps();
		 System.out.println("");
	}

	@Test(priority='9',description="9.To Verify Search Of Downloaded Apps in System Apps Section")
	public void VerifySearchOfDownloadedAppInSystemAppTab() throws InterruptedException, Throwable{
		Reporter.log("\n9.To Verify Search Of Downloaded Apps in System Section",true);
		
		dynamicjobspage.ClickOnSystemAppsTab();

		dynamicjobspage.EnterSearchOfSystemApps(Config.RunAtStartup_DownloadedApps);
		dynamicjobspage.SearchErrorMessage_SystemApps();
		dynamicjobspage.ClearSearchOfSystemApps();
		Reporter.log("PASS>> Verification of Search Of Downloaded Apps in System Apps Section",true);
	}
	
	
	
	
   @Test(priority='A',description="10.To Verify Permission List in Downloaded Apps section")
   public void VerifPermissionListDownloadedAppSection() throws InterruptedException, Throwable{
	Reporter.log("\n10.To Verify Permission List in Downloaded Apps section",true);
	
	dynamicjobspage.ClickOnDownloadedAppsTab(); 
	dynamicjobspage.ClickOnPermission_DownloadedApps(Config.Lock_DownloadedApps);
	dynamicjobspage.VerifyOfPermissionList();
	dynamicjobspage.ClickOnAppsPermissionOkButton();
	
	 System.out.println("");
}

   @Test(priority='B',description="11.To Verify Permission List in System Apps section")
    public void VerifyPersmissionListSystemAppsSection() throws InterruptedException, Throwable{
	Reporter.log("\n11.To Verify Permission List in System Apps section",true);

	
	dynamicjobspage.ClickOnSystemAppsTab();
	dynamicjobspage.ClickOnPermission_SystemApps(Config.RunAtStartup_SystemApps);
	dynamicjobspage.VerifyOfPermissionList();
	dynamicjobspage.ClickOnAppsPermissionOkButton();
	
	
	 System.out.println("");
}
}
  
