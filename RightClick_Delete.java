package RightClick;
import java.io.IOException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;


public class RightClick_Delete extends Initialization {

	@Test(priority='0',description="1.To Verify warning message on Right Click Delete")
	public void VerifyRightClickDeleteWarningMessage() throws Exception {
		Reporter.log("\n1.To Verify warning message on Right Click Delete",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnDelete();
		rightclickDevicegrid.WarningMessageOnDeviceDelete(Config.DeviceName);
		rightclickDevicegrid.ClickOnNoButtonDeleteDeviceDialog();
		
	}
	
	@Test(priority='1',description="2.To Verify deleting device from right click")
	public void VerifyRightClickDelete() throws Exception {
		Reporter.log("\n2.To Verify deleting device from right click",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnDelete();
		rightclickDevicegrid.WarningMessageOnDeviceDelete(Config.DeviceName);
		rightclickDevicegrid.DeleteDeviceFromRightClick(); //If uncommented device will be deleted from console		
	}
	
	
    
	
}
