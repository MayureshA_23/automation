package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class InstalledAppDetails_CustomReport  extends Initialization {
@Test(priority='0',description="Verify generating Installed App Details Custom report without filters") 
public void VerifyInstalledAppDetailsRep_TC_RE_133() throws InterruptedException
	{Reporter.log("\nVerify generating Installed App Details Custom report without filters",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectInstalledAppDetails);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("InstAppDetails CustomRep without filters","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("InstAppDetails CustomRep without filters");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("InstAppDetails CustomRep without filters");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();	
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("InstAppDetails CustomRep without filters");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("InstAppDetails CustomRep without filters");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep(Config.Installed_App);
		customReports.VerifyingAppName(Config.Installed_App);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='1',description="Verify generating Installed App Detail Custom report with filters -The report should contain the details of specified package app") 
public void VerifactionOfInstalledAppDetRep_TC_RE_134() throws InterruptedException
	{Reporter.log("\nVerify generating Installed App Detail Custom report with filters- The report should contain the details of specified package app",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectInstalledAppDetails);
		customReports.Selectvaluefromdropdown(Config.SelectInstalledAppDetails);
		customReports.SelectValueFromColumn(Config.SelectAppPacakage);
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.EnterPackageId);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("InstAppDetails CustomRep with filters","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("InstAppDetails CustomRep with filters");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("InstAppDetails CustomRep with filters");
		customReports.ClickRequestReportButton();	
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("InstAppDetails CustomRep with filters");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("InstAppDetails CustomRep with filters");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerifyPackageIdColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
@Test(priority='2',description="Verify generating Installed App Detail Custom report with filters- The report should contain the details of Application") 
public void VerifactionOfInstalledAppDetRep_TC_RE_135() throws InterruptedException
	{Reporter.log("\nVerify generating Installed App Detail Custom report with filters- The report should contain the details of Application",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectInstalledAppDetails);
		customReports.Selectvaluefromdropdown(Config.SelectInstalledAppDetails);
		customReports.SelectValueFromColumn("Application Name");
		customReports.SelectOperatorFromDropDown(Config.SelectlikeOperator);
		customReports.SendValueToTextfield(Config.ApplicationNameAsGmail);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("InstAppDet CustomRep with filters to check App Det","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("InstAppDet CustomRep with filters to check App Det");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("InstAppDet CustomRep with filters to check App Det");
		customReports.ClickRequestReportButton();
		customReports.ClickOnSearchReportButton("InstAppDet CustomRep with filters to check App Det");
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("InstAppDet CustomRep with filters to check App Det");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.CheckingApplicationNameColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='3',description="Verify generating Installed App Detail Custom report with filters -The report should contain the details of Installed Application only") 
public void VerifactionOfInstalledAppDetailsForInstalledApplication() throws InterruptedException
	{Reporter.log("\nVerify generating Installed App Detail Custom report with filters -Verification of The report should contain the details of Installed Application only",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectInstalledAppDetails);
		customReports.Selectvaluefromdropdown(Config.SelectInstalledAppDetails);
		customReports.SelectValueFromColumn(Config.SelectAppType);
		customReports.SelectOperatorFromDropDown(Config.SelectEqualOperator);
		customReports.SendValueToTextfield(Config.SendJobStatusAsInstalled);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("InstAppDet CustomRep with filters to check InstAppDetails","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("InstAppDet CustomRep with filters to check InstAppDetails");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("InstAppDet CustomRep with filters to check InstAppDetails");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("InstAppDet CustomRep with filters to check InstAppDetails");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("InstAppDet CustomRep with filters to check InstAppDetails");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerifyOfInstalledAppStatusColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='4',description="Verify generating Installed App Detail Custom report with filters- Varification of The report should contain the details of Application versions which is >= 2.0 in home") 
public void VerifactionOfInstalledAppDetailsForApplicationVersionForHome() throws InterruptedException
{Reporter.log("\nVerify generating Installed App Detail Custom report with filters- The report should contain the details of Application versions which is >= 2.0 in home",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectInstalledAppDetails);
		customReports.Selectvaluefromdropdown(Config.SelectInstalledAppDetails);
		customReports.SelectValueFromColumn(Config.SelectApplicationVersion);
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield(Config.EnterApplicationVersion);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("InstAppDetCustomRep to check AppVer with greaterthan OP for Home Grp","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("InstAppDetCustomRep to check AppVer with greaterthan OP for Home Grp");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("InstAppDetCustomRep to check AppVer with greaterthan OP for Home Grp");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("InstAppDetCustomRep to check AppVer with greaterthan OP for Home Grp");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("InstAppDetCustomRep to check AppVer with greaterthan OP for Home Grp");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep(Config.Varify_App);
		customReports.VarifyingAppVersionColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
}
@Test(priority='5',description="Verify generating Installed App Detail Custom report with filters- The report should contain the details of Application versions which is >= 2.0 for particular group") 
public void VerifactionOfInstalledAppDetailsForApplicationVersionForSubGroup() throws InterruptedException
	{Reporter.log("\nVerify generating Installed App Detail Custom report with filters- The report should contain the details of Application versions which is >= 2.0 for particular group",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectInstalledAppDetails);
		customReports.Selectvaluefromdropdown(Config.SelectInstalledAppDetails);
		customReports.SelectValueFromColumn(Config.SelectApplicationVersion);
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield(Config.EnterApplicationVersion);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("InstAppDetCustomRep to check AppVer with greaterthan OP for Particular Grp","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("InstAppDetCustomRep to check AppVer with greaterthan OP for Particular Grp");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("InstAppDetCustomRep to check AppVer with greaterthan OP for Particular Grp");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("InstAppDetCustomRep to check AppVer with greaterthan OP for Particular Grp");;
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep(Config.Varify_App);
		customReports.VarifyingAppVersionColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='6',description="Verify Sort by and Group by for Installed App Details") 
public void VerifactionOfInstalledJobDetailsReport_TC_RE_139() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for Installed App Details",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("InstAppDetails CustomRep for SortBy And GroupBy","test");
		customReports.Selectvaluefromsortbydropdown(Config.SortBy_AppPackage);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);	
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("InstAppDetails CustomRep for SortBy And GroupBy");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("InstAppDetails CustomRep for SortBy And GroupBy");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("InstAppDetails CustomRep for SortBy And GroupBy");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("InstAppDetails CustomRep for SortBy And GroupBy");
		reportsPage.WindowHandle();
		//customReports.ChooseDevicesPerPage();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}	
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
