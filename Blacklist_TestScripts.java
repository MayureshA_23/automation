package BlackList_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

/*Precondition: Below mentioned varibles in config
String SearchDeviceName1=Client0001;
String SearchDeviceName2=Client0002;
No device should be there in block listed page*/

public class Blacklist_TestScripts extends Initialization{
	
	@Test(priority='1',description="Verify �No device available �information message in Blacklisted.(When no device is available in Blacklist window.)") 
	public void VerifyNoDeviceAvailableInformationWhenNoDeviceisAvailableInBlacklistedWindow() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=======Verify �No device available �information message in Blacklisted.(When no device is available in Blacklist window.)=====");
    blacklistedpage.ClickOnBlacklisted();
	blacklistedpage.MessageWhenNoDeviceAvailableInBlacklistedWindow();
	}
	
	@Test(priority='2',description="1.To verify warning message on blacklisting a device") 
	public void VerifyWarningMessageOnBlacklistingADevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=======To verify warning message on blacklisting a device	======");
	blacklistedpage.ClickOnHomeGroup();
	commonmethdpage.SearchDevice(Config.DeviceName);
	blacklistedpage.ClickOnDevice1ToBlackList(Config.DeviceName);
	blacklistedpage.ClickOnBlacklistButton();
	blacklistedpage.WarningMessageOnBlacklistingADevice();
	}
	
	@Test(priority='3',description="1.To verify clicking on No button of the blacklist warning dialog") 
	public void VerifyClickingNo_BlacklistWarningDialog() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=====To verify clicking on No button of the blacklist warning dialog======");
	blacklistedpage.ClickinOnNoButtonWarningDialogBlacklist();
	}
	
	@Test(priority='4',description="1.To verify blacklisting a device") 
	public void VerifyBlacklistingADevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=======To verify blacklisting SINGLE device========");
	blacklistedpage.ClickOnBlacklistButton();
	blacklistedpage.ClickOnYesButtonWarningDialogBlacklist(Config.DeviceName);
	blacklistedpage.ClickOnBlacklisted();
	blacklistedpage.IstheBlacklistedDevicePresent(Config.DeviceName);
	}
	
	@Test(priority='5',description="1.To verify warning message while clicking on whitelist button without selecting a device") 
	public void VerifyWarningMessageOnClickingWhitelistButtonWithoutSelectingADevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=====To verify warning message while clicking on whitelist button without selecting a device=======");
	blacklistedpage.ClickOnWhitelistButton();
	blacklistedpage.WarningMessageOnWhitelistingWithoutSelectingDevice();
	}
	
	@Test(priority='6',description="1.To verify warning message on whitelisting a device") 
	public void VerifyWarningMessageOnWhitelistingADevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("========To verify warning message on blacklisting a device=========");
	blacklistedpage.ClickOnDeviceToWhiteList();
	blacklistedpage.ClickOnWhitelistButton();
	blacklistedpage.WarningMessageOnWhitelistingADevice();
	}
	
	@Test(priority='7',description="1.To verify clicking on No button of the white warning dialog") 
	public void VerifyClickingNo_WhitelistWarningDialog() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("==========To verify clicking on No button of the whitelist warning dialog==========");
	blacklistedpage.ClickingOnNoButtonWarningDialogWhitelist(); 
	}
	
	@Test(priority='8',description="1.To verify whitelisting a device") 
	public void VerifyWhitelistingSingleDevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("==========To verify whitelisting SINGLE device============");
	blacklistedpage.ClickOnDeviceToWhiteList();
	blacklistedpage.ClickOnWhitelistButton();
	blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
	blacklistedpage.ClickOnHomeGroup();
	commonmethdpage.SearchDevice(Config.DeviceName);
	
	}

	@Test(priority='9',description="To verify blacklisting multiple devices") 
	public void VerifyBlacklistingMultipleDevices() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException {
	Reporter.log("========To verify blacklisting MULTIPLE devices===========");
	commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
	quickactiontoolbarpage.SelectingMultipleDevices();
	blacklistedpage.ClickOnBlacklistButton();
	blacklistedpage.ClickOnYesButtonWarningDialogBlacklistForMultipleDevices();
	blacklistedpage.ClickOnBlacklisted();
	blacklistedpage.IstheBlacklistedDevicePresent(Config.DeviceName);
	blacklistedpage.IstheBlacklistedDevicePresent(Config.MultipleDevices1);
	}
	
	@Test(priority='A',description="Verify Device sorting in Blacklist window .")
	public void VerifySortingOfDevicesInBlacklistedWindow() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("===BEFORE SORTING Verify Device sorting in Blacklist window ==");
	
	blacklistedpage.GetJobDevicesNameInBlacklistedWindow();
	Reporter.log("===AFTER SORTING Verify Device sorting in Blacklist .)==");
	blacklistedpage.ClickOnDeviceNameColumnColumnOfBlacklisteWindow();
	blacklistedpage.GetJobDevicesNameInBlacklistedWindow();
	}
	
	@Test(priority='B',description="1.To Verify Device Search in Blacklisted window") 
	public void VerifyDeviceSearchesInBlacklistedWindow() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=======To Verify Device Search in Blacklisted window============");
	
	blacklistedpage.ClickOnSearchFieldBlacklistedWindow(Config.DeviceName);
	
	}

	
	@Test(priority='C',description="1.To verify Whitelisting multiple devices from blacklisting list") 
	public void VerifyWhitelistingMultipeDevices() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=======To verify Whitelisting MULTIPLE  devices from blacklisting list============");
	blacklistedpage.SelectMultipleDevicesToWhitelist(Config.DeviceName,Config.MultipleDevices1);
	blacklistedpage.ClickOnWhitelistButton();
	blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
	
 }

	@Test(priority='D',description="1.To verify warning message while clicking on delete button without selecting a device") 
	public void VerifyWarningMessageOnClickingDeleteButtonWithoutSelectingADevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("========To verify warning message while clicking on delete button without selecting a device=========");
	blacklistedpage.ClickOnHomeGroup();
	commonmethdpage.SearchDevice(Config.DeviceName);
	blacklistedpage.ClickOnDevice1ToBlackList(Config.DeviceName);
	blacklistedpage.ClickOnBlacklistButton();
	blacklistedpage.ClickOnYesButtonWarningDialogBlacklist(Config.DeviceName);
	blacklistedpage.ClickOnBlacklisted();
	blacklistedpage.IstheBlacklistedDevicePresent(Config.DeviceName);
	blacklistedpage.ClickOnDeleteButton_Blacklist();
	blacklistedpage.WarningMessageOnWhitelistingWithoutSelectingDevice();//common
	}

	@Test(priority='E',description="1.To verify warning message on deleting a device") 
	public void VerifyWarningMessageOnDeletingADevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=====To verify warning message on deleting a device==========");
	blacklistedpage.ClickOnDeviceToWhiteList();//common
	blacklistedpage.ClickOnDeleteButton_Blacklist();
	blacklistedpage.WarningMessageOnDeletingADeviceFromBlacklistedlist();
	}
	
	@Test(priority='F',description="1.To verify clicking on No button of the delete warning dialog") 
	public void VerifyClickingNo_DeleteWarningDialog() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=====To verify clicking on No button of the whitelist warning dialog=======");
	blacklistedpage.ClickingOnNoButtonWarningDialogWhitelist(); 
	}
	
	
	/*@Test(priority='G',description="1.To verify clicking on Yes button of the delete warning dialog") 
	public void VerifyDeletingSingleDeviceFromBlacklistedList() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=====To verify deleting SINGLE device from the blacklisted list=========");
	blacklistedpage.ClickOnDeleteButton_Blacklist();
	blacklistedpage.ClickOnYesButtonWarningDialogDelete();
	blacklistedpage.MessageOnSuccessfullDeletionOfDevicesFromBlacklistedList();
	blacklistedpage.VerifyDeviceCountAfterDeletingASingleDeviceFromBlacklisted();
	
	}
	
	@Test(priority='H',description="1.To verify deleting multiple devices from the blacklisted list") 
	public void VerifyDeletingMultipleDevicesFromBlacklistedList() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("=======To verify deleting MULTIPLE devices from the blacklisted listd==========");
	blacklistedpage.ClickOnHomeGroup();
	blacklistedpage.ClickOnNewdeviceToBlackList(); // selecting a new device to blacklist
	blacklistedpage.ClickOnBlacklistButton();
	blacklistedpage.ClickOnYesButtonWarningDialogBlacklist();
	blacklistedpage.ClickOnBlacklisted();
	blacklistedpage.SelectMultipleDevicesToDelete();
	blacklistedpage.ClickOnDeleteButton_Blacklist();
	blacklistedpage.ClickOnYesButtonWarningDialogDelete();
	blacklistedpage.MessageOnSuccessfullDeletionOfDevicesFromBlacklistedList();
	
}*/

	
	

}



