package DeviceEnrollment;

import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceEnrollmentRules extends Initialization
{
     @Test(priority=1,description="Verify Device enrolment Rules in Account Settings")
     public void VerifyDeviceEnrolmentRulesInAccountSettings() throws InterruptedException
     {
    	 commonmethdpage.ClickOnSettings();
    	 commonmethdpage.ClickonAccsettings();
    	 DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
    	 accountsettingspage.VerifyDeviceEnrollmentRulesInAccSettings();
    	 accountsettingspage.VerifyAuthTypesInDropDown();
    	 commonmethdpage.ClickOnHomePage();
     }
     
     @Test(priority=2,description="Verify Device enrolment Rules-Device Authentication Type Required Password error message")
     public void VerifyDeviceEnrollmentRequiredPWDErrorMessage() throws InterruptedException
     {
    	 commonmethdpage.ClickOnSettings();
    	 commonmethdpage.ClickonAccsettings();
    	 DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
    	 DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
    	 DeviceEnrollment_SCanQR.SettingPassword("12345");
    	 accountsettingspage.ClickOnDeviceEnollmentRulespplyButton_ErrorMessage(accountsettingspage.PasswordLengthErrorMessage);
    	 commonmethdpage.ClickOnHomePage();
     }
     
     @Test(priority=3,description="Verify Device enrolment Rules-Device Authentication Type as OAuth Authentication with Advanced Device Authentication enabled and keep all values as blank.")
     public void VerifyOAuthWihtADAEnabledAndKeepingValesBlank() throws InterruptedException
     {
    	 commonmethdpage.ClickOnSettings();
    	 commonmethdpage.ClickonAccsettings();
    	 DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
    	 DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
     	 DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
     	 accountsettingspage.ClickOnDeviceEnollmentRulespplyButton_ErrorMessage(accountsettingspage.OAuthEmptyErrorMessage);
     	commonmethdpage.ClickOnHomePage();
     	 
     }
     
     @Test(priority=4,description="Verify Device enrolment Rules-Device Authentication Type as OAuth Authentication with Advanced Device Authentication disabled and keep all required field as blank for native and web application.")
     public void VerifyOAuthWihtADADisabledAndKeepingValesBlank() throws InterruptedException
     {
    	 commonmethdpage.ClickOnSettings();
    	 commonmethdpage.ClickonAccsettings();
    	 DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
    	 DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
     	 DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
     	 DeviceEnrollment_SCanQR.SettingOAuthType_NativeappTab("4","GSUITE");
     	 accountsettingspage.ClearingTextFields();
     	 accountsettingspage.ClickOnDeviceEnollmentRulespplyButton_ErrorMessage(accountsettingspage.OAuthEmptyErrorMessage);
     	commonmethdpage.ClickOnHomePage();
     }
     
     @Test(priority=5,description="Verify entering invalid data in tenant ID field.")
     public void VerifyErrorMessageByKeepingAPPIDEmpty() throws InterruptedException
     {
    	 commonmethdpage.ClickOnSettings();
    	 commonmethdpage.ClickonAccsettings();
    	 DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
    	 DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
     	 DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
     	accountsettingspage.EnteringDataInTenantID();
     	accountsettingspage.VerifyErrorMessageOnEnteringInvalidTenantID();
    	  	 
     }
     
     
     
     
     
     
     
     
     
}
