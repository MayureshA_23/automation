package Profiles_Windows_Testscripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class PeriodicAppLaunch extends Initialization{
	
	@Test(priority='1',description="To verify clicking on Periodic App Launch") 
    public void VerifyClickingOnPeriodicAppLaunch() throws InterruptedException
	{
        Reporter.log("1.To verify clicking on Periodic App Launch",true);
        profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
        profilesWindows.ClickOnPeriodicAppLaunch();
    }
	@Test(priority='2',description="To Verify warning message Saving Periodic App Launch profile without profile Name")
	public void VerifyWarningMessageOnSavingPeriodicAppLaunchProfileWithoutName() throws InterruptedException{
		Reporter.log("\n2.To Verify warning message Saving Periodic App Launch profile without profile Name",true);
		profilesWindows.ClickOnConfigureButton_PeriodicAppLaunchProfile();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='3',description="To Verify the parameters of Periodic App Launch Window")
	public void VerifyParamentersOfPeriodicAppLaunchWindow() throws InterruptedException{
		Reporter.log("\n3.To Verify the parameters of Periodic App Launch Window",true);
		profilesWindows.EnterPeriodicAppLaunchProfileName();
		profilesWindows.VerifyAlltheParametersOfPeriodicAppLaunchWindow();
	}

	@Test(priority='4',description="To Verify warning message Saving Periodic App Launch Profile with Periodicity less than 1 min")
	public void VerifyWarningMessageOnSavingPeriodicAppLaunchProfileWithPeriodicityLessThanOneMinute() throws InterruptedException{
		Reporter.log("\n4.To Verify warning message Saving Periodic App Launch Profile with Periodicity less than 1 min",true);
		profilesWindows.EnterPeriodAppLaunchLessThanOne();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	@Test(priority='5',description="To Verify Saving Periodic App Launch profile")
	public void VerifySavingPeriodicAppLaunchProfile() throws InterruptedException{
		Reporter.log("\n5.To Verify Saving Periodic App Launch profile",true);
		profilesWindows.EnterPeriodAppLaunchTime();
	    profilesWindows.ClickOnSaveButton();
		profilesWindows.NotificationOnProfileCreated();
	}

}
