package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//0InstallApplication should be created job
//"##Autotest" and "AutomationFolderName" this folder should not be present in present in jobs section
//JTouch App should be removed from App store
public class CompositeJob extends Initialization 
{
	@Test(priority=0,description="Job Creation")
  public void CreatingJobs() throws InterruptedException, IOException
  {
          androidJOB.clickOnJobs();
          androidJOB.clickNewJob();
          androidJOB.clickOnAndroidOS();
          androidJOB.clickOnTextMessageJob();
          androidJOB.textMessageJobWhenNoFieldIsEmpty("TextMessageDontDelete", "0text","Normal Text");
          androidJOB.ClickOnOkButton();
          androidJOB.JobCreatedMessage();
          androidJOB.clickNewJob();
          androidJOB.clickOnAndroidOS();
          androidJOB.clickOnTextMessageJob();
          androidJOB.textMessageJobWhenNoFieldIsEmpty("AutomationDontDeleteTextMessage", "0text","Normal Text");
          androidJOB.ClickOnOkButton();
          androidJOB.JobCreatedMessage();
          androidJOB.clickNewJob();
          androidJOB.clickOnAndroidOS();
          androidJOB.clickOnFileTransferJob();
          androidJOB.enterJobName("AutomationDonotDeleteFTJob");
          androidJOB.clickOnAdd();
          androidJOB.browseFile();
          androidJOB.clickOkBtnOnBrowseFileWindow();
          androidJOB.clickOkBtnOnAndroidJob();
          androidJOB.verifyFileTransferJobCreation("AutomationDonotDeleteFTJob");
          androidJOB.UploadcompleteStatus();        
  }

	@Test(priority = 1, description = "Composite job-Verify delete option in composite job.")
	public void VerifyingDeleteOptionInCompositeJob()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("Composite job-Verify delete option in composite job.", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnCompositeJob();
		androidJOB.ClickOnAddButtonCompositeJob();
		androidJOB.SearchJobForCompositeAction("0InstallApplication");
		androidJOB.DeletingCompositeJob();
		androidJOB.CheckingdeletedCompositeJob();
		androidJOB.ClickOnBackButtonJobWindow();
	}

	@Test(priority = 2, description = "Composite job-Verify moveup/Move down button in composite jon.")
	public void VerifyingMoveUpAndDownBtnInCompositeJob()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		

		androidJOB.ClickOnCompositeJob();
		androidJOB.ClickOnAddButtonCompositeJob();
		androidJOB.SearchJobForCompositeAction("TextMessageDontDelete");
		androidJOB.ClickOnAddButtonCompositeJob();
		androidJOB.SearchJobForCompositeAction("0InstallApplication");
		androidJOB.ClickOnMoveDownAndVerify();
		androidJOB.ClickOnMoveUpAndVerify();

	}

	@Test(priority = 3, description = "3. Verify Creation of Composite Job")
	public void VerifyCreationofCompositeJob() throws InterruptedException {
		androidJOB.ClickOnAddButtonCompositeJob();
		androidJOB.SearchJobForCompositeAction("AutomationDontDeleteTextMessage");
		androidJOB.ClickOnAddButtonCompositeJob();
		androidJOB.SearchJobForCompositeAction("AutomationDonotDeleteFTJob");
		androidJOB.CompositeJobName("DontDeleteCompositeJob");
		androidJOB.ClickOnOnOkButtonAndroidJob();

	}

	@Test(priority = 4, description = "4. Composite job should get deployed successfully.")
	public void ApplyTheCompositeJobOnDevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.JobName1);
		androidJOB.JobInitiatedMessage(); 

		androidJOB.CheckStatusOfappliedInstalledJob(Config.JobName1, Config.TotalTimeForJobName1ToGetDeployedOnDevice);
		Reporter.log("");
		Reporter.log("****************************************");
		Reporter.log("");
	}

	@Test(priority = 5, description = "5. Modify the Composite job and Apply")
	public void ModifyTheCompositeJobAndApply()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("DontDeleteCompositeJob");
		androidJOB.ClickOnOnJob("DontDeleteCompositeJob");

		androidJOB.clickOnModifyJob();
		androidJOB.CompositeJobName("DontDeleteCompositeJobModified");
		androidJOB.ClickOnOkButtonModifyAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DontDeleteCompositeJobModified");
		androidJOB.JobInitiatedMessage(); 

		androidJOB.CheckStatusOfappliedInstalledJob("DontDeleteCompositeJobModified", Config.TotalTimeForJobName1ToGetDeployedOnDevice);
		Reporter.log("");
		Reporter.log("****************************************");
		Reporter.log("");
	}
	   @Test(priority=6,description="Composite Job - Verify creating composite job by adding android version.")
	    public void VerifyCreatingAndroidJobsByAddingAndroidVersion() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	    {
	        androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.clickOnTextMessageJob();
			androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
			androidJOB.ClickOnOkButton();
			androidJOB.JobCreatedMessage();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.clickOnFileTransferJob();
			androidJOB.enterJobName(Config.FileTransferJobName);
			androidJOB.clickOnAdd();
			androidJOB.browseFile("./Uploads/UplaodFiles/Job On Android/FileTransferSingle/\\File1.exe");//./Uploads/UplaodFiles/FileStore/\\Image.exe
			androidJOB.clickOkBtnOnBrowseFileWindow();
			androidJOB.clickOkBtnOnAndroidJob();
			androidJOB.JobCreatedMessage();
	    	androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("comp_job");
			androidJOB.CompositeJobName(Config.CompositeJobName);
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction(Config.TextMessageJobname);
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction(Config.FileTransferJobName);
		   		   
			androidJOB.SelectingConnectionTypeInNixAgentSettingsJob("comp_job_min_version_logic","0");
			androidJOB.SelectingConnectionTypeInNixAgentSettingsJob("comp_job_min_version_num","27");
			androidJOB.ClickOnOnOkButtonAndroidJob();
			commonmethdpage.ClickOnHomePage();
			commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField(Config.CompositeJobName);
			androidJOB.CheckStatusOfappliedInstalledJob(Config.CompositeJobName, Config.TotalTimeForJobName1ToGetDeployedOnDevice);
	    }
	    @Test(priority=7,description="Jobs - Verify applying copied job to device")
	    public void verifyApplyingCopiedJobtodevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.ClickOnNewFolder();
			androidJOB.NamingFolder(Config.JobFolderName);
			dynamicjobspage.SearchJobInSearchField(Config.TextMessageJobname);
			androidJOB.ClickOnOnJob(Config.TextMessageJobname);
			androidJOB.ClickOnCopyJobOption();
			androidJOB.CopyNixUpgradeJob(Config.JobFolderName);
			androidJOB.ClickOnOkBtnCopyFolder();
			commonmethdpage.ClickOnHomePage(); 
			


			commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			commonmethdpage.SearchJob(Config.JobFolderName);
          commonmethdpage.Selectjob(Config.JobFolderName);
          androidJOB.DoubleClickOnFolder("scheduleJob",Config.JobFolderName);
      	commonmethdpage.SearchJob(Config.TextMessageJobname+"_copy");
      	commonmethdpage.Selectjob(Config.TextMessageJobname+"_copy");

      	commonmethdpage.ClickOnApplyButtonApplyJobWindow();
          androidJOB.CheckStatusOfappliedInstalledJob(Config.TextMessageJobname+"_copy",600);

	    }

	    @Test(priority=8,description="Verify by adding install/FT job in composite job")
	    public void VerifyByAddingInstallJonInCompositeJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	    {
	    	androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("comp_job");
			androidJOB.CompositeJobName(Config.CompositeJobName);
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction(Config.TextMessageJobname);
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction(Config.InstallJObName);
			androidJOB.ClickOnOnOkButtonAndroidJob();
			commonmethdpage.ClickOnHomePage();
			commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
	    	androidJOB.SearchJob(Config.CompositeJobName);
			androidJOB.SelectingChargingStateInApplyJob("Don't Care");
			androidJOB.ClickOnApplyJobOkButton();
			androidJOB.CheckStatusOfappliedInstalledJob(Config.CompositeJobName, Config.TotalTimeForJobName1ToGetDeployedOnDevice);			
	    }
	    @Test(priority=9,description="Verify Composite job UI is not getting displayed when we add notification policy in Android fence jobs")
	    public void VerifyByErrorMsgCompositeJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	    {
	    	androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnNotificationPolicy();
			androidJOB.EnableConnectionPolicy();
			androidJOB.EnableMDM();
			androidJOB.EnterJobName(Config.NotificationJobName);
			androidJOB.ClickOnSaveJob();
			androidJOB.JobCreatedMessage();
			commonmethdpage.ClickOnHomePage();
	    	androidJOB.clickOnJobs();

			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			deviceinfopanelpage.ClickOnTimeFence();
			deviceinfopanelpage.ClickOnEnableTimeFence(false);
			deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1");
			deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2");
			deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();

			deviceinfopanelpage.ClickOnFenceEnteredTab();
			deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
			deviceinfopanelpage.SearchFenceJob(Config.NotificationJobName);
			deviceinfopanelpage.SelectFenceJob(Config.NotificationJobName);
			deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
			androidJOB.VerifyConfirmationMessageNoCompositeJobTextShouldBeDisplayed();
			deviceinfopanelpage.ClickOnCloseButtonOfTimeFence();


			  }
	    
	    
	    @Test(priority='A',description="Verify adding alert message job inside composite job")
	    public void CreatingAlertMsgCompositeJobTC_JO_805() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	    {
//	    	androidJOB.clickOnJobs();
//			androidJOB.clickNewJob();
//			androidJOB.clickOnAndroidOS();

			androidJOB.ClickOnalert_messageJob();
			androidJOB.CreateNewAlertimg_option();
			androidJOB.InputJobNameAlertMessage("AlertMessageJobType");
			androidJOB.alertTitle_Input("Alert");
			androidJOB.alertTitleColorClick();
			androidJOB.WaitForSelectColorPopUp();
			androidJOB.InputColorAlertTitle("#e0cd1f");
			androidJOB.ClickOnoKbtnAlertTitleColor();
			androidJOB.InputAlertDescription("DescriptionAlert");
			androidJOB.alertDescriptionColorClick();
			androidJOB.WaitForSelectColorPopUp();
			androidJOB.InputColorAlertTitle("#37de13");
			androidJOB.ClickOnoKbtnAlertTitleColor();
			androidJOB.alertIconSelectionInput();
			androidJOB.selectDropDownPredefined_icons();
			androidJOB.ClickOnUploadIcon();
			androidJOB.SelectPredefinedIcon();
			androidJOB.ClickOnalertBackgroundColor();
			androidJOB.WaitForSelectColorPopUp();
			androidJOB.InputColorAlertTitle("#204cc8");
			androidJOB.ClickOnoKbtnAlertTitleColor();
			androidJOB.Alert_Display_typeClick();
			androidJOB.SelectDropDownAlert_Display_type();
			androidJOB.EnableAlertBuzz();
			androidJOB.InputAlertBuzzTime("20");
			androidJOB.EnableCloseAlertPopupEnable();
			androidJOB.InputCloseAlertPopup("30");
			androidJOB.ClickOnSaveOkButton();
			androidJOB.JobCreatedMessage();

			commonmethdpage.ClickOnHomePage(); 

			
			
			
			
			   }
	    
	    
	    
	    @Test(priority='B',description="Verify adding alert message job inside composite job")
	    public void VerifyAddingAlertMsgCompositeJobTC_JO_805() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	    {
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

	        	androidJOB.clickOnJobs();
				androidJOB.clickNewJob();
				androidJOB.clickOnAndroidOS();

				
				androidJOB.ClickOnCompositeJob();
				androidJOB.ClickOnAddButtonCompositeJob();
				androidJOB.SearchJobForCompositeAction("AlertMessageJobType");
				androidJOB.CompositeJobName("AlertMessageCompositeJob");
				androidJOB.ClickOnOnOkButtonAndroidJob();
				androidJOB.JobCreatedMessage();
				commonmethdpage.ClickOnHomePage(); 
				commonmethdpage.SearchDevice();
				commonmethdpage.ClickOnApplyButton();
				commonmethdpage.SearchJob("AlertMessageCompositeJob");
				commonmethdpage.Selectjob("AlertMessageCompositeJob");
				commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		        androidJOB.CheckStatusOfappliedInstalledJob("AlertMessageCompositeJob",600);
		        androidJOB.VerifyDeviceSide();
		        

				
		    }   
	    @Test(priority='C',description="Verify Adding SureMDM appstore profile in Composite job and deploying on NON AE devices")
	    public void CreationApplicationPolicy() throws Throwable
	    {

			appstorepage.ClickOnAppStore();
			appstorepage.ClickOnAddNewAppAndroid();
			appstorepage.ClickOnUploadAPKAndroid();
			appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\FileTouch.exe"); //D:\UplaodFiles\App Store-Android\J Touch.apk
			appstorepage.VerifyOfUploadApkAndroid();
			appstorepage.EnterCategoryAndroid("J Touch");
			appstorepage.EnterDescriptionAndroid("J Touch");
			appstorepage.ClickOnUploadApkPopupAddButton();
			commonmethdpage.ClickOnHomePage();
			profilesAndroid.ClickOnProfiles();
			profilesAndroid.AddProfile();
			profilesAndroid.ClickOnApplicationSettingsPolicy();
			profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
			profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
			profilesAndroid.ClickOnSureMDMAppStore();
			profilesAndroid.SelectingTheCreatedAppDropdown("J Touch");
			profilesAndroid.ClickOnInstallSilentlyCheck_Box();
			profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
			profilesAndroid.EnterSystemSettingsProfileName("CreationApplicationPolicy");
			profilesAndroid.ClickOnSaveApplicationPolicy();

	    }
	    @Test(priority='D',description="Verify Adding SureMDM appstore profile in Composite job and deploying on NON AE devices")
	    public void CreationFTjob() throws Throwable
	    {

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName("NixSingleFileTransfer");
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe");
		androidJOB.EnterDevicePath("/sdcard/Download");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage(); 
		
		
	    }

	    @Test(priority='E',description="Verify Adding SureMDM appstore profile in Composite job and deploying on NON AE devices")
	    public void VerifyAddingSureMDMAppstoreProfileInCompositeJobAndDeployingOnNON_AEDevices() throws Throwable{

			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

      	androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();

			
			androidJOB.ClickOnCompositeJob();
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("CreationApplicationPolicy");
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("NixSingleFileTransfer");

			androidJOB.CompositeJobName("AddingApplicationPolicy_FTJob");
			androidJOB.ClickOnOnOkButtonAndroidJob();
			androidJOB.JobCreatedMessage();
			commonmethdpage.ClickOnHomePage(); 
			commonmethdpage.SearchDevice();
			commonmethdpage.ClickOnApplyButton();
			commonmethdpage.SearchJob("AddingApplicationPolicy_FTJob");
			commonmethdpage.Selectjob("AddingApplicationPolicy_FTJob");
			commonmethdpage.ClickOnApplyButtonApplyJobWindow();
	        androidJOB.CheckStatusOfappliedInstalledJob("AddingApplicationPolicy_FTJob",600);
	        
			commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
			androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
			androidJOB.ClickOnHomeButtonDeviceSide();

			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
			profilesAndroid.VerifyAppStore();
			profilesAndroid.VerifyAppInstall("J Touch");
			androidJOB.unInstallApplication("com.bs.smarttouch.gp");
//			profilesAndroid.ClickOnProfiles();
//			appstorepage.DeleteCreatedProfile("CreationApplicationPolicy");


	    	
	    	
	    	
 }
	    @Test(priority='F',description="Verify adding same job in composite job twice")
	    public void TC_JO_918() throws Throwable{

	    	
	    	androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
          androidJOB.ClickOnCompositeJob();

			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("0TextMessage");
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("0TextMessage");
			androidJOB.VerifyTwoSameCompositeJobs("0TextMessage", "1", "0TextMessage", "2");
			androidJOB.CompositeJobName("SameComositeJobs");
			androidJOB.ClickOnOnOkButtonAndroidJob();
			androidJOB.JobCreatedMessage();
			commonmethdpage.ClickOnHomePage(); 

	    	
	     }
	    
	    @Test(priority='G',description="Composite Job - Verify jobs added in Composite job with delay time deploying one after the other")
	    public void TC_JO_887() throws Throwable{

			androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnCompositeJob();
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("TextMessageDontDelete");
			deviceinfopanelpage.ClickOnDelayCompositeJob("2");
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("AutomationDonotDeleteFTJob");
			deviceinfopanelpage.ClickOnDelayCompositeJob("2");

			androidJOB.CompositeJobName("CompositeJobWithDelay");
			androidJOB.ClickOnOnOkButtonAndroidJob();
			androidJOB.JobCreatedMessage();

			commonmethdpage.ClickOnHomePage(); 
			androidJOB.SearchDeviceInconsole(Config.DeviceName); 
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("CompositeJobWithDelay"); 
			androidJOB.clickonJobQueue();
			quickactiontoolbarpage.ClickingOnSubJobQueue();
			quickactiontoolbarpage.VerifySequenceOfSubJobs("TextMessageDontDelete");
			quickactiontoolbarpage.VerifySequenceOfSubJobs("AutomationDonotDeleteFTJob");
			quickactiontoolbarpage.VerifyingDeployedJobsForSubJobQueue_Composite("TextMessageDontDelete", 20, "Deployed", 1);
			quickactiontoolbarpage.VerifyingProgressedJobsForSubJobQueue_Composite("AutomationDonotDeleteFTJob", 20, "In progress", 2, "Deployed");
          quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
          androidJOB.CheckStatusOfappliedCompositeJob("CompositeJobWithDelay",100);
	    	
	    	
	    	  }
	    
	    @Test(priority='H',description="Verify sequence of composite job when delay is added inside it.")
	    public void TC_JO_915() throws Throwable{
  
			androidJOB.SearchDeviceInconsole(Config.DeviceName); 
			androidJOB.clickonJobQueue();
			androidJOB.clickOnJobhistoryTab();
			quickactiontoolbarpage.SelectCompositeJob("CompositeJobWithDelay");
          quickactiontoolbarpage.ClickingOnSubJobQueue();
			quickactiontoolbarpage.VerifyInJobHistoryCompleted_WithDelayCompositeJob("TextMessageDontDelete","Delay","AutomationDonotDeleteFTJob","Delay");
          quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
          androidJOB.ClickOnJobQueueCloseButton();


	    
	      }
	    @Test(priority='I',description="Verify sequence of composite job once job got deployed")
	    public void TC_JO_914_Creating() throws Throwable{

	    	
	    	
			androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnCompositeJob();
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("TextMessageDontDelete");
			androidJOB.ClickOnAddButtonCompositeJob();
			androidJOB.SearchJobForCompositeAction("AutomationDonotDeleteFTJob");

			androidJOB.CompositeJobName("CompositeJobWithoutDelay");
			androidJOB.ClickOnOnOkButtonAndroidJob();
			androidJOB.JobCreatedMessage();

			commonmethdpage.ClickOnHomePage(); 
			androidJOB.SearchDeviceInconsole(Config.DeviceName); 
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("CompositeJobWithoutDelay"); 
			androidJOB.clickonJobQueue();
			quickactiontoolbarpage.ClickingOnSubJobQueue();
			quickactiontoolbarpage.VerifySequenceOfSubJobs("TextMessageDontDelete");
			quickactiontoolbarpage.VerifySequenceOfSubJobs("AutomationDonotDeleteFTJob");
          quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
          androidJOB.CheckStatusOfappliedCompositeJob("CompositeJobWithoutDelay",100);
	
	    	
	    	
	    	  }
	    @Test(priority='J',description="Verify sequence of composite job once job got deployed")
	    public void TC_JO_914() throws Throwable{

	    	
	    	
			androidJOB.SearchDeviceInconsole(Config.DeviceName); 
			androidJOB.clickonJobQueue();
			androidJOB.clickOnJobhistoryTab();
			quickactiontoolbarpage.SelectCompositeJob("CompositeJobWithoutDelay");
          quickactiontoolbarpage.ClickingOnSubJobQueue();
			quickactiontoolbarpage.VerifyInJobHistoryCompleted_CompositeJob("TextMessageDontDelete","AutomationDonotDeleteFTJob");
          quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
          androidJOB.ClickOnJobQueueCloseButton();

	    }
	    	
  	    @Test(priority='K',description="Verify sequence of composite job when it has error job present in it")
  	    public void TC_JO_916() throws Throwable{

  			androidJOB.clickOnJobs();
  			androidJOB.clickNewJob();
  			androidJOB.clickOnAndroidOS();
  			quickactiontoolbarpage.CreatingSureFoxSettingsJob(Config.FailJob);
  			androidJOB.clickNewJob();
  			androidJOB.clickOnAndroidOS();
  			androidJOB.ClickOnCompositeJob();
  			androidJOB.ClickOnAddButtonCompositeJob();
  			androidJOB.SearchJobForCompositeAction("TextMessageDontDelete");
  			androidJOB.ClickOnAddButtonCompositeJob();
  			androidJOB.SearchJobForCompositeAction("AutomationDonotDeleteFTJob");
  			androidJOB.ClickOnAddButtonCompositeJob();
  			androidJOB.SearchJobForCompositeAction(Config.FailJob);

  			androidJOB.CompositeJobName("CompositeJobWithErrorJob");
  			androidJOB.ClickOnOnOkButtonAndroidJob();
  			androidJOB.JobCreatedMessage();
  			commonmethdpage.ClickOnHomePage(); 
  			androidJOB.SearchDeviceInconsole(Config.DeviceName); 
  			commonmethdpage.ClickOnApplyButton();
  			androidJOB.SearchField("CompositeJobWithErrorJob"); 
  			quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
  			quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobErrorStateSymbol);
  			quickactiontoolbarpage.SelectCompositeJobError("CompositeJobWithErrorJob");
              quickactiontoolbarpage.ClickingOnSubJobQueue();
  			quickactiontoolbarpage.VerifySequenceOfSubJobs("TextMessageDontDelete");
  			quickactiontoolbarpage.VerifySequenceOfSubJobs("AutomationDonotDeleteFTJob");
  			quickactiontoolbarpage.VerifySequenceOfSubJobs(Config.FailJob);
  			quickactiontoolbarpage.VerifyInJobHistoryErrorTab_CompositeJob("TextMessageDontDelete","AutomationDonotDeleteFTJob", Config.FailJob);
              quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
              quickactiontoolbarpage.ClickOnJobQueueCloseBtn();

  	

  	    	
  	    	
  }
  	    
  	    
  	    
	    @Test(priority='L',description="Jobs - Verify Job folder path for jobs present in console")
	    public void TC_JO_306() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.ClickOnNewFolder();
			androidJOB.NamingFolder("AutomationFolderName");
			dynamicjobspage.SearchJobInSearchField(Config.TextMessageJobname);
			androidJOB.ClickOnOnJob(Config.TextMessageJobname);
			androidJOB.ClickOnCopyJobOption();
			androidJOB.CopyNixUpgradeJob("AutomationFolderName");
			androidJOB.ClickOnOkBtnCopyFolder();
			dynamicjobspage.SearchJobInSearchField("AutomationFolderName");
          androidJOB.DoubleClickOnFolderNixUpgradeJob("AutomationFolderName");
			androidJOB.VerifyJobFolderPath("AutomationFolderName");


	    }    
	   
}
