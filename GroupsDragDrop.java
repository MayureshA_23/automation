package Groups_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class GroupsDragDrop extends Initialization {

	@Test(priority = 1, description = "Verify drag and drop of the device in Group and subgroup  ")
	public void GroupsTC_GR_006()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		commonmethdpage.refesh();

		commonmethdpage.ClickOnHomePage();
		groupspage.ClickOnGroup(Config.SourceGroupName);
		groupspage.ClickOnDropDownGroups(Config.SubGroupDevice);
		groupspage.ClickOnGroup(Config.SubGroupDevice);
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		groupspage.DragAndDropDevice();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		devicegridpage.RefreshDeviceGrid();
		deviceinfopanelpage.VerifyDeviceNameAndroid();

	}

	@Test(priority = 2, description = "Verify Remove Job in the Group Job Queue in pending,InProgress, Failed Status tabs.")
	public void groupsTC_GR_034_PendingJobInsideJobQueue()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		// *********PENDING JOB INSIDE GROUPJOB QEUE*********

		commonmethdpage.ClickOnHomePage();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.EnterJobWNameFileApk);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseAPK("./Uploads/UplaodFiles/FileStore/\\Game.exe");
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();

		commonmethdpage.ClickOnHomePage();

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		commonmethdpage.SearchDevice(Config.DeviceName);
		groupspage.ClickOnApplyGroups();
		groupspage.SearchTextFieldAllJobStatus(Config.EnterJobWNameFileApk, Config.SourceGroupName); // AllJobStatusJobs
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending", Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobPendingStateSymbol);
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
		commonmethdpage.ClickOnHomePage();

		androidJOB.ClearSearchFieldConsole();

		commonmethdpage.SearchDevice(Config.DeviceName);
		groupspage.clickOnGrpJobQeue();
		groupspage.VerifyJobPendingJobSectionGroupRtClick();
		groupspage.SelectPendingJobStatusJobs();
		groupspage.ClickOnRemoveBtnJob();
		groupspage.CloseBtnRtClickJobHistory();
	}

	@Test(priority = 2, description = "Verify Remove Job in the Group Job Queue in pending,InProgress, Failed Status tabs.")
	public void groupsTC_GR_034_InprogressJobInsideJobQueue()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		// ****************INPROGRESS JOB INSIDE GROUP JOB QUEUE*****************
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		groupspage.ClickOnApplyGroups();
		groupspage.SearchTextFieldAllJobStatus(Config.EnterJobWNameFileApk, Config.SourceGroupName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		// androidJOB.clickOnDoneBtnInSettings();
		quickactiontoolbarpage.DisablingNixService();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress", Config.DeviceName);
		commonmethdpage.ClickOnHomePage();

		groupspage.clickOnGrpJobQeue();
		groupspage.VerifyJobInProgressJobSectionGroupRtClick();
		groupspage.SelectPendingJobStatusJobs();
		groupspage.ClickOnRemoveBtnJob();
		groupspage.CloseBtnRtClickJobHistory();
	}

	@Test(priority = 2, description = "Verify Remove Job in the Group Job Queue in pending,InProgress, Failed Status tabs.")
	public void groupsTC_GR_034_FailedJobInsideJobQueue()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		// **************** FAILED JOB INSIDE GROUPJOBQEUE********************

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		quickactiontoolbarpage.ClickOnNixSettings();
		// quickactiontoolbarpage.EnablingCheckBoxes("Enable Nix Service", 0);
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		quickactiontoolbarpage.CreatingSureFoxSettingsJob(Config.FailJob);
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		groupspage.ClickOnApplyGroups();
		groupspage.SearchTextFieldFailJob(Config.FailJob);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error", Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobErrorStateSymbol);
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();

		groupspage.clickOnGrpJobQeue();
		groupspage.VerifyJobFailedJobSectionGroupRtClick();
		groupspage.SelectFailJob();
		groupspage.ClickOnRemoveBtnJob();
		groupspage.CloseBtnRtClickJobHistory();

	}

	// Any Default Job 'FileJob' should be present and device should not be enrolled

	@Test(priority = 3, description = "Verify re-appy on re-registration in group properties.")
	public void GroupsTC_GR_038()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException, AWTException {

		commonmethdpage.refesh();

		commonmethdpage.ClickOnHomePage();

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.FileTransferJobTAGS);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileTRansferJobTAGS("./Uploads/UplaodFiles/FileStore/\\File.exe");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		groupspage.ClickOnHomeGrup();

		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		devicegridpage.RefreshDeviceGrid();

		groupspage.DeleteDeviceFromConsole();
		groupspage.ConfirmationPopUpDeleteDevice();
		groupspage.PendingDeleteSection();
		commonmethdpage.SearchPendingDeleteDeviceInput(Config.DeviceName);
		groupspage.SelectDevicePendingDelete();

		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnGrpProperties();
		groupspage.ClickOnAddPropertiesBtn();
		groupspage.SearchJob(Config.FileTransferJobTAGS);

		groupspage.DefaultJobSelect();
		groupspage.OkBtnDefaultJob_Click();
		groupspage.ForceApplyOnRe_Registration();
		groupspage.grouppropertiesokbtnClick();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickingOnDoneButton();

		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDeviceModel(Config.DeviceModel);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.FileTransferJobTAGS, 360);

	}

	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException {
		if (ITestResult.FAILURE == result.getStatus()) {
			deviceGrid.RefreshGrid();
		}
	}

}
