package JobsOnAndroid;

import org.testng.annotations.Test;

import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class FileTransfer extends Initialization  {
	@Test(priority = 1, description = "File Transfer - Verify creating and deploying single File transfer job.")
	public void VerifySingleFileTransfer() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName("NixSingleFileTransfer");
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe");
//		androidJOB.EnterDevicePath("/sdcard/Download");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.JobCreatedMessage(); 
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.DeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("NixSingleFileTransfer"); 
//		androidJOB.JobInitiatedOnDevice(); 
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		androidJOB.CheckStatusOfappliedInstalledJob("NixSingleFileTransfer",200);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
//		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
//		androidJOB.ClickOnHomeButtonDeviceSide();
//		Reporter.log("File Transfer - Verify creating and deploying single File transfer job.",true);
	}

//	@Test(priority = 2, description = "File Transfer - Verify creating and deploying single File transfer job.")
//	public void VerifyMultiFileTransfer() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.clickOnFileTransferJob();
//		androidJOB.enterJobName("NixMultiFileTransfer");
//		androidJOB.clickOnAdd();
//		androidJOB.browseFile("./Uploads/apk/\\CreateDeployMultipleAPK");
//		androidJOB.EnterDevicePath("/sdcard/Download");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.JobCreatedMessage(); 
//		androidJOB.UploadcompleteStatus();
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.DeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("NixMultiFileTransfer"); 
//		androidJOB.JobInitiatedOnDevice(); 
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		androidJOB.CheckStatusOfappliedInstalledJob("NixMultiFileTransfer",1000);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
//		androidJOB.verifyFolderIsDownloadedInDevice("Cult Team_v1.71_apkpure.com.apk,com.myntra.android.apk");
//		androidJOB.ClickOnHomeButtonDeviceSide();
//		Reporter.log("File Transfer - Verify creating and deploying single File transfer job.",true);
//	}
	
//	@Test(priority =3, description = "File Transfer - verify file transfer job with URL.")
//	public void VerifyFileTransferWithURL() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.clickOnFileTransferJob();
//		androidJOB.enterJobName("NixFileTransferWithURL");
//		androidJOB.clickOnAdd();
//		androidJOB.enterFilePathURL(Config.EnterApkUrl);
//		androidJOB.EnterDevicePath("/sdcard/Download");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.JobCreatedMessage(); 
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.DeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("NixFileTransferWithURL"); 
//		androidJOB.JobInitiatedOnDevice(); 
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		androidJOB.CheckStatusOfappliedInstalledJob("NixFileTransferWithURL",600);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
//		androidJOB.verifyFolderIsDownloadedInDevice("astrocontacts.apk");
//		androidJOB.ClickOnHomeButtonDeviceSide();
//		Reporter.log("File Transfer - verify file transfer job with URL.",true);
//	}

	@Test(priority =4, description = "File Transfer - verify file transfer job with Multiple URLs.")
	public void VerifyFileTransferWithMultiURL() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName("NixFileTransferWithMultiURL");
		androidJOB.clickOnAdd();
		androidJOB.enterFilePathURL(Config.MultipleFileTransferURL);
		androidJOB.EnterDevicePath("/sdcard/Download");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage(); 
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("NixFileTransferWithMultiURL"); 
		androidJOB.JobInitiatedOnDevice(); 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("NixFileTransferWithMultiURL",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("surevideo.apk,surelock.apk");
		androidJOB.ClickOnHomeButtonDeviceSide();
		Reporter.log("File Transfer - verify file transfer job with Multiple URLs.",true);
	}
	@Test(priority =6, description = "File Transfer - Verify creating file transfer job with single/multiple files with foreign character in file name for Android devices")
	public void VerifyFileTransferFilesWithForeignCharacterFileNameForAndroidDevicesTC_JO_821() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName("FileTransferWithForeignCharcaterFileName");
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/Job On Android/FileTransferSingle/\\File3.exe");//D:\UplaodFiles\Job On Android\FileTransferSingle\cmo ests.pdf
		androidJOB.EnterDevicePath("/sdcard/Download");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage(); 
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("FileTransferWithForeignCharcaterFileName"); 
		androidJOB.JobInitiatedOnDevice(); 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("FileTransferWithForeignCharcaterFileName",200);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("cmo ests.pdf");
		androidJOB.verifyFolderNameDownloadedInDevice("?");
		androidJOB.ClickOnHomeButtonDeviceSide();
		Reporter.log("File Transfer - Verify creating file transfer job with single/multiple files with foreign character in file name for Android devices",true);
}	
		
	@Test(priority =7, description = "File Transfer - Verify modifying the file transfer job with foreign character in filename for Android")
	public void VerifyModifyFileTransferFilesWithForeignCharacterFileNameForAndroidDevicesTC_JO_822() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {

		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("FileTransferWithForeignCharcaterFileName");
		androidJOB.ClickOnOnJob("FileTransferWithForeignCharcaterFileName");
		androidJOB.ModifyJobButton();
		androidJOB.ClearJobName();
		androidJOB.enterJobName("EditedFileTransferWithForeignCharcaterFileName");
		androidJOB.ClickOnOkButtonModifyAndroidJob();
		androidJOB.JobmodifiedMessage(); 
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EditedFileTransferWithForeignCharcaterFileName"); 
		androidJOB.JobInitiatedOnDevice(); 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("EditedFileTransferWithForeignCharcaterFileName",200);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("cmo ests.pdf");
		androidJOB.verifyFolderNameDownloadedInDevice("?");
		androidJOB.ClickOnHomeButtonDeviceSide();
		Reporter.log("File Transfer - Verify modifying the file transfer job with foreign character in filename for Android",true);
}	
	
	

/*	@Test(priority =8, description = "File Transfer - Verify Saving install and file transfer job with foreign key.")
	public void VerifyFileTransferforeignKeyTC_JO_844() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {

		
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName("##FileTransferWithForeignKey$$");
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/Job On Android/FileTransferSingle/\\File4.exe");
		androidJOB.EnterDevicePath("/sdcard/Download");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage(); 
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("##FileTransferWithForeignKey$$"); 
		androidJOB.JobInitiatedOnDevice(); 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("##FileTransferWithForeignKey$$",360);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("##Test$$!!.pdf");
		androidJOB.ClickOnHomeButtonDeviceSide();
		Reporter.log("File Transfer - Verify Saving install and file transfer job with foreign key.",true);

	}
		
		
	*/	
				
	@Test(priority =9, description = "Verify by adding install/FT job in combination other job")
	public void MovingOtherPlatformDeviceInOneGroup() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException, AWTException {

		commonmethdpage.ClickOnHomePage();
		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName(Config.SourceGroupName);
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.ClickOnHomeGrup();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		accountsettingspage.SelectMultipleDevice();
		accountsettingspage.RightClickOndeviceInsideGroups(Config.DeviceName);
		accountsettingspage.MoveToGroupMultiple1();		
		groupspage.ClickOnHomeGrup();
        androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchLinuxDevice(Config.DeviceNameLinux);
		devicegridpage.RefreshDeviceGrid();
        accountsettingspage.SelectMultipleDevice();
		accountsettingspage.RightClickOndeviceInsideGroups(Config.DeviceNameLinux);
		accountsettingspage.MoveToGroupMultiple1();
		groupspage.ClickOnHomeGrup();
        androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchWindowsDeviceName(Config.WindowsDeviceName);
		devicegridpage.RefreshDeviceGrid();
    	accountsettingspage.SelectMultipleDevice();
		accountsettingspage.RightClickOndeviceInsideGroups(Config.WindowsDeviceName);
		accountsettingspage.MoveToGroupMultiple1();
		groupspage.ClickOnHomeGrup();
        androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchIOSDevice(Config.iOS_DeviceName);
		devicegridpage.RefreshDeviceGrid();
    	accountsettingspage.SelectMultipleDevice();
		accountsettingspage.RightClickOndeviceInsideGroups(Config.iOS_DeviceName);
		accountsettingspage.MoveToGroupMultiple1();
}	
				
	@Test(priority ='A', description = "Verify by adding install/FT job in combination other job")
	public void TC_JO_774() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException, AWTException {
	
		groupspage.ClickOnGroup(Config.SourceGroupName);
		accountsettingspage.SelectMultipleDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("NixMultiFileTransfer");
		androidJOB.VerifyDeviceChargingState("Don't Care","Plugged In");
		androidJOB.CloseApplyJobPopUp();
}		
		
	@Test(priority ='B', description = "Verify last modified date and time for job present in console")
	public void TC_JO_307() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException, AWTException {		
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("EditedFileTransferWithForeignCharcaterFileName");
		androidJOB.ClickOnOnJob("EditedFileTransferWithForeignCharcaterFileName");
		androidJOB.ModifyJobButton();
		androidJOB.ClickOnOkButtonModifyAndroidJob();
		androidJOB.JobmodifiedMessage(); 
		androidJOB.GettingLastmodifiedTime();
		
}
	
	
	
}
