package Settings_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class Security extends Initialization {
	
	@Test(priority=1,description="Security Option in Settings")
	public void VerifysecurityOp_TC_ST_533() throws InterruptedException{
		Reporter.log("\n1.Security Option in Settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.verifysecurityOption();
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority=2,description="“Enable Two-Factor Authentication” should be shown when clicked on security option from the settings ")
	public void VerifysecurityOp_TC_ST_534() throws InterruptedException{
		Reporter.log("\n2.Enable Two-Factor Authentication” should be shown when clicked on security option from the settings ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.verifysecurityOption();
		accountsettingspage.clickOnSecurity();
		accountsettingspage.validateOptionsInSecurityTab();
		accountsettingspage.closeSecurityTab();
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority=3,description="Enable Disable Two-Factor Authentication ")
	public void VerifyTwoFactorAuthentication_TC_ST_535() throws InterruptedException{
		Reporter.log("\n3.Enable Disable Two-Factor Authentication ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.verifysecurityOption();
		accountsettingspage.clickOnSecurity();
		accountsettingspage.clickOnTwoFactorAuthention();
		accountsettingspage.validatecheckbox();
		accountsettingspage.closeSecurityTab();
		commonmethdpage.ClickOnHomePage();
	}
}
