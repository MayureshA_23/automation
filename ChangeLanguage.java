package Settings_TestScripts;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class ChangeLanguage extends Initialization {
	
	@Test(priority=1 ,description="Verify UI of Change Language option") 
	public void VerifyUIofChangeLanguageoptionTC_ST_638() throws Throwable {
		Reporter.log("\n1.Verify UI of Change Language option",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ChangeLanguageVisible();
		accountsettingspage.ChooseYourLangauge();
		accountsettingspage.CloseChooseyourlanguage();
}
	
	@Test(priority=2 ,description="Verify all the modules by changing language to German") 
	public void VerifyallthemodulesbychanginglanguagetoGermanTC_ST_639() throws Throwable {
		Reporter.log("\n2.Verify all the modules by changing language to German",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ChangeLanguageVisible();
		accountsettingspage.GermanLanguageValidation();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.GermanLanguageVisible();
		accountsettingspage.GermantoEnglish();
}
	
	@Test(priority=3 ,description="Verify all the modules by changing language to French") 
	public void VerifyallthemodulesbychanginglanguagetoFrenchTC_ST_640() throws Throwable {
		Reporter.log("\n3.Verify all the modules by changing language to French",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ChangeLanguageVisible();
		accountsettingspage.FrenchLanguageValidation();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.FrenchLanguageVisible();
		accountsettingspage.FrenchtoEnglish();
}
	
	@Test(priority=4 ,description="Verify the Language remains the French when user Logs out and logs in.") 
	public void VerifytheLanguageremainsthesamewhenuserLogsoutandlogsinTC_ST_641() throws Throwable {
		Reporter.log("\n4.Verify the Language remains the French when user Logs out and logs in.",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ChangeLanguageVisible();
		accountsettingspage.FrenchLanguageValidation();
		accountsettingspage.FrenchVisible();		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.LogoutSessionFrench();
		commonmethdpage.loginAgain();
		accountsettingspage.loginBack();
		accountsettingspage.FrenchVisible();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.FrenchtoEnglish();
}
	
	@Test(priority=5 ,description="Verify the Language remains the German when user Logs out and logs in.") 
	public void VerifytheLanguageremainsthesamewhenuserLogsoutandlogsinTC_ST_643() throws Throwable {
		Reporter.log("\n5.Verify the Language remains the German when user Logs out and logs in.",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ChangeLanguageVisible();
		accountsettingspage.GermanLanguageValidation();
		accountsettingspage.GremanVisible();		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.LogoutSessionGerman();
		commonmethdpage.loginAgain();
		accountsettingspage.loginBack();
		accountsettingspage.GremanVisible();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.GermantoEnglish();
}
	
	@Test(priority=6 ,description="Verify by changing language to Spanish") 
	public void VerifybychanginglanguagetoSpanishTC_ST_793() throws Throwable {
		Reporter.log("\n6.Verify by changing language to Spanish",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ChangeLanguageVisible();
		accountsettingspage.SpanishLanguageValidation();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.SpanishLanguageVisible();
		accountsettingspage.SpanishtoEnglish();
}
	
	@Test(priority=7 ,description="Verify by changing language to Italian") 
	public void VerifybychanginglanguagetoSpanishTC_ST_796() throws Throwable {
		Reporter.log("\n7.Verify by changing language to Italian",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ChangeLanguageVisible();
		accountsettingspage.ItalianLanguageValidation();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ItalianLanguageVisible();
		accountsettingspage.ItaliantoEnglish();
}
	
	
	
	
	
	
	
	
	
}
