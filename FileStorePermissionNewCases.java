package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;


//PRECONDITION- ACC should be in the form of Enterprise,Standard,Legacy and Premium

public class FileStorePermissionNewCases extends Initialization {

	@Test(priority='1',description="1.Verify file store permissions in user management in Enterprise Account")
	public void TC_ST_01EnterpriseAcc() throws InterruptedException{
		Reporter.log("\n1.Verify file store permissions in user management in Enterprise Account",true);
		usermanagement.NavigateUrl(Config.EnterpriseAccURL);
		usermanagement.LoginAsNewUser(Config.EnterpriseAccUser,Config.EnterpriseAccPwd);
		usermanagement.WaitForLogin();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.VerifyFileStorePermissionRole();
		usermanagement.RolesClose();
	}	
	@Test(priority='2',description="2.Verify file store permissions in user management in PremiumAcc Account")
	public void TC_ST_01PremiumAcc() throws InterruptedException{
		Reporter.log("\n2.Verify file store permissions in user management in PremiumAcc Account",true);
		usermanagement.NavigateUrl(Config.PremiumAccURL);
		usermanagement.LoginAsNewUser(Config.PremiumAccUser,Config.PremiumAccPwd);
		usermanagement.WaitForLogin();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.VerifyFileStorePermissionRole();
		usermanagement.RolesClose();
	}	
	@Test(priority='3',description="3.Verify file store permissions in user management in Legacy Account")
	public void TC_ST_01Legacy() throws InterruptedException{
		Reporter.log("\n3.Verify file store permissions in user management in Legacy Account",true);
		usermanagement.NavigateUrl(Config.LegacyAccURL);
		usermanagement.LoginAsNewUser(Config.LegacyAccUser,Config.LegacyAccPwd);
		usermanagement.WaitForLogin();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.VerifyFileStorePermissionRole();
		usermanagement.RolesClose();
	}	
	@Test(priority='4',description="4.Verify file store permissions in standard accounts")
	public void TC_ST_02() throws InterruptedException{
		Reporter.log("\n4.Verify file store permissions in standard accounts",true);	
		usermanagement.NavigateUrl(Config.StandardAccURL);
		usermanagement.LoginAsNewUser(Config.StandardAccUser,Config.StandardAccPwd);
		usermanagement.WaitForLogin();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.VerifyFileStorePermissionInSatndardAccount();
		usermanagement.RolesClose();

	}

}
