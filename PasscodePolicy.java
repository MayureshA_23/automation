package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class PasscodePolicy extends Initialization{
	
	@Test(priority='1',description="1.To verify clicking on passcode policy option")
	public void VerifyClickingOnPasscodePolicy() throws InterruptedException{
		profilesAndroid.ClickOnProfile();
		profilesiOS.clickOniOSOption();
		profilesiOS.AddProfile();
		profilesiOS.ClickOnPasscodePolicyOption();
		}
	
	@Test(priority='2',description="2.To verify summary of passcode policy Option")
	public void VerifySummaryofPasscodePolicy() throws InterruptedException
	{
		profilesiOS.VerifySummaryof_PasscodePolicy();
	}
	
	@Test(priority='3',description="3.To Verify warning message Saving passcode policy profile without name")
	public void VerifyWarningMessageOnSavingBrandingProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving passcode policy profile without name=========");
		profilesiOS.ClickOnConfigureButton_PasscodePolicy();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='4',description="4.To Verify parameters of passcode policy")
	public void VerifyParametersOfPasscodePolicy() throws InterruptedException{
		Reporter.log("=======To Verify parameters of passcode policy=========");
		profilesiOS.VerifyPasscodePolicyParameters();
	}
	@Test(priority='5',description="5.To Verify Saving a passcode policy")
	public void VerifyWarningMessageOnSavingABrandingProfileEnteringRequiredFields() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify Saving a passcode policy=========");
		profilesiOS.EnterPasscodePolicyProfileName();
		profilesiOS.ClickOnForceUserToSetPasscode();
		profilesiOS.ClickOnRequireAlphanumericValue();
		profilesiOS.VerifyChoosingMinimumPasscodeLength();
		profilesiOS.EnterPasscodeAge();
		profilesiOS.EnterPasscodeHistory();
		profilesiOS.ClickOnSavebutton();
	    profilesAndroid.NotificationOnProfileCreated();
	   
		
	}
	
		
	
}
	
