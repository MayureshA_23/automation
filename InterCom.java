package Settings_TestScripts;
import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;


public class InterCom extends Initialization{
	
	@Test(priority = 1, description = "Verify intercomis displaying for SuperUser TC_ST_476")
	public void VerifyintercomisdisplayingforSuperUserTC_ST_476() throws InterruptedException {
		Reporter.log("\n1.Verify alert template feature in Account Settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.AccountSettings();
		accountsettingspage.accountManagement();
		accountsettingspage.IntercomaccessforUserDropDown();
		accountsettingspage.checkForIntercomaccessforUserConfiemationMessage();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.logoutFromAccount();
		accountsettingspage.loginAgainLink();
		accountsettingspage.EnterUNToLoginToConsole("Mithilesh_Superuser");
		accountsettingspage.EnterPassAndClickLoginToConsole("Mithilesh@123");
		accountsettingspage.verifyForInterCom();
		accountsettingspage.clickHome();
	}
	

}
