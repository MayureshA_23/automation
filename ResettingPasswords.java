package UserManagement_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//Demouser1 and Demouser2 and ALLRolespermissions needs to be deleted and Demouser2 should be kept launched 

public class ResettingPasswords extends Initialization {

	@Test(priority = '1', description = "1.Verify sub user-1 resetting password for other sub user-2 from user management")
	public void TC_ST_448() throws InterruptedException {
		Reporter.log("\n1.Verify sub user-1 resetting password for other sub user-2 from user management", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.RoleWithAllPermissions("ALLRolespermissions", " user Roles");
		usermanagement.CreateDemoUser1(Config.InputUser1, Config.pswd, Config.pswd, "ALLRolespermissions");
		usermanagement.CreateDemoUser2(Config.InputUser2, Config.pswd, Config.pswd, "ALLRolespermissions");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.InputUser1, Config.pswd);
		usermanagement.UsermanagementWindowsSwitch();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.InputUser2, Config.pswd);
		usermanagement.SwitchToMainWindow();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.SearchUserTextField(Config.InputUser2);
		usermanagement.selectUser(Config.InputUser2, Config.NewPwd, Config.RetypePwd);
		usermanagement.SwitchToChildWindow();
		usermanagement.ClickONallDevices();
		usermanagement.CloseBrowsers();
		usermanagement.SwitchToMainWindow();
	}

	@Test(priority = '2', description = "2.Verify all Sub-user active sessions are getting logged out when super user resets the password from user management")
	public void TC_ST_446()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log(
				"\n2.Verify all Sub-user active sessions are getting logged out when super user resets the password from user management",
				true);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);

		usermanagement.UsermanagementWindowsSwitch();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.InputUser1, Config.pswd);

		usermanagement.SwitchToMainWindow();

		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.SearchUserTextFieldDemouser1(Config.InputUser1);
		usermanagement.selectDemoUser1(Config.InputUser1, Config.NewPwdAgain, Config.RetypePwdAgain);

		usermanagement.SwitchToChildWindow();
		usermanagement.ClickONallDevices();
		usermanagement.CloseBrowsers();
		usermanagement.SwitchToMainWindow();
		commonmethdpage.ClickOnHomePage();

	}
	// @Test(priority='3',description="3.Verify Super user resetting password from
	// user management")
	// public void TC_ST_447() throws InterruptedException{
	// Reporter.log("\n3.Verify Super user resetting password from user
	// management",true);
	// commonmethdpage.ClickOnSettings();
	// usermanagement.logoutAsAdmin();
	// loginPage.ClickOnLoginAgainLink();
	// usermanagement.LoginAsNewUser(Config.userID, Config.password);
	//
	// usermanagement.UsermanagementWindowsSwitch();
	// commonmethdpage.ClickOnSettings();
	// usermanagement.logoutAsAdmin();
	// loginPage.ClickOnLoginAgainLink();
	// usermanagement.LoginAsNewUser(Config.userID, Config.password);
	//
	// usermanagement.SwitchToMainWindow();
	// commonmethdpage.ClickOnSettings();
	// usermanagement.ClickOnUserManagementOption();
	// usermanagement.VerifyUserManagementIsDisplayed();
	// usermanagement.SearchSuperUserTextField(Config.userID);
	// usermanagement.selectSuperUser("42Gears@000", "42Gears@000"); //current pwd-
	// 345
	// usermanagement.SwitchToChildWindow();
	// usermanagement.ClickONallDevices();
	// usermanagement.CloseBrowsers();
	// usermanagement.SwitchToMainWindow();
	// commonmethdpage.ClickOnHomePage();
	// commonmethdpage.ClickOnSettings();
	//
	// usermanagement.logoutAsAdmin();
	//// loginPage.ClickOnLoginAgainLink();
	// usermanagement.LoginAsNewUser(Config.userID,"42Gears@000");
	// commonmethdpage.ClickOnSettings();
	// usermanagement.ClickOnUserManagementOption();
	// usermanagement.VerifyUserManagementIsDisplayed();
	// usermanagement.SearchSuperUserTextField(Config.userID);
	// usermanagement.selectSuperUser(Config.password,Config.password); //current
	// pwd- 345
	//
	// commonmethdpage.ClickOnSettings();
	// usermanagement.logoutAsAdmin();
	//// loginPage.ClickOnLoginAgainLink();
	// usermanagement.LoginAsNewUser(Config.userID,Config.password);
	// commonmethdpage.ClickOnSettings();
	// usermanagement.ClickOnUserManagementOption();
	// usermanagement.VerifyUserManagementIsDisplayed();
	//
	// }

}
