package DynamicJobs_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DataUsage extends Initialization
{
  @Test(priority=0,description="Verify Telecom management turn On Pop up validate Yes button")
   public void VerifyTelecomManagementPopUpYesButton() throws Throwable
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnTelecomManagementEditButton();
	   dynamicjobspage.DisableTelecomManagement();
	   dynamicjobspage.ClickOnTelecomManagementOkButton();
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.ClickOnDataUsageYesButton();
	   dynamicjobspage.EnableTelecomManagement();
	   dynamicjobspage.ClickOnTelecomManagementOkButton();
	   dynamicjobspage.ConfirmationMessageTelecomManagement();
   }
   
   @Test(priority=1,description="Verify Telecom management turn On Pop up validate No button")
   public void VerifyTelecomManagementPopUpNoButton() throws Throwable
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnTelecomManagementEditButton();
	   dynamicjobspage.DisableTelecomManagement();
	   dynamicjobspage.ClickOnTelecomManagementOkButton();
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.ClickOnDataUsageNoButton();
	   dynamicjobspage.VerifyNoDataMessageIsShown();
	   dynamicjobspage.ClickOnDataUsageRefreshButton();
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.ClickOnDataUsageNoButton();
	   dynamicjobspage.VerifyMobileDataIsNotShownAfterRefreshing();
	   dynamicjobspage.VerifyWifiDataIsShownAfterRefreshing();	  
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=2,description="Verify Data usage window for android device")
   public void VerifyDataUsagewindow() throws Throwable
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	  dynamicjobspage.ClickOnTelecomManagementEditButton();
	   dynamicjobspage.EnableTelecomManagement();
	   dynamicjobspage.ClickOnTelecomManagementOkButton();
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.VerifyDataUsagePeriod();
	   dynamicjobspage.VerifyStartDateAndEndDateOFBillingCycle();
	   dynamicjobspage. VerifyDataUsageGraphIsDisplayed();
	   dynamicjobspage.ClickOnWifiDataUsageSection();
	   dynamicjobspage. VerifyDataUsageGraphIsDisplayed();
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=3,description="Verify Editing in Current Billing cycle Data usage window")
   public void VerifyEditingCurrentBillingCycle() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.VerifyEditingInCurrentBillingCycleDataUsage();
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=4,description="Verify the particular app consuming data in the list of apps")
   public void VerifyParticularAppsConsumingData() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.ClickOnWifiDataUsageSection();
	   dynamicjobspage.VerifyParticularApplicationConsumingDataInListOfApps("YouTube");
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=5,description="Verify back arrow button present in per app data page.")
   public void VerifybackArrowButtonInPerAppDataPage() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.ClickOnWifiDataUsageSection();
	   dynamicjobspage.ClickOnapplicationnameInDataUsagePage("YouTube");
	   dynamicjobspage.ClickOnDatausageBackArrowButton();
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=6,description="Verify Custom billing cycle")
   public void VerifyCustomBillingCycle() throws Throwable
   {
//	 commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//	   accountsettingspage.UninstallingSurefox("SureMDM Agent","com.nix");
	  deviceGrid.InstallApplications_ADB("3200ec4c4e7a762d","\"D:\\UplaodFiles\\Job On Android\\nixagentv21.77.apk\"");
	   commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
	   dynamicjobspage.NixAdminSettings();
	   dynamicjobspage.EnterSureMDMAccountID(Config.AccId);
	   commonmethdpage.ClickOnAppiumButtons("Register");
	   dynamicjobspage.EnterRegisterServerPath(Config.ServerPath);
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnRefreshButton();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.ClickOnDataUsageRefreshButton();
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.SelectCustomBillingCycle();
	   dynamicjobspage.ClickOnUpdateButton();
	   dynamicjobspage.VerifyDataUsageGraphIsDisplayed();
	   dynamicjobspage.ClickOnWifiDataUsageSection();
	   dynamicjobspage.VerifyDataUsageGraphIsDisplayed();
	   dynamicjobspage.ClickONAllDataUsageSection();
	   dynamicjobspage.VerifyDataUsageGraphIsDisplayed(); 
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=7,description="Data usage-Verify Custom billing cycle Future date")
   public void VerifyCustomBillingCycleFutureDate() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnMoreOption();
	   dynamicjobspage.ClickOnDataUsage_UM();
	   dynamicjobspage.SelectingBillingCycle("Custom");
	   dynamicjobspage.VerifyCustombillingFutureDate(dynamicjobspage.DataUsageEndDate);
	   dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=8,description="Verify data usage by upgrading from 14.26 to latest nix in above Lollipop device .")
   public void VerifyDataUsageFromUpgradingNix() throws Throwable
   {
	    androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("nix_upgrade");
		dynamicjobspage.enterJobNameNixUpgrade("NixUpgrade_Automation");
		dynamicjobspage.enterNixAgentLink(Config.NixDownloadLink);
		dynamicjobspage.nixUpgradeOkButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("NixUpgrade_Automation");
		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.JobInitiatedMessage();
		androidJOB.CheckingJobStatusUsingConsoleLogs("NixUpgrade_Automation");
	   commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnRefreshButton();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnDataUsage_UM();
		dynamicjobspage.ClickOnDataUsageRefreshButton();
		dynamicjobspage.ClickOnDataUsageCloseBtn();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnDataUsage_UM();
		dynamicjobspage.VerifyDataUsageGraphIsDisplayed();
		dynamicjobspage.ClickOnWifiDataUsageSection();
		dynamicjobspage.VerifyDataUsageGraphIsDisplayed();
		dynamicjobspage.ClickOnShowlegacyDataUsage();
		dynamicjobspage.VerifyDataUsageGraphIsDisplayed();
		dynamicjobspage.ClickOnWifiDataUsageSection();
		dynamicjobspage.VerifyDataUsageGraphIsDisplayed(); 
		dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
   
   @Test(priority=9,description="Verify CBC(Current Billing Cycle)under billing cycle drop down.")
   public void VerifyCBCUnderBillingCycleDropDown() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
   {
	   commonmethdpage.SearchDevice(Config.DeviceName);
	   dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnDataUsage_UM();
		dynamicjobspage.VerifyDataUsagePeriod();
		dynamicjobspage.ClickOnDataUsageCloseBtn();
   }
	
	
	
   
}
