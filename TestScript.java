package TrialTest_1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class TestScript extends Initialization {
	
	@Test(priority='1',description="Mithilesh trial") 
	public void AccountSettings() throws Throwable {

		Reporter.log("",true);
		accountRegistrationPage.logoutfromApp();
		commonmethdpage.loginAgain();
		accountRegistrationPage.enterUsereName(Config.userID);
		accountRegistrationPage.enterPassword(Config.password);
		accountRegistrationPage.pressEnterToLogin();
		driver.manage().timeouts().implicitlyWait(60 , TimeUnit.SECONDS);	
		driver.findElement(By.xpath("//*[@id=\"userProfileButton\"]")).click();
		//commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOniosSettings();
		accountsettingspage.UploadpemCertificate();
		driver.findElement(By.xpath("//*[@id=\"certificate_pem_upload\"]")).click();
		
		androidJOB.browseFile("./Uploads/UplaodFiles/MDM_ 42Gears Mobility Systems Private Limited_Certificate.pem/");
		


		Reporter.log("Pass >>Verify Login to SureMDM console with valid credentials and press \"Enter\" button through keyboard.",true);
	}

}
