package JobsOnAndroid;
import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import PageObjectRepository.DeviceEnrollmentHonor8_ScanQR_POM;

public class NixAgentSettingsJob extends Initialization {

	@Test(priority=1,description="\n Verfying Nix Agent Settings - Verify creating Nix agent settings jobs.")
	public void CheckingUIofNixAgentSettingsJobs() throws InterruptedException {

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickofNixSettingsJob();
		androidJOB.VerifycreatingNixagentsettingsjobsUI();
		androidJOB.ClickOnnixAgent_modalCloseBtn();

	}

	@Test(priority=2,description="\n Nix Agent Settings - Verify creating and deploying Nix agent settings by Enabling time synchronization with server.")
	public void CreatingAndDeployingNixAgentSettingJobEnablingtimesynchronizationwithserver() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();

		androidJOB.ClickofNixSettingsJob();

		androidJOB.Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Enable_time_synchronization_with_server();
		androidJOB.NixAgentSettingsJobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("NixAgentEnableTimeSyncWithServer");
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("NixAgentEnableTimeSyncWithServer",360);
		androidJOB.DeviceSideTimeSyncWithServerNixAgentSettingJob();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
//		androidJOB.ClikOnNixSettings();
		commonmethdpage.commonScrollMethod("Uninstall Suremdm Nix");
		androidJOB.AfterDepolyementValidationWhetherDDeviceSideTimeIsChangedAfterFiveMins();
		androidJOB.clickingOnDoneButton();  
	}

	@Test(priority=3,description="\n ModifyNix Agent Settings Jobs")
	public void ModificationNixAgentJob() throws InterruptedException, IOException {

		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("NixAgentEnableTimeSyncWithServer");
		androidJOB.ClickOnOnJob("NixAgentEnableTimeSyncWithServer");
		androidJOB.selectJobEnablingAllOptions();
		androidJOB.ModifyJobButton();
		androidJOB.ForModification();
		androidJOB.modifyNixAgentJob();
		androidJOB.VerfyModificationSuccessMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDeviceInconsole();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("NixAgentEnableTimeSyncWithServer");
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("NixAgentEnableTimeSyncWithServer",360);  
		androidJOB.DeviceSideTimeSyncWithServerNixAgentSettingJob();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
//		androidJOB.ClikOnNixSettings();
		commonmethdpage.commonScrollMethod("Uninstall Suremdm Nix");
		androidJOB.AfterDepolyementValidationWhetherDDeviceSideTimeIsChangedAfterFiveMins();
		androidJOB.clickingOnDoneButton();
	}


	@Test(priority=4,description="\n Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Enable_Schedule_Reboot ")
	public void EnableScheduleReboot() throws InterruptedException, AWTException, IOException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickofNixSettingsJob();
		androidJOB.Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Enable_Schedule_Reboot();
		androidJOB.SelectDayForScheduleReboot();
		androidJOB.NixAgentSettingsJobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDeviceInconsole();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableScheduleRebootNixAgentSettigsJob");
		androidJOB.JobInitiatedMessage(); 

		androidJOB.CheckStatusOfappliedInstalledJob("EnableScheduleRebootNixAgentSettigsJob",360);
		androidJOB.WaitForShutdownn();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.AppiumSideVerificationScheduleShutDown();
	}

	@Test(priority=5,description="\n Nix Agent Settings - Verify creating and deploying Nix agent settings by Enable Nix Password.")
	public void EnableNixPassword() throws InterruptedException, IOException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickofNixSettingsJob();
		androidJOB.Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Nix_Password();
		androidJOB.NixAgentSettingsJobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDeviceInconsole();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("NixAgentEnableNixPassword");
		androidJOB.JobInitiatedMessage(); 
		androidJOB.CheckStatusOfappliedInstalledJob("NixAgentEnableNixPassword",360);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		androidJOB.SendingPasswordToEnterNixSettings("0000");
		androidJOB.DisableNixChangePwd("0000");
		androidJOB.clickingOnDoneButton();


	}
    @Test(priority=6,description="Nix Agent Settings - Verify Creating and deploying Nix agent settings with all options and by specifying the connection type.")
    public void VerifyDeployingNixSettingsBySpecifyingConnectionType() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("nix_settings");
		androidJOB.SendingNixAgentSettingsJob("Nix Agent Settings_Automation");
		androidJOB.SelectingOptionsInNixAgentsettingsJob("job_nixsettings_EnableTimeSync_input");
		androidJOB.SelectingOptionsInNixAgentsettingsJob("job_nixsettings_EnablePeriodicUpdate_input");
		androidJOB.SelectingOptionsInNixAgentsettingsJob("job_nixsettings_EnableNixPassword_input");
		androidJOB.SelectingConnectionTypeInNixAgentSettingsJob("job_nixsettings_ConnType_input","WIFI");
		androidJOB.ClickOnLocatioTrackingJobOkButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Nix Agent Settings_Automation");
		androidJOB.CheckingJobStatusUsingConsoleLogs("Nix Agent Settings_Automation");	
    }

// dont run

/*	@Test(priority=5,description="\n Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Enable_Schedule_ShutDown ")
	public void EnableScheduleShutdown() throws InterruptedException, AWTException, IOException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickofNixSettingsJob();
		androidJOB.Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Schedule_ShutDown();
		androidJOB.SelectDayForScheduleShutdown();
		androidJOB.NixAgentSettingsJobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableScheduleShutDownNixAgentSettigsJob");
		androidJOB.JobInitiatedMessage(); 
		androidJOB.ClickOnApplyButton();
		androidJOB.CheckStatusOfappliedInstalledJob(Config.ScheduleShutDownJob,360);
		androidJOB.AppiumConfigurationforNIX();
		androidJOB.AppiumSideVerificationScheduleShutDown();
	}


	@Test(priority=6,description="\n Enabling all options and deploying the jobs")
	public void EnablingAllOptionsAndDeployingTheJobs() throws InterruptedException, AWTException, IOException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickofNixSettingsJob();
		androidJOB.Verify_Creating_and_deploying_Nix_agent_settings_with_all_options_and_by_specifying_the_connectiontype();
		androidJOB.SelectDayForScheduleReboot1();

		androidJOB.EnableScheduleShutDow1();
		androidJOB.SelectDayForScheduleShutdown1();

		androidJOB.SelectingConnectionTypeDropDown();
		androidJOB.NixAgentSettingsJobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnablingAllOptions");
		androidJOB.SelectJob("EnablingAllOptions");
		androidJOB.ClickOnApplyButton();
		androidJOB.clickonJobQueue();
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnableAllOptions,360);  
		androidJOB.AppiumConfigurationforNIX();
		androidJOB.ClikOnNixSettings();
		androidJOB.ScrollDeviceSyncTime();
		androidJOB.AfterDepolyementValidationWhetherDDeviceSideTimeIsChangedAfterFiveMins();
	}

	@Test(priority=7,description="\n AppiumSideAll Options and Deploying ScheduleReboot")
	public void AppiumDeviceVerifyAllOptionsReboot() throws IOException, InterruptedException {
		androidJOB.AppiumConfigurationforNIX();
		androidJOB.AppiumSideVerificationScheduleShutDown();
		androidJOB.AppiumConfigurationforNIX();
		androidJOB.AppiumSideVerificationScheduleShutDown();
	}

	@Test(priority=8,description="\n AppiumSideAll Options and Deploying Nix password")
	public void AppiumDeviceVerifyAllOptionsNixPassw() throws IOException, InterruptedException {
		androidJOB.AppiumConfigurationforNIX();
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		androidJOB.SendingPasswordToEnterNixSettings("0000");
	}

*/
}





