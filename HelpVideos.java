package Settings_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class HelpVideos extends Initialization{

	@Test(priority=1,description="1.Verifying Help Videos option in console")
	public void Verifying_Help_Videos_optionInConsole() throws InterruptedException{
		Reporter.log("\n1.Verifying Help Videos option in console",true);
		Reporter.log("");
		commonmethdpage.ClickOnSettings();
		helpPage.ClickOnHelp();
		helpVideos.ClickOnHelpvideos();
	}
	
	@Test(priority=2,description="1.Verifying Search video in Help Videos")
	public void VerifyingSearchOptionInHelpVideos() throws InterruptedException{
		Reporter.log("\n1.Verifying Search video in Help Videos",true);
		
		helpVideos.VerifyingSearchOptionIHelpVideos();
	}
	
//	@Test(priority = 3, description = "Verify Help video in sureMDM console Help section TC_ST_705")
//	public void VerifyHelpvideoinsureMDMconsoleHelpsectionTC_ST_705() throws InterruptedException {
//		Reporter.log("\n1.Verify Help video in sureMDM console Help section TC_ST_705",true);
//		commonmethdpage.ClickOnSettings();
//		helpPage.ClickOnHelp();
//		helpVideos.ClickOnHelpvideos();
//		helpVideos.verifyVedioIsDisplayingInHelpVedio();
//		helpVideos.clickHelpVedioCloseButton();
//		helpVideos.clickHelpCloseButton();
//	}
	
}