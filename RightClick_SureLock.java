package RightClick;


import org.testng.Reporter;

import java.io.IOException;


import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;
 // Make sure SureLock is not installed on the device
public class RightClick_SureLock extends Initialization {

	@Test(priority='1',description="1.To Verify Install Option SureLock RightClick")
	public void VerifyInstallOptionSureLock() throws Exception {
		Reporter.log("\n1.To Verify Install Option SureLock RightClick",true);
		commonmethdpage.SearchDeviceInconsole();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureLock();
		rightclickDevicegrid.VerifyInstallOptionWhenSLnotInstalled();
		commonmethdpage.ClickOnDynamicRefresh();
	}
	@Test(priority='2',description="2.To verify Activate and Launch SureLock Options")
	public void VerifyUpgradeActivateDeactivateOptions() throws Throwable {
		Reporter.log("\n2.To verify Activate and Launch SureLock Options",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.InstallationSureLockLowerversion();
		commonmethdpage.ClickOnDynamicRefresh();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureLock();
		rightclickDevicegrid.VerifyActivateLicenseLaunchSureLockOptionAndUpgradeOption();
	}
	
	@Test(priority='3',description="3.To verify Activate License")
	public void VerifyActivateLicenseAndLaunchSureLockOption() throws Exception {
		Reporter.log("\n3.To verify Activate License",true);	
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureLock();
		rightclickDevicegrid.ActivationOfSureLockRightClick(Config.SLActivationCode);
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnDynamicRefresh();	
	}

	@Test(priority='4',description="4.To verify Deactivate Option SureLock Settings Right Click")
	public void VerifyDeactivateOptionSureLock() throws Exception 
	{
		Reporter.log("\n4.To verify Deactivate Option SureLock Settings Right Click",true);
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnDynamicRefresh();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureLock();
		rightclickDevicegrid.VerifyDeactivateSureLockRightClick();
		rightclickDevicegrid.ClickOnrefreshButtonDeviceInfo();
		
	}
	
	@Test(priority='5',description="5.To Verify Deactivation SureLock Functionality")
	public void VerifyDeactivationRightClickSureLock() throws InterruptedException 
	{
		Reporter.log("\n5.To Verify Deactivation SureLock Functionality",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnSureLock();
		rightclickDevicegrid.DeactivateSureLockRightClick();//common
		rightclickDevicegrid.ClickOnRefreshDeviceGrid();
	}	
}
	
