package Settings_TestScripts;

import java.io.IOException;
import java.util.Date;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;


public class MiscellaneousSettings extends Initialization{
	
	
	@Test(priority=1,description="1.To Verify Miscellaneous Settings with Default Connectivity For Install/File Transfer Job")
	public void VerifyMiscSettingsDefaultConnectivity() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n1.To Verify Miscellaneous Settings with Default Connectivity For Install/File Transfer Job",true);
	    accountsettingspage.GoToMiscSettings();
	    accountsettingspage.DefaultConnectivity();
	    Reporter.log("PASS >> Verification of Miscellaneous Settings with Default Connectivity For Install/File Transfer Job",true);
	 }
	
	@Test(priority=2,description="2.To Verify Miscellaneous Settings with Use Old Remote Support Checked")
	public void VerifyMiscSettingsOldRemoteSupport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n2.To Verify Miscellaneous Settings with Use Old Remote Support Checked",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.SelectUseOldRemoteSupport();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		accountsettingspage.ConfirmationMessageVerify(true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfSwitchToOldRemoteSupport();
		remotesupportpage.SwitchToMainWindow();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Use Old Remote Support Checked",true);
	}
	
	
	@Test(priority=3,description="3.To Verify Miscellaneous Settings with Use Old Remote Support UnChecked")
	public void VerifiyMiscSettingDisablingOldRemoteSupport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n3.To Verify Miscellaneous Settings with Use Old Remote Support UnChecked",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.UnSelectUseOldRemoteSupport();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		accountsettingspage.ConfirmationMessageVerify(true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfSwitchToNewRemoteSupport();
		remotesupportpage.SwitchToMainWindow();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Use Old Remote Support UnChecked",true);
	}
	
	@Test(priority=4,description="4.To Verify Miscellaneous Settings with Provide Remote Access to 42Gears Support Team is Granted")
	public void VerifyProvideRemoteAccessGranted() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n4.To Verify Miscellaneous Settings with Provide Remote Access to 42Gears Support Team is Granted",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.ClickOnGrantForRemoteAccess();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnMiscellaneousSettings();
		accountsettingspage.VerifyGrantForRemoteAccess();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Provide Remote Access to 42Gears Support Team is Granted",true);
	}
	
	@Test(priority=5,description="5.To Verify Miscellaneous Settings with Provide Remote Access to 42Gears Support Team is Revoked")
	public void VerifyProvideRemoteAccessRevoked() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n5.To Verify Miscellaneous Settings Provide Remote Access to 42Gears Support Team is Revoked",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.ClickOnRevokeForRemoteAccess();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnMiscellaneousSettings();
		accountsettingspage.VerifyRevokeForRemoteAccess();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Provide Remote Access to 42Gears Support Team is Revoked",true);
	}
	
	
	@Test(priority=6,description="6.To Verify Miscellaneous Settings with Don't Pause Screen Capture Checked")
	public void VerifyMiscSettingsDontPauseScreenEnabled() throws InterruptedException{
		Reporter.log("6.To Verify Miscellaneous Settings with Don't Pause Screen Capture Checked",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.SelectPauseScreenCapture();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnMiscellaneousSettings();
		accountsettingspage.VerifyPauseScreenCaptureSelected();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Use GCM Checked",true);
		}
	
	
	@Test(priority=7,description="7.To Verify Miscellaneous Settings with Don't Pause Screen Capture UnChecked")
	public void VerifyMiscSettingsDontPauseScreenDisabled() throws InterruptedException{
		Reporter.log("\n7.To Verify Miscellaneous Settings with Don't Pause Screen Capture UnChecked",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.UnSelectPauseScreenCapture();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnMiscellaneousSettings();
		accountsettingspage.VerifyPauseScreenCaptureUnSelected();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Don't Pause Screen Capture UnChecked",true);
		}
	
	
	@Test(priority=8,description="8.To Verify Miscellaneous Settings with Zip All Downloads Checked")
	public void VerifyMiscSettingsZipAllDownloadEnabled() throws InterruptedException{
		Reporter.log("8.To Verify Miscellaneous Settings with Zip All Downloads Checked",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.SelectZipAllDownloads();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnMiscellaneousSettings();
		accountsettingspage.VerifyZipAllDownloadsSelected();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Zip All Downloads Checked",true);
		}
	
	@Test(priority=9,description="9.To Verify Miscellaneous Settings with Zip All Downloads UnChecked")
	public void VerifyMiscSettingsZipAllDownloadDisbled() throws InterruptedException{
		Reporter.log("\n9.To Verify Miscellaneous Settings with Zip All Downloads UnChecked",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.UnSelectPauseScreenCapture();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnMiscellaneousSettings();
		accountsettingspage.VerifyPauseScreenCaptureUnSelected();
		Reporter.log("PASS >> Verification of Miscellaneous Settings with Zip All Downloads UnChecked",true);
		}
	@Test(priority=10,description="10.Enable and disable option in Miscellaneous Settings")
	public void VerifyDisaableNPSSurvyOption() throws InterruptedException{
		Reporter.log("\n10.Enable and disable option in Miscellaneous Settings",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.verifyDisableNPSOPtion();
		commonmethdpage.ClickOnHomePage();
		}
	@Test(priority=11,description="11.Disable the NPS From settings ")
	public void VerifyDisaableNPSSurvyOptionSuccessMsg() throws InterruptedException{
		Reporter.log("\n11.Disable the NPS From settings",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.verifyDisableNPSOPtion();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		accountsettingspage.ConfirmationMessageVerify(true);
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority=12,description="12.Verify the new option which is added under miscellaneous settings 'Export Settings'")
	public void VerifyExportSettings() throws InterruptedException{
		Reporter.log("\n12.Verify the new option which is added under miscellaneous settings 'Export Settings'",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.verifyExportsetting();
		accountsettingspage.VerifyExportSettingOpInDropDown();
		commonmethdpage.ClickOnHomePage();
	}
	
//	@Test(priority=13,description = "Verify UI of change language setting")
//	public void verifyLaunuages_TC_ST_792() throws InterruptedException
//	{
//	     Reporter.log("\n13.Verify UI of change language setting",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.clickOnChangeLanguage();
//		accountsettingspage.validateLanguages();
//		accountsettingspage.ChangeLanguageCloseBtn();
//	}

        @Test(priority=14,description="Verify the enable advance device management should be displayed")
	public void VerifytheenableadvancedevicemanagementshouldbedisplayedTC_ST_830() throws InterruptedException{
		Reporter.log("\n14.Verify the enable advance device management should be displayed",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonMiscellaneousSetting();
		accountsettingspage.EnableAdvancedDeviceManagement();		
}

	   @Test(priority=15,description="Verify Default connectivity-WiFi only for Install Application")
	   public void VerifyDefaultconnectivityAnyNetworkforInstallApplicationTC_ST_335() throws InterruptedException{
		 Reporter.log("\n15.Verify Default connectivity-Any Network for Install Application",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.DefaultconnectivityoptionforInstallFileTransferjobs();
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 androidJOB.clickOnJobs();
		 androidJOB.clickNewJob();
		 androidJOB.clickOnAndroidOS();	 
		 androidJOB.ClickOnInstallApplication();		 
		 androidJOB.enterJobName(Config.EnterTrialInstallJob);
		 androidJOB.clickOnAddInstallApplication();
		 androidJOB.enterFilePathURL(Config.EnterApkUrl);
		 androidJOB.clickOkBtnOnIstallJob();
		 androidJOB.clickOkBtnOnAndroidJob();
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.SearchAutomationDevice();
		 commonmethdpage.ClickOnApplyButton();
		 accountsettingspage.SelectTrialsInstallJob();
		 accountsettingspage.DeployjobonVisible();
		 accountsettingspage.ClickonJobApplyButton();
		 accountsettingspage.JobButtonVisible();
		 accountsettingspage.SelectTrialsInstallJobAgain();
		 commonmethdpage.ClickOnHomePage();	
}

	@Test(priority=16,description="Verify Default connectivity-WiFi only for File Transfer")
	   public void VerifyDefaultconnectivityAnyNetworkforFileTransferTC_ST_336() throws InterruptedException{
		 Reporter.log("\n16.Verify Default connectivity-Any Network for File Transfer",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.DefaultconnectivityoptionforInstallFileTransferjobs();
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 androidJOB.clickOnJobs();
		 androidJOB.clickNewJob();
		 androidJOB.clickOnAndroidOS();			 
		 androidJOB.clickOnFileTransferJob();		 
		 androidJOB.enterJobName(Config.EnterTrialFileTransferJob);		 
		 androidJOB.clickOnAddInstallApplication();
		 androidJOB.enterFilePathURL(Config.EnterApkUrl);
		 androidJOB.clickOkBtnOnIstallJob();
		 androidJOB.clickOkBtnOnAndroidJob();
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.SearchAutomationDevice();
		 commonmethdpage.ClickOnApplyButton();		 
		 accountsettingspage.SelectTrialsTrialsFileTransferJob();
		 accountsettingspage.DeployjobonVisible();
		 accountsettingspage.ClickonJobApplyButton();
		 accountsettingspage.JobButtonVisible();	 
		 accountsettingspage.SelectTrialsFileTransferJobAgain();
		 commonmethdpage.ClickOnHomePage();	
}
	
	   
	   @Test(priority=17,description="Verify Default connectivity-Mobile Data Only Install Application")
	   public void VerifyDefaultconnectivityMobileDataOnlyInstallApplicationTC_ST_337() throws InterruptedException{
		 Reporter.log("\n17.Verify Default connectivity-Mobile Data Only Install Application",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.DefaultconnectivityMobileDataOnlyInstallFileTransferjobs();//
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 androidJOB.clickOnJobs();
		 androidJOB.clickNewJob();
		 androidJOB.clickOnAndroidOS();	 
		 androidJOB.ClickOnInstallApplication();		 
		 androidJOB.enterJobName(Config.EnterTrialMobileDataOnlyJob);//
		 androidJOB.clickOnAddInstallApplication();
		 androidJOB.enterFilePathURL(Config.EnterApkUrl);
		 androidJOB.clickOkBtnOnIstallJob();
		 androidJOB.clickOkBtnOnAndroidJob();
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.SearchAutomationDevice();
		 commonmethdpage.ClickOnApplyButton();
		 accountsettingspage.SelectTrialsMobileDataOnlyInstallJob();//
		 accountsettingspage.DeployMobileDataOnlyjobonVisible();
		 accountsettingspage.ClickonJobApplyButton();
		 accountsettingspage.JobButtonVisible();
		 accountsettingspage.SelectTrialsMobileDataOnlyInstallJobAgain();//
		 commonmethdpage.ClickOnHomePage();	
}
	
	@Test(priority=18,description="Verify Default connectivity-Mobile Data Only for File Transfer")
	   public void VerifyDefaultconnectivityMobileDataOnlyFileTransferTC_ST_338() throws InterruptedException{
		 Reporter.log("\n18.Verify Default connectivity-Mobile Data Only for File Transfer",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.DefaultconnectivityMobileDataOnlyTransferfilejobs();//
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 androidJOB.clickOnJobs();
		 androidJOB.clickNewJob();
		 androidJOB.clickOnAndroidOS();	 
		 androidJOB.clickOnFileTransferJob();		 
		 androidJOB.enterJobName(Config.EnterTrialMobileDataFileTransferJob);//
		 androidJOB.clickOnAddInstallApplication();
		 androidJOB.enterFilePathURL(Config.EnterApkUrl);
		 androidJOB.clickOkBtnOnIstallJob();
		 androidJOB.clickOkBtnOnAndroidJob();
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.SearchAutomationDevice();
		 commonmethdpage.ClickOnApplyButton();
		 accountsettingspage.SelectTrialsMobileDataFileTransferJob();//
		 accountsettingspage.DeployMobileDataOnlyjobonVisible();
		 accountsettingspage.ClickonJobApplyButton();
		 accountsettingspage.JobButtonVisible();
		 accountsettingspage.SelectTrialsMobileDataFileTransferJobAgain();//
		 commonmethdpage.ClickOnHomePage();	
}
	
	
	   @Test(priority=19,description="Verify Default connectivity-Any Network Install Application")
	   public void VerifyDefaultconnectivityAnyNetworkInstallApplicationTC_ST_339() throws InterruptedException{
		 Reporter.log("\n19.Verify Default connectivity-Any Network Install Application",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.DefaultconnectivityAnyNetworkInstallFileTransferjobs();//
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 androidJOB.clickOnJobs();
		 androidJOB.clickNewJob();
		 androidJOB.clickOnAndroidOS();	 
		 androidJOB.ClickOnInstallApplication();		 
		 androidJOB.enterJobName(Config.EnterTrialAnyNetworkJob);//
		 androidJOB.clickOnAddInstallApplication();
		 androidJOB.enterFilePathURL(Config.EnterApkUrl);
		 androidJOB.clickOkBtnOnIstallJob();
		 androidJOB.clickOkBtnOnAndroidJob();
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.SearchAutomationDevice();
		 commonmethdpage.ClickOnApplyButton();
		 accountsettingspage.SelectTrialsAnyNetworkInstallJob();//
		 accountsettingspage.DeployAnyNetworkjobonVisible();//
		 accountsettingspage.ClickonJobApplyButton();
		 accountsettingspage.JobButtonVisible();
		 accountsettingspage.SelectTrialsAnyNetworkInstallJobAgain();//
		 commonmethdpage.ClickOnHomePage();	
}
	
	   @Test(priority=20,description="Verify Default connectivity-Any Network for File Transfer")
	   public void VerifyDefaultconnectivityAnyNetworkFileTransferTC_ST_340() throws InterruptedException{
		 Reporter.log("\n20.Verify Default connectivity-Any Network for File Transfer",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.DefaultconnectivityAnyNetworkTransferfilejobs();//
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 androidJOB.clickOnJobs();
		 androidJOB.clickNewJob();
		 androidJOB.clickOnAndroidOS();	 
		 androidJOB.clickOnFileTransferJob();		 
		 androidJOB.enterJobName(Config.EnterTrialAnyNetworkFileTransferJob);//
		 androidJOB.clickOnAddInstallApplication();
		 androidJOB.enterFilePathURL(Config.EnterApkUrl);
		 androidJOB.clickOkBtnOnIstallJob();
		 androidJOB.clickOkBtnOnAndroidJob();
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.SearchAutomationDevice();
		 commonmethdpage.ClickOnApplyButton();
		 accountsettingspage.SelectTrialsAnyNetworkFileTransferJob();//
		 accountsettingspage.DeployAnyNetworkFileTransferjobonVisible();//
		 accountsettingspage.ClickonJobApplyButton();
		 accountsettingspage.JobButtonVisible();
		 accountsettingspage.SelectTrialsAnyNetworkFileTransferJobAgain();//
		 commonmethdpage.ClickOnHomePage();	
}
	
	
	   @Test(priority=21,description="Verify Enabling Global Search in Miscellaneous Settings")
	   public void VerifyEnablingGlobalSearchinMiscellaneousSettingsTC_ST_341() throws InterruptedException{
		 Reporter.log("\n21.Verify Enabling Global Search in Miscellaneous Settings",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.EnableGlobalSearchCheckbox();
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.EnableGlobalSearchAutomationDevice();
		 accountsettingspage.EnableGlobalSearchAutomationDeviceVisible();
			 
	}	
	
	   @Test(priority=22,description="Verify Disable Global Search in Miscellaneous Settings")
	   public void VerifyDisableGlobalSearchinMiscellaneousSettingsTC_ST_342() throws InterruptedException{
		 Reporter.log("\n22.Verify Disable Global Search in Miscellaneous Settings",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();
		 accountsettingspage.DisableGlobalSearchCheckboxs();
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.DisableGlobalSearchAutomationDevice();
		 accountsettingspage.DisableGlobalSearchAutomationDeviceVisible();
			 
	}	
	
	   @Test(priority=23,description="Verify  Disable Auto Search in Miscellaneous Settings")
	   public void VerifyDisableAutoSearchinMiscellaneousSettingsTC_ST_341() throws InterruptedException{
		 Reporter.log("\n23.Verify  Disable Auto Search in Miscellaneous Settings",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();		 
		 accountsettingspage.DisableEnableAutosearchCheckboxs();
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.DisableEnableAutosearchAutomationDevice();
		 accountsettingspage.DisableEnableAutosearchAutomationDeviceVisible();			 
	}
	
	
	   @Test(priority=24,description="Verify  Enable Auto Search in Miscellaneous Settings")
	   public void VerifyEnableAutoSearchinMiscellaneousSettingsTC_ST_342() throws InterruptedException{
		 Reporter.log("\n24.Verify  Enable Auto Search in Miscellaneous Settings",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();		 
		 accountsettingspage.EnableAutosearchCheckboxs();
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();
		 accountsettingspage.EnableAutosearchAutomationDevice();
		 accountsettingspage.EnableAutosearchAutomationDeviceVisible();			 
	}
	
	   @Test(priority=25,description="Verify that in Device grid Battery Temperature coloumn of all devices is shown with Celsius (*C) as temperature unit")
	   public void VerifythatinDevicegridBatteryTemperaturecoloumnofalldevicesisshownwithCelsiusTC_ST_346() throws InterruptedException{
		 Reporter.log("\n25.Verify that in Device grid Battery Temperature coloumn of all devices is shown with Celsius (*C) as temperature unit",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();			 
		 accountsettingspage.DevicegridBatteryTemperatureinCelsius();		 
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();		 
		 accountsettingspage.TemperatureUnitAutomationDevice();
		 accountsettingspage.BatteryTemperatureVisible();
		 accountsettingspage.BatteryTemperatureValidation();
		 accountsettingspage.OriginalElement();
		 
	}
	
	
	@Test(priority=26,description="Verify that in Device grid Battery Temperature coloumn of all devices is shown with Fahrenheit (*F) as temperature unit")
	   public void VerifythatinDevicegridBatteryTemperaturecoloumnofalldevicesisshownwithFahrenheit() throws InterruptedException{
		 Reporter.log("\n26.Verify that in Device grid Battery Temperature coloumn of all devices is shown with Fahrenheit (*F) as temperature unit",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();			 
		 accountsettingspage.DevicegridBatteryTemperatureinFahrenheit();		 
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 commonmethdpage.ClickOnHomePage();		 
		 accountsettingspage.TemperatureUnitAutomationDevice();
		 accountsettingspage.BatteryTemperatureVisible();
		 accountsettingspage.BatteryTemperatureFahrenheitValidation();	
		 accountsettingspage.OriginalElement();
	}
	
	
	   @Test(priority=27,description="Verify that in Custom reports of device details, Battery Temperature of all devices is shown with Celsius as temperature unit in view custom report")
	   public void VerifythatinCustomreportsdetailsBatteryTemperatureshownwithCelsiusastemperatureunitinviewcustomreportTC_ST_348() throws InterruptedException{
		 Reporter.log("\n27.Verify that in Custom reports of device details, Battery Temperature of all devices is shown with Celsius/ as temperature unit in view custom report",true);		
		 commonmethdpage.ClickOnSettings();
		 accountsettingspage.ClickOnAccountSettings();
		 accountsettingspage.ClickonMiscellaneousSetting();			 
		 accountsettingspage.DevicegridBatteryTemperatureinCelsius();
		 accountsettingspage.ClickOnMiscellaneousApplyBtn();
		 accountsettingspage.ConfirmationMessageVerify(true);
		 reportsPage.ClickOnReports_Button();
		 customReports.ClickOnCustomReports();
		 customReports.ClickOnAddButtonCustomReport();
		 accountsettingspage.EnteringCustomeReportDetails(Config.CustomereportName,Config.CustomereportDescription);
		 accountsettingspage.ClickonLeftSideBatteryTemperature();			 
		 customReports.ClickOnAddButtonInCustomReport();	
		 accountsettingspage.ClickonBatteryTemperature();
		 accountsettingspage.ClickOnSaveButtonCustomReport();
		 //customReports.ClickOnSaveButton_CustomReports();
		 customReports.ClickOnOnDemandReport();
		 customReports.SearchCustomizedReport("Trails Battery Temperature");
		 customReports.ClickOnCustomizedReportNameInOndemand();
		 customReports.ClickRequestReportButton();			 
		 customReports.ClickOnViewReportButton();
		 customReports.ClickOnRefreshInViewReport();
	     customReports.ClickOnSearchReportButton("Trails Battery Temperature");	 
		 customReports.ClickOnRefreshInViewReport();
		 customReports.ClickOnView("Trails Battery Temperature");
		 reportsPage.WindowHandle();
		 accountsettingspage.SearchDeviceinReports();
		 accountsettingspage.ClickonBatteryVisible();
		 reportsPage.SwitchBackWindow();
		 accountsettingspage.TrialsBatteryDelete();			
		 customReports.ClearSearchedReport();	
		 commonmethdpage.ClickOnHomePage();	
	}
	
// vinimanoj 
	   
	   @Test(priority=28,description="Verify exporting data when Do not Mask Hide PII is selected from the drop down list")
	   	public void VerifyexportingdatawhenDonotMaskHidePIITC_ST_849() throws InterruptedException, Exception {
	   		Reporter.log("\n28.Verify exporting data when Do not Mask Hide PII is selected from the drop down list",true);
	   		commonmethdpage.ClickOnSettings();
	   		commonmethdpage.ClickonAccsettings();
	   		accountsettingspage.Miscellaneous_Settings();
	   		accountsettingspage.scrolldowntoMiscellaneous_SettingsApplyButton();
	   		accountsettingspage.secureDataEXportedDropDown();
	   		accountsettingspage.verifyExportedDoNotForMaskHidePIITDisplayed();
	   		accountsettingspage.verifyExportedForHidePIITDisplayed();
	   		accountsettingspage.verifyExportedDoNotForHidePIITDisplayed();
	   		accountsettingspage.verifyExportedForMaskPIITDisplayed();
	   		accountsettingspage.verifyexportedForMaskPIITDisplayed();
	   		accountsettingspage.clickHome();
		}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



}
