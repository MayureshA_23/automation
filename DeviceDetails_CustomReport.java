package CustomReportsScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//create subuser and in rules as UserName=POCO,Pwd=Demo@123,if already created then ignore.
//and  enable report permission and globle report permission for the created sub user

public class DeviceDetails_CustomReport extends Initialization {
	
/*@Test(priority='0',description="Group creation")
public void CustomReportCase_1() throws Throwable
	{
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
	}*/
@Test(priority='1',description="Verify generating Device details Custom report with filters") 
public void VerifyDeviceDetailsRep_TC_RE_75() throws InterruptedException
	{Reporter.log("\nVerify generating Device details Custom report with filters -the devices in the specified group whose os version is not equal to 6.0.1===",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
	    customReports.VerifyAlltheWarningToastsOfNameDescription();
	    customReports.EnterCustomReportsNameDescription_DeviceDetails("Devdetails CustRep with filters as OS version is NotequalTo 6.0.1","test");
	    customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList("Device Details");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceDetails);
		customReports.SelectValueFromColumn(Config.EnterOperatingSystem);
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.EnterOSVersion);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Devdetails CustRep with filters as OS version is NotequalTo 6.0.1");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Devdetails CustRep with filters as OS version is NotequalTo 6.0.1");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Devdetails CustRep with filters as OS version is NotequalTo 6.0.1");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Devdetails CustRep with filters as OS version is NotequalTo 6.0.1");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.VerifyDevicePlatform(Config.Device_Platform);
		customReports.VerifyingDevicemodel(Config.Device_Model);
		customReports.VerifingOsVersion();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		customReports.ClickOnDownloadReportLinkButton();		
	}
@Test(priority='2',description="Verify generating Device details Custom report without filters")
public void VerifyDeviceDetailsReportWithFilter() throws InterruptedException
	{Reporter.log("\nVerify generating Device details Custom report without filters",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Devdetails CustRep without filters","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList("Device Details");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Devdetails CustRep without filters");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Devdetails CustRep without filters");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Devdetails CustRep without filters");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Devdetails CustRep without filters");
		reportsPage.WindowHandle();
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.CheckingDeviceplatform();
		customReports.VerifyingDevicemodel(Config.Device_Model);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport(); 
	}
@Test(priority='3',description="Verify generating Device details Custom report with filters -The report should display all the devices in the specified group whose Battery is greater than to 5")
public void VerifyDeviceDetailsReportForBatteryPercentColumn() throws InterruptedException
	{Reporter.log("\nVerify generating Device details Custom report with filters -The report should display all the devices in the specified group whose Battery is greater than to 5",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Devdetails CustRep with filters as greater than","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList("Device Details");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceDetails);
		customReports.SelectValueFromColumn(Config.SelectValueAsBatteryPercent);
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield(Config.EnterBatteryPercentage);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Devdetails CustRep with filters as greater than");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Devdetails CustRep with filters as greater than");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Devdetails CustRep with filters as greater than");
		reportsPage.WindowHandle();
		customReports.VerifingBatteryPercentage();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
@Test(priority='4',description="Verify generating Device details Custom report with filters , sort by and Group by options") 
public void VerifactionOfDevDetRep_TC_RE_97() throws InterruptedException
	{Reporter.log("\nVerify generating Device details Custom report with filters , sort by and Group by options",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Devdetails CustomReport for SortBy And GroupBy","test");
		customReports.Selectvaluefromsortbydropdown(Config.SelectModel);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Devdetails CustomReport for SortBy And GroupBy");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Devdetails CustomReport for SortBy And GroupBy");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Devdetails CustomReport for SortBy And GroupBy");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Devdetails CustomReport for SortBy And GroupBy");
		reportsPage.WindowHandle();
		//customReports.ChooseDevicesPerPage();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	} 
@Test(priority='5',description="Verify Access To Other Users in Device details Custom reports") 
public void VerifyDeviceDetailsReportVisibleInSubUser() throws InterruptedException
	{Reporter.log("\n=Verify Access To Other Users in Device details Custom reports",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnAllowAccessToUSerCheckBox();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceDetails_CustomReportName,"This is Custom report");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceDetails_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		loginPage.logoutfromApp();
		usermanagement.ClickOnLoginLink();
		customReports.logintoApp(Config.subuser_name,Config.subuser_passwrd);
		customReports.ClickOnLogin();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton(Config.DeviceDetails_CustomReportName);
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView(Config.DeviceDetails_CustomReportName);
		reportsPage.WindowHandle();
		customReports.CheckingCustomreportAtSubUserAccount();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		customReports.ClickOnCustomReports();
		customReports.DeletingGeneratedReport(Config.DeviceDetails_CustomReportName);
		loginPage.logoutfromApp();
	}
@Test(priority='6',description="Verify Sort by and Group by Applying Aggregate Option for Device details")
public void VerifyDeviceDetailsReportByApplyingAggregateOption() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for Device details",true);
		usermanagement.ClickOnLoginLink();
		customReports.logintoApp(Config.userID,Config.password);
		customReports.ClickOnLogin();
		commonmethdpage.ClickOnHomePage();
		System.out.println(">> Counting Online Devices in grid before creating filter");
		customReports.ClickOnAllDevices();
		customReports.ChooseDevicesPerPageInConsole();
		customReports.androidDevices();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList("Device Details");
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceDetails_CustomReportName,"This is Custom report");
		customReports.Selectvaluefromsortbydropdown(Config.SelectModel);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByPlatform);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.AggregateOptionCount);
		customReports.SendingAliasName(Config.EnterAliasNameAsSelectedPlatform);	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceDetails_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection(Config.DeviceDetails_CustomReportName);
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton(Config.DeviceDetails_CustomReportName);
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView(Config.DeviceDetails_CustomReportName);
	    reportsPage.WindowHandle();
		customReports.VerifyingDevicePlatformInHome();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
@Test(priority='7',description="Verify MAC address are shown in Custom Device detail report for Windows Device (Nix Enrollmennt)")
public void VerifyMACAddressInDeviceDetailsReport_NixEnrollment() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify MAC address are shown in Custom Device detail report for Windows Device (Nix Enrollmennt)",true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice("KARTHIKBS-239");
		customReports.GetMACAddressFromConsole();
		commonmethdpage.ClickOnAllDevicesButton();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDet CustRep to Verify MAC address for Windows Device of Nix Enrollmennt","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevDet CustRep to Verify MAC address for Windows Device of Nix Enrollmennt");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevDet CustRep to Verify MAC address for Windows Device of Nix Enrollmennt");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevDet CustRep to Verify MAC address for Windows Device of Nix Enrollmennt");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep("KARTHIKBS-239");
		customReports.VerifyMACAddressInReport(21);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='8',description="Verify MAC address are shown in Custom Device detail report for Windows Device (EMM Enrollment)")
public void VerifyMACAddressInDeviceDetailsReport_EMMEnrollment() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify MAC address are shown in Custom Device detail report for Windows Device (EMM Enrollment)",true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice("42GEARS");
		customReports.GetMACAddressFromConsole();
		commonmethdpage.ClickOnAllDevicesButton();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDet CustRep to Verify MAC address for Windows Device of EMM Enrollment","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevDet CustRep to Verify MAC address for Windows Device of EMM Enrollment");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevDet CustRep to Verify MAC address for Windows Device of EMM Enrollment");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevDet CustRep to Verify MAC address for Windows Device of EMM Enrollment");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep("42GEARS");
		customReports.VerifyMACAddressInReport(21);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='9',description="Verify MAC address are shown in Custom Device detail report for Windows Device (Dual Enrollment)")
public void VerifyMACAddressInDeviceDetailsReport_DualEnrollment() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify MAC address are shown in Custom Device detail report for Windows Device (Dual Enrollment)",true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice("ASUS");
		customReports.GetMACAddressFromConsole();
		commonmethdpage.ClickOnAllDevicesButton();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDet CustRep to Verify MAC address for Windows Device of Dual Enrollment","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevDet CustRep to Verify MAC address for Windows Device of Dual Enrollment");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevDet CustRep to Verify MAC address for Windows Device of EMM Enrollment");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevDet CustRep to Verify MAC address for Windows Device of Dual Enrollment");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep("ASUS");
		customReports.VerifyMACAddressInReport(21);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='A',description="Verify Device locale column in Device details custom report")
public void VerifyDevicelocaleColInDevDetRep_TC_003() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify Device locale column in Device details custom report",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Devdetails CustRep to Verify Device locale column","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Devdetails CustRep to Verify Device locale column");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Devdetails CustRep to Verify Device locale column");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Devdetails CustRep to Verify Device locale column");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Devdetails CustRep to Verify Device locale column");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep(Config.DeviceLocaleCol_Val);
		customReports.ScrollToColumn("Device Locale");
		customReports.VerifyDeviceLocaleCol(Config.DeviceLocaleCol_Val);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='B',description="Verify adding filter for Device locale column in Device details custom report")
public void VerifyDevicelocaleColInDevDetRep_TC_004() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify adding filter for Device locale column in Device details custom report",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Devdetails CustRep to Verify adding filter for Device locale column","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown(Config.SelectDeviceDetails);
		customReports.SelectValueFromColumn("Device Locale");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.DeviceLocaleCol_Val);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Devdetails CustRep to Verify adding filter for Device locale column");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Devdetails CustRep to Verify adding filter for Device locale column");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Devdetails CustRep to Verify adding filter for Device locale column");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Devdetails CustRep to Verify adding filter for Device locale column");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep(Config.DeviceLocaleCol_Val);
	    customReports.VerifyingReport_NotEqualToOp();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
			
			}
		}
	}
}
