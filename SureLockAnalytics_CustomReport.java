package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class SureLockAnalytics_CustomReport extends Initialization {
	@Test(priority='0',description="Verify SureLock Analytics  Custom reports") 
	public void VerifySureLockAnalytics_TC_RE_88_TC_RE_147() throws InterruptedException
		{Reporter.log("\n1.Verify SureLock Analytics  Custom reports",true);
			commonmethdpage.ClickOnSettings();
			commonmethdpage.ClickonAccsettings();
			accountsettingspage.ClickOnDataAnalyticsTab();
			accountsettingspage.EnableDataAnalyticsCheckBox();
			accountsettingspage.DisableSureLockAnalyticsCheckBox();
			accountsettingspage.ClickOnDataAnalyticsApplyButton();
			accountsettingspage.EnablingSureLockAnalyticsChckBox();
			accountsettingspage.ClickOnDataAnalyticsApplyButton();
			accountsettingspage.VerifySureLockAnalyticsEnablePopUp();
			accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureLockAnalytics  Customreport Without Filter","test");
			customReports.ClickOnColumnInTable("SureLock Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureLockAnalytics  Customreport Without Filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.VerifyingGeneratedReportInOndemandSection("SureLockAnalytics  Customreport Without Filter");
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();				
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureLockAnalytics  Customreport Without Filter");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureLockAnalytics  Customreport Without Filter");
			reportsPage.WindowHandle();
			accountsettingspage.SearchUsedApplicationORWebSiteInReport("SureMDM Nix");
			accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.nix");
			accountsettingspage.VerifyApplicationName("SureMDM Nix");
			accountsettingspage.VerifyApplicationEndTime();
			accountsettingspage.VerifyApplicationTotalTime();
			accountsettingspage.SearchUsedApplicationORWebSiteInReport("Play Store");
			accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.android.vending");
			accountsettingspage.VerifyApplicationName("Play Store");
			accountsettingspage.VerifyApplicationEndTime();
			accountsettingspage.VerifyApplicationTotalTime();
			accountsettingspage.SearchUsedApplicationORWebSiteInReport("Chrome");
			accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.android.chrome");
			accountsettingspage.VerifyApplicationName("Chrome");
			accountsettingspage.VerifyApplicationEndTime();
			accountsettingspage.VerifyApplicationTotalTime();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	
	@Test(priority='1',description="Verify SureLock Analytics Custom reports for Sub group") 
	public void VerifySureLockAnalytics_TC_RE_148() throws InterruptedException
		{Reporter.log("\n2.Verify SureLock Analytics Custom reports for Sub group",true);
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureLockAnalytics  Customreport Without Filter For Subgrp","test");
			customReports.ClickOnColumnInTable("SureLock Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureLockAnalytics  Customreport Without Filter For Subgrp");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.VerifyingGeneratedReportInOndemandSection("SureLockAnalytics  Customreport Without Filter For Subgrp");
			customReports.ClickOnAddGroup();
			customReports.ClickOnOneGroup();
			customReports.ClickOnAddGroupButton();
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();				
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureLockAnalytics  Customreport Without Filter For Subgrp");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureLockAnalytics  Customreport Without Filter For Subgrp");
			reportsPage.WindowHandle();
			accountsettingspage.SearchUsedApplicationORWebSiteInReport("SureMDM Nix");
			accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.nix");
			accountsettingspage.VerifyApplicationName("SureMDM Nix");
			accountsettingspage.VerifyApplicationEndTime();
			accountsettingspage.VerifyApplicationTotalTime();
			accountsettingspage.SearchUsedApplicationORWebSiteInReport("Play Store");
			accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.android.vending");
			accountsettingspage.VerifyApplicationName("Play Store");
			accountsettingspage.VerifyApplicationEndTime();
			accountsettingspage.VerifyApplicationTotalTime();
			accountsettingspage.SearchUsedApplicationORWebSiteInReport("Chrome");
			accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.android.chrome");
			accountsettingspage.VerifyApplicationName("Chrome");
			accountsettingspage.VerifyApplicationEndTime();
			accountsettingspage.VerifyApplicationTotalTime();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	@Test(priority='1',description="Verify Sort by and Group by for Surelock Analytics") 
	public void VerifySureLockAnalytics_TC_RE_149() throws InterruptedException
		{Reporter.log("\n2.Verify Sort by and Group by for Surelock Analytics",true);
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureLockAnalytics CustomReport for SortBy And GroupBy","test");
			customReports.ClickOnColumnInTable("SureLock Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.Selectvaluefromsortbydropdown("Package Name");
			customReports.SelectvaluefromsortbydropdownForOrder("asc");
			customReports.SelectvaluefromGroupByDropDown("Device Name");
			customReports.SelectvaluefromAggregateOptionsDropDown("max");
			customReports.SendingAliasName("My Device");
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureLockAnalytics CustomReport for SortBy And GroupBy");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureLockAnalytics CustomReport for SortBy And GroupBy");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureLockAnalytics CustomReport for SortBy And GroupBy");
			reportsPage.WindowHandle();
			customReports.VerificationOfValuesInColumn();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	@Test(priority='1',description="Verify Duration column in SureLock Anaytics Report should be displayed as HH:MM:SS") 
	public void VerifySureLockAnalytics_TC_RE_222() throws InterruptedException
		{Reporter.log("\n2.Verify Duration column in SureLock Anaytics Report should be displayed as HH:MM:SS",true);
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureLockAnalytics CustRep to verify DurationCol In ViewRep","test");
			customReports.ClickOnColumnInTable("SureLock Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureLockAnalytics CustRep to verify DurationCol In ViewRep");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.Select1WeekDateInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureLockAnalytics CustRep to verify DurationCol In ViewRep");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureLockAnalytics CustRep to verify DurationCol In ViewRep");
			reportsPage.WindowHandle();
			customReports.ChooseDevicesPerPage();
			customReports.CheckingDurationColDateFormat_InSureLockAnalytics();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
			
		}
	@Test(priority='1',description="Verify duration column filter for the SureLock Analytics report") 
	public void VerifySureLockAnalytics_TC_RE_226() throws InterruptedException
		{Reporter.log("\n2.Verify duration column filter for the SureLock Analytics report",true);
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.ClickOnColumnInTable("SureLock Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureLockAnalytics CustRep duration column filter","test");
			customReports.Selectvaluefromdropdown("SureLock Analytics");
			customReports.SelectValueFromColumn("Duration");
			customReports.SelectOperatorFromDropDown("<=");
			customReports.SendValueToTextfield(Config.DurationFormat3);
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureLockAnalytics CustRep duration column filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.Select1WeekDateInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureLockAnalytics CustRep duration column filter");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureLockAnalytics CustRep duration column filter");
			reportsPage.WindowHandle();
			customReports.ChooseDevicesPerPage();
			customReports.CheckingDurationColDateFormat_InSureLockAnalytics();
			customReports.VerifyingDurationColForGivenRange_InSureLockAnalytics();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
