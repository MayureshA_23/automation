package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

// PRECONDITIONS 

// CREATE A ANDROID PROFILE AutoAndroid
//CREATE A IOS PROFILE AutoIOS
// CREATE A WINDOWS PROFILE AutoWindows
// CREATE A MACOS PROFILE AutoMacOs
//bentyl6 should b deleted
//Profile Permission should be deleted

public class ProfilePermission  extends Initialization{ 

	@Test(priority='1',description="1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException{
		Reporter.log("\n1.To Verify roles option is present",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
	}
	@Test(priority='2',description="2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException{
		Reporter.log("\n2.To Verify super user is present",true);
	
		usermanagement.toVerifySuperUsertemplete();
	}
	@Test(priority='3',description="3.To create a roll for Filestore Permission")
	public void CreateRollForProfilePermission() throws InterruptedException{
		Reporter.log("\n3.Create a Roll for Device action",true);
	
		usermanagement.CreateRoleProfilePermission("Profile Permission","FOR USER pij");
		
	}
	@Test(priority='4',description="4.To create User for Device Permissions")
	public void CreateUserForProfilePermission() throws InterruptedException{
		Reporter.log("\n4. User with Device Permissions",true);
		usermanagement.CreateUserForPermissions("AutoProfilePermission", "42Gears@123", "AutoProfilePermission", "Profile Permission");  // uncomment remove later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoProfilePermission", "42Gears@123");
		usermanagement.RefreshCurrentPage();
		Thread.sleep(5000);

	}
	@Test(priority='5',description="3.To create a roll for Profile Permission")
	public void LoginasUserClickOnProfiles() throws InterruptedException{
		Reporter.log("\n5.Create a Roll for Device action",true);
	
		usermanagement.clickOnProfile();
		
	}
	@Test(priority='6',description="3.To create a roll for Profile Permission")
	public void AddAndroidProfileEnable() throws InterruptedException{
		Reporter.log("\n6.Create a Roll for Device action",true);
	
		usermanagement.SearchProfile("AutoAndroid");
		usermanagement.SelectAndroidProfileUser();
		usermanagement.AddAndroidProfileEnable();
		
	}
	@Test(priority='7',description="3.To create a roll for Profile Permission")
	public void EditAndroidProfileEnable() throws InterruptedException{
		Reporter.log("\n7.Create a Roll for Device action",true);
	
		usermanagement.EditAndroidProfileEnabled();
		
	}
	@Test(priority='8',description="3.To create a roll for Profile Permission")
	public void DeleteAndroidProfileEnable() throws InterruptedException{
		Reporter.log("\n8.Create a Roll for Device action",true);
		usermanagement.DeleteAndroidProfileEnable();
	}
	
	@Test(priority='9',description="3.To create a roll for Profile Permission")
	public void AddIOSprofileEnable() throws InterruptedException{
		Reporter.log("\n9.Create a Roll for Device action",true);
	
		usermanagement.clickOnIOSprofile();//remove later
		usermanagement.SearchProfile("AutoIOS");

		usermanagement.SelectIOSprofile();
		usermanagement.AddIOSEnable();
	}
	@Test(priority='A',description="3.To create a roll for Profile Permission")
	public void EditIOSprofileEnable() throws InterruptedException{
		Reporter.log("\n10.Create a Roll for Device action",true);
	
		usermanagement.EditIOSprofile();
		
	}
	@Test(priority='B',description="3.To create a roll for Profile Permission")
	public void DeleteIOSprofileEnable() throws InterruptedException{
		Reporter.log("\n11.Create a Roll for Device action",true);
	
		usermanagement.DeleteIOSprofile();
		
	}
//	@Test(priority='C',description="3.To create a roll for Profile Permission")
//	public void SelectDefaultIOSprofileEnable() throws InterruptedException{
//		Reporter.log("\n12.Create a Roll for Device action",true);
//	
//		usermanagement.SetDefaultPolicyIOSProfileEnable();
//		usermanagement.NotificationOnRemovingDefaultProfile();	
//	}
	
	@Test(priority='D',description="3.To create a roll for Profile Permission")
	public void AddWindowsprofileEnable() throws InterruptedException{
		Reporter.log("\n13.Create a Roll for Device action",true);
	
		usermanagement.clickOnWindowsProfile();
		usermanagement.SearchProfile("AutoWindows");

		usermanagement.WindowsProfileSelect();
		usermanagement.AddWindowsProfile();
	}
	@Test(priority='E',description="3.To create a roll for Profile Permission")
	public void EditWindowsprofileEnable() throws InterruptedException{
		Reporter.log("\n14.Create a Roll for Device action",true);
		usermanagement.EditWindowsProfile();
	}
	@Test(priority='F',description="3.To create a roll for Profile Permission")
	public void DeleteWindowsprofileEnable() throws InterruptedException{
		Reporter.log("\n15.Create a Roll for Device action",true);
		usermanagement.DeletewindowsProfile();
		
	}
//	@Test(priority='G',description="3.To create a roll for Profile Permission")
//	public void SelectDefaultWindowsprofileEnable() throws InterruptedException{
//		Reporter.log("\n16.Create a Roll for Device action",true);
//	
//		usermanagement.SetDefaultPolicyWindowsProfileEnable();
//		
//	}
	@Test(priority='H',description="3.To create a roll for Profile Permission")
	public void AddMacOsprofileEnable() throws InterruptedException{
		Reporter.log("\n17.Create a Roll for Device action",true);
		usermanagement.ClickOnMacOsProfile();
		usermanagement.SearchProfile("AutoMacOs");
		usermanagement.MacOsProfileSelect();
		usermanagement.AddMacOsProfileEnable();
	}
	@Test(priority='I',description="3.To create a roll for Profile Permission")
	public void EditMacOsprofileEnable() throws InterruptedException{
		Reporter.log("\n18.Create a Roll for Device action",true);
	
		usermanagement.EditMAcOSProfile();
		
	}
	@Test(priority='J',description="3.To create a roll for Profile Permission")
	public void DeleteMacOsprofileEnable() throws InterruptedException{
		Reporter.log("\n19.Create a Roll for Device action",true);
	
		usermanagement.DeleteMacOSprofile();
		
	}
//	@Test(priority='K',description="3.To create a roll for Profile Permission")
//	public void SelectDefaultMacOsprofileEnable() throws InterruptedException{
//		Reporter.log("\n20.Create a Roll for Device action",true);
//		usermanagement.SetDefaultPolicyMacOsProfileEnable();
//	}
	@Test(priority='L',description="3.To create a roll for Profile Permission")
	public void DisablingProfilePermissons() throws InterruptedException{
		Reporter.log("\n21.Create a Roll for Device action",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID ,Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.DisablingAllProfilePermsissions();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();	
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoProfilePermission", "42Gears@123");
		usermanagement.RefreshCurrentPage();
	}
	@Test(priority='M',description="3.To create a roll for Profile Permission")
	public void DisableAndroidPermission() throws InterruptedException{
		Reporter.log("\n22.Create a Roll for Device action",true);
		usermanagement.clickOnProfile();
		usermanagement.SearchProfile("AutoAndroid");
		usermanagement.SelectAndroidProfileUser();
		usermanagement.DisabledAndroidProfiles();	
	}
	@Test(priority='N',description="3.To create a roll for Profile Permission")
	public void DisableIOSPermissions() throws InterruptedException{
		Reporter.log("\n23.Create a Roll for Device action",true);
		usermanagement.clickOnIOSprofile();
		usermanagement.SearchProfile("AutoIOS");
		usermanagement.SelectIOSprofile();
		usermanagement.DisabledIOSProfiles();	
	}
	@Test(priority='Q',description="3.To create a roll for Profile Permission")
	public void DisableWindowsPermissions() throws InterruptedException{
		Reporter.log("\n24.Create a Roll for Device action",true);
		usermanagement.clickOnWindowsProfile();
		usermanagement.SearchProfile("AutoWindows");
		usermanagement.WindowsProfileSelect();
		usermanagement.DisabledIOSProfiles();
		Thread.sleep(3000);
	}
	@Test(priority='R',description="3.To create a roll for Profile Permission")
	public void DisableMacOsPermissions() throws InterruptedException{
		Reporter.log("\n25.Create a Roll for Device action",true);
		usermanagement.ClickOnMacOsProfile();
		usermanagement.SearchProfile("AutoMacOs");
		usermanagement.MacOsProfileSelect();
		usermanagement.DisabledMacOsProfiles();	
	}
		

}
