package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.PassScreenshot;
import Library.WebDriverCommonLib;

public class DeviceEnrollement_POM extends WebDriverCommonLib{
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib(); 

	@FindBy(id="generateQRCode")
	private WebElement ClickOnDeviceEnrollement;
	
	@FindBy(id="downloadQrCodeBtn")
	private WebElement downloadQRCodeButton;
	
	@FindBy(id="prtQrCodeBtn")
	private WebElement printQRcodebutton;
	
	@FindBy(id="changeGrp")
	private WebElement optionsbutton;
	
	@FindBy(xpath="//h4[@id='modalTitle']")
	private WebElement DeviceEnrollmentWindow;
	
	@FindBy(xpath="//span[text()='Select any group to generate QR code.']")
	private WebElement warningMessageWhenNoGroupIsSelected;
	
	@FindBy(id="okbtn")
	private WebElement GenerateQRCodeButton;
	
	@FindBy(xpath=".//*[@id='deviceNmeOpt']")
	private WebElement ClickOnSelectDeviceNameDropDown;
	
	public void verifyClickingOnDeviceEnrollment() throws InterruptedException
	{
		ClickOnDeviceEnrollement.click();
		waitForXpathPresent("//span[.='QR Code Enrollment']");
		sleep(3);
	}
	
	
	
	public void verifyDeviceEnrollment_Print() throws InterruptedException, AWTException{
		printQRcodebutton.click();
		sleep(5);
		String originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		PassScreenshot.captureScreenshot(Initialization.driver, "PrintQRCodeScreenshot");
		sleep(3);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_W);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_W);
		sleep(4);
		Initialization.driver.switchTo().window(originalHandle);   
		
	  }
	
	public void VerifyClickinOnOptions() throws InterruptedException{
		
		optionsbutton.click();
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 10);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[@id='modalTitle']"))); 
        PassScreenshot.captureScreenshot(Initialization.driver, "OptionsScreenshot");	  
        
        String Actualvalue1 = DeviceEnrollmentWindow.getText();
		String Expectedvalue1 = "Device Enrollment";
		String PassStatement1 = "PASS >> "+Expectedvalue1+" window  displayed successfully";
		String FailStatement1 = "FAIL >> "+Expectedvalue1+ " window is not displayed" ;
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
	    sleep(3);
	}
	
	public void VerifyWarningMessageWhenNoGroupIsSelected() throws InterruptedException{
		
	    GenerateQRCodeButton.click();
		sleep(4);
		boolean isdisplayed=true;
		 
		  try{
			  downloadQRCodeButton.isDisplayed();
				   	 
				    }catch(Exception e)
				    {
				    	isdisplayed=false;
				   	 
				    }
		   Assert.assertTrue(isdisplayed,"FAIL >>  No warning message displayed");
		   Reporter.log("PASS >> warning message 'Select any group to generate QR code.' is displayed",true );
	
	}
	
	public void VerifyDropDown()
	{
	Select SelectDeviceNameDropDown = new Select(ClickOnSelectDeviceNameDropDown);
	SelectDeviceNameDropDown.selectByVisibleText("Use System Generated Name");
	
	// pending
	
}
	//kavya
	
	@FindBy(xpath = "//button[normalize-space()='Device Enrollment']")
    private WebElement DeviceEnrollment;
    
    @FindBy(xpath = "//span[normalize-space()='QR Code Enrollment']")
    private WebElement QRCodeEnrollment;
    
    @FindBy(xpath = "//span[normalize-space()='Manual Enrollment']")
    private WebElement selectManualEnrollment;
    
    @FindBy(xpath = "//span[normalize-space()='List View']")
    private WebElement ListView;
    
    @FindBy(xpath = "//span[normalize-space()='Default View']")
    private WebElement DefaultView;
	
	public void clickOnDeviceEnrollment()
    {
   	 DeviceEnrollment.click();
   	 waitForXpathPresent("//span[normalize-space()='QR Code Enrollment']");
    }
public void verifyPromtsInDevEnrollment()
    {
   	 WebElement[] promts= {QRCodeEnrollment,selectManualEnrollment,ListView,DefaultView};
   	 String [] actual= {"QR Code Enrollment","Manual Enrollment","List View","Default View"};
   	 
   	 for (int i = 0; i < actual.length - 1; i++) 
		{
			boolean value=promts[i].isDisplayed();
			String PassStatement = "PASS >> " + actual[i]+ " is displayed successfully when clicked on Device Enrollment Button";
			String FailStatement = "Fail >> " + actual[i] + " is not displayed even when clicked on Device Enrollment Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
   	 
    }

		
	
}
