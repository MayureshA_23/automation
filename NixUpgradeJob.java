package JobsOnAndroid;

import org.testng.annotations.Test;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//"NixJobUpgradeFolder"and"AutoFolder1", this folder name should not be present
public class NixUpgradeJob  extends Initialization {
	
		@Test(priority=1,description="Verify the UI of Nix Upgrade Job")
		public void VerifyModifydeployInstall() throws InterruptedException{
			androidJOB.clickOnJobs();
			androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS();
			androidJOB.clickOnNixUpgrade();
			androidJOB.VerifyNixUpgrade();
			Reporter.log("Pass >>Verify the UI of Nix Upgrade Job",true); }

		@Test(priority=2,description="Verify the Creation of Nix Upgrade Job with Blank field")
		public void CreateNixUpgradeJobWithField() throws InterruptedException{
			androidJOB.enterNixAgentLink(Config.nixLatestAPK);
			androidJOB.enterNixDevicePath();
			androidJOB.nixUpgradeOkButton();
			androidJOB.EmptyJobNameMessage();
			Reporter.log("Pass >>Verify the Creation of Nix Upgrade Job with Blank field",true);
			}

		@Test(priority=3,description="Verify the Creation of Nix Upgrade Job")
		public void CreateNixUpgradeJob() throws InterruptedException{
			
//			androidJOB.clickOnJobs();
//			androidJOB.clickNewJob(); 
//			androidJOB.clickOnAndroidOS();
//			androidJOB.clickOnNixUpgrade();

			androidJOB.enterJobNameNixUpgrade("NixUpgradeJob");
			androidJOB.enterNixAgentLink(Config.nixLatestAPK);
			androidJOB.enterNixDevicePath();
			androidJOB.nixUpgradeOkButton();
			androidJOB.JobCreatedMessage();

			Reporter.log("Pass >>Verify the Creation of Nix Upgrade Job",true); }

		@Test(priority=4,description="Verify the Modification of Nix Upgrade Job")
		public void modificationNixUpgrade() throws InterruptedException{
			
			dynamicjobspage.SearchJobInSearchField("NixUpgradeJob");
			androidJOB.ClickOnOnJob("NixUpgradeJob");
			androidJOB.clickOnModifyJob();
			androidJOB.enterJobNameNixUpgrade("NixUpgradeJobModified");
			androidJOB.nixUpgradeOkButton();
			androidJOB.JobmodifiedMessage();
			Reporter.log("Pass >>Verify the Modification of Nix Upgrade Job",true); }

		@Test(priority=5,description="Verify the Deletion of Nix Upgrade Job")
		public void verifyModifyDelete() throws InterruptedException{
			dynamicjobspage.SearchJobInSearchField("NixUpgradeJobModified");
			androidJOB.ClickOnOnJob("NixUpgradeJobModified");
			androidJOB.deleteCreatedJob();
			androidJOB.JobDeletedMessage();
			Reporter.log("Pass >>Verify the Deletion of Nix Upgrade Job",true); }

		@Test(priority=6,description="Verify the Creation of Nix Upgrade Job inside Folder")
		public void verifyNixJobInsideFolder() throws InterruptedException{
			androidJOB.ClickOnNewFolder();
			androidJOB.NamingFolder("NixJobUpgradeFolder");
			androidJOB.SearchingJobFolder("NixJobUpgradeFolder");
//			androidJOB.DoubleClickOnFolder("NixJobUpgradeFolder");
			androidJOB.DoubleClickOnFolderNixUpgradeJob("NixJobUpgradeFolder");
	        androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS();
			androidJOB.clickOnNixUpgrade();
			androidJOB.enterJobNameNixUpgrade("NixFolderJob");
			androidJOB.enterNixAgentLink(Config.nixLatestAPK);
			androidJOB.enterNixDevicePath();
			androidJOB.nixUpgradeOkButton();
			dynamicjobspage.SearchJobInSearchField("NixFolderJob");
			androidJOB.ClickOnOnJob("NixFolderJob");
			androidJOB.CheckJobIsCreatedInsideFolder();
			androidJOB.BackFromFolder();
			androidJOB.SearchJobInSearchField("NixJobUpgradeFolder");
			androidJOB.ClickOnOnJob("NixJobUpgradeFolder");
			androidJOB.deleteCreatedJob();
			Reporter.log("Pass >>Verify the Creation of Nix Upgrade Job inside Folder",true);
			}

		@Test(priority=7,description="Verify the Nix Upgrade Job by Apply on Device")

		public void NixUpgradeApplyOnDevice() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
			androidJOB.getNIXVersion();
			androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS();
			androidJOB.clickOnNixUpgrade();
			androidJOB.enterJobNameNixUpgrade("NixUpgradeJob");
			androidJOB.enterNixAgentLink(Config.nixLatestAPK);
			androidJOB.enterNixDevicePath();
			androidJOB.nixUpgradeOkButton();
			androidJOB.JobCreatedMessage();
			commonmethdpage.ClickOnHomePage();
			androidJOB.SearchDeviceInconsole(Config.DeviceName); 
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("NixUpgradeJob");
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckStatusOfappliedInstalledJob("NixUpgradeJob",Config.TimeToDeployInstallJob);
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
			androidJOB.getNIXVersionAfterUpgrade();
			androidJOB.compareNIXversion();
			Reporter.log("Pass >>Verify the Nix Upgrade Job by Apply on Device",true); 
			}
			
			
		@Test(priority=8,description="Verify copying nix upgrade job")

		public void NixUpgradeJobCopying() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

			androidJOB.clickOnJobs();
			androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS();
			androidJOB.clickOnNixUpgrade();
			androidJOB.enterJobNameNixUpgrade("NixUpgradeJobToBeCopied");
			androidJOB.enterNixAgentLink(Config.nixLatestAPK);
			androidJOB.enterNixDevicePath();
			androidJOB.nixUpgradeOkButton();
			androidJOB.JobCreatedMessage();
			androidJOB.ClickOnNewFolder();
			androidJOB.NamingFolder("NixJobUpgradeFolder");

			dynamicjobspage.SearchJobInSearchField("NixUpgradeJobToBeCopied");
			androidJOB.ClickOnOnJob("NixUpgradeJobToBeCopied");
			androidJOB.ClickOnCopyJobOption();
			androidJOB.CopyNixUpgradeJob("NixJobUpgradeFolder");
			androidJOB.ClickOnOkBtnCopyFolder();
			commonmethdpage.ClickOnHomePage(); 



			Reporter.log("Pass >>Verify copying nix upgrade job",true);
			
		
		
		
	}
		
	// Need to ask
		
		@Test(priority=9,description="Verify copying folder present in jobs"+"Jobs - Verify applying copied job to device")

		public void CopyingFolders() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

			androidJOB.clickOnJobs();
			androidJOB.ClickOnNewFolder();
			androidJOB.NamingFolder("AutoFolder1");
			androidJOB.SearchingJobFolder("AutoFolder1");
			androidJOB.DoubleClickOnFolderNixUpgradeJob("AutoFolder1");
			androidJOB.ClickOnNewFolder();
			androidJOB.NamingFolder("AutoFolder22");
			androidJOB.SearchingJobFolder("AutoFolder22");
			androidJOB.DoubleClickOnFolderNixUpgradeJob("AutoFolder22");
			androidJOB.clickOnJobs();
			androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS();
			androidJOB.clickOnTextMessageJob();
			androidJOB.verifyTextMessagePrompt();
			androidJOB.textMessageJobWhenNoFieldIsEmpty("TextVerifyJob","TextVerifyJob","notify Text");
			androidJOB.ClickOnOkButton();
			androidJOB.JobCreatedMessage();
			androidJOB.BackFromFolder();
			androidJOB.BackFromFolder();

			dynamicjobspage.SearchJobInSearchField("AutoFolder1");
			androidJOB.ClickOnOnJob("AutoFolder1");
			androidJOB.ClickOnCopyJobOption();
		//	androidJOB.CopyNixUpgradeJob("Home");

			androidJOB.ClickOnOkBtnCopyFolder_Folder();

			
			
			
			
	}
	@Test(priority='A',description="Verify copying folder present in jobs"+"Jobs - Verify applying copied job to device")

	public void CopyingFolderPresentInJobs() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		
		

		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("AutoFolder1_copy");
		commonmethdpage.Selectjob("AutoFolder1_copy");
		androidJOB.DoubleClickOnFolderNixUpgradeJob("AutoFolder1_copy");
		androidJOB.DoubleClickOnFolderNixUpgradeJob("AutoFolder22");
		commonmethdpage.SearchJob("TextVerifyJob");
		commonmethdpage.Selectjob("TextVerifyJob");

		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
	    androidJOB.CheckStatusOfappliedInstalledJob("TextVerifyJob",600);

		
		
		
		}
			
		
			
	}

		
		
		
		
		
		
		
		
		
		
		
		
		
	


