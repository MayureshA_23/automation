package OnDemandReports_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class DeviceConnected extends Initialization {

	@Test(priority = 1, description = "Generate And View the 'Device Connected Report'  for Selected 'Home' group")
	public void DeviceConnectedReportCurrentDateHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("1.Generate And View the 'Device Connected Report'  for Selected 'Home' group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 2, description = "Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Last 30 Days'")
	public void DeviceConnectedReportLast30DaysSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n2.Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Last 30 Days'",
				true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days_DeviceConnectedReport();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}
	
	@Test(priority = 3, description = "Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Last 30 Days'")
	public void DeviceConnectedReportLast30DaysHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n3.Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Last 30 Days'",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days_DeviceConnectedReport();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 4, description = "Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Today'")
	public void DeviceConnectedReportHomeGroup_Today() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n4.Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Today'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_DeviceConnectedReport();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}
	
	@Test(priority = 5, description = "Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Today'")
	public void DeviceConnectedReportSubGroup_Today() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n5.Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Today'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_DeviceConnectedReport();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 6, description = "Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Custom Range'")
	public void DeviceConnectedReportHomeGroup_CustomRange() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n6.Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Custom Range'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Custom Range");
		reportsPage.SelectingDateRange_DeviceconnectReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifyOfDeviceConnectedReport_CustomRange();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 7, description = "Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Custom Range'")
	public void DeviceConnectedReportSubGroup_CustomRange() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n7.Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Custom Range'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Custom Range");
		reportsPage.SelectingDateRange_DeviceconnectReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifyOfDeviceConnectedReport_CustomRange();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 8, description = "Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Current Month'")
	public void DeviceConnectedReportCurrentMonthHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n8.Generate And View the 'Device Connected Report'  for Selected 'Home' group and 'Current Month'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.verifySelectedDateForThisMonth_DeviceConnectedReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}
	@Test(priority = 9, description = "Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Current Month'")
	public void DeviceConnectedReportCurrentMonthSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n9.Generate And View the 'Device Connected Report'  for Selected 'Sub' group and 'Current Month'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.verifySelectedDateForThisMonth_DeviceConnectedReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 10, description = "Generate And View the 'Device Connected Report'  for 'Yesterday' date")
	public void DeviceConnectedReportYesterDayHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n10.Generate And View the 'Device Connected Report'  for 'Yesterday' date", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedYesterdayDate_DeviceConnected();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 11, description = "Generate And View the 'Device Connected Report'  for 'Last 1 Week' date")
	public void DeviceConnectedReportLast1WeekHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n11.Generate And View the 'Device Connected Report'  for 'Last 1 Week' date", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.verifySelectedDateForLast1week_DeviceConnectedReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();
	}
	@Test(priority = 12, description = "Generate And View the 'Device Connected Report'  for 'Last 1 Week' date and 'Home' Group")
	public void DeviceConnectedReportLast1WeekSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n12.Generate And View the 'Device Connected Report'  for 'Last 1 Week' date and 'Home' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.verifySelectedDateForLast1week_DeviceConnectedReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 13, description = "6.Download and Verify the data for Device Connected Report for 'Home' Group")
	public void VerifyDownloadingDeviceConnectedReportHomeGroup() throws InterruptedException {
		Reporter.log("\n13.Download and Verify the data for Device Connected Report for 'Home' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@Test(priority = 14, description = "6.Download and Verify the data for Device Connected Report for 'Sub' Group")
	public void VerifyDownloadingDeviceConnectedReportSubGroup() throws InterruptedException {
		Reporter.log("\n14.Download and Verify the data for Device Connected Report for 'Sub' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}
	
	@Test(priority = 15, description = "Generate And View the 'Device Connected Report'  for Selected 'Home' group")
	public void DeviceConnectedReportCurrentDateHomeGroup_LastConnected() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n15.Generate And View the 'Device Connected Report'  for Selected 'Home' group", true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.GetDeviceLastConnectedDate();
		reportsPage.getdatetimeFormatAsexpected(reportsPage.lastDate);
		
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceConnectReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyDeviceConnectedReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.DeviceStatusColumnVerificationDeviceConnectedReport();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.getLastConnectedDatetime_DeviceHealthReport(2);
		reportsPage.splitDateAndTimeAsExpected(reportsPage.lastconnecteddatefromreport);
		reportsPage.compareLastConnectedDate();
		reportsPage.SwitchBackWindow();
	}
	 
	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();

		}
	}

}