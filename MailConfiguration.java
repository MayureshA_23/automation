package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class MailConfiguration extends Initialization{
	
	@Test(priority='1',description="1.To verify clicking on Mail Configuration") 
    public void VerifyClickingOnMailConfiguration() throws InterruptedException {
    Reporter.log("To verify clicking on Mail Configuration");
    profilesAndroid.ClickOnProfile();
	profilesiOS.clickOniOSOption();
	profilesiOS.AddProfile();
	profilesiOS.ClickOnMailConfigurationProfile();
}
	
	
	@Test(priority='2',description="1.To Verify warning message Saving a Wifi Configuration Profile without Profile Name")
	public void VerifyWarningMessageOnSavingMailConfigurationProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Wifi Configuration Profile without Profile Name========");
		profilesiOS.ClickOnConfigureButtonMailConfigurationProfile();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='3',description="1.To Verify warning message Saving a Mail Configuration Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingMailConfigurationProfileWithoutAllFields() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Mail Configuration Profile without entering all the fields=========");
		profilesiOS.EnterMailConfigurationProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	@Test(priority='4',description="1.To Verify warning message Saving a Mail Configuration Profile without Outgoing Mail")
	public void VerifyWarningMessageOnSavingMailConfigurationProfileWithoutOutgoingMail() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
	 Reporter.log("To Verify warning message Saving a Mail Configuration Profile without Outgoing Mail");
	 profilesiOS.IncomingMail();
	 profilesiOS.ClickOnSavebutton();
	 profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	@Test(priority='5',description="1.To Verify Saving a Mail Configuration Profile")
	public void VerifySavingMailConfigurationProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
	 Reporter.log("To Verify warning message Saving a Mail Configuration Profile without Outgoing Mail");
	 profilesiOS.ClickOnOutogingMail();
	 profilesiOS.EnterOutgoingMailDetail();
	 profilesiOS.ClickOnSavebutton();
	 profilesAndroid.NotificationOnProfileCreated();
	 
	}

}
