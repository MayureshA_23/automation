package DeviceGrid_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.WebDriverCommonLib;
import bsh.ParseException;


public class CustomColumn extends Initialization
{
	
	@Test(priority=0,description="Disabling All Custom Columns")
	public void DisablingAllCustomColumns() throws InterruptedException
	{
		Reporter.log("\n1.Disabling All Custom Columns",true);
		deviceGrid.DisablingAllCustomColumns();		
	}
	@Test(priority=1,description="Verify adding custom columns")
	public void VerifyAddingCustomColumn() throws InterruptedException
	{
		Reporter.log("\n2.Verify adding custom columns",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumnName);
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumnName);
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName1);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumnName1);
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumnName1);
		deviceGrid.ClickOnCustomColumnsCloseButton();
	}

	@Test(priority=2,description="Verify modifying custom columns")
	public void VerifyModifyingCustomColumn() throws InterruptedException
	{
		Reporter.log("\n3.Verify modifying custom columns",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnModified);
		deviceGrid.ModifyingcustomColumn(Config.CustomColumnModified,Config.CustomColumnName1);
		deviceGrid.VerifyCustomColumnsModifiedSuccessfully(Config.CustomColumnModified);
		deviceGrid.ClickOnCustomColumnsCloseButton();
	}

	@Test(priority=3,description="Verify deleting custom columns")
	public void VerifyDeletingCustomColumn() throws InterruptedException
	{
		Reporter.log("\n4.Verify deleting custom columns",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.SearchingCustomColumn(Config.CustomColumnModified);
		deviceGrid.SelectingCustomColumn(Config.CustomColumnModified);
		deviceGrid.ClickOnCustomColumnDeleteButton();
		deviceGrid.ClickOnCustomColumnDeleteConfirmationNoButton();
		deviceGrid.VerifyCustomColumnIsNotDeletedonClickingNoButton(Config.CustomColumnModified);
		deviceGrid.SearchingCustomColumn(Config.CustomColumnModified);
		deviceGrid.SelectingCustomColumn(Config.CustomColumnModified);
		deviceGrid.ClickOnCustomColumnDeleteButton();
		deviceGrid.ClickOnCustomColumnDeleteConfirmationYesbutton();
		deviceGrid.VerifyCustomColumnDeletionOnClickingYesButton(Config.CustomColumnModified);
		deviceGrid.ClickOnCustomColumnsCloseButton();
	}

	@Test(priority=4,description="Verify search in Custom Columns")
	public void VerifySearchInCustomColumns() throws InterruptedException
	{
		Reporter.log("\n5.Verify search in Custom Columns",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.SearchingCustomColumn(Config.CustomColumnName);
		deviceGrid.VerifySearchInCustomColumn(Config.CustomColumnName);
		deviceGrid.ClickOnCustomColumnsCloseButton();
	}

	@Test(priority=5,description="Verify refresh in Custom Columns")
	public void VerifyRefreshInCustomColumn() throws InterruptedException
	{
		Reporter.log("\n6.Verify refresh in Custom Columns",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.ClickOnCustomColumnRefreshButton();
		deviceGrid.ClickOnCustomColumnsCloseButton();
	}
	
	@Test(priority=6,description="Verify searching and adding Custom Columns")
	public void VerifySearchingAndAddingCustomColumns() throws InterruptedException
	{
		Reporter.log("\n7.Verify searching and adding Custom Columns",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumnName);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumnName);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumnName);	
	}	
	
	
	@Test(priority=7,description="Verify searching and adding Installed App Version")
	public void VerifySearchingAndAddinginstalledAppVersion() throws InterruptedException
	{
		Reporter.log("\n8.Verify searching and adding Installed App Version",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOnInstalledApplicationVersionList();
		deviceGrid.SearchingForApplicationInstalledAppVersionList("com.android.chrome");
		deviceGrid.SelectingApplicationIninstalledAppVersionList("( com.android.chrome  )");
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid("Chrome ( com.android.chrome  ) ");
	}
	
	@Test(priority=8,description="Verify creating new custom columns with Unique value and display on device grid")
	public void VerifyCreatingCCWithUniquevalues() throws InterruptedException
	{
		Reporter.log("\n9.Verify creating new custom columns with Unique value and display on device grid",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumn_UniqueValue);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.SendingCustomColumnName(Config.CustomColumn_UniqueValue);
		deviceGrid.ClickingOnuniqueValueCheckBox();
		deviceGrid.ClickOnAddNewColumnOkButton();
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumn_UniqueValue);
		deviceGrid.VerifyUniquevalueColumnAfterCreating("Unique");
		deviceGrid.ClickOnCustomColumnsCloseButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumn_UniqueValue);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumn_UniqueValue);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumn_UniqueValue);
	}
	
	@Test(priority=9,description="Verify adding values to the created Unique custom column")
	public void VerifyAddingValuesToTheCreateduniqueCC() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n10.Verify adding values to the created Unique custom column",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.AddingValuesToCustomColumn(Config.UniqueValue1);
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.UniqueValue1);
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		deviceGrid.AddingValuesToCustomColumn(Config.UniqueValue2);
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.UniqueValue2);
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.ReadingUniqueValue();
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		deviceGrid.ReadingUniqueValue1();
		deviceGrid.VerifyuniqueValuesAreDifferntForDevice();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.AddingValuesToCustomColumn(Config.UniqueValue1);
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
	}
	
	@Test(priority='A',description="Verify modifying Unique value column to Non-Unique value column")
	public void VerifyModifyinguniqueValueColumnToNonUniqueValueColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n11.Verify modifying Unique value column to Non-Unique value column",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.ModifyingUniqueValueColumn(Config.CustomColumn_UniqueValue);
		deviceGrid.ClickingOnuniqueValueCheckBox();
		deviceGrid.ClickOnOkButtonAftermodifyingUniquevalueColumn();
		deviceGrid.SearchingCustomColumn(Config.CustomColumn_UniqueValue);
		deviceGrid.VerifyUniquevalueColumnAfterCreating("NA");
		deviceGrid.ClickOnCustomColumnsCloseButton();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.AddingValuesToCustomColumn(Config.UniqueValue2);
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.UniqueValue2);
		deviceGrid.DeSelectingCustomColumn(Config.CustomColumn_UniqueValue);
	}
	
	@Test(priority='B',description="Verify creating new custom columns with Non-Unique value and display on device grid")
	public void verifyCreatingNewCCWithNonUniqueValue() throws InterruptedException
	{
		Reporter.log("\n12.Verify creating new custom columns with Non-Unique value and display on device grid",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumn_UniqueValue1);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumn_UniqueValue1);
		deviceGrid.SearchingCustomColumn(Config.CustomColumn_UniqueValue1);
		deviceGrid.VerifyUniquevalueColumnAfterCreating("NA");
		deviceGrid.ClickOnCustomColumnsCloseButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumn_UniqueValue1);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumn_UniqueValue1);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumn_UniqueValue1);	
	}
	
	@Test(priority='C',description="Verify adding values to the created Unique custom column")
	public void VerifyAddingValuesToTheCreatedUniqueCustomColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n13.Verify adding values to the created Unique custom column",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumn_UniqueValue1);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumn_UniqueValue1);
		deviceGrid.ClickOnColumnsButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.AddingValuesToCustomColumn(Config.SameValue);
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.SameValue);
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		deviceGrid.AddingValuesToCustomColumn(Config.SameValue);
		deviceGrid.VerifyNoErrorMessageOnAddingSameValues(deviceGrid.ErrorMessage);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.SameValue);
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.ReadingUniqueValue();
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		deviceGrid.ReadingUniqueValue1();
		deviceGrid.VerifyValuesAreSameForDevice();
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		deviceGrid.AddingValuesToCustomColumn(Config.UniqueValue2);
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.UniqueValue2);	
	}
	
	@Test(priority='D',description="Verify modifying Non-Unique value column to Unique value column")
	public void VerifyModifyingNonUniqueColumn() throws InterruptedException
	{
		Reporter.log("\n14.Verify modifying Non-Unique value column to Unique value column",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.ModifyingUniqueValueColumn(Config.CustomColumn_UniqueValue1);
		deviceGrid.VerifynoteMessage();
		deviceGrid.VerifyCheckBoxStatusWhenTriedtoModifyNonUniqueCCToUniqueCC();
		deviceGrid.SearchingCustomColumn(Config.CustomColumn_UniqueValue1);
		deviceGrid.VerifyUniquevalueColumnAfterCreating("NA");
		deviceGrid.ClickOnCustomColumnsCloseButton();
	}
	
	@Test(priority='E',description="Verify tool tip for advance search in custom column")
	public void VerifyToolTipMessageInCustomColumn() throws InterruptedException
	{
		Reporter.log("\n15.Verify tool tip for advance search in custom column",true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ScrollToColumn("CustomColumn1");
		deviceGrid.ReadingToolTipMessage();
		deviceGrid.SelectSearchOption("Basic Search");
	}
	
	@Test(priority='F',description="Verify searching by N/A in custom column")
	public void VerifySearchingNAInCC() throws InterruptedException
	{
		Reporter.log("\n16.Verify searching by N/A in custom column",true);
		commonmethdpage.ClickOnAllDevicesButton();	
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();//CustomColumn_Automation1
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName);
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName1);
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnModified);
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumn_UniqueValue);
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumn_UniqueValue1);
		deviceGrid.DeletingExistingCustomColumn("CustomColumns_ThingsDevice");
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn("CustomColumnTest");
		deviceGrid.VerifyCustomColumnsAddedSuccessfully("CustomColumnTest");
		deviceGrid.ClickOnCustomColumnsCloseButton();

		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList("CustomColumnTest");
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist("CustomColumnTest");
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid("CustomColumnTest");
		
		deviceGrid.ScrollToColumn("CustomColumn1");
		deviceGrid.ReadingNACountInCustomColumn();
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ScrollToColumn("CustomColumn1");
		deviceGrid.EnterValueInAdvanceSearchColumn("CustomColumn1","N/A");
		deviceGrid.ReadingNACountInCustomColumn_AdvanceSearch();
		deviceGrid.VerifySearchingNAInCustomColumn();
		deviceGrid.SelectSearchOption("Basic Search");
	}
	
	@Test(priority='G',description="Verify custom column for things device")
	public void VerifyCustomColumnForThingsDevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n17.Verify custom column for things device",true);
		commonmethdpage.ClickOnHomePage();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn("CustomColumns_ThingsDevice");
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn("CustomColumns_ThingsDevice");
		deviceGrid.VerifyCustomColumnsAddedSuccessfully("CustomColumns_ThingsDevice");
		deviceGrid.ClickOnCustomColumnsCloseButton();
		commonmethdpage.SearchDevice(Config.ThingsDeviceName);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList("CustomColumns_ThingsDevice");
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist("CustomColumns_ThingsDevice");
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid("CustomColumns_ThingsDevice");
	
	}
	
	@Test(priority='H',description="Verify custom column name and data in the device Info Panel",dependsOnMethods="VerifyCustomColumnForThingsDevice")
	public void VerifyCustomColumnNameAndDataInDeviceiNfoPanel() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n18.Verify custom column name and data in the device Info Panel",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.AddingValuesToCustomColumn("Test");
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid("Test");
		deviceGrid.VerifyEnabledCustomColumnInDeviceInfoPanel("CustomColumns_ThingsDevice");
	}
	@Test(priority='I',description="Verify mouse hover should display the format search can be performed in the Installed application column in device grid")
	public void VerifyOfOpertorFormatsForInstalledAppCol_TC_DG_01() throws InterruptedException,IOException,InvalidFormatException
	{
		Reporter.log("\n19.Verify mouse hover should display the format search can be performed in the Installed application column in device grid",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOnInstalledApplicationVersionList();
		deviceGrid.SearchingForApplicationInstalledAppVersionList("com.android.chrome");
		deviceGrid.SelectingApplicationIninstalledAppVersionList("( com.android.chrome  )");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Chrome ( com.android.chrome  ) ");
		deviceGrid.VerifyOfOperatorFormatInstallAppCol();
		deviceGrid.SelectSearchOption("Basic Search");
	}
	@Test(priority='J',description="Verify custom column by adding dot operator(.)on column name")
	public void VerifyOfCreatingCCWithDot() throws InterruptedException,Exception,IOException,EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n20.Verify custom column by adding dot operator(.)on column name",true);
		deviceGrid.InstallAppWithLowerversion_NotePad();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn("CustomColumns.WithDot");
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumnWithDotOpAndWrngMsg("CustomColumns.WithDot");
		deviceGrid.ClickOnAddCustomColumnCloseButton();
		deviceGrid.ClickOnCustomColumnsCloseButton();
		
	}
	@Test(priority='K',description="Verify Application version in device grid after upgrading from one version to another",dependsOnMethods="VerifyOfCreatingCCWithDot")
	public void VerifyOfAppVerInDevGridAfterUpgrade() throws Exception, InterruptedException
	{
		Reporter.log("\n21.Verify Application version in device grid after upgrading from one version to another",true);
		deviceGrid.DisablingAllInstalledApplicationColumns();
		
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnDynamicRefresh();
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnDynamicRefresh();
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.ClickOnAllDevicesButton();
		
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOnInstalledApplicationVersionList();
		deviceGrid.SearchingForApplicationInstalledAppVersionList("com.kamikazelabs.gemezone");
		deviceGrid.SelectingApplicationIninstalledAppVersionList(" ( com.kamikazelabs.gemezone  ) ");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("GAMEZONE ( com.kamikazelabs.gemezone )");
		deviceGrid.getValueForLowerInstalledAppVersion_GameZone();
		deviceGrid.InstallAppWithHigherrversion_NotePad();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnDynamicRefresh();
		commonmethdpage.clickOnGridrefreshbutton();
		
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("GAMEZONE ( com.kamikazelabs.gemezone )");
		deviceGrid.getValueForUpperGameZoneAppVersion();
		deviceGrid.compareGameZoneAppversion();
		
	}
	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException{
		if(ITestResult.FAILURE==result.getStatus())
		{
			deviceGrid.RefreshGrid();
		}
	}
}
