package PageObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;
import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;
import io.appium.java_client.MobileBy;
import Common.Initialization;
import Library.AssertLib;
import Library.WebDriverCommonLib;

public class AvaylerPOM extends WebDriverCommonLib{
	AssertLib ALib=new AssertLib();
	String parentwinID;
	String childwinID;
	String URL;
	
@FindBy(xpath="//h1[text()='Admin Center']")
private WebElement AdminCenter;	
	
public void AdminCenter()throws InterruptedException{

	//boolean flag=AdminCenter.isDisplayed();
	//if(flag)
	//{	   		
		sleep(5);
		String option = Initialization.driver.findElement(By.xpath("//h1[text()='Admin Center']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
//	}
//	else
//	{		
//		//ALib.AssertFailMethod("FAIL >> Admin Center is not Displayed");
//		Reporter.log("FAIL >> Admin Center is not Displayed",true);
//	}
}	
	

@FindBy(xpath="//button[text()='Login']")
private WebElement ClickonLogin ;	

public void ClickonLogin() throws InterruptedException {
	sleep(2);
	ClickonLogin.click();
	Reporter.log("PASS >> Click on Admin Login ", true);
	sleep(2);
}






//h1[text()='Dynamic Scheduling Engine']

//h1[text()='Dynamic Pricing Engine']

//h1[text()='Technician Portal']


@FindBy(xpath="//h1[text()='Dynamic Scheduling Engine']")
private WebElement DynamicSchedulingEngineVisible ;	
	
public void DynamicSchedulingEngineVisible()throws InterruptedException{

	boolean flag=DynamicSchedulingEngineVisible.isDisplayed();
	if(flag)
	{	   		
		sleep(5);
		String option = Initialization.driver.findElement(By.xpath("//h1[text()='Dynamic Scheduling Engine']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Dynamic Scheduling Engine is not Displayed");
		Reporter.log("FAIL >> Dynamic Scheduling Engine is not Displayed",true);
	}
}	
	
@FindBy(xpath="//h1[text()='Dynamic Pricing Engine']")
private WebElement DynamicPricingEngineVisible ;	
	
public void DynamicPricingEngineVisible()throws InterruptedException{

	boolean flag=DynamicPricingEngineVisible.isDisplayed();
	if(flag)
	{	   		
		sleep(5);
		String option = Initialization.driver.findElement(By.xpath("//h1[text()='Dynamic Pricing Engine']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Dynamic Pricing Engine is not Displayed");
		Reporter.log("FAIL >> Dynamic Pricing Engine is not Displayed",true);
	}
}	
	

@FindBy(xpath="//h1[text()='Technician Portal']")
private WebElement TechnicianPortalVisible ;	
	
public void TechnicianPortalVisible()throws InterruptedException{

	boolean flag=TechnicianPortalVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[text()='Technician Portal']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Technician Portal is not Displayed");
		Reporter.log("FAIL >> Technician Portal is not Displayed",true);
	}
}	





//@FindBy(xpath="//h1[text()='Technician Portal']")
//private WebElement TechnicianPortalVisible ;	
//	
//public void TechnicianPortalVisible()throws InterruptedException{
//
//	boolean flag=TechnicianPortalVisible.isDisplayed();
//	if(flag)
//	{	   		
//		sleep(5);
//		String option = Initialization.driver.findElement(By.xpath("//h1[text()='Technician Portal']")).getText();
//		Reporter.log("PASS >> Text is Displayed : "+option,true);
//		sleep(2); 		
//	}
//	else
//	{		
//		ALib.AssertFailMethod("FAIL >> Technician Portal is not Displayed");
//		Reporter.log("FAIL >> Technician Portal is not Displayed",true);
//	}
//}	
@FindBy(xpath="//h1[text()='Technician Portal']")
private WebElement ClickonTechnicianPortal ;	

public void ClickonTechnicianPortal() throws InterruptedException {
	ClickonTechnicianPortal.click();
	Reporter.log("PASS >> Click on Technician Portal ", true);
	sleep(2);
}

@FindBy(xpath="//*[text()='Avayler Logo']")
private WebElement AvaylertextVisible ;	
	
public void AvaylertextVisible()throws InterruptedException{

	boolean flag=AvaylertextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Avayler Logo']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Avayler textis not Displayed");
		Reporter.log("FAIL >> Avayler Text is not Displayed",true);
	}
}	




//*[text()='Technician Portal']

@FindBy(xpath="//*[text()='Technician Portal']")
private WebElement TechnicianPortaltextVisible ;	
	
public void TechnicianPortaltextVisible()throws InterruptedException{

	boolean flag=TechnicianPortaltextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Technician Portal']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Technician text is not Displayed");
		Reporter.log("FAIL >> Technician Text is not Displayed",true);
	}
}	


//*[text()='PricingProfiles']

@FindBy(xpath="//*[text()='PricingProfiles']")
private WebElement PricingProfilestextVisible ;	
	
public void PricingProfilestextVisible()throws InterruptedException{

	boolean flag=PricingProfilestextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='PricingProfiles']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Pricing Profiles is not Displayed");
		Reporter.log("FAIL >> Pricing Profiles Text is not Displayed",true);
	}
}	


//*[text()='Workflows']

@FindBy(xpath="//*[text()='Workflows']")
private WebElement WorkflowstextVisible ;	
	
public void WorkflowstextVisible()throws InterruptedException{

	boolean flag=WorkflowstextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Workflows']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Workflows is not Displayed");
		Reporter.log("FAIL >> Workflows Text is not Displayed",true);
	}
}

//*[text()='Amazing Module 3']

@FindBy(xpath="//*[text()='Amazing Module 3']")
private WebElement AmazingModuletextVisible ;	
	
public void AmazingModuletextVisible()throws InterruptedException{

	boolean flag=AmazingModuletextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Amazing Module 3']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Amazing Module is not Displayed");
		Reporter.log("FAIL >> Amazing ModuleText is not Displayed",true);
	}
}

//*[text()='Fantastic Module 4']

@FindBy(xpath="//*[text()='Fantastic Module 4']")
private WebElement FantasticModuletextVisible ;	
	
public void FantasticModuletextVisible()throws InterruptedException{

	boolean flag=FantasticModuletextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Fantastic Module 4']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Fantastic Module is not Displayed");
		Reporter.log("FAIL >> Fantastic Module is not Displayed",true);
	}
}

//*[text()='Users Management']


@FindBy(xpath="//*[text()='Users Management']")
private WebElement UsersManagementtextVisible ;	
	
public void UsersManagementtextVisible()throws InterruptedException{

	boolean flag=UsersManagementtextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Users Management']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Users Management is not Displayed");
		Reporter.log("FAIL >> Users Management is not Displayed",true);
	}
}

//*[text()='Settings']

@FindBy(xpath="//*[text()='Settings']")
private WebElement SettingstextVisible ;	
	
public void SettingstextVisible()throws InterruptedException{

	boolean flag=SettingstextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Settings']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Settings is not Displayed");
		Reporter.log("FAIL >> Settings is not Displayed",true);
	}
}

//h1[text()='Workflow Setup']

@FindBy(xpath="//h1[text()='Workflow Setup']")
private WebElement WorkflowSetuptextVisible ;	
	
public void WorkflowSetuptextVisible()throws InterruptedException{

	boolean flag=WorkflowSetuptextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[text()='Workflow Setup']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Workflow Setup is not Displayed");
		Reporter.log("FAIL >> Workflow Setup is not Displayed",true);
	}
}

//*[text()='Start of Day']

@FindBy(xpath="//span[normalize-space()='Start your day']")
private WebElement StartofDaytextVisible ;	
	
public void StartofDaytextVisible()throws InterruptedException{

	boolean flag=StartofDaytextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Start your day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Start of Day is not Displayed");
		Reporter.log("FAIL >> Start of Day is not Displayed",true);
	}
}

//*[text()='Break']

@FindBy(xpath="//span[normalize-space()='Take your break']")
private WebElement BreaktextVisible ;	
	
public void BreaktextVisible()throws InterruptedException{

	boolean flag=BreaktextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Take your break']")).getText();
		Reporter.log("PASS >> Take your Break is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Take your Break is not Displayed");
		Reporter.log("FAIL >>Take your Break is not Displayed",true);
	}
}


//*[text()='End of Day']

@FindBy(xpath="//span[normalize-space()='End your day']")
private WebElement EndofDaytextVisible ;	
	
public void EndofDaytextVisible()throws InterruptedException{

	boolean flag=EndofDaytextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='End your day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> End your Day is not Displayed");
		Reporter.log("FAIL >> End your Day is not Displayed",true);
	}
}

//*[text()='Customer Disclaimers']

@FindBy(xpath="(//span[text()='Job disclaimers'])[1]")
private WebElement JobDisclaimerstextVisible ;	
	
public void JobDisclaimerstextVisible()throws InterruptedException{

	boolean flag=JobDisclaimerstextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("(//span[text()='Job disclaimers'])[1]")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Job Disclaimers is not Displayed");
		Reporter.log("FAIL >> Job Disclaimers is not Displayed",true);
	}
}

//*[text()='Job Pre-Fulfilment']

@FindBy(xpath="//span[text()='Start your job']")
private WebElement StartyourjobtextVisible ;	
	
public void StartyourjobtextVisible()throws InterruptedException{

	boolean flag=StartyourjobtextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Start your job']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Start Your Job is not Displayed");
		Reporter.log("FAIL >> Start Your Job is not Displayed",true);
	}
}

//*[text()='Job unable to start']

@FindBy(xpath="//span[text()='Unable to start job']")
private WebElement unabletostartjobtextVisible ;	
	
public void unabletostartjobtextVisible()throws InterruptedException{

	boolean flag=unabletostartjobtextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Unable to start job']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>  unable to start Job is not Displayed");
		Reporter.log("FAIL >>  unable to start Job is not Displayed",true);
	}
}

//span[normalize-space()="Job Can't Fulfil"]

@FindBy(xpath="//span[normalize-space()=\"Job Can't Fulfil\"]")
private WebElement JobCantFulfiltextVisible ;	
	
public void JobCantFulfiltextVisible()throws InterruptedException{

	boolean flag=JobCantFulfiltextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()=\"Job Can't Fulfil\"]")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Job Can't Fulfil is not Displayed");
		Reporter.log("FAIL >> Job Can't Fulfil is not Displayed",true);
	}
}


//*[text()='Job Disclaimers']

@FindBy(xpath="(//span[text()='Job disclaimers'])[2]")
private WebElement JobDisclaimers2textVisible ;	
	
public void JobDisclaimers2textVisible()throws InterruptedException{

	boolean flag=JobDisclaimers2textVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("(//span[text()='Job disclaimers'])[2]")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Job Disclaimers 2 is not Displayed");
		Reporter.log("FAIL >> Job Disclaimers 2 is not Displayed",true);
	}
}

//*[text()='Job Product Specific Fulfilment']

@FindBy(xpath="//span[normalize-space()='Your workflow']")
private WebElement YourWorkflowtextVisible ;	
	
public void YourWorkflowtextVisible()throws InterruptedException{

	boolean flag=YourWorkflowtextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Your workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Your workflow is not Displayed");
		Reporter.log("FAIL >> Your workflow is not Displayed",true);
	}
}

//*[text()='Job Product Specific Cannot Fulfil']

@FindBy(xpath="//span[normalize-space()='Unable to complete']")
private WebElement UnabletocompletetextVisible ;	
	
public void UnabletocompletetextVisible()throws InterruptedException{

	boolean flag=UnabletocompletetextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Unable to complete']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Unable to complete is not Displayed");
		Reporter.log("FAIL >> Unable to complete is not Displayed",true);
	}
}

//*[text()='Job Customer Review']

@FindBy(xpath="//span[normalize-space()='Customer review']")
private WebElement CustomerReviewtextVisible ;	
	
public void CustomerReviewtextVisible()throws InterruptedException{

	boolean flag=CustomerReviewtextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Customer review']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>  Customer Review is not Displayed");
		Reporter.log("FAIL >>  Customer Review is not Displayed",true);
	}
}

//*[text()='Job Tech Review']


@FindBy(xpath="//span[normalize-space()='Finish your job']")

private WebElement FinishYourJobtextVisible ;	
	
public void FinishYourJobtextVisible()throws InterruptedException{

	boolean flag=FinishYourJobtextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Finish your job']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Finish your job is not Displayed");
		Reporter.log("FAIL >> Finish your job is not Displayed",true);
	}
}


//*[text()='Job Close']

@FindBy(xpath="//span[normalize-space()='Complete your job']")
private WebElement CompleteyourjobtextVisible ;	
	
public void CompleteyourjobtextVisible()throws InterruptedException{

	boolean flag=CompleteyourjobtextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Complete your job']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Complete your job is not Displayed");
		Reporter.log("FAIL >> Complete your job is not Displayed",true);
	}
}

//*[text()='User Name']

@FindBy(xpath="//*[text()='User Name']")
private WebElement UserNametextVisible ;	
	
public void UserNametextVisible()throws InterruptedException{

	boolean flag=UserNametextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='User Name']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> User Name is not Displayed");
		Reporter.log("FAIL >> User Name is not Displayed",true);
	}
}

//a[normalize-space()='Console']

@FindBy(xpath="//a[normalize-space()='Console']")
private WebElement ConsoletextVisible ;	
	
public void ConsoletextVisible()throws InterruptedException{

	boolean flag=ConsoletextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//a[normalize-space()='Console']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Console is not Displayed");
		Reporter.log("FAIL >> Console is not Displayed",true);
	}
}

//a[normalize-space()='My Account']

@FindBy(xpath="//a[normalize-space()='My Account']")
private WebElement MyAccounttextVisible ;	
	
public void MyAccounttextVisible()throws InterruptedException{

	boolean flag=MyAccounttextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//a[normalize-space()='My Account']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> My Account is not Displayed");
		Reporter.log("FAIL >> My Account is not Displayed",true);
	}
}

//a[normalize-space()='Log out']

@FindBy(xpath="//a[normalize-space()='Log out']")
private WebElement LogouttextVisible ;	
	
public void LogouttextVisible()throws InterruptedException{

	boolean flag=LogouttextVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//a[normalize-space()='Log out']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Log out is not Displayed");
		Reporter.log("FAIL >> Log out is not Displayed",true);
	}
}


@FindBy(xpath="//span[normalize-space()='Start your day']")
//span[normalize-space()='Start your day']
private WebElement ClickonStartofDay ;	

public void ClickonStartofDay() throws InterruptedException {
	sleep(2);
	ClickonStartofDay.click();
	Reporter.log("PASS >> Click on Admin Start of Day", true);
	sleep(2);
}


//h2[text()='Start of Day']

@FindBy(xpath="//span[normalize-space()='Start your day']")
private WebElement StartofDayVisible ;	
	
public void StartofDayVisible()throws InterruptedException{

	boolean flag=StartofDayVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Start your day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Start your Day is not Displayed");
		Reporter.log("FAIL >> Start your Day is not Displayed",true);
	}
}

//div[@title='Now Serving']

@FindBy(xpath="//div[@title='Now Serving']")
private WebElement NowServingVisible ;	
	
public void NowServingVisible()throws InterruptedException{

	boolean flag=NowServingVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@title='Now Serving']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Now Serving is not Displayed");
		Reporter.log("FAIL >> Now Serving is not Displayed",true);
	}
}

//span[normalize-space()='Version:']

@FindBy(xpath="//span[normalize-space()='Version:']")
private WebElement VersionVisible ;	
	
public void VersionVisible()throws InterruptedException{

	boolean flag=VersionVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Version:']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Version is not Displayed");
		Reporter.log("FAIL >> Version is not Displayed",true);
	}
}

//span[normalize-space()='Published:']

@FindBy(xpath="//span[normalize-space()='Published:']")
private WebElement PublishedVisible ;	
	
public void PublishedVisible()throws InterruptedException{

	boolean flag=PublishedVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Published:']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Published is not Displayed");
		Reporter.log("FAIL >> Published is not Displayed",true);
	}
}

//span[normalize-space()='Disable Workflow']

@FindBy(xpath="//span[normalize-space()='Disable Workflow']")
private WebElement DisableWorkflowVisible ;	
	
public void DisableWorkflowVisible()throws InterruptedException{

	boolean flag=DisableWorkflowVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Disable Workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Disable Workflow is not Displayed");
		Reporter.log("FAIL >> Disable Workflow is not Displayed",true);
	}
}

//span[normalize-space()='View Workflow']

@FindBy(xpath="//span[normalize-space()='View Workflow']")
private WebElement ViewWorkflowVisible ;	
	
public void ViewWorkflowVisible()throws InterruptedException{

	boolean flag=ViewWorkflowVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='View Workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> View Workflow is not Displayed");
		Reporter.log("FAIL >> View Workflow is not Displayed",true);
	}
}

//div[@title='Draft Changeset']

@FindBy(xpath="//div[@title='Draft Changeset']")
private WebElement DraftChangesetVisible ;	
	
public void DraftChangesetVisible()throws InterruptedException{

	boolean flag=DraftChangesetVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@title='Draft Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Draft Changeset is not Displayed");
		Reporter.log("FAIL >> Draft Changeset is not Displayed",true);
	}
}

//span[normalize-space()='Reset Draft']

@FindBy(xpath="//span[normalize-space()='Reset Draft']")
private WebElement ResetDraftVisibles ;	
	
public void ResetDraftVisibles()throws InterruptedException{

	boolean flag=ResetDraftVisibles.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Reset Draft is not Displayed");
		Reporter.log("FAIL >> Reset Draft is not Displayed",true);
	}
}

//span[normalize-space()='Publish Draft']

@FindBy(xpath="//span[normalize-space()='Publish Draft']")
private WebElement PublishDraftVisible ;	
	
public void PublishDraftVisible()throws InterruptedException{

	boolean flag=PublishDraftVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Publish Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Publish Draft is not Displayed");
		Reporter.log("FAIL >> Publish Draft is not Displayed",true);
	}
}

//span[normalize-space()='Edit Draft']

@FindBy(xpath="//span[normalize-space()='Edit Draft']")
private WebElement EditDraftVisible ;	
	
public void EditDraftVisible()throws InterruptedException{

	boolean flag=EditDraftVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Edit Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Edit Draft is not Displayed");
		Reporter.log("FAIL >> Edit Draft is not Displayed",true);
	}
}


//div[@title='Archived Workflows']

@FindBy(xpath="//div[@title='Archived Workflows']")
private WebElement ArchivedWorkflowsVisible ;	
	
public void ArchivedWorkflowsVisible()throws InterruptedException{

	boolean flag=ArchivedWorkflowsVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@title='Archived Workflows']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Archived Workflows is not Displayed");
		Reporter.log("FAIL >> Archived Workflows is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Disable Workflow']")
private WebElement ClickonDisableWorkflow ;	

public void ClickonDisableWorkflow() throws InterruptedException {
	sleep(2);
	ClickonDisableWorkflow.click();
	Reporter.log("PASS >> Click on Disable Workflow", true);
	sleep(2);
}


//p[contains(text(),'This will remove the survey ')]

@FindBy(xpath="//p[contains(text(),'This will remove the survey ')]")
private WebElement WarningMessageVisibles ;	
	
public void WarningMessageVisibles()throws InterruptedException{

	boolean flag=WarningMessageVisibles.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[contains(text(),'This will remove the survey ')]")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Warning Message is not Displayed");
		Reporter.log("FAIL >> Warning Message is not Displayed",true);
	}
}


//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelVisibles ;	
	
public void CancelVisibles()throws InterruptedException{

	boolean flag=CancelVisibles.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cancel is not Displayed");
		Reporter.log("FAIL >> Cancel is not Displayed",true);
	}
}

//span[normalize-space()='Disable Survey Variation']

@FindBy(xpath="//span[normalize-space()='Disable Survey Variation']")
private WebElement DisableSurveyVariationVisible ;	
	
public void DisableSurveyVariationVisible()throws InterruptedException{

	boolean flag=DisableSurveyVariationVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Disable Survey Variation']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Disable Survey Variation is not Displayed");
		Reporter.log("FAIL >> Disable Survey Variation is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickonCancel ;	

public void ClickonCancel() throws InterruptedException {
	sleep(2);
	ClickonCancel.click();
	Reporter.log("PASS >> Click on Cancel", true);
	sleep(2);
}

@FindBy(xpath="//span[normalize-space()='Disable Workflow']")
private WebElement ClickontheDisableWorkflow ;	

public void ClickontheDisableWorkflow() throws InterruptedException {
	sleep(2);
	ClickontheDisableWorkflow.click();
	Reporter.log("PASS >> Click on Disable Workflow", true);
	sleep(2);
}


@FindBy(xpath="//span[normalize-space()='Disable Survey Variation']")
private WebElement ClickontheDisableSurveyVariation ;	

public void ClickontheDisableSurveyVariation() throws InterruptedException {
	sleep(2);
	ClickontheDisableSurveyVariation.click();
	Reporter.log("PASS >> Click on Disable Survey Variation", true);
	sleep(2);
}

//span[normalize-space()='View Workflow']

@FindBy(xpath="//span[normalize-space()='View Workflow']")
private WebElement ViewWorkflowisVisible ;	
	
public void ViewWorkflowisVisible()throws InterruptedException{

	boolean flag=ViewWorkflowisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='View Workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> View Workflow is not Displayed");
		Reporter.log("FAIL >> View Workflow is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='View Workflow']")
private WebElement ClickontheViewWorkflow ;	

public void ClickontheViewWorkflow() throws InterruptedException {
	sleep(2);
	ClickontheViewWorkflow.click();
	Reporter.log("PASS >> Click on View Workflow", true);
	sleep(2);
}

//h1[normalize-space()='Start of day survey']

@FindBy(xpath="//h1[normalize-space()='Start of day survey']")
private WebElement StartofdaysurveyVisible ;	
	
public void StartofdaysurveyVisible()throws InterruptedException{

	boolean flag=StartofdaysurveyVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[normalize-space()='Start of day survey']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Start of day survey is not Displayed");
		Reporter.log("FAIL >> Start of day survey is not Displayed",true);
	}
}

//div[@class='MuiAlert-message']

@FindBy(xpath="//div[@class='MuiAlert-message']")
private WebElement MuiAlertmessageVisible ;	
	
public void MuiAlertmessageVisible()throws InterruptedException{

	boolean flag=MuiAlertmessageVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@class='MuiAlert-message']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> MuiAlert-message is not Displayed");
		Reporter.log("FAIL >> MuiAlert-message is not Displayed",true);
	}
}

//span[normalize-space()='CheckIn']

@FindBy(xpath="//span[normalize-space()='CheckIn']")
private WebElement CheckInVisible ;	
	
public void CheckInVisible()throws InterruptedException{

	boolean flag=CheckInVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='CheckIn']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> CheckIn is not Displayed");
		Reporter.log("FAIL >> CheckIn is not Displayed",true);
	}
}


//span[normalize-space()='Tool Check']

@FindBy(xpath="//span[normalize-space()='Tool Check']")
private WebElement ToolCheckVisible ;	
	
public void ToolCheckVisible()throws InterruptedException{

	boolean flag=ToolCheckVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Tool Check']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Tool Check is not Displayed");
		Reporter.log("FAIL >> Tool Check is not Displayed",true);
	}
}

//span[normalize-space()='Stock Check']

@FindBy(xpath="//span[normalize-space()='Stock Check']")
private WebElement StockCheckVisible ;	
	
public void StockCheckVisible()throws InterruptedException{

	boolean flag=StockCheckVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Stock Check']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Stock Check is not Displayed");
		Reporter.log("FAIL >> Stock Check is not Displayed",true);
	}
}

//span[normalize-space()='Test Index Page']

@FindBy(xpath="//span[normalize-space()='Test Index Page']")
private WebElement TestIndexPageVisible ;	
	
public void TestIndexPageVisible()throws InterruptedException{

	boolean flag=TestIndexPageVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Test Index Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Test Index Page is not Displayed");
		Reporter.log("FAIL >> Test Index Page is not Displayed",true);
	}
}

//span[normalize-space()='Test Page One']

@FindBy(xpath="//span[normalize-space()='Test Page One']")
private WebElement TestPageOneVisible ;	
	
public void TestPageOneVisible()throws InterruptedException{

	boolean flag=TestPageOneVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Test Page One']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Test Page One is not Displayed");
		Reporter.log("FAIL >> Test Page One is not Displayed",true);
	}
}

//span[normalize-space()='Test Page Two']

@FindBy(xpath="//span[normalize-space()='Test Page Two']")
private WebElement TestPageTwoVisible ;	
	
public void TestPageTwoVisible()throws InterruptedException{

	boolean flag=TestPageTwoVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Test Page Two']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Test Page Two is not Displayed");
		Reporter.log("FAIL >> Test Page Two is not Displayed",true);
	}
}

//span[normalize-space()='PPE']

@FindBy(xpath="//span[normalize-space()='PPE']")
private WebElement PPEVisible ;	
	
public void PPEVisible()throws InterruptedException{

	boolean flag=PPEVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='PPE']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>PPE is not Displayed");
		Reporter.log("FAIL >> PPE is not Displayed",true);
	}
}


//span[normalize-space()='Reset Draft']

@FindBy(xpath="//span[normalize-space()='Reset Draft']")
private WebElement ResetDraftVisible ;	
	
public void ResetDraftVisible()throws InterruptedException{

	boolean flag=ResetDraftVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Reset Draft is not Displayed");
		Reporter.log("FAIL >> Reset Draft is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Reset Draft']")
private WebElement ClickontheResetDraft ;	

public void ClickontheResetDraft() throws InterruptedException {
	sleep(2);
	ClickontheResetDraft.click();
	Reporter.log("PASS >> Click on RESET DRAFT", true);
	sleep(2);
}

//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']

@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement WarningMessageVisible ;	
	
public void WarningMessageVisible()throws InterruptedException{

	boolean flag=WarningMessageVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Warning Message is not Displayed");
		Reporter.log("FAIL >> Warning Message is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelVisible ;	
	
public void CancelVisible()throws InterruptedException{

	boolean flag=CancelVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel is not Displayed");
		Reporter.log("FAIL >> Cancel is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickontheCancelbutton ;	

public void ClickontheCancelbutton() throws InterruptedException {
	sleep(2);
	ClickontheCancelbutton.click();
	Reporter.log("PASS >> Click on Cancel", true);
	sleep(2);
}

//@FindBy(xpath="//span[normalize-space()='Reset Draft']")
//private WebElement ClickontheResetDraft ;	
//
//public void ClickontheResetDraft() throws InterruptedException {
//	sleep(2);
//	ClickontheResetDraft.click();
//	Reporter.log("PASS >> Click on RESET DRAFT", true);
//	sleep(2);
//}

//span[normalize-space()='Reset Changeset']

@FindBy(xpath="//span[normalize-space()='Reset Changeset']")
private WebElement ResetChangesetVisible ;	
	
public void ResetChangesetVisible()throws InterruptedException{

	boolean flag=ResetChangesetVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Reset Changeset is not Displayed");
		Reporter.log("FAIL >> Reset Changeset is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Reset Changeset']")
private WebElement ClickontheResetChangeset ;	

public void ClickontheResetChangeset() throws InterruptedException {
	sleep(2);
	ClickontheResetChangeset.click();
	Reporter.log("PASS >> Click on Reset Changeset", true);
	sleep(2);
}

//span[normalize-space()='Publish Draft']

@FindBy(xpath="//span[normalize-space()='Publish Draft']")
private WebElement PublishDraftisVisible ;	
	
public void PublishDraftisVisible()throws InterruptedException{

	boolean flag=PublishDraftisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Publish Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Publish Draft is not Displayed");
		Reporter.log("FAIL >> Publish Draft is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Publish Draft']")
private WebElement ClickonthePublishDraft ;	

public void ClickonthePublishDraft() throws InterruptedException {
	sleep(2);
	ClickonthePublishDraft.click();
	Reporter.log("PASS >> Click on Reset Changeset", true);
	sleep(2);
}


//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']

@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement MessageisbeenVisible ;	
	
public void MessageisbeenVisible()throws InterruptedException{

	boolean flag=MessageisbeenVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Message is not Displayed");
		Reporter.log("FAIL >> Message is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelbuttonVisible ;	
	
public void CancelbuttonVisible()throws InterruptedException{

	boolean flag=CancelbuttonVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel Button is not Displayed");
		Reporter.log("FAIL >> Cancel Button is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickontheCancelButton ;	

public void ClickontheCancelButton() throws InterruptedException {
	sleep(2);
	ClickontheCancelButton.click();
	Reporter.log("PASS >> Click on Cancel Button", true);
	sleep(2);
}

//span[normalize-space()='Publish Draft Changeset']

@FindBy(xpath="//span[normalize-space()='Publish Draft Changeset']")
private WebElement PublishDraftChangesetVisible ;	
	
public void PublishDraftChangesetVisible()throws InterruptedException{

	boolean flag=PublishDraftChangesetVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Publish Draft Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Publish Draft Changeset is not Displayed");
		Reporter.log("FAIL >> Publish Draft Changeset is not Displayed",true);
	}
}

//span[normalize-space()='Reset Changeset']

@FindBy(xpath="//span[normalize-space()='Reset Changeset']")
private WebElement ResetChangesetisVisible ;	
	
public void ResetChangesetisVisible()throws InterruptedException{

	boolean flag=ResetChangesetisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Reset Changeset is not Displayed");
		Reporter.log("FAIL >> Reset Changeset is not Displayed",true);
	}
}


//span[normalize-space()='Edit Draft']

@FindBy(xpath="//span[normalize-space()='Edit Draft']")
private WebElement EditDraftisVisible ;	
	
public void EditDraftisVisible()throws InterruptedException{

	boolean flag=EditDraftisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Edit Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Edit Draft is not Displayed");
		Reporter.log("FAIL >> Edit Draft is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Edit Draft']")
private WebElement ClickontheEditDraftButton ;	

public void ClickontheEditDraftButton() throws InterruptedException {
	sleep(2);
	ClickontheEditDraftButton.click();
	Reporter.log("PASS >> Click on Cancel Button", true);
	sleep(2);
}

//span[normalize-space()='New Page']

@FindBy(xpath="//span[normalize-space()='New Page']")
private WebElement NewPageisVisible ;	
	
public void NewPageisVisible()throws InterruptedException{

	boolean flag=NewPageisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='New Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page is not Displayed");
		Reporter.log("FAIL >> New Page is not Displayed",true);
	}
}

//span[text()='CheckIn']
@FindBy(xpath="//span[text()='CheckIn']")
private WebElement CheckInisVisible ;	
	
public void CheckInisVisible()throws InterruptedException{

	boolean flag=CheckInisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='CheckIn']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>CheckIn is not Displayed");
		Reporter.log("FAIL >> CheckIn is not Displayed",true);
	}
}

//span[text()='Tool Check']


@FindBy(xpath="//span[text()='Tool Check']")
private WebElement ToolCheckisVisible ;	
	
public void ToolCheckisVisible()throws InterruptedException{

	boolean flag=ToolCheckisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Tool Check']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Tool Check is not Displayed");
		Reporter.log("FAIL >> Tool Check is not Displayed",true);
	}
}

//span[text()='Stock Check']

@FindBy(xpath="//span[text()='Stock Check']")
private WebElement StockCheckisVisible ;	
	
public void StockCheckisVisible()throws InterruptedException{

	boolean flag=StockCheckisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Stock Check']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Stock Check is not Displayed");
		Reporter.log("FAIL >> Stock Check is not Displayed",true);
	}
}

//span[text()='Test Index Page']

@FindBy(xpath="//span[text()='Test Index Page']")
private WebElement TestIndexPageisVisible ;	
	
public void TestIndexPageisVisible()throws InterruptedException{

	boolean flag=TestIndexPageisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Test Index Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Test Index Page is not Displayed");
		Reporter.log("FAIL >> Test Index Page is not Displayed",true);
	}
}

//span[text()='Test Page One']

@FindBy(xpath="//span[text()='Test Index Page']")
private WebElement TestPageOneisVisible ;	
	
public void TestPageOneisVisible()throws InterruptedException{

	boolean flag=TestPageOneisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Test Index Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Test Page One is not Displayed");
		Reporter.log("FAIL >> Test Page One is not Displayed",true);
	}
}

//span[text()='Test Page Two']

@FindBy(xpath="//span[text()='Test Page Two']")
private WebElement TestPageTwoisVisible ;	
	
public void TestPageTwoisVisible()throws InterruptedException{

	boolean flag=TestPageTwoisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Test Page Two']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Test Page Two is not Displayed");
		Reporter.log("FAIL >> Test Page Two is not Displayed",true);
	}
}

//span[text()='PPE']

@FindBy(xpath="//span[text()='PPE']")
private WebElement PPEisVisible ;	
	
public void PPEisVisible()throws InterruptedException{

	boolean flag=PPEisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='PPE']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>PPE is not Displayed");
		Reporter.log("FAIL >> PPE is not Displayed",true);
	}
}

//span[normalize-space()='New Page']
@FindBy(xpath="//span[normalize-space()='New Page']")
private WebElement ClickontheNewPageButton ;	

public void ClickontheNewPageButton() throws InterruptedException {
	sleep(2);
	ClickontheNewPageButton.click();
	Reporter.log("PASS >> Click on New Page", true);
	sleep(2);
}


//h3[normalize-space()='Page Element']

@FindBy(xpath="//h3[normalize-space()='Page Element']")
private WebElement PageElementisVisible ;	
	
public void PageElementisVisible()throws InterruptedException{

	boolean flag=PageElementisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Page Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Page Element is not Displayed");
		Reporter.log("FAIL >> Page Element is not Displayed",true);
	}
}

//*[@value='New Page']

@FindBy(xpath="//*[@value='New Page']")
private WebElement NewPagetitleisVisible ;	
	
public void NewPagetitleisVisible()throws InterruptedException{

	boolean flag=NewPagetitleisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[@value='New Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New page title is not Displayed");
		Reporter.log("FAIL >> New page title is not Displayed",true);
	}
}

//*[text()= 'Description']

@FindBy(xpath="//*[text()= 'Description']")
private WebElement DescriptionfieldisVisible ;	
	
public void DescriptionfieldisVisible()throws InterruptedException{

	boolean flag=DescriptionfieldisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()= 'Description']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Description is not Displayed");
		Reporter.log("FAIL >> Description is not Displayed",true);
	}
}

//label[text()= 'Button Text']

@FindBy(xpath="//label[text()= 'Button Text']")
private WebElement ButtonTextfieldisVisible ;	
	
public void ButtonTextfieldisVisible()throws InterruptedException{

	boolean flag=ButtonTextfieldisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//label[text()= 'Button Text']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Button Text is not Displayed");
		Reporter.log("FAIL >> Button Text is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelButtonfieldisVisible ;	
	
public void CancelButtonfieldisVisible()throws InterruptedException{

	boolean flag=CancelButtonfieldisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel is not Displayed");
		Reporter.log("FAIL >> Cancel is not Displayed",true);
	}
}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement SaveButtonfieldisVisible ;	
	
public void SaveButtonfieldisVisible()throws InterruptedException{

	boolean flag=SaveButtonfieldisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Save is not Displayed");
		Reporter.log("FAIL >> Save is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickontheCancelButtonfield ;	

public void ClickontheCancelButtonfield() throws InterruptedException {
	sleep(2);
	ClickontheCancelButtonfield.click();
	Reporter.log("PASS >> Click on Cancel Button", true);
	sleep(2);
}

@FindBy(xpath="//span[text()='CheckIn']")
private WebElement ClickontheCheckInButtonfield ;	

public void ClickontheCheckInButtonfield() throws InterruptedException {
	sleep(2);
	ClickontheCheckInButtonfield.click();
	Reporter.log("PASS >> Click on CheckIn Button", true);
	sleep(2);
}

//h3[normalize-space()='Page Element']

@FindBy(xpath="//h3[normalize-space()='Page Element']")
private WebElement PageElementfieldofCheckInisVisible ;	
	
public void PageElementfieldofCheckInisVisible()throws InterruptedException{

	boolean flag=PageElementfieldofCheckInisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Page Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Page Element is not Displayed");
		Reporter.log("FAIL >> Page Element is not Displayed",true);
	}
}

//*[@value='CheckIn']

@FindBy(xpath="//*[@value='CheckIn']")
private WebElement fieldofCheckInTitleisVisible ;	
	
public void fieldofCheckInTitleisVisible()throws InterruptedException{

	boolean flag=fieldofCheckInTitleisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[@value='CheckIn']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>CheckIn Title is not Displayed");
		Reporter.log("FAIL >> CheckIn Title is not Displayed",true);
	}
}

//*[text()='Vehicle']

@FindBy(xpath="//*[text()='Vehicle']")
private WebElement fieldofCheckInVehicleisVisible ;	
	
public void fieldofCheckInVehicleisVisible()throws InterruptedException{

	boolean flag=fieldofCheckInVehicleisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[text()='Vehicle']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>CheckIn Vehicle is not Displayed");
		Reporter.log("FAIL >> CheckIn Vehicle is not Displayed",true);
	}
}

//*[@value='Continue']

@FindBy(xpath="//*[@value='Continue']")
private WebElement fieldofCheckInContinueisVisible ;	
	
public void fieldofCheckInContinueisVisible()throws InterruptedException{

	boolean flag=fieldofCheckInContinueisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[@value='Continue']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>CheckIn Continue is not Displayed");
		Reporter.log("FAIL >> CheckIn Continue is not Displayed",true);
	}
}
//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement fieldofCheckInRemoveisVisible ;	
	
public void fieldofCheckInRemoveisVisible()throws InterruptedException{

	boolean flag=fieldofCheckInRemoveisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Remove is not Displayed");
		Reporter.log("FAIL >> Remove is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']
@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement fieldofCheckInCancelisVisible ;	
	
public void fieldofCheckInCancelisVisible()throws InterruptedException{

	boolean flag=fieldofCheckInCancelisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel is not Displayed");
		Reporter.log("FAIL >> Cancel is not Displayed",true);
	}
}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement fieldofCheckInSaveisVisible ;	
	
public void fieldofCheckInSaveisVisible()throws InterruptedException{

	boolean flag=fieldofCheckInSaveisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Save is not Displayed");
		Reporter.log("FAIL >> Save is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickontheCancelofCheckInButtonfield ;	

public void ClickontheCancelofCheckInButtonfield() throws InterruptedException {
	sleep(2);
	ClickontheCancelofCheckInButtonfield.click();
	Reporter.log("PASS >> Click on CheckIn Button", true);
	sleep(2);
}

//span[text()='Break']
//span[normalize-space()='Take your break']
@FindBy(xpath="//span[normalize-space()='Take your break']")
private WebElement ClickontheBreakoption ;	

public void ClickontheBreakoption() throws InterruptedException {
	sleep(2);
	ClickontheBreakoption.click();
	Reporter.log("PASS >> Click on Break option", true);
	sleep(2);
}


//h2[text()='Break']

@FindBy(xpath="//h2[normalize-space()='Take your break']")
private WebElement BreakTextisVisible ;	
	
public void BreakTextisVisible()throws InterruptedException{

	boolean flag=BreakTextisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h2[normalize-space()='Take your break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Take Your Break is not Displayed");
		Reporter.log("FAIL >>Take your Break is not Displayed",true);
	}
}

//div[@title='Now Serving']

@FindBy(xpath="//div[@title='Now Serving']")
private WebElement NOWSERVINGTextisVisible ;	
	
public void NOWSERVINGTextisVisible()throws InterruptedException{

	boolean flag=NOWSERVINGTextisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@title='Now Serving']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Now Serving is not Displayed");
		Reporter.log("FAIL >> Now Serving is not Displayed",true);
	}
}


//span[@title='Version']

@FindBy(xpath="//span[@title='Version']")
private WebElement VERSIONTextisVisible ;	
	
public void VERSIONTextisVisible()throws InterruptedException{

	boolean flag=VERSIONTextisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@title='Version']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Version is not Displayed");
		Reporter.log("FAIL >> Version is not Displayed",true);
	}
}


//span[@title='Published']

@FindBy(xpath="//span[@title='Published']")
private WebElement PUBLISHEDTextisVisible ;	
	
public void PUBLISHEDTextisVisible()throws InterruptedException{

	boolean flag=PUBLISHEDTextisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@title='Published']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Published is not Displayed");
		Reporter.log("FAIL >> Published is not Displayed",true);
	}
}

//span[normalize-space()='Disable Workflow']

@FindBy(xpath="//span[normalize-space()='Disable Workflow']")
private WebElement DISABLEWORKFLOWBUTTONisVisible ;	
	
public void DISABLEWORKFLOWBUTTONisVisible()throws InterruptedException{

	boolean flag=DISABLEWORKFLOWBUTTONisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Disable Workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Disable Workflow is not Displayed");
		Reporter.log("FAIL >> Disable Workflow is not Displayed",true);
	}
}

//span[normalize-space()='View Workflow']

@FindBy(xpath="//span[normalize-space()='View Workflow']")
private WebElement VIEWWORKFLOWBUTTONisVisible ;	
	
public void VIEWWORKFLOWBUTTONisVisible()throws InterruptedException{

	boolean flag=VIEWWORKFLOWBUTTONisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='View Workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>View Workflow is not Displayed");
		Reporter.log("FAIL >> View Workflow is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Disable Workflow']")
private WebElement ClickontheDISABLEWORKFLOW ;	

public void ClickontheDISABLEWORKFLOW() throws InterruptedException {
	sleep(2);
	ClickontheDISABLEWORKFLOW.click();
	Reporter.log("PASS >> Click on Disable Workflow", true);
	sleep(2);
}


//h2[normalize-space()='Warning: Survey will be removed from active use']

@FindBy(xpath="//h2[normalize-space()='Warning: Survey will be removed from active use']")
private WebElement WARNINGMESSAGEisVisible ;	
	
public void WARNINGMESSAGEisVisible()throws InterruptedException{

	boolean flag=WARNINGMESSAGEisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h2[normalize-space()='Warning: Survey will be removed from active use']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>WARNING MESSAGE is not Displayed");
		Reporter.log("FAIL >> WARNING MESSAGE is not Displayed",true);
	}
}

//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']


@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement TEXTMESSAGEisVisible ;	
	
public void TEXTMESSAGEisVisible()throws InterruptedException{

	boolean flag=TEXTMESSAGEisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEXT MESSAGE is not Displayed");
		Reporter.log("FAIL >> TEXT MESSAGE is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CANCELTABisVisible ;	
	
public void CANCELTABisVisible()throws InterruptedException{

	boolean flag=CANCELTABisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>CANCEL TAB is not Displayed");
		Reporter.log("FAIL >> CANCEL TAB is not Displayed",true);
	}
}

//span[normalize-space()='Disable Survey Variation']

@FindBy(xpath="//span[normalize-space()='Disable Survey Variation']")
private WebElement DISABLESURVEYVARIATIONTABisVisible ;	
	
public void DISABLESURVEYVARIATIONTABisVisible()throws InterruptedException{

	boolean flag=DISABLESURVEYVARIATIONTABisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Disable Survey Variation']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Disable Survey Variation is not Displayed");
		Reporter.log("FAIL >> Disable Survey Variation is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickontheCANCELTAB ;	

public void ClickontheCANCELTAB() throws InterruptedException {
	sleep(2);
	ClickontheCANCELTAB.click();
	Reporter.log("PASS >> Click on Disable Workflow", true);
	sleep(2);
}

//span[normalize-space()='View Workflow']

@FindBy(xpath="//span[normalize-space()='View Workflow']")
private WebElement ClickontheVIEWWORKFLOWTAB ;	

public void ClickontheVIEWWORKFLOWTAB() throws InterruptedException {
	sleep(2);
	ClickontheVIEWWORKFLOWTAB.click();
	Reporter.log("PASS >> Click on View Workflow", true);
	sleep(2);
}

//h1[normalize-space()='Autogenerated']

@FindBy(xpath="//h1[normalize-space()='Autogenerated']")
private WebElement AutogeneratedMessageisVisible ;	
	
public void AutogeneratedMessageisVisible()throws InterruptedException{

	boolean flag=AutogeneratedMessageisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[normalize-space()='Autogenerated']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Autogenerated Message is not Displayed");
		Reporter.log("FAIL >> Autogenerated Message  is not Displayed",true);
	}
}

//span[normalize-space()='Break']

@FindBy(xpath="//span[normalize-space()='Break']")
private WebElement BreakTabisVisible ;	
	
public void BreakTabisVisible()throws InterruptedException{

	boolean flag=BreakTabisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Break Tab is not Displayed");
		Reporter.log("FAIL >> Break Tab  is not Displayed",true);
	}
}

//span[normalize-space()='Complete Break']

@FindBy(xpath="//span[normalize-space()='Complete Break']")
private WebElement CompleteBreakTabisVisible ;	
	
public void CompleteBreakTabisVisible()throws InterruptedException{

	boolean flag=CompleteBreakTabisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Complete Break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Complete Break is not Displayed");
		Reporter.log("FAIL >> Complete Break  is not Displayed",true);
	}
}

//div[@title='Draft Changeset']

@FindBy(xpath="//div[@title='Draft Changeset']")
private WebElement DRAFTCHANGESETTabisVisible ;	
	
public void DRAFTCHANGESETTabisVisible()throws InterruptedException{

	boolean flag=DRAFTCHANGESETTabisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@title='Draft Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Draft Changeset is not Displayed");
		Reporter.log("FAIL >> Draft Changeset  is not Displayed",true);
	}
}

//span[normalize-space()='Reset Draft']

@FindBy(xpath="//span[normalize-space()='Reset Draft']")
private WebElement RESETDRAFTTabisVisible ;	
	
public void RESETDRAFTTabisVisible()throws InterruptedException{

	boolean flag=RESETDRAFTTabisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Reset Draft is not Displayed");
		Reporter.log("FAIL >> Reset Draft  is not Displayed",true);
	}
}

//span[normalize-space()='Publish Draft']

@FindBy(xpath="//span[normalize-space()='Publish Draft']")
private WebElement PUBLISHDRAFTTabisVisible ;	
	
public void PUBLISHDRAFTTabisVisible()throws InterruptedException{

	boolean flag=PUBLISHDRAFTTabisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Publish Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Publish Draft is not Displayed");
		Reporter.log("FAIL >> Publish Draft  is not Displayed",true);
	}
}

//span[normalize-space()='Edit Draft']

@FindBy(xpath="//span[normalize-space()='Edit Draft']")
private WebElement EDITDRAFTTabisVisible ;	
	
public void EDITDRAFTTabisVisible()throws InterruptedException{

	boolean flag=EDITDRAFTTabisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Edit Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Edit Draft is not Displayed");
		Reporter.log("FAIL >> Edit Draft  is not Displayed",true);
	}
}

//h2[normalize-space()='Warning: All pending changes will be lost!']

@FindBy(xpath="//h2[normalize-space()='Warning: All pending changes will be lost!']")
private WebElement MESSAGEWARNINGTABisVisible ;	
	
public void MESSAGEWARNINGTABisVisible()throws InterruptedException{

	boolean flag=MESSAGEWARNINGTABisVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h2[normalize-space()='Warning: All pending changes will be lost!']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>WARNING is not Displayed");
		Reporter.log("FAIL >> WARNING  is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Reset Draft']")
private WebElement ClickontheRESETDraftTAB ;	

public void ClickontheRESETDraftTAB() throws InterruptedException {
	sleep(2);
	ClickontheRESETDraftTAB.click();
	Reporter.log("PASS >> Click on Reset Draft", true);
	sleep(2);
}

//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']

@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement MESSAGETEXTisBEENVisible ;	
	
public void MESSAGETEXTisBEENVisible()throws InterruptedException{

	boolean flag=MESSAGETEXTisBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Message is not Displayed");
		Reporter.log("FAIL >> Message  is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']
@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CanceloptionisBEENVisible ;	
	
public void CanceloptionisBEENVisible()throws InterruptedException{

	boolean flag=CanceloptionisBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel is not Displayed");
		Reporter.log("FAIL >> Cancel  is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickontheCanceloptionTAB ;	

public void ClickontheCanceloptionTAB() throws InterruptedException {
	sleep(2);
	ClickontheCanceloptionTAB.click();
	Reporter.log("PASS >> Click on Cancel Tab", true);
	sleep(2);
}

//span[normalize-space()='Reset Changeset']

@FindBy(xpath="//span[normalize-space()='Reset Changeset']")
private WebElement RESETChangesetoptionisBEENVisible ;	
	
public void RESETChangesetoptionisBEENVisible()throws InterruptedException{

	boolean flag=RESETChangesetoptionisBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Reset Changeset is not Displayed");
		Reporter.log("FAIL >> Reset Changeset  is not Displayed",true);
	}
}

//span[normalize-space()='Publish Draft']

@FindBy(xpath="//span[normalize-space()='Publish Draft']")
private WebElement ClickonthePublishDraftoptionTAB ;	

public void ClickonthePublishDraftoptionTAB() throws InterruptedException {
	sleep(2);
	ClickonthePublishDraftoptionTAB.click();
	Reporter.log("PASS >> Click on Reset Draft", true);
	sleep(2);
}

//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']

@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement MESSTEXTisBEENVisible ;	
	
public void MESSTEXTisBEENVisible()throws InterruptedException{

	boolean flag=MESSTEXTisBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>MESSAGE is not Displayed");
		Reporter.log("FAIL >> MESSAGE  is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CANcelTaboptionisBEENVisible ;	
	
public void CANcelTaboptionisBEENVisible()throws InterruptedException{

	boolean flag=CANcelTaboptionisBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel is not Displayed");
		Reporter.log("FAIL >>Cancel  is not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickontheCANceloptionTAB ;	

public void ClickontheCANceloptionTAB() throws InterruptedException {
	sleep(2);
	ClickontheCANceloptionTAB.click();
	Reporter.log("PASS >> Click on Cancel Tab", true);
	sleep(2);
}

//span[normalize-space()='Publish Draft Changeset']

@FindBy(xpath="//span[normalize-space()='Publish Draft Changeset']")
private WebElement PUBlishDRAftCHAngesetTaboptionisBEENVisible ;	
	
public void PUBlishDRAftCHAngesetTaboptionisBEENVisible()throws InterruptedException{

	boolean flag=PUBlishDRAftCHAngesetTaboptionisBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Publish Draft Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Publish Draft Changeset is not Displayed");
		Reporter.log("FAIL >>Publish Draft Changeset  is not Displayed",true);
	}
}

//span[normalize-space()='Edit Draft']

@FindBy(xpath="//span[normalize-space()='Edit Draft']")
private WebElement ClickontheEDITDraftoptionTAB ;	

public void ClickontheEDITDraftoptionTAB() throws InterruptedException {
	sleep(2);
	ClickontheEDITDraftoptionTAB.click();
	Reporter.log("PASS >> Click on EDIT Draft", true);
	sleep(2);
}

//h1[normalize-space()='Break']
//h1[normalize-space()='Take your break']
@FindBy(xpath="//h1[normalize-space()='Take your break']")
private WebElement BREAKtextisBEENVisible ;	
	
public void BREAKtextisBEENVisible()throws InterruptedException{

	boolean flag=BREAKtextisBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[normalize-space()='Take your break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Take your BREAK is not Displayed");
		Reporter.log("FAIL >>Take your BREAK  is not Displayed",true);
	}
}

//span[normalize-space()='Back to Workflow Setup']
//span[normalize-space()='Back to Workflow Setup']
@FindBy(xpath="//span[normalize-space()='Back to Workflow Setup']")
private WebElement BacktoWorkflowSetupBEENVisible ;	
	
public void BacktoWorkflowSetupBEENVisible()throws InterruptedException{

	boolean flag=BacktoWorkflowSetupBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Back to Workflow Setup']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Back to Workflow Setup is not Displayed");
		Reporter.log("FAIL >>Back to Workflow Setup  is not Displayed",true);
	}
}

//span[normalize-space()='New Page']
//span[normalize-space()='New Page']
@FindBy(xpath="//span[normalize-space()='New Page']")
private WebElement NEWPAGEISBEENVisible ;	
	
public void NEWPAGEISBEENVisible()throws InterruptedException{

	boolean flag=NEWPAGEISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='New Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page is not Displayed");
		Reporter.log("FAIL >>New Page  is not Displayed",true);
	}
}

//span[text()='Break']

@FindBy(xpath="//span[text()='Start your break']")
private WebElement StartyourBREAKOPTIONISBEENVisible ;	
	
public void StartyourBREAKOPTIONISBEENVisible()throws InterruptedException{

	boolean flag=StartyourBREAKOPTIONISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Start your break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Start Your BREAK is not Displayed");
		Reporter.log("FAIL >>Start Your BREAK  is not Displayed",true);
	}
}

//span[text()='Complete Break']

@FindBy(xpath="//span[text()='Complete Break']")
private WebElement CompleteBreakISBEENVisible ;	
	
public void CompleteBreakISBEENVisible()throws InterruptedException{

	boolean flag=CompleteBreakISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Complete Break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Complete Break is not Displayed");
		Reporter.log("FAIL >>Complete Break  is not Displayed",true);
	}
}

@FindBy(xpath="//span[text()='Start your break']")
private WebElement ClickontheBREakoptionTAB ;	

public void ClickontheBREakoptionTAB() throws InterruptedException {
	sleep(2);
	ClickontheBREakoptionTAB.click();
	Reporter.log("PASS >> Click on Start Your break", true);
	sleep(2);
}

//h3[text()='Page Element']
//h3[normalize-space()='Page Element']
@FindBy(xpath="//h3[normalize-space()='Page Element']")
private WebElement PageElementtextISBEENVisible ;	
	
public void PageElementtextISBEENVisible()throws InterruptedException{

	boolean flag=PageElementtextISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Page Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Page Element is not Displayed");
		Reporter.log("FAIL >>Page Element  is not Displayed",true);
	}
}

//*[@value='Break']

@FindBy(xpath="//input[@aria-label='Title']")
private WebElement BREAKTITLEFIELDISBEENVisible ;	
	
public void BREAKTITLEFIELDISBEENVisible()throws InterruptedException{

	boolean flag=BREAKTITLEFIELDISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@aria-label='Title']")).getText();
		Reporter.log("PASS >> Text is Displayed : Title "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Start your BREAK TITLE is not Displayed");
		Reporter.log("FAIL >>Start Your BREAK TITLE  is not Displayed",true);
	}
}

//textarea[@aria-label='Description']

@FindBy(xpath="//textarea[@aria-label='Description']")
private WebElement DESCRIPTIONTITLEFIELDISBEENVisible ;	
	
public void DESCRIPTIONTITLEFIELDISBEENVisible()throws InterruptedException{

	boolean flag=DESCRIPTIONTITLEFIELDISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Description']")).getText();
		Reporter.log("PASS >> Text is Displayed : Description "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Description TITLE is not Displayed");
		Reporter.log("FAIL >>Description TITLE  is not Displayed",true);
	}
}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement REMOVEFIELDISBEENVisible ;	
	
public void REMOVEFIELDISBEENVisible()throws InterruptedException{

	boolean flag=REMOVEFIELDISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Remove is not Displayed");
		Reporter.log("FAIL >>Remove  is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CANCelFIELDtabISBEENVisible ;	
	
public void CANCelFIELDtabISBEENVisible()throws InterruptedException{

	boolean flag=CANCelFIELDtabISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>CANCEL is not Displayed");
		Reporter.log("FAIL >>CANCEL  is not Displayed",true);
	}
}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement SAVEFIELDtabISBEENVisible ;	
	
public void SAVEFIELDtabISBEENVisible()throws InterruptedException{

	boolean flag=SAVEFIELDtabISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>SAVE is not Displayed");
		Reporter.log("FAIL >>SAVE  is not Displayed",true);
	}
}

//span[text()='You are now taking your break']

@FindBy(xpath="//span[text()='You are now taking your break']")
private WebElement YouarenowtakingyourtextISBEENVisible ;	
	
public void YouarenowtakingyourtextISBEENVisible()throws InterruptedException{

	boolean flag=YouarenowtakingyourtextISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='You are now taking your break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>You are now taking your break is not Displayed");
		Reporter.log("FAIL >>You are now taking your break  is not Displayed",true);
	}
}

@FindBy(xpath="//span[text()='You are now taking your break']")
private WebElement ClickonYouarenowtakingyourbreakoptionTAB ;	

public void ClickonYouarenowtakingyourbreakoptionTAB() throws InterruptedException {
	sleep(2);
	ClickonYouarenowtakingyourbreakoptionTAB.click();
	Reporter.log("PASS >> Click on You are now taking your break", true);
	sleep(2);
}

//h3[text()='Instruction Question']

@FindBy(xpath="//h3[text()='Instruction Question']")
private WebElement InstructionQuestiontextISBEENVisible ;	
	
public void InstructionQuestiontextISBEENVisible()throws InterruptedException{

	boolean flag=InstructionQuestiontextISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[text()='Instruction Question']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Instruction Question is not Displayed");
		Reporter.log("FAIL >>Instruction Question  is not Displayed",true);
	}
}

//input[@value='You are now taking your break']
//input[@value='You are now taking your break']

@FindBy(xpath="//input[@value='You are now taking your break']")
private WebElement YouarenowtakingyourbreakQuestionISBEENVisible ;	
	
public void YouarenowtakingyourbreakQuestionISBEENVisible()throws InterruptedException{

	boolean flag=YouarenowtakingyourbreakQuestionISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='You are now taking your break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Question title is not Displayed");
		Reporter.log("FAIL >> Question title  is not Displayed",true);
	}
}

//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement QuestionNAMEISBEENVisible ;	
	
public void QuestionNAMEISBEENVisible()throws InterruptedException{

	boolean flag=QuestionNAMEISBEENVisible.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Question Name']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Question NAME is not Displayed");
		Reporter.log("FAIL >> Question NAME  is not Displayed",true);
	}
}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement REMOVETABISBEENDISPLAYED ;	
	
public void REMOVETABISBEENDISPLAYED()throws InterruptedException{

	boolean flag=REMOVETABISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Remove is not Displayed");
		Reporter.log("FAIL >> Remove  is not Displayed",true);
	}
}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CANCELTABISBEENDISPLAYED ;	
	
public void CANCELTABISBEENDISPLAYED()throws InterruptedException{

	boolean flag=CANCELTABISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> CANCEL is not Displayed");
		Reporter.log("FAIL >> CANCEL  is not Displayed",true);
	}
}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement SAVEOPTIONTABISBEENDISPLAYED ;	
	
public void SAVEOPTIONTABISBEENDISPLAYED()throws InterruptedException{

	boolean flag=SAVEOPTIONTABISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> SAVE is not Displayed");
		Reporter.log("FAIL >> SAVE  is not Displayed",true);
	}
}

//span[text()='The Break question']

@FindBy(xpath="//span[text()='The Break question']")
private WebElement TheBreakquestionISBEENDISPLAYED ;	
	
public void TheBreakquestionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=TheBreakquestionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='The Break question']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> The Break question is not Displayed");
		Reporter.log("FAIL >> The Break question  is not Displayed",true);
	}
}

@FindBy(xpath="//span[text()='The Break question']")
private WebElement ClickonTheBreakquestionoptionTAB ;	

public void ClickonTheBreakquestionoptionTAB() throws InterruptedException {
	sleep(2);
	ClickonTheBreakquestionoptionTAB.click();
	Reporter.log("PASS >> Click on The Break question", true);
	sleep(2);
}

//h3[text()='Question Select ']

@FindBy(xpath="//h3[text()='Question Select ']")
private WebElement QuestionSelecttextISBEENDISPLAYED ;	
	
public void QuestionSelecttextISBEENDISPLAYED()throws InterruptedException{

	boolean flag=QuestionSelecttextISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[text()='Question Select ']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> The Question Select is not Displayed");
		Reporter.log("FAIL >> The Question Select  is not Displayed",true);
	}
}

//input[@value='The Break question']

@FindBy(xpath="//input[@value='The Break question']")
private WebElement TheBreakquestiontextfieldISBEENDISPLAYED ;	
	
public void TheBreakquestiontextfieldISBEENDISPLAYED()throws InterruptedException{

	boolean flag=TheBreakquestiontextfieldISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='The Break question']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> The Break question is not Displayed");
		Reporter.log("FAIL >> The Break question is not Displayed",true);
	}
}
//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement QuestionNametextfieldISBEENDISPLAYED ;	
	
public void QuestionNametextfieldISBEENDISPLAYED()throws InterruptedException{

	boolean flag=QuestionNametextfieldISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Question Name']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Question Name is not Displayed");
		Reporter.log("FAIL >> Question Name is not Displayed",true);
	}
}

//legend[@class='MuiFormLabel-root']

@FindBy(xpath="//legend[@class='MuiFormLabel-root']")
private WebElement DisplaymodefieldISBEENDISPLAYED ;	
	
public void DisplaymodefieldISBEENDISPLAYED()throws InterruptedException{

	boolean flag=DisplaymodefieldISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//legend[@class='MuiFormLabel-root']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Display Mode is not Displayed");
		Reporter.log("FAIL >> Display Mode is not Displayed",true);
	}
}

//input[@value='radio']

@FindBy(xpath="//span[normalize-space()='Radio']")
private WebElement RadioButtonoptionISBEENDISPLAYED ;	
	
public void RadioButtonoptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=RadioButtonoptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Radio']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Radio button  is not Displayed");
		Reporter.log("FAIL >> Radio Button is not Displayed",true);
	}
}

//input[@value='dropdown']

@FindBy(xpath="//span[normalize-space()='Dropdown']")
private WebElement dropdownButtonoptionISBEENDISPLAYED ;	
	
public void dropdownButtonoptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=dropdownButtonoptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Dropdown']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> dropdown button  is not Displayed");
		Reporter.log("FAIL >> dropdown Button is not Displayed",true);
	}
}

//p[normalize-space()='Answer Options']

@FindBy(xpath="//p[normalize-space()='Answer Options']")
private WebElement AnswerOptionsISBEENDISPLAYED ;	
	
public void AnswerOptionsISBEENDISPLAYED()throws InterruptedException{

	boolean flag=AnswerOptionsISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[normalize-space()='Answer Options']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Answer Options  is not Displayed");
		Reporter.log("FAIL >> Answer Options is not Displayed",true);
	}
}

//*[@value='yes']

@FindBy(xpath="//*[@value='yes']")
private WebElement AnswerOptionsYESISBEENDISPLAYED ;	
	
public void AnswerOptionsYESISBEENDISPLAYED()throws InterruptedException{

	boolean flag=AnswerOptionsYESISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//*[@value='yes']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Answer Options TEXT FIELD  is not Displayed");
		Reporter.log("FAIL >> Answer Options TEXT FIELD is not Displayed",true);
	}
}

//span[normalize-space()='Add Answer Option']

@FindBy(xpath="//span[normalize-space()='Add Answer Option']")
private WebElement AddAnswerOptionISBEENDISPLAYED ;	
	
public void AddAnswerOptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=AddAnswerOptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Add Answer Option']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Add Answer Option  is not Displayed");
		Reporter.log("FAIL >> Add Answer Option is not Displayed",true);
	}

}

//span[normalize-space()='Optional ?']

@FindBy(xpath="//span[normalize-space()='Optional ?']")
private WebElement checkboxOptionISBEENDISPLAYED ;	
	
public void checkboxOptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=checkboxOptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Optional ?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> checkbox Option  is not Displayed");
		Reporter.log("FAIL >> checkbox Option is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement RemoveTABOptionISBEENDISPLAYED ;	
	
public void RemoveTABOptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=RemoveTABOptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> REMOVE Option  is not Displayed");
		Reporter.log("FAIL >> REMOVE Option is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelTABOptionISBEENDISPLAYED ;	
	
public void CancelTABOptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=CancelTABOptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cancel Option  is not Displayed");
		Reporter.log("FAIL >> Cancel Option is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement SAVEEETABOptionISBEENDISPLAYED ;	
	
public void SAVEEETABOptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=SAVEEETABOptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> SAVE Option  is not Displayed");
		Reporter.log("FAIL >> SAVE Option is not Displayed",true);
	}

}

//span[normalize-space()='Next']

@FindBy(xpath="//span[normalize-space()='Next']")
private WebElement NEXTTABOptionISBEENDISPLAYED ;	
	
public void NEXTTABOptionISBEENDISPLAYED()throws InterruptedException{

	boolean flag=NEXTTABOptionISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Next']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> NEXT Option  is not Displayed");
		Reporter.log("FAIL >> NEXT Option is not Displayed",true);
	}

}

@FindBy(xpath="//span[normalize-space()='Next']")
private WebElement ClickonTheNEXToptionTAB ;	

public void ClickonTheNEXToptionTAB() throws InterruptedException {
	sleep(2);
	ClickonTheNEXToptionTAB.click();
	Reporter.log("PASS >> Click on The NEXT OPTION", true);
	sleep(2);
}

//h3[normalize-space()='Button Element']

@FindBy(xpath="//h3[normalize-space()='Button Element']")
private WebElement ButtonElementISBEENDISPLAYED ;	
	
public void ButtonElementISBEENDISPLAYED()throws InterruptedException{

	boolean flag=ButtonElementISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Button Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Button Element  is not Displayed");
		Reporter.log("FAIL >> Button Element is not Displayed",true);
	}

}

//input[@value='Next']

@FindBy(xpath="//input[@value='Next']")
private WebElement NEXTBUTTONTEXTISBEENDISPLAYED ;	
	
public void NEXTBUTTONTEXTISBEENDISPLAYED()throws InterruptedException{

	boolean flag=NEXTBUTTONTEXTISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='Next']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> NEXT Button TEXT   is not Displayed");
		Reporter.log("FAIL >> NEXT Button TEXT  is not Displayed",true);
	}

}

//span[normalize-space()='Navigation Enabled?']V
@FindBy(xpath="//span[normalize-space()='Navigation Enabled?']")
private WebElement NavigationEnabledBUTTONISBEENDISPLAYED ;	
	
public void NavigationEnabledBUTTONISBEENDISPLAYED()throws InterruptedException{

	boolean flag=NavigationEnabledBUTTONISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Navigation Enabled?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Navigation Enabled Button is not Displayed");
		Reporter.log("FAIL >> Navigation Enabled Button is not Displayed",true);
	}

}

//span[normalize-space()='Actions Enabled?']

@FindBy(xpath="//span[normalize-space()='Actions Enabled?']")
private WebElement ActionsEnabledBUTTONISBEENDISPLAYED ;	
	
public void ActionsEnabledBUTTONISBEENDISPLAYED()throws InterruptedException{

	boolean flag=ActionsEnabledBUTTONISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Actions Enabled?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Actions Enabled Button is not Displayed");
		Reporter.log("FAIL >> Actions Enabled Button is not Displayed",true);
	}

}

//span[normalize-space()='Optional?']

@FindBy(xpath="//span[normalize-space()='Optional?']")
private WebElement OptionalBUTTONISBEENDISPLAYED ;	
	
public void OptionalBUTTONISBEENDISPLAYED()throws InterruptedException{

	boolean flag=OptionalBUTTONISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Optional?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Optional Button is not Displayed");
		Reporter.log("FAIL >> Optional Button is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement RemoveordeleteBUTTONISBEENDISPLAYED ;	
	
public void RemoveordeleteBUTTONISBEENDISPLAYED()throws InterruptedException{

	boolean flag=RemoveordeleteBUTTONISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Remove Button is not Displayed");
		Reporter.log("FAIL >> Remove Button is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelorstopBUTTONISBEENDISPLAYED ;	
	
public void CancelorstopBUTTONISBEENDISPLAYED()throws InterruptedException{

	boolean flag=CancelorstopBUTTONISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> cancel Button is not Displayed");
		Reporter.log("FAIL >> cancel Button is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement SaveorstoreBUTTONISBEENDISPLAYED ;	
	
public void SaveorstoreBUTTONISBEENDISPLAYED()throws InterruptedException{

	boolean flag=SaveorstoreBUTTONISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> SAVE Button is not Displayed");
		Reporter.log("FAIL >> SAVE Button is not Displayed",true);
	}

}

//span[normalize-space()='Complete Break']

@FindBy(xpath="//span[normalize-space()='Complete Break']")
private WebElement ClickontheCOMPLETEBREakoptionTAB ;	

public void ClickontheCOMPLETEBREakoptionTAB() throws InterruptedException {
	sleep(2);
	ClickontheCOMPLETEBREakoptionTAB.click();
	Reporter.log("PASS >> Click on Complete break", true);
	sleep(2);
}

//h3[normalize-space()='Page Element']

@FindBy(xpath="//h3[normalize-space()='Page Element']")
private WebElement PAGEELEMENTTEXTISBEENDISPLAYED ;	
	
public void PAGEELEMENTTEXTISBEENDISPLAYED()throws InterruptedException{

	boolean flag=PAGEELEMENTTEXTISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Page Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Page Element is not Displayed");
		Reporter.log("FAIL >> Page Element is not Displayed",true);
	}

}

//input[@value='Complete Break']

@FindBy(xpath="//input[@value='Complete Break']")
private WebElement CompleteBreakTITLEISBEENDISPLAYED ;	
	
public void CompleteBreakTITLEISBEENDISPLAYED()throws InterruptedException{

	boolean flag=CompleteBreakTITLEISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='Complete Break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Complete Break is not Displayed");
		Reporter.log("FAIL >> Complete Break is not Displayed",true);
	}

}

//textarea[@aria-label='Description']

@FindBy(xpath="//textarea[@aria-label='Description']")
private WebElement DescriptionFIELDTITLEISBEENDISPLAYED ;	
	
public void DescriptionFIELDTITLEISBEENDISPLAYED()throws InterruptedException{

	boolean flag=DescriptionFIELDTITLEISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Description']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Description is not Displayed");
		Reporter.log("FAIL >> Description is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement RemoveCAPISBEENDISPLAYED ;	
	
public void RemoveCAPISBEENDISPLAYED()throws InterruptedException{

	boolean flag=RemoveCAPISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> REMOVE is not Displayed");
		Reporter.log("FAIL >> REMOVE is not Displayed",true);
	}

}


//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CANCELCAPISBEENDISPLAYED ;	
	
public void CANCELCAPISBEENDISPLAYED()throws InterruptedException{

	boolean flag=CANCELCAPISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> CANCEL is not Displayed");
		Reporter.log("FAIL >> CANCEL is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement SAVECAPISBEENDISPLAYED ;	
	
public void SAVECAPISBEENDISPLAYED()throws InterruptedException{

	boolean flag=SAVECAPISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> SAVE is not Displayed");
		Reporter.log("FAIL >> SAVE is not Displayed",true);
	}

}

//(//span[text()='Complete Break'])[2]
		
@FindBy(xpath="(//span[text()='Complete Break'])[2]")
private WebElement completebreakCAPISBEENDISPLAYED ;	
			
public void completebreakCAPISBEENDISPLAYED()throws InterruptedException{

	boolean flag=completebreakCAPISBEENDISPLAYED.isDisplayed();
		if(flag)
			{	   		
				String option = Initialization.driver.findElement(By.xpath("(//span[text()='Complete Break'])[2]")).getText();
				Reporter.log("PASS >> Text is Displayed : "+option,true);
				sleep(2); 		
			}
		else
			{		
				ALib.AssertFailMethod("FAIL >> Complete Break is not Displayed");
				Reporter.log("FAIL >> Complete Break is not Displayed",true);
			}

		}

@FindBy(xpath="(//span[text()='Complete Break'])[2]")
private WebElement ClickontheCOMPLETEBREakCAPoptionTAB ;	

public void ClickontheCOMPLETEBREakCAPoptionTAB() throws InterruptedException {
	sleep(2);
	ClickontheCOMPLETEBREakCAPoptionTAB.click();
	Reporter.log("PASS >> Click on Complete break SUBMODULE", true);
	sleep(2);
}

//h3[normalize-space()='Button Element']

@FindBy(xpath="//h3[normalize-space()='Button Element']")
private WebElement ButtonElementCAPISBEENDISPLAYED ;	
	
public void ButtonElementCAPISBEENDISPLAYED()throws InterruptedException{

	boolean flag=ButtonElementCAPISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Button Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Button Element is not Displayed");
		Reporter.log("FAIL >> Button Element is not Displayed",true);
	}

}

//input[@value='Complete Break']

@FindBy(xpath="//input[@value='Complete Break']")
private WebElement CompleteBreaktextfieldCAPISBEENDISPLAYED ;	
	
public void CompleteBreaktextfieldCAPISBEENDISPLAYED()throws InterruptedException{

	boolean flag=CompleteBreaktextfieldCAPISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='Complete Break']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Complete Break text field is not Displayed");
		Reporter.log("FAIL >> Complete Break text field is not Displayed",true);
	}

}

//span[normalize-space()='Navigation Enabled?']

@FindBy(xpath="//span[normalize-space()='Navigation Enabled?']")
private WebElement NavigationEnabledcheckboxISBEENDISPLAYED ;	
	
public void NavigationEnabledcheckboxISBEENDISPLAYED()throws InterruptedException{

	boolean flag=NavigationEnabledcheckboxISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Navigation Enabled?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Navigation Enabled field is not Displayed");
		Reporter.log("FAIL >> Navigation Enabled field is not Displayed",true);
	}

}

//legend[@for='select-nav-type']

@FindBy(xpath="//legend[@for='select-nav-type']")
private WebElement NavigationtypetextISBEENDISPLAYED ;	
	
public void NavigationtypetextISBEENDISPLAYED()throws InterruptedException{

	boolean flag=NavigationtypetextISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//legend[@for='select-nav-type']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Navigation type field is not Displayed");
		Reporter.log("FAIL >> Navigation type field is not Displayed",true);
	}

}

//div[@id='select-nav-type']

@FindBy(xpath="//div[@id='select-nav-type']")
private WebElement NavigationtypeFIELDTOFILLISBEENDISPLAYED ;	
	
public void NavigationtypeFIELDTOFILLISBEENDISPLAYED()throws InterruptedException{

	boolean flag=NavigationtypeFIELDTOFILLISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@id='select-nav-type']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Navigation type field TO FILL is not Displayed");
		Reporter.log("FAIL >> Navigation type field TO FILL is not Displayed",true);
	}

}


//div[normalize-space()='Job list']
@FindBy(xpath="//div[normalize-space()='Job list']")
private WebElement SELECTtypeFIELDTOFILLISBEENDISPLAYED ;	
	
public void SELECTtypeFIELDTOFILLISBEENDISPLAYED()throws InterruptedException{

	boolean flag=SELECTtypeFIELDTOFILLISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[normalize-space()='Job list']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> SELECT type field TO FILL is not Displayed");
		Reporter.log("FAIL >> SELECT type field TO FILL is not Displayed",true);
	}

}

//span[normalize-space()='Actions Enabled?']

@FindBy(xpath="//span[normalize-space()='Actions Enabled?']")
private WebElement ACTIONENABLEDCHECKBOXISBEENDISPLAYED ;	
	
public void ACTIONENABLEDCHECKBOXISBEENDISPLAYED()throws InterruptedException{

	boolean flag=ACTIONENABLEDCHECKBOXISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Actions Enabled?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Actions Enabled CHECKBOX is not Displayed");
		Reporter.log("FAIL >> Actions Enabled CHECKBOX is not Displayed",true);
	}

}

//span[normalize-space()='Optional?']

@FindBy(xpath="//span[normalize-space()='Optional?']")
private WebElement OPTIONALCHECKBOXISBEENDISPLAYED ;	
	
public void OPTIONALCHECKBOXISBEENDISPLAYED()throws InterruptedException{

	boolean flag=OPTIONALCHECKBOXISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Optional?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> OPTIONAL CHECKBOX is not Displayed");
		Reporter.log("FAIL >> OPTIONAL CHECKBOX is not Displayed",true);
	}

}

@FindBy(xpath="//span[normalize-space()='End of Day']")
private WebElement ClickonEndOfDay ;	

public void ClickonEndOfDay() throws InterruptedException {
	sleep(2);
	ClickonEndOfDay.click();
	Reporter.log("PASS >> Click on End of Day", true);
	sleep(2);
}


//h2[normalize-space()='End of Day']

@FindBy(xpath="//h2[normalize-space()='End of Day']")
private WebElement EndofDaytextISBEENDISPLAYED ;	
	
public void EndofDaytextISBEENDISPLAYED()throws InterruptedException{

	boolean flag=EndofDaytextISBEENDISPLAYED.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h2[normalize-space()='End of Day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> End of Day is not Displayed");
		Reporter.log("FAIL >> End of Day is not Displayed",true);
	}

}

//div[@title='Now Serving']

@FindBy(xpath="//div[@title='Now Serving']")
private WebElement NowServingtextISBEENDISPLAYEDofendofday ;	
	
public void NowServingtextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=NowServingtextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@title='Now Serving']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Now Serving is not Displayed");
		Reporter.log("FAIL >> Now Serving is not Displayed",true);
	}

}


//span[@title='Version']

@FindBy(xpath="//span[@title='Version']")
private WebElement VersiontextISBEENDISPLAYEDofendofday ;	
	
public void VersiontextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=VersiontextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@title='Version']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Version is not Displayed");
		Reporter.log("FAIL >> Version is not Displayed",true);
	}

}

//span[@title='Published']

@FindBy(xpath="//span[@title='Published']")
private WebElement PublishedtextISBEENDISPLAYEDofendofday ;	
	
public void PublishedtextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=PublishedtextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@title='Published']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Published is not Displayed");
		Reporter.log("FAIL >> Published is not Displayed",true);
	}

}

//span[normalize-space()='Disable Workflow']

@FindBy(xpath="//span[normalize-space()='Disable Workflow']")
private WebElement DisableWorkflowtabISBEENDISPLAYEDofendofday ;	
	
public void DisableWorkflowtabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=DisableWorkflowtabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Disable Workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Disable Workflow is not Displayed");
		Reporter.log("FAIL >> Disable Workflow is not Displayed",true);
	}

}

//span[normalize-space()='View Workflow']

@FindBy(xpath="//span[normalize-space()='View Workflow']")
private WebElement ViewWorkflowtabISBEENDISPLAYEDofendofday ;	
	
public void ViewWorkflowtabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=ViewWorkflowtabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='View Workflow']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> View Workflow is not Displayed");
		Reporter.log("FAIL >> View Workflow is not Displayed",true);
	}

}

//span[normalize-space()='Disable Workflow']

@FindBy(xpath="//span[normalize-space()='Disable Workflow']")
private WebElement ClickonDisableWorkflowtabofendofday ;	

public void ClickonDisableWorkflowtabofendofday() throws InterruptedException {
	sleep(2);
	ClickonDisableWorkflowtabofendofday.click();
	Reporter.log("PASS >> Click on Disable Workflow", true);
	sleep(2);
}

//h2[normalize-space()='Warning: Survey will be removed from active use']

@FindBy(xpath="//h2[normalize-space()='Warning: Survey will be removed from active use']")
private WebElement WarningtextISBEENDISPLAYEDofendofday ;	
	
public void WarningtextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=WarningtextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h2[normalize-space()='Warning: Survey will be removed from active use']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Warning is not Displayed");
		Reporter.log("FAIL >> Warning is not Displayed",true);
	}

}

//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']

@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement MessagetextISBEENDISPLAYEDofendofday ;	
	
public void MessagetextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=MessagetextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Message is not Displayed");
		Reporter.log("FAIL >> Message is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelTabISBEENDISPLAYEDofendofday ;	
	
public void CancelTabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=CancelTabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cancel is not Displayed");
		Reporter.log("FAIL >> Cancel is not Displayed",true);
	}

}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickonCanceltabofendofday ;	

public void ClickonCanceltabofendofday() throws InterruptedException {
	sleep(2);
	ClickonCanceltabofendofday.click();
	Reporter.log("PASS >> Click on Cancel", true);
	sleep(2);
}

//span[normalize-space()='Disable Survey Variation']

@FindBy(xpath="//span[normalize-space()='Disable Survey Variation']")
private WebElement DisableSurveyVariationTabISBEENDISPLAYEDofendofday ;	
	
public void DisableSurveyVariationTabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=DisableSurveyVariationTabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Disable Survey Variation']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Disable Survey Variation is not Displayed");
		Reporter.log("FAIL >> Disable Survey Variation is not Displayed",true);
	}

}


@FindBy(xpath="//span[normalize-space()='View Workflow']")
private WebElement ClickonViewWorkflowtabofendofday ;	

public void ClickonViewWorkflowtabofendofday() throws InterruptedException {
	sleep(2);
	ClickonViewWorkflowtabofendofday.click();
	Reporter.log("PASS >> Click on View Workflow", true);
	sleep(2);
}

//h1[normalize-space()='Autogenerated']

@FindBy(xpath="//h1[normalize-space()='Autogenerated']")
private WebElement AutogeneratedtextISBEENDISPLAYEDofendofday ;	
	
public void AutogeneratedtextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AutogeneratedtextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[normalize-space()='Autogenerated']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Autogenerated is not Displayed");
		Reporter.log("FAIL >> Autogenerated is not Displayed",true);
	}

}

//div[@class='MuiAlert-message']

@FindBy(xpath="//div[@class='MuiAlert-message']")
private WebElement MuiAlertmessagetextISBEENDISPLAYEDofendofday ;	
	
public void MuiAlertmessagetextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=MuiAlertmessagetextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@class='MuiAlert-message']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> MuiAlert-message is not Displayed");
		Reporter.log("FAIL >> MuiAlert-message is not Displayed",true);
	}

}

//span[@class='indexstyles__NodeLabel-sc-1vjj53q-1 iyWhDF']

@FindBy(xpath="//span[@class='indexstyles__NodeLabel-sc-1vjj53q-1 iyWhDF']")
private WebElement NewPagetextISBEENDISPLAYEDofendofday ;	
	
public void NewPagetextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=NewPagetextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@class='indexstyles__NodeLabel-sc-1vjj53q-1 iyWhDF']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> New Page is not Displayed");
		Reporter.log("FAIL >> New Page is not Displayed",true);
	}

}

//span[@class='MuiButton-label']

@FindBy(xpath="//span[@class='MuiButton-label']")
private WebElement BackButtonISBEENDISPLAYEDofendofday ;	
	
public void BackButtonISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=BackButtonISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@class='MuiButton-label']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Back Button is not Displayed");
		Reporter.log("FAIL >> Back Button is not Displayed",true);
	}

}

@FindBy(xpath="//span[@class='MuiButton-label']")
private WebElement ClickonBackButtontabofendofday ;	

public void ClickonBackButtontabofendofday() throws InterruptedException {
	sleep(2);
	ClickonBackButtontabofendofday.click();
	Reporter.log("PASS >> Click on Back Button", true);
	sleep(2);
}

//div[@title='Draft Changeset']

@FindBy(xpath="//div[@title='Draft Changeset']")
private WebElement DraftChangesetISBEENDISPLAYEDofendofday ;	
	
public void DraftChangesetISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=DraftChangesetISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@title='Draft Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Draft Changeset is not Displayed");
		Reporter.log("FAIL >> Draft Changeset is not Displayed",true);
	}

}

//span[normalize-space()='Reset Draft']

@FindBy(xpath="//span[normalize-space()='Reset Draft']")
private WebElement ResetDraftISBEENDISPLAYEDofendofday ;	
	
public void ResetDraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=ResetDraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Reset Draft is not Displayed");
		Reporter.log("FAIL >> Reset Draft is not Displayed",true);
	}

}

//span[normalize-space()='Publish Draft']

@FindBy(xpath="//span[normalize-space()='Publish Draft']")
private WebElement PublishDraftISBEENDISPLAYEDofendofday ;	
	
public void PublishDraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=PublishDraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Publish Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Publish Draft is not Displayed");
		Reporter.log("FAIL >> Publish Draft is not Displayed",true);
	}

}

//span[normalize-space()='Edit Draft']

@FindBy(xpath="//span[normalize-space()='Edit Draft']")
private WebElement EditDraftISBEENDISPLAYEDofendofday ;	
	
public void EditDraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=EditDraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Edit Draft']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Edit Draft is not Displayed");
		Reporter.log("FAIL >> Edit Draft is not Displayed",true);
	}

}

//span[normalize-space()='Reset Draft']

@FindBy(xpath="//span[normalize-space()='Reset Draft']")
private WebElement ClickonResetDrafttabofendofday ;	

public void ClickonResetDrafttabofendofday() throws InterruptedException {
	sleep(2);
	ClickonResetDrafttabofendofday.click();
	Reporter.log("PASS >> Click on Reset Draft", true);
	sleep(2);
}

//h2[normalize-space()='Warning: All pending changes will be lost!']

@FindBy(xpath="//h2[normalize-space()='Warning: All pending changes will be lost!']")
private WebElement WarningResetDraftISBEENDISPLAYEDofendofday ;	
	
public void WarningResetDraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=WarningResetDraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h2[normalize-space()='Warning: All pending changes will be lost!']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Warning is not Displayed");
		Reporter.log("FAIL >> Warning is not Displayed",true);
	}

}

//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']

@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement MessagetextResetDraftISBEENDISPLAYEDofendofday ;	
	
public void MessagetextResetDraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=MessagetextResetDraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Message text is not Displayed");
		Reporter.log("FAIL >> Message Text is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CanceltabofresetdraftISBEENDISPLAYEDofendofday ;	
	
public void CanceltabofresetdraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=CanceltabofresetdraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel Tab is not Displayed");
		Reporter.log("FAIL >> Cancel Tab is not Displayed",true);
	}

}

//span[normalize-space()='Reset Changeset']

@FindBy(xpath="//span[normalize-space()='Reset Changeset']")
private WebElement ResetChangesettabofresetdraftISBEENDISPLAYEDofendofday ;	
	
public void ResetChangesettabofresetdraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=ResetChangesettabofresetdraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Reset Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Reset Changeset Tab is not Displayed");
		Reporter.log("FAIL >> Reset Changeset Tab is not Displayed",true);
	}

}

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement Clickoncanceltabofresetdraftendofday ;	

public void Clickoncanceltabofresetdraftendofday() throws InterruptedException {
	sleep(2);
	Clickoncanceltabofresetdraftendofday.click();
	Reporter.log("PASS >> Click on Cancel", true);
	sleep(2);
}


//span[normalize-space()='Publish Draft']

@FindBy(xpath="//span[normalize-space()='Publish Draft']")
private WebElement ClickonPublishedtabofPublishdraftendofday ;	

public void ClickonPublishedtabofPublishdraftendofday() throws InterruptedException {
	sleep(2);
	ClickonPublishedtabofPublishdraftendofday.click();
	Reporter.log("PASS >> Click on Publish Draft", true);
	sleep(2);
}

//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']

@FindBy(xpath="//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")
private WebElement MessagetextofpublishdraftISBEENDISPLAYEDofendofday ;	
	
public void MessagetextofpublishdraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=MessagetextofpublishdraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//p[@class='MuiTypography-root MuiDialogContentText-root MuiTypography-body1 MuiTypography-colorTextSecondary']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Message text is not Displayed");
		Reporter.log("FAIL >> Message Text is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelTabofpublishdraftISBEENDISPLAYEDofendofday ;	
	
public void CancelTabofpublishdraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=CancelTabofpublishdraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel is not Displayed");
		Reporter.log("FAIL >> Cancel Text is not Displayed",true);
	}

}

//span[normalize-space()='Publish Draft Changeset']

@FindBy(xpath="//span[normalize-space()='Publish Draft Changeset']")
private WebElement PublishDraftChangesetTabofpublishdraftISBEENDISPLAYEDofendofday ;	
	
public void PublishDraftChangesetTabofpublishdraftISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=PublishDraftChangesetTabofpublishdraftISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Publish Draft Changeset']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Publish Draft Changeset is not Displayed");
		Reporter.log("FAIL >> Publish Draft Changeset is not Displayed",true);
	}

}


@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement ClickonCanceltabofPublishdraftendofday ;	

public void ClickonCanceltabofPublishdraftendofday() throws InterruptedException {
	sleep(2);
	ClickonCanceltabofPublishdraftendofday.click();
	Reporter.log("PASS >> Click on Cancel", true);
	sleep(2);
}


//span[normalize-space()='Edit Draft']

@FindBy(xpath="//span[normalize-space()='Edit Draft']")
private WebElement ClickonEditDraftTabendofday ;	

public void ClickonEditDraftTabendofday() throws InterruptedException {
	sleep(2);
	ClickonEditDraftTabendofday.click();
	Reporter.log("PASS >> Click on Edit Draft", true);
	sleep(2);
}

//h1[normalize-space()='End of Day']

@FindBy(xpath="//h1[normalize-space()='End of Day']")
private WebElement EndofDaytextISBEENDISPLAYEDofendofday ;	
	
public void EndofDaytextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=EndofDaytextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[normalize-space()='End of Day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>End of Day is not Displayed");
		Reporter.log("FAIL >> End of Day is not Displayed",true);
	}

}

//(//*[text()='New Page'])[2]

@FindBy(xpath="(//*[text()='New Page'])[2]")
private WebElement NewPagetabISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void NewPagetabISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=NewPagetabISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("(//*[text()='New Page'])[2]")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page Tab is not Displayed");
		Reporter.log("FAIL >> New Page tab is not Displayed",true);
	}

}

@FindBy(xpath="(//*[text()='New Page'])[2]")
private WebElement ClickonNewPageTabendofdayeditdraft ;	

public void ClickonNewPageTabendofdayeditdraft() throws InterruptedException {
	sleep(2);
	ClickonNewPageTabendofdayeditdraft.click();
	Reporter.log("PASS >> Click on New Page", true);
	sleep(2);
}

//h3[normalize-space()='Page Element']

@FindBy(xpath="//h3[normalize-space()='Page Element']")
private WebElement NewPagetabPageElementISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void NewPagetabPageElementISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=NewPagetabPageElementISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Page Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Page Element is not Displayed");
		Reporter.log("FAIL >> Page Element is not Displayed",true);
	}

}

//input[@value='New Page']

@FindBy(xpath="//input[@value='New Page']")
private WebElement NewPagetitleISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void NewPagetitleISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=NewPagetitleISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='New Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page title is not Displayed");
		Reporter.log("FAIL >> New Page title is not Displayed",true);
	}

}

//textarea[@aria-label='Description']

@FindBy(xpath="//textarea[@aria-label='Description']")
private WebElement NewPageDescriptionISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void NewPageDescriptionISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=NewPageDescriptionISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Description']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page Description is not Displayed");
		Reporter.log("FAIL >> New Page Description is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement NewPageRemoveISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void NewPageRemoveISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=NewPageRemoveISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page Remove is not Displayed");
		Reporter.log("FAIL >> New Page Remove is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement NewPageCancelISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void NewPageCancelISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=NewPageCancelISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page Cancel is not Displayed");
		Reporter.log("FAIL >> New Page Cancel is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement NewPageSaveISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void NewPageSaveISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=NewPageSaveISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page Save is not Displayed");
		Reporter.log("FAIL >> New Page Save is not Displayed",true);
	}

}

//span[text()='Rate Your Day']

@FindBy(xpath="//span[text()='Rate Your Day']")
private WebElement RateYourDayISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void RateYourDayISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=RateYourDayISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Rate Your Day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Rate Your Day is not Displayed");
		Reporter.log("FAIL >> Rate Your Day is not Displayed",true);
	}

}

@FindBy(xpath="//span[text()='Rate Your Day']")
private WebElement ClickonRateYourDayendofdayeditdraft ;	

public void ClickonRateYourDayendofdayeditdraft() throws InterruptedException {
	sleep(2);
	ClickonRateYourDayendofdayeditdraft.click();
	Reporter.log("PASS >> Click on Rate Your Day", true);
	sleep(2);
}


//h3[normalize-space()='Section Element']

@FindBy(xpath="//h3[normalize-space()='Section Element']")
private WebElement SectionElementISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void SectionElementISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=SectionElementISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Section Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Section Element is not Displayed");
		Reporter.log("FAIL >> Section Element is not Displayed",true);
	}

}

//input[@value='Rate Your Day']

@FindBy(xpath="//input[@value='Rate Your Day']")
private WebElement RateYourDaytitlefieldISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void RateYourDaytitlefieldISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=RateYourDaytitlefieldISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='Rate Your Day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Rate Your Day title field is not Displayed");
		Reporter.log("FAIL >> Rate Your Day title field is not Displayed",true);
	}

}

//textarea[@aria-label='Description']

@FindBy(xpath="//textarea[@aria-label='Description']")
private WebElement RateYourDayDescriptionfieldISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void RateYourDayDescriptionfieldISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=RateYourDayDescriptionfieldISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Description']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Rate Your Day Description field is not Displayed");
		Reporter.log("FAIL >> Rate Your Day Description field is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement RateYourDayRemovetabISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void RateYourDayRemovetabISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=RateYourDayRemovetabISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Rate Your Day Remove tab is not Displayed");
		Reporter.log("FAIL >> Rate Your Day Remove tab is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement RateYourDayCanceltabISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void RateYourDayCanceltabISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=RateYourDayCanceltabISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Rate Your Day Cancel tab is not Displayed");
		Reporter.log("FAIL >> Rate Your Day Cancel tab is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement RateYourDaySavetabISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void RateYourDaySavetabISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=RateYourDaySavetabISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Rate Your Day Save tab is not Displayed");
		Reporter.log("FAIL >> Rate Your Day Save tab is not Displayed",true);
	}

}

//span[text()='Additional Feedback']

@FindBy(xpath="//span[text()='Additional Feedback']")
private WebElement AdditionalFeedbackISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void AdditionalFeedbackISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=AdditionalFeedbackISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Additional Feedback']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback is not Displayed");
		Reporter.log("FAIL >> Additional Feedback is not Displayed",true);
	}

}

@FindBy(xpath="//span[text()='Additional Feedback']")
private WebElement ClickonAdditionalFeedbackendofdayeditdraft ;	

public void ClickonAdditionalFeedbackendofdayeditdraft() throws InterruptedException {
	sleep(2);
	ClickonAdditionalFeedbackendofdayeditdraft.click();
	Reporter.log("PASS >> Click on Additional Feedback", true);
	sleep(2);
}

//h3[normalize-space()='Question Text']

@FindBy(xpath="//h3[normalize-space()='Question Text']")
private WebElement AdditionalFeedbackQuestionTextISBEENDISPLAYEDofendofdayeditdraft ;	
	
public void AdditionalFeedbackQuestionTextISBEENDISPLAYEDofendofdayeditdraft()throws InterruptedException{

	boolean flag=AdditionalFeedbackQuestionTextISBEENDISPLAYEDofendofdayeditdraft.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Question Text']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Question Text is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Question Text is not Displayed",true);
	}

}

//input[@value='Please give us any additional feedback for your day.']

@FindBy(xpath="//input[@value='Please give us any additional feedback for your day.']")
private WebElement AdditionalFeedbackQuestionTextfieldISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackQuestionTextfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackQuestionTextfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='Please give us any additional feedback for your day.']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Question Text field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Question Text field is not Displayed",true);
	}

}

//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement AdditionalFeedbackQuestionNamefieldISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackQuestionNamefieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackQuestionNamefieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Question Name']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Question Name field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Question Name field is not Displayed",true);
	}

}

//input[@aria-label='Validation Expression']

@FindBy(xpath="//input[@aria-label='Validation Expression']")
private WebElement AdditionalFeedbackValidationExpressionfieldISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackValidationExpressionfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackValidationExpressionfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@aria-label='Validation Expression']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Validation Expression field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Validation Expression field is not Displayed",true);
	}

}

//span[normalize-space()='Multiline Textbox ?']

@FindBy(xpath="//span[normalize-space()='Multiline Textbox ?']")
private WebElement AdditionalFeedbackMultilineTextboxcheckboxISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackMultilineTextboxcheckboxISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackMultilineTextboxcheckboxISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Multiline Textbox ?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Multiline Textbox checkbox field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Multiline Textbox checkbox field is not Displayed",true);
	}

}

//span[normalize-space()='Optional ?']

@FindBy(xpath="//span[normalize-space()='Optional ?']")
private WebElement AdditionalFeedbackOptionalcheckboxISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackOptionalcheckboxISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackOptionalcheckboxISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Optional ?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Optional checkbox field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Optional checkbox field is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement AdditionalFeedbackRemovetabISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackRemovetabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackRemovetabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Remove Tab field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Remove Tab field is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement AdditionalFeedbackCanceltabISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackCanceltabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackCanceltabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Cancel Tab field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Cancel Tab field is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement AdditionalFeedbackSavetabISBEENDISPLAYEDofendofday ;	
	
public void AdditionalFeedbackSavetabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=AdditionalFeedbackSavetabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Additional Feedback Save Tab field is not Displayed");
		Reporter.log("FAIL >> Additional Feedback Save Tab field is not Displayed",true);
	}

}

//span[normalize-space()='TEST ANDY']
@FindBy(xpath="//span[normalize-space()='TEST ANDY']")
private WebElement TESTANDYtabISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYtabISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYtabISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='TEST ANDY']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY Tab field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY Tab field is not Displayed",true);
	}

}

@FindBy(xpath="//span[normalize-space()='TEST ANDY']")
private WebElement ClickonTESTANDYendofdayeditdraft ;	

public void ClickonTESTANDYendofdayeditdraft() throws InterruptedException {
	sleep(2);
	ClickonTESTANDYendofdayeditdraft.click();
	Reporter.log("PASS >> Click on TEST ANDY", true);
	sleep(2);
}

//h3[normalize-space()='Signature Question']

@FindBy(xpath="//h3[normalize-space()='Signature Question']")
private WebElement TESTANDYSignatureQuestiontextISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYSignatureQuestiontextISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYSignatureQuestiontextISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Signature Question']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY Signature Question Text field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY Signature Question Text field is not Displayed",true);
	}

}

//input[@value='TEST ANDY']

@FindBy(xpath="//input[@value='TEST ANDY']")
private WebElement TESTANDYQuestiontextfieldISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYQuestiontextfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYQuestiontextfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='TEST ANDY']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY  Question Text field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY  Question Text field is not Displayed",true);
	}

}

//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement TESTANDYQuestionNamefieldISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYQuestionNamefieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYQuestionNamefieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//textarea[@aria-label='Question Name']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY  Question Name field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY  Question Name field is not Displayed",true);
	}

}

//span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1']

@FindBy(xpath="//span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1']")
private WebElement TESTANDYOptionalcheckboxfieldISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYOptionalcheckboxfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYOptionalcheckboxfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY  Optional checkbox field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY  Optional checkbox field is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement TESTANDYRemovetabfieldISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYRemovetabfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYRemovetabfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY  Remove tab field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY  Remove tab field is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement TESTANDYCanceltabfieldISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYCanceltabfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYCanceltabfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY  Cancel tab field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY  Cancel tab field is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement TESTANDYSavetabfieldISBEENDISPLAYEDofendofday ;	
	
public void TESTANDYSavetabfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=TESTANDYSavetabfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY  Save tab field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY  Save tab field is not Displayed",true);
	}

}

//span[text()='Complete']

@FindBy(xpath="//span[text()='Complete']")
private WebElement CompletetabfieldISBEENDISPLAYEDofendofday ;	
	
public void CompletetabfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=CompletetabfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Complete']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>TEST ANDY  Complete tab field is not Displayed");
		Reporter.log("FAIL >> TEST ANDY  Complete tab field is not Displayed",true);
	}

}

@FindBy(xpath="//span[text()='Complete']")
private WebElement ClickonCompleteendofdayeditdraft ;	

public void ClickonCompleteendofdayeditdraft() throws InterruptedException {
	sleep(2);
	ClickonCompleteendofdayeditdraft.click();
	Reporter.log("PASS >> Click on Complete", true);
	sleep(2);
}

//h3[normalize-space()='Button Element']

@FindBy(xpath="//h3[normalize-space()='Button Element']")
private WebElement ButtonElementtextfieldISBEENDISPLAYEDofendofday ;	
	
public void ButtonElementtextfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=ButtonElementtextfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h3[normalize-space()='Button Element']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Button Element text field is not Displayed");
		Reporter.log("FAIL >> Button Element text field is not Displayed",true);
	}

}

//input[@value='Complete']

@FindBy(xpath="//input[@value='Complete']")
private WebElement ButtontextfieldISBEENDISPLAYEDofendofday ;	
	
public void ButtontextfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=ButtontextfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//input[@value='Complete']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Button  text field is not Displayed");
		Reporter.log("FAIL >> Button  text field is not Displayed",true);
	}

}

//span[normalize-space()='Navigation Enabled?']

@FindBy(xpath="//span[normalize-space()='Navigation Enabled?']")
private WebElement NavigationEnabledtextcheckboxfieldISBEENDISPLAYEDofendofday ;	
	
public void NavigationEnabledtextcheckboxfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=NavigationEnabledtextcheckboxfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Navigation Enabled?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Navigation Enabled text checkbox field is not Displayed");
		Reporter.log("FAIL >> Navigation Enabled text checkbox field is not Displayed",true);
	}

}

//div[@id='select-nav-type']

@FindBy(xpath="//div[@id='select-nav-type']")
private WebElement NavigationtypecheckboxfieldISBEENDISPLAYEDofendofday ;	
	
public void NavigationtypecheckboxfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=NavigationtypecheckboxfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[@id='select-nav-type']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Navigation type text checkbox field is not Displayed");
		Reporter.log("FAIL >> Navigation type text checkbox field is not Displayed",true);
	}

}

//div[normalize-space()='Home page']

@FindBy(xpath="//div[normalize-space()='Home page']")
private WebElement SelectScreentypecheckboxfieldISBEENDISPLAYEDofendofday ;	
	
public void SelectScreentypecheckboxfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=SelectScreentypecheckboxfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//div[normalize-space()='Home page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Select Screen Type text checkbox field is not Displayed");
		Reporter.log("FAIL >> Select Screen Type text checkbox field is not Displayed",true);
	}

}

//span[normalize-space()='Actions Enabled?']

@FindBy(xpath="//span[normalize-space()='Actions Enabled?']")
private WebElement ActionsEnabledtypecheckboxfieldISBEENDISPLAYEDofendofday ;	
	
public void ActionsEnabledtypecheckboxfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=ActionsEnabledtypecheckboxfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Actions Enabled?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Actions Enabled Type text checkbox field is not Displayed");
		Reporter.log("FAIL >> Actions Enabled Type text checkbox field is not Displayed",true);
	}

}

//span[normalize-space()='Optional?']

@FindBy(xpath="//span[normalize-space()='Optional?']")
private WebElement OptionaltypecheckboxfieldISBEENDISPLAYEDofendofday ;	
	
public void OptionaltypecheckboxfieldISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=OptionaltypecheckboxfieldISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Optional?']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Optional Type text checkbox field is not Displayed");
		Reporter.log("FAIL >> Optional Type text checkbox field is not Displayed",true);
	}

}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement RemovefieldofcompleteISBEENDISPLAYEDofendofday ;	
	
public void RemovefieldofcompleteISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=RemovefieldofcompleteISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Remove']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Remove tab field of complete is not Displayed");
		Reporter.log("FAIL >> Remove tab field of complete is not Displayed",true);
	}

}

//span[normalize-space()='Cancel']

@FindBy(xpath="//span[normalize-space()='Cancel']")
private WebElement CancelfieldofcompleteISBEENDISPLAYEDofendofday ;	
	
public void CancelfieldofcompleteISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=CancelfieldofcompleteISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Cancel tab field of complete is not Displayed");
		Reporter.log("FAIL >> Cancel tab field of complete is not Displayed",true);
	}

}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement SavefieldofcompleteISBEENDISPLAYEDofendofday ;	
	
public void SavefieldofcompleteISBEENDISPLAYEDofendofday()throws InterruptedException{

	boolean flag=SavefieldofcompleteISBEENDISPLAYEDofendofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Save']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Save tab field of complete is not Displayed");
		Reporter.log("FAIL >> Save tab field of complete is not Displayed",true);
	}

}

//span[@class='MuiButton-label']

@FindBy(xpath="//span[@class='MuiButton-label']")
private WebElement BACKTOWORKFLOWSETUPtextisdisplayedofviewworkflowstartofday ;	
	
public void BACKTOWORKFLOWSETUPtextisdisplayedofviewworkflowstartofday()throws InterruptedException{

	boolean flag=BACKTOWORKFLOWSETUPtextisdisplayedofviewworkflowstartofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[@class='MuiButton-label']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>BACK TO WORKFLOW SETUP text is not Displayed");
		Reporter.log("FAIL >> BACK TO WORKFLOW SETUP text is not Displayed",true);
	}

}


@FindBy(xpath="//span[@class='MuiButton-label']")
private WebElement ClickonBACKTOWORKFLOWSETUPofviewworkflow ;	

public void ClickonBACKTOWORKFLOWSETUPofviewworkflow() throws InterruptedException {
	sleep(2);
	ClickonBACKTOWORKFLOWSETUPofviewworkflow.click();
	Reporter.log("PASS >> Click on BACK TO WORKFLOW SETUP", true);
	sleep(2);
}

//h1[normalize-space()='Start of Day']

@FindBy(xpath="//h1[normalize-space()='Start your day']")
private WebElement STARTOFDAYtextisdisplayedofEDITDRAFTstartofday ;	
	
public void STARTOFDAYtextisdisplayedofEDITDRAFTstartofday()throws InterruptedException{

	boolean flag=STARTOFDAYtextisdisplayedofEDITDRAFTstartofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//h1[normalize-space()='Start your day']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>START your DAY text is not Displayed");
		Reporter.log("FAIL >> START your DAY text is not Displayed",true);
	}

}

//span[normalize-space()='Back to Workflow Setup']

@FindBy(xpath="//span[normalize-space()='Back to Workflow Setup']")
private WebElement BacktoWorkflowSetuptextisdisplayedofEDITDRAFTstartofday ;	
	
public void BacktoWorkflowSetuptextisdisplayedofEDITDRAFTstartofday()throws InterruptedException{

	boolean flag=BacktoWorkflowSetuptextisdisplayedofEDITDRAFTstartofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Back to Workflow Setup']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Back to Workflow Setup text is not Displayed");
		Reporter.log("FAIL >> Back to Workflow Setup text is not Displayed",true);
	}

}

//span[text()='Vehicle']

@FindBy(xpath="//span[text()='Vehicle']")
private WebElement VECHICLEtextisdisplayedofEDITDRAFTstartofday ;	
	
public void VECHICLEtextisdisplayedofEDITDRAFTstartofday()throws InterruptedException{

	boolean flag=VECHICLEtextisdisplayedofEDITDRAFTstartofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Vehicle']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>VEHICLE text is not Displayed");
		Reporter.log("FAIL >>VEHICLE text is not Displayed",true);
	}

}

//span[text()='Tool Check']

@FindBy(xpath="//span[text()='Tool Check']")
private WebElement TOOLCHECKtextisdisplayedofEDITDRAFTstartofday ;	
	
public void TOOLCHECKtextisdisplayedofEDITDRAFTstartofday()throws InterruptedException{

	boolean flag=TOOLCHECKtextisdisplayedofEDITDRAFTstartofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Tool Check']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Tool Check text is not Displayed");
		Reporter.log("FAIL >>Tool Check text is not Displayed",true);
	}

}

//span[text()='Stock Check']

@FindBy(xpath="//span[text()='Stock Check']")
private WebElement STOCKCHECKtextisdisplayedofEDITDRAFTstartofday ;	
	
public void STOCKCHECKtextisdisplayedofEDITDRAFTstartofday()throws InterruptedException{

	boolean flag=STOCKCHECKtextisdisplayedofEDITDRAFTstartofday.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[text()='Stock Check']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>Stock Check text is not Displayed");
		Reporter.log("FAIL >>Stock Check text is not Displayed",true);
	}

}

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERNEWPAGETITLEOfEDITDRAFTstartofday ;	

public void enterNEWPAGE(String val) throws InterruptedException {
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(val);
	
	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}
public void abcd() throws InterruptedException {
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.CLEAR);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys("A");
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickonSAVEBUTTONOFEDITDRAFTOFSTARTOFDAY ;	

public void ClickonSAVEBUTTONOFEDITDRAFTOFSTARTOFDAY() throws InterruptedException {
	sleep(2);
	ClickonSAVEBUTTONOFEDITDRAFTOFSTARTOFDAY.click();
	Reporter.log("PASS >> Click on Save Button", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);
	sleep(2);
}

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement PressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY ;	

public void PressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY() throws InterruptedException {
	sleep(2);
	PressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY.click();
	Reporter.log("PASS >> Press on Save Button", true);
	Reporter.log("PASS >> Title Has Been Saved Successfully", true);
	sleep(2);
}

@FindBy(xpath="//textarea[@aria-label='Description']")
private WebElement ENTERdescriptionOfEDITDRAFTstartofday ;	

public void enterDescription(String val) throws InterruptedException {
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERdescriptionOfEDITDRAFTstartofday.sendKeys(val);
	
	Reporter.log("PASS >> Description entered successfully  ",true);
}


public void abcde() throws InterruptedException {
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.click();
	
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	ENTERNEWPAGETITLEOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement enteronSAVEBUTTONofEDITDRAFTOFSTARTOFDAY ;	

public void enteronSAVEBUTTONofEDITDRAFTOFSTARTOFDAY() throws InterruptedException {
	sleep(2);
	enteronSAVEBUTTONofEDITDRAFTOFSTARTOFDAY.click();
	Reporter.log("PASS >> Press on Save Button", true);
	Reporter.log("PASS >> Description  Has not Been Saved Successfully", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);
	sleep(2);
}


@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickorpressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY ;	

public void ClickorpressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY() throws InterruptedException {
	sleep(2);
	ClickorpressonSAVEBUTTONofEDITDRAFTOFSTARTOFDAY.click();
	Reporter.log("PASS >> Press on Save Button", true);
	Reporter.log("PASS >>Title and description is been saved successfully", true);

}

//(//span[@class='MuiButton-label'])[5]
@FindBy(xpath="(//span[@class='MuiButton-label'])[5]")
private WebElement ClickorpressonRemoveBUTTONofEDITDRAFTOFSTARTOFDAY ;	

public void ClickorpressonRemoveBUTTONofEDITDRAFTOFSTARTOFDAY() throws InterruptedException {
	sleep(2);
	ClickorpressonRemoveBUTTONofEDITDRAFTOFSTARTOFDAY.click();
	Reporter.log("PASS >> Click on Remove Button", true);
			

}
		
@FindBy(xpath="//span[normalize-space()='New Section']")
private WebElement NEWSECTIONTABisdisplayedofEDITDRAFTstartofday ;	
			
public void NEWSECTIONTABisdisplayedofEDITDRAFTstartofday()throws InterruptedException{

	boolean flag=NEWSECTIONTABisdisplayedofEDITDRAFTstartofday.isDisplayed();
		if(flag)
		{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='New Section']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
	    sleep(2); 		
		}
		else
		{		
		ALib.AssertFailMethod("FAIL >>New Section TAB is not Displayed");
		Reporter.log("FAIL >>New Section TAB is not Displayed",true);
		}

}

@FindBy(xpath="//span[normalize-space()='New Question']")
private WebElement NEWQUESTIONTABisdisplayedofEDITDRAFTstartofday ;	
			
public void NEWQUESTIONTABisdisplayedofEDITDRAFTstartofday()throws InterruptedException{

	boolean flag=NEWQUESTIONTABisdisplayedofEDITDRAFTstartofday.isDisplayed();
		if(flag)
		{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='New Question']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
	    sleep(2); 		
		}
		else
		{		
		ALib.AssertFailMethod("FAIL >>New Question TAB is not Displayed");
		Reporter.log("FAIL >>New Question TAB is not Displayed",true);
		}

}

//span[normalize-space()='New Section']
@FindBy(xpath="//span[normalize-space()='New Section']")
private WebElement ClickorpressonNEWSECTIONTABofEDITDRAFTOFSTARTOFDAY ;	

public void ClickorpressonNEWSECTIONTABofEDITDRAFTOFSTARTOFDAY() throws InterruptedException {
	sleep(2);
	ClickorpressonNEWSECTIONTABofEDITDRAFTOFSTARTOFDAY.click();
	Reporter.log("PASS >> Click on NEW SECTION TAB", true);
}

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement titleofnewsectionOfEDITDRAFTstartofday ;	

public void enterNewsectiontitle(String val) throws InterruptedException {
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(val);
	
	Reporter.log("PASS >> Title Entered successfully  ",true);
}


public void Cleartitlenewsection() throws InterruptedException {
	titleofnewsectionOfEDITDRAFTstartofday.click();
	
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	titleofnewsectionOfEDITDRAFTstartofday.sendKeys(Keys.BACK_SPACE);
	
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}



//span[normalize-space()='Save']
@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickorpressonSaveofNEWSECTIONOFSTARTOFDAY ;	

public void ClickorpressonSaveofNEWSECTIONOFSTARTOFDAY() throws InterruptedException {
	sleep(2);
	ClickorpressonSaveofNEWSECTIONOFSTARTOFDAY.click();
	Reporter.log("PASS >> Click on SAVE TAB", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);
}

@FindBy(xpath="//textarea[@aria-label='Description']")
private WebElement ENTERdescriptionOfNEWSECTIONstartofday ;	

public void enterDescriptionNEWSECTION(String val) throws InterruptedException {
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERdescriptionOfNEWSECTIONstartofday.sendKeys(val);
	
	Reporter.log("PASS >> Description entered successfully  ",true);
}


//span[normalize-space()='Save']
@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickorpressonSaveofNEWSECTION ;	

public void ClickorpressonSaveofNEWSECTION() throws InterruptedException {
	sleep(2);
	ClickorpressonSaveofNEWSECTION.click();
	Reporter.log("PASS >> Click on SAVE TAB", true);
	Reporter.log("PASS >> TITLE & DESCRIPTION is been saved successfully ", true);
}

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement ClickorpressonREMOVEofNEWSECTION ;	

public void ClickorpressonREMOVEofNEWSECTION() throws InterruptedException {
	sleep(2);
	ClickorpressonREMOVEofNEWSECTION.click();
	Reporter.log("PASS >> Click on REMOVE TAB", true);
	Reporter.log("PASS >> NEW SECTION is been saved successfully REMOVED ", true);
}


//span[text()='TEST NEW PAGE']

@FindBy(xpath="//span[text()='TEST NEW PAGE']")
private WebElement Clickorpressontestnewpageofnewpage ;	

public void Clickorpressontestnewpageofnewpage() throws InterruptedException {
	sleep(2);
	Clickorpressontestnewpageofnewpage.click();
	Reporter.log("PASS >> Click on test new page ", true);
	
}

//span[text()='New Question']

@FindBy(xpath="//span[text()='New Question']")
private WebElement ClickorpressonNewQuestionoftestnewpage ;	

public void ClickorpressonNewQuestionoftestnewpage() throws InterruptedException {
	sleep(2);
	ClickorpressonNewQuestionoftestnewpage.click();
	Reporter.log("PASS >> Click on New Question ", true);
	
}

//span[text()='Text']

@FindBy(xpath="//span[text()='Text']")
private WebElement Clickorpressontextofnewquestion ;	

public void Clickorpressontextofnewquestion() throws InterruptedException {
	sleep(2);
	Clickorpressontextofnewquestion.click();
	Reporter.log("PASS >> Click on Text ", true);
	
}

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement titleofNewQuestionOftext ;	

public void Cleartitleofnewquestion() throws InterruptedException {
	titleofNewQuestionOftext.click();
	
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	titleofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}


//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickorpressonSaveofnewquestion ;	

public void ClickorpressonSaveofnewquestion() throws InterruptedException {
	sleep(2);
	ClickorpressonSaveofnewquestion.click();
	Reporter.log("PASS >> Click on Save ", true);
	Reporter.log("PASS >> Error occured due to field is not fill ", true);
}


@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERNEWQuestionTITLEOfEDITDRAFTstartofday ;	

public void enterNEWQUESTION(String val) throws InterruptedException {
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERNEWQuestionTITLEOfEDITDRAFTstartofday.sendKeys(val);
	
	Reporter.log("PASS >> TEST NEW QUESTION entered successfully  ",true);
}


@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickorpressonSaveBUTTONQUESTION ;	

public void ClickorpressonSaveBUTTONQUESTION() throws InterruptedException {
	sleep(2);
	ClickorpressonSaveBUTTONQUESTION.click();
	Reporter.log("PASS >> Click on Save ", true);
	sleep(2);
}


@FindBy(xpath="//textarea[@class='MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputMultiline MuiOutlinedInput-inputMultiline']")
private WebElement ENTERQuestionNAMEOfEDITDRAFTstartofday ;	

public void enterQUESTIONNAME(String val) throws InterruptedException {
	ENTERQuestionNAMEOfEDITDRAFTstartofday.click();
	sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERQuestionNAMEOfEDITDRAFTstartofday.sendKeys(val);
	
	Reporter.log("PASS >>  QUESTION NAME entered successfully  ",true);
}

//input[@aria-label='Validation Expression']

@FindBy(xpath="//input[@aria-label='Validation Expression']")
private WebElement ENTERValidationExpressionOfEDITDRAFTstartofday ;	

public void enterValidationExpression(String val) throws InterruptedException {
	ENTERValidationExpressionOfEDITDRAFTstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERValidationExpressionOfEDITDRAFTstartofday.sendKeys(val);
	
	Reporter.log("PASS >>  Validation Expression entered successfully  ",true);
}


//span[normalize-space()='Multiline Textbox ?']
@FindBy(xpath="//span[normalize-space()='Multiline Textbox ?']")
private WebElement ClickorpressonMultilineTextbox ;	

public void ClickorpressonMultilineTextbox() throws InterruptedException {
	sleep(2);
	ClickorpressonMultilineTextbox.click();
	
	Reporter.log("PASS >> Multiline Textbox is been Clicked Successfully ", true);
	sleep(2);
}

@FindBy(xpath="//span[normalize-space()='Optional ?']")
private WebElement ClickorpressonOptionalTextbox ;	

public void ClickorpressonOptionalTextbox() throws InterruptedException {
	sleep(2);
	ClickorpressonOptionalTextbox.click();
	
	Reporter.log("PASS >> Optional Textbox is been Clicked Successfully ", true);
	sleep(2);
}

//span[text()='Remove']
@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement ClickorpressonRemovetabofquestionname ;	

public void ClickorpressonRemovetabofquestionname() throws InterruptedException {
	sleep(2);
	ClickorpressonRemovetabofquestionname.click();
	
	Reporter.log("PASS >> Click on Remove", true);
	sleep(2);
}


@FindBy(xpath="//span[text()='TEST NEW PAGE']")
private WebElement ClickTESTNEWPAGEAGAIN ;	

public void ClickTESTNEWPAGEAGAIN() throws InterruptedException {
	sleep(2);
	ClickTESTNEWPAGEAGAIN.click();
	
	Reporter.log("PASS >> Click on TEST NEW PAGE", true);
	sleep(2);
}

//span[text()='Discard Changes']

@FindBy(xpath="//span[text()='Discard Changes']")
private WebElement ClickDiscardChanges ;	

public void ClickDiscardChanges() throws InterruptedException {
	sleep(2);
	ClickDiscardChanges.click();
	
	Reporter.log("PASS >> Click on Discard Changes", true);
	sleep(2);
}

//span[text()='Number']
@FindBy(xpath="//span[text()='Number']")
private WebElement ClickonNumber ;	

public void ClickonNumber() throws InterruptedException {
	sleep(2);
	ClickonNumber.click();
	
	Reporter.log("PASS >> Click on Number", true);
	sleep(2);
}


//(//input[@class='MuiInputBase-input MuiOutlinedInput-input'])[1]

@FindBy(xpath="(//input[@class='MuiInputBase-input MuiOutlinedInput-input'])[1]")
private WebElement NumberofNewQuestionOftext ;	

public void ClearNumberofnewquestion() throws InterruptedException {
	NumberofNewQuestionOftext.click();
	
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	NumberofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickonsaveofNumber ;	

public void ClickonsaveofNumber() throws InterruptedException {
	sleep(2);
	ClickonsaveofNumber.click();
	
	Reporter.log("PASS >> Click on Save", true);
	Reporter.log("PASS >> Error Message POP UP Please fill out this field", true);
	sleep(2);
}


@FindBy(xpath="(//input[@class='MuiInputBase-input MuiOutlinedInput-input'])[1]")
private WebElement ENTERquestiontextofnumberstartofday ;	

public void enterQUESTIONTEXTOFNUMBER(String val) throws InterruptedException {
	ENTERquestiontextofnumberstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERquestiontextofnumberstartofday.sendKeys(val);
	
	Reporter.log("PASS >>  question text entered successfully  ",true);
}

//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement ENTERquestionNAMEofnumberstartofday ;	

public void enterQUESTIONNAMEOFNUMBER(String val) throws InterruptedException {
	ENTERquestionNAMEofnumberstartofday.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERquestionNAMEofnumberstartofday.sendKeys(val);
	
	Reporter.log("PASS >>  QUESTION NAME entered successfully  ",true);
}

//span[normalize-space()='Save']
//span[normalize-space()='Decimal']
@FindBy(xpath="//span[normalize-space()='Decimal']")
private WebElement ClickonDECIMALofNumber ;	

public void ClickonDECIMALofNumber() throws InterruptedException {
	sleep(2);
	ClickonDECIMALofNumber.click();
	
	Reporter.log("PASS >> Click on DECIMAL", true);
	sleep(2);
}

@FindBy(xpath="//input[@aria-label='Min value']")
private WebElement MinBtn ;

public void EnterMinDigit()throws InterruptedException{
	
	  
	MinBtn.sendKeys("9");
    sleep(2);
    Reporter.log("PASS >> Digit Entered Successfully",true);
      
}

//input[@aria-label='Max value']

@FindBy(xpath="//input[@aria-label='Max value']")
private WebElement MaxBtn ;

public void EnterMaxDigit()throws InterruptedException{
	
	  
	MaxBtn.sendKeys("23");
    sleep(2);
    Reporter.log("PASS >> Digit Entered Successfully",true);
      
}


@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement ClickSAVEBUTTONOFNUMBER ;	

public void ClickSAVEBUTTONOFNUMBER() throws InterruptedException {
	sleep(2);
	ClickSAVEBUTTONOFNUMBER.click();
	
	Reporter.log("PASS >> Click on Save", true);
	
	sleep(2);
}

@FindBy(xpath="//span[normalize-space()='Integer']")
private WebElement ClickonIntegerofNumber ;	

public void ClickonIntegerofNumber() throws InterruptedException {
	sleep(2);
	ClickonIntegerofNumber.click();
	
	Reporter.log("PASS >> Click on Integer", true);
	sleep(2);
}

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement RemovetabclickonNumber ;	

public void RemovetabclickonNumber() throws InterruptedException {
	sleep(2);
	RemovetabclickonNumber.click();
	
	Reporter.log("PASS >> Click on Remove", true);
	sleep(2);
	}

//span[text()='TEST NEW PAGE']

@FindBy(xpath="//span[text()='TEST NEW PAGE']")
private WebElement TESTNEWPAGEISclick ;	

public void TESTNEWPAGEISclick() throws InterruptedException {
	sleep(2);
	TESTNEWPAGEISclick.click();
	
	Reporter.log("PASS >> Click on TEST NEW PAGE", true);
	sleep(2);
	}

//span[normalize-space()='New Question']

@FindBy(xpath="//span[normalize-space()='New Question']")
private WebElement NEWQUESTIONISclickED ;	

public void NEWQUESTIONISclickED() throws InterruptedException {
	sleep(2);
	NEWQUESTIONISclickED.click();
	
	Reporter.log("PASS >> Click on New Question", true);
	sleep(2);
	}

//span[normalize-space()='Select']

@FindBy(xpath="//span[normalize-space()='Select']")
private WebElement SELECTISclickED ;	

public void SELECTISclickED() throws InterruptedException {
	sleep(2);
	SELECTISclickED.click();
	
	Reporter.log("PASS >> Click on Select", true);
	sleep(2);
	}

//input[@value='New Question']

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement SELECTofNewQuestionOftext ;	

public void ClearSELECTofnewquestion() throws InterruptedException {
	SELECTofNewQuestionOftext.click();
	
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}


//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKONSAVEOFSELECT ;	

public void CLICKONSAVEOFSELECT() throws InterruptedException {
	sleep(2);
	CLICKONSAVEOFSELECT.click();
	
	Reporter.log("PASS >> Click on SAVE", true);
	Reporter.log("PASS >> ERROR MESSAGE Please fill out this field", true);
	sleep(2);
	}



@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERquestiontextofSELECT ;	

public void enterQUESTIONTEXTOFSELECT(String val) throws InterruptedException {
	ENTERquestiontextofSELECT.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERquestiontextofSELECT.sendKeys(val);
	
	Reporter.log("PASS >>  QUESTION TEXT entered successfully  ",true);
}

//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement ENTERquestionNAMEofSELECT ;	

public void enterQUESTIONNAMEOFSELECT(String val) throws InterruptedException {
	ENTERquestionNAMEofSELECT.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERquestionNAMEofSELECT.sendKeys(val);
	
	Reporter.log("PASS >>  QUESTION NAME entered successfully  ",true);
}

//span[normalize-space()='Radio']

@FindBy(xpath="//span[normalize-space()='Radio']")
private WebElement CLICKONRADIOCHECKBOXOFSELECT ;	

public void CLICKONRADIOCHECKBOXOFSELECT() throws InterruptedException {
	sleep(2);
	CLICKONRADIOCHECKBOXOFSELECT.click();
	
	Reporter.log("PASS >> Click on RADIO BUTTON", true);
	
	sleep(2);
	}

//span[normalize-space()='Dropdown']

@FindBy(xpath="//span[normalize-space()='Dropdown']")
private WebElement CLICKONDROPDOWNCHECKBOXOFSELECT ;	

public void CLICKONDROPDOWNCHECKBOXOFSELECT() throws InterruptedException {
	sleep(2);
	CLICKONDROPDOWNCHECKBOXOFSELECT.click();
	
	Reporter.log("PASS >> Click on Dropdown BUTTON", true);
	
	sleep(2);
	}

//(//input[@class='MuiInputBase-input MuiInput-input'])[1]
		
		@FindBy(xpath="(//input[@class='MuiInputBase-input MuiInput-input'])[1]")
		private WebElement ENTERSETTHETEXTPLEASEofSELECT ;	

		public void enterSETTEHTEXTPLEASEOFSELECT(String val) throws InterruptedException {
			ENTERSETTHETEXTPLEASEofSELECT.click();
			//sleep(2);
			//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
			//sleep(2);
			ENTERSETTHETEXTPLEASEofSELECT.sendKeys(val);
			
			Reporter.log("PASS >>  SET THE TEXT entered successfully  ",true);
		}		
		

//(//input[@class='MuiInputBase-input MuiInput-input'])[2]
		
		
		@FindBy(xpath="(//input[@class='MuiInputBase-input MuiInput-input'])[2]")
		private WebElement ENTERSETTHETEXTPLEASE2ofSELECT ;	

		public void enterSETTEHTEXTPLEASE2OFSELECT(String val) throws InterruptedException {
			ENTERSETTHETEXTPLEASE2ofSELECT.click();
			//sleep(2);
			//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
			//sleep(2);
			ENTERSETTHETEXTPLEASE2ofSELECT.sendKeys(val);
			
			Reporter.log("PASS >>  SET THE TEXT 2 entered successfully  ",true);
		}				
		
		
//span[normalize-space()='Add Answer Option']
		
		@FindBy(xpath="//span[normalize-space()='Add Answer Option']")
		private WebElement CLICKONADDANSWEROPTIONOFSELECT ;	

		public void CLICKONADDANSWEROPTIONOFSELECT() throws InterruptedException {
			sleep(2);
			CLICKONADDANSWEROPTIONOFSELECT.click();
			
			Reporter.log("PASS >> Click on ADD ANSWER OPTION", true);
			
			sleep(2);
			}	
		
//(//input[@class='MuiInputBase-input MuiInput-input'])[3]	

		@FindBy(xpath="(//input[@class='MuiInputBase-input MuiInput-input'])[3]")
		private WebElement ENTERSETTHETEXTPLEASE3ofSELECT ;	

		public void enterSETTEHTEXTPLEASE3OFSELECT(String val) throws InterruptedException {
			ENTERSETTHETEXTPLEASE3ofSELECT.click();
			//sleep(2);
			//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
			//sleep(2);
			ENTERSETTHETEXTPLEASE3ofSELECT.sendKeys(val);
			
			Reporter.log("PASS >>  SET THE TEXT 3 entered successfully  ",true);
		}			
		
		
//span[normalize-space()='Optional ?']		
		
		@FindBy(xpath="//span[normalize-space()='Optional ?']")
		private WebElement CLICKONoptionalOFSELECT ;	

		public void CLICKONoptionalOFSELECT() throws InterruptedException {
			sleep(2);
			CLICKONoptionalOFSELECT.click();
			
			Reporter.log("PASS >> Click on optional", true);
			
			sleep(2);
			}			
		
	
//span[normalize-space()='Save']	
		
@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKONsavebuttonOFSELECT ;	

public void CLICKONsavebuttonOFSELECT() throws InterruptedException {
	sleep(2);
	CLICKONsavebuttonOFSELECT.click();
			
	Reporter.log("PASS >> Click on Save button", true);
			
	sleep(2);
}			
		
		
//span[normalize-space()='Remove']		
@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement CLICKONremovebuttonOFSELECT ;	

public void CLICKONremovebuttonOFSELECT() throws InterruptedException {
	sleep(2);
	CLICKONremovebuttonOFSELECT.click();
			
	Reporter.log("PASS >> Click on remove button", true);
			
	sleep(2);
}				
		
//span[normalize-space()='Multi-Select']		
		
@FindBy(xpath="//span[normalize-space()='Multi-Select']")
private WebElement CLICKONMultiSelect ;	

public void CLICKONMultiSelect() throws InterruptedException {
	sleep(2);
	CLICKONMultiSelect.click();
			
	Reporter.log("PASS >> Click on Multi-Select", true);
			
	sleep(2);
}			


//input[@class='MuiInputBase-input MuiOutlinedInput-input']

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement MultiSELECTofNewQuestionOftext ;	

public void ClearMultiSELECTofnewquestion() throws InterruptedException {
	MultiSELECTofNewQuestionOftext.click();
	
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	MultiSELECTofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}


//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKONSAVEOFMultiSELECT ;	

public void CLICKONSAVEOFMultiSELECT() throws InterruptedException {
	sleep(2);
	CLICKONSAVEOFMultiSELECT.click();
	
	Reporter.log("PASS >> Click on SAVE", true);
	Reporter.log("PASS >> ERROR MESSAGE Please fill out this field", true);
	sleep(2);
	}


//input[@class='MuiInputBase-input MuiOutlinedInput-input']


@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERmultiSELECTquestiontext ;	

public void entermultiSELECTquestiontext(String val) throws InterruptedException {
	ENTERmultiSELECTquestiontext.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERmultiSELECTquestiontext.sendKeys(val);
	
	Reporter.log("PASS >>  Multi Select question text entered successfully  ",true);
}		


//input[@aria-label='Question Name']

@FindBy(xpath="//input[@aria-label='Question Name']")
private WebElement ENTERmultiSELECTquestionname ;	

public void entermultiSELECTquestionname(String val) throws InterruptedException {
	ENTERmultiSELECTquestionname.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERmultiSELECTquestionname.sendKeys(val);
	
	Reporter.log("PASS >>  Multi Select question name entered successfully  ",true);
}		

//span[normalize-space()='Headless Question?']

@FindBy(xpath="//span[normalize-space()='Headless Question?']")
private WebElement CLICKONheadlessquestion ;	

public void CLICKONheadlessquestion() throws InterruptedException {
	sleep(2);
	CLICKONheadlessquestion.click();
			
	Reporter.log("PASS >> Click on Headless Question", true);
			
	sleep(2);
}			

//span[normalize-space()='Optional ?']

@FindBy(xpath="//span[normalize-space()='Optional ?']")
private WebElement CLICKONoptionalofmultiselect ;	

public void CLICKONoptionalofmultiselect() throws InterruptedException {
	sleep(2);
	CLICKONoptionalofmultiselect.click();
			
	Reporter.log("PASS >> Click on Optional", true);
			
	sleep(2);
}			


//(//input[@class='MuiInputBase-input MuiOutlinedInput-input'])[3]

@FindBy(xpath="(//input[@class='MuiInputBase-input MuiOutlinedInput-input'])[3]")
private WebElement ENTERAnsweroptionmultiSELECT ;	

public void enterAnsweroptionmultiSELECT(String val) throws InterruptedException {
	ENTERAnsweroptionmultiSELECT.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERAnsweroptionmultiSELECT.sendKeys(val);
	
	Reporter.log("PASS >>  Answer Option entered successfully  ",true);
}		


//span[normalize-space()='Preselected']

@FindBy(xpath="//span[normalize-space()='Preselected']")
private WebElement CLICKONPreselectedofmultiselect ;	

public void CLICKONPreselectedofmultiselect() throws InterruptedException {
	sleep(2);
	CLICKONPreselectedofmultiselect.click();
			
	Reporter.log("PASS >> Click on Preselected", true);
			
	sleep(2);
}		


//span[normalize-space()='Add Answer Option']

@FindBy(xpath="//span[normalize-space()='Add Answer Option']")
private WebElement CLICKONADDANSWEROPTIONofmultiselect ;	

public void CLICKONADDANSWEROPTIONofmultiselect() throws InterruptedException {
	sleep(2);
	CLICKONADDANSWEROPTIONofmultiselect.click();
			
	Reporter.log("PASS >> Click on ADD ANSWER OPTION", true);
			
	sleep(2);
}		

//body/div[@id='__next']/div[@class='dashboardstyles__DashboardContainer-sc-j26381-0 ioHwzv']/div[@class='dashboardstyles__PageContent-sc-j26381-1 ZRMNf']/div[@class='loading-wrapperstyles__Container-sc-iluo8b-0 etkgFB']/div[@class='survey-editorstyles__PageWrapper-sc-1uwbivv-0 dDZvMi']/div[@class='survey-editorstyles__ContentWrapper-sc-1uwbivv-2 cNqfBC']/form[@class='survey-editorstyles__QuestionEditForm-sc-1uwbivv-4 ifUiAi']/div[@class='MuiBox-root jss57']/div[@class='section-wrapperstyles__StyledSectionWrapper-sc-tqbqoh-0 edkJPr']/div[@class='section-wrapperstyles__StyledSectionWrapper-sc-tqbqoh-0 indexstyles__StyledSubSectionWrapper-sc-9khska-0 edkJPr bVTHSk answer-option-liststyles__AnswerOptionsSection-sc-1xet2u1-0 iWUqha']/div[@role='list']/div/div[2]/div[1]/div[3]/button[1]/span[1]//*[local-name()='svg']

@FindBy(xpath="(//button[@title='Delete Answer Option'])[2]")
private WebElement CLICKDeleteoptionofmultiselect ;	

public void CLICKDeleteoptionofmultiselect() throws InterruptedException {
	sleep(2);
	CLICKDeleteoptionofmultiselect.click();
			
	Reporter.log("PASS >> Click on Delete Option", true);
			
	sleep(2);
}		

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKSAVEOPTIONBUTTONofmultiselect ;	

public void CLICKSAVEOPTIONBUTTONofmultiselect() throws InterruptedException {
	sleep(2);
	CLICKSAVEOPTIONBUTTONofmultiselect.click();
			
	Reporter.log("PASS >> Click on SAVE Option", true);
			
	sleep(2);
}		


//span[normalize-space()='Remove']
@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement CLICKREMOVEOPTIONBUTTONofmultiselect ;	

public void CLICKREMOVEOPTIONBUTTONofmultiselect() throws InterruptedException {
	sleep(2);
	CLICKREMOVEOPTIONBUTTONofmultiselect.click();
			
	Reporter.log("PASS >> Click on REMOVE Option", true);
			
	sleep(2);
}		

//span[normalize-space()='Instruction']

@FindBy(xpath="//span[normalize-space()='Instruction']")
private WebElement CLICKInstruction ;	

public void CLICKInstruction() throws InterruptedException {
	sleep(2);
	CLICKInstruction.click();
			
	Reporter.log("PASS >> Click on Instruction", true);
			
	sleep(2);
}		

//input[@class='MuiInputBase-input MuiOutlinedInput-input']

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement InstructionofNewQuestionOftext ;	

public void Clearinstructionofnewquestion() throws InterruptedException {
	InstructionofNewQuestionOftext.click();
	
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	InstructionofNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}


//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKInstructiononSAVEBUTTON ;	

public void CLICKInstructiononSAVEBUTTON() throws InterruptedException {
	sleep(2);
	CLICKInstructiononSAVEBUTTON.click();
			
	Reporter.log("PASS >> Click on SAVE", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		



@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERInstructionquestiontext ;	

public void enterinstructionquestiontext(String val) throws InterruptedException {
	ENTERInstructionquestiontext.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERInstructionquestiontext.sendKeys(val);
	
	Reporter.log("PASS >>  Instruction question text entered successfully  ",true);
}		


@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement ENTERInstructionquestionName ;	

public void enterinstructionquestionName(String val) throws InterruptedException {
	ENTERInstructionquestionName.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERInstructionquestionName.sendKeys(val);
	
	Reporter.log("PASS >>  Instruction question Name entered successfully  ",true);
}		

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKInstructiononSAVEofBUTTON ;	

public void CLICKInstructiononSAVEofBUTTON() throws InterruptedException {
	sleep(2);
	CLICKInstructiononSAVEofBUTTON.click();
			
	Reporter.log("PASS >> Click on SAVE", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//span[text()='Remove']

@FindBy(xpath="//span[text()='Remove']")
private WebElement CLICKInstructiononRemoveofBUTTON ;	

public void CLICKInstructiononRemoveofBUTTON() throws InterruptedException {
	sleep(2);
	CLICKInstructiononRemoveofBUTTON.click();
			
	Reporter.log("PASS >> Click on Remove", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		


//span[normalize-space()='Button']

@FindBy(xpath="//span[normalize-space()='Button']")
private WebElement CLICKButton ;	

public void CLICKButton() throws InterruptedException {
	sleep(2);
	CLICKButton.click();
			
	Reporter.log("PASS >> Click on Button", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//input[@aria-label='Button Text']

@FindBy(xpath="//input[@aria-label='Button Text']")
private WebElement ENTERButtonquestiontext ;	

public void enterButtonquestionText(String val) throws InterruptedException {
	ENTERButtonquestiontext.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERButtonquestiontext.sendKeys(val);
	
	Reporter.log("PASS >>  Button question text entered successfully  ",true);
}		



//span[normalize-space()='Save']
@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKsaveofButtonmethod ;	

public void CLICKsaveofButtonmethod() throws InterruptedException {
	sleep(2);
	CLICKsaveofButtonmethod.click();
			
	Reporter.log("PASS >> Click on Save", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		


//span[normalize-space()='Navigation Enabled?']

@FindBy(xpath="//span[normalize-space()='Navigation Enabled?']")
private WebElement CLICKNavigationEnabledofButtonmethod ;	

public void CLICKNavigationEnabledofButtonmethod() throws InterruptedException {
	sleep(2);
	CLICKNavigationEnabledofButtonmethod.click();
			
	Reporter.log("PASS >> Click on Navigation Enabled", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//div[@id='select-nav-type']

@FindBy(xpath="//div[@id='select-nav-type']")
private WebElement CLICKNavigationtypeofButtonmethod ;	

public void CLICKNavigationtypeofButtonmethod() throws InterruptedException {
	sleep(2);
	CLICKNavigationtypeofButtonmethod.click();
			
	Reporter.log("PASS >> Click on Navigation type field", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//li[normalize-space()='Screen / Page']

@FindBy(xpath="//li[normalize-space()='Screen / Page']")
private WebElement CLICKscreenorpageofNavigationtype ;	

public void CLICKscreenorpageofNavigationtype() throws InterruptedException {
	sleep(2);
	CLICKscreenorpageofNavigationtype.click();
			
	Reporter.log("PASS >> Click on Screen Page field", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//li[normalize-space()='Workflow']

@FindBy(xpath="//li[normalize-space()='Workflow']")
private WebElement CLICKWorkflowofNavigationtype ;	

public void CLICKWorkflowofNavigationtype() throws InterruptedException {
	sleep(2);
	CLICKWorkflowofNavigationtype.click();
			
	Reporter.log("PASS >> Click on Workflow field", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//li[normalize-space()='Workflow-Page']

@FindBy(xpath="//li[normalize-space()='Workflow-Page']")
private WebElement CLICKWorkflowpageofNavigationtype ;	

public void CLICKWorkflowpageofNavigationtype() throws InterruptedException {
	sleep(2);
	CLICKWorkflowpageofNavigationtype.click();
			
	Reporter.log("PASS >> Click on Workflow Page field", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}

//div[@class='MuiInputBase-root MuiOutlinedInput-root Mui-focused Mui-focused MuiInputBase-formControl MuiInputBase-marginDense MuiOutlinedInput-marginDense']//div[@role='button']
//(//div[@aria-haspopup='listbox'])[2]

		@FindBy(xpath="(//div[@aria-haspopup='listbox'])[2]")
		private WebElement CLICKSelectpagetypeofNavigationtype ;	

		public void CLICKSelectpagetypeofNavigationtype() throws InterruptedException {
			sleep(2);
			CLICKSelectpagetypeofNavigationtype.click();
					
			Reporter.log("PASS >> Click on Select Page Type field", true);
//			Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
			sleep(2);
		}

//li[normalize-space()='Vehicle']

@FindBy(xpath="//li[normalize-space()='Vehicle']")
private WebElement CLICKVehicleofselectpagetype ;	

public void CLICKVehicleofselectpagetype() throws InterruptedException {
	sleep(2);
	CLICKVehicleofselectpagetype.click();
					
	Reporter.log("PASS >> Click on Vehicle of select page type", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		
		
//li[normalize-space()='Tool Check']		
		
@FindBy(xpath="//li[normalize-space()='Tool Check']")
private WebElement CLICKtoolcheckofselectpagetype ;	

public void CLICKtoolcheckofselectpagetype() throws InterruptedException {
	sleep(2);
	CLICKtoolcheckofselectpagetype.click();
					
	Reporter.log("PASS >> Click on tool Check of select page type", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}				

//li[normalize-space()='Stock Check']

@FindBy(xpath="//li[normalize-space()='Stock Check']")
private WebElement CLICKStockCheckofselectpagetype ;	

public void CLICKStockCheckofselectpagetype() throws InterruptedException {
	sleep(2);
	CLICKStockCheckofselectpagetype.click();
					
	Reporter.log("PASS >> Click on Stock Check of select page type", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}			

//li[normalize-space()='TestNewPage']

@FindBy(xpath="//li[normalize-space()='TestNewPage']")
private WebElement CLICKTestNewPageofselectpagetype ;	

public void CLICKTestNewPageofselectpagetype() throws InterruptedException {
	sleep(2);
	CLICKTestNewPageofselectpagetype.click();
					
	Reporter.log("PASS >> Click on TestNewPage of select page type", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		


//span[normalize-space()='Actions Enabled?']

@FindBy(xpath="//span[normalize-space()='Actions Enabled?']")
private WebElement CLICKActionenabledofButtonmethod ;	

public void CLICKActionenabledofButtonmethod() throws InterruptedException {
	sleep(2);
	CLICKActionenabledofButtonmethod.click();
					
	Reporter.log("PASS >> Click on Actions Enabled of Button Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//div[@class='MuiInputBase-root MuiOutlinedInput-root Mui-focused Mui-focused MuiInputBase-formControl MuiInputBase-marginDense MuiOutlinedInput-marginDense']//div[@role='button']

//div[@class='MuiSelect-root MuiSelect-select MuiSelect-selectMenu MuiSelect-outlined MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputMarginDense MuiOutlinedInput-inputMarginDense']
//(//div[@aria-haspopup='listbox'])[3]

@FindBy(xpath="(//div[@aria-haspopup='listbox'])[3]")
private WebElement CLICKActionenabledoptiontext ;	

public void CLICKActionenabledoptiontext() throws InterruptedException {
	sleep(2);
	CLICKActionenabledoptiontext.click();
					
	Reporter.log("PASS >> Click on Actions Enabled of option Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//li[normalize-space()='Set Job To Done']

@FindBy(xpath="//li[normalize-space()='Set Job To Done']")
private WebElement CLICKsetjobtodoneoptiontext ;	

public void CLICKsetjobtodoneoptiontext() throws InterruptedException {
	sleep(2);
	CLICKsetjobtodoneoptiontext.click();
					
	Reporter.log("PASS >> Click on Set Job to Done of option Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//li[normalize-space()='Set Job-Item To Done']

@FindBy(xpath="//li[normalize-space()='Set Job-Item To Done']")
private WebElement CLICKsetjobitemtodoneoptiontext ;	

public void CLICKsetjobitemtodoneoptiontext() throws InterruptedException {
	sleep(2);
	CLICKsetjobitemtodoneoptiontext.click();
					
	Reporter.log("PASS >> Click on Set Job-item to Done of option Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//li[normalize-space()='Set Break To Done']

@FindBy(xpath="//li[normalize-space()='Set Break To Done']")
private WebElement CLICKsetbreaktodoneoptiontext ;	

public void CLICKsetbreaktodoneoptiontext() throws InterruptedException {
	sleep(2);
	CLICKsetbreaktodoneoptiontext.click();
					
	Reporter.log("PASS >> Click on Set break to Done of option Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}		

//span[normalize-space()='Add Action Option']

@FindBy(xpath="//span[normalize-space()='Add Action Option']")
private WebElement CLICKAddActionOptiontab ;	

public void CLICKAddActionOptiontab() throws InterruptedException {
	sleep(2);
	CLICKAddActionOptiontab.click();
					
	Reporter.log("PASS >> Click on Add Action Option of tab Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//div[@class='MuiBox-root jss19 indexstyles__InlineFieldsWrapper-sc-z4b06v-2 hmCpdf']//span[@class='MuiIconButton-label']//*[local-name()='svg']//*[name()='path' and contains(@d,'M14.12 10.')]

@FindBy(xpath="(//span[@class='MuiIconButton-label'])[4]")
private WebElement CLICKdeleteboxOptiontab ;	

public void CLICKdeleteboxOptiontab() throws InterruptedException {
	sleep(2);
	CLICKdeleteboxOptiontab.click();
					
	Reporter.log("PASS >> Click on Delete Option  tab Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Optional?']

@FindBy(xpath="//span[normalize-space()='Optional?']")
private WebElement CLICKoptionalboxOptiontab ;	

public void CLICKoptionalboxOptiontab() throws InterruptedException {
	sleep(2);
	CLICKoptionalboxOptiontab.click();
					
	Reporter.log("PASS >> Click on Optional  tab Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKsaveboxOptiontab ;	

public void CLICKsaveboxOptiontab() throws InterruptedException {
	sleep(2);
	CLICKsaveboxOptiontab.click();
					
	Reporter.log("PASS >> Click on Save  tab Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement CLICKRemoveboxOptiontab ;	

public void CLICKRemoveboxOptiontab() throws InterruptedException {
	sleep(2);
	CLICKRemoveboxOptiontab.click();
					
	Reporter.log("PASS >> Click on Remove  tab Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Photo']

@FindBy(xpath="//span[normalize-space()='Photo']")
private WebElement CLICKPhotoOptiontab ;	

public void CLICKPhotoOptiontab() throws InterruptedException {
	sleep(2);
	CLICKPhotoOptiontab.click();
					
	Reporter.log("PASS >> Click on Photo  tab Method", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	


//input[@class='MuiInputBase-input MuiOutlinedInput-input']

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement PhotofieldNewQuestionOftext ;	

public void ClearPhotoofnewquestion() throws InterruptedException {
	PhotofieldNewQuestionOftext.click();
	
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	PhotofieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}


//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKSaveofPhotoOptiontab ;	

public void CLICKSaveofPhotoOptiontab() throws InterruptedException {
	sleep(2);
	CLICKSaveofPhotoOptiontab.click();
					
	Reporter.log("PASS >> Click on Save  tab ", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERPhotoquestiontext ;	

public void enterPhotoquestiontext(String val) throws InterruptedException {
	ENTERPhotoquestiontext.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERPhotoquestiontext.sendKeys(val);
	
	Reporter.log("PASS >>  Photo question text entered successfully  ",true);
}		

//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement ENTERPhotoquestionName;	

public void enterPhotoquestionName(String val) throws InterruptedException {
	ENTERPhotoquestionName.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERPhotoquestionName.sendKeys(val);
	
	Reporter.log("PASS >>  Photo question text entered successfully  ",true);
}		

//span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1']

@FindBy(xpath="//span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1']")
private WebElement CLICKOptionalPhotoOptiontab ;	

public void CLICKOptionalPhotoOptiontab() throws InterruptedException {
	sleep(2);
	CLICKOptionalPhotoOptiontab.click();
					
	Reporter.log("PASS >> Click on Optional checkbox", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKtabsavePhotoOptiontab ;	

public void CLICKtabsavePhotoOptiontab() throws InterruptedException {
	sleep(2);
	CLICKtabsavePhotoOptiontab.click();
					
	Reporter.log("PASS >> Click on Save tab", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Remove']

@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement CLICKtabRemovePhotoOptiontab ;	

public void CLICKtabRemovePhotoOptiontab() throws InterruptedException {
	sleep(2);
	CLICKtabRemovePhotoOptiontab.click();
					
	Reporter.log("PASS >> Click on Remove tab", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Signature']

@FindBy(xpath="//span[normalize-space()='Signature']")
private WebElement CLICKSignatureOptiontab ;	

public void CLICKSignatureOptiontab() throws InterruptedException {
	sleep(2);
	CLICKSignatureOptiontab.click();
					
	Reporter.log("PASS >> Click on Signature tab", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//input[@class='MuiInputBase-input MuiOutlinedInput-input']

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement SignaturefieldNewQuestionOftext ;	

public void ClearSignatureofnewquestion() throws InterruptedException {
	SignaturefieldNewQuestionOftext.click();
	
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
	SignaturefieldNewQuestionOftext.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKSaveofSignatureOptiontab ;	

public void CLICKSaveofSignatureOptiontab() throws InterruptedException {
	sleep(2);
	CLICKSaveofSignatureOptiontab.click();
					
	Reporter.log("PASS >> Click on Save tab", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERSignaturequestiontext;	

public void enterSignaturequestiontext(String val) throws InterruptedException {
	ENTERSignaturequestiontext.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERSignaturequestiontext.sendKeys(val);
	
	Reporter.log("PASS >>  Signature question text entered successfully  ",true);
}		

//textarea[@aria-label='Question Name']

@FindBy(xpath="//textarea[@aria-label='Question Name']")
private WebElement ENTERSignaturequestionName;	

public void enterSignaturequestionName(String val) throws InterruptedException {
	ENTERSignaturequestionName.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERSignaturequestionName.sendKeys(val);
	
	Reporter.log("PASS >>  Signature question Name entered successfully  ",true);
}		

@FindBy(xpath="//span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1']")
private WebElement CLICKoptionalofSignatureOptiontab ;	

public void CLICKoptionalofSignatureOptiontab() throws InterruptedException {
	sleep(2);
	CLICKoptionalofSignatureOptiontab.click();
					
	Reporter.log("PASS >> Click on Optional tab", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKSavetabofSignatureOption ;	

public void CLICKSavetabofSignatureOption() throws InterruptedException {
	sleep(2);
	CLICKSavetabofSignatureOption.click();
					
	Reporter.log("PASS >> Click on Save tab", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	

//span[normalize-space()='Remove']
@FindBy(xpath="//span[normalize-space()='Remove']")
private WebElement CLICKRemovetabofSignatureOption ;	

public void CLICKRemovetabofSignatureOption() throws InterruptedException {
	sleep(2);
	CLICKRemovetabofSignatureOption.click();
					
	Reporter.log("PASS >> Click on Remove tab", true);
//	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	


/////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////Start Your Break/////////////////////////////////////////////////////////

//span[normalize-space()='New Page']

@FindBy(xpath="//span[normalize-space()='New Page']")
private WebElement NewPagetextisdisplayedofEDITDRAFTBreakModule ;	
	
public void NewPagetextisdisplayedofEDITDRAFTBreakModule()throws InterruptedException{

	boolean flag=NewPagetextisdisplayedofEDITDRAFTBreakModule.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='New Page']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Page text is not Displayed");
		Reporter.log("FAIL >>New Page text is not Displayed",true);
	}

}

//span[normalize-space()='New Section']

@FindBy(xpath="//span[normalize-space()='New Section']")
private WebElement NewSectiontextisdisplayedofEDITDRAFTBreakModule ;	
	
public void NewSectiontextisdisplayedofEDITDRAFTBreakModule()throws InterruptedException{

	boolean flag=NewSectiontextisdisplayedofEDITDRAFTBreakModule.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='New Section']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Section text is not Displayed");
		Reporter.log("FAIL >>New Section text is not Displayed",true);
	}

}

//span[normalize-space()='New Question']

@FindBy(xpath="//span[normalize-space()='New Question']")
private WebElement NewQuestiontextisdisplayedofEDITDRAFTBreakModule ;	
	
public void NewQuestiontextisdisplayedofEDITDRAFTBreakModule()throws InterruptedException{

	boolean flag=NewQuestiontextisdisplayedofEDITDRAFTBreakModule.isDisplayed();
	if(flag)
	{	   		
		String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='New Question']")).getText();
		Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>New Question text is not Displayed");
		Reporter.log("FAIL >>New Question text is not Displayed",true);
	}

}

//input[@class='MuiInputBase-input MuiOutlinedInput-input']

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement StartyourBreakTitle ;	

public void ClearStartyourbreaktitle() throws InterruptedException {
	StartyourBreakTitle.click();
	
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
	StartyourBreakTitle.sendKeys(Keys.BACK_SPACE);
//	Reporter.log("PASS >> TEST NEW PAGE entered successfully  ",true);
}

@FindBy(xpath="//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
private WebElement ENTERStartyourbreaktitle;	

public void enterStartyourbreak(String val) throws InterruptedException {
	ENTERStartyourbreaktitle.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERStartyourbreaktitle.sendKeys(val);
	
	Reporter.log("PASS >>  Start Your Break entered successfully  ",true);
}		


//span[normalize-space()='Save']

@FindBy(xpath="//span[normalize-space()='Save']")
private WebElement CLICKSaveofStartyourBreakOption ;	

public void CLICKSaveofStartyourBreakOption() throws InterruptedException {
	sleep(2);
	CLICKSaveofStartyourBreakOption.click();
					
	Reporter.log("PASS >> Click on Save tab", true);
	Reporter.log("PASS >> Error Message Displayed is: Please fill out this field ", true);		
	sleep(2);
}	


@FindBy(xpath="//textarea[@aria-label='Description']")
private WebElement ENTERStartyourbreakDescription;	

public void enterStartyourbreakDescription(String val) throws InterruptedException {
	ENTERStartyourbreakDescription.click();
	//sleep(2);
	//ENTERNEWPAGETITLEOfEDITDRAFTstartofday.clear();
	//sleep(2);
	ENTERStartyourbreakDescription.sendKeys(val);
	
	Reporter.log("PASS >>  Start Your Break Description entered successfully  ",true);
}		











}
