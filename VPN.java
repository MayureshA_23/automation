package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class VPN  extends Initialization{
	
	@Test(priority='1',description="1.To verify clicking on VPN") 
    public void VerifyClickingOnVPN() throws InterruptedException {
    Reporter.log("To verify clicking on VPN");
    profilesAndroid.ClickOnProfile();
	profilesiOS.clickOniOSOption();
	profilesiOS.AddProfile();
	profilesiOS.ClickOnVPN();
}
	@Test(priority='2',description="1.To Verify warning message Saving VPN without Profile Name")
	public void VerifyWarningMessageOnSavingVPNWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving VPN without Profile Name========");
		profilesiOS.ClickOnConfigureButtonVPNProfile();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='3',description="1.To Verify warning message Saving a VPN Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingVPNProfileWihtoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a VPN Profile without entering all the fields=========");
		profilesiOS.EnterVPNProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	@Test(priority='4',description="1.To Verify parameters of VPN profile")
	public void VerifyParametersOfVPNProfile() throws InterruptedException{
		Reporter.log("=======To Verify parameters of VPN profile=========");
		profilesiOS.VerifyVPNParameters(); //after pulse secure is enabled
	}

	@Test(priority='5',description="1.To Verify warning message while saving VPN profile without server URL ")
	public void VerifyWarningSavingVPNProfileWithoutServerURL() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify warning message while saving VPN profile without server URL =========");
		
		profilesiOS.EnterConnectionName();
		profilesiOS.ChooseConnectionType();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.warningMessageWithoutSSIDorPasswordorServerURL();
			
}
	@Test(priority='6',description="1.To Verify saving VPN profile")
	public void VerifySavingVPNProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("=======To Verify saving VPN profile=========");
		profilesiOS.EnterServer_VPN();
		profilesiOS.EnterPasswordVPN();
		profilesiOS.ClickOnSavebutton();
		profilesAndroid.NotificationOnProfileCreated();
		
	}
}
		
		
	
	
