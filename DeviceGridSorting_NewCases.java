package DeviceGrid_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import bsh.ParseException;

public class DeviceGridSorting_NewCases extends Initialization {
	// 1. Verify data for SureLock Version column.
	// 2. Verify search for SureLock Version column.
	// 3. Verify sorting for SureLock Version column.
	@Test(priority = 15, description = "Validation of SureLock Version column in Device Grid.")
	public void verifyValueForSureLockVersionColumn_TC_DG_022()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify data for SureLock Version column******");
		deviceGrid.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnGridColumnSearch();
		deviceGrid.enterColumnName("SureLock Version");
		deviceGrid.verifySurelockColumnIsEnabledAndGetFinalValue(Config.SureLockVersion);
		deviceGrid.clearSearchBox();
	}

	@Test(priority = 16, description = "Validation of SureLock Version column in Device Grid.")
	public void verifyAdvanceSearchForSureLockVersionAColumn_TC_DG_022()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		// 2. Verify search for SureLock Version column.
		System.out.println("*****Verify search for SureLock Version column******");

		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.EnterValueInSureLockVersionSearchBox("11>");
		deviceGrid.verifyTheVersionOnAllDevices("11>", "SureLockVersion");

	}

	@Test(priority = 17, description = "Validation of SureLock Version column in Device Grid.")
	public void verifySortingForSureLockVersionColumn_TC_DG_022()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting for SureLock Version column******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("SureLockVersion", deviceGrid.SurelockVersionSort);
		deviceGrid.clickColumForSort("SureLockVersion", deviceGrid.SurelockVersionSort);
		deviceGrid.VerifydecendingOrder("SureLockVersion");
		deviceGrid.clickPagination2InGrid("SureLockVersion");
		deviceGrid.scrollToCoulmn("SureLockVersion", deviceGrid.SurelockVersionSort);
		deviceGrid.VerifydecendingOrder("SureLockVersion");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.scrollToCoulmn("SureLockVersion", deviceGrid.SurelockVersionSort);
		deviceGrid.VerifydecendingOrder("SureLockVersion");
		deviceGrid.MoveDevicesToGroup("Home");
		// deviceGrid.clickOnDeviceInGrid("SureLockVersion");
		deviceGrid.clickDeviceGridRefresh();

	}

	// Validation of OS Version column in Device Grid.
	// 1. Verify data for OS Version column.
	// 2. Verify sorting for OS Version column.
	// 3. Verify search for OS Version column.

	@Test(priority = 18, description = "Validation of OS Version column in Device Grid.")
	public void verifyOSVersionColumnInDeviceGrid_TC_DG_021()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("***** Verify data for OS Version column******");
		deviceGrid.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnGridColumnSearch();
		deviceGrid.enterColumnName("OS Version");
		deviceGrid.verifyOSColumnIsEnabledAndGetFinalValue(Config.OSVersion);
		deviceGrid.clearSearchBox();

	}

	@Test(priority = 19, description = "Validation of OS Version column in Device Grid.")
	public void verifyAdvanceSearchForOSVersionAColumn_TC_DG_022()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("***** Verify search for OS Version column******");
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.EnterValueInOSVersionSearchBox("7>");
		deviceGrid.verifyTheVersionOnAllDevices("7>", "ReleaseVersion");

	}

	@Test(priority = 20, description = "Validation of OS Version column in Device Grid.")
	public void verifyOSVersionColumnSorting_TC_DG_022()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting for OS Version column******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("ReleaseVersion", deviceGrid.OSVersionSort);
		deviceGrid.clickColumForSort("ReleaseVersion", deviceGrid.OSVersionSort);
		deviceGrid.VerifydecendingOrder("ReleaseVersion");
		deviceGrid.clickPagination2InGrid("ReleaseVersion");
		deviceGrid.scrollToCoulmn("ReleaseVersion", deviceGrid.OSVersionSort);
		deviceGrid.VerifydecendingOrder("ReleaseVersion");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.scrollToCoulmn("ReleaseVersion", deviceGrid.OSVersionSort);
		deviceGrid.VerifydecendingOrder("ReleaseVersion");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.clickDeviceGridRefresh();

	}

	// Verify sorting of agent version column in device grid
	@Test(priority = 21, description = "//Verify sorting of agent version column in device grid")
	public void verifySortingOfAgentVersion()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting of agent version column in device grid******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.clickDeviceGridRefresh();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("AgentVersion", deviceGrid.AgentVersionSort);
		deviceGrid.clickColumForSort("AgentVersion", deviceGrid.AgentVersionSort);
		deviceGrid.VerifydecendingOrder("AgentVersion");

	}

	// Verify sorting of SL/SF/SV version columns in device grid
	// SL
	@Test(priority = 22, description = "//Verify sorting of SL/SF/SV version columns in device grid")
	public void verifySortingOfSLVersion()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting of SureLock version columns in device grid******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("SureLockVersion", deviceGrid.SurelockVersionSort);
		deviceGrid.clickColumForSort("SureLockVersion", deviceGrid.SurelockVersionSort);
		deviceGrid.VerifydecendingOrder("SureLockVersion");

	}

	// Verify sorting of SL/SF/SV version columns in device grid
	// SF
	@Test(priority = 23, description = "//Verify sorting of SL/SF/SV version columns in device grid",enabled=false)
	public void verifySortingOfSFVersion()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting of SureFox version columns in device grid******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("SureFoxVersion", deviceGrid.SureFoxVersionSort);
		deviceGrid.clickColumForSort("SureFoxVersion", deviceGrid.SureFoxVersionSort);
		deviceGrid.VerifyAcesdingOrder("SureFoxVersion");

	}

	// Verify sorting of SL/SF/SV version columns in device grid
	// SV
	@Test(priority = 24, description = "//Verify sorting of SL/SF/SV versioncolumns in device grid",enabled=false)
	public void verifySortingOfSVVersion()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting of SureVideo version columns indevice grid******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("SureVideoVersion", deviceGrid.SureVideoVersionSort);
		deviceGrid.clickColumForSort("SureVideoVersion", deviceGrid.SureVideoVersionSort);
		deviceGrid.VerifyAcesdingOrder("SureVideoVersion");

	}

	// Validation of Security Patch Date column in Device Grid.

	// 1. Verify data for Security Patch Date column.

	// 2. Verify sorting for Security Patch Date column.

	// 3. Verify search for Security Patch Date column.
	@Test(priority = 25, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifySecurityPatchDateColumnInDeviceGrid_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		// 1. Verify data for Security Patch Date column.
		System.out.println("*****Verify data for Security Patch Date column******");
		deviceGrid.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnGridColumnSearch();
		deviceGrid.enterColumnName("Security Patch Date");
		deviceGrid.verifySecurityPatchDateColumnIsEnabledAndGetFinalValue(Config.SecurityPatchColumnValue);

	}

	@Test(priority = 26, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifySortingOfSecurityPatchDateColumn_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting for Security Patch Date column******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnAllDevices();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchDateSort);
		deviceGrid.clickColumForSort("SecurityPatchDate", deviceGrid.SecurityPatchDateSort);
		deviceGrid.VerifyDateSorting();
		deviceGrid.clickPagination2InGrid("SecurityPatchDate");
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchDateSort);
		deviceGrid.VerifyDateSorting();
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchDateSort);
		deviceGrid.VerifyDateSorting();
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.RefreshGrid();
	}

	@Test(priority = 27, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifyAdvanceSearchForSecurityPatchDateColumnWithYesterdayDate_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify search for Security Patch Date column with Yesterday Date option ******");
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchSearchBox);
		deviceGrid.clickOnSecurityPatchSearchBox();
		deviceGrid.SelectDateFromCale_Yestrday();
		deviceGrid.verifyAdvanceSearchOfSecurityPatchDate();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();

	}

	@Test(priority = 28, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifyAdvanceSearchForSecurityPatchDateColumnWithLastWeekDate_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify search for Security Patch Date column with Last Week Date option ******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchSearchBox);
		deviceGrid.clickOnSecurityPatchSearchBox();
		deviceGrid.SelectDateFromCale_Last1week();
		deviceGrid.verifyAdvanceSearchOfSecurityPatchDate();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();

	}

	@Test(priority = 29, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifyAdvanceSearchForSecurityPatchDateColumnWithLast30Days_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify search for Security Patch Date column with Last 30 Days option ******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchSearchBox);
		deviceGrid.clickOnSecurityPatchSearchBox();
		deviceGrid.SelectDateFromCale_Last30Days();
		deviceGrid.verifyAdvanceSearchOfSecurityPatchDate();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();

	}

	@Test(priority = 30, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifyAdvanceSearchForSecurityPatchDateColumnWithThisMonth_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify search for Security Patch Date column with This Month option ******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchSearchBox);
		deviceGrid.clickOnSecurityPatchSearchBox();
		deviceGrid.SelectDateFromCale_ThisMonth();
		deviceGrid.verifyAdvanceSearchOfSecurityPatchDate();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();

	}

	@Test(priority = 31, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifyAdvanceSearchForSecurityPatchDateColumnWithToday_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify search for Security Patch Date column with Today option ******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchSearchBox);
		deviceGrid.clickOnSecurityPatchSearchBox();
		deviceGrid.SelectDateFromCale_Today();
		deviceGrid.verifyAdvanceSearchOfSecurityPatchDate();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();

	}

	@Test(priority = 32, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifyAdvanceSearchForSecurityPatchDateColumnWithCustomDate_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify search for Security Patch Date column with Custom Date option ******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchSearchBox);
		deviceGrid.clickOnSecurityPatchSearchBox();
		deviceGrid.SelectDateFromCale_CustomRange();
		deviceGrid.clickOnDate_1();
		deviceGrid.getCurrentDayAndSelect();

		deviceGrid.clickcalenderApplyButton();
		deviceGrid.verifyAdvanceSearchOfSecurityPatchDate();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();

	}

	@Test(priority = 33, description = "Validation of Security Patch Date column in Device Grid.")
	public void verifyAdvanceSearchForSecurityPatchDateColumnWithCustomDateCancle_TC_DG_051()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println(
				"*****Verify search for Security Patch Date column with Custom Date option and then cancle  ******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("SecurityPatchDate", deviceGrid.SecurityPatchSearchBox);
		deviceGrid.clickOnSecurityPatchSearchBox();
		deviceGrid.SelectDateFromCale_CustomRange();
		deviceGrid.clickcalenderCancleButton();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();

	}
	// Validation of Group Path column in Device Grid.

	// 1. Verify data for Group Path column.

	// 2. Verify sorting for Group Path column.

	// 3. Verify search for Group Path column.
	@Test(priority = 34, description = "Validation of Group Path column in Device Grid.")
	public void verifyGroupPathColumnInDeviceGrid_TC_DG_053()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		// 1. Verify data for Group Path column.
		System.out.println("***** Verify data for Group Path column ******");
		deviceGrid.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnGridColumnSearch();
		deviceGrid.enterColumnName("Group Path");
		deviceGrid.verifyDeviceGroupPathColumnIsEnabledAndGetFinalValue(Config.DeviceGroupPathColumnValue);

	}

	@Test(priority = 35, description = "Validation of Group Path column in DeviceGrid.")
	public void verifyGroupPathColumnSorting_TC_DG_053()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		// 2. Verify sorting for Group Path column.
		System.out.println("***** Verify sorting for Group Path column ******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnAllDevices();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("DeviceGroupPath", deviceGrid.DeviceGroupPathSort);
		deviceGrid.clickColumForSort("DeviceGroupPath", deviceGrid.DeviceGroupPathSort);
		deviceGrid.VerifydecendingOrder("DeviceGroupPath");
		deviceGrid.clickPagination2InGrid("DeviceGroupPath");
		deviceGrid.clickOnAllDevices();
		deviceGrid.scrollToCoulmn("DeviceGroupPath", deviceGrid.DeviceGroupPathSort);
		deviceGrid.VerifydecendingOrder("DeviceGroupPath");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.scrollToCoulmn("DeviceGroupPath", deviceGrid.DeviceGroupPathSort);
		deviceGrid.VerifydecendingOrder("DeviceGroupPath");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.RefreshGrid();
	}

	@Test(priority = 36, description = " Validation of Group Path column in Device Grid.")
	public void verifyAdvanceSearchForGroupPathColumn_TC_DG_022()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("***** Verify Search for Group Path column ******");
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.EnterValueInGroupPathColumn(Config.DeviceGroupPathColumnValue);
		deviceGrid.verifySameGroupPathDevices(Config.DeviceGroupPathColumnValue, "DeviceGroupPath");

	}

	// Validation of GPU Usage column in Device Grid.

	// 1. Verify data for GPU Usage column.

	// 3. Verify sorting for GPU Usage column.

	// 2. Verify search for GPU Usage column.
	@Test(priority = 37, description = "Validation of GPU Usage column in Device Grid.")
	public void verifyGPUUsageColumnInDeviceGrid_TC_DG_033()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		// 1. Verify data for GPU Usage column.
		System.out.println("***** Verify data for GPU Usage column ******");
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();
		deviceGrid.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnGridColumnSearch();
		deviceGrid.enterColumnName("GPU Usage");
		deviceGrid.verifyGPUUsageColumnIsEnabledAndGetFinalValue(Config.GpuUsageColumnValue);

	}

	@Test(priority = 38, description = "Validation of GPU Usage column in Device Grid.")
	public void verifyAdvanceSearchForGPUUsageColumn_TC_DG_033()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify search for GPU Usage column******");
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.EnterValueInGPUUsageColumn("<20");
		deviceGrid.verifyGpuUsageDevices(">10", "GpuUsage");

	}

	@Test(priority = 39, description = "Validation of GPU Usage column in Device Grid.")
	public void verifyGpuUsageColumnSorting_TC_DG_033()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify sorting for GPU Usage column******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("GpuUsage", deviceGrid.GpuUsageSort);
		deviceGrid.clickColumForSort("GpuUsage", deviceGrid.GpuUsageSort);
		deviceGrid.VerifydecendingOrderOfPercenttageValue("GpuUsage");
		deviceGrid.clickPagination2InGrid("GpuUsage");
		deviceGrid.scrollToCoulmn("GpuUsage", deviceGrid.GpuUsageSort);
		deviceGrid.VerifydecendingOrderOfPercenttageValue("GpuUsage");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.scrollToCoulmn("GpuUsage", deviceGrid.GpuUsageSort);
		deviceGrid.VerifydecendingOrderOfPercenttageValue("GpuUsage");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.clickDeviceGridRefresh();
	}

	@Test(priority = 40, description = "Verify Installed Application Version column in Device Grid")
	public void verifySortingOperationOnTheSearchResult_TC_DG_05()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify Sorting for Installed Application Versioncolumn in Device Grid******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickSearchDeviceOption();
		deviceGrid.clickDeviceGridRefresh();
		deviceGrid.clickOnAllDevices();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.clickOnGridColumnDropdown();
		deviceGrid.clickOnInstalledApplicationVersion();
		deviceGrid.clickOnCustomAppListSearchBox();
		deviceGrid.clearCustomAppSearchBox();
		deviceGrid.enterAppNameForSearch(Config.SearchInstalledAppForSorting);
		deviceGrid.EnableApp();
		deviceGrid.clickSearchOptionsGrid();
		deviceGrid.clickadvSearchDeviceOption();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToInstalledApp();
		deviceGrid.clickOnInstalledAppSort();
		deviceGrid.VerifydecendingOrderOfInstalledApp("Application1");

	}

	@Test(priority = 41, description = "Sorting should be performed")
	public void verifySortingOperationOnOnlineAndOfflineColumn_TC_DG_250()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println(
				"*****Verify Sorting Operation on status column for Online And Offline devices in Device Grid******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnAllDevices();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.clickStatusColumn();
		deviceGrid.clickConnectionStatus();
		deviceGrid.VerifydecendingOrder("ConnectionStatus");
	}

	@Test(priority = 42, description = "Veify Sorting a column in device grid and move to Next page or Group",enabled=false)
	public void verifySortingImeiColumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		System.out.println("*****Verify Sorting on IMEI column in DeviceGrid******");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.clickOnAllDevices();
		deviceGrid.clickOnPaginationDropDown();
		deviceGrid.selectPagination();
		deviceGrid.scrollToCoulmn("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.clickColumForSort("PhoneNumber", deviceGrid.PhoneNumberSort);
		deviceGrid.scrollToCoulmn("IMEI", deviceGrid.IMEISort);
		deviceGrid.clickColumForSort("IMEI", deviceGrid.IMEISort);
		deviceGrid.VerifydecendingOrder("IMEI");
		deviceGrid.clickPagination2InGrid("IMEI");
		deviceGrid.scrollToCoulmn("IMEI", deviceGrid.IMEISort);
		deviceGrid.VerifydecendingOrder("IMEI");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.scrollToCoulmn("IMEI", deviceGrid.IMEISort);
		deviceGrid.VerifydecendingOrder("IMEI");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.clickDeviceGridRefresh();
	}
	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException {
			if(ITestResult.FAILURE==result.getStatus())
			{
				deviceGrid.RefreshGrid();
			}
		}
}
