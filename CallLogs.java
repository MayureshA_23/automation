package DynamicJobs_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Utility;

public class CallLogs extends Initialization
{

	@Test(priority='1',description="1.To Verify Dynamic Call Log Job Window's UI")
	public void DynamicCallLogJob_TC1() throws InterruptedException, Throwable{
		Reporter.log("\n1.To Verify Dynamic Call Log Job Window's UI",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.VerifyOfCallLogWindow();
	    dynamicjobspage.VerifyOfGridMessage();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window's UI",true);
	}
	
	
	
	@Test(priority='2',description="2.To Verify Dynamic Call Log Job Window when Call log is OFF, Update interval textbox must be grayed out")
	public void DynamicCallLogJob_TC2() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Dynamic Call Log Job Window when Call log is OFF, Update interval textbox must be grayed out",true);
	
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_Off();
	    dynamicjobspage.VerifyOfUpdateIntervalGrayedOut();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Call log is OFF, Update interval textbox must be grayed out",true);
	}
	
  	@Test(priority='3',description="3.To Verify Dynamic Call Log Job Window when Call log is ON, Update interval textbox must be Enabled")
	public void DynamicCallLogJob_TC3() throws InterruptedException, Throwable{
		Reporter.log("\n3.To Verify Dynamic Call Log Job Window when Call log is ON, Update interval textbox must be Enabled",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_On();
	    dynamicjobspage.VerifyOfUpdateInterval();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Call log is ON, Update interval textbox must be Enabled",true);
	}
	
  	@Test(priority='4',description="4.To Verify Dynamic Call Log Job Window when Update interval is less than fourteen")
	public void DynamicCallLogJob_TC4() throws InterruptedException, Throwable{
		Reporter.log("\n4.To Verify Dynamic Call Log Job Window when Update interval is less than fourteen",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_On();
	    dynamicjobspage.ClearCallLogInterval();
	    dynamicjobspage.EnterCallLogInterval("10");
	    dynamicjobspage.ClickOnCallLogOkBtn();
	    dynamicjobspage.CallLogErrorMessage();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Update interval is less than fourteen",true);
	} 
	
	@Test(priority='5',description="5.To Verify Dynamic Call Log Job Window when Clicked on Cancel button")
	public void DynamicCallLogJob_TC5() throws InterruptedException, Throwable{
		Reporter.log("\n5.To Verify Dynamic Call Log Job Window when Clicked on Cancel button",true);
		String CallLogInterval="20";
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_On();
	    dynamicjobspage.ClearCallLogInterval();
	    dynamicjobspage.EnterCallLogInterval(CallLogInterval);
	    dynamicjobspage.ClickOnCallLogCancelBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Clicked on Cancel button",true);
	}
	
	@Test(priority='6',description="6.To Verify Dynamic Call Log Job Window when Call Log is On and Clicked On Ok button")
	public void DynamicCallLogJob_TC6() throws InterruptedException, Throwable{ 
		Reporter.log("\n6.To Verify Dynamic Call Log Job Window when Call Log is On and Clicked On Ok button",true);
		String CallLogInterval="16";
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_On();
	    dynamicjobspage.ClearCallLogInterval();
	    dynamicjobspage.EnterCallLogInterval(CallLogInterval);
	    dynamicjobspage.ClickOnCallLogOkBtn();
	    dynamicjobspage.CallLogConfirmationMessage();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Call Log is On and Clicked On Ok button",true);
	}
	
	@Test(priority='7',description="7.To Verify Dynamic Call Log Job Window when Clicked on Incoming Tab Section")
	public void DynamicCallLogJob_TC7() throws InterruptedException, Throwable{
		Reporter.log("\n7.To Verify Dynamic Call Log Job Window when Clicked on Incoming Tab Section",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.ClickOnIncomingTab();
	    dynamicjobspage.VerifyOfIncomingCallLogUpdate();
	//    dynamicjobspage.ClickOnExport();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Clicked on Incoming Tab Section",true);
	} 
	
		@Test(priority='8',description="8.To Verify Dynamic Call Log Job Window when Clicked on Outgoing Tab Section")
	public void DynamicCallLogJob_TC8() throws InterruptedException, Throwable{
		Reporter.log("\n8.To Verify Dynamic Call Log Job Window when Clicked on Outgoing Tab Section",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.ClickOnOutgoingTab();
	    dynamicjobspage.VerifyOfOutgoingCallLogUpdate();
	  //  dynamicjobspage.ClickOnExport();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Clicked on Outgoing Tab Section",true);
	}
	
	@Test(priority='9',description="9.To Verify Dynamic Call Log Job Window when Clicked on Missed Tab Section")
	public void DynamicCallLogJob_TC9() throws InterruptedException, Throwable{
		Reporter.log("\n9.To Verify Dynamic Call Log Job Window when Clicked on Missed Tab Section",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.ClickOnMissedTab();
	    dynamicjobspage.VerifyOfMissedCallLogUpdate();
	//    dynamicjobspage.ClickOnExport();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Clicked on Missed Tab Section",true);
	}
	
	@Test(priority='A',description="10.To Verify Dynamic Call Log Job Window when Clicked on Rejected Tab Section")
	public void DynamicCallLogJob_TCA() throws InterruptedException, Throwable{
		Reporter.log("\n10.To Verify Dynamic Call Log Job Window when Clicked on Rejected Tab Section",true);
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.ClickOnRejectedTab();
	    dynamicjobspage.VerifyOfRejectedCallLogUpdate();
	//    dynamicjobspage.ClickOnExport();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Call Log Job Window when Clicked on Rejected Tab Section",true);
	}
	
	@Test(priority='B',description="11.To Create Static Jobs Call Logs tracking Jobs_ON&OFF")
	public void CreatingStaticJobsCallTracking() throws InterruptedException 
	{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		dynamicjobspage.ClickOnStaticJobCallLogTrackingJob();
		dynamicjobspage.CreatingStaticJobCalllLogTracking_ON("CallLogTracking_ON");		
	    commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority='C',description="To Verify Call Log Deatils In Dynamic Job Call Tracking For First Interval")
	public void VerifyingCallDetails_AllSectionfirstInterval() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		commonmethdpage.refreshpage(); 
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnCallLog();
	    //dynamicjobspage.WaitForInterval();
		dynamicjobspage.CountingFirstIntervalCalls();
		dynamicjobspage.ClickOnCallLogCloseBtn();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_Off();		
	    dynamicjobspage.ClickOnCallLogOkBtn();
	   dynamicjobspage.CallLogConfirmationMessage();
	}
	
	@Test(priority='D',description="To Verify Dynamic Job Status And Telecom Policy  Status While Apply Static CallLog Tracking_ON Job On Device")
	public void ToVerifyDynamicJobStatusAndTelcomPolicyStatus() throws Throwable 
	{

		commonmethdpage.refreshpage(); 
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnApplyJobButton();
		dynamicjobspage.Applying_StaticCallLogTracking_OnJob("CallLogTracking_ON");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		dynamicjobspage.VerifyOfCallLogsActivityLogsDeployed();
	//	dynamicjobspage.CheckStatusOfappliedCallLogTrackingJob();
		dynamicjobspage.ClickOnMoreOption();	
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.ToVerifyDynamicJobStatus_ON();
	    dynamicjobspage.ClickOnCallLogOkBtn();
	    dynamicjobspage.ClickOnTelecomManagementEditButton();
	    dynamicjobspage.ClickOnTelecomManagementCallLogTrackingButton();
	    dynamicjobspage.ToVerifyTelecomManagementCallLogStatus_ON();
	}
	
	@Test(priority='E',description="To Verify Dynamic Call Log Tracking status When Call Log Tracking Status Turned Off In Telecom Management")
	public void VerifyDynamicCallLogTrackingStatus() throws Throwable
	{
		commonmethdpage.refreshpage(); 
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		 dynamicjobspage.ClickOnTelecomManagementEditButton();
		 dynamicjobspage.ClickOnTelecomManagementCallLogTrackingButton();
		 dynamicjobspage.Turning_OFFTelecomManagementCallLogTracking();
		 dynamicjobspage.ClickOnMoreOption();
		 dynamicjobspage.ClickOnCallLog();
		 dynamicjobspage.ToVerifyDynamicJobStatus_OFF();
		 dynamicjobspage.ClickOnCallLogCloseBtn();		 
	}
	@Test(priority='F',description="To Verify Telecom Management Call Log Tracking Status When Dynamic Call Log Tracking Is On")
	public void VerifyTelecomManagementCallLogTrackingStatus() throws Throwable
	{
		commonmethdpage.refreshpage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnCallLog();
		dynamicjobspage.SelectCallLogStatus_On();
		dynamicjobspage.ClickOnCallLogOkBtn();
		dynamicjobspage.CallLogConfirmationMessage();
		dynamicjobspage.ClickOnTelecomManagementEditButton();
		dynamicjobspage.ClickOnTelecomManagementCallLogTrackingButton();		
		dynamicjobspage.ToVerifyTelecomManagementCallLogStatus_ON();
	}
	@Test(priority='G',description="To Verify Call Log Deatils In Dynamic Job Call Tracking For Second Interval")
	public void VerifyingCallDetails_AllSectionSecondInterval() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		dynamicjobspage.WaitForInterval();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnCallLog();
		dynamicjobspage.CountingSecondIntervalCalls();
		dynamicjobspage.ToVerifyCallLogsForSecondInterval();
		dynamicjobspage.ClickOnCallLogCloseBtn();
	}
	
	

	
	@AfterMethod
	public void aftermethod(ITestResult result) throws InterruptedException, Throwable
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			Utility.captureScreenshot(driver, result.getName());
			commonmethdpage.refreshpage();
			commonmethdpage.SearchDevice();
		}
	}
}
