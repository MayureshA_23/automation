package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;

public class CustomReports_POM extends WebDriverCommonLib {
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	WebDriverWait wait;
	public String PARENTWINDOW;
	@FindBy(xpath="//span[text()='Custom Reports']")
	private WebElement ClickOnCustomReports;
	
	@FindBy(xpath="//h5[text()='Use this section to create custom report templates']")
	private WebElement SummaryCustomReports; 
	
	@FindBy(xpath="//button[text()='Create']")
	private WebElement CreateButton_CustomReports;
	
	@FindBy(xpath="//span[text()='Name']")
	private WebElement Name;
	
	@FindBy(xpath="//button[@id='custom_reports_save_btn']")
	private WebElement SaveButton_CustomReports;
	
	
	@FindBy(xpath="//li[text()='Device Name']")
	private WebElement DeviceName;
	
	@FindBy(xpath="//li[text()='Device IP Address']")
	private WebElement DeviceIPAddress;
	
	@FindBy(xpath="//li[text()='Notes']")
	private WebElement Notes;
	
	@FindBy(xpath="//li[text()='Platform']")
	private WebElement platform;
	
	@FindBy(xpath="//li[text()='Model']")
	private WebElement model;
	
	@FindBy(xpath="//li[text()='Operating System']")
	private WebElement OperatingSystem;
	
	@FindBy(xpath="//li[text()='Knox Status']")
	private WebElement KnoxStatus;
	
	@FindBy(xpath="//li[text()='OS Version']")
	private WebElement OSVersion;
	
	@FindBy(xpath="//li[text()='Group Path']")
	private WebElement GroupPath;
	
	@FindBy(id="addSelectedNodes")
	private WebElement Add;
	
	@FindBy(id="custom_report_name_input")
	private WebElement Name_CustomReport;
	
	@FindBy(id="custom_report_description_input")
	private WebElement Description_CustomReport;
	
	@FindBy(id="custom_reports_save_btn")
	private WebElement SaveButton;
	
	@FindBy(xpath="//span[text()='Customized report saved.']")
	private WebElement SuccessullMessage_CustomReportCreated;
	
	@FindBy(xpath="//span[text()='Device Details']")
	private WebElement isDeviceDetailsPresentUnderOnDemandReport;
	
	@FindBy(xpath=".//*[@id='custRpt_tableHolder']/div/div[1]/div/div[1]/i")
	private WebElement AddButtonCustomReport;
	
	@FindBy(id="generateReportBtn")
	private WebElement generateReportButton;
	
	@FindBy(xpath ="//span[contains(text(),'View Reports')]")
	private WebElement viewReportsOption;
	
	@FindBy(xpath=".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[2]")
	private WebElement viewReport;
	
	@FindBy(id="reportname")
	private WebElement ReportNameinViewReportHeader;
	
	@FindBy(xpath = "//span[@class='ReportBadge']")
	private WebElement badgeCount;
	
	@FindBy(xpath="	.//*[@id='OfflineReportGrid']/tbody/tr[1]/td[1]/p")
	private WebElement reportFirstRow;
	
	@FindBy(xpath=".//*[@id='custRpt_formHolder']/div[1]/article[2]/div[2]/div/div/div[1]/div[1]/div/select[1]")
	private WebElement AddFilterTableDropdown;
	
	@FindBy(xpath="//div[text()='Alias Name']")
	private WebElement ScrollDownToLastElement;
	
	
	@FindBy(xpath="//span[text()='On Demand Reports']")
	 private WebElement OnDemandReportOption;
	
	
	
	
	
	
	

	public void ClickOnCustomReports() throws InterruptedException
	{
		ClickOnCustomReports.click();
        waitForXpathPresent("//div[@class='actionbutton ct-menuBtn sc-open-custRpt-form']");
        sleep(2);
	}
	
	public void SummayOfCustomReports() throws InterruptedException
	{
		boolean value = SummaryCustomReports.isDisplayed();
		String pass = "PASS >> Summary of Custom Reports displayed";
		String fail = "FAIL >> summary of custom reports NOT displayed";
		ALib.AssertTrueMethod(value, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	sleep(2);
	}
	public void ClickOnCreateButton() throws InterruptedException
	{
		CreateButton_CustomReports.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	sleep(2);
	  	boolean isNameDisplayed = Name.isDisplayed();
	  	String pass = "PASS >> Name is displayed - Clicked on Create button successfully";
	  	String fail = "FAIL>> Name is not Displaye - did not click on Create Button";
	  	ALib.AssertTrueMethod(isNameDisplayed, pass, fail);
	  	
	}
	
	 public void VerifyAlltheWarningToastsOfNameDescription() throws InterruptedException
	  {
	  
		  Reporter.log("======Verify Total warning count of all Name and Desciption======");
	      List<WebElement> parameters = Initialization.driver.findElements(By.xpath("//div[1]/div/span/ul/li"));
		  //verifying total parameters
	   		
	   	  int totalWarnings = parameters.size();
	   	  int expectedWarning = 2; //at 4th position 'password' parameter is hidden.
	   	  String PassStatement2 = "PASS >> Total warning count is: "+expectedWarning+"  ";
	   	  String FailStatement2 = "FAIL >> Total warning count is Wrong";
	   	  ALib.AssertEqualsMethodInt(expectedWarning, totalWarnings, PassStatement2, FailStatement2);
	   		
	   	  //verifying warning toasts one by one
	   	  Reporter.log("========Verify warning toasts of Name and Description one by one=========");
	   	  for(int i=0;i<totalWarnings;i++){
	   	  if(i==0){	
	   		  String actual =parameters.get(i).getText();
	   		  Reporter.log(actual);
	   		  String expected = "Enter Name";
	   		  String PassStatement ="PASS >> 1st warning toast is displayed - correct";
	   		  String FailStatement ="FAIL >> 1st warning toast is NOT displayed - incorrect";
	   		  ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   		}
	   	  if(i==1){	
	   		  String actual =parameters.get(i).getText();
	   		  Reporter.log(actual);
	   		  String expected = "Enter Description";
	   		  String PassStatement ="PASS >> 2nd warning toast is displayed - correct";
	   		  String FailStatement ="FAIL >> 2nd warning toast is NOT displayed - incorrect";
	   		  ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   		}
	
	   	  }
	  }
	 public void ClickOnSaveButton_CustomReports() throws InterruptedException
	 {
		 sleep(2);
		 
		 SaveButton_CustomReports.click();
		// SaveButton_CustomReports.click();
		 //Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 sleep(4);
	 }
	 
	 public void SelectFromDeviceDetails_TableList()
	 {
		 DeviceName.click();
		 DeviceIPAddress.click();
		 Notes.click();
		 platform.click();
		 model.click();
		 OperatingSystem.click();
		 KnoxStatus.click();
		 OSVersion.click();
		 GroupPath.click();
	 }
	 
	 public void ClickOnAddButton()
	 {
		 Add.click();
	 }
	 
	 public void EnterCustomReportsNameDescription_DeviceDetails(String ReportName,String ReportDescription)
	 {
		 Name_CustomReport.sendKeys(ReportName);
		 Description_CustomReport.sendKeys(ReportDescription);
	 }
	 
	 public void SelectingRangeInDatePicker() throws InterruptedException {
			currentdate.click();
			waitForXpathPresent(
					"//div[@class='daterangepicker ltr show-ranges show-calendar opensleft drop-up']/div/ul/li[3]");
			sleep(3);
			SimpleDateFormat sdf = new SimpleDateFormat("dd");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_YEAR, -7);
			for(int i = 6; i<= 6; i++){
			    cal.add(Calendar.DAY_OF_YEAR, 1);
			    System.out.println(sdf.format(cal.getTime()));
			}
			Initialization.driver.findElement(By.xpath("(//td[text()=" + sdf.format(cal.getTime()) + "])[last()]")).click();
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd");
			Calendar cal1 = Calendar.getInstance();
			cal1.add(Calendar.DAY_OF_YEAR, 0);
			System.out.println(sdf1.format(cal1.getTime()));
			Initialization.driver.findElement(By.xpath("(//td[text()=" + sdf1.format(cal1.getTime()) + "])[last()]")).click();

		}
	 
	 @FindBy(xpath = "//li[text()='Total Physical Memory (MB)']")
		private WebElement SelectCol;

		public void ClickOnColumnInTable(String col) throws InterruptedException {
			Initialization.driver.findElement(By.xpath("//li[text()='" + col + "']")).click();
			Reporter.log("PASS >> Selected col in the table", true);
			waitForidPresent("addSelectedNodes");
		}

		public void ClickOnDataUsageCol() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("(//*[@id='tablesList_tree']/ul/li[text()='Data Usage'])[last()]"))
					.click();
			sleep(2);
		}

		@FindBy(xpath = "//table[@class='table table-no-bordered table-hover']/tbody/tr/td[1]")
		private List<WebElement> DevIpAdd;

		public void VerifyingDeviceIPAddressCol_EqualOp() {
			if (DevIpAdd.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value = false;
				String PassStatement = "PASS >> Device IP address is displayed correctly as expected only";
				String FailStatement = "Fail >> Device IP address is not displayed as expected";
				for (int i = 0; i < DevIpAdd.size(); i++) {
					String ActualValue = DevIpAdd.get(i).getText();
					System.out.println("PASS >> Device IP Addess  is:" + ActualValue + " ");

					value = ActualValue.equals(Config.IP_Address);
					if (value == false) {
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void VerifyingReport_NotEqualToOp() {
			boolean flag;
			if (Initialization.driver.findElement(By.xpath("//div[contains(text(),' No Data available.')]"))
					.isDisplayed()) {
				flag = true;
			} else {
				flag = false;
			}
			String PassSmt = "PASS >> 'No Data available' is displayed as Expected.";
			String Failsmt = "FAil >> 'No Data available' is not  displayed.";
			ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
		}

		public void VerifyStatus_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.KNOXStatus)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyNixPollingStatus_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals("FCM")) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyNetworkTypeStatus_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals("Wi-Fi")) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyGPSEnabledStatus_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals("Enabled")) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyGroupPathStatus_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.Group_Path)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyGroupPathStatus_LikeOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.GroupName_Like)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyTag_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.Tag)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyDeviceUserNameCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.DeviceUser_Name)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyBluetoothStatusCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals("On")) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyUsbStatusCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals("Plugged In")) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyBluetoothSSIDCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.BluetoothSSID)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyOSBuildNumberCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.Android_OSBuildNumber)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyOnlineStatus_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals("Online")) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyTag_LikeOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.TagName_Like)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyDeviceUserNameCol_LikeOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.DeviceUserName_Like)) {
						String PassStatement = "PASS >> Report is generated as Expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated as Expected.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifySureLockVersionCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value;
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains(Config.SureLock_version);
					if (value == true) {
						String PassStmt = "PASS >>Correct Data is displayed: " + ActualValue + " ";
						System.out.println(PassStmt);
					} else {
						String FailStmt = "Fail >> Wrong Data is displayed: " + ActualValue + " ";
						ALib.AssertFailMethod(FailStmt);
					}
				}
			}
		}
		
		public void VerifyingPhoneSignalCol_LessThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String value = col.get(i).getText();
					int num = Integer.parseInt(value);
					if (num <= 100) {
						String PassStatement = "PASS >> Report generated as expected";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report not generated as expected";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingPhoneSignalCol_GreaterThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String value = col.get(i).getText();
					int num = Integer.parseInt(value);
					if (num >= 50) {
						String PassStatement = "PASS >> Report generated as expected";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report generated as expected";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingSSIDCol_EqualOp() {
			if (DeviceLocalIPAdd.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			for (int i = 0; i < DeviceLocalIPAdd.size(); i++) {
				String DeviceBatteryPercentage = DeviceLocalIPAdd.get(i).getText();
				if (DeviceBatteryPercentage.contains(Config.SSID)) {
					String PassStatement = "PASS >> Correct data is displayed in the report.";
					System.out.println(PassStatement);
				} else {
					String FailStatement = "FAIL >> Wrong data is displayed in the report.";
					ALib.AssertFailMethod(FailStatement);
				}
			}
		}

		public void VerifyingSSIDCol_LikeOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.SSID)) {
						String PassStatement = "PASS >> Correct data is displayed in the report.";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Wrong data is displayed in the report.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingAgentVersionCol_EqualOp() {
			if (DeviceLocalIPAdd.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value;
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains(Config.AgentVersion);
					if (value == true) {
						String PassStmt = "PASS >>Correct Data is displayed: " + ActualValue + " ";
						System.out.println(PassStmt);
					} else {
						String FailStmt = "Fail >> Wrong Data is displayed: " + ActualValue + " ";
						ALib.AssertFailMethod(FailStmt);
					}
				}
			}
		}

		public void VerifyingAgentVersionCol_LessThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = DeviceLocalIPAdd.get(i).getText();
					double num = Double.parseDouble(DeviceBatteryPercentage);
					if (num <= 20.01) {
						String PassStatement = "PASS >> Correct Data is displayed:" + num + "";
						System.out.println(PassStatement);

					} else {
						String FailStatement = "FAIL >> Wrong Data is displayed: " + num + " ";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingAgentVersionCol_GreaterThanOrEqualOp() {
			if (DeviceLocalIPAdd.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					double num = Double.parseDouble(DeviceBatteryPercentage);
					if (num >= 18.59)// pass the agentversion
					{
						String PassStatement = "PASS >> Correct Data is displayed:" + num + "";
						System.out.println(PassStatement);

					} else {
						String FailStatement = "FAIL >> Wrong Data is displayed: " + num + " ";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingIMEICol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.IMEI)) {
						String PassStatement = "PASS >> IMEI is Same as expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> IMEI is not Same as expected: " + DeviceBatteryPercentage + "";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingIMEICol_LikeOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value;
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains("8698");
					if (value == true) {
						String PassMessage = "PASS >> Report is generated as Expected";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report is not generated as Expected";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyIMSICol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.IMSI_Type)) {
						String PassStatement = "PASS >> IMSI is Same as expected: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> IMSI is not Same as expected";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyIMSICol__LikeOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value;
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains("4044");
					if (value == true) {
						String PassMessage = "PASS >> Report is generated as Expected";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report is not generated as Expected";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingProtocalCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String Protocal = col.get(i).getText();
					if (Protocal.contains(Config.Protocal_Type)) {
						String PassStatement = "PASS >> Expected protocal is displayed: " + Protocal + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Expected protocal is not displayed.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}
		public void VerifyingProtocalCol_LikeOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value;
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains("Unsecure");
					if (value == true) {
						String PassMessage = "PASS >> Report is generated as Expected";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report is not generated as Expected";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingNetworkOperatorCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.equals(Config.Network_OPerator);
					System.out.println(value);
					if (value == true) {
						String PassMessage = "PASS >> Expected Network Operator is displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Expected Network Operator not is displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingOperatingSystemCol_LikeOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains("AND");
					if (value == true) {
						String PassMessage = "PASS >> Report is generated as Expected";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report is not generated as Expected";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}
		
		public void VerifyLastDeviceTimeCol() {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				String hello = ActualValue.substring(0, 10);
				System.out.println(hello);
				value = hello.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report contains last connected devices";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not contains last connected devices";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifyingPhoneSignalCol_EqualOp() {
			if (DeviceLocalIPAdd.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			for (int i = 0; i < DeviceLocalIPAdd.size(); i++) {
				String DeviceBatteryPercentage = DeviceLocalIPAdd.get(i).getText();
				int num = Integer.parseInt(DeviceBatteryPercentage);
				if (num == 0) {
					String PassStatement = "PASS >> Signal is equal to 0";
					System.out.println(PassStatement);

				} else {
					String FailStatement = "FAIL >> Signal is not equal to 0";
					ALib.AssertFailMethod(FailStatement);
				}
			}
		}
		
		@FindBy(xpath = "//select[@class='form-control ct-model-input ct-select-ele allowHtmlChar parllSelects frhEle selected_rule_option cust-selectEle ']")
		private WebElement RootStatus;
		public void SelectRootStatus(String dropdown) {
			Select var2 = new Select(RootStatus);
			var2.selectByValue(dropdown);
			Reporter.log("PASS >> Selected Operator from dropdown", true);
		}
		
		public void VerifyingMacAddressCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("70");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		
		public void ClickOnSearchReportButtonInsidedViewedReport_MTDAppScan() throws InterruptedException {
			ClickOnSearchButtonInViewedReport.sendKeys(Config.Defualt_App3);
			sleep(3);
			waitForXpathPresent("//p[contains(text(),'The APK is potentially malware')]");
			sleep(3);
			ClickOnSearchButtonInViewedReport.clear();
		}

		@FindBy(xpath = ("//p[contains(text(),'The APK is potentially malware')]"))
		private WebElement MTDAppScanInfo;
		public void CheckingMTDAppScanInfo() {
			String Info = MTDAppScanInfo.getText();
			System.out.println(">> The App Category in generated report:" + Info + " ");
			boolean flag;
			if (Info.equals(Config.Defualt_App3)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassSmt = "PASS >> MTD Custom report contain dangerous applications, threat is found on device";
			String Failsmt = "FAil >> MTD Custom report not contain dangerous applications, threat is found on device";
			ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
		}

		
		public void VerifyingDeviceLocalIPAddressCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains(Config.LocalIP_Like);
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}
		
		public void SelectLast30DaysInCustomReport() throws InterruptedException {
			CurrentdateInCustomReport.click();
			waitForXpathPresent("//li[text()='Custom Range']");
			sleep(3);
			Initialization.driver.findElement(By.xpath("(//li[text()='Last 30 Days'])[last()]")).click();
			Reporter.log("Selected the today date in Custom report", true);
		}
		
		public void VerifyingSecurityPatchDate() {
			DateFormat dateFormat = new SimpleDateFormat("yyyy");
			Date date = new Date();
			String todate = dateFormat.format(date);
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				String hello = ActualValue.substring(0, 4);
				System.out.println(hello);
				value = hello.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report contains only this year Security Patch Date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not contain only this year Security Patch Date ";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}
		public void VerifyingOperatingSystemCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals("ANDROID 10");
					if (value == true) {
						String PassMessage = "PASS >> Expected Operating System is displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Expected Operating System not is displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}
		
		@FindBy(xpath = "(//table[@class='table table-no-bordered table-hover'])/tbody/tr/td[1]")
		private List<WebElement> Platform;

		public void VerifyingDevicePlatform__EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.Platform);
					if (value == true) {
						String PassMessage = "PASS >> Expected platform is displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Expected platform not is displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}
		
		public void VerifyingDevicePlatform__LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("Win");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}
		
		public void VerifyingNetworkOperatorCol_LikeOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains("Jio");
					System.out.println(value);
					if (value == true) {
						String PassMessage = "PASS >> Expected Network Operator is displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Expected Network Operator not is displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingRootStatusCol_EqualOp() throws InterruptedException {
			if (col.size() == 0) {
				SwitchBackWindow();
				ALib.AssertFailMethod("Data is not genertaed in the Report");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = DeviceLocalIPAdd.get(i).getText();
					if (DeviceBatteryPercentage.contains(Config.RootStatus_Type)) {
						String PassStatement = "PASS >> Expected RootStatus is displayed: " + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Expected RootStatus is not displayed.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}


				public void VerifyingMacAddressCol_EqualOp() {
					boolean value;
					if (col.size() == 0) {
						ALib.AssertFailMethod("Reports Is Empty");
					} else {
						for (int i = 0; i < col.size(); i++) {
							String ActualValue = col.get(i).getText();
							value = ActualValue.equals(Config.MAC_Address);
							System.out.println(value);
							if (value == true) {
								String PassMessage = "PASS >> Expected MAC Address is displayed.";
								System.out.println(PassMessage);
							} else {
								String FailMessage = "FAIL>> Expected MAC Address not is displayed.";
								ALib.AssertFailMethod(FailMessage);
							}
						}
					}
				}




		public void VerifySureVideoVersionCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value;
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains(Config.SureVideo_version);
					if (value == true) {
						String PassStmt = "PASS >>Correct Data is displayed: " + ActualValue + " ";
						System.out.println(PassStmt);
					} else {
						String FailStmt = "Fail >> Wrong Data is displayed: " + ActualValue + " ";
						ALib.AssertFailMethod(FailStmt);
					}
				}
			}
		}

		public void VerifyVersionCol_LessThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.Col_Val1)) {
						String PassStatement = "PASS >> Correct Data is displayed:" + DeviceBatteryPercentage + "";
						System.out.println(PassStatement);
					} else {
						double num = Double.parseDouble(DeviceBatteryPercentage);
						if (num <= 5) {
							String PassStatement = "PASS >> Correct Data is displayed:" + num + "";
							System.out.println(PassStatement);
						} else {
							String FailStatement = "FAIL >> Wrong Data is displayed: " + num + " ";
							ALib.AssertFailMethod(FailStatement);
						}
					}
				}
			}

			
			 boolean value; for (int i = 0; i < col.size(); i++){ String ActualValue =
			 col.get(i).getText(); value=ActualValue.contains("496"); if(value==true) {
			 String PassMessage = "PASS >> Report is generated as Expected";
			  System.out.println(PassMessage); } else { String FailMessage =
			  "FAIL >> Report is not generated as Expected";
			  ALib.AssertFailMethod(FailMessage); } }
			 

		}

		public void VerifyVersionCol_GreaterThanOrEqualOp() {
            if (col.size() == 0) {
                    ALib.AssertFailMethod("Reports Is Empty");
            } else {
                    for (int i = 0; i < col.size(); i++) {
                            String value = col.get(i).getText();
                            long num = Long.parseLong(value);
                            if (num >= 5) {
                                    String PassStatement = "PASS >> Correct Data is displayed:" + num + "";
                                    System.out.println(PassStatement);

                            } else {
                                    String FailStatement = "FAIL >> Wrong Data is displayed: " + num + " ";
                                    ALib.AssertFailMethod(FailStatement);
                            }
                    }
            }
    }
		
		public void VerifyDefaultLauncherCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			for (int i = 0; i < col.size(); i++) {
				String DeviceBatteryPercentage = col.get(i).getText();
				if (DeviceBatteryPercentage.equals(Config.DefaultLauncher)) {
					String PassStatement = "PASS >> Report is generated as Expected.";
					System.out.println(PassStatement);
				} else {
					String FailStatement = "FAIL >> Report is not generated with wrong data.";
					ALib.AssertFailMethod(FailStatement);
				}
			}
		}

		public void VerifyDefaultLauncherCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("android");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifySerialNumberCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			for (int i = 0; i < col.size(); i++) {
				String DeviceBatteryPercentage = col.get(i).getText();
				if (DeviceBatteryPercentage.equals(Config.SerialNumber)) {
					String PassStatement = "PASS >> Report is generated as Expected.";
					System.out.println(PassStatement);
				} else {
					String FailStatement = "FAIL >> Report is not generated with wrong data.";
					ALib.AssertFailMethod(FailStatement);
				}
			}
		}

		public void VerifySerialNumberCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("RZ");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifyDevicePhoneNumberCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.Phone_Number)) {
						String PassStatement = "PASS >> Report is generated as Expected.";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated with wrong data.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyDevicePhoneNumberCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("496");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifySimSerialNumberCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.SimSerialNumber)) {
						String PassStatement = "PASS >> Report is generated as Expected.";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated with wrong data.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifySimSerialNumberCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("2477");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifyRoamingCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					if (DeviceBatteryPercentage.equals(Config.Roaming_Status)) {
						String PassStatement = "PASS >> Report is generated as Expected.";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated with wrong data.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyOSVersionCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String value = col.get(i).getText();
					int num = Integer.parseInt(value);
					if (num == 9) {
						String PassStatement = "PASS >> Report is generated as Expected.";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated with wrong data.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyOSVersionCol_LessThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					String hello = DeviceBatteryPercentage.substring(0, 1);
					double num = Double.parseDouble(hello);
					if (num <= 5) {
						String PassStatement = "PASS >> Correct Data is displayed:" + num + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Wrong Data is displayed: " + num + " ";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyOSVersionCol_GreaterThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					String hello = DeviceBatteryPercentage.substring(0, 2);
					double num = Double.parseDouble(hello);
					if (num >= 5) {
						String PassStatement = "PASS >> Correct Data is displayed:" + num + "";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Wrong Data is displayed: " + num + " ";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingDeviceIPAddressCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains(Config.IPAdd_Like);
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifyTableInSelectedTableList(String TableName) throws InterruptedException {
			String ActualTableName = Initialization.driver
					.findElement(By.xpath("//div[@id='selected_tablesList_tree']/ul/li[text()='" + TableName + "']"))
					.getText();
			System.out.println(ActualTableName);
			boolean Val = ActualTableName.equals(TableName);
			String Pass = "PASS >> TableName Is Showing Correctly In Selected Table List";
			String Fail = "FAIL >> TableName Isn't Showing Correctly In Selected Table List";
			ALib.AssertTrueMethod(Val, Pass, Fail);
			sleep(2);
		}
		
		String originalHandle = Initialization.driver.getWindowHandle();
		public void SwitchBackWindow() throws InterruptedException {

			Initialization.driver.close();
			Initialization.driver.switchTo().window(originalHandle);
			sleep(10);
		}
		
		@FindBy(xpath = "//span[@class='tableDevCount_line']")
		private WebElement DeviceCountInConsole;

		@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/button")
		private WebElement allbuttonselectInConsole;
		
		@FindBy(xpath = "//a[text()='20']")
		private WebElement Select20;
		
		@FindBy(xpath = "//a[text()='50']")
		private WebElement Select50;

		@FindBy(xpath = "//a[text()='500']")
		private WebElement Select500;
		public void ChooseDevicesPerPageInConsole() throws InterruptedException {
			String DeviceCoutInConsole = DeviceCountInConsole.getText();
			System.out.println(DeviceCoutInConsole);
			String[] arrSplit_3 = DeviceCoutInConsole.split("\\s");
			String value1 = null;
			for (int i = 0; i < 1; i++) {
				value1 = arrSplit_3[i];
			}
			int i = Integer.parseInt(value1);
			System.out.println(">> Total number of devices being shown when 100 per page is selected:" + i + " ");

			if (i >= 1 && i < 11) {
				System.out.println("count is less than 10, so that there is no range selection option");
			} else if (i > 10 && i < 21) {
				allbuttonselectInConsole.click();
				sleep(4);
				Select20.click();

			} else if (i > 20 && i < 51) {
				allbuttonselectInConsole.click();
				sleep(4);
				Select50.click();
			} else if (i > 50 && i < 101) {
				allbuttonselectInConsole.click();
				sleep(4);
				Select100.click();
			} else {
				allbuttonselectInConsole.click();
				sleep(4);
				allbutton.click();
			}
		}
		
		@FindBy(xpath = ("//button[text()='Request Report']"))
		private WebElement ClickRequestReport;
		public void ClickRequestReportButton() throws InterruptedException {
			sleep(3);
			ClickRequestReport.click();
			Reporter.log("PASS >> Requested for custom report", true);
			waitForXpathPresent("//span[contains(text(),'The request to generate this report has been added to the queue.')]");
			sleep(3);
		}
		
		public void VarifyingOneWeekDateInAppDataUsageViewReport() throws InterruptedException {
			{
				List<WebElement> ls = Initialization.driver
						.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[6]"));
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();

				String day1 = dateFormat.format(date);
				Calendar cal = Calendar.getInstance();

				cal.add(Calendar.DATE, -1);
				Date todate6 = cal.getTime();
				String day2 = dateFormat.format(todate6);

				cal.add(Calendar.DATE, -1);
				Date todate5 = cal.getTime();
				String day3 = dateFormat.format(todate5);

				cal.add(Calendar.DATE, -1);
				Date todate4 = cal.getTime();
				String day4 = dateFormat.format(todate4);

				cal.add(Calendar.DATE, -1);
				Date todate3 = cal.getTime();
				String day5 = dateFormat.format(todate3);

				cal.add(Calendar.DATE, -1);
				Date todate2 = cal.getTime();
				String day6 = dateFormat.format(todate2);

				cal.add(Calendar.DATE, -1);
				Date todate1 = cal.getTime();
				String day7 = dateFormat.format(todate1);
				boolean value;
				for (int i = 0; i < ls.size(); i++) {
					String ActualValue = ls.get(i).getText();
					String datedate = ActualValue.substring(0, 10);
					value = datedate.equals(day1) || datedate.equals(day2) || datedate.equals(day3) || datedate.equals(day4)
							|| datedate.equals(day5) || datedate.equals(day6) || datedate.equals(day7);
					if (value) {
						String PassMessage = "PASS >> Report get generated successfully for One Week Date";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report not generated successfully for One Week Date";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}
		
		@FindBy(xpath = ("(//select[@class='form-control ct-model-input ct-select-ele parllSelects'])[1]"))
		private WebElement SortByDropDown;
		public void Selectvaluefromsortbydropdown(String value) {
			Select var = new Select(SortByDropDown);
			var.selectByValue(value);
		}
		
		@FindBy(xpath = "//select[@class='form-control ct-model-input ct-select-ele parllSelects thrEle whereOptr allowHtmlChar']")
		private WebElement OperatorDropDown;
		public void SelectOperatorFromDropDown(String dropdown) {
			Select var2 = new Select(OperatorDropDown);
			var2.selectByValue(dropdown);
			Reporter.log("Selected Operator from dropdown", true);
		}
		
		@FindBy(xpath = ("//select[@id='all_cols_seltd_tables_to_group']"))
		private WebElement ValueFromGroupByDropDown;
		public void SelectValueFromGroupByDropDown() {
			Select variable = new Select(ValueFromGroupByDropDown);
			variable.deselectByVisibleText("Device Name");
		}
		
		public void SelectvaluefromGroupByDropDown(String value) {
			Select var = new Select(GroupByDropDown);
			var.selectByValue(value);
			Reporter.log("Selected value in the GroupBy DropDown", true);
		}
		
		@FindBy(xpath = ("(//li[text()='Last 1 Week'])[last()]"))
		private WebElement OneWeekDateInCustomReport;
		
		@FindBy(xpath = "//input[@class='form-control ct-model-input parllSelects frhEle selected_rule_date sc-date-input cust-datepicker']")
		private WebElement currentdate;
		public void SelectOneWeekDateInCustomReport() throws InterruptedException {
			currentdate.click();
			waitForXpathPresent(
					"//div[@class='daterangepicker ltr show-ranges show-calendar opensleft drop-up']/div/ul/li[3]");
			sleep(3);
			OneWeekDateInCustomReport.click();
			Reporter.log("Clicked on one week Date", true);
		}
		
		public void VerifyingWifiDataUsageColumn() {
			List<WebElement> WiFiCol = Initialization.driver
					.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[5]"));
			if (WiFiCol.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			for (int i = 0; i < WiFiCol.size(); i++) {
				String DeviceBatteryPercentage = WiFiCol.get(i).getText();
				int num = Integer.parseInt(DeviceBatteryPercentage);
				if (num >= 500) {
					String PassStatement = "PASS >> Data Is Showing Correctly.";
					System.out.println(PassStatement);
				} else {
					String FailStatement = "FAIL >> Data Isn't Showing Correctly.";
					ALib.AssertFailMethod(FailStatement);
				}
			}
		}

		
		public void VerifyingMobileDataUsageColumn() {
            List<WebElement> MobDataCol = Initialization.driver
                            .findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[4]"));
            if (MobDataCol.size() == 0) {
                    ALib.AssertFailMethod("Reports Is Empty");
            }
            for (int i = 0; i < MobDataCol.size(); i++) {
                    String value = MobDataCol.get(i).getText();
                    long num = Long.parseLong(value);
                    if (num >= 500) {
                            String PassStatement = "PASS >> Data Is Showing Correctly.";
                            System.out.println(PassStatement);
                    } else {
                            String FailStatement = "FAIL >> Data Isn't Showing Correctly.";
                            ALib.AssertFailMethod(FailStatement);
                    }
            }
    }
		
		@FindBy(xpath = ("(//input[@class='form-control ct-model-input parllSelects frhEle group_by_aggr_alias_text'])[1]"))
		private WebElement AliasName;
		public void SendingAliasName(String value) throws InterruptedException {
			AliasName.sendKeys(value);
			Reporter.log("Provided AliasName For Device Name", true);
			sleep(2);
		}
		
		@FindBy(xpath = ("//select[@class='form-control ct-model-input ct-select-ele groupColName']"))
		private WebElement GroupByDropDown;

		@FindBy(xpath = ("(//select[@class='form-control ct-model-input ct-select-ele groupColAggr'])[1]"))
		private WebElement AggregateOptionsDropDown;
		public void SelectvaluefromAggregateOptionsDropDown(String value) {
			Select var = new Select(AggregateOptionsDropDown);
			var.selectByValue(value);
			Reporter.log("PSelected value in the Aggregate Options DropDown", true);
		}
		
		@FindBy(xpath = ("(//select[@class='form-control ct-model-input ct-select-ele parllSelects'])[2]"))
		private WebElement SortByDropDownforOrder;
		public void SelectvaluefromsortbydropdownForOrder(String value) {
			Select var = new Select(SortByDropDownforOrder);
			var.selectByValue(value);
		}
		
		@FindBy(xpath = "(//select[@class='form-control ct-model-input ct-select-ele parllSelects secEle selected_tables_cols_names'])[last()]")
		private WebElement ColumnNAmeDropDown;
		public void SelectValueFromColumn(String Value) {
			Select var1 = new Select(ColumnNAmeDropDown);
			var1.selectByValue(Value);
			Reporter.log("Selected Value from column", true);
		}

		@FindBy(xpath = ("(//li[text()='Yesterday'])[last()]"))
		private WebElement yesterdaydateInOndemand;
		public void SelectYesterdayDateInOndemand() throws InterruptedException {
			currentdateInondemand.click();
			sleep(1);
			yesterdaydateInOndemand.click();
			Reporter.log("Selected the yesterday date in ondemand report", true);
		}

		@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[3]")
		private WebElement ApplicationNameColumnAppDataUsageReport;

		public void VerifyAppNameInAppDataUsageReport(String AppName) {
			String ReportAppName = ApplicationNameColumnAppDataUsageReport.getText();
			boolean Val = ReportAppName.equals(AppName);
			String Pass = "Application Name Is Showing Correctly";
			String Fail = "Application Name Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		public void VarifyingYestrDate_APPDataUsage() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[6]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate7 = dateFormat.format(date);
			System.out.println(todate7);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date Yesterday = cal.getTime();
			String fromdate = dateFormat.format(Yesterday);
			System.out.println(fromdate);
			System.out.println("Actual yesterdayDate:" + fromdate + " ");

			boolean flag;
			if (ls.size() == 0) {
				ALib.AssertFailMethod("No Data Present In Report");
			} else {
				for (int i = 1; i < ls.size(); i++) {
					String ActualValue = ls.get(i).getText();
					String datedate = ActualValue.substring(0, 10);
					System.out.println(datedate);
					flag = datedate.equals(fromdate);
					System.out.println(flag + " :Yesterday date is matched");
				}
			}

		}

		public void VarifyingCurrentDate_APPDataUsage() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[6]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				String todate = dateFormat.format(date);
				boolean value;
				for (int i = 0; i < ls.size(); i++) {
					String ActualValue = ls.get(i).getText();
					String hello = ActualValue.substring(0, 10);
					value = hello.equals(todate);
					if (value == true) {
						String PassMessage = "PASS >> Report get generated successfully for current date";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report not generated successfully for current date";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}
		
		@FindBy(xpath = ("(//li[text()='Today'])[last()]"))
		private WebElement TodaydateInOndemand;
		public void SelectTodayDateInOndemand() throws InterruptedException {
			currentdateInondemand.click();
			sleep(2);
			TodaydateInOndemand.click();
			Reporter.log("Selected the today's date in ondemand report", true);
		}
		
		@FindBy(xpath = "(//input[@class='form-control ct-model-input parllSelects frhEle selected_rule_val cust-textEle'])[last()]")
		private WebElement SendValue;
		public void SendValueToTextfield(String Version) throws InterruptedException {
			SendValue.sendKeys(Version);
			waitForidPresent("custom_reports_save_btn");
			sleep(2);
		}
		
		@FindBy(xpath = "(//select[@class='form-control ct-model-input ct-select-ele parllSelects fstEle selected_tables_names'])[last()]")
		private WebElement TableNameDropDown;
		public void Selectvaluefromdropdown(String device) {
			Select var = new Select(TableNameDropDown);
			var.selectByValue(device);
			Reporter.log("Selected Value from dropdown", true);
		}

		@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[2]")
		private WebElement PackageColumnInAppDataUsageReport;
		public void VerifyPackageNameInAppDataUsageReport(String PackageName) {
			String ReportPackageName = PackageColumnInAppDataUsageReport.getText();
			boolean Val = ReportPackageName.equals(PackageName);
			String Pass = "Application Package Name Is Showing Correctly";
			String Fail = "Application Package Name Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		public void VerifyingGeneratedReportInOndemandSection(String reportname) throws InterruptedException {
			Initialization.driver.findElement(By.xpath("(//table[@id='reportsGrid']/tbody/tr/td/div/span[1])[last()]"))
					.click();
			boolean OptionPresent = Initialization.driver.findElement(By.xpath("//span[text()='" + reportname + "']"))
					.isDisplayed();
			String pass = "PASS >> Report is generated and listed in OnDemand Reports section";
			String fail = "FAIL >> Report not generated and not listed in OnDemand Reports section";
			ALib.AssertTrueMethod(OptionPresent, pass, fail);
			sleep(2);
			SearchOnDemandReports.clear();
		}
		
		@FindBy(xpath = ("(//input[@id='date'])[1]"))
		private WebElement currentdateInondemand;
		public void SelectOneWeekDateInOndemandReport() throws InterruptedException {
			currentdateInondemand.click();
			sleep(3);
			OneWeekDateInOnDemand.click();
			waitForXpathPresent("//button[text()='Request Report']");
		}

		@FindBy(xpath = ("(//li[text()='Last 1 Week'])[last()]"))
		private WebElement OneWeekDateInOnDemand;
		
		@FindBy(xpath = ".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[text()='View']")
		private WebElement viewReport1;
		
		public void ClickOnRefreshInViewReport() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("//section[@id='report_viewSect']/div/div[2]/div/div/button"))
					.click();
			waitForXpathPresent("//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[2]");
			sleep(4);
		}
		
		@FindBy(xpath = "//span[text()='View Reports']")
		private WebElement ViewReportsButton;
		public void ClickOnViewReportButton() throws InterruptedException {
			ViewReportsButton.click();
			waitForXpathPresent("//div[@id='deleteReport']");
			sleep(7);
		}
		
		@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[6]")
		private List<WebElement> DurationColumnInSureFoxAnlayticsRep;

		public void CheckingDurationColDateFormat_InSureFoxAnalytics() throws InterruptedException {
			if (DurationColumnInSureFoxAnlayticsRep.size() == 0) {
				SwitchBackWindow();
				ALib.AssertFailMethod("Data Not Generated In Report");
			} else {
				LocalDateTime myDateObj = LocalDateTime.now();
				DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("HH:mm:ss");
				String formattedDate = myDateObj.format(myFormatObj);
				System.out.println(formattedDate);
				for (int i = 0; i < DurationColumnInSureFoxAnlayticsRep.size(); i++) {
					String ActualValue = DurationColumnInSureFoxAnlayticsRep.get(i).getText();
					if (formattedDate.compareTo(ActualValue) >= 0 || formattedDate.compareTo(ActualValue) <= 0) {
						Reporter.log("PASS >> Report get generated successfully in the expected Time Format", true);
					} else {
						ALib.AssertFailMethod("FAIL >> Report not get generated successfully in the expected Time Format");
					}
				}
			}
		}
		
		@FindBy(xpath = "//li[@class='list-group-item node-selected_tablesList_tree']")
		private List<WebElement> Tablecols;

		public void VerifyColumnsInSureLockCheckListTable() {
			boolean OptionPresent2 = Initialization.driver.findElement(By.xpath("//li[text()='SureLock CheckList']"))
					.isDisplayed();
			String pass2 = "PASS >> SureLock CheckList Option is displayed";
			String fail2 = "FAIL >> SureLock CheckList is not displayed";
			ALib.AssertTrueMethod(OptionPresent2, pass2, fail2);

			boolean OptionPresent3 = Initialization.driver.findElement(By.xpath("//li[text()='SureLock Default Launcher']"))
					.isDisplayed();
			String pass3 = "PASS >> SureLock Default Launcher Option is displayed";
			String fail3 = "FAIL >> SureLock Default Launcher Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent3, pass3, fail3);

			boolean OptionPresent4 = Initialization.driver
					.findElement(By.xpath("(//li[text()='SureLock Device Administrator'])[2]")).isDisplayed();
			String pass4 = "PASS >> SureLock Device Administrator Option is displayed";
			String fail4 = "FAIL >> SureLock Device Administrator Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent4, pass4, fail4);

			boolean OptionPresent5 = Initialization.driver.findElement(By.xpath("//li[text()='Samsung KNOX']"))
					.isDisplayed();
			String pass5 = "PASS >> Samsung KNOX Option is displayed";
			String fail5 = "FAIL >> Samsung KNOX Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent5, pass5, fail5);

			boolean OptionPresent6 = Initialization.driver
					.findElement(By.xpath("(//li[text()='Apps With Usage Access'])[2]")).isDisplayed();
			String pass6 = "PASS >> Apps With Usage Access Option is displayed";
			String fail6 = "FAIL >> Apps With Usage Access Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent6, pass6, fail6);

			boolean OptionPresent7 = Initialization.driver
					.findElement(By.xpath("(//li[text()='Disable USB Debugging'])[2]")).isDisplayed();
			String pass7 = "PASS >> Disable USB Debugging Option is displayed";
			String fail7 = "FAIL >> Disable USB Debugging Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent7, pass7, fail7);

			boolean OptionPresent8 = Initialization.driver.findElement(By.xpath("//li[text()='Storage Permission']"))
					.isDisplayed();
			String pass8 = "PASS >> Storage Permission Option is displayed";
			String fail8 = "FAIL >> Storage Permission Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent8, pass8, fail8);

			boolean OptionPresent9 = Initialization.driver.findElement(By.xpath("//li[text()='Contacts Permission']"))
					.isDisplayed();
			String pass9 = "PASS >> Contacts Permission Option is displayed";
			String fail9 = "FAIL >> Contacts Permission Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent9, pass9, fail9);

			boolean OptionPresent10 = Initialization.driver.findElement(By.xpath("//li[text()='Location Permission']"))
					.isDisplayed();
			String pass10 = "PASS >> Location Permission Code Option is displayed";
			String fail10 = "FAIL >> Location Permission Code Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent10, pass10, fail10);

			boolean OptionPresent11 = Initialization.driver.findElement(By.xpath("//li[text()='Camera Permission']"))
					.isDisplayed();
			String pass11 = "PASS >> Camera Permission Option is displayed";
			String fail11 = "FAIL >> Camera Permission Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent11, pass11, fail11);

			boolean OptionPresent12 = Initialization.driver.findElement(By.xpath("//li[text()='Telephone Permission']"))
					.isDisplayed();
			String pass12 = "PASS >> Telephone Permission Option is displayed";
			String fail12 = "FAIL >> Telephone Permission Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent12, pass12, fail12);

			boolean OptionPresent13 = Initialization.driver.findElement(By.xpath("//li[text()='SMS Permission']"))
					.isDisplayed();
			String pass13 = "PASS >> SMS Permission Option is displayed";
			String fail13 = "FAIL >> SMS Permission Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent13, pass13, fail13);

			boolean OptionPresent14 = Initialization.driver
					.findElement(By.xpath("//li[text()='Configure System Permission']")).isDisplayed();
			String pass14 = "PASS >> Configure System Permission Option is displayed";
			String fail14 = "FAIL >> Configure System Permission Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent14, pass14, fail14);

			boolean OptionPresent15 = Initialization.driver
					.findElement(By.xpath("(//li[text()='Display Over other Apps'])[2]")).isDisplayed();
			String pass15 = "PASS >> Display Over other Apps Option is displayed";
			String fail15 = "FAIL >> Display Over other Apps Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent15, pass15, fail15);

		}

		@FindBy(xpath = ("//p[contains(text(),'+919742348717')]"))
		private WebElement ContactNum;
		public void CheckingContactNumber() {
			String report = ContactNum.getText();
			System.out.println(report);
			boolean flag;
			if (report.contains(Config.EnterContactNumber)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassSmt = "PASS >> Report containing only perticular number SMS Log";
			String Failsmt = "FAil >> Report not containing only perticular number SMS Log";
			ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
		}
		
		public void VarifyingOneWeekDateInSmsLogReport() {
			// allbutton.click();
			// allbuttonselect.click();

			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[6]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();

			String day1 = dateFormat.format(date);
			System.out.println(day1);

			Calendar cal = Calendar.getInstance();

			cal.add(Calendar.DATE, -1);
			Date todate6 = cal.getTime();
			String day2 = dateFormat.format(todate6);
			System.out.println(day2);

			cal.add(Calendar.DATE, -1);
			Date todate5 = cal.getTime();
			String day3 = dateFormat.format(todate5);
			System.out.println(day3);

			cal.add(Calendar.DATE, -1);
			Date todate4 = cal.getTime();
			String day4 = dateFormat.format(todate4);
			System.out.println(day4);

			cal.add(Calendar.DATE, -1);
			Date todate3 = cal.getTime();
			String day5 = dateFormat.format(todate3);
			System.out.println(day5);

			cal.add(Calendar.DATE, -1);
			Date todate2 = cal.getTime();
			String day6 = dateFormat.format(todate2);
			System.out.println(day6);

			cal.add(Calendar.DATE, -1);
			Date todate1 = cal.getTime();
			String day7 = dateFormat.format(todate1);
			System.out.println(day7);
			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String datedate = ActualValue.substring(0, 10);
				System.out.println(datedate);
				value = datedate.equals(day1) || datedate.equals(day2) || datedate.equals(day3) || datedate.equals(day4)
						|| datedate.equals(day5) || datedate.equals(day6) || datedate.equals(day7);
				if (value) {
					String PassMessage = "PASS >> Report get generated successfully for One Week Date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for One Week Date";
					ALib.AssertFailMethod(FailMessage);
				}
			}

		}

		public void VerifyingAppName(String app) {
			String ReportPackageName = Initialization.driver.findElement(By.xpath("//p[contains(text(),'" + app + "')]"))
					.getText();
			boolean Val = ReportPackageName.equals(app);
			String Pass = "Application Showing Correctly.";
			String Fail = "Application Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		public void CheckingApplicationNameColumn() {
			List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[3]"));
			boolean flag;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				if (ActualValue.equals(Config.Application_Name)) {
					flag = true;
				} else {
					flag = false;
				}
				String PassStmt = "PASS >> Data Showing correctly.";
				String FailStmt = "Fail >> Data Isn't Showing correctly.";
				ALib.AssertTrueMethod(flag, PassStmt, FailStmt);
			}
		}
		
		public void VarifyingAppVersionColumn() {
            List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[5]"));
            boolean flag;
            for (int i = 0; i < ls.size(); i++) {
                    String ActualValue = ls.get(i).getText();
                    String value = ActualValue.substring(0, 1);
                    long version = Long.parseLong(value);
                    if (version >= Config.App_Version) {
                            flag = true;
                    } else {
                            flag = false;
                    }
                    String PassStmt = "PASS >>The report contains the details of Application which is having Version as >= 2.0";
                    String FailStmt = "Fail >> The report not containing the details of Application which is not having Version as >= 2.0";
                    ALib.AssertTrueMethod(flag, PassStmt, FailStmt);
            }
    }
		
		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr/td[4]"))
		private List<WebElement> ApplicationType;
		public void VerifyOfInstalledAppStatusColumn() {
			boolean value = false;
			String PassStatement = "PASS >> The report contains the details of Installed Application only";
			String FailStatement = "Fail >> The repor doesn't contains the details of Installed Application";
			for (int i = 0; i < ApplicationType.size(); i++) {
				String ActualValue = ApplicationType.get(i).getText();
				System.out.println("Application type is:" + ActualValue + " ");

				value = ActualValue.equals("Installed");
				if (value == false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VarifyingLongitudeValueInDeviceLocationReport() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[3]"));
			for (int i = 0; i < ls.size(); i++) {
				String Actuallogitudeactual = ls.get(i).getText();
				float logitudeactual = Float.parseFloat(Actuallogitudeactual);
				System.out.println(logitudeactual);

				boolean flag;
				if (logitudeactual > (Config.longitudeValue)) {
					flag = true;
				} else {
					flag = false;
				}
				String PassStatement = "PASS >> logitude value greater than the expected value";
				String FailStatement = "FAIL >> logitude value is not greater than the expected value ";
				ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
			}
		}
		
		public void VarifyingAddressInDeviceLocationReport() {
			List<WebElement> ls = Initialization.driver.findElements(By.xpath("//p[contains(text(),'AMR Tech Park')]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			for (int i = 0; i < ls.size(); i++) {
				String Actualaddress = ls.get(i).getText();
				boolean flag;
				if (Actualaddress.contains(Config.ExpectedDeviceAddress)) {
					flag = true;
				} else {
					flag = false;
				}
				String PassStatement = "PASS >> Report containing the specified address";
				String FailStatement = "FAIL >> Report not containing the specified address ";
				ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
			}
		}
		
		public void VarifyingOneWeekDateInDataUsageViewReport() {
			{
				List<WebElement> ls = Initialization.driver
						.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[4]"));
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();

				String day1 = dateFormat.format(date);
				// System.out.println(day1);

				Calendar cal = Calendar.getInstance();

				cal.add(Calendar.DATE, -1);
				Date todate6 = cal.getTime();
				String day2 = dateFormat.format(todate6);
				// System.out.println(day2);

				cal.add(Calendar.DATE, -1);
				Date todate5 = cal.getTime();
				String day3 = dateFormat.format(todate5);
				// System.out.println(day3);

				cal.add(Calendar.DATE, -1);
				Date todate4 = cal.getTime();
				String day4 = dateFormat.format(todate4);
				// System.out.println(day4);

				cal.add(Calendar.DATE, -1);
				Date todate3 = cal.getTime();
				String day5 = dateFormat.format(todate3);
				// System.out.println(day5);

				cal.add(Calendar.DATE, -1);
				Date todate2 = cal.getTime();
				String day6 = dateFormat.format(todate2);
				// System.out.println(day6);

				cal.add(Calendar.DATE, -1);
				Date todate1 = cal.getTime();
				String day7 = dateFormat.format(todate1);
				// System.out.println(day7);
				if (ls.size() == 0) {
					ALib.AssertFailMethod("Reports Is Empty");
				}
				boolean value;
				for (int i = 0; i < ls.size(); i++) {
					String ActualValue = ls.get(i).getText();
					String datedate = ActualValue.substring(0, 10);
					System.out.println(datedate);
					value = datedate.equals(day1) || datedate.equals(day2) || datedate.equals(day3) || datedate.equals(day4)
							|| datedate.equals(day5) || datedate.equals(day6) || datedate.equals(day7);
					if (value) {
						String PassMessage = "PASS >> Report get generated successfully for One Week Date";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report not generated successfully for One Week Date";
						ALib.AssertFailMethod(FailMessage);
					}
				}

			}
		}

		public void ClickOnSearchReportButtonInsidedViewedReport1() throws InterruptedException {
			ClickOnSearchButtonInViewedReport.sendKeys(Config.Device_Name);
			ClickOnSearchButtonInViewedReport.clear();
		}
		
		public void VerifyingDeviceModel_LikeOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains(Config.Model);
					System.out.println(value);
					if (value == true) {
						String PassMessage = "PASS >> Report is generated as Expected";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not generated as Expected";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingDeviceTimeZoneCol_LikeOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains(Config.DeviceTimeZone_Like);
					System.out.println(value);
					if (value == true) {
						String PassMessage = "PASS >> Report is generated as Expected";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not generated as Expected";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingAndroidEnterpriseStatus_LikeOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains("Enrolled");
					System.out.println(value);
					if (value == true) {
						String PassMessage = "PASS >> Report is generated as Expected";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not generated as Expected";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		@FindBy(xpath = "(//table[@class='table table-no-bordered table-hover'])/tbody/tr/td[1]")
		private List<WebElement> col;

		public void VerifyingDeviceModel_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.Model_Name);
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingDeviceTimeZoneCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.Device_Time_Zone);
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingDeviceAndroidIDCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.AndroidID);
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingFOTARegistrationStatusCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.FOTARegistration_Status);
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingFirmwareVersionCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.Firmware_Version);
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingDeviceHashCodeCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.HashCode);
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingAndroidEnterpriseStatus_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals(Config.AndroidEnterpriseStatus);
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingWiFiHotspotStatus_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals("ON");
					if (value == true) {
						String PassMessage = "PASS >> Report is displayed as expected displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Report is not displayed as expected displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingRegisteredDeviceCol() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[1]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			System.out.println(todate);
			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String hello = ActualValue.substring(0, 10);
				System.out.println(hello);
				value = hello.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report contains only today Registed devices devices";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not contains today Registed devices devices";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifyBatteryPercentageCol_EqualOp() throws InterruptedException {
			if (DevIpAdd.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value = false;
				String PassStatement = "PASS >> BatteryPercentage is displayed correctly as expected only";
				String FailStatement = "Fail >> BatteryPercentage is not displayed.";
				for (int i = 0; i < DevIpAdd.size(); i++) {
					String ActualValue = DevIpAdd.get(i).getText();
					System.out.println("PASS >> BatteryPercentage  is:" + ActualValue + " ");

					value = ActualValue.equals("50");
					if (value == false) {
						ALib.AssertTrueMethod(value, PassStatement, FailStatement);
					}
				}
				ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			}
		}

		public void VerifyBatteryPercentageCol_GreaterThanOrEqualOp() throws InterruptedException {
			if (DeviceLocalIPAdd.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			for (int i = 0; i < DeviceLocalIPAdd.size(); i++) {
				String DeviceBatteryPercentage = DeviceLocalIPAdd.get(i).getText();
				int num = Integer.parseInt(DeviceBatteryPercentage);
				if (num >= 5) {
					String PassStatement = "PASS >> BatteryPercentage of devices is greater than 5%";
					System.out.println(PassStatement);
				} else {
					String FailStatement = "FAIL >> BatteryPercentage of devices is not greater than 5%";
					ALib.AssertFailMethod(FailStatement);
				}
			}
		}

		public void VerifyDataUageCol_GreaterThanOrEqualOp() throws InterruptedException {
            if (DeviceLocalIPAdd.size() == 0) {
                    ALib.AssertFailMethod("Reports Is Empty");
            } else {
                    for (int i = 0; i < col.size(); i++) {
                            String Data = DeviceLocalIPAdd.get(i).getText();
                            String val = Data.substring(0, 2);
                            long data = Long.parseLong(val);
                            if (data >= 5) {
                                    String PassStatement = "PASS >> DataUsage of devices is greater than 5%";
                                    System.out.println(PassStatement);
                            } else {
                                    String FailStatement = "FAIL >> DataUsage of devices is not greater than 5%";
                                    ALib.AssertFailMethod(FailStatement);
                            }
                    }
            }
    }
		
		public void VerifyingAvailablePhysicalMemoryColumn() {
            if (DeviceLocalIPAdd.size() == 0) {
                    ALib.AssertFailMethod("Reports Is Empty");
            }
            for (int i = 0; i < DeviceLocalIPAdd.size(); i++) {
                    String Data = DeviceLocalIPAdd.get(i).getText();
                    long num = Long.parseLong(Data);
                    if (num >= 1024) {
                            String PassStatement = "PASS >> Available Physical Memory is greater than 1024MB";
                            System.out.println(PassStatement);
                    } else {
                            String FailStatement = "FAIL >> Available Physical Memory is greater not than 1024MB";
                            ALib.AssertFailMethod(FailStatement);
                    }
            }
    }
		public void VerifyBatteryPercentageCol_LessThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					int num = Integer.parseInt(DeviceBatteryPercentage);
					if (num <= 100) {
						String PassStatement = "PASS >> BatteryPercentage of the devices is lesser than 100%";
						System.out.println(PassStatement);

					} else {
						String FailStatement = "FAIL >> BatteryPercentage of the devices is not lesser than 100%";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyDataUsageCol_LessThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					String val = DeviceBatteryPercentage.substring(0, 1);
					int num = Integer.parseInt(val);
					if (num <= 100) {
						String PassStatement = "PASS >> Report is generated as Expected.";
						System.out.println(PassStatement);
					} else {
						String FailStatement = "FAIL >> Report is not generated with wrong data.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

		public void VerifyingBatteryState_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals("Charging");
					if (value == true) {
						String PassMessage = "PASS >> BatteryState is displayed as Expected.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> BatteryState State not is displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}

		public void VerifyingBatteryState__LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("Nor");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected.";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated with wrong data.";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifyingBatteryChemistryCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println(ActualValue);
					value = ActualValue.equals("Li-ion");
					if (value == true) {
						String PassMessage = "PASS >> Expected BatteryChemistry is displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Expected BatteryChemistry not is displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}

		}

		public void VerifyingBatteryChemistryCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("Li");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected.";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated with wrong data.";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VerifyingReportForUsage_EqualOp() {
            if (col.size() == 0) {
                    ALib.AssertFailMethod("Reports Is Empty");
            } else {
                    for (int i = 0; i < col.size(); i++) {
                            String DeviceBatteryPercentage = col.get(i).getText();
                            String val = DeviceBatteryPercentage.substring(0, 1);
                            long num = Long.parseLong(val);
                            if (num == 0) {
                                    String PassStatement = "PASS >> Report is generated as Expected.";
                                    System.out.println(PassStatement);
                            } else {
                                    String FailStatement = "FAIL >> Report is not generated with wrong data.";
                                    ALib.AssertFailMethod(FailStatement);
                            }
                    }
            }
    }
		

		public void VerifyingReport_LessThanOrEqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String DeviceBatteryPercentage = col.get(i).getText();
					double num = Integer.parseInt(DeviceBatteryPercentage);
					if (num <= 50000) {
						String PassStatement = "PASS >> Report is generated as Expected.";
						System.out.println(PassStatement);

					} else {
						String FailStatement = "FAIL >> Report is not generated with wrong data.";
						ALib.AssertFailMethod(FailStatement);
					}
				}
			}
		}

	
		public void ClickOnSearchReportButtonInsidedViewedReport(String value) throws InterruptedException {
			ClickOnSearchButtonInViewedReport.sendKeys(value);
			sleep(3);
			ClickOnSearchButtonInViewedReport.clear();
			sleep(2);
		}
		
		public void VerifyingReport_GreaterThanOrEqualOp() {
            if (col.size() == 0) {
                    ALib.AssertFailMethod("Reports Is Empty");
            } else {
                    for (int i = 0; i < col.size(); i++) {
                            String Data = DeviceLocalIPAdd.get(i).getText();
                            long num = Long.parseLong(Data);
                            if (num >= 500) {
                                    String PassStatement = "PASS >> Report is generated as Expected.";
                                    System.out.println(PassStatement);

                            } else {
                                    String FailStatement = "FAIL >> Report is not generated with wrong data.";
                                    ALib.AssertFailMethod(FailStatement);
                            }
                    }
            }
    }
		
		public void VerifySureFoxVersionCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				boolean value;
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					value = ActualValue.contains(Config.SureFox_version);
					if (value == true) {
						String PassStmt = "PASS >>Correct Data is displayed: " + ActualValue + " ";
						System.out.println(PassStmt);
					} else {
						String FailStmt = "Fail >> Wrong Data is displayed: " + ActualValue + " ";
						ALib.AssertFailMethod(FailStmt);
					}
				}
			}
		}
		
		public void VarifyingYestDateInDataUsageRep() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[4]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate7 = dateFormat.format(date);
			System.out.println(todate7);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date Yesterday = cal.getTime();
			String fromdate = dateFormat.format(Yesterday);
			System.out.println(fromdate);
			System.out.println("Actual yesterdayDate:" + fromdate + " ");

			boolean flag;
			if (ls.size() == 0) {
				ALib.AssertFailMethod("No Data Present In Report");
			} else {
				for (int i = 1; i < ls.size(); i++) {
					String ActualValue = ls.get(i).getText();
					String datedate = ActualValue.substring(0, 10);
					System.out.println(datedate);
					flag = datedate.equals(fromdate);
					System.out.println(flag + " :Yesterday date is matched");
				}
			}
		}

		public void VerifyingMobileDataUsageInDataUsageRep() {
            List<WebElement> deviceCount = Initialization.driver
                            .findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
            if (deviceCount.size() == 0) {
                    ALib.AssertFailMethod("Reports Is Empty");
            } else {
                    for (int i = 0; i < deviceCount.size(); i++) {
                            String MobileData = deviceCount.get(i).getText();
                            long MobDatUsage = Long.parseLong(MobileData);

                            boolean flag;

                            if (MobDatUsage >= (Config.Data_usage)) {
                                    flag = true;
                            } else {
                                    flag = false;
                            }
                            String PassStatement = "PASS >> Data Is Showing Correctly.";
                            String FailStatement = "FAIL >> Data Isn't Showing Correctly.";
                            ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
                    }
            }
    }
		
		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr[1]"))
		private WebElement DatausageInfo;

		public void VerifyingDataUsage() throws InterruptedException {
			boolean flag = DatausageInfo.isDisplayed();
			String PassStatement = "PASS >> The report contains data of Wifi , Mobile data";
			String FailStatement = "FAIL >> The report not contains data of Wifi , Mobile data";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		public void VerificationOfMobileDataUsageColumn() {
			List<WebElement> deviceCount = Initialization.driver
					.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
			if (deviceCount.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < deviceCount.size(); i++) {
					String MobileData = deviceCount.get(i).getText();
					double MobDatUsage = Double.parseDouble(MobileData);

					boolean flag;

					if (MobDatUsage >= (Config.Data_usage)) {
						flag = true;
					} else {
						flag = false;
					}
					String PassStatement = "PASS >> Matched :Data usge is greater than the value which is given in filter";
					String FailStatement = "FAIL >> Not matched :Data usge is not greater than the value which is given in filter";
					ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
				}
			}
		}
		public void VarifyingYesterDateInViewReport_DeviceLocation() throws InterruptedException {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate7 = dateFormat.format(date);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date Yesterday = cal.getTime();
			String fromdate = dateFormat.format(Yesterday);
			System.out.println("Actual yesterdayDate:" + fromdate + " ");

			boolean flag;
			if (ls.size() == 0) {
				ALib.AssertFailMethod("No Data Present In Report");
			} else {
				for (int i = 1; i < ls.size(); i++) {
					String ActualValue = ls.get(i).getText();
					String datedate = ActualValue.substring(0, 10);
					flag = datedate.equals(fromdate);
					Reporter.log(flag + " :Yesterday date is matched", true);
				}
			}
			sleep(3);
			waitForXpathPresent("//p[contains(text(),'AMR Tech Park')]");

		}

		public void VarifyingCurrentDateInDeviceLocationReport() throws InterruptedException {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			System.out.println("Actual date is:" + todate + " ");

			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String datedate = ActualValue.substring(0, 10);
				System.out.println(datedate);
				value = datedate.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for current date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for current date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		
		public void VerifyPackageIdColumn() {
			List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
			boolean flag;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				if (ActualValue.equals(Config.EnterPackageId)) {
					flag = true;
				} else {
					flag = false;
				}
				String PassStmt = "PASS >> Data Showing correctly.";
				String FailStmt = "Fail >> Data Isn't Showing correctly.";
				ALib.AssertTrueMethod(flag, PassStmt, FailStmt);
			}
		}
		
		public void VarifyingYesterDate2InViewReport() throws InterruptedException {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("(//table[@id='devicetable']/tbody/tr/td[6])[1]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate7 = dateFormat.format(date);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date todate = cal.getTime();
			String fromdate = dateFormat.format(todate);
			System.out.println("Actual yesterdayDate:" + fromdate + " ");

			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String datedate = ActualValue.substring(0, 10);
				System.out.println(datedate);
				value = datedate.equals(fromdate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for Yesterday";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for Yesterday date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr"))
		private WebElement CheckData;
		public void CheckingSmsLogData() throws InterruptedException {
			boolean flag = CheckData.isDisplayed();
			String PassStatement = "PASS >> The report contain data of SMS log ";
			String FailStatement = "FAIL >> The report not contain data of SMS log";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		
		JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
		public void ScrollToColumn(String ColumnName) throws InterruptedException {
			WebElement scroll = Initialization.driver.findElement(By.xpath("(//div[text()='" + ColumnName + "'])"));
			js.executeScript("arguments[0].scrollIntoView()", scroll);
			sleep(4);
		}
		
		public void VerifyColumnsInSureLockCheckListReport() throws InterruptedException {
			String[] exp = { "Device Name", "SureLock Default Launcher", "SureLock Device Administrator", "Samsung KNOX",
					"Apps With Usage Access", "Disable USB Debugging", "Storage Permission", "Contacts Permission",
					"Location Permission", "Camera Permission", "Telephone Permission", "SMS Permission",
					"Configure System Permission", "Display Over Other Apps" };
			for (int i = 0; i < exp.length - 1; i++) {
				ScrollToColumn(exp[i]);
				sleep(2);
				String optionValue = cols.get(i).getText();
				System.out.println(optionValue);
				if (optionValue.equals(exp[i])) {
					Reporter.log("passed on: " + optionValue, true);
				} else {
					ALib.AssertFailMethod("Fail >> Expected columns are not displayed");
				}
			}
		}
		public void VerifyDefaultLauncherColumn() {
			List<WebElement> value = Initialization.driver
					.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
			if (value.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < value.size(); i++) {
					String Actualaddress = value.get(i).getText();
					boolean flag;
					if (Actualaddress.contains(Config.SureLockDefaultLauncher)) {
						flag = true;
					} else {
						flag = false;
					}
					String PassStatement = "PASS >> Data Showing Is Showing Correctly.";
					String FailStatement = "FAIL >> Data Showing Isn't Showing Correctly.";
					ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
				}
			}
		}
		
		@FindBy(xpath = ("(//a[text()='Download'])[1]"))
		private WebElement ClickOnDownloadLink;
		public void ClickOnDownloadReportLinkButton() throws InterruptedException {

			ClickOnDownloadLink.click();
			waitForXpathPresent("//span[contains(text(),'Custom Reports')]");
			sleep(5);
		}

		@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[2]")
		private WebElement SureLockDefaultLauncher;
		public void VerifySureLockDefaultLauncher(String DefaultLauncher) {
			String ReportPackageName = SureLockDefaultLauncher.getText();
			boolean Val = ReportPackageName.equals(DefaultLauncher);
			String Pass = "SureLock Default Launcher Is Showing Correctly";
			String Fail = "SureLock Default Launcher Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		public void VerifyingDurationColForGivenRange_InSureLockAnalytics() throws InterruptedException {
			if (DurationColumnInSureLockAnlayticsRep.size() == 0) {
				SwitchBackWindow();
				ALib.AssertFailMethod("Data Not Generated In Report");
			} else {
				for (int i = 0; i < DurationColumnInSureLockAnlayticsRep.size(); i++) {
					String ActualValue = DurationColumnInSureLockAnlayticsRep.get(i).getText();
					if (ActualValue.equals(Config.DurationFormat1) || ActualValue.equals(Config.DurationFormat2)
							|| ActualValue.equals(Config.DurationFormat3)) {
						Reporter.log("PASS >> Data is displayed accrding to selected range.", true);
					} else {
						ALib.AssertFailMethod("PASS >> Data doesn't displayed accrding to selected range.");
					}
				}
			}
		}
		
		@FindBy(xpath = ("(//li[text()='Last 1 Week'])[last()]"))
		private WebElement WeekDateInOndemand;
		public void Select1WeekDateInOndemand() throws InterruptedException {
			currentdateInondemand.click();
			sleep(2);
			WeekDateInOndemand.click();
			Reporter.log("Selected the WeekDateInOndemand", true);
		}
		
		public void VerifyingDurationColForGivenRange_InSureVideoAnalytics() throws InterruptedException {
			if (DurationColumnInSureFoxAnlayticsRep.size() == 0) {
				SwitchBackWindow();
				ALib.AssertFailMethod("Data Not Generated In Report");
			} else {
				for (int i = 0; i < DurationColumnInSureFoxAnlayticsRep.size(); i++) {
					String ActualValue = DurationColumnInSureFoxAnlayticsRep.get(i).getText();
					if (ActualValue.equals(Config.DurationFormat1) || ActualValue.equals(Config.DurationFormat2)
							|| ActualValue.equals(Config.DurationFormat3)) {
						Reporter.log("PASS >> Data is displayed accrding to selected range.", true);
					} else {
						ALib.AssertFailMethod("PASS >> Data doesn't displayed accrding to selected range.");
					}
				}
			}
		}

		@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[5]")
		private List<WebElement> DurationColumnInSureLockAnlayticsRep;

		public void CheckingDurationColDateFormat_InSureLockAnalytics() throws InterruptedException {
			if (DurationColumnInSureLockAnlayticsRep.size() == 0) {
				SwitchBackWindow();
				ALib.AssertFailMethod("Data Not Generated In Report");
			} else {
				LocalDateTime myDateObj = LocalDateTime.now();
				DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("HH:mm:ss");
				String formattedDate = myDateObj.format(myFormatObj);
				System.out.println(formattedDate);
				for (int i = 0; i < DurationColumnInSureLockAnlayticsRep.size(); i++) {
					String ActualValue = DurationColumnInSureLockAnlayticsRep.get(i).getText();
					if (formattedDate.compareTo(ActualValue) >= 0 || formattedDate.compareTo(ActualValue) <= 0) {
						Reporter.log("PASS >> Report get generated successfully in the expected Time Format", true);
					} else {
						ALib.AssertFailMethod("FAIL >> Report not get generated successfully in the expected Time Format");
					}
				}
			}
		}
		
		public void VerifySortingInSystemLogRep() throws InterruptedException {

			for (int i = 1; i <= 2; i++) {
				Initialization.driver.findElement(By.xpath("(//tr//th[" + i + "])[1]")).click();
				List<WebElement> originalList = Initialization.driver.findElements(
						By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[" + i + "]"));
				List<String> originalTextList = originalList.stream().map(s -> s.getText()).collect(Collectors.toList());
				System.out.println(originalTextList);
				List<String> ReversesortedList = originalTextList.stream().sorted(Comparator.reverseOrder())
						.collect(Collectors.toList());
				System.out.println(ReversesortedList);
				Assert.assertTrue(originalTextList.equals(ReversesortedList));
				System.out.println("Sorted successfully in the descending order");

				Initialization.driver.findElement(By.xpath("(//tr//th[" + i + "])[1]")).click();
				sleep(3);
				List<WebElement> originalList1 = Initialization.driver.findElements(
						By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[" + i + "]"));
				List<String> originalTextList1 = originalList1.stream().map(s -> s.getText()).collect(Collectors.toList());
				System.out.println(originalTextList1);
				List<String> sortedList = originalTextList1.stream().sorted().collect(Collectors.toList());
				System.out.println(sortedList);
				Assert.assertTrue(originalTextList1.equals(sortedList));
				System.out.println("Sorted successfully in the ascending order");
			}
		}
		
		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr/td[3]"))
		private List<WebElement> SystemLogMessage;

		@FindBy(xpath = ("//p[contains(text(),'Device Started.')]"))
		private List<WebElement> Systemlog;
		public void ClickOnSearchReportButtonInsidedViewedReport_SystemLog() throws InterruptedException {
			ClickOnSearchButtonInViewedReport.sendKeys(Config.Input_Val17);
			sleep(3);
			waitForXpathPresent("//p[contains(text(),'Device Started.')]");
			sleep(3);
			ClickOnSearchButtonInViewedReport.clear();
		}

		public void VerificationOfMessagefield() {
			boolean value = false;
			String PassStatement = "PASS >> The report contains the details device which are having the message like Device Started.";
			String FailStatement = "Fail >> The report not contains the details device which are having the message like Device Started.";
			for (int i = 0; i < SystemLogMessage.size(); i++) {
				String ActualValue = Systemlog.get(i).getText();
				// System.out.println(ActualValue);
				value = ActualValue.equals("Device Started.");
				if (value == false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		
		public void ClickOnView(String ReportName) throws InterruptedException {
			Reporter.log("Waiting For Report To Generate", true);
			waitForElementPresent("//table[@id='OfflineReportGrid']/tbody/tr[1]/td/p[text()='" + ReportName
					+ "']/parent::td/following-sibling::td[4]/a[text()='View']");
			viewReport1.click();
			sleep(2);
		}
		
		@FindBy(xpath = "//span[@class='tableDevCount_line']")
		private WebElement DeviceCount;
		public void ChooseDevicesPerPage() throws InterruptedException {
			try {
				if (DeviceCount.isDisplayed()) {
					String DeviceCout = DeviceCount.getText();
					String[] arrSplit_3 = DeviceCout.split("\\s");
					String value1 = null;
					for (int i = 0; i < arrSplit_3.length; i++) {
						value1 = arrSplit_3[i];
					}
					int i = Integer.parseInt(value1);
					System.out.println(">> Total number of devices shown:" + i + " ");

					if (i >= 1 && i < 21) {
						System.out.println(">> count is less than 20, so that there is no range selection option");
					} else if (i > 20 && i < 51) {
						allbuttonselect.click();
						sleep(4);
						Select50.click();
						System.out.println(">> count is less than 50, so selected 50 per page");
					} else if (i > 50 && i < 101) {
						allbuttonselect.click();
						sleep(4);
						Select100.click();
						System.out.println(">> count is less than 100, so selected 100 per page");
					} else {
						allbuttonselect.click();
						sleep(4);
						allbutton.click();
						System.out.println(">> count is more than 500, so selected all per page");
					}
				} else {
					SwitchBackWindow();
					ALib.AssertFailMethod("No Data Present In Report");
				}
			} catch (Exception e) {
				SwitchBackWindow();
				ALib.AssertFailMethod("No Data Present In Report");
			}
		}

		@FindBy(xpath = "//div[@id='reportList_tableCont']/section[1]/div[2]/div[2]/div[4]/div[1]/span[2]/span[1]/button")
		private WebElement allbuttonselect;
		
		@FindBy(xpath = "//a[text()='100']")
		private WebElement Select100;
		
		@FindBy(xpath = "//a[text()='All']")
		private WebElement allbutton;
		
		@FindBy(xpath = ("//section[@id='report_viewSect']/div/div[2]/div[1]/div[2]/input[1]"))
		private WebElement ClickOnSearchReport;
		public void ClearSearchedReport() throws InterruptedException {
			ClickOnSearchReport.clear();
			Initialization.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		
		@FindBy(xpath = "//div[@class='fixed-table-header']/table/thead/tr/th")
		private List<WebElement> Tableheader;

		@FindBy(xpath = "//*[@id='devicetable']/tbody/tr/td[2]")
		private List<WebElement> elementList;
		
		private ArrayList<Integer> getIntegerArray(ArrayList<String> stringArray) {
			ArrayList<Integer> result = new ArrayList<Integer>();
			for (String stringValue : stringArray) {
				try {
					result.add(Integer.parseInt(stringValue));
				} catch (NumberFormatException nfe) {
					System.out.println("failed");
				}
			}
			return result;
		}
		
		public void VerificationOfValuesInColumn() throws InterruptedException {
			if (elementList.size() == 0) {
				SwitchBackWindow();
				ALib.AssertFailMethod("Data Not Generated In Report");
			} else {
				for (int k = 1; k < Tableheader.size(); k++) {

					ArrayList<String> OriginalList = new ArrayList<String>();
					for (int i = 0; i < elementList.size(); i++) {
						OriginalList.add(elementList.get(i).getText());

					}
					System.out.println(OriginalList);

					ArrayList<Integer> resultList1 = getIntegerArray(OriginalList);
					ArrayList<Integer> resultList2 = getIntegerArray(OriginalList);
					System.out.println(resultList1);
					Collections.sort(resultList2);
					Assert.assertTrue(resultList1.equals(resultList2));
				}
			}
		}
		
		@FindBy(xpath = "//*[@id='report_genrateSect']/div[1]/div[1]/div[1]/div/input")
		private WebElement SearchOnDemandReports;
		public void SearchCustomizedReport(String reportname) throws InterruptedException {
			SearchOnDemandReports.clear();
			sleep(2);
			SearchOnDemandReports.click();
			SearchOnDemandReports.sendKeys(reportname);
			waitForVisibilityOf("(//table[@id='reportsGrid']/tbody/tr/td/div/span[1])[last()]");
			sleep(2);
		}
		@FindBy(xpath = ("(//table[@id='reportsGrid']/tbody/tr/td/div/span[1])[last()]"))
		private WebElement EnterSavedCustomizedReportName;
		public void ClickOnCustomizedReportNameInOndemand() throws InterruptedException {
			EnterSavedCustomizedReportName.click();
			sleep(2);
			//waitForVisibilityOf("(//table[@id='reportsGrid']/tbody/tr/td/div/span[1])[last()]");
			waitForXpathPresent("(//table[@id='reportsGrid']/tbody/tr/td/div/span[1])[last()]");
		}
		
		public void VerifyApplicationProgram() {
			String ReportPackageName = Initialization.driver
					.findElement(By.xpath("//li[@class='list-group-item node-selected_tablesList_tree'][2]")).getText();
			System.out.println(ReportPackageName);
			boolean Val = ReportPackageName.equals("Application Permission");
			String Pass = "Column Is Showing Correctly";
			String Fail = "ColumnIsn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
			String ReportPackageName1 = Initialization.driver
					.findElement(By.xpath("//li[@class='list-group-item node-selected_tablesList_tree'][3]")).getText();
			boolean Val1 = ReportPackageName1.equals("Package Name");
			String Pass1 = "Column Is Showing Correctly";
			String Fail1 = "ColumnIsn't Showing Correctly";
			ALib.AssertTrueMethod(Val1, Pass1, Fail1);
			String ReportPackageName2 = Initialization.driver
					.findElement(By.xpath("//li[@class='list-group-item node-selected_tablesList_tree'][4]")).getText();
			boolean Val2 = ReportPackageName2.equals("Permission");
			String Pass2 = "Column Is Showing Correctly";
			String Fail2 = "ColumnIsn't Showing Correctly";
			ALib.AssertTrueMethod(Val2, Pass2, Fail2);
			String ReportPackageName3 = Initialization.driver
					.findElement(By.xpath("//li[@class='list-group-item node-selected_tablesList_tree'][5]")).getText();
			boolean Val3 = ReportPackageName3.equals("Status");
			String Pass3 = "Column Is Showing Correctly";
			String Fail3 = "ColumnIsn't Showing Correctly";
			ALib.AssertTrueMethod(Val3, Pass3, Fail3);
			String ReportPackageName4 = Initialization.driver
					.findElement(By.xpath("//li[@class='list-group-item node-selected_tablesList_tree'][6]")).getText();
			boolean Val4 = ReportPackageName4.equals("Permission Group");
			String Pass4 = "Column Is Showing Correctly";
			String Fail4 = "ColumnIsn't Showing Correctly";
			ALib.AssertTrueMethod(Val4, Pass4, Fail4);
			String ReportPackageName5 = Initialization.driver
					.findElement(By.xpath("//li[@class='list-group-item node-selected_tablesList_tree'][7]")).getText();
			boolean Val5 = ReportPackageName5.equals("Last Updated");
			String Pass5 = "Column Is Showing Correctly";
			String Fail5 = "ColumnIsn't Showing Correctly";
			ALib.AssertTrueMethod(Val5, Pass5, Fail5);
		}
		
		public void VarifyingOneWeekDateInCallLogReport() throws InterruptedException {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[5]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();

			String day1 = dateFormat.format(date);
			// System.out.println(day1);

			Calendar cal = Calendar.getInstance();

			cal.add(Calendar.DATE, -1);
			Date todate6 = cal.getTime();
			String day2 = dateFormat.format(todate6);
			// System.out.println(day2);

			cal.add(Calendar.DATE, -1);
			Date todate5 = cal.getTime();
			String day3 = dateFormat.format(todate5);
			// System.out.println(day3);

			cal.add(Calendar.DATE, -1);
			Date todate4 = cal.getTime();
			String day4 = dateFormat.format(todate4);
			// System.out.println(day4);

			cal.add(Calendar.DATE, -1);
			Date todate3 = cal.getTime();
			String day5 = dateFormat.format(todate3);
			// System.out.println(day5);

			cal.add(Calendar.DATE, -1);
			Date todate2 = cal.getTime();
			String day6 = dateFormat.format(todate2);
			// System.out.println(day6);

			cal.add(Calendar.DATE, -1);
			Date todate1 = cal.getTime();
			String day7 = dateFormat.format(todate1);
			// System.out.println(day7);
			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String datedate = ActualValue.substring(0, 10);
				// System.out.println(datedate);
				value = datedate.equals(day1) || datedate.equals(day2) || datedate.equals(day3) || datedate.equals(day4)
						|| datedate.equals(day5) || datedate.equals(day6) || datedate.equals(day7);
				if (value) {
					String PassMessage = "PASS >> Report get generated successfully for One Week Date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for One Week Date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VarifyingCurrentDateInViewReport() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[5]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			System.out.println("Actual date is:" + todate + " ");

			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String hello = ActualValue.substring(0, 10);
				System.out.println(hello);
				value = hello.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for current date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for current date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VarifyingYesterDateInViewReport_Calllog() throws InterruptedException {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[5]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate7 = dateFormat.format(date);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date todate = cal.getTime();
			String fromdate = dateFormat.format(todate);
			System.out.println("Actual yesterdayDate:" + fromdate + " ");

			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String datedate = ActualValue.substring(0, 10);
				System.out.println(datedate);
				value = datedate.equals(fromdate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for Yesterday";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for Yesterday date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void VarifyingCalltype_Calllog() throws InterruptedException {
			List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[4]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String calltype = ls.get(i).getText();
				System.out.println(calltype);
				value = calltype.equals(Config.EnterCallType);
				if (value == true) {
					String PassMessage = "PASS >>The report contain data of all Incoming calls";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> The report not contain data of all Incoming calls";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}
		
		public void VarifyingCurrentDateInViewReport_SmsLog() throws InterruptedException {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[6]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			System.out.println("Actual date is:" + todate + " ");
			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String datedate = ActualValue.substring(0, 10);
				System.out.println(datedate);
				value = datedate.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for current date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for current date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void Varifycallduration() {
			List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[6]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			boolean flag;
			for (int i = 0; i < ls.size(); i++) {
				String Callduration = ls.get(i).getText();
				System.out.println(Callduration);
				int call = Integer.parseInt(Callduration);
				if (call <= (Config.Actl_duration)) {
					flag = true;
				} else {
					flag = false;
				}
				String PassSmt = "PASS >> The report contains data of Call log report wrt filter applied i.e <= 20sec duration time";
				String Failsmt = "FAIL >> The report doesn't contains data of Call log report wrt filter applied i.e <= 20sec duration time";
				ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
			}
		}

		
		public void VarifyingAppliedJob() throws InterruptedException {

			List<WebElement> value = Initialization.driver
					.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[2]"));
			if (value.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < value.size(); i++) {
					String Actualval = value.get(i).getText();
					boolean flag;
					if (Actualval.contains(Config.Job_Name)) {
						flag = true;
					} else {
						flag = false;
					}
					String PassStatement = "PASS >> Job in Viewed report is same as applied job";
					String FailStatement = "FAIL >> Job in Viewed report is not same as applied job ";
					ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
				}
			}
		}
		
		public void VarifyingAppliedJobStatus() {

			List<WebElement> value = Initialization.driver
					.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[5]"));
			if (value.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < value.size(); i++) {
					String Actualval = value.get(i).getText();
					boolean flag;
					if (Actualval.equals("Deployed")) {
						flag = true;
					} else {
						flag = false;
					}
					String PassStatement = "PASS >> Status of the applied job is deployed as per the provided filter";
					String FailStatement = "Fail >> Status of the applied job is not as per the provided filter";
					ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
				}
			}
		}

		@FindBy(xpath = ("//input[@class='form-control']"))
		private WebElement ClickOnSearchButtonInViewedReport;
		public void SearchBoxInsideViewRep(String val) throws InterruptedException {
			ClickOnSearchButtonInViewedReport.clear();
			sleep(2);
			ClickOnSearchButtonInViewedReport.sendKeys(val);
			sleep(2);
		}
		
		@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[1]")
		private WebElement DeviceNameInReport;
		public void VerifyingDeviceName(String DeviceName) {
			String ReportPackageName = DeviceNameInReport.getText();
			boolean Val = ReportPackageName.equals(DeviceName);
			String Pass = "PASS >> DeviceName Is Showing Correctly";
			String Fail = "FAIL >> DeviceName Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		@FindBy(xpath = "(//button[text()='Add'])[1]")
		private WebElement ClickOnAddGroup1;
		public void ClickOnAddGroupButton() throws InterruptedException {
			ClickOnAddGroup1.click();
			waitForXpathPresent("//button[text()='Request Report']");
			sleep(2);
		}
		
		@FindBy(xpath = ("(//li[text()='dontouch'])[1]"))
		private WebElement ClickOnGroup;
		public void ClickOnOneGroup() throws InterruptedException {
			ClickOnGroup.click();
			sleep(2);
			waitForXpathPresent("(//button[text()='Add'])[1]");
			sleep(2);
		}
		
		@FindBy(xpath = "//div[@id='reportDetails']/div[1]/span[2]")
		private WebElement ClickAddGroup;
		public void ClickOnAddGroup() throws InterruptedException {
			ClickAddGroup.click();
			waitForXpathPresent("(//button[@id='adddevicebtn'])[1]");
			sleep(3);
		}
		
		public void VerifyingReportInViewReportSection(String val) {
			Initialization.driver.findElement(By.xpath("(//table[@id='OfflineReportGrid']/tbody/tr/td[1]/p)[last()]"))
					.click();
			boolean OptionPresent = Initialization.driver.findElement(By.xpath("//p[text()='" + val + "']")).isDisplayed();
			String pass = "PASS >> Report is generated and listed in View Reports section";
			String fail = "FAIL >> Report not generated and not listed in View Reports section";
			ALib.AssertTrueMethod(OptionPresent, pass, fail);
		}

		@FindBy(xpath = ("(//li[text()='Last 30 Days'])[last()]"))
		private WebElement Last30daysInOndemand;
		public void SelectLast30daysInOndemand() throws InterruptedException {
			currentdateInondemand.click();
			sleep(1);
			Last30daysInOndemand.click();
			Reporter.log("Selected the Last30days in ondemand report", true);
		}
		
		public void ClickOnCancelButton() {
			Initialization.driver.findElement(By.xpath("//button[@id='custom_reports_cancel_btn']")).click();
		}
		public void ClickOnAddButtonInCustomReport() throws InterruptedException {
			Add.click();
			Reporter.log("PASS >> Table Added to Selected table list", true);
			sleep(1);
		}
		public void VarifyingLastConnectedDate() {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				String hello = ActualValue.substring(0, 10);
				System.out.println(hello);
				value = hello.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report contains only today's last connected devices";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not contains only today's last connected devices";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		@FindBy(xpath = "//table[@class='table table-no-bordered table-hover']/tbody/tr/td[1]")
		private List<WebElement> DeviceLocalIPAdd;

		public void VerifyingDeviceLocalIPAddressCol_EqualOp() {
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			boolean value = false;
			String PassStatement = "PASS >> Device Local IP address is displayed correctly as expected only";
			String FailStatement = "Fail >> Device Local IP address is not displayed as expected";
			for (int i = 0; i < DeviceLocalIPAdd.size(); i++) {
				String ActualValue = col.get(i).getText();
				System.out.println("PASS >> Device Local IP Addess  is:" + ActualValue + " ");

				value = ActualValue.equals(Config.LocalIP_Address);
				if (value == false) {
					ALib.AssertTrueMethod(value, PassStatement, FailStatement);
				}
			}
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		public void VerifyingNotesCol_EqualOp() {
			boolean value;
			if (col.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			} else {
				for (int i = 0; i < col.size(); i++) {
					String ActualValue = col.get(i).getText();
					System.out.println("Note displayed in report:" + ActualValue + " ");
					value = ActualValue.equals("AutomationDevice");
					if (value == true) {
						String PassMessage = "PASS >> Expected Note is displayed.";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL>> Expected Note not is displayed.";
						ALib.AssertFailMethod(FailMessage);
					}
				}

			}
		}

		public void VerifyingNotesCol_LikeOp() {
			boolean value;
			for (int i = 0; i < col.size(); i++) {
				String ActualValue = col.get(i).getText();
				value = ActualValue.contains("Automation");
				if (value == true) {
					String PassMessage = "PASS >> Report is generated as Expected";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report is not generated as Expected";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

	 public void SaveButton_CustomReports()
	 {
		 SaveButton.click();
	 }
	 
	 public void NotificationOnCustomReportCreated() throws InterruptedException{
			boolean value =true;
			 
			 try{
				 SuccessullMessage_CustomReportCreated.isDisplayed();
				   	 
				    }catch(Exception e)
				    {
				    	value=false;
				   	 
				    }
		 
		   Assert.assertTrue(value,"FAIL >> Customized Report saved Notification not displayed ");
		   Reporter.log("PASS >> Notification 'Customize report saved.' is displayed",true );
		   sleep(4);
	 }
	 
	 public void IsSavedCustomizedReportDisplayed() throws InterruptedException
     {
     	List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='customReport-listTable']/tbody/tr/td"));
     	String ExpectedName = ls.get(0).getText();
     	String ActualName = Config.DeviceDetails_CustomReportName;
     	String PassStatement = "PASS >> name of the custom report is displayed";
    	String FailStatement ="FAIL >> name of the custom report is NOT displayed ";
    	ALib.AssertEqualsMethod(ExpectedName, ActualName, PassStatement, FailStatement);
    	
    	String ExpectedDescription = ls.get(1).getText();
     	String ActualDescription = Config.DeviceDetails_CustomReportDescription;
     	String PassStatement1 = "PASS >> Description of custom report is displayed";
    	String FailStatement1 ="FAIL >> Description of custom report is NOT displayed";
    	ALib.AssertEqualsMethod(ExpectedDescription, ActualDescription, PassStatement1, FailStatement1);
    	
    }
	 public void isDeviceDetailsCustomReportPresentInOnDemandReport()
	 {
		 boolean isPresent = isDeviceDetailsPresentUnderOnDemandReport.isDisplayed();
		 String pass = "PASS >> Custom Report 'Device Details' present under On Demand Report";
		 String fail = "FAIL >> Custom Report 'Device Details' NOT present under On demand Report";
		 ALib.AssertTrueMethod(isPresent, pass, fail);
	 }
	 
	 public void VerifyExpandAppliedJobDetails()
	 {
		 List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='tablesList_tree']/ul/li"));
		
	   	 //verifying parameters of Applied Job Details
	   	 Reporter.log("========Verify 'Applied Job Details' parameters one by one=========");
	   		
	   	 for(int i=87;i<91;i++){
	   	 if(i==87)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Job Name";
	   		String PassStatement ="PASS >> 1st option is present - correct";
	   		String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==88)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Time Scheduled";
	   		String PassStatement ="PASS >> 2nd option is present - correct";
	   		String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==89)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Time Deployed";
	   		String PassStatement ="PASS >> 3rd option is present - correct";
	   		String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==90)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Status";
	   		String PassStatement ="PASS >> 4th option is present - correct";
	   		String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
	 }
	 }
	 
	 public void VerifyExpandCallLog()
	 {
		 List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='tablesList_tree']/ul/li"));
		
	   	 //verifying parameters of Applied Job Details
	   	 Reporter.log("========Verify 'Call Log' parameters one by one=========");
	   		
	   	 for(int i=61;i<65;i++){
	   	 if(i==61)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Number";
	   		String PassStatement ="PASS >> 1st option is present - correct";
	   		String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==62)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Name";
	   		String PassStatement ="PASS >> 2nd option is present - correct";
	   		String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==63)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Call Type";
	   		String PassStatement ="PASS >> 3rd option is present - correct";
	   		String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==64)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Datetime";
	   		String PassStatement ="PASS >> 4th option is present - correct";
	   		String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==65)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Duration (Seconds)";
	   		String PassStatement ="PASS >> 5th option is present - correct";
	   		String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
	 }
	 }
	 
	 public void VerifyDeviceDetails()
	 {
		 List<WebElement> parameters = Initialization.driver.findElements(By.xpath(".//*[@id='tablesList_tree']/ul/li"));
		
	   	 //Verify custom reports by expanding tables list of Device Details
	   	 Reporter.log("========Verify custom reports by expanding tables list of Device Details=========");
	   		
	   	 for(int i=1;i<58;i++){
	   	 if(i==1)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Device Name";
	   		String PassStatement ="PASS >> 1st option is present - correct";
	   		String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==2)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Device IP Address";
	   		String PassStatement ="PASS >> 2nd option is present - correct";
	   		String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==3)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Last Connected Time";
	   		String PassStatement ="PASS >> 3rd option is present - correct";
	   		String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==4)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Device Local IP Address";
	   		String PassStatement ="PASS >> 4th option is present - correct";
	   		String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==5)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Notes";
	   		String PassStatement ="PASS >> 5th option is present - correct";
	   		String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==6)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Platform";
	   		String PassStatement ="PASS >> 6st option is present - correct";
	   		String FailStatement ="FAIL >> 6st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==7)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Model";
	   		String PassStatement ="PASS >> 7nd option is present - correct";
	   		String FailStatement ="FAIL >> 7nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==8)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Device Registered";
	   		String PassStatement ="PASS >> 8th option is present - correct";
	   		String FailStatement ="FAIL >> 8th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==9)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Battery Percent";
	   		String PassStatement ="PASS >> 9th option is present - correct";
	   		String FailStatement ="FAIL >> 9th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==10)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Battery Level";
	   		String PassStatement ="PASS >> 10th option is present - correct";
	   		String FailStatement ="FAIL >> 10th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
	 	 if(i==11)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Battery State";
	   		String PassStatement ="PASS >> 1st option is present - correct";
	   		String FailStatement ="FAIL >> 1st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==12)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Battery Chemistry";
	   		String PassStatement ="PASS >> 2nd option is present - correct";
	   		String FailStatement ="FAIL >> 2nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==13)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Backup Battery Percent";
	   		String PassStatement ="PASS >> 3rd option is present - correct";
	   		String FailStatement ="FAIL >> 3rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==14)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Backup Battery Level";
	   		String PassStatement ="PASS >> 4th option is present - correct";
	   		String FailStatement ="FAIL >> 4th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==15)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Backup Battery State";
	   		String PassStatement ="PASS >> 5th option is present - correct";
	   		String FailStatement ="FAIL >> 5th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==16)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Total Physical Memory (MB)";
	   		String PassStatement ="PASS >> 6st option is present - correct";
	   		String FailStatement ="FAIL >> 6st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==17)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Available Physical Memory (MB)";
	   		String PassStatement ="PASS >> 7nd option is present - correct";
	   		String FailStatement ="FAIL >> 7nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==18)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Total Storage Memory (MB)";
	   		String PassStatement ="PASS >> 8th option is present - correct";
	   		String FailStatement ="FAIL >> 8th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==19)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Available Storage Memory (MB)";
	   		String PassStatement ="PASS >> 9th option is present - correct";
	   		String FailStatement ="FAIL >> 9th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==20)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Operating System";
	   		String PassStatement ="PASS >> 10th option is present - correct";
	   		String FailStatement ="FAIL >> 10th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==21)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "MAC Address";
	   		String PassStatement ="PASS >> 21st option is present - correct";
	   		String FailStatement ="FAIL >> 21st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==22)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Network Operator";
	   		String PassStatement ="PASS >> 22nd option is present - correct";
	   		String FailStatement ="FAIL >>22nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==23)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Phone Signal";
	   		String PassStatement ="PASS >> 23rd option is present - correct";
	   		String FailStatement ="FAIL >> 23rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==24)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Network Signal";
	   		String PassStatement ="PASS >> 24th option is present - correct";
	   		String FailStatement ="FAIL >> 24th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==25)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SSID";
	   		String PassStatement ="PASS >> 25th option is present - correct";
	   		String FailStatement ="FAIL >> 25th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==26)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Agent Version";
	   		String PassStatement ="PASS >> 26th option is present - correct";
	   		String FailStatement ="FAIL >> 26st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==27)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "IMEI";
	   		String PassStatement ="PASS >> 27th option is present - correct";
	   		String FailStatement ="FAIL >> 27th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==28)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "IMSI";
	   		String PassStatement ="PASS >> 28th option is present - correct";
	   		String FailStatement ="FAIL >> 28th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==29)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Protocol";
	   		String PassStatement ="PASS >> 29th option is present - correct";
	   		String FailStatement ="FAIL >> 29th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==30)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Wi-Fi Signal";
	   		String PassStatement ="PASS >>30th option is present - correct";
	   		String FailStatement ="FAIL >> 30th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
	 	 if(i==31)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Root Status";
	   		String PassStatement ="PASS >> 31st option is present - correct";
	   		String FailStatement ="FAIL >> 31st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==32)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Knox Status";
	   		String PassStatement ="PASS >> 32nd option is present - correct";
	   		String FailStatement ="FAIL >> 32nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==33)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureLock Version";
	   		String PassStatement ="PASS >> 33rd option is present - correct";
	   		String FailStatement ="FAIL >> 33rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==34)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureFox Version";
	   		String PassStatement ="PASS >> 34th option is present - correct";
	   		String FailStatement ="FAIL >> 34th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==35)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureVideo Version";
	   		String PassStatement ="PASS >> 35th option is present - correct";
	   		String FailStatement ="FAIL >> 35th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==36)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Default Launcher";
	   		String PassStatement ="PASS >> 36st option is present - correct";
	   		String FailStatement ="FAIL >> 36st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==37)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Serial Number";
	   		String PassStatement ="PASS >> 37th option is present - correct";
	   		String FailStatement ="FAIL >> 37th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==38)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Device Phone Number";
	   		String PassStatement ="PASS >>38th option is present - correct";
	   		String FailStatement ="FAIL >> 38th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==39)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Sim Serial Number";
	   		String PassStatement ="PASS >> 39option is present - correct";
	   		String FailStatement ="FAIL >> 39th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==40)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Roaming";
	   		String PassStatement ="PASS >> 40th option is present - correct";
	   		String FailStatement ="FAIL >> 40th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==41)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "OS Version";
	   		String PassStatement ="PASS >> 41st option is present - correct";
	   		String FailStatement ="FAIL >> 41st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==42)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Last Device Time";
	   		String PassStatement ="PASS >> 42nd option is present - correct";
	   		String FailStatement ="FAIL >> 42nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==43)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Data Usage";
	   		String PassStatement ="PASS >> 43rd option is present - correct";
	   		String FailStatement ="FAIL >> 43rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==44)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "CPU Usage";
	   		String PassStatement ="PASS >> 44th option is present - correct";
	   		String FailStatement ="FAIL >> 44th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==45)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "GPU Usage";
	   		String PassStatement ="PASS >> 45th option is present - correct";
	   		String FailStatement ="FAIL >> 45th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==46)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Temperature";
	   		String PassStatement ="PASS >> 46st option is present - correct";
	   		String FailStatement ="FAIL >> 46st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==47)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Is Supervised";
	   		String PassStatement ="PASS >> 47th option is present - correct";
	   		String FailStatement ="FAIL >> 47th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==48)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureLock Licensed";
	   		String PassStatement ="PASS >> 48th option is present - correct";
	   		String FailStatement ="FAIL >> 48th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==49)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureFox Licensed";
	   		String PassStatement ="PASS >> 49th option is present - correct";
	   		String FailStatement ="FAIL >> 49th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==50)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureVideo Licensed";
	   		String PassStatement ="PASS >> 50th option is present - correct";
	   		String FailStatement ="FAIL >> 50th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
	 	 if(i==51)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureLock Admin";
	   		String PassStatement ="PASS >> 51st option is present - correct";
	   		String FailStatement ="FAIL >> 51st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==52)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureFox Admin";
	   		String PassStatement ="PASS >> 52nd option is present - correct";
	   		String FailStatement ="FAIL >> 52nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==53)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "SureVideo Admin";
	   		String PassStatement ="PASS >> 53rd option is present - correct";
	   		String FailStatement ="FAIL >> 53rd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==54)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Nix Admin";
	   		String PassStatement ="PASS >> 54th option is present - correct";
	   		String FailStatement ="FAIL >> 54th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==55)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Nix Polling";
	   		String PassStatement ="PASS >> 55th option is present - correct";
	   		String FailStatement ="FAIL >> 55th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	}
		 if(i==56)
	   	 {
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Network Type";
	   		String PassStatement ="PASS >> 56st option is present - correct";
	   		String FailStatement ="FAIL >> 56st option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==57)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "GPS Enabled";
	   		String PassStatement ="PASS >> 57nd option is present - correct";
	   		String FailStatement ="FAIL >> 57nd option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==58)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Group Path";
	   		String PassStatement ="PASS >> 58th option is present - correct";
	   		String FailStatement ="FAIL >> 58th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	   	 if(i==59)
	   	 {	
	   		String actual =parameters.get(i).getText();
	   		Reporter.log(actual);
	   		String expected = "Tags";
	   		String PassStatement ="PASS >> 59th option is present - correct";
	   		String FailStatement ="FAIL >> 59th option is NOT present - incorrect";
	   		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	   	 }
	 } 	
	 }
	 
	 public void ClickOnAddMoreButton() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("(//div[@class='btnCol'])[last()]")).click();
			sleep(2);
			waitForVisibilityOf("//option[text()='Device Details']");
		}
	 
	 public void VarifyingCurrentDateInViewReport_AppliedJobDetails() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			// String todateActual=Currendate.getText();
			System.out.println(todate);

			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String hello = ActualValue.substring(0, 10);
				System.out.println(hello);
				value = hello.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for current date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for current date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}
	
	 public void VarifyingOneWeekDateInDevLocationReport() {
			{
				List<WebElement> ls = Initialization.driver
						.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();

				String day1 = dateFormat.format(date);
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				Date todate6 = cal.getTime();
				String day2 = dateFormat.format(todate6);
				cal.add(Calendar.DATE, -1);
				Date todate5 = cal.getTime();
				String day3 = dateFormat.format(todate5);

				cal.add(Calendar.DATE, -1);
				Date todate4 = cal.getTime();
				String day4 = dateFormat.format(todate4);

				cal.add(Calendar.DATE, -1);
				Date todate3 = cal.getTime();
				String day5 = dateFormat.format(todate3);

				cal.add(Calendar.DATE, -1);
				Date todate2 = cal.getTime();
				String day6 = dateFormat.format(todate2);

				cal.add(Calendar.DATE, -1);
				Date todate1 = cal.getTime();
				String day7 = dateFormat.format(todate1);
				boolean value;
				for (int i = 0; i < ls.size(); i++) {
					String ActualValue = ls.get(i).getText();
					String datedate = ActualValue.substring(0, 10);
					System.out.println(datedate);
					value = datedate.equals(day1) || datedate.equals(day2) || datedate.equals(day3) || datedate.equals(day4)
							|| datedate.equals(day5) || datedate.equals(day6) || datedate.equals(day7);
					if (value) {
						String PassMessage = "PASS >> Report get generated successfully for One Week Date";
						System.out.println(PassMessage);
					} else {
						String FailMessage = "FAIL >> Report not generated successfully for One Week Date";
						ALib.AssertFailMethod(FailMessage);
					}
				}
			}
		}
	 
	 @FindBy(xpath = "//div[@class='customReportWrapper']")
		private List<WebElement> customReportWrapper;

		public void CheckingReportCreatedDateFormat() throws InterruptedException {
			LocalDateTime myDateObj = LocalDateTime.now();
			DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss a");
			String formattedDate = myDateObj.format(myFormatObj);
			for (int i = 0; i < customReportWrapper.size(); i++) {
				String ActualFormat = customReportWrapper.get(i).getText();
				System.out.println(ActualFormat);

				if (formattedDate.compareTo(ActualFormat) >= 0 || formattedDate.compareTo(ActualFormat) <= 0) {
					Reporter.log("PASS >> Report get generated successfully in the expected Date & Time Format", true);
				} else {
					ALib.AssertFailMethod(
							"FAIL >> Report not get generated successfully in the expected Date & Time Format");
				}
			}
		}
		
	 public void VerifingDataPickerMoment() throws InterruptedException, AWTException {
			currentdate.click();
			waitForXpathPresent(
					"//div[@class='daterangepicker ltr show-ranges show-calendar opensleft drop-up']/div/ul/li[3]");
			sleep(3);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_DOWN);
			Thread.sleep(4000);
			robot.keyPress(KeyEvent.VK_UP);
			Reporter.log("PASS >> screen is scrolled", true);
			if (TodaydateInOndemand.isDisplayed()) {
				Reporter.log("PASS >> Date picker is moving along with the screen when we scrolls the screen", true);
			} else {
				ALib.AssertFailMethod("FAIL >> Date picker is not moving along with the screen when we scrolls the screen");
			}
		}
	 
	 public void VerifyTimeZoneFor1WeekDateInCalLogsRep() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[5]"));
			LocalDateTime myDateObj = LocalDateTime.now();
			DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("HH:mm:ss");
			String ConsoleTime = myDateObj.format(myFormatObj);
			System.out.println("current time: " + ConsoleTime);
			String StartedTime = "12:00:00";
			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String ActualTimings = ActualValue.substring(11, 19);
				System.out.println(ActualTimings);
				if ((ConsoleTime.compareTo(StartedTime) > 0) || ((ConsoleTime.compareTo(ActualTimings) < 0))) {
					Reporter.log("PASS >> Report get generated successfully in the expected time zone", true);
				} else {
					ALib.AssertFailMethod("FAIL >> Report not get generated successfully in the expected time zone");
				}
			}
		}
	 
	 public void SendRepToSearchBox(String val) throws InterruptedException {
			SendReportToDelete.sendKeys(val);
			waitForXpathPresent("//table[@id='customReport-listTable']/tbody/tr");
			sleep(3);
		}
	 
	 	public static String os;
		@FindBy(xpath = "//*[@class='OsBuildNumber']/p[1]")
		private WebElement OSBuildNum;

		public void GetDeviceOSBuildNumber() {
			os = OSBuildNum.getText();
			System.out.println(os);
		}

	 public void VerifySearchBox(String rep) {
			if (Initialization.driver.findElement(By.xpath("//div[text()='" + rep + "']")).isDisplayed()) {
				Reporter.log("Generated report is displaying as for the search", true);
			} else {
				Reporter.log("Generated report is not displaying as for the search", true);
			}

		}
	 
	 @FindBy(xpath = ("(//div[text()='All Jobs List'])[1]"))
		private WebElement aliasenameJobname;
	 public void CheckingAlisaNameForJobNameCol() throws InterruptedException {
			String devname = aliasenameJobname.getText();
			System.out.println(devname);

			boolean flag;
			if (devname.equals(Config.Alias_ActlJobName)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassStmt = "PASS >>Alias names are displaying instead of predefined columns name for those column for which Alias name given in the generated report.";
			String FailStmt = "Fail >> Alias names are not displaying instead of predefined name for those column in the generated report.";
			ALib.AssertFalseMethod(flag, PassStmt, FailStmt);
		}
	 
	 @FindBy(xpath = ("(//input[@class='form-control ct-model-input parllSelects frhEle group_by_aggr_alias_text'])[2]"))
		private WebElement AliasName_JobName;
	 public void SendingAliasName_JobName(String value) {
			AliasName_JobName.sendKeys(value);
			Reporter.log("Provided AliasName For Job Name", true);
		}
	 
	 @FindBy(xpath = "//input[@id='uName']")
		private WebElement userNAmeEdt;

		@FindBy(xpath = "//input[@id='pass']")
		private WebElement passwordEdt;
	 public void logintoApp(String un, String pwd) throws InterruptedException {
			userNAmeEdt.sendKeys(un);
			sleep(7);
			passwordEdt.sendKeys(pwd);
			sleep(3);
			waitForidPresent("loginBtn");
		}
	 
	 @FindBy(id = "loginBtn")
		private WebElement loginBtn;
	 public void ClickOnLogin() throws InterruptedException {
			loginBtn.click();
			Reporter.log("Logged In to the account succefully", true);
			sleep(20);
		}
	 
		public void ClickOnApplyButton() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("(//button[text()='Apply'])[last()]")).click();
			sleep(2);
		}
		
	 @FindBy(xpath = "//*[@class='table table-no-bordered table-hover']/thead/tr/th")
		private List<WebElement> cols;

		public void VerifyColumnsInOSUpdateReport() {
			String[] exp = { "Device Name", "Status", "Published Date", "KB Number", "Description", "Type" };
			for (int i = 0; i < exp.length; i++) {
				String optionValue = cols.get(i).getText();
				if (optionValue.equals(exp[i])) {
					Reporter.log("passed on: " + optionValue, true);
				} else {
					ALib.AssertFailMethod("Fail >> Expected columns are not displayed");
				}
			}
		}
		
	 public void SelectTodayDateInCustomReport() throws InterruptedException {
			CurrentdateInCustomReport.click();
			waitForXpathPresent("//li[text()='Custom Range']");
			sleep(3);
			Initialization.driver.findElement(By.xpath("(//li[text()='Today'])[last()]")).click();
			Reporter.log("Selected the today date in Custom report", true);
		}
	 
	 public void VerifyTimeZoneForTodaysDate() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
			LocalDateTime myDateObj = LocalDateTime.now();
			DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("HH:mm:ss");
			String ConsoleTime = myDateObj.format(myFormatObj);
			System.out.println("current time: " + ConsoleTime);
			String StartedTime = "12:00:00";
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String ActualTimings = ActualValue.substring(11, 19);
				System.out.println(ActualTimings);
				if ((ConsoleTime.compareTo(StartedTime) > 0) || ((ConsoleTime.compareTo(ActualTimings) < 0))) {
					Reporter.log("PASS >> Report get generated successfully in the expected time zone", true);
				} else {
					ALib.AssertFailMethod("FAIL >> Report not get generated successfully in the expected time zone");
				}
			}
		}

	 public void VerifyTimeZoneForYesterday() {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
			LocalDateTime myDateObj = LocalDateTime.now();
			DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("HH:mm:ss");
			String ConsoleTime = myDateObj.format(myFormatObj);
			System.out.println("current time: " + ConsoleTime);
			String StartedTime = "12:00:00";
			String EndTime = "11:59:00";
			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String ActualTimings = ActualValue.substring(11, 19);
				System.out.println(ActualTimings);
				if ((ConsoleTime.compareTo(StartedTime) > 0) || ((ConsoleTime.compareTo(EndTime) < 0))) {
					Reporter.log("PASS >> Report get generated successfully in the expected time zone", true);
				} else {
					ALib.AssertFailMethod("FAIL >> Report not get generated successfully in the expected time zone");
				}
			}
		}
	 
	 public void VarifyingYesterDateInViewReport_SystemLog() throws InterruptedException {
			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[2]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate7 = dateFormat.format(date);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date todate = cal.getTime();
			String fromdate = dateFormat.format(todate);
			System.out.println("Actual yesterdayDate:" + fromdate + " ");

			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String datedate = ActualValue.substring(0, 10);
				value = datedate.equals(fromdate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for Yesterday";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for Yesterday date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

	 
	 @FindBy(xpath = ("//*[@id='custRpt_formHolder']/div[1]/article[2]/div[2]/div/div/div[1]/div[1]/div/input[2]"))
		private WebElement CurrentdateInCustomReport;
	 public void SelectYesterdayDateInCustomReport() throws InterruptedException {
			CurrentdateInCustomReport.click();
			waitForXpathPresent("//li[text()='Custom Range']");
			sleep(3);
			Initialization.driver.findElement(By.xpath("(//li[text()='Yesterday'])[last()]")).click();
			Reporter.log("Selected the yesterday in Custom report", true);
		}
	
	 @FindBy(xpath = ("(//div[text()='My Device'])[1]"))
		private WebElement aliasename;
	 public void CheckingAlisaNameForDeviceNameCol() throws InterruptedException {
			String devname = aliasename.getText();
			System.out.println(devname);
			boolean flag;
			if (devname.equals(Config.GroupByNameAsDeviceName)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassStmt = "PASS >>Alias name is displaying instead of predefined name for the column in the generated report.";
			String FailStmt = "Fail >> Alias name is not displaying instead of predefined name for the column in the generated report.";
			ALib.AssertFalseMethod(flag, PassStmt, FailStmt);
		}
	 
	 @FindBy(xpath = "(//span[@class='drp-selected'])[last()]")
		private WebElement datetime;
		public String ExpectedDateAndTime;

		public void LocalDateAndTimeFormat() {
			LocalDateTime myDateObj = LocalDateTime.now();
			DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd/MM/yyyy h:mm a");
			String formattedDate = myDateObj.format(myFormatObj);
			DateTimeFormatter StartingDateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			String formattedDate1 = myDateObj.format(StartingDateFormat);
			String StartedTime = "12:00";
			String TimeType = "AM";
			String ExpectedFormat = formattedDate1.concat(" ").concat(StartedTime).concat(" ").concat(TimeType);
			ExpectedDateAndTime = ExpectedFormat.concat(" ").concat("-").concat(" ").concat(formattedDate);
			System.out.println(ExpectedDateAndTime);
		}
		
	 public void GetDateAndTimeFormat() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("//div[@class='blockContWrapper']/div/div/div/input[2]")).click();
			sleep(2);
			String ActualValue1 = datetime.getText();
			System.out.println(ActualValue1);
			LocalDateAndTimeFormat();
			if (ExpectedDateAndTime.contains(ActualValue1)) {
				Reporter.log("Date and time is displaying with Expected Format", true);
			} else {
				Reporter.log("Date and time is not displaying with Expected Format", true);
			}
		}

	 @FindBy(xpath = "//div[text()='Name']")
		private WebElement NamecolsInCustRep;

		@FindBy(xpath = "//div[text()='Description']")
		private WebElement DescripcolsInCustRep;

		@FindBy(xpath = "//div[text()='Created Date']")
		private WebElement CreatedDatecolsInCustRep;
	 public void VerifyColumnsInCustReport() throws InterruptedException {
			String actual = NamecolsInCustRep.getText();
			String expected = Config.NameCol;
			String PassStatement = "PASS>> Name Column " + expected + " displayed successfully";
			String FailStatement = "FAIL >>Name Column wrongly displayed";
			ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			String actual1 = DescripcolsInCustRep.getText();
			String expected1 = Config.Descrip;
			String PassStatement1 = "PASS>> Name Column " + expected1 + " displayed successfully";
			String FailStatement1 = "FAIL >>Name Column wrongly displayed";
			ALib.AssertEqualsMethod(expected1, actual1, PassStatement1, FailStatement1);

			String actual2 = CreatedDatecolsInCustRep.getText();
			String expected2 = Config.CreatedDate;
			String PassStatement2 = "PASS>> Name Column " + expected2 + " displayed successfully";
			String FailStatement2 = "FAIL >>Name Column wrongly displayed";
			ALib.AssertEqualsMethod(expected2, actual2, PassStatement2, FailStatement2);
		}
	 
	 public void SearchingReportInCustomReportsList(String val) throws InterruptedException {
			SendReportToDelete.sendKeys(val);
			waitForXpathPresent("//table[@id='customReport-listTable']/tbody/tr");
			sleep(3);
			Initialization.driver.findElement(By.xpath("(//table[@id='customReport-listTable']/tbody/tr)[last()]")).click();
			sleep(1);

	 }
	 
	 public void CheckingCustomreportAtSubUserAccount() {
			String CurrentDeviceName = DeviceNameInReport.getText();
			boolean flag;
			if (CurrentDeviceName.equals(Config.Device_Name)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassStatement = "PASS>> Created All custom report in super user account is visible in sub user also";
			String FailStatement = "FAIL>> Created All custom report in super user account is not visible in sub user also";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}

	 
	 @FindBy(xpath = ("//input[@id='isPublicCustomReport']"))
	private WebElement AllowAcceToUser;
	 public void ClickOnAllowAccessToUSerCheckBox() {
			AllowAcceToUser.click();
		}
	 
		@FindBy(xpath = "//span[contains(text(),'Selected custom report is deleted.')]")
		private WebElement delectreport;
	 public void VarifyDeletedReport() throws InterruptedException {
			boolean check = delectreport.isDisplayed();
			String PassStatement = "PASS >> User able to delete created custom report";
			String FailStatement = "FAIL >>User not able to delete created custom report";
			ALib.AssertTrueMethod(check, PassStatement, FailStatement);
		}
	 
		@FindBy(xpath = ("//section[@id='customiseReport_Sect']/div[1]/div[1]/div//input[1] "))
		private WebElement SendReportToDelete;
	 public void DeleteReport(String val) throws InterruptedException {
			SendReportToDelete.sendKeys(val);
			waitForXpathPresent("//table[@id='customReport-listTable']/tbody/tr");
			sleep(3);

			Initialization.driver.findElement(By.xpath("(//table[@id='customReport-listTable']/tbody/tr)[last()]")).click();

			Initialization.driver.findElement(By.xpath("//div[@id='deleteCustomRptBtn']")).click();
			waitForXpathPresent("(//button[@class='btn smdm_btns smdm_grn_clr addbutton buttonwidth'])[7]");
			sleep(5);

			Initialization.driver.findElement(By.xpath("//div[@id='ConfirmationDialog']/div[1]/div[1]/div[2]/button[2]"))
					.click();
			waitForXpathPresent("//span[contains(text(),'Selected custom report is deleted.')]");
			sleep(3);
		}

	 public void CompareValueInBothTableAfterModify() throws InterruptedException {
			List<WebElement> DeviceDetails_List = Initialization.driver
					.findElements(By.xpath("//li[@class='list-group-item node-selected_tablesList_tree']"));
			System.out.println(">> Number of tables in tablelists:" + DeviceDetails_List.size() + "  ");

			boolean flag;
			if (DeviceDetails_List.size() == Config.LenthOfTheDeviceList_AfterModifyClickOnModify) {
				flag = true;
			} else {
				flag = false;
			}
			String PassSmt = "PASS >> Both the sizes are matching in table list and selected list After modifying";
			String Failsmt = "FAIL >> Sizes Are Not MAtching";
			ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
			waitForXpathPresent("//span[contains(text(),'Custom Reports')]");
			sleep(2);
		}

	 public void ClickOnModifyButton() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("(//table[@id='customReport-listTable']/tbody/tr)[last()]")).click();
			sleep(1);
			Initialization.driver.findElement(By.xpath("//div[@id='modifyCustomRptBtn']")).click();
			sleep(1);
		}

		@FindBy(xpath = ("(//button[text()='Update'])[last()]"))
		private WebElement Updatebtn;
	 public void ClickOnUpdateButton() throws InterruptedException {
			Updatebtn.click();
			Reporter.log("PASS >> Clicking on Update button", true);
		}
	 
	 public void CheckingValueSortedOrNot() {
			List<WebElement> CustomColumn = Initialization.driver
					.findElements(By.xpath("//table[@id='devicetable']/tbody/tr/td[1]"));
			for (int i = 0; i < CustomColumn.size(); i++) {
				ArrayList<String> a = new ArrayList<>();
				a.add(CustomColumn.get(i).getText());
				Collections.sort(a);
			}
			System.out.println("PASS >>The data for that particular field is sorted as per user selected option");
		}

	
	 public void ClickOnGenerateReport() throws InterruptedException
	 {
		 generateReportButton.click();
		 sleep(15);
	 }
	  public void ClickOnAddButtonCustomReport() throws InterruptedException
	  {
		  AddButtonCustomReport.click();
		  sleep(2);
		  
	 }
	  
	  public void ClickOnSearchReportButtonInsidedViewedReport_CColumn() throws InterruptedException {
			ClickOnSearchButtonInViewedReport.sendKeys(Config.SendAliasNameAsTestingdevice);
			sleep(3);
			waitForXpathPresent("//p[contains(text(),'Testing device')]");
			sleep(3);
			ClickOnSearchButtonInViewedReport.clear();
		}

	  
	  @FindBy(xpath = ("//p[contains(text(),'Testing device')]"))
		private WebElement CcolumnName;
		public void CheckingCcolumnNameValue(String val) {
			String report = CcolumnName.getText();
			System.out.println(">> The Custom Column Value in the generated report:" + report + " ");
			boolean flag;
			if (report.equals(val)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassSmt = "PASS >> Custom columns fields and data is available in custom column reports for that particular group";
			String Failsmt = "FAil >> Custom columns fields and data is not available in custom column reports for that particular group";
			ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
		}
		
	  public void ClickOnOnDemandReport() throws InterruptedException
	    {
		  sleep(4);
	     OnDemandReportOption.click();
	     sleep(4);
	    }
	
		public void VerifyViewLinkIsClicked() {
			String CurrentDeviceName = DeviceNameInReport.getText();
			boolean flag;
			if (CurrentDeviceName.equals(Config.Device_Name)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassSmt = "PASS >> Clicked on Viewlink";
			String Failsmt = "FAil >> Did not Clicked on Viewlink";
			ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
		}
		
		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr/td[40]"))
		private WebElement DeviceOsVersion;
		public void VerifingOsVersion() throws InterruptedException {
			String DeviceVersion = DeviceOsVersion.getText();
			System.out.println(">> Current device OS version is:" + DeviceVersion + " ");
			boolean flag;
			if (DeviceVersion.equals(Config.EnterOSVersion)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassStatement = "PASS >> Os version of device is not equal to 6.0.1";
			String FailStatement = "FAIL >> Os version of device is  equal to 6.0.1";
			ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
		}
		
		@FindBy(xpath = "(//table[@id='devicetable']/tbody/tr/td)[last()]")
		private WebElement DevicelocaleCol;
		public void VerifyDeviceLocaleCol(String DefaultLauncher) {
			String ReportPackageName = DevicelocaleCol.getText();
			boolean Val = ReportPackageName.equals(DefaultLauncher);
			String Pass = "Language code for specific device will be displayed Correctly";
			String Fail = "Language code for specific device Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr/td[6]"))
		private WebElement deviceplatform;
		public void CheckingDeviceplatform() {
			String CurrentDeviceplatform = deviceplatform.getText();

			if (CurrentDeviceplatform.equals(Config.Device_Platform)) {
				Reporter.log("PASS >> Device platform in the report is same as in the device grid", true);
			} else {
				Reporter.log("FAIL >> device platform in the report is not same as in the device grid", true);
			}
		}
		
		public void VerifyMACAddressInReport(int columnNumber) throws InterruptedException {
			js.executeScript("arguments[0].scrollIntoView()", Initialization.driver
					.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[" + columnNumber + "]/p")));
			sleep(4);
			String MACInDeviceDetailsReport = Initialization.driver
					.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[" + columnNumber + "]/p")).getText();
			System.out.println(MACInDeviceDetailsReport);
			String val = MACInDeviceDetailsReport.substring(0, 17);
			System.out.println(val);
			if (val.equals(MacAdd)) {
				Reporter.log("PASS>> MACAddress Is Shown Correctly As Shown In Device Grid", true);
			} else {
				ALib.AssertFailMethod("FAIL>> MACAddress Is Not Shown Correctly As Shown In Device Grid");
			}
		}
		
		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr/td[9]"))
		private WebElement BatteryPercentage;
		public void VerifingBatteryPercentage() throws InterruptedException {
			try {
				String DeviceBatteryPercentage = BatteryPercentage.getText();
				int num = Integer.parseInt(DeviceBatteryPercentage);

				boolean flag;
				if (num >= Config.Battery_Percentage) {
					flag = true;
				} else {
					flag = false;
				}
				String PassStatement = "PASS >> Battery Percentage of device is greater than 5%";
				System.out.println(PassStatement);
			} catch (Exception e) {
				Initialization.driver.close();
				Initialization.driver.switchTo().window(originalHandle);
				sleep(2);
				String FailStatement = "FAIL >> Battery Percentage of device is not greater than 5%";
				ALib.AssertFailMethod(FailStatement);
			}
		}
		
		public static String MacAdd;
		@FindBy(xpath = "(//div[@id='networkinfo-card']/div/p[1])[1]")
		private WebElement MACAddress;

		public void GetMACAddressFromConsole() {
			MacAdd = MACAddress.getText();
			System.out.println(MacAdd);
		}
		
		static int androidPlatformCountInGrid;
		@FindBy(xpath = "(//table[@id='devicetable']/tbody/tr/td[6])[last()]")
		private WebElement PlatformCount;
		public void VerifyingDevicePlatformInHome() {
			String val = PlatformCount.getText();
			int value = Integer.parseInt(val);
			System.out.println("Android platform devices count in the generated report:" + value + " ");
			if (value == androidPlatformCountInGrid || value == androidPlatformCountInGrid - 1
					|| value == androidPlatformCountInGrid + 1 || value == androidPlatformCountInGrid - 2
					|| value == androidPlatformCountInGrid + 2 || value == androidPlatformCountInGrid - 3
					|| value == androidPlatformCountInGrid + 3) {
				System.out.println("PASS >> Platform count in reports and grid matching");
			} else {
				String FailMessage = "FAIL >> Platform count in reports and grid not matching";
				ALib.AssertFailMethod(FailMessage);
			}
		}
		
		@FindBy(xpath = ("//table[@id='devicetable']/tbody/tr/td[7]"))
		private WebElement devicemodel;
		public void VerifyingDevicemodel(String DeviceModel) {
			String DevicemodelName = devicemodel.getText();
			boolean Val = DevicemodelName.equals(DeviceModel);
			String Pass = "DeviceModel Is Showing Correctly";
			String Fail = "DeviceModel Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		@FindBy(id = "all_ava_devices")
		private WebElement AllDevices;
		public void ClickOnAllDevices() throws InterruptedException {
			AllDevices.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(10);
		}
		
		public void androidDevices() throws InterruptedException {
			List<WebElement> androidDevice = Initialization.driver
					.findElements(By.xpath("//i[@class='fa fa-android andr_icn']"));
			androidPlatformCountInGrid = androidDevice.size();
			System.out.println(">> andoid model Devices count in All Groups in device grid: " + androidPlatformCountInGrid);
			waitForXpathPresent("//a[text()='Reports']");
			sleep(3);
		}
		@FindBy(xpath = "//table[@id='devicetable']/tbody/tr/td[6]")
		private WebElement DevicePlatformInReport;
		public void VerifyDevicePlatform(String platform) {
			String ReportPackageName = DevicePlatformInReport.getText();
			boolean Val = ReportPackageName.equals(platform);
			String Pass = "PASS >> Device Platform Is Showing Correctly";
			String Fail = "FAIL >> Device Platform Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		
		@FindBy(xpath = ("(//table[@id='devicetable']/tbody/tr/td[2])[last()]"))
		private WebElement OSname;
		public void VerifyOS() {
			String CurrentOSName = OSname.getText();
			System.out.println(CurrentOSName);

			boolean flag;
			if (CurrentOSName.equals(Config.OS_Name)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassSmt = "PASS >> NAme of the OS in the report is same as in the device grid";
			String Failsmt = "FAil >> NAme of the OS in the report is not same as in the device grid";
			ALib.AssertTrueMethod(flag, PassSmt, Failsmt);
		}
		public void VerifyingDeviceDate() {

			List<WebElement> ls = Initialization.driver
					.findElements(By.xpath("//table[@class='table table-no-bordered table-hover']/tbody/tr/td[12]"));
			if (ls.size() == 0) {
				ALib.AssertFailMethod("Reports Is Empty");
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String todate = dateFormat.format(date);
			System.out.println("Actual date is:" + todate + " ");

			boolean value;
			for (int i = 0; i < ls.size(); i++) {
				String ActualValue = ls.get(i).getText();
				String hello = ActualValue.substring(0, 10);
				System.out.println(hello);
				value = hello.equals(todate);
				if (value == true) {
					String PassMessage = "PASS >> Report get generated successfully for current date";
					System.out.println(PassMessage);
				} else {
					String FailMessage = "FAIL >> Report not generated successfully for current date";
					ALib.AssertFailMethod(FailMessage);
				}
			}
		}

		public void DeletingGeneratedReport(String val) throws InterruptedException {
			SendReportToDelete.sendKeys(val);
			waitForXpathPresent("//table[@id='customReport-listTable']/tbody/tr");
			sleep(3);

			Initialization.driver.findElement(By.xpath("//table[@id='customReport-listTable']/tbody/tr")).click();

			Initialization.driver.findElement(By.xpath("//div[@id='deleteCustomRptBtn']")).click();
			waitForXpathPresent("(//button[@class='btn smdm_btns smdm_grn_clr addbutton buttonwidth'])[7]");
			sleep(2);

			Initialization.driver.findElement(By.xpath("//div[@id='ConfirmationDialog']/div[1]/div[1]/div[2]/button[2]"))
					.click();
			waitForXpathPresent("//span[contains(text(),'Custom Reports')]");
			sleep(2);
			waitForidPresent("deleteDeviceBtn");
			sleep(4);

		}
		
	  public void ModifyReport() throws InterruptedException {
			Initialization.driver.findElement(By.xpath("(//table[@id='customReport-listTable']/tbody/tr)[last()]")).click();
			sleep(1);
			Initialization.driver.findElement(By.xpath("//div[@id='modifyCustomRptBtn']")).click();
			sleep(1);
			Initialization.driver.findElement(By.xpath("(//li[text()='OS'])[2]")).click();
			Reporter.log("PASS >> Clicked on OS Column", true);
			sleep(1);
			Initialization.driver.findElement(By.xpath("//div[@id='removeSelectedNodes']")).click();
			Reporter.log("PASS >> Removed OS Column", true);

		}

	  public void ClickOnSearchReportButton(String reportname) throws InterruptedException {
		    sleep(2);
			ClickOnSearchReport.clear();
			sleep(2);
			ClickOnSearchReport.sendKeys(reportname);
			sleep(3);
			waitForXpathPresent("//p[@class='rpt_nme st-scrollTxt-left Unread']");
		}

		public void AddingFewColInDevHistoryTable() {
			Initialization.driver.findElement(By.xpath("(//li[text()='OS'])[last()]")).click();
			Reporter.log("Clicked on OS Column", true);
			Initialization.driver.findElement(By.xpath("(//li[text()='Release Version'])[last()]")).click();
			Reporter.log("Clicked on Release Version Column", true);
			Initialization.driver.findElement(By.xpath("(//li[text()='Operator'])[last()]")).click();
			Reporter.log("Clicked on Operator Column", true);
		}
		
		@FindBy(xpath = "//li[text()='Device History']")
		private WebElement DevHistory;
		JavascriptExecutor jc = (JavascriptExecutor) Initialization.driver;
		public void VerifyDeviceHistory() throws InterruptedException {
			jc.executeScript("arguments[0].scrollIntoView()", DevHistory);
			boolean OptionPresent1 = DevHistory.isDisplayed();
			String pass1 = "PASS >> Device History Option is displayed";
			String fail1 = "FAIL >> Device History Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent1, pass1, fail1);

			boolean OptionPresent2 = Initialization.driver.findElement(By.xpath("//li[text()='OS']")).isDisplayed();
			String pass2 = "PASS >> OS Option is displayed";
			String fail2 = "FAIL >> OS is not displayed";
			ALib.AssertTrueMethod(OptionPresent2, pass2, fail2);

			boolean OptionPresent3 = Initialization.driver.findElement(By.xpath("//li[text()='Release Version']"))
					.isDisplayed();
			String pass3 = "PASS >> Release Version Option is displayed";
			String fail3 = "FAIL >> Release Version Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent3, pass3, fail3);

			boolean OptionPresent4 = Initialization.driver.findElement(By.xpath("(//li[text()='Operator'])[2]"))
					.isDisplayed();
			String pass4 = "PASS >> Operator Option is displayed";
			String fail4 = "FAIL >> Operator Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent4, pass4, fail4);

			boolean OptionPresent5 = Initialization.driver.findElement(By.xpath("//li[text()='Phone Signal Strength']"))
					.isDisplayed();
			String pass5 = "PASS >> Phone Signal Strength Option is displayed";
			String fail5 = "FAIL >> Phone Signal Strength Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent5, pass5, fail5);

			boolean OptionPresent6 = Initialization.driver.findElement(By.xpath("(//li[text()='SSID'])[2]")).isDisplayed();
			String pass6 = "PASS >> SSID Option is displayed";
			String fail6 = "FAIL >> SSID Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent6, pass6, fail6);

			boolean OptionPresent7 = Initialization.driver.findElement(By.xpath("(//li[text()='Root Status'])[2]"))
					.isDisplayed();
			String pass7 = "PASS >> Root Status Option is displayed";
			String fail7 = "FAIL >> Root Status Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent7, pass7, fail7);

			boolean OptionPresent8 = Initialization.driver.findElement(By.xpath("//li[text()='Knox Status']"))
					.isDisplayed();
			String pass8 = "PASS >> Knox Status Option is displayed";
			String fail8 = "FAIL >> Knox Status Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent8, pass8, fail8);

			boolean OptionPresent9 = Initialization.driver.findElement(By.xpath("//li[text()='SureLock Version Code']"))
					.isDisplayed();
			String pass9 = "PASS >> SureLock Version Code Option is displayed";
			String fail9 = "FAIL >> SureLock Version Code Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent9, pass9, fail9);

			boolean OptionPresent10 = Initialization.driver.findElement(By.xpath("//li[text()='SureFox Version Code']"))
					.isDisplayed();
			String pass10 = "PASS >> SureFox Version Code Option is displayed";
			String fail10 = "FAIL >> SureFox Version Code Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent10, pass10, fail10);

			boolean OptionPresent11 = Initialization.driver.findElement(By.xpath("//li[text()='SureVideo Version Code']"))
					.isDisplayed();
			String pass11 = "PASS >> SureVideo Version Code Option is displayed";
			String fail11 = "FAIL >> SureVideo Version Code Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent11, pass11, fail11);

			boolean OptionPresent12 = Initialization.driver.findElement(By.xpath("//li[text()='Device Date/Time']"))
					.isDisplayed();
			String pass12 = "PASS >> Device Date/Time Option is displayed";
			String fail12 = "FAIL >> Device Date/Time Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent12, pass12, fail12);

			boolean OptionPresent13 = Initialization.driver.findElement(By.xpath("//li[text()='Phone Roaming']"))
					.isDisplayed();
			String pass13 = "PASS >> Phone Roaming Option is displayed";
			String fail13 = "FAIL >> Phone Roaming Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent13, pass13, fail13);

			boolean OptionPresent14 = Initialization.driver.findElement(By.xpath("//li[text()='Phone Number']"))
					.isDisplayed();
			String pass14 = "PASS >> Phone Number Option is displayed";
			String fail14 = "FAIL >> Phone Number Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent14, pass14, fail14);

			boolean OptionPresent15 = Initialization.driver.findElement(By.xpath("(//li[text()='Sim Serial Number'])[2]"))
					.isDisplayed();
			String pass15 = "PASS >> Sim Serial Number Option is displayed";
			String fail15 = "FAIL >> Sim Serial Number Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent15, pass15, fail15);

			boolean OptionPresent16 = Initialization.driver.findElement(By.xpath("//li[text()='CPU Usage Percentage']"))
					.isDisplayed();
			String pass16 = "PASS >> CPU Usage Percentage Option is displayed";
			String fail16 = "FAIL >> CPU Usage Percentage is not displayed";
			ALib.AssertTrueMethod(OptionPresent16, pass16, fail16);

			boolean OptionPresent17 = Initialization.driver.findElement(By.xpath("//li[text()='GPU Usage Percentage']"))
					.isDisplayed();
			String pass17 = "PASS >> GPU Usage Percentage Option is displayed";
			String fail17 = "FAIL >> GPU Usage Percentage Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent17, pass17, fail17);

			boolean OptionPresent18 = Initialization.driver
					.findElement(By.xpath("//li[text()='Battery Temperature (Celcius)']")).isDisplayed();
			String pass18 = "PASS >> Battery Temperature (Celcius) Option is displayed";
			String fail18 = "FAIL >> Battery Temperature (Celcius) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent18, pass18, fail18);

			boolean OptionPresent19 = Initialization.driver.findElement(By.xpath("//li[text()='SureLock Is Licensed']"))
					.isDisplayed();
			String pass19 = "PASS >> SureLock Is Licensed Option is displayed";
			String fail19 = "FAIL >> SureLock Is Licensed Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent19, pass19, fail19);

			boolean OptionPresent20 = Initialization.driver.findElement(By.xpath("//li[text()='SureFox Is Licensed']"))
					.isDisplayed();
			String pass20 = "PASS >> SureFox Is Licensed Option is displayed";
			String fail20 = "FAIL >> SureFox Is Licensed Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent20, pass20, fail20);

			boolean OptionPresent21 = Initialization.driver.findElement(By.xpath("//li[text()='SureVideo Is Licensed']"))
					.isDisplayed();
			String pass21 = "PASS >> SureVideo Is Licensed Option is displayed";
			String fail21 = "FAIL >> SureVideo Is Licensed Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent21, pass21, fail21);

			boolean OptionPresent22 = Initialization.driver.findElement(By.xpath("(//li[text()='Network Type'])[2]"))
					.isDisplayed();
			String pass22 = "PASS >> Network Type Option is displayed";
			String fail22 = "FAIL >> Network Type Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent22, pass22, fail22);

			boolean OptionPresent23 = Initialization.driver
					.findElement(By.xpath("//li[text()='Battery Temperature (Celcius)']")).isDisplayed();
			String pass23 = "PASS >> Battery Temperature (Celcius) Option is displayed";
			String fail23 = "FAIL >> Battery Temperature (Celcius) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent23, pass23, fail23);

			boolean OptionPresent24 = Initialization.driver.findElement(By.xpath("//li[text()='Bluetooth Enabled']"))
					.isDisplayed();
			String pass24 = "PASS >> Bluetooth Enabled Option is displayed";
			String fail24 = "FAIL >> Bluetooth Enabled Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent24, pass24, fail24);

			boolean OptionPresent25 = Initialization.driver.findElement(By.xpath("(//li[text()='USB Status'])[2]"))
					.isDisplayed();
			String pass25 = "PASS >>  USB Status Option is displayed";
			String fail25 = "FAIL >>  USB Status Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent25, pass25, fail25);

			boolean OptionPresent26 = Initialization.driver.findElement(By.xpath("//li[text()='BSSID']")).isDisplayed();
			String pass26 = "PASS >> BSSID Option is displayed";
			String fail26 = "FAIL >> BSSID Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent26, pass26, fail26);

			boolean OptionPresent27 = Initialization.driver.findElement(By.xpath("//li[text()='OS Build Number']"))
					.isDisplayed();
			String pass27 = "PASS >> OS Build Number Option is displayed";
			String fail27 = "FAIL >> OS Build Number Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent27, pass27, fail27);

			boolean OptionPresent28 = Initialization.driver.findElement(By.xpath("//li[text()='GPS Enabled']"))
					.isDisplayed();
			String pass28 = "PASS >> GPS Enabled Option is displayed";
			String fail28 = "FAIL >> GPS Enabled Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent28, pass28, fail28);

			boolean OptionPresent29 = Initialization.driver
					.findElement(By.xpath("//li[text()='Is USB Debugging Disabled']")).isDisplayed();
			String pass29 = "PASS >> Is USB Debugging Disabled Option is displayed";
			String fail29 = "FAIL >> Is USB Debugging Disabled Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent29, pass29, fail29);

			boolean OptionPresent30 = Initialization.driver.findElement(By.xpath("(//li[text()='Serial Number'])[2]"))
					.isDisplayed();
			String pass30 = "PASS >> Serial Number Option is displayed";
			String fail30 = "FAIL >> Serial Number Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent30, pass30, fail30);

			boolean OptionPresent31 = Initialization.driver.findElement(By.xpath("//li[text()='IMEI']")).isDisplayed();
			String pass31 = "PASS >> IMEI Option is displayed";
			String fail31 = "FAIL >> IMEI Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent31, pass31, fail31);

			boolean OptionPresent32 = Initialization.driver.findElement(By.xpath("(//li[text()='Battery Percent'])[2]"))
					.isDisplayed();
			String pass32 = "PASS >> Battery Percent Option is displayed";
			String fail32 = "FAIL >> Battery Percent Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent32, pass32, fail32);

			boolean OptionPresent33 = Initialization.driver
					.findElement(By.xpath("//li[text()='Total Physical Memory (in bytes)']")).isDisplayed();
			String pass33 = "PASS >> Total Physical Memory (in bytes) Option is displayed";
			String fail33 = "FAIL >> Total Physical Memory (in bytes) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent33, pass33, fail33);

			boolean OptionPresent34 = Initialization.driver
					.findElement(By.xpath("//li[text()='Available Physical Memory (in bytes)']")).isDisplayed();
			String pass34 = "PASS >> Available Physical Memory (in bytes) Option is displayed";
			String fail34 = "FAIL >> Available Physical Memory (in bytes) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent34, pass34, fail34);

			boolean OptionPresent35 = Initialization.driver
					.findElement(By.xpath("//li[text()='Total Storage Memory (in bytes)']")).isDisplayed();
			String pass35 = "PASS >> Total Storage Memory (in bytes) Option is displayed";
			String fail35 = "FAIL >> Total Storage Memory (in bytes) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent35, pass35, fail35);

			boolean OptionPresent36 = Initialization.driver
					.findElement(By.xpath("//li[text()='Available Storage Memory (in bytes)']")).isDisplayed();
			String pass36 = "PASS >> Available Storage Memory (in bytes) Option is displayed";
			String fail36 = "FAIL >> Available Storage Memory (in bytes) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent36, pass36, fail36);

			boolean OptionPresent37 = Initialization.driver
					.findElement(By.xpath("//li[text()='Storage Memory Usage (in bytes)']")).isDisplayed();
			String pass37 = "PASS >> Storage Memory Usage (in bytes) Option is displayed";
			String fail37 = "FAIL >> Storage Memory Usage (in bytes) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent37, pass37, fail37);

			boolean OptionPresent38 = Initialization.driver
					.findElement(By.xpath("//li[text()='Physical Memory Usage (in bytes)']")).isDisplayed();
			String pass38 = "PASS >> Physical Memory Usage (in bytes) Option is displayed";
			String fail38 = "FAIL >> Physical Memory Usage (in bytes) Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent38, pass38, fail38);

			boolean OptionPresent39 = Initialization.driver.findElement(By.xpath("//li[text()='Agent Version']"))
					.isDisplayed();
			String pass39 = "PASS >> Agent Version Option is displayed";
			String fail39 = "FAIL >> Agent Version Option is not displayed";
			ALib.AssertTrueMethod(OptionPresent39, pass39, fail39);
		}
		
		//Karthik

}
