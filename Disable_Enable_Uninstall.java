package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Disable_Enable_Uninstall extends Initialization{
	
	@Test(priority=1, description="create a runscript to Disable Uninstall")
	public void ToDisableUninstall() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.ClickDisableUninstall();
		runScript.sendPackageNameORpath("com.mobile.indiapp");
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("DisableUninstall");
		commonmethdpage.ClickOnHomePage();
		
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableUninstall");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("DisableUninstall",60);
		runScript.OpenSettingsPage();
		commonmethdpage.ClickOnHomePage();
		runScript.clickOnApps();
		runScript.checkUninstallDisabled();
		
	}
	@Test(priority=2, description="create a runscript to Enable Uninstall")
	public void ToEnableUninstall() throws InterruptedException, IOException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.ClickOnToEnableUninstall();
		runScript.sendPackageNameORpath("com.mobile.indiapp");
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("EnableUninstall");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableUninstall");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("EnableUninstall",60);
		runScript.OpenSettingsPage();
		commonmethdpage.ClickOnHomePage();
		runScript.clickOnApps();
		runScript.checkEnabledUninstall();
	}
}
