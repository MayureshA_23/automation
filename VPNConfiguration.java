package Profiles_Windows_Testscripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class VPNConfiguration extends Initialization{
	
	@Test(priority='1',description="To verify clicking on VPN configuration") 
    public void VerifyClickingOnVPNConfiguration() throws InterruptedException
	{
        Reporter.log("1.To verify clicking on VPN Confugration",true);
        profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
        profilesWindows.ClickOnVPNConfiguration();
    }
	
	@Test(priority='2',description="To Verify warning message Saving VPN  configuration profile without profile Name")
	public void VerifyWarningMessageOnSavingVPNConfigurationProfileWithoutName() throws InterruptedException{
		Reporter.log("\n2.To Verify warning message Saving VPN configuration profile without profile Name",true);
		profilesWindows.ClickOnConfigureButton_VPNConfigurationProfile();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='3',description="To Verify warning message Saving VPN  configuration Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingVPNConfigurationProfileWithoutAllFielfs() throws InterruptedException{
		Reporter.log("\n3.To Verify warning message Saving VPN Configuration Profile without entering all the fields",true);
		profilesWindows.EnterVPNConfigurationProfileName();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutAllRequiredFields();
		
	}
	
	
	

}
