package Profiles_Windows_Testscripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class PasswordPolicy  extends Initialization{
	
	@Test(priority='1',description="To verify clicking on password policy") 
    public void VerifyClickingOnPasswordPolicy() throws InterruptedException {
    Reporter.log("1.To verify clicking on password policy",true);
        profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
	
}
	@Test(priority='2',description="To Verify warning message Saving a profile without profile Name")
	public void VerifyWarningMessageOnSavingAProfileWithoutName() throws InterruptedException{
		Reporter.log("\n2.To Verify warning messages while Saving a Password Policy profile",true);
		profilesWindows.clickOnConfigureButton_PasswordPolicy();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='3',description="To Verify Saving password policy profile Name")
	public void VerifySavingPasswordPolicyProfile() throws InterruptedException{
		Reporter.log("\n3.To Verify Saving password policy profile Name");
		profilesWindows.EnterPasswordPolicyProfileName();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.NotificationOnProfileCreated();
	}
	
}
