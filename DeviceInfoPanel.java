package DeviceInfoPanel;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceInfoPanel extends Initialization {

	// Create windows text msg job-JobEnterTimeFence and JobExitTimeFence

	@Test(priority = '1', description = "1.Verify Device Details for Linux device in device info panel.")
	public void TC_1DeviceInfoForLinux()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("1.Verify Device Details for Linux device in device info panel.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchLinuxDevice(Config.DeviceNameLinux);
		deviceinfopanelpage.VerifyDeviceModelName_Linux(Config.Linux_ModelName);// Config.Linux_ModelName
		deviceinfopanelpage.VerifyOperatingSystem(Config.Linux_OS);// Config.Linux_OS
		deviceinfopanelpage.VerifyDeviceName(Config.DeviceNameLinux);// Config.DeviceNameLinux
		deviceinfopanelpage.VerifyAgentVersion(Config.Linux_AgentVersion);// Config.Linux_AgentVersion
//		deviceinfopanelpage.VerifyLastDeviceTime(Config.Linux_LastDeviceTime);// Config.Linux_LastDeviceTime
		deviceinfopanelpage.VerifyAddNotes(Config.Linux_Notes);// Config.Linux_Notes
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelLinux(Config.Linux_CustomCol);// Config.Linux_CustomCol
	}

	@Test(priority = '2', description = "2.Verify Device Details for Windows device in device info panel.")
	public void TC_2DeviceInfoForWindows()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n2.Verify Device Details for Windows device in device info panel.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchWindowsDeviceName(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyDeviceModelName_Windows(Config.WindowsDeviceModelName);
		deviceinfopanelpage.VerifyOperatingSystem_Windows(Config.Windows_OS);
		deviceinfopanelpage.VerifyDeviceNameWindows(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyAgentVersionWindows(Config.Windows_AgentVersion);
//		deviceinfopanelpage.VerifyLastDeviceTimeWindows(Config.Windows_LastDeviceTime);
		deviceinfopanelpage.VerifyAddNotesForWindows(Config.Windows_AddNotes);
		deviceinfopanelpage.VerifyLocTrackingForWindows(Config.Windows_LocTracking);
		deviceinfopanelpage.VerifyTelecomManagementForWindows(Config.Windows_Telecom);
		deviceinfopanelpage.VerifyGeoFenceForWindows(Config.Windows_GeoFence);
		deviceinfopanelpage.VerifyTimeFenceForWindows(Config.Windows_TimeFence);
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelWindows(Config.Windows_CustomCol);

	}

	@Test(priority = '3', description = "3.Verify Device Details for EMM device and MACos in device info panel.")
	public void TC_3DeviceInfoForEMMWindows()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n3.Verify Device Details for EMM device and MACos in device info panel.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice_WindowsEMM(Config.Windows_DeviceNameEMM);
		deviceinfopanelpage.VerifyDeviceModelName_Windows(Config.WinEMM_DeviceModelName);
		deviceinfopanelpage.VerifyOperatingSystem_WindowsEMM(Config.WinEMM_OS);
		deviceinfopanelpage.VerifyDeviceNameWindows(Config.Windows_DeviceNameEMM);
		deviceinfopanelpage.VerifyAgentVersionWindows(Config.WindowsEMM_AgentVersion);
//		deviceinfopanelpage.VerifyLastDeviceTimeWindows(Config.WindowsEMM_LastDeviceTime);
		deviceinfopanelpage.VerifyLocTrackingForWindowsEMM(Config.WindowsEMM_LocTracking);
		deviceinfopanelpage.VerifyInstalledProfileForWindowsEMM(Config.WindowsEMM_InstalledPro);
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelWindows(Config.WindowsEMM_CustomCol);

		// MAC OS

		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchMacOSDevice(Config.Macos_DeviceName);
		deviceinfopanelpage.VerifyDeviceModelName_Windows(Config.MacOS_DeviceModel);
		deviceinfopanelpage.VerifyOperatingSystem_MAC(Config.MacOS_DeviceOS);
		deviceinfopanelpage.VerifyDeviceNameWindows(Config.Macos_DeviceName);
		deviceinfopanelpage.VerifyAgentVersionWindows(Config.MacOS_NixAgentVerson);
	//	deviceinfopanelpage.VerifyLastDeviceTimeWindows(Config.MacOS_LastDeviceTime);
		deviceinfopanelpage.VerifyAddNotesForWindows(Config.MacOS_AddNotes);
		deviceinfopanelpage.VerifyLocTrackingForWindows(Config.MacOS_LocTracking);
		deviceinfopanelpage.VerifyInstalledProfileForMAC(Config.MacOS_InstalledPro);
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelWindows(Config.MacOS_CustomCol);

	}

	@Test(priority = '4', description = "4.Verify Device Details for IOS in device info panel.")
	public void TC_4DeviceInfoForIos()throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n4.Verify Device Details for IOS in device info panel.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchIOSDevice(Config.iOS_DeviceName);
		deviceinfopanelpage.VerifyDeviceModelName_Windows(Config.IOS_DeviceModel);
		deviceinfopanelpage.VerifyOperatingSystem_MAC(Config.iOS_OS);
		deviceinfopanelpage.VerifyDeviceNameWindows(Config.iOS_DeviceName);
	//	deviceinfopanelpage.VerifyLastDeviceTimeIOS(Config.iOS_LastDeviceTime);
		deviceinfopanelpage.VerifyAddNotesForIOS(Config.iOS_AddNotes);
		deviceinfopanelpage.VerifyLocTrackingForIOS(Config.iOS_LocTracking);
		deviceinfopanelpage.VerifyTelecomManagementForIOS(Config.iOS_Telecom);
		deviceinfopanelpage.VerifyInstalledProfileForIOS(Config.iOS_InstalledPro);
		deviceinfopanelpage.VerifySupervisedForIOS(Config.iOS_Supervised);
		deviceinfopanelpage.VerifyActivationLockBypassCodeForIOS(Config.iOS_ActivatedLockCode);
		deviceinfopanelpage.VerifyCustomColInDeviceinfoPanelWindows(Config.iOS_CustomCol);
	}

	@Test(priority = '5', description = "5.Verify Network Panel for Android device.")
	public void TC_5NetworkPanelAndroidDevice()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n5.Verify Network Panel for Android device.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.VerifyAndroidMACaddress(Config.AndroidMACaddress);
		deviceinfopanelpage.VerifyAndroidIPaddress(Config.AndroidIPaddress);
		deviceinfopanelpage.VerifyAndroidLocalIPaddress(Config.AndroidLocalIPaddress);
		deviceinfopanelpage.VerifyAndroidConnection(Config.AndroidConnection);
		deviceinfopanelpage.VerifyAndroidWifiSignal(Config.AndroidWifiSignal);
		deviceinfopanelpage.VerifyAndroidMobileSignal(Config.AndroidMobileSignal);
    	deviceinfopanelpage.VerifyAndroidIMEI(Config.AndroidIMEI);
		deviceinfopanelpage.VerifyAndroidSerialNumber(Config.AndroidSerialNumber);
		deviceinfopanelpage.VerifyAndroidIMSI(Config.AndroidIMSI);
		deviceinfopanelpage.VerifyAndroidWIFI_SSID(Config.AndroidWIFI_SSID);
		deviceinfopanelpage.VerifyAndroidMobileNetwork(Config.AndroidMobileNetwork);

	}

	@Test(priority = '6', description = "6.Verify Network Panel for Linux device.")
	public void TC_6NetworkPanelLinuxDevice()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n6.Verify Network Panel for Linux device.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchLinuxDevice(Config.DeviceNameLinux);
		deviceinfopanelpage.VerifyAndroidMACaddress(Config.Linux_MACaddress);
		deviceinfopanelpage.VerifyAndroidIPaddress(Config.Linux_IPaddress);
		deviceinfopanelpage.VerifyLinuxConnection(Config.LinuxConnection);

	}

	@Test(priority = '7', description = "7.Enroll windows EMM device to console.")
	public void TC_7NetworkPanelEMMDevice()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n7.Enroll windows EMM device to console.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice_WindowsEMM(Config.Windows_DeviceNameEMM);
		deviceinfopanelpage.VerifyAndroidMACaddress(Config.WindowsEMM_MACaddress);
		deviceinfopanelpage.VerifyEMMIPaddress(Config.EMMIPaddress);
	   deviceinfopanelpage.VerifyAndroidLocalIPaddress("192.168.43.90");
		deviceinfopanelpage.VerifyAndroidConnection(Config.WindowsEMM_Connection);
	}

	@Test(priority = '8', description = "8. Enroll windows device to console.")
	public void TC_8NetworkPanelWindowsDevice()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n8. Enroll windows device to console.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchWindowsDeviceName(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyAndroidMACaddress(Config.Windows_MACaddress);
		deviceinfopanelpage.VerifyWindowsIPaddress(Config.WindowsIPaddress);
		deviceinfopanelpage.VerifyWindowsLocalIPaddress(Config.WindowsLocalIPaddress);
		deviceinfopanelpage.VerifyWindowsConnection(Config.WindowsConnection);
		deviceinfopanelpage.VerifyWindowsWifiSignal(Config.WindowsWifiSignal);
		deviceinfopanelpage.VerifyWindowsMobileSignal(Config.WindowsMobileSignal);
		deviceinfopanelpage.VerifyAndroidIMEI(Config.WindowsIMEI); // Remove comment
		deviceinfopanelpage.VerifyWindowsSerialNumber(Config.WindowsSerialNumber);
	    deviceinfopanelpage.VerifyAndroidIMSI("");
    	 deviceinfopanelpage.VerifyWindowsWIFI_SSID("");
		 deviceinfopanelpage.VerifyAndroidMobileNetwork("");

	}


	@Test(priority = '9', description = "Verify Device Info Panel for Memory Management of Linux device")
	public void VerifyDeviceInfoMemoryManagemntLINUX()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n15.Verify Device Info Panel for Memory Management of Linux device",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchLinuxDevice(Config.DeviceNameLinux);
		deviceinfopanelpage.VerifyMemoryOptn();
	}

	@Test(priority = 'A', description = "Verify  Mac address in EMM Enrollment")
	public void VerifyDeviceInfoMACaddressEMM()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n16.Verify Device Info Panel for Memory Management of Linux device",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice_WindowsEMM(Config.Windows_DeviceNameEMM);
		deviceinfopanelpage.VerifyEMMmacAddress();
	}

	@Test(priority = 'B', description = "Verify Device Info Panel for Memory Management of Windows devices enrolled through EMM")
	public void VerifyDeviceInfoMemoryManagementEMM()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n17.Verify Device Info Panel for Memory Management of Windows devices enrolled through EMM",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice_WindowsEMM(Config.Windows_DeviceNameEMM);
		deviceinfopanelpage.VerifyEMMmemory();
	}

	@Test(priority = 'C', description = "Verify Mac address in Nix Windows enrollment.")
	public void VerifyMacAddressNixWindow()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n18.Verify Mac address in Nix Windows enrollment.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchWindowsDeviceName(Config.WindowsDeviceName);
		deviceinfopanelpage.VerifyMACadressNIxWin(Config.Windows_MACaddress);
	}

	@Test(priority = 'D', description = "Verify Device Info Panel for Memory Management of Android device.")
	public void VerifyMemoryMangementAndroid()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n19.Verify Device Info Panel for Memory Management of Android device.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.MemManagementAndroid();
	}

	@Test(priority = 'E', description = "Verify Sim Serial Number (ICCID) is shown in Device info Panel.")
	public void VerifySimSerialNumberICCIDAndroid()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n20.Verify Sim Serial Number (ICCID) is shown in Device info Panel.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		deviceinfopanelpage.VerifySimSerialNumber(Config.SimSerialNumberAndroid);
	}

	@Test(priority = 'F', description = "Verify Sim Serial Number (ICCID) is shown in Device info Panel only for devices which have Sim inserted in them.")
	public void VerifySimSerialNumberICCIDAndroid2()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n21.Verify Sim Serial Number (ICCID) is shown in Device info Panel only for devices which have Sim inserted in them.",true);
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsoleGroups(Config.DeviceName);
		deviceinfopanelpage.VerifySimSerialNumberForNonInsertedSimAndroidDevice();
	}

	@Test(priority = 'G', description = "Verify If VR devices are displaying in device grid when no other devices are present in console")
	public void VerifyDeviceInfoVRdevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n22.Verify If VR devices are displaying in device grid when no other devices are present in console",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchVRDevice(Config.DeviceNameVR);
		deviceinfopanelpage.VerifyDeviceInfoVR();
	}

	@Test(priority = 'H', description = "Verify Device Info Panel for Memory Management of Windows Mobile device")
	public void VerifyDeviceInfoWindowsMobileDevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n23.Verify Device Info Panel for Memory Management of Windows Mobile device",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchWindowsMobileDeviceName(Config.WindowsMobileDeviceName);
		deviceinfopanelpage.VerifyDeviceInfoForWindowsMobile();
		deviceinfopanelpage.MemManagementWindowsMobile();
	}

	@Test(priority = 'I', description = "Verify DeviceInfo for Things Devices")
	public void VerifyDeviceInfoThingsdevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n24.Verify DeviceInfo for Things Devices",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevicePrinterDevice(Config.DeviceNamePrinter);
		deviceinfopanelpage.DeviceInfo_ThingsScanner_PrinterDevice();
	}

	@Test(priority = 'J', description = "Verify Apply and save button in ThingsInfo for Things DatalogicSmart Battery.")
	public void VerifyDeviceInfoThingsDatalogicSmartBatterydevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n25.Verify Apply and save button in ThingsInfo for Things DatalogicSmart Battery.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDeviceDataLogicDevice(Config.DeviceNameDataLogic);
		deviceinfopanelpage.DATAlogicDevices();

	}

	@Test(priority = 'K', description = "Verify Device icon in Device column under device grid for IoT devices.")
	public void VerifyColourOfIOTandNormalDevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n26.Verify Device icon in Device column under device grid for IoT devices.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDevicePrinterDevice(Config.DeviceNamePrinter);
		deviceinfopanelpage.VerfyDeviceIconInDeviceColumnForIoTdevices();
	}
	// DONT RUN

	
/*	  @Test(priority='R',description="Verify changing the Parent device name to change name of child IoT devices and Verify refreshing Parent device to refresh child device(IoT devices)" ) public void VerifyChangeInChildDeviceNameIOT() throws InterruptedException,EncryptedDocumentException, InvalidFormatException, IOException {
	  androidJOB.ClearSearchFieldConsole(); 
	  commonmethdpage.SearchDevice();
	  deviceinfopanelpage.VerifyChangingParentDeviceNameToChangeNameOfChildIoTdevices();
	  commonmethdpage.ClickOnDynamicRefresh();
	  commonmethdpage.clickOnGridrefreshbutton();
	  androidJOB.ClearSearchFieldConsole();
	  commonmethdpage.SearchDevicePrinterDevice(); deviceinfopanelpage.
	  verifyWithChildIOTdeviceName("TestDonotTouch Handheld Scanner"); }*/
	 
	@Test(priority = 'L', description = "Verify deleting Parent device from device grid without deleting child IoT devices.")
	public void VerifydeletingParentDevice()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n27.Verify deleting Parent device from device grid without deleting child IoT devices.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchParentDevice(Config.ParentDeviceName);
		deviceinfopanelpage.VerifyDeleteParentDeviceErrorMessage();
	}

	@Test(priority = 'M', description = "Verify details of Things DataLogicSmart Battery")
	public void VerifydetailsOfThingsDataLogicSmartBattery()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n28.Verify details of Things DataLogicSmart Battery",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDeviceDataLogicDevice(Config.DeviceNameDataLogic);
		deviceinfopanelpage.VerifyThingsInfo_DataLogicDevice();
	}

	@Test(priority = 'N', description = "Verify Efota registration status column in device grid(For above nougat samsung devices")
	public void VerifyEFOTAcolumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n28.Verify Efota registration status column in device grid(For above nougat samsung devices",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDeviceFOTA(Config.DeviceNameFOTA);
		deviceinfopanelpage.ScrollToColumn("FOTA Registration Status");
		deviceinfopanelpage.VerifyColOfEFOTA();
	}

	@Test(priority = 'O', description = "Verify  Mac address in Dual enrollment.")
	public void VerifyMacAddressDualEnrollment()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n29.Verify  Mac address in Dual enrollment.",true);
		androidJOB.ClearSearchFieldConsole();
		commonmethdpage.SearchDualEnrollmentDeviceName(Config.DualEnrollmentDeviceName);
		deviceinfopanelpage.ClickOnMoreMacAddress();
		deviceinfopanelpage.VerifyDUALenrollmentMACaddress(Config.DualEnrollment_MacAddress,
				Config.DualEnrollment_MacAddress_LocalAreaConnection, Config.DualEnrollment_showMoremacAddress1,
				Config.DualEnrollment_showMoremacAddressLocal, Config.DualEnrollment_showMoremacAddress2,
				Config.DualEnrollment_showMoremacAddressWifi, Config.DualEnrollment_showMoremacAddress3,
				Config.DualEnrollment_showMoremacAddress_BluetoothNetworkConnection);// Config.DualEnrollment_MacAddress,Config.DualEnrollment_MacAddress_LocalAreaConnection,Config.DualEnrollment_showMoremacAddress1,Config.DualEnrollment_showMoremacAddressLocal,Config.DualEnrollment_showMoremacAddress2,Config.DualEnrollment_showMoremacAddressWifi,Config.DualEnrollment_showMoremacAddress3,Config.DualEnrollment_showMoremacAddress_BluetoothNetworkConnection
	}


}
