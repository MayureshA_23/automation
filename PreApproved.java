package PreApproved_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;

public class PreApproved extends Initialization{

	@Test(priority=1,description="Verify Confirmation prompt when user clicks on force delete in the Pending delete") 
	public void VerifyConfirmationpromptwhenuserclicksonforcedeleteinthePendingdeleteTC_PD_002() throws Throwable {
		Reporter.log("\n1.Verify Confirmation prompt when user clicks on force delete in the Pending delete",true);		
		PreApproved.VerifyingPreApprovedButton();
		PreApproved.ClickonPendingDelete();
		PreApproved.PromptMessageDisplayed();		
}
	
	@Test(priority=2,description="Verify that user is able to Block enrollment in pre-approved") 
	public void VerifythatuserisabletoBlockenrollmentinpreapprovedTC_DE_097() throws Throwable {
		Reporter.log("\n2.Verify that user is able to Block enrollment in pre-approved",true);		
		PreApproved.VerifyingPreApprovedButton();
		PreApproved.ClickonPreapproved();
		PreApproved.BlockEnrollment();
		PreApproved.ConfirmationMessageVerify(true);
		PreApproved.VerifyBlockEnrollment();
		PreApproved.AllowEnrollment();
		PreApproved.ConfirmationMessageVerify(true);			
}
	
	@Test(priority=3,description="Verify that user is able to Allow enrollment in pre-approved") 
	public void VerifythatuserisabletoAllowenrollmentinpreapprovedTC_DE_098() throws Throwable {
		Reporter.log("\n3.Verify that user is able to Allow enrollment in pre-approved",true);		
		PreApproved.VerifyingPreApprovedButton();
		PreApproved.ClickonPreapproved();
		PreApproved.BlockEnrollment();
		PreApproved.ConfirmationMessageVerify(true);
		PreApproved.AllowEnrollment();
		PreApproved.VerifyAllowEnrollment();
		PreApproved.ConfirmationMessageVerify(true);			
}
	
	@Test(priority=4,description="Verify Importing file with details of macOS/iOS device which is already present in the console") 
	public void VerifyImportingfilewithdetailsofmacOSiOSdevicewhichisalreadypresentintheconsoleTC_DE_096() throws Throwable {
		Reporter.log("\n4.Verify Importing file with details of macOS/iOS device which is already present in the console.",true);		
		PreApproved.VerifyingPreApprovedButton();
		PreApproved.ClickonPreapproved();
		PreApproved.ClickonImportButton();
		PreApproved.browseImportFile("./Uploads/UplaodFiles/Import Preapproved/\\Preapproved.exe");
		PreApproved.ConfirmationErrorMessageVerify(true);		
}
	
	@Test(priority=5,description="Verify Invite User in Preapproved.") 
	public void VerifyInviteUserinPreapprovedTC_DE_076() throws Throwable {
		Reporter.log("\n5.Verify Invite User in Preapproved.",true);		
		PreApproved.VerifyingPreApprovedButton();
		PreApproved.ClickonPreapproved();
		PreApproved.SelectDevicePresent();
		PreApproved.InviteUserVisible();		
		PreApproved.ClickonInviteUser();
		PreApproved.verifyOptionsInInviteUser();
		PreApproved.CloseInviteUserTab();
		PreApproved.SelectDevicePresent();
}
	
	@Test(priority=6,description="Verify Invite User by selecting device in preapproved devices section for Email") 
	public void VerifyInviteUserbyselectingdeviceinpreapproveddevicessectionforEmailTC_DE_077() throws Throwable {
		Reporter.log("\n6.Verify Invite User by selecting device in preapproved devices section for Email",true);
		
		PreApproved.VerifyingPreApprovedButton();
		PreApproved.ClickonPreapproved();
		PreApproved.SelectDevicePresent();
		PreApproved.InviteUserVisible();		
		PreApproved.ClickonInviteUser();		
		PreApproved.InviteUserbyEmailCheckboxSelected();
		PreApproved.InviteUserTabOKButton();
		PreApproved.InviteUserMessage(true);		
}
	
	@Test(priority=7,description="Verify Invite User by selecting device in preapproved devices section for SMS") 
	public void VerifyInviteUserbyselectingdeviceinpreapproveddevicessectionforSMSTC_DE_078() throws Throwable {
		Reporter.log("\n7.Verify Invite User by selecting device in preapproved devices section for SMS",true);
		
		PreApproved.VerifyingPreApprovedButton();
		PreApproved.ClickonPreapproved();
		PreApproved.SelectDevicePresent();
		PreApproved.InviteUserVisible();		
		PreApproved.ClickonInviteUser();		
		PreApproved.InviteUserbySMSCheckboxSelected();
		PreApproved.InviteUserTabOKButton();
		PreApproved.InviteUserMessage(true);		
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
