package SanityTestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class QuickActionToolbar_TestScripts extends Initialization{
	@Test(priority=0,description="Quick Action Tool bar_Apply Device")
	public void ApplyJobWindow() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====1.Quick Action Tool bar_Apply Device=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		quickactiontoolbarpage.VerifyOfApplyJobWindow();
		quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
		
	}
	@Test(priority=1,description="Quick Action Tool bar_Verify Queue")
	public void JobQueueWindow() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====2.Quick Action Tool bar_Verify Queue=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.VerifyOfQueuePage();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}
	@Test(priority=2,description="Quick Action Tool bar_To test turning on Location Tracking.")
	public void LocationTracking() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====3.Quick Action Tool bar_To test turning on Location Tracking.=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
	   	commonmethdpage.SearchDevice(Config.DeviceName);
    	quickactiontoolbarpage.CheckingLocationTrackingStatus();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.ClickingOnTurnOnButton();
    	quickactiontoolbarpage.VerifyOfUpdatedPeridicity("2", "ON [Every 2 Min]");	
	}
	
	@Test(priority=3,description="Verify locate in Quick Action Tool Bar.")
	public void VerifyOfLocate() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====4.Verify locate in Quick Action Tool Bar.=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.CheckingLocationTrackingStatus();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.ClickingOnTurnOnButton();
    	quickactiontoolbarpage.TurningOnLocationTracking("2");
    	quickactiontoolbarpage.WaitingForUpdate(180);	
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.VerifyingDeviceNameInRealTimeLocationTracking(Config.DeviceName);
    	quickactiontoolbarpage.VerifyingDeviceAddressInRealTimeLocationTracking(Config.LoctionTrackingAddress,Config.PinCode);
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();
	}
	@Test(priority=4,description="Verify Blocklist multiple devices.")
	public void BlockListMultipleDevices() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====5.Verify Blocklist multiple devices.=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchMultipleDevice(Config.MultipleDeviceName);
		rightclickDevicegrid.SelectMultipleDevice();
		blacklistedpage.ClickOnBlacklistButton();
		blacklistedpage.ClickOnYesButtonWarningDialogBlacklistForMultipleDevices();
		blacklistedpage.ClickOnBlacklisted();
		blacklistedpage.IstheBlacklistedDevicePresent(Config.DeviceName);
		blacklistedpage.IstheBlacklistedDevicePresent(Config.MultipleDevices1);
		blacklistedpage.SelectMultipleDevicesToWhitelist(Config.DeviceName,Config.MultipleDevices1);
		blacklistedpage.ClickOnWhitelistButton();
		blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
		
	}
	/*@Test(priority=5,description="Quick Action Tool bar_Multiple device deletion")
	public void DeleteMultipleDeviceFromConsole() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====6.Quick Action Tool bar_Multiple device deletion=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchMultipleDevice();
		rightclickDevicegrid.SelectMultipleDevice();
		groupspage.RightClickMultipleDevice();
		rightclickDevicegrid.clicOnDeleteForMultipleDevice();
		dynamicjobspage.ClickOnRebootDeviceYesBtn();
		rightclickDevicegrid.clickOnPendingDelete();
		rightclickDevicegrid.VerifyOfDeletedDeviceInPendingDelete(Config.MultipleDevices1);
		rightclickDevicegrid.VerifyOfDeletedDeviceInPendingDelete(Config.MultipleDevices2);
	}*/
}
