package DynamicJobs_TestScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Utility;

public class Reboot extends Initialization{

	@Test(priority='1',description="1.To Verify Dynamic Reboot Job Popup UI when clicked on No Button")
	public void DynamicRebootJob_TC1() throws InterruptedException, Throwable{
		Reporter.log("\n1.To Verify Dynamic Reboot Job Popup UI when clicked on No Button",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnRebootDevice();
	    dynamicjobspage.VerifyOfRebootDevicePopup();
	    dynamicjobspage.ClickOnRebootDeviceNoBtn();
	    dynamicjobspage.ConfirmationMessageRebootDeviceVerify_False();
		Reporter.log("PASS>> Verification of Dynamic Reboot Job Popup UI when clicked on No Button",true);
	}
	
	@Test(priority='2',description="2.To Verify Dynamic Reboot Job Popup UI when clicked on YES Button")
	public void DynamicRebootJob_TC2() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Dynamic Reboot Job Popup UI when clicked on YES Button",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnRebootDevice();
	    dynamicjobspage.VerifyOfRebootDevicePopup();
	    dynamicjobspage.ClickOnRebootDeviceYesBtn();
	 	dynamicjobspage.VerifyOfRebootActivityLogs();
		Reporter.log("PASS>> Verification of Dynamic Reboot Job Popup UI when clicked on YES Button",true);
	}

	
	/*@Test(priority='3',description="1.Reboot the device continuously")
	public void RebootContinuously() throws InterruptedException, Throwable{
		Reporter.log("\n1.Reboot the device continuously",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();	
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.sendPackageNameORpath(Config.PackageName);
		dynamicjobspage.RebootDevice();
		
		
	}*/
	
	
}
