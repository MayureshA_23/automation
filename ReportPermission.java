package UserManagement_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ReportPermission extends Initialization {
	@Test(priority = '1', description = "1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException {
		Reporter.log("\n1.To Verify roles option is present", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
	}

	@Test(priority = '2', description = "2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException {
		Reporter.log("\n2.To Verify super user is present", true);
		usermanagement.toVerifySuperUsertemplete();
	}

	@Test(priority = '3', description = "3.create arole for user")
	public void CreateRoleReportPermissions() throws InterruptedException {
		Reporter.log("\n3.Create role with reportpermissions", true);
		usermanagement.ReportPermissionRole();
	}
	@Test(priority = '4', description = "4.To create user for Job Permission")
	public void CreateUserForReportPermissions() throws InterruptedException {
		Reporter.log("\n4.Create a user for JobPermission", true);
		usermanagement.CreateUserForPermissions("AutoReportPermission", "42Gears@123", "AutoReportPermission",
				"Report Permission"); // uncomment remove later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoReportPermission", "42Gears@123");
		usermanagement.RefreshCurrentPage();
	}

	@Test(priority = '5', description = "5.To Verify system log For ondemand reports ")
	public void EnabledsystemLogOnDemand() throws InterruptedException {
		Reporter.log("\n5.To Verify system log For ondemand reports ", true);
		usermanagement.ClickOnReport();
		usermanagement.SystemLogOnDemand();
	}

	@Test(priority = '6', description = "6.To Verify Aset Tracking For ondemand reports ")
	public void EnabledAssetTrackingOnDemand() throws InterruptedException {
		Reporter.log("\n6.To Verify system log For ondemand reports ", true);
		usermanagement.AssetTrackingOnDemand();
	}

	@Test(priority = '7', description = "7.To Verify Call log Tracking For ondemand reports ")
	public void EnabledCallLogTrackingOnDemand() throws InterruptedException {
		Reporter.log("\n7.To Verify system log For ondemand reports ", true);
		usermanagement.CallLogTrackingOnDemand();
	}

	@Test(priority = '8', description = "8.To Verify job deployed For ondemand reports ")
	public void EnabledJobDeployedOnDemand() throws InterruptedException {
		Reporter.log("\n8.To Verify Job Deployed For ondemand reports ", true);
		usermanagement.JobsDeployedOnDemand();
	}

	@Test(priority = '9', description = "9.To Verify Installed Job For ondemand reports ")
	public void EnabledInstalledJobOnDemand() throws InterruptedException {
		Reporter.log("9.To Verify Installed job For ondemand reports ", true);
		usermanagement.instlledJobOnDemand();
	}

	@Test(priority = 'A', description = "10.To Verify Device health For ondemand reports ")
	public void EnabledDeviceHealthOnDemand() throws InterruptedException {
		Reporter.log("10.To Verify Device health For ondemand reports ", true);
		usermanagement.DeviceHealthOnDemand();
	}

	@Test(priority = 'B', description = "11.To Verify Device history For ondemand reports ")
	public void EnabledDeviceHistoryOnDemand() throws InterruptedException {
		Reporter.log("11.To Verify Device history For ondemand reports ", true);
		usermanagement.DeviceHistoryOnDemand();
	}

	@Test(priority = 'C', description = "12.To Verify Device history For ondemand reports ")
	public void EnabledDataUsageOnDemand() throws InterruptedException {
		Reporter.log("12.To Verify Device history For ondemand reports ", true);
		usermanagement.DataUsageOnDemand();
	}

	@Test(priority = 'D', description = "13.To Verify Device history For ondemand reports ")
	public void EnabledDeviceConnectedOnDemand() throws InterruptedException {
		Reporter.log("13.To Verify Device history For ondemand reports ", true);
		usermanagement.DeviceConnectedOnDemand();
	}

	@Test(priority = 'E', description = "14.To Verify Device history For ondemand reports ")
	public void EnabledAppVersionOnDemand() throws InterruptedException {
		Reporter.log("14.To Verify Device history For ondemand reports ", true);
		usermanagement.AppVersionOnDemand();
		usermanagement.ComplianceReportOnDemand();
	}
	// ############################# Schedule reports access
	// ################################################

	@Test(priority = 'F', description = "15.To Verify SystemLogs for schedule reports  ")
	public void EnabledSystemLogScheduleReports() throws InterruptedException {
		Reporter.log("15.To Verify Device history For schedule reports ", true);
		usermanagement.ClilckOnScheduleReports();
		usermanagement.SystemLogScheduleReport();
	}

	@Test(priority = 'G', description = "16.To Verify assest tracking For Schedule reports ")
	public void EnabledAssetTrackingScheduleReports() throws InterruptedException {
		Reporter.log("16.To Verify Asset tracking For schedule reports ", true);
		usermanagement.AssestTrackringScheduleReport();
	}

	@Test(priority = 'H', description = "17.To Verify assest tracking For schedule reports ")
	public void CallLogTrackingScheduleReports() throws InterruptedException {
		Reporter.log("17.To Verify Asset tracking For schedule reports ", true);
		usermanagement.CallLogTrackingScheduleReport();
	}
	
	@Test(priority = 'I', description = "18.To Verify Job Deployed For schedule reports ")
	public void JobDeployedScheduleReports() throws InterruptedException {
		Reporter.log("18.To Verify Job Deployed For schedule reports ", true);
		usermanagement.JobsDeployedScheduleReport();
	}

	@Test(priority = 'J', description = "19.To Verify installed  Job For schedule reports ")
	public void installJobScheduleReports() throws InterruptedException {
		Reporter.log("19.To Verify Job Deployed For schedule reports ", true);
		usermanagement.InstalledJobReportScheduleReport();
	}

	@Test(priority = 'K', description = "20.To Verify Device health For schedule reports ")
	public void DeviceHealthScheduleReports() throws InterruptedException {
		Reporter.log("20.To Verify Device health For schedule reports ", true);
		usermanagement.DeviceHealthScheduleReport();
	}

	@Test(priority = 'L', description = "21.To Verify Device History For schedule reports ")
	public void DeviceHisScheduleReports() throws InterruptedException {
		Reporter.log("21.To Verify Device History For schedule reports ", true);
		usermanagement.DeviceHistoryScheduleReport();
	}

	@Test(priority = 'M', description = "22.To Verify Data usage For schedule reports ")
	public void DataUsageScheduleReports() throws InterruptedException {
		Reporter.log("22.To Verify Data usage For schedule reports ", true);

		usermanagement.DataUsageScheduleReport();
	}

	@Test(priority = 'N', description = "23.To Verify Device health For schedule reports ")
	public void DeviceconnectedScheduleReports() throws InterruptedException {
		Reporter.log("23.To Verify Device connected For schedule reports ", true);
		usermanagement.DeviceConnectedScheduleReport();
	}
	// ####################################### DISABLING THE PERMISSIONS AND
	// CHECKING ##############################################

	@Test(priority = 'O', description = "24.To Disable the permission from admin ")
	public void DisablingReportPermissions() throws InterruptedException {
		Reporter.log("24.To Verify Device connected For schedule reports ", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.disablingReportPermission();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoReportPermission", "42Gears@123");
		usermanagement.RefreshCurrentPage();

	}

	@Test(priority = 'P', description = "25.Disabled permission for on demand  ")
	public void DisabledOnDemand() throws InterruptedException {
		Reporter.log("25.To Verify Access denied permisson for ondemand repoerts ", true);
		usermanagement.OnDemandReportsDisabled();
	}

	@Test(priority = 'Q', description = "26.Disabled permission for on demand  ")
	public void DisabledScheduledReports() throws InterruptedException {
		Reporter.log("26.To Verify Access denied permisson for ondemand repoerts ", true);
		usermanagement.ScheduleReportsDisabled();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID, Config.password);
		

	}
	
	//Internal Issues
	
	@Test(priority='R',description="SubUser can see reports generated by other sub users")
	public void VerifySubusercanseereportsgeneratedbyothersubuser() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.ReportPermission);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Automation Allow ReportsGroups Roles");   
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName("Automation Allow ReportsGroups Roles");
		usermanagement.SelectingAllPermissions("Group/Tag/Filter Permissions");
		usermanagement.SelectingAllPermissions("Report Permissions");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.ReportPermission,Config.pwd3,Config.ReportPermission,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Automation Allow ReportsGroups Roles");
		usermanagement.ClickOnCreateUserButton();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Automation Allow ReportsGroups Roles1");
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName("Automation Allow ReportsGroups Roles1");
		usermanagement.SelectingAllPermissions("Group/Tag/Filter Permissions");
		usermanagement.SelectingAllPermissions("Report Permissions");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Automation Allow ReportsGroups Roles1");
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.ReportPermission,Config.pwd3);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		usermanagement.ReadingReportNameGeneratedBySubUser();
		usermanagement.VerifyReportGeneratedBytSubUser("Asset Tracking");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.AutomationUser,Config.pwd3);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		usermanagement.ReadingReportNameGeneratedBySubUser();
		usermanagement.VerifySubUserUnableToSeeReportGeneratedByotherSubUser("Asset Tracking");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID, Config.password);
	}
	
	/*@Test(priority='S',description="Sub user can generate report of all devices on console even though ts assigned with Group permission.")
	public void VerifySubUserCanGenerateReportOnlyForAssignedGroup() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		rightclickDevicegrid.ClickonNewGroup();
		rightclickDevicegrid.CreateNewGroup("##AutomationSubUser");
		Filter.ClickOnAllDevices();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("##AutomationSubUser");
		rightclickDevicegrid.VerifyDeviceMoved();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.ReportPermission);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Automation Allow ReportsGroups Roles");
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName("Automation Allow ReportsGroups Roles");
		usermanagement.SelectingAllPermissions("Report Permissions");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnDeviceGroupSet();
		usermanagement.DeletingExistingDeviceGroupSet("Automation Device Groups Role");
		usermanagement.ClickAddDeviceGroupSet();
		usermanagement.SendingRoleName("Automation Device Groups Role");
		usermanagement.DeselectingAllGroupsInDeviceGroup();
		usermanagement.SelectingGroupNameInDeviceGroup("##AutomationSubUser");
		usermanagement.ClickOnDevieGroupSaveButton("created");
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.ReportPermission,Config.pwd3,Config.ReportPermission,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Automation Allow ReportsGroups Roles");
		usermanagement.AssigningGroupSetToUser("Automation Device Groups Role");
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.ReportPermission,Config.pwd3);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
	}*/

}
