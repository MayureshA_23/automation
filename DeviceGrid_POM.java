package PageObjectRepository;

import java.awt.AWTException;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.tools.ant.taskdefs.Sleep;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.WebDriverCommonLib;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;


public class DeviceGrid_POM extends WebDriverCommonLib {

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	AccountSettings_POM acc=new AccountSettings_POM();

	@FindBy(xpath="//body[@id='masterBody']/ul[2]/li[span[text()='Move to Group']]")
	private WebElement MoveToGroup;

	@FindBy(xpath="//button[text()='Move']")
	private WebElement MoveToGroupOkBtn;
	
	@FindBy(xpath="//*[@id='dataGrid']/tbody/tr[1]")
	private WebElement SelectfirstDeviceInConsole;
	
	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[1]/div[3]/input")
	private WebElement SearchField;

	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[2]/div[1]/table/tbody/tr/td[3]/div/input")
	private WebElement ModelSearch_AdvanceSearch;

	@FindBy(xpath="//p[text()='Client0245']")
	private WebElement SearchedDeviceName1;

	@FindBy(xpath="//p[text()='KKR']")
	private WebElement SearchedDeviceName2;

	@FindBy(xpath="//span[text()='No matching result found.']")
	private WebElement warningMessageForInvalidSearch;

	@FindBy(xpath=".//*[@id='agent_771cefdf-f993-424e-9af5-3c64fb2e49bf']")
	private WebElement AgentVersion; 

	@FindBy(xpath="//p[text()='8.11']")
	private WebElement SureLockVersion;

	@FindBy(xpath="//p[text()='6.76']")
	private WebElement SureFoxVersion;

	@FindBy(xpath="//p[text()='3.09']")
	private WebElement SureVideoVersion;

	@FindBy(xpath=".//*[@id='IMEI_771cefdf-f993-424e-9af5-3c64fb2e49bf']")
	private WebElement IMEINumber_Client0245;

	@FindBy(id="knoxStatus_771cefdf-f993-424e-9af5-3c64fb2e49bf")
	private WebElement KnoxStatus_Client0245;

	//@FindBy(id="modal_fdc83cdd-69be-4f06-b3b0-6ed7df91780a") // sandhya
	//private WebElement DeviceModelAndroid; 

	@FindBy(id="modal_771cefdf-f993-424e-9af5-3c64fb2e49bf") // nexus 5X
	private WebElement DeviceModelAndroid; 

	@FindBy(id="modal_3351e0bc-c188-4cdf-9a6e-0078261a8542") // TOSHIBA PC
	private WebElement DeviceModelWindows;

	@FindBy(id="modal___7d32b5f9-99b2-4e79-a8b9-35542a179f26") // iPhone 6 Plus
	private WebElement DeviceModeliOS;

	@FindBy(id="modal_de779208-d9ff-46eb-8a7b-24cc0b3bc5de") // B150M-D3H-CF Pranay Ubuntu
	private WebElement DeviceModelLinux;

	@FindBy(id="deviceNetwork_771cefdf-f993-424e-9af5-3c64fb2e49bf")
	private WebElement NetworkType;

	@FindBy(id="rootStatus_771cefdf-f993-424e-9af5-3c64fb2e49bf")
	private WebElement RootStatus;

	@FindBy(id="notes_771cefdf-f993-424e-9af5-3c64fb2e49bf")
	private WebElement Notes;

	//@FindBy(id="deviceName_fdc83cdd-69be-4f06-b3b0-6ed7df91780a") //sandhya
	//private WebElement DeviceNameValidation;

	@FindBy(xpath="//p[text()='Client0281']") //model = Nexus 5x
	private WebElement DeviceNameValidation;

	@FindBy(xpath="//p[text()='Client0280']") //model = A0001
	private WebElement DeviceNameValidation1;

	@FindBy(id="phoneRoaming_771cefdf-f993-424e-9af5-3c64fb2e49b")
	private WebElement RoamingStatus; //sandhya

	@FindBy(id="deviceTime_771cefdf-f993-424e-9af5-3c64fb2e49bf")
	private WebElement LastConnected;//sandhya

	@FindBy(id="ldt_771cefdf-f993-424e-9af5-3c64fb2e49bf")
	private WebElement LastDeviceName; //sandhya

	@FindBy(id="temperature_771cefdf-f993-424e-9af5-3c64fb2e49bf")
	private WebElement Temperature; //sandhya

	@FindBy(xpath=".//*[@id='searchOptions_cont']/button")
	private WebElement searchOptions;

	@FindBy(id="advSearchDeviceBtn")
	private WebElement advanceSearchButton;

	@FindBy(xpath="//a[text()='2']")
	private WebElement PageNumber2;

	@FindBy(xpath="//a[text()='3']")
	private WebElement PageNumber3;

	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[2]/div[4]/div[1]/span[2]/span[1]/button")
	private WebElement DevicesPerPageButton;

	@FindBy(xpath="//a[text()='100']")
	private WebElement Select100;

	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[1]/div[1]/div/button")
	private WebElement ColumnDropDown;

	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[1]/div[1]/div/div/ul[1]/li[2]/label/span/input")
	private WebElement UncheckModel;

	@FindBy(id="releaseV_fdc83cdd-69be-4f06-b3b0-6ed7df91780a")//sandhya D2105
	private WebElement OSversionD2105;

	@FindBy(xpath="//p[text()='MotoG']") // XT108
	private WebElement operatorDeviceName;

	@FindBy(id="operator_5df5a816-f9f3-4efc-93c4-cc967354c631") // MotoG XT108
	private WebElement NetworkOperatorRowData;

	@FindBy(xpath=".//*[@id='JobQueueGridContainer']/div[1]/div[2]/div[1]/table/thead/tr/th[6]/div[text()='Status']")
	private WebElement StatusColumn_JobsDeployed;

	@FindBy(xpath=".//*[@id='searchOptions_cont']/button")
	private WebElement  ClickOnSearchRadioButton;

	@FindBy(id="advSearchDeviceBtn")
	private WebElement advanceSearchRadioButton;

	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[2]/div[1]/table/tbody/tr/td[2]/div/input")
	private WebElement AdvSearch_DeviceNameSearchField;

	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[1]/div[3]/input")
	private WebElement DeviceNameSearchField;


	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[2]/div[1]/table/tbody/tr/td[5]/div/input")
	private WebElement AdvSearch_BatterySearchField;

	@FindBy(xpath=".//*[@id='tableContainer']/div[3]/div[2]/div[4]/div[1]/span[2]/span[text()=' devices per page']")
	private WebElement text_DevicesPerPage;





	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;






	public void ClickOnMoveToGroup() throws InterruptedException
	{
		MoveToGroup.click();
		waitForXpathPresent("//button[text()='Move']");
		sleep(5);
		Reporter.log("Clicking on Move To Group by right clicking on Device ,Naviagtes to Move To Group popup",true);
	}

	public void ClickOnMoveToGroupOkBtn() throws InterruptedException
	{
		MoveToGroupOkBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		Reporter.log("Clicking on Move Button in  Move To Group popup, Naviagtes to Home page",true);
	}

	public void MoveToGroup(String GroupName) throws InterruptedException
	{
		WebElement wb = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr/td[@class='DeviceName']/p[text()='" +Config.DeviceName + "']"));
		sleep(5);
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(wb).contextClick(wb).perform();
		sleep(5);
		ClickOnMoveToGroup();
		Initialization.driver.findElement(By.xpath("//div[@id='groupList']/ul/li[text()='" + GroupName + "']")).click();
		ClickOnMoveToGroupOkBtn();
		waitForidPresent("deleteDeviceBtn");
		sleep(8);
	}

	public void SearchDeviceUsing() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		//selecting 4 Apps 
		for(int i=0; i<4;i++)
		{
			if(i==0)
			{

				SearchField.sendKeys(Config.searchWithDeviceName);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Client0245']")));
				boolean isDeviceDisplayed = SearchedDeviceName1.isDisplayed();
				String pass = "PASS >> Searched Device name is displayed";
				String fail = "FAIL >> searched device name is NOT displayed";
				ALib.AssertTrueMethod(isDeviceDisplayed, pass, fail);
				sleep(5);

			}

			if(i==1)
			{
				SearchField.clear();
				SearchField.sendKeys(Config.searchWithModel);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Client0245']")));
				boolean isDeviceDisplayed = SearchedDeviceName1.isDisplayed();
				String pass = "PASS >> Searched Device name is displayed";
				String fail = "FAIL >> searched device name is NOT displayed";
				ALib.AssertTrueMethod(isDeviceDisplayed, pass, fail);
				sleep(5);

			}

			if(i==2)
			{
				SearchField.clear();
				SearchField.sendKeys(Config.searchWithNotes);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Client0245']")));
				boolean isDeviceDisplayed = SearchedDeviceName1.isDisplayed();
				String pass = "PASS >> Searched Device name is displayed";
				String fail = "FAIL >> searched device name is NOT displayed";
				ALib.AssertTrueMethod(isDeviceDisplayed, pass, fail);
				sleep(5);

			}

			if(i==3)
			{
				SearchField.clear();
				SearchField.sendKeys(Config.searchWithIMEI);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Client0245']")));
				boolean isDeviceDisplayed = SearchedDeviceName1.isDisplayed();
				String pass = "PASS >> Searched Device name is displayed";
				String fail = "FAIL >> searched device name is NOT displayed";
				ALib.AssertTrueMethod(isDeviceDisplayed, pass, fail);
				sleep(5);
				SearchField.clear();
				sleep(2);

			}

		}
	}
	public void SearchDeviceUsingIPAddress() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{

		for(int i=0; i<1;i++)
		{
			if(i==0)
			{

				SearchField.sendKeys(Config.searchWithIPAddress);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='No matching result found.']")));
				boolean isdisplayed =true;

				try{
					warningMessageForInvalidSearch.isDisplayed();

				}catch(Exception e)
				{
					isdisplayed=false;

				}

				Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
				Reporter.log("PASS >> 'No matching result found.' is received",true );	
				waitForPageToLoad();
				SearchField.clear();
			}

		}
	}

	public void EnterTwoDevicesNamesBasicSearchField() throws InterruptedException //keeping this as method 
	{
		SearchField.sendKeys(Config.searchTwoDevices);
		sleep(6);
	}

	public void EnterTwoDevicesNamesAvancedSearchField() throws InterruptedException //keeping this as method 
	{
		AdvSearch_DeviceNameSearchField.sendKeys(Config.searchTwoDevices);
		sleep(6);
	}

	//this method is reused in Advanced Search
	public void VerifySearchDeviceWithMulipleDeviceName() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{

		for(int i=0; i<1;i++)
		{
			if(i==0)
			{
				boolean isDeviceDisplayed = DeviceNameValidation.isDisplayed(); //1st device
				String pass = "PASS >> Searched Device name is displayed";
				String fail = "FAIL >> searched device name is NOT displayed";
				ALib.AssertTrueMethod(isDeviceDisplayed, pass, fail);
				sleep(5);

				// Device 
				boolean isDeviceDisplayed1 = DeviceNameValidation1.isDisplayed(); //2nd device
				String pass1 = "PASS >> Searched Device name is displayed";
				String fail1 = "FAIL >> searched device name is NOT displayed";
				ALib.AssertTrueMethod(isDeviceDisplayed1, pass1, fail1);
				sleep(2);
				DeviceNameSearchField.clear();
				sleep(2);
			}


		}
	}

	public void NixAgentVersion() throws InterruptedException
	{
		//SearchField.clear();
		SearchField.sendKeys(Config.searchWithDeviceName);
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Client0245']")));
		boolean isDeviceDisplayed = SearchedDeviceName1.isDisplayed();
		String pass = "PASS >> Searched Device name is displayed";
		String fail = "FAIL >> searched device name is NOT displayed";
		ALib.AssertTrueMethod(isDeviceDisplayed, pass, fail);
		sleep(5);


		String actual = AgentVersion.getText();
		String expected = Config.NixAgentVersion;
		String PassStatement = "PASS>> Nix Agent version of "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Nix Agent version wrongly displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}

	public void IMEINumber()
	{
		String actual = IMEINumber_Client0245.getText();
		String expected = Config.searchWithIMEI;
		String PassStatement = "PASS>> IMEI number "+expected+" displayed successfully";
		String FailStatement = "FAIL >>IMEI number wrongly displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}

	public void KnoxStatus()
	{
		String actual = KnoxStatus_Client0245.getText();
		String expected = Config.KnoxStatus;
		String PassStatement = "PASS>> Knox status "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Knox status wrongly displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}

	public void DeviceModel()  
	{
		String actual = DeviceModelAndroid.getText();
		String expected = Config.searchWithModel; //getting the device model
		String PassStatement = "PASS>> Device Model "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Device Model wrongly displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}
	public void NetworkType()  
	{
		String actual = NetworkType.getText();
		String expected = Config.NetworkType; 
		String PassStatement = "PASS>> Network Type "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Network Type wrongly displayed or Not displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}

	public void RootStatus()
	{
		String actual = RootStatus.getText();
		String expected = Config.RootStatus; 
		String PassStatement = "PASS>> Root Status "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Root Status wrongly displayed or Not displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}

	public void Notes()
	{
		String actual = Notes.getText();
		String expected = Config.Notes; 
		String PassStatement = "PASS>> Notes "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Notes wrongly displayed or Not displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}
	public void DeviceNameValidation()
	{
		//single device search
		String actual = DeviceNameValidation.getText();
		String expected = Config.searchWithDeviceName; //device name validation
		String PassStatement = "PASS>> Device Name "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Device Name wrongly displayed or Not displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

	}

	public void ClearDeviceNameFiledAdvancedSearch()
	{
		DeviceNameSearchField.clear();
	}
	public void RoamingStatus()
	{
		String actual = RoamingStatus.getText();
		String expected = Config.RoamingStatus; //device name validation
		String PassStatement = "PASS>> Roaming Status"+expected+" displayed successfully";
		String FailStatement = "FAIL >>Roaming Status Name wrongly displayed or Not displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}
	public void LastConnected()
	{
		String actual = LastConnected.getText();
		Reporter.log(actual);
	}
	public void LastDeviceTime()
	{
		String actual = LastDeviceName.getText();
		Reporter.log(actual);
	}

	public void SureLockVersion()
	{
		boolean surelockVersion =SureLockVersion.isDisplayed();
		String PassStatement = "PASS>> Surelock version displayed successfully";
		String FailStatement = "FAIL >>SureLock version wrongly displayed" ;
		ALib.AssertTrueMethod(surelockVersion, PassStatement, FailStatement);
	}
	public void SureFoxVersion()
	{
		boolean surelockVersion =SureFoxVersion.isDisplayed();
		String PassStatement = "PASS>> Surelock version displayed successfully";
		String FailStatement = "FAIL >>SureLock version wrongly displayed" ;
		ALib.AssertTrueMethod(surelockVersion, PassStatement, FailStatement);
	}
	public void SureVideoVersion()
	{
		boolean surevideoVersion =SureVideoVersion.isDisplayed();
		String PassStatement = "PASS>> SureVideo version displayed successfully";
		String FailStatement = "FAIL >>SureVideo version wrongly displayed" ;
		ALib.AssertTrueMethod(surevideoVersion, PassStatement, FailStatement);
	}
	public void DeviceTemperature() throws InterruptedException
	{
		String actual = Temperature.getText();
		Reporter.log(actual);
		SearchField.clear();
		sleep(15);

	}
	public void ClickOnSearchOption()
	{
		searchOptions.click();
	}
	public void ClickOnAdvanceSearchButton()
	{
		advanceSearchButton.click();
	}

	// Advance Search device using models of all platforms
	public void AdvanceSearchDeviceUsingModelsofAllPlatforms() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{

		for(int i=0; i<4;i++)
		{
			if(i==0)
			{
				ModelSearch_AdvanceSearch.sendKeys(Config.searchAndroidModel);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='modal_fdc83cdd-69be-4f06-b3b0-6ed7df91780a']")));
				sleep(5);
				String actual = DeviceModelAndroid.getText();
				String expected = Config.searchAndroidModel; //getting the android device model
				String PassStatement = "PASS>> Device Model "+expected+" displayed successfully";
				String FailStatement = "FAIL >>Device Model wrongly displayed" ;
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				sleep(5);

			}

			if(i==1)
			{
				ModelSearch_AdvanceSearch.clear();
				ModelSearch_AdvanceSearch.sendKeys(Config.searchWindowsModel);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='modal_3351e0bc-c188-4cdf-9a6e-0078261a8542']")));
				sleep(4);
				String actual = DeviceModelWindows.getText();
				String expected = Config.searchWindowsModel; //getting the Windows device model
				String PassStatement = "PASS>> Device Model "+expected+" displayed successfully";
				String FailStatement = "FAIL >>Device Model wrongly displayed" ;
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				sleep(5);
			}

			if(i==2)
			{
				ModelSearch_AdvanceSearch.clear();
				ModelSearch_AdvanceSearch.sendKeys(Config.searchiOSModel);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='modal___7d32b5f9-99b2-4e79-a8b9-35542a179f26']")));
				sleep(5);
				String actual = DeviceModeliOS.getText();
				String expected = Config.searchiOSModel; //getting the iOS device model
				String PassStatement = "PASS>> Device Model "+expected+" displayed successfully";
				String FailStatement = "FAIL >>Device Model wrongly displayed" ;
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				sleep(5);

			}
			if(i==3)
			{
				ModelSearch_AdvanceSearch.clear();
				ModelSearch_AdvanceSearch.sendKeys(Config.searchLinuxModel);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='modal___7d32b5f9-99b2-4e79-a8b9-35542a179f26']")));
				sleep(5);
				String actual = DeviceModelLinux.getText();
				String expected = Config.searchLinuxModel; //getting the iOS device model
				String PassStatement = "PASS>> Device Model "+expected+" displayed successfully";
				String FailStatement = "FAIL >>Device Model wrongly displayed" ;
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				ModelSearch_AdvanceSearch.clear(); // clearing the name to get unsearched result
				ModelSearch_AdvanceSearch.sendKeys(Keys.ENTER);

			}

		}
	}
	public void NumberOfDevicesFor20Page() throws InterruptedException
	{

		for(int i=0; i<3;i++)
		{
			if(i==0)
			{
				List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
				int totalDevices = deviceCount.size();
				int expectedDeviceCount = 20;
				String PassStatement2 = "PASS >> Total device count for page 1 is: "+expectedDeviceCount+"  ";
				String FailStatement2 = "FAIL >> Total device count for page 1 is Wrong";
				ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
				sleep(5);

			}
			if(i==1)
			{
				PageNumber2.click();
				WebDriverWait wait = new WebDriverWait(Initialization.driver,20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
				List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
				int totalDevices = deviceCount.size();
				int expectedDeviceCount = 20;
				String PassStatement2 = "PASS >> Total device count for page 2 is: "+expectedDeviceCount+"  ";
				String FailStatement2 = "FAIL >> Total device count for page 2 is Wrong";
				ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
				sleep(5);

			}
			if(i==2)
			{
				PageNumber3.click();
				WebDriverWait wait = new WebDriverWait(Initialization.driver,20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
				List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
				int totalDevices = deviceCount.size();
				int expectedDeviceCount = 20;
				String PassStatement2 = "PASS >> Total device count for page 3 is: "+expectedDeviceCount+"  ";
				String FailStatement2 = "FAIL >> Total device count for page 3 is Wrong";
				ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
				sleep(5);
			}

		}
	}

	public void ClickOnDevicesPerPagebutton()
	{
		DevicesPerPageButton.click();
	}

	public void chooseDevicesPerPage100() throws InterruptedException
	{
		Select100.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver,20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		int expectedDeviceCount = 100;
		String PassStatement2 = "PASS >> Total device count for page is: "+expectedDeviceCount+"  ";
		String FailStatement2 = "FAIL >> Total device count for page is Wrong";
		ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
		sleep(5);
	}

	public void ClickOnColumnDropdown()
	{
		ColumnDropDown.click();

	}

	public void VerifyAllColumnsAvailibilityDeviceGrid() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='tableContainer']/div[3]/div[1]/div[1]/div/div/ul[1]/li/label"));
		int totalColAvailable = count.size();
		int expectedColumnCount = 41;
		String PassStatement2 = "PASS >> Total column count in device grid is: "+(expectedColumnCount-1)+"  ";
		String FailStatement2 = "FAIL >> Total column count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalColAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalColAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void UncheckModelColumn()
	{
		UncheckModel.click();
	}

	public void WarningMessageWhileSearchingDeviceModelWhenModelColumnUnchecked() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{

		for(int i=0; i<1;i++)
		{
			if(i==0)
			{

				SearchField.sendKeys(Config.searchAndroidModel);
				WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
				WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='No matching result found.']")));
				boolean isdisplayed =true;

				try{
					warningMessageForInvalidSearch.isDisplayed();

				}catch(Exception e)
				{
					isdisplayed=false;

				}

				Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
				Reporter.log("PASS >> 'No matching result found.' is received",true );	
				waitForPageToLoad();
				SearchField.clear();
			}
		}
	}

	public void OSVersionofDevices()
	{
		SearchedDeviceName1.click(); //
		String actual = OSversionD2105.getText();
		String expected = Config.d2105; //getting the OS version for d2105
		String PassStatement = "PASS>> Device OS version "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Device OS version wrongly displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

	}

	public void NetworkOperator()
	{
		operatorDeviceName.click(); // Selecting the device
		String deviceName = operatorDeviceName.getText();
		Reporter.log(deviceName);
		String actual = NetworkOperatorRowData.getText();
		String expected = Config.networkOperatorName; //getting Network Operator
		String PassStatement = "PASS>> Device "+deviceName+" Network Operator "+expected+" displayed successfully";
		String FailStatement = "FAIL >>Device "+deviceName+" Network Operator wrongly displayed" ;
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	}

	public void ClickOnWarningSymbolOfAdevice() throws InterruptedException
	{

		//Clicking the warning symbol of the device
		List<WebElement> clickOnwarningSymbolOfaDevice = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[1]"));
		clickOnwarningSymbolOfaDevice.get(9).click(); // clicking on the warning symbol of the first device
		WebDriverWait wait = new WebDriverWait(Initialization.driver,20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("jobqueuetitle")));

	}

	public void GetJobStatusValue() //before and after sorting
	{
		//Getting Total count of jobs applied
		List<WebElement> getjobStatus = Initialization.driver.findElements(By.xpath(".//*[@id='jobQueueDataGrid']/tbody/tr/td[6]"));
		int count = getjobStatus.size(); // Getting count of total jobs applied
		System.out.println(count);
		for(int i=0;i<count;i++){
			String actual =getjobStatus.get(i).getText();
			Reporter.log(actual);
		}


	}

	//***Clicking On the Status Column of JobsDeployed**
	public void ClickOnStatusColumnOfJobsDeployed() throws InterruptedException
	{
		StatusColumn_JobsDeployed.click();
		StatusColumn_JobsDeployed.click();
		sleep(2);
	}

	public void RightClickOnDevice()
	{
		WebElement wb= Initialization.driver.findElement(By.xpath("//p[text()='MotoG']"));
		Actions act=new Actions(Initialization.driver);
		act.moveToElement(wb).contextClick(wb).perform(); 
	}

	public void SortingDeviceName_GetDeviceNames() throws InterruptedException //before and after sorting
	{
		//Getting Total count devices
		List<WebElement> totalDevices = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[2]"));
		int count = totalDevices.size(); // Getting count of devices
		System.out.println(count);
		for(int i=0;i<count;i++){
			String actual =totalDevices.get(i).getText();
			Reporter.log(actual);
		}
		sleep(3);
	}
	public void ClickOnDeviceColumn() throws InterruptedException
	{
		List<WebElement> DeviceColumn = Initialization.driver.findElements(By.xpath(".//*[@id='tableHeader']/tr/th[2]/div[1]"));
		DeviceColumn.get(0).click(); // Clicking on Device Column
		//WebDriverWait wait = new WebDriverWait(Initialization.driver,20);
		//wait.until(ExpectedConditions.elementToBeClickable(By.id("deleteDeviceBtn")));
		sleep(6);
	}

	public void SortingModel_GetModel() throws InterruptedException //before and after sorting
	{
		//Getting Total count devices
		List<WebElement> totalModel = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[3]"));
		int count = totalModel.size(); // Getting count of devices
		System.out.println(count);
		for(int i=0;i<count;i++){
			String actual =totalModel.get(i).getText();
			Reporter.log(actual);
		}
		sleep(3);
	}

	public void ClickOnModelColumn() throws InterruptedException
	{
		List<WebElement> modelColumn = Initialization.driver.findElements(By.xpath(".//*[@id='tableHeader']/tr/th[3]/div[1]"));
		modelColumn.get(0).click(); // Clicking on Model Column
		//WebDriverWait wait = new WebDriverWait(Initialization.driver,20);
		//wait.until(ExpectedConditions.elementToBeClickable(By.id("deleteDeviceBtn")));
		sleep(6);
	}

	public void SortingBluetoothName_GetBluetoothName() throws InterruptedException //before and after sorting
	{
		//Getting Total count devices
		List<WebElement> totalBluetoothName = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[31]"));
		int count = totalBluetoothName.size(); // Getting count of devices
		System.out.println(count);
		for(int i=0;i<count;i++){
			String actual =totalBluetoothName.get(i).getText();
			Reporter.log(actual);
		}
		sleep(3);
	}

	public void ClickOnBluetoothNameColumn() throws InterruptedException
	{
		List<WebElement> BluetoothNameColumn = Initialization.driver.findElements(By.xpath(".//*[@id='tableHeader']/tr/th[28]/div[1]"));
		BluetoothNameColumn.get(0).click(); // Clicking on BluetoothName Column
		//WebDriverWait wait = new WebDriverWait(Initialization.driver,20);
		//wait.until(ExpectedConditions.elementToBeClickable(By.id("deleteDeviceBtn")));
		sleep(6);
	}

	public void VerifySignal() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[12]"));
		int totalSignalAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total Signal count in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total signal count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalSignalAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalSignalAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void VerifyBattery() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[5]"));
		int totalBatteryCountAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total Battery count in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total Battery count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalBatteryCountAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalBatteryCountAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void VerifyIPAddress() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[5]"));
		int totalIPAddressCountAvailable = count.size();
		int expectedIPAddressCount = 20;
		String PassStatement2 = "PASS >> Total IP address count in device grid is: "+expectedIPAddressCount+"  ";
		String FailStatement2 = "FAIL >> Total IP address count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedIPAddressCount, totalIPAddressCountAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalIPAddressCountAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void VerifyCPUUsage() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[24]"));
		int totalCPUusageAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total Battery count in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total Battery count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalCPUusageAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalCPUusageAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void VerifyDataUsage() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[23]"));
		int totalDataUsageAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total Data Usage count in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total Data Usage count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalDataUsageAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalDataUsageAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void VerifySupervised() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[26]"));
		int totalSupervisedAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total supervised count in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total supervised count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalSupervisedAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalSupervisedAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void VerifyGPUUsage() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[24]"));
		int totalGPUUsageAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total GPU usage count in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total GPU usage count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalGPUUsageAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalGPUUsageAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void GetDeviceModel() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[3]"));
		int totalModelAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total Model count in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total Model count in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalModelAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalModelAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}

	public void VerifyCPUusage() throws InterruptedException
	{
		List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[23]"));
		int totalCPUusageAvailable = count.size();
		int expectedColumnCount = 20;
		String PassStatement2 = "PASS >> Total CPU usage in device grid is: "+expectedColumnCount+"  ";
		String FailStatement2 = "FAIL >> Total CPU usage in device grid is Wrong";
		ALib.AssertEqualsMethodInt(expectedColumnCount, totalCPUusageAvailable, PassStatement2, FailStatement2);
		for(int i=0;i<totalCPUusageAvailable;i++){
			String actual =count.get(i).getText();
			Reporter.log(actual);
		}
		sleep(2);
	}


	public void ClickOnDeviceNameSearchFieldAdvSearch() throws InterruptedException
	{
		AdvSearch_DeviceNameSearchField.sendKeys(Config.searchWithDeviceName);
		sleep(6);//due to advance search
	}



	public void TextofTwoRadioButtonsAdvanceSearch(){
		Reporter.log("======Verify text summary of basic search======");
		List<WebElement> textSummary = Initialization.driver.findElements(By.xpath(".//*[@id='searchOptions_cont']/ul/li/label/p"));
		//verifying total parameters

		int totalSummary = textSummary.size();
		int expectedSummary = 2;
		String PassStatement2 = "PASS >> Total summary count is: "+expectedSummary+"  ";
		String FailStatement2 = "FAIL >> Total summary count is Wrong";
		ALib.AssertEqualsMethodInt(expectedSummary, totalSummary, PassStatement2, FailStatement2);

		//verifying parameters one by one
		Reporter.log("========Verify summary of Basic Search and Advances Search one by one=========");
		for(int i=0;i<totalSummary;i++){
			if(i==0){	
				String actual =textSummary.get(i).getText();
				Reporter.log(actual);
				String expected = Config.BasicSearchTextSummary;
				String PassStatement ="PASS >> summary of Basice Search is correct";
				String FailStatement ="FAIL >> summary of Basice Search is incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}


			if(i==1){	
				String actual =textSummary.get(i).getText();
				Reporter.log(actual);
				String expected = Config.AdvancedSearchTextSummary;
				String PassStatement ="PASS >> summary of Advanced Search is correct";
				String FailStatement ="FAIL >> summary of Advanced Search is incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}

	}

	// Single and multiple search
	public void VerifyBatteryAdvancedSearch() throws InterruptedException
	{
		for(int i=0; i<4;i++)
		{
			if(i==0)
			{
				Reporter.log("Total search results for battery % <=20 is:");
				AdvSearch_BatterySearchField.sendKeys(Config.BatteryPercentage); //single search
				sleep(6);
				List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[5]"));
				int totalBatteryCountAvailable = count.size();
				System.out.println("Total search results for battery % <=20 is:" +totalBatteryCountAvailable);
				for(int j=0; j<totalBatteryCountAvailable; j++){
					String actual =count.get(j).getText();
					Reporter.log(actual);
				}

				AdvSearch_BatterySearchField.clear();
				sleep(2);
			}

			if(i==1)
			{
				Reporter.log("Total search results for battery % <=45 is:");
				AdvSearch_BatterySearchField.sendKeys(Config.BatteryPercentage1); //single search
				sleep(6);
				List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[5]"));
				int totalBatteryCountAvailable = count.size();
				System.out.println("Total search results for battery % <=45 is:" +totalBatteryCountAvailable);

				for(int j=0; j<totalBatteryCountAvailable; j++){
					String actual =count.get(j).getText();
					Reporter.log(actual);
				}

				AdvSearch_BatterySearchField.clear();
				sleep(2);
			}

			if(i==2)
			{
				Reporter.log("Total search results for battery % >30 is:");
				AdvSearch_BatterySearchField.sendKeys(Config.BatteryPercentage2);//single search
				sleep(6);
				List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[5]"));
				int totalBatteryCountAvailable = count.size();
				System.out.println("Total search results for battery % >30 is:" +totalBatteryCountAvailable);

				for(int j=0; j<totalBatteryCountAvailable; j++){
					String actual =count.get(j).getText();
					Reporter.log(actual);
				}

				AdvSearch_BatterySearchField.clear();
				sleep(2);
			}


			if(i==3)
			{
				Reporter.log("Total search results for battery % >30,<80 is:");
				AdvSearch_BatterySearchField.sendKeys(Config.BatteryPercentageMultipleValues);//Multiple search
				sleep(6);
				List<WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[5]"));
				int totalBatteryCountAvailable = count.size();
				System.out.println("Total search results for battery % >30,<80 is:" +totalBatteryCountAvailable);

				for(int j=0; j<totalBatteryCountAvailable; j++){
					String actual =count.get(j).getText();
					Reporter.log(actual);
				}

				AdvSearch_BatterySearchField.clear();
				sleep(2);
			}
		}
	}

	public void IsDevicesPerPageTextAvailable()
	{
		boolean isTextDisplayed = text_DevicesPerPage.isDisplayed();
		String pass = "PASS >> devices per page is displayed";
		String fail ="FAIL >> device per page is NOT displayed";
		ALib.AssertTrueMethod(isTextDisplayed, pass, fail);
	}

	public void ScrollDownFrontPage() throws InterruptedException{

		Reporter.log("To verify Scroll down to the last Element of Functionality");

		List<WebElement> ab = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr/td[31]"));

		//js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		WebElement scrollDown = ab.get(5);
		js.executeScript("window.scrollBy(-2000,0).arguments[0].scrollIntoView(true);", scrollDown);
		//js.executeScript("arguments[0].window.scrollBy(2000,0)", scrollDown);
		/*
		boolean isScrolledDown = el.get(0).isDisplayed();
		String pass = "PASS >> Scrolled Down to the last element succesfully";
		String fail = "FAIL >> Failed to Scroll Down to the last element succesfully";
		ALib.AssertTrueMethod(isScrolledDown, pass, fail);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    sleep(2);
		 */
	}

	//Custom Column

	@FindBy(xpath="//div[@title='Columns']")
	private WebElement ColumnsDropDownButton;

	@FindBy(xpath="//span[text()='Custom Column']")
	private WebElement CustomColumnList;

	@FindBy(id="customColumn_modalBtn")
	private WebElement CustomColumnConfigButton;

	@FindBy(xpath="//div[@class='tableContainer ct-noCont-wrapper']/div[1]/div[1]/div[2]/input")
	private WebElement customcolumnSearchField;

	@FindBy(id="deleteCustColNme_btn")
	private WebElement CustomColumnDeleteButton;

	@FindBy(xpath="//p[contains(text(),'want to delete this column')]/parent::div/following-sibling::div/button[text()='Yes']")
	private WebElement CustomColumnDeleteConfirmationYesbutton;

	@FindBy(xpath="//p[contains(text(),'want to delete this column')]/parent::div/following-sibling::div/button[text()='No']")
	private WebElement CustomColumnDeleteConfirmationNobutton;

	@FindBy(id="newCustColNme_btn")
	private WebElement CustomColumnAddButton;

	@FindBy(xpath="//span[text()='New Column Name']/parent::div/input")
	private WebElement CustomColumnNameTextField;

	@FindBy(id="addNewColumn")
	private WebElement AddnewColumnOKButton;

	@FindBy(xpath="//h4[text()='Custom Columns']/parent::div/button[@aria-label='Close']")
	private WebElement CustomColumnClosebutton;

	@FindBy(id="editCustColNme_btn")
	private WebElement CustomColumnmodifybutton;

	
	@FindBy(xpath="//span[text()='Error connecting to server. Please try again.']")
	private WebElement ECSPOPup;
	
	@FindBy(xpath="//div[@id='customColumn_modal']//button[@name='refresh']")
	private WebElement CustomColumnRefreshButton;

	@FindBy(id="CustomColList_search")
	private WebElement CustomColumnListSearchTextField;

	@FindBy(xpath="//span[text()='Installed Application Version']")
	private WebElement CustomColumnInstalledAppVersionList;


	@FindBy(id="custom_applist_search")
	private WebElement installedAppListSearchField;


	public void ClickOnColumnsButton() throws InterruptedException
	{
		
		ColumnsDropDownButton.click();
		sleep(5);
		Reporter.log("Clicked On Custom Columns Button",true);
	}

	public void ClickOncustomColumnList() throws InterruptedException
	{
		CustomColumnList.click();
		waitForidPresent("customColumn_modalBtn");
		sleep(2);
		Reporter.log("Clicked On Custom Columns List Button",true);
	}

	public void ClickOnCustomColumnConfigbutton() throws InterruptedException
	{
		CustomColumnConfigButton.click();
		waitForidPresent("newCustColNme_btn");
		sleep(2);
		Reporter.log("Clicked On Custom Columns Config Button",true);
	}

	public void SearchingCustomColumn(String CustomColumnName) throws InterruptedException
	{
		customcolumnSearchField.clear();
		sleep(2);
		customcolumnSearchField.sendKeys(CustomColumnName);
		sleep(3);
	}

	public void SelectingCustomColumn(String CustomColumnName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//p[text()='"+CustomColumnName+"']")).click();
		sleep(2);
	}
	
	public void VerifyECSPOPUp() throws InterruptedException
	{
try {
		if(ECSPOPup.isDisplayed()) {
			ALib.AssertFailMethod("ECS is displayed" );
		}}catch(Exception e) {
			Reporter.log("ECS is not displayed",true);}

	}

	public void VerifyCustomColumnsAddedSuccessfully(String CustomColumnName) throws InterruptedException
	{
		SearchingCustomColumn(CustomColumnName);
		try
		{
			Initialization.driver.findElement(By.xpath("//p[text()='"+CustomColumnName+"']")).isDisplayed();
			Reporter.log("PASS>>> Custom Column Created Successfully",true);
			sleep(4);
		}

		catch(Exception e)
		{
			ClickOnCustomColumnsCloseButton();
			ALib.AssertFailMethod("FAIL>>> Custom Column Is Not Created");
			sleep(4);
		}
	}

	public void VerifyCustomColumnsModifiedSuccessfully(String ModifiedCustomColumnName) throws InterruptedException
	{
		SearchingCustomColumn(ModifiedCustomColumnName);
		try
		{
			Initialization.driver.findElement(By.xpath("//p[text()='"+ModifiedCustomColumnName+"']")).isDisplayed();
			Reporter.log("PASS>>> Custom Column Modified Successfully",true);
			sleep(4);
		}

		catch(Exception e)
		{
			ClickOnCustomColumnsCloseButton();
			ALib.AssertFailMethod("FAIL>>> Custom Column Is Not Modified");
			sleep(4);
		}
	}

	public void ClickOnCustomColumnDeleteButton() throws InterruptedException
	{
		CustomColumnDeleteButton.click();
		Reporter.log("Clicked On Custom Columns Delete Button",true);
		waitForXpathPresent("//p[contains(text(),'want to delete this column')]");
		sleep(3);

	}

	public void ClickOnCustomColumnDeleteConfirmationYesbutton() throws InterruptedException
	{
		CustomColumnDeleteConfirmationYesbutton.click();
		waitForXpathPresent("//span[text()='Column deleted successfully.']");
		sleep(3);
	}

	public void ClickOnCustomColumnDeleteConfirmationNoButton() throws InterruptedException
	{
		CustomColumnDeleteConfirmationNobutton.click();
		sleep(3);
	}

	public void DeletingExistingCustomColumn(String CustomColumnName) throws InterruptedException
	{
		SearchingCustomColumn(CustomColumnName);
		try
		{
			Initialization.driver.findElement(By.xpath("//p[text()='"+CustomColumnName+"']")).isDisplayed();
			Reporter.log("Custom Column Have been Created Already",true);
			Initialization.driver.findElement(By.xpath("//p[text()='"+CustomColumnName+"']")).click();
			ClickOnCustomColumnDeleteButton();
			ClickOnCustomColumnDeleteConfirmationYesbutton();
		}

		catch (Exception e) 
		{
			Reporter.log("No Custom Solummn Is Found With Searched Name",true);
		}
	}

	public void ClickOnAddCustomColumnButton() throws InterruptedException
	{
		CustomColumnAddButton.click();
		waitForXpathPresent("//span[text()='New Column Name']/parent::div/input");
		sleep(3);
	}

	public void SendingCustomColumnName(String CustomColumnName) throws InterruptedException
	{
		CustomColumnNameTextField.clear();
		sleep(2);
		CustomColumnNameTextField.sendKeys(CustomColumnName);
		sleep(3);
	}

	public void ClickOnAddNewColumnOkButton() throws InterruptedException
	{
		AddnewColumnOKButton.click();
		waitForXpathPresent("//span[text()='New column added successfully.']");
		sleep(7);
	}

	public void AddingNewCustomColumn(String CustomColumnName) throws InterruptedException
	{
		SendingCustomColumnName(CustomColumnName);
		ClickOnAddNewColumnOkButton();
	}
	public void AddingNewCustomColumnWithDotOpAndWrngMsg(String CustomColumnName)throws InterruptedException
	{
		SendingCustomColumnName(CustomColumnName);
		AddnewColumnOKButton.click();
		waitForXpathPresent("//span[text()='The name cannot contain the dot (.) character.']");
		
		boolean surelockVersion =Initialization.driver.findElement(By.xpath("//span[text()='The name cannot contain the dot (.) character.']")).isDisplayed();
		String PassStatement = "PASS>> 'The name cannot contain the dot (.) character.' displayed successfully";
		String FailStatement = "FAIL >>'The name cannot contain the dot (.) character.' is not displayed" ;
		ALib.AssertTrueMethod(surelockVersion, PassStatement, FailStatement);
		
		sleep(6);
	}
	
	public void ClikOnAddNewColumnButtonWhenColmnNameisDot() throws InterruptedException
	{
		AddnewColumnOKButton.click();
		waitForXpathPresent("//span[text()='The name cannot contain the dot (.) character.']");
		sleep(2);
		Reporter.log("Error Message The name cannot contain the dot (.) character. Is displayed Successfully",true);
	}


	@FindBy(xpath="//span[text()='The name cannot contain the dot (.) character.']")
	private WebElement ErrorMessageWhenCustomColHasDot;
	public void VerifyErrorMessagewhenColumnNameIncludesDot() throws InterruptedException
	{
		try
		{
			ErrorMessageWhenCustomColHasDot.isDisplayed();
			Reporter.log("PASS>>> Error Message The name cannot contain the dot (.) character. Is displayed Successfully",true);
		}catch(Exception e)
		{
			ALib.AssertFailMethod("FAIL>>> Error Message The name cannot contain the dot (.) character. Is Not Displayed");
		}
		sleep(6);
	}

	public void ClickOnAddCustomColumnCloseButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='addCustCol_modal']/div/div/div[1]/button")).click();
		sleep(3);
	}

	public void ClickOnCustomColumnsCloseButton() throws InterruptedException
	{
		CustomColumnClosebutton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(6);
	}

	public void ClickOnModifybutton() throws InterruptedException
	{
		CustomColumnmodifybutton.click();
		waitForXpathPresent("//span[text()='New Column Name']/parent::div/input");
		sleep(3);
	}

	public void ModifyingcustomColumn(String NewColumnName,String OldcolumnName) throws InterruptedException
	{
		SearchingCustomColumn(OldcolumnName);
		Initialization.driver.findElement(By.xpath("//p[text()='"+OldcolumnName+"']")).click();
		ClickOnModifybutton();
		CustomColumnNameTextField.clear();
		sleep(2);
		CustomColumnNameTextField.sendKeys(NewColumnName);
		sleep(3);
		AddnewColumnOKButton.click();
	}

	public void VerifyCustomColumnIsNotDeletedonClickingNoButton(String ColumnName) throws InterruptedException
	{
		SearchingCustomColumn(ColumnName);
		try
		{
			Initialization.driver.findElement(By.xpath("//p[text()='"+ColumnName+"']")).isDisplayed();
			Reporter.log("PASS>>> Custom Column Is Not Deleted on Clicking No Button",true);
		}

		catch (Exception e) 
		{
			ClickOnCustomColumnsCloseButton();
			ALib.AssertFailMethod("FAIL>>> Custom Column Is Deleted Even On Clicking No button");
		}
	}

	public void VerifyCustomColumnDeletionOnClickingYesButton(String ColumnName) throws InterruptedException
	{
		try
		{
			SearchingCustomColumn(ColumnName);
			Initialization.driver.findElement(By.xpath("//p[text()='"+ColumnName+"']")).isDisplayed();
			ClickOnCustomColumnsCloseButton();
			ALib.AssertFailMethod("FAIL>>> Custom Column Is Not Deleted Even On Clicking Yes button");
		}
		catch (Exception e) 
		{
			Reporter.log("PASS>>> Custom Column Is  Deleted on Clicking Yes Button",true);
		}
	}

	public void VerifySearchInCustomColumn(String ColumnName)
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//p[text()='"+ColumnName+"']")).isDisplayed();
			Reporter.log("PASS>>> Custom Column Searched Successfully",true);
		}

		catch (Exception e) 
		{
			ALib.AssertFailMethod("FAIL>>> Unable To Search Custom Column");
		}
	}

	public void ClickOnCustomColumnRefreshButton() throws InterruptedException
	{
		CustomColumnRefreshButton.click();
		sleep(3);
		Reporter.log("PASS>>> Clicked On Refresh Button",true);

	}

	public void SearchingCustomColumnInColumnList(String CustomColumnName) throws InterruptedException
	{
		CustomColumnListSearchTextField.clear();
		sleep(2);
		CustomColumnListSearchTextField.sendKeys(CustomColumnName);
		waitForXpathPresent("//span[text()='"+CustomColumnName+"']/parent::label");
		sleep(2);
	}

	public void SelectingCustomColumnCheckBoxInColumnlist(String CustomColumnName) throws InterruptedException
	{
		boolean status=Initialization.driver.findElement(By.xpath("//span[text()='"+CustomColumnName+"']/parent::label/span[1]/input")).isSelected();
		if(status==false)
		{
			Initialization.driver.findElement(By.xpath("//span[text()='"+CustomColumnName+"']/parent::label/span[1]/input")).click();
			Reporter.log("Custom Column Is Selected",true);
			sleep(10);
			ClickOnColumnsButton();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);	    	
		}
		else
		{
			Reporter.log("Custom Column Is Already Selected",true);
		}
	}

	public void VerifySelectedCstomColumnIsPresentInGrid(String ColumnName)
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//th//div[text()='"+ColumnName+"']")).isDisplayed();
			Reporter.log("PASS>>> Selected Column Is Present In Grid",true);
		}catch(Exception e)
		{
			ALib.AssertFailMethod("FAIL>>> Selected Column IS Not Present In Grid");
		}
	}
	
	public void VerifyOfOperatorFormatInstallAppCol() throws InterruptedException
	{
		Actions actions = new Actions(Initialization.driver);
		actions.moveToElement(Initialization.driver.findElement(By.cssSelector("td[title='For example : >20, >=20, <20, <=20, =20, <>20'] span[class='clearBtn fa fa-times-circle']"))).perform();
		sleep(4);
		try {
		Initialization.driver.findElement(By.cssSelector("td[title='For example : >20, >=20, <20, <=20, =20, <>20'] span[class='clearBtn fa fa-times-circle']")).isDisplayed();
		Reporter.log("Pass >> Operator format is shown correctly for installed app column.",true);
		}catch (Exception e) {
			ALib.AssertFailMethod("Fail >> Operator format is not shown correctly for installed app column.");
		}
	}
	public void ClickOnInstalledApplicationVersionList() throws InterruptedException
	{
		CustomColumnInstalledAppVersionList.click();
		waitForidPresent("custom_applist_search");
		sleep(2);
	}

	public void SearchingForApplicationInstalledAppVersionList(String PackageName) throws InterruptedException
	{
		installedAppListSearchField.clear();
		sleep(2);
		installedAppListSearchField.sendKeys(PackageName);
		waitForXpathPresent("//span[contains(text(),'( "+PackageName+"  )')]");
		sleep(2); 
	}

	public void enableInstallAppCheckBox(String PackageName) throws InterruptedException
	{
		if(Initialization.driver.findElement(By.xpath("//span[contains(text(),'( "+PackageName+"  )')]")).isSelected()) {
			Reporter.log("CheckBox is already enabled",true);
		}
		else {
	Initialization.driver.findElement(By.xpath("//span[contains(text(),'( "+PackageName+"  )')]")).click();
	}}
	
	
	public void SelectingApplicationIninstalledAppVersionList(String app) throws InterruptedException
	{
		boolean status=Initialization.driver.findElement(By.xpath("//span[contains(text(),'"+app+"')] /parent::label/span/input")).isSelected();
		if(status==false)
		{
			Initialization.driver.findElement(By.xpath("//span[contains(text(),'"+app+"')] /parent::label/span/input")).click();
			Reporter.log("Installed Application Version Column Is Selected",true);
			sleep(3);
			ClickOnColumnsButton();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);	    	
		}
		else
		{
			Reporter.log("Installed Application Version Column Is Already Selected",true);
		}
	}


	@FindBy(id="isQuniqueColumn")
	private WebElement uniquevalueCheckBox_CustomColumns;

	@FindBy(id="custColumn_val")
	private WebElement LatestCustomColumnValueTextArea;

	@FindBy(id="addCutomColumnValue")
	private WebElement uniqueValueSaveButton;

	public String Message="Value saved successfully.";
	public String ErrorMessage="Value already assigned to another device. Cannot insert duplicate value to a unique column.";

	public void ClickingOnuniqueValueCheckBox() throws InterruptedException
	{
		uniquevalueCheckBox_CustomColumns.click();
		sleep(2);
	}

	public void VerifyUniquevalueColumnAfterCreating(String Value)
	{
		String uniqueval = Initialization.driver.findElement(By.xpath("//table[@id='userCustomColumnTable']/tbody/tr[1]/td[2]")).getText();
		if(uniqueval.equals(Value))
		{
			Reporter.log("PASS>>> Correct Data Is In Custom Column Unique Value Column ",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Correct Data Is Not In Custom Column Unique Value Column");
		}
	}

	public void ClickOnEditButtonOfLatestCustomColumn() throws InterruptedException
	{
		Initialization.driver.findElement(By.id("CustomColumn1_editbtn")).click();
		waitForXpathPresent("//h4[contains(text(),'Enter value')]");
		sleep(2);
	}

	public void AddingValuesToCustomColumn(String Value) throws InterruptedException
	{
		ClickOnEditButtonOfLatestCustomColumn();
		LatestCustomColumnValueTextArea.clear();
		sleep(2);
		LatestCustomColumnValueTextArea.sendKeys(Value);
		sleep(2);
	}

	public void ClickOnSaveButton_CustomColumnValue(String Message) throws InterruptedException
	{
		uniqueValueSaveButton.click();
		waitForXpathPresent("//span[text()='"+Message+"']");
		sleep(3);
	}


	String uniqueVal1;
	String uniqueVal2;
	public void ReadingUniqueValue()
	{
		uniqueVal1= Initialization.driver.findElement(By.xpath("//td[@class='CustomColumn1']")).getText();
	}	
	public void ReadingUniqueValue1()
	{
		uniqueVal2= Initialization.driver.findElement(By.xpath("//td[@class='CustomColumn1']")).getText();
	}

	public void VerifyuniqueValuesAreDifferntForDevice()
	{
		if(uniqueVal1.equals(uniqueVal2))
		{
			ALib.AssertFailMethod("FAIL>>> Unique Values Are Not Different");
		}
	}

	public void VerifyValuesAreSameForDevice()
	{
		if(!uniqueVal1.equals(uniqueVal2))
		{
			ALib.AssertFailMethod("FAIL>>> Values Are Not Same");
		}
	}


	public void VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(String ActualUniqueValue)
	{
		String UniqueValueInDeviceGrid = Initialization.driver.findElement(By.xpath("//td[@class='CustomColumn1']")).getText();
		String UniqueValueInDeviceInfoPanel = Initialization.driver.findElement(By.xpath("//div[@id='CustomColumn1_name']/span")).getText();
		if(UniqueValueInDeviceGrid.equals(ActualUniqueValue)&&UniqueValueInDeviceInfoPanel.equals(ActualUniqueValue))
		{
			Reporter.log("PASS>> Unique Value Is Same In Info Panel And Devie Grid",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Unique Value Is Not Same In Info Panel And Devie Grid");
		}
	}

	String Lowerversion;
	String upperversion;
	public void getValueForLowerInstalledAppVersion()
	{
		String Lower = Initialization.driver.findElement(By.xpath("//td[@class='Application1']")).getText();
		Lowerversion=Lower.substring(7, 11);
	}
	String LowerVerGameZoneApp;
	String upper;
	public void getValueForLowerInstalledAppVersion_GameZone()
	{
		LowerVerGameZoneApp = Initialization.driver.findElement(By.xpath("//td[@class='Application1']/p")).getText();
		System.out.println(LowerVerGameZoneApp);
	}
	public void getValueForUpperGameZoneAppVersion() 
	{
		 upper = Initialization.driver.findElement(By.xpath("//td[@class='Application1']/p")).getText();
		 System.out.println(upper);
	}
	public void compareGameZoneAppversion() throws InterruptedException {
		int val=upper.compareTo(LowerVerGameZoneApp);
		System.out.println(val);
		sleep(3);
		if(val==3)
		{
			Reporter.log("App version Updated in DeviceGrid", true);	
		}else {
			ALib.AssertFailMethod("App version is not Updated in DeviceGrid" );
		}
	}
	public void getValueForUpperInstalledAppVersion()
	{
		 String upper = Initialization.driver.findElement(By.xpath("//td[@class='Application1']")).getText();
		 upperversion=upper.substring(7, 11);
	}
	
	public void compareInstalledAppversion() {
		double version=Double.parseDouble(Lowerversion);  
		double AfterUpgrade=Double.parseDouble(upperversion);  

		if(version<AfterUpgrade) {
			Reporter.log("App version Updated in DeviceGrid", true);
		}else {
			ALib.AssertFailMethod("App version is not Updated in DeviceGrid" );
		}
	}

	public void ClickOnAllDevice()
	{
		List<WebElement> DevicesConsole = Initialization.driver.findElements(By.xpath("//*[@id='dataGrid']/tbody/tr"));
		Actions action = new Actions(Initialization.driver);
		for( WebElement device:DevicesConsole) {
			action.keyDown(Keys.CONTROL).click(device).perform();
		}
	}
	public void ModifyingUniqueValueColumn(String ColumnName) throws InterruptedException
	{
		SearchingCustomColumn(ColumnName);
		Initialization.driver.findElement(By.xpath("//p[text()='"+ColumnName+"']")).click();
		ClickOnModifybutton();
	}

	public void ClickOnOkButtonAftermodifyingUniquevalueColumn() throws InterruptedException
	{
		AddnewColumnOKButton.click();
		waitForXpathPresent("//span[text()='Column details edited successfully.']");
		sleep(2);
	}

	public void DeSelectingCustomColumn(String CustomColumnName) throws InterruptedException
	{

		ClickOnColumnsButton();
		ClickOncustomColumnList();
		SearchingCustomColumnInColumnList(CustomColumnName);
		boolean status=Initialization.driver.findElement(By.xpath("//span[text()='"+CustomColumnName+"']/parent::label/span[1]/input")).isSelected();
		if(status==false)
		{
			Reporter.log("Custom Column Is Already DeSelected",true);
		}
		else
		{
			Initialization.driver.findElement(By.xpath("//span[text()='"+CustomColumnName+"']/parent::label/span[1]/input")).click();
			Reporter.log("Custom Column Is DeSelected",true);
			sleep(10);
			ClickOnColumnsButton();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
		}
		sleep(3);
	}

	public void VerifyNoErrorMessageOnAddingSameValues(String Message)
	{
		try{
			ClickOnSaveButton_CustomColumnValue(Message);
			ALib.AssertFailMethod("FAIL>>> Error Message IS Shown On Adding Same Value To Non Unique Custom Column");
		}
		catch (Exception e) 
		{
			Reporter.log("PASS>>> No Error Message IS Shown On Adding Same Value To Non Unique Custom Column",true);
		}
	}

	public void VerifynoteMessage()
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//span[contains(text(),'Column already created with non-unique value cannot be modified. Instead, create a new column with unique value.')]")).isDisplayed();
			Reporter.log("PASS>>> Note Message Is Displayed Successfully When Tried To Change Non Unique Column To Unique Value Column",true);
		}

		catch(Exception e)
		{
			ALib.AssertFailMethod("FAIL>>> Note Message Is Not Displayed When Tried To Change Non Unique Column To Unique Value Column");
		}
	}

	public void VerifyCheckBoxStatusWhenTriedtoModifyNonUniqueCCToUniqueCC() throws InterruptedException
	{
		String checkboxstatus = uniquevalueCheckBox_CustomColumns.getAttribute("class");
		if(checkboxstatus.equals("ct-selbox-in disabledclass"))
		{
			Reporter.log("PASS>>> Unable To Click On CheckBox When Tried To Modify Non Unique Column",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Check Box IS In Enabled State When Tried To Modify Non Unique Column");
		}
		AddnewColumnOKButton.click();
		sleep(3);
	}

	public void DisablingAllCustomColumns() throws InterruptedException
	{
		ClickOnColumnsButton();
		ClickOncustomColumnList();

		List<WebElement> CheckBoxes = Initialization.driver.findElements(By.xpath("//div[@id='CustomColList']/ul/li/label/span/input"));
		for(int i=0;i<CheckBoxes.size();i++)
		{

			boolean CheckBoxStatus = CheckBoxes.get(i).isSelected();

			if(CheckBoxStatus)
			{

				CheckBoxes.get(i).click();
				sleep(3);
			}
		}
		sleep(3);
		ClickOnColumnsButton();
	}
	
	
	public void ClickOnDevi() throws InterruptedException
	{
	
	}
	@FindBy(xpath="//button[@class='searchOptListBtn btn btn-default dropdown-toggle']/i[2]")
	private WebElement SearchOptions;

	public void SelectSearchOption(String options) throws InterruptedException {
		sleep(20);
		waitForXpathPresent("//button[@class='searchOptListBtn btn btn-default dropdown-toggle']/i[2]");
		SearchOptions.click();
		Initialization.driver.findElement(By.xpath("//label[contains(text(),'"+options+"')]")).click();
		Reporter.log("Click On Advanced Search",true);
	}

	public void ScrollToColumn(String ColumnName) throws InterruptedException
	{
		WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='"+ColumnName+"']"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		sleep(3);
	}
	public void ReadingToolTipMessage()
	{
		String Tooltipmessage = Initialization.driver.findElement(By.xpath("(//td[@class='CustomColumn1'])[1]")).getAttribute("title");
		if(Tooltipmessage.equals("For example : name1, name2"))
		{
			Reporter.log("PASS>>> Tool Tip Message Is Shown Correctly",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Tool Tip Message Is not shown correctly");
		}

	}
	int NA_Count_BasicSearch=0;
	int NA_Count_AdvanceSearch=0;
	public void ReadingNACountInCustomColumn()
	{
		List<WebElement> ele=Initialization.driver.findElements(By.xpath("//td[@class='CustomColumn1']"));
		for(int i=0;i<ele.size();i++)
		{
			if(ele.get(i).getText().equals("N/A"))
			{
				NA_Count_BasicSearch++;
			}
		}
		System.out.println(NA_Count_BasicSearch);
	}

	public void ReadingNACountInCustomColumn_AdvanceSearch() throws InterruptedException
	{
		sleep(4);
		List<WebElement> ele=Initialization.driver.findElements(By.xpath("//td[@class='CustomColumn1']"));
		for(int i=1;i<ele.size();i++)
		{
			if(ele.get(i).getText().equals("N/A"))
			{
				NA_Count_AdvanceSearch++;
			}
			else
			{
				ALib.AssertFailMethod("FAIL>>> Different Value Is Shown On Searching N/A");
			}
		}
		System.out.println(NA_Count_AdvanceSearch);
	}

	public void VerifySearchingNAInCustomColumn()
	{
		if(NA_Count_BasicSearch==NA_Count_AdvanceSearch)
		{
			Reporter.log("PASS>>> N/A Count Is Shown Correctly On Searching In Custom Column",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> N/A Count Is not Shown Correctly On Searching In Custom Column");
		}
	}



	//////////////////////////ADVANCE SEARCH///////////////////////////////////

	public void VerifyColumnSorting(String ColumnName) throws InterruptedException {
		int i=0;
		sleep(20);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}
		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		//waitForPageToLoad(100);
		sleep(20);

		if (ColumnName.equalsIgnoreCase("Battery")) {//Reverse Sorting for Integers
			List<Integer> JavaReverseSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				JavaReverseSorting.add(Integer.valueOf(columnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
			}
			Collections.sort(JavaReverseSorting, Collections.reverseOrder());
			System.out.println(JavaReverseSorting);
			sleep(5);
			List<WebElement> ColumnReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaReverseSorting.size();j++) {

				if(JavaReverseSorting.get(j).equals(Integer.valueOf(ColumnReverSortedUIValue.get(j).getText().replaceAll("[^a-zA-Z0-9]", "")))) {
					Reporter.log(JavaReverseSorting.get(j)+" Column Value");
				}else {
					ALib.AssertFailMethod(ColumnReverSortedUIValue+"Reversing Sorting of Column has been failed");
				}
			}
		}else {// Reverse Sorting for String
			List<String> JavaReverseSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				JavaReverseSorting.add(columnValue.getText());
			}

			Collections.sort(JavaReverseSorting, Collections.reverseOrder());
			System.out.println(JavaReverseSorting);
			sleep(5);
			List<WebElement> ColumnReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaReverseSorting.size();j++) {
				if(JavaReverseSorting.get(j).equals(ColumnReverSortedUIValue.get(j).getText())) {
					Reporter.log(JavaReverseSorting.get(j)+" Column Value");
				}else {
					ALib.AssertFailMethod(ColumnReverSortedUIValue.get(j).getText() +"Reversing Sorting of Column has been failed");
				}
			}
		}


		//Natural Sorting
		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		sleep(20);
		if (ColumnName.equalsIgnoreCase("Battery")) {//Natural Sorting for Integers
			List<Integer> JavaSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				JavaSorting.add(Integer.valueOf(columnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
			}
			Collections.sort(JavaSorting);
			System.out.println(JavaSorting);
			sleep(5);
			List<WebElement> ColumnSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaSorting.size();j++) {

				if(JavaSorting.get(j).equals(Integer.valueOf(ColumnSortedUIValue.get(j).getText().replaceAll("[^a-zA-Z0-9]", "")))) {
					Reporter.log(JavaSorting.get(j)+" Column Value");
				}else {
					ALib.AssertFailMethod(ColumnSortedUIValue+"Natural Sorting of Column has been failed");
				}
			}
		}else {// Natural Sorting for String
			List<String> JavaSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				JavaSorting.add(columnValue.getText());
			}
			Collections.sort(JavaSorting);
			System.out.println(JavaSorting);
			sleep(5);
			List<WebElement> ColumnSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaSorting.size();j++) {
				if(JavaSorting.get(j).equals(ColumnSortedUIValue.get(j).getText())) {
					Reporter.log(JavaSorting.get(j)+" Column Value");
				}else {
					ALib.AssertFailMethod(ColumnSortedUIValue+"Natural Sorting of Column has been failed");
				}
			}
		}
	}

	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/button")
	private WebElement Pagination;
	@FindBy(xpath="(//a[text()='10'])[1]")
	private WebElement Select10DevicesPagination;
	
	@FindBy(xpath="(//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/ul/li)[last()]")
	private WebElement SelectLastPagination;
	
	public void SelectLastPagination() throws InterruptedException {
		if(Pagination.isDisplayed()) {
			Pagination.click();
			SelectLastPagination.click();
		}}

	public void VerifySortingInPagination(String SelectPagination ) throws InterruptedException {
		if(Pagination.isDisplayed()) {
			Pagination.click();
			sleep(3);
			Initialization.driver.findElement(By.xpath("(//a[text()='"+SelectPagination+"'])[1]")).click();

		}}

	@FindBy(xpath="//*[@id=\"tableContainer\"]/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/ul/li[last()]")
	private WebElement PresentPagination;
	public void VerifyBeforeSortingInPagination( ) throws InterruptedException {
		if(Pagination.isDisplayed()) {
			Pagination.click();
			sleep(3);
			String SelectPagination = PresentPagination.getText();
			Initialization.driver.findElement(By.xpath("(//a[text()='"+SelectPagination+"'])[1]")).click();
		}
	}

	public void selectPage(String PaginationNumber,String ColumnName) throws InterruptedException {	
		if(Pagination.isDisplayed()) {
			sleep(4);
			Initialization.driver.findElement(By.xpath("//a[text()='"+PaginationNumber+"']")).click();
			VerifySortingIsNotChanged(ColumnName);
		}}

	public void selectPageAndVerifySortingSecurityPatch(String PaginationNumber,String ColumnName) throws InterruptedException, ParseException {	
		sleep(1);
		if(Pagination.isDisplayed()) {
			sleep(4);
			Initialization.driver.findElement(By.xpath("//a[text()='"+PaginationNumber+"']")).click();
			VerifySortingIsNotChangedDateTime(ColumnName);
		}}

	public void VerifySortingIsNotChangedDateTime(String ColumnName) throws ParseException, InterruptedException {
		sleep(4);
		int i=0;
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}
		List<Date> listDates = new ArrayList<Date>();
		SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		for(WebElement col:ColumnValue) {
			listDates.add(DATE_FOMAT.parse(col.getText()));
		}
		Collections.sort(listDates, new Comparator<Date>() {
			@Override
			public int compare(Date lhs, Date rhs) {
				if(lhs.getTime()<rhs.getTime()) {
					return -1;
				}else if(lhs.getTime() == rhs.getTime()) {
					return 0;
				}else {
					return 1;
				}
			}
		});

		List<String> JavaSorting = new ArrayList<>();
		SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		for(Date d : listDates) {
			JavaSorting.add(DATE_FOMAT2.format(d));
		}

		List<WebElement> NaturalSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		for(int j=0;j<NaturalSortColumnValue.size();j++) {
			String NaturalSort = NaturalSortColumnValue.get(j).getText();

			SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
			Date NaturalValueDateFormate = DATE_FOMAT3.parse(NaturalSort);
			String NaturalValueStringFormate = DATE_FOMAT3.format(NaturalValueDateFormate);
			if(NaturalValueStringFormate.equals( JavaSorting.get(j))){
				Reporter.log(NaturalValueStringFormate +" Column Value");
			}else {
				Reporter.log(NaturalValueStringFormate +" Column Value");
				//ALib.AssertFailMethod(NaturalValueStringFormate +"Reversing Sorting of Column has been failed");
			}
		}}

	@FindBy(xpath="//a[@id='locationTrackingEditBtn']")
	private WebElement EditLocationButton;

	@FindBy(xpath="//button[@onclick='locationTrackingOkClick()']")
	private WebElement ApplyLocationTracking;

	@FindBy(xpath="//*[@id='locationTrackingModal']/div/div/div[2]/div[1]/div/label")
	private WebElement LocationSwitch;
	public void CheckingLocationTrackingStatus() throws InterruptedException
	{
		if(DeviceINfoLocationTrackingStatus.getText().equals("OFF"))
		{
			Reporter.log("Location Tracking Is OFF ",true);
		}
		else
		{	
			EditLocationButton.click();
			waitForidPresent("isLocationTrackingOn");
			sleep(3);
			LocationSwitch.click();
			ApplyLocationTracking.click();
			sleep(4);
			waitForXpathPresent("//span[text()='Device tracking updated successfully.']");
			Reporter.log("Location Tacking Updated Message Displayed Successfully",true);

		}
	}

	public void clearColValue(String ColumnName) throws InterruptedException {
		//VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).clear();	
	}

	public void VerifyColumnIsPresentOnGridAndScroll(String ColumnName) throws InterruptedException
	{  sleep(2);
	//WebElement scroll = Initialization.driver.findElement(By.xpath("//div[text()='"+ColumnName+"']"));
	WebElement scroll = Initialization.driver.findElement(By.xpath("//div[contains(text(),'"+ColumnName+"')]"));
	js.executeScript("arguments[0].scrollIntoView()", scroll);
	//boolean ColumnNameInGrid = Initialization.driver.findElement(By.xpath("//div[text()='"+ColumnName+"']")).isDisplayed();
	boolean ColumnNameInGrid = Initialization.driver.findElement(By.xpath("//div[contains(text(),'"+ColumnName+"')]")).isDisplayed();

	Assert.assertTrue(ColumnNameInGrid);
	}

	public void VerifySortingIsNotChangedWithNA(String ColumnName) throws InterruptedException{
		sleep(6);
		int i=0;
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}

		if (ColumnName.contains("Version")){
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Double> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {

				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(Double.valueOf(ColumnValue.getText()));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				if(NaturalJavaSorting.get(j).equals(Double.valueOf(NaturalSortedUIValue.get(k).getText()))) {
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}
		}
		else if (ColumnName.contains("Battery Temperature")){
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Double> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					String textvalue = ColumnValue.getText();
					String stringtoAdd=textvalue.substring(0,(textvalue.length()-2));
					NaturalJavaSorting.add(Double.valueOf(stringtoAdd));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {

				String textvalue = NaturalSortedUIValue.get(k).getText();
				String stringtocomp=textvalue.substring(0,(textvalue.length()-2));
				if(NaturalJavaSorting.get(j).equals(Double.valueOf(stringtocomp))) {
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}
		}


		else if (ColumnName.equalsIgnoreCase("CPU Usage")||ColumnName.equalsIgnoreCase("GPU Usage")||ColumnName.equalsIgnoreCase("Cell Signal Strength")||ColumnName.equalsIgnoreCase("Battery")||ColumnName.equalsIgnoreCase("Free Storage Memory")||ColumnName.equalsIgnoreCase("Free Program Memory")) {
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Integer> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(Integer.valueOf(ColumnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				if(NaturalJavaSorting.get(j).equals(Integer.valueOf(NaturalSortedUIValue.get(k).getText().replaceAll("[^a-zA-Z0-9]", "")))) {
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}}

		else if (ColumnName.contains("IMEI")) {
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Long> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(Long.valueOf(ColumnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				if(NaturalJavaSorting.get(j).equals(Long.valueOf(NaturalSortedUIValue.get(k).getText()))) {
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}}
		else {
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<String> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(ColumnValue.getText());
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				if(NaturalJavaSorting.get(j).equals(NaturalSortedUIValue.get(k).getText())) {
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}}
	}

	public void VerifyColumnHaving_NA_Sorting(String ColumnName) throws InterruptedException {
		VerifyBeforeSortingInPagination();
		sleep(10);
		int i=0;
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			String dsds = column.getText();
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}
		Initialization.driver.findElement(By.xpath("//div[contains(text(),'"+ColumnName+"')]")).click();
		sleep(10);
		if ((ColumnName.equalsIgnoreCase("Agent Version"))||(ColumnName.contains("SureLock Version"))) {//Reverse Sorting for Integers
			List<Double> JavaReverseSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				if (!(columnValue.getText().equals("N/A")||columnValue.getText().equals("Not Installed")||columnValue.getText().equals("No Signal"))){
					JavaReverseSorting.add(Double.valueOf(columnValue.getText()));
				}}
			Collections.sort(JavaReverseSorting, Collections.reverseOrder());
			System.out.println(JavaReverseSorting);
			sleep(5);
			List<WebElement> ColumnReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaReverseSorting.size();j++) {

				if(JavaReverseSorting.get(j).equals(Double.valueOf(ColumnReverSortedUIValue.get(j).getText()))) {
					Reporter.log(JavaReverseSorting.get(j)+" Column Value",true);
				}else {
					ALib.AssertFailMethod(JavaReverseSorting.get(j)+"Reversing Sorting of Column has been failed");
				}
			}
		}

		else if (ColumnName.equalsIgnoreCase("Battery Temperature")) {//Reverse Sorting for Integers
			List<Double> JavaReverseSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				if (!(columnValue.getText().equals("N/A"))||(columnValue.getText().equals("Not Installed")||columnValue.getText().equals("No Signal"))) {
					String textvalue = columnValue.getText();
					String stringtoAdd=textvalue.substring(0,(textvalue.length()-2));
					JavaReverseSorting.add(Double.valueOf(stringtoAdd));
				}}
			Collections.sort(JavaReverseSorting, Collections.reverseOrder());
			System.out.println(JavaReverseSorting);
			sleep(5);
			List<WebElement> ColumnReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaReverseSorting.size();j++) {
				if (!(ColumnReverSortedUIValue.get(j).getText().equals("N/A")||ColumnReverSortedUIValue.get(j).getText().equals("Not Installed")||ColumnReverSortedUIValue.get(j).getText().equals("Unknown")||ColumnReverSortedUIValue.get(j).getText().equals("No Signal"))){
					String UIValue = ColumnReverSortedUIValue.get(j).getText();
					String stringtoAdd=UIValue.substring(0,(UIValue.length()-2));
					if(JavaReverseSorting.get(j).equals(Double.valueOf(stringtoAdd))) {
						Reporter.log(JavaReverseSorting.get(j)+" Column Value",true);
					}else {
						ALib.AssertFailMethod(ColumnReverSortedUIValue+"Reversing Sorting of Column has been failed");
					}
				}}
		}

		else if (ColumnName.equalsIgnoreCase("CPU Usage")||ColumnName.equalsIgnoreCase("GPU Usage")||ColumnName.equalsIgnoreCase("Cell Signal Strength")||ColumnName.equalsIgnoreCase("Battery")||ColumnName.equalsIgnoreCase("Free Storage Memory")||ColumnName.equalsIgnoreCase("Free Program Memory")) {//Reverse Sorting for Integers
			List<Integer> JavaReverseSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				if (!(columnValue.getText().equals("N/A"))||(columnValue.getText().equals("Not Installed")||columnValue.getText().equals("No Signal"))) {
					JavaReverseSorting.add(Integer.valueOf(columnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
				}}
			Collections.sort(JavaReverseSorting, Collections.reverseOrder());
			System.out.println(JavaReverseSorting);
			sleep(5);
			List<WebElement> ColumnReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaReverseSorting.size();j++) {

				if(JavaReverseSorting.get(j).equals(Integer.valueOf(ColumnReverSortedUIValue.get(j).getText().replaceAll("[^a-zA-Z0-9]", "")))) {
					Reporter.log(JavaReverseSorting.get(j)+" Column Value",true);
				}else {
					ALib.AssertFailMethod(ColumnReverSortedUIValue+"Reversing Sorting of Column has been failed");
				}
			}
		}

		else if (ColumnName.contains("IMEI")) {//Reverse Sorting for Integers
			List<Long> JavaReverseSorting = new ArrayList<>();
			List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement columnValue : columnValues) {
				if (!(columnValue.getText().equals("N/A"))||(columnValue.getText().equals("Not Installed")||columnValue.getText().equals("No Signal"))) {
					JavaReverseSorting.add(Long.valueOf(columnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
				}}

			Collections.sort(JavaReverseSorting, Collections.reverseOrder());
			System.out.println(JavaReverseSorting);
			sleep(5);
			List<WebElement> ColumnReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaReverseSorting.size();j++) {
				Long s1 = JavaReverseSorting.get(j);
				String s2 = ColumnReverSortedUIValue.get(j).getText().replaceAll("[^a-zA-Z0-9]", "");
				if(JavaReverseSorting.get(j).equals(Long.valueOf(ColumnReverSortedUIValue.get(j).getText().replaceAll("[^a-zA-Z0-9]", "")))) {
					Reporter.log(JavaReverseSorting.get(j)+" Column Value",true);
				}else {
					ALib.AssertFailMethod(JavaReverseSorting.get(j)+"Reversing Sorting of Column has been failed");
				}
			}
		}

		else {
			List<String> JavaSorting = new ArrayList<>();
			List<WebElement> ColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : ColumnValues) {
				String toberemoved = ColumnValue.getText();
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					JavaSorting.add(ColumnValue.getText());
				}
			}
			Collections.sort(JavaSorting, Collections.reverseOrder());
			sleep(5);
			List<WebElement> ReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for(int j=0;j<JavaSorting.size();j++) {
				String sds = ReverSortedUIValue.get(j).getText();
				String dsd = JavaSorting.get(j);
				if(JavaSorting.get(j).equals(ReverSortedUIValue.get(j).getText())) {
					Reporter.log(JavaSorting.get(j)+" Column Value",true);
				}else {
					ALib.AssertFailMethod(JavaSorting.get(j) +"Reversing Sorting of Column has been failed");
				}
			}

		}

		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		Initialization.driver.findElement(By.xpath("//div[contains(text(),'"+ColumnName+"')]")).click();
		sleep(10);

		/**************************************************************Natural Sorting*********************************************************************************************/
		if ((ColumnName.equalsIgnoreCase("Agent Version"))||(ColumnName.contains("SureLock Version"))) {
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Double> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(Double.valueOf(ColumnValue.getText()));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				if(NaturalJavaSorting.get(j).equals(Double.valueOf(NaturalSortedUIValue.get(k).getText()))) {
					Reporter.log(NaturalJavaSorting.get(j) +"Sorting is working fine",true);
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}
		}
		else if (ColumnName.contains("Battery Temperature")){
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Double> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					String textvalue = ColumnValue.getText();
					String stringtoAdd=textvalue.substring(0,(textvalue.length()-2));
					NaturalJavaSorting.add(Double.valueOf(stringtoAdd));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {

				String textvalue = NaturalSortedUIValue.get(k).getText();
				String stringtocomp=textvalue.substring(0,(textvalue.length()-2));
				if(NaturalJavaSorting.get(j).equals(Double.valueOf(stringtocomp))) {
					Reporter.log(NaturalJavaSorting.get(j) +"Sorting is working fine",true);
					j=j+1;

				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}
		}

		else if (ColumnName.equalsIgnoreCase("CPU Usage")||ColumnName.equalsIgnoreCase("GPU Usage")||ColumnName.equalsIgnoreCase("Cell Signal Strength")||ColumnName.equalsIgnoreCase("Battery")||ColumnName.equalsIgnoreCase("Free Storage Memory")||ColumnName.equalsIgnoreCase("Free Program Memory")) {
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Integer> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(Integer.valueOf(ColumnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				if(NaturalJavaSorting.get(j).equals(Integer.valueOf(NaturalSortedUIValue.get(k).getText().replaceAll("[^a-zA-Z0-9]", "")))) {
					Reporter.log(NaturalJavaSorting.get(j) +"Sorting is working fine",true);
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}}
		else if (ColumnName.contains("IMEI")) {
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<Long> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(Long.valueOf(ColumnValue.getText().replaceAll("[^a-zA-Z0-9]", "")));
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				if(NaturalJavaSorting.get(j).equals(Long.valueOf(NaturalSortedUIValue.get(k).getText()))) {
					Reporter.log(NaturalJavaSorting.get(j) +"Sorting is working fine",true);
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}}

		else {
			List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			List<String> NaturalJavaSorting = new ArrayList<>();
			List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
			for (WebElement ColumnValue : NaturalSortColumnValues) {
				if (!(ColumnValue.getText().equals("N/A")||ColumnValue.getText().equals("Not Installed")||ColumnValue.getText().equals("Unknown")||ColumnValue.getText().equals("No Signal"))){
					NaturalJavaSorting.add(ColumnValue.getText());
				}
			}
			Collections.sort(NaturalJavaSorting);
			int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
			int j=0;
			for (int k=sort;k<sort+NaturalJavaSorting.size();k++) {
				String sds = NaturalJavaSorting.get(j);
				String sasa = NaturalSortedUIValue.get(k).getText();
				if(NaturalJavaSorting.get(j).equals(NaturalSortedUIValue.get(k).getText())) {
					Reporter.log(NaturalJavaSorting.get(j) +"Sorting is working fine",true);
					j=j+1;
				}else {
					ALib.AssertFailMethod(NaturalJavaSorting.get(j) +"Natural Sorting of Column has been failed");
				}

			}}
	}

	public void verifyDeviceAdvanceSearchForValuesSelectedfromFilter(String ValuetoCompare,String ColumnName) throws InterruptedException
	{ sleep(10);
	int i=0;
	VerifyColumnIsPresentOnGridAndScroll(ColumnName);
	List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
	for (WebElement column:AllColumnsinGrid) {
		if(column.getText().equalsIgnoreCase(ColumnName)){
			i=i+1;
			break;
		}
		i=i+1;
	}

	List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
	if(columnValues.size()==0) {
		try {
			NoDeviceAvailable.isDisplayed();
			Reporter.log("No Device is availble with filter" +ValuetoCompare);}
		catch(Exception e) {
			ALib.AssertFailMethod("Advance Search is not working");
		}
	}else {
		for(WebElement columnValue:columnValues) {
			String test = columnValue.getText();
			//if (ValuetoCompare.contains(columnValue.getText())){
			if (columnValue.getText().contains(ValuetoCompare)){
				Reporter.log(columnValue.getText() +" is expected value",true);
			}else {
				ALib.AssertFailMethod("Advance Search is not working");
			}
		}
	}
	}

	public void verifyDeviceAdvanceSearchWithDoubleValues(String Operator,int BatteryPercentage,String ColumnName ) throws InterruptedException {
		sleep(6);
		int i=0;
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}  
		List<WebElement> TotalBatteryPercentage = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		if(TotalBatteryPercentage.size()==0) {
			try {
				NoDeviceAvailable.isDisplayed();
				Reporter.log("No Device is available with filter",true);}
			catch(Exception e) {
				ALib.AssertFailMethod("Advance Search is not working");
			}
		}
		for(  WebElement Percentage  :TotalBatteryPercentage) {
			//String valToComp = Percentage.getText().substring(0,Percentage.getText().length()-2 );
			String valToComp = Percentage.getText();
			String test = valToComp.replaceAll("[^a-zA-Z0-9]", "");
			if(Operator.matches("<")) {
				if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
					if(Double.valueOf(valToComp) <BatteryPercentage) {
						Reporter.log(" Sorting is working fine");
					}else {
						ALib.AssertFailMethod("Sorting is not working fine");
					}}}
			else if(Operator.matches(">=")) {

				if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
					if(Double.valueOf(valToComp)>=BatteryPercentage) {
						Reporter.log(" Sorting is working fine");
					}else {
						ALib.AssertFailMethod("Sorting is not working fine");
					}}
			}

			else if(Operator.matches("=<")) {

				if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
					if(Double.valueOf(valToComp)<=BatteryPercentage) {
						Reporter.log(" Sorting is working fine");
					}else {
						ALib.AssertFailMethod("Sorting is not working fine");
					}}
				else if(Operator.matches("=")) {

					if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
						if(Double.valueOf(valToComp)==BatteryPercentage) {
							Reporter.log(" Sorting is working fine");
						}else {
							ALib.AssertFailMethod("Sorting is not working fine");
						}}
				}
				else  {
					if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
						if(Double.valueOf(valToComp)>BatteryPercentage) {
							Reporter.log(" Sorting is working fine");
						}else {
							ALib.AssertFailMethod("Sorting is not working fine");
						}}
				}

			}}
	}

	@FindBy(xpath=".//*[@id='tableContainer']/div[4]/div[1]/div[1]/button")
	private WebElement DeviceGridRefreshButton;
	
	@FindBy(id="homeSection")
	private WebElement homeButton;
	public void DeviceGridRefresh() throws InterruptedException
	{
		homeButton.click();
		DeviceGridRefreshButton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		Reporter.log("Clicked on Device Grid Refresh Button");
	}

	@FindBy(xpath="//span[contains(text(),'All Devices')]")
	private WebElement AllDevice;
	public void ClickOnAllDevices() throws InterruptedException
	{
		AllDevice.click();
		sleep(2);
	}

	public void VerifyAdvanceSearchDataUsage(String Operator,int ColValue,String ColumnName ) throws InterruptedException {
		sleep(6);
		int i=0;
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}  

		List<WebElement> GetValuesFromUI = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		if(GetValuesFromUI.size()==0) {
			try {
				NoDeviceAvailable.isDisplayed();
				Reporter.log("No Device is available with filter",true);}
			catch(Exception e) {
				ALib.AssertFailMethod("Advance Search is not working");
			}
		}
		for(  WebElement GetValue  :GetValuesFromUI) {
			Long covertedValue;
			if(GetValue.getText().contains("GB")) {
				String trimedValue = GetValue.getText().substring(0, GetValue.getText().length()-2);
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1073741824);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1073741824);
				}
			}
			else if(GetValue.getText().contains("KB")) {
				String trimedValue = GetValue.getText().substring(0, GetValue.getText().length()-2);

				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1000);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1000);
				}
			}
			else if(GetValue.getText().contains("MB")) {
				String trimedValue = GetValue.getText().substring(0, GetValue.getText().length()-2);
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1000000);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1000000);
				}
			}
			else {
				String trimedValue = GetValue.getText().substring(0, GetValue.getText().length()-5);
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue));
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue));
				}
			}

			if(Operator.matches("<")) {
				if(covertedValue <ColValue) {
					Reporter.log("Advanced search is wroking fine"+ covertedValue,true);
				}else {
					ALib.AssertFailMethod("Advanced search is not wroking properly"+ covertedValue);
				}}
			else if(Operator.matches(">=")) {
				if(covertedValue>=ColValue) {
					Reporter.log("Advanced search is wroking fine"+ covertedValue,true);
				}else {
					ALib.AssertFailMethod("Advanced search is not wroking properly"+ covertedValue);
				}
			}

			else if(Operator.matches("=<")) {
				if(covertedValue<=ColValue) {
					Reporter.log("Advanced search is wroking fine"+ covertedValue,true);
				}else {
					ALib.AssertFailMethod("Advanced search is not wroking properly"+ covertedValue);
				}}
			else if(Operator.matches("=")) {
				if(covertedValue==ColValue) {
					Reporter.log("Advanced search is wroking fine"+ covertedValue,true);
				}else {
					ALib.AssertFailMethod("Advanced search is not wroking properly"+ covertedValue);
				}
			}
			else  {
				if(covertedValue>ColValue) {
					Reporter.log("Advanced search is wroking fine"+ covertedValue,true);
				}else {
					ALib.AssertFailMethod("Advanced search is not wroking properly"+ covertedValue);
				}
			}

		}}

	public void verifyDeviceAndRelatedProperty(String DeviceName,String ColumnName,String ValuetoCompare) throws InterruptedException {
		sleep(2);
		DeviceGridRefresh();
		sleep(6);
		String uIDeviceName = Initialization.driver.findElement(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p")).getText();
		if(uIDeviceName.equalsIgnoreCase(DeviceName)) {
			Reporter.log("Device is present in Device Grid");
		}else {
			ALib.AssertFailMethod("Device is not present in Device Grid");
		}

		sleep(10);
		int i=0;
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}

		String columnValues = Initialization.driver.findElement(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td["+i+"]")).getText();
		if (columnValues.contains(ValuetoCompare)){
			Reporter.log(columnValues +" is expected value",true);
		}else {
			ALib.AssertFailMethod("Advance Search is not working");
		}
	}

	public void ClearAdvanceSeach(String ClassName) throws InterruptedException
	{    
		Thread.sleep(10);
		Initialization.driver.findElement(By.xpath("//td[@class='"+ClassName+"']/div/input")).clear();
		Thread.sleep(10);
	}


	@FindBy(xpath="//*[@id=\"tableContainer\"]/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;
	public void BasicSearch(String searchProperty) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{sleep(10);
	SearchDeviceTextField.clear();
	sleep(10);
	SearchDeviceTextField.sendKeys(searchProperty);
	}

	public void DisableInstallApplicationversionColumns( ) throws InterruptedException {
		List<WebElement> InstallAppCols = Initialization.driver.findElements(By.xpath("//ul[@class='app_lst_container st-list_container']/li/label/span/input"));
		for( WebElement installapCol : InstallAppCols) {
			if(installapCol.isSelected())
			{
				installapCol.click();
				Reporter.log("Disabling column",true);
			}
		}
	}

	public void VerifySortingOfDataUsage(String ColumnName) throws InterruptedException {
		VerifyBeforeSortingInPagination();
		sleep(5);
		int i=0;
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
	//	waitForPageToLoad();
		sleep(100);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}

		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		sleep(20);

		List<Long> JavaReverseSorting = new ArrayList<>();
		List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		for (WebElement columnValue : columnValues) {
			if (!(columnValue.getText().equals("N/A"))||(columnValue.getText().equals("Not Installed")||columnValue.getText().equals("No Signal"))) {
				String removelater = columnValue.getText();
				if(columnValue.getText().contains("GB")) {
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-2);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue)*1073741824);
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue)*1073741824);
					}
					JavaReverseSorting.add(covertedValue);}
				else if(columnValue.getText().contains("KB")) {
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-2);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue)*1000);
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue)*1000);
					}
					JavaReverseSorting.add(covertedValue);
				}
				else if(columnValue.getText().contains("MB")) {
					String asa = columnValue.getText();
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-2);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue)*1000000);
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue)*1000000);
					}
					JavaReverseSorting.add(covertedValue);
				}
				else {
					String asa = columnValue.getText();
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-5);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue));
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue));
					}
					JavaReverseSorting.add(Long.valueOf(covertedValue));
				}
			}}

		Collections.sort(JavaReverseSorting, Collections.reverseOrder());
		System.out.println(JavaReverseSorting);
		sleep(5);
		List<WebElement> ColumnReverSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		List<Long> ReverseSorting = new ArrayList<>();
		for(int j=0;j<JavaReverseSorting.size();j++) {

			if(ColumnReverSortedUIValue.get(j).getText().contains("GB")) {
				String trimedValue = ColumnReverSortedUIValue.get(j).getText().substring(0, ColumnReverSortedUIValue.get(j).getText().length()-2);
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1073741824);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1073741824);
				}
				ReverseSorting.add(covertedValue);}
			else if(ColumnReverSortedUIValue.get(j).getText().contains("KB")) {
				String trimedValue = ColumnReverSortedUIValue.get(j).getText().substring(0, ColumnReverSortedUIValue.get(j).getText().length()-2);
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1000);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1000);
				}
				ReverseSorting.add(covertedValue);
			}
			else if(ColumnReverSortedUIValue.get(j).getText().contains("MB")) {
				String trimedValue = ColumnReverSortedUIValue.get(j).getText().substring(0, ColumnReverSortedUIValue.get(j).getText().length()-2);
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1000000);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1000000);
				}
				ReverseSorting.add(covertedValue);
			}
			else {
				String trimedValue = ColumnReverSortedUIValue.get(j).getText().replaceAll("[^0-9.]", "");
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue));
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue));
				}
				ReverseSorting.add(Long.valueOf(covertedValue));
			}
		}
		System.out.println(ReverseSorting);
		for (int k=0;k<ReverseSorting.size();k++) {
			if(ReverseSorting.get(k).equals(JavaReverseSorting.get(k))){
				Reporter.log(ReverseSorting.get(k)+" Column Value",true);
			}else {
				ALib.AssertFailMethod(ReverseSorting.get(k)+"Reversing Sorting of Column has been failed");
			}
		}
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		sleep(20);

		/**************************************************************Natural Sorting*********************************************************************************************/

		List<WebElement> NaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		List<Long> NaturalJavaSorting = new ArrayList<>();
		List<WebElement> NaturalSortColumnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		for (WebElement columnValue : NaturalSortColumnValues) {
			if (!(columnValue.getText().equals("N/A")||columnValue.getText().equals("Not Installed")||columnValue.getText().equals("Unknown")||columnValue.getText().equals("No Signal"))){
				if(columnValue.getText().contains("GB")) {
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-2);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue)*1073741824);
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue)*1073741824);
					}
					NaturalJavaSorting.add(covertedValue);}
				else if(columnValue.getText().contains("KB")) {
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-2);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue)*1000);
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue)*1000);
					}
					NaturalJavaSorting.add(covertedValue);
				}
				else if(columnValue.getText().contains("MB")) {
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-2);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue)*1000000);
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue)*1000000);
					}
					NaturalJavaSorting.add(covertedValue);
				}
				else {
					String trimedValue = columnValue.getText().substring(0, columnValue.getText().length()-5);
					Long covertedValue;
					try {
						covertedValue = (long) (Integer.parseInt(trimedValue));
					} catch (NumberFormatException e) {
						covertedValue = (long) (Double.parseDouble(trimedValue));
					}
					NaturalJavaSorting.add(Long.valueOf(covertedValue));
				}
			}
		}


		int sort = NaturalSortedUIValue.size()-NaturalJavaSorting.size();
		Collections.sort(NaturalJavaSorting);
		List<Long> NaturalSorting = new ArrayList<>();
		for (int j=sort;j<sort+NaturalJavaSorting.size();j++){
			String toberemovedLater = NaturalSortedUIValue.get(j).getText();
			if(NaturalSortedUIValue.get(j).getText().contains("GB")) {
				String trimedValue = NaturalSortedUIValue.get(j).getText().substring(0, NaturalSortedUIValue.get(j).getText().length()-2);
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1073741824);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1073741824);
				}
				NaturalSorting.add(covertedValue);}
			else if(NaturalSortedUIValue.get(j).getText().contains("KB")) {
				String trimedValue = NaturalSortedUIValue.get(j).getText().substring(0, NaturalSortedUIValue.get(j).getText().length()-2);
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1000);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1000);
				}
				NaturalSorting.add(covertedValue);
			}
			else if(NaturalSortedUIValue.get(j).getText().contains("MB")) {
				String trimedValue = NaturalSortedUIValue.get(j).getText().substring(0, NaturalSortedUIValue.get(j).getText().length()-2);
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue)*1000000);
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue)*1000000);
				}
				NaturalSorting.add(covertedValue);
			}
			else {
				String trimedValue = NaturalSortedUIValue.get(j).getText().substring(0, NaturalSortedUIValue.get(j).getText().length()-5);
				Long covertedValue;
				try {
					covertedValue = (long) (Integer.parseInt(trimedValue));
				} catch (NumberFormatException e) {
					covertedValue = (long) (Double.parseDouble(trimedValue));
				}
				NaturalSorting.add(Long.valueOf(covertedValue));
			}}

		for (int k=0;k<NaturalJavaSorting.size();k++) {
			if(NaturalJavaSorting.get(k).equals(NaturalSorting.get(k))){
				Reporter.log(NaturalJavaSorting.get(k)+" Column Value",true);
			}else {
				ALib.AssertFailMethod(NaturalJavaSorting.get(k)+"Reversing Sorting of Column has been failed");
			}
		}
	}

	public void closeColumnGrid( ) throws InterruptedException {
		gridColumnClick.click();//To remove SerchOptions Pop up
		sleep(10);}

	public void waitForXpathPresent(String wbXpath){
		WebDriverWait wait = new WebDriverWait(Driver.driver,40);
		wait.until(ExpectedConditions.
				presenceOfAllElementsLocatedBy(By.xpath(wbXpath)));		
	}

	@FindBy(xpath ="//span[@id='deviceNotes']/../a" )
	private WebElement NotesEdit;
	public void ClickOnEditNotes() throws InterruptedException
	{
		NotesEdit.click();
	}

	@FindBy(xpath="(//span[text()='Move to Group'])[2]")
	private WebElement Movetogroup;
	public void MoveDeviceToGroup(String GroupName) throws InterruptedException {
		WebElement Device = Initialization.driver.findElement(By.xpath("//p[text()='"+Config.DeviceName+"']"));
		Actions action = new Actions(Initialization.driver);
		sleep(2);
		action.contextClick(Device).perform();
		Movetogroup.click();
		sleep(3);
		MovetoGroupSearch.sendKeys(GroupName);
		sleep(3);
		Initialization.driver.findElement(By.xpath("(//li[text()='"+GroupName+"'])[2]")).click();
		MoveButton.click();
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")).click();
		sleep(2);
		Reporter.log("Devices moved successfully",true);
		
	}


	@FindBy(xpath="//div[@class='editable-buttons']/button")
	private WebElement SaveButton;

	@FindBy(xpath =".//*[@class='editable-input']/textarea" )
	private WebElement textAreaAddNotesWindow;

	public void SelectAllNotesAndEnterNotesgrid() throws InterruptedException
	{
		textAreaAddNotesWindow.sendKeys(Keys.CONTROL + "a");
		textAreaAddNotesWindow.sendKeys("AutomationMDM");
		sleep(1);
		SaveButton.click();
	}


	public void verifyDeviceFreeStorage(String ColumnName) throws InterruptedException
	{ sleep(10);
	int i=0;
	VerifyColumnIsPresentOnGridAndScroll(ColumnName);
	List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
	for (WebElement column:AllColumnsinGrid) {
		if(column.getText().equalsIgnoreCase(ColumnName)){
			i=i+1;
			break;
		}
		i=i+1;
	}

	List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));

	for(WebElement columnValue:columnValues) {
		String test = columnValue.getText();
		if (!columnValue.getText().isEmpty()||columnValue.getText().contains("N/A")){
			Reporter.log(columnValue.getText() +" is expected value",true);
		}else {
			ALib.AssertFailMethod("Advance Search is not working");
		}
	}
	}
	public void VarificationInvalidSearch() throws EncryptedDocumentException, IOException, InterruptedException
	{
		SearchDeviceTextField.clear();
		sleep(10);
		SearchDeviceTextField.sendKeys("ab12@");
		waitForXpathPresent("//span[text()='No matching result found.']");
		boolean isdisplayed =true;

		try{
			warningMessageForInvalidSearch.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}

		Assert.assertTrue(isdisplayed,"FAIL >> warning NOT displayed");
		Reporter.log("PASS >> 'No matching result found.' is received",true );	
	}

	public void verifyDeviceAvailable(String DeviceUserNameDevice) throws InterruptedException {
		sleep(6);
		String uIDeviceName = Initialization.driver.findElement(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p")).getText();
		if(uIDeviceName.equalsIgnoreCase(DeviceUserNameDevice)) {
			Reporter.log("Device is present in Device Grid");
		}else {
			ALib.AssertFailMethod("Device is not present in Device Grid");
		}
	} 

	public void VerifyColumnIsNotPresentOnGrid(String ColumnName) throws InterruptedException
	{  
		try{
			WebElement scroll = Initialization.driver.findElement(By.xpath("//div[text()='"+ColumnName+"']"));
			js.executeScript("arguments[0].scrollIntoView()", scroll);
			boolean ColumnNameInGrid = Initialization.driver.findElement(By.xpath("//div[text()='"+ColumnName+"']")).isDisplayed();
			Assert.assertTrue(ColumnNameInGrid);
		}
		catch(Exception e) {
			Reporter.log("Pass >> Column is present in Device Grid",true );
		}
		Reporter.log("PASS >> Column is not present in Device Grid",true );	
	}
	public void EnterValueForAdvanceSeach(String AdvanceSearchValue, String ClassName) throws InterruptedException
	{
		WebElement scroll = Initialization.driver.findElement(By.xpath("//td[@class='"+ClassName+"']/div/input"));
		js.executeScript("arguments[0].scrollIntoView()", scroll);
		Initialization.driver.findElement(By.xpath("//td[@class='"+ClassName+"']/div/input")).clear();
		Initialization.driver.findElement(By.xpath("//td[@class='"+ClassName+"']/div/input")).sendKeys(AdvanceSearchValue);
		sleep(5);
		
	}

	@FindBy(xpath="//input[@id='job_text']")
	private WebElement TrackingPerodicity;
	public void TurningOnLocationTracking(String TrackingPeriodicity) throws InterruptedException
	{
		TrackingPerodicity.clear();
		TrackingPerodicity.sendKeys(TrackingPeriodicity);
		ApplyLocationTracking.click();
		Thread.sleep(500);
		Reporter.log("Location Tracking Updated Successfully Message Displayed",true);
	}

	@FindBy(xpath="//div[@id='tableContainer']/div[4]/div[1]/div[1]/button")
	private WebElement Refresh;
	public void RefreshDeviceGrid() throws InterruptedException
	{  
		sleep(4);
		Refresh.click();
		sleep(10);
	}
	
	public void RefreshGrid() throws InterruptedException
	{  
		Initialization.driver.navigate().refresh();
		sleep(20);
	}
	
	

	public void ClickAndEnterResetAdvanceSeach(String AdvanceSearchValue, String ClassName,String ColumnName,int numberOfDownPress) throws InterruptedException, AWTException
	{
		WebElement column = Initialization.driver.findElement(By.xpath("//td[@class='"+ClassName+"']/div/select"));
		column.click();

		int i=0;
		while(i<=numberOfDownPress) {
			column.sendKeys(Keys.UP);
			i=i+1;
		}
		column.sendKeys(Keys.ENTER);
	}

	public void ClickAndEnterValueAdvanceSeach(String AdvanceSearchValue, String ClassName,int numberOfDownPress) throws InterruptedException, AWTException
	{
		sleep(1);
		WebElement column = Initialization.driver.findElement(By.xpath("//td[@class='"+ClassName+"']/div/select"));
		column.click();

		int i=0;
		while(i<=numberOfDownPress) {
			column.sendKeys(Keys.DOWN);
			i=i+1;
		}
		column.sendKeys(Keys.ENTER);
		sleep(10);
	}

	@FindBy(xpath="//div[@class='keep-open btn-group']")
	private WebElement gridColumn;
	public void clickOnGridColumn() throws InterruptedException
	{ sleep(10);
	    homeButton.click();
		WebDriverWait wait= new WebDriverWait(Initialization.driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='keep-open btn-group']")));
		gridColumn.click();
	}

	@FindBy(id="grid_column_search")
	private WebElement columnSearch;
	public void EnterColumnName(String Column)
	{
		columnSearch.clear();
		columnSearch.sendKeys(Column);
	}

	@FindBy(xpath="//button[@class='btn btn-default dropdown-toggle']")
	private WebElement 	gridColumnClick;
	public void Enable_DisbleColumn(String ColumnName,String Enable_Disable) throws InterruptedException
	{  
		waitForXpathPresent("//label[text()=' "+ColumnName+"']/span/input");
		//	waitForidClickable(Initialization.driver.findElement(By.xpath("//label[text()=' "+ColumnName+"']/span/input")));
		sleep(1);
		if(Enable_Disable.equalsIgnoreCase("Enable")) {
			if(Initialization.driver.findElement(By.xpath("//label[text()=' "+ColumnName+"']/span/input")).isSelected()) {
				Reporter.log("Column is already enabled",true);
			}else {
				sleep(1);
				Initialization.driver.findElement(By.xpath("//label[text()=' "+ColumnName+"']/span/input")).click();
				Reporter.log("Column is enabled",true);
			}}else {
				if(Initialization.driver.findElement(By.xpath("//label[text()=' "+ColumnName+"']/span/input")).isSelected()) {
					Initialization.driver.findElement(By.xpath("//label[text()=' "+ColumnName+"']/span/input")).click();
					Reporter.log("Column is disabled",true);
				}
				else  {
					Reporter.log("Column is Disabled",true);
				}
			}
		sleep(10);

		gridColumnClick.click();//To remove SerchOptions Pop up
		sleep(20);
	}  
	public void selectPageAndVerifySortingWith_NA(String PaginationNumber,String ColumnName) throws InterruptedException {	
		sleep(3);
		if(Pagination.isDisplayed()) {
			sleep(6);
			WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='"+PaginationNumber+"']")));
			Initialization.driver.findElement(By.xpath("//a[text()='"+PaginationNumber+"']")).click();
			VerifySortingIsNotChangedWithNA(ColumnName);
			Reporter.log("Sorting is working fine after Page Pagination",true);
		}}

	public void verifyDeviceAdvanceSearchStringValues(String ValuetoCompare,String ColumnName) throws InterruptedException
	{ sleep(10);
	int i=0;
	VerifyColumnIsPresentOnGridAndScroll(ColumnName);
	List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
	for (WebElement column:AllColumnsinGrid) {
		if(column.getText().equalsIgnoreCase(ColumnName)){
			i=i+1;
			break;
		}
		i=i+1;
	}

	List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
	for(WebElement columnValue:columnValues) {
		String test = columnValue.getText();
		//if (ValuetoCompare.contains(columnValue.getText())){
		if (columnValue.getText().contains(ValuetoCompare)){
			Reporter.log(columnValue.getText() +" is expected value",true);
		}else {
			ALib.AssertFailMethod("Advance Search is not working");
		}
	}
	}
	 public void enterValueInModelFieldForAdvSearch(String val) throws InterruptedException
	 {
		 Initialization.driver.findElement(By.xpath("//input[@id='devicemodel_text']")).sendKeys(val);
		 sleep(2);
	 }
	 

	@FindBy(xpath="//*[contains(text(),'No device available in this group.')]")
	private WebElement NoDeviceAvailable;

	public void verifyDeviceAdvanceSearchWithIntValues(String Operator,int BatteryPercentage,String ColumnName ) throws InterruptedException {
		sleep(5);
		int i=0;
		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}  
		List<WebElement> TotalBatteryPercentage = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		if(TotalBatteryPercentage.size()==0) {
			try {
				NoDeviceAvailable.isDisplayed();
				Reporter.log("No Device is available with filter",true);}
			catch(Exception e) {
				ALib.AssertFailMethod("Advance Search is not working");
			}
		}
		for(  WebElement Percentage  :TotalBatteryPercentage) {
			String valToComp;
			if(ColumnName.equalsIgnoreCase("Battery Temperature")) {
				valToComp = Percentage.getText().substring(0,Percentage.getText().length()-2 );
			}else{
				valToComp = Percentage.getText();}
			String test = valToComp.replaceAll("[^a-zA-Z0-9]", "");
			if(Operator.matches("<")) {
				if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
					if(Integer.valueOf(valToComp.replaceAll("[^a-zA-Z0-9]", "")) <BatteryPercentage) {
						Reporter.log(" Sorting is working fine for value " +BatteryPercentage,true);
					}else {
						ALib.AssertFailMethod("Sorting is not working fine");
					}}}
			else if(Operator.matches(">=")) {

				if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
					if(Integer.valueOf(valToComp.replaceAll("[^a-zA-Z0-9]", ""))>=BatteryPercentage) {
						Reporter.log(" Sorting is working fine for value " +BatteryPercentage,true);
					}else {
						ALib.AssertFailMethod("Sorting is not working fine");
					}}
			}

			else if(Operator.matches("=<")) {

				if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
					if(Integer.valueOf(valToComp.replaceAll("[^a-zA-Z0-9]", ""))<=BatteryPercentage) {
						Reporter.log(" Sorting is working fine for value " +BatteryPercentage,true);
					}else {
						ALib.AssertFailMethod("Sorting is not working fine");
					}}
				else if(Operator.matches("=")) {

					if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
						if(Integer.valueOf(valToComp.replaceAll("[^a-zA-Z0-9]", ""))==BatteryPercentage) {
							Reporter.log(" Sorting is working fine for value " +BatteryPercentage,true);
						}else {
							ALib.AssertFailMethod("Sorting is not working fine");
						}}
				}
				else  {
					if(!(valToComp.replaceAll("[^a-zA-Z0-9]", "").isEmpty())) {
						if(Integer.valueOf(valToComp.replaceAll("[^a-zA-Z0-9]", ""))>BatteryPercentage) {
							Reporter.log(" Sorting is working fine for value" +BatteryPercentage,true);
						}else {
							ALib.AssertFailMethod("Sorting is not working fine");
						}}
				}

			}}
	}

	public void SearchMultipleDevice(String DevicesName,String ColumnName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).clear();
		Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).sendKeys(DevicesName);

		sleep(3);
		List<String> deviceName = Arrays.asList(DevicesName.split(","));  
		Collections.sort(deviceName);
		int i=1;
		for(int k=0;k<deviceName.size();k++) {
			String uIDeviceName = Initialization.driver.findElement(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr["+i+"]/td[2]/p")).getText();
			String sds = deviceName.get(k);
			if(uIDeviceName.contains(deviceName.get(k))) {
				Reporter.log("Enrolled device is present in Device Grid",true);
			}else {
				ALib.AssertFailMethod("Enrolled device is not present in Device Grid");
			}
			i=i+1;}
	}

	public void verifyEnrolledDeviceAvailable() throws InterruptedException {
		sleep(4);
		String uIDeviceName = Initialization.driver.findElement(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p")).getText();
		if(uIDeviceName.equalsIgnoreCase(Config.DeviceName)) {
			Reporter.log("Enrolled device is present in Device Grid",true);
		}else {
			ALib.AssertFailMethod("Enrolled device is not present in Device Grid");
		}
	}

	public void verifyEnrolledDeviceForMultiSearch(String Dev1,String Dev2) throws InterruptedException
	{
		sleep(4);
		List <WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']/tbody/tr/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			String DevsName=uIDeviceName.get(i).getText();
			System.out.println(DevsName);
			if(DevsName.equalsIgnoreCase(Dev1)||DevsName.equalsIgnoreCase(Dev2)) {
				Reporter.log("PASS>> Enrolled device is present in Device Grid",true);
		}else {
			ALib.AssertFailMethod("FaIL>> Enrolled device is not present in Device Grid");
		}
		}
	}
	@FindBy(id="devicename_searchoption")
	private WebElement DevSearchOps;
	public void clickOnExpectedSearchOpInDeviceNameCol(String value)
	{
		 Select Op=new Select(DevSearchOps);
		 Op.selectByValue(value);
	}
	@FindBy(xpath="//*[text()='Contains: Device name contains']")
	private WebElement ContainsOp;
	
	@FindBy(xpath="//*[text()='Starts with: Device name starts with']")
	private WebElement StartOp;
	
	@FindBy(xpath="//*[text()='Ends with: Device name ends with']")
	private WebElement EndOp;
	public void VerifyOfDeviceNameSearchOptions()
	{
		DevSearchOps.click();
		try {
			ContainsOp.isDisplayed();
			StartOp.isDisplayed();
			EndOp.isDisplayed();
			Reporter.log("PASS>> Options are displayed as expected",true);
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL>> Options are not displayed as expected");
		}
	}
	public void verifyOfDevNameSearchWithContains(String DeviceNameContains) throws InterruptedException
	{
		DevSearchOps.click();
		sleep(2);
		String uIDeviceName = Initialization.driver.findElement(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p")).getText();
		if(uIDeviceName.contains(DeviceNameContains)) {
			Reporter.log("PASS >> Contains Search option is working as expected",true);
		}else {
			ALib.AssertFailMethod("FAIL >> Contains Search option is working as expected");
		}
	}
	
	public void verifyOfDevNameSearchStartWith(String DeviceNameStartsWith) throws InterruptedException
	{
		sleep(3);
		DevSearchOps.click();
		sleep(5);
		StartOp.click();
		sleep(4);
		List <WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']/tbody/tr/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			String DevsName=uIDeviceName.get(i).getText();
			char val=DevsName.charAt(0);
			String val1=String.valueOf(val);
			
			try {
				if (val1.equalsIgnoreCase(DeviceNameStartsWith)) {
					Reporter.log("PASS >> StartWith Search option is working as expected",true);
				}
			}catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> StartWith Search option is not working as expected");
			}
		}
	}
	public void verifyOfDevNameSearchEndsWith(String DeviceNameEndsWith)throws InterruptedException
	{
		sleep(2);
		DevSearchOps.click();
		sleep(2);
		EndOp.click();
		waitForXpathPresent("//table[@id='dataGrid']/tbody/tr/td[2]/p");
		sleep(4);
		List <WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']/tbody/tr/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			String DevsName=uIDeviceName.get(i).getText();
			char v=DevsName.charAt(DevsName.length()-1);
			String val1=String.valueOf(v);
			try {
				if (val1.equalsIgnoreCase(DeviceNameEndsWith)) {
					Reporter.log("PASS >> EndsWith Search option is working as expected",true);
				}
			}catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> EndsWith Search option is not working as expected");
			}
		}
	}
	ArrayList<String> coldev=new ArrayList<>();
	ArrayList<String> coldev1=new ArrayList<>();
	public void ReadingValueFromConsole(String options) throws InterruptedException
	{
		DevSearchOps.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//*[text()='"+options+"']")).click();
		waitForXpathPresent("//table[@id='dataGrid']/tbody/tr/td[2]/p");
		sleep(10);
		List<WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']/tbody/tr/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			coldev.add(uIDeviceName.get(i).getText());
		}
	}
	public void verifyValueInDevNameColFilter_Contains()
	{
		List<WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			coldev1.add(uIDeviceName.get(i).getText());
		}
		for(int i=0;i<coldev1.size();i++)
		{
			if(coldev.contains(coldev1.get(i)))
			{
				Reporter.log("PASS>> Values in Filter Is Shown Correctly",true);
			}else
			{
				ALib.AssertFailMethod("FAIL>>> Values in  Filter Is Not Shown Correctly");
			}
		}
	}
	public void verifyFilterAfterUpdate_Contains(String val1)throws InterruptedException
	{
		sleep(5);
		List<WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			String val=uIDeviceName.get(i).getText();
			String value=val.toLowerCase();
			if(value.contains(val1))
			{
				Reporter.log("PASS>> Values in Filter Is Shown Correctly",true);
			}else
			{
				ALib.AssertFailMethod("FAIL>>> Values in  Filter Is Not Shown Correctly");
			}
		}
	}
	public void verifyFilterAfterUpdate_StartsWith(String val)throws InterruptedException
	{
		sleep(4);
		String val2=null;
		String FilterVal=null;
		List<WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			val2=uIDeviceName.get(i).getText();
			char FirstVal=val2.charAt(0);
			FilterVal=String.valueOf(FirstVal);
		}
		for(int i=0;i<val2.length();i++)
			
			if (FilterVal.equalsIgnoreCase(val)) {
				Reporter.log("PASS>> Values in Filter Is Shown Correctly",true);
			}else
			{
				ALib.AssertFailMethod("FAIL>>> Values in  Filter Is Not Shown Correctly");
			}
		
}
	public void verifyFilterAfterUpdate_EndsWith(String val)throws InterruptedException
	{
		sleep(10);
		List<WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			String val1=uIDeviceName.get(i).getText();
			char val4=val1.charAt(val1.length()-1);
			String EndsFilterVal=String.valueOf(val4);
			if(EndsFilterVal.equalsIgnoreCase(val))
			{
				Reporter.log("PASS>> Values in Filter Is Shown Correctly",true);
			}else {
				ALib.AssertFailMethod("FAIL>>> Values in  Filter Is Not Shown Correctly");
			}
		}
	}
			
	public void verifyValueInDevNameColFilter_StartsWith()
	{
		String FilterVal=null;
		List<WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			coldev1.add(uIDeviceName.get(i).getText());
			String value1=coldev.toString();
			char value2=value1.charAt(1);
			FilterVal=String.valueOf(value2);
		}	
		for(int i=0;i<coldev.size();i++)
		{
			String value=coldev.toString();
			char val=value.charAt(1);
			String ConsoleVal=String.valueOf(val);
			try {
				if (ConsoleVal.equalsIgnoreCase(FilterVal)) {
					Reporter.log("PASS >> StartsWith Search option is working as expected",true);
				}
			}catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> StartsWith Search option is not working as expected");
			}
			}
	}
	public void verifyValueInDevNameColFilter_EndsWith()
	{
		String EndsFilterVal=null;
		List<WebElement> uIDeviceName = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr[1]/td[2]/p"));
		for(int i=0;i<uIDeviceName.size();i++)
		{
			coldev1.add(uIDeviceName.get(i).getText());
			String value1=coldev1.toString();
			char value2=value1.charAt(value1.length()-2);
			EndsFilterVal=String.valueOf(value2);
		}
		for(int i=0;i<coldev.size();i++)
		{
			String DevsName=coldev.toString();
			char v=DevsName.charAt(DevsName.length()-2);
			String ConsoleVal1=String.valueOf(v);
			try {
				if (ConsoleVal1.equalsIgnoreCase(EndsFilterVal)) {
					Reporter.log("PASS >> EndsWith Search option is working as expected",true);
				}
			}catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> EndsWith Search option is not working as expected");
			}
		}
	}
	
	
	@FindBy(id="locateBtn")
	private WebElement LocateBtn;

	public String  PARENTWINDOW;

	public void ClickOnLocate_UM() throws InterruptedException
	{
		Reporter.log("Clicking On Locate Button",true);
		PARENTWINDOW=Initialization.driver.getWindowHandle();
		LocateBtn.click();
		sleep(5);
	}

	@FindBy(xpath="//button[text()='Turn On']")
	private WebElement LocationTrackingTurnOnButton;
	public void ClickingOnTurnOnButton() throws InterruptedException
	{
		LocationTrackingTurnOnButton.click();
		sleep(3);
		Reporter.log("Enable Location Tracking Pop Up Is Displayed",true);
	}

	public void WaitingForUpdate(int hardwait) throws InterruptedException {
		sleep(hardwait);
	}

	@FindBy(xpath="//a[@id='locationTrackingEditBtn']/preceding-sibling::span")
	private WebElement DeviceINfoLocationTrackingStatus;


	@FindBy(xpath="(//span[text()='Move to Group'])[1]")
	private WebElement MovetoGroup;

	@FindBy(xpath="(//input[@class='form-control ct-input-hasBtn searchInput sc-treeview-search'])[2]")
	private WebElement MovetoGroupSearch;

	@FindBy(xpath="//button[text()='Move']")
	private WebElement MoveButton;
	public void MoveDevicesToGroup(String GroupName) throws InterruptedException {
		WebElement dev1 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[1]/td[2]"));
		WebElement dev2 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[2]/td[2]"));
		Actions action = new Actions(Initialization.driver);
		action.keyDown(Keys.CONTROL).click(dev1).click(dev2).keyUp(Keys.CONTROL).build().perform();
		sleep(2);
		action.contextClick(dev1).perform();
		sleep(3);
		WebDriverWait wait= new WebDriverWait(Initialization.driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[text()='Move to Group'])[1]")));
		MovetoGroup.click();
		sleep(3);
		MovetoGroupSearch.sendKeys(GroupName);
		sleep(3);
		Initialization.driver.findElement(By.xpath("(//li[text()='"+GroupName+"'])[2]")).click();
		MoveButton.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")).click();
		sleep(2);
		Reporter.log("Devices moved successfully",true);
	}

	public void VerifySortingIsNotChanged(String ColumnName) throws InterruptedException{
		sleep(6);
		int i=0;
		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		for (WebElement column:AllColumnsinGrid) {
			if(column.getText().equalsIgnoreCase(ColumnName)){
				i=i+1;
				break;
			}
			i=i+1;
		}
		List<String> JavaSorting = new ArrayList<>();
		List<WebElement> ColumnNaturalSortedUIValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		for (WebElement columnValue : ColumnNaturalSortedUIValue) {
			JavaSorting.add(columnValue.getText());
		}
		Collections.sort(JavaSorting);
		sleep(3);
		for (int k=0;k<ColumnNaturalSortedUIValue.size();k++) {
			if(JavaSorting.get(k).equals(ColumnNaturalSortedUIValue.get(k).getText())) {
				Reporter.log(JavaSorting.get(k)+" Column Value");
			}else {
				ALib.AssertFailMethod(JavaSorting.get(k)+"Natural Sorting of Column has been failed");
			}

		}
	}

	public void NavigateToOtherPages(String PageToNavigate, String BackToPage) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//a[text()='"+PageToNavigate+"']")).click();
		sleep(20);
		Initialization.driver.findElement(By.xpath("//a[text()='"+BackToPage+"']")).click();
	}

	public void EnterValueInAdvanceSearchColumn(String ColumnName,String ValueTobeSearched) throws InterruptedException
	{  
		sleep(4);
		WebElement scroll = Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input"));
		js.executeScript("arguments[0].scrollIntoView()", scroll);
		Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).clear();
		Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).sendKeys(ValueTobeSearched);
		//Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).sendKeys(Keys.ENTER);
		sleep(4);
		waitForidPresent("deleteDeviceBtn");
	}

	public void VerifySearchedvaluesInGrid(String ColumnName,String Value)
	{
		List<WebElement> values=Initialization.driver.findElements(By.xpath("(//td[@class='"+ColumnName+"'])[2]"));
		for(int i=0;i<values.size();i++)
		{
			if(!values.get(i).getText().equals(Value))
			{
				ALib.AssertFailMethod("PASS>>> Value Is Not Shown As Searched");
			}
		}
	}



	//Device Grid 22-05-2020///WFH

	@FindBy(id="refreshButton")
	private WebElement DynamicRefreshButton;

	public String DeviceOSBuildNumber;

	public void ReadingOSBuildNumber()
	{
		DeviceOSBuildNumber = Initialization.driver.findElement(By.xpath("//td[@class='OsBuildNumber']")).getText();
		System.out.println(DeviceOSBuildNumber);
	}
	public String DeviceOSBuildNumber1;

	public void ReadingOSBuildNumberForWindows()
	{
		DeviceOSBuildNumber1 = Initialization.driver.findElement(By.xpath("//td[@class='OsBuildNumber']")).getText();
	}
	public void verifyDeviceOSBuildNumber(String OSBuildNumber) throws InterruptedException
	{
		ReadingOSBuildNumber();
		System.out.println(DeviceOSBuildNumber);
		if(DeviceOSBuildNumber.contains("N/A")|| !DeviceOSBuildNumber.equals(OSBuildNumber))
		{
			ALib.AssertFailMethod("FAIL>>> OS Build Number IS Not Shown Correctly");
		}
		else
		{
			Reporter.log("PASS>>> OS Build Number Is Shown Correctly",true);
		}
		js.executeScript("arguments[0].scrollIntoView()",Initialization.driver.findElement(By.xpath("(//div[text()='Device'])[1]")));
		sleep(4);
	}

	public void checkingTableInReports(String TableName) throws InterruptedException
	{
		JavascriptExecutor jc=(JavascriptExecutor)Initialization.driver;
		WebElement Table = Initialization.driver.findElement(By.xpath("//li[text()='"+TableName+"']"));
		jc.executeScript("arguments[0].scrollIntoView()",Table);
		sleep(4);
		Table.click();
		waitForXpathPresent("//div[@id='addSelectedNodes']");
		sleep(2);
	}

	public void VerifyOSBuildNumberInReport(int columnNumber) throws InterruptedException
	{
		js.executeScript("arguments[0].scrollIntoView()",Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td["+columnNumber+"]/p")));
		sleep(4);
		String OsBuildNumberInDeviceDetailsReport = Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td["+columnNumber+"]/p")).getText();
		System.out.println(OsBuildNumberInDeviceDetailsReport);
		if(OsBuildNumberInDeviceDetailsReport.equals(DeviceOSBuildNumber))
		{

			Reporter.log("PASS>>> OS Build Number Is Shown Correctly As Shown In Device Grid",true);
		}
		else
		{
			SwitchBackWindow();
			ALib.AssertFailMethod("FAIL>>> OS Build Number Is Not Shown Correctly As Shown In Device Grid");
		}
	}


	String originalHandle;
	public void WindowHandle() throws InterruptedException
	{
		originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		sleep(10);
	}
	public void SwitchBackWindow() throws InterruptedException
	{

		Initialization.driver.close();
		Initialization.driver.switchTo().window(originalHandle);
		sleep(10); 
	}

	public void ClicOnDynamicRefreshButton() throws InterruptedException
	{
		DynamicRefreshButton.click();
		while (Initialization.driver.findElement(By.xpath("//i[@class='fa fa-mobile dev_icn ']/following-sibling::div/div[4]")).isDisplayed()) 
		{
			sleep(1);
		}
		sleep(2);
	}

	int TotalLicenseused;
	int GridDeviceCount;
	int AssetTrackingReportDeviceCount;
	int DashboardDeviceCnt;


	public void ReadingLicenseCount(String Id)
	{
		try
		{
			String Text = Initialization.driver.findElement(By.xpath("//span[@id='"+Id+"']")).getText();
			String[] LicenseUsed = Text.split(" ");
			int Count = Integer.parseInt(LicenseUsed[0]); 
			TotalLicenseused=TotalLicenseused+Count;
		}
		catch(Exception e)
		{
			Reporter.log(Id+" has not used any license",true);
		}
		System.out.println("Total License Used Is : "+TotalLicenseused);
	}

	public void ReadingDeviceCountInDeviceGrid()
	{
		String CountInGrid = Initialization.driver.findElement(By.xpath("//span[@class='tableDevCount_line']")).getText();
		String[] DeviceCountInGrid = CountInGrid.split(" ");
		GridDeviceCount = Integer.parseInt(DeviceCountInGrid[0]); 
		System.out.println("Device Count In Device Grid Is "+GridDeviceCount);
	}

	public void ReadingDeviceCountInAssettrackingReport()
	{
		String CountInAssetReport = Initialization.driver.findElement(By.xpath("//span[@class='tableDevCount_line']")).getText();
		String[] DeviceCountInAssetReport1= CountInAssetReport.split(":");
		String[] DeviceCountInAssetReport=DeviceCountInAssetReport1[1].split(" ");
		AssetTrackingReportDeviceCount = Integer.parseInt(DeviceCountInAssetReport[1]);
		System.out.println("Device Count In Asset Tracking Report Is : "+AssetTrackingReportDeviceCount);
	}

	public void EnablingOnlineOfflineDashBoard() throws InterruptedException
	{
		WebElement onlineofflineDashBoardCheckBox = Initialization.driver.findElement(By.xpath("(//ul[@class='custom-legend doughnut-legend']//span[@class='valueCover'])[2]"));
		String OnlineOfflineDashBoardStatus = onlineofflineDashBoardCheckBox.getAttribute("class");
		if(OnlineOfflineDashBoardStatus.equals("chkIcn fa fa-square-o"))
		{
			onlineofflineDashBoardCheckBox.click();
			sleep(5);
		}
	}

	public void ReadingDeviceCountInOnlineOfflineDashBoard() throws InterruptedException
	{
		EnablingOnlineOfflineDashBoard();
		WebElement OnlineOfflineDashboard=Initialization.driver.findElement(By.xpath("//li[@class='totalCount']/span[2]")); 
		js.executeScript("arguments[0].scrollIntoView(true);", OnlineOfflineDashboard);
		String DeviceCountInDashBoard = OnlineOfflineDashboard.getText();
		DashboardDeviceCnt = Integer.parseInt(DeviceCountInDashBoard);
		System.out.println("Device Count In Dash Board is : "+DashboardDeviceCnt);
	}

	public void VerifyDeviceCountMatchingInGrid_AssetReport_DashBoard_LicenseUsedSection()
	{
		if(TotalLicenseused==GridDeviceCount&&TotalLicenseused==AssetTrackingReportDeviceCount&&TotalLicenseused==DashboardDeviceCnt)
		{
			Reporter.log("Device Count Matched In Asset Report,Dash Board & Device Grid,License Used",true);
		}

		else
		{
			ALib.AssertFailMethod("Device Count didn't Matched In Asset Report,Dash Board & Device Grid,License Used");
		}
	}

	public String OSBuildNumberInConsole;
	String OSBuildNumberInDevice;
	public void ReadingOSBuildNumberInConsole()
	{
		WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='OsBuildNumber']"));
		js.executeScript("arguments[0].scrollIntoView(true);", ele);
		OSBuildNumberInConsole = ele.getText();
	}

	public void SearchingInsideSettings(String SearchContent) throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@index='0']").click();
		sleep(2);
		Initialization.driverAppium.findElementById("com.android.settings.intelligence:id/search_src_text").sendKeys(SearchContent);
		sleep(4);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+SearchContent+"']").click();
		sleep(3);
	}

	public void ReadingOSBuildNumberInDevice()
	{
		OSBuildNumberInDevice= Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Build number']/../android.widget.TextView[2]").getText();
	}

	public void VerifyOsbuilsNumberIsMatchingInDeviceAndConsole()
	{
		if(OSBuildNumberInConsole.equals(OSBuildNumberInDevice))
		{
			Reporter.log("PASS>>> OS Build Number Is Matched In Both Device And Console",true);
		}

		else
		{
			ALib.AssertFailMethod("FAIL>>> OS Build Number Is Not Matched In Device And Console");
		}
	}

	@FindBy(xpath="//div[@id='storagemem']/p")
	public WebElement StorageMemory_Memorymanagement1;

	@FindBy(xpath="//div[@id='storagemem_0']/p")
	public WebElement StorageMemory_Memorymanagement2;

	@FindBy(xpath="//div[@id='programmem']/p")
	public WebElement ProgramMemory_Memorymanagement;

	SoftAssert sof=new SoftAssert();
	boolean elementStatus;

	public void VerifymemorymanagementInInfoPanel(WebElement ele)
	{
		try
		{
			elementStatus=	ele.isDisplayed();
			String Memory = ele.getText();
			if(Memory.contains("free of") && !Memory.contains("N/A"))
			{
				Reporter.log("PASS>>> Memory Is Shown Correctly In Memory Management: "+Memory,true); 
			}
			else
			{
				ALib.AssertFailMethod("FAIL>>> Memory Is Not Shown Correctly In Memory Management: "+Memory);
			}
		}
		catch (Exception e) 
		{
			Reporter.log("Only One Drive Is Enabled",true);
		}
	}
	public void VerifymemorymanagementInInfoPanel_Nix(WebElement ele)
	{
		try
		{
			elementStatus=	ele.isDisplayed();
			String Memory = ele.getText();
			if(Memory.contains("free of") && !Memory.contains("N/A"))
			{
				Reporter.log("PASS>>> Memory Is Shown Correctly In Memory Management: "+Memory,true); 
			}
			else
				ALib.AssertFailMethod("FAIL>>> Memory Is Not Shown Correctly In Memory Management: "+Memory);
		}
		catch (Exception e) 
		{
			ALib.AssertFailMethod("Only One Drive Is Shown");
		}
	}

	@FindBy(xpath="//button[@class='btn btn-default dropdown-toggle']")
	private WebElement MenuItems;

	@FindBy(xpath="//ul[@class='maincolumnListOptions']//input[@type='checkbox']")
	private List<WebElement> MenuCheckBox;

	public void ClickOnMenuInConsole() throws InterruptedException {
		Actions action=new Actions(Initialization.driver);
		action.click(MenuItems).build().perform();
		waitForXpathPresent("//ul[@class='maincolumnListOptions']//input[@type='checkbox']");
		sleep(2);
		Reporter.log("clicked On Menu",true);
	}

	public void SelectAllMenuOptions() throws InterruptedException {
		for(int i=0;i<MenuCheckBox.size()-1;i++) 
		{
			boolean flag=MenuCheckBox.get(i).isSelected();
			if(flag==false)
			{
				MenuCheckBox.get(i).click();
				sleep(1);
			}
		}
	}

	public void VerifyFreeStorageMemoryForEMMEnrolledDevice(String Value)
	{
		String DeviceFreeStorage = Initialization.driver.findElement(By.xpath("//td[@class='MemoryStorageAvailable']")).getText();
		System.out.println(DeviceFreeStorage);
		boolean value = DeviceFreeStorage.contains(Value);
		String PassStatement="PASS >>Device Free Storage For EMM Device is Displayed Correctly"+" : "+DeviceFreeStorage ;
		String FailStatement="FAIL >>Device Free Storage Foe EMM Device is not Displayed Correctly"+" : "+DeviceFreeStorage;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyEnabledCustomColumnInDeviceInfoPanel(String ColumnName)
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//div[@id='dis-card']//div[text()='"+ColumnName+"']")).isDisplayed();
			Reporter.log("Enabled Custom Column Name Is Shown Correctly In Info Panel",true);
		}
		catch (Exception e) 
		{
			ALib.AssertFailMethod("Enabled Custom Column Name Is Not Shown Correctly In Info Panel");	
		}
	}


	///////////////////////////////////XSLT MUltipleDevices////////////////////////

	@FindBy(xpath="//ul[@id='customMenu']/li/span[text()='SureLock']")
	private WebElement MultipldeviceRightClick_Surelock;

	@FindBy(id="activation_code")
	private WebElement ActivationCodeTextField;

	@FindBy(id="activationcode_okbtn")
	private WebElement ActivationCodeOkButton;

	@FindBy(id="activation_code_pswd")
	private WebElement ActivationCode_PwdTextField;


	Actions action = new Actions(Initialization.driver);
	public void SelectingAndRightClickingOnMultipleDevice() throws InterruptedException
	{
		WebElement dev1 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[1]/td[2]"));
		WebElement dev2 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[2]/td[2]"));
		action.keyDown(Keys.CONTROL).click(dev1).click(dev2).keyUp(Keys.CONTROL).build().perform();
		sleep(2);
		action.contextClick(dev1).build().perform();
		sleep(3);
	}

	public void SelctingXSLT_RightClick(String XSLT) throws InterruptedException
	{
		WebElement xslt = Initialization.driver.findElement(By.xpath("//ul[@id='customMenu']/li/span[text()='"+XSLT+"']"));
		action.moveToElement(xslt).click().build().perform();
		sleep(4);
		Reporter.log("Clicked on "+XSLT+" XSLT",true);
	}

	public void SelectingXSLTOptions(String XSLTType,String Option) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//ul[@id='"+XSLTType+"']/li/span[text()='"+Option+"']")).click();
		sleep(4);
		Reporter.log("Clicked On "+Option+" Button",true);
	}

	public void InstallingApplicationThroughRightClick()
	{
		Initialization.driver.findElement(By.xpath("//p[contains(text(),'Do you want to install?')]/parent::div/following-sibling::div/button[text()='Yes']")).click();
		Reporter.log("Clicked On XSLT install Yes Button",true);
	}

	public void ClickOninstallbuttonforNonKnoxDevice() throws InterruptedException
	{
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,300);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='INSTALL']")));
		sleep(3);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='INSTALL']").click();
	}

	public void WaitingForConsoleUpdation(int waittime) throws InterruptedException
	{
		Reporter.log("Waiting For Console Updation",true);
		sleep(waittime);
	}


	public void ClickOnButtonsforNonKnoxDevice(String Button,int SleepTime) throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='"+Button+"']").click();
		sleep(SleepTime);
	}

	public void verifyApplicationInstallation(String Package,String ApplicationName)
	{
		boolean installationStatus = Initialization.driverAppium.isAppInstalled(Package);
		if(installationStatus)
		{
			Reporter.log("PASS>>> "+ApplicationName+" Application Installed Successfully Through XSLT",true);
		}
		else
		{
			System.out.println("No");
			//	 ALib.AssertFailMethod("FAIL>>> "+ApplicationName+" Application Is Not Installed Through XSLT");
		}
	}

	public void SendingActivationCode(String ActivationCode) throws InterruptedException
	{
		ActivationCodeTextField.sendKeys(ActivationCode);
		sleep(2);
		ActivationCodeOkButton.click();
		sleep(3);
	}

	public void EnteringSurLock(String DeviceSerialNumber) throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb -s "+DeviceSerialNumber+" shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surelock");
		sleep(4);	
	}

	CommonMethods_POM common=new CommonMethods_POM();
	public void ClickOnAboutSureLock(String Element) throws InterruptedException
	{
		common.commonScrollMethod(Element);
		common.ClickOnAppiumText(Element);
	}

	public void VerifySureLockIsLicensedWhenActivatedFromConsole()
	{
		String ActivationStatus=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Activate']").getAttribute("enabled");
		if(ActivationStatus.equals("true"))
		{
			ALib.AssertFailMethod("FAIL>>> SureLock Is Not Activated Even When Activated From Console");
		}

		else if(ActivationStatus.equals("false"))
		{
			Reporter.log("PASS>>> SureLock Is Activated Even When Activated From Console",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> SureLock Is Not Activated Even When Activated From Console");
		}
	}

	public void VerifySureLockDeactivateddWhenDeActivatedFromConsole()
	{
		try
		{
			Initialization.driverAppium.findElementById("com.gears42.surelock:id/skipButton").isDisplayed();
			Reporter.log("PASS>>> SureLock Is Deactivated Successfully",true);
		}

		catch (Exception e) 
		{
			ALib.AssertFailMethod("FAIL>>> SureLock Is Not Deactivated");	
		} 
	}

	public void SendingSureLockPassword(String Password) throws InterruptedException
	{
		ActivationCode_PwdTextField.sendKeys(Password);
		sleep(2);
		ActivationCodeOkButton.click();
	}

	public void VerifyExitFromSureLockAndSureFox()
	{

		String pacakgeName = Initialization.driverAppium.getCapabilities().getCapability("appPackage").toString();
		if(pacakgeName.equals("com.nix"))
		{
			Reporter.log("PASS>>> Device Succcessfully Exited From SureLock Kiosk Mode",true);
		}

		else
		{
			ALib.AssertFailMethod("FAIL>>> Device Has Not Exited From SureLock Kiosk Mode");
		}
	}

	public void VerifySureLockLaunch() throws InterruptedException
	{


		try{
			Initialization.driverAppium.findElementById("com.gears42.surelock:id/continueButton").isDisplayed();
			Reporter.log("PASS>>>SureLock Has Launched Successfully",true);
		}

		catch (Exception e) {
			try
			{
				Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Tap the screen 5 times']").isDisplayed();
				Reporter.log("PASS>>>SureLock Has Launched Successfully",true);
			}
			catch (Exception f) 
			{
				ALib.AssertFailMethod("FAIL>>> SureLock Has Not Launched");
			}
		}
		sleep(3);	

	}

	public void VerifySureLockLaunch1() throws InterruptedException
	{

		try{
			Initialization.driverAppium1.findElementById("com.gears42.surelock:id/continueButton").isDisplayed();
			Reporter.log("PASS>>>SureLock Has Launched Successfully",true);
		}

		catch (Exception e) {
			try
			{
				Initialization.driverAppium1.findElementByXPath("//android.widget.TextView[@text='Tap the screen 5 times']").isDisplayed();
				Reporter.log("PASS>>>SureLock Has Launched Successfully",true);
			}
			catch (Exception f) 
			{
				ALib.AssertFailMethod("FAIL>>> SureLock Has Not Launched");
			}
		}
		sleep(3);	

	}

	public void UninstallingApplication(String Package)
	{
		Initialization.driverAppium.removeApp(Package);
	}

	public void UninstallingApplication1(String Package)
	{
		Initialization.driverAppium1.removeApp(Package);
	}

	public void InstallApplications_ADB(String UDID,String FilePath) throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb -s "+UDID+" install "+FilePath);
		sleep(10);
	}

	public void VerifySureLockUpgradation()
	{
		String Version = Initialization.driver.findElement(By.xpath("//td[@class='SureLockVersion']/p")).getText();
		float sureLockVersion = Float.parseFloat(Version);
		System.out.println("Current Version Of SureLock Is "+sureLockVersion);
		if(sureLockVersion>(9.61))
		{
			ALib.AssertFailMethod("FAIL>>> SureLock Is Not Upgraded");
		}
		else
		{
			Reporter.log("PASS>>> SureLock Is Upgradded Sucessfully",true);
		}
	}

	public void EnteringSurFox(String DeviceSerialNumber) throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb -s "+DeviceSerialNumber+" shell am broadcast -a com.gears42.surefox.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surefox");
		sleep(4);	
	}

	public void VerifySureFoxIsLicensedWhenActivatedFromConsole()
	{
		String ActivationStatus=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Activate']").getAttribute("enabled");
		if(ActivationStatus.equals("true"))
		{
			ALib.AssertFailMethod("FAIL>>> SureFox Is Not Activated Even When Activated From Console");
		}

		else if(ActivationStatus.equals("false"))
		{
			Reporter.log("PASS>>> SureFox Is Activated Even When Activated From Console",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> SureFox Is Not Activated Even When Activated From Console");
		}
	}

	public void VerifySureFoxDeactivateddWhenDeActivatedFromConsole()
	{
		try
		{
			Initialization.driverAppium.findElementById("com.gears42.surefox:id/skipButton").isDisplayed();
			Initialization.driverAppium.findElementById("com.gears42.surefox:id/skipButton").click();
			sleep(3);
			Reporter.log("PASS>>> SureFox Is Deactivated Successfully",true);
		}

		catch (Exception e) 
		{
			ALib.AssertFailMethod("FAIL>>> SureFox Is Not Deactivated");	
		} 
	}

	public void VerifySureFoxLaunchDevice2()
	{
		try{
			boolean boo = Initialization.driverAppium1.findElementByXPath("//android.widget.TextView[@text='Tap the screen 5 times']").isDisplayed();
			Reporter.log("PASS>>> SureFox Launcehed Successfully",true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> SureFox Is Not Launched");
		}

	}
	public void VerifySureFoxLaunchDevice1()
	{
		try{
			boolean boo = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Tap the screen 5 times']").isDisplayed();
			Reporter.log("PASS>>> SureFox Launcehed Successfully",true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> SureFox Is Not Launched");
		}

	}

	public void VerifySureFoxUpgradation()
	{
		String Version = Initialization.driver.findElement(By.xpath("//td[@class='SureFoxVersion']/p")).getText();
		float sureFoxVersion = Float.parseFloat(Version);
		System.out.println("Current Version Of SureLock Is "+sureFoxVersion);
		if(sureFoxVersion>(7.83))
		{
			Reporter.log("PASS>>> SureFox Is Upgraded Sucessfully",true);

		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> SureFox Is Not Upgraded");
		}
	}

	public void VerifyWhetherApplicationIsLicensed(String ApplicationName)
	{
		String LicensedStatus = Initialization.driver.findElement(By.xpath("//td[@class='"+ApplicationName+"']/p/img")).getAttribute("title");
		if(!LicensedStatus.equals("Licensed version"))
		{
			ALib.AssertFailMethod("FAIL>>> Application Is Not Activated Even After Activating From Console");
		}
		Reporter.log("PASS>>> Application Is Activated",true);

	}

	public void VerifyApplicationLicenseIsDeactivated(String ApplicationName)
	{
		String LicensedStatus = Initialization.driver.findElement(By.xpath("//td[@class='"+ApplicationName+"']/p/img")).getAttribute("title");
		if(LicensedStatus.equals("Licensed version"))
		{ 
			ALib.AssertFailMethod("FAIL>>> Application Is Not DeActivated Even After Activating From Console");
		}
		Reporter.log("PASS>>> Application Is Deactivated Successfully",true);
	}

	public void VerifySureVideoLaunchDevice1()
	{
		try{
			Initialization.driverAppium.findElementById("com.gears42.surevideo:id/button_continue_done").isDisplayed();
			Reporter.log("PASS>>> SureVideo Launched Successfully",true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> SureVideo Is Not Launched");
		}

	}
	public void VerifySureVideoLaunchDevice2()
	{
		try{
			Initialization.driverAppium.findElementById("com.gears42.surevideo:id/button_continue_done").isDisplayed();
			Reporter.log("PASS>>> SureVideo Launcehed Successfully",true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> SureVideo Is Not Launched");
		}
	}

	public void VerifExitfromSureVideoDevice1()
	{
		try
		{
			Initialization.driverAppium.findElementById("com.gears42.surevideo:id/button_continue_done").isDisplayed();
			ALib.AssertFailMethod("FAIL>>> Device Is Not Exited From SureVideo");
		}
		catch (Exception e) 
		{
			Reporter.log("PASS>>> Exited From SureVideo Successfully",true);
		}
	}

	public void VerifExitfromSureVideoDevice2()
	{
		try
		{
			Initialization.driverAppium1.findElementById("com.gears42.surevideo:id/button_continue_done").isDisplayed();
			ALib.AssertFailMethod("FAIL>>> Device Is Not Exited From SureVideo");
		}
		catch (Exception e) 
		{
			Reporter.log("PASS>>> Exited From SureVideo Successfully",true);
		}
	}

	public void VerifySureVideoUpgradation()
	{
		String Version = Initialization.driver.findElement(By.xpath("//td[@class='SureVideoVersion']/p")).getText();
		float sureFoxVersion = Float.parseFloat(Version);
		System.out.println("Current Version Of SureVideo Is "+sureFoxVersion);
		if(sureFoxVersion>(3.30))
		{
			Reporter.log("PASS>>> SureVideo Is Upgraded Sucessfully",true);

		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> SureVideo Is Not Upgraded");
		}
	}


	//Location Tracking

	@FindBy(xpath="//td[@class='ConnectionStatus']/div/i[2]")
	private WebElement LocationTrackingIcon;

	
	public void VerifyLocationTrackingICOnWhenLocationIsNotAvailable(String div)
	{
		String LocationTrackingStatus = LocationTrackingIcon.getAttribute("title");
		System.out.println(LocationTrackingStatus);
		String LocationTrackingStatus1 = LocationTrackingIcon.getText();
		System.out.println(LocationTrackingStatus1);
		
		if(LocationTrackingStatus.equals("Location tracking of <'"+div+"'> turned ON."))
		{
			Reporter.log("PASS>>> Location Tracking Icon Is Displayed Correctly When Location Tracking Is Not Available",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Location Tracking Icon Is Not Displayed Correctly When Location Tracking Is Not Available");
		}

	}

	public void VerifyLocationTrackingIconWhenLocationIsAvailableButLocationTrackingIsOFF()
	{
		String LocationTrackingStatus = LocationTrackingIcon.getAttribute("class");
		if(LocationTrackingStatus.equals("locTrack_icn fa fa-map-marker tracking-color-orange")||LocationTrackingStatus.equals("locTrack_icn fa fa-map-marker tracking-color-green"))
		{
			Reporter.log("PASS>>> Location Tracking Icon Is Displayed Correctly When Location Tracking Is  Available",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Location Tracking Icon Is Not Displayed Correctly When Location Tracking Is  Available");
		}
	}

	public void VerifyLocationTrackingIconWhenLocationIsAvailable()
	{
		String LocationTrackingStatus = LocationTrackingIcon.getAttribute("title");
		if(LocationTrackingStatus.equals("Tracking[On], Location[Latest Available]"))
		{
			Reporter.log("PASS>>> Location Tracking Icon Is Displayed Correctly When Location Tracking Is  Available",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Location Tracking Icon Is Not Displayed Correctly When Location Tracking Is  Available");
		}
	}

	public void VerifyIntelAMTStatusInGrid()
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//th[@class='AMTConnectionStatus']")).isDisplayed();
			Reporter.log("PASS>>> Intel AMT Status Column Is Displayed In Console",true);
		}
		catch(Exception e)
		{
			ALib.AssertFailMethod("FAIL>>> Intel AMT Status Is Not Displayed In Consle");
		}
	}


	public void AssertTrueMethod(boolean value,String PassStatement,String FailStatement)
	{
		Assert.assertTrue(value,FailStatement);
		Reporter.log(PassStatement,true);
	}

	
	 public void AppiumConfigurationCommonMethod(String appPackage,String activity) throws IOException, InterruptedException
		{
		 
		    
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("deviceName", "My Phone");
			caps.setCapability("platformName", "Android");
			caps.setCapability("appPackage",appPackage);
			caps.setCapability("appActivity",activity);
			caps.setCapability("noReset", "true");
			System.out.println("No Reset happened");
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,4000);
			System.out.println("about to launch");
			try {
			   Initialization.driverAppium=new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			sleep(5);
		}
	 
	 @FindBy(xpath="//div[@id='jobQueueButton']")
		private WebElement JobQueueButton;
	 public void clickonJobQueue() throws InterruptedException
		{
			JobQueueButton.click();
			waitForXpathPresent("//div[@id='jobQueueRefreshBtn']");
			sleep(4);
		}
	 
	 @FindBy(xpath="//li[@id='jobQueueHistoryTab']")
		private WebElement JobHistoryTab;

		public void clickOnJobhistoryTab() throws InterruptedException
		{
			JobHistoryTab.click();
			waitForXpathPresent("//div[@id='jobQueueHistoryRefreshBtn']");
			sleep(5);
		}
		
	 public void CheckStatusOfappliedInstalledJob(String JobName,int MaximumTime) throws InterruptedException
		{
			clickonJobQueue();
			try {

				WebDriverWait wait1 = new WebDriverWait(Initialization.driver, MaximumTime);
				wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='No Pending jobs']"))).isDisplayed();
				Reporter.log("PASS >> Job Deployed Successfully On Device",true);
				sleep(3);
				clickOnJobhistoryTab();
				System.out.println("Clicked On Job Completed Tab");
				String LatestJobName = LatestJobNameInsideHistory.getText();
				if(LatestJobName.equals(JobName))
				{
					VerifyJobStatusInsideJobQueue();
				}
				else
				{
					ClickOnJobQueueCloseButton();
					ALib.AssertFailMethod("FAIL >> Job Name Is Different In Job Queue");
				}
			}
			catch (Exception e)
			{
				String CurrentIncompletedJobStatus = IncompleteJobStatus.getText();
				System.out.println("Incompleted Job Status is: " +CurrentIncompletedJobStatus);
				ClickOnJobQueueCloseButton();
				ALib.AssertFailMethod("FAIL >> Job Did not deployed within given time");
			}
			ClickOnJobQueueCloseButton();
		}

		@FindBy(xpath="//span[contains(text(),'Initiated')]")
		private WebElement InitiateJob;
		public void JobInitiatedOnDevice() throws InterruptedException
		{

			if(InitiateJob.isDisplayed()) {
				Reporter.log(" Job is initiated message is displayed",true);
			}else {
				ALib.AssertFailMethod("Install job is initiated message is not displayed" );
			}
			sleep(3);

		}
		
		@FindBy(xpath="//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
		private WebElement SearchTextBoxApplyJob;

		@FindBy(xpath=".//*[@id='applyJob_okbtn']")
		private WebElement OkButtonApplyJob;
		
		@FindBy(id="applyJobButton")
		private WebElement ApplyButton;
		
		public void ClickOnApplyButton() throws InterruptedException
		{
			ApplyButton.click();
			waitForidPresent("scheduleJob");
			sleep(9);
			Reporter.log("Clicked On Apply Button",true);
		}
		
		@FindBy(xpath="//a[text()='Home']")
		private WebElement Home;
		public void ClickOnHomePage() throws InterruptedException
		{
			Home.click();   
			waitForidPresent("deleteDeviceBtn");
			sleep(6);
			Reporter.log("Clicked On Home",true);
		}
		
		public void SearchDevice(String DeviceName) throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
		{
			
			SearchDeviceTextField.clear();
			sleep(3);
			SearchDeviceTextField.sendKeys(DeviceName);
			waitForXpathPresent("//p[text()='"+DeviceName+"']");
			sleep(5);
		}
		
		public void SearchDeviceInconsole() throws  EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
		{

			SearchDeviceTextField.clear();
			sleep(3);
			SearchDeviceTextField.sendKeys(Config.DeviceName);
			waitForXpathPresent("//p[text()='"+Config.DeviceName+"']");
			sleep(5);
		}
		public void SearchField(String JobName) throws InterruptedException
		{
			SearchTextBoxApplyJob.sendKeys(JobName);
			sleep(4);
			waitForXpathPresent("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='"+JobName+"']");
			sleep(6);
			WebElement JobSelected = Initialization.driver.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='"+JobName+"']"));
			JobSelected.click();
			sleep(2);
			OkButtonApplyJob.click();
			sleep(2);
		}
	 @FindBy(xpath="//table[@id='jobQueueHistoryGrid']/tbody/tr[1]/td[6]")
		private WebElement AppliedJobStatus;
	 public void VerifyJobStatusInsideJobQueue()
		{
			String ActualVal=AppliedJobStatus.getText();

			String Expected="Deployed";
			String Pass="PASS >>> Job Status Is Deployed Inside Job Queue";
			String Fail="FAIL >>> Job Status Is"+" "+ActualVal+" "+"Inside Job Queue";
			ALib.AssertEqualsMethod(Expected, ActualVal, Pass, Fail);
		}
	 
	 @FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[6]/p")
		private WebElement IncompleteJobStatus;
	 
	 @FindBy(xpath="//table[@id='jobQueueHistoryGrid']/tbody/tr[1]/td[2]")
		private WebElement LatestJobNameInsideHistory;
	 
		@FindBy(xpath="//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
		private WebElement SearchJobField;
		        public void ClearSearchJobInSearchField() throws InterruptedException
	    {
	            SearchJobField.clear();
	            
	            sleep(4);

	    }
		        
		        @FindBy(xpath="//div[@id='device_jobQ_modal']/div[1]/button")
		    	private WebElement JobQueueCloseButton;
		        public void ClickOnJobQueueCloseButton() throws InterruptedException 
		    	{try {
		    		JobQueueCloseButton.click();
		    		waitForidPresent("deleteDeviceBtn");
		    		sleep(2);}catch(Exception e) {

		    		}
		    	}
		        
		        public void ClickOnAdvanceSeach(String ClassName, String ColumnName) throws InterruptedException
		    	{
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		Initialization.driver.findElement(By.xpath("//td[@class='"+ClassName+"']/div/input")).click();
		    	}
		        
		        public void SelectDateFromCal(String LastConnected) throws InterruptedException {
		    		sleep(3);
		    		List<WebElement> sasa = Initialization.driver.findElements(By.xpath("//*[@class='daterangepicker ltr show-ranges show-calendar openscenter']"));
		    		int i=0;
		    		for (WebElement cal:sasa) {

		    			String styleval = cal.getAttribute("style");
		    			if (!styleval.isEmpty()) {
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		Initialization.driver.findElement(By.xpath("//*[@class='daterangepicker ltr show-ranges show-calendar openscenter']["+i+"]/div/ul/li[contains(text(),'"+LastConnected+"')]")).click();
		    		//ApplyBtn.click();
		    		sleep(10);
		    	}
		        
		        public void verifySelectedTimeForThisMonth(String ColumnName) throws InterruptedException, ParseException
		    	{ 
		    		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		    		Calendar aCalendar = Calendar.getInstance();

		    		aCalendar.set(Calendar.DATE, 1);
		    		aCalendar.set(Calendar.HOUR_OF_DAY, 0);
		    		Date firstDateOfCurrentMonth = aCalendar.getTime();
		    		String dayFirst = DateForMat.format(firstDateOfCurrentMonth);
		    		Date MonthsFirstDay = DateForMat.parse(dayFirst);

		    		Date date = new Date();
		    		String stringEndDate= DateForMat.format(date);
		    		Date EndRange = DateForMat.parse(stringEndDate);

		    		sleep(10);
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement columnValue:columnValues) {
		    			Date consoleDate = DateForMat.parse(columnValue.getText());

		    			if (((consoleDate.compareTo(MonthsFirstDay) > 0) && ((consoleDate.compareTo(EndRange) < 0)))||(consoleDate.compareTo(EndRange) == 0)){
		    				Reporter.log(columnValue.getText() +" is expected value",true);
		    			}else {
		    				Reporter.log(columnValue.getText() +" is expected value",true);

		    			}
		    		}
		    	}
		        
		        public void verifySelectedTimeForLast30Days(String ColumnName) throws InterruptedException, ParseException
		    	{ 
		    		Date getdate = new Date();
		    		Date strtdate = new Date(getdate.getTime()-1000L*60L*60L*24L*30L);

		    		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		    		String stringDate= DateFor.format(strtdate);
		    		Date previouRange = DateFor.parse(stringDate);

		    		Date date = new Date();
		    		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		    		String stringEndDate= DateForMat.format(date);
		    		Date EndRange = DateFor.parse(stringEndDate);

		    		sleep(10);
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement columnValue:columnValues) {
		    			Date consoleDate = DateFor.parse(columnValue.getText());

		    			if ((consoleDate.compareTo(previouRange) > 0) && ((consoleDate.compareTo(EndRange) < 0))||(consoleDate.compareTo(EndRange) == 0)||(consoleDate.compareTo(previouRange) == 0)){
		    				Reporter.log(columnValue.getText() +" is expected value",true);
		    			}else {
		    				ALib.AssertFailMethod("Advance Search is not working");
		    			}
		    		}
		    	}

		        public void VerifyDecendingDateTimeSorting(String ColumnName) throws InterruptedException, ParseException {	
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			String tmp = column.getText();
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		    		sleep(3);
		    		List<Date> listDates = new ArrayList<Date>();
		    		SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		    		List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement col:ColumnValue) {
		    			String fortest = col.getText();
		    			listDates.add(DATE_FOMAT.parse(col.getText()));
		    		}
		    		Collections.sort(listDates, new Comparator<Date>() {
		    			@Override
		    			public int compare(Date lhs, Date rhs) {
		    				if(lhs.getTime()>rhs.getTime()) {
		    					return -1;
		    				}else if(lhs.getTime() == rhs.getTime()) {
		    					return 0;
		    				}else {
		    					return 1;
		    				}
		    			}
		    		});

		    		List<String> JavaReverseSorting = new ArrayList<>();
		    		SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		    		for(Date d : listDates) {
		    			JavaReverseSorting.add(DATE_FOMAT2.format(d));
		    		}

		    		List<WebElement> ReverseSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(int j=0;j<ReverseSortColumnValue.size();j++) {
		    			String ReverseSort = ReverseSortColumnValue.get(j).getText();

		    			SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		    			Date ReverseValueDateFormate = DATE_FOMAT3.parse(ReverseSort);
		    			String ReverseValueStringFormate = DATE_FOMAT3.format(ReverseValueDateFormate);
		    			if(ReverseValueStringFormate.equals( JavaReverseSorting.get(j))){
		    				Reporter.log(ReverseValueStringFormate +" Column Value");
		    			}else {
		    				ALib.AssertFailMethod(ReverseValueStringFormate +"Reversing Sorting of Column has been failed");
		    			}
		    		}	
		    	}

		        public void VerifyAscendingDateTimeSorting(String ColumnName) throws InterruptedException, ParseException {
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		    		sleep(3);
		    		List<Date> listDates = new ArrayList<Date>();
		    		SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		    		List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement col:ColumnValue) {
		    			String sdsd = col.getText();
		    			listDates.add(DATE_FOMAT.parse(col.getText()));
		    		}
		    		Collections.sort(listDates, new Comparator<Date>() {
		    			@Override
		    			public int compare(Date lhs, Date rhs) {
		    				if(lhs.getTime()<rhs.getTime()) {
		    					return -1;
		    				}else if(lhs.getTime() == rhs.getTime()) {
		    					return 0;
		    				}else {
		    					return 1;
		    				}
		    			}
		    		});

		    		List<String> JavaSorting = new ArrayList<>();
		    		SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		    		for(Date d : listDates) {
		    			JavaSorting.add(DATE_FOMAT2.format(d));
		    		}

		    		List<WebElement> NaturalSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(int j=0;j<NaturalSortColumnValue.size();j++) {
		    			String NaturalSort = NaturalSortColumnValue.get(j).getText();

		    			SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
		    			Date NaturalValueDateFormate = DATE_FOMAT3.parse(NaturalSort);
		    			String NaturalValueStringFormate = DATE_FOMAT3.format(NaturalValueDateFormate);
		    			if(NaturalValueStringFormate.equals( JavaSorting.get(j))){
		    				Reporter.log(NaturalValueStringFormate +" Column Value");
		    			}else {
		    				ALib.AssertFailMethod(NaturalValueStringFormate +"Reversing Sorting of Column has been failed");
		    			}
		    		}
		    	}
		        public void VerifyDecendingDateTimeSortingForSecurityPatchDateCol(String ColumnName) throws InterruptedException, ParseException {	
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			String tmp = column.getText();
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		    		sleep(3);
		    		List<Date> listDates = new ArrayList<Date>();
		    		SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy");
		    		List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement col:ColumnValue) {
		    			String fortest = col.getText();
		    			listDates.add(DATE_FOMAT.parse(col.getText()));
		    		}
		    		Collections.sort(listDates, new Comparator<Date>() {
		    			@Override
		    			public int compare(Date lhs, Date rhs) {
		    				if(lhs.getTime()>rhs.getTime()) {
		    					return -1;
		    				}else if(lhs.getTime() == rhs.getTime()) {
		    					return 0;
		    				}else {
		    					return 1;
		    				}
		    			}
		    		});

		    		List<String> JavaReverseSorting = new ArrayList<>();
		    		SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy");
		    		for(Date d : listDates) {
		    			JavaReverseSorting.add(DATE_FOMAT2.format(d));
		    		}

		    		List<WebElement> ReverseSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(int j=0;j<ReverseSortColumnValue.size();j++) {
		    			String ReverseSort = ReverseSortColumnValue.get(j).getText();

		    			SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy");
		    			Date ReverseValueDateFormate = DATE_FOMAT3.parse(ReverseSort);
		    			String ReverseValueStringFormate = DATE_FOMAT3.format(ReverseValueDateFormate);
		    			if(ReverseValueStringFormate.equals( JavaReverseSorting.get(j))){
		    				Reporter.log(ReverseValueStringFormate +" Column Value");
		    			}else {
		    				ALib.AssertFailMethod(ReverseValueStringFormate +"Reversing Sorting of Column has been failed");
		    			}
		    		}	
		    	}
		        public void VerifyAscendingDateTimeSortingForSecurityPatchDateCol(String ColumnName) throws InterruptedException, ParseException {
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		    		sleep(3);
		    		List<Date> listDates = new ArrayList<Date>();
		    		SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy");
		    		List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement col:ColumnValue) {
		    			String sdsd = col.getText();
		    			listDates.add(DATE_FOMAT.parse(col.getText()));
		    		}
		    		Collections.sort(listDates, new Comparator<Date>() {
		    			@Override
		    			public int compare(Date lhs, Date rhs) {
		    				if(lhs.getTime()<rhs.getTime()) {
		    					return -1;
		    				}else if(lhs.getTime() == rhs.getTime()) {
		    					return 0;
		    				}else {
		    					return 1;
		    				}
		    			}
		    		});

		    		List<String> JavaSorting = new ArrayList<>();
		    		SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy");
		    		for(Date d : listDates) {
		    			JavaSorting.add(DATE_FOMAT2.format(d));
		    		}

		    		List<WebElement> NaturalSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(int j=0;j<NaturalSortColumnValue.size();j++) {
		    			String NaturalSort = NaturalSortColumnValue.get(j).getText();

		    			SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy");
		    			Date NaturalValueDateFormate = DATE_FOMAT3.parse(NaturalSort);
		    			String NaturalValueStringFormate = DATE_FOMAT3.format(NaturalValueDateFormate);
		    			if(NaturalValueStringFormate.equals( JavaSorting.get(j))){
		    				Reporter.log(NaturalValueStringFormate +" Column Value");
		    			}else {
		    				ALib.AssertFailMethod(NaturalValueStringFormate +"Reversing Sorting of Column has been failed");
		    			}
		    		}
		    	}
		        public void verifySelectedTimeForToday(String ColumnName) throws InterruptedException, ParseException
		    	{ 
		    		Date date = new Date();
		    		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		    		String stringDate= DateFor.format(date);
		    		Date d1 = DateFor.parse(stringDate);

		    		sleep(10);
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		if(columnValues.size()==0) {
		    			try {
		    				NoDeviceAvailable.isDisplayed();
		    				Reporter.log("No Device is availble with filter" +d1);}
		    			catch(Exception e) {
		    				ALib.AssertFailMethod("Advance Search is not working");
		    			}
		    		}else {
		    			for(WebElement columnValue:columnValues) {
		    				Date d2 = DateFor.parse(columnValue.getText());
		    				if (d1.compareTo(d2) == 0){
		    					Reporter.log(columnValue.getText() +" is expected value",true);
		    				}else {
		    					ALib.AssertFailMethod("Advance Search is not working");
		    				}
		    			}
		    		}
		    	}


		        public void selectPageAndVerifySortingDateTime(String PaginationNumber,String ColumnName) throws InterruptedException, ParseException {	
		    		sleep(3);
		    		if(Pagination.isDisplayed()) {
		    			sleep(6);
		    			Initialization.driver.findElement(By.xpath("//a[text()='"+PaginationNumber+"']")).click();
		    			VerifySortingNotChangedSecurityPatch(ColumnName);
		    		}}

		        public void VerifySortingNotChangedSecurityPatch(String ColumnName) throws InterruptedException, ParseException {
		    		sleep(2);
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		sleep(3);
		    		List<Date> listDates = new ArrayList<Date>();
		    		SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy");
		    		List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement col:ColumnValue) {
		    			if (!(col.getText().equals("N/A"))){
		    				String sdsd = col.getText();
		    				listDates.add(DATE_FOMAT.parse(col.getText()));
		    			}}
		    		Collections.sort(listDates, new Comparator<Date>() {
		    			@Override
		    			public int compare(Date lhs, Date rhs) {
		    				if(lhs.getTime()<rhs.getTime()) {
		    					return -1;
		    				}else if(lhs.getTime() == rhs.getTime()) {
		    					return 0;
		    				}else {
		    					return 1;
		    				}
		    			}
		    		});

		    		List<String> JavaSorting = new ArrayList<>();
		    		SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy");
		    		for(Date d : listDates) {
		    			JavaSorting.add(DATE_FOMAT2.format(d));
		    		}



		    		List<WebElement> NaturalSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));

		    		int sort = NaturalSortColumnValue.size()-JavaSorting.size();
		    		int j=0;
		    		for (int k=sort;k<sort+JavaSorting.size();k++) {

		    			String NaturalSort = NaturalSortColumnValue.get(k).getText();

		    			SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy");
		    			Date NaturalValueDateFormate = DATE_FOMAT3.parse(NaturalSort);
		    			String NaturalValueStringFormate = DATE_FOMAT3.format(NaturalValueDateFormate);
		    			if(NaturalValueStringFormate.equals( JavaSorting.get(j))){
		    				Reporter.log(NaturalValueStringFormate +" Column Value");
		    			}else {
		    				ALib.AssertFailMethod(NaturalValueStringFormate +"Reversing Sorting of Column has been failed");
		    			}
		    			j=j+1;
		    		}
		    	}

		        public void verifySelectedTimeForYesterday(String ColumnName) throws InterruptedException, ParseException
		    	{ 
		    		Date date = new Date();
		    		date .setTime(date.getTime()-24*60*60*1000); 
		    		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		    		String stringDate= DateFor.format(date);
		    		Date d1 = DateFor.parse(stringDate);

		    		sleep(10);
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		if(columnValues.size()==0) {
		    			try {
		    				NoDeviceAvailable.isDisplayed();
		    				Reporter.log("No Device is availble with filter" +d1);}
		    			catch(Exception e) {
		    				ALib.AssertFailMethod("Advance Search is not working");
		    			}
		    		}else {
		    			for(WebElement columnValue:columnValues) {
		    				Date d2 = DateFor.parse(columnValue.getText());
		    				if (d1.compareTo(d2) == 0){
		    					Reporter.log(columnValue.getText() +" is expected value",true);
		    				}else {
		    					ALib.AssertFailMethod("Advance Search is not working");
		    				}
		    			}
		    		}
		    	}

		        public void verifySelectedTimeForLastWeek(String ColumnName) throws InterruptedException, ParseException
		    	{ 
		    		Date strtdate = new Date();
		    		strtdate .setTime(strtdate.getTime()-7*24*60*60*1000); 

		    		SimpleDateFormat DateFor = new SimpleDateFormat("dd MMM yyyy");
		    		String stringDate= DateFor.format(strtdate);
		    		Date previouRange = DateFor.parse(stringDate);

		    		Date date = new Date();
		    		SimpleDateFormat DateForMat = new SimpleDateFormat("dd MMM yyyy");
		    		String stringEndDate= DateForMat.format(date);
		    		Date EndRange = DateFor.parse(stringEndDate);

		    		sleep(10);
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		List<WebElement> columnValues = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement columnValue:columnValues) {
		    			Date consoleDate = DateFor.parse(columnValue.getText());

		    			if ((consoleDate.compareTo(previouRange) > 0) && ((consoleDate.compareTo(EndRange) < 0))||(consoleDate.compareTo(EndRange) == 0)){
		    				Reporter.log(columnValue.getText() +" is expected value",true);
		    			}else {
		    				ALib.AssertFailMethod("Advance Search is not working");
		    			}
		    		}
		    	}

		        public void VerifyDecendingOrderSecurityPatchSorting(String ColumnName) throws InterruptedException, ParseException {	int i=0;
		    	VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    	List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    	for (WebElement column:AllColumnsinGrid) {
		    		String tmp = column.getText();
		    		if(column.getText().equalsIgnoreCase(ColumnName)){
		    			i=i+1;
		    			break;
		    		}
		    		i=i+1;
		    	}

		    	Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		    	sleep(3);
		    	List<Date> listDates = new ArrayList<Date>();
		    	SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy");
		    	List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    	for(WebElement col:ColumnValue) {
		    		if (!(col.getText().equals("N/A"))){
		    			String fortest = col.getText();
		    			listDates.add(DATE_FOMAT.parse(col.getText()));
		    		}
		    	}
		    	Collections.sort(listDates, new Comparator<Date>() {
		    		@Override
		    		public int compare(Date lhs, Date rhs) {
		    			if(lhs.getTime()>rhs.getTime()) {
		    				return -1;
		    			}else if(lhs.getTime() == rhs.getTime()) {
		    				return 0;
		    			}else {
		    				return 1;
		    			}
		    		}
		    	});

		    	List<String> JavaReverseSorting = new ArrayList<>();
		    	SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy");
		    	for(Date d : listDates) {
		    		JavaReverseSorting.add(DATE_FOMAT2.format(d));
		    	}
		    	System.out.println(DATE_FOMAT2);
		    	List<WebElement> ReverseSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    	for(int j=0;j<JavaReverseSorting.size();j++) {
		    		String ReverseSort = ReverseSortColumnValue.get(j).getText();

		    		SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy");
		    		Date ReverseValueDateFormate = DATE_FOMAT3.parse(ReverseSort);
		    		String ReverseValueStringFormate = DATE_FOMAT3.format(ReverseValueDateFormate);
		    		if(ReverseValueStringFormate.equals( JavaReverseSorting.get(j))){
		    			Reporter.log(ReverseValueStringFormate +" Column Value",true);
		    		}else {
		    			ALib.AssertFailMethod(ReverseValueStringFormate +"Reversing Sorting of Column has been failed");
		    		}
		    	}	
		    	}

		        public void VerifyAscendingOrderSecurityPatchSorting(String ColumnName) throws InterruptedException, ParseException {
		    		sleep(2);
		    		int i=0;
		    		VerifyColumnIsPresentOnGridAndScroll(ColumnName);
		    		List<WebElement> AllColumnsinGrid = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[1]/tr/th/div[1]"));
		    		for (WebElement column:AllColumnsinGrid) {
		    			if(column.getText().equalsIgnoreCase(ColumnName)){
		    				i=i+1;
		    				break;
		    			}
		    			i=i+1;
		    		}

		    		Initialization.driver.findElement(By.xpath("(//div[text()='"+ColumnName+"'])[1]")).click();
		    		sleep(3);
		    		List<Date> listDates = new ArrayList<Date>();
		    		SimpleDateFormat DATE_FOMAT = new SimpleDateFormat("dd MMM yyyy");
		    		List<WebElement> ColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));
		    		for(WebElement col:ColumnValue) {
		    			if (!(col.getText().equals("N/A"))){
		    				String sdsd = col.getText();
		    				listDates.add(DATE_FOMAT.parse(col.getText()));
		    			}}
		    		Collections.sort(listDates, new Comparator<Date>() {
		    			@Override
		    			public int compare(Date lhs, Date rhs) {
		    				if(lhs.getTime()<rhs.getTime()) {
		    					return -1;
		    				}else if(lhs.getTime() == rhs.getTime()) {
		    					return 0;
		    				}else {
		    					return 1;
		    				}
		    			}
		    		});

		    		List<String> JavaSorting = new ArrayList<>();
		    		SimpleDateFormat DATE_FOMAT2 = new SimpleDateFormat("dd MMM yyyy");
		    		for(Date d : listDates) {
		    			JavaSorting.add(DATE_FOMAT2.format(d));
		    		}



		    		List<WebElement> NaturalSortColumnValue = Initialization.driver.findElements(By.xpath("(//*[@id='tableHeader'])[2]/../tbody/tr/td["+i+"]"));

		    		int sort = NaturalSortColumnValue.size()-JavaSorting.size();
		    		int j=0;
		    		for (int k=sort;k<sort+JavaSorting.size();k++) {

		    			String NaturalSort = NaturalSortColumnValue.get(k).getText();

		    			SimpleDateFormat DATE_FOMAT3 = new SimpleDateFormat("dd MMM yyyy");
		    			Date NaturalValueDateFormate = DATE_FOMAT3.parse(NaturalSort);
		    			String NaturalValueStringFormate = DATE_FOMAT3.format(NaturalValueDateFormate);
		    			if(NaturalValueStringFormate.equals( JavaSorting.get(j))){
		    				Reporter.log(NaturalValueStringFormate +" Column Value");
		    			}else {
		    				ALib.AssertFailMethod(NaturalValueStringFormate +"Reversing Sorting of Column has been failed");
		    			}
		    			j=j+1;
		    		}
		    	}
		        
		        
		        public void verifyOfDefaultColInNewDns1()
		        {
		        	List<WebElement> ColumnHeaderCount=Initialization.driver.findElements(By.xpath("//*[@id='tableHeader']/tr/th"));
		        	int countOfColumns = ColumnHeaderCount.size();
		        	System.out.println(countOfColumns);
		        	for (int i = 0; i <=6; i++) {
		    			String actual = ColumnHeaderCount.get(i).getText();
		    			Reporter.log("Column Name Found --" + actual,true);

		    			if (i == 0) {
		    				if (actual.equalsIgnoreCase("Job Status"))
		    						{
		    					Reporter.log("PASS >> Column Name is correct",true);
		    						}else {
		    					Reporter.log("PASS >> Column Name isn't correct",true);	
		    						}
		    			}
		    			if(i==1)
		    			{
		    				if (actual.equalsIgnoreCase("Device"))
    						{
		    					Reporter.log("PASS >> Column Name is correct",true);
    						}else {
    							Reporter.log("PASS >> Column Name isn't correct",true);	
    						}
		    			}
		    			if(i==2)
		    			{
		    				if (actual.equalsIgnoreCase("Platform / Model"))
    						{
		    					Reporter.log("PASS >> Column Name is correct",true);
    						}else {
    							Reporter.log("PASS >> Column Name isn't correct",true);	
    						}
		    			}
		    			if(i==3)
		    			{
		    				if (actual.equalsIgnoreCase("Status"))
    						{
		    					Reporter.log("PASS >> Column Name is correct",true);
    						}else {
    							Reporter.log("PASS >> Column Name isn't correct",true);	
    						}
		    			}
		    			if(i==4)
		    			{
		    				if (actual.equalsIgnoreCase("Battery"))
    						{
		    					Reporter.log("PASS >> Column Name is correct",true);
    						}else {
    							Reporter.log("PASS >> Column Name isn't correct",true);	
    						}
		    			}
		    			if(i==5)
		    			{
		    				if (actual.equalsIgnoreCase("Last Connected"))
    						{
		    					Reporter.log("PASS >> Column Name is correct",true);
    						}else {
    							Reporter.log("PASS >> Column Name isn't correct",true);	
    						}
		    			}
		    			if(i==6)
		    			{
		    				if (actual.equalsIgnoreCase("Last Device Time"))
    						{
		    					Reporter.log("PASS >> Column Name is correct",true);
    						}else {
    							Reporter.log("PASS >> Column Name isn't correct",true);	
    						}
		    			}
		        	}
		        	
		        }
		     @FindBy(xpath="//span[@id='deviceOwnerName']/../a")
		     private WebElement DeviceNameEdi;
		     
		     @FindBy(xpath="//input[@id='device_name_input']")
		     private WebElement EnterDeviceName;
		     
		     public void EnterDeviceName(String name) throws InterruptedException
		     {
		    	 EnterDeviceName.clear();
		    	 sleep(2);
		    	 EnterDeviceName.sendKeys(name);
		     }
		     
		     /*public void clickOnSaveBtnInEditDeviceName()throws InterruptedException
		     {
		    	 SaveBtnInEditDevName.click();
		    	 sleep(2);
		    	 waitForXpathPresent("//span[text()='Device name change Initiated.']");
		    	 Initialization.driver.findElement(By.xpath("//span[text()='Device name change Initiated.']")).isDisplayed();
		    	 sleep(2);
		     }*/
		     @FindBy(xpath="//span[text()='Device name changed successfully']")
		     private WebElement DeviceNameChanged;
		     public void verifyOfDeviceNameChangedMsg()
		     {
		    	String actual = DeviceNameChanged.getText();
		 		String expected = "Device name changed successfully";
		 		String PassStatement = "PASS>> 'Device name changed successfully' "+expected+" displayed successfully";
		 		String FailStatement = "FAIL >>'Device name changed successfully' wrongly displayed" ;
		 		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
		     }
		    String DeviceDataUsageBeforeSorting=null;
		 	public void ReadingDataUsageNumFromGridBeforeSorting()
		 	{
		 		DeviceDataUsageBeforeSorting=Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr//td[@class='Data Usage']/p")).getText();
		 		System.out.println(DeviceDataUsageBeforeSorting);
		 	}
		 	String DeviceDataUsageAfterSorting=null;
		 	public void ReadingDataUsageNumFromGridafterSorting()
		 	{
		 		DeviceDataUsageAfterSorting=Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr//td[@class='Data Usage']/p")).getText();
		 		System.out.println(DeviceDataUsageAfterSorting);
		 	}
		 	public void clickOnColumns(String column)throws InterruptedException
		 	{
		 		sleep(4);
		 		
		 		Initialization.driver.findElement(By.xpath("(//div[text()='"+column+"'])[1]")).click();
		 		sleep(6);
		 	}
		    public void RightclickOnFirstDevice() throws InterruptedException
		    {
		    	action.contextClick(Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr"))).perform();
		    	sleep(2);
		    }
		    public void selectMultipleDeviceInGrid() throws InterruptedException
		    {
		    	sleep(10);
		    	WebElement dev1 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[1]/td[2]"));
		 		WebElement dev2 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[2]/td[2]"));
		 		WebElement dev3 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[3]/td[2]"));
		 		WebElement dev4 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[4]/td[2]"));
		 		WebElement dev5 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[5]/td[2]"));
		 		WebElement dev6 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[6]/td[2]"));
		 		WebElement dev7 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[7]/td[2]"));
		 		WebElement dev8 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[8]/td[2]"));
		 		WebElement dev9 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[9]/td[2]"));
		 		WebElement dev10 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[10]/td[2]"));
		 		action.keyDown(Keys.CONTROL).click(dev1).click(dev2).click(dev3).click(dev4).click(dev5).click(dev6).click(dev7).click(dev8).click(dev9).click(dev10).keyUp(Keys.CONTROL).build().perform();
		 		sleep(2);
		    }
		    public void RightClickOndevice(String name) throws InterruptedException {
				action.contextClick(Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[10]/td[2]"))).perform();
				sleep(2);
			}
		    String Grp_Path=null;
		    public void ReadingGroupPathofDeviceGrp()
		    {
		    	Grp_Path=Initialization.driver.findElement(By.xpath("(//td[@class='DeviceGroupPath']/p)[1]")).getText();
		    	System.out.println(Grp_Path);
		    }
		    public void VerifyOfGroupPathofDeviceSubGrp() throws InterruptedException
		    {
		    	String Grp_Path1=Initialization.driver.findElement(By.xpath("(//td[@class='DeviceGroupPath']/p)[1]")).getText();
		    	System.out.println(Grp_Path1);
		    	sleep(2);
		    	try {
		    		if(Grp_Path!=Grp_Path1)
		    		{
		    			Reporter.log("Group path is changed from one group to another group",true);
		    		}
		    	}catch (Exception e) {
					ALib.AssertFailMethod("Group path isn't changed from one group to another group");
				}
		    }
		    public void InstallAppWithLowerversion() throws Exception{
				Runtime.getRuntime().exec("adb install ./Uploads/apk/surelockv9.61.apk");
				sleep(30);
			}
		    
		    public void InstallAppWithHigherrversion() throws Exception{
				Runtime.getRuntime().exec("adb install ./Uploads/apk/surelockv9.61.apk");
				sleep(30);
			}
		   
		    public void verifyOfFreeStorageMemoryCol_EMMWindowDevice()
			{
		    	//System.out.println(Initialization.driver.findElement(By.xpath("//td[@class='MemoryStorageAvailable']/p")).getText());
		    	String val=Initialization.driver.findElement(By.xpath("//td[@class='MemoryStorageAvailable']/p")).getText();
			try {
				if(val.equals("N/A"))
				{
					Reporter.log("N/A is displaying as expected",true);
				}
			}catch (Exception e) {
				ALib.AssertFailMethod("N/A is not displaying as expected");
			}
			}
		    public void DisablingAllInstalledApplicationColumns() throws InterruptedException
			{
				ClickOnColumnsButton();
				ClickOnInstalledApplicationVersionList();

				List<WebElement> CheckBoxes = Initialization.driver.findElements(By.xpath("//div[@id='CustomerAppList']/ul/li/label/span/input"));
				for(int i=0;i<CheckBoxes.size();i++)
				{

					boolean CheckBoxStatus = CheckBoxes.get(i).isSelected();

					if(CheckBoxStatus)
					{

						CheckBoxes.get(i).click();
						sleep(3);
					}
				}
				ClickOnColumnsButton();
			}
		    public void InstallAppWithLowerversion_NotePad() throws Exception{
				Runtime.getRuntime().exec("adb install ./Uploads/apk/com.kamikazelabs.gemezone.apk");
				Reporter.log("Installing app in device",true);
				sleep(30);
			}
		    public void InstallAppWithHigherrversion_NotePad() throws Exception{
				Runtime.getRuntime().exec("adb install ./Uploads/apk/GameZone_v2.9_apkpure.com.apk");
				sleep(30);
			}

		 // Madhu
	    	@FindBy(xpath="//span[@id='deviceOwnerName']/following-sibling::a")
	    	private WebElement DeviceNameEdit;

	    	@FindBy(xpath="//div[@class='editable-buttons']/button[text()='Save']")
	    	private WebElement SaveBtnInEditDevName;

	    	public void ClickOnEditDevName()throws InterruptedException
	    	{
	    		DeviceNameEdit.click();
	    		sleep(2);
	    	}

	    	public void clickOnSaveBtnInEditDeviceName()throws InterruptedException
	    	{
	    		SaveBtnInEditDevName.click();
	    		sleep(2);
	    		waitForXpathPresent("//span[text()='Device name change Initiated.']");
	    		Initialization.driver.findElement(By.xpath("//span[text()='Device name change Initiated.']")).isDisplayed();
	    		waitForXpathPresent("//span[text()='Device name changed successfully']");
	    	}

	    	@FindBy(xpath="//*[@id=\"customColumn_modal\"]/div/div/div[2]/div[2]/div[1]/div[2]/div[4]/div[1]/span[2]/span/button")
	    	private WebElement paginationInCustomColumn;

	    	public void clickOnPaginationOfCustomColumn() {
	    		paginationInCustomColumn.click();
	    	}
	    	public void verifyCustomColumnAddButton() throws InterruptedException
	    	{
	    		CustomColumnConfigButton.isDisplayed();
	    		Reporter.log("PASS>> Add button is displayed in custom column of device grid.",true);       
	    	}

	    	// Custom column method 
	    	@FindBy(xpath="//*[@id=\"CustomColList\"]/ul/li[1]")
	    	private WebElement customColDefMessage;

	    	public void customColumnDefaultMessage() {
	    		try
	    		{
	    			String defMsg = Initialization.driver.findElement(By.xpath("//*[@id='CustomColList']/ul/li")).getText();
	    			System.out.println("\n"+defMsg+"\n");
	    			Reporter.log("PASS>>> Configure message is displayed successfully.",true);
	    		}

	    		catch(Exception e)
	    		{
	    			ALib.AssertFailMethod("FAIL>>> Configure message is not displayed.");
	    		}
	    	}

	    	public void verifyPagination(String SelectPagination ) throws InterruptedException {
	    		try {
	    			if(Pagination.isDisplayed()) {
	    				Pagination.click();
	    				sleep(5);
	    				Initialization.driver.findElement(By.xpath("(//a[text()='"+SelectPagination+"'])[1]")).click();
	    				sleep(5);
	    			}
	    		} catch(Exception e) {
	    			ALib.AssertFailMethod("Pagination is not found");
	    		}
	    	}

	    	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[2]/div[4]/span")
	    	private WebElement deviceCount;
	    	String actual=null;
	    	public void readDeviceCount() {
	    		actual = deviceCount.getText();
	    		System.out.println(actual);
	    	}

	    	public void verifyDeviceCountAfterPagination()
	    	{
	    		System.out.println(deviceCount.getText());
	    		try {
	    			if(actual.contains(deviceCount.getText())) {
	    				Reporter.log(deviceCount.getText()+" "+"Pagination is same as before");
	    			}
	    		} catch (Exception e) {
	    			ALib.AssertFailMethod("Pagination is not same as before");
	    		}	
	    	}
	    	public void VerifyColumnIsNotPresentOnGridAndScroll(String ColumnName) throws InterruptedException
	    	{  
	    		try {
	    			sleep(2);
	    			WebElement scroll = Initialization.driver.findElement(By.xpath("//div[contains(text(),'"+ColumnName+"')]"));
	    			js.executeScript("arguments[0].scrollIntoView()", scroll);
	    			boolean ColumnNameInGrid = Initialization.driver.findElement(By.xpath("//div[contains(text(),'"+ColumnName+"')]")).isDisplayed();

	    			Assert.assertTrue(ColumnNameInGrid);
	    		}catch(Exception e) {
	    			Reporter.log("Column is not Present in device grid");
	    		}
	    	}
	    	
//	    	public void EnterValueInAdvanceSearchColumnOfInstallAppColumn(String ColumnName,String ValueTobeSearched) throws InterruptedException
//	    	{  
//	    		sleep(4);
//	    		WebElement scroll = Initialization.driver.findElement(By.xpath("//*[@id=\"tableContainer\"]/div[4]/div[2]/div[1]/table/tbody/tr/td[41]/div/input"));
//	    		js.executeScript("arguments[0].scrollIntoView()", scroll);
//	    		Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).clear();
//	    		Initialization.driver.findElement(By.xpath("//*[@class='"+ColumnName+"']/div/input")).sendKeys(ValueTobeSearched);
//	    		sleep(4);
//	    		waitForidPresent("deleteDeviceBtn");
//	    	}
	    	
	    	/*public void DisablingAllInstalledApplicationColumns() throws InterruptedException
	    	{
	    		ClickOnColumnsButton();
	    		ClickOnInstalledApplicationVersionList();

	    		List<WebElement> CheckBoxes = Initialization.driver.findElements(By.xpath("//div[@id='CustomerAppList']/ul/li/label/span/input"));
	    		for(int i=0;i<CheckBoxes.size();i++)
	    		{

	    			boolean CheckBoxStatus = CheckBoxes.get(i).isSelected();

	    			if(CheckBoxStatus)
	    			{

	    				CheckBoxes.get(i).click();
	    				sleep(3);
	    			}
	    		}
	    		ClickOnColumnsButton();
	    	}
*/	    	@FindBy(xpath="//span[text()='There are no devices that meet the filter criteria you have selected.']")
	    	private WebElement ErrMsgForInstallApp;
	    	
	    	public void verifyErrMsgInInstallAppCol() throws InterruptedException {
	    		//sleep(3);
	    		String actual = ErrMsgForInstallApp.getText();
	    		String expected = "There are no devices that meet the filter criteria you have selected.";
	    		String PassStatement = "PASS>> 'Error message' "+expected+" displayed successfully";
	    		String FailStatement = "FAIL >>'Error message for install application is' wrongly displayed" ;
	    		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	    	}
	    	
	    	@FindBy(xpath="//*[@id='SwipetoUnlock']")
	    	private WebElement swipeToUnlock;
	    	public void verifySwipeToUnlockInAndroidRemoteSupport() throws InterruptedException {
	    		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
	    		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='SwipetoUnlock']")));
	    		boolean isDeviceDisplayed = swipeToUnlock.isDisplayed();
	    		String pass = "PASS >> Swipe to Unlock is displayed";
	    		String fail = "FAIL >> Swipe to Unlock is NOT displayed";
	    		ALib.AssertTrueMethod(isDeviceDisplayed, pass, fail);
	    		sleep(5);
	    	}
	    	public void clickOnSwipeToUnlock() throws InterruptedException {
	    		sleep(2);
	    		swipeToUnlock.click();
	    		sleep(2);
	    	}
	    	public boolean verifyDeviceUnlock() {
	    		return Initialization.driverAppium.isDeviceLocked();
	    	}
	    	public void verifyLockAndUnlockStatusOfDevice() throws InterruptedException {
	    		if(verifyDeviceUnlock()) {
	    			Reporter.log("Device is locked.",true);
	    		} else {
	    			sleep(3);
	    			Initialization.driverAppium.findElementById("com.nix:id/button2").isDisplayed();
	    			Reporter.log("Device is unlocked",true);
	    		}
	    	}
	    	@FindBy(xpath = "//*[@id='networkinfo-card']/div[3]/p")
	    	private WebElement MacAddressMacOS;

	    	@FindBy(xpath = "//*[@id='networkinfo-card']/div[5]/p")
	    	private WebElement IPAddessMacOS;

	    	@FindBy(xpath = "//*[@id='networkinfo-card']/div[7]")
	    	private WebElement ConnectionMacOS;

	    	@FindBy(xpath = "//*[@id='networkinfo-card']/div[9]/p")
	    	private WebElement SerialNumberMacOS;

	    	public void verifyMacAddressOfMacOS(String MacOSMACaddress) throws InterruptedException { 

	    		String ActualResult=MacAddressMacOS.getText();
	    		Reporter.log(ActualResult,true);
	    		String ExpectedResult=MacOSMACaddress ;
	    		String Pass="PASS>> MAC Address for MacOS "+ExpectedResult+" is Displayed";
	    		String Fail="FAIL>> MAC Address for MacOS "+ExpectedResult+" is not Displayed";
	    		ALib.AssertEqualsMethod(ExpectedResult, ActualResult, Pass, Fail);
	    		sleep(2);
	    	}
	    	public void verifyIPAddressOfMacOS(String MacOSIPAddress) throws InterruptedException {

	    		String ActualResult=IPAddessMacOS.getText();
	    		Reporter.log(ActualResult,true);
	    		String ExpectedResult=MacOSIPAddress ;
	    		String Pass="PASS>> IPAddress for MacOS "+ExpectedResult+" is Displayed";
	    		String Fail="FAIL>> IPAddress for MacOS "+ExpectedResult+" is not Displayed";
	    		ALib.AssertEqualsMethod(ExpectedResult, ActualResult, Pass, Fail);
	    		sleep(2);
	    	}

	    	public void verifyConnectionOfMacOS(String MacOSConnection) throws InterruptedException {

	    		String ActualResult=ConnectionMacOS.getText();
	    		Reporter.log(ActualResult,true);
	    		String ExpectedResult=MacOSConnection ;
	    		String Pass="PASS>> Connection for MacOS "+ExpectedResult+" is Displayed";
	    		String Fail="FAIL>> Connection for MacOS "+ExpectedResult+" is not Displayed";
	    		ALib.AssertEqualsMethod(ExpectedResult, ActualResult, Pass, Fail);
	    		sleep(2);
	    	}
	    	public void verifySerialNumOfMacOS(String SerialNum) throws InterruptedException {

	    		String ActualResult=SerialNumberMacOS.getText();
	    		Reporter.log(ActualResult,true);
	    		String ExpectedResult=SerialNum ;
	    		String Pass="PASS>> Serial Number for MacOS "+ExpectedResult+" is Displayed";
	    		String Fail="FAIL>> Serial Number for MacOS "+ExpectedResult+" is not Displayed";
	    		ALib.AssertEqualsMethod(ExpectedResult, ActualResult, Pass, Fail);
	    		sleep(2);
	    	}
	    	public String[] AndroidOptions={"SureLock","SureFox","SureVideo"};
	        public void readingOptionsOnRightClick(String[] androidOptions)
	    	{
	    		List<WebElement> Options = Initialization.driver.findElements(By.xpath("//ul[@id='gridMenu']/li"));
	    		
	    		for(int i=0;i<Options.size();i++)
	    		{
	    			for(int j=0;j<AndroidOptions.length;j++) {
	    				if(Options.get(i).getText().equals((androidOptions)[j]))
		    		    {
		    		    	ALib.AssertFailMethod(androidOptions[j] + " sure products are displayed by right clicking on single iOS device.");
		    		    }
	    			}
	    		}
	    	}
	        public void SelectingApplicationIninstalledAppVersionList() throws InterruptedException
	    	{
	    		boolean status=Initialization.driver.findElement(By.xpath("//span[contains(text(),'( com.android.chrome  )')] /parent::label/span/input")).isSelected();
	    		if(status==false)
	    		{
	    			Initialization.driver.findElement(By.xpath("//span[contains(text(),'( com.android.chrome  )')] /parent::label/span/input")).click();
	    			Reporter.log("Installed Application Version Column Is Selected",true);
	    			sleep(3);
	    			ClickOnColumnsButton();
	    			waitForidPresent("deleteDeviceBtn");
	    			sleep(5);	    	
	    		}
	    		else
	    		{
	    			Reporter.log("Installed Application Version Column Is Already Selected",true);
	    		}
	    	}
//Harpinder
	      //Notes in device info panel
		    @FindBy(xpath="//textarea[@class='form-control input-large']")
			private WebElement AddNotes;
			@FindBy(xpath="//button[@type='submit']")
			private WebElement NoteSaveButton;
			@FindBy(xpath="//span[text()='Device notes have been added for the device named \"sanity1123\".']")
			private WebElement NotesAddedNotification;
			@FindBy(xpath="//span[@id='deviceNotes']")
			private WebElement deviceNotes;
			
			public void clickOnNotesEdit() throws InterruptedException
			{
				waitForElementPresent("//span[@id='deviceNotes']/../a");
				NotesEdit.click();
				Reporter.log("Clicked On Notes Edit",true);
				
			}
			public void clickOnNoteSaveButton() throws InterruptedException
			{
				waitForElementPresent("//button[@type='submit']");
				NoteSaveButton.click();
				Reporter.log("Clicked On Note Save Button",true);
				
			}
			public void clearAddNotesField() throws InterruptedException 
			{
				waitForElementPresent("//textarea[@class='form-control input-large']");
				AddNotes.clear();
				Reporter.log("Text Field is cleared",true);
			}
			public void enterNotefordevice(String Enternotes) throws InterruptedException 
			{
				AddNotes.sendKeys(Enternotes);
				Reporter.log("Note Entered for device",true);
				sleep(2);
			}
			
			public void verifyNotesAdded(String Enternotes) 
			{  
				String ActualResult = deviceNotes.getText();
			    String ExpectedResult = Enternotes;
			    String PassStatement = "Device Note is added :"+ActualResult;
			    String FailStatement = "Device Note is not added";
			    ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement,FailStatement);
				Reporter.log("Newly added note is verified",true);
			}

	 
			  //Device locale column in device grid
				@FindBy(xpath="//input[@id='grid_column_search']")
				private WebElement GridColumnSearch;
				@FindBy(xpath="//button[@class='btn btn-default dropdown-toggle']//span[@class='caret']")
				private WebElement GridColumnDropdown;
				@FindBy(xpath="//input[@data-field='Locale']")
				private WebElement DeviceLocaleGridColumnStatus;
				@FindBy(xpath="//div[@id='tableContainer']//div[@class='fixed-table-body']")
				private WebElement DeviceGridBody;
				@FindBy(xpath="//td[@class='Locale']")
				private WebElement DeviceLocaleGridColumnValue;
				@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
				private WebElement SearchBox;
				
				public void clearSearchBox() throws InterruptedException
				{
					SearchBox.clear();
					SearchBox.click();
					SearchBox.sendKeys(Keys.ENTER);
					Reporter.log("SearchBox is cleared",true);
					sleep(2);
				}
				public void clickOnDeviceGridBody() throws InterruptedException
				{
					DeviceGridBody.click();
					Reporter.log("Clicked On Device Grid Body",true);
					sleep(2);
				}
				public void clickOnGridColumnDropdown() throws InterruptedException
				{
					waitForElementPresent("//button[@class='btn btn-default dropdown-toggle']//span[@class='caret']");
					GridColumnDropdown.click();
					Reporter.log("Clicked On Grid Column Dropdown",true);
					
				}
				public void clickOnGridColumnSearch() throws InterruptedException
				{
					waitForElementPresent("//input[@id='grid_column_search']");
					GridColumnSearch.clear();
					GridColumnSearch.click();
					Reporter.log("Clicked On Grid Column Search",true);
					
				}
				public void enterColumnName(String ColumnName) throws InterruptedException 
				{
					GridColumnSearch.sendKeys(ColumnName);
					Reporter.log("Enter column name",true);
					sleep(2);
				}
				
		  public void compareResult() throws InterruptedException
		  {
			  clickOnDeviceGridBody();
		      String ActualResult = DeviceLocaleGridColumnValue.getText();
		      String ExpectedResult =Config.DeviceLocaleColumnValue;
		      String PassStatement ="Actual value of column :"+ActualResult; 
		      String FailStatement ="Value of column is not correct";
		      ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement, FailStatement);
		  Reporter.log("Device grid value compared with expected value" ,true);
		  }
		  
		  public void verifyGridColumnIsEnabledAndGetFinalValue() throws InterruptedException 
		  {
			  waitForElementPresent("//input[@data-field='Locale']");
			  if(DeviceLocaleGridColumnStatus.isSelected())
	  		{
	        		compareResult();
	  		}
	  		else
	  		{	
	  			DeviceLocaleGridColumnStatus.click();
	    			compareResult();
	  		}
			  Reporter.log("verified Grid Column Value" ,true);
		    }

			  //Network Panel For iOS Device
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 vlu macAddr_valLine']")
			  private WebElement MacAddressofiOS;
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 vlu'][1]")
			  private WebElement iPAddressofiOS;
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 vlu'][2]")
			  private WebElement ConnectionofiOS;
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 vlu'][3]")
			  private WebElement IMEIofiOS;
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 vlu'][4]")
			  private WebElement PhoneNumberofiOS;
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 vlu'][5]")
			  private WebElement SerialNumberofiOS;
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 vlu'][6]")
			  private WebElement SimSerialNumberofiOS;
			  
			  String iOSNetworkPanelElements;
			  public void printValue(String s1,WebElement NetworkPanel) throws InterruptedException
			  {
				  String s=NetworkPanel.getText(); 
				  sleep(2);
				  System.out.println(s1+s);
			  }
			  public void iOSNetworkPanelElements() throws InterruptedException
			  {
				  WebElement[] NetworkPanelElementsForiOS = {MacAddressofiOS,iPAddressofiOS,ConnectionofiOS,IMEIofiOS,PhoneNumberofiOS,SerialNumberofiOS,SimSerialNumberofiOS};
					 
		             for(int i=0;i<NetworkPanelElementsForiOS.length;i++)
				        {
		            	 if( NetworkPanelElementsForiOS[i].isDisplayed())
		            	 {
		            		 iOSNetworkPanelElements=NetworkPanelElementsForiOS[i].getText();
				          }
		            	 else
		            	 {
		            		 ALib.AssertFailMethod("Elements are not displayed");
		            	 }
		            	 System.out.println(iOSNetworkPanelElements);
		   		        }
		             Reporter.log("Value checked of all elements present in Network Panel for iOS device" ,true);
		             sleep(2);
			  }
			 
			  //Installed Application Version column in Device Grid
			  @FindBy(xpath="//span[normalize-space()='Installed Application Version']")
			  private WebElement InstalledApplicationVersion;
			  @FindBy(xpath="//input[@id='custom_applist_search']")
			  private WebElement CustomAppListSearchBox;
			  @FindBy(xpath="//span[contains(text(),'9Apps ( com.mobile.indiapp')]")
			  private WebElement InstalledAppindevice;
			  @FindBy(xpath="//td[@class='Application1']")
			  private WebElement InstalledApColumn;
			  @FindBy(xpath="//input[@data-field='com.mobile.indiapp']")
			  private WebElement InstalledApCheckBox;

			
			  public void clickOnInstalledAppCheckBox() throws InterruptedException
				{
				  waitForElementPresent("//span[contains(text(),'9Apps ( com.mobile.indiapp')]");
				  InstalledApCheckBox.click();
				 Reporter.log("Click On Installed App checkbox",true);
					sleep(2);
				}
			  public void clickOnInstalledApplicationVersion() throws InterruptedException
				{
				  waitForElementPresent("//span[normalize-space()='Installed Application Version']");
				  InstalledApplicationVersion.click();
				  Reporter.log("Clicked On Installed Application Version",true);
					sleep(2);
				}
			  public void clickOnCustomAppListSearchBox() throws InterruptedException
				{
				  waitForElementPresent("//input[@id='custom_applist_search']");
				    CustomAppListSearchBox.click();
					Reporter.log("Clicked On Custom App List Search Box",true);
					sleep(2);
				}
			  public void enterAppNameForSearch(String InstalledApp) throws InterruptedException 
				{
				  	CustomAppListSearchBox.clear();
				  	sleep(2);
				    CustomAppListSearchBox.sendKeys(InstalledApp);
					Reporter.log("Enter App Name For Search",true);
					sleep(2);
				}
			  public void clearCustomAppSearchBox() throws InterruptedException 
				{
				    CustomAppListSearchBox.clear();
					Reporter.log("Cleared serach box",true);
					sleep(2);
				}
			  public void compareResult1() throws InterruptedException
			  {
				 //clickOnDeviceGridBody();
				 waitForElementPresent("//td[@class='Application1']");
			     String ActualResult = InstalledApColumn.getText();
			     String ExpectedResult =Config.InstalledAppVersion;
			     String PassStatement ="Actual value of Installed App :"+ActualResult; 
			     String FailStatement ="Value of Installed App is not correct";
			     ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement, FailStatement);
			     Reporter.log("Compare the installed app value with Expected value" ,true);
			  }
			  public void installedAppValue() throws InterruptedException
			  {
				  waitForElementPresent("//span[contains(text(),'9Apps ( com.mobile.indiapp')]");
				   if(InstalledApCheckBox.isSelected())
				    		{			    	
							  clickOnDeviceGridBody();
							  sleep(2);
							  compareResult1();
				    		}			    			
				     else
				     {
				    	 clickOnInstalledAppCheckBox();
		    			clickOnDeviceGridBody();
		    			sleep(2);
		    			compareResult1();
		    		}
			  Reporter.log("verified the installed app value" ,true);
			  }

			  //Verify Elements present in right Click On Device
			  @FindBy(xpath="//tr/td/p[contains (text(),'"+Config.DeviceName+"')]")
			  private WebElement SelectDevice;
			  @FindBy(xpath="//tr/td/p[contains (text(),'"+Config.DeviceWithoutIntegratedNix+"')]")
			  private WebElement SelectDevice2;
			  @FindBy(xpath="//tr/td/p[contains (text(),'"+Config.SearchiOSdevice+"')]")
			  private WebElement SelectiOSDevice;
			  @FindBy(xpath="//tr/td/p[contains (text(),'"+Config.WindowsDeviceName+"')]")
			  private WebElement SelectWindowsDevice;
			  @FindBy(xpath="//tr/td/p[contains (text(),'"+Config.Macos_DeviceName+"')]")
			  private WebElement SelectMacDevice;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Refresh']")
			  private WebElement RightClickRefresh;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Remote']")
			  private WebElement RightClickRemote;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Reboot']")
			  private WebElement RightClickReboot;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Move to Group']")
			  private WebElement RightClickMovetoGroup;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Tags']")
			  private WebElement RightClickTags;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Delete']")
			  private WebElement RightClickDelete;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Push Application']")
			  private WebElement RightClickPushApplication;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Push File']")
			  private WebElement RightClickPushFile;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='SureLock']")
			  private WebElement RightClickSureLock;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='SureFox']")
			  private WebElement RightClickSureFox;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='SureVideo']")
			  private WebElement RightClickSureVideo;
			  @FindBy(xpath="//ul[@id='gridMenu']//span[text()='Configure Nix']")
			  private WebElement RightClickConfigureNix;
			  
			  String RightClickelements ;
			  
			  public void rightClickOnDevice(WebElement RightClickElement) throws InterruptedException
			  {
				  
				  Actions act1=new Actions(Initialization.driver);
				  act1.moveToElement(RightClickElement).contextClick(RightClickElement).perform();
				  Reporter.log("Right click on device" ,true);
				  sleep(2);
			  }
			  
			  public void RightClickElements(WebElement...elements) throws InterruptedException
			    {
				     for(int i=0;i<elements.length;i++)
				        {
		            	 if( elements[i].isDisplayed())
		            	 {
		            		 RightClickelements=elements[i].getText();
				          }
		            	 else
		            	 {
		            		 ALib.AssertFailMethod("Elements are not displayed");
		            	 }
		            	 System.out.println(RightClickelements);
		   		        }
				  Reporter.log("Verified elements present on Right click of device" ,true);
				  sleep(3);
			      }
			  
			  public void VerifyRightClickOptionsAcrossAndroidplatform() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			      {
				    
				     waitForElementPresent("//tr/td/p[contains (text(),'"+Config.DeviceName+"')]");
				     sleep(2);
					  clickOnDeviceGridBody();
				     rightClickOnDevice(SelectDevice);
				     System.out.println("Elements Present on Right click of Android device");
				     RightClickElements(RightClickRefresh,RightClickRemote,RightClickReboot,RightClickMovetoGroup,RightClickTags,RightClickDelete,RightClickPushApplication,RightClickPushFile,RightClickSureLock,RightClickSureFox,RightClickSureVideo);
	            	 sleep(3);
	                }
			  
			  public void VerifyRightClickForTheiOSPlatformDeviceWhichEnrolledNewlyOREnrolledRecently() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			     {		  
				  waitForElementPresent("//tr/td/p[contains (text(),'"+Config.SearchiOSdevice+"')]");
				  sleep(2);
				  clickOnDeviceGridBody();
				  rightClickOnDevice(SelectiOSDevice);
				  System.out.println("Elements Present on Right click of iOS device");
				  RightClickElements(RightClickRefresh,RightClickRemote,RightClickMovetoGroup,RightClickTags,RightClickDelete,RightClickConfigureNix);
	              sleep(3);
	                }
			  
			  public void VerifyRightClickForTheWindowsPlatformDeviceWhichEnrolledNewlyOREnrolledRecently() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			     {			 			  
				  waitForElementPresent("//tr/td/p[contains (text(),'"+Config.WindowsDeviceName+"')]");
				  sleep(2);
				  clickOnDeviceGridBody();
				  rightClickOnDevice(SelectWindowsDevice);
				  System.out.println("Elements Present on Right click of Windows device");
				  RightClickElements(RightClickRefresh,RightClickRemote,RightClickReboot,RightClickMovetoGroup,RightClickTags,RightClickDelete);
	              sleep(3);
	                }
			  
			  public void VerifyRightClickForTheMacOSPlatformDeviceWhichEnrolledNewlyOREnrolledRecently() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			    {
				  waitForElementPresent("//tr/td/p[contains (text(),'"+Config.Macos_DeviceName+"')]");
				  sleep(2);
				  clickOnDeviceGridBody();
				  rightClickOnDevice(SelectMacDevice);
				  System.out.println("Elements Present on Right click of macOS device");
				  RightClickElements(RightClickRefresh,RightClickRemote,RightClickMovetoGroup,RightClickTags,RightClickDelete);
			  	   sleep(3);
	                }
			  
			  //Verify Launch and Exit SureLock Option in right click Menu
			  @FindBy(xpath="//i[@class='surelock_home fa fa-shield tracking-color-red shieldcustomIcn shield4gray']")
			  private WebElement Surelockshieldgray;
			  @FindBy(xpath="//span[text()='Launch SureLock']")
			  private WebElement LaunchSureLock;
			  @FindBy(xpath="//div[@id='deviceInfo']//span[text()='Refresh']")
			  private WebElement DeviceRefresh;
			  @FindBy(xpath="//i[@class='surelock_home fa fa-shield tracking-color-green shieldcustomIcn shield4green']")
			  private WebElement Surelockshieldgreen;
			  @FindBy(xpath="//span[text()='Exit SureLock']")
			  private WebElement ExitSureLock;
			  @FindBy(xpath="//input[@id='activation_code_pswd']")
			  private WebElement SureLockpswd;
			  @FindBy(xpath="//button[@id='activationcode_okbtn']")
			  private WebElement SureLockpswdButton;
			  @FindBy(xpath="//tr[@data-index='1']//p[contains(text(),'Integrated SureLock Launch')]")
			  private WebElement SureLockLaunchJobDeployed;
			  @FindBy(xpath="//div[@id='jobQueueButton']//i[@class='icn fa fa-long-arrow-right']")
			  private WebElement jobQueue;
			  @FindBy(xpath="//li[@id='jobQueueHistoryTab']//a[text()='Completed Jobs']")
			  private WebElement CompletedJobsStatus;
			  @FindBy(xpath="//div[@id='device_jobQ_modal']//button[@class='close']")
			  private WebElement cancleJobStatusscreen;
		
			  public void clickOnLaunchSureLock() throws InterruptedException
				{
				  waitForElementPresent("//span[text()='Launch SureLock']");
				  LaunchSureLock.click();
				 Reporter.log("Click On Launch SureLock",true);
				}
			  public void clickOnExitSureLock() throws InterruptedException
				{
				  waitForElementPresent("//span[text()='Exit SureLock']");
				  ExitSureLock.click();
				  Reporter.log("Click On Exit SureLock",true);
					
				}
			  
			  public void enterSureLockPasswd() throws InterruptedException 
				{
				  SureLockpswd.sendKeys(Config.SurelockPassword);
				  Reporter.log("Enter surelock password" ,true);
					sleep(2);
				}
			  public void clickOnSurelock() throws InterruptedException
			  {
				  waitForElementPresent("//ul[@id='gridMenu']//span[text()='SureLock']");
				  RightClickSureLock.click();
				  Reporter.log("Click on surelock option" ,true);
			  }
			  public void clickOnSureLockpswdButton() throws InterruptedException
			  {
				  waitForElementPresent("//button[@id='activationcode_okbtn']");
				  SureLockpswdButton.click();
				  Reporter.log("Click on surelock password button" ,true);
					sleep(2);
			  }
			  
			  public void clickonLaunchSurelock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
				  
				  clickOnLaunchSureLock();
				  
			  }
			 
			  public void verifySurelockShieldcolor() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
				  waitForElementPresent("//i[@class='surelock_home fa fa-shield tracking-color-green shieldcustomIcn shield4green']");
				  if(Surelockshieldgreen.isDisplayed())
				  {
					  System.out.println("Surelock is launched");  
				  }
				  else if(Surelockshieldgray.isDisplayed())
				  {
					  System.out.println("Surelock is in Exit state");  
				  }
				  
			  }
			  public void refreshDevice() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='deviceInfo']//span[text()='Refresh']");
				  DeviceRefresh.click();
				  Reporter.log("Device Refreshed" ,true);
			  }
			  public void clickJobQueue() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='jobQueueButton']//i[@class='icn fa fa-long-arrow-right']");
				  jobQueue.click();
				  Reporter.log("click on job queue" ,true);
			  }
			  public void clickCompletedJobsStatus() throws InterruptedException
			  {
				  waitForElementPresent("//li[@id='jobQueueHistoryTab']//a[text()='Completed Jobs']");
				  CompletedJobsStatus.click();
				  Reporter.log("click on Completed Jobs Status and verified job is deployed" ,true);
			  }
			  public void clickCancleJobStatusscreen() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='device_jobQ_modal']//button[@class='close']");
				  cancleJobStatusscreen.click();
				  Reporter.log("click on Cancle Job Status screen" ,true);
			  }
			  
			  public void LaunchSurelock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
				  
				  rightClickOnDevice(SelectDevice2);
				  sleep(2);
				  clickOnSurelock();
				  sleep(2);
				  clickonLaunchSurelock();
				  sleep(2);
				  		 			  
			  }
			  public void LaunchStanalonSurelock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
				  
				  rightClickOnDevice(SelectDevice);
				  sleep(2);
				  clickOnSurelock();
				  sleep(2);
				  clickonLaunchSurelock();
				  sleep(2);
				  		 			  
			  }
			  public void verifySurelockLaunchedJob() throws InterruptedException
			  {
				  clickJobQueue();
				  clickCompletedJobsStatus();
				  waitForElementPresent("//tr[@data-index='0']//p[contains(text(),'Integrated SureLock Launch')]/../../child::td[6]//p[contains(text(),'Deployed')]");
				  clickCancleJobStatusscreen();
				  sleep(2);
	  
			  }
			  public void waitForGreenShieldAndVerifySurelockIslaunched() throws InterruptedException
			  {
				  waitForElementPresent("//i[@class='surelock_home fa fa-shield tracking-color-green shieldcustomIcn shield4green']");
				  if(Surelockshieldgreen.isDisplayed())
				  {
				  Reporter.log("Surelock is launched" ,true);
				  }
				  else
					{
						ALib.AssertFailMethod("Surelock not Launched");
					}
				  sleep(2); 
			  }
			  public void ExitSurelock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {   
				  rightClickOnDevice(SelectDevice2);
				  clickOnSurelock();
				  clickOnExitSureLock();
				  enterSureLockPasswd();
				  clickOnSureLockpswdButton();
				 			 
			  }
			  public void ExitStandalonSurelock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {   
				  rightClickOnDevice(SelectDevice);
				  clickOnSurelock();
				  clickOnExitSureLock();
				  enterSureLockPasswd();
				  clickOnSureLockpswdButton();
				 			 
			  }
			  public void verifySurelockExitJob() throws InterruptedException
			  {
				  clickJobQueue();
				  clickCompletedJobsStatus();
				  waitForElementPresent("//tr[@data-index='0']//p[contains(text(),'SureLock Exit')]/../../child::td[6]//p[contains(text(),'Deployed')]");
				  clickCancleJobStatusscreen();
				  sleep(2);
	  
			  }
			  public void waitForGrayShieldAndVerifySurelockIsExited() throws InterruptedException
			  {
				  waitForElementPresent("//i[@class='surelock_home fa fa-shield tracking-color-red shieldcustomIcn shield4gray']");
				  if(Surelockshieldgray.isDisplayed())
				  {
					  Reporter.log("Surelock is Exited" ,true);
				  }
				 
				  else
					{
						ALib.AssertFailMethod("Surelock not Exited");
					}
				  sleep(4);
			  }
			  //Verify colour of shield icon when UEM Nix is installed, but Integrated SureLock is in exit state (disabled).
			  public void verifyGrayColorShield() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
			  try {			     
				  
				 if(Surelockshieldgreen.isDisplayed())
				 { 
					 ExitSurelock();		 
					 verifySurelockExitJob();
					 refreshDevice();
					 waitForGrayShieldAndVerifySurelockIsExited();
					 Reporter.log("Verified Shield color is gray and Surelock is in Exit state" ,true);
					 
				 }
				
				 }
			  catch(Exception e) 
			       {
				  Reporter.log("Verified Shield color is gray and Surelock is in Exit state" ,true);
				     
					 }

				}
	       //Verify colour of shield icon when UEM Nix is installed and Integrated SureLock is in foreground.
			  
			  public void verifyGreenColorShield() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
			  try {			     
				  
				 if(Surelockshieldgray.isDisplayed())
				 { 
					 LaunchSurelock();		 
					 verifySurelockLaunchedJob();
					 refreshDevice();
					 waitForGreenShieldAndVerifySurelockIslaunched();
					 Reporter.log("Verified Shield color is green and SureLock is in foreground" ,true);
					 
				 }			 
				 		
				 }
			  catch(Exception e) 
			       {
				  Reporter.log("Verified Shield color is green and SureLock is in foreground" ,true);
				     
					 }

				}
	        //Verify colour of shield icon when Suremdm nix,surelock is installed and surelock is running in device
			  
			  @FindBy(xpath="//i[@class='surelock_home fa fa-shield tracking-color-green']")
			  private WebElement StandalonSurelockShieldColorGreen;

			  public void verifyGreenShieldForStandalonSurelock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
			  try {			     
				  
				 if(StandalonSurelockShieldColorRed.isDisplayed())
				 { 
					 LaunchStanalonSurelock();		 
					 verifySurelockLaunchedJob();
					 refreshDevice();
					 waitForGreenShieldAndVerifyStandAlonSurelockIslaunched();
					 Reporter.log("Verified Shield color is green" ,true);
					 
				 }
				
				 }
			  catch(Exception e) 
			       {
				  Reporter.log("Verified Shield color is green " ,true);
				     
					 }

				}
			  
			  public void waitForGreenShieldAndVerifyStandAlonSurelockIslaunched() throws InterruptedException
			  {
				  waitForElementPresent("//i[@class='surelock_home fa fa-shield tracking-color-green']");
				  if(StandalonSurelockShieldColorGreen.isDisplayed())
				  {
					  Reporter.log("Verified Standalon Surelock is  launched" ,true);  
				  }
				  else
					{
						ALib.AssertFailMethod("Surelock not Launched");
					}
				  
				  sleep(2); 
			  }
			  
			  //Verify colour of shield icon when Suremdm nix,surelock is installed and surelock is exited in device.
			  
			  @FindBy(xpath="//i[@class='surelock_home fa fa-shield tracking-color-red']")
			  private WebElement StandalonSurelockShieldColorRed;
			  
			  public void verifyRedShieldForStandalonSurelock() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
			  {
			  try {			     
				  
				 if(StandalonSurelockShieldColorGreen.isDisplayed())
				 { 
					 ExitStandalonSurelock();		 
					 verifySurelockExitJob();
					 refreshDevice();
					 waitForRedShieldAndVerifyStandAlonSurelockIsExited();
					 Reporter.log("Verified Shield color is Red and standalon surelock is in exit state" ,true);
					 
				 }
				
				 }
			  catch(Exception e) 
			       {
				  Reporter.log("Verified Shield color is Red and standalon surelock is in exit state" ,true);
				     
					 }

				}
			  public void waitForRedShieldAndVerifyStandAlonSurelockIsExited() throws InterruptedException
			  {
				  waitForElementPresent("//i[@class='surelock_home fa fa-shield tracking-color-red']");
				  if(StandalonSurelockShieldColorRed.isDisplayed())
				  {
				  Reporter.log("Verified Standalon Surelock is Exited" ,true);
				  }
				  else
					{
						ALib.AssertFailMethod("Surelock not Exited");
					}
				  sleep(2); 
			  }
			  //Verify Location Tracking in device info panel
			  @FindBy(xpath="//div[@class='content col-xs-6 col-md-6 dev_info_val locationtrackingdiv']//span[contains(text(),'OFF')]")
			  private WebElement LocationTrackingStatusOff;
			  @FindBy(xpath="//input[@id='isLocationTrackingOn']/../label")
			  private WebElement LocationTrackingToggleButton;
			  @FindBy(xpath="//div[@class='modal-dialog locationTractPOP']//button[@class='btn smdm_btns smdm_grn_clr okbutton addbutton']")
			  private WebElement LocationTrackingButton;
			  @FindBy(xpath="//i[@class='locTrack_icn fa fa-map-marker tracking-color-red']")
			  private WebElement LocationTrackingIconRed;
			  @FindBy(xpath="//i[@class='locTrack_icn fa fa-map-marker tracking-color-green']")
			  private WebElement LocationTrackingIconGreen;
			  @FindBy(xpath="//div[@id='locateBtn']")
			  private WebElement LocationTrackingButtonInQATB;
			  @FindBy(xpath="//div[@id='addGeoTagSection']//div[@class='locateTagsSec']")
			  private WebElement RealTimeLocationTracking;
			  @FindBy(xpath="//p[contains(text(),'Date : 8 Feb 2021')]")
			  private WebElement DateInLocation;
			  @FindBy(xpath="//div[@id='locationTrackingModal']//button[@class='close']")
			  private WebElement PopClose;
			  @FindBy(xpath="(//i[@class='locTrack_icn fa fa-map-marker tracking-color-yellow'])[1]")
			  private WebElement YellowIcon;
			  
			
		/*
		 * @FindBy(xpath="//input[@id='job_text']") private WebElement
		 * TrackingPerodicity;
		 */
			  
			
			  public void clickOnLocationEdit() throws InterruptedException
			  {
				  waitForElementPresent("//a[@id='locationTrackingEditBtn']");
				  EditLocationButton.click();
				  Reporter.log("Click on Location Edit Button in Device info panel" ,true);
				  sleep(2); 
			  }
			  
			  public void verifyTogglebutonStatus() throws InterruptedException
			  {
				  waitForElementPresent("//input[@id='isLocationTrackingOn']/../label");
				  LocationTrackingToggleButton.isEnabled();
				  Reporter.log("Checked location tracking toggle button is on or off" ,true);
				  sleep(2); 
			  }
			  public void clickLocationTogglebuton() throws InterruptedException
			  {
				  waitForElementPresent("//input[@id='isLocationTrackingOn']/../label");
				  LocationTrackingToggleButton.click();
				  Reporter.log("Click on Location Tracking Toggle Button" ,true);
				  sleep(2); 
			  }
//			  public void checkLocationIsAlreadyOn() throws InterruptedException
//			  {
//				  waitForElementPresent("//input[@id='job_text']");
//				  if( TrackingPerodicity.isEnabled())
//					 {
//						 PopClose.click();
//					 }
//				  else
//				  {
//					  clickLocationTogglebuton();
//					  EnterPerodicityForLocationTracking();
//				  }
//				  Reporter.log("Click on Location Tracking Toggle Button" ,true);
//				  sleep(2); 
//			  }
			  public void EnterPerodicityForLocationTracking() throws InterruptedException
			  {
				  waitForElementPresent("//input[@id='job_text']");
							 
				  TrackingPerodicity.clear();
				  TrackingPerodicity.sendKeys("1");		
				  Reporter.log("Enter Perodicity For Location Tracking" ,true);
				  sleep(2); 
			  }
			  public void clickLocationButton() throws InterruptedException
			  {
				  waitForElementPresent("//div[@class='modal-dialog locationTractPOP']//button[@class='btn smdm_btns smdm_grn_clr okbutton addbutton']");
				  LocationTrackingButton.click();	
				  Reporter.log("click on Location Tracking Button" ,true);
				  sleep(2); 
			  }
			  public void verifyLocationTracking() throws InterruptedException
			  {
				  waitForElementPresent("//i[@class='locTrack_icn fa fa-map-marker tracking-color-green']");
				 if( LocationTrackingIconGreen.isDisplayed()||YellowIcon.isDisplayed())
				 {
				  Reporter.log("Location tracking is updating" ,true);
				 }
				  sleep(2); 
			  }
			  public void clickLocationTracking() throws InterruptedException
			  {
				 // waitForElementPresent("//i[@class='locTrack_icn fa fa-map-marker tracking-color-green']");
				  LocationTrackingButtonInQATB.click();
				  Reporter.log("Click on Location Tracking Button In QATB" ,true);
				  sleep(2); 
				  
			  }
			  public void verifyLocationTrackingIn() throws InterruptedException
			  {
				  changeTab();
				  waitForElementPresent("//div[@id='addGeoTagSection']");
				  if(RealTimeLocationTracking.isDisplayed())
				  {
				  Reporter.log("Verified in Location Screen, location is updating" ,true);
				  }
				  else
					{
						ALib.AssertFailMethod("location is not updating");
					}
				  sleep(2); 
			  }
			  public void changeTab()
			  {
				  ArrayList<String> newTab = new ArrayList<String>(Initialization.driver.getWindowHandles());
				    
				  Initialization.driver.switchTo().window(newTab.get(1));
			  }
			  public void changeToPreviousTab()
			  {
				  ArrayList<String> newTab = new ArrayList<String>(Initialization.driver.getWindowHandles());
				    
				  Initialization.driver.switchTo().window(newTab.get(0));
			  }

			//Verify Call log and SMS log tracking in telecom management policy job
			  @FindBy(xpath=".//*[@id='data_usage_policy']/span")
			  private WebElement TelecomManagementPolicyJob;
			  @FindBy(xpath="//input[@id='job_name_input']")
			  private WebElement JobNameTextField;
			  @FindBy(xpath="//a[contains(text(),'Call Log Tracking')]")
			  private WebElement CallLogTracking;
			  @FindBy(xpath="//a[contains(text(),'SMS Log Tracking')]")
			  private WebElement SMSLogTracking;
			  @FindBy(xpath="//div[@id='callLog_tabPanel']//select[@class='form-control ct-model-input ct-select-ele']")
			  private WebElement CallLogTrackingDropDown;
			  @FindBy(xpath="//div[@id='callLog_tabPanel']//input[@id='call_tracking_interval']")
			  private WebElement CallTrackingPeriodicity;
			  @FindBy(xpath="//div[@id='telecom_mgnment_modal']//button[@id='okbtn']")
			  private WebElement TelecomMgnmentOkButton;
			  @FindBy(xpath="//div[@id='network_datausage']//input[@id='MobileEnableDataUsage']")
			  private WebElement DataUsageCheckBox;
			  @FindBy(xpath="//table[@id='jobDataGrid']//tbody//tr//td[2]/p[contains(text(),'CallAndSmsLogTracking')]")
			  private WebElement CreatedTelecomMngmtJob;
			  @FindBy(xpath="//div[@id='SMSLog_tabPanel']//select[@id='SMStrackTrackingOn']")
			  private WebElement SmsLogTrackingDropDown;
			  @FindBy(xpath="//div[@id='SMSLog_tabPanel']//input[@id='sms_tracking_interval']")
			  private WebElement SmsTrackingPeriodicity;
			  @FindBy(xpath="//tr[@data-index='0']//p[contains(text(),'CallAndSmsLogTracking')]/../../child::td[6]//p[contains(text(),'Deployed')]")
			  private WebElement TelecomJob;
			  @FindBy(xpath="//div[@id='dis-card']//a[@id='dataUsageEditBtn']")
			  private WebElement dataUsageEditBtn;
			  @FindBy(xpath="//div[@id='telecom_mgnment_modal']//button[@class='close']")
			  private WebElement CloseTelecom;
			  @FindBy(xpath="//div[@id='callLog_tabPanel']//select//option[@class='ct-selOpt'][2]")
			  private WebElement CallLogON;
			  @FindBy(xpath="//div[@id='SMSLog_tabPanel']//select//option[@class='ct-selOpt'][2]")
			  private WebElement SmsLogON;
			
			
			  public void uncheckDataUsage() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='network_datausage']//input[@id='MobileEnableDataUsage']");
				  DataUsageCheckBox.click();
				  Reporter.log("Uncheck the checkbox of data usage" ,true);
			  }
			
			  public void clickOnTelecomManagementPolicyJob() throws InterruptedException
			  {
				  waitForElementPresent(".//*[@id='data_usage_policy']/span");
				  TelecomManagementPolicyJob.click();
				  Reporter.log("Click on Telecom Management Policy Job" ,true);
			  }
			  public void enterJobName(String JobName) throws InterruptedException
			  {
				  waitForElementPresent("//input[@id='job_name_input']");
				  JobNameTextField.sendKeys(JobName);
				  Reporter.log("Entered job name" ,true);
			  }
			  public void clickCallLogTracking() throws InterruptedException
			  {
				  waitForElementPresent("//a[contains(text(),'Call Log Tracking')]");
				  CallLogTracking.click();
				  Reporter.log("Click on call log tracking" ,true);
			  }
			  public void selectOnOROfForCallTracking(String Value) throws InterruptedException
			  {
				 waitForElementPresent("//div[@id='callLog_tabPanel']//select[@class='form-control ct-model-input ct-select-ele']");
				  Select CallLog = new Select(CallLogTrackingDropDown);
				  CallLog.selectByVisibleText(Value);
				  Reporter.log("Select ON for Call Log Tracking from dropdown" ,true);
			  }
			  
			  public void EnterTrackingPeriodicityForCall(String Minutes) throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='callLog_tabPanel']//input[@id='call_tracking_interval']");
				  CallTrackingPeriodicity.sendKeys(Minutes);
				  Reporter.log("Enter Tracking Periodicity" ,true);
			  }
			  public void clickSmsLogTracking() throws InterruptedException
			  {
				  waitForElementPresent("//a[contains(text(),'SMS Log Tracking')]");
				  SMSLogTracking.click();
				  Reporter.log("Click on Sms Log Tracking " ,true);
			  }
			  public void clickOnButton() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='telecom_mgnment_modal']//button[@id='okbtn']");
				  TelecomMgnmentOkButton.click();
				  Reporter.log("Click on Ok Button " ,true);
			  }
			  public void verifyJobIsCreated() throws InterruptedException
			  {
				  waitForElementPresent("//table[@id='jobDataGrid']//tbody//tr//td[2]/p[contains(text(),'CallAndSmsLogTracking')]");
				  CreatedTelecomMngmtJob.isDisplayed();
				  Reporter.log("Verified job is created " ,true);
			  }
			  public void selectOnOROfForSmsTracking(String Value) throws InterruptedException
			  {
				 waitForElementPresent("//div[@id='SMSLog_tabPanel']//select[@id='SMStrackTrackingOn']");
				  Select CallLog = new Select(SmsLogTrackingDropDown);
				  CallLog.selectByVisibleText(Value);
				  Reporter.log("Select ON for Sms Log Tracking from dropdown" ,true);
			  }
			  public void EnterTrackingPeriodicityForSms(String Minutes) throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='SMSLog_tabPanel']//input[@id='sms_tracking_interval']");
				  SmsTrackingPeriodicity.sendKeys(Minutes);
				  Reporter.log("Enter Tracking Periodicity For sms Logs" ,true);
			  }
			 

		
		        public void verifyTelecommanagementJob() throws InterruptedException
		        {
		  
		             clickJobQueue(); 
		             clickCompletedJobsStatus(); 
		            waitForElementPresent("//tr[@data-index='0']//p[contains(text(),'CallAndSmsLogTracking')]/../../child::td[6]//p[contains(text(),'Deployed')]");
		           TelecomJob.isDisplayed();
		           clickCancleJobStatusscreen();
		           sleep(2);
		  }
		 
			  public void clickdataUsageEditBtn() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='dis-card']//a[@id='dataUsageEditBtn']");
				  dataUsageEditBtn.click();
				  Reporter.log("click on data Usage Edit Button " ,true);
			  }
			  public void jobVerifyCallLogOn()
				{
					String actual = CallLogON.getText();
					String expected = "On"; 
					String PassStatement = "PASS>> Call Log is "+expected+" ";
					String FailStatement = "FAIL >>Call Log Is Not ON" ;
					ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				}
			  public void jobVerifySmsLogOn()
				{
					String actual = SmsLogON.getText();
					String expected = "On"; 
					String PassStatement = "PASS>> Sms Log is "+expected+" ";
					String FailStatement = "FAIL >>Sms Log Is Not ON" ;
					ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
				}
			  public void closeTelecomScreen() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='telecom_mgnment_modal']//button[@class='close']");
				  CloseTelecom.click();
				 sleep(2);
			  }
			  //Harpider-sorting
			  //Validation of SureLock Version column in Device Grid.
	           // 1. Verify data for SureLock Version column.
	           // 2. Verify sorting for SureLock Version column.
	           // 3. Verify search for SureLock Version column.
			  @FindBy(xpath="//input[@data-field='SureLockVersion']")
			  private WebElement SureLockVersionColumnStatus;
			  @FindBy(xpath="//td[@class='SureLockVersion']")
			  private WebElement SureLockVersionColumnValue;
			
			// 1. Verify data for SureLock Version column.
			  public void compareSurelockColumnValue(String sv) throws InterruptedException
			  {
				  clickOnDeviceGridBody();
			      String ActualResult = SureLockVersionColumnValue.getText();
			      String ExpectedResult =sv;
			      String PassStatement ="Actual value of Surelock Version is :"+ActualResult; 
			      String FailStatement ="Value of Surelock Version is not correct";
			      ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement, FailStatement);
			  Reporter.log("Surelock Version column value compared with expected value" ,true);
			  }
			  
			  public void verifySurelockColumnIsEnabledAndGetFinalValue(String sv) throws InterruptedException 
			  {
				  waitForElementPresent("//input[@data-field='SureLockVersion']");
				  if(SureLockVersionColumnStatus.isSelected())
		  		{
					  compareSurelockColumnValue(sv);
		  		}
		  		else
		  		{	
		  			SureLockVersionColumnStatus.click();
		  			compareSurelockColumnValue(sv);
		  		}
				  Reporter.log("Verified Surelock Version column value" ,true);
			    }
			  
			// 3. Verify search for SureLock Version column.
			  
			  @FindBy(xpath="//div[@id='searchOptions_cont']//i[@class='icn2 fa fa-caret-down']")
			  private WebElement SearchOptionsGrid;
			  @FindBy(xpath="//input[@id='advSearchDeviceBtn']")
			  private WebElement advSearchDeviceOption;
			  @FindBy(xpath="//tr[@class='advSearchRow']//input[@data-id='SureLockVersion']")
			  private WebElement SureLockVersionSearchBox;
			  @FindBy(xpath="//div[@id='main_div_id']//span[@class='tableDevCount_line']")
			  private WebElement GridDevCount;
			  @FindBy(xpath="//table[@id='dataGrid']//tbody//tr[1]")
			  private WebElement DevicePresent;
			  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='SureLockVersion']")
			  public WebElement SurelockVersionSort;
			  @FindBy(xpath="//div[@id='tableContainer']//span//button[@class='btn btn-default  dropdown-toggle']")
			  private WebElement PaginationDropDown;
			  @FindBy(xpath="//div[@id='tableContainer']//span//ul//li/a[contains(text(),'10')]")
			  private WebElement Pagination10;
			  @FindBy(xpath="//table[@id='dataGrid']//tbody//tr[1]//td[10]")
			  private WebElement Firstdevice;
			  @FindBy(xpath="//input[@id='globalSearchBtn']")
			  private WebElement SearchOption;
			  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='PhoneNumber']")
			  public WebElement PhoneNumberSort;
			  @FindBy(xpath="//table[@id='dataGrid']//tbody//tr//td[@class='SureLockVersion']")
			  public WebElement SureLockVersionColumn;
			  @FindBy(xpath="//div[@class='pull-right pagination']//li/a[text()='2']")
			  public WebElement Pagination2;
			  @FindBy(xpath="//tr[@class='advSearchRow']//input[@data-id='ReleaseVersion']")
			  private WebElement OSVersionSearchBox;
			  @FindBy(xpath="//tr[@class='advSearchRow']//input[@data-id='SecurityPatchDate']")
			  private WebElement SecurityPatchDateSearchBox;
			  public void clickSecurityPatchDateSearchBox() throws InterruptedException
			  {
				  waitForElementPresent("//tr[@class='advSearchRow']//input[@data-id='SecurityPatchDate']");
				  SecurityPatchDateSearchBox.click();
				  Reporter.log("Click on Security Patch Date Search Box " ,true);
				  
			  }
			  public void clickPagination2InGrid(String column) throws InterruptedException
			  {
				  waitForElementPresent("//div[@class='pull-right pagination']//li/a[text()='2']");
				  Pagination2.click();
				  Reporter.log("Click on Pagination 2 " ,true);
				  waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+column+"']");
				  sleep(3);
			  }
			  public void clickSearchOptionsGrid() throws InterruptedException
			  {
				  waitForElementPresent("//div[@id='searchOptions_cont']//i[@class='icn2 fa fa-caret-down']");
				  SearchOptionsGrid.click();
				  Reporter.log("Click on Search Option in device grid " ,true);
			  }
			  public void clickadvSearchDeviceOption() throws InterruptedException
			  {
				  waitForElementPresent("//input[@id='advSearchDeviceBtn']");
				  advSearchDeviceOption.click();
				  Reporter.log("Click on advance Search Device Option in device grid " ,true);
				  sleep(4);
			  }
			  public void clickSearchDeviceOption() throws InterruptedException
			  {
				  waitForElementPresent("//input[@id='globalSearchBtn']");
				  SearchOption.click();
				  Reporter.log("Click on Search Device Option in device grid " ,true);
				  sleep(5);
			  }
			  public void scrollToColumn(WebElement Coulmn) throws InterruptedException
			  {
				  js.executeScript("arguments[0].scrollIntoView()", Coulmn);
				  Reporter.log("Scrolled to that column in device grid " ,true);
				  sleep(5);
			  }
			 
			  //JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
			  public void EnterValueInSureLockVersionSearchBox(String VersionValue) throws InterruptedException
			  {
				 
				  waitForElementPresent("//tr[@class='advSearchRow']//input[@data-id='SureLockVersion']");
				  scrollToColumn(SureLockVersionSearchBox);
				  sleep(3);
				  SureLockVersionSearchBox.sendKeys(VersionValue);
				  Reporter.log("Entered value in advance Search Box in device grid " ,true);
				  sleep(2);
			  }
			  

		  public void verifyTheVersionOnAllDevices(String SearchVerion,String ColumnName) throws InterruptedException
		  {
		 			  
			  waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+ColumnName+"']");
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']//tbody//tr//td[@class='"+ColumnName+"']"));
			  ArrayList<String> AllDevices = new ArrayList<String>();
			  String Version = SearchVerion.replace(">", "").replace("<", "");
			  System.out.println("Version " + Version);
			  for(int i=0;i<devices.size();i++)
				{
				  //System.out.println(devices.get(i).getText());
				  AllDevices.add(devices.get(i).getText());
				}
			  System.out.println("List of versions :"+AllDevices);
			  
					for(int i=0;i<devices.size();i++)
					{
						if (SearchVerion.contains(">")) 
						{    		
					    	if ((new VersionNumberComp()).compare(Version, devices.get(i).getText()) >= 0)
					    	{
					    		System.out.println(devices.get(i).getText());
					    		Reporter.log("Version number is less than expected value " ,true);
					    	}
					    					    	
					   	} 
						
						else if (SearchVerion.contains("<"))
						{
				    	if ((new VersionNumberComp()).compare(Version, devices.get(i).getText()) <= 0) 
							{
					    		System.out.println(devices.get(i).getText());
					    		Reporter.log("Version number is greater than expected value " ,true);
					    	}
						}
						else
						{
			    		if ((new VersionNumberComp()).compare(Version, devices.get(i).getText()) != 0)
							{
					    		System.out.println(devices.get(i).getText());
					    		Reporter.log("Version number is not equal to expected value " ,true);
					    	}
						}
					   				    
					}
										

			}
		// 2. Verify sorting for SureLock Version column.

		  public void clickColumForSort(String ColumnName ,WebElement Column) throws InterruptedException
		  {
			  waitForElementPresent("//div[@class='fixed-table-header']//table//thead//tr//th[@class='"+ColumnName+"']");
			  Column.click();
			  Reporter.log("Click on "+ColumnName+" sort " ,true);
			  sleep(4);
		  }
		  public void clickOnPaginationDropDown() throws InterruptedException
		  {
			  sleep(5);
			  waitForElementPresent("//div[@id='tableContainer']//span//button[@class='btn btn-default  dropdown-toggle']");
			  PaginationDropDown.click();
			  Reporter.log("Click on Pagination " ,true);
			  sleep(2);
		  }
		  
		  
		  public void scrollToCoulmn(String ColumnName ,WebElement Column) throws InterruptedException
		  {
			
		       waitForElementPresent("//table[@id='dataGrid']//thead//tr//th[@class='"+ColumnName+"']");
			  js.executeScript("arguments[0].scrollIntoView()", Column);
		       Reporter.log("Scroll to the "+ColumnName+" column " ,true);
		       sleep(2);
		  }
		  public void selectPagination() throws InterruptedException
		  {
			  waitForElementPresent("//div[@id='tableContainer']//span//ul//li/a[contains(text(),'10')]");
			  Pagination10.click();
			  Reporter.log("Select pagination 10 " ,true);
			  sleep(4);
		  }
		  
		
		  public void VerifyAcesdingOrder(String Column) throws InterruptedException
		  {
			  //waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+Column+"']");
		      ArrayList<String> AllDevices = new ArrayList<String>();
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath(Column));
			  System.out.println("Size " + devices.size());
			  for(int i=0;i<devices.size();i++)
				{
				  //System.out.println(devices.get(i).getText());
				  AllDevices.add(devices.get(i).getText());
				}
			  System.out.println("Consle sorted values : "+AllDevices);
			  ArrayList<String> AllDevicesSortedAsccending = new ArrayList<String>(AllDevices);
			  AllDevicesSortedAsccending.sort(new VersionNumberComp());
			  System.out.println("Automation Script sorted values : "+AllDevicesSortedAsccending);
			  if (!AllDevicesSortedAsccending.equals(AllDevices)) {
				 ALib.AssertFailMethod("List is not sorted in Ascending order"); // Error case
				 System.out.println("List is not in ascending");
			  } 
			  else
			  {
				  System.out.println("List is sorted in ascending order");  
			  }
			  System.out.println("Sorting Acesding Order SurelockVersion END");
			  sleep(4);
		  }
		  
		  public void VerifydecendingOrder(String Column) throws InterruptedException
		  {
			 // waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+Column+"']");
		      ArrayList<String> AllDevices = new ArrayList<String>();
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']//tbody//tr//td[@class='"+Column+"']"));
			  System.out.println("Size " + devices.size());
			  for(int i=0;i<devices.size();i++)
				{
				  //System.out.println(devices.get(i).getText());
				  AllDevices.add(devices.get(i).getText());
				}
			  System.out.println("Consle sorted values : "+AllDevices);
			  ArrayList<String> AllDevicesSortedAsccending = new ArrayList<String>(AllDevices);
			  AllDevicesSortedAsccending.sort(new VersionNumberComp());
			 Collections.reverse(AllDevicesSortedAsccending);
			  //Collections.sort(AllDevicesSortedAsccending, Collections.reverseOrder());
			
			  System.out.println("Automation Script sorted values : "+AllDevicesSortedAsccending);
			  if (!AllDevicesSortedAsccending.equals(AllDevices)) 
			  {
				  ALib.AssertFailMethod("List is not sorted in descending order");
				  System.out.println("List is not sorted in descending order"); // Error case
			  } 
			  else
			  {
				  System.out.println("List is sorted in descending order");  
			  }
		  }
			  

		//Validation of OS Version column in Device Grid.
			//1. Verify data for OS Version column.
			//2. Verify sorting for OS Version column.
			//3. Verify search for OS Version column.
		  @FindBy(xpath="//input[@data-field='ReleaseVersion']")
		  public WebElement OSVersionColumnStatus;
		  @FindBy(xpath="//td[@class='ReleaseVersion']")
		  private WebElement OSColumnValue;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='ReleaseVersion']")
		  public WebElement OSVersionSort;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='AgentVersion']")
		  public WebElement AgentVersionSort;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='SureFoxVersion']")
		  public WebElement SureFoxVersionSort;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='SureVideoVersion']")
		  public WebElement SureVideoVersionSort;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='SecurityPatchDate']")
		  public WebElement SecurityPatchDateSort;
		  public void compareOSColumnValue(String OSv) throws InterruptedException
		  {
			  clickOnDeviceGridBody();
		      String ActualResult = OSColumnValue.getText();
		      String ExpectedResult =OSv;
		      String PassStatement ="Actual value of OS Version is :"+ActualResult; 
		      String FailStatement ="Value of OS Version is not correct";
		      ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement, FailStatement);
		  Reporter.log("OS Version column value compared with expected value" ,true);
		  }
		 
		  
		  public void verifyOSColumnIsEnabledAndGetFinalValue(String OSv) throws InterruptedException 
		  {
			  waitForElementPresent("//input[@data-field='ReleaseVersion']");
			  if(OSVersionColumnStatus.isSelected())
	  		{
				  
				  compareOSColumnValue(OSv);
	  		}
	  		else
	  		{	
	  			OSVersionColumnStatus.click();
	  			compareOSColumnValue(OSv);
	  		}
			  Reporter.log("Verified OS Version column value" ,true);
		    }
		  public void EnterValueInOSVersionSearchBox(String VersionValue) throws InterruptedException
		  {
			 
			  waitForElementPresent("//tr[@class='advSearchRow']//input[@data-id='ReleaseVersion']");
			  scrollToColumn(OSVersionSearchBox);
			  sleep(3);
			  OSVersionSearchBox.sendKeys(VersionValue);
			  Reporter.log("Entered value in advance Search Box in device grid " ,true);
			  sleep(2);
		  }
		 // Validation of Security Patch Date column in Device Grid.

		//	1. Verify data for Security Patch Date column.

		//	2. Verify sorting for Security Patch Date column.

		//	3. Verify search for Security Patch Date column.
		  @FindBy(xpath="//td[@class='SecurityPatchDate']")
		  public WebElement SecurityPatchDateValue;
		  @FindBy(xpath="//input[@data-field='SecurityPatchDate']")
		  public WebElement SecurityPatchDateColumnSataus;
		  @FindBy(xpath="//input[@id='adv_SecurityPatchDate']")
		  public WebElement SecurityPatchSearchBox;
		  @FindBy(xpath="//button[@title='Refresh']")
		  private WebElement GridRefresh;
		  @FindBy(xpath="//th[@class='prev available']")
		  private WebElement CalenderClick;
		  @FindBy(xpath="(//div[@class='drp-calendar left']//table/tbody/tr/td[text()='1' and @class='available'])[1]")
		  private WebElement Calender_1;
		  @FindBy(xpath="(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']//button[text()='Apply'])[4]")
		  private WebElement CalenderApplyButton;
		  @FindBy(xpath="(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']//button[text()='Cancel'])[4]")
		  public WebElement CalenderCancleButton;
		
		  public void clickDeviceGridRefresh() throws InterruptedException
		  {
			  GridRefresh.click();
			  
			  Reporter.log("Click on Grid Refresh" ,true);
			  sleep(3);
		  }
		  public void clickOnDeviceInGrid(String device) throws InterruptedException
		  {
			  Initialization.driver.findElement(By.xpath(device)).click();
			  Reporter.log("Click on device" ,true);
			  sleep(3);
		  }
		//table[@id='dataGrid']//tbody//tr//td[@class='GpuUsage']
		  public void clickOnSecurityPatchSearchBox() throws InterruptedException
		  {
			  waitForElementPresent("//input[@id='adv_SecurityPatchDate']");
			  SecurityPatchSearchBox.click();
			  Reporter.log("Click on Security Patch Date Saerch Box" ,true);
			  sleep(3);
		  }
		  public void compareSecurityPatchDateColumnValue(String SecurityPatch) throws InterruptedException
		  {
			  clickOnDeviceGridBody();
		      String ActualResult = SecurityPatchDateValue.getText();
		      String ExpectedResult =SecurityPatch;
		      String PassStatement ="Actual value of Security Patch Date is :"+ActualResult; 
		      String FailStatement ="Value of  Security Patch Date column is not correct";
		      ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement, FailStatement);
		  Reporter.log("Security Patch Date column value compared with expected value" ,true);
		  }
		 
		  
		  public void verifySecurityPatchDateColumnIsEnabledAndGetFinalValue(String SecurityPatch) throws InterruptedException 
		  {
			  waitForElementPresent("//input[@data-field='SecurityPatchDate']");
			  if(SecurityPatchDateColumnSataus.isSelected())
	  		{
				  
				  compareSecurityPatchDateColumnValue(SecurityPatch);
	  		}
	  		else
	  		{	
	  			SecurityPatchDateColumnSataus.click();
	  			compareSecurityPatchDateColumnValue(SecurityPatch);
	  		}
			  Reporter.log("Verified Security Patch Date column value" ,true);
		    }
		  public void VerifyDateSorting(String Column) throws InterruptedException
		  {
			  waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='SecurityPatchDate']");
		      ArrayList<String> AllDevices = new ArrayList<String>();
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath(Column));
			  System.out.println("Size " + devices.size());
			  for(int i=0;i<devices.size();i++)
				{
				  //System.out.println(devices.get(i).getText());
				  AllDevices.add(devices.get(i).getText());
				}
			  System.out.println("Consle sorted values : "+AllDevices);
			  ArrayList<String> AllDevicesSortedAsccending = new ArrayList<String>(AllDevices);
			 AllDevicesSortedAsccending.sort(new DateComparator());
			 Collections.reverse(AllDevicesSortedAsccending);
			  System.out.println("Automation Script sorted values : "+AllDevicesSortedAsccending);
			  if (!AllDevicesSortedAsccending.equals(AllDevices)) {
				 ALib.AssertFailMethod("List is not sorted in Descending order"); // Error case
				
			  } 
			  else
			  {
				  System.out.println("List is sorted in Descending order");  
			  }
			  
			  sleep(4);	  
		  }
		  public void SelectDateFromCale(String SecurityPatchDate ,String option) throws InterruptedException 
		  {
			  waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li['"+SecurityPatchDate+"'])[4]");
//			sleep(3);
//		  List<WebElement> sasa = Initialization.driver.findElements(By.xpath("//*[@class='daterangepicker ltr show-ranges show-calendar openscenter']"));
//		  int i=0;
//		  for (WebElement cal:sasa) 
//		  {
//		  
//		  String styleval = cal.getAttribute("style"); 
//		  if (!styleval.isEmpty()) 
//		  {
//		  i=i+1; break;
//		  } 
//		  i=i+1; 
//		  }
		  
		  //Initialization.driver.findElement(By.xpath("//*[@class='daterangepicker ltr show-ranges show-calendar openscenter']["+i+"]/div/ul/li[contains(text(),'"+SecurityPatchDate+"')]")).click();
		  Initialization.driver.findElement(By.xpath("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li['"+SecurityPatchDate+"'])[4]")).click();
		  //ApplyBtn.click();
		  sleep(10);
		  Reporter.log("Clicked on " +option+ " Option" ,true);
		  }
		  
		  public void getSecuritypatchSelectedDate()
		  {
				DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
				DateFormat dateFormat1 = new SimpleDateFormat("MMM yyyy");
		        Calendar cal = Calendar.getInstance();
		        cal.add(Calendar.MINUTE, 2);
		        Date todate1 = cal.getTime();    
		        String dNow = dateFormat.format(todate1);
		        
		        String dNow2=dateFormat1.format(todate1);
		        System.out.println(dNow2);
		        String DateRange="01 "+dNow2+" 12:00:00 am - "+dNow+" 11:59:00 pm";
		        System.out.println(DateRange);
			
			}
		  public void verifyAdvanceSearchOfSecurityPatchDate()
		  {
			 // ArrayList<String> AllDevices = new ArrayList<String>();
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']//tbody//tr//td[@class='SecurityPatchDate']"));
			  System.out.println("Matching result : " + devices.size());
			  String DateRange= Initialization.driver.findElement(By.xpath("//td[@class='SecurityPatchDate']/div//input")).getAttribute("value");
			   System.out.println(DateRange);
			  
			 
			  String[] range = DateRange.split("-");
			  
			  String MinDate = range[0];	  
			 
			  String MaxDate = range[1].trim();	  
			 
			  
			  if(devices.size()==0)
			  {
				  Reporter.log("There is no matching result" ,true);
			  }
			  else
			  {
			  
				  for(int i=0;i<devices.size();i++)
					{
				        if ((new DateComparator().compare(devices.get(i).getText(), MinDate) < 0) ||  (new DateComparator().compare(devices.get(i).getText(), MaxDate) > 0) ){
					        System.out.println("Date is not in the range");

				        } 
				        else 
				        {
					        System.out.println("Date is in the range");
				        }
					 }
			      
			  }
//			  else
//		      {
//		    	  ALib.AssertFailMethod("Expected result is not matching with console data");
//		      } 
		  }
		  public void clickcalenderApplyButton() throws InterruptedException
		  {
			  waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']//button[text()='Apply'])[4]");
			  CalenderApplyButton.click();
			  Reporter.log("Clicked on Apply button " ,true);
			  sleep(5);
		  }
		  public void clickcalenderCancleButton() throws InterruptedException
		  {
		    waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']//button[text()='Cancel'])[4]");
		    CalenderCancleButton.click();
		    Reporter.log("Clicked on Cancle button " ,true);
			  sleep(2);
		  }
		  public void getCurrentDayAndSelect()
		  {
				DateFormat dateFormat = new SimpleDateFormat("d");
		        Date date = new Date();
		        String todate = dateFormat.format(date);
		        //System.out.println(todate);
				Initialization.driver.findElement(By.xpath("(//table[@class='table-condensed']/tbody/tr/td[text()='"+todate+"'])[1]")).click();
				Reporter.log("Selected Current Date " ,true);
			}
		  public void clickOnDate_1() throws InterruptedException
		  {
			  waitForElementPresent("(//div[@class='drp-calendar left']//table/tbody/tr/td[text()='1' and @class='available'])[1]");
		      Calender_1.click();
		      Reporter.log("Selected 1 of month " ,true);
		  }
		//Validation of Group Path column in Device Grid.

			//1. Verify data for Group Path column.

			//2. Verify sorting for Group Path column.

			//3. Verify search for Group Path column.
		  
		  @FindBy(xpath="//input[@data-field='DeviceGroupPath']")
		  public WebElement DeviceGroupPathColumnSataus;
		  @FindBy(xpath="//td[@class='DeviceGroupPath']")
		  public WebElement DeviceGroupPathValue;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='DeviceGroupPath']")
		  public WebElement DeviceGroupPathSort;
		  @FindBy(xpath="//li[@id='all_ava_devices']")
		  public WebElement AllDevices;
		  @FindBy(xpath="//tr[@class='advSearchRow']//input[@data-id='DeviceGroupPath']")
		  private WebElement DeviceGroupPathSearchBox;
		  @FindBy(xpath="//td[@class='GpuUsage']")
		  public WebElement GpuUsageValue;
		  @FindBy(xpath="//input[@data-field='GpuUsage']")
		  public WebElement GpuUsageColumnSataus;
		  @FindBy(xpath="//tr[@class='advSearchRow']//input[@data-id='GpuUsage']")
		  private WebElement GpuUsageSearchBox;
		  public void clickOnAllDevices() throws InterruptedException
		  {
			  waitForElementPresent("//li[@id='all_ava_devices']");
			  AllDevices.click();
			  Reporter.log("Clicked on Alldevices" ,true);
		  }
		  public void compareDeviceGroupPathColumnValue(String Grp) throws InterruptedException
		  {
			  clickOnDeviceGridBody();
		      String ActualResult = DeviceGroupPathValue.getText();
		      String ExpectedResult =Grp;
		      String PassStatement ="Actual value of Group path is :"+ActualResult; 
		      String FailStatement ="Value of Group path is not correct";
		      ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement, FailStatement);
		  Reporter.log("Group path column value compared with expected value" ,true);
		  }
		 
		  
		  public void verifyDeviceGroupPathColumnIsEnabledAndGetFinalValue(String Grp) throws InterruptedException 
		  {
			  waitForElementPresent("//input[@data-field='DeviceGroupPath']");
			  if(DeviceGroupPathColumnSataus.isSelected())
	  		{
				  
				  compareDeviceGroupPathColumnValue(Grp);
	  		}
	  		else
	  		{	
	  			DeviceGroupPathColumnSataus.click();
	  			compareDeviceGroupPathColumnValue(Grp);
	  		}
			  Reporter.log("Verified Device Group Path column value" ,true);
		    }
		  
		  	  
		  public void EnterValueInGroupPathColumn(String Name) throws InterruptedException
		  {
			 
			  waitForElementPresent("//tr[@class='advSearchRow']//input[@data-id='DeviceGroupPath']");
			  scrollToColumn(DeviceGroupPathSearchBox);
			  sleep(3);
			  DeviceGroupPathSearchBox.sendKeys(Name);
			  Reporter.log("Entered value in advance Search Box in device grid " ,true);
			  sleep(2);
		  }
		  
		  public void verifySameGroupPathDevices(String SearchValue,String ColumnName) throws InterruptedException
		  {
			 
		  			  
		  	  waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+ColumnName+"']");
		  	  List<WebElement> devices = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']//tbody//tr//td[@class='"+ColumnName+"']"));
		  	  ArrayList<String> AllDevices = new ArrayList<String>();
		  	for(int i=0;i<devices.size();i++)
	  		{
		  		AllDevices.add(devices.get(i).getText());
		  	}
		  	System.out.println(AllDevices);

		  	 if (!AllDevices.isEmpty())
		  	 {
		  		for(int i=0;i<devices.size();i++)
		  		{
		  		  
		  		  if(SearchValue.equals(devices.get(i).getText()))
		  		  {
		  			 
		  		  }
		  		
		  		 else
			  	 {
			  		ALib.AssertFailMethod("There is no matching result" );
			  	 }
		  		
		  		}
		  		System.out.println("Matching result is correct ");	  		
		  	 }
		  	 else
		  	 {
		  		ALib.AssertFailMethod("There is no matching result" );
		  	 }
		  }
		  public void compareGPUUsageColumnValue(String GPUsage) throws InterruptedException
		  {
			  clickOnDeviceGridBody();
		      String ActualResult = GpuUsageValue.getText();
		      String ExpectedResult =GPUsage;
		      String PassStatement ="Actual value of Gpu Usage Value is :"+ActualResult; 
		      String FailStatement ="Value of Gpu Usage Value is not correct";
		      ALib.AssertEqualsMethod1(ActualResult,ExpectedResult,PassStatement, FailStatement);
		  Reporter.log("Gpu Usage column value compared with expected value" ,true);
		  }
		  
		//Validation of GPU Usage column in Device Grid.

			//1. Verify data for GPU Usage column.
		 
			//2. Verify sorting for GPU Usage column.

			//3. Verify search for GPU Usage column.
		 
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='GpuUsage']")
		  public WebElement GpuUsageSort;
		 
		  
		  public void verifyGPUUsageColumnIsEnabledAndGetFinalValue(String GPUsage) throws InterruptedException 
		  {
			  waitForElementPresent("//input[@data-field='GpuUsage']");
			  if(GpuUsageColumnSataus.isSelected())
	  		{
				  
				  compareGPUUsageColumnValue(GPUsage);
	  		}
	  		else
	  		{	
	  			GpuUsageColumnSataus.click();
	  			compareGPUUsageColumnValue(GPUsage);
	  		}
			  Reporter.log("Verified Device Gpu Usage column value" ,true);
		    }
		  public void EnterValueInGPUUsageColumn(String Name) throws InterruptedException
		  {
			 
			  waitForElementPresent("//tr[@class='advSearchRow']//input[@data-id='GpuUsage']");
			  scrollToColumn(GpuUsageSearchBox);
			  sleep(3);
			  GpuUsageSearchBox.sendKeys(Name);
			  Reporter.log("Entered value in advance Search Box in device grid " ,true);
			  sleep(2);
		  }
		  public void verifyGpuUsageDevices(String Value,String ColumnName) throws InterruptedException
		  {
			  waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+ColumnName+"']");
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']//tbody//tr//td[@class='"+ColumnName+"']"));
			  ArrayList<String> AllDevices = new ArrayList<String>();
			  String Version = Value.replace(">", "").replace("<", "");
			  System.out.println("Version " + Version);
			  for(int i=0;i<devices.size();i++)
				{
				  //System.out.println(devices.get(i).getText());
				  AllDevices.add(devices.get(i).getText());
				}
			  System.out.println("Number of matching devices :"+AllDevices.size());
			  System.out.println("List of versions :"+AllDevices);
			  
					for(int i=0;i<devices.size();i++)
					{
						if (Value.contains(">")) 
						{    		
					    	if ((new VersionNumberComp()).compare(Version, devices.get(i).getText()) >= 0)
					    	{
					    		System.out.println(devices.get(i).getText());
					    		Reporter.log("Version number is less than expected value " ,true);
					    	}
					    					    	
					   	} 
						
						else if (Value.contains("<"))
						{
				    	if ((new VersionNumberComp()).compare(Version, devices.get(i).getText()) <= 0) 
							{
					    		System.out.println(devices.get(i).getText());
					    		Reporter.log("Version number is greater than expected value " ,true);
					    	}
						}
						else
						{
			    		if ((new VersionNumberComp()).compare(Version, devices.get(i).getText()) != 0)
							{
					    		System.out.println(devices.get(i).getText());
					    		Reporter.log("Version number is not equal to expected value " ,true);
					    	}
						}  				    
					}				
		      	}
		  public void VerifydecendingOrderOfPercenttageValue(String Column) throws InterruptedException
		  {
			 // waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+Column+"']");
		      ArrayList<String> AllDevices = new ArrayList<String>();
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath(Column));
			  System.out.println("Size " + devices.size());
			  for(int i=0;i<devices.size();i++)
				{
				  //System.out.println(devices.get(i).getText());
				  if (devices.get(i).getText().contains("%")) {
					  AllDevices.add(devices.get(i).getText().substring(0, devices.get(i).getText().length()-1));
				  }
				  else {
					  AllDevices.add(devices.get(i).getText());
				  }
				 }
			  System.out.println("Consle sorted values : "+AllDevices);
			  ArrayList<String> AllDevicesSortedAsccending = new ArrayList<String>(AllDevices);
			  AllDevicesSortedAsccending.sort(new VersionNumberComp());
			 Collections.reverse(AllDevicesSortedAsccending);
			  //Collections.sort(AllDevicesSortedAsccending, Collections.reverseOrder());
			
			  System.out.println("Automation Script sorted values : "+AllDevicesSortedAsccending);
			  if (!AllDevicesSortedAsccending.equals(AllDevices)) 
			  {
				  ALib.AssertFailMethod("List is not sorted in descending order");
				  System.out.println("List is not sorted in descending order"); // Error case
			  } 
			  else
			  {
				  System.out.println("List is sorted in descending order");  
			  }
		  }
		  //Perform Sorting operation on the search result in the Installed application column in device grid
		  @FindBy(xpath="//span[contains(text(),'AstroContacts ( com.gears42.astrocontacts')]")
		  public WebElement AstroContactsApp;
		  @FindBy(xpath="//input[@data-field='com.gears42.astrocontacts']")
		  public WebElement AstroContactsAppCheckBox;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th/div[contains(text(),'AstroContacts ( com.gears42.astrocontacts  ) ')]")
		  private WebElement AstroContactsSort;
		  @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='ConnectionStatus']")
		  public WebElement ConnectionStatusColumn;
		public void EnableApp() throws InterruptedException
		{
			waitForElementPresent("//span[contains(text(),'AstroContacts ( com.gears42.astrocontacts')]");
			 if(AstroContactsAppCheckBox.isSelected())
	 		{			    	
				  clickOnDeviceGridBody();
				  sleep(2);
				  
	 		}			    			
	      else
	      {
		  AstroContactsAppCheckBox.click();
			clickOnDeviceGridBody();
			sleep(2);
			
	    	}
	         Reporter.log("App is enabled" ,true);
		  }

		public void scrollToInstalledApp() throws InterruptedException
		  {
			
		       waitForElementPresent("//table[@id='dataGrid']//thead//tr//th/div[contains(text(),'AstroContacts ( com.gears42.astrocontacts  ) ')]");
			  js.executeScript("arguments[0].scrollIntoView()", AstroContactsSort);
		       Reporter.log("Scroll to the Installed App column " ,true);
		       sleep(2);
		  }
		public void clickOnInstalledAppSort() throws InterruptedException
		{
			AstroContactsSort.click();
			sleep(2);
		}
		 public void VerifydecendingOrderOfInstalledApp(String Column) throws InterruptedException
		  {
			 // waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='"+Column+"']");
		      ArrayList<String> AllDevices = new ArrayList<String>();
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath(Column));
			  System.out.println("Size " + devices.size());
			  for(int i=0;i<devices.size();i++)
				{
				 		 
					 AllDevices.add(devices.get(i).getText().split(" ")[0]);
				  
				 }
			  System.out.println("Consle sorted values : "+AllDevices);
			  ArrayList<String> AllDevicesSortedAsccending = new ArrayList<String>(AllDevices);
			  AllDevicesSortedAsccending.sort(new VersionNumberComp());
			 Collections.reverse(AllDevicesSortedAsccending);
			  //Collections.sort(AllDevicesSortedAsccending, Collections.reverseOrder());
			
			  System.out.println("Automation Script sorted values : "+AllDevicesSortedAsccending);
			  if (!AllDevicesSortedAsccending.equals(AllDevices)) 
			  {
				  ALib.AssertFailMethod("List is not sorted in descending order");
				  System.out.println("List is not sorted in descending order"); // Error case
			  } 
			  else
			  {
				  System.out.println("List is sorted in descending order");  
			  }
		  }
		 
		 //Sorting should be performed
		 @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='ConnectionStatus']")
		  public WebElement StatusColumn;
		 @FindBy(xpath="//ul[@id='dataGrid_statusCol_sortMenu']//span[contains(text(),'Online Status')]")
		  public WebElement ConnectionStatus;
		
		 public void clickStatusColumn() throws InterruptedException
		 {
			 waitForElementPresent("//div[@class='fixed-table-header']//table//thead//tr//th[@class='ConnectionStatus']");
			 StatusColumn.click();
			 Reporter.log("Click on status column " ,true);
			 sleep(3);
			 
		 }
		 public void clickConnectionStatus() throws InterruptedException
		 {
			 waitForElementPresent("//ul[@id='dataGrid_statusCol_sortMenu']//span[contains(text(),'Online Status')]");
			 ConnectionStatus.click();
			 Reporter.log("Click on Connection Status " ,true);
			 sleep(3);
		 }
		 
		//Verify Sorting for OS Build Number in Device grid
		 @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='OsBuildNumber']")
		  public WebElement OsBuildNumberSort;
		 
		 
		 //Veify Sorting a column in device grid and move to Next page or Group
		 
		 @FindBy(xpath="//div[@class='fixed-table-header']//table//thead//tr//th[@class='IMEI']")
	     public WebElement IMEISort;
		 
		 public void VerifyDateSorting() throws InterruptedException
		  {
			  waitForElementPresent("//table[@id='dataGrid']//tbody//tr//td[@class='SecurityPatchDate']");
		      ArrayList<String> AllDevices = new ArrayList<String>();
			  List<WebElement> devices = Initialization.driver.findElements(By.xpath("//table[@id='dataGrid']//tbody//tr//td[@class='SecurityPatchDate']"));
			  System.out.println("Size " + devices.size());
			  for(int i=0;i<devices.size();i++)
				{
				  //System.out.println(devices.get(i).getText());
				  AllDevices.add(devices.get(i).getText());
				}
			  System.out.println("Consle sorted values : "+AllDevices);
			  ArrayList<String> AllDevicesSortedAsccending = new ArrayList<String>(AllDevices);
			 AllDevicesSortedAsccending.sort(new DateComparator());
			 Collections.reverse(AllDevicesSortedAsccending);
			  System.out.println("Automation Script sorted values : "+AllDevicesSortedAsccending);
			  if (!AllDevicesSortedAsccending.equals(AllDevices)) {
				 ALib.AssertFailMethod("List is not sorted in Descending order"); // Error case
				
			  } 
			  else
			  {
				  System.out.println("List is sorted in Descending order");  
			  }
			  
			  sleep(4);	  
		  }
		 public void SelectDateFromCale_Yestrday() throws InterruptedException 
		  {
			 waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[2])[4]");
		  Initialization.driver.findElement(By.xpath("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[2])[4]")).click();
		  //ApplyBtn.click();
		  sleep(10);
		  Reporter.log("Clicked on Yestrday Option" ,true);
		  }
		  public void SelectDateFromCale_Last1week() throws InterruptedException 
		  {
			 waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[3])[4]");
		  Initialization.driver.findElement(By.xpath("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[3])[4]")).click();
		  //ApplyBtn.click();
		  sleep(10);
		  Reporter.log("Clicked on Last 1 week Option" ,true);
		  }
		  public void SelectDateFromCale_Last30Days() throws InterruptedException 
		  {
			 waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[4])[4]");
		  Initialization.driver.findElement(By.xpath("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[4])[4]")).click();
		  //ApplyBtn.click();
		  sleep(10);
		  Reporter.log("Clicked on Last 30 Days Option" ,true);
		  }
		  public void SelectDateFromCale_ThisMonth() throws InterruptedException 
		  {
			 waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[5])[4]");
		  Initialization.driver.findElement(By.xpath("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[5])[4]")).click();
		  //ApplyBtn.click();
		  sleep(10);
		  Reporter.log("Clicked on This Month Option" ,true);
		  }
		  public void SelectDateFromCale_Today() throws InterruptedException 
		  {
			 waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[1])[4]");
		  Initialization.driver.findElement(By.xpath("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[1])[4]")).click();
		  //ApplyBtn.click();
		  sleep(10);
		  Reporter.log("Clicked on Today Option" ,true);
		  }
		  public void SelectDateFromCale_CustomRange() throws InterruptedException 
		  {
			 waitForElementPresent("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[6])[4]");
		  Initialization.driver.findElement(By.xpath("(//div[@class='daterangepicker ltr show-ranges show-calendar openscenter']/div[1]/ul/li[6])[4]")).click();
		  //ApplyBtn.click();
		  sleep(10);
		  Reporter.log("Clicked on Custom Range Option" ,true);
		  }
	}  



	//Class for Sorting

	 class VersionNumberComp implements Comparator<String> 
	{

	  	private boolean isNumeric(String strNum) {
		    if (strNum == null) {
		        return false;
		    }
		    try {
		        Integer.parseInt(strNum);
		    } catch (NumberFormatException nfe) {
		        return false;
		    }
		    return true;
		}


	  @Override
	  	  public int compare(String version1, String version2)
	  {
			    String[] v1 = version1.split("\\."); // {"1", "2"} or {"NA"}
			    String[] v2 = version2.split("\\."); // {"0", "1", "1"} or {"Not Installed"}

			    if (!isNumeric(v1[0]) && !isNumeric(v2[0])) { // Both are strings
			    	return v1[0].compareTo(v2[0]);
			    }
			    
			    if (!isNumeric(v1[0])){ // NA, 1.2 // First is string
			    	return -1;
			    }

			    if (!isNumeric(v2[0])){ // 1.2, NA // Second is string
			    	return 1;
			    }
			    
			    // Both are valid versions
			    // Iterate though each and compare till we reach end of one of the version
			    int i=0;
			    for (; i<v1.length && i <v2.length; i++) {
			    	if (!v1[i].equals(v2[i])) { //  version is different
			    		return Integer.parseInt(v1[i]) > Integer.parseInt(v2[i]) ? 1 : -1; // compare it and return result
			    	}
			    }

			    if (i==v1.length && i==v2.length) {  // Both are same
			    	return 0;
			    } else if (i==v1.length) {  // If v1 last element is reached, v1 is smaller // Example 1.2 and 1.2.1
			    	return -1;
			    } else if (i==v2.length) { // else if v2 last element is reached, v2 is smaller // Example 1.2.1 and 1.2
			    	return 1;
			    }
			   
			    return 0;
			  }
//		 public static boolean isNumeric(String strNum) {
//			    if (strNum == null) {
//			        return false;
//			    }
//			    try {
//			        Long.parseLong(strNum);
//			    } catch (NumberFormatException nfe) {
//			        return false;
//			    }
//			    return true;
//			}
	//
//		  @Override
//		  public int compare(String version1, String version2) {
//		    String[] v1 = version1.split(" |\\."); // Split with space or dot {"1", "2"} or {"NA"}
//		    String[] v2 = version2.split(" |\\."); // Split with space or dot {"0", "1", "1"} or {"Not", "Installed"}
	//
//		    if (!isNumeric(v1[0]) && !isNumeric(v2[0])) { // Both are strings
//		    	return v1[0].compareTo(v2[0]);
//		    }
//		    
//		    if (!isNumeric(v1[0])){ // NA, 1.2 // First is string
//		    	System.out.println(v1[0]);
//		    	return -1;
//		    }
	//
//		    if (!isNumeric(v2[0])){ // 1.2, NA // Second is string
//		    	System.out.println(v2[0]);
	//
//		    	return 1;
//		    }
//		    
//		    // Both are valid versions
//		    // Iterate though each and compare till we reach end of one of the version
//		    int i=0;
//		    for (; i<v1.length && i <v2.length; i++) {
//		    	if (!v1[i].equals(v2[i])) { //  version is different
//		    		return Long.parseLong(v1[i]) > Long.parseLong(v2[i]) ? 1 : -1; // compare it and return result
//		    	}
//		    }
	//
//		    if (i==v1.length && i==v2.length) {  // Both are same
//		    	return 0;
//		    } else if (i==v1.length) {  // If v1 last element is reached, v1 is smaller // Example 1.2 and 1.2.1
//		    	return -1;
//		    } else if (i==v2.length) { // else if v2 last element is reached, v2 is smaller // Example 1.2.1 and 1.2
//		    	return 1;
//		    }
//		   
//		    return 0;
//		  }
	}

	// Date Sorting 
	 class DateComparator implements Comparator<String> {

		  @Override

		  public int compare(String date1String, String date2String) 
		  {

				String date1Str = date1String;
				String date2Str = date2String;

				String[] date1StrTemp = date1String.split(" ");
				if (date1StrTemp.length > 1 && date1StrTemp[1].length() > 3) { // If second string exist and month has length > 3, take only 3 chars.
					date1StrTemp[1] = date1StrTemp[1].substring(0, 3);
					date1Str = String.join(" ", date1StrTemp);
				}
				
				String[] date2StrTemp = date2String.split(" ");
				if (date2StrTemp.length > 1 && date2StrTemp[1].length() > 3) { // If second string exist and month has length > 3, take only 3 chars.
					date2StrTemp[1] = date2StrTemp[1].substring(0, 3);
					date2Str = String.join(" ", date2StrTemp);
				}
		
			    Date date1 = null;
				try {
					date1 = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).parse(date1Str);
				} catch (ParseException e) {
				// If date is not a valid date. Its an arbitrary string
				}
			    Date date2 = null;
				try {
					date2 = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).parse(date2Str);
				} catch (ParseException e) {
					// If date is not a valid date. Its an arbitrary string
				}
				if (date1==null && date2==null) { // Both are arbitrary strings
					return date1Str.compareTo(date2Str);
				} else if (date1 == null) { // First is arbitrary string
					return -1;
				} else if (date2 == null) { // Second is arbitrary string
					return 1;
				}

			    return date1.compareTo(date2); // Both are valid date strings, compare and return result
			  }
		  

	
}






