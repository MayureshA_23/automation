package OnDemandReports_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class AssetTrackingReport extends Initialization {

//	@Test(priority = 1, description = "1.Generate and View the Asset Tracking Report for Home Groups'")
//	public void GenerateSystemLogReportHomeGroup() throws InterruptedException, NumberFormatException,
//			EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		Reporter.log("1.Generate and View the Device Activity Report", true);
//		reportsPage.ClickOnReports_Button();
//		reportsPage.ClickOnAssetTrackingReport();
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		reportsPage.ClickOnView();
//		reportsPage.WindowHandle();
//		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
//		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(); // common
//		reportsPage.DeviceModelColumnVerificationAssetReport();
//		reportsPage.OSVersionColumnVerificationAssetReport();
//		reportsPage.OSTypeColumnVerificationAssetReport();
//		reportsPage.OnlineStatusColumnVerificationAssetReport();
//		reportsPage.KnoxStatusColumnVerificationAssetReport();
//		reportsPage.SwitchBackWindow();
//		reportsPage.ClickOnOnDemandReport();
//
//	}
//
	
//	@Test(priority=4,description="Verify Scheduled Jobs count in the Asset Tracking report")
//	public void VerifyScheduledjobsCountInAssetTrackinhReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		quickactiontoolbarpage.ClickOnNixSettings();
//		quickactiontoolbarpage.DisablingNixService();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField(Config.InstallJObName);
//		androidJOB.ClickOnApplyJobOkButton();
//		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
//		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Pending",1);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnOnDemandReport();
//		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Scheduled Jobs Column",60);
//		accountsettingspage.SwitchBackWindow();
//		commonmethdpage.ClickOnHomePage();
//	}
//
//	@Test(priority=5,description="Verify InProgress Jobs count column in the Asset Tracking report",dependsOnMethods="VerifyScheduledjobsCountInAssetTrackinhReport")
//	public void VerifyInprogressjobsCountInAssetTrackinhReport() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
//	{
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		quickactiontoolbarpage.ClickOnNixSettings();
//		quickactiontoolbarpage.EnablingCheckBoxes("Enable Nix Service",0);
//		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		accountsettingspage.ReadingConsoleMessageForJobDeployment("surevideo.apk",Config.DeviceName);
//		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Inprogress",2);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnOnDemandReport();
//		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Inprogress Jobs Coulumn",61);
//		accountsettingspage.SwitchBackWindow();
//		commonmethdpage.ClickOnHomePage();
//	}
//
//	@Test(priority=6,description="Verify Failed Jobs count column in the Asset Tracking report")
//	public void VerifyFailedjobsCountInAssetTrackinhReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		quickactiontoolbarpage.ClearingConsoleLogs();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
//		androidJOB.ClickOnApplyJobOkButton();
//		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
//		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Error",3);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnOnDemandReport();
//		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Failed Jobs Column",62);
//		accountsettingspage.SwitchBackWindow();
//		commonmethdpage.ClickOnHomePage();
//	}
//
//	@Test(priority=7,description="Verify Deployed Jobs count column in the Asset Tracking report")
//	public void VerifyDeployedjobsCountInAssetTrackinhReport() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
//	{
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Completed Jobs",4);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnOnDemandReport();
//		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Deployed Jobs Coulumn",63);
//		accountsettingspage.SwitchBackWindow();
//		commonmethdpage.ClickOnHomePage();
//	}
//	
//	@Test(priority=8,description="Verify Session Logs filter is present in On Demand Reports ")
//	public void VerifySessionLogFilterInOnDemandReports() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnOnDemandReport();
//		reportsPage.systemreports();
//		reportsPage.VerifySessionLogFiltersInOnDemandReports();
//		reportsPage.VerifySessionLogsFilterIsEnabledByDefaultInOnDemandReports();
//		commonmethdpage.ClickOnHomePage();
//		
//	}
//	
//	@Test(priority=9,description="Verify Mac address in Asset Tracking Report")
//	public void VerifyMacAddressInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
//		reportsPage.ReadingMACAddressInDeviceInfo();
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnOnDemandReport();
//		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//		quickactiontoolbarpage.SearchingInsideReport(Config.Windows_DeviceName1);
//		reportsPage.ReadingMACAddressInReport();
//		reportsPage.VerifyMACAddressInReport();
//		accountsettingspage.SwitchBackWindow();
//		commonmethdpage.ClickOnHomePage();
//	}
	
///////////////////////////////Asset Tracking Report For HOME Group/////////////////////////////////////////////////////////////////////////////////////////	
	@Test(priority=0,description="Verify Device Name in Asset Tracking Report")
	public void VerifyDeviceNameInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.DeviceName);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(1);
		reportsPage.VerifyColumnInReport("Device Name");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=1,description="Verify Device Model in Asset Tracking Report")
	public void VerifyDeviceModelInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.DeviceModel);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(2);
		reportsPage.VerifyColumnInReport("Device Model");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=2,description="Verify Device OS Version in Asset Tracking Report")
	public void VerifyDeviceOSVersionInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueOfOSVersionInDeviceInfo();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(3);
		reportsPage.VerifyColumnInReport("OS Version");
		 accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=3,description="Verify Device OS Type in Asset Tracking Report")
	public void VerifyDeviceOSTypeInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	  
	   commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueOfOSTypeInDeviceInfo();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(4);
		reportsPage.VerifyColumnInReport("OS Type");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=4,description="Verify Device Status in Asset Tracking Report")
	public void VerifyDeviceStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.DeviceStatus);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(6);
		reportsPage.VerifyColumnInReport("Device Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=5,description="Verify Device Locale in Asset Tracking Report")
	public void VerifyDeviceLocaleInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Locale");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(10);
		reportsPage.VerifyColumnInReport("Device Locale");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=6,description="Verify Mac Address in Asset Tracking Report")
	public void VerifyMacAddressInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.MacAddress);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(13);
		reportsPage.VerifyColumnInReport("Mac Address");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=7,description="Verify IMEI in Asset Tracking Report")
	public void VerifyIMEIInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IMEI");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(14);
		reportsPage.VerifyColumnInReport("IMEI");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=8,description="Verify Battery in Asset Tracking Report")
	public void VerifyBatteryInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingPercentageValueInDeviceGrid(reportsPage.Battery);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(16);
		reportsPage.VerifyColumnInReport("Battery");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=9,description="Verify WifiSSID in Asset Tracking Report")
	public void VerifyWifiSSIDInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("WifiSSID");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(18);
		reportsPage.VerifyColumnInReport("Wifi SSID");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=10,description="Verify Wifi signal strenth in Asset Tracking Report")
	public void VerifyWifiSignalStrenthInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(4);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(19);
		reportsPage.VerifyColumnInReport("Wifi signal strenth");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=11,description="Verify Cell signal strenth in Asset Tracking Report")
	public void VerifyCellSignalStrenthInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingPercentageValueInDeviceGrid(reportsPage.CellSignal);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(21);
		reportsPage.VerifyColumnInReport("Cell signal strenth");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=12,description="Verify Network Operator in Asset Tracking Report")
	public void VerifyNetworkOperatorInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Operator");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(20);
		reportsPage.VerifyColumnInReport("Network Operator");
		accountsettingspage.SwitchBackWindow();
			}
	@Test(priority=13,description="Verify Sim Serial Number in Asset Tracking Report")
	public void VerifySimSerialNumberInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SimSerialNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(24);
		reportsPage.VerifyColumnInReport("Sim Serial Number");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=14,description="Verify Global IP in Asset Tracking Report")
	public void VerifyGlobalIPInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(1);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(26);
		reportsPage.VerifyColumnInReport("Global IP");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=15,description="Verify Local IP in Asset Tracking Report")
	public void VerifyLocalIPInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(2);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(27);
		reportsPage.VerifyColumnInReport("Local IP");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=16,description="Verify Root Status in Asset Tracking Report")
	public void VerifyRootStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("RootStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(28);
		reportsPage.VerifyColumnInReport("Root Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=17,description="Verify Knox Status in Asset Tracking Report")
	public void VerifyKnoxStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("KnoxStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(29);
		reportsPage.VerifyColumnInReport("Knox Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=18,description="Verify Agent Version in Asset Tracking Report")
	public void VerifyAgentVersionInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AgentVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(30);
		reportsPage.VerifyColumnInReport("Agent Version");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=19,description="Verify SureLock Version in Asset Tracking Report")
	public void VerifySureLockVersionInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureLockVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(31);
		reportsPage.VerifyColumnInReport("SureLock Version");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=20,description="Verify SureFox Version in Asset Tracking Report")
	public void VerifySureFoxVersionInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureFoxVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(32);
		reportsPage.VerifyColumnInReport("SureFox Version");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=21,description="Verify SureVideo Version in Asset Tracking Report")
	public void VerifySureVideoVersionInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureVideoVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(33);
		reportsPage.VerifyColumnInReport("SureVideo Version");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=22,description="Verify Bluetooth SSID in Asset Tracking Report")
	public void VerifyBluetoothSSIDInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("BSSID");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(34);
		reportsPage.VerifyColumnInReport("Bluetooth SSID");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=23,description="Verify Bluetooth Status in Asset Tracking Report")
	public void VerifyBluetoothStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("BluetoothEnabled");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(36);
		reportsPage.VerifyColumnInReport("Bluetooth Status");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=24,description="Verify USB Status in Asset Tracking Report")
	public void VerifyUSBStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("USBPluggedIn");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(37);
		reportsPage.VerifyColumnInReport("USB Status");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=25,description="Verify Group Path in Asset Tracking Report")
	public void VerifyGroupPathInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("DeviceGroupPath");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(40);
		reportsPage.VerifyColumnInReport("Group Path");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=26,description="Verify Group Name in Asset Tracking Report")
	public void VerifyGroupNameInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueOfGroupName();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(39);
		reportsPage.VerifyColumnInReport("Group Name");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=27,description="Verify Notes in Asset Tracking Report")
	public void VerifyNotesInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.Notes);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(41);
		reportsPage.VerifyColumnInReport("Notes");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=28,description="Verify OsBuildNumber in Asset Tracking Report")
	public void VerifyOsBuildNumberInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("OsBuildNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(43);
		reportsPage.VerifyColumnInReport("OsBuildNumber");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=29,description="Verify Free Storage Memory in Asset Tracking Report")
	public void VerifyFreeStorageMemoryInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingPercentageValueInDeviceGrid(reportsPage.FreeStorageMemory);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(44);
		reportsPage.VerifyColumnInReport("Free Storage Memory");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=30,description="Verify Free Program Memory in Asset Tracking Report")
	public void VerifyFreeProgramMemoryInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInConsole(reportsPage.FreeProgramMemory);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(45);
		reportsPage.VerifyColumnInReport("Free Program Memory");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=31,description="Verify IMEI2 in Asset Tracking Report")
	public void VerifyIMEI2InassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IMEI2");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(53);
		reportsPage.VerifyColumnInReport("IMEI2");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=32,description="Verify Threat Protection in Asset Tracking Report")
	public void VerifyThreatProtectionInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("VerifyAppEnable");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(56);
		reportsPage.VerifyColumnInReport("Threat Protection");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=33,description="Verify Unknown Source Apps in Asset Tracking Report")
	public void VerifyUnknownSourceAppsInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AllowUnknownSource");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(58);
		reportsPage.VerifyColumnInReport("Unknown Source Apps");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=34,description="Verify AndroidID in Asset Tracking Report")
	public void VerifyAndroidIDInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AndroidID");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(59);
		reportsPage.VerifyColumnInReport("AndroidID");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=35,description="Verify HashCode in Asset Tracking Report")
	public void VerifyHashCodeInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("HashCode");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(60);
		reportsPage.VerifyColumnInReport("HashCode");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=36,description="Verify FOTA Registration Status in Asset Tracking Report")
	public void VerifyFotaRegistrationStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("EfotaRegistrationStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(61);
		reportsPage.VerifyColumnInReport("FOTA Registration Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=37,description="Verify Firmware Version in Asset Tracking Report")
	public void VerifyCurrentFirmwareVersionInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("CurrentFirmwareVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(62);
		reportsPage.VerifyColumnInReport("Firmware Version");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=38,description="Verify IntelAMT Configuration Status in Asset Tracking Report")
	public void VerifyIntelAMTConfigurationStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AMTConnectionStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(67);
		reportsPage.VerifyColumnInReport("IntelAMT Configuration Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=39,description="Verify DEP Server in Asset Tracking Report")
	public void VerifyDEPServerInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("DEPServerName");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(68);
		reportsPage.VerifyColumnInReport("DEP Server");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=40,description="Verify InProgress Jobs Count in Asset Tracking Report")
	public void VerifyInProgressJobsCountInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.clickOnJobQueueButton();
		reportsPage.ReadingValueInDeviceInfo(reportsPage.InprogressJobs);
		reportsPage.clickOnCloseButton();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(64);
		reportsPage.VerifyColumnInReport("InProgress Jobs Count");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=41,description="Verify Failed Jobs Count in Asset Tracking Report")
	public void VerifyFailedJobsCountInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.clickOnJobQueueButton();
		reportsPage.ReadingValueInDeviceInfo(reportsPage.ErrorJobs);
		reportsPage.clickOnCloseButton();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(65);
		reportsPage.VerifyColumnInReport("Failed Jobs Count");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=42,description="Verify Success Jobs Count in Asset Tracking Report")
	public void VerifySuccessJobsCountInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.clickOnJobQueueButton();
		reportsPage.ReadingValueInDeviceInfo(reportsPage.SuccessJobs);
		reportsPage.clickOnCloseButton();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(66);
		reportsPage.VerifyColumnInReport("Success Jobs Count");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=43,description="Verify Scheduled Jobs Count in Asset Tracking Report")
	public void VerifyScheduledJobsCountInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueFromConfig(Config.ScheduledJobsCOunt);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(63);
		reportsPage.VerifyColumnInReport("Scheduled Jobs Count");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=44,description="Verify Platform Integrity in Asset Tracking Report")
	public void VerifyPlatformIntegrityInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("BasicIntegrity");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(55);
		reportsPage.VerifyColumnInReport("Platform Integrity");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=45,description="Verify Last logged In User in Asset Tracking Report")
	public void VerifyLastloggedInUserInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("LastLoggedInUser");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(11);
		reportsPage.VerifyColumnInReport("Last logged In User");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=46,description="Verify Hostname in Asset Tracking Report")
	public void VerifyHostnameInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	
    	commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Hostname");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(12);
		reportsPage.VerifyColumnInReport("Hostname");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=47,description="Verify Phone Serial Number in Asset Tracking Report")
	public void VerifyPhoneSerialNumberInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SerialNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(22);
		reportsPage.VerifyColumnInReport("Phone Serial Number");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=48,description="Verify Processor in Asset Tracking Report")
	public void VerifyProcessorInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Processor");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(23);
		reportsPage.VerifyColumnInReport("Processor");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=49,description="Verify Phone Number in Asset Tracking Report")
	public void VerifyPhoneNumberInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("PhoneNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(25);
		reportsPage.VerifyColumnInReport("Phone Number");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=50,description="Verify Surelock Settings Identifier in Asset Tracking Report")
	public void VerifySurelockSettingsIdentifierInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureLockSettingsVersionCode");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(35);
		reportsPage.VerifyColumnInReport("Surelock Settings Identifier");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=50,description="Verify Supervised(IOS) in Asset Tracking Report")
	public void VerifySupervisedInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IsSupervised");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(42);
		reportsPage.VerifyColumnInReport("Supervised(IOS)");
		accountsettingspage.SwitchBackWindow();
	}
//	@Test(priority=51,description="Verify SureLock Default Launcher in Asset Tracking Report")
//	public void VerifySureLockDefaultLauncherInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.SureLockDefaultLauncher);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(46);
//		reportsPage.VerifyColumnInReport("SureLock Default Launcher");
//	accountsettingspage.SwitchBackWindow();
//	}
//	@Test(priority=52,description="Verify SureLock Device Administartor in Asset Tracking Report")
//	public void VerifySureLockDeviceAdministartorInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//	
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.SureLockAdministrator);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(47);
//		reportsPage.VerifyColumnInReport("SureLock Device Administartor");
//	accountsettingspage.SwitchBackWindow();
//	}
//	@Test(priority=53,description="Verify Apps With Usage Access in Asset Tracking Report")
//	public void VerifyAppsWithUsageAccessInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.AppsWithUsageAccess);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(49);
//		reportsPage.VerifyColumnInReport("Apps With Usage Access");
//	accountsettingspage.SwitchBackWindow();
//	}
//	@Test(priority=53,description="Verify Samsung KNOX in Asset Tracking Report")
//	public void VerifySamsungKNOXInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.SamsungKNOX);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(48);
//		reportsPage.VerifyColumnInReport("Samsung KNOX");
//	accountsettingspage.SwitchBackWindow();
//	}
//	
//	@Test(priority=54,description="Verify Disable USB Debugging in Asset Tracking Report")
//	public void VerifyDisableUSBDebuggingInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.UsbDebuggingDisabled);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(50);
//		reportsPage.VerifyColumnInReport("Disable USB Debugging");
//	accountsettingspage.SwitchBackWindow();
//	}
	@Test(priority=55,description="Verify Wi-Fi Hotspot Status in Asset Tracking Report")
	public void VerifyWiFiHotspotStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IsMobileHotSpotEnabled");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(51);
		reportsPage.VerifyColumnInReport("Wi-Fi Hotspot Status");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=56,description="Verify Encryption Status in Asset Tracking Report")
	public void VerifyEncryptionStatusInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IsEncryptionEnabled");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(52);
		reportsPage.VerifyColumnInReport("Encryption Status");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=57,description="Verify CTS Verified in Asset Tracking Report")
	public void VerifyCTSVerifiedInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("CtsProfileMatch");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(54);
		reportsPage.VerifyColumnInReport("CTS Verified");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=58,description="Verify USB Debugging in Asset Tracking Report")
	public void VerifyUSBDebuggingInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("ADBEnable");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(57);
		reportsPage.VerifyColumnInReport("USB Debugging");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=59,description="Verify IMSI in Asset Tracking Report")
	public void VerifyIMSIInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.readValueOfIMSIFromDeviceInfo();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(15);
		reportsPage.VerifyColumnInReport("IMSI");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=60,description="Verify Default Launcher Application in Asset Tracking Report")
	public void VerifyDefaultLauncherApplicationInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueFromConfig(Config.DefaultLauncherApplication);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(38);
		reportsPage.VerifyColumnInReport("Default Launcher Application");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=61,description="Verify Registered Date in Asset Tracking Report")
	public void VerifyRegisteredDateInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.getStringFromGridAndsplitDate("LastTimeStamp");
		reportsPage.splitTime();
		reportsPage.englishTime();
		reportsPage.joinDateAndTime();
		reportsPage.convertDateTimeFormat();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.getTimeStampFromReport(5);
		reportsPage.VerifyColumnInReport("Registered Date");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=62,description="Verify Last Connected in Asset Tracking Report")
	public void VerifyLastConnectedInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException
	{

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.getStringFromGridAndsplitDate("LastTimeStamp");
		reportsPage.splitTime();
		reportsPage.englishTime();
		reportsPage.joinDateAndTime();
		reportsPage.convertDateTimeFormat();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.getTimeStampFromReport(7);
		reportsPage.VerifyColumnInReport("Last Connected");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=63,description="Verify Device Time in Asset Tracking Report")
	public void VerifyDeviceTimeInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.getStringFromGridAndsplitDate("DeviceTimeStamp");
		reportsPage.splitTime();
		reportsPage.englishTime();
		reportsPage.joinDateAndTime();
		reportsPage.convertDateTimeFormat();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.getTimeStampFromReport(8);
		reportsPage.VerifyColumnInReport("Device Time");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=64,description="Verify Device Time Zone in Asset Tracking Report")
	public void VerifyDeviceTimeZonelInassetTrackingReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("DeviceTimeZone");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(9);
		reportsPage.VerifyColumnInReport("Device Time Zone");
		accountsettingspage.SwitchBackWindow();
		
	}
	
///////////////////////////////Asset Tracking Report For Sub Group/////////////////////////////////////////////////////////////////////////////////////////		

	@Test(priority =65, description = "Generate and View the Asset Tracking Report for Sub Groups'")
	public void GenerateSystemLogReportSubGroup() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		System.out.println("///////////////////////////////Asset Tracking Report For Sub Group/////////////////////////////////////");
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.DeviceName);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		reportsPage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(1);
		reportsPage.VerifyColumnInReport("Device Name");
	}
	@Test(priority=66,description="Verify Device Model in Asset Tracking Report")
	public void VerifyDeviceModelInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.DeviceModel);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(2);
		reportsPage.VerifyColumnInReport("Device Model");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=67,description="Verify Device OS Version in Asset Tracking Report")
	public void VerifyDeviceOSVersionInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueOfOSVersionInDeviceInfo();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(3);
		reportsPage.VerifyColumnInReport("OS Version");
		 accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=68,description="Verify Device OS Type in Asset Tracking Report")
	public void VerifyDeviceOSTypeInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueOfOSTypeInDeviceInfo();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(4);
		reportsPage.VerifyColumnInReport("OS Type");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=69,description="Verify Device Status in Asset Tracking Report")
	public void VerifyDeviceStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.DeviceStatus);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(6);
		reportsPage.VerifyColumnInReport("Device Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=70,description="Verify Device Locale in Asset Tracking Report")
	public void VerifyDeviceLocaleInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Locale");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(10);
		reportsPage.VerifyColumnInReport("Device Locale");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=71,description="Verify Mac Address in Asset Tracking Report")
	public void VerifyMacAddressInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.MacAddress);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(13);
		reportsPage.VerifyColumnInReport("Mac Address");
		accountsettingspage.SwitchBackWindow();
		
		
	}
	@Test(priority=72,description="Verify IMEI in Asset Tracking Report")
	public void VerifyIMEIInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IMEI");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(14);
		reportsPage.VerifyColumnInReport("IMEI");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=73,description="Verify Battery in Asset Tracking Report")
	public void VerifyBatteryInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingPercentageValueInDeviceGrid(reportsPage.Battery);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(16);
		reportsPage.VerifyColumnInReport("Battery");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=74,description="Verify WifiSSID in Asset Tracking Report")
	public void VerifyWifiSSIDInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("WifiSSID");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(18);
		reportsPage.VerifyColumnInReport("Wifi SSID");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=75,description="Verify Wifi signal strenth in Asset Tracking Report")
	public void VerifyWifiSignalStrenthInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(4);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(19);
		reportsPage.VerifyColumnInReport("Wifi signal strenth");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=76,description="Verify Cell signal strenth in Asset Tracking Report")
	public void VerifyCellSignalStrenthInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingPercentageValueInDeviceGrid(reportsPage.CellSignal);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(21);
		reportsPage.VerifyColumnInReport("Cell signal strenth");
		accountsettingspage.SwitchBackWindow();
		
		
	}
	@Test(priority=77,description="Verify Network Operator in Asset Tracking Report")
	public void VerifyNetworkOperatorInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Operator");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(20);
		reportsPage.VerifyColumnInReport("Network Operator");
		accountsettingspage.SwitchBackWindow();
			}
	@Test(priority=78,description="Verify Sim Serial Number in Asset Tracking Report")
	public void VerifySimSerialNumberInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SimSerialNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(24);
		reportsPage.VerifyColumnInReport("Sim Serial Number");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=79,description="Verify Global IP in Asset Tracking Report")
	public void VerifyGlobalIPInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(1);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(26);
		reportsPage.VerifyColumnInReport("Global IP");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=80,description="Verify Local IP in Asset Tracking Report")
	public void VerifyLocalIPInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(2);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(27);
		reportsPage.VerifyColumnInReport("Local IP");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=81,description="Verify Root Status in Asset Tracking Report")
	public void VerifyRootStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("RootStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(28);
		reportsPage.VerifyColumnInReport("Root Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=82,description="Verify Knox Status in Asset Tracking Report")
	public void VerifyKnoxStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("KnoxStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(29);
		reportsPage.VerifyColumnInReport("Knox Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=83,description="Verify Agent Version in Asset Tracking Report")
	public void VerifyAgentVersionInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AgentVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(30);
		reportsPage.VerifyColumnInReport("Agent Version");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=84,description="Verify SureLock Version in Asset Tracking Report")
	public void VerifySureLockVersionInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureLockVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(31);
		reportsPage.VerifyColumnInReport("SureLock Version");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=85,description="Verify SureFox Version in Asset Tracking Report")
	public void VerifySureFoxVersionInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureFoxVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(32);
		reportsPage.VerifyColumnInReport("SureFox Version");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=86,description="Verify SureVideo Version in Asset Tracking Report")
	public void VerifySureVideoVersionInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureVideoVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(33);
		reportsPage.VerifyColumnInReport("SureVideo Version");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=87,description="Verify Bluetooth SSID in Asset Tracking Report")
	public void VerifyBluetoothSSIDInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("BSSID");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(34);
		reportsPage.VerifyColumnInReport("Bluetooth SSID");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=88,description="Verify Bluetooth Status in Asset Tracking Report")
	public void VerifyBluetoothStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("BluetoothEnabled");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(36);
		reportsPage.VerifyColumnInReport("Bluetooth Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=89,description="Verify USB Status in Asset Tracking Report")
	public void VerifyUSBStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("USBPluggedIn");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(37);
		reportsPage.VerifyColumnInReport("USB Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=90,description="Verify Group Path in Asset Tracking Report")
	public void VerifyGroupPathInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("DeviceGroupPath");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(40);
		reportsPage.VerifyColumnInReport("Group Path");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=91,description="Verify Group Name in Asset Tracking Report")
	public void VerifyGroupNameInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueOfGroupName();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(39);
		reportsPage.VerifyColumnInReport("Group Name");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=92,description="Verify Notes in Asset Tracking Report")
	public void VerifyNotesInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceInfo(reportsPage.Notes);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(41);
		reportsPage.VerifyColumnInReport("Notes");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=93,description="Verify OsBuildNumber in Asset Tracking Report")
	public void VerifyOsBuildNumberInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("OsBuildNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(43);
		reportsPage.VerifyColumnInReport("OsBuildNumber");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=94,description="Verify Free Storage Memory in Asset Tracking Report")
	public void VerifyFreeStorageMemoryInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingPercentageValueInDeviceGrid(reportsPage.FreeStorageMemory);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(44);
		reportsPage.VerifyColumnInReport("Free Storage Memory");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=95,description="Verify Free Program Memory in Asset Tracking Report")
	public void VerifyFreeProgramMemoryInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInConsole(reportsPage.FreeProgramMemory);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(45);
		reportsPage.VerifyColumnInReport("Free Program Memory");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=96,description="Verify IMEI2 in Asset Tracking Report")
	public void VerifyIMEI2InassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IMEI2");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(53);
		reportsPage.VerifyColumnInReport("IMEI2");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=97,description="Verify Threat Protection in Asset Tracking Report")
	public void VerifyThreatProtectionInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("VerifyAppEnable");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(56);
		reportsPage.VerifyColumnInReport("Threat Protection");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=98,description="Verify Unknown Source Apps in Asset Tracking Report")
	public void VerifyUnknownSourceAppsInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AllowUnknownSource");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(58);
		reportsPage.VerifyColumnInReport("Unknown Source Apps");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=99,description="Verify AndroidID in Asset Tracking Report")
	public void VerifyAndroidIDInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AndroidID");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(59);
		reportsPage.VerifyColumnInReport("AndroidID");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=100,description="Verify HashCode in Asset Tracking Report")
	public void VerifyHashCodeInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("HashCode");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(60);
		reportsPage.VerifyColumnInReport("HashCode");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=101,description="Verify FOTA Registration Status in Asset Tracking Report")
	public void VerifyFotaRegistrationStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("EfotaRegistrationStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(61);
		reportsPage.VerifyColumnInReport("FOTA Registration Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=102,description="Verify Firmware Version in Asset Tracking Report")
	public void VerifyCurrentFirmwareVersionInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("CurrentFirmwareVersion");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(62);
		reportsPage.VerifyColumnInReport("Firmware Version");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=103,description="Verify IntelAMT Configuration Status in Asset Tracking Report")
	public void VerifyIntelAMTConfigurationStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("AMTConnectionStatus");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(67);
		reportsPage.VerifyColumnInReport("IntelAMT Configuration Status");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=104,description="Verify DEP Server in Asset Tracking Report")
	public void VerifyDEPServerInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("DEPServerName");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(68);
		reportsPage.VerifyColumnInReport("DEP Server");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=105,description="Verify InProgress Jobs Count in Asset Tracking Report")
	public void VerifyInProgressJobsCountInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.clickOnJobQueueButton();
		reportsPage.ReadingValueInDeviceInfo(reportsPage.InprogressJobs);
		reportsPage.clickOnCloseButton();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(64);
		reportsPage.VerifyColumnInReport("InProgress Jobs Count");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=106,description="Verify Failed Jobs Count in Asset Tracking Report")
	public void VerifyFailedJobsCountInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.clickOnJobQueueButton();
		reportsPage.ReadingValueInDeviceInfo(reportsPage.ErrorJobs);
		reportsPage.clickOnCloseButton();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(65);
		reportsPage.VerifyColumnInReport("Failed Jobs Count");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=107,description="Verify Success Jobs Count in Asset Tracking Report")
	public void VerifySuccessJobsCountInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.clickOnJobQueueButton();
		reportsPage.ReadingValueInDeviceInfo(reportsPage.SuccessJobs);
		reportsPage.clickOnCloseButton();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(66);
		reportsPage.VerifyColumnInReport("Success Jobs Count");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=108,description="Verify Scheduled Jobs Count in Asset Tracking Report")
	public void VerifyScheduledJobsCountInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueFromConfig(Config.ScheduledJobsCOunt);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(63);
		reportsPage.VerifyColumnInReport("Scheduled Jobs Count");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=109,description="Verify Platform Integrity in Asset Tracking Report")
	public void VerifyPlatformIntegrityInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("BasicIntegrity");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(55);
		reportsPage.VerifyColumnInReport("Platform Integrity");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=110,description="Verify Last logged In User in Asset Tracking Report")
	public void VerifyLastloggedInUserInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("LastLoggedInUser");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(11);
		reportsPage.VerifyColumnInReport("Last logged In User");
		accountsettingspage.SwitchBackWindow();
		
	}
	@Test(priority=111,description="Verify Hostname in Asset Tracking Report")
	public void VerifyHostnameInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Hostname");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(12);
		reportsPage.VerifyColumnInReport("Hostname");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=112,description="Verify Phone Serial Number in Asset Tracking Report")
	public void VerifyPhoneSerialNumberInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SerialNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(22);
		reportsPage.VerifyColumnInReport("Phone Serial Number");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=113,description="Verify Processor in Asset Tracking Report")
	public void VerifyProcessorInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("Processor");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(23);
		reportsPage.VerifyColumnInReport("Processor");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=114,description="Verify Phone Number in Asset Tracking Report")
	public void VerifyPhoneNumberInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("PhoneNumber");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(25);
		reportsPage.VerifyColumnInReport("Phone Number");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=115,description="Verify Surelock Settings Identifier in Asset Tracking Report")
	public void VerifySurelockSettingsIdentifierInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("SureLockSettingsVersionCode");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(35);
		reportsPage.VerifyColumnInReport("Surelock Settings Identifier");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=116,description="Verify Supervised(IOS) in Asset Tracking Report")
	public void VerifySupervisedInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IsSupervised");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(42);
		reportsPage.VerifyColumnInReport("Supervised(IOS)");
		accountsettingspage.SwitchBackWindow();
	}
//	@Test(priority=117,description="Verify SureLock Default Launcher in Asset Tracking Report")
//	public void VerifySureLockDefaultLauncherInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.SureLockDefaultLauncher);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(46);
//		reportsPage.VerifyColumnInReport("SureLock Default Launcher");
//	accountsettingspage.SwitchBackWindow();
//	}
//	@Test(priority=118,description="Verify SureLock Device Administartor in Asset Tracking Report")
//	public void VerifySureLockDeviceAdministartorInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//	
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.SureLockAdministrator);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(47);
//		reportsPage.VerifyColumnInReport("SureLock Device Administartor");
//	accountsettingspage.SwitchBackWindow();
//	}
//	@Test(priority=119,description="Verify Apps With Usage Access in Asset Tracking Report")
//	public void VerifyAppsWithUsageAccessInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.AppsWithUsageAccess);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(49);
//		reportsPage.VerifyColumnInReport("Apps With Usage Access");
//	accountsettingspage.SwitchBackWindow();
//	}
//	@Test(priority=120,description="Verify Samsung KNOX in Asset Tracking Report")
//	public void VerifySamsungKNOXInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.SamsungKNOX);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(48);
//		reportsPage.VerifyColumnInReport("Samsung KNOX");
//	accountsettingspage.SwitchBackWindow();
//	}
//	
//	@Test(priority=121,description="Verify Disable USB Debugging in Asset Tracking Report")
//	public void VerifyDisableUSBDebuggingInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		reportsPage.clickOnSureLockPermissionShow();
//		reportsPage.ReadingValueInDeviceInfo(reportsPage.UsbDebuggingDisabled);
//		reportsPage.closePopUp();
//	reportsPage.ClickOnReports_Button();
//	accountsettingspage.ClickOnViewReportButton();
//	accountsettingspage.ClickOnRefreshInViewReport();
//	reportsPage.SearchReport("Asset Tracking");
//	accountsettingspage.ClickOnView("Asset Tracking");
//		accountsettingspage.WindowHandle();	
//	quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
//		reportsPage.ReadingValueInReport(50);
//		reportsPage.VerifyColumnInReport("Disable USB Debugging");
//	accountsettingspage.SwitchBackWindow();
//	}
	@Test(priority=122,description="Verify Wi-Fi Hotspot Status in Asset Tracking Report")
	public void VerifyWiFiHotspotStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IsMobileHotSpotEnabled");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(51);
		reportsPage.VerifyColumnInReport("Wi-Fi Hotspot Status");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=123,description="Verify Encryption Status in Asset Tracking Report")
	public void VerifyEncryptionStatusInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("IsEncryptionEnabled");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(52);
		reportsPage.VerifyColumnInReport("Encryption Status");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=124,description="Verify CTS Verified in Asset Tracking Report")
	public void VerifyCTSVerifiedInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("CtsProfileMatch");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(54);
		reportsPage.VerifyColumnInReport("CTS Verified");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=125,description="Verify USB Debugging in Asset Tracking Report")
	public void VerifyUSBDebuggingInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("ADBEnable");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(57);
		reportsPage.VerifyColumnInReport("USB Debugging");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=126,description="Verify IMSI in Asset Tracking Report")
	public void VerifyIMSIInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.readValueOfIMSIFromDeviceInfo();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(15);
		reportsPage.VerifyColumnInReport("IMSI");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=127,description="Verify Default Launcher Application in Asset Tracking Report")
	public void VerifyDefaultLauncherApplicationInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueFromConfig(Config.DefaultLauncherApplication);
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(38);
		reportsPage.VerifyColumnInReport("Default Launcher Application");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=128,description="Verify Registered Date in Asset Tracking Report")
	public void VerifyRegisteredDateInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException
	{
	
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.getStringFromGridAndsplitDate("LastTimeStamp");
		reportsPage.splitTime();
		reportsPage.englishTime();
		reportsPage.joinDateAndTime();
		reportsPage.convertDateTimeFormat();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.getTimeStampFromReport(5);
		reportsPage.VerifyColumnInReport("Registered Date");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=129,description="Verify Last Connected in Asset Tracking Report")
	public void VerifyLastConnectedInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException
	{

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.getStringFromGridAndsplitDate("LastTimeStamp");
		reportsPage.splitTime();
		reportsPage.englishTime();
		reportsPage.joinDateAndTime();
		reportsPage.convertDateTimeFormat();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.getTimeStampFromReport(7);
		reportsPage.VerifyColumnInReport("Last Connected");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=130,description="Verify Device Time in Asset Tracking Report")
	public void VerifyDeviceTimeInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.getStringFromGridAndsplitDate("DeviceTimeStamp");
		reportsPage.splitTime();
		reportsPage.englishTime();
		reportsPage.joinDateAndTime();
		reportsPage.convertDateTimeFormat();
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.getTimeStampFromReport(8);
		reportsPage.VerifyColumnInReport("Device Time");
		accountsettingspage.SwitchBackWindow();
	}
	@Test(priority=131,description="Verify Device Time Zone in Asset Tracking Report")
	public void VerifyDeviceTimeZonelInassetTrackingReport1() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		reportsPage.ReadingValueInDeviceGrid("DeviceTimeZone");
		reportsPage.ClickOnReports_Button();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.ReadingValueInReport(9);
		reportsPage.VerifyColumnInReport("Device Time Zone");
		accountsettingspage.SwitchBackWindow();
		
	}	@Test(priority =132, description = "Download and Verify the data for Asset tracking Report")
	public void VerifyDownloadingAssetTrackingReport() throws InterruptedException
	{
		accountsettingspage.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.SearchReport("Asset Tracking");
		reportsPage.DownloadReport();
		commonmethdpage.ClickOnHomePage();
	}
//	@AfterMethod
//	public void ttt(ITestResult result) throws InterruptedException {
//		if (ITestResult.FAILURE == result.getStatus()) {
//			Utility.captureScreenshot(driver, result.getName());
//			reportsPage.SwitchBackWindow();
//
//		}
//	}
}
