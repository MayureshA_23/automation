package NewUI_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Groups_TestScripts extends Initialization{
	@Test(priority=0,description="Verify delting the Group")
	public void DeletingGroup() throws InterruptedException
	{
		Reporter.log("=====1.Verify delting the Group=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName("Demo123");
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
	}
	@Test(priority=1,description="verify applying job to group.")
	public void ApplyJobToGroup() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("=====2.verify applying job to group.=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		rightclickDevicegrid.VerifyGroupPresence("Automation");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchMultipleDevice(Config.MultipleDeviceName);
		rightclickDevicegrid.SelectMultipleDevice();
		groupspage.RightClickMultipleDevice();
		rightclickDevicegrid.MoveToGroupMultiple();
		newUIScriptsPage.ApplyJobtoGroupBtn();
		androidJOB.SearchField("0TextMessage");
		newUIScriptsPage.clickOnYesBtnInApplyJobToGroup();
		newUIScriptsPage.VerifyOfappliedJobOnGroup("0TextMessage",Config.MultipleDevices2,Config.MultipleDevices1);	
	}
}
