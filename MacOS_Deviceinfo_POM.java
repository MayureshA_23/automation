package PageObjectRepository;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;

public class MacOS_Deviceinfo_POM extends WebDriverCommonLib
{
	 AssertLib ALib=new AssertLib();
	 SoftAssert Soft=new SoftAssert();
	 JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
	 Actions act=new Actions(Initialization.driver);
	 
		@FindBy(xpath="//*[@id='dis-card']/div[3]/p")
	    private WebElement DeviceInfoDeviceModel;
		
		@FindBy(xpath="//*[@id='dis-card']/div[5]/p")
	    private WebElement DeviceInfoDeviceOS;
		
		@FindBy(xpath="//*[@id='dis-card']/div[7]/a")
		private WebElement EditDeviceName;
		
		@FindBy(xpath="//span[@class='editable-clear-x']")
		private WebElement ClearDevicenameButton;
		
		@FindBy(xpath="//input[@id='device_name_input']")
		private WebElement DeviceNameInput;
		
		@FindBy(xpath="//button[@type='submit']")
		private WebElement SaveButton;
		
		@FindBy(xpath="//td[@class='DeviceName']")
		private WebElement DeviceGridDeviceName;
		
		@FindBy(xpath="//*[@class='accor_deviceinfo_wrap cont_wrapper clearfix']/div[9]/p")
		private WebElement DeviceInfoAgrentVersion;
		
		@FindBy(xpath="//*[@id='dis-card']/div[15]/span")
		private WebElement LocationTracingStatus;
		
		@FindBy(xpath="//*[@id='networkinfo-card']/div[3]")
	    private WebElement DeviceInfoMAC;
		
		@FindBy(xpath="//td[@class='DeviceIPAddress']")
		private WebElement DeviceIPInDeviceGrid;
		
		@FindBy(xpath="(//div[@id='networkinfo-card']//p)[1]")
		private WebElement DeviceInfoIP;
		
		@FindBy(xpath="(//div[@id='networkinfo-card']//p)[2]")
		private WebElement DeviceInfoSerialNumber;
		
		@FindBy(xpath="//div[@id='dis-card']//div[11]")
		private WebElement DeviceInfoLastConnectedTime;
		
		@FindBy(xpath="//td[@class='DeviceTimeStamp']")
		private WebElement DeviceGridLastConnectedTime;
		
		@FindBy(xpath="//*[@id='networkinfo-card']/div[7]")
		private WebElement DeviceInfoConnectionStatus;
		
		@FindBy(xpath="//*[@id='dis-card']/div[17]/span")
		private WebElement DeviceInfoProfileStatus;
		
		@FindBy(xpath="//div[@id='storagemem']")
		private WebElement DeviceInfostorageMemory;
		
		 @FindBy(xpath="//td[@class='DeviceName']")
	     private WebElement devicename;
		 
		 @FindBy(xpath="//td[@class='DeviceModelName']")
	     private WebElement devicemodel;
		 
		 @FindBy(xpath="//td[@class='DeviceIPAddress']")
	     private WebElement deviceIP;
		 
		 @FindBy(xpath="//td[@class='DeviceTimeZone']")
	     private WebElement devicetimezone;
		 
		 @FindBy(xpath="//li[@data-action='refresh']")
		 private WebElement RightClickRefresh;
		 
		 @FindBy(xpath="//li[@data-action='remote']")
		 private WebElement RightClickRemote;
		 
		 @FindBy(xpath="//*[@id='gridMenu']/li[5]")
		 private WebElement RightClickMoveToGroup;
		 
		 @FindBy(id="newGroup")
	     private WebElement NewGroup;
	     
	     @FindBy(xpath="(//div[@class='input-group ct-w100']/input[@class='form-control ct-model-input  ct-select-ele'])[1]")
	     private WebElement Groupname;
	     
	     @FindBy(id="groupokbtn")
	     private WebElement CreateGroupOKButton;
	     
	     @FindBy(xpath="//input[@placeholder='Search Devices']")
	 	private WebElement SearchDeviceTextField;
	
	     @FindBy(id="all_ava_devices")
	     private WebElement AllDevices;
	     
	     @FindBy(xpath="(//input[@placeholder='Search By Group Name'])[2]")
	     private WebElement SearchGroupsButton;
	     
	     @FindBy(xpath="//li[@class='list-group-item node-groupList search-result']")
	     private WebElement SearchedGroupName;
	     
	     @FindBy(xpath="//*[@id='groupListModal']/div/div/div[3]/button")
	     private WebElement MoveDeviceButton;
	     
	     @FindBy(xpath="//div[@id='deleteGroup']")
	     private WebElement DeletegroupButton;
	     
	     @FindBy(id="MoveDeviceWhileDeletingGroup")
	     private WebElement ConfirmDeleteButton;
	     
	     @FindBy(xpath="//ul[@id='gridMenu']//li[7]")
	     private WebElement RightClickDelete;
	     
	     @FindBy(xpath="//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	     private WebElement DeleteDeviceButton;
	     
	     @FindBy(xpath="//div[text()='No device available in this group.']")
	 	 private WebElement NotificationOnNoDeviceFound;
	     
	     @FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	     private WebElement SearchDeviceinHomeSection;
	     
	     @FindBy(xpath="//span[text()='Tags']")
	     private WebElement TagsButton;
	     
	     @FindBy(xpath="//*[@id='groupstree']/ul/li[1]")
	     private WebElement HomeButton;
	     
	     @FindBy(xpath="//div[@id='newGroup']")
	     private WebElement AddNewTag;
	     
	     @FindBy(xpath="//*[@id='groupForm']/div/input")
	     private WebElement TagName;
	     
	     @FindBy(xpath="//button[@id='groupokbtn']")
	     private WebElement CreateTagOKButton;
	     
	     @FindBy(xpath="//*[@id='groupsbuttonpanel']/div/ul/li[1]/a")
	     private WebElement GroupButton;
	     
	     @FindBy(xpath="//*[@id='gridMenu']/li[6]")
	     private WebElement RighClickTag;
	     
	     @FindBy(xpath="//*[@id='TagOptMenu']/li[1]")
	     private WebElement AddToTagsButton;
	     
	     @FindBy(xpath="//label[text()=' !!Auto']")
	 	private WebElement CheckboxTag;
	 	
	 	@FindBy(id="tagsDeviceSaveBtn")
	 	private WebElement SaveTag;
	 	
	 	@FindBy(xpath="//*[@id=\"taglistpopup\"]/div/div/div[2]/div/span/div/ul/li[1]/div/input")
	 	private WebElement SearchTag;	    
	 	
	 	@FindBy(xpath="//span[text()='Device(s) added to tag(s) successfully.']")
	 	private WebElement DeviceAddedTagMsg;
	 	
	 	@FindBy(xpath="//li[@data-action='removeTags']")
	 	private WebElement RightClickRemoveTag;
	 	
	 	@FindBy(xpath="//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[1]/div/input")
	     private WebElement RemoveTagSearch;
	 	
	 	@FindBy(xpath="//button[@id='tagsDeviceSaveBtn']")
	 	private WebElement RemoveTagSaveButton;
	 	
	 	@FindBy(xpath="//span[text()='Device(s) removed from tag(s) successfully.']")
	 	private WebElement TagRemovedMsg;
	    
		public void VerifyDeviceModelInDeviceInfoSection()
		  {
			  String ActualModel=DeviceInfoDeviceModel.getText();
			  String ExpectedModel=Config.MacOS_DeviceModel;
			  String PassStatement ="PASS >> Actual Device Model and Expected Device Model are matching";
			  String FailStatement ="FAIL >> Actual Device Model and Expected Device Model are not matching";
			  ALib.AssertEqualsMethod(ActualModel, ExpectedModel, PassStatement, FailStatement);
		  }
		public void VerifyDeviceOSInDeviceInfoSection()
		  {
			  String ActualOS=DeviceInfoDeviceOS.getText();
			  String ExpectedOS=Config.MacOS_DeviceOS;
			  String PassStatement ="PASS >> Actual Device OS and Expected Device OS are matching";
			  String FailStatement ="FAIL >> Actual Device OS and Expected Device OS are not matching";
			  ALib.AssertEqualsMethod(ActualOS, ExpectedOS, PassStatement, FailStatement);	  
		  }
		
		public void VerifyDeviceNameChanginginDeviceInfoSection() throws InterruptedException
		{
			EditDeviceName.click();
			waitForXpathPresent("//button[@type='submit']");
			ClearDevicenameButton.click();
			DeviceNameInput.sendKeys(Config.MacOS_ChangedDeviceName);
			SaveButton.click();
			waitForXpathPresent("//span[text()='Device name changed successfully.']");
			String DeviceNameInGrid = DeviceGridDeviceName.getText();
			if(DeviceNameInGrid.equals(Config.MacOS_ChangedDeviceName))
			{
				Reporter.log(">>>>Device Name is Same As Changed In Device Info Section",true);
			}
			else
			{
				Reporter.log(">>>>Device Name is Different From Changed In Device Info Section",true);
			}
			EditDeviceName.click();
			waitForXpathPresent("//button[@type='submit']");
			sleep(3);
			ClearDevicenameButton.click();
			sleep(5);
			DeviceNameInput.sendKeys(Config.Macos_DeviceName);
			SaveButton.click();			
		}
		public void VerifyAgentVersionInDeviceInfoSection()
		{
			String ActualAgentVersion = DeviceInfoAgrentVersion.getText();
			String ExpectedAgentVersion=Config.MacOS_NixAgentVerson;
			String PassStatement ="PASS >> Actual Agent Version and Expected Agent Version are matching"+" : "+ActualAgentVersion;
			String FailStatement ="FAIL >> Actual Agent version and Expected Agent Version are not matching"+" : "+ActualAgentVersion;
			ALib.AssertEqualsMethod(ActualAgentVersion, ExpectedAgentVersion, PassStatement, FailStatement);		
		}
		public void VerifyLocationTrackingInDeivceInfoSectio()
		{
			String TrackStatus = LocationTracingStatus.getText();	
			boolean flag=TrackStatus.contains("ON");
			String PassStatement="PASS >> Location Tracking Is On";
			String FailStatement="FAIL >> Location Tracking Is"+" : "+TrackStatus;
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		public void VerifyDeviceMACAddressInDeviceInfoSection()
		  {
			  String ActualDeivceMAC=DeviceInfoMAC.getText();
			  String ExpectedDeviceMAC=Config.MacOS_DeviceMacAddress;
			  String PassStatement ="PASS >> Actual Device MAC and Expected Device MAC are matching";
			  String FailStatement ="FAIL >> Actual Device MAc and Expected Device MAc are not matching";
			  ALib.AssertEqualsMethod(ActualDeivceMAC, ExpectedDeviceMAC, PassStatement, FailStatement);
		  }
		public void VerifyDeviceIPAddressInDeivceInfoSection()
		 {
			 boolean flag;
			 String DeviceGridIP=DeviceIPInDeviceGrid.getText();
			 String DeviceinfoIP=DeviceInfoIP.getText();
			 if(DeviceGridIP.equals(DeviceinfoIP))
			 {
				 flag=true;
			 }
			 else
			 {
				 flag=false;
			 }
			 String PassStatement ="PASS >> DeviceGridIP and DeviceinfoIP are matching";
			 String FailStatement ="FAIL >> DeviceGridIP and DeviceinfoIP are not matching";
			 ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
		 }
		public void VerifyDeviceSerialNumberInDeivceInfoSection()
		 {
			 String ActualDeviceSerialNumber=DeviceInfoSerialNumber.getText();
			 String ExpectedDeviceSerialNumber=Config.MacOS_DeviceSerialNumber;
			 String PassStatement ="PASS >> Actual Device Serial Number and Expected Device Serial Number are matching";
			 String FailStatement ="FAIL >> Actual Device Serial Number and Expected Device Serial Number are not matching";
			 ALib.AssertEqualsMethod(ActualDeviceSerialNumber, ExpectedDeviceSerialNumber, PassStatement, FailStatement);
			 
		 }
		public void VerifyDeviceLastConnectedTimeInDeviceinfoSection()
		{
			boolean flag;
			String DeviceInfoTime=DeviceInfoLastConnectedTime.getText();
			String DeviceGridTime = DeviceGridLastConnectedTime.getText();
			if(DeviceInfoTime.equals(DeviceGridTime))
			 {
				 flag=true;
			 }
			 else
			 {
				 flag=false;
			 }
			 String PassStatement ="PASS >> DeviceGrid Last Connected time and Deviceinfo Last Connected time are matching";
			 String FailStatement ="FAIL >> DeviceGrid Last Connected time and Deviceinfo Last Connected time are not matching";
			 ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		public void VerifyDeviceConnectionStatusInDeviceInfoSection()
		{
			String ConnectionStatus = DeviceInfoConnectionStatus.getText();
			if(ConnectionStatus.contains("Secured"))
			{
				Reporter.log("The Device Is under Secured Condition",true);
			}
			else if (ConnectionStatus.contains("Unsecured")) 
			{
			  Reporter.log("The Device Is Not Under Secured Condition",true);	
			}
			else
			{	
				Reporter.log("Device Connection is"+" : "+ConnectionStatus);
				Soft.fail("Device Connection is"+" : "+ConnectionStatus);
				
			}
		}
		public void VerifyDeviceprofilestatusInDeviceInfoSection()
		{
			String ProfileStatus = DeviceInfoProfileStatus.getText();
			boolean flag=ProfileStatus.contains("None");
			String PassStatement ="PASS >> No Profiles are Applied On device";
			String FailStatement ="FAIL >> The Profile Applied On Device Is"+" : "+ProfileStatus;
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);		
		}
		public void VerifyDeviceStorageMemoryInDeviceInfoSection() throws InterruptedException
		{
			String DeviceMemory = DeviceInfostorageMemory.getText();
			boolean flag=DeviceMemory.contains("N/A");
			String PassStatement ="PASS >> Device Available Storage Memory is"+" : "+ DeviceMemory;
			String FailStatement ="FAIL >> Device Available Storage Memory is"+" : "+ DeviceMemory;
			ALib.AssertFalseMethod(flag, PassStatement, FailStatement);		
			sleep(5);
		}
		 
	//	___________________________________________________________________________________________________________________
	//	___________________________________________________________________________________________________________________
		
		 public void VerifyDeviceName()
	     {
	    	 String DeviceGridName = devicename.getText();
	    	 String actual =DeviceGridName;
		   	 Reporter.log(actual);
		   	 String expected = Config.Macos_DeviceName;
		   	 String PassStatement ="PASS >> Actual and Expected values are matching" + " : "+DeviceGridName;
		   	 String FailStatement ="FAIL >> Actual and Expected values are not matching"+" : "+DeviceGridName;
		   	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	     }
		 public void VerifyDeviceModel()
	     {
	    	 String DeviceGridmodel = devicemodel.getText();
	    	 String actual=DeviceGridmodel;
	    	 String expected=Config.MacOS_DeviceModel;
	    	 String PassStatement ="PASS >> Actual and Expected values are matching"+" : "+DeviceGridmodel;
		   	 String FailStatement ="FAIL >> Actual and Expected values are not matching"+" : "+DeviceGridmodel;
		   	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	     }
		 public void verifyDeviceBatteryStatus()
		    {
		    	String BatteryLevel = Initialization.driver.findElement(By.xpath("//td[@class='Battery']")).getText();
		    	boolean value=BatteryLevel.contains("N/A");
		    	
		    	String PassStatement="PASS >>Battery Level is Displayed "+" " +BatteryLevel ;
		   	    String FailStatement="FAIL >>Battery Level is not Displayed";
		   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		    }
		 public void VerifyIPAddress()
	     {
	    	 String DeviceGridIP = deviceIP.getText();
	    	 boolean value = DeviceGridIP.contains(".");
	    	 String PassStatement ="PASS >> IP Address is not empty"+" : "+DeviceGridIP;
		   	 String FailStatement ="FAIL >> IP Address is empty"+" : "+DeviceGridIP;
		   	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		   		
	      }
		 public void verifyDeviceLastConnected()
		    {
		    	String Lastconnectedtime = Initialization.driver.findElement(By.xpath("//td[@class='LastTimeStamp']")).getText();
		    	boolean value=Lastconnectedtime.contains("N/A");
		    	String PassStatement="PASS >>Last Connected Time is Displayed "+" " +Lastconnectedtime ;
		   	    String FailStatement="FAIL >>Last Connected Time is not Displayed";
		   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		    	
		    }
		    public void verifyLastDeviceTime()
		    {
		    	String LastDeviceTime = Initialization.driver.findElement(By.xpath("//td[@class='DeviceTimeStamp']")).getText();
		    	boolean value = LastDeviceTime.contains("N/A");
		    	String PassStatement="PASS >>Device Last Time is Displayed "+" " +LastDeviceTime ;
		   	    String FailStatement="FAIL >>Device Last Time is not Displayed";
		   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		    	
		    }
		    public void verifyDeviceRegistered()
		    {
		    	String DeviceRegisteredTime = Initialization.driver.findElement(By.xpath("//td[@class='DeviceRegistered']")).getText();
		    	boolean value = DeviceRegisteredTime.contains("N/A");
		    	String PassStatement="PASS >>Device Registered Time is Displayed "+" " +DeviceRegisteredTime ;
		   	    String FailStatement="FAIL >>Device Registered Time is not Displayed";
		   	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		    	
		    }
		    
		 public void VerifyDeviceTimeZone()
	     {
	    	 String DeviceGridTImeZone = devicetimezone.getText();
	    	 boolean value = DeviceGridTImeZone.contains("N/A");
	    	 String PassStatement="PASS >> Time Zone is N/A For MacOS Device"+" : "+DeviceGridTImeZone;
	    	 String FailStatement ="FAIL >> Time Zone is" + DeviceGridTImeZone ;
	    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	     }
		 public void VerifyDeviceNetworkOperator()
	     {
	    	WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='Operator']"));
	    	js.executeScript("arguments[0].scrollIntoView()",ele);
	    	String DeviceGridOperator = ele.getText();
	    	boolean value = DeviceGridOperator.contains("N/A");
	    	String PassStatement="PASS >> Network Operator is N/A For MacOS Device"+" : "+DeviceGridOperator;
	   	    String FailStatement ="FAIL >> Network operator" + DeviceGridOperator ;
	        ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	     }
		 public void verifDeviceRoamingStatus()
		   {
			   String DeviceRoamingStatus = Initialization.driver.findElement(By.xpath("//td[@class='PhoneRoaming']")).getText();
			   boolean flag=DeviceRoamingStatus.contains("N/A");
			   String PassStatement="PASS >> Roaming Status is N/A for Windows Device"+" : "+DeviceRoamingStatus;
		  	   String FailStatement="FAIL >> Roaming Status for Windows Device"+DeviceRoamingStatus;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		   }
		   public void verifyDeviceSignalStrength()
		   {
			   String DeviceSignalStrength = Initialization.driver.findElement(By.xpath("//td[@class='PhoneSignal']")).getText();
			   boolean flag=DeviceSignalStrength.contains("N/A");
			   String PassStatement="PASS >> Signal Strength is N/A for Windows Device"+" : "+DeviceSignalStrength;
		  	   String FailStatement="FAIL >> Signal Strength for Windows Device"+DeviceSignalStrength;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		   }
		 public void VerifyDeviceNixAgentVersion()
	     {
	       WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='AgentVersion']"));
	      js.executeScript("arguments[0].scrollIntoView()",ele);
	       String DeviceAgentVersion = ele.getText();
	       String actual=DeviceAgentVersion;
	       String expected=Config.MacOS_NixAgentVerson;
	       String PassStatement ="PASS >> Actual and Expected Nix Version values are matching"+" : "+DeviceAgentVersion;
	  	   String FailStatement ="FAIL >> Actual and Expected Nix Version values are not matching"+" : "+DeviceAgentVersion;
	  	   ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
	     }
		 public void VerifyDeviceOSVersion()
	     {
	    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='ReleaseVersion']"));
	    	 js.executeScript("arguments[0].scrollIntoView()",ele);
	    	 String DeviceOSVersion = ele.getText();
	    	 String actual=DeviceOSVersion;
	    	 String expected=Config.MacOS_DeviceOsInDeviceGrid;
	    	 String PassStatement ="PASS >> Actual and Expected OS Versions are matching"+" : "+DeviceOSVersion;
	    	 String FailStatement ="FAIL >> Actual and Expected OS Versions  are not matching"+" : "+DeviceOSVersion;
	    	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement); 
	     }
		 public void verifyDeviceSureLockVersion()
		   {
			   String DeviceSureLockVersion=Initialization.driver.findElement(By.xpath("//td[@class='SureFoxVersion']")).getText();
			   boolean flag=DeviceSureLockVersion.contains("Not Installed");
			   String PassStatement="PASS >> SureLock is Not Installed for This Device"+" : "+DeviceSureLockVersion;
		  	   String FailStatement="FAIL >> SureLock Version For This Device Is"+DeviceSureLockVersion;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
		   }
		   public void verifyDeviceSureFoxVersion()
		   {
			  String DeviceSureFoxVersion = Initialization.driver.findElement(By.xpath("//td[@class='SureFoxVersion']")).getText();
			  boolean flag=DeviceSureFoxVersion.contains("Not Installed");
			  String PassStatement="PASS >> SureFox is Not Installed for This Device"+" : "+DeviceSureFoxVersion;
		 	   String FailStatement="FAIL >> SureFox Version For This Device Is"+DeviceSureFoxVersion;
		 	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	  
		   }
		   public void verifyDeviceSureVideoVersin()
		   {
			    String DeviceSureVideoVersion = Initialization.driver.findElement(By.xpath("//td[@class='SureVideoVersion']")).getText();
			    boolean flag=DeviceSureVideoVersion.contains("Not Installed");
			    String PassStatement="PASS >> SureVideo is Not Installed for This Device"+" : "+DeviceSureVideoVersion;
			 	String FailStatement="FAIL >> SureVideo Version For This Device Is"+DeviceSureVideoVersion;
			 	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	    
		   }
		   public void verifyDeviceIMEI()
		   {
			   String DeviceIMEI = Initialization.driver.findElement(By.xpath("//td[@class='IMEI']")).getText();
			   boolean flag=DeviceIMEI.contains("N/A");
			   String PassStatement="PASS >> IMEI Is N/A For Windows Device"+" : "+DeviceIMEI;
			   String FailStatement="FAIL >> IMEI For This Device Is"+DeviceIMEI;
			   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		   }
		   public void verifyDeviceIMEI2()
		   {
			   String DeviceIMEI2 = Initialization.driver.findElement(By.xpath("//td[@class='IMEI2']")).getText();
			   boolean flag=DeviceIMEI2.contains("N/A");
			   String PassStatement="PASS >> IMEI2 Is N/A For Windows Device"+" : "+DeviceIMEI2;
			   String FailStatement="FAIL >> IMEI2 For This Device Is"+DeviceIMEI2;
			   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		   }
		   public void VerifyDevicePhoneNumber()
		     {
		    	WebElement ele=Initialization.driver.findElement(By.xpath("//td[@class='PhoneNumber']"));
		        js.executeScript("arguments[0].scrollIntoView()",ele);
		        String DevicePhoneNumber = ele.getText();
		        boolean value = DevicePhoneNumber.contains("N/A");
		        String PassStatement ="PASS >> SIM Card is not supported for this device"+" : "+DevicePhoneNumber;
		   	    String FailStatement ="FAIL >> Device Phone number is "+DevicePhoneNumber;
		   	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		           
		     }
		   public void VerifyDeivceSerialNumber()
		     {
		    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='SerialNumber']"));
		    	 js.executeScript("arguments[0].scrollIntoView()",ele);
		    	 String DeviceSerialNumber = ele.getText();
		    	 String actual=DeviceSerialNumber;
		    	 String expected=Config.MacOS_DeviceSerialNumber;
		    	 String PassStatement ="PASS >> Actual and Expected SerialNumbers are matching"+" : "+DeviceSerialNumber;
		    	 String FailStatement ="FAIL >> Actual and Expected SerialNumbers  are not matching"+" : "+DeviceSerialNumber;
		    	 ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
		     }
		   public void verifyDeviceRootStatus()
		     {
		    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='RootStatus']"));
		    	 js.executeScript("arguments[0].scrollIntoView()",ele);
		    	 String DeviceRootStatus = ele.getText();
		    	 boolean value = DeviceRootStatus.contains("Unknown");
		    	 String PassStatement ="PASS >> Root Status is Unknown for MacOS Device"+" : "+DeviceRootStatus;
		    	 String FailStatement ="FAIL >> Root Status is "+DeviceRootStatus;
		    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		    	 
		     }
		     public void verifyDeviceKnoxStatus()
		     {
		    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='KnoxStatus']"));
		    	 js.executeScript("arguments[0].scrollIntoView()",ele);
		    	 String DeviceKnoxStatus = ele.getText();
		    	 boolean value = DeviceKnoxStatus.contains("N/A");
		    	 String PassStatement ="PASS >> KNOX Status is N/A for MacOS Device"+" : "+DeviceKnoxStatus;
		    	 String FailStatement ="FAIL >> KNOX Status is "+DeviceKnoxStatus;
		    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		     }
		     public void verifyDeviceDataUsage()
		     {
		  	    String DeviceDataUsage = Initialization.driver.findElement(By.xpath("//td[@class='DataUsage']")).getText();
		  	    boolean flag = DeviceDataUsage.contains("Unknown");
		  	    String PassStatement="PASS >> DataUsage is Unknown for This Device"+" : "+DeviceDataUsage;
		  		String FailStatement="FAIL >> DataUsage For This Device Is"+DeviceDataUsage;
		  		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
		     }
		     public void verifyDeviceCPUUsage()
		     {
		    	 String DeviceCPUUsage = Initialization.driver.findElement(By.xpath("//td[@class='CpuUsage']")).getText();
		    	 boolean flag=DeviceCPUUsage.contains("N/A");
		    	 String PassStatement="PASS >> CPU Usage is N/A for This Device"+" : "+DeviceCPUUsage;
			     String FailStatement="FAIL >> CPU Usage For This Device Is"+DeviceCPUUsage;
			     ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		    	 
		     }
		     public void VerifyDeviceGPUUsage()
		     {
		    	 String DeviceGPUUsage = Initialization.driver.findElement(By.xpath("//td[@class='GpuUsage']")).getText();
		    	 boolean flag=DeviceGPUUsage.contains("N/A");
		    	 String PassStatement="PASS >> GPU Usage is N/A for This Device"+" : "+DeviceGPUUsage;
			     String FailStatement="FAIL >> GPU Usage For This Device Is"+DeviceGPUUsage;
			     ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyDeviceBatteryTemperature()
		     {
		  	   String DeviceBatteryTemperature = Initialization.driver.findElement(By.xpath("//td[@class='Temperature']")).getText();
		  	   boolean flag=DeviceBatteryTemperature.contains("Unknown");
		  	   String PassStatement="PASS >> Battery Temperature of This Device"+" : "+DeviceBatteryTemperature;
		  	   String FailStatement="FAIL >> Battery Temperature of This Device Is N/A";
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);  	   
		     }
		     
		     public void verifyDevicesupervisedStatus()
		     {
		  	   String DevicesupervisedStatus = Initialization.driver.findElement(By.xpath("//td[@class='IsSupervised']")).getText();
		  	   boolean flag=DevicesupervisedStatus.contains("N/A");
		  	   String PassStatement="PASS >> Supervised Status of This Device is"+" : "+DevicesupervisedStatus;
		  	   String FailStatement="FAIL >> Supervised Status of This Device is"+" : "+DevicesupervisedStatus;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);   
		     }
		     public void verifyDeviceEnrollmentStatus()
		     {
		  	   String DevicEnrollmentStatus = Initialization.driver.findElement(By.xpath("//td[@class='Isenrolled']")).getText();
		  	   boolean flag=DevicEnrollmentStatus.contains("Yes");
		  	   String PassStatement="PASS >> Enrollment Status of This Device is"+" : "+DevicEnrollmentStatus;
		  	   String FailStatement="FAIL >> Enrollment Status of This Device is"+" : "+DevicEnrollmentStatus;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
		     }
		     public void verifyDevicePollingMechanism()
		     {
		    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='NixPollingType']"));
		         js.executeScript("arguments[0].scrollIntoView()",ele);
		    	 String DevicePollingStatus = ele.getText();
		    	 boolean value = DevicePollingStatus.contains("APNS");
		    	 String PassStatement="PASS >> Polling Mechanism Status is APNS for MacOS Device"+" : "+DevicePollingStatus;
		    	 String FailStatement="FAIL >> Polling Mechanism Status for MacOS Device"+DevicePollingStatus;
		    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		     }
		     public void verifyNetworkType()
		     {
		  	   String DeviceNetworkType = Initialization.driver.findElement(By.xpath("//td[@class='NetworkType']")).getText();
		  	   boolean flag=DeviceNetworkType.contains("Unknown");
		  	   String PassStatement="PASS >> NetworkType of This Device is"+" : "+DeviceNetworkType;
		  	   String FailStatement="FAIL >> NetworkType of This Device is"+" : "+DeviceNetworkType;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);  
		     }
		     public void verifyUsername()
		     {
		  	   String DeviceUserName = Initialization.driver.findElement(By.xpath("//td[@class='DeviceUserName']")).getText();
		  	   boolean flag=DeviceUserName.contains("N/A");
		  	   String PassStatement="PASS >> UserName of This Device is"+" : "+DeviceUserName+" : "+"Normal Enrollment";
		  	   String FailStatement="FAIL >> UserName of This Device is"+" : "+DeviceUserName+" : "+"Other Enrollment";
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);    
		     }
		     public void VerifyGPSStatus()
		     {
		    	 String GPSStatus = Initialization.driver.findElement(By.xpath("//td[@class='GPSEnabled']")).getText();
		    	 boolean flag=GPSStatus.contains("N/A");
		    	 String PassStatement="PASS >> GPS Status of This Device is"+" : "+GPSStatus;
			  	 String FailStatement="FAIL >> GPS Status of This Device is"+" : "+GPSStatus;
			  	 ALib.AssertTrueMethod(flag, PassStatement, FailStatement);   
		    	 
		    	 
		     }
		     public void verifySimSerialNumber()
		     {
		  	   String DeviceSimSerialNumber = Initialization.driver.findElement(By.xpath("//td[@class='SimSerialNumber']")).getText();
		  	   boolean flag=DeviceSimSerialNumber.contains("N/A");
		  	   String PassStatement="PASS >> SimSerialNumber of This Device is"+" : "+DeviceSimSerialNumber;
		  	   String FailStatement="FAIL >> SimSerialNumber of This Device is"+" : "+DeviceSimSerialNumber;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
		     }
		     public void verifyBluetoothStatus()
		     {
		    	 String DeviceBlueToothStatus = Initialization.driver.findElement(By.xpath("//td[@class='BluetoothEnabled']")).getText();
		    	 boolean flag=DeviceBlueToothStatus.contains("N/A");
		    	 String PassStatement="PASS >> Bluetooth Status of This Device is"+" : "+DeviceBlueToothStatus;
			  	 String FailStatement="FAIL >> Bluetooth Status of This Device is"+" : "+DeviceBlueToothStatus;
			  	 ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		    	 
		     }
		     public void verifyUSBStatus()
		     {
		  	   String DeviceUSBStatus = Initialization.driver.findElement(By.xpath("//td[@class='USBPluggedIn']")).getText();
		  	   boolean flag = DeviceUSBStatus.contains("N/A");
		  	   String PassStatement="PASS >> USB Status of This Device is"+" : "+DeviceUSBStatus;
		  	   String FailStatement="FAIL >> USB Status of This Device is"+" : "+DeviceUSBStatus;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	   
		     }
		     public void verifyBluetoothSSID()
		     {
		  	   String DeviceBluetoothSSID = Initialization.driver.findElement(By.xpath("//td[@class='BSSID']")).getText();
		  	   boolean flag = DeviceBluetoothSSID.contains("N/A");
		  	   String PassStatement="PASS >> Bluetooth SSID of This Device is"+" : "+DeviceBluetoothSSID;
		  	   String FailStatement="FAIL >> Bluetooth SSID of This Device is"+" : "+DeviceBluetoothSSID;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifySurelockSettingsIdentifier()
		     {
		  	   String SurelockSettingsIdentifier = Initialization.driver.findElement(By.xpath("//td[@class='SureLockSettingsVersionCode']")).getText();
		  	   boolean flag = SurelockSettingsIdentifier.contains("N/A");
		  	   String PassStatement="PASS >> SurelockSettingsIdentifier of This Device is"+" : "+SurelockSettingsIdentifier;
		  	   String FailStatement="FAIL >> SurelockSettingsIdentifier of This Device is"+" : "+SurelockSettingsIdentifier;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyDeviceOSBuildNumber()
		     {
		  	   String verifyDeviceOSBuildNumber = Initialization.driver.findElement(By.xpath("//td[@class='OsBuildNumber']")).getText();
		  	   boolean flag = verifyDeviceOSBuildNumber.contains("N/A");
		  	   String PassStatement="PASS >> OS BuildNumber of This Device is"+" : "+verifyDeviceOSBuildNumber;
		  	   String FailStatement="FAIL >> OS BuildNumber of This Device is"+" : "+verifyDeviceOSBuildNumber;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyDeviceFreestorageMemeory()
		     {
		     	String DeviceFreeStorage = Initialization.driver.findElement(By.xpath("//td[@class='MemoryStorageAvailable']")).getText();
		     	boolean value = DeviceFreeStorage.contains("N/A");
		     	String PassStatement="PASS >>Device Free Storage is Displayed "+" " +DeviceFreeStorage ;
		    	String FailStatement="FAIL >>Device Free Storage is not Displayed"+" : "+DeviceFreeStorage;
		    	ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		     }
		     public void verifyDeviceProgramMemory()
		     {
		     	String DeviceProgramStorage = Initialization.driver.findElement(By.xpath("//td[@class='PhysicalMemoryAvailable']")).getText();
		     	boolean value=DeviceProgramStorage.contains("N/A");
		     	String PassStatement="PASS >>Device Program Memory is"+" " +DeviceProgramStorage ;
		    	String FailStatement="FAIL >>Device Program Memory is"+" s"+DeviceProgramStorage;
		    	ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		     	
		     }
		     public void verifyBluetoothName()
		     {
		    	 String BluetooothName = Initialization.driver.findElement(By.xpath("//td[@class='RealDeviceName']")).getText();
		    	 boolean value=BluetooothName.contains("N/A");
		    	 String PassStatement="PASS >>Bluetooth Name Of This Device is"+" " +BluetooothName ;
			     String FailStatement="FAIL >>Bluetooth Name Of This Device is"+" : "+BluetooothName;
			     ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		    	 
		     }
		     
		     public void verifySecurityPatchDate()
		     {
		  	   String SecurityPatchDate = Initialization.driver.findElement(By.xpath("//td[@class='SecurityPatchDate']")).getText();
		  	   boolean flag = SecurityPatchDate.contains("N/A");
		  	   String PassStatement="PASS >> SecurityPatchDate of This Device is"+" : "+SecurityPatchDate;
		  	   String FailStatement="FAIL >> SecurityPatchDate of This Device is"+" : "+SecurityPatchDate;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyAndriodEnterpriseStatus()
		     {
		  	   String AndroidEnterpriseStatus = Initialization.driver.findElement(By.xpath("//td[@class='AfwProfile']")).getText();
		  	   boolean flag = AndroidEnterpriseStatus.contains("N/A");
		  	   String PassStatement="PASS >> AndroidEnterpriseStatus of This Device is"+" : "+AndroidEnterpriseStatus;
		  	   String FailStatement="FAIL >> AndroidEnterpriseStatus of This Device is"+" : "+AndroidEnterpriseStatus;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyLastTimeSync()
		     {
		  	   String LastTimeSync = Initialization.driver.findElement(By.xpath("//td[@class='MTPSystemScanTimeStamp']")).getText();
		  	   boolean flag = LastTimeSync.contains("N/A");
		  	   String PassStatement="PASS >> LastTimeSync of This Device is"+" : "+LastTimeSync;
		  	   String FailStatement="FAIL >> LastTimeSync of This Device is"+" : "+LastTimeSync;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifythreatCount()
		     {
		  	   String threatCount = Initialization.driver.findElement(By.xpath("//td[@class='MTPSystemScanThreatCount']")).getText();
		  	   boolean flag = threatCount.contains("N/A");
		  	   String PassStatement="PASS >> ThreatCount of This Device is"+" : "+threatCount;
		  	   String FailStatement="FAIL >> ThreatCount of This Device is"+" : "+threatCount;
		  	   ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyDeviceWifiStatus()
		     {
		    	 WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='IsMobileHotSpotEnabled']"));
		    	 js.executeScript("arguments[0].scrollIntoView()",ele);
		    	 String DeviceWiFistatus = ele.getText();
		         boolean value = DeviceWiFistatus.contains("N/A");
		         String PassStatement="PASS >> WiFiHotspot Status is N/A for Windows Device";
		    	 String FailStatement="FAIL >> WiFiHotspot Status for Windows Device"+DeviceWiFistatus;
		    	 ALib.AssertTrueMethod(value, PassStatement, FailStatement);  
		     }
		     public void verifyEncryptionStatus()
		     {
		   	  String EncryptionStatus = Initialization.driver.findElement(By.xpath("//td[@class='IsEncryptionEnabled']")).getText();
		   	  boolean flag = EncryptionStatus.contains("YES");
		   	  String PassStatement="PASS >> EncryptionStatus of This Device is"+" : "+EncryptionStatus;
		   	  String FailStatement="FAIL >> EncryptionStatus of This Device is"+" : "+EncryptionStatus;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyCTSVerifiedStatus()
		     {
		   	  String CTSVerifiedStatus = Initialization.driver.findElement(By.xpath("//td[@class='CtsProfileMatch']")).getText();
		   	  boolean flag = CTSVerifiedStatus.contains("N/A");
		   	  String PassStatement="PASS >> CTSVerifiedStatus of This Device is"+" : "+CTSVerifiedStatus;
		   	  String FailStatement="FAIL >> CTSVerifiedStatus of This Device is"+" : "+CTSVerifiedStatus;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyPlatformIntegrity()
		     {
		   	  String PlatformIntegrity = Initialization.driver.findElement(By.xpath("//td[@class='BasicIntegrity']")).getText();
		   	  boolean flag = PlatformIntegrity.contains("N/A");
		   	  String PassStatement="PASS >> PlatformIntegrity of This Device is"+" : "+PlatformIntegrity;
		   	  String FailStatement="FAIL >> PlatformIntegrity of This Device is"+" : "+PlatformIntegrity;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);	  
		     }
		     public void verifyThreatProtection()
		     {
		   	  String ThreatProtection = Initialization.driver.findElement(By.xpath("//td[@class='VerifyAppEnable']")).getText();
		   	  boolean flag = ThreatProtection.contains("N/A");
		   	  String PassStatement="PASS >> ThreatProtection of This Device is"+" : "+ThreatProtection;
		   	  String FailStatement="FAIL >> ThreatProtection of This Device is"+" : "+ThreatProtection;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);  
		     }
		     public void verifyUSBEnableStatus()
		     {
		   	  String USBEnableStatus = Initialization.driver.findElement(By.xpath("//td[@class='ADBEnable']")).getText();
		   	  boolean flag = USBEnableStatus.contains("N/A");
		   	  String PassStatement="PASS >> USBEnableStatus of This Device is"+" : "+USBEnableStatus;
		   	  String FailStatement="FAIL >> USBEnableStatus of This Device is"+" : "+USBEnableStatus;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void veriyUnknownSourceApps()
		     {
		   	  String UnknownSourceAppsStatus = Initialization.driver.findElement(By.xpath("//td[@class='AllowUnknownSource']")).getText();
		   	  boolean flag = UnknownSourceAppsStatus.contains("N/A");
		   	  String PassStatement="PASS >> UnknownSourceAppsStatus of This Device is"+" : "+UnknownSourceAppsStatus;
		   	  String FailStatement="FAIL >> UnknownSourceAppsStatus of This Device is"+" : "+UnknownSourceAppsStatus;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		     }
		     public void verifyDeviceLocalIP()
		     {
		   	  String DeviceLocalIP = Initialization.driver.findElement(By.xpath("//td[@class='DeviceLocalIPAddress']")).getText();
		   	  boolean flag = DeviceLocalIP.contains("N/A");
		   	  String PassStatement="PASS >> DeviceLocalIP of This Device is"+" : "+DeviceLocalIP;
		   	  String FailStatement="FAIL >> DeviceLocalIP of This Device is"+" : "+DeviceLocalIP;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		   	  
		     }
		     public void verifyWifiSSID()
		     {
		   	  String WifiSSID = Initialization.driver.findElement(By.xpath("//td[@class='WifiSSID']")).getText();
		   	  boolean flag=WifiSSID.contains("N/A");
		   	  String PassStatement="PASS >> WifiSSID of This Device is"+" : "+WifiSSID;
		   	  String FailStatement="FAIL >> WifiSSID of This Device is"+" : "+WifiSSID;
		   	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		   	  js.executeScript("arguments[0].scrollIntoView()",devicename);
		   	  
		     }
		//--------------------RIGHT CLICK--------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------
		     public void RightClickRefreshForMacOS()
		     {
		    	 act.contextClick(devicename).perform();
		    	 waitForXpathPresent("//li[@data-action='refresh']");
		    	 RightClickRefresh.click();
		     }
		     public void RightClickRemoteForMacOs() throws InterruptedException
		     {
		    	 act.contextClick(devicename).perform();
		    	 waitForXpathPresent("//li[@data-action='refresh']");
		    	 RightClickRemote.click();
		     }
		     
		     public void ClickOnNewGroup() throws InterruptedException
		     {
		   	  NewGroup.click();
		   	  waitForXpathPresent("(//div[@class='input-group ct-w100']/input[@class='form-control ct-model-input  ct-select-ele'])[1]");
		   	  sleep(3);
		     }
		     
		     public void SendingGroupName(String GroupName) throws InterruptedException
		     {
		   	  Groupname.sendKeys(GroupName);
		   	  CreateGroupOKButton.click();
		   	  sleep(3);
		    }
		     public void ClickOnAllDeviceTab() throws InterruptedException
		     {
		    	 AllDevices.click();
				  waitForXpathPresent("//td[@class='DeviceName']");
				  sleep(3);
		     }
		     public void MovingDeviceToGroup() throws InterruptedException
		     { 
		   	  AllDevices.click();
			  waitForXpathPresent("//td[@class='DeviceName']");
			  SearchDeviceTextField.clear();
			  sleep(2);
		      SearchDeviceTextField.sendKeys(Config.Macos_DeviceName);
			  waitForXpathPresent("//p[text()='"+Config.Macos_DeviceName+"']");
		   	  sleep(4);	   	  
		      act.contextClick(devicename).perform();
		      waitForXpathPresent("//li[@data-action='refresh']");
		      sleep(2);
		      RightClickMoveToGroup.click();
		      sleep(2);
		      SearchGroupsButton.sendKeys("AATestGroupPath");
			  waitForXpathPresent("//ul/li[@class='list-group-item node-groupList']");
			  sleep(5);
			  SearchedGroupName.click();
			  sleep(3);
			  MoveDeviceButton.click();	
			  sleep(8);
			  waitForXpathPresent("//p[text()='"+Config.Macos_DeviceName+"']");
			  sleep(3);
		     }
			  public void VerifyingDeviceGroupPath() throws InterruptedException
			  {
			  WebElement ele = Initialization.driver.findElement(By.xpath("//td[@class='DeviceGroupPath']"));
			  js.executeScript("arguments[0].scrollIntoView()",ele);
			  sleep(5);
			  String ActualGroupPath = Initialization.driver.findElement(By.xpath("//td[@class='DeviceGroupPath']")).getText();
			  String ExpectedGroupPath=Config.MacOS_RightClickGroupPath;
			  String PassStatement="PASS >> GroupPath is Same As Expeced Group Path"+" "+"RightClick Move To Group Working Fine";
			  String FailStatement="FAIL >> GroupPath is Different From Expected GroupPath"+" "+"RightClick Move To Group isn't Working Fine";	  
			  ALib.AssertEqualsMethod(ExpectedGroupPath, ActualGroupPath, PassStatement, FailStatement);
			  }
			  public void DeletingTheGroup() throws InterruptedException
			  {
			  DeletegroupButton.click();
			  waitForidPresent("MoveDeviceWhileDeletingGroup");
			  sleep(4);
			  ConfirmDeleteButton.click();
			  waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
			  sleep(10);		  
		   }
		     
		   public void ClickOnTags() throws InterruptedException
		   {
			   TagsButton.click();
			   waitForXpathPresent("//div[@id='newGroup']");
		       sleep(4);
		   }
		   
		   public void ClickOnAddTagPlusButton() throws InterruptedException
		   {
			   AddNewTag.click();
		       waitForXpathPresent("//button[@id='groupokbtn']");
		       sleep(2);
		   }
		   
		   public void EnterTagName(String tagName)
		   {
			   TagName.sendKeys(tagName);
		   }
		   
		   public void ClickOnOKButtonTags() throws InterruptedException
		   {
			   CreateTagOKButton.click();
			   waitForidPresent("deleteDeviceBtn");
               sleep(5);
		   }
		    
		    public void RightClickTags() throws InterruptedException
		    {
		    	js.executeScript("arguments[0].scrollIntoView()",devicename);
		    	sleep(2);
		    }
		    
		    public void ClickOnGroups() throws InterruptedException
		    {
		    	
		    	GroupButton.click();
		    	waitForidPresent("deleteDeviceBtn");
				sleep(6);
		    }
		    public void RightClickingOnDevice() throws InterruptedException
		    {
		    	act.contextClick(devicename).perform();
				sleep(2);
				
		    }
		    public void ClickingOnTags() throws InterruptedException
		    {
		    	RighClickTag.click();
				act.moveToElement(AddToTagsButton).perform();
				sleep(2);
		    }
		    public void ClickingOnAddToTags() throws InterruptedException
		    {
				AddToTagsButton.click();
				waitForXpathPresent("(//h4[text()='Tag List'])[1]");
				sleep(4);
		    }
		    public void AddingDeviceToTag(String tagname) throws InterruptedException
		    {
		    	SearchTag.sendKeys(tagname);
				sleep(2);
				CheckboxTag.click();
				sleep(3);
				SaveTag.click();
		    }
		    public void VerifyingDeviceAddedToTag() throws InterruptedException
		    {
		    	waitForXpathPresent("//span[text()='Device(s) added to tag(s) successfully.']");
				sleep(4);
				boolean flag;
				if(DeviceAddedTagMsg.isDisplayed())
				{
					flag=true;
				}
				else
				{
					flag=false;
				}
				String PassStatement="PASS >> Device Added To Respective Tag Successfully" ;
			   	String FailStatement="FAIL >> Device Not Added To Tag";
			   	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		    }
		    public void VerifyingDeviceInsideTag() throws InterruptedException
		    {
		    	TagsButton.click();
		    	waitForXpathPresent("//div[@id='newGroup']");
		    	sleep(3);
		    	
		    	waitForXpathPresent("//p[text()='"+Config.Macos_DeviceName+"']");
		    	if(Initialization.driver.findElement(By.xpath("//p[text()='"+Config.Macos_DeviceName+"']")).isDisplayed())
		    	{
		    		Reporter.log("PASS >>Device Is Present Inside The Respective Tag Added",true);
		    	}
		    	else
		    	{
		    		Reporter.log("FAIL >>Device Isn't Present Inside The Respective Tag Added",true);
		    	}
		    	
		    }
		    	
		    			
		      
		      public void RightClickRemoveTag() throws InterruptedException
		      {
		    	waitForXpathPresent("//*[@id='groupsbuttonpanel']/div/ul/li[1]/a");
		    	sleep(5);
		    	GroupButton.click();
		    	SearchDeviceTextField.sendKeys(Config.Macos_DeviceName);
		    	SearchDeviceTextField.clear();
		    	sleep(2);
		    	SearchDeviceTextField.sendKeys(Config.Macos_DeviceName);
		    	waitForXpathPresent("//p[text()='"+Config.Macos_DeviceName+"']");
				sleep(7);
				act.contextClick(devicename).perform();
				sleep(2);
				RighClickTag.click();
				act.moveToElement(RightClickRemoveTag).perform();
				sleep(2);
				RightClickRemoveTag.click();
				waitForXpathPresent("//button[@id='tagsDeviceSaveBtn']");
				sleep(4);
				RemoveTagSearch.sendKeys("!!Auto");
				sleep(2);
				RemoveTagSaveButton.click();		   
				waitForXpathPresent("//span[text()='Device(s) removed from tag(s) successfully.']");
				sleep(2);
				if(TagRemovedMsg.isDisplayed())
				{
					Reporter.log("PASS >>Tags Applied On The Device Has Been Removed",true);
				}
				else
				{
					Reporter.log("FAIL >>Tags Applied On The Device Hasn't Been Removed",true);
				}
		      }
		     public void ConfirmingDeletingDeviceFromRightClick() throws InterruptedException
		     {
		    	
		    	 RightClickDelete.click();
		    	 waitForXpathPresent("//p[text()='Do you wish to continue?']");
		    	 sleep(2);
		    	 DeleteDeviceButton.click();
		    	 Reporter.log("PASS >> Device Has Been Deleted Successfully",true);
		     }
		     public void VerifyingDeletedDevicePresenceInsideHomeSection() throws InterruptedException
		     {
		    	 
		    	 while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed())
		    	 {}
		    	 SearchDeviceTextField.sendKeys(Config.Macos_DeviceName);
		    	 SearchDeviceTextField.clear();
		    	 sleep(2);
		    	 SearchDeviceTextField.sendKeys(Config.Macos_DeviceName);
		    	 boolean value;
		    	 waitForXpathPresent("//div[text()='No device available in this group.']");
		    	 try
		    	 {
		    		 NotificationOnNoDeviceFound.isDisplayed();
		    		 value=true;
		    	 }
		    	 catch(Exception e)
		    	 {
		    		 value=false;
		    	 }
		    	 String passStatement="PASS >> Deleted Device is not present Inside AllDevice group"+"  "+"Right Click Delete is Working Fine";
		    	 String failStatement="FAIL >> Deleted Device is present Inside AllDevice group"+"  "+"Right Click Delete isn't Working Fine";
		    	 ALib.AssertTrueMethod(value, passStatement, failStatement);   	  
		     }		
}
