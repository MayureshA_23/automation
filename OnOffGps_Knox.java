package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class OnOffGps_Knox extends Initialization{
	@Test(priority=1, description="To Turn OFF Gps") 
	
	public void ToTurnOFFGps() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.clickOnTurnOFF_GPS();
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName(Config.RunScriptOffGPSJob);
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.RunScriptOffGPSJob);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.RunScriptOffGPSJob, Config.TimeToDeployRunscriptJob);
		runScript.ScrollStatusbarOnDeviceSide();
		runScript.checkGpsIsTurnedOff();
		androidJOB.ClickOnHomeButtonDeviceSide();
	}	
	@Test(priority=2, description="To Turn ON Gps") 
	public void ToTurnONGps() throws InterruptedException, IOException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.ClickOnTurnOnGPS();
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("TurnONGps");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("TurnONGps");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.RunScriptOnGPSJob, Config.TimeToDeployRunscriptJob);
		runScript.ScrollStatusbarOnDeviceSide();
		runScript.checkGPSisTurnedON();
		androidJOB.ClickOnHomeButtonDeviceSide();
	}

}
