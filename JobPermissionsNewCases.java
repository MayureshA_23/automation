package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

// One Folder Should be present in Job Section inside SuperUser account
//"donotdelete" Job should be present in Job section inside SuperUser
public class JobPermissionsNewCases extends Initialization {
	
	
	@Test(priority='1',description="1.Verify selecting \"Move jobs to folder\" and assigning to sub user")
	public void TC_ST_661() throws InterruptedException{
	Reporter.log("\n1.Verify selecting \"Move jobs to folder\" and assigning to sub user",true);
	usermanagement.ClickOnJobAndCreateJobFolder("DonotdeleteFolder");
	commonmethdpage.ClickOnSettings();
	usermanagement.ClickOnUserManagementOption();
	usermanagement.VerifyUserManagementIsDisplayed();
	usermanagement.InputRolesDescription(Config.RoleSelectJobPermission,"UserRoles" );
	usermanagement.EnableJobPermissionRole_Movejobstofolder();
	usermanagement.CreateDemoUser1(Config.InputUser4,Config.pswd,Config.pswd,Config.RoleSelectJobPermission);
	commonmethdpage.ClickOnSettings();
	usermanagement.logoutAsAdmin();
	loginPage.ClickOnLoginAgainLink();
	usermanagement.LoginAsNewUser(Config.InputUser4,Config.pswd);
	androidJOB.clickOnJobs();
	androidJOB.SearchJobInSearchField(Config.JobNameUM);
	usermanagement.SelectTheJob();                      //donotdelete
	usermanagement.ClickOnjob_moveToFolder();
	usermanagement.ClickOnFolder();                  //DonotdeleteFolder
	usermanagement.ClickOnOkbTnMoveToFolder();
	}
	
	@Test(priority='2',description="2.Verify Deselecting \"Move jobs to folder\" and assigning to sub user")
	public void TC_ST_662() throws InterruptedException{
	Reporter.log("\n2.Verify Deselecting \"Move jobs to folder\" and assigning to sub user",true);
	commonmethdpage.ClickOnSettings();
	usermanagement.logoutAsAdmin();
	loginPage.ClickOnLoginAgainLink();
	usermanagement.LoginAsNewUser(Config.userID, Config.password);
	commonmethdpage.ClickOnSettings();
	usermanagement.ClickOnUserManagementOption();
	usermanagement.VerifyUserManagementIsDisplayed();
	usermanagement.ClickOnRoles();
	usermanagement.SearchRole(Config.RoleSelectJobPermission);
	usermanagement.SelectCreatedRoleJobPermission();
	usermanagement.ClickOnEdit();
	usermanagement.DisableJobPermissionRole_Movejobstofolder();
	commonmethdpage.ClickOnSettings();
	usermanagement.logoutAsAdmin();
	loginPage.ClickOnLoginAgainLink();
	usermanagement.LoginAsNewUser(Config.InputUser4,Config.pswd);
	androidJOB.clickOnJobs();
	androidJOB.SearchJobInSearchField(Config.JobNameUM);
	usermanagement.SelectTheJob();                      //donotdelete
	usermanagement.VerifyAccessMeesage_OnClickOnNewFolder();
	usermanagement.DeleteFolder("DonotdeleteFolder");	
	}
	
	@Test(priority='3')
	public void DelteCreatedUsersAndRole() throws InterruptedException{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnUserTab();
		usermanagement.DeletingExistingUser(Config.InputUser4);
		usermanagement.ClickOnRoles();
		usermanagement.DeletingExistingRole(Config.RoleSelectJobPermission);

	}	
}