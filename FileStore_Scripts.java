package FileStore_TestScripts;

import java.io.IOException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class FileStore_Scripts extends Initialization {
	//Updated Cases
	// Precondition: Add files to be upload in D:\UplaodFiles\FileStore
	// * Add Astro contact in App store 
     @Test(priority = 0, description = "upload Files in File store")
	  public void uploadFilesFileStore() throws InterruptedException, Throwable {
		
		filestorepage.ClickOnFileStore();
		filestorepage.NamingFolderWhileCreation("FileSharing");

		filestorepage.NamingFolderWhileCreation("MultiFile");
		filestorepage.DoubleClickOperationOnRenamedFolder("MultiFile");
		filestorepage.ClickOnUploadFile(Config.FileStore_imagefile);
		filestorepage.VerifyOfUploadedFile(Config.FileStore_ImageName);

		filestorepage.ClickOnUploadFile(Config.FileStore_audiofile);
		filestorepage.VerifyOfUploadedFile(Config.FileStore_AudioName);


		filestorepage.ClickOnUploadFile(Config.FileStore_videofile);
		filestorepage.VerifyOfUploadedFile(Config.FileStore_VideoName);
		filestorepage.ClickOnFileStoreDroppable();

		filestorepage.ClickOnUploadFile(Config.FileStore_imagefile);
		filestorepage.VerifyOfUploadedFile(Config.FileStore_ImageName);
		filestorepage.VerifyOfSize(Config.FileStore_ImageName,Config.FileStore_ImageSize);

		filestorepage.ClickOnUploadFile(Config.FileStore_videofile);
		filestorepage.VerifyOfUploadedFile(Config.FileStore_VideoName);
		filestorepage.VerifyOfSize(Config.FileStore_VideoName, Config.FileStore_VideoSize);

		filestorepage.ClickOnUploadFile(Config.FileStore_audiofile);
		filestorepage.VerifyOfUploadedFile(Config.FileStore_AudioName);
		filestorepage.VerifyOfSize(Config. FileStore_AudioName, Config.FileStore_AudioSize); 

		filestorepage.ClickOnUploadFile(Config.FileStore_docfile);
		filestorepage.VerifyOfUploadedFile(Config. FileStore_DocName);
		filestorepage.VerifyOfSize(Config. FileStore_DocName, Config.FileStore_DocSize); 

		filestorepage.ClickOnUploadFile(Config.FileStore_APKfile);
		filestorepage.VerifyOfUploadedFile(Config.FileStore_APKfileName);
		filestorepage.VerifyOfSize(Config.FileStore_APKfileName,Config.FileStore_ApkSize);

		filestorepage.ClickOnFileStore();

	}

	@Test(priority = 1, description = "File Store-File Sharing policy _Android")
	public void VerifyFileStoreSharing() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("FileSharing");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FileSharingProfile");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FileSharingProfile");
		commonmethdpage.Selectjob("FileSharingProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
        androidJOB.CheckStatusOfappliedInstalledJob("FileSharingProfile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("FileSharing");
		Reporter.log("PASS>>File Store-File Sharing policy _Android", true);
	}

	@Test(priority = 2, description = "File Store_Verifying download+File Store_ Verify delete file or folder")
	public void VerifyFileStoreVerifyDownload() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.Select_IMG();
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FilestoreprofileImage");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FilestoreprofileImage");
		commonmethdpage.Selectjob("FilestoreprofileImage");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
        androidJOB.CheckStatusOfappliedInstalledJob("FilestoreprofileImage",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		profilesAndroid.verifyFolderOnDevice("Shared");
		filestorepage.isImagePresent();
		filestorepage.clickOnDownloadButton();
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		profilesAndroid.verifyFolderIsDownloadedInDevice("Image.jpg");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		profilesAndroid.LongPressOnFolder("Image.jpg");
		filestorepage.clickOnDelete();
		filestorepage.clickOnDeleteOKBUTTON();
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
	//	rightclickDevicegrid.LaunchFileManager();
		profilesAndroid.verifyFolderNotInDevice("Image.jpg");
		Reporter.log("PASS>>File Store_Verifying download", true);
	}
	 

	@Test(priority = 3, description = "File Store_Verify downloading video files")
	public void VerifyFileStoreVerifyVideoDownload() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.select_video();
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("Filestoreprofilevideo");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("Filestoreprofilevideo");
		commonmethdpage.Selectjob("Filestoreprofilevideo");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("Filestoreprofilevideo",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		profilesAndroid.verifyFolderOnDevice("Shared");
		filestorepage.isVideoPresent();
		filestorepage.clickOnDownloadButton();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		profilesAndroid.verifyFolderIsDownloadedInDevice("Video.mp4");
		Reporter.log("PASS>>File Store_Verify downloading video files", true);
	}

	@Test(priority = 4, description = "File Store-Verify downloading audio files+File Store_Deselecting file or folder")
	public void VerifyFileStoreVerifyAudioDownload() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectingAudio_File();
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FilestoreprofileAudio");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FilestoreprofileAudio");
		commonmethdpage.Selectjob("FilestoreprofileAudio");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("FilestoreprofileAudio",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		profilesAndroid.verifyFolderOnDevice("Shared");
		profilesAndroid.LongPressOnFolder("Audio.mp3");
		filestorepage.clickOnCancle();                            // changed
		filestorepage.VerifyDownloadbuttonIsNotPresent();
		filestorepage.isAudioPresent();
		filestorepage.clickOnDownloadButton();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		profilesAndroid.verifyFolderIsDownloadedInDevice("Audio.mp3");
		Reporter.log("PASS>>File Store-Verify downloading audio files+File Store_Deselecting file or folder", true);
	}

	@Test(priority = 5, description = "File Store-Verify downloading document files")
	public void VerifyFileStoreVerifyDocDownload() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectingXl_file();
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("Filestoreprofile Doc");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("Filestoreprofile Doc");
		commonmethdpage.Selectjob("Filestoreprofile Doc");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("Filestoreprofile Doc",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		profilesAndroid.verifyFolderOnDevice("Shared");
		filestorepage.isDocPresent();
		filestorepage.clickOnDownloadButton();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		profilesAndroid.verifyFolderIsDownloadedInDevice("File.xlsx");
		Reporter.log("PASS>>File Store-Verify downloading document files", true);
	}

	//combined two cases
	@Test(priority = 6, description = "File Store-Verify downloading apk files + File Store_Verify status")
	public void VerifyFileStoreVerifyApkDownload() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectApk();
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FilestoreprofileAPK");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FilestoreprofileAPK");
		commonmethdpage.Selectjob("FilestoreprofileAPK");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
       androidJOB.CheckStatusOfappliedInstalledJob("FilestoreprofileAPK",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		profilesAndroid.verifyFolderOnDevice("Shared");
		filestorepage.isApkpresent();
		filestorepage.clickOnDownloadButton();
		filestorepage.isProgressBarDisplayed();
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		profilesAndroid.verifyFolderIsDownloadedInDevice("Game.apk");
		Reporter.log("PASS>>File Store-Verify downloading apk files + File Store_Verify status", true);
	}

	@Test(priority = 7, description = "File Store-Verify downloading files from nested folders+File Store-Opening downloaded files")
	public void VerifyFileStorenestedfolders() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("MultiFile");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FilestoreprofileMultiFile");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FilestoreprofileMultiFile");
		commonmethdpage.Selectjob("FilestoreprofileMultiFile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
        androidJOB.CheckStatusOfappliedInstalledJob("FilestoreprofileMultiFile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		profilesAndroid.verifyFolderOnDevice("Shared");
		profilesAndroid.verifyFolderOnDevice("MultiFile");
		profilesAndroid.OnDeviceOnClickOnFolder("MultiFile");
		profilesAndroid.verifyFolderOnDevice("Audio.mp3");
		profilesAndroid.verifyFolderOnDevice("Image.jpg");
		profilesAndroid.verifyFolderOnDevice("Video.mp4");
		filestorepage.ClickOnbackButton();
		profilesAndroid.LongPressOnFolder("MultiFile");
		filestorepage.clickOnDownloadButton();
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		profilesAndroid.verifyFolderIsDownloadedInDevice("MultiFile");
		Reporter.log("PASS>>File Store-Verify downloading files from nested folders+File Store-Opening downloaded files", true);
	}

	@Test(priority = 8, description = "File Store_Verify search")
	public void VerifyFileStoreSearch() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		filestorepage.SearchFolder("Shared");
		profilesAndroid.verifyFolderOnDevice("Shared");
		Reporter.log("PASS>>File Store_Verify search", true);
	}

	@Test(priority = 9, description = "File Store_Verify removing file from selected items")
	public void VerifyRemoveFile() throws InterruptedException, Throwable {
		Reporter.log("File Store_Verify removing file from selected items", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("MultiFile");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.RemoveSelectedFile("MultiFile");
		profilesAndroid.VerifyFileAddedIsremoved("MultiFile");
		Reporter.log("PASS>>File Store_Verify removing file from selected items",true);
	}

	@Test(priority = 'A', description = "File Store_Verify Deleting file message")
	public void VerifyDeletingFileMessage() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
//		profilesAndroid.ClickOnProfiles(); 
//		profilesAndroid.AddProfile();
//		profilesAndroid.clickOnFileSharingPolicy();
//		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("MultiFile");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.CliCkOnRemoveButton();
		profilesAndroid.verifyRemoveMessage();
		Reporter.log("PASS>>File Store_Verify Deleting file message", true);
	}

	@Test(priority = 'B', description = "File Store_Verify User and Shared folder is not displayed in profile.")
	public void VerifyUserAndShareFolderNotPresent() throws InterruptedException, Throwable {
		Reporter.log("File Store-File Sharing policy _Android", true);
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.verifyShareAnduserFolderNotPresent();
		profilesAndroid.ClickOnRemoveFileTransferButton();

		Reporter.log("PASS>>File Store_Verify User and Shared folder is not displayed in profile.", true);
	}


	@Test(priority='C',description="File Store_apply App store profile on File store profile.")
	public void ApplyAppStoreOnFileStore() throws Throwable {
//    	profilesAndroid.ClickOnProfiles(); 
//		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("ApplicationProfile");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("ApplicationProfile");
		commonmethdpage.Selectjob("ApplicationProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("ApplicationProfile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("MultiFile");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FilestoreprofileMultiFile");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FilestoreprofileMultiFile");
		commonmethdpage.Selectjob("FilestoreprofileMultiFile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		androidJOB.CheckStatusOfappliedInstalledJob("FilestoreprofileMultiFile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStoreIsNotPresent();
		Reporter.log("PASS>>File Store_ Apply File Store profile on app store profile",true); }

	@Test(priority='D',description="File Store_ Apply File Store profile on app store profile")
	public void ApplyFileStoreOnAppStore() throws Throwable {
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("MultiFile");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("FilestoreprofileMultiFile");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FilestoreprofileMultiFile");
		commonmethdpage.Selectjob("FilestoreprofileMultiFile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		androidJOB.CheckStatusOfappliedInstalledJob("FilestoreprofileMultiFile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("Home");
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("ApplicationProfile");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("ApplicationProfile");
		commonmethdpage.Selectjob("ApplicationProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		androidJOB.CheckStatusOfappliedInstalledJob("ApplicationProfile",600);

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderNotOnDevice("Home");
		Reporter.log("PASS>>File Store_apply App store profile on File store profile.",true); 
		}

	@Test(priority='E',description="File Store_Applying app store and file store together")
	public void ApplyAppStoreFileStoreTogether() throws Throwable {
		profilesAndroid.ClickOnProfiles(); 
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.clickOnFileSharingPolicy();
		profilesAndroid.ClickOnFileSharingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
		profilesAndroid.SelectFolder("MultiFile");
		profilesAndroid.ClickOnAddBottonAfterselectingfile();
		profilesAndroid.ClickOnDone();
		profilesAndroid.clickOnProfileTextfield("AppStoreFileStoreTogether");
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("AppStoreFileStoreTogether");
		commonmethdpage.Selectjob("AppStoreFileStoreTogether");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
        androidJOB.CheckStatusOfappliedInstalledJob("AppStoreFileStoreTogether",600);
        commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
		filestorepage.LaunchFileStore();
		profilesAndroid.verifyFolderOnDevice("MultiFile");
		Reporter.log("PASS>>File Store_Applying app store and file store together",true); 
		}



	//*******************************NEW CASES*************************

@Test(priority='F',description="Verify filestore by adding files in the sub-sub folders")
public void TC_FS_091() throws Throwable {

	filestorepage.ClickOnFileStore();
	filestorepage.ClickOnSharedDroppable();

	filestorepage.NamingFolderWhileCreation("Folder1");
	filestorepage.DoubleClickOperationOnRenamedFolderSubGroup("Folder1");
	filestorepage.NamingFolderWhileCreation("Folder2");

	filestorepage.DoubleClickOperationOnRenamedFolderSubGroup("Folder2");


	filestorepage.ClickOnUploadFile(Config.FileStore_imagefile);
	filestorepage.VerifyOfUploadedFile(Config.FileStore_ImageName);
	filestorepage.VerifyOfSize(Config.FileStore_ImageName,Config.FileStore_ImageSize);
	
	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");

	filestorepage.LaunchFileStore();
	profilesAndroid.verifyFolderOnDevice("Home");
	profilesAndroid.verifyFolderOnDevice("Shared");
	profilesAndroid.verifySharedFolderOnDevice("Shared");
	profilesAndroid.verifySharedFolderOnDevice("Folder1");
	profilesAndroid.verifySharedFolderOnDevice("Folder2");
	filestorepage.isImagePresent();
	filestorepage.clickOnDownloadButton();
	commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
	profilesAndroid.verifyFolderIsDownloadedInDeviceSUBfolders("Image.jpg");
	Reporter.log("PASS>>File Store-File Sharing policy _Android", true);


}

@Test(priority='G',description="Verify creating multiple folders inside new folder")
public void TC_FS_093() throws Throwable {

	Reporter.log("File Store-Verify creating multiple folders inside new folder", true);

	filestorepage.ClickOnFileStore();
	filestorepage.ClickOnFile_Store();
	filestorepage.NamingFolderWhileCreation("NewFolder01");
	filestorepage.DoubleClickOperationOnRenamedFolderSubGroup("NewFolder01");
	filestorepage.NamingFolderWhileCreation("NewFolder02");
	filestorepage.NamingFolderWhileCreation("NewFolder03");
	filestorepage.NamingFolderWhileCreation("NewFolder04");

	Reporter.log("PASS>>File Store-Verify creating multiple folders inside new folder", true);


}
@Test(priority='H',description="Verify downloading files from nested folders")
public void TC_FS_094() throws Throwable {

	Reporter.log("File Store-Verify downloading files from nested folders", true);
	filestorepage.ClickOnFile_Store();
    filestorepage.NamingFolderWhileCreation("NestedFolder01");
	filestorepage.DoubleClickOperationOnRenamedFolderSubGroup("NestedFolder01");
	filestorepage.NamingFolderWhileCreation("NestedFolder02");
	filestorepage.DoubleClickOperationOnRenamedFolderSubGroup("NestedFolder02");
	filestorepage.NamingFolderWhileCreation("NestedFolder03");
	filestorepage.DoubleClickOperationOnRenamedFolderSubGroup("NestedFolder03");
	filestorepage.NamingFolderWhileCreation("NestedFolder04");
	filestorepage.DoubleClickOperationOnRenamedFolderSubGroup("NestedFolder04");


	filestorepage.ClickOnUploadFile(Config.FileStore_imagefile);
	filestorepage.VerifyOfUploadedFile(Config.FileStore_ImageName);
	filestorepage.VerifyOfSize(Config.FileStore_ImageName,Config.FileStore_ImageSize);

	filestorepage.ClickOnFile_Store();
    profilesAndroid.ClickOnProfiles(); 
	profilesAndroid.AddProfile();
	profilesAndroid.clickOnFileSharingPolicy();
	profilesAndroid.ClickOnFileSharingPolicyConfigButton();
	profilesAndroid.VerifyClickingOnFilsharingPolicyAddButtton();
	profilesAndroid.SelectFolder("NestedFolder01");
	profilesAndroid.ClickOnAddBottonAfterselectingfile();
	profilesAndroid.ClickOnDone();
	profilesAndroid.clickOnProfileTextfield("NestedFolderFileStore");
	profilesAndroid.ClickOnSaveButton();
	profilesAndroid.NotificationOnProfileCreated();
	commonmethdpage.ClickOnHomePage(); 
	commonmethdpage.SearchDevice();
	commonmethdpage.ClickOnApplyButton();
	commonmethdpage.SearchJob("NestedFolderFileStore");
	commonmethdpage.Selectjob("NestedFolderFileStore");
	commonmethdpage.ClickOnApplyButtonApplyJobWindow();
	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

    androidJOB.CheckStatusOfappliedInstalledJob("NestedFolderFileStore",600);

	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.efss.splashscreen.EFSSSplashScreen");
	filestorepage.LaunchFileStore();
	profilesAndroid.verifyFolderOnDevice("Home");
	profilesAndroid.verifyFolderOnDevice("Shared");
	profilesAndroid.verifySharedSubFolderOnDevice("NestedFolder01");
	profilesAndroid.verifySharedSubFolderOnDevice("NestedFolder02");
	profilesAndroid.verifySharedSubFolderOnDevice("NestedFolder03");
	profilesAndroid.verifySharedSubFolderOnDevice("NestedFolder04");

	filestorepage.isImagePresent();
	filestorepage.clickOnDownloadButton();
	commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
   profilesAndroid.verifyNESTEDFolderIsDownloadedInDevice("Image.jpg");
   
	profilesAndroid.ClickOnProfiles();
	appstorepage.DeleteCreatedProfile("FileSharingProfile");
	appstorepage.DeleteCreatedProfile("FilestoreprofileImage");
	appstorepage.DeleteCreatedProfile("Filestoreprofilevideo");
	appstorepage.DeleteCreatedProfile("FilestoreprofileAudio");
	appstorepage.DeleteCreatedProfile("Filestoreprofile Doc");
	appstorepage.DeleteCreatedProfile("FilestoreprofileAPK");
	appstorepage.DeleteCreatedProfile("FilestoreprofileMultiFile");
	appstorepage.DeleteCreatedProfile("ApplicationProfile");
	appstorepage.DeleteCreatedProfile("AppStoreFileStoreTogether");
	appstorepage.DeleteCreatedProfile("NestedFolderFileStore");


    Reporter.log("PASS>>File Store-Verify downloading files from nested folders", true);

}


	
}






