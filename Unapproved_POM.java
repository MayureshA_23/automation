package PageObjectRepository;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class Unapproved_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(id="unapprovedSection")
	private WebElement UnapprovedSectionButton;
	
	@FindBy(id="uaApproveBtn")
	private WebElement ApproveDeviceButton;
	
	@FindBy(id="uaApproveBtn")
	private WebElement UnapproveRefreshButton;
	
	@FindBy(id="uaDeleteBtn")
	private WebElement UnapprovedDeviceDeleteButton;
	
	@FindBy(xpath="//div[contains(@class,'unapproved_pg')]/div/div/div/input")
	private WebElement UnapprovedSearchButton;
	
	@FindBy(xpath="//table[@id='unapprovedGrid']/thead/tr/th/div[text()='Device Name']")
	private WebElement UnapprovedDeviceNameColumn;
	
	@FindBy(xpath="//table[@id='unapprovedGrid']/thead/tr/th/div[text()='Model']")
	private WebElement UnapprovedModelColumn;
	
	@FindBy(xpath="//table[@id='unapprovedGrid']/thead/tr/th/div[text()='Platform']")
	private WebElement UnapprovedPlatformColumn;
	
	@FindBy(xpath="//table[@id='unapprovedGrid']/thead/tr/th/div[text()='Last Connected']")
	private WebElement UnapprovedLastConnectedColumn;
	
	@FindBy(xpath="//span[text()='Please select a device.']")
	private WebElement ApproveErrorMeesage;
	
	@FindBy(xpath="//span[text()='Device(s) approved successfully.']")
	private WebElement ApproveMeesage;
	
	@FindBy(xpath="//span[text()='Device(s) deleted successfully.']")
	private WebElement DeleteMeesage;
	
	@FindBy(id="preapprovedSection")
	private WebElement PreapprovedSectionButton;
	
	@FindBy(id="preapprove_all")
	private WebElement AutomaticallyApproveAllDeviceCheckbox;
	
	@FindBy(id="paImportBtn")
	private WebElement ImportButton;
	
	@FindBy(id="paDeleteBtn")
	private WebElement PreapprovedDeleteButton;
	
	@FindBy(xpath="//span[text()='Device(s) deleted successfully.']")
	private WebElement DeleteUnApprovedDeviceMessage;
	
	public boolean CheckUnapprovedSectionButton()
	{
		boolean value = true;
		try{
			UnapprovedSectionButton.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}

	public void VerifyUnapprovedSectionButton()
	{
		boolean value=CheckUnapprovedSectionButton();
		String PassStatement = "PASS >> 'Unapproved Section' is displayed successfully when logged into SureMDM Console";
		String FailStatement = "Fail >> 'Unapproved Section' as is not displayed even when logged into SureMDM Console";	
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void ClickOnUnapproved() throws InterruptedException
	{
		UnapprovedSectionButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForidPresent("uaDeleteBtn");
		sleep(3);
		Reporter.log("Clicked on Unapproved section",true);
	}
	
	public void VerifyOfUnapprovedUI()
	{
		WebElement[] UnapprovedElement={ApproveDeviceButton,UnapproveRefreshButton,UnapprovedDeviceDeleteButton,UnapprovedSearchButton,
				UnapprovedDeviceNameColumn,UnapprovedModelColumn,UnapprovedPlatformColumn,UnapprovedLastConnectedColumn};
		String[] ElementsText={"Approve Device Button","Unapprove Refresh Button","Unapproved DeviceDelete Button","Unapproved Search TextBox",
				"Unapproved DeviceName Column","Unapproved Model Column","Unapproved Platform Column","Unapproved Last Connected Column"};
		for(int i=0;i<UnapprovedElement.length;i++)
		{
		    boolean value=UnapprovedElement[i].isEnabled();
		    String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on Unapproved";
		    String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on Unapproved";
		    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}
	
	
	public boolean VerifyOfApproveDeviceButtonEnabled()
	{
		String ActualValue=ApproveDeviceButton.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		return value;
	}
	
	public boolean VerifyOfUnapprovedDeviceDeleteButtonEnabled()
	{
		String ActualValue=UnapprovedDeviceDeleteButton.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		return value;
	}
	
	public void SelectDevice(String DeviceName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//table[@id='unapprovedGrid']/tbody/tr/td[text()='"+DeviceName+"']")).click();
		sleep(2);
	}
	
	public void ClickOnApprove()
	{
		ApproveDeviceButton.click();
	}
	
	public void ErrorMessage_Approve() throws Throwable
	{
		String PassStatement="PASS >> Error Message : 'Please select a device.' is displayed successfully when clicked on Approve Device without selecting any device";
		String FailStatement="Fail >> Error Message : 'Please select a device.' is not displayed even when clicked on Approve Device without selecting any device";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ApproveErrorMeesage, true, PassStatement, FailStatement,2);
	}
	
	public void ConfirmationMessage_Approve() throws Throwable
	{
		String PassStatement="PASS >> Confirmation Message : 'Device(s) approved successfully.' is displayed successfully when clicked on Approve Device with selecting any device";
		String FailStatement="Fail >> Confirmation Message : 'Device(s) approved successfully.' is not displayed even when clicked on Approve Device with selecting any device";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ApproveMeesage, true, PassStatement, FailStatement,5);
	}
	
	public boolean VerifyOfDevice(String DeviceName)
	{
		boolean value=true;
		try{
		Initialization.driver.findElement(By.xpath("//td[@class='DeviceName']/p[text()='"+DeviceName+"']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	
	public void VerifyOfApproveDevice(String DeviceName)
	{
		boolean value=VerifyOfDevice(DeviceName);
		String PassStatement = "PASS >> Device is Approved succesfully when clicked on Approve Device and Present in Home Page";
		String FailStatement = "Fail >> Device is not Approved even when clicked on Approve Device";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void ClickOnDelete()
	{
		UnapprovedDeviceDeleteButton.click();
	}
	
	public void ErrorMessage_delete() throws Throwable
	{
		String PassStatement="PASS >> Error Message : 'Please select a device.' is displayed successfully when clicked on Delete Device without selecting any device";
		String FailStatement="Fail >> Error Message : 'Please select a device.' is not displayed even when clicked on Delete Device without selecting any device";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ApproveErrorMeesage, true, PassStatement, FailStatement,2);
	}
	
	public boolean VerifyOfDeleteDevice_Unapproved(String DeviceName)
	{
		boolean value=true;
		try{
		Initialization.driver.findElement(By.xpath("//table[@id='unapprovedGrid']/tbody/tr/td[text()='"+DeviceName+"']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	
	public void VerifyOfDeleteDevice_Home(String DeviceName)
	{
		boolean value=VerifyOfDevice(DeviceName);
		String PassStatement = "PASS >> Device is deleted succesfully when clicked on Delete Device button of unapproved section and not present in Home Page";
		String FailStatement = "Fail >> Device is not deleted even when clicked on Delete Device button of unapproved section and not present in Home Page ";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void ConfirmationMessage_Delete() throws Throwable
	{
		String PassStatement="PASS >> Confirmation Message : 'Device(s) deleted successfully.' is displayed successfully when clicked on Approve Device with selecting any device";
		String FailStatement="Fail >> Confirmation Message : 'Device(s) deleted successfully.' is not displayed even when clicked on Approve Device with selecting any device";
		Initialization.commonmethdpage.ConfirmationMessageVerify(DeleteMeesage, true, PassStatement, FailStatement,2);
	}
	
	public void EnterSearchUnapproved(String Search)
	{
		UnapprovedSearchButton.sendKeys(Search);
	}
	
	public void ClearSearchUnapproved()
	{
		UnapprovedSearchButton.clear();
	}
	
	public void VerifyOfSearch_DeviceName(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//table[@id='unapprovedGrid']/tbody/tr/td[text()='"+Search+"']")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" DeviceName is Searched succesfully in Unapproved Page when searched with Device Name";
		String FailStatement="FAIL >> "+Search+" DeviceName is not Searched in User Management Page when searched with Device Name";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfSearch_Model(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//table[@id='unapprovedGrid']/tbody/tr/td[text()='"+Search+"']")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" Model is Searched succesfully in Unapproved Page when searched with Model Name";
		String FailStatement="FAIL >> "+Search+" Model is not Searched in User Management Page when searched with Model Name";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfSearch_Platforms(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//table[@id='unapprovedGrid']/tbody/tr/td[text()='"+Search+"']")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" Platform is Searched succesfully in Unapproved Page when searched with Platform Name";
		String FailStatement="FAIL >> "+Search+" Platform is not Searched in User Management Page when searched with Platform Name";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifydeletingUnapprovedDevice()
	{
		WebElement SearchBox = Initialization.driver.findElement(By.xpath("(//input[@class='form-control'])[5]"));
		SearchBox.sendKeys(Config.Unapproved_DeleteDevice);
		boolean mark;
		if(Initialization.driver.findElement(By.xpath("//td[text()='"+Config.Unapproved_DeleteDevice+"']")).isDisplayed())
		{
			Initialization.driver.findElement(By.xpath("//td[text()='"+Config.Unapproved_DeleteDevice+"']")).click();
		}
		UnapprovedDeviceDeleteButton.click();
		waitForXpathPresent("//span[text()='Device(s) deleted successfully.']");
		if(DeleteUnApprovedDeviceMessage.isDisplayed())
		{
			mark=true;
		}
		else
		{
			mark=false;
		}
		String PassStatement="PASS >> "+"Unapproved Device Is Deleted Successfully";
		String FailStatement="FAIL >> "+"Unapproved Device Is Not Deleted";
		ALib.AssertTrueMethod(mark, PassStatement, FailStatement);
		
	}
		
	
		
	
	
	public void VerifyOfSearch_LastConnected(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//table[@id='unapprovedGrid']/tbody/tr/td[text()='"+Search+"']")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" LastConnected time is Searched succesfully in Unapproved Page when searched with LastConnected time";
		String FailStatement="FAIL >> "+Search+" DeviceName is not Searched in User Management Page when searched with LastConnected time";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	
	
	
	
	
	public void ClickOnPreapproved() throws InterruptedException
	{
		PreapprovedSectionButton.click();
		waitForidPresent("paImportBtn");
		sleep(2);
		Reporter.log("Clicked on Preapproved section",true);
	}
	
	public boolean VerifyOfAutomaticallyApproveAllDeviceCheckboxEnabled()
	{
		boolean value;
		try{
			String ActualValue=AutomaticallyApproveAllDeviceCheckbox.getAttribute("disabled");
		  	 value=ActualValue.contains("");
		  	}catch(Exception e)
		  	{
		  		value=false;
		  	}
		return value;
	}
	
	public boolean VerifyOfPreapprovedDeleteButtonEnabled()
	{
		String ActualValue=PreapprovedDeleteButton.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		return value;
	}
	
	public void ClickOnImport_UM() throws AWTException, InterruptedException
	{
		ImportButton.click();
	}
	
	public void ClickOnImport() throws InterruptedException, IOException
	{
		ImportButton.click();
		sleep(5);
		Runtime.getRuntime().exec("C:\\Users\\sandhyas1792\\Desktop\\UplaodFiles\\Preapproved\\File.exe");
		sleep(7);
	}
	
}
