package Settings_TestScripts;


import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

import Common.Initialization;

public class Plugins extends Initialization {

	@Test(priority=1 ,description="Verify that Plugin Store is present Under Plugin Section") 
	public void VerifythatPluginStoreispresentUnderPluginSectionTC_ST_566() throws Throwable {
		Reporter.log("\n1.Verify that Plugin Store is present Under Plugin Section",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnPluginsButton();
		accountsettingspage.PluginStoreVisible();
		accountsettingspage.ClickOnPluginsStoreButton();			
}
    
	@Test(priority=2 ,description="Show all the approved Plugins in Plugin Store") 
	public void ShowalltheapprovedPluginsinPluginStoreTC_ST_567() throws Throwable {
		Reporter.log("\n2.Show all the approved Plugins in Plugin Store",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnPluginsButton();
		accountsettingspage.PluginStoreVisible();
		accountsettingspage.ClickOnPluginsStoreButton();	
		accountsettingspage.PluginRenameD();
		accountsettingspage.PluginGroupCreator();
		accountsettingspage.PluginGroupAssignmentWizard();
		accountsettingspage.PluginFixTheNameVisible();
		//accountsettingspage.PluginExpoJobVisible();
		//accountsettingspage.PluginColumnifyVisible();	
}
   
	@Test(priority=3 ,description="Search Bar in Plugin store") 
	public void SearchBarinPluginstoreTC_ST_568() throws Throwable {
		Reporter.log("\n3.Search Bar in Plugin store",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnPluginsButton();
		accountsettingspage.PluginStoreVisible();
		accountsettingspage.ClickOnPluginsStoreButton();	
		accountsettingspage.PluginStoreSearchBarVisible();		
}
	
	@Test(priority=4,description="Search Plugin by Plugin name under search bar") 
	public void SearchPluginbyPluginnameundersearchbarTC_ST_569() throws Throwable {
		Reporter.log("\n4.Search Plugin by Plugin name under search bar",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnPluginsButton();
		accountsettingspage.PluginStoreVisible();
		accountsettingspage.ClickOnPluginsStoreButton();	
		accountsettingspage.PluginStoreSearchBar();
		accountsettingspage.PluginSearchNameMatch();
}
	
	@Test(priority=5,description="Plugin details Pop-up Displayed") 
	public void PlugindetailsPopupDisplayedTC_ST_570() throws Throwable {
		Reporter.log("\n5.Plugin details Pop-up Displayed",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnPluginsButton();
		accountsettingspage.PluginStoreVisible();
		accountsettingspage.ClickOnPluginsStoreButton();	
		accountsettingspage.ClickonMoreDetails();
		accountsettingspage.PluginDetailsPopupVisible();
}
	
	@Test(priority=6,description="Install button on the Plugin") 
	public void InstallbuttononthePluginTC_ST_571() throws Throwable {
		Reporter.log("\n6.Install button on the Plugin ",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnPluginsButton();
		accountsettingspage.PluginStoreVisible();
		accountsettingspage.ClickOnPluginsStoreButton();	
		accountsettingspage.InstallNowButtonVisible();
		

}
	

	
	
}
