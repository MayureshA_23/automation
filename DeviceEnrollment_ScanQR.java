package DeviceEnrollment;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceEnrollment_ScanQR extends Initialization
{
		//SureLock Should Not Be Installed In The Device

	
	/*@Test(priority=0,description="Creating QR Code")
	public void CreatingQRCode() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		deviceEnrollment.verifyClickingOnDeviceEnrollment();
		DeviceEnrollment_SCanQR.OpeningQRCode(Config.QrCodeName);
		commonmethdpage.ClickOnHomePage();
	}*/
	/*@Test(priority=1,description="Enrolling Device Manually Using AccountID")
     public void EnrollingDeviecUsingAccId() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException
     {
		 commonmethdpage.ClickOnSettings();
		 commonmethdpage.ClickonAccsettings();
    	 DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
    	 DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
    	 DeviceEnrollment_SCanQR.ClickOnApplyButton();
    	 commonmethdpage.ClickOnHomePage();
    	 androidJOB.AppiumConfigurationforNIX();  
    	 DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
    	 DeviceEnrollment_SCanQR.ClickOnContinueButton();
    	 DeviceEnrollment_SCanQR.SendingAccountID(Config.AccountId);
    	 DeviceEnrollment_SCanQR.ClickOnRegisterButton();
    	 DeviceEnrollment_SCanQR.SelectingDeviceNametype();
    	 DeviceEnrollment_SCanQR.SendingDeviceName(Config.Enrolled_DeviceName);
    	 DeviceEnrollment_SCanQR.ClickOnSetDeviceName();
    	 DeviceEnrollment_SCanQR.clicOnGridRefresh();
    	 commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceName);
    	 PendingDelete.clickOnDeleteDevice();
    	 DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);
    	 DeviceEnrollment_SCanQR.ClikOnNixSettings();
    	 DeviceEnrollment_SCanQR.ClickingOnImportExportSettings();
    	 DeviceEnrollment_SCanQR.ClickingOnResetSettings();
    	 DeviceEnrollment_SCanQR.ClickOnDoneInNix();
    	 DeviceEnrollment_SCanQR.ClickOnDoneInNix();
     }*/


	@Test(priority=2,description="Enrolling Device USing QR Code")
	public void EnrollingDeviceUsingQR() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_NoAuthentication();
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");  
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceIMEI);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();

	}

	@Test(priority=3,description="Enrolling Device With Require Password And ADA Checked")
	public void EnrollingDeviceWithAuthPassword_EnablingADA() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		DeviceEnrollment_SCanQR.SettingPassword(Config.AuthPassword);
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		DeviceEnrollment_SCanQR.SendPasswordForAuthentication_ADAChecked(Config.AuthPassword);
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceIMEI);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
	}

	/////////////
	@Test(priority=4,description="Verify Android devices enrolment with Require Password and Include password with QR Code by enabling Advanced Device Authentication check box.")
	public void VerifyingDeviceEnrollment_IncludePassWordWithQR_Enabled() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		DeviceEnrollment_SCanQR.SettingPassword(Config.AuthPassword);
		DeviceEnrollment_SCanQR.EnablingIncludingPasswordwithQR();
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		DeviceEnrollment_SCanQR.SendPasswordForAuthentication_ADAChecked(Config.AuthPassword);
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceIMEI);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
	}
	@Test(priority=5,description="Verify Android/iOS devices enrolment with Require Password by enabling Advanced Device Authentication check box.Note:Include Password option should be disabled.")
	public void VerifyingDeviceEnrollment_IncludePassWordWithQR_Disabled() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		DeviceEnrollment_SCanQR.SettingPassword(Config.AuthPassword);
		DeviceEnrollment_SCanQR.DisablingIncludePasswordWithQR();
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm"); 
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		DeviceEnrollment_SCanQR.SendPasswordForAuthentication_ADAChecked(Config.AuthPassword);
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceIMEI);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
	}
	@Test(priority=6,description="Verify Android/iOS device Scan QR enrolment without any Authentication by disabling Advanced Device Authentication check box")
	public void EnrollingDeviceWithoutAnyAuthDisablingADA_ScanQR() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_NoAuthentication();
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm"); 
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceIMEI);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);	
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();	    
	}
	@Test(priority=7,description="Verify Android/iOS device enrolment with Require Password by disabling Advanced Device Authentication check box.Note:Include Password checkbox should be disabled.")
	public void EnrollingDeviceWithPasswordByDisablingADA_DisablingIncludePassWordWithQR_ScanQR() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		DeviceEnrollment_SCanQR.SettingPassword(Config.AuthPassword);
		DeviceEnrollment_SCanQR.DisablingIncludePasswordWithQR();
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		DeviceEnrollment_SCanQR.SendPasswordForAuthentication_ADAUnchecked(Config.AuthPassword);
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceIMEI);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
	}
	@Test(priority=8,description="Verify Android/iOS device enrolment with Require Password and Include password with QR Code by disabling Advanced Device Authentication check box.")
	public void EnrollingDeviceWithPasswordByDisabelingADA_EnabelingIncludePassWordWithQR_ScanQR() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		DeviceEnrollment_SCanQR.SettingPassword(Config.AuthPassword);
		DeviceEnrollment_SCanQR.EnablingIncludingPasswordwithQR();
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		DeviceEnrollment_SCanQR.ClickOnEnrollment();
		
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.Enrolled_DeviceIMEI);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);	
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();	    
	}
	@Test(priority=9,description="Verify device enrolment UI with OAuth Authentication by enabling Advanced Device Authentication check box.")
	public void VerifyEnrolMentUIWithOauthByEnablingADA() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		DeviceEnrollment_SCanQR.VerifyOAthUIByEnablingADA();
	}

	//############ MDM URL Should Be Whitelisted In Google API Console###############//
	@Test(priority='A',description="Verify Android device enrolment with GSUITE OAuth Authentication by enabling Advanced Device Authentication check box.")
	public void VerifyAndroidEnrolmentWithGSUITEOAuthByEnablingADA() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		DeviceEnrollment_SCanQR.SendingAuthEndpoint(Config.AuthEndpoint);
		DeviceEnrollment_SCanQR.SendingTokenEndpoint(Config.TokenEndpoint);
		DeviceEnrollment_SCanQR.SendingClientID(Config.ClientID);
		DeviceEnrollment_SCanQR.SendingClientSecret(Config.ClientSecret);
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		deviceEnrollment.verifyClickingOnDeviceEnrollment(); 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		DeviceEnrollment_SCanQR.SendingEmailIDForGsuite(Config.OAuthGmailID);
		DeviceEnrollment_SCanQR.ClickingOnNextButton_GSuite();
		DeviceEnrollment_SCanQR.SendingPasswordForGsuite(Config.OAuthGmailPassword);
		DeviceEnrollment_SCanQR.ClickingOnNextButton_GSuite();
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.OAuthUserName);
		DeviceEnrollment_SCanQR.VerifyingEnrolledDeviceUserNameInConsole(Config.OAuthUserName);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);	
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();	    
	}

	//############ MDM URL Should Be Whitelisted In AZURE Portal ###############//

	@Test(priority='B',description="Verify Android device enrolment with Azure AD OAuth Authentication by enabling Advanced Device Authentication check box.")
	public void VerifyAndroidEnrolmentWithAzureADOAuthByEnablingADA() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		DeviceEnrollment_SCanQR.SendingAuthEndpoint(Config.OAuthEndpoint_Azureportal);
		DeviceEnrollment_SCanQR.SendingTokenEndpoint(Config.OAuthTokenEndpoint_Azureportal);
		DeviceEnrollment_SCanQR.SendingClientID(Config.ClientID_Azureportal);
		DeviceEnrollment_SCanQR.SendingClientSecret(Config.ClientSecret_Azureportal);
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		DeviceEnrollment_SCanQR.SendingEmailIDForAZURE_AD(Config.OAuthEmailID_Azureportal);
		DeviceEnrollment_SCanQR.SendingEmailPasswordForAZURE_AD(Config.OAuthEmailPassword_Azureportal);
		DeviceEnrollment_SCanQR.ClickOnSignIn();
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.OAuthUsername_Azureportal);
		DeviceEnrollment_SCanQR.VerifyingEnrolledDeviceUserNameInConsole(Config.OAuthUsername_Azureportal);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);	
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();	    
	}

	@Test(priority='C',description="Verify device enrolment UI with OAuth Authentication by disabling Advanced Device Authentication check box")
	public void VerifyEnrolMentUIWithOAuthauthenticationBydisablingADA() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		DeviceEnrollment_SCanQR.VerifyNativeAppTabUI();
		DeviceEnrollment_SCanQR.ClickingOnWebapplicationTab();
		DeviceEnrollment_SCanQR.VerifyOAthUIByDisablingADA();
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority='D',description="Verify Android device enrolment with GSUITE OAuth Authentication by disabling Advanced Device Authentication check box.")
	public void VerifyAndroidEnrolmentWithGSUITEOAuthByDisablingADA() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		DeviceEnrollment_SCanQR.SettingOAuthType_NativeappTab("4","GSUITE");
		DeviceEnrollment_SCanQR.SendingAuthEndpoint_NativeApp(Config.NativeAuthEndPoint_ADADisabled);
		DeviceEnrollment_SCanQR.SendingTokenEndPoint_NativeApp(Config.NativeTokenEndpoint_ADADisabled);
		DeviceEnrollment_SCanQR.SendingClientID_NativeApp(Config.NativeClientID__ADADisabled);
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnSettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollment();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.ClickOnScanQRButton();
		DeviceEnrollment_SCanQR.ClickOnAuthorizeMeButton();
		DeviceEnrollment_SCanQR.SendingEmailIDForGsuite(Config.OAuthGmailID);
		DeviceEnrollment_SCanQR.ClickingOnNextButton_GSuite();
		DeviceEnrollment_SCanQR.SendingPasswordForGsuite(Config.OAuthGmailPassword);
		DeviceEnrollment_SCanQR.ClickingOnNextButton_GSuite();
		commonmethdpage.ClickOnHomePage();
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice_Enrollment(Config.OAuthUserName);
		DeviceEnrollment_SCanQR.VerifyingEnrolledDeviceUserNameInConsole(Config.OAuthUserName); 
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);	
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		DeviceEnrollment_SCanQR.ClickingOnImportExportSettings("Import/Export Settings");
		DeviceEnrollment_SCanQR.ClickingOnResetSettings();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();
		DeviceEnrollment_SCanQR.ClickOnDoneInNix();	
	}
	/*@Test(priority='E',description="Verify Android device enrollment with Azure AD OAuth Authentication by disabling Advanced Device Authentication check box.")
	public void VerifyAndroidEnrolmentWithAzureADOAuthByDisablingADA() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.ClickOnSettings();
    	commonmethdpage.ClickonAccsettings();
    	DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
    	DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
    	DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
    	DeviceEnrollment_SCanQR.SettingOAuthType_NativeappTab("5","AZURE AD");
    	DeviceEnrollment_SCanQR.SendingAuthEndpoint_NativeApp(Config.OAuthEndpoint_Azureportal_ADADisabled);
    	DeviceEnrollment_SCanQR.SendingTokenEndPoint_NativeApp(Config.OAuthTokenEndpoint_Azureportal_ADADisabled);
    	DeviceEnrollment_SCanQR.SendingClientID_NativeApp(Config.ClientID_Azureportal_ADADisabled);
    	DeviceEnrollment_SCanQR.ClickOnApplyButton();
    	commonmethdpage.ClickOnSettings();
    	deviceEnrollment.verifyClickingOnDeviceEnrollment();
    	androidJOB.AppiumConfigurationforNIX();
    	DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
   	    DeviceEnrollment_SCanQR.ClickOnContinueButton();
   	     DeviceEnrollment_SCanQR.ClickOnScanQRButton();
   	    DeviceEnrollment_SCanQR.ClickOnContinueButton();
    	DeviceEnrollment_SCanQR.SendingAccountID(Config.AccountId);
    	DeviceEnrollment_SCanQR.CliCkOnRegisterButton_DisableADA();
    	DeviceEnrollment_SCanQR.ClickOnAuthorizeMeButton();
    	DeviceEnrollment_SCanQR.SendingEmailIDForAZURE_AD(Config.OAuthEmailID_Azureportal);
   	    DeviceEnrollment_SCanQR.SendingEmailPasswordForAZURE_AD(Config.OAuthEmailPassword_Azureportal);
   	    DeviceEnrollment_SCanQR.ClickOnSignIn();
   	    commonmethdpage.ClickOnHomePage();
   	    applyload.clicOnGridRefresh();
	    commonmethdpage.SearchDevice_Enrollment(Config.OAuthUsername_Azureportal);
	    DeviceEnrollment_SCanQR.VerifyingEnrolledDeviceUserNameInConsole(Config.OAuthUsername_Azureportal);
	    PendingDelete.clickOnDeleteDevice();
	    PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
	    DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete();	
	    DeviceEnrollment_SCanQR.ClikOnNixSettings();
	    DeviceEnrollment_SCanQR.ClickingOnImportExportSettings();
	    DeviceEnrollment_SCanQR.ClickingOnResetSettings();
	    DeviceEnrollment_SCanQR.ClickOnDoneInNix();
	    DeviceEnrollment_SCanQR.ClickOnDoneInNix();	
	}
	 */




}
