package CustomReportsScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class CustomColumn_CustomReport extends Initialization
{
@Test(priority='0',description="Verify Custom Columns report in custom report-for particular group")
public void VerifyCustomColumnForHomeGroup_TC_RE_150() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify Custom Columns report in custom report- for particular group",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Custom Columns");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustomColumns custrepor for Home group","Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustomColumns custrepor for Home group");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CustomColumns custrepor for Home group");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustomColumns custrepor for Home group");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustomColumns custrepor for Home group");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep(Config.SendAliasNameAsTestingdevice);
		customReports.CheckingCcolumnNameValue(Config.SendAliasNameAsTestingdevice);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='1',description="Verify Custom Columns report in custom report-for particular device")
public void VerifyCustomColumnForaGroup_TC_RE_151() throws InterruptedException
	{Reporter.log("\nVerify Custom Columns report in custom report-for particular device",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Custom Columns");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustomColumns custrepor for particular group","Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustomColumns custrepor for particular group");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CustomColumns custrepor for particular group");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustomColumns custrepor for particular group");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustomColumns custrepor for particular group");
		reportsPage.WindowHandle();
		customReports.ClickOnSearchReportButtonInsidedViewedReport_CColumn();
		customReports.CheckingCcolumnNameValue(Config.SendAliasNameAsTestingdevice);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
 @Test(priority='2',description="Verify Sort by and Group by for Custom Column")
 public void VerifyCustomColumnInGroupWithFilter() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for Custom Column",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Custom Columns");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustomColumns custrepor for Sortby and Groupby","Test");
		customReports.Selectvaluefromsortbydropdown(Config.Sort_CostumColumn);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustomColumns custrepor for Sortby and Groupby");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CustomColumns custrepor for Sortby and Groupby");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustomColumns custrepor for Sortby and Groupby");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustomColumns custrepor for Sortby and Groupby");
		reportsPage.WindowHandle();
		customReports.CheckingValueSortedOrNot();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
 @Test(priority='3',description="Verify generating custom report with the custom columns with the special characters(eg : , . / @ # $%^&*())")
 public void VerifyCustomColumnwithspecialchar() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify Sort by and Group by for Custom Column",true);
		deviceGrid.DisablingAllCustomColumns();	
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn("te,st");
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn("te,st");
		deviceGrid.VerifyCustomColumnsAddedSuccessfully("te,st");
		deviceGrid.ClickOnCustomColumnsCloseButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList("te,st");
		commonmethdpage.SearchDevice(Config.Device_Name);
		deviceGrid.AddingValuesToCustomColumn("###TestDevice");
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Custom Columns");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Custrep with the custcol with the special chars","Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustomColumns custrepor for Sortby and Groupby");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CustomColumns custrepor for Sortby and Groupby");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustomColumns custrepor for Sortby and Groupby");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustomColumns custrepor for Sortby and Groupby");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep("###TestDevice");
		customReports.CheckingCcolumnNameValue("###TestDevice");
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
 
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
