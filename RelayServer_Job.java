package JobsOnAndroid;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class RelayServer_Job extends Initialization{
	
	
	@Test(priority=1, description="\n Verify UI of relay server configuration Job under android jobs")
	public void TC_JO_899() throws InterruptedException, IOException{
		Reporter.log("\n Verify UI of relay server configuration Job under android jobs",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnRelayServerJob();
		androidJOB.VerifyUIrelayServer();
		androidJOB.CloseRelayServer();
		deviceinfopanelpage.ClickOnSelectJobTypeBackButton();
		deviceinfopanelpage.ClickOnSelectOSCancelButton();
		commonmethdpage.ClickOnHomePage();

		


}
}