package OnDemandReports_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class DataUsage extends Initialization {

	@Test(priority = 1, description = "1. Generate And View the 'Data Usage Report'  for Current Billing Cycle for Home Group")
	public void GenerateViewDataUsageCurrentBillingCycle_HomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("1. Generate And View the 'Data Usage Report'  for Current Billing Cycle",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDataUsageReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
	    accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyDataUsageReportColumns();
		accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.WifiDataUsageColumnVerificationDataUsageReport();
		reportsPage.MobileDataUsageColumnVerificationDataUsageReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 2, description = "2.Generate And View the 'Data Usage Report'  for Previous Billing Cycle - 1")
	public void GenerateViewDataUsgaePrvious1BillingCycle() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n2.Generate And View the 'Data Usage Report'  for Previous Billing Cycle - 1", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDataUsageReport();
		reportsPage.SelectBillingCycle();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
	    accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyDataUsageReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.WifiDataUsageColumnVerificationDataUsageReport();
		reportsPage.MobileDataUsageColumnVerificationDataUsageReport();
		reportsPage.SwitchBackWindow();

	}
	@Test(priority = 3, description = "3.Generate And View the 'Data Usage Report'  for Current Billing Cycle for Sub Group")
	public void GenerateViewDataUsageCurrentBillingCycle_SubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n3.3.Generate And View the 'Data Usage Report'  for Current Billing Cycle for Sub Group",true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
		
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDataUsageReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyDataUsageReportColumns();
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName);
		reportsPage.WifiDataUsageColumnVerificationDataUsageReport();
		reportsPage.MobileDataUsageColumnVerificationDataUsageReport();
		reportsPage.SwitchBackWindow();
		
	}

	@Test(priority = 4, description = "4.Download for Data Usage Report")
	public void VerifyDownloadingDataUsageReport() throws InterruptedException {
		Reporter.log("\n4.Download for Data Usage Report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDataUsageReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
	    accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}
	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();
		}
	}

}