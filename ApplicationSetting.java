package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
///*Execute at last of All modules*
public class ApplicationSetting extends Initialization{
	

	
	@Test(priority=1, description="Application Settings - Verify Run At StartUp in Application Settings Job.")
	public void ApplicationSettingsRunAtStartup() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("Application Settings - Verify Run At StartUp in Application Settings Job.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("RunAtStartup");
		androidJOB.SearchApplication("ES File Explorer");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.RunAtStartUpJob("Run At Startup");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("RunAtStartup");
		androidJOB.CheckStatusOfappliedInstalledJob("RunAtStartup",500);
		dynamicjobspage.RebootDevice();
		commonmethdpage.UnlockingDeviceThroughPassword();
		dynamicjobspage.GetTopActivity();
	}
	
	@Test(priority=2, description="Application Settings - Verify Uninstall in Application Settings job.")
	public void ApplicationSettingsUninstall() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("Application Settings - Verify Uninstall in Application Settings job.",true);
		//commonmethdpage.InstallApplicationsFromPlayStore("com.gears42.surefox", "42Gears Mobility Systems");
		commonmethdpage.InstallApplicationsFromPlayStore("com.myairtelapp", "Airtel");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("UninstallJob");
		androidJOB.SearchApplication("Airtel");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.SearchApplication("SureFox");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.RunAtStartUpJob("Uninstall");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("UninstallJob");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("UninstallJob",500);
		androidJOB.CheckApplicationIsNotInstalledDeviceSide("com.myairtelapp");
	}
	
	@Test(priority=3, description="Application Settings - Verify Clear data in Application Settings job.")
	public void ApplicationSettingsClearData() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		Reporter.log("Application Settings - Verify Clear data in Application Settings job.",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("ClearDataJob");
		androidJOB.SearchApplication("ES File Explorer");	
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.RunAtStartUpJob("Clear Data");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ClearDataJob");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("ClearDataJob",500);
		commonmethdpage.AppiumConfigurationCommonMethod("com.estrongs.android.pop","com.estrongs.android.pop.app.openscreenad.NewSplashActivity");
		dynamicjobspage.verifyClearData();
	}
	
	@Test(priority=4, description="Application Settings - Verify Lock Apps in Application settings job.")
	public void ApplicationSettingsLockApps() throws Throwable{
		Reporter.log("Application Settings - Verify Lock Apps in Application settings job.",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnApplicationSettings();
		androidJOB.ApplicationSettingJobName("LockAppsJob");
		androidJOB.SearchApplication("Play Store");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.SearchApplication("Chrome");
		androidJOB.ClickOnSelectedJob();
		androidJOB.ClickOnAddApplication();
		androidJOB.ClickOnAdvancesAndSetPassword();
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("LockAppsJob");
		androidJOB.CheckStatusOfappliedInstalledJob("LockAppsJob",500);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.LaunchPlayStore();
		androidJOB.CheckApplicationLockAndEnterPassword();
		androidJOB.ClickOnHomeButtonDeviceSide();
		androidJOB.LaunchChrome();
		androidJOB.CheckApplicationLockAndEnterPassword();
		androidJOB.ClickOnHomeButtonDeviceSide();
		commonmethdpage.ClickOnHomePage();
		dynamicjobspage.ClickOnApps();
		dynamicjobspage.ClickOnAllAppsTab();
		dynamicjobspage.EnterSearchOfAllApps("Play Store");
		dynamicjobspage.ClickOnLockCheckBox("applock_com.android.vending");
		dynamicjobspage.ClickOnApplyChanges();
		dynamicjobspage.ClickOnAdvancedSettingsOkButton();
		dynamicjobspage.ClearSearchOfAllApps();
		dynamicjobspage.EnterSearchOfAllApps("Chrome");
		dynamicjobspage.UnlockChrome("applock_com.android.chrome");
		dynamicjobspage.ClickOnApplyChanges();
		dynamicjobspage.ClickOnAdvancedSettingsOkButton();
		dynamicjobspage.ClearSearchOfAllApps();
		dynamicjobspage.clickOnCloseDynamicApps();		
	}
	
}
