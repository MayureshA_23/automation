package OnDemandReports_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

// Pre-Condition
// Pass device name in the config
// need SIM supported device

public class ComplianceReports extends Initialization {

	@Test(priority = 0, description = "Verify all columns in Compliance Reports.")
	public void verifyAllColInComplianceReport()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("0.Verify all columns in Compliance Reports.", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 1, description = "Generate All Compliance Reports for Home group.")
	public void verifyAllComplianceReport_Home() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("Creating pre-condition for compliance report.", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("Ice Cream Sandwich(4.0)");
		androidJOB.SelectAndriodVersionSecond("Ice Cream Sandwich(4.0.3)");
		androidJOB.outOfComplianceActions("Send Message", 1, "osVersion_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobOsVersionSendMessage", 100);
		
		androidJOB.SearchDeviceInconsole(Config.DeviceName);

		// reportsPage.getStringFromGridAndsplitDate("Last Connected"); 
		// reportsPage.splitTime(); 
		// reportsPage.englishTime(); 
		// reportsPage.joinDateAndTime(); 
		// reportsPage.convertDateTimeFormat();

		Reporter.log("1.Generate All Compliance Reports for Home group.", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("OS Version");
		reportsPage.statusVerificationInComplianceReports("Non-Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 2, description = "Generate All Compliance Reports for Sub group.")
	public void verifyAllComplianceReport_SubGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("2.Generate All Compliance Reports for Sub group.", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch");
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("OS Version");
		reportsPage.statusVerificationInComplianceReports("Non-Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 3, description = "Generate Mobile network connectivity compliance report for 'Home' group.")
	public void verifyMobNetConnCompRep_HomeGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("3.Compliance job- PreCondition-To verify Mobile Network Connectivity.(Immediately)");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("Home");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnMobNetConCompliance();
		androidJOB.ClickonConfigureButton();
		androidJOB.outOfComplianceActions("Send Message");
		androidJOB.EnterJobNameTextField("MobileNetworkConnSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("MobileNetworkConnSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("MobileNetworkConnSendMessage", 100);

		Reporter.log("3.Generate Mobile network connectivity compliance report for 'Home' group.", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("MobileNetworkConnectivity");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("Mobile Network Connectivity");
		reportsPage.statusVerificationInComplianceReports("Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 4, description = "Generate Mobile network connectivity Compliance Reports for Sub group.")
	public void verifyMobNetConnCompRep_SubGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("4.Generate Mobile network connectivity Compliance Reports for Sub group.", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch");
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("MobileNetworkConnectivity");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("Mobile Network Connectivity");
		reportsPage.statusVerificationInComplianceReports("Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 5, description = "Generate Mobile Signal Strength Compliance reports for 'Home' group")
	public void verifyMobSigStrengthCompRep_HomeGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("5.Compliance job- PreCondition-To verify Mobile Signal Strength.", true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("Home");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnMobSignalStrength();
		androidJOB.ClickonConfigureButton();
		androidJOB.mobSigStrengthPercent("1");
		androidJOB.outOfComplianceActions("Send Message");
		androidJOB.EnterJobNameTextField("MobSignalStrengthSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("MobSignalStrengthSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("MobSignalStrengthSendMessage", 100);

		Reporter.log("5.Generate Mobile Signal Strength Compliance reports for 'Home' group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("MobileSignalStrength");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("Mobile Signal strength");
		reportsPage.statusVerificationInComplianceReports("Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 6, description = "Generate Mobile Signal Strength Compliance reports for sub group")
	public void verifyMobSigStrengthCompRep_SubGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("6.Generate Mobile Signal Strength Compliance Reports for Sub group.", true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("Home");
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch");
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("MobileSignalStrength");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("Mobile Signal strength");
		reportsPage.statusVerificationInComplianceReports("Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 7, description = "Generate Wifi Signal Strength Compliance reports for 'Home' group")
	public void verifyWifiSigStrengthCompRep_HomeGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("7.Compliance job- PreCondition-To verify Wifi Signal Strength.", true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("Home");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnwifiSigStrength();
		androidJOB.ClickonConfigureButton();
		androidJOB.wifiSigStrengthPercent();
		androidJOB.outOfComplianceActions("Send Message");
		androidJOB.EnterJobNameTextField("WifiSignalStrengthSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("WifiSignalStrengthSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("WifiSignalStrengthSendMessage", 300);

		Reporter.log("7.Generate Wifi Signal Strength Compliance reports for 'Home' group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("WifiSignalStrength");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("Wifi Signal strength");
		reportsPage.statusVerificationInComplianceReports("Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 8, description = "Generate Wifi Signal Strength Compliance reports for sub group")
	public void verifyWifiSigStrengthCompRep_SubGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("8.Generate wifi Signal Strength Compliance Reports for Sub group.", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch");
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("WifiSignalStrength");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("Wifi Signal strength");
		reportsPage.statusVerificationInComplianceReports("Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 9, description = "Generate OS Version Reports for Home group.")
	public void verifyOSVersionReport_Home()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("Creating pre-condition for compliance report.", true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("Home");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("Ice Cream Sandwich(4.0)");
		androidJOB.SelectAndriodVersionSecond("Ice Cream Sandwich(4.0.3)");
		androidJOB.outOfComplianceActions("Send Message", 1, "osVersion_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobOsVersionSendMessage", 100);
		Thread.sleep(200);

		Reporter.log("9.Generate OS Version Reports for Home group.", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("OS Version");
		reportsPage.statusVerificationInComplianceReports("Non-Compliant");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 10, description = "Generate OS Version Reports for Sub group.")
	public void verifyOSVersionReport_SubGroup()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("10.Generate OS Version Reports for Sub group.", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("dontouch");
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.clickOnComplianceReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.compliancePolicyColVerificationComplianceReport("OS Version");
		reportsPage.statusVerificationInComplianceReports("Non-Compliant");
		reportsPage.SwitchBackWindow();
	}

//	@Test(priority = 11, description = "Generate Battry Policy Reports for Home group.")
//	public void verifyBatteryPolicyReport_Home()
//			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("Creating pre-condition for compliance report.", true);
//		// commonmethdpage.ClickOnHomePage();
//		// commonmethdpage.ClickOnAllDevicesButton();
//		// commonmethdpage.SearchDevice(Config.Macos_DeviceName);
//		// rightclickDevicegrid.RightClickOndevice(Config.Macos_DeviceName);
//		// rightclickDevicegrid.MoveToGroup("Home");
//		// androidJOB.clickOnJobs();
//		// androidJOB.clickNewJob();
//		// androidJOB.clickOnAndroidOS();
//		// androidJOB.ClickOnComplianceJob();
//		// androidJOB.clickOnBatteryPolicyCompliance();
//		// androidJOB.batteryPolicyPercent();
//		// androidJOB.ClickonConfigureButton();
//		// androidJOB.selectPlugged();
//		// androidJOB.outOfComplianceActions("Send Message");
//		// androidJOB.EnterJobNameTextField("BatteryPolicySendMessage");
//		// androidJOB.ClickOnSaveButtonJobWindow();
//		// commonmethdpage.ClickOnHomePage();
//		// androidJOB.SearchDeviceInconsole(Config.Macos_DeviceName);
//		// commonmethdpage.ClickOnApplyButton();
//		// androidJOB.SearchField("BatteryPolicySendMessage");
//		// androidJOB.CheckStatusOfappliedInstalledJob("BatteryPolicySendMessage", 100);
//		// Thread.sleep(200);
//
//		Reporter.log("11.Generate Battery Policy Reports for Home group.", true);
//		reportsPage.ClickOnReports_Button();
//		reportsPage.ClickOnOnDemandReport();
//		reportsPage.clickOnComplianceReport();
//		reportsPage.uncheck_CompRulesCheckBox();
//		reportsPage.SelectComplianceOptns("BatteryPolicy");
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		reportsPage.ClickOnView();
//		reportsPage.WindowHandle();
//		reportsPage.SearchDeviceInReportsView(Config.Macos_DeviceName);
//		reportsPage.DeviceNameColumnVerificationForMacOS();
//		reportsPage.compliancePolicyColVerificationComplianceReport("Battery Policy");
//		reportsPage.statusVerificationInComplianceReports("Non-Compliant");
//		reportsPage.SwitchBackWindow();
//	}
//
//	@Test(priority = 12, description = "Generate Battry Policy Reports for Sub group.")
//	public void verifyBatteryPolicyReport_subGroup()
//			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("12.Generate Battery Policy Reports for Home group.", true);
//		commonmethdpage.ClickOnHomePage();
//		rightclickDevicegrid.VerifyGroupPresence("dontouch");
//		commonmethdpage.ClickOnAllDevicesButton();
//		commonmethdpage.SearchDevice(Config.Macos_DeviceName);
//		rightclickDevicegrid.RightClickOndevice(Config.Macos_DeviceName);
//		rightclickDevicegrid.MoveToGroup("dontouch");
//		reportsPage.ClickOnReports_Button();
//		reportsPage.ClickOnOnDemandReport();
//		reportsPage.clickOnComplianceReport();
//		reportsPage.uncheck_CompRulesCheckBox();
//		reportsPage.SelectComplianceOptns("BatteryPolicy");
//		reportsPage.ClickOnSelectGroup();
//		reportsPage.SearchGroup(Config.GroupName_Reports);
//		reportsPage.SelectTheGroup();
//		reportsPage.ClickOnOkButtonGroupList();
//		accountsettingspage.ClickONRequestReportButton();
//		accountsettingspage.ClickOnViewReportButton();
//		accountsettingspage.ClickOnRefreshInViewReport();
//		reportsPage.ClickOnView();
//		reportsPage.WindowHandle();
//		reportsPage.SearchDeviceInReportsView(Config.Macos_DeviceName);
//		reportsPage.DeviceNameColumnVerificationForMacOS();
//		reportsPage.compliancePolicyColVerificationComplianceReport("Battery Policy");
//		reportsPage.statusVerificationInComplianceReports("Non-Compliant");
//		reportsPage.SwitchBackWindow();
//	}
	
	@Test(priority = 13, description = "PreCondition")
	public void PreCondition()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n13.PreCondition");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnJailBrokenCompliance();
		androidJOB.ClickOnConfigureButton("rooted_BLK");
		androidJOB.clickOnandRoottoggle();
		androidJOB.outOfComplianceActions("Send Message", 2, "rooted_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobrooted_BLKSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobrooted_BLKSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobrooted_BLKSendMessage", 300);
		androidJOB.verifyOutOfComplianceMsgOnDevice();
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
		Reporter.log(
				"Pass >> Compliance job - Verify creating Compliance job with Rooted and out of Compliance action as Send Message.",
				true);
	}

	@Test(priority = 14, description = "14.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReport_JailBrokenRootedHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n14.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("JailbrokenOrRooted");

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Jailbroken/Rooted");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 15, description = "15.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReport_JailBrokenRootedSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n15.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("JailbrokenOrRooted");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Jailbroken/Rooted");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();

		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 16, description = "16.Compliance Job - Precondition-verify creating compliance job with Online Device connectivity")
	public void ComplianceJobOnlineDeviceSendMessage() throws Throwable {
		Reporter.log(
				"\n16.Compliance Job - Precondition-verify creating compliance job with Online Device connectivity");

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnOnlineDeviceConnectivity();

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");

		androidJOB.ClickOnConfigureButton("conn_BLK");
		androidJOB.ComplianceDurationDeviceShouldBeoffLine("1");
		androidJOB.outOfComplianceActions("Send Message", 2, "conn_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobDeviceConectivitySendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();

		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobDeviceConectivitySendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceConectivitySendMessage", 150);
		androidJOB.makeDeviceOffForGivenTime(250);

		androidJOB.makeDeviceOnLine();
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
		Reporter.log(
				"Pass >>Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions",
				true);
	}

	@Test(priority = 17, description = "17.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportOnlineDeviceConnectivityHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n17.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("OnlineDeviceConnectivity");

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Online Device Connectivity");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 18, description = "18.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportOnlineDeviceConnectivitySubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n18.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("OnlineDeviceConnectivity");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Online Device Connectivity");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();

		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 19, description = "19.Compliance job - PreCondition-Verify creating Compliance job with Device storage and out of Compliance action as Send Message.")
	public void TC_JO_879()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log(
				"\n19.Compliance job - PreCondition-Verify creating Compliance job with Device storage and out of Compliance action as Send Message.",
				true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SelctingComplianceJobsRule("Device Storage");
		androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
		androidJOB.devicestorage_percent("90");
		androidJOB.outOfComplianceActions("Send Message", 2, "DeviceStorage_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobDeviceStorageSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobDeviceStorageSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceStorageSendMessage", 300);
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
		Reporter.log(
				"Pass >> Compliance job - Verify creating Compliance job with Device storage and out of Compliance action as Send Message.",
				true);
	}

	@Test(priority = 20, description = "20.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportDeviceStorageHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n20.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("DeviceStorage");

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Device Storage");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 21, description = "21.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportDeviceStorageSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n21.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("DeviceStorage");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Device Storage");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();

		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 22, description = "22.Compliance job- PreCondition-To verify blacklisted apps for Lock device action.(Immediately)")
	public void VrifyBlackListedAppsForLockDeviceAction()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log(
				"\n22.Compliance job- PreCondition-To verify blacklisted apps for Lock device action.(Immediately)",
				true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppLockDeviceAction");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Lock Device (Android / iOS/iPadOS)");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppLockDeviceAction");
		androidJOB.JobInitiatedMessage();
		androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppLockDeviceAction", 600);
		androidJOB.VerifyIsDeviceLocked();
		dynamicjobspage.UnLockingDeviceUsingADB();

	}

	@Test(priority = 23, description = "23.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportBlackListHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n23.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("BlacklistedApplications");

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Blacklisted Apps");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 24, description = "24.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportBlackLisSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n24.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("BlacklistedApplications");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Blacklisted Apps");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();

		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 25, description = "25.Compliance job- PreCondition-Kiosk")
	public void ComplianceKiosk()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n25.Compliance job- PreCondition-Kiosk", true);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_Kiosk");
		androidJOB.SelctingComplianceJobsRule("Kiosk");
		androidJOB.ClickonConfigureButton();
		androidJOB.EnablekioskMode();
		androidJOB.outOfComplianceActions("Send Message", 2, "kioskenabled_blk");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_Kiosk");
		androidJOB.JobInitiatedMessage();
		androidJOB.CheckStatusOfappliedInstalledJob("Automation_Kiosk", 600);
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();

	}

	@Test(priority = 26, description = "26.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportKisoskHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n26.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("Kiosk");

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();

		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Kiosk Enabled");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 27, description = "27.Generate And View the 'Compliance Report'  for 'Home/Sub' group'")
	public void ComplianceReportKisoskSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n27.Generate And View the 'Compliance Report'  for 'Home/Sub' group'", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnComplianceReportReportOption();
		reportsPage.uncheck_CompRulesCheckBox();
		reportsPage.SelectComplianceOptns("Kiosk");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();

		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnRefreshInViewReport();

		accountsettingspage.ClickOnView("Compliance Report");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyComplianceJobReportColumns();
		reportsPage.VerifyComplianceDeviceNameReport(Config.DeviceName);
		reportsPage.VerifyCompliancePolicy("Kiosk Enabled");
		reportsPage.ComplianceStatusColumnVerification("Compliant", "Non-Compliant");
		reportsPage.LastConnectedDate();

		reportsPage.SwitchBackWindow();

	}

	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				String FailedWindow = Initialization.driver.getWindowHandle();
				if (FailedWindow.equals(reportsPage.originalHandle)) {
					commonmethdpage.ClickOnHomePage();
				} else {
					reportsPage.SwitchBackWindow();
				}
			} catch (Exception e) {

			}
		}
	}
}
