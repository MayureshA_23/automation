package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DifferentVersionsOfNixJobs extends Initialization {
	
	@Test(priority=1, description="Verify Applying jobs to multiple versions of nix")
	public void VerifyApplyingJobsToMultipleVersionsOfNix() throws InterruptedException, IOException{
		Reporter.log("\n Verify Applying jobs to multiple versions of nix",true);
		androidJOB.clickOnJobs(); 
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS(); 
		androidJOB.ClickOnInstallApplication(); 
		androidJOB.enterJobName(Config.NameOfJob); 
		androidJOB.clickOnAddInstallApplication(); 
		androidJOB.browseAPK("./Uploads/apk/\\FileTouch.exe"); 
		androidJOB.clickOkBtnOnIstallJob(); 
		androidJOB.clickOkBtnOnAndroidJob();

	
	
}
	//Enroll 3-4 devices with different versions of nix Market and previous versions
	@Test(priority=2, description="Verify Applying jobs to multiple versions of nix")
	public void VerifyApplyingJobsToMultipleVersionsOfNix1() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		
		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.NameOfJob); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.NameOfJob,360);

		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.MultipleDevices1);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.NameOfJob); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.NameOfJob,360);

		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.MultipleDevices2);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.NameOfJob); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.NameOfJob,360);

		
		
		
		
		
		
		
		
		
		
		
	}
}

