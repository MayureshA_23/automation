package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class BlackListWhiteListApps extends Initialization{
	

	@Test(priority='1',description="1.To verify clicking on the iOS option")
	public void VerifyClickingOn_iOS_Option() throws InterruptedException{
		profilesAndroid.ClickOnProfile();
		profilesiOS.clickOniOSOption();
		profilesiOS.AddProfile();
		
	}
	@Test(priority='2',description="1.To verify summary black list whitelist apps")
	public void VerifySummaryofBlackListWhiteListApps() throws InterruptedException
	{
		profilesiOS.VerifySummaryof_BlackListWhiteListApps();
	}
	
	@Test(priority='3',description="1.To Verify warning message Saving a profile without profile Name")
	public void VerifyWarningMessageOnSavingAProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning messages while Saving a Password Policy profile=========");
		profilesiOS.clickOnBlackListAppConfigure();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='4',description="1.To Verify warning message Saving a profile without entering all the fields")
	public void VerifyWarningMessageOnSavingAProfileEnteringRequiredFields() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a profile without entering all the fields=========");
		profilesiOS.EnterBlacklistProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
		
	}
	
	
	@Test(priority='5',description="to verify clicking on add button without selecting an application")
	public void VerifyWarningMessageWhileClickinAddWithoutSelectingSelectingApp() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify clicking on add button without selecting an application");
		//profilesiOS.clickOnBlackListAppConfigure();
		profilesiOS.ClickOnAddButton();
		profilesiOS.ClickOnAddButtonInBlackListWhiteListApplicationList();
		profilesiOS.WarningMessageOnClickingAddButtonWithoutChoosingApp();
	}
	
	
	@Test(priority='6',description="To verify choosing blacklisting application")
	public void VerifyBlacklistingApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("To verify choosing blacklisting application");
		
		profilesiOS.ClickChooseApp();
	}
	
	@Test(priority='7',description="To verify warning message while deleting blacklisted application")
	public void VerifyWarningMessageWhileDeletingBlacklistedApplication() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("To verify warning message while deleting blacklisted application");
	
	    profilesiOS.SelectFirstRowApp();  
	    profilesiOS.ClickOnDeleteButton();
	     profilesiOS.WarningMessageOnApplicationDelete();
	}
	
	@Test(priority='8',description="To verify Cancelling the warning dialog of delete")
	public void Verify_CancellingWarningDialogOfDelete() throws InterruptedException{
		Reporter.log("To verify Cancelling the warning dialog of delete",true);
		profilesiOS.ClickOnNoButonOnDeleteWarningPopUp();
	}
	
	@Test(priority='9',description="To verify Deleting-Clicking on Yes of warning dialog of delete")
	public void Verify_DeletingBlacklistedApplication() throws InterruptedException{
		Reporter.log("To verify Deleting-Clicking on Yes of warning dialog of delete",true);
	    profilesiOS.ClickOnDeleteButton();
	    profilesiOS.ClickOnYesDeleteApplication();
	}
	
	@Test(priority='A',description="To verify saving the Blacklist profile")
	public void Verify_SavingBlacklistProfile() throws InterruptedException{
		Reporter.log("To verify saving the profile ",true);
	    profilesiOS.ClickOnSavebutton();
	    profilesAndroid.NotificationOnProfileCreated();
	}
	
	@Test(priority='B',description="To verify creating and saving whitelist profile")
	public void Verify_SavingWhitelistProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("To verify saving whitelist profile ",true);
	    profilesiOS.AddProfile();
	    profilesiOS.clickOnWhiteListAppConfigure();
	    profilesiOS.EnterWhitelistProfileName();
	    profilesiOS.ClickOnAddButton();
	    profilesiOS.ClickChooseApp();
	    profilesiOS.ClickOnSavebutton();
	    profilesAndroid.NotificationOnProfileCreated();
	    
	}
	
	
	
}
	    
	
	
	

		
		
		
	
	
	

		



