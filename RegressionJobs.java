package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class RegressionJobs extends Initialization
{
    @Test(priority=1,description="Nix Agent Settings - Verify Creating and deploying Nix agent settings with all options and by specifying the connection type.")
    public void VerifyDeployingNixSettingsBySpecifyingConnectionType() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("nix_settings");
		androidJOB.SendingNixAgentSettingsJob("Nix Agent Settings_Automation");
		androidJOB.SelectingOptionsInNixAgentsettingsJob("job_nixsettings_EnableTimeSync_input");
		androidJOB.SelectingOptionsInNixAgentsettingsJob("job_nixsettings_EnablePeriodicUpdate_input");
		androidJOB.SelectingOptionsInNixAgentsettingsJob("job_nixsettings_EnableNixPassword_input");
		androidJOB.SelectingConnectionTypeInNixAgentSettingsJob("job_nixsettings_ConnType_input","WIFI");
		androidJOB.ClickOnLocatioTrackingJobOkButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Nix Agent Settings_Automation");
		androidJOB.CheckingJobStatusUsingConsoleLogs("Nix Agent Settings_Automation");	
    }
    
    @Test(priority=2,description="Composite Job - Verify creating composite job by adding android version.")
    public void VerifyCreatingAndroidJobsByAddingAndroidVersion() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
    {
        androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.FileTransferJobName);
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/Job On Android/FileTransferSingle/\\File1.exe");//./Uploads/UplaodFiles/FileStore/\\Image.exe
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("comp_job");
		androidJOB.CompositeJobName(Config.CompositeJobName);
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.TextMessageJobname);
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.FileTransferJobName);
		androidJOB.SelectingConnectionTypeInNixAgentSettingsJob("comp_job_min_version_logic","0");
		androidJOB.SelectingConnectionTypeInNixAgentSettingsJob("comp_job_min_version_num","27");
		androidJOB.ClickOnOnOkButtonAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.CompositeJobName);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.CompositeJobName, Config.TotalTimeForJobName1ToGetDeployedOnDevice);
    }
    
   @Test(priority=3,description="Geo Fence - UI Validation")
    public void verifyGEOFenceUI() throws InterruptedException
    {
    	 androidJOB.clickOnJobs();
 		androidJOB.clickNewJob();
 		androidJOB.clickOnAndroidOS();
 		androidJOB.ClickOnGeoFenceJobType("geo_fencing_job");	 
 		androidJOB.VerifyGeoFenceUI();
    }
    
    @Test(priority=4,description="Geo Fence - Verify download templates in Select Fence in Geo fence job")
    public void VerifyDownloadTemplate() throws EncryptedDocumentException, InvalidFormatException, InterruptedException, IOException
    {
    	androidJOB.VerifydownloadedFileInLocation("GeoFences_Template",androidJOB.DownloadGeoFenceTemplateButton);
    	androidJOB.ClosingGeoFenceTab();
    	commonmethdpage.ClickOnHomePage();
    }
    
    @Test(priority=5,description="Jobs - Verify applying copied job to device")
    public void verifyApplyingCopiedJobtodevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	androidJOB.clickOnJobs();
    	androidJOB.ClickOnNewFolder();
		androidJOB.NamingFolder(Config.JobFolderName);
		androidJOB.SearchJobInSearchField(Config.TextMessageJobname);
		androidJOB.ClickOnOnJob(Config.TextMessageJobname);
		androidJOB.ClickOnCopyJobOption();
		androidJOB.SelctingFolder(Config.JobFolderName);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchingJobFolder(Config.JobFolderName);
		androidJOB.DoubleClickOnFolder("scheduleJob",Config.JobFolderName);
		androidJOB.SearchField(Config.TextMessageJobname+"_copy");
	//	androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckingJobStatusUsingConsoleLogs(Config.CompositeJobName+"_copy");
    }
    
    @Test(priority=6,description="Compliance job- To verify Blocklisted apps by adding the package name of an applictaion which is set as device admin")
    public void VerifyBlocklistedAppsByAddingPackageSetAsDeviceAdmin() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin");
		androidJOB.SelctingComplianceJobsRule("Blacklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob("com.nix");
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Uninstall App");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin");
		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckingJobStatusUsingConsoleLogs("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin");
		androidJOB.VerifyUninstallationOfApplicaatioSetAsDeviceAdmin();
	}
    
    @Test(priority=7,description="Verify creating and applying runscript job for Nix permission checklist")
    public void VerifApplyingRunScriptForNixPermissionCheckList() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		accountsettingspage.sendingRunScript(Config.NixPermission_RunScript);
		runScript.RunScriptName("Nix Permission_Automation");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Nix Permission_Automation");
	//	androidJOB.ClickOnApplyJobOkButton();
		accountsettingspage.ReadingConsoleMessageForJobDeployment("Nix Permission_Automation",Config.DeviceName);	
    }
    
    @Test(priority=8,description="Verify UI to keep Wifi Always On/Off")
    public void VerifyUITokeepWifiAlwaysOn() throws InterruptedException, IOException
    {
    	androidJOB.clickOnJobs();
    	androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("security_policy");
		androidJOB.SwitchingToPeripheralSettingsTab();
		androidJOB.VerfyingKeepWIFIAlwaysOnOff();
		androidJOB.ClickOnSecurityPolicyJobCloseButton();
		commonmethdpage.ClickOnHomePage();
    }
    
    @Test(priority=9,description="Verify creating and applying Security policy job with 'WiFi Always ON' in Nix v18.07 or above ")
    public void VerifyApplyingSecurityPolicyJObWithWiFiAlwaysOn() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException
    {
    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
    	androidJOB.clickOnJobs();
    	androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("security_policy");
		androidJOB.SwitchingToPeripheralSettingsTab();
		androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiAlwaysOn");
		androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
		androidJOB.SelectingWifiStatus("ALWAYS_ON");
		androidJOB.ClickOnLocatioTrackingJobOkButton();
    	commonmethdpage.ClickOnHomePage();
    	commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOn");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOn",Config.DeviceName);	
		quickactiontoolbarpage.SwitchingWifiStatus("OFF");
		androidJOB.VerifyWiFiStatus("1");
		Reporter.log("PASS>>> Wifi Status Is On Even After Trying To Turn Off",true);
    }
    
    @Test(priority=10,description="Verify creating and applying Security policy job with 'WiFi Always OFF' in Nix v18.07 or above")
    public void VerifyApplyingSecurityPolicyJObWithWiFiAlwaysOff() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException
    {
    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
    	androidJOB.clickOnJobs();
    	androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("security_policy");
		androidJOB.SwitchingToPeripheralSettingsTab();
		androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiAlwaysOFF");
		androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
		androidJOB.SelectingWifiStatus("ALWAYS_OFF");
		androidJOB.ClickOnLocatioTrackingJobOkButton();
    	commonmethdpage.ClickOnHomePage();
    	commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOFF");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOFF",Config.DeviceName);	
		quickactiontoolbarpage.SwitchingWifiStatus("ON");
		androidJOB.VerifyWiFiStatus("0");
		Reporter.log("PASS>>> Wifi Status Is On Even After Trying To Turn Off",true);
    }
    
    @Test(priority=11,description="Verify creating and applying Security policy job with 'WiFi Dont Care' in Nix v18.07 or above")
    public void VerifyApplyingSecurityPolicyJObWithWiFiDontCare() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
    {
    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
    	androidJOB.clickOnJobs();
    	androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("security_policy");
		androidJOB.SwitchingToPeripheralSettingsTab();
		androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiDontCare");
		androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
		androidJOB.SelectingWifiStatus("DONT_CARE");
		androidJOB.ClickOnLocatioTrackingJobOkButton();
    	commonmethdpage.ClickOnHomePage();
    	commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("AutomationSecurityPolicy_WiFiDontCare");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiDontCare",Config.DeviceName);	
		quickactiontoolbarpage.SwitchingWifiStatus("OFF");
		androidJOB.VerifyWiFiStatus("0");
		quickactiontoolbarpage.SwitchingWifiStatus("ON");
		androidJOB.VerifyWiFiStatus("1");
		Reporter.log("PASS>>> Wifi Status Is Changed As Per Condition",true);
    }
    
    @Test(priority=12,description="Verify behaviour of Wifi after reboot with WiFi Always ON/Always OFF/Dont care")
    public void VerifyApplyingSecurityPolicyJObWithWiFiAlwaysOnAfterReboot() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
    {
    	commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOn");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOn",Config.DeviceName);	
		androidJOB.RebootingTheDevice();
    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
    	quickactiontoolbarpage.ClickOnNixSettings();
		quickactiontoolbarpage.SwitchingWifiStatus("OFF");
		androidJOB.VerifyWiFiStatus("1");
		Reporter.log("PASS>>> Wifi Status Is On Even After Trying To Turn Off After Reboot",true);
    }
    
    @Test(priority=13,description="Verify different combinations of Security Policy Job along with WiFi ")
    public void VerifyDifferentCombinationsOfSecurityPolicyJobsalongWithWIFI() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
        androidJOB.clickOnJobs();
    	androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("security_policy");
		androidJOB.SwitchingToPeripheralSettingsTab();
		androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiAlwaysOn_DisableCamera");
		androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
		androidJOB.SelectingWifiStatus("ALWAYS_ON");
		androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("12","Disable Camera");
		androidJOB.ClickOnLocatioTrackingJobOkButton();
    	commonmethdpage.ClickOnHomePage();
    	commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOn_DisableCamera");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOn_DisableCamera",Config.DeviceName);	
		quickactiontoolbarpage.SwitchingWifiStatus("OFF");
		androidJOB.VerifyWiFiStatus("1");
		androidJOB.VerifyAppIsNotOpenedOnDisablingInSecurityPolicy("com.sec.android.app.camera");
    }
    
    @Test(priority=14,description="Verify modifying WiFi Always On/Always Off/Dont Care in already created Security Policy Job")
    public void VerifyModifyingWifiAlreadyCreatedSecurityJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
    	androidJOB.clickOnJobs();
    	androidJOB.SearchJobInSearchField("AutomationSecurityPolicy_WiFiDontCare");
    	androidJOB.ClickOnOnJob("AutomationSecurityPolicy_WiFiDontCare");
    	androidJOB.clickOnModifyJob();
    	androidJOB.SwitchingToPeripheralSettingsTab();
    	androidJOB.DisablingCheckBoxesInPeriPheralSettingsTab("12","Disable Camera");
    	androidJOB.SelectingWifiStatus("ALWAYS_ON");
    	androidJOB.ClickOnLocatioTrackingJobOkButton();
    	commonmethdpage.ClickOnHomePage();
    	commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("AutomationSecurityPolicy_WiFiDontCare");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiDontCare",Config.DeviceName);	
		quickactiontoolbarpage.SwitchingWifiStatus("OFF");
		androidJOB.VerifyWiFiStatus("1");		
    }
    
   @Test(priority=15,description="Verify job deployed status on plugged in charging state")
    public void VerifyJobDeploymentStatusONPluggedInChargingState() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
    {
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.InstallJObName);   
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileInstallApplication("./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\File1.exe");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob(Config.InstallJObName);
		androidJOB.SelectingChargingStateInApplyJob("Plugged In");
		androidJOB.ClickOnApplyJobOkButton();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.clickonJobQueue();
		androidJOB.VerifyChargingPluggeStatusInsidJobQueue("Plugged In");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("surevideo.apk",Config.DeviceName);
		Reporter.log("Job Is Deployed Successfully As The Device Is Plugged In");
    }
    
    @Test(priority=16,description="Verify Job deploy on Don't care state in charging state")
    public void VerifyJobDeploymentStatusONDontCareChargingState() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
    	androidJOB.SearchJob(Config.InstallJObName);
		androidJOB.SelectingChargingStateInApplyJob("Don't Care");
		androidJOB.ClickOnApplyJobOkButton();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.clickonJobQueue();
		androidJOB.VerifyChargingPluggeStatusInsidJobQueue("Any");
		accountsettingspage.ReadingConsoleMessageForJobDeployment("surevideo.apk",Config.DeviceName);
		Reporter.log("Job Is Deployed Successfully As The Charging State Was Don't Care");
    }
    
    @Test(priority=17,description="Verify by adding install/FT job in composite job")
    public void VerifyByAddingInstallJonInCompositeJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
    {
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("comp_job");
		androidJOB.CompositeJobName(Config.CompositeJobName);
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.TextMessageJobname);
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.InstallJObName);
		androidJOB.ClickOnOnOkButtonAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
    	androidJOB.SearchJob(Config.CompositeJobName);
		androidJOB.SelectingChargingStateInApplyJob("Don't Care");
		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob(Config.CompositeJobName, Config.TotalTimeForJobName1ToGetDeployedOnDevice);			
    }
    
 /*   @Test(priority=18,description="Verify device charging status with combination of other platform device")
    public void VerifyDeviceChargeStatusWithCombinationOfOtherPlatfomDevice() throws InterruptedException, AWTException
    {
    	commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.Windows_DeviceName1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob(Config.InstallJObName);
		androidJOB.VerifyDeviceChargingStateOptionInApplyJob();
		quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
    }
  */  
    
    
    
    
    
    
    
}
