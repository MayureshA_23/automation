package JobsOnAndroid;

import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class SecurityPolicy extends Initialization

{
	   @Test(priority=1,description="Location tracking - verify creating and deploying location tracking job.")
	    public void VerifyLocationTracking_ONjob() throws Throwable
	    {
	    	androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("location_track");
			androidJOB.SendingJobName("Location Tracking On");
			androidJOB.EnablingLocationTrackingCheckBox();
			androidJOB.ClickOnLocatioTrackingJobOkButton();
			commonmethdpage.ClickOnHomePage();
			commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("Location Tracking On");
			//androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("Location Tracking On");
			dynamicjobspage.ClickOnRefreshButton();
			DeviceEnrollment_SCanQR.clicOnGridRefresh();
			androidJOB.VerifyLocationTrackingONStatusInConsole();		
	    }
	    
	    @Test(priority=2,description="Location Tracking - Verify disabling location tracking .")
	    public void VerifyLocationTracking_OFFjob() throws Throwable
	    {
	    	androidJOB.clickOnJobs();
	    	dynamicjobspage.SearchJobInSearchField("Location Tracking On");
	    	androidJOB.ClickOnOnJob("Location Tracking On");
	    	androidJOB.clickOnModifyJob();
	    	androidJOB.DisablingLocationTrackingCheckBox();
	    	androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	quickactiontoolbarpage.ClearingConsoleLogs();
			commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("Location Tracking On");
			//androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("Location Tracking On");
			dynamicjobspage.ClickOnRefreshButton();
			DeviceEnrollment_SCanQR.clicOnGridRefresh();
			androidJOB.VerifyLocationTrackingOFFStatusInConsole();    	
	    }
	    
	   
	    @Test(priority=3,description="Security Policy Job - Verify Peripheral settings by enabling/disabling disable Bluetooth")
	    public void VerifyPeripheralSettingsDisabling_EnablingBluetooth() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	    {
	    	commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings", "com.android.settings.Settings");
	    	androidJOB.ClickOnConnectionInSettings();
	    	androidJOB.EnablingBluetoothInDevice();
	    	androidJOB.ClickOnHomeButtonDeviceSide();
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_DisableBluetooth");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("8","Disable Bluetooth");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_DisableBluetooth");
			//androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_DisableBluetooth");
			commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings","com.android.settings.Settings");
			androidJOB.ClickOnConnectionInSettings();
			androidJOB.VerifyBlueToothStatusAfterApplyingDisableBluetoothJob();
			androidJOB.ClickOnHomeButtonDeviceSide();
	    }
	    
		//Invalid Case
		
	 /*   @Test(priority=5,description="Security Policy Job - Verify peripheral settings by enabling/disabling Disable WIFI")
	    public void VerifyPeripheralSettingsDisabling_EnablingWiFi() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	    {
	    	commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings","com.android.settings.Settings");
	    	androidJOB.ClickOnConnectionInSettings();
	    	androidJOB.EnablingWifiInDevice();
	    	androidJOB.ClickOnHomeButtonDeviceSide();
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_DisableWiFi");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("9","Disable WiFi");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_DisableWiFi");
		//	androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_DisableWiFi");
			commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings","com.android.settings.Settings");		androidJOB.ClickOnConnectionInSettings();
			androidJOB.VerifyWiFiStatusAfterApplyingDisableWiFiJOb();
			androidJOB.ClickOnHomeButtonDeviceSide();
			
	    } //should be the last test case
	 
	*/    
	    @Test(priority=4,description="Security Policy Job - Verify Peripheral Settings by selecting GPS as(Dont care/AlwaysOn/Always OFF)")
	    public void VerifyPeriPheralSettingsBySelectingGPSAsAlwaysOff() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_GPS_AlwaysOFF");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.SelectingSecurityPolicyParameters(androidJOB.SecurityPloicy_PeripheralGPSDropDown,"ALWAYS_OFF");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
			commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_GPS_AlwaysOFF");
		//	androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_GPS_AlwaysOFF");
			commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings","com.android.settings.Settings");
			deviceGrid.SearchingInsideSettings("Location");
			androidJOB.VeifyLocationStatusAfterApplyingSecurityPolicy_GPS_AlwaysOFFJob();
			androidJOB.ClickOnHomeButtonDeviceSide();
	    }
	    
	    @Test(priority=5,description="Security Policy Job - Verify Peripheral Settings by selecting GPS as(Dont care/AlwaysOn/Always OFF)")
	    public void VerifyPeriPheralSettingsBySelectingGPSAsAlwaysON() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_GPS_AlwaysON");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
	     	androidJOB.SelectingSecurityPolicyParameters(androidJOB.SecurityPloicy_PeripheralGPSDropDown,"ALWAYS_ON");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
			commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
	     	androidJOB.SearchField("AutomationSecurityPolicy_GPS_AlwaysON");
		//	androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_GPS_AlwaysON");
			commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings","com.android.settings.Settings");
			deviceGrid.SearchingInsideSettings("Location");
			androidJOB.VeifyLocationStatusAfterApplyingSecurityPolicy_GPS_AlwaysONJob();
			androidJOB.ClickOnHomeButtonDeviceSide();
	    }
	    
	    @Test(priority=6,description="Security Policy Job - Verify Peripheral Settings by selecting GPS as(Dont care/AlwaysOn/Always OFF)")
	    public void VerifyPeriPheralSettingsBySelectingGPSAsDontCare() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_GPS_DONTCARE");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
	     	androidJOB.SelectingSecurityPolicyParameters(androidJOB.SecurityPloicy_PeripheralGPSDropDown,"DONT_CARE");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
			commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_GPS_DONTCARE");
			//androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_GPS_DONTCARE");
			commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings","com.android.settings.Settings");
			deviceGrid.SearchingInsideSettings("Location");
			androidJOB.VeifyLocationStatusAfterApplyingSecurityPolicy_GPS_DontCare();	
			androidJOB.ClickOnHomeButtonDeviceSide();
	    }
	    
	    @Test(priority=7,description="Security Policy Job- Verify Peripheral settings by enabling/disabling Disable camera.")
	    public void VerifyCameraStatus_DisableCamera() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_DisableCamera");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("12","Disable Camera");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_DisableCamera");
		//	androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_DisableCamera");
			androidJOB.VerifyCameraIsNotOpenedInDevice();
	    }
	    
	    @Test(priority=8,description="Security Job - Verify modifying the Security policy job.")
	    public void VerifymodifyingSecurityPolicyJob_EnableCamera() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	        commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
	    	androidJOB.clickOnJobs();
	    	dynamicjobspage.SearchJobInSearchField("AutomationSecurityPolicy_DisableCamera");
	    	androidJOB.ClickOnOnJob("AutomationSecurityPolicy_DisableCamera");
	    	androidJOB.clickOnModifyJob();
	    	androidJOB.SwitchingToPeripheralSettingsTab();
	    	androidJOB.DisablingCheckBoxesInPeriPheralSettingsTab("12","Disable Camera");
	    	androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_DisableCamera");
			//androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_DisableCamera");
			androidJOB.VerifyCameraIsOpenedInDevice();
			androidJOB.ClickOnHomeButtonDeviceSide();
		}
/*	    @Test(priority=9,description="Security Policy Job - Verify creating and deploying security policy job with Password Policy")
	    public void VerifySecuPolicyWithPaswordPolicy() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm"); 
//	    	androidJOB.clickOnJobs();
//	    	androidJOB.clickNewJob();
//			androidJOB.clickOnAndroidOS();
//			androidJOB.ClickOnJobType("security_policy");
//			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_PasswordPolicy");
//			androidJOB.SelectingPasswordPolicyInDevice(2);
//			androidJOB.SelectingSecurityPolicyParameters(androidJOB.MinimumPasswordDropDown,"SEVEN");
//			androidJOB.SelectingSecurityPolicyParameters(androidJOB.PasswordStrengthDropDown,"ATLEAST_NUMERIC");
//			androidJOB.SelectingSecurityPolicyParameters(androidJOB.TimeLapseDropDown,"FIVE");
//			androidJOB.SelectingSecurityPolicyParameters(androidJOB.PasswordAttemptsDropDown,"SEVEN");
//			androidJOB.ClickOnLocatioTrackingJobOkButton();
//			commonmethdpage.ClickOnHomePage();
//			commonmethdpage.SearchDevice(Config.DeviceName);
//			commonmethdpage.ClickOnApplyButton();
//			androidJOB.SearchField("AutomationSecurityPolicy_PasswordPolicy");
//		//	androidJOB.ClickOnApplyJobOkButton();
//			androidJOB.JobInitiatedMessage();
//			commonmethdpage.VerifyIsDeviceLocked();
//          dynamicjobspage.UnLockingDeviceUsingADBSecurityPolicyJob();
			androidJOB.WaitForSetPwdPopUp("0000");
			androidJOB.WaitForSetPwdPopUp2("0000");
	    	commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm"); 
           androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_PasswordPolicy");
			androidJOB.VerifySecurPolicyPasswordProfileDeployedOnDevice();
			androidJOB.clickOnJobs();
	    	dynamicjobspage.SearchJobInSearchField("AutomationSecurityPolicy_PasswordPolicy");
	    	androidJOB.ClickOnOnJob("AutomationSecurityPolicy_PasswordPolicy");
	    	androidJOB.clickOnModifyJob();
	    	androidJOB.SelectingPasswordPolicyInDevice(1);
	    	androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	quickactiontoolbarpage.ClearingConsoleLogs();
			commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_PasswordPolicy");
		//	androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_PasswordPolicy");
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
	    }*/

	    @Test(priority='A',description="Verify UI to keep Wifi Always On/Off")
	    public void VerifyUITokeepWifiAlwaysOn() throws InterruptedException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.VerfyingKeepWIFIAlwaysOnOff();
			androidJOB.ClickOnSecurityPolicyJobCloseButton();
			commonmethdpage.ClickOnHomePage();
	    }
	    
/*	    @Test(priority='B',description="Verify creating and applying Security policy job with 'WiFi Always ON' in Nix v18.07 or above ")
	    public void VerifyApplyingSecurityPolicyJObWithWiFiAlwaysOn() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException
	    {
	    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiAlwaysOn");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.SelectingWifiStatus("ALWAYS_ON");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOn");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOn",Config.DeviceName);	
			quickactiontoolbarpage.SwitchingWifiStatus("OFF");
			androidJOB.VerifyWiFiStatus("1");
			Reporter.log("PASS>>> Wifi Status Is On Even After Trying To Turn Off",true);
	    }
	    @Test(priority='C',description="Verify creating and applying Security policy job with 'WiFi Always OFF' in Nix v18.07 or above")
	    public void VerifyApplyingSecurityPolicyJObWithWiFiAlwaysOff() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException
	    {
	    	

       	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiAlwaysOFF");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.SelectingWifiStatus("ALWAYS_OFF");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOFF");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOFF",Config.DeviceName);	
			quickactiontoolbarpage.SwitchingWifiStatus("ON");
			androidJOB.VerifyWiFiStatus("0");
			Reporter.log("PASS>>> Wifi Status Is On Even After Trying To Turn Off",true);
	    }*/
	    
	    @Test(priority='D',description="Verify creating and applying Security policy job with 'WiFi Dont Care' in Nix v18.07 or above")
	    public void VerifyApplyingSecurityPolicyJObWithWiFiDontCare() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	    {
	    	

	    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiDontCare");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.SelectingWifiStatus("DONT_CARE");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_WiFiDontCare");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiDontCare",Config.DeviceName);	
			quickactiontoolbarpage.SwitchingWifiStatus("OFF");
			androidJOB.VerifyWiFiStatus("0");
			quickactiontoolbarpage.SwitchingWifiStatus("ON");
			androidJOB.VerifyWiFiStatus("1");
			Reporter.log("PASS>>> Wifi Status Is Changed As Per Condition",true);
	    }

/*	    @Test(priority='E',description="Verify behaviour of Wifi after reboot with WiFi Always ON/Always OFF/Dont care")
	    public void VerifyApplyingSecurityPolicyJObWithWiFiAlwaysOnAfterReboot() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	    {
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOn");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOn",Config.DeviceName);	
			androidJOB.RebootingTheDevice();
	    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
	    	quickactiontoolbarpage.ClickOnNixSettings();
			quickactiontoolbarpage.SwitchingWifiStatus("OFF");
			androidJOB.VerifyWiFiStatus("1");
			Reporter.log("PASS>>> Wifi Status Is On Even After Trying To Turn Off After Reboot",true);
	    }
	    
	    @Test(priority='F',description="Verify different combinations of Security Policy Job along with WiFi ")
	    public void VerifyDifferentCombinationsOfSecurityPolicyJobsalongWithWIFI() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {

	    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
	        androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_WiFiAlwaysOn_DisableCamera");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.SelectingWifiStatus("ALWAYS_ON");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("12","Disable Camera");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_WiFiAlwaysOn_DisableCamera");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiAlwaysOn_DisableCamera",Config.DeviceName);	
			quickactiontoolbarpage.SwitchingWifiStatus("OFF");
			androidJOB.VerifyWiFiStatus("1");
			androidJOB.VerifyAppIsNotOpenedOnDisablingInSecurityPolicy("com.sec.android.app.camera");
	    }
	    
	    @Test(priority='G',description="Verify modifying WiFi Always On/Always Off/Dont Care in already created Security Policy Job")
	    public void VerifyModifyingWifiAlreadyCreatedSecurityJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {

	    	commonmethdpage.AppiumConfigurationCommonMethodWithUDID("com.nix","com.nix.MainFrm",Config.UDID);
	    	androidJOB.clickOnJobs();
	    	dynamicjobspage.SearchJobInSearchField("AutomationSecurityPolicy_WiFiDontCare");
	    	androidJOB.ClickOnOnJob("AutomationSecurityPolicy_WiFiDontCare");
	    	androidJOB.clickOnModifyJob();
	    	androidJOB.SwitchingToPeripheralSettingsTab();
	    	androidJOB.DisablingCheckBoxesInPeriPheralSettingsTab("12","Disable Camera");
	    	androidJOB.SelectingWifiStatus("ALWAYS_ON");
	    	androidJOB.ClickOnLocatioTrackingJobOkButton();
	    	commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_WiFiDontCare");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("AutomationSecurityPolicy_WiFiDontCare",Config.DeviceName);	
			quickactiontoolbarpage.SwitchingWifiStatus("OFF");
			androidJOB.VerifyWiFiStatus("1");		
	    }*/


	    
	    //sim card is needed for this case 
	    
	    /*@Test(priority='A',description="Security Policy Job - Verify Peripheral settings by selecting Mobile data as(Dontcare/Always On/Always OFF)")
	    public void VerifyPeriPheralSettingsBySelectingMobileDataAsAlwaysOff() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_Data_AlwaysOFF");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.SelectingSecurityPolicyParameters(androidJOB.SecurityPolicy_PeripheralMobileDataDropDown,"ALWAYS_OFF");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
			commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_Data_AlwaysOFF");
			androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_Data_AlwaysOFF");
			androidJOB.AppiumConfigurationfor3rdPartyApp("com.android.settings","com.android.settings.Settings");
			androidJOB.SearchingForOptionsInSettings("Mobile data usage");
			androidJOB.VerifyMobileDataStatusAfterApplyingSecurityPolicy_MobileData_AlwaysOFF();	
			androidJOB.ClickOnHomeButtonDeviceSide();
	    }
	    
	    @Test(priority='B',description="Security Policy Job - Verify Peripheral settings by selecting Mobile data as(Dontcare/Always On/Always OFF)")
	    public void VerifyPeriPheralSettingsBySelectingMobileDataAsDontCare() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	androidJOB.clickOnJobs();
	    	androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnJobType("security_policy");
			androidJOB.SwitchingToPeripheralSettingsTab();
			androidJOB.SendingSecurityPolicyJobName("AutomationSecurityPolicy_Data_Don'tCare");
			androidJOB.EnablingCheckBoxesInPeripheralSettigsTab("7","Enforce Peripheral Settings On Device CheckBox");
			androidJOB.SelectingSecurityPolicyParameters(androidJOB.SecurityPolicy_PeripheralMobileDataDropDown,"DONT_CARE");
			androidJOB.ClickOnLocatioTrackingJobOkButton();
			commonmethdpage.ClickOnHomePage();
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("AutomationSecurityPolicy_Data_Don'tCare");
			androidJOB.ClickOnApplyJobOkButton();
			androidJOB.JobInitiatedMessage();
			androidJOB.CheckingJobStatusUsingConsoleLogs("AutomationSecurityPolicy_Data_DontCare");
			androidJOB.AppiumConfigurationfor3rdPartyApp("com.android.settings","com.android.settings.Settings");
			androidJOB.SearchingForOptionsInSettings("Mobile data usage");
			androidJOB.VerifyMobileDataStatusAfterApplyingSecurityPolicy_MobileData_DontCare();
	    }
	    */
	    
	}
