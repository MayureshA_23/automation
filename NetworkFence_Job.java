package JobsOnAndroid;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class NetworkFence_Job extends Initialization {

	
	  @Test(priority=1, description="Verify creating network fence job with composite job along with some other jobs like install/Text message added in fence entered" ) 
	  public void CompositeJob() throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException {
	  androidJOB.clickOnJobs(); 
	  androidJOB.clickNewJob();
	  androidJOB.clickOnAndroidOS(); 
	  androidJOB.clickOnTextMessageJob();
	  androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname,"0text","Normal Text"); 
	  androidJOB.ClickOnOkButton();
	  androidJOB.JobCreatedMessage(); 
	  androidJOB.clickNewJob();
	  androidJOB.clickOnAndroidOS(); 
	  androidJOB.clickOnFileTransferJob();
	  androidJOB.enterJobName(Config.FileTransferJobName); 
	  androidJOB.clickOnAdd();
	  androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe"); 
	  androidJOB.EnterDevicePath("/sdcard/Download");
	  androidJOB.clickOkBtnOnBrowseFileWindow();
	  androidJOB.clickOkBtnOnAndroidJob();
	  androidJOB.JobCreatedMessage();
	  androidJOB.clickOnJobs();
	  androidJOB.clickNewJob();
	  androidJOB.clickOnAndroidOS(); 
	  androidJOB.ClickOnJobType("comp_job");
	  androidJOB.CompositeJobName(Config.CompositeJobName);
	 androidJOB.ClickOnAddButtonCompositeJob();
	 androidJOB.SearchJobForCompositeAction(Config.TextMessageJobname);
	 androidJOB.ClickOnAddButtonCompositeJob();
	 androidJOB.SearchJobForCompositeAction(Config.FileTransferJobName);
	  androidJOB.ClickOnOnOkButtonAndroidJob(); 
	  androidJOB.JobCreatedMessage();
	  commonmethdpage.ClickOnHomePage();

//	  androidJOB.SelectingJobToAddOutOfComplianceAction(Config.TextMessageJobname);

//	  androidJOB.SelectingJobToAddOutOfComplianceAction(Config.FileTransferJobName) ; 
	  
	  } 
	 
	@Test(priority =2, description = "Verify creating network fence job with composite job along with some other jobs like install/Text message added in fence entered")
	public void TC_JO_803()throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException {

		
		  androidJOB.clickOnJobs(); 
		  androidJOB.clickNewJob();
		  androidJOB.clickOnAndroidOS(); 
		  deviceinfopanelpage.ClickOnNetworkFence();
		  deviceinfopanelpage.ClickOnEnableNetwork(false);
		  deviceinfopanelpage.EnterWifiSSID(Config.WifiSSID);
		  deviceinfopanelpage.ClickOnNetworkFenceEnteredTab();
		  deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		  deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		  deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
		  deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		  deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		  deviceinfopanelpage.SearchFenceJob(Config.FileTransferJobName);
		  deviceinfopanelpage.SelectFenceJob(Config.FileTransferJobName);
		  deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		  deviceinfopanelpage.CheckNetworkFenceEnteredDeviceAlert(false);
		  deviceinfopanelpage.CheckNetworkFenceEnteredMDMAlert(false);
		  deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceEntered(false,"complianceEmailID@mailinator.com");
		  deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		  deviceinfopanelpage.EnterJobnameNetworkFenceOkButton("NetworkFenceJobEntry");
		 
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(); // ANDROID /WINDOWS
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("NetworkFenceJobEntry");
//		androidJOB.Selectjob("NetworkFenceJobEntry");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJob("NetworkFenceJobEntry", Config.DeploymentTime);
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice();
		deviceinfopanelpage.VerifyNetworkFenceStatus("ON");

		deviceinfopanelpage.VerifyNetworkFenceEntryAlertForDevice(Config.DeviceName, "Network fence", 250);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxEntryMessageNetworkFence();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Network fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com", "complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();

	}

	@Test(priority = 3, description = "Verify creating network fence job with composite job along with some other jobs like install/Text message added in fence exited")
	public void TC_JO_804()throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException {

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnNetworkFence();
		deviceinfopanelpage.ClickOnEnableNetwork(false);
		deviceinfopanelpage.EnterWifiSSID(Config.WifiSSID2);
		deviceinfopanelpage.ClickOnNetworkFenceExiedTab();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob(Config.FileTransferJobName);
		deviceinfopanelpage.SelectFenceJob(Config.FileTransferJobName);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceExited(false, "complianceEmailID@mailinator.com");
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.EnterJobnameNetworkFenceOkButton("NetworkFenceJobExit");

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(); // ANDROID /WINDOWS
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("NetworkFenceJobExit");
//		androidJOB.Selectjob("NetworkFenceJobExit");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJob("NetworkFenceJobExit", Config.DeploymentTime);
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice();
		deviceinfopanelpage.VerifyNetworkFenceStatus("ON");

		deviceinfopanelpage.VerifyNetworkFenceExitAlertForDevice(Config.DeviceName, "Network fence", 60);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessageNetworkFence();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Network fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com", "complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();

	}
	@Test(priority=4, description="Network Fence -Verify creating network fence job with composite job added in both fence entered and fence exited")
	public void TC_JO_798() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException
	{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnNetworkFence();
		deviceinfopanelpage.ClickOnEnableNetwork(false);
		deviceinfopanelpage.EnterWifiSSID(Config.WifiSSID);
		deviceinfopanelpage.ClickOnNetworkFenceEnteredTab(); 
		deviceinfopanelpage.ClickOnNetworkFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceEntered(false,"complianceEmailID@mailinator.com");
		
		deviceinfopanelpage.ClickOnNetworkFenceExiedTab();
		deviceinfopanelpage.ClickOnNetworkFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckNetworkFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckNetworkFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForNetworkFenceExited(false,"complianceEmailID@mailinator.com");
		deviceinfopanelpage.ClickedOnNetworkFenceJobSave();
		deviceinfopanelpage.EnterJobnameNetworkFenceOkButton("NetworkFenceJob");
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnDynamicRefresh();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("NetworkFenceJob");
//		androidJOB.Selectjob("NetworkFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJob("NetworkFenceJob", Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.VerifyNetworkFenceStatus("ON");
		
		deviceinfopanelpage.VerifyNetworkFenceEntryAlertForDevice(Config.DeviceName, "Network fence", 600);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
//		deviceinfopanelpage.ClickOnMailBox();
//		deviceinfopanelpage.VerifyTextMessage("Subject: Testing");
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.ClickOnInbox();
		deviceinfopanelpage.InboxEntryMessageNetworkFence();
      deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Network fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com", "complianceEmailID@mailinator.com");
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
	//	deviceinfopanelpage.SelectDifferentNetwork("noni", "Noni 5"); 
		deviceinfopanelpage.SelectDifferentNetwork(Config.WifiSSID,Config.WifiSSID2); 
    	commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		deviceinfopanelpage.VerifyNetworkFenceExitAlertForDevice(Config.DeviceName, "Network fence", 600);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessageNetworkFence();
      deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Network fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com", "complianceEmailID@mailinator.com");
	}
}
