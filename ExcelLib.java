package Library;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelLib {
	String filepath ="D:\\Automation\\Com.42Gears.SureMDM\\uploads\\DataFile.xlsx";
	

	public String getDatafromExcel(String sheetno,int rowno,int colno) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		FileInputStream fis = new FileInputStream(filepath);
		Workbook wb= WorkbookFactory.create(fis);
		Sheet sh= wb.getSheet(sheetno);
		DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
		Cell cell =sh.getRow(rowno).getCell(colno);
		String Data = formatter.formatCellValue(cell);
		return Data;
	}

//	public void setExcelData(String sheetName, int rowno, int colno, String data) throws EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		FileInputStream fis = new FileInputStream(filepath);
//		Workbook wb= WorkbookFactory.create(fis);
//		Sheet sh= wb.getSheet(sheetName);
//	
//		Row row =sh.getRow(rowno);
//		 Cell cell	=row.createCell(colno);
//		
//		FileOutputStream fos=new FileOutputStream(filepath);
//		cell.setCellValue(data);
//		wb.write(fos);
//		wb.
//	}
//

}
