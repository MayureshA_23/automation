package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ComplianceJobs extends Initialization
{
	///SnapChat Application & SMS Pop Up application  Should Be Installed In The Device
	@Test(priority=0,description="Job Creation")
    public void CreatingJobs() throws InterruptedException, IOException
    {
            androidJOB.clickOnJobs();
            androidJOB.clickNewJob();
            androidJOB.clickOnAndroidOS();
            androidJOB.clickOnTextMessageJob();
            androidJOB.textMessageJobWhenNoFieldIsEmpty("TextMessageDontDelete", "0text","Normal Text");
            androidJOB.ClickOnOkButton();
            androidJOB.JobCreatedMessage();
            androidJOB.clickNewJob();
            androidJOB.clickOnAndroidOS();
            androidJOB.clickOnTextMessageJob();
            androidJOB.textMessageJobWhenNoFieldIsEmpty("AutomationDontDeleteTextMessage", "0text","Normal Text");
            androidJOB.ClickOnOkButton();
            androidJOB.JobCreatedMessage();
            androidJOB.clickNewJob();
            androidJOB.clickOnAndroidOS();
            androidJOB.clickOnFileTransferJob();
            androidJOB.enterJobName("AutomationDonotDeleteFTJob");
            androidJOB.clickOnAdd();
            androidJOB.browseFile();
            androidJOB.clickOkBtnOnBrowseFileWindow();
            androidJOB.clickOkBtnOnAndroidJob();
            androidJOB.verifyFileTransferJobCreation("AutomationDonotDeleteFTJob");
            androidJOB.UploadcompleteStatus();        
    }
	
	@Test(priority=1,description="Compliance job- To verify blacklisted apps for send message action.(immediately)")
	public void VerifyBlackListedAppForSendMessageAction() throws Throwable
	{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppForSendMessageAction");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Send Message");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.clickOnGridrefreshbutton();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppForSendMessageAction"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppForSendMessageAction",600);
		androidJOB.VerifyTextMessageWhenDeviceIsOutOfCompliance();
	//	quickactiontoolbarpage.ClickOnDeviceLogs();
	//	androidJOB.VerifyDeviceLogsAfterApplyingOutOfComplianceJob();
	}

	@Test(priority=2,description="Compliance job- To verify blacklisted apps for Move to blacklist action.(Immediately)")
	public void VerifyBlackListedAppsForMoveToBlackListApplication() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
			androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnComplianceJob();
			androidJOB.SendingComplianceJobName("Automation_BlackListedAppMoveToBlackListAction");
			androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
			androidJOB.ClickonConfigureButton();
			androidJOB.ClickOnaddButtonInsideComplianceJob();
			androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
			androidJOB.ClickOnComplianceJobPackageNameOKButton();
			androidJOB.outOfComplianceActions("Move to Blocklist");
			androidJOB.Delay_OutOfComplianceActions("Immediately");
			androidJOB.ClickOnComplianceJobSaveButton();
			commonmethdpage.ClickOnHomePage();
			commonmethdpage.SearchDevice();
	    	commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("Automation_BlackListedAppMoveToBlackListAction"); 
			androidJOB.JobInitiatedMessage(); 
	        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppMoveToBlackListAction",600);
			androidJOB.VerifyDevicePresenceInsideBlackListesSection();
			androidJOB.WhiteListingDevice();
			commonmethdpage.ClickOnHomePage();
		}
	

	@Test(priority=3,description="Compliance job- To verify blacklisted apps for Lock device action.(Immediately)")
	public void VrifyBlackListedAppsForLockDeviceAction() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppLockDeviceAction");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Lock Device (Android / iOS/iPadOS)");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppLockDeviceAction"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppLockDeviceAction",600);
		androidJOB.VerifyIsDeviceLocked();
		dynamicjobspage.UnLockingDeviceUsingADB();

	}

	@Test(priority=4,description="Compliance job- To verify blacklisted apps for Email notification.(Immediately)")
	public void VerifyBlackListedAppsForEmailnotification() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppEmailNotificationAction");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("E-Mail Notification");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.SendingEmailID(Config.NotifiedEmail_Misc);
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppEmailNotificationAction"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppEmailNotificationAction",600);
		androidJOB.VerifyEmailNotification("https://www.mailinator.com",Config.NotifiedEmail_Misc);
//		androidJOB.VerifyEmailAlertForAppBlackList();

	}

	@Test(priority=5,description="Compliance job- To verify blacklisted apps for Apply job action.(Immediately)")
	public void VerifyBlackListedAppsForApplyJobAction() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		androidJOB.CreatingInstallJobForOutOfCompliance(Config.OutOFComplianceJobName);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppApplyJobAction");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Apply Job");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.OutOFComplianceJobName);
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppApplyJobAction"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppApplyJobAction",600);
		androidJOB.VerifyJobDeployedInDevice();		

	}

	/*@Test(priority=6,description="Compliance job- To verify blacklisted apps for Send SMS action.(Immediately)")
	public void VerifyBlackListedAppsForSendSMSAction() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		androidJOB.AppiumFor3rdPartyApplication("net.everythingandroid.smspopup", "net.everythingandroid.smspopup.ui.SmsPopupConfigActivity");
		androidJOB.clickOnJobs();
  		androidJOB.clickNewJob();
  		androidJOB.clickOnAndroidOS();
  		androidJOB.ClickOnComplianceJob();
  		androidJOB.SendingComplianceJobName("Automation_BlackListedAppSendSMSAction");
  		androidJOB.SelctingComplianceJobsRule("Blacklisted Applications");
  		androidJOB.ClickonConfigureButton();
  		androidJOB.ClickOnaddButtonInsideComplianceJob();
  		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
  		androidJOB.ClickOnComplianceJobPackageNameOKButton();
  		androidJOB.outOfComplianceActions("Send Sms");
  		androidJOB.Delay_OutOfComplianceActions("Immediately");
  		androidJOB.SendingphoneNumber(Config.OutOfCompliancePhoneNumber);
  		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppSendSMSAction");
		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckingJobStatusUsingConsoleLogs("Automation_BlackListedAppSendSMSAction");
		androidJOB.AppiumFor3rdPartyApplication("net.everythingandroid.smspopup", "net.everythingandroid.smspopup.ui.SmsPopupConfigActivity");
		androidJOB.VerifySMSNotificationInDevice();
	}*/

	@Test(priority=7,description="Compliance job- To verify blacklisted apps for Uninstall app action.(Immediately)")
	public void VerifyBlackListedAppsForUninstallAppAction() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppUninstallAppAction");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancepackageName_Uninstall);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Uninstall App");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppUninstallAppAction"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppUninstallAppAction",600);
		androidJOB.VerifyBlackListAppUninstallation();

        }

	@Test(priority=8,description="Compliance job- To verify blacklisted apps by not entering any package id.")
	public void VerifyBlackListedAppsByNotEnteringAnyPackageID() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
  		androidJOB.clickNewJob();
  		androidJOB.clickOnAndroidOS();
  		androidJOB.ClickOnComplianceJob();
  		androidJOB.SendingComplianceJobName("Automation_BlackListedAppSendSMSAction_WithoutPackageName");
  		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
  		androidJOB.ClickonConfigureButton();
  		androidJOB.outOfComplianceActions("Send Message");
  		androidJOB.Delay_OutOfComplianceActions("Immediately");
  		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppSendSMSAction_WithoutPackageName"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppSendSMSAction_WithoutPackageName",600);
		androidJOB.VerifyNoActionSinceNoPackagenameIsEmpty();

	}

	/*@Test(priority=9,description="Compliance job- To verify blacklisted apps by adding the same out of compliance action")
	public void VerifyBlackListedAppsByAddingDuplicateOutOfComplianceAction() throws InterruptedException
	{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppSendSMSAction_WithoutPackageName");
		androidJOB.SelctingComplianceJobsRule("Blacklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Wipe the Device");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.AddingActionInComplianceBlackListedApplication("Wipe the Device","Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
	}*/
	
	@Test(priority='9',description="Compliance job-Modify the already created Blacklisted Compliance job")
	public void ModifyAlreadyCreatedBlacklistedComplianceJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("Automation_BlackListedAppSendSMSAction_WithoutPackageName");
		androidJOB.ClickOnOnJob("Automation_BlackListedAppSendSMSAction_WithoutPackageName");
		androidJOB.clickOnModifyJob();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob(Config.CompliancePackageName);
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.SendingComplianceJobName("Automation_ModifiedComplianceJob");
		androidJOB.ClickOnComplianceJobSaveButtonAfterModifyingJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_ModifiedComplianceJob"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_ModifiedComplianceJob",600);
		androidJOB.VerifyTextMessageWhenDeviceIsOutOfCompliance();

	//	androidJOB.VerifyDeviceLogsAfterApplyingOutOfComplianceJob();
	}
	
	@Test(priority='A',description="Compliance job- To verify blacklisted apps by adding the package name of an applictaion which is set as device admin")
	public void VerifyBlacklistedAppsByAddingPackageSetAsDeviceAdmin() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob("com.nix");
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Uninstall App");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin"); 
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin",600);
		androidJOB.VerifyUninstallationOfApplicaatioSetAsDeviceAdmin();

	}
    @Test(priority='B',description="Compliance job- To verify Blocklisted apps by adding the package name of an applictaion which is set as device admin")
    public void VerifyBlocklistedAppsByAddingPackageSetAsDeviceAdmin() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SendingComplianceJobName("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin");
		androidJOB.SelctingComplianceJobsRule("Blocklisted Applications");
		androidJOB.ClickonConfigureButton();
		androidJOB.ClickOnaddButtonInsideComplianceJob();
		androidJOB.SendingPackageNameInComplianceJob("com.nix");
		androidJOB.ClickOnComplianceJobPackageNameOKButton();
		androidJOB.outOfComplianceActions("Uninstall App");
		androidJOB.Delay_OutOfComplianceActions("Immediately");
		androidJOB.ClickOnComplianceJobSaveButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin");
		androidJOB.JobInitiatedMessage(); 
        androidJOB.CheckStatusOfappliedInstalledJob("Automation_BlackListedAppUninstallAppActionSetAsDeviceAdmin",600);
        androidJOB.VerifyUninstallationOfApplicaatioSetAsDeviceAdmin();
	}


}
