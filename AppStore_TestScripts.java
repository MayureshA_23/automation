package SanityTestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class AppStore_TestScripts extends Initialization {

	@Test(priority=1,description="Validate App Store-Android,iOS")
	public void AddAppInApplicationPolicy() throws Throwable
	{
		Reporter.log("1.Validate App Store-Android,iOS",true);
		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreADDApplication.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		commonmethdpage.ClickOnHomePage();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("CreationApplicationPolicy");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("CreationApplicationPolicy");
		commonmethdpage.Selectjob("CreationApplicationPolicy");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("CreationApplicationPolicy",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		profilesAndroid.VerifyAppInstall("SMS Popup");
		androidJOB.unInstallApplication("net.everythingandroid.smspopup");
		profilesAndroid.ClickOnProfiles();
		appstorepage.DeleteCreatedProfile("CreationApplicationPolicy");
		
	}
}
