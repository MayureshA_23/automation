package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.gargoylesoftware.htmlunit.javascript.host.media.webkitAudioContext;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.LongPressOptions;

public class AndroidJobPOM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();
	ExcelLib ELib = new ExcelLib();
	Actions action = new Actions(Initialization.driver);

	@FindBy(xpath = "//*[@id='job_install_name_input']")
	private WebElement jobName;

	@FindBy(xpath = "//*[@id='job_name']")
	private WebElement EnterjobName;

	@FindBy(xpath = ".//*[@id='install_addBtn']/span")
	private WebElement addBtn;

	@FindBy(xpath = ".//*[@id='commonModalSmall']/div")
	private WebElement fileTransferPrompt;

	@FindBy(xpath = ".//*[@id='commonModalSmall']/div/div/div[1]/h4")
	private WebElement fileTransferWindow;

	@FindBy(xpath = ".//*[@id='job_filepath_input']")
	private WebElement filePathURL;

	@FindBy(xpath = ".//*[@id='browseFiles']")
	private WebElement fileBrowseBtn;

	@FindBy(xpath = ".//*[@id='job_devicepath_input']")
	private WebElement devicePath;

	@FindBy(xpath = "(.//*[@id='okbtn'])[2]")
	private WebElement okBtnOnFileTransfer_JobWindow;

	@FindBy(xpath = "//*[@id='okbtn']")
	private WebElement okBtnOnFileTransferJobWindow;

	@FindBy(xpath = "(//button[@id='okbtn'])[1]")
	private WebElement OkBtnCopy;

	@FindBy(xpath = ".//*[@id='install_deleteBtn']/span")
	private WebElement deleteJob;

	@FindBy(xpath = ".//*[@id='install_modBtn']/span")
	private WebElement modifyJob1;

	@FindBy(xpath = ".//*[@id='ok_btn_modified']")
	private WebElement okBtnOnFileTransferJobUpdate;

	@FindBy(xpath = "//span[text()='Job name cannot be empty']")
	private WebElement errorWithEmptyFields;

	@FindBy(xpath = "//li[text()='Please fill out this field.']")
	private WebElement errorWhenNoFileUploaded;

	@FindBy(xpath = ".//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='0FileTransfer']")
	private WebElement createdFileTransferJob;

	@FindBy(xpath = ".//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='UpdatedFileTransferJob']")
	private WebElement updatedFileTransferJob;

	@FindBy(xpath = ".//*[@id='job_modify']")
	private WebElement modifyJob;

	@FindBy(xpath = ".//*[@id='jobSection']/a")
	private WebElement jobsTab;

	@FindBy(xpath = ".//*[@id='job_new_job']/span")
	private WebElement newJobOption;

	@FindBy(xpath = ".//*[@id='jobDataGrid']/tbody/..//p[text()='sandeep']")
	private WebElement testfolder;

	@FindBy(xpath = ".//*[@id='andIcon']")
	private WebElement androidOS;

	@FindBy(xpath = ".//*[@id='file_transfer']/span")
	private WebElement fileTransfer;

	public void enterJobName(String name) throws InterruptedException {
		try {
			sleep(2);
			jobName.clear();
			sleep(2);
			jobName.sendKeys(name);
			Reporter.log("PASS >> Entered Job Name", true);
			sleep(2);
		} catch (Exception e) {
			sleep(2);
			EnterjobName.clear();
			sleep(2);
			EnterjobName.sendKeys(name);
			Reporter.log("PASS >> Entered Job Name", true);
		}
	}

	public void clickOnJobs() throws InterruptedException {
		sleep(3);
		jobsTab.click();
		Reporter.log("PASS >> Clicked on Jobs", true);
		waitForidPresent("job_new_job");
		sleep(5);
	}

	public void ClearJobName() throws InterruptedException {
		jobName.clear();
		Reporter.log("Cleared Job Name", true);
		sleep(2);
	}

	public void goToTestFolder() throws InterruptedException {
		Actions action = new Actions(Initialization.driver);
		action.doubleClick(testfolder);
		action.build().perform();
		Thread.sleep(5000);
		Reporter.log("Navigated to Test Folder", true);
	}

	public void clickNewJob() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement newJobOption = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("job_new_job")));
		newJobOption.click();
		sleep(3);
		Reporter.log("PASS >> Clicked on New Job", true);
	}

	public void SelectDelayForCompliance(String elementIndex, String SelectValue) throws InterruptedException {
		WebElement delayDropDwn = Initialization.driver
				.findElement(By.xpath("(//select[@class='sc-act-delayUnit delayUnit_sel form-control ct-select-ele'])["
						+ elementIndex + "]"));
		Select outOfCompliancedelayDropDwn = new Select(delayDropDwn);
		outOfCompliancedelayDropDwn.selectByVisibleText(SelectValue);

	}

	public void VerifyEmailNotification(String emailURL, String ComplianceEmailID)
			throws InterruptedException, AWTException {
		JavascriptExecutor jse = (JavascriptExecutor) Initialization.driver;
		jse.executeScript("window.open()");
		String Mainwindow = Initialization.driver.getWindowHandle();
		Initialization.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		Initialization.driver.get(emailURL);
		sleep(5);
		Initialization.driver.findElement(By.xpath("//input[@id='addOverlay']")).sendKeys(ComplianceEmailID);
		sleep(3);

		Actions action = new Actions(Initialization.driver);
		action.sendKeys(Keys.ENTER).build().perform();

		waitForXpathPresent("//table[@class='table-striped jambo_table']/tbody/tr[1]/td[2]");
		sleep(5);
		String MailinatorFromField = Initialization.driver
				.findElement(By.xpath("//table[@class='table-striped jambo_table']/tbody/tr[1]/td[2]")).getText();
		String MailinatorReceivedTime = Initialization.driver
				.findElement(By.xpath("//table[@class='table-striped jambo_table']/tbody/tr[1]/td[4]")).getText();
		if (MailinatorFromField.contains("suremdm_notification@42gears.com")
				|| MailinatorFromField.contains("42g.200@gmail.com")) {
			if (MailinatorReceivedTime.contains("moments ago") || MailinatorReceivedTime.contains("just now")
					|| MailinatorReceivedTime.contains("minute ago") || MailinatorReceivedTime.contains("2 minutes ago")
					|| MailinatorReceivedTime.contains("5 min ago") || MailinatorReceivedTime.contains("3 min ago")
					|| MailinatorReceivedTime.contains("min ago") || MailinatorReceivedTime.contains("just now ago"))

			{
				Reporter.log("PASS>>> Received Mail Notification Successfully", true);
				Initialization.driver.close();
				Initialization.driver.switchTo().window(Mainwindow);
			} else {
				Initialization.driver.close();
				Initialization.driver.switchTo().window(Mainwindow);
				ALib.AssertFailMethod("FAIL>>> No Mail Notification Is Received");
			}
		}

	}

	@FindBy(xpath = ".//*[@id='app_settings_job_type']/option[text()='Show Apps']")
	private WebElement ShowApps;
	@FindBy(xpath = ".//*[@id='app_settings_job_type']/option[text()='Hide Apps']")
	private WebElement HideApps;

	public void ShowAppsJobType(String ApplicationType) throws InterruptedException {
		ApplicationTypes.click();
		sleep(2);
		Initialization.driver
				.findElement(By.xpath(".//*[@id='app_settings_job_type']/option[text()='" + ApplicationType + "']"))
				.click();
		sleep(2);
		ApplicationTypes.click();
		sleep(2);
	}

	public void Selectjob(String job) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//p[text()='" + job + "']")).click();
		System.out.println("Job Selected to apply");
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='logContent']/ul/li[1]/p")
	private WebElement FirstLog;
	@FindBy(id = "applyJob_okbtn")
	private WebElement ApplyButtonJobWindow;

	public void ClickOnApplyButtonApplyJobWindow() throws InterruptedException {
		ApplyButtonJobWindow.click();
		System.out.println("Job Applied on the device");
		sleep(4);
	}

	public void clickOnJobCompletedTab() throws InterruptedException {
		JobHistoryTab.click();
		System.out.println("Clicked on Job Completed Tab");
		waitForXpathPresent("//div[@id='jobQueueHistoryRefreshBtn']");
		sleep(6);

	}

	public void SearchJob(String name) throws InterruptedException {
		SearchTextBoxApplyJob.sendKeys(name);
		waitForXpathPresent("//p[text()='" + name + "']");
		System.out.println("Job is searched");
		sleep(2);
		Initialization.driver
				.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + name + "']"))
				.click();
		sleep(2);
	}

	public void ClickonJobQueue() throws InterruptedException {
		JobQueueButton.click();
		waitForXpathPresent("//div[@id='jobQueueRefreshBtn']");
		sleep(3);
	}

	public void CheckStatusOfappliedInstalledJobTimeFence(String JobName, int MaximumTime) throws InterruptedException {
		ClickonJobQueue();
		try {

			WebDriverWait wait1 = new WebDriverWait(Initialization.driver, MaximumTime);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='No Pending jobs']")))
					.isDisplayed();
			Reporter.log("PASS >> Job Deployed Successfully On Device", true);
			sleep(3);
			clickOnJobCompletedTab();
			System.out.println("Clicked On Job Completed Tab");
			String LatestJobName = LatestJobNameInsideHistory.getText();
			if (LatestJobName.equals(JobName)) {
				VerifyJobStatusInsideJobQueue();
			} else {
				ALib.AssertFailMethod("FAIL >> Job Name Is Different In Job Queue");

			}

		} catch (Exception e) {
			String CurrentIncompletedJobStatus = IncompleteJobStatus.getText();
			System.out.println("Incompleted Job Status is: " + CurrentIncompletedJobStatus);
			ALib.AssertFailMethod("FAIL >> Job Did not deployed within given 25 mins time");
		}

	}

	public void makeDeviceOnLine() throws Throwable {
		sleep(1);
		Runtime.getRuntime().exec("adb shell svc data enable");
		sleep(4);
		Runtime.getRuntime().exec("adb shell svc wifi enable");
		ClickOnRefreshButton();
		sleep(100);
		ClickOnRefreshButton();
	}

	@FindBy(id = "refreshButton")
	private WebElement RefreshButton;

	@FindBy(id = "homeSection")
	private WebElement homeButton;

	public void ClickOnRefreshButton() throws Throwable {
		homeButton.click();
		RefreshButton.click();
		while (Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed()) {
		}
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		Reporter.log("Clicked on Refresh Button");
	}

	public void makeDeviceOffForGivenTime(int DurationOfOffline) throws Throwable {
		sleep(1);
		Runtime.getRuntime().exec("adb shell svc data disable");
		sleep(4);
		Runtime.getRuntime().exec("adb shell svc wifi disable");
		ClickOnRefreshButton();
		sleep(DurationOfOffline);
	}

	@FindBy(id = "offline_timeVal")
	private WebElement offline_timeVal;

	public void ComplianceDurationDeviceShouldBeoffLine(String duration) {
		offline_timeVal.clear();
		offline_timeVal.sendKeys(duration);
	}

	@FindBy(xpath = "//span[text()='Online Device Connectivity']")
	private WebElement OnlineDeviceConnectivity;

	public void ClickOnOnlineDeviceConnectivity() throws InterruptedException {
		OnlineDeviceConnectivity.click();
		sleep(2);
	}

	public void addComplianceJob(String ElementId) {
		waitForXpathPresent(
				"//*[@id='" + ElementId + "']/div[2]/div[2]/div[2]/div[2]/div[1]/div[5]/div/div/span[1]/button");
		WebElement addComplianceJob = Initialization.driver.findElement(By.xpath(
				"//*[@id='" + ElementId + "']/div[2]/div[2]/div[2]/div[2]/div[1]/div[5]/div/div/span[1]/button"));
		addComplianceJob.click();
	}

	@FindBy(xpath = "//*[@id='saveBtn']")
	private WebElement SaveButton;

	public void ClickOnSaveButtonJobWindow() throws InterruptedException {
		SaveButton.click();
		sleep(6);
	}

	public void complianceDelay(int delaytime) throws InterruptedException {
		sleep(delaytime);
	}

	@FindBy(xpath = "//*[@id='conmptableContainer']/div[1]/div[1]/div[2]/input")
	private WebElement complianceSearchJob;

	@FindBy(xpath = "//*[@id='okbtn']")
	private WebElement OkComplianceApplyJob;

	public void SearchFieldComplianceJob(String JobName) throws InterruptedException {
		sleep(1);
		complianceSearchJob.sendKeys(JobName);
		sleep(10);
		WebElement JobSelected = Initialization.driver
				.findElement(By.xpath("//*[@id='CompJobDataGrid']/tbody/tr[1]/td[2]/p"));
		sleep(8);
		JobSelected.click();
		sleep(2);
		OkComplianceApplyJob.click();
		sleep(4);
	}

	public void verifyComplianceApplyJob() {
		if (Initialization.driverAppium.findElementById("com.nix:id/textViewBody").getText().equals("ComplianceJob")) {
			Reporter.log("Compliance Apply Job has been Applied sucessfully", true);
		} else {
			ALib.AssertFailMethod("Compliance Apply Job has been Failed");
		}
	}

	@FindBy(xpath = "//*[@id='mailGrid']/tbody/tr/td[3]/span")
	private WebElement firstMail;

	public void ClickOnFirstEmail() throws InterruptedException {
		firstMail.click();
	}

	public void verifyOutOfComplianceMsgOnDevice() throws InterruptedException {
		String NonConplianceMsgDeviceSide = Initialization.driverAppium.findElementById("com.nix:id/textViewBody")
				.getText();
		sleep(10);
		String expectedMsg = "Device(" + Config.DeviceName + ") is non compliant with";
		if (NonConplianceMsgDeviceSide.contains(expectedMsg)) {
			Reporter.log("Device is out of compliance", true);
		} else {
			ALib.AssertFailMethod("Device Out of compliance msg is not received on device");
		}
	}

	@FindBy(xpath = "//*[@id='simpleText_messCover']/textarea")
	private WebElement FirstMsg;

	public void readFirstMsg() throws InterruptedException {
		String Consolereadmsg = FirstMsg.getText();
		String expectedmessage = "Device(" + Config.DeviceName + ") is non compliant";
		if (Consolereadmsg.contains(expectedmessage)) {
			Reporter.log("Device is out of compliance", true);
		} else {
			ALib.AssertFailMethod("Device Out of compliance msg is not received on Console ");
		}
	}

	public void outOfComplianceActions(String complianceAction, int indexforOutOfComplianceActions, String ElementId)
			throws InterruptedException {
		WebElement OutOfComplianceActionsDropDownMenu = Initialization.driver.findElement(
				By.xpath("(//select[@id='ComplianceActionOption'])[" + indexforOutOfComplianceActions + "]"));
		Select outOfCompliance = new Select(OutOfComplianceActionsDropDownMenu);
		outOfCompliance.selectByVisibleText(complianceAction);
		if (outOfCompliance.getFirstSelectedOption().getText().equalsIgnoreCase("E-Mail Notification")) {
			sleep(1);
			WebElement EnterEmailID = Initialization.driver.findElement(
					By.xpath("//*[@id='" + ElementId + "']/div[2]/div[2]/div[2]/div[2]/div[1]/div[5]/div/div/input"));
			EnterEmailID.sendKeys("complianceEmailID@mailinator.com");
		}

		if (outOfCompliance.getFirstSelectedOption().getText().equalsIgnoreCase("Send Sms")) {
			sleep(1);
			WebElement enterMobileNumber = Initialization.driver.findElement(
					By.xpath("//*[@id='" + ElementId + "']/div[2]/div[2]/div[2]/div[2]/div[1]/div[5]/div/div/input"));
			enterMobileNumber.sendKeys("+916366231496");
		}

	}

	@FindBy(id = "androidfirst")
	private WebElement androidfirst;

	public void SelectAndriodVersionFirst(String firstAndroidVersion) throws InterruptedException {
		Select outOfCompliance = new Select(androidfirst);
		outOfCompliance.selectByVisibleText(firstAndroidVersion);
	}

	@FindBy(id = "androidsecond")
	private WebElement androidsecond;

	public void SelectAndriodVersionSecond(String secondAndroidVersion) throws InterruptedException {
		Select outOfCompliance = new Select(androidsecond);
		outOfCompliance.selectByVisibleText(secondAndroidVersion);
	}

	@FindBy(xpath = "//*[@id='comp_rules_list']/li[1]/a/span[2]")
	private WebElement OsVersionCompliance;

	public void clickOnOsVersionCompliance() throws InterruptedException {
		OsVersionCompliance.click();
	}

	public void ClickOnConfigureButton(String ElementId) throws InterruptedException { // ConfigureButton
		Initialization.driver.findElement(By.xpath("//*[@id='" + ElementId + "']/div[1]/div/button")).click();
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='devicestorage_percent']")
	private WebElement devicestorage_percent;

	public void devicestorage_percent(String EnterStoragePercent) throws InterruptedException {
		devicestorage_percent.clear();
		sleep(1);
		devicestorage_percent.sendKeys(EnterStoragePercent);
		sleep(2);
	}

	public void VerifyTextMessageWhenDeviceIsOutOfComplianceDeviceStorage() {
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, 40);

		try {
			wait.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains(@text,'Device("
							+ Config.DeviceName + ") is non compliant with Device Storage rule')]")));
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Device("
					+ Config.DeviceName + ") is non compliant with Device Storage rule')]").isDisplayed();
			Reporter.log("PASS>>> Out OF Compliance Message Is Displayed Successfully", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Out Of Complaince Message Is Not Displayed");
		}
	}

	public void OutOFComplianceDeviceStorage(String complianceAction, int indexforOutOfComplianceActions)
			throws InterruptedException {

		WebElement OutOfComplianceActionsDropDownMenu = Initialization.driver
				.findElement(By.xpath("(//div[@id='DeviceStorage_BLK']//select[@id='ComplianceActionOption'])["
						+ indexforOutOfComplianceActions + "]"));

		Select outOfCompliance = new Select(OutOfComplianceActionsDropDownMenu);
		outOfCompliance.selectByVisibleText(complianceAction);

		sleep(2);

	}

	@FindBy(xpath = "(//div[@class='action_optBtn']/button)[2]")
	private WebElement action_optBtn;

	public void ClickOnAddActionBtnDeviceStorage() throws InterruptedException {
		action_optBtn.click();
		sleep(2);
		// Select sel = new Select(OutOfComplianceActionsDropDownMenu);
		// sel.selectByVisibleText(OutOfComplianceAction);
		// Select sel1 = new Select(OutOfComplianceDelayDropDown);
		// sel1.selectByVisibleText(Delay);
	}

	public void clickOnAndroidOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("andIcon")));
		androidOS.click();
		Reporter.log("PASS >> Clicked on Android Job", true);
		sleep(4);
	}

	public void VerifyIconEverNotewhenHidden() {
		WebElement downloadericon = Initialization.driverAppium
				.findElementById("com.samsung.android.app.galaxyfinder:id/item_button");
		WebElement appname = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Evernote']");
		if (downloadericon.isDisplayed() && appname.isDisplayed()) {
			Reporter.log("PASS >> App is hidden successfully", true);
		} else {
			ALib.AssertFailMethod("FAIL >> App is not hidden");
		}
	}

	@FindBy(xpath = "//span[text()='Nix Agent Settings']")
	private WebElement NixSettingsJobsClick;

	public void ClickofNixSettingsJob() throws InterruptedException {
		NixSettingsJobsClick.click();
		Reporter.log("Clicked on NixAgentSettingsJobs", true);
		waitForXpathPresent("//span[text()='Enable time synchronization with Server']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='job_nixsettings_EnableNixPassword_input']")
	private WebElement EnableNixPasswordBox;

	@FindBy(xpath = "//*[@id='job_nixsettings_NixPassword_input']")
	private WebElement passwordInputNixAgent;

	public void Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Nix_Password()
			throws InterruptedException {
		EnterJobName.sendKeys("NixAgentEnableNixPassword");
		sleep(2);
		EnableNixPasswordBox.click();
		sleep(4);
		passwordInputNixAgent.sendKeys("0000");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='job_nixsettings_EnableScheduleReboot_input']")
	private WebElement EnableScheduleRebootNixAgentSettigsJob;

	@FindBy(xpath = "//*[@id='job_nixsettings_ScheduleReboot_time']")
	private WebElement ScheduleRebootTime;

	@FindBy(xpath = "//*[@id='schedukeReboot_time_dropdown']/div[2]/button[1]")
	private WebElement Dropdownbutton;

	public void Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Enable_Schedule_Reboot()
			throws InterruptedException, AWTException {
		Robot r = new Robot();
		EnterJobName.sendKeys("EnableScheduleRebootNixAgentSettigsJob");
		sleep(2);
		EnableScheduleRebootNixAgentSettigsJob.click();
		sleep(2);
		ScheduleRebootTime.click();
		sleep(2);

		Date date = new Date(); // Instantiate a Date object
		//System.out.println(date);
		String[] s1 = date.toString().split(" ");
		String day = s1[0];
		String[] sd1 = s1[3].split(":");
		String hour = sd1[0];

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, 5);
		String IncreasedfiveMins = cal.getTime().toString();
		System.out.println(IncreasedfiveMins);
		AddedTime = IncreasedfiveMins.substring(11, 16);
		String AddedTime2 = IncreasedfiveMins.substring(14, 16);
		System.out.println(AddedTime2);
		sleep(4);

		ScheduleRebootTime.clear();
		sleep(2);
		ScheduleRebootTime.click();
		sleep(2);
		Dropdownbutton.click();
		sleep(1);
		Dropdownbutton.click();
		sleep(1);
		WebElement ScheduleRebootTimeDropDown = Initialization.driver.findElement(By.xpath(
				"//*[@id='schedukeReboot_time_dropdown']/div[2]/div/div[1]/div[contains(text(),'" + hour + "')]"));
		JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
		je.executeScript("arguments[0].scrollIntoView()", ScheduleRebootTimeDropDown);
		sleep(4);
		ScheduleRebootTimeDropDown.click();// ScheduleRebootDropDown
		sleep(2);
		r.keyPress(KeyEvent.VK_BACK_SPACE);
		sleep(1);
		r.keyPress(KeyEvent.VK_BACK_SPACE);
		sleep(1);
		// WebElement
		// MinuteAdd=Initialization.driver.findElement(By.xpath("//*[@id='schedukeReboot_time_dropdown']/div[2]/div/div[1]/div[contains(text(),'"+minute+"')]"));
		ScheduleRebootTime.sendKeys(AddedTime2);

		sleep(1);
	}

	public void SelectDayForScheduleReboot() throws InterruptedException {
		Date date = new Date(); // Instantiate a Date object
		System.out.println(date);
		String[] s1 = date.toString().split(" ");
		String day = s1[0];
		System.out.println(day);
		// EnterJobName.click();
		sleep(1);
		// Initialization.driver.findElement(By.xpath("//*[@id='job_nixsettings_ScheduleReboot_day']/div[2]/div/span[contains(text(),'"+day+"')]")).click();
		WebElement ClickingOnDay = Initialization.driver.findElement(
				By.xpath("(//div[@class='days_list pull-left'])[1]/div/span[contains(text(),'" + day + "')]"));

		Actions actions = new Actions(Initialization.driver);
		actions.moveToElement(ClickingOnDay).click().build().perform();
		sleep(2);
	}

	public void NixAgentSettingsJobCreatedMessage() throws InterruptedException {
		OkButtonNixAgentSettings.click();
		sleep(3);
		boolean isdisplayed = true;

		try {
			JobCreatedSuccessfullyMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}

		Assert.assertTrue(isdisplayed, "FAIL >> \"Job Created successfully\"is not displayed");
		Reporter.log("PASS >> \"Job Created successfully\" is displayed", true);
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='job_nixsettings_periodicity_TimeSync']")
	private WebElement job_nixsettings_periodicity_TimeSync;

	@FindBy(xpath = "//*[@id='job_nixsettings_EnableTimeSync_input']")
	private WebElement EnableTimeSync;

	@FindBy(xpath = "//*[@id='nixAgent_modal']/div[2]/div[1]/div[1]/div/input")
	private WebElement EnterJobName;

	@FindBy(xpath = "//*[@id='okbtn']")
	private WebElement OkButtonNixAgentSettings;

	public void Verify_creating_and_deploying_Nix_agent_settings_by_enabling_Enable_time_synchronization_with_server()
			throws InterruptedException {
		Select periodicityDropdown1 = new Select(job_nixsettings_periodicity_TimeSync);
		EnterJobName.sendKeys("NixAgentEnableTimeSyncWithServer");
		sleep(2);
		EnableTimeSync.click();
		sleep(4);
		periodicityDropdown1.selectByValue("MINUTES_5");
		periodicityDropdown1.selectByIndex(1);
		sleep(2);
	}

	@FindBy(xpath = "//span[text()='Enable time synchronization with Server']")
	private WebElement EnableTimeSyncWithServer;
	@FindBy(xpath = "//span[text()='Enable Device Info update']")
	private WebElement EnablePeriodicDeviceInfoUpdate;
	@FindBy(xpath = "//span[text()='Enable Nix Password']")
	private WebElement EnableNixPassword;
	@FindBy(xpath = "//span[text()=' Enable Schedule Reboot']")
	private WebElement EnableScheduleReboot;
	@FindBy(xpath = "//span[text()='Enable Schedule Shut Down']")
	private WebElement EnableScheduleShutDown;
	@FindBy(xpath = "//span[text()='Connection Type']")
	private WebElement ConnectionType;

	public void VerifycreatingNixagentsettingsjobsUI() throws InterruptedException {
		boolean EnableTimeSyncWithServerText = EnableTimeSyncWithServer.isDisplayed();
		String pass1 = "PASS >> EnableTimeSyncWithServerText is displayed";
		String fail1 = "FAIL >> EnableTimeSyncWithServerText is not displayed";
		ALib.AssertTrueMethod(EnableTimeSyncWithServerText, pass1, fail1);
		sleep(2);
		boolean EnablePeriodicDeviceInfoUpdateText = EnablePeriodicDeviceInfoUpdate.isDisplayed();
		String pass2 = "PASS >> EnablePeriodicDeviceInfoUpdateText is displayed";
		String fail2 = "FAIL >> EnablePeriodicDeviceInfoUpdateText is not displayed";
		ALib.AssertTrueMethod(EnablePeriodicDeviceInfoUpdateText, pass2, fail2);
		sleep(2);
		boolean EnableNixPasswordText = EnableNixPassword.isDisplayed();
		String pass3 = "PASS >> EnablePeriodicDeviceInfoUpdateText is displayed";
		String fail3 = "FAIL >> EnablePeriodicDeviceInfoUpdateText is not displayed";
		ALib.AssertTrueMethod(EnableNixPasswordText, pass3, fail3);
		sleep(2);
		boolean EnableScheduleRebootText = EnableScheduleReboot.isDisplayed();
		String pass4 = "PASS >> EnableTimeSyncWithServerText is displayed";
		String fail4 = "FAIL >> EnableTimeSyncWithServerText is not displayed";
		ALib.AssertTrueMethod(EnableScheduleRebootText, pass4, fail4);
		sleep(2);
		boolean EnableScheduleShutDownText = EnableScheduleShutDown.isDisplayed();
		String pass5 = "PASS >> EnablePeriodicDeviceInfoUpdateText is displayed";
		String fail5 = "FAIL >> EnablePeriodicDeviceInfoUpdateText is not displayed";
		ALib.AssertTrueMethod(EnableScheduleShutDownText, pass5, fail5);
		sleep(2);
		boolean ConnectionTypeText = ConnectionType.isDisplayed();
		String pass6 = "PASS >> EnablePeriodicDeviceInfoUpdateText is displayed";
		String fail6 = "FAIL >> EnablePeriodicDeviceInfoUpdateText is not displayed";
		ALib.AssertTrueMethod(ConnectionTypeText, pass6, fail6);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='nixAgent_modal']/div[1]/div")
	private WebElement nixAgent_modalCloseBtn;

	public void ClickOnnixAgent_modalCloseBtn() throws InterruptedException {

		nixAgent_modalCloseBtn.click();
		sleep(2);
	}

	public void VerifyEvernoteWhenShowApps() {
		WebElement appname = Initialization.driverAppium.findElementByXPath("//android.widget.AdapterView");
		if (appname.isDisplayed()) {
			Reporter.log("PASS >> App is shown successfully", true);
		} else {
			ALib.AssertFailMethod("FAIL>>App is not shown");
		}
	}

	public void AdbCommandsForEvernoteApp() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell input keyevent 33");// e
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 50");// v
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 33");// e
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 46");// r
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 42");// n
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 43");// o
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 48");// t
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 33");// e
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 4");// back button
		sleep(4);
	}

	public void SelectJob(String JobName) throws InterruptedException {
		try {
			WebElement JobSelected = Initialization.driver.findElement(
					By.xpath("//.//*[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']"));
			JobSelected.click();
			sleep(4);
		} catch (Exception e) {
		}
	}

	public void VerifyWhetherUnInstallOptionIsDispalyed() {
		boolean flag;
		try {
			WebDriverWait wait1 = new WebDriverWait(Initialization.driverAppium, 120);
			wait1.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Uninstall']")));
			flag = true;
		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS>>App is shown successfully";
		String FailStatement = "FAIL>>App is not shown successfully";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	@FindBy(id = "applyJobButton")
	private WebElement ApplyButton;

	public void ClickOnApplyButton() throws InterruptedException {
		try {
			ApplyButton.click();
			waitForidPresent("scheduleJob");
			sleep(6);
		} catch (Exception e) {
		}
	}

	public void SwipeDevice() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell input swipe 200 900 200 300");
		System.out.println("Swipe Up device");
		sleep(6);
	}

	public void searchAppPLaystore(String value) throws InterruptedException, IOException {

		WebDriverWait wait1 = new WebDriverWait(Initialization.driverAppium, 120);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.vending:id/search_bar_hint")));
		sleep(3);
		Initialization.driverAppium.findElementById("com.android.vending:id/search_bar_hint").click();
		sleep(2);
		Initialization.driverAppium.findElementById("com.android.vending:id/search_bar_text_input").sendKeys(value);
		Runtime.getRuntime().exec("adb shell input keyevent 66");
		sleep(2);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.RelativeLayout[contains(@content-desc,'" + value + "')]").click();
	}

	public void VerifyWhetherInstallOptionIsDispalyed() {
		boolean flag;
		try {
			WebDriverWait wait1 = new WebDriverWait(Initialization.driverAppium, 120);
			wait1.until(
					ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Install']")));
			flag = true;
		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS>>App is hidden successfully";
		String FailStatement = "FAIL>>App is not hidden successfully";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	@FindBy(xpath = "//*[@id='surelock_settings']/span")
	private WebElement sureLockSettings;

	public void clickOnSureLockSettings() throws InterruptedException {
		sureLockSettings.click();
	}

	@FindBy(xpath = "//*[@id='surevideo_settings']/span")
	private WebElement surevideo_settings;

	public void clickOnsurevideo_settings() throws InterruptedException {
		surevideo_settings.click();
		waitForXpathPresent("//h4[contains(text(),'SureVideo Settings')]");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='surevideoSettings']/div/div[2]")
	private WebElement surevideoSettings;
	@FindBy(xpath = "//i[@class='fa fa-search']")
	private WebElement surevideoSettingsSearch;
	@FindBy(xpath = "//*[@id='surevideo_sett_search']")
	private WebElement surevideo_sett_search;

	public void ClickOnsurevideoSettings() throws InterruptedException {

		surevideoSettings.click();
		waitForXpathPresent("//i[@class='fa fa-search']");
		sleep(2);
		surevideoSettingsSearch.click();
		sleep(2);

	}

	public void ClickOn_surevideoSettingsSearch(String EnterSLSettings) throws InterruptedException {

		surevideo_sett_search.clear();
		sleep(2);
		surevideo_sett_search.sendKeys(EnterSLSettings);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='sureVideoScreenSaverSettings']/div/div[1]/p[1][contains(text(),'Screensaver Settings')]")
	private WebElement sureVideoScreenSaverSettings;

	public void ClickOnScreenSaverSettings() throws InterruptedException {

		sureVideoScreenSaverSettings.click();
		waitForXpathPresent("//p[contains(text(),'Enable Screensaver')]");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='enableScreenSaver']/div/div[2]/span/input")
	private WebElement enableScreenSaver;

	public void EnableScreenSaver() throws InterruptedException {

		enableScreenSaver.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='muteAudioScreenSaver']/div/div[2]/span/input")
	private WebElement muteAudioScreenSaverSV;

	public void muteAudioScreenSaverSV() throws InterruptedException {

		muteAudioScreenSaverSV.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='screen_sav_pg']/footer/div[2]/button")
	private WebElement screen_sav_pg;

	public void screen_sav_pg() throws InterruptedException {

		screen_sav_pg.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='surevideo_sett_pg']/footer/div[2]/button")
	private WebElement surevideo_sett_pg;

	public void surevideo_sett_pg() throws InterruptedException {

		surevideo_sett_pg.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='surefox_settings']/span")
	private WebElement sureFoxSettings;

	public void clickOnSureFoxSettings() throws InterruptedException {
		sleep(2);
		sureFoxSettings.click();
	}

	@FindBy(id = "DownloadXML")
	private WebElement saveAsFile;

	public void clickOnSaveAsFile() throws InterruptedException {
		waitForidPresent("DownloadXML");
		sleep(1);
		saveAsFile.click();
	}

	@FindBy(id = "EditXML")
	private WebElement editXML;

	public void clickOnEditXML() throws InterruptedException {
		sleep(10);
		editXML.click();
	}

	@FindBy(id = "AdvanceOptionBtn")
	private WebElement AdvanceOption;

	public void clickOnAdvanceOption() throws InterruptedException {
		sleep(1);
		AdvanceOption.click();
	}

	@FindBy(id = "install_if_not_installed")
	private WebElement InstallIfNotInstalledCheckBox;

	public void enableAdvanceOption() throws InterruptedException {
		waitForidPresent("install_if_not_installed");
		sleep(2);
		InstallIfNotInstalledCheckBox.click();
	}

	@FindBy(id = "s_activationCode")
	private WebElement ActivationCode;

	public void EnterActivationode() throws InterruptedException {
		ActivationCode.sendKeys("909686");
	}

	public void EnterActivationodeSureFox() throws InterruptedException {
		ActivationCode.sendKeys("567446");
	}

	@FindBy(xpath = "//*[@id='AdanvcedOptionModal']/div/div/div[3]/button")
	private WebElement SaveEnableAdvncSureLck;

	public void ClickOnSaveEnableAdvncSureLckFox() throws InterruptedException {
		SaveEnableAdvncSureLck.click();
	}

	@FindBy(id = "ThirdPartySettingsXML")
	private WebElement EditXMLTextArea;

	public void EnterEditXMLTextArea(String FilePath) throws Throwable {
		Initialization.commonmethdpage.ReadfromFile(FilePath, EditXMLTextArea);
	}

	public void ClearEditXMLTextArea() throws InterruptedException {
		sleep(2);
		waitForidPresent("ThirdPartySettingsXML");
		EditXMLTextArea.clear();
	}

	@FindBy(id = "SaveBtn")
	private WebElement EditXMLSaveButton;

	public void ClickOnEditXMLSaveBtn() throws Throwable {
		EditXMLSaveButton.click();
	}

	public void isSureLockFileDownloaded(String downloadPath, String fileName) throws InterruptedException {
		sleep(2);
		// keep clearing your download folder
		String SureLockSetting = "";
		File dir = new File(downloadPath);
		File[] filesInDownload = dir.listFiles();
		for (int i = 0; i < filesInDownload.length; i++) {
			if (filesInDownload[i].getName().contains(fileName)) {
				Reporter.log("Sure Lock Setting XML has been downloaded", true);
				SureLockSetting = "Xml File is found";
				break;

			}
		}
		if (SureLockSetting.isEmpty()) {
			ALib.AssertFailMethod("Sure Lock Setting XMl is not downloaded");
		}
	}

	public void VerifyIsFileDownloaded(String downloadPath, String fileName) throws InterruptedException {
		sleep(2);
		// keep clearing your download folder
		String FileName = "";
		File dir = new File(downloadPath);
		File[] filesInDownload = dir.listFiles();
		for (int i = 0; i < filesInDownload.length; i++) {
			if (filesInDownload[i].getName().contains(fileName)) {
				Reporter.log("Screen Shot Functionality is  working fine", true);
				FileName = "File is found";
				break;

			}
		}
		if (FileName.isEmpty()) {
			ALib.AssertFailMethod("Screen Shot Functionality is not working fine");
		}
	}

	public void verifySurelockPrompt() throws InterruptedException {
		String[] verifySureLockWebElement = { "Allowed Applications", "SureLock Settings", "Samsung Knox Settings",
				"Allowed Widgets", "Manage Shortcuts", "Phone Settings", "Multi-User Profile Settings",
				"Import / Export Settings", "About SureLock" };
		for (int i = 1; i <= verifySureLockWebElement.length; i++) {
			sleep(2);
			String SurelockPromptElement = Initialization.driver
					.findElement(By.xpath("//*[@id='sureFox_page_body_id']/ul/li[" + i + "]/div/div[2]/p")).getText();
			if (SurelockPromptElement.equals(verifySureLockWebElement[i - 1])) {
				sleep(2);
				Reporter.log(SurelockPromptElement + "Webelement is present on Surelock Prompt Setting Page", true);
			} else {
				ALib.AssertFailMethod(
						SurelockPromptElement + "Webelement is not present on Surelock Prompt Seiting Page");
			}

		}

		String[] verifySureLockPromptButton = { "Save", "Save As File", "Edit XML", "Advanced Option" };
		for (int i = 1; i <= verifySureLockPromptButton.length; i++) {
			int j = i + 1;
			// if (j==5) {
			// j=j+2;// refresh is hidden in the web page
			// }
			String SurelockPromptButton = Initialization.driver
					.findElement(By.xpath("//*[@id='Load']/../button[" + j + "]")).getText();
			if (SurelockPromptButton.equals(verifySureLockPromptButton[i - 1])) {
				Reporter.log(SurelockPromptButton + "Button is present on Surelock Prompt Setting Page", true);
			} else {
				ALib.AssertFailMethod(SurelockPromptButton + "Button is not present on Surelock Prompt Setting Page");
			}

		}
	}

	public void verifySureFoxPrompt() throws InterruptedException {
		String[] verifySureLockWebElement = { "Allowed Websites", "Blocked Website", "Manage Categories",
				"Browser Preferences", "SureFox Pro Settings", "Samsung Knox Settings", "Display Settings",
				"Import / Export Settings", "About SureFox" };
		for (int i = 1; i <= verifySureLockWebElement.length; i++) {
			sleep(2);
			String SurelockPromptElement = Initialization.driver
					.findElement(By.xpath("//*[@id='sureFox_page_body_id']/ul/li[" + i + "]/div/div[2]/p")).getText();
			if (SurelockPromptElement.equals(verifySureLockWebElement[i - 1])) {
				sleep(2);
				Reporter.log(SurelockPromptElement + " Webelement is present on SureFox Prompt Setting Page", true);
			} else {
				ALib.AssertFailMethod(
						SurelockPromptElement + " Webelement is not present on SureFox Prompt Seiting Page");
			}

		}

		String[] verifySureLockPromptButton = { "Save", "Save As File", "Edit XML", "Advanced Option" };
		for (int i = 1; i <= verifySureLockPromptButton.length; i++) {
			int j = i + 1;
			if (j == 5) {
				j = j + 2;// refresh is hidden in the web page
			}
			String SurelockPromptButton = Initialization.driver
					.findElement(By.xpath("//*[@id='Load']/../button[" + j + "]")).getText();
			if (SurelockPromptButton.equals(verifySureLockPromptButton[i - 1])) {
				Reporter.log(SurelockPromptButton + "Button is present on SureFox Prompt Setting Page", true);
			} else {
				ALib.AssertFailMethod(SurelockPromptButton + "Button is not present on SureFox Prompt Setting Page");
			}

		}
	}

	@FindBy(id = "AllowedApplications")
	private WebElement allowedApplications;

	public void clickOnAllowedApplications() throws InterruptedException {
		waitForidPresent("AllowedApplications");
		allowedApplications.click();
	}

	@FindBy(id = "surefoxAllowedWebsite")
	private WebElement surefoxAllowedWebsite;

	public void clickOnsurefoxAllowedWebsite() throws InterruptedException {
		waitForidPresent("surefoxAllowedWebsite");
		surefoxAllowedWebsite.click();
	}

	@FindBy(xpath = "//button[text()='Add URL']")
	private WebElement addURL;

	public void clickOnsurefoxAddURL() throws InterruptedException {
		waitForXpathPresent("//button[text()='Add URL']");
		addURL.click();
	}

	@FindBy(xpath = "//*[@id='URL']/input")
	private WebElement webSiteURL;

	@FindBy(xpath = "//*[@id='Name']/input")
	private WebElement webSiteDisplayName;

	@FindBy(xpath = "//*[@id='url_https']/span/input")
	private WebElement httpsRadioButton;

	public void EnterWebSite(String WebsiteURL, String WebsiteDisplayName) throws InterruptedException {
		waitForXpathPresent("//*[@id='url_https']/span/input");
		httpsRadioButton.click();
		webSiteURL.sendKeys(WebsiteURL);
		webSiteDisplayName.sendKeys(WebsiteDisplayName);
	}

	@FindBy(xpath = "//*[@id='doneButton']")
	private WebElement DoneAddingAllowWebsites;

	public void clickOnDoneAddingAllowWebsites() throws InterruptedException {
		waitForXpathPresent("//*[@id='doneButton']");
		DoneAddingAllowWebsites.click();
	}

	public void VerifySurefoxSettingsJobModifiySuccessFully() throws InterruptedException {

		clickOnsurefoxAllowedWebsite();

		boolean AllowedSiteUrlSF = addURL.isDisplayed();
		String pass = "PASS>> Console didnt load and user is able to modify the job successfully";
		String fail = "FAIL>> Console keeps loading and user is not able to modify the job successfully";
		ALib.AssertTrueMethod(AllowedSiteUrlSF, pass, fail);
		sleep(2);

	}

	public void LaunchSureFox() throws IOException {

		Runtime.getRuntime().exec("adb shell monkey -p com.gears42.surefox -c android.intent.category.LAUNCHER 1");

	}

	public void LaunchSureVideo() throws IOException {

		Runtime.getRuntime().exec("adb shell monkey -p com.gears42.surevideo -c android.intent.category.LAUNCHER 1");

	}

	public void SureFoxAdminDeviceSide() throws IOException, InterruptedException {
		Runtime.getRuntime().exec(
				"adb shell am broadcast -a com.gears42.surefox.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surefox");
		sleep(2);
	}

	public void clickOnSureFoxSetting() throws InterruptedException {
		sleep(4);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'Browser Preferences')]").click();
	}

	public void verifyBackButtonBrowisng(String VerifyRunScript) throws IOException, InterruptedException {
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'SureLock
		// Settings')]").click();
		List<WebElement> sureFoxSettingCheckBox = Initialization.driverAppium.findElementsById("android:id/checkbox");
		String BackButtonBrowisngCheckBox = sureFoxSettingCheckBox.get(3).getAttribute("checked");
		if (BackButtonBrowisngCheckBox.equalsIgnoreCase(VerifyRunScript)) {
			Reporter.log("SureFox Setting is applied,Use back button for Browsing is disabled", true);
		} else {
			ALib.AssertFailMethod("SureFox Setting failed,Use back button for Browsing is not disabled");
		}
	}

	public void unInstallSureFox() throws InterruptedException, IOException {
		sleep(3);
		Runtime.getRuntime()
				.exec("adb shell am start -S \"com.android.settings/.Settings\\$DeviceAdminSettingsActivity\"");
		sleep(3);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureFox']").click();
		sleep(2);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.Button[@text='DEACTIVATE' or @text='Deactivate']").click();
		sleep(2);
		Initialization.driverAppium.removeApp("com.gears42.surefox");
		sleep(3);
	}

	public void SureFoxSettingName(String SettingNAme) throws InterruptedException {
		sleep(2);
		JobName.clear();
		JobName.sendKeys(SettingNAme);
		PasswordField.clear();

		PasswordField.sendKeys("0000");
		saveJob.click();
		sleep(2);
		waitForidPresent("mailSection");
		sleep(5);
	}

	@FindBy(xpath = "//*[text()='Browser Preferences']")
	private WebElement SureFoxBrowserPreferences;

	public void clickOnSureFoxBrowserPreferences() throws InterruptedException {
		waitForXpathPresent("//*[text()='Browser Preferences']");
		SureFoxBrowserPreferences.click();
	}

	@FindBy(xpath = "//*[@id='browser_sett_pg']/footer/div[2]/button")
	private WebElement DoneSureFoxPreferences;

	public void clickOnDoneSureFoxPreferences() throws InterruptedException {
		DoneSureFoxPreferences.click();
	}
	// *[@id='browser_sett_pg']/footer/div[2]/button

	@FindBy(xpath = "//*[@id='EnableJavascript']/div/div[2]/span/input")
	private WebElement JavaScriptCheckBox;

	public void clickOnJavaScriptCheckBox() throws InterruptedException {
		waitForXpathPresent("//*[@id='EnableJavascript']/div/div[2]/span/input");
		JavaScriptCheckBox.click();// disable checkbox
	}

	@FindBy(xpath = "//*[@id='site_details_pg']/footer/div/button[1]")
	private WebElement SureFoxAllowWebsitesDone;

	public void clickOnSureFoxAllowWebsitesDone() throws InterruptedException {
		waitForXpathPresent("//*[@id='site_details_pg']/footer/div/button[1]");
		SureFoxAllowWebsitesDone.click();
	}

	@FindBy(id = "SureLockSettings")
	private WebElement sureLockSetting;

	public void clickOnsureLockSetting() throws InterruptedException {
		waitForidPresent("SureLockSettings");
		sleep(1);
		sureLockSetting.click();
	}

	@FindBy(xpath = "//*[@id='surelock_settings_pg']/div/ul[1]/li[4]/div/div[2]/span/input")
	private WebElement useSystemWallpaper;

	public void enableUseSystemWallpaper() throws InterruptedException {
		sleep(2);
		waitForXpathPresent("//*[@id='surelock_settings_pg']/div/ul[1]/li[4]/div/div[2]/span/input");
		useSystemWallpaper.click();
	}

	@FindBy(xpath = "//*[@id='surelock_settings_pg']/div/ul[1]/li[5]/div/div[2]/span/input")
	private WebElement backGroundColor;

	@FindBy(xpath = "//*[@id='colourPicker_popup']/div/div/div[3]/button[2]")
	private WebElement colorOk;

	public void enablebackGroundColor() throws InterruptedException {
		sleep(5);
		backGroundColor.click();
		sleep(5);
		colorOk.click();
	}

	@FindBy(xpath = "//*[@id='UseBackButtonForBrowsing']/div/div[2]/span/input")
	private WebElement BackButtonBrowsing;

	public void ClickOnBackBtnBrwsngCheckBox() throws InterruptedException {
		sleep(2);
		BackButtonBrowsing.click();
	}

	@FindBy(xpath = "//*[@id='surelock_settings_pg']/header/span[3]/i")
	private WebElement sureLockSettingSearch;
	@FindBy(id = "surelock_sttings")
	private WebElement enterSureLockSettingName;
	@FindBy(xpath = "//li[@id='text_color_settings']/div/div/p[1]")
	private WebElement selectTextSetting;

	@FindBy(xpath = "//div[@id='blue_color']/div/span[1]/input")
	private WebElement BlueRadioButton;

	@FindBy(xpath = "//*[@id='text-color-popup']/div/div/div[3]/button[2]")
	private WebElement doneSelectTextColour;

	@FindBy(xpath = "//*[@id='surelock_settings_pg']/footer/div[2]/button")
	private WebElement doneTxtStng;

	@FindBy(id = "add_app")
	private WebElement addAppsButton;

	public void clickOnAddAppsButton() throws InterruptedException {
		sleep(3);
		waitForidPresent("add_app");
		addAppsButton.click();
	}

	@FindBy(xpath = "//span[text()='Available Applications']/../span[4]/i")
	private WebElement SearchApp;

	public void ClickOnSearch() throws InterruptedException {
		sleep(2);
		waitForXpathPresent("//span[text()='Available Applications']/../span[4]/i");
		SearchApp.click();
	}

	@FindBy(id = "activation_code")
	private WebElement activationCode;
	@FindBy(xpath = "//p[text()='About SureLock']")
	private WebElement aboutSureLock;
	@FindBy(xpath = "//*[@id='about_surelock']/footer/div/button")
	private WebElement doneSureLockAbout;

	public void aboutSureLock() throws InterruptedException {
		sleep(2);
		aboutSureLock.click();
		sleep(1);
		waitForidPresent("activation_code");
		activationCode.sendKeys("909686");
		sleep(1);
		doneSureLockAbout.click();
		sleep(2);

	}

	@FindBy(xpath = "//p[text()='About SureFox']")
	private WebElement aboutSureFox;

	@FindBy(xpath = "//*[@id='about_surefox']/footer/div/button")
	private WebElement doneSureFoxAbout;

	public void aboutSureFox() throws InterruptedException {
		aboutSureFox.click();
		sleep(1);
		waitForidPresent("activation_code");
		activationCode.sendKeys("567446");
		sleep(1);
		doneSureFoxAbout.click();
		sleep(2);

	}

	@FindBy(id = "surelock_app")
	private WebElement EnterSurelockAppName;
	@FindBy(xpath = "//*[@id='ava_app_list']/div/div[1]/ul/li/div/div[3]")
	private WebElement ClickOnSearchedApp;

	/*
	 * public void EnterApplicationName(String applicationName) throws
	 * InterruptedException { EnterSurelockAppName.clear();
	 * EnterSurelockAppName.sendKeys(applicationName); sleep(3);
	 * waitForXpathPresent("//*[@id='ava_app_list']/div/div[1]/ul/li/div/div[3]");
	 * ClickOnSearchedApp.click(); }
	 */ public void EnterApplicationName(String applicationName) throws InterruptedException {
		EnterSurelockAppName.clear();
		EnterSurelockAppName.sendKeys(applicationName);
		sleep(3);
		WebElement element = Initialization.driver
				.findElement(By.xpath("//div[p[text()='" + applicationName + "']]/following-sibling::div/span/input"));
		((JavascriptExecutor) Initialization.driver).executeScript("arguments[0].scrollIntoView();", element);
		element.click();
	}

	@FindBy(xpath = "//*[@id='ava_app_list']/footer/div[2]/button[1]")
	private WebElement doneButtonAddApplication;

	public void clickOnDoneButtonAddApplication() {
		doneButtonAddApplication.click();
	}

	@FindBy(xpath = "//*[@id='surelock_settings_pg']/footer/div[2]/button")
	private WebElement doneButtonSureLockSetting;

	public void clickOndoneButtonSureLockSetting() throws InterruptedException {
		sleep(1);
		waitForXpathPresent("//*[@id='surelock_settings_pg']/footer/div[2]/button");
		doneButtonSureLockSetting.click();
	}

	@FindBy(id = "application_icon_size")
	private WebElement applicationIconSetting;

	@FindBy(xpath = "//*[@id='small_size_icon']/div/span[1]/input")
	private WebElement iconSizeSmall;

	@FindBy(xpath = "//*[@id='icon-size-popup']/div/div/div[3]/button[2]")
	private WebElement iconSizeDone;

	public void changeApplicationIconSize() throws InterruptedException {
		waitForidPresent("application_icon_size");
		applicationIconSetting.click();
		sleep(1);
		iconSizeSmall.click();
		iconSizeDone.click();
	}

	public void verifyEnableUseSysWallpaper() throws IOException, InterruptedException {
		sleep(4);
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'SureLock
		// Settings')]").click();
		List<WebElement> surelockSettingCheckBox = Initialization.driverAppium.findElementsById("android:id/checkbox");
		String useSysWallpaper = surelockSettingCheckBox.get(0).getAttribute("checked");
		if (useSysWallpaper.equalsIgnoreCase("true")) {
			Reporter.log("Use system wallpaper checkbox is checked", true);
		} else {
			ALib.AssertFailMethod("Use system wallpaper checkbox is not checked");
		}
	}

	public void verifyDisableUseSysWallpaper() throws IOException, InterruptedException {
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'SureLock
		// Settings')]").click();
		List<WebElement> surelockSettingCheckBox = Initialization.driverAppium.findElementsById("android:id/checkbox");
		String useSysWallpaper = surelockSettingCheckBox.get(0).getAttribute("checked");
		if (useSysWallpaper.equalsIgnoreCase("false")) {
			Reporter.log("Use system wallpaper checkbox is not checked", true);
		} else {
			ALib.AssertFailMethod("Use system wallpaper checkbox should not be checked");
		}
	}

	@FindBy(xpath = "(//span[text()='Modify']/../i)[1]")
	private WebElement modifyButton;

	public void clickOnModifyOption() throws InterruptedException {
		modifyButton.click();
		sleep(3);
	}

	public void verifyEnableBackGroundColour() throws IOException, InterruptedException {
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'SureLock Settings')]")
				.click();
		List<WebElement> surelockSettingCheckBox = Initialization.driverAppium.findElementsById("android:id/checkbox");
		String backGroundColour = surelockSettingCheckBox.get(1).getAttribute("checked");
		if (backGroundColour.equalsIgnoreCase("true")) {
			Reporter.log("Background colour checkbox is checked", true);
		} else {
			ALib.AssertFailMethod("Background colourr checkbox is not checked");
		}
	}

	public void verifyReadMessage(String TextJobName) {

		boolean readMessage = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'" + TextJobName + "')]").isDisplayed();
		String pass = "Pass >> Read text is displayed";
		String fail = "FAIL >> Read text is displayed";
		ALib.AssertTrueMethod(readMessage, pass, fail);
	}

	public void VerifyDeviceChargingState(String ChargingStatus1, String ChargingStatus2) throws InterruptedException {

		DeviceChargingStateDropDown.click();
		String GetAllDisplayedOptions = DeviceChargingStateDropDown.getText();
		System.out.println(GetAllDisplayedOptions);
		try {
			if (GetAllDisplayedOptions.contains(ChargingStatus1) & GetAllDisplayedOptions.contains(ChargingStatus1)) {

				Reporter.log("PASS>>> Status is true", true);

			}
		} catch (Exception e) {

			ALib.AssertFailMethod("FAIL>>> Status is not true");
		}
	}

	public void CloseApplyJobPopUp() throws InterruptedException {

		Initialization.driver.findElement(By.xpath("//*[@id='applyJob_header']/button")).click();
		sleep(2);

	}

	public void GettingLastmodifiedTime() {

		Date objDate = new Date();
		System.out.println(objDate.toString());
		String strDateFormat = "dd-MMM-yyyy hh:mm:ss a"; // Date format is Specified
		SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // Date format string is passed as an argument to
																		// the Date format object
		System.out.println(objSDF.format(objDate)); // Date formatting is applied to the current date

		Reporter.log("Latest modified Time_Date is displayed", true);
	}

	public void verifyMultiAppSureLockSetting(String AppsName) throws InterruptedException {
		String[] ApplicationsName = AppsName.split(",");
		for (String appName : ApplicationsName) {
			if (Initialization.driverAppium
					.findElementByXPath("//android.widget.TextView[contains(@text,'" + appName + "')]").isDisplayed()) {
				Reporter.log(appName + "Sure Lock Apps are displayed", true);
			} else {
				ALib.AssertFailMethod(appName + "Sure Lock Apps are not displayed");
			}
		}

	}

	public void SureLockAdminDeviceSide() throws IOException, InterruptedException {
		sleep(2);
		Runtime.getRuntime().exec(
				"adb shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surelock");
		Reporter.log("Navigated to Admin Settings", true);
		sleep(10);
	}

	public void clickOnSureLockSetting() throws InterruptedException {
		sleep(4);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'SureLock Settings')]")
				.click();
	}

	public void verifyAppSureLockSetting(String AppsName) throws InterruptedException {
		sleep(10);
		if (Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'" + AppsName + "')]").isDisplayed()) {
			Reporter.log(AppsName + "Sure Lock Apps are displayed", true);
		} else {
			ALib.AssertFailMethod(AppsName + "Sure Lock Apps are not displayed");
		}
	}

	public void DisablebackGroundColor() throws InterruptedException {
		sleep(5);
		backGroundColor.click();
		sleep(5);
		// colorOk.click();
	}

	public void verifyAppsOnSureFox(String appName) throws IOException {
		String[] ApplicationsName = appName.split(",");
		for (String AppName : ApplicationsName) {
			if (Initialization.driverAppium.findElementByXPath("//android.view.View[contains(@text,'" + AppName + "')]")
					.isDisplayed()) {
				Reporter.log(AppName + "Sure Fox setting for  websites has been applied sucessfully", true);
			} else {
				ALib.AssertFailMethod("Sure Fox setting for  websites is not sucessfull");
			}
		}
	}

	@FindBy(id = "job_name")
	private WebElement JobName;
	@FindBy(id = "job_passwordBox")
	private WebElement PasswordField;
	@FindBy(xpath = ".//*[@id='saveAsJobModal']/div/div/div/button[text()='Save']")
	private WebElement saveJob;

	public void SurelockSettingName(String SettingNAme) throws InterruptedException {
		sleep(4);
		JobName.clear();
		JobName.sendKeys(SettingNAme);
		PasswordField.clear();
		PasswordField.sendKeys("0000");
		saveJob.click();
		sleep(2);
		waitForidPresent("mailSection");
		sleep(5);
	}

	public void verifyPluginSupport() throws IOException, InterruptedException {
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'SureLock
		// Settings')]").click();
		List<WebElement> sureFoxSettingCheckBox = Initialization.driverAppium.findElementsById("android:id/checkbox");
		String PluginSupport = sureFoxSettingCheckBox.get(2).getAttribute("checked");
		if (PluginSupport.equalsIgnoreCase("false")) {
			Reporter.log("Enable Plug-in support checkbox is not checked", true);
		} else {
			ALib.AssertFailMethod("Enable Plug-in support checkbox should not be checked");
		}
	}

	public void verifyJavaScript(String VerifyRunScript) throws IOException, InterruptedException {
		sleep(2);
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'SureLock
		// Settings')]").click();
		List<WebElement> sureFoxSettingCheckBox = Initialization.driverAppium.findElementsById("android:id/checkbox");
		String JavaScript = sureFoxSettingCheckBox.get(1).getAttribute("checked");
		if (JavaScript.equalsIgnoreCase(VerifyRunScript)) {
			Reporter.log("SureFox Setting is applied", true);
		} else {
			ALib.AssertFailMethod("SureLock Setting failed");
		}
	}

	@FindBy(xpath = "//*[@id='done_btn']")
	private WebElement DoneAllowedApplication;

	public void clickOnDoneAllowedApplication() {
		waitForXpathPresent("//*[@id='done_btn']");
		DoneAllowedApplication.click();
	}

	@FindBy(xpath = "//*[@id='SaveAsBtn']")
	private WebElement saveSureLockSetting;

	public void clickOnsaveSureLockSetting() {
		saveSureLockSetting.click();
	}

	@FindBy(xpath = "//*[@id='SaveAsBtn']")
	private WebElement saveSureFoxSetting;

	public void clickOnsaveSureFoxSetting() {
		saveSureFoxSetting.click();
	}

	public void clickOnFileTransferJob() throws InterruptedException {
		sleep(2);
		fileTransfer.click();
		Reporter.log("PASS >> Clicked on File Transfer Job", true);
		sleep(5);
	}

	public void clickOnAdd() throws InterruptedException {
		sleep(2);
		addBtn.click();
		Reporter.log("PASS >> Clicked on Add Job", true);
		sleep(2);
	}

	public void verifyFileTransferPrompt() throws InterruptedException {
		Thread.sleep(2000);
		fileTransferPrompt.isDisplayed();
		Reporter.log("Android File Transfer Job Prompt displayed successfully", true);
	}

	public void enterFilePathURL(String path) {
		filePathURL.sendKeys(path);
		Reporter.log("PASS >> Enter File Path", true);
	}

	public void clickOkBtnOnBrowseFileWindow() throws InterruptedException {
		sleep(2);
		OkButtonJob.click();
		sleep(5);
		// Reporter.log("Clicked OK button on File Transfer Job Window", true);

	}

	@FindBy(xpath = "//*[@id='jobQueueHistoryTab']/a")
	private WebElement LatestJobNameInsideHistoryCompletedJobs;

	public void browseFileTRansferJobTAGS(String FilePATH) throws IOException, InterruptedException {
		fileBrowseBtn.click();
		sleep(8);
		Runtime.getRuntime().exec(FilePATH);
		Reporter.log("Uploaded File", true);
		sleep(15);
	}

	public void browseFile() throws IOException, InterruptedException {
		fileBrowseBtn.click();
		sleep(8);
		Runtime.getRuntime().exec("./Uploads/UplaodFiles/Job On Android/FileTransferSingle/\\File1.exe");
		Reporter.log("Uploaded File", true);
		sleep(10);
	}

	public void verifyFileTransferJobCreation() throws InterruptedException {
		boolean filetransferjob = createdFileTransferJob.isDisplayed();
		String pass = "PASS >> File Transfer Job Created Successfully ";
		String fail = "FAIL >> File Transfer job NOT created";
		ALib.AssertTrueMethod(filetransferjob, pass, fail);
		sleep(15);
	}

	public void clickOkBtnOnAndroidJob() throws InterruptedException {
		sleep(2);
		okBtnOnAndroidJobWindow.click();
		Reporter.log("PASS >> Clicked OK button on File Transfer Page", true);
		sleep(4);
	}

	public void clickOkBtnOnFileTransferJobUpdate() throws InterruptedException {
		okBtnOnFileTransferJobUpdate.click();
		Reporter.log("Clicked OK button on File Transfer Page Update", true);
		sleep(4);
	}

	public void verifyWarningMessageWhenAllFieldsEmpty() {

		boolean isdisplayed = true;

		try {
			errorWithEmptyFields.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}

		Assert.assertTrue(isdisplayed, "FAIL >> Warning message is displayed");
		Reporter.log("PASS >> 'Job name cannot be empty'is received", true);
		waitForPageToLoad();
	}

	public void verifyErrorWhenFileNotUploaded() throws InterruptedException {
		boolean isdisplayed = true;

		try {
			errorWhenNoFileUploaded.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}

		Assert.assertTrue(isdisplayed, "FAIL >> Notifcation did not displayed");
		Reporter.log("PASS >> 'Please select a message.' is received", true);
		sleep(1);
	}

	public void verifyFileTransferJobCreation(String jobName) throws InterruptedException {
		waitForXpathPresent(".//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + jobName + "']");
		boolean filetransferjob = Initialization.driver
				.findElement(By.xpath(".//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + jobName + "']"))
				.isDisplayed();
		String pass = "PASS >> File Transfer Job Created Successfully ";
		String fail = "FAIL >> File Transfer Job NOT created";
		ALib.AssertTrueMethod(filetransferjob, pass, fail);
		sleep(15);
	}

	public void clickOnCreatedFileTransferJob() throws InterruptedException {
		createdFileTransferJob.click();
		Reporter.log("Clicked on File Transfer Job", true);
		sleep(2);
	}

	public void verifyFileTransferJobUpdate() throws InterruptedException {
		boolean modifiedFileTransferJob = updatedFileTransferJob.isDisplayed();
		String pass = "PASS >> File Transfer Job Created Successfully ";
		String fail = "FAIL >> File Transfer job NOT created";
		ALib.AssertTrueMethod(modifiedFileTransferJob, pass, fail);
		Reporter.log("File Transfer Job updated successfully", true);
		sleep(4);
	}

	public void clickOnModifyJob() throws InterruptedException {
		modifyJob.click();
		Reporter.log("Clicked on Modify Job option", true);
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='jobDataGrid']/tbody/tr[1]/td[text()='Upload Completed']")
	private WebElement UploadCompletedStatus;

	public void UploadcompleteStatus() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(Initialization.driver, 1200);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(
					By.xpath(".//*[@id='jobDataGrid']/tbody/tr[1]/td[text()='Upload Completed']"))).isDisplayed();
			System.out.println("PASS>>> Upload Completed Message displayed");
			sleep(3);
		} catch (Throwable e) {
			System.out.println("Install job in queue do device refresh");
			Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath(".//*[@id='jobDataGrid']/tbody/tr[1]/td[text()='Upload Completed']"))).isDisplayed();
				System.out.println("PASS>>> Upload Completed displayed");
			} catch (Throwable e1) {
				System.out.println("FAIL>>> Upload completed message not displayed");
			}
		}
	}

	public void WaitForSetPwdPopUp(String Pwd) throws InterruptedException {

		sleep(20);
		// AppiumwaitForPresenceOfElement(Initialization.driverAppium,200,"//android.widget.TextView[@text='"
		// + AppName + "']");
		Initialization.driverAppium.findElementByClassName("android.widget.EditText").sendKeys(Pwd); //// android.widget.Button[@text='INSTALL'
																										//// and
																										//// @index='1']
		sleep(2);
		Initialization.driverAppium.findElementByXPath("android.widget.Button[@text='Continue' and @index='1']")
				.click();
		sleep(2);
	}

	public void WaitForSetPwdPopUp2(String Pwd) throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("android.widget.EditText").sendKeys(Pwd);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("android.widget.Button[@text='Continue' and @index='1']")
				.click();
		sleep(2);

	}

	@FindBy(xpath = ".//*[@id='jobDataGrid']/tbody/tr[1]/td[5]")
	private WebElement UploadedFileSize;

	public void CheckFileSize(String Size) throws InterruptedException {
		String actual = UploadedFileSize.getText();
		Reporter.log(actual);
		String expected = Size;
		String PassStatement = "PASS >> File Size" + Size + " present";
		String FailStatement = "FAIL >> Filee size" + Size + "is not present";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

	}

	@FindBy(xpath = ".//*[@id='jobDataGrid']/tbody/tr[1]/td/p[text()='0FileTransfer']")
	private WebElement FileTransferJob;

	@FindBy(xpath = "//span[contains (text(),'The job named')]")
	private WebElement JobInitiatedMessage;

	public void ClickOnFileTransferJob() throws InterruptedException {
		FileTransferJob.click();
		sleep(3);

	}

	public void JobInitiatedMessage() throws InterruptedException {
		try {
			JobInitiatedMessage.isDisplayed();
			Reporter.log("Job Initiated Message Displayed Successfully", true);
		} catch (Exception e) {

			ALib.AssertFailMethod("Job Initiated Message Is Not Displayed");
		}
		sleep(2);

	}

	public void browseFile(String FilePath) throws IOException, InterruptedException {
		fileBrowseBtn.click();
		sleep(5);
		Runtime.getRuntime().exec(FilePath);
		Reporter.log("Uploaded File", true);
		sleep(5);
	}

	@FindBy(id = "job_devicepath_input")
	private WebElement EnterdevicePath;

	public void EnterDevicePath(String DevicePath) throws InterruptedException {
		EnterdevicePath.clear();
		sleep(3);
		EnterdevicePath.sendKeys(DevicePath);
	}

	@FindBy(xpath = "//p[text()='software testing.pdf']")
	private WebElement SoftwareTestingPdf;

	public void DeleteExistingFile() throws InterruptedException {
		SoftwareTestingPdf.click();
		sleep(3);
		deleteJob.click();
		sleep(4);
	}

	public void CreatingMultiFileTransferJob(String JobName) throws InterruptedException, IOException {
		clickOnJobs();
		clickNewJob();
		clickOnAndroidOS();
		clickOnFileTransferJob();
		enterJobName(JobName);
		clickOnAdd();
		browseFile("D:\\UplaodFiles\\Job On Android\\ModifyFileTransfer\\DataMiningPDF.exe");
		clickOkBtnOnBrowseFileWindow();
		clickOnAdd();
		browseFile("D:\\UplaodFiles\\Job On Android\\ModifyFileTransfer\\File1.exe");
		clickOkBtnOnBrowseFileWindow();
		clickOnAdd();
		browseFile("D:\\UplaodFiles\\Job On Android\\ModifyFileTransfer\\File2.exe");
		clickOkBtnOnBrowseFileWindow();
		clickOnAdd();
		browseFile("D:\\UplaodFiles\\Job On Android\\ModifyFileTransfer\\File3.exe");
		clickOkBtnOnBrowseFileWindow();
		clickOkBtnOnAndroidJob();
		verifyFileTransferJobCreation(JobName);
		UploadcompleteStatus();
	}

	// ############################# Install Application
	// ########################################

	@FindBy(xpath = ".//*[@id='install_program']")
	private WebElement InstallApplication;

	public void ClickOnInstallApplication() throws InterruptedException {
		sleep(3);
		InstallApplication.click();
		waitForidPresent("funcHolder");
		sleep(3);
	}

	@FindBy(xpath = "//span[text()='Add files to install.']")
	private WebElement AddFilesToInstallErrorMessage;

	public void verifyErrorWhenFileIsNotAdded() throws InterruptedException {
		boolean isdisplayed = true;

		try {
			AddFilesToInstallErrorMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}

		Assert.assertTrue(isdisplayed, "FAIL >> Notifcation did not displayed");
		Reporter.log("PASS >> 'Add files to install.' is received", true);
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='install_addBtn']/i")
	private WebElement AddButtonInstallApplication;

	public void clickOnAddInstallApplication() throws InterruptedException {
		try {
			AddButtonInstallApplication.click();
			waitForidPresent("install_add_page");
			sleep(2);
		} catch (Exception e) {
		}
	}

	public void browseFileInstallApplication() throws IOException, InterruptedException {
		fileBrowseBtn.click();
		sleep(8);
		Runtime.getRuntime().exec("./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\File1.exe");
		Reporter.log("Uploaded File", true);
		sleep(8);
	}

	public void browseFileInstallApplication(String FileLocation) throws IOException, InterruptedException {
		fileBrowseBtn.click();
		sleep(5);
		Runtime.getRuntime().exec(FileLocation);
		Reporter.log("Uploaded File", true);
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
	// *[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input
	private WebElement SearchJobField;

	public void SearchJobInSearchField(String JobToBeSearched) throws InterruptedException {
		SearchJobField.clear();
		sleep(2);
		SearchJobField.sendKeys(JobToBeSearched);
		waitForXpathPresent("//p[text()='" + JobToBeSearched + "']");
		WebElement JobSelected = Initialization.driver.findElement(By.xpath("//p[text()='" + JobToBeSearched + "']"));
		sleep(3);
		waitForXpathPresent("//p[text()='" + JobToBeSearched + "']");
		JobSelected.click();
		sleep(3);
	}

	@FindBy(xpath = "//span[text()='Job modified successfully.']")
	private WebElement Job_modified_successfully;

	public void VerfyModificationSuccessMessage() throws InterruptedException {
		boolean msg = Job_modified_successfully.isDisplayed();
		String pass1 = "PASS >>'Job modified successfully.' message is displayed";
		String fail1 = "FAIL >>'Job modified successfully.' message is not displayed";
		ALib.AssertTrueMethod(msg, pass1, fail1);
		sleep(6);
	}

	@FindBy(xpath = "//*[@id='job_nixsettings_ConnType_input']/option[2]")
	private WebElement ConnectionTypeWifi;

	public void modifyNixAgentJob() throws InterruptedException {
		ConnectionTypeWifi.click();
		sleep(1);
		OkButtonNixAgentSettings.click();
		sleep(1);
	}

	@FindBy(xpath = "//*[@id='job_modify']/i")
	private WebElement ClickModifyJobButton;

	public void ModifyJobButton() throws InterruptedException {
		ClickModifyJobButton.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='NixAgentEnableTimeSyncWithServer']")
	private WebElement EnablingAllOptions;

	public void selectJobEnablingAllOptions() throws InterruptedException {
		EnablingAllOptions.click();
		sleep(1);
	}

	public void ForModification() throws InterruptedException {
		Select periodicityDropdown1 = new Select(job_nixsettings_periodicity_TimeSync);
		periodicityDropdown1.selectByValue("MINUTES_5");
		periodicityDropdown1.selectByIndex(1);
		sleep(2);
	}

	public void CreaginMultiInstallJob() throws InterruptedException, IOException {
		clickOnAddInstallApplication();
		browseFileInstallApplication("");
		clickOkBtnOnBrowseFileWindow();
		clickOnAddInstallApplication();
		browseFileInstallApplication("");
		clickOkBtnOnBrowseFileWindow();
		clickOkBtnOnAndroidJob();
		UploadcompleteStatus();

	}
	// ############################ Text Message
	// #############################################

	@FindBy(id = "job_msg_buzz_interval")
	private WebElement BuzzInterval;

	@FindBy(id = "job_msg_buzz_enable")
	private WebElement enableBuzz;

	public void ClickOnEnableBuzz(String buzzDuration) throws InterruptedException {
		enableBuzz.click();
		BuzzInterval.sendKeys(buzzDuration);
	}

	@FindBy(xpath = ".//*[@id='text_message']")
	private WebElement TextMessageButton;

	public void clickOnTextMessageJob() throws InterruptedException {
		TextMessageButton.click();
		waitForidPresent("txtMessage_modal");
		sleep(4);
	}

	public void AppiumwaitForPresenceOfElement(AndroidDriver<WebElement> Driver, int time, String Xpath) {
		WebDriverWait wait = new WebDriverWait(Driver, time);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xpath)));
	}

	@FindBy(id = "close_message_enable")
	private WebElement closeMessage;
	@FindBy(id = "close_interval")
	private WebElement closeinterval;

	public void ClickOnEnableCloseMessage(String buzzDuration) throws InterruptedException {
		closeMessage.click();
		closeinterval.sendKeys(buzzDuration);
	}

	public void verifyForceReadMessage(String TextJobName) {
		AppiumwaitForPresenceOfElement(Initialization.driverAppium, 400,
				"//android.widget.TextView[@text='" + TextJobName + "']");
		boolean readMessage = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='" + TextJobName + "']").isDisplayed();
		String pass = "Pass >> ForceRead text is displayed";
		String fail = "FAIL >> ForceRead text is displayed";
		ALib.AssertTrueMethod(readMessage, pass, fail);
	}

	public void verifyTextMessagePrompt() {
		WebElement[] txtElements = { JobNameTextFieldTextMessage, SubjectTextField, BodyTextFieldTextMessage };
		for (WebElement textElemet : txtElements) {
			boolean element = textElemet.isDisplayed();
			String pass = "PASS >> WebElement is displayed";
			String fail = "FAIL >> WebElement is not displayed";
			ALib.AssertTrueMethod(element, pass, fail);
		}
	}

	public void ClickOnBackButtonInDeviceSide() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell input keyevent 4");
		sleep(2);

	}

	@FindBy(xpath = ".//*[@id='okbtn']")
	private WebElement OkButtonJob;

	public void ClickOnOkButton() throws InterruptedException {
		OkButtonJob.click();
		sleep(3);
	}

	public void BlankFieldErrorMessage() throws InterruptedException {
		List<WebElement> optionvalues = Initialization.driver.findElements(
				By.xpath(".//*[@id='txtMessage_modal']/div[2]/div/span/ul/li[text()='Please fill out this field.']"));
		String actual = optionvalues.get(0).getText();
		Reporter.log(actual, true);
		String expected = "Please fill out this field.";
		String PassStatement = "PASS >>Job Name field is empty and error text is displayed ";
		String FailStatement = "FAIL >> Job Name filed is not empty and error text is not displayed";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

		String actual2 = optionvalues.get(1).getText();
		Reporter.log(actual, true);
		String expected2 = "Please fill out this field.";
		String PassStatement2 = "PASS >> Body field is empty and error message is displayed";
		String FailStatement2 = "FAIL >> Body field id not empty and error message is not displayed";
		ALib.AssertEqualsMethod(expected2, actual2, PassStatement2, FailStatement2);
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='job_msg_name_input']")
	private WebElement JobNameTextFieldTextMessage;

	@FindBy(xpath = ".//*[@id='job_msg_body_input']")
	private WebElement BodyTextFieldTextMessage;

	@FindBy(xpath = ".//*[@id='job_msg_subject_input']")
	private WebElement SubjectTextField;

	public void textMessageJobWhenNoFieldIsEmpty(String TextJobName, String Subject, String Message)
			throws InterruptedException {
		JobNameTextFieldTextMessage.sendKeys(TextJobName);
		sleep(2);
		SubjectTextField.sendKeys(Subject);
		sleep(1);
		BodyTextFieldTextMessage.sendKeys(Message);
		sleep(3);
	}

	@FindBy(xpath = "//span[text()='Job created successfully.']")
	private WebElement JobCreatedSuccessfullyMessage;

	public void JobCreatedMessage() throws InterruptedException {
		boolean isdisplayed = true;

		try {
			JobCreatedSuccessfullyMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}

		Assert.assertTrue(isdisplayed, "FAIL >> \"Job Created successfully\"is not displayed");
		Reporter.log("PASS >> \"Job Created successfully\" is displayed", true);
		sleep(5);

	}

	@FindBy(id = "job_msg_radio1")
	private WebElement GetReadNotificationRadioBtn;

	public void ClickOnGetReadNotification() throws InterruptedException {
		GetReadNotificationRadioBtn.click();
		sleep(2);
	}

	@FindBy(id = "job_msg_radio2")
	private WebElement ForceReadRadioBtn;

	public void ClickOnForceReadMessage() throws InterruptedException {
		ForceReadRadioBtn.click();
		sleep(2);

	}

	// ##################################### Lock Device
	// ################################

	@FindBy(xpath = ".//*[@id='lock_device']")
	private WebElement LockDeviceButton;

	public void clickOnLockDevice() throws InterruptedException {
		LockDeviceButton.click();
		waitForidPresent("commonModalSmall");
		sleep(4);
	}

	@FindBy(xpath = "//li[text()='Please enter a name for the job.']")
	private WebElement ErrorMessageLockDevice;

	public void verifyBlankfieldTextmessage() throws InterruptedException {
		String actual2 = ErrorMessageLockDevice.getText();
		Reporter.log(actual2, true);
		String expected2 = "Please enter a name for the job.";
		String PassStatement2 = "PASS >> 'Please enter a name for the job.' is displayed";
		String FailStatement2 = "FAIL >> 'Please enter a name for the job.' is not displayed";
		ALib.AssertEqualsMethod(expected2, actual2, PassStatement2, FailStatement2);
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='LockDevice_header']/div")
	private WebElement CloseBtnLockDeviceJob;

	public void ClickOnCloseBtnLockDeviceJob() throws InterruptedException {

		CloseBtnLockDeviceJob.click();
		sleep(2);

	}

	@FindBy(xpath = ".//*[@id='job_name_input']")
	private WebElement LockJobNameTextField;

	public void CreateLockJobForLockDeviceOption() throws InterruptedException {
		LockJobNameTextField.sendKeys("0lockJobLockDevice");
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='job_option2_input']")
	private WebElement ChangePasswordRadioButton;

	public void CreateLockJobChangePasswordButton() throws InterruptedException {
		ChangePasswordRadioButton.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='passwordInputGroup']/span/ul/li")
	private WebElement PleaseFillOutThePasswordFieldMessage;

	public void LockJobEnterDevicePasswordErrorCapture() throws InterruptedException {
		String actual2 = PleaseFillOutThePasswordFieldMessage.getText();
		Reporter.log(actual2, true);
		String expected2 = "This is a mandatory field.";
		String PassStatement2 = "PASS >> 'This is a mandatory field.' is displayed";
		String FailStatement2 = "FAIL >> 'This is a mandatory field.' is not displayed";
		ALib.AssertEqualsMethod(expected2, actual2, PassStatement2, FailStatement2);
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='job_option3_input']")
	private WebElement EnterDevicePasswordTextField;

	public void CreateLockJobChangePassword(String EnterPwd) throws InterruptedException {
		LockJobNameTextField.sendKeys("0lockJobChangePassword");
		sleep(2);
		CreateLockJobChangePasswordButton();
		EnterDevicePasswordTextField.sendKeys(EnterPwd);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
	private WebElement SearchTextBoxApplyJob;

	public void SearchField(String JobName) throws InterruptedException {
		sleep(2);
		SearchTextBoxApplyJob.sendKeys(JobName);
		sleep(4);
		waitForXpathPresent("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']");
		sleep(6);
		WebElement JobSelected = Initialization.driver
				.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']"));
		JobSelected.click();
		sleep(2);
		OkButtonApplyJob.click();
		sleep(2);
	}

	public void SearchJobsInApplyJob(String JobName) throws InterruptedException {
		SearchTextBoxApplyJob.sendKeys(JobName);
		sleep(2);
		waitForXpathPresent("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']");
		sleep(6);
		WebElement JobSelected = Initialization.driver
				.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']"));
		JobSelected.click();
	}

	public void ClickOnApplyJobOkButton() throws InterruptedException {
		OkButtonApplyJob.click();
		waitForXpathPresent("//span[contains(text(),'initiated')]");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;

	public void SearchDeviceInconsole(String Device)
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {

		SearchDeviceTextField.clear();
		sleep(3);
		SearchDeviceTextField.sendKeys(Device);
		waitForXpathPresent("//p[text()='" + Device + "']");
		sleep(5);
	}

	public void clickOnAllDevices() throws InterruptedException {
		SelectAllDevices.click();
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='all_ava_devices']")
	private WebElement SelectAllDevices;

	public void ClearSearchFieldConsole() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")).clear();
		sleep(3);
	}

	public void SearchDeviceInconsoleGroups(String Devicename) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id='tableContainer']/div[4]/div[1]/div[3]/input"))
				.sendKeys(Devicename);
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 40);
		wait1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//p[text()='" + Devicename + "']")));
		sleep(4);
	}

	public void UninstallSureVideo() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb uninstall com.gears42.surevideo");
		sleep(2);
	}
	// ###################### Device side Automation ######################

	@FindBy(xpath = "//android.widget.TextView[@text()='SureVideo']")
	private WebElement SurevideoIcon;

	public void LaunchPlayStore() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell am start -n com.android.vending/com.android.vending.AssetBrowserActivity");
		sleep(6);
	}

	public void ClickOnMenuPlayStore() throws InterruptedException {
		Initialization.driverAppium.findElementById("com.android.vending:id/navigation_button").click();
		sleep(4);
	}

	public void ClickOnHomeButtonDeviceSide() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell input keyevent 3");
		sleep(8);
	}

	public void verifyFolderIsDownloadedInDevice(String DownloadedFolder) throws InterruptedException {
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.ImageView[@content-desc='Storage']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Download']").click();
		sleep(1);
		try {
			String[] fileName = DownloadedFolder.split(",");
			for (String file : fileName) {
				sleep(3);
				boolean fileDisplay = Initialization.driverAppium
						.findElementByXPath("//android.widget.TextView[@text='" + file + "']").isDisplayed();
				String pass = file + "PASS >> is displayed";
				String fail = file + " is not displayed";
				ALib.AssertTrueMethod(fileDisplay, pass, fail);
				sleep(3);
			}
		} catch (Exception e) {
			String[] fileName = DownloadedFolder.split(",");
			for (String file : fileName) {
				sleep(3);
				boolean fileDisplay = Initialization.driverAppium
						.findElementByXPath("//android.widget.TextView[@text='" + file + "']").isDisplayed();
				String pass = file + "PASS >> is displayed";
				String fail = file + " is not displayed";
				ALib.AssertTrueMethod(fileDisplay, pass, fail);
				sleep(3);
			}
		}
	}

	public void verifyFolderNameDownloadedInDevice(String file) throws InterruptedException {
		sleep(2);
		// Initialization.driverAppium.findElementByXPath("//android.widget.ImageView[@content-desc='Storage']").click();
		// sleep(1);
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Download']").click();
		// sleep(1);
		try {
			if (Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + file + "']")
					.isDisplayed())
				;
			ALib.AssertFailMethod("Incorrect filename is displayed");
		}

		catch (Exception e) {

			Reporter.log("Correct FileName is displayed", true);

		}
	}

	public void clickOnMyAppsAndGames() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='My apps & games']").click();
		sleep(4);
	}

	CommonMethods_POM common = new CommonMethods_POM();

	public void ScrollToSureVideoAndCheckForInstalled() throws InterruptedException {
		boolean isSureVideoPresent = common.commonScrollMethod("SureVideo Kiosk Video Looper");
		String pass = "PASS >> Scrolled to SureVideo and found the SureVideo app: " + isSureVideoPresent + " ";
		String fail = "FAIL >> Scrolled to SureVideo and found the SureVideo app: " + isSureVideoPresent + " ";
		ALib.AssertTrueMethod(isSureVideoPresent, pass, fail);
	}

	public void PopUpToInstallSureVideoInNonKnoxDevice() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='INSTALL' and @index='1']")
				.click();// ("com.android.packageinstaller:id/ok_button").click();
		sleep(6);
		Initialization.driverAppium.findElementById("com.android.packageinstaller:id/ok_button").click();
		sleep(5);

	}

	// ######################## file transfer######################################

	public void OpenFileManagerAndCheckForTransferedFile() throws IOException, InterruptedException {

		boolean isFilePresent = common.commonScrollMethod("software testing.pdf");
		String pass = "PASS >> Software testing PDF is present FileTransfer completed: " + isFilePresent + " ";
		String fail = "FAIL >> Software testing PDF is present FileTransfer is not completed" + isFilePresent + " ";
		ALib.AssertTrueMethod(isFilePresent, pass, fail);
		sleep(3);

	}

	public void openFileManagerAndCheckFormultipleinstalledFile() throws IOException, InterruptedException {
		boolean isFilePresent = common.commonScrollMethod("dataMining.pdf");
		String pass = "PASS >> Scrolled to File and found the file: " + isFilePresent + " ";
		String fail = "FAIL >> Scrolled to File and found the file: " + isFilePresent + " ";
		ALib.AssertTrueMethod(isFilePresent, pass, fail);
		sleep(3);
		boolean isFilePresent2 = common.commonScrollMethod("test.txt");
		String pass2 = "PASS >> Scrolled to File and found the file: " + isFilePresent2 + " ";
		String fail2 = "FAIL >> Scrolled to File and found the file: " + isFilePresent2 + " ";
		ALib.AssertTrueMethod(isFilePresent2, pass2, fail2);
		sleep(3);
		boolean isFilePresent3 = common.commonScrollMethod("test2.txt");
		String pass3 = "PASS >> Scrolled to File and found the file: " + isFilePresent3 + " ";
		String fail3 = "FAIL >> Scrolled to File and found the file: " + isFilePresent3 + " ";
		ALib.AssertTrueMethod(isFilePresent3, pass3, fail3);
		sleep(3);
		boolean isFilePresent4 = common.commonScrollMethod("test3.pdf");
		String pass4 = "PASS >> Scrolled to File and found the file: " + isFilePresent4 + " ";
		String fail4 = "FAIL >> Scrolled to File and found the file: " + isFilePresent4 + " ";
		ALib.AssertTrueMethod(isFilePresent4, pass4, fail4);
		sleep(3);
	}

	public void checkTextMessageStatusOnDeviceSide() throws IOException {
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.MainFrm");
	}

	public void ClickOnMailBoxNix() {
		Initialization.driverAppium.findElementByXPath("android.widget.Button[@text='Mailbox' and @index='2']").click();
	}

	public void ClickOnMailBoxOfNix() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Mailbox']").click();
		sleep(4);
	}

	public void LaunchNix() throws InterruptedException, IOException {
		System.out.println("\nTest Case 0 : GetStartedWitCustomizeNix()");

		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.MainFrm"); // to launch SureMDM Nix App
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.MainFrm");
		Initialization.driverAppium.findElementById("com.nix:id/button2").isDisplayed(); // Settings
		System.out.println("PASS>> Settings Button is displayed in Nix Homepage hence settings file auto imported");
		Reporter.log("Step: Launched Nix");
		sleep(3);
	}

	public void clickOnTextMessageInMailBox() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Subject: 0text']").click();
		sleep(3);
		boolean MessageInMaliBox = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Normal Text. Double tap this field to edit it.']")
				.isDisplayed();
		String pass = "PASS >> Normal text is displayed";
		String fail = "FAIL >> Normal text is not displayed";
		ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
		sleep(10);
	}

	public void ForceReadMessage() {
		boolean MessageInMaliBox = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='ForceRead. Double tap this field to edit it.']")
				.isDisplayed();
		String pass = "PASS >> Force read is displayed";
		String fail = "FAIL >> Force read is not displayed";
		ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
	}

	public void ReadNotification() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell service call statusbar 1");
		sleep(3);
		boolean MessageInMaliBox = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text=' 1 Unread Messages']").isDisplayed();
		String pass = "PASS >> Force read is displayed";
		String fail = "FAIL >> Force read is not displayed";
		ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text=' 1 Unread Messages']").click();
		sleep(5);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Subject: Notify']").click();

		boolean notifyText = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='notify Text. Double tap this field to edit it.']")
				.isDisplayed();
		String pass1 = "PASS >> Force read is displayed";
		String fail1 = "FAIL >> Force read is not displayed";
		ALib.AssertTrueMethod(notifyText, pass1, fail1);
	}

	public void LockDevice() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell input keyevent 82");
		sleep(1);
		Runtime.getRuntime().exec("adb shell input text 1111");
		sleep(2);
		Runtime.getRuntime().exec("adb shell input keyevent 66");

	}

	public void changePassword() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell input keyevent 82");
		sleep(1);
		Runtime.getRuntime().exec("adb shell input text 0000");
		sleep(2);
		Runtime.getRuntime().exec("adb shell input keyevent 66");
	}

	@FindBy(xpath = ".//*[@id='app_settings_new']")
	private WebElement ApplicationSettings;

	public void ClickOnApplicationSettings() throws InterruptedException {
		ApplicationSettings.click();
		waitForidPresent("applicationSett_modal");
		sleep(4);

	}

	@FindBy(xpath = ".//*[@id='job_msg_name_input']")
	private WebElement JobNameTextField;
	@FindBy(xpath = ".//*[@id='panelApplicationList']/div[1]/div[1]/div[2]/input")
	private WebElement SearchField;

	public void ApplicationSettingJobName(String ApplicationJobName) throws InterruptedException {
		JobNameTextField.sendKeys(ApplicationJobName);
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='appSettingsTable']/tbody/tr/td[1]/span/input")
	private WebElement SelectSearchedCheckBox;

	public void SearchApplication(String SearchAplication) throws InterruptedException {
		SearchField.clear();
		SearchField.sendKeys(SearchAplication);
		waitForXpathPresent("//p[text()='" + SearchAplication + "']");
		sleep(1);
		Initialization.driver.findElement(By.xpath("//p[text()='" + SearchAplication + "']")).click();
		sleep(2);
		// SelectSearchedCheckBox.click();
	}

	public void ClickOnSelectedJob() throws InterruptedException {
		SelectSearchedCheckBox.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='addToFinalList']")
	private WebElement AddApplication;

	public void ClickOnAddApplication() throws InterruptedException {
		AddApplication.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='advanced_option']")
	private WebElement AdvanceButton;
	@FindBy(xpath = ".//*[@id='setPassword']")
	private WebElement SetPasswordField;
	@FindBy(xpath = ".//*[@id='AppLockPassword']/div/div/div[3]/button[1]")
	private WebElement OkButtonPassword;

	public void ClickOnAdvancesAndSetPassword() throws InterruptedException {
		AdvanceButton.click();
		waitForidPresent("AppLockPassword");
		sleep(2);
		SetPasswordField.clear();
		SetPasswordField.sendKeys("0000");
		OkButtonPassword.click();
		waitForidPresent("removeFromFinalList");

		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='app_settings_job_type']")
	private WebElement ApplicationTypes;
	@FindBy(xpath = ".//*[@id='app_settings_job_type']/option[text()='Run at startup']")
	private WebElement SelectRunAtStartupOption;

	public void RunAtStartUpJob(String ApplicationType) throws InterruptedException {
		ApplicationTypes.click();
		sleep(2);
		Initialization.driver
				.findElement(By.xpath(".//*[@id='app_settings_job_type']/option[text()='" + ApplicationType + "']"))
				.click();
		sleep(2);
		ApplicationTypes.click();
		sleep(2);
	}

	public void CheckApplicationLockAndEnterPassword() throws InterruptedException {
		Initialization.driverAppium.findElementByClassName("android.widget.EditText").sendKeys("0000");
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Unlock']").click();
		sleep(7);
	}

	public void checkUnlockedPlaystore() {
		boolean menu = Initialization.driverAppium.findElementById("com.android.vending:id/navigation_button")
				.isDisplayed();
		String pass1 = "PASS >> play store unlocked successfully";
		String fail1 = "FAIL >> play store not unlocked";
		ALib.AssertTrueMethod(menu, pass1, fail1);
	}

	public void LaunchChrome() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell am start -n com.android.chrome/com.google.android.apps.chrome.Main");
		sleep(5);
	}

	public void unlockChrome() {
		// com.android.chrome:id/home_button
		boolean menu = Initialization.driverAppium.findElementById("com.android.chrome:id/search_provider_logo")
				.isDisplayed();
		String pass1 = "PASS >> Chrome unlocked successfully";
		String fail1 = "FAIL >> Chrome is not unlocked";
		ALib.AssertTrueMethod(menu, pass1, fail1);
	}

	public void LaunchGoogleDrive() throws IOException, InterruptedException {
		Runtime.getRuntime().exec(
				"adb shell am start -n com.google.android.apps.docs/com.google.android.apps.docs.doclist.activity.DocListActivity");
		sleep(5);
	}

	public void CheckDataClearChrome() throws InterruptedException {
		boolean menu = Initialization.driverAppium.findElementById("com.android.chrome:id/terms_accept").isDisplayed();
		String pass1 = "PASS >> data is cleared for chrome successfully";
		String fail1 = "FAIL >> data is not cleared from chrome";
		ALib.AssertTrueMethod(menu, pass1, fail1);
		sleep(2);
	}

	public void ClearDataGoogleDrive() throws InterruptedException {
		// android.widget.TextView[@text='Skip']
		boolean menu = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Skip']")
				.isDisplayed();
		String pass1 = "PASS >> data is cleared for Drive successfully";
		String fail1 = "FAIL >> data is not cleared from Drive";
		ALib.AssertTrueMethod(menu, pass1, fail1);
		sleep(2);
	}

	public void PopupDuringUninstall() throws InterruptedException {
		Initialization.driverAppium.findElementById("android:id/button1").click();
		sleep(10);
	}

	public void CheckUninstalledApps() throws IOException, InterruptedException {
		Initialization.driverAppium.findElementById("com.android.vending:id/search_box_idle_text").click();
		Initialization.driverAppium.findElementById("com.android.vending:id/search_box_text_input").clear();
		Initialization.driverAppium.findElementById("com.android.vending:id/search_box_text_input")
				.sendKeys("SureLock Kiosk Lockdown");
		Runtime.getRuntime().exec("adb shell input keyevent 66"); // android.widget.TextView[@text='SureLock Kiosk
		// Lockdown']
		sleep(3);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureLock Kiosk Lockdown']")
				.click();
		sleep(5);
		boolean Install = Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='INSTALL']")
				.isDisplayed();
		String pass1 = "PASS >> SureLock is Uninstalled";
		String fail1 = "FAIL >> Application is not uninstalled";
		ALib.AssertTrueMethod(Install, pass1, fail1);// SureFox Kiosk Browser Lockdown
		// Initialization.driverAppium.findElementById("com.android.vending:id/search_button").clear();

		Initialization.driverAppium.findElementById("com.android.vending:id/search_button").click();
		sleep(2);
		Initialization.driverAppium.findElementById("com.android.vending:id/search_box_text_input")
				.sendKeys("SureFox Kiosk Browser Lockdown");
		sleep(4);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='SureFox Kiosk Browser Lockdown']").click();
		sleep(2);
		boolean Install2 = Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='INSTALL']")
				.isDisplayed();
		String pass2 = "PASS >> SureFox is Uninstalled";
		String fail2 = "FAIL >> SureFox is not uninstalled";
		ALib.AssertTrueMethod(Install2, pass2, fail2);
	}

	@FindBy(xpath = ".//*[@id='comp_job']")
	private WebElement CompositeJob;

	public void ClickOnCompositeJob() throws InterruptedException {
		sleep(2);
		CompositeJob.click();
		Reporter.log("PASS >> Clicked on Composite Job",true);
		waitForidPresent("comp_addDelayBtn");
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='comp_addBtn']")
	private WebElement AddButtonCompositeJob;

	public void ClickOnAddButtonCompositeJob() throws InterruptedException {
		AddButtonCompositeJob.click();
		waitForidPresent("selJob_addList_modal");
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='conmptableContainer']/div[1]/div[1]/div[2]/input")
	private WebElement SearchJobComposite;

	public void SearchJobForCompositeAction(String Jobname) throws InterruptedException {
		SearchJobComposite.sendKeys(Jobname);
		sleep(2);
		waitForXpathPresent("//p[text()='" + Jobname + "']");
		sleep(2);
		Initialization.driver
				.findElement(By.xpath(".//*[@id='CompJobDataGrid']/tbody/tr/td[2]/p[text()='" + Jobname + "']"))
				.click();
		sleep(2);
		okBtnOnFileTransferJobWindow.click();
		waitForidPresent("comp_addDelayBtn");
		sleep(2);
	}

	public void VerifyTwoSameCompositeJobs(String Jobname1, String indexJobname1, String Jobname2, String indexJobname2)
			throws InterruptedException {

		String CompJob1 = Initialization.driver
				.findElement(By.xpath("(//*[@id='subJobsTable']/tbody/tr/td/p[contains(text(),'" + Jobname1 + "')])['"
						+ indexJobname1 + "']"))
				.getText();
		System.out.println(CompJob1);
		String CompJob2 = Initialization.driver
				.findElement(By.xpath("(//*[@id='subJobsTable']/tbody/tr/td/p[contains(text(),'" + Jobname2 + "')])['"
						+ indexJobname2 + "']"))
				.getText();
		System.out.println(CompJob2);

		if (CompJob1.equals(CompJob2)) {

			Reporter.log("Both composite Jobs are equal", true);

		}

		else {

			ALib.AssertFailMethod("Both composite Jobs are not equal");

		}

	}

	@FindBy(xpath = "//*[@id='comp_rules_list']/li[6]/a/span[text()='Password Policy']")
	private WebElement PasswordPolicyClick;

	public void ClickOnPasswordPolicyComplianceJob() throws InterruptedException {
		PasswordPolicyClick.click();
		waitForXpathPresent(
				"//*[@id='passcode_BLK']/div[1]/div/p[text()='Create compliance rules for Password Policy on devices.']");
	}

	public void outOfComplianceActions(String complianceAction, int indexforOutOfComplianceActions)
			throws InterruptedException {
		WebElement OutOfComplianceActionsDropDownMenu = Initialization.driver.findElement(
				By.xpath("(//select[@id='ComplianceActionOption'])[" + indexforOutOfComplianceActions + "]"));

		Select outOfCompliance = new Select(OutOfComplianceActionsDropDownMenu);
		outOfCompliance.selectByVisibleText(complianceAction);
		if (outOfCompliance.getFirstSelectedOption().getText().equalsIgnoreCase("E-Mail Notification")) {
			sleep(1);
			EnterEmailID.sendKeys("complianceEmailID@mailinator.com");
		}

		if (outOfCompliance.getFirstSelectedOption().getText().equalsIgnoreCase("Send Sms")) {
			sleep(1);
			enterMobileNumber.sendKeys("+916366231499");
		}

	}

	@FindBy(xpath = "//span[text()='Compliance Job']")
	private WebElement ComplianceJobNew;

	/*
	 * @FindBy(xpath="(//select[@id='ComplianceActionOption'])[2]") private
	 * WebElement OutOfComplianceActionsDropDownMenuClick;
	 */

	@FindBy(xpath = "//*[@id='passcode_BLK']/div[2]/div[2]/div[2]/div[2]/div[1]/div[5]/div/div/input")
	private WebElement EnterEmailID;

	@FindBy(xpath = "//*[@id='passcode_BLK']/div[2]/div[2]/div[2]/div[2]/div[1]/div[5]/div/div/input")
	private WebElement enterMobileNumber;
	@FindBy(xpath = ".//*[@id='comp_job_min_version_num']")
	private WebElement AndroidVersionsCompositeJob;
	@FindBy(xpath = ".//*[@id='comp_job_min_version_num']/option[text()='Lollipop(5.0)']")
	private WebElement LolipopVersion;
	@FindBy(xpath = ".//*[@id='comp_job_min_version_logic']")
	private WebElement VersionRestriction;

	public void SelectAndroidVersionsCompositeJob(String Version, String Symbol) throws InterruptedException {
		AndroidVersionsCompositeJob.click();
		sleep(2);
		Initialization.driver
				.findElement(By.xpath(".//*[@id='comp_job_min_version_num']/option[text()='" + Version + "']")).click();

		VersionRestriction.click();
		sleep(3);
		Initialization.driver
				.findElement(By.xpath(".//*[@id='comp_job_min_version_logic']/option[text()='" + Symbol + "']"))
				.click();
		sleep(2);

	}

	public void EnterJobNameTextField(String name) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='job_name']"));
		ls.get(0).sendKeys(name);
	}

	public void CheckApplicationIsInstalledDeviceSide() {
		boolean surevideo = Initialization.driverAppium.isAppInstalled("com.gears42.surevideo");
		String pass2 = "PASS >> SureVideo is installed";
		String fail2 = "FAIL >> SureVideo is Not installed";
		ALib.AssertTrueMethod(surevideo, pass2, fail2);
	}

	public void CHeckTextMessageComposite() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Subject: test']").click();
		sleep(3);
		boolean MessageInMaliBox = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='meeting @4pm']").isDisplayed();
		String pass = "PASS >> Composite text is displayed";
		String fail = "FAIL >> Composite text is not displayed ";
		ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
		sleep(10);
	}

	@FindBy(xpath = ".//*[@id='logContainer']/div[2]/button")
	private WebElement LogsMenu;
	@FindBy(xpath = ".//*[@id='logsOperationMenu']/li[1]")
	private WebElement filterOption;
	@FindBy(xpath = ".//*[@id='enable_logfilter']")
	private WebElement EnableFilter;
	@FindBy(xpath = ".//*[@id='logFilter_deviceName']")
	private WebElement DeviceNameFilter;
	@FindBy(xpath = ".//*[@id='LogFilter_okbtn']")
	private WebElement OkButtonFilter;

	public void OsVersionMissMatchMessage() {
		LogsMenu.click();
		filterOption.click();
		waitForidPresent("logsFilter_modal");
		EnableFilter.click();
		DeviceNameFilter.sendKeys("donotdelete");
		OkButtonFilter.click();
		waitForidPresent("logContainer");
		List<WebElement> optionvalues = Initialization.driver
				.findElements(By.xpath(".//*[@id='logContent']/ul/li and contains(text(),'')"));

	}

	@FindBy(xpath = "//*[@id='blWhitelistBtn']/i")
	private WebElement WhiteListed;

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement WhiteListYesButtonPopUp;

	@FindBy(xpath = "//*[@id='all_ava_devices']/span[text()='All Devices']")
	private WebElement AllDevice;

	@FindBy(xpath = "//*[@id='blacklistGrid']/tbody/tr/td[1][text()='" + Config.DeviceName + "']")
	private WebElement DeviceInBlackList;

	@FindBy(xpath = ".//*[@id='comp_job__name_input']")
	private WebElement CompositeJobName;

	public void AllDeviceOption() throws InterruptedException {
		AllDevice.click();
		sleep(5);

	}

	public void VerifyDevicePresentInBlackList() throws InterruptedException {
		boolean DeviceInsideBlackListSection = DeviceInBlackList.isDisplayed();
		String PASS = "PASS>> Device is Present inside BlackList";
		String FAIL = "FAIL>> Device is NOT Present inside BlackList";
		ALib.AssertTrueMethod(DeviceInsideBlackListSection, PASS, FAIL);
		sleep(2);
	}

	public void AddButtonClick(String JobSelect) throws InterruptedException {

		AddButton.click();
		sleep(2);
		waitForXpathPresent("//*[@id='selJob_addList_modal']/div[1]/h4[text()='Select Jobs To Add']");
		SearchBox.sendKeys(JobSelect);
		sleep(4);
		RefReshBtn.click();
		sleep(2);
		JTouchJob.click();
		sleep(2);
		okbutton.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='passcode_BLK']/div[2]/div[2]/div[2]/div[2]/div[1]/div[5]/div/div/span[1]/button[text()='Add Job']")
	private WebElement AddButton;

	@FindBy(xpath = "//*[@id='conmptableContainer']/div[1]/div[1]/div[2]/input")
	private WebElement SearchBox;

	@FindBy(xpath = "//*[@id='CompJobDataGrid']/tbody/tr/td[2]/p[text()='JTouch Job']")
	private WebElement JTouchJob;

	@FindBy(xpath = "(//*[@id='okbtn'])[1]")
	private WebElement okbutton;

	@FindBy(xpath = "//*[@id='conmptableContainer']/div[1]/div[1]/div[1]")
	private WebElement RefReshBtn;

	public void VerifyPasswordPolicyPopUpInDeviceSide() throws InterruptedException {
		if (Initialization.driverAppium.findElementById("com.nix:id/SetPwdButton").isDisplayed()) {

			Reporter.log("PASS >> PasswordPolicyPopUp", true);
		} else {
			ALib.AssertFailMethod("FAIL >> PasswordPolicyPopUp");
		}
	}

	@FindBy(xpath = "//*[@id='blacklistedSection']/a/p")
	private WebElement blacklistedSection;
	@FindBy(xpath = "//*[@id='blRefreshBtn']")
	private WebElement blRefreshBtn;

	public void ClickOnBlackListButton() throws InterruptedException {
		blacklistedSection.click();
		sleep(4);
		// RefreshButton.click();
		blRefreshBtn.click();
		sleep(6);
		// waitForXpathPresent("//*[@id='blacklistGrid']/tbody/tr/td[1][text()="+Config.DeviceName+"]");
	}

	public void VerifyIfDevicePresentThenMoveToWhiteList() throws InterruptedException {
		DeviceInBlackList.click();
		sleep(1);
		WhiteListed.click();
		sleep(1);
		waitForXpathPresent(
				"//*[@id='ConfirmationDialog']/div/div/div[1]/p[text()='Are you sure you want to whitelist the device(s)?']");
		waitForXpathPresent(
				"//*[@id='ConfirmationDialog']/div/div/div[1]/p[text()='Are you sure you want to allowlist the device(s)?']");
		sleep(2);
		WhiteListYesButtonPopUp.click();
		waitForXpathPresent("//span[text()='Device(s) allowlisted successfully.']");
		// sleep(2);

	}

	public void SelectAndroidVersionsCompositeJobForEquals(String Version) throws InterruptedException {
		AndroidVersionsCompositeJob.click();
		sleep(2);
		Initialization.driver
				.findElement(By.xpath(".//*[@id='comp_job_min_version_num']/option[text()='" + Version + "']")).click();
		sleep(2);
	}

	public void CHeckTextMessageCompositeVersionEqualTo() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Subject: madman']").click();
		sleep(3);
		boolean MessageInMaliBox = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='equal to version']").isDisplayed();
		String pass = "PASS >> Composite Job text is displayed";
		String fail = "FAIL >> Composite Job text is not displayed ";
		ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
		sleep(10);
	}

	public void FileTransferForEqualtoVersion() throws InterruptedException {
		boolean isFilePresent2 = common.commonScrollMethod("test.txt");
		String pass2 = "PASS >> Scrolled to File and found the file: " + isFilePresent2 + " ";
		String fail2 = "FAIL >> Scrolled to File and found the file: " + isFilePresent2 + " ";
		ALib.AssertTrueMethod(isFilePresent2, pass2, fail2);
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='installConfigTable']/tbody/tr/td[1]/p[text()='surevideo.apk']")
	private WebElement SelectApk;

	public void CheckAllOptionsinInstallJob() throws InterruptedException {
		SelectApk.click();
		boolean OptionPresent1 = addBtn.isDisplayed();
		String pass1 = "PASS >> Add Button is displayed";
		String fail1 = "FAIL >> Add button is not displayed";
		ALib.AssertTrueMethod(OptionPresent1, pass1, fail1);
		sleep(2);
		boolean OptionPresent2 = deleteJob.isDisplayed();
		String pass2 = "PASS >> Delete Button is displayed";
		String fail2 = "FAIL >> delete button is not displayed";
		ALib.AssertTrueMethod(OptionPresent2, pass2, fail2);
		sleep(2);
		boolean OptionPresent3 = modifyJob1.isDisplayed();
		String pass3 = "PASS >> Modify Button is displayed";
		String fail3 = "FAIL >> Modify button is not displayed";
		ALib.AssertTrueMethod(OptionPresent3, pass3, fail3);
		sleep(2);
		boolean OptionPresent4 = moveUp.isDisplayed();
		String pass4 = "PASS >> Delete Button is displayed";
		String fail4 = "FAIL >> delete button is not displayed";
		ALib.AssertTrueMethod(OptionPresent4, pass4, fail4);
		sleep(2);
		boolean OptionPresent5 = moveDown.isDisplayed();
		String pass5 = "PASS >> Modify Button is displayed";
		String fail5 = "FAIL >> Modify button is not displayed";
		ALib.AssertTrueMethod(OptionPresent5, pass5, fail5);
		sleep(2);

	}

	@FindBy(xpath = "//li[@id='jobQueueHistoryTab']")
	private WebElement JobHistoryTab;

	public void clickOnJobhistoryTab() throws InterruptedException {
		JobHistoryTab.click();
		waitForXpathPresent("//div[@id='jobQueueHistoryRefreshBtn']");
		sleep(5);
	}

	@FindBy(xpath = "//div[@id='jobQueueButton']")
	private WebElement JobQueueButton;

	@FindBy(xpath = "//table[@id='jobQueueHistoryGrid']/tbody/tr[1]/td[6]")
	private WebElement AppliedJobStatus;

	@FindBy(xpath = "//div[@id='device_jobQ_modal']/div[1]/button")
	private WebElement JobQueueCloseButton;

	@FindBy(xpath = "(//i[@class='st-jobStatus dev_st_icn good fa fa-check-circle'])[1]")
	private WebElement JobSuccess;

	@FindBy(xpath = "//select[@id='download_job_via']")
	private WebElement NetworkType;

	@FindBy(xpath = "//div[@id='job_new_folder']")
	private WebElement NewFolder_Job;

	@FindBy(xpath = "//input[@id='jobFolderName']")
	private WebElement FolderNameTextField;

	@FindBy(xpath = "//button[@id='foldername_ok']")
	private WebElement FolderCreateOkButton;

	@FindBy(xpath = "//div[@id='jobPageHolder']/div[1]/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement FolderSearcTextBox;

	@FindBy(xpath = "//div[@id='backToParent']")
	private WebElement FolderCreatedBackButton;

	@FindBy(xpath = "//span[text()='New folder created.']")
	private WebElement FolderCreateSuccessMessage;

	public void clickingOnDoneButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, 40);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Done']").click();
		Reporter.log("Clicked on Done button", true);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//android.widget.TextView[@text='Online']")));
		sleep(50);
	}

	public void clickOnDoneBtnInSettings() {
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, 40);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Done']").click();
		Reporter.log("Clicked on Done button", true);
	}

	public void DisableNixChangePwd(String EnterOldpwd) throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Change Password']").click();
		Initialization.driverAppium.findElementById("com.nix:id/oldPassword").sendKeys(EnterOldpwd);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='CHANGE']").click();
	}

	public void clickonJobQueue() throws InterruptedException {
		JobQueueButton.click();
		waitForXpathPresent("//div[@id='jobQueueRefreshBtn']");
		sleep(4);
	}

	public void EnableNix() throws InterruptedException {
		try {
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox").isDisplayed();
		String nixcheckbox = Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox").getAttribute("checked");
		sleep(2);
		if (nixcheckbox.equals("true")) {
			Reporter.log("Nix Service Is Already Enabled", true);
			sleep(2);
		} else {
			Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox").click();
			Reporter.log("Nix Service was Disabled Hence Enabled now", true);
		}
		}catch (Exception e) {
			Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox").click();
			Reporter.log("Nix Service was Disabled Hence Enabled now", true);
		}
	}

	public void ClikOnNixSettings() throws IOException, InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Settings']").click();
		sleep(2);
	}

	public void ClickOnjobQueueForMultipleDevices() throws InterruptedException {
		JobQueueButton.click();
		waitForXpathPresent("//a[text()='Pending']");
		sleep(3);
	}

	public void AfterDepolyementValidationWhetherDDeviceSideTimeIsChangedAfterFiveMins() {

		WebDriverWait wait1 = new WebDriverWait(Initialization.driverAppium, 480);
		boolean flag;
		try {
			wait1.until(ExpectedConditions.presenceOfElementLocated(
					By.xpath("//android.widget.TextView[contains(@text,'" + AddedTime + "')]")));
			flag = true;
		} catch (Exception e) {
			System.out.println("fail");
			flag = false;
		}
		String UpdateTimeSync = Initialization.driverAppium
				.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + AddedTime + "')]")).getText();
		System.out.println(UpdateTimeSync);
		String PassStatement = "" + UpdateTimeSync + ": Updated Time is displayed.";
		String FailStatement = "" + UpdateTimeSync + ": Updated Time is not displayed.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	public void VerifyJobStatusInsideJobQueue() {
		String ActualVal = AppliedJobStatus.getText();

		String Expected = "Deployed";
		String Pass = "PASS >>> Job Status Is Deployed Inside Job Queue";
		String Fail = "FAIL >>> Job Status Is" + " " + ActualVal + " " + "Inside Job Queue";
		ALib.AssertEqualsMethod(Expected, ActualVal, Pass, Fail);
	}

	public void ClickOnJobQueueCloseButton() throws InterruptedException {
		try {
			JobQueueCloseButton.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(2);
		} catch (Exception e) {

		}
	}

	public void SelectingMultipleJobs(String JobName) throws AWTException, InterruptedException {
		WebElement job1 = Initialization.driver
				.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]"));
		WebElement job2 = Initialization.driver
				.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr[2]/td[2]"));
		action.keyDown(Keys.CONTROL).click(job1).click(job2).keyUp(Keys.CONTROL).build().perform();
		sleep(2);
		OkButtonApplyJob.click();
		waitForXpathPresent("//span[contains(text(),'Initiated')]");
		sleep(2);
	}

	public void CheckingJobStatusUsingConsoleLogs(String JobName) {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 140);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[contains(text(),'The job named " + "\""
					+ JobName + "\" was successfully deployed on the device named " + "\"" + Config.DeviceName
					+ "\"')]")));
			Reporter.log("PASS>>> Job Successfully Deployed On Device", true);
			sleep(3);
		} catch (Exception a) {
			ALib.AssertFailMethod("FAIL>>> Job Is Not Deployed On Device");
		}

	}

	public void SelectingNetworkType() {
		Select sel = new Select(NetworkType);
		sel.selectByValue("0");
	}

	public void ClickOnNewFolder() throws InterruptedException {
		NewFolder_Job.click();
		waitForXpathPresent("//h4[text()='Create Folder']");
		sleep(4);
	}

	public void NamingFolder(String FolderName) throws InterruptedException {
		FolderNameTextField.sendKeys(FolderName);
		FolderCreateOkButton.click();
		try {
			FolderCreateSuccessMessage.isDisplayed();
			Reporter.log("New folder created Message Is Displayed", true);
			sleep(4);
		} catch (Exception e) {
			Reporter.log("New folder created Message Is Not Displayed", true);
			sleep(4);
		}
	}

	public void SearchingJobFolder(String FolderName) throws InterruptedException {
		SearchJobField.clear();
		sleep(2);
		SearchJobField.sendKeys(FolderName);
		waitForXpathPresent("//p[text()='" + FolderName + "']");
		sleep(3);
	}

	public void DoubleClickOnFolder(String id, String JobFolderName) throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Initialization.driver.findElement(By.xpath("//p[text()='" + JobFolderName + "']"))).perform();
		waitForidPresent(id);
	}

	public void DoubleClickOnFolderNixUpgradeJob(String FolderName) throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Initialization.driver.findElement(By.xpath("//p[text()='" + FolderName + "']"))).perform();
		// waitForidPresent(id);
		sleep(3);
	}

	public void BackFromFolder() throws InterruptedException {
		FolderCreatedBackButton.click();
		waitForidPresent("job_new_job");
		sleep(3);
	}

	@FindBy(xpath = "//li[@class='aplyOpt']")
	private WebElement Rightclickapply;

	@FindBy(xpath = "//div[@id='groupstree']/ul/li[text()='dontouch']")
	private WebElement rightclicksubgrp;

	public void VerifyrightclickGroupApply() throws InterruptedException {

		Actions act = new Actions(Initialization.driver);
		act.moveToElement(rightclicksubgrp).contextClick(rightclicksubgrp).perform();
		sleep(10);
		Rightclickapply.click();
		sleep(15);

	}

	@FindBy(xpath = ".//*[@id='applyJob_okbtn']")
	private WebElement OkButtonApplyJob;

	public void ClickOnApplyJobOkButtonForGroup() throws InterruptedException {

		// This method is created separately as while applying on group a new pop up
		// needs to be handled and also job initiated message does not come.
		OkButtonApplyJob.click();
		sleep(5);

	}

	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement Applyjobyesbutton;

	public void PopUpGroupApplyYes() throws InterruptedException {
		Applyjobyesbutton.click();
		sleep(3);

	}

	@FindBy(css = "div#jobQueueRefreshBtn")
	private WebElement incompleteJobRefreshButton;

	@FindBy(xpath = "//table[@id='jobQueueHistoryGrid']/tbody/tr[1]/td[2]")
	private WebElement LatestJobNameInsideHistory;

	@FindBy(xpath = "//input[@id='job_name_input']")
	private WebElement JobNameField;

	@FindBy(xpath = "//a[@id='locationTrackingEditBtn']/preceding-sibling::span")
	private WebElement DeviceINfoLocationTrackingStatus;

	@FindBy(xpath = "//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[6]/p")
	private WebElement IncompleteJobStatus;

	public int DurationOfJobAsInprogress = 0;

	public void CheckStatusOfappliedInstalledJob(String JobName, int MaximumTime) throws InterruptedException {
		clickonJobQueue();
		try {

			WebDriverWait wait1 = new WebDriverWait(Initialization.driver, MaximumTime);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='No Pending jobs']")))
					.isDisplayed();
			Reporter.log("PASS >> Job Deployed Successfully On Device", true);
			sleep(3);
			clickOnJobhistoryTab();
			System.out.println("Clicked On Job Completed Tab");
			String LatestJobName = LatestJobNameInsideHistory.getText();
			if (LatestJobName.equals(JobName)) {
				VerifyJobStatusInsideJobQueue();
			} else {
				ClickOnJobQueueCloseButton();
				ALib.AssertFailMethod("FAIL >> Job Name Is Different In Job Queue");
			}
		} catch (Exception e) {
			String CurrentIncompletedJobStatus = IncompleteJobStatus.getText();
			System.out.println("Incompleted Job Status is: " + CurrentIncompletedJobStatus);
			ClickOnJobQueueCloseButton();
			ALib.AssertFailMethod("FAIL >> Job Did not deployed within given time");
		}
		ClickOnJobQueueCloseButton();
	}

	public void ClearSearchJobInSearchField() throws InterruptedException {
		SearchJobField.clear();

		sleep(4);

	}

	public void SendingPasswordToEnterNixSettings(String Password) throws InterruptedException {
		try {
			boolean flag = Initialization.driverAppium.findElementById("com.nix:id/password_edit").isDisplayed();
			if (flag) {
				Reporter.log("PASS>>> Password Is Set To Enter Nix Settings", true);
			} else {
				ALib.AssertFailMethod("FAIL>>> Password Is Not Set To Enter Nix Settings");
			}

		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Password Is Not Set To Enter Nix Settings");
		}
		Initialization.driverAppium.findElementById("com.nix:id/password_edit").sendKeys(Password);
		sleep(2);
		Initialization.driverAppium.findElementById("android:id/button1").click();
	}

	public void AppiumSideVerificationScheduleShutDown() throws InterruptedException {
		{
			try {
				Initialization.driverAppium
						.findElementByXPath("//android.widget.Button[@text='Settings' and @index='0']").isDisplayed();

				ALib.AssertFailMethod("FAIL >>>Settings Button Is Still Displayed In Device");
			}

			catch (Exception e) {
				Reporter.log("PASS >>>Settings Button IS NOt Displayed In Device", true);
			}
		}
	}

	@FindBy(xpath = "//div[@id ='comp_addBtn']")
	private WebElement CompositeAddJobBtn;

	@FindBy(xpath = "(//button[contains(text(),'Add Job')])[1]")
	private WebElement ComplianceAddJobButton;

	public void WaitForShutdownn() throws InterruptedException {
		sleep(230);
	}

	public String CurrentTime;
	public String AddedTime;

	public void DeviceSideTimeSyncWithServerNixAgentSettingJob() throws InterruptedException {

		Date dNow = new Date(); // Instantiate a Date object
		SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");
		CurrentTime = dateformat.format(dNow);
		System.out.println(CurrentTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dNow);
		cal.add(Calendar.MINUTE, 5);
		String dNow1 = cal.getTime().toString();
		AddedTime = dNow1.substring(11, 16);
		System.out.println(AddedTime);
		sleep(4);

	}

	int NumbofJobs;

	public void GettingInitialCountOfJobsInJobQueue(String TabName, int TabNumber) throws InterruptedException {
		// ClickonJobQueue();
		String NumberofJobsBeforeApplyingJob = Initialization.driver
				.findElement(By.xpath("//div[@id='jobQ-info-container']/div[" + TabNumber + "]/div[2]")).getText();
		NumbofJobs = Integer.parseInt(NumberofJobsBeforeApplyingJob);
		Reporter.log("Number Of jobs Present In" + " " + TabName + " " + "before applying the job" + "is" + "  "
				+ NumbofJobs, true);
		ClickOnJobQueueCloseButton();
	}

	public void ClickOnJobType(String JObName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='" + JObName + "']")).click();
		sleep(5);
	}

	public void ClickOnGeoFenceJobType(String JObName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='" + JObName + "']")).click();
		waitForXpathPresent("//button[@title='Zoom in']");
		sleep(5);
	}

	public void SendingJobName(String JobName) throws InterruptedException {

		JobNameField.clear();
		sleep(2);
		JobNameField.sendKeys(JobName);
		sleep(2);
	}

	public void EnablingLocationTrackingCheckBox() throws InterruptedException {
		boolean val = Initialization.driver.findElement(By.xpath("//input[@id='job_check']")).isSelected();
		if (val) {
			Reporter.log("Location Tracking Is Already Enabled", true);
			sleep(2);
		}

		else {
			Initialization.driver.findElement(By.xpath("//input[@id='job_check']")).click();
			Reporter.log("Location Tracking Is Enabled Now", true);
		}
	}

	public void ClickOnLocatioTrackingJobOkButton() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//button[@id='okbtn']")).click();
		waitForXpathPresent("//span[contains(text(),'successfully.')]");
		sleep(3);
	}

	public void VerifyLocationTrackingONStatusInConsole() {
		try {
			Initialization.driver.findElement(By.xpath(
					"//i[@class='locTrack_icn fa fa-map-marker tracking-color-red' or @class='locTrack_icn fa fa-map-marker tracking-color-green']"))
					.isDisplayed();
			Reporter.log("Location Tracking Is On In Console", true);
		}

		catch (Exception e) {
			if (DeviceINfoLocationTrackingStatus.getText().contains("ON")) {
				Reporter.log("Location Tracking Is ON But Location Symbol Is Not Updated ", true);
			} else {
				ALib.AssertFailMethod("Location Tracking Is Off In Console Even After Applying Job");
			}
		}
	}

	@FindBy(xpath = "//span[text()='Job deleted successfully.']")
	private WebElement JobDeletedSuccessfullyMessage;

	public void JobDeletedMessage() throws InterruptedException {
		sleep(2);
		if (JobDeletedSuccessfullyMessage.isDisplayed()) {
			Reporter.log("Job deleted successfully message is displayed", true);
		} else {
			ALib.AssertFailMethod("Job deleted successfully message is not displayed");
		}
	}

	public void VerifyOfDeletedJob() {
		try {

		} catch (Exception e) {

		}
	}

	public void CheckStatusOfappliedInstalledJobTAGS(String JobName, int MaximumTime) throws InterruptedException {

		try {

			WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 4);
			while (wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='" + JobName + "']")))
					.isDisplayed()) {
				sleep(4);
				incompleteJobRefreshButton.click();
				waitForXpathPresent("//li[@id='jobQueueHistoryTab']");
				sleep(1);
				DurationOfJobAsInprogress += 5;
				if (DurationOfJobAsInprogress >= MaximumTime) {
					ClickOnJobQueueCloseButton();
					ALib.AssertFailMethod("Job Is Not Deployed WithIn Given Time");
				}
			}
		}

		catch (Exception e)

		{

			waitForXpathPresent("(//div[contains(text(),'No Job available')])[1]");
			sleep(4);
			CompletedJobsSection();
			sleep(2);
			String LatestJobName = LatestJobNameInsideHistoryCompletedJobs.getText();
			if (LatestJobName.equals(JobName)) {
				VerifyJobStatusInsideJobQueue();
			}
		}

		ClickOnJobQueueCloseButton();

	}

	public void CompletedJobsSection() throws InterruptedException {
		LatestJobNameInsideHistoryCompletedJobs.click();
		waitForXpathPresent("//*[@id='jobQueueRefreshBtn']/i");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='jobQueueButton']/i")
	private WebElement jobQueueButton;

	public void clickOnJobhistoryTabTAGS() throws InterruptedException {
		jobQueueButton.click();
		waitForXpathPresent("//*[@id='jobQueueRefreshBtn']/i");
		sleep(2);
	}

	@FindBy(id = "job_delete")
	private WebElement jobDelete;

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement ClickOnYesButton;

	public void deleteCreatedJob() throws InterruptedException {
		jobDelete.click();
		sleep(2);
		ClickOnYesButton.click();
	}

	public void ClickOnOnJob(String JObName) throws InterruptedException {
		// Initialization.driver.findElement(By.xpath("//p[text()='" + JObName +
		// "']")).click();
		Initialization.driver
				.findElement(By.xpath("//table[@id='jobDataGrid']/tbody/tr/td/p[text()='" + JObName + "']")).click();

		sleep(2);
	}

	public void DisablingLocationTrackingCheckBox() throws InterruptedException {
		boolean val = Initialization.driver.findElement(By.xpath("//input[@id='job_check']")).isSelected();
		if (val) {
			Initialization.driver.findElement(By.xpath("//input[@id='job_check']")).click();
			Reporter.log("Location Tracking Is Disabled Now", true);
			sleep(2);
		}

		else {

			Reporter.log("Location Tracking Is Already Disabled", true);
		}
	}

	public void VerifyLocationTrackingOFFStatusInConsole() {
		try {
			Initialization.driver
					.findElement(By.xpath("//i[@class='locTrack_icn fa fa-map-marker tracking-color-orange']"))
					.isDisplayed();
			Reporter.log("Location Tracking Is OFF In Console", true);
		}

		catch (Exception e) {
			if (DeviceINfoLocationTrackingStatus.getText().contains("ON")) {
				ALib.AssertFailMethod("Location Tracking Is ON In Console Even After Applying Job");
			} else {
				Reporter.log("Location Tracking Is OFF But Location Symbol Is Not Updated ", true);
			}
		}
	}

	public void SendingSecurityPolicyJobName(String jobNAme) {
		Initialization.driver.findElement(By.xpath("//input[@id='job_sp_name_input']")).sendKeys(jobNAme);
	}

	public void SelectingPasswordPolicyInDevice(int i) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("(//input[@name='ifEnablePwd'])[" + i + "]")).click();
		sleep(2);
	}

	@FindBy(xpath = "//select[@id='job_sp_input_3']")
	public WebElement MinimumPasswordDropDown;

	@FindBy(xpath = "//select[@id='job_sp_input_4']")
	public WebElement PasswordStrengthDropDown;

	@FindBy(xpath = "//select[@id='job_sp_input_5']")
	public WebElement TimeLapseDropDown;

	@FindBy(xpath = "//select[@id='job_sp_input_6']")
	public WebElement PasswordAttemptsDropDown;

	@FindBy(xpath = "//a[text()='Peripheral Settings']")
	private WebElement PeripheralSettingsTab;

	@FindBy(xpath = "//input[@id='job_sp_input_7']")
	private WebElement EnforcePeripheralSettingsCheckBox;

	@FindBy(xpath = "//select[@id='job_sp_input_11']")
	public WebElement SecurityPloicy_PeripheralGPSDropDown;

	@FindBy(xpath = "//select[@id='job_sp_input_10']")
	public WebElement SecurityPolicy_PeripheralMobileDataDropDown;

	public void SelectingSecurityPolicyParameters(WebElement WebElement, String Value) throws InterruptedException {
		Select sel = new Select(WebElement);
		sel.selectByValue(Value);
		sleep(3);

	}

	public void VerifySecurPolicyPasswordProfileDeployedOnDevice() {
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Set Password Now']")
					.isDisplayed();
			Reporter.log("PASS>>> Security Policy - Password Policy Job Is Deployed Successfully To The Device", true);
		} catch (Exception e) {
			try {
				Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Set password']")
						.isDisplayed();
				Reporter.log("PASS>>> Security Policy - Password Policy Job Is Deployed Successfully To The Device",
						true);
			} catch (Exception k) {
				ALib.AssertFailMethod("FAIL>>> Security Policy -Password PolicyJob Is Not Deployed Successfully");
			}
		}
	}

	public void AppiumConfigurationForMultipleDevices(String appPackage, String appActivity, String Device)
			throws MalformedURLException, InterruptedException {

		System.out.println("Launchig Appium");
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "Nokia 6.1");
		cap.setCapability("platformName", "Android");
		if (Device.equals("3200ec4c4e7a762d")) {
			cap.setCapability("platformVersion", "9.0");
			cap.setCapability("udid", "3200ec4c4e7a762d");
		} else {
			cap.setCapability("platformVersion", "8");
			cap.setCapability("udid", "BKFDU17401001226");
		}
		cap.setCapability("appPackage", appPackage);
		cap.setCapability("appActivity", appActivity);
		cap.setCapability("noReset", "true");
		Initialization.driverAppium = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		sleep(5);
	}

	public void AppiumConfigurationDevice1(String appPackage, String appActivity, String Device)
			throws MalformedURLException, InterruptedException {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "Nokia 6.1");
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "9.0");
		cap.setCapability("udid", Device);
		cap.setCapability("appPackage", appPackage);
		cap.setCapability("appActivity", appActivity);
		cap.setCapability("noReset", "true");
		Initialization.driverAppium1 = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4733/wd/hub"), cap);
		sleep(5);
	}

	public void ClickOnConnectionInSettings() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Connections']").click();
		sleep(2);
	}

	public void EnablingBluetoothInDevice() throws InterruptedException {
		String BluetoothStatus = Initialization.driverAppium
				.findElementByXPath("//android.widget.Switch[@content-desc='Bluetooth']").getText();
		System.out.println(BluetoothStatus);
		if (BluetoothStatus.equalsIgnoreCase("OFF")) {
			Initialization.driverAppium.findElementByXPath("//android.widget.Switch[@content-desc='Bluetooth']")
					.click();
			Reporter.log("Bluetooth Is Turned On", true);
		} else if (BluetoothStatus.equalsIgnoreCase("ON")) {
			Reporter.log("BlueTooth Is Already On In Device", true);
		}
	}

	public void SwitchingToPeripheralSettingsTab() throws InterruptedException {
		PeripheralSettingsTab.click();
		sleep(3);
	}

	public void EnablingCheckBoxesInPeripheralSettigsTab(String id, String CheckBoxName) throws InterruptedException {
		boolean val = Initialization.driver.findElement(By.xpath("//input[@id='job_sp_input_" + id + "']"))
				.isSelected();
		if (val) {
			Reporter.log(CheckBoxName + " " + "Is Already Selected", true);
		} else {
			Initialization.driver.findElement(By.xpath("//input[@id='job_sp_input_" + id + "']")).click();
			Reporter.log(CheckBoxName + " " + "Is Enabled");
		}
		sleep(2);
	}

	public void DisablingCheckBoxesInPeriPheralSettingsTab(String id, String CheckBoxName) throws InterruptedException {
		boolean val = Initialization.driver.findElement(By.xpath("//input[@id='job_sp_input_" + id + "']"))
				.isSelected();
		if (val) {
			Initialization.driver.findElement(By.xpath("//input[@id='job_sp_input_" + id + "']")).click();
			Reporter.log(CheckBoxName + " " + "Is Disabled", true);
		} else {

			Reporter.log(CheckBoxName + " " + "Is Already Disabled", true);
		}
		sleep(2);
	}

	public void VerifyBlueToothStatusAfterApplyingDisableBluetoothJob() {
		String BluetoothStatus = Initialization.driverAppium
				.findElementByXPath("//android.widget.Switch[@content-desc='Bluetooth']").getText();
		System.out.println(BluetoothStatus);
		if (BluetoothStatus.equalsIgnoreCase("OFF")) {
			Reporter.log("PASS>>> Bluetooth Status Is Disabled In Device", true);
		} else {
			ALib.AssertFailMethod("FAIL>>> Bluetooth Status Is Enabled In Device");
		}
	}

	public void EnablingWifiInDevice() {
		String WifiStatus = Initialization.driverAppium
				.findElementByXPath("//android.widget.Switch[@content-desc='Wi-Fi']").getText();
		System.out.println(WifiStatus);
		if (WifiStatus.equalsIgnoreCase("OFF")) {
			Initialization.driverAppium.findElementByXPath("//android.widget.Switch[@content-desc='Wi-Fi']").click();
			Reporter.log("WiFi Is Turned On", true);
		} else if (WifiStatus.equalsIgnoreCase("ON")) {
			Reporter.log("WiFi Is Already On In Device", true);
		}
	}

	public void VerifyWiFiStatusAfterApplyingDisableWiFiJOb() {
		String WifiStatus = Initialization.driverAppium
				.findElementByXPath("//android.widget.Switch[@content-desc='Wi-Fi']").getText();
		System.out.println(WifiStatus);
		if (WifiStatus.equalsIgnoreCase("OFF")) {

			Reporter.log("PASS>>> WiFi Is Turned Off");
		}

		else {
			ALib.AssertFailMethod("FAIL>>> WiFi Status Is Enabled In Device");
		}
	}

	public void SearchingForOptionsInSettings(String SearchContent) throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@content-desc='Search']").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='1']").sendKeys(SearchContent);
		sleep(2);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='" + SearchContent + "' and @index='1']").click();
		sleep(6);

	}

	public void VeifyLocationStatusAfterApplyingSecurityPolicy_GPS_AlwaysOFFJob() {
		String LocationSwitchStatus = Initialization.driverAppium
				.findElementById("com.android.settings:id/switch_widget").getText();
		if (LocationSwitchStatus.equalsIgnoreCase("Location, Off")) {
			Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").click();
			String LocationStatusAfterClickingLocationSwitch = Initialization.driverAppium
					.findElementById("com.android.settings:id/switch_widget").getText();
			if (!LocationStatusAfterClickingLocationSwitch.equalsIgnoreCase("Location, Off")) {
				ALib.AssertFailMethod("FAIL>>> Loction Tracking Status Is On Eveen After Applying GPS_ALWAYSOFF Job");
			}

			else {
				Reporter.log(
						"PASS>>> Location Tracking Status Is Off After Applying GPS_ALWAYSOFF Job And It Can't Be Turned On",
						true);
			}
		} else {
			ALib.AssertFailMethod("FAIL>>> Loction Tracking Status Is On Eveen After Applying GPS_ALWAYSOFF Job");
		}
	}

	public void VeifyLocationStatusAfterApplyingSecurityPolicy_GPS_AlwaysONJob() {

		String LocationSwitchStatus = Initialization.driverAppium
				.findElementById("com.android.settings:id/switch_widget").getText();
		if (LocationSwitchStatus.equalsIgnoreCase("Location, On")) {
			Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").click();
			String LocationStatusAfterClickingLocationSwitch = Initialization.driverAppium
					.findElementById("com.android.settings:id/switch_widget").getText();
			if (!LocationStatusAfterClickingLocationSwitch.equalsIgnoreCase("Location, On")) {
				ALib.AssertFailMethod("FAIL>>> Loction Tracking Status Is OFF Even After Applying GPS_ALWAYSON Job");
			}

			else {
				Reporter.log(
						"PASS>>> Location Tracking Status Is ON After Applying GPS_ALWAYSON Job And It Can't Be Turned Off",
						true);
			}
		} else {
			ALib.AssertFailMethod("FAIL>>> Loction Tracking Status Is OFF Eveen After Applying GPS_ALWAYSON Job");
		}
	}

	public void VeifyLocationStatusAfterApplyingSecurityPolicy_GPS_DontCare() {
		String LocationSwitchStatus = Initialization.driverAppium
				.findElementById("com.android.settings:id/switch_widget").getText();
		if (LocationSwitchStatus.equalsIgnoreCase("Location, On")) {
			Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").click();
			String LocationStatusAfterClickingLocationSwitch = Initialization.driverAppium
					.findElementById("com.android.settings:id/switch_widget").getText();
			if (LocationStatusAfterClickingLocationSwitch.equalsIgnoreCase("Location, Off")) {
				Reporter.log("PASS>>> Location Tracking Status Is OFF After Clicking On Location Switch", true);

			}

			else {
				ALib.AssertFailMethod("FAIL>>> Loction Tracking Status Is ON Even After Clicking On Location Switch");
			}
		} else {
			ALib.AssertFailMethod("FAIL>>> Loction Tracking Status Is OFF Even After Applying GPS_ALWAYSON Job");
		}
	}

	public void VerifyCameraIsNotOpenedInDevice() throws MalformedURLException, InterruptedException {

		try {
			common.AppiumConfigurationCommonMethod("com.sec.android.app.camera", "com.sec.android.app.camera.Camera");
			Initialization.driverAppium.findElementById("SHOOTINGMODE_SINGLE").isDisplayed();
			ALib.AssertFailMethod("FAIL>>> Camera Can BE Opened Even After Applying Disable Camera Job");
		}

		catch (Exception e) {
			Reporter.log("PASS>>> Camera Is Not Opened After Applying Disable Camera Job", true);
		}
	}

	public void VerifyCameraIsOpenedInDevice() {
		try {
			common.AppiumConfigurationCommonMethod("com.sec.android.app.camera", "com.sec.android.app.camera.Camera");
			Initialization.driverAppium.findElementById("SHOOTINGMODE_SINGLE").isDisplayed();
			Reporter.log("PASS>>> Camera Is  Opened After Applying Enable Camera Job", true);
		}

		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Camera Can not Be Opened Even After Applying Enable Camera Job");
		}
	}

	public void VerifyMobileDataStatusAfterApplyingSecurityPolicy_MobileData_AlwaysOFF() {
		String MobileDataStatus = Initialization.driverAppium.findElementByXPath("//android.widget.Switch[@index='0']")
				.getText();
		String MobileDataSwitchStatus = Initialization.driverAppium
				.findElementByXPath("//android.widget.Switch[@index='0']").getAttribute("enabled");
		if (MobileDataStatus.equalsIgnoreCase("Off") && MobileDataSwitchStatus.equalsIgnoreCase("false")) {
			Initialization.driverAppium.findElementByXPath("//android.widget.Switch[@index='0']").click();
			String MobileDataStatusAfterClickingDataSwitch = Initialization.driverAppium
					.findElementByXPath("//android.widget.Switch[@index='0']").getText();
			if (MobileDataStatusAfterClickingDataSwitch.equalsIgnoreCase("on")) {
				ALib.AssertFailMethod("FAIL>>> Mobile Data Status Is On Even After Applying Data_ALWAYSOFF Job");
			}

			else {
				Reporter.log(
						"PASS>>> Mobile Data Status Is Off After Applying DAta_ALWAYSOFF Job And It Can't Be Turned On",
						true);
			}

		} else {
			ALib.AssertFailMethod("FAIL>>> Mobile Data Status Is On Even After Applying Data_ALWAYSOFF Job");
		}

	}

	public void VerifyMobileDataStatusAfterApplyingSecurityPolicy_MobileData_DontCare() {
		Initialization.driverAppium.findElementByXPath("//android.widget.Switch[@index='0']").click();
		String MobileDataStatus = Initialization.driverAppium.findElementByXPath("//android.widget.Switch[@index='0']")
				.getText();
		String MobileDataSwitchStatus = Initialization.driverAppium
				.findElementByXPath("//android.widget.Switch[@index='0']").getAttribute("enabled");
		if (MobileDataStatus.equalsIgnoreCase("on") && MobileDataSwitchStatus.equalsIgnoreCase("true")) {

			Reporter.log("PASS>>> Mobile Data Status Can Be Modified On Applying Job", true);
		}

		else {
			ALib.AssertFailMethod("FAIL>>> Mobile Data Status Cannot Be Modified Even After Applying Job");
		}
	}

	/// Compliance Jobs

	@FindBy(xpath = "(//input[@id='job_name'])[1]")
	private WebElement CompliancejobNameTextField;

	@FindBy(xpath = "(//p[contains(text(),'Create compliance')]/following-sibling::button[text()='Configure'])[2]")
	private WebElement CompliancejobConfigureButton;

	@FindBy(xpath = "(//div[@class='actionbutton ct-menuBtn fileName_actBtn'])[1]")
	private WebElement ComplianceJobAddPackageButton;

	@FindBy(xpath = "//input[@id='job_pkgname_input']")
	private WebElement ComplianceJobPackageTextField;

	@FindBy(xpath = "(//select[@id='ComplianceActionOption'])[2]")
	private WebElement OutOfComplianceActionsDropDownMenu;

	@FindBy(xpath = "(//select[@class='sc-act-delayUnit delayUnit_sel form-control ct-select-ele'])[2]")
	private WebElement OutOfComplianceDelayDropDown;

	@FindBy(xpath = "//button[@id='saveBtn']")
	private WebElement ComplianceJobSaveButton;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Device(" + Config.DeviceName
			+ ") is non compliant with Blacklisted Apps rule')]")
	private WebElement OutOfComplainceMessage;

	@FindBy(xpath = "//h4[@id='device_log_title']/preceding-sibling::button")
	private WebElement DeviceLogCloseButton;

	@FindBy(id = "blacklistedSection")
	private WebElement BlackListedButton;

	@FindBy(id = "blRefreshBtn")
	private WebElement BlackListPageRefreshButton;

	@FindBy(id = "blWhitelistBtn")
	private WebElement WhiteListButton;

	@FindBy(xpath = "(//div[@class='email_inputRow input-group ']/input)[1]")
	private WebElement ComplianceMailNotificationEmailIDField;

	// @FindBy(id = "comp_addBtn")
	// private WebElement ComplianceAddJobButton;

	@FindBy(xpath = ".//*[@id='compjob_JobGridContainer']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement ComplianceJobSearchField;

	@FindBy(xpath = "(//button[text()='Add Action'])[2]")
	private WebElement ComplianceJobAddActionButton;

	public void ClickOnComplianceJob() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='compliance_job']")).click();
		waitForXpathPresent("//button[@id='saveBtn']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='saveBtn']")
	private WebElement SaveButtonClick;

	public void ClickOnSaveButtonComplianceJob() throws InterruptedException {
		SaveButton.click();
		sleep(6);
	}

	public void SendingComplianceJobName(String ComplianceJobName) throws InterruptedException {
		CompliancejobNameTextField.clear();
		sleep(2);
		CompliancejobNameTextField.sendKeys(ComplianceJobName);
		sleep(2);
	}

	public void SelctingComplianceJobsRule(String RuleName) throws InterruptedException {
		Initialization.driver
				.findElement(By.xpath("//ul[@id='comp_rules_list']/li/a/span[2][text()='" + RuleName + "']")).click();
		waitForXpathPresent("//p[contains(text(),'Create compliance')]/following-sibling::button[text()='Configure']");
		sleep(2);
	}

	public void ClickonConfigureButton() throws InterruptedException {
		CompliancejobConfigureButton.click();
		sleep(2);
	}

	public void ClickOnaddButtonInsideComplianceJob() throws InterruptedException {
		ComplianceJobAddPackageButton.click();
		waitForXpathPresent("//button[@id='okbtn']");
		sleep(1);
	}

	public void SendingPackageNameInComplianceJob(String PackageName) throws InterruptedException {
		ComplianceJobPackageTextField.sendKeys(PackageName);
		sleep(2);
	}

	public void ClickOnComplianceJobPackageNameOKButton() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//button[@id='okbtn']")).click();
		waitForXpathPresent("(//div[@class='actionbutton ct-menuBtn fileName_actBtn'])[1]");
		sleep(5);
	}

	public void outOfComplianceActions(String complianceAction) throws InterruptedException {
		Select outOfCompliance = new Select(OutOfComplianceActionsDropDownMenu);
		outOfCompliance.selectByVisibleText(complianceAction);
		sleep(2);
	}

	// public void outOfComplianceActions_UninstallApp(String
	// indexforOutOfComplianceActions) throws InterruptedException {
	//
	// WebElement OutOfComplianceActionsDropDownMenu =
	// Initialization.driver.findElement(
	// By.xpath("(//select[@id='ComplianceActionOption'])[" +
	// indexforOutOfComplianceActions + "]"));
	// OutOfComplianceActionsDropDownMenu.click();
	// String GetAllDisplayedOptions = OutOfComplianceActionsDropDownMenu.getText();
	// System.out.println(GetAllDisplayedOptions);
	// try {
	// if (GetAllDisplayedOptions.contains("Uninstall App")) {
	// ALib.AssertFailMethod(" 'Uninstall App' is displayed ");
	//
	// }
	// } catch (Exception e) {
	// Reporter.log("PASS>>> Uninstall App is not displayed", true);
	// }
	// }

	public void Delay_OutOfComplianceActions(String Delay) throws InterruptedException {
		Select OutOfComplianceDelay = new Select(OutOfComplianceDelayDropDown);
		OutOfComplianceDelay.selectByVisibleText(Delay);
		sleep(2);
	}

	public void ClickOnComplianceJobSaveButton() throws InterruptedException {
		ComplianceJobSaveButton.click();
		waitForXpathPresent("//span[text()='Job created successfully.']");
		sleep(7);
	}

	public void ClickOnComplianceJobSaveButtonAfterModifyingJob() throws InterruptedException {
		ComplianceJobSaveButton.click();
		waitForXpathPresent("//span[text()='Job modified successfully.']");
		sleep(4);
	}

	public void VerifyTextMessageWhenDeviceIsOutOfCompliance() {
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, 40);

		try {
			wait.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains(@text,'Device("
							+ Config.DeviceName + ") is non compliant with Blacklisted Apps rule')]")));
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Device("
					+ Config.DeviceName + ") is non compliant with Blacklisted Apps rule')]").isDisplayed();
			Reporter.log("PASS>>> Out OF Compliance Message Is Displayed Successfully", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Out Of Complaince Message Is Not Displayed");
		}
	}

	public void ClickOnDeviceLogCloseBtn() throws InterruptedException {
		DeviceLogCloseButton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
		Reporter.log("PASS >> Clicked on Close Button of Device Log", true);
	}

	public void VerifyDeviceLogsAfterApplyingOutOfComplianceJob() throws InterruptedException {
		String OutOFComplainceLogs = Initialization.driver
				.findElement(By.xpath("//table[@id='devicelogtable']/tbody/tr[1]")).getText();
		if (OutOFComplainceLogs.contains("Device" + " " + Config.DeviceName + " uninstalled package")) {
			ClickOnDeviceLogCloseBtn();
			Reporter.log("PASS>>> Out Of Compliance Toast Message Is Displayed Successfully In Device Logs", true);
		}

		else {
			ClickOnDeviceLogCloseBtn();
			ALib.AssertFailMethod("FAIL>>> Out Of Compliance Toast Message Is Not Displayed In Device Logs");
		}

	}

	public void ClickOnBlackListedDeviceButtonInConsole() throws InterruptedException {
		BlackListedButton.click();
		waitForidPresent("blRefreshBtn");
		sleep(2);
	}

	public void ClickOnBlacklistPageRefreshButton() throws InterruptedException {
		BlackListPageRefreshButton.click();
		sleep(2);
	}

	public void VerifyDevicePresenceInsideBlackListesSection() throws InterruptedException {
		ClickOnBlackListedDeviceButtonInConsole();
		ClickOnBlacklistPageRefreshButton();
		try {
			Initialization.driver.findElement(By.xpath("//td[text()='" + Config.DeviceName + "']")).isDisplayed();
			Reporter.log("PASS>>> Device Is Present Inside BlackListed Section", true);
		}

		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Device Is Not Present Inside BlackListed Section");
		}
	}

	public void ClickOnWhiteListButton() throws InterruptedException {
		WhiteListButton.click();
		waitForXpathPresent("//p[contains(text(),'Are you sure you want to allowlist the device(s)?')]");
		sleep(1);
		Initialization.driver.findElement(By.xpath(
				"//p[contains(text(),'want to allowlist')]/parent::div/following-sibling::div/button[text()='Yes']"))
				.click();
		waitForXpathPresent("//span[text()='Device(s) allowlisted successfully.']");
	}

	public void WhiteListingDevice() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//td[text()='" + Config.DeviceName + "']")).click();
		sleep(2);
		ClickOnWhiteListButton();
	}

	public void VerifyIsDeviceLocked() {
		try {
			sleep(7);
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Settings']").isDisplayed();
			ALib.AssertFailMethod("FAIL>>> Device Is Not Locked As Per BlackList Application Action List");
		}

		catch (Exception e) {
			Reporter.log("PASS>>> Device Is Locked Successfully", true);
		}

	}

	public void SendingEmailID(String Email) throws InterruptedException {
		ComplianceMailNotificationEmailIDField.sendKeys(Email);
		sleep(2);
	}

	public void VerifyEmailAlertForAppBlackList() throws InterruptedException, AWTException {
		JavascriptExecutor jse = (JavascriptExecutor) Initialization.driver;
		jse.executeScript("window.open()");
		String Mainwindow = Initialization.driver.getWindowHandle();
		Initialization.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		Initialization.driver.get(Config.NotifiedUrl_Misc);
		sleep(5);
		Initialization.driver.findElement(By.xpath("//input[@id='addOverlay']")).sendKeys(Config.NotifiedEmail_Misc);
		sleep(3);

		Actions action = new Actions(Initialization.driver);
		action.sendKeys(Keys.ENTER).build().perform();

		waitForXpathPresent("//table[@class='table-striped jambo_table']/thead/tr/th[3]");
		sleep(5);

		// String MailinatorFromField =
		// Initialization.driver.findElement(By.xpath("//table[@class='table
		// table-striped jambo_table']/tbody/tr[1]/td[3]")).getText();
		String MailinatorFromField = Initialization.driver
				.findElement(By.xpath("(//a[contains(text(),'out of Compliance')] )[1]")).getText();
		String MailinatorReceivedTime = Initialization.driver
				.findElement(By.xpath("//table[@class='table-striped jambo_table']/tbody/tr[1]/td[5]")).getText(); // if(MailinatorFromField.contains("42g.200@gmail.com")&&MailinatorReceivedTime.contains("moments
		// ago")||MailinatorReceivedTime.contains("minute ago"))
		if (MailinatorFromField.contains("out of Compliance") && MailinatorReceivedTime.contains("moments ago")
				|| MailinatorReceivedTime.contains("minute ago"))

		{
			Reporter.log("PASS>>> Received Mail Notification Successfully", true);
			Initialization.driver.close();
			Initialization.driver.switchTo().window(Mainwindow);
		}

		else {
			Initialization.driver.close();
			Initialization.driver.switchTo().window(Mainwindow);
			ALib.AssertFailMethod("FAIL>>> No Mail Notification Is Received");
		}
	}

	public void ClickONAddJobButtonInCompositeJob() throws InterruptedException {
		CompositeAddJobBtn.click();
		waitForXpathPresent("//h4[text()='Select Jobs To Add']");
		sleep(4);
	}

	public void SelectingJobToAddCompositeJob(String JobName) throws InterruptedException {
		ClickONAddJobButtonInCompositeJob();
		SelectingJob(JobName);
		sleep(2);
	}

	public void CreatingInstallJobForOutOfCompliance(String JobName) throws InterruptedException, IOException {
		clickOnJobs();
		clickNewJob();
		clickOnAndroidOS();
		ClickOnInstallApplication();
		enterJobName(JobName);
		clickOnAddInstallApplication();
		browseFileInstallApplication("D:\\UplaodFiles\\Job On Android\\CompianceInstallJob\\File1.exe");
		clickOkBtnOnBrowseFileWindow();
		clickOkBtnOnAndroidJob();
		UploadcompleteStatus();
	}

	public void CreatingMultiInstallJob(String JobName) throws InterruptedException, IOException {
		clickOnJobs();
		clickNewJob();
		clickOnAndroidOS();
		ClickOnInstallApplication();
		enterJobName(JobName);
		clickOnAddInstallApplication();
		browseFileInstallApplication("D:\\UplaodFiles\\Job On Android\\CompianceInstallJob\\File1.exe");
		clickOkBtnOnBrowseFileWindow();
		clickOnAddInstallApplication();
		browseFileInstallApplication("D:\\UplaodFiles\\Job On Android\\InstallJobSingle\\File1.exe");
		clickOkBtnOnBrowseFileWindow();
		clickOkBtnOnAndroidJob();
		UploadcompleteStatus();
	}

	public void CreatingCompositeJob(String Job) throws InterruptedException {

		ClickOnCompositeJob();
		SelectingJobToAddOutOfComplianceAction(Job);

	}

	public void ClickONAddJobButtonInComplianceJob() throws InterruptedException {
		ComplianceAddJobButton.click();
		waitForXpathPresent("//h4[text()='Select Jobs To Add']");
		sleep(4);
	}

	public void SelectingJob(String JobName) throws InterruptedException {
		ComplianceJobSearchField.sendKeys(JobName);
		sleep(4);
		Initialization.driver
				.findElement(By.xpath("(//table[@id='CompJobDataGrid']/tbody/tr/td/p[text()='" + JobName + "'])[1]"))
				.click();
		sleep(3);
		Initialization.driver.findElement(By.xpath("//div[@id='selJob_addList_modal']//button[@id='okbtn']")).click();
		sleep(2);
	}

	public void SelectingJobToAddOutOfComplianceAction(String JobName) throws InterruptedException {
		ClickONAddJobButtonInComplianceJob();
		SelectingJob(JobName);
		sleep(2);
	}

	public void VerifyJobDeployedInDevice() throws InterruptedException, IOException {
		common.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		sleep(50);
		boolean AppIstallationStatus = Initialization.driverAppium.isAppInstalled("com.bhanu.torch");
		if (AppIstallationStatus == true) {
			Reporter.log("PASS>>> Job Deployed Successfully To The Device And Application Is Installed In The Device",
					true);
		} else {
			ALib.AssertFailMethod("FAIL>>> Job Has Not Been Deployed To The Device");
		}
	}

	public void SendingphoneNumber(String phoneNmuber) throws InterruptedException {
		ComplianceMailNotificationEmailIDField.sendKeys(phoneNmuber);
		sleep(2);
	}

	public void VerifySMSNotificationInDevice() {
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, 60);
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Close' and @index='0']")));
		String TextMessage = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'non compliant')]").getText();
		if (TextMessage.contains("Device(" + Config.DeviceName + ") is non compliant with Blacklisted Apps rule at")) {
			Reporter.log("PASS>>> Text Message Is Received Successully When Device Is Out Of Compliance", true);
		}

		else {
			ALib.AssertFailMethod("FAIL>>> Text Message Is Not Received Even When Device Is Out Of Compliance");
		}
	}

	public void VerifyBlackListAppUninstallation() throws IOException, InterruptedException {
		common.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		boolean ApplicationUninstallStatus = Initialization.driverAppium
				.isAppInstalled(Config.CompliancepackageName_Uninstall);
		if (ApplicationUninstallStatus == false) {
			Reporter.log("PASS>>> Application Uninstalled Successfully", true);
		}

		else {
			ALib.AssertFailMethod("FAIL>>> Application Is Not Uninstalled");
		}
	}

	public void VerifyNoActionSinceNoPackagenameIsEmpty() {
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Device("
					+ Config.DeviceName + ") is non compliant with Blacklisted Apps rule')]").isDisplayed();
			ALib.AssertFailMethod("Received out Of Compliance Message Even When PackageName Is Empty");
		}

		catch (Exception e) {
			Reporter.log("out Of Compliance Message Is Not Received As PackageName Is Empty", true);
		}
	}

	public void AddingActionInComplianceBlackListedApplication(String OutOfComplianceAction, String Delay)
			throws InterruptedException {
		ComplianceJobAddActionButton.click();
		sleep(2);
		Select sel = new Select(OutOfComplianceActionsDropDownMenu);
		sel.selectByVisibleText(OutOfComplianceAction);
		Select sel1 = new Select(OutOfComplianceDelayDropDown);
		sel1.selectByVisibleText(Delay);
	}

	public void VerifyUninstallationOfApplicaatioSetAsDeviceAdmin() {
		boolean isinstalled = Initialization.driverAppium.isAppInstalled("com.nix");
		if (isinstalled) {
			Reporter.log("PASS>>> Application Is Not Uninstalled As Device Admin Is Enabled", true);
		}

		else {
			ALib.AssertFailMethod("FAIL>>> Application Is Uninstalled Though Admin Is Enabled");
		}
	}

	public void DeletingCompositeJob() throws InterruptedException {
		Initialization.driver
				.findElement(By.xpath("//*[@id='subJobsTable']/tbody/tr/td[1]/p[text()='0InstallApplication']"))
				.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//*[@id='comp_dltbtn']/span[text()='Delete']")).click();
		sleep(2);

	}

	public void CheckingdeletedCompositeJob() {

		boolean var;
		try {
			Initialization.driver
					.findElement(By.xpath("//*[@id='subJobsTable']/tbody/tr/td[1]/p[text()='0InstallApplication']"))
					.isDisplayed();
			var = false;
		} catch (Exception e) {
			var = true;
		}

		String PassStatement = "PASS >> job is not available available";
		String FailStatement = "Job is available";
		ALib.AssertTrueMethod(var, PassStatement, FailStatement);

	}

	public void ClickOnMoveDownAndVerify() throws InterruptedException {

		Initialization.driver.findElement(By.xpath("//*[@id='subJobsTable']/tbody/tr[1]/td[1]")).click();
		Reporter.log("Apk selected", true);
		sleep(5);

		Initialization.driver.findElement(By.xpath("//*[@id='comp_movedown']/span[text()='Move Down']")).click();
		// Initialization.driver.findElement(By.xpath("//*[@id='comp_movedown']")).click();
		String attributeSeleted = Initialization.driver.findElement(By.xpath("//*[@id='subJobsTable']/tbody/tr[2]"))
				.getAttribute("class");
		if (attributeSeleted.equalsIgnoreCase("selected")) {
			Reporter.log("Move Down is working fine", true);
		} else {
			ALib.AssertFailMethod("Move Down is not working fine");
		}
	}

	public void CompositeJobName(String jobname) throws InterruptedException {
		CompositeJobName.clear();
		CompositeJobName.sendKeys(jobname);
		sleep(2);
		waitForidPresent("ok_btn");

	}

	public void ClickOnMoveUpAndVerify() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id='comp_moveup']/span[text()='Move Up']")).click();
		sleep(3);
		String attributeSeleted = Initialization.driver.findElement(By.xpath("//*[@id='subJobsTable']/tbody/tr[1]"))
				.getAttribute("class");
		if (attributeSeleted.equalsIgnoreCase("selected")) {
			Reporter.log("Move up is working fine", true);
		} else {
			ALib.AssertFailMethod("Move up is not working fine");
		}
	}

	public void ClickOnOnOkButtonAndroidJob() throws InterruptedException {
		okBtnOnAndroidJobWindow.click();
		sleep(4);
	}

	public void ClickOnOkButtonModifyAndroidJob() throws InterruptedException {
		OkButtonModify.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='install_moveUp']/span")
	private WebElement moveUp;

	@FindBy(xpath = ".//*[@id='install_moveDown']/span")
	private WebElement moveDown;

	@FindBy(xpath = ".//*[@id='ok_btn']")
	private WebElement okBtnOnAndroidJobWindow;

	@FindBy(id = "ok_btn_modified")
	private WebElement OkButtonModify;

	@FindBy(xpath = "//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
	private WebElement SearchJobInJobList;

	@FindBy(xpath = "//*[@id=\"backToPanel2\"]")
	private WebElement BackButton;

	public void ClickOnBackButtonJobWindow() throws InterruptedException {
		BackButton.click();
		sleep(4);
	}

	@FindBy(xpath = "//span[contains(text(),'Please enter a name for the job.')]")
	private WebElement JobNameEmptyMessage;

	public void JobNameCannotBeEmptyMessage() throws InterruptedException {

		boolean isdisplayed = true;

		try {
			JobNameEmptyMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}

		Assert.assertTrue(isdisplayed, "FAIL >> 'Please enter a name for the job.'is not displayed");
		Reporter.log("PASS >> 'Please enter a name for the job.' is being displayed", true);
		// waitForPageToLoad();
		JobNameTextFieldTextMessage.sendKeys("demo");
		sleep(2);
	}

	@FindBy(xpath = "//span[contains(text(),'Message cannot be empty.')]")
	private WebElement MessageCannotBeEmpty;

	public void VerifyMessageBodyEmpty() throws InterruptedException {

		boolean isdisplayed = true;

		try {
			MessageCannotBeEmpty.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}

		Assert.assertTrue(isdisplayed, "FAIL >> 'message body empty'is not displayed");
		Reporter.log("PASS >> 'Message body empty is displayed' is being displayed", true);
		JobNameTextFieldTextMessage.clear();
	}

	public void verifyPopupisClosed(String TextJobName) throws InterruptedException {
		sleep(60);
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + TextJobName + "']")
					.isDisplayed();
			ALib.AssertFailMethod("Popup is not closed and Job is failed");
		} catch (Exception e) {
			Reporter.log("Close Pop-Up job is working fine");
		}
	}

	@FindBy(id = "mailSection")
	private WebElement inbox;

	public void SearchJobInList(String JobName) throws InterruptedException {
		SearchJobInJobList.clear();
		sleep(2);
		SearchJobInJobList.sendKeys(JobName);
		waitForidPresent("job_new_job");
		sleep(5);
		Initialization.driver.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr/td[1]")).click();
		sleep(2);
	}

	public void ClickOnInbox() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, inbox);

		inbox.click();
		waitForidPresent("mailDeleteButton");
		sleep(2);
	}

	public void verifyReadNotificationInConsole(String JobName) throws InterruptedException {
		sleep(10);
		waitForVisibilityOf("//*[@id='MailDetail']/div/div[1]/div[2]/div[1]/span[2]");
		Assert.assertTrue(
				Initialization.driver.findElement(By.xpath("//*[@id='MailDetail']/div/div[1]/div[2]/div[1]/span[2]"))
						.getText().contains(JobName));
	}

	public void ReadNotification(String txtJobName) throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell service call statusbar 1");
		sleep(3);
		boolean MessageInMaliBox = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'Unread Messages')]").isDisplayed();
		String pass = "PASS >> Force read is displayed";
		String fail = "FAIL >> Force read is not displayed";
		ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Unread Messages')]")
				.click();
		sleep(5);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Subject: " + txtJobName + "']")
				.click();
		sleep(1);
		boolean notifyText = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='TextJobReadNotification']").isDisplayed();
		String pass1 = "PASS >> Force read is displayed";
		String fail1 = "FAIL >> Force read is not displayed";
		ALib.AssertTrueMethod(notifyText, pass1, fail1);
		Runtime.getRuntime().exec("adb shell service call statusbar 2");
	}

	@FindBy(xpath = "//span[contains(text(),'The job named')]") // MR72
	private WebElement InitiateJob;

	public void JobInitiatedOnDevice() throws InterruptedException {

		if (InitiateJob.isDisplayed()) {
			Reporter.log(" Job is initiated message is displayed", true);
		} else {
			ALib.AssertFailMethod("Install job is initiated message is not displayed");
		}
		sleep(3);

	}

	@FindBy(xpath = "//div[@id='nix_upgrade']")
	private WebElement nixUpgrade;

	public void clickOnNixUpgrade() throws InterruptedException {
		nixUpgrade.click();
	}

	@FindBy(id = "nix_agent_link")
	private WebElement nixAgentLink;

	@FindBy(id = "agent_download_path")
	private WebElement agentDownloadPath;

	@FindBy(xpath = "//input[@id='jobName']")
	private WebElement jobNameNixUpgrade;

	@FindBy(xpath = "//input[@id='job_install_name_input']")
	private WebElement enterJobName;

	@FindBy(xpath = "//*[@id='UpgradeNix_header']/h4")
	private WebElement upgradeNixHeader;

	@FindBy(id = "nixupgradejobokbtn")
	private WebElement nixupgradejobokbtn;

	public void VerifyNixUpgrade() throws InterruptedException {
		waitForXpathPresent("//*[@id='UpgradeNix_header']/h4");
		sleep(2);
		if ((upgradeNixHeader.getText()).equals("Nix Upgrade")) {
			Reporter.log("Nix Upgrade tittle is present", true);
		} else {
			ALib.AssertFailMethod("Nix Upgrade tittle is not present");
		}

		if (jobNameNixUpgrade.isDisplayed()) {
			Reporter.log("Nix Upgrade Job is present", true);
		} else {
			ALib.AssertFailMethod("Nix Upgrade Job is not present");
		}

		if (agentDownloadPath.isDisplayed()) {
			Reporter.log("Device Path is present", true);
		} else {
			ALib.AssertFailMethod("Device Path is not present");
		}

		if (nixAgentLink.isDisplayed()) {
			Reporter.log("Nix Agent Link is present", true);
		} else {
			ALib.AssertFailMethod("Nix Agent Link is not present");
		}
	}

	public void enterJobName1(String NixUpgradeJob) {
		jobNameNixUpgrade.sendKeys(NixUpgradeJob);
	}

	@FindBy(xpath = "//span[text()='Please enter a name for the job.']")
	private WebElement JobNameCantbeEmpltyMessage;

	public void EmptyJobNameMessage() throws InterruptedException {
		sleep(1);
		if (JobNameCantbeEmpltyMessage.isDisplayed()) {
			Reporter.log("'Please enter a name for the job.' is displayed", true);
		} else {
			ALib.AssertFailMethod("Error message is not displayed");
		}
	}

	public void verifyElementsOnCreateJobPage() throws InterruptedException {
		String[] ElementsOnCreateJobPage = { "Install Application", "File Transfer", "Nix Upgrade", "Text Message",
				"Alert Message", "Run Script", "Remote Data Wipe", "Lock Device", "Nix Agent Settings", "Composite Job",
				"Notification Policy", "SureLock Settings", "SureFox Settings", "SureVideo Settings",
				"Location Tracking", "Security Policy", "Application Settings", "Wi-Fi / Hotspot Settings",
				"Email Configuration Settings", "Telecom Management Policy", "Call Log Tracking", "SMS Log Tracking",
				"Geo Fence", "Time Fence", "Network Fence", "Remote Buzz", "Compliance Job",
				"Relay Server Configuration", "Device Info Configuration" };
		for (int j = 1; j <= ElementsOnCreateJobPage.length; j++) {
			String CreateJobPageElement = Initialization.driver
					.findElement(By.xpath("//div[@id='opHolder']/div[" + j + "]")).getText();
			if (CreateJobPageElement.equals(ElementsOnCreateJobPage[j - 1])) {
				Reporter.log(CreateJobPageElement + " is  present in the Select OS page", true);
			} else {
				ALib.AssertFailMethod(CreateJobPageElement + " is not present in Select OS page");
			}
		}
	}

	@FindBy(id = "nps_surevey_modal")
	private WebElement recommend42Gears;

	@FindBy(xpath = "//*[@id='nps_surevey_modal']/button/span")
	private WebElement recommend42GearsClose;

	public void clickOkBtnOnIstallJob() throws InterruptedException {
		if (recommend42Gears.isDisplayed()) {
			recommend42GearsClose.clear();
		}
		okBtnOnInstallJobFileUpload.click();
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='okbtn']")
	private WebElement okBtnOnInstallJobFileUpload;

	@FindBy(xpath = "//button[text()='Done'][@class='btn smdm_btns smdm_grn_clr okbutton addbutton ct-model-input sc-advsett-done']")
	private WebElement DoneAdvancedSettings;

	@FindBy(xpath = "//span[text()='Advanced Settings applied successfully.']")
	private WebElement AdvancedSettingsMsg;

	@FindBy(xpath = "//span[text()='Advanced Settings']")
	private WebElement AdvancedSettingsbtn;

	public void verifyAdvancedSettingsMsg() throws InterruptedException {
		boolean isdisplayed = true;

		try {
			AdvancedSettingsMsg.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		String pass = "PASS >> Advanced Settings applied successfully. Message displayed ";
		String fail = "FAIL >>  Advanced Settings applied successfully. Message did not displayed";
		ALib.AssertTrueMethod(isdisplayed, pass, fail);
		sleep(15);
	}

	public void clickOnAdvancedSettingBtn() throws InterruptedException {
		AdvancedSettingsbtn.click();
		sleep(4);
	}

	public void clickOnDoneAdvancedSettingsBtn() {

		DoneAdvancedSettings.click();
	}

	public void selectAPKOnConfigurationJob() throws InterruptedException {
		FirstAPKEntered.click();
		Reporter.log("Apk selected", true);
		sleep(5);
	}

	public void verifyMoveDown() throws InterruptedException {
		sleep(2);
		moveDown.click();
		String attributeSeleted = Initialization.driver
				.findElement(By.xpath("//*[@id='installConfigTable']/tbody/tr[2]")).getAttribute("class");
		if (attributeSeleted.equalsIgnoreCase("selected")) {
			Reporter.log("Move Down is working fine", true);
		} else {
			ALib.AssertFailMethod("Move Down is not working fine");
		}
	}

	@FindBy(xpath = "//*[@id='job_install_min_version_logic']")
	private WebElement androidVersionlogic;
	@FindBy(xpath = "//*[@id='appCompVersion']")
	private WebElement appCompVersion;

	public void selectAndroidlogic(String logic) {
		Select androidlogic = new Select(androidVersionlogic);
		androidlogic.selectByVisibleText(logic);
	}

	@FindBy(xpath = "//*[@id='job_installCopy']")
	private WebElement installCopy;

	public void unCheckInstallCopy() {
		installCopy.click();
	}

	public void CheckApplicationIsNotInstalledDeviceSide(String BundleID) {
		boolean checkappInstalled = Initialization.driverAppium.isAppInstalled(BundleID);
		if (!checkappInstalled) {
			Reporter.log("Pass >>Application is not installed");
		} else {
			ALib.AssertFailMethod("Application is installed");
		}
	}

	public void browseAPK(String FileAPK) throws IOException, InterruptedException {
		fileBrowseBtn.click();
		sleep(8);
		Runtime.getRuntime().exec(FileAPK);
		Reporter.log("Uploaded File", true);
		sleep(10);
	}

	public void browseMultipleAPK() throws IOException, InterruptedException {
		fileBrowseBtn.click();
		sleep(8);
		Runtime.getRuntime().exec("D:\\apk\\CreateDeployMultipleAPK");
		Reporter.log("Uploaded Multiple Files", true);
		sleep(10);
	}

	public void unInstallApplication(String BundleID) throws InterruptedException {
		sleep(3);
		Initialization.driverAppium.removeApp(BundleID);
		sleep(3);
	}

	public void CheckApplicationsInstalledDeviceSide(String BundleID) throws InterruptedException {
		sleep(20);
		String[] bundle = BundleID.split(",");
		for (String packageName : bundle) {
			boolean checkappInstalled = Initialization.driverAppium.isAppInstalled(packageName);
			String pass2 = "PASS >> Application is installed";
			String fail2 = "FAIL >> Application is Not installed";
			ALib.AssertTrueMethod(checkappInstalled, pass2, fail2);
			sleep(3);
		}
	}

	public void CheckApplicationsUnInstalledDeviceSide(String BundleID) throws InterruptedException {
		sleep(20);
		String[] bundle = BundleID.split(",");
		for (String packageName : bundle) {
			boolean checkappInstalled = Initialization.driverAppium.isAppInstalled(packageName);
			String pass2 = "PASS >> Application is installed";
			String fail2 = "FAIL >> Application is Not installed";
			ALib.AssertFalseMethod(checkappInstalled, pass2, fail2);
			sleep(3);
		}
	}

	@FindBy(xpath = "//*[@id='eam_apps_select']/..")
	private WebElement UseAppsFromAppStoreCheckBox;

	public void clickOnUseAppsFromAppStoreCheckBox() {
		UseAppsFromAppStoreCheckBox.click();
	}

	@FindBy(xpath = "//*[@id='eam_apps_list']")
	private WebElement appsDropDown;

	public void SelectAppFromAppsDropDown(String ApplicationName) {
		Select appDrownDown = new Select(appsDropDown);
		try {
			appDrownDown.selectByVisibleText(ApplicationName);
		} catch (Exception e) {
			appDrownDown.selectByVisibleText("Fake GPS (com.lexa.fakegps)");
		}
	}

	@FindBy(xpath = "//*[@id='job_install_min_version_num']")
	private WebElement androidVersionNum;

	public void selectAndroidNum(String AndroidNum) {
		Select androidNum = new Select(androidVersionNum);
		androidNum.selectByVisibleText(AndroidNum);
	}

	@FindBy(xpath = "//*[@id='appName_List']")
	private WebElement appNameList;

	public void selectappName(String appName) {
		Select appNameListDropDown = new Select(appNameList);
		appNameListDropDown.selectByVisibleText(appName);
	}

	@FindBy(xpath = "//*[@id='appCompOperator']")
	private WebElement appCompOperatoreList;

	public void selectappCompOperator(String appCompOperator) {
		Select appCompOperatoreListDropDown = new Select(appCompOperatoreList);
		appCompOperatoreListDropDown.selectByVisibleText(appCompOperator);
	}

	public void selectappVersion(String appCompVersionValue) {
		appCompVersion.sendKeys(appCompVersionValue);
	}

	int NumbofInCompleteJobs;

	public void GettingInitialCountOfInCompleteJobsInJobQueue(String TabName, int TabNumber, int MaximumTime)
			throws InterruptedException {
		String NumberofInCompleteJobsBeforeApplyingJob = Initialization.driver
				.findElement(By.xpath("//div[@id='jobQ-info-container']/div[" + TabNumber + "]/div[2]")).getText();
		NumbofInCompleteJobs = Integer.parseInt(NumberofInCompleteJobsBeforeApplyingJob);
		Reporter.log("Number Of Incomplete jobs Present In" + " " + TabName + " " + "before applying the job" + "is"
				+ "  " + NumbofInCompleteJobs, true);
		if (NumbofInCompleteJobs > 0) {
			String Status = IncompleteJobStatus.getText();
			System.out.println("Incomplete Job Status: " + Status);
			if (Status.equalsIgnoreCase("In Progress") || Status.equalsIgnoreCase("Pending")) {
				try {
					System.out.println("Waiting for the InComplete Job to be deployed before applying any other job");
					WebDriverWait wait1 = new WebDriverWait(Initialization.driver, MaximumTime);
					wait1.until(
							ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='No Pending jobs']")))
							.isDisplayed();
					System.out.println("InComplete Job gone from the queue");
				} catch (Exception e) {
					ALib.AssertFailMethod("In Complete Job stuck for given 1 hr time");
				}
				ClickOnJobQueueCloseButton();
			}
		} else {
			ClickOnJobQueueCloseButton();
		}
	}

	public void clickOnInstallBtnInDevice() throws InterruptedException {
		try {
			Initialization.driverAppium.findElementById("android:id/button1").isDisplayed();
			sleep(2);
			Initialization.driverAppium.findElementById("android:id/button1").click();
			Reporter.log("Clicked on install button in device", true);
			sleep(5);
		} catch (Exception e) {
			Reporter.log("Install button is not displayed", true);
			sleep(2);
		}
	}

	public void CheckApplicationIsInstalledDeviceSide(String BundleID) throws InterruptedException {
		sleep(10);
		boolean checkappInstalled = Initialization.driverAppium.isAppInstalled(BundleID);
		String pass2 = "PASS >> Application is installed";
		String fail2 = "FAIL >> Application is Not installed";
		ALib.AssertTrueMethod(checkappInstalled, pass2, fail2);
		sleep(3);
	}

	public void unInstallMultipleApplication(String BundleID) throws InterruptedException {
		String[] bundle = BundleID.split(",");
		for (String packageName : bundle) {
			sleep(3);
			Initialization.driverAppium.removeApp(packageName);
			sleep(3);
		}
	}

	int NumbofFailedJobs;

	public void GettingInitialCountOfFailedJobsInJobQueue(String TabName, int TabNumber) throws InterruptedException {
		String NumberofJobsBeforeApplyingJob = Initialization.driver
				.findElement(By.xpath("//div[@id='jobQ-info-container']/div[" + TabNumber + "]/div[2]")).getText();
		NumbofFailedJobs = Integer.parseInt(NumberofJobsBeforeApplyingJob);
		Reporter.log("Number Of jobs Present In" + " " + TabName + " " + "before applying the job" + "is" + "  "
				+ NumbofFailedJobs, true);
		ClickOnJobQueueCloseButton();
	}

	public void verifyMoveUp() throws InterruptedException {
		sleep(2);
		moveUp.click();
		String attributeSeleted = Initialization.driver
				.findElement(By.xpath("//*[@id='installConfigTable']/tbody/tr[1]")).getAttribute("class");
		if (attributeSeleted.equalsIgnoreCase("selected")) {
			Reporter.log("Move up is working fine", true);
		} else {
			ALib.AssertFailMethod("Move up is not working fine");
		}
	}

	public void verifyModifyOption() throws InterruptedException {
		sleep(3);
		modifyJob1.click();
		if (filePathURL.isDisplayed()) {
			Reporter.log("File Path URL is present in the  Install Job page", true);
		} else {
			ALib.AssertFailMethod("File Path URL is not present in the  Install Job page");
		}
		ClickOnOkButton();
	}

	@FindBy(xpath = "//table[@id='installConfigTable']/tbody/tr")
	private List<WebElement> numberofAPKEntered;

	@FindBy(xpath = ".//*[@id='install_deleteBtn']")
	private WebElement deleteBtn;

	@FindBy(xpath = "//*[@id='installConfigTable']/tbody/tr[1]/td[1]")
	private WebElement FirstAPKEntered;

	public void verifyDeleteInInstallJob() throws InterruptedException {
		sleep(2);
		int APKsEntered = numberofAPKEntered.size();
		FirstAPKEntered.click();
		deleteBtn.click();
		int numberofAPKAfterDelete = numberofAPKEntered.size();
		if (APKsEntered > numberofAPKAfterDelete) {
			Reporter.log("Delete is working fine", true);
		} else {
			ALib.AssertFailMethod("Delete is not working fine");
		}
	}

	public void verifyElementsOnJobsPage() throws InterruptedException {
		String[] ElementsOnJobPage = { "New Job", "New Folder", "Move to Folder", "Copy", "Delete", "Modify" };
		for (int j = 1; j <= ElementsOnJobPage.length; j++) {
			String JobPageElement = Initialization.driver
					.findElement(By.xpath("//div[@class='ct-collapseMenu-cover']/div/div[" + j + "]/span")).getText();
			if (JobPageElement.contains((ElementsOnJobPage[j - 1]))) {
				Reporter.log(JobPageElement + " is  present in the job page", true);
			} else {
				ALib.AssertFailMethod(JobPageElement + " is not present in Job page");
			}
		}
	}

	public String NixversionValue = "";
	public String NixversionAfterUpgradeValue = "";
	@FindBy(xpath = "//span[text()='Job modified successfully.']")
	private WebElement JobModifiedSuccessfullyMessage;

	public void JobmodifiedMessage() throws InterruptedException {
		if (JobModifiedSuccessfullyMessage.isDisplayed()) {
			Reporter.log("Job created successfully message is displayed", true);
		} else {
			ALib.AssertFailMethod("Job created successfully message is not displayed");
		}

	}

	public void getNIXVersion() throws InterruptedException {
		sleep(10);
		String Nixversion = Initialization.driverAppium.findElementById("com.nix:id/nixAgentVersion").getText();
		NixversionValue = (String) Nixversion.subSequence(9, 13);
		System.out.println(NixversionValue);
	}

	@FindBy(xpath = "//div[@id='dis-card']//div[text()='Agent Version']/following-sibling::div[1]/p")
	private WebElement NixVersion;

	public void ReadingNixVersionFromDevice() {
		String[] Nixver = NixVersion.getText().split("\\.");
		if (Nixver.length == 3) {
			Reporter.log("PASS>>> Nix Version Is Displayed In X.Y.Z Fromat", true);
		} else {
			ALib.AssertFailMethod("FAIL>>> Nix Version Is Not Displayed In X.Y.Z Format");
		}
	}

	@FindBy(xpath = "//td[@class='AgentVersion']/p")
	private WebElement NixVersionCl;

	public void VerifyOfNixVFromColumn() {
		String[] Nixver = NixVersionCl.getText().split("\\.");
		if (Nixver.length == 3) {
			Reporter.log("PASS>>> Nix Version Is Displayed In X.Y.Z Fromat in Agent Version Col", true);
		} else {
			ALib.AssertFailMethod("FAIL>>> Nix Version Is Not Displayed In X.Y.Z Format in Agent Version Col");
		}

	}

	@FindBy(xpath = "//td[@class='SureLockVersion']/p")
	private WebElement SureLockCl;

	public void VerifyOfSLFromtColumn() {
		String[] SLver = SureLockCl.getText().split("\\.");
		if (SLver.length == 3) {
			Reporter.log("PASS>>> SL Version Is Displayed In X.Y.Z Fromat in SL Version Col", true);
		} else {
			ALib.AssertFailMethod("FAIL>>> SL Version Is Not Displayed In X.Y.Z Format in SL Version Col");
		}

	}

	String AgentVersion = null;

	public void GetNixVersionFromAngentVersionCol() {
		AgentVersion = Initialization.driver
				.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr//td[@class='AgentVersion']/p")).getText();
		System.out.println(AgentVersion);
	}

	public void getNIXVersionAfterUpgrade() throws InterruptedException {
		sleep(3);
		String NixversionAfterUpgrade = Initialization.driverAppium.findElementById("com.nix:id/nixAgentVersion")
				.getText();
		NixversionAfterUpgradeValue = (String) NixversionAfterUpgrade.subSequence(9, 13);
	}

	public void compareNIXversion() {
		double nixversion = Double.parseDouble(NixversionValue);
		double nixversionAfterUpgrade = Double.parseDouble(NixversionAfterUpgradeValue);

		if (nixversion < nixversionAfterUpgrade) {
			Reporter.log("Nix is upgraded", true);
		} else {
			ALib.AssertFailMethod("Nix is not upgraded");
		}
	}

	public void CheckJobIsCreatedInsideFolder() throws InterruptedException {
		boolean jobIsInsideFolder = FolderCreatedBackButton.isEnabled();
		String pass = "PASS >> Job Is displayed InsideFolder";
		String fail = "FAIL >> Job Is not displayed InsideFolder";
		ALib.AssertTrueMethod(jobIsInsideFolder, pass, fail);
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='jobName']")
	private WebElement JobNameNixUpgrade;

	public void enterJobNameNixUpgrade(String name) throws InterruptedException {
		waitForXpathPresent("//*[@id='jobName']");
		sleep(1);
		JobNameNixUpgrade.clear();
		JobNameNixUpgrade.sendKeys(name);
		Reporter.log("Entered Job Name", true);
		sleep(2);
	}

	public void enterNixAgentLink(String nixLate) {
		nixAgentLink.clear();
		nixAgentLink.sendKeys(nixLate);
	}

	public void enterNixDevicePath() {
		agentDownloadPath.clear();
		agentDownloadPath.sendKeys("/sdcard/");
	}

	public void nixUpgradeOkButton() throws InterruptedException {
		nixupgradejobokbtn.click();
		sleep(1);
	}

	@FindBy(xpath = "//*[@id='alert_message']")
	private WebElement alert_message;

	public void Verifyalert_messageJobPresent() throws InterruptedException {

		boolean alert_messageJob = alert_message.isDisplayed();
		String pass = "PASS >> alert_message Job is present ";
		String fail = "FAIL >> alert_message Job is not present ";
		ALib.AssertTrueMethod(alert_messageJob, pass, fail);
		sleep(2);

	}

	public void ClickOnalert_messageJob() throws InterruptedException {

		alert_message.click();
		waitForXpathPresent("//h4[text()='Select Options']");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[2]/div[1]/div[1]/span[@class='img_option']")
	private WebElement img_option;

	public void CreateNewAlertimg_option() throws InterruptedException {

		img_option.click();
		waitForXpathPresent("//h4[text()='Create Alert Message']");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='job_alert_name_input']")
	private WebElement job_alert_name_input;

	@FindBy(xpath = "//*[@id='alertTitle']")
	private WebElement alertTitle;

	@FindBy(xpath = "//*[@id='alertTitleColor']")
	private WebElement alertTitleColor;

	@FindBy(xpath = "//*[@id='pickedClrInput']")
	private WebElement pickedClrInput;

	@FindBy(xpath = "//*[@id='colorPicker_modal']/div/div/div[3]/button[text()='OK']")
	private WebElement OKButton;

	@FindBy(xpath = "//*[@id='alertDescription']")
	private WebElement alertDescription;

	@FindBy(xpath = "//*[@id='alertDescriptionColor']")
	private WebElement alertDescriptionColor;

	@FindBy(xpath = "//*[@id='alertIconSelection']")
	private WebElement alertIconSelection;

	@FindBy(xpath = "//*[@id='preDefinediconBtn']")
	private WebElement preDefinediconBtn;

	public void InputJobNameAlertMessage(String AlertMessageJobName) throws InterruptedException {

		job_alert_name_input.sendKeys(AlertMessageJobName);
		sleep(2);

	}

	public void ClearInputJobNameAlertMessage() throws InterruptedException {

		job_alert_name_input.clear();
		sleep(2);

	}

	public void alertTitle_Input(String alertTitleNAME) throws InterruptedException {

		alertTitle.sendKeys(alertTitleNAME);
		sleep(2);

	}

	public void alertTitleColorClick() throws InterruptedException {
		alertTitleColor.click();
		sleep(2);

	}

	public void WaitForSelectColorPopUp() throws InterruptedException {

		waitForXpathPresent("//h4[text()='Select Color']");
		sleep(2);

	}

	public void InputColorAlertTitle(String ColorAlertTitle) throws InterruptedException {
		pickedClrInput.clear();
		sleep(2);
		pickedClrInput.sendKeys(ColorAlertTitle);
		sleep(2);
	}

	public void ClickOnoKbtnAlertTitleColor() throws InterruptedException {

		OKButton.click();
		sleep(2);

	}

	public void InputAlertDescription(String AlertDescriptionInput) throws InterruptedException {

		alertDescription.sendKeys(AlertDescriptionInput);
		sleep(2);
	}

	public void alertDescriptionColorClick() throws InterruptedException {

		alertDescriptionColor.click();

	}

	@FindBy(xpath = "//*[@id='alertIconSelection']/option[text()='Choose from Predefined icons']")
	private WebElement Predefined_icons_Select;

	public void alertIconSelectionInput() throws InterruptedException {

		alertIconSelection.click();
		sleep(2);

	}

	public void selectDropDownPredefined_icons() throws InterruptedException {

		Predefined_icons_Select.click();
		sleep(2);

	}

	public void ClickOnUploadIcon() throws InterruptedException {

		preDefinediconBtn.click();
		waitForXpathPresent("//h4[text()='Select an Icon']");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='fontAwesomeIcons_modal']/div/div/div[2]/ul/li/div/i[@class='fa fa-address-book-o']")
	private WebElement address_book;

	@FindBy(xpath = "//*[@id='fontAwesomeIcons_modal']/div/div/div[3]/button[text()='OK']")
	private WebElement OKBtnPredefinedIcon;

	public void SelectPredefinedIcon() throws InterruptedException {

		address_book.click();
		sleep(2);
		OKBtnPredefinedIcon.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='alertBackgroundColor']")
	private WebElement alertBackgroundColor;

	public void ClickOnalertBackgroundColor() throws InterruptedException {

		alertBackgroundColor.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='Alert_Display_type']")
	private WebElement Alert_Display_type;

	public void Alert_Display_typeClick() throws InterruptedException {

		Alert_Display_type.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='Alert_Display_type']/option[text()='Display Alert']")
	private WebElement Alert_Display_typeDisplayAlert;

	public void SelectDropDownAlert_Display_type() throws InterruptedException {
		Alert_Display_typeDisplayAlert.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='Alert_Display_type']/option[text()='Display Popup']")
	private WebElement Alert_Display_typeDisplayPopUp;

	public void SelectDropDownAlert_Display_type2() throws InterruptedException {
		Alert_Display_typeDisplayPopUp.click();
		waitForXpathPresent("//span[text()='Enable Snooze']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='AlertBuzzEnable']")
	private WebElement AlertBuzzEnable;

	@FindBy(xpath = "//*[@id='AlertBuzzEnableInput']")
	private WebElement AlertBuzzEnableInput;

	@FindBy(xpath = "//*[@id='CloseAlertPopupEnable']")
	private WebElement CloseAlertPopupEnable;

	@FindBy(xpath = "//*[@id='CloseAlertPopupEnableInput']")
	private WebElement CloseAlertPopupEnableInput;

	public void EnableAlertBuzz() throws InterruptedException {

		AlertBuzzEnable.click();
		sleep(2);

	}

	public void InputAlertBuzzTime(String TimeSecs) throws InterruptedException {

		AlertBuzzEnableInput.sendKeys(TimeSecs);
		sleep(2);
	}

	public void EnableCloseAlertPopupEnable() throws InterruptedException {

		CloseAlertPopupEnable.click();
		sleep(2);

	}

	public void InputCloseAlertPopup(String CloseTime) throws InterruptedException {

		CloseAlertPopupEnableInput.sendKeys(CloseTime);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='SaveAlert']")
	private WebElement SaveAlert;

	public void ClickOnSaveOkButton() throws InterruptedException {
		SaveAlert.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='jobDataGrid']/tbody/tr/td/p[text()='AlertMessageJobType']")
	private WebElement AlertMessageJobType;

	public void SelectAlertMessageJob() throws InterruptedException {

		AlertMessageJobType.click();
		sleep(2);

	}

	public void SelectAlertMessageJobModify(String SelectModifyJob) throws InterruptedException {

		Initialization.driver
				.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr/td/p[text()='" + SelectModifyJob + "']"))
				.click();
		sleep(2);

	}

	public void WaitFor_CreateAlertMessagePopUp() throws InterruptedException {

		waitForXpathPresent("//h4[text()='Create Alert Message']");
		sleep(2);

	}

	public void WaitFor_AlertMessagePopUp(String WaitForXpath) throws InterruptedException {

		waitForXpathPresent("//h4[text()='" + WaitForXpath + "']");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='previewBtn']")
	private WebElement previewBtn_AlertMessageJob_Click;

	@FindBy(xpath = "//*[@id='previewBlkModal']/div/div/button")
	private WebElement previewBlkModalCloseBtn;

	public void ScrollToPreviewButton() {

		JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
		je.executeScript("arguments[0].scrollIntoView()", previewBtn_AlertMessageJob_Click);
	}

	public void ClickOnPreviewButton() throws InterruptedException {

		previewBtn_AlertMessageJob_Click.click();
		waitForXpathPresent("//*[@id='AlertTextBlk']");
		sleep(2);

	}

	public void previewBlkModalCloseBtn_Click() throws InterruptedException {

		previewBlkModalCloseBtn.click();
		sleep(2);
	}

	@FindBy(xpath = "//span[text()='Please provide a title for the alert.']")
	private WebElement ErrorMessageTitleEmpty;

	public void VerifySaveWithoutFillingTheData() throws InterruptedException {

		String ErrorMessageNotitle = ErrorMessageTitleEmpty.getText();
		String EXpectedError = "Please provide a title for the alert.";
		String Pass = "PASS>>'Please provide a title for the alert.' is Displayed";
		String Fail = "FAIL>>'Please provide a title for the alert.' is NOT Displayed";
		ALib.AssertEqualsMethod(EXpectedError, ErrorMessageNotitle, Pass, Fail);
		sleep(2);

	}

	@FindBy(xpath = "//span[text()='Please provide a description for the alert.']")
	private WebElement ErrorMessageDescriptionEmpty;

	public void VerifySaveWithoutFillingAlertDescription() throws InterruptedException {

		String ErrorMessageNotitle = ErrorMessageDescriptionEmpty.getText();
		String EXpectedError = "Please provide a description for the alert.";
		String Pass = "PASS>>'Please provide a description for the alert.' is Displayed";
		String Fail = "FAIL>>'Please provide a description for the alert.' is NOT Displayed";
		ALib.AssertEqualsMethod(EXpectedError, ErrorMessageNotitle, Pass, Fail);
		sleep(2);

	}

	public void VerifyDeviceSide() throws InterruptedException {
		Boolean AlertMessageOnDeviceSide = Initialization.driverAppium
				.findElementByXPath("//android.webkit.WebView[@index='0']").isDisplayed();
		String Pass = "PASS>> Alert Message Displayed";
		String Fail = "FAIL>> Alert Message Not Displayed";
		ALib.AssertTrueMethod(AlertMessageOnDeviceSide, Pass, Fail);
		sleep(20);

		Boolean NixSettingsButton = Initialization.driverAppium
				.findElementByXPath("//android.widget.Button[@text='Settings']").isDisplayed();
		String Pass1 = "PASS>> NixSettingsButton Displayed";
		String Fail1 = "FAIL>> NixSettingsButton Not Displayed";
		ALib.AssertTrueMethod(NixSettingsButton, Pass1, Fail1);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[1]/button")
	private WebElement CloseAlertmessage;

	public void ClickOnCloseAlertMessage() throws InterruptedException {

		CloseAlertmessage.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[1]/button")
	private WebElement CloseAlertCreateMessagePopup;

	public void CloseBtn() throws InterruptedException {

		CloseAlertCreateMessagePopup.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='EnableSnooze']")
	private WebElement EnableSnooze;

	public void EnableSnoozeCheckBox() throws InterruptedException {
		EnableSnooze.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='EnableSnoozeInput']")
	private WebElement EnableSnoozeInput;

	public void InputSnoozeDuration(String SnoozeTime) throws InterruptedException {

		EnableSnoozeInput.sendKeys(SnoozeTime);
		sleep(4);
	}

	public void VerifyDeviceSideSnoozePopup() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Snooze']").click();
		sleep(20);

		Boolean SnoozePopup = Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Snooze']")
				.isDisplayed();
		String Pass = "PASS>> Snooze Popup is displayed";
		String Fail = "FAIL>> Snooze Popup is not displayed";
		ALib.AssertTrueMethod(SnoozePopup, Pass, Fail);
		sleep(2);

	}

	public void ClickOnOpenSnoozePopUp() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Open']").click();
		sleep(2);

		Boolean AlertMessageOnDeviceSide = Initialization.driverAppium
				.findElementByXPath("//android.webkit.WebView[@index='0']").isDisplayed();
		String Pass = "PASS>> Alert Message Displayed";
		String Fail = "FAIL>> Alert Message Not Displayed";
		ALib.AssertTrueMethod(AlertMessageOnDeviceSide, Pass, Fail);
		sleep(20);

	}

	public void ClickOnOpenSnoozePopUpVideo() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Open']").click();
		sleep(2);

		Boolean AlertMessageOnDeviceSide = Initialization.driverAppium
				.findElementByXPath("//android.widget.SeekBar[@index='2']").isDisplayed();
		String Pass = "PASS>> Alert Message Displayed";
		String Fail = "FAIL>> Alert Message Not Displayed";
		ALib.AssertTrueMethod(AlertMessageOnDeviceSide, Pass, Fail);
		sleep(20);

	}

	@FindBy(xpath = "//*[@id='backToPanel1']")
	private WebElement backToPanel1;

	@FindBy(xpath = "//*[@id='osPanel_cancel_btn']")
	private WebElement osPanel_cancel_btn;

	public void ClickOnNewJobBackButton() throws InterruptedException {

		backToPanel1.click();
		waitForXpathPresent("//*[@id='andIcon']");
		sleep(2);

	}

	public void osPanel_cancel_btnClick() throws InterruptedException {

		osPanel_cancel_btn.click();
		waitForXpathPresent("//*[@id='job_new_job']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='alertIconSelection']/option[2][text()='Upload Custom icon']")
	private WebElement UploadCustomIcon;

	public void UploadCustomIcon_Select() throws Throwable {
		UploadCustomIcon.click();
		sleep(4);

	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[2]/div[3]/div[3]/div[3]/div[2]/div/div/div[1]/div/div[2]/i")
	private WebElement inputFileFromExplorer;

	@FindBy(xpath = "(//input[@type='file'])[2]")
	private WebElement uploadFile;

	public void clickOnuploadalertFile(String FileName) throws InterruptedException, IOException {
		JavascriptExecutor executor = (JavascriptExecutor) Initialization.driver;
		executor.executeScript("arguments[0].click();", uploadFile);
		sleep(2);
		// uploadalert.click();
		Runtime.getRuntime().exec(FileName);

		sleep(8);
	}

	public void ScrollToUploadButton() throws InterruptedException {

		JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
		je.executeScript("arguments[0].scrollIntoView()", alertDescriptionColor);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[2]/div[1]/div[2]/span[@class='img_option']")
	private WebElement UploadExistingAlert;

	public void ClickOnUploadExistingAlert() throws InterruptedException {

		UploadExistingAlert.click();
		waitForXpathPresent("//h4[text()='Upload Existing Alert']");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='Select_File_Type']")
	private WebElement Select_File_Type;

	public void ClickOnSelect_File_Type() throws InterruptedException {

		Boolean ImageSelected = Select_File_Type.isDisplayed();
		String Pass = "PASS>> Image selected by default is displayed";
		String Fail = "FAIL>> Image selected by default is NOT displayed";
		ALib.AssertTrueMethod(ImageSelected, Pass, Fail);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='Select_File_Type']/option[2]")
	private WebElement VideoFile;

	@FindBy(xpath = "//*[@id='alertVideoURL']")
	private WebElement alertVideoURL;

	public void AlertVideoUrl(String urlVideo) throws InterruptedException {

		alertVideoURL.clear();
		sleep(2);

		alertVideoURL.sendKeys(urlVideo);
		sleep(2);
	}

	@FindBy(xpath = "//span[text()='Please provide an image for the alert.']")
	private WebElement AlerImageErrorMessage;

	public void VerfyErrorMessageAlertImage() throws InterruptedException {

		String ErrorMessageNotitle = AlerImageErrorMessage.getText();
		String EXpectedError = "Please provide an image for the alert.";
		String Pass = "PASS>> 'Please provide an image for the alert.' is Displayed";
		String Fail = "FAIL>>'Please provide an image for the alert.' is NOT Displayed";
		ALib.AssertEqualsMethod(EXpectedError, ErrorMessageNotitle, Pass, Fail);
		sleep(2);

	}

	public void SelectVideoFile() throws InterruptedException {

		Select_File_Type.click();
		sleep(2);

		VideoFile.click();
		waitForXpathPresent("//span[text()='Alert Video URL']");
		sleep(2);
	}

	@FindBy(id = "uploadAlertImage")
	private WebElement uploadAlertImage;

	public void uploadAlertImageClick() throws InterruptedException, IOException {

		uploadAlertImage.click();

		sleep(8);

		// Runtime.getRuntime().exec(Filename);

		sleep(8);
	}

	@FindBy(xpath = "(//input[@type='file'])[1]")
	private WebElement uploadalert;

	public void clickOnuploadalert(String FileName) throws InterruptedException, IOException {
		JavascriptExecutor executor = (JavascriptExecutor) Initialization.driver;
		executor.executeScript("arguments[0].click();", uploadalert);
		sleep(2);
		// uploadalert.click();
		Runtime.getRuntime().exec(FileName);

		sleep(8);

	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[1]/button[@class='close']")
	private WebElement closeButton_AlertMessage_modal;

	public void CloseButtonClick() throws InterruptedException {

		closeButton_AlertMessage_modal.click();
		waitForXpathPresent("//*[@id='install_program']");
		sleep(2);
	}

	// ***********************ALERT MESSGAE JOB****************************

	@FindBy(xpath = "//*[@id='enableBuzzAlertMsg']/div[4]/div/input[@value='SureMDM Nix Agent']")
	private WebElement alert_popup_title_input;

	public void selectAlert_Display_typeDropDown() throws InterruptedException {

		Select select = new Select(Alert_Display_type);
		select.selectByValue("Popup");
		sleep(2);

	}

	@FindBy(xpath = "//textarea[contains(text(),'Your administrator has sent a message, please check it out!!')]")
	private WebElement MessageTextArea;

	public void GetDefaultValueAndMessageTextBox() throws InterruptedException {

		boolean DefaultValue = alert_popup_title_input.isDisplayed();
		String pass = "PASS>> 'SureMDM Nix Agent' is displayed";
		String fail = "FAIL>> 'SureMDM Nix Agent' is Not Dispalyed";
		ALib.AssertTrueMethod(DefaultValue, pass, fail);
		sleep(2);

		boolean DefaultMessage = MessageTextArea.isDisplayed();
		String pass1 = "PASS>> 'Your administrator has sent a message, please check it out!!' is displayed";
		String fail1 = "FAIL>> 'Your administrator has sent a message, please check it out!!' is Not Dispalyed";
		ALib.AssertTrueMethod(DefaultMessage, pass1, fail1);
		sleep(2);
	}

	public void verifyCustomizingValueTextField(String EnterVAlue) throws InterruptedException {

		alert_popup_title_input.clear();
		sleep(1);
		alert_popup_title_input.sendKeys(EnterVAlue);
		sleep(1);
		Reporter.log(EnterVAlue);

	}

	public void verifyCustomizingMessageTextField(String EnterVAlue) throws InterruptedException {

		MessageTextArea.clear();
		sleep(1);
		MessageTextArea.sendKeys(EnterVAlue);
		sleep(1);
		Reporter.log(EnterVAlue);

	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[1]/button")
	private WebElement CloseBtnAlertMessage_modal;

	public void CloseBtnAlertMessage_modal() throws InterruptedException {

		CloseBtnAlertMessage_modal.click();
		sleep(2);

	}

	public void ScrollToEnableSnooze() {

		JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
		je.executeScript("arguments[0].scrollIntoView()", EnableSnooze);

	}

	public void VerifyDeviceSideSnoozePopupAgain() throws InterruptedException {

		String SnoozePopup = Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Snooze']")
				.getText();
		String ExpectedResult = SnoozePopup;
		String Pass = "PASS>> Snooze Popup is displayed";
		String Fail = "FAIL>> Snooze Popup is not displayed";
		ALib.AssertEqualsMethod(SnoozePopup, ExpectedResult, Pass, Fail);
		sleep(2);

	}

	public void MessageTextField(String InputMessage) throws InterruptedException {

		MessageTextArea.clear();
		sleep(1);
		MessageTextArea.sendKeys(InputMessage);
		sleep(2);

	}

	public void ClickOnOpenAlertPopUp() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Open']").click();
		sleep(2);

	}

	public void VerifyAlertPopUp3(String MessageDisplayed) throws InterruptedException {

		boolean MessageDisplayed3 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='" + MessageDisplayed + "']").isDisplayed();
		String Pass = "PASS>> '" + Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='" + MessageDisplayed + "']") + "' is displayed";
		String Fail = "FAIL>> '" + Initialization.driverAppium.findElementByXPath(
				"//android.widget.TextView[@text='" + MessageDisplayed + "']") + "' is NOt displayed";
		ALib.AssertTrueMethod(MessageDisplayed3, Pass, Fail);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='AlertMessage_modal']/div/div/div[2]/div[3]/div[3]/div[5]/div/div/span[3]/i")
	private WebElement AlertMessage_modalRight;

	public void ClickOnRightAlignment() throws InterruptedException {

		AlertMessage_modalRight.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='AlertTextBlk']/h1") // //*[@id="AlertTextBlk"]/p
	private WebElement AlertTextBlkTitle;
	@FindBy(xpath = "//*[@id='AlertTextBlk']/p") //
	private WebElement AlertTextBlkDescription;

	public void VerifyPreviewFunctionTitle() throws InterruptedException {

		String GetAlertMessage = AlertTextBlkTitle.getText();
		System.out.println(GetAlertMessage);
		String ExpectedText = "Alert!!!";
		String pass = "PASS>>'" + ExpectedText + "' is correct";
		String fail = "FAIL>>'" + ExpectedText + "' is Not correct";
		ALib.AssertEqualsMethod(ExpectedText, GetAlertMessage, pass, fail);
		sleep(2);

		String GetAlertMessageDescription = AlertTextBlkDescription.getText();
		System.out.println(GetAlertMessageDescription);
		String ExpectedDescription = "TestAlert!!!";
		String pass1 = "PASS>>'" + ExpectedDescription + "' is correct";
		String fail1 = "FAIL>>'" + ExpectedDescription + "' is Not correct";
		ALib.AssertEqualsMethod(ExpectedDescription, GetAlertMessageDescription, pass1, fail1);
		sleep(2);

	}

	public void VerifyOnlyPreviewPopUpCloses() throws InterruptedException {

		previewBlkModalCloseBtn_Click();

		boolean PreviewButton = previewBtn_AlertMessageJob_Click.isDisplayed();
		String pass = "PASS>> Only preview prompt is close";
		String fail = "FAIL>> Only preview prompt IS not closed";
		ALib.AssertTrueMethod(PreviewButton, pass, fail);
		sleep(2);

	}

	public void VerifyDefaultBackgrndColor() throws InterruptedException {

		boolean flag;

		String BackgroudColourRed = alertBackgroundColor.getCssValue("color");
		System.out.println(BackgroudColourRed);
		if (BackgroudColourRed.equals("rgba(102, 102, 102, 1)")) {
			flag = true;
		} else {
			flag = false;
		}
		String Pass = "Default Red Color is displayed";
		String Fail = "Default Red Color is Not displayed";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	////// Regression Jobs 27-11-2020

	@FindBy(xpath = "//div[contains(@class,'col-xs-12 col-md-8 input-group')]//input[@id='job_nixsettings_name_input']")
	private WebElement NixAgentSettingsJobNameField;

	@FindBy(id = "job_nixsettings_ConnType_input")
	private WebElement NixAgentSettingsConnectionTypeDropDown;

	@FindBy(id = "EnableGeoFencing")
	private WebElement EnableGeoFenceCheckBox;

	@FindBy(xpath = "//span[text()='Select Fence']")
	private WebElement SelectFence;

	@FindBy(xpath = "//span[text()='Fence Entered']")
	private WebElement FenceEntered;

	@FindBy(xpath = "//span[text()='Fence Exited']")
	private WebElement FenceExited;

	@FindBy(xpath = "//button[@title='Zoom in']")
	private WebElement GeoFenceZoomInButton;

	@FindBy(xpath = "//button[@title='Zoom out']")
	private WebElement GeoFenceZoomoutButton;

	@FindBy(xpath = "//div[@id='you_location_img']")
	private WebElement CurrentLocationSymbol;

	@FindBy(xpath = "//button[@title='Stop drawing']")
	private WebElement StopDrawingButton;

	@FindBy(xpath = "//button[@title='Draw a circle']")
	private WebElement DrawFenceButton;

	@FindBy(id = "geoFenc_save")
	private WebElement geoFenceSaveButton;

	@FindBy(xpath = "//div[@aria-label='Map']")
	private WebElement Map;

	@FindBy(id = "fenceClose")
	private WebElement GeoFenceCloseButton;

	@FindBy(xpath = "//span[text()='Download Template']")
	public WebElement DownloadGeoFenceTemplateButton;

	@FindBy(id = "job_copy")
	private WebElement CopyJobOption;

	@FindBy(id = "job_sp_input_9")
	private WebElement SecuritypolicyWiFiDropDown;

	@FindBy(id = "DeviceChargingState")
	private WebElement DeviceChargingStateDropDown;

	@FindBy(xpath = "//div[@id='securityPolicy_modal']//button[@class='close']")
	private WebElement SecurityJobCloseButton;

	public void SendingNixAgentSettingsJob(String JobName) throws InterruptedException {
		NixAgentSettingsJobNameField.sendKeys(JobName);
		sleep(3);
	}

	public void SelectingOptionsInNixAgentsettingsJob(String Id) throws InterruptedException {
		Initialization.driver.findElement(By.id(Id)).click();
		sleep(3);
	}

	public void SelectingConnectionTypeInNixAgentSettingsJob(String ID, String Option) throws InterruptedException {
		WebElement DropDown = Initialization.driver.findElement(By.id(ID));
		Select sel = new Select(DropDown);
		sel.selectByValue(Option);
		sleep(2);
	}

	public void VerifyGeoFenceUI() throws InterruptedException {
		if ((EnableGeoFenceCheckBox.isSelected())) {
			Reporter.log("Enable Geo Fence Check Box Is Checked Automatically", true);
		} else {
			ALib.AssertFailMethod("FAIL>>> Enable Geo Fence Check Box Is Not Checked Automatically");
		}

		WebElement[] ele = { SelectFence, FenceEntered, FenceExited, GeoFenceZoomInButton, GeoFenceZoomoutButton,
				CurrentLocationSymbol, StopDrawingButton, DrawFenceButton, geoFenceSaveButton, Map };
		String[] name = { "Select Fence", "Fence Entered", "Fence Exited", "ZoomIn Button", "ZoomOut Button",
				"Current Location Symbol", "Stop Drawing", "Draw Fence", "Save Fence", "Map" };

		for (int i = 0; i < ele.length; i++) {
			boolean value = ele[i].isDisplayed();
			String PassStatement = "PASS >> " + name[i] + " is displayed successfully";
			String FailStatement = "FAIL >> " + name[i] + " is not displayed";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			sleep(2);
		}
	}

	public void ClosingGeoFenceTab() throws InterruptedException {
		GeoFenceCloseButton.click();
		waitForidPresent("geo_fencing_job");
		sleep(2);
	}

	public void VerifydownloadedFileInLocation(String FileName, WebElement Element)
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		String downloadPath = "C:\\Users\\kumari.swati\\Downloads";// C:\Users\kumari.swati
		String fileName = FileName + ".csv";
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		Element.click();
		sleep(10);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;

			}
		}
		boolean value = result > initial;
		String PassStatement = "PASS >> File is Downloaded successfully";
		String FailStatement = "Fail >> File is not Downloaded";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnCopyJobOption() throws InterruptedException {
		CopyJobOption.click();
		waitForXpathPresent("//h4[text()='Copy Job']");
		sleep(2);
	}

	public void SelctingFolder(String JobName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//li[text()='" + JobName + "']")).click();
		sleep(2);
		OkButtonJob.click();
		waitForXpathPresent("//span[text()='Job copied successfully.']");
		sleep(2);
	}

	public void VerfyingKeepWIFIAlwaysOnOff() {
		String[] optt = { "Don't Care", "Always On", "Always Off" };
		ArrayList<String> options = new ArrayList<>();
		Select sel = new Select(SecuritypolicyWiFiDropDown);
		List<WebElement> opt = sel.getOptions();
		for (int i = 0; i < opt.size(); i++) {
			System.out.println(opt.get(i).getText());
			options.add(opt.get(i).getText());
		}
		boolean val = Arrays.asList(optt).equals(options);
		String Pass = "Pass>>> Wifi Options are shown correctly";
		String Fail = "Fail>>> Wifi Options are not shown correctly";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}

	public void ClickOnSecurityPolicyJobCloseButton() throws InterruptedException {
		SecurityJobCloseButton.click();
		waitForidPresent("geo_fencing_job");
		sleep(2);
	}

	public void VerifyWiFiStatus(String Status) throws IOException {
		Process pr = Runtime.getRuntime().exec("adb shell settings get global wifi_on");
		BufferedReader ny = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String WifiStatus = ny.readLine();
		System.out.println(WifiStatus);
		if (WifiStatus.equals(Status)) {
			Reporter.log("PASS>>> Status Is Shown Correctly", true);
		} else {
			ALib.AssertFailMethod("FAIL>>> Status Is Not Shown Correctly");
		}
	}

	public void SelectingWifiStatus(String Status) throws InterruptedException {
		Select sel = new Select(SecuritypolicyWiFiDropDown);
		sel.selectByValue(Status);
		sleep(2);
	}

	public void RebootingTheDevice() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb reboot -p");
		sleep(120);
	}

	public void VerifyAppIsNotOpenedOnDisablingInSecurityPolicy(String Package)
			throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell am start com.sec.android.app.camera");
		sleep(6);
		Process pr = Runtime.getRuntime().exec("adb shell \"dumpsys activity activities | grep mResumedActivity\"");
		BufferedReader ny = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String Appstatus = ny.readLine();
		if (Appstatus.contains(Package)) {
			ALib.AssertFailMethod("FAIL>>> App Is Launched Even After Disabling It From Security Ploicy");
		} else {
			Reporter.log("PASS>>> App Is Not Launched After Disabling It From Security Ploicy", true);
		}
	}

	public void SelectingChargingStateInApplyJob(String ChargingState) throws InterruptedException {
		Select sel = new Select(DeviceChargingStateDropDown);
		sel.selectByVisibleText(ChargingState);
		sleep(2);
	}

	public void VerifyChargingPluggeStatusInsidJobQueue(String ChargingStatus) throws InterruptedException {
		String ChargingStatusInConsole = Initialization.driver
				.findElement(By.xpath("//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[9]")).getText();
		if (ChargingStatus.equals(ChargingStatusInConsole)) {
			ClickOnJobQueueCloseButton();
			Reporter.log("Device Charging Status In Job Queue Is Shown Correctly", true);
		} else {
			ClickOnJobQueueCloseButton();
			ALib.AssertFailMethod("Device Charging Status In Job Queue Is Not Shown Correctly");
		}

	}

	public void VerifyDeviceChargingStateOptionInApplyJob() throws InterruptedException {
		ArrayList<String> options = new ArrayList<>();
		Select sel = new Select(DeviceChargingStateDropDown);
		List<WebElement> RawOptions = sel.getOptions();
		for (int i = 0; i < RawOptions.size(); i++) {
			options.add(RawOptions.get(i).getText());
		}
		if (options.contains("Plugged In") && options.contains("Don't Care")) {
			Reporter.log("Charging State Options are displayed correctly", true);
		}

		else {
			Initialization.quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
			ALib.AssertFailMethod("Charging State Options are Not displayed correctly");
		}
	}

	// **********************************************SURELOCK
	// SETTINGS*************************************************************
	// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	@FindBy(xpath = "//*[@id='AllowedApplications']/div/div[2]")
	private WebElement DynamicallowedApplications;

	public void clickOnDynamicXSLTAllowedApplications() throws InterruptedException {
		DynamicallowedApplications.click();
		waitForVisibilityOf("//*[@id='add_folder']");
		Reporter.log("Clicked On allowedApplications");
	}

	public void verifyFolderInSureLockSettingHomeScreen(String FolderName) throws InterruptedException {
		sleep(10);
		if (Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'" + FolderName + "')]").isDisplayed()) {

			Reporter.log(FolderName + "Sure Lock FolderName are displayed", true);
			Initialization.driverAppium
					.findElementByXPath("//android.widget.TextView[contains(@text,'" + FolderName + "')]").click();
			Reporter.log("Clicked on " + FolderName, true);

		} else {
			ALib.AssertFailMethod(FolderName + "Sure Lock FolderName are not displayed,hence didnt click on Folder");
		}
	}

	public void verifyDeviceSideRemoveAppSureLockSetting(String AppsName) throws InterruptedException {
		sleep(10);

		boolean flag = false;
		try {
			if (Initialization.driverAppium
					.findElementByXPath("//android.widget.TextView[contains(@text,'" + AppsName + "')]")
					.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >>'" + AppsName + "'Sure Lock Apps are not displayed";
		String FailStatement = "FAIL >> '" + AppsName + "'Sure Lock Apps are  displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}

	@FindBy(xpath = "//*[@id='add_folder']")
	private WebElement AddFolderConsole;

	public void ClickOnAddFolder() {
		AddFolderConsole.click();
		Reporter.log("Clicked on Add Folder", true);
	}

	@FindBy(xpath = "//input[@id='newFolderName']")
	private WebElement FolderName;

	public void enterFolderName(String name) {
		FolderName.sendKeys(name);
		Reporter.log("Enter Folder name", true);
	}

	@FindBy(xpath = "//*[@id='new_folder_popup']/div/div/div[3]/button[2][contains(text(),'OK')]")
	private WebElement OkBtnOnFolder;

	public void tapOkBtnOnFolder() {
		OkBtnOnFolder.click();
		Reporter.log("Tapped Ok button on Folder", true);
	}

	@FindBy(xpath = "//android.widget.Button[@text='Add Folder']")
	private WebElement AddFolder;

	public void tapOnAddFolder() {
		AddFolder.click();
		Reporter.log("Tapped on Add Folder", true);
	}

	public void verifyAddFolder() {
		AddFolder.isDisplayed();
		Reporter.log("Add Folder is Displayed", true);
	}

	@FindBy(xpath = "(//*[@id='allowedApps']/ul/li[2]/div/div[2]/p[contains(text(),'')])[1]")
	private WebElement tapOnCreatedFolder;

	public void ClickOnCreatedFolderAllowedApplicationSureLock() {

		tapOnCreatedFolder.click();
		Reporter.log("Clicked On Created Folder", true);

	}

	public void VerifyFolderOnDeviceSide() {

		Initialization.driverAppium.findElementById("com.nix:id/selection_item_image").click();
		Reporter.log("Clicked on Folder On Devcie Side", true);

	}

	@FindBy(xpath = "//button[@id='done_btn']")
	private WebElement done_btn;

	public void ClickOnDoneBtn() {
		done_btn.click();

	}

	/*
	 * public void tapOnscreenNTimes(int n) throws IOException,
	 * InterruptedException{ int i = 0; while(i<n) { WebElement activate =
	 * Initialization.driverAppium.findElementById(
	 * "com.gears42.surelock:id/childLayout"); new
	 * TouchAction(Initialization.driverAppium)
	 * .tap(tapOptions().withElement((ElementOption) activate))
	 * .waitAction(waitOptions(Duration.ofMillis(50))).perform(); i++; }
	 * Reporter.log("Tapped on screen "+(n-1)+" times", true); sleep(2); }
	 */

	public void goToSLAdminSettings() throws IOException, InterruptedException {
		sleep(2);
		Runtime.getRuntime().exec(
				"adb shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surelock");
		Reporter.log("Navigated to Admin Settings", true);
		sleep(10);
	}

	public void goToSVAdminSettings() throws IOException, InterruptedException {
		sleep(2);
		Runtime.getRuntime().exec(
				"adb shell am broadcast -a com.gears42.surevideo.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surevideo");
		Reporter.log("Navigated to Admin Settings", true);
		sleep(10);
	}

	public void TapDevice() throws IOException, InterruptedException {

		Runtime.getRuntime().exec("adb shell input keyevent POWER");
		sleep(2);
	}

	@FindBy(xpath = "//android.widget.TextView[text='Select the applications to be allowed in Lockdown Mode']")
	private WebElement AllowedApplications;

	public void tapOnAllowedApplications() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Allowed Applications']")
				.click();
		Reporter.log("Clicked on AllowedApplications", true);
	}

	@FindBy(id = "com.gears42.surelock:id/instruction_txt")
	private WebElement AllowedAppinstructions;

	public void verifyAndTapAllowedAppinstructionsText() throws InterruptedException {
		try {
			AllowedAppinstructions.click();
			Reporter.log("Tapped on Allowed App instructions text", true);
		} catch (Exception e) {
			Reporter.log("Allowed App instructions text is not displayed", true);
		}
		sleep(3);
	}

	public void tapOnDoneBtn() {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Done' and @index='2']").click();
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/main_done_button").click();
		Reporter.log("Tapped on Done Button", true);
	}

	public void tapOnDoneBtn2() throws InterruptedException {
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/main_done_button").click();
		Reporter.log("Tapped on Done Button", true);
		sleep(10);
	}

	/*
	 * public void verifyEnableUseSysWallpaper() throws IOException,
	 * InterruptedException{ sleep(4); //Initialization.driverAppium.
	 * findElementByXPath("//android.widget.TextView[contains(@text,'SureLock Settings')]"
	 * ).click(); List<WebElement> surelockSettingCheckBox =
	 * Initialization.driverAppium.findElementsById("android:id/checkbox"); String
	 * useSysWallpaper = surelockSettingCheckBox.get(0).getAttribute("checked");
	 * if(useSysWallpaper.equalsIgnoreCase("true")) {
	 * Reporter.log("Use system wallpaper checkbox is checked", true); }else {
	 * ALib.AssertFailMethod("Use system wallpaper checkbox is not checked" ); } }
	 */
	@FindBy(xpath = "(//p[contains(text(),'Use System Wallpaper should be disabled')])[1]")
	private WebElement disabled_Wallpaper;
	@FindBy(xpath = "(//p[contains(text(),'Use System Wallpaper should be disabled')])[2]")
	private WebElement disabled_CentreWallpaper;
	@FindBy(xpath = "//p[contains(text(),'Tap to configure wallpaper')]")
	private WebElement configure_wallpaper;
	@FindBy(xpath = "//p[contains(text(),'Center')]")
	private WebElement Center;

	public void VerifyWallpaper() throws InterruptedException {

		boolean EnableWallpaper = configure_wallpaper.isDisplayed();
		String pass = "PASS>>'Tap to configure wallpaper'is displayed";
		String fail = "FAIL>>'Tap to configure wallpaper'is not displayed";
		ALib.AssertTrueMethod(EnableWallpaper, pass, fail);
		sleep(2);
		boolean EnableWallpaperPosition = Center.isDisplayed();
		String pass1 = "PASS>>'Center'is displayed";
		String fail1 = "FAIL>>'Center'is not displayed";
		ALib.AssertTrueMethod(EnableWallpaperPosition, pass1, fail1);
		sleep(2);
	}

	public void VerifyWhenUseSysWallpaperIsDisabled() throws InterruptedException {

		boolean DisabledWallpaper = disabled_Wallpaper.isDisplayed();
		String pass = "PASS>>'Use System Wallpaper should be disabled' is displayed";
		String fail = "FAIL>>'Use System Wallpaper should be disabled' is not displayed";
		ALib.AssertTrueMethod(DisabledWallpaper, pass, fail);
		sleep(1);
		boolean DisabledWallpaperPosition = disabled_CentreWallpaper.isDisplayed();
		String pass1 = "PASS>>'Use System Wallpaper should be disabled'for WallpaperPostion is displayed";
		String fail1 = "FAIL>>'Use System Wallpaper should be disabled' for WallpaperPostion is not displayed";
		ALib.AssertTrueMethod(DisabledWallpaperPosition, pass1, fail1);
		sleep(1);
	}

	public void DeviceSideClickOnSureLockSettings() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureLock Settings']").click();
		sleep(2);
		Reporter.log("Clicked On SureLock Settings Device Side", true);

	}

	public void DeviceSideClickOnSureVideoSettings() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureVideo Settings']").click();
		sleep(2);
		Reporter.log("Clicked On SureVideo Settings Device Side", true);

	}

	@FindBy(id = "android:id/checkbox")
	private WebElement IsSysWallaperCheckBoxEnabled;

	/*
	 * public void verifyEnableUseSysWallpaper() throws IOException,
	 * InterruptedException{ sleep(4);
	 * 
	 * if(Initialization.driverAppium.findElementById("android:id/checkbox").
	 * isEnabled()) { Reporter.log("Use system wallpaper checkbox is checked",
	 * true); }else {
	 * ALib.AssertFailMethod("Use system wallpaper checkbox is not checked" ); } }
	 */
	public void verifyEnableUseSysWallpaper(String Text, int a) throws IOException, InterruptedException {
		sleep(4);
		List<WebElement> ListOfNixCheckBox = Initialization.driverAppium
				.findElementsByClassName("android.widget.CheckBox");
		String CheckBoxStatus = ListOfNixCheckBox.get(a).getAttribute("checked");

		if (CheckBoxStatus.equals("true")) {

			Reporter.log("CehckBox Is Alreay Enabled", true);
		} else {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + Text + "']").click();
			Reporter.log("Enabled" + " " + Text + "Check Box", true);
			sleep(3);
		}
	}

	@FindBy(xpath = "//p[contains(text(),'Row and Column Size')]")
	private WebElement RowColDisplaySettings;

	public void VerifyInRowColumnSizeWindow() throws InterruptedException {

		RowColDisplaySettings.click();
		waitForXpathPresent("//*[@id='portrait_row']");
		sleep(2);
		Reporter.log("Clicked on Row and Column Size", true);

	}

	@FindBy(xpath = "//*[@id='portrait_row']")
	private WebElement portrait_row;
	@FindBy(xpath = "//*[@id='portrait_column']")
	private WebElement portrait_column;
	@FindBy(xpath = "//*[@id='landscap_row']")
	private WebElement landscap_row;
	@FindBy(xpath = "//*[@id='landscap_column']")
	private WebElement landscap_column;
	@FindBy(xpath = "//*[@id='settings-row-and-column-size-popup']/div/div/div[3]/button[2]")
	private WebElement OkBtn;

	public void Inputportrait_rowAndportrait_column(String Row, String Col) throws InterruptedException {

		portrait_row.clear();
		portrait_row.sendKeys(Row);
		sleep(1);
		portrait_column.clear();
		portrait_column.sendKeys(Col);
		sleep(2);

	}

	public void Inputlandscap_rowAndlandscap_column(String Row, String Col) throws InterruptedException {

		landscap_row.clear();
		landscap_row.sendKeys(Row);
		sleep(1);
		landscap_column.clear();
		landscap_column.sendKeys(Col);
		sleep(2);

	}

	public void ClickOnOkBtn() throws InterruptedException {

		OkBtn.click();
		waitForVisibilityOf("//p[contains(text(),'Row and Column Size')]");
		sleep(2);
	}

	public void VerifyOnDeviceSideInsideSLSettings() {

		String RowColSize_DisplaySettings = Initialization.driverAppium.findElementById("android:id/summary").getText();
		System.out.println(RowColSize_DisplaySettings);
		if (RowColSize_DisplaySettings.substring(8, 10).equals("2,2")
				&& RowColSize_DisplaySettings.substring(23, 25).equals("2,2")) {
			Reporter.log("Row And Col is equal On device", true);

		} else {
			ALib.AssertFailMethod("Row And Col is Not equal On device");
		}

	}

	public void DeviceSideClickOnRowAndCol() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Row and Column Size']")
				.click();
		String PortraitRow = Initialization.driverAppium.findElementById("com.gears42.surelock:id/rows").getText();
		System.out.println(PortraitRow + "Portrait Row Value");
		String PortraitCol = Initialization.driverAppium.findElementById("com.gears42.surelock:id/column").getText();
		System.out.println(PortraitCol + "Portrait Col  Value");
		String LandscapeRow = Initialization.driverAppium.findElementById("com.gears42.surelock:id/rowLandscape")
				.getText();
		System.out.println(LandscapeRow + "Landscape Row Value");
		String LandscapeCol = Initialization.driverAppium.findElementById("com.gears42.surelock:id/columnLandscape")
				.getText();
		System.out.println(LandscapeCol + "Landscape Col Value");

		if (PortraitRow.equals("2")) {

			Reporter.log(PortraitRow + "Portrait Row Value is SAME as Devcie Side", true);
		} else {
			ALib.AssertFailMethod("Portrait Row  Value is Not SAME as Devcie Side");
		}
		if (PortraitRow.equals("2")) {

			Reporter.log(PortraitCol + "Portrait Col Value is SAME as Devcie Side", true);
		} else {
			ALib.AssertFailMethod("Portrait  Col Value is Not SAME as Devcie Side");
		}
		if (LandscapeRow.equals("4")) {

			Reporter.log(LandscapeRow + "Landscape Row Value is SAME as Devcie Side", true);
		} else {
			ALib.AssertFailMethod("Landscape Row Value is Not SAME as Devcie Side");
		}
		if (LandscapeCol.equals("3")) {

			Reporter.log(LandscapeCol + "Landscape Col Value is SAME as Devcie Side", true);
		} else {
			ALib.AssertFailMethod("Landscape Col Value is Not SAME as Devcie Side");
		}

	}

	public void DisableRowColSize() throws InterruptedException {

		portrait_row.clear();
		sleep(1);
		portrait_column.clear();
		sleep(2);
		landscap_row.clear();
		sleep(2);
		landscap_column.clear();
		sleep(2);

	}

	@FindBy(xpath = "(//p[contains(text(),'Icon Size')])[2]")
	private WebElement IconSizeSureLockSetting;

	public void ClickOnIconSize() {

		IconSizeSureLockSetting.click();
		waitForXpathPresent("//span[contains(text(),'Small (50%)')]");
	}

	@FindBy(xpath = "//*[@id='small_size_icon']/div/span/input")
	private WebElement small_size_icon;
	@FindBy(xpath = "//*[@id='icon-size-popup']/div/div/div[3]/button[2]")
	private WebElement DoneBtnIconSize;

	public void Select_small_size_icon() throws InterruptedException {

		small_size_icon.click();
		Reporter.log("small_size_icon is  Selected", true);
		sleep(2);
	}

	public void ClickOnDoneBtnIconSize() throws InterruptedException {
		DoneBtnIconSize.click();
		Reporter.log("Clicked On Done Btn", true);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='medium_size_icon']/div/span/input")
	private WebElement medium_size_icon;

	public void Select_medium_size_icon() throws InterruptedException {

		medium_size_icon.click();
		Reporter.log("medium_size_icon is  Selected", true);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='large_size_icon']/div/span/input")
	private WebElement large_size_icon;

	public void Select_large_size_icon() throws InterruptedException {

		large_size_icon.click();
		Reporter.log("large_size_icon is  Selected", true);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='surelock_settings_pg']/header/span[3]/i")
	private WebElement SearchBar;
	@FindBy(xpath = "//*[@id='surelock_sttings']")
	private WebElement surelock_sttings_search_bar;

	public void ClickOnSearchBar(String InputSLsettings) throws InterruptedException {

		SearchBar.click();
		sleep(1);
		surelock_sttings_search_bar.sendKeys(InputSLsettings);
		sleep(2);
	}

	@FindBy(xpath = "(//p[contains(text(),'Text Color')])[1]")
	private WebElement TextColor;

	public void SelectTextColor() throws InterruptedException {

		TextColor.click();
		Reporter.log("Clicked on Text Color", true);
		sleep(1);

	}

	@FindBy(xpath = "//*[@id='blue_color']/div/span/input")
	private WebElement blue_color;

	public void SelectTextColor_BLUE() throws InterruptedException {

		blue_color.click();
		Reporter.log("Clicked On Color", true);
		sleep(1);
	}

	@FindBy(xpath = "//*[@id='text-color-popup']/div/div/div[3]/button[2]")
	private WebElement textcolorpopupDoneBtn;

	public void ClickOnDoneBtnTextColor() {
		textcolorpopupDoneBtn.click();
		Reporter.log("Clikced On Done Btn Of TextColor", true);
	}

	@FindBy(xpath = "(//p[contains(text(),'Font Size')])[1]")
	private WebElement FontSize;

	public void ClickOnFontSize() throws InterruptedException {

		FontSize.click();
		Reporter.log("Clicked On FontSize", true);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='large_fontsize']/div/span/input")
	private WebElement large_fontsize;

	public void SelectLargeFontSize() throws InterruptedException {

		large_fontsize.click();
		Reporter.log("Selected large_fontsize", true);

		sleep(2);
	}

	@FindBy(xpath = "//*[@id='font-size-popup']/div/div/div[3]/button[2]")
	private WebElement font_size_popupDoneBtn;

	public void ClickOnfont_size_popupDoneBtn() throws InterruptedException {

		font_size_popupDoneBtn.click();
		Reporter.log("Clicked On font_size_popupDoneBtn", true);
		sleep(2);

	}

	public void DeviceSideSLSettingClickOnSearchBar(String InputSLSettings) {

		Initialization.driverAppium.findElementById("com.gears42.surelock:id/toolbar_search_button").click();
		Initialization.driverAppium.findElementById("android:id/search_src_text").sendKeys(InputSLSettings);

	}

	public void DeviceSideSVSettingClickOnSearchBar(String InputSVSettings) {

		Initialization.driverAppium.findElementById("com.gears42.surevideo:id/toolbar_search_button").click();
		Initialization.driverAppium.findElementById("android:id/search_src_text").sendKeys(InputSVSettings);

	}

	public void TwoTimeClickOnSLsettingsAfterSearchDeviceSide(String SLsettings) throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + SLsettings + "']").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + SLsettings + "']").click();
		sleep(2);
		Reporter.log("Clicked On " + SLsettings, true);

	}

	public void DeviceSideClickOnFontSize(String FontSizeText) throws InterruptedException {

		/*
		 * sleep(2); if(Initialization.driverAppium.
		 * findElementByXPath("//android.widget.CheckedTextView[@text='Large (100 %)' and @index='3']"
		 * ).isEnabled()) { Reporter.log(FontSizeText+
		 * "Pass FontSizeText is Selected On Device Side",true); } else {
		 * ALib.AssertFailMethod("FAIL ,FontSizeText is Not Selected On DevcieSide"); }
		 */ sleep(4);
		String VALUE = Initialization.driverAppium
				.findElementByXPath("//android.widget.CheckedTextView[@text='Large (100 %)' and @index='3']")
				.getAttribute("checked");

		if (VALUE.equals("true")) {

			Reporter.log(FontSizeText + "CehckBox Is Alreay Enabled", true);
		} else {
			ALib.AssertFailMethod(FontSizeText + "FAIL TO ENBALE THE TEXT COLOR");
		}
	}

	public void verifyEnableScreensaverSettings_MuteAudioSV(String Text) throws IOException, InterruptedException {
		sleep(4);
		String Screensaver = null;
		List<WebElement> Surevideo = Initialization.driverAppium.findElementsById("android:id/checkbox");
		for (int i = 0; i < Surevideo.size(); i++) {

			if (i == 0 || i == 4) {
				Screensaver = Surevideo.get(i).getAttribute("checked");
				System.out.println(Screensaver);
				String pass = "PASS>> EnableScreensaverSettings_MuteAudioSV ";
				String fail = "FAIL>> EnableScreensaverSettings_MuteAudioSV ";
				ALib.AssertEqualsMethod("true", Screensaver, pass, fail);

			}

		}

	}

	public void verifyDisableScreensaverSettings_MuteAudioSV(String Text) throws IOException, InterruptedException {
		sleep(4);
		String Screensaver = null;
		List<WebElement> Surevideo = Initialization.driverAppium.findElementsById("android:id/checkbox");
		for (int i = 0; i < Surevideo.size(); i++) {

			if (i == 4) {

				Screensaver = Surevideo.get(i).getAttribute("checked");
				String pass = "PASS>> EnableScreensaverSettings_MuteAudioSV is disabled";
				String fail = "FAIL>> EnableScreensaverSettings_MuteAudioSV is not disabled";
				ALib.AssertEqualsMethod("false", Screensaver, pass, fail);

			}

		}
	}

	//
	// List<WebElement> ListOfNixCheckBox =
	// Initialization.driverAppium.findElementsByClassName("android.widget.CheckBox");
	// String CheckBoxStatus = ListOfNixCheckBox.get(a).getAttribute("checked");
	//
	// if (CheckBoxStatus.equals("true")) {
	//
	// Reporter.log("CehckBox Is Alreay Enabled", true);
	// } else {
	//
	//
	// ALib.AssertFailMethod(Text + "FAIL TO ENBALE THE TEXT COLOR");
	//
	// }
	// }

	public void DeviceSideClickOnTextColor(String SLsettings, int i) throws InterruptedException {

		sleep(4);
		List<WebElement> ListOfNixCheckBox = Initialization.driverAppium
				.findElementsByClassName("android.widget.CheckedTextView");
		String CheckBoxStatus = ListOfNixCheckBox.get(i).getAttribute("checked");

		if (CheckBoxStatus.equals("true")) {

			Reporter.log(SLsettings + "CehckBox Is Alreay Enabled", true);
		} else {
			ALib.AssertFailMethod(SLsettings + "FAIL TO ENBALE ");
		}

	}

	public void DeviceSideClickOnIconSize(String IconSize) {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Icon Size']").click();
		/*
		 * if(Initialization.driverAppium.findElementByXPath(
		 * "//android.widget.CheckedTextView[contains(@text(),'Small']").isEnabled()) {
		 * Reporter.log(IconSize+ "Pass IconSize is Selected On Device Side",true); }
		 * else { ALib.AssertFailMethod("FAIL ,IconSize is Not Selected On DevcieSide");
		 * }
		 */
		String value = Initialization.driverAppium
				.findElementByXPath("//android.widget.CheckedTextView[@text()='" + IconSize + "']")
				.getAttribute("checked");
		if ("true".equals(value)) {
			System.out.println(IconSize + " is Checked");
		} else {
			ALib.AssertFailMethod("FAIL ,IconSize is Not Selected On DevcieSide");

		}

	}

	@FindBy(xpath = "//*[@id='icon_relocation_settings']/div/div[2]/span")
	private WebElement icon_relocation_settings;

	public void Enable_icon_relocation_settings() throws InterruptedException {

		icon_relocation_settings.click();
		Reporter.log("Clicked On icon_relocation_settings Checkbox", true);
		sleep(2);

	}

	public void VerifyDeviceSideDisable_icon_relocation_settings(String Text, int a) throws InterruptedException {

		sleep(4);
		List<WebElement> ListOfNixCheckBox = Initialization.driverAppium
				.findElementsByClassName("android.widget.CheckBox");
		String CheckBoxStatus = ListOfNixCheckBox.get(a).getAttribute("checked");

		if (CheckBoxStatus.equals("true")) {

			ALib.AssertFailMethod("FAIL CehckBox Is Alreay Enabled");
		} else {

			Reporter.log("Disabled" + " " + Text + "Check Box", true);
			sleep(3);
		}
	}

	public void ClickOnAllowIconRelocationAfterSearchingOnDeviceSide() {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Allow Icon Relocation']")
				.click();

	}

	public void OneTimeClickAfterSearchSLsettings(String EnterSLsettingName) {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + EnterSLsettingName + "']")
				.click();

	}

	public void VerifyRelocateICON_DeviceSideSLhomescreen(String AppsName) {

		WebElement IconNAme = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'" + AppsName + "')]");

		TouchAction action = new TouchAction(Initialization.driverAppium);

		action.longPress((LongPressOptions) IconNAme).release().perform();
		// new TouchAction((MobileDriver)
		// Initialization.driverAppium).press(filename).waitAction().release().perform();

	}

	@FindBy(xpath = "//*[@id='hide_apptitle_settings']/div/div/span/input[@type='checkbox']")
	private WebElement hide_apptitle_settings_CheckBox;

	public void Enablehide_apptitle_settings_CheckBox() throws InterruptedException {

		hide_apptitle_settings_CheckBox.click();
		Reporter.log("Clicked On hide_apptitle_settings_CheckBox", true);
		sleep(2);
	}

	@FindBy(xpath = "(//p[contains(text(),'Single Application Mode')])[2]")
	private WebElement SingleApplicationMode;

	public void ClickOnSingleApplicationMode() throws InterruptedException {

		SingleApplicationMode.click();
		waitForVisibilityOf("//*[@id='single_application_mode']/div/div[2]/span/input[@type='checkbox']");
		Reporter.log("Clicked On SingleApplicationMode", true);

	}

	@FindBy(xpath = "//*[@id='single_application_mode']/div/div[2]/span/input[@type='checkbox']")
	private WebElement single_application_modeEnable;
	@FindBy(xpath = "//*[@id='singleAppMode_message_popup']/div/div/div/button[text()='OK']")
	private WebElement singleAppMode_message_popup;
	@FindBy(xpath = "//*[@id='single_appmode_settings']/footer/div[2]/button")
	private WebElement single_appmode_settingsDoneBtn;

	public void Enablesingle_application_mode() throws InterruptedException {

		single_application_modeEnable.click();
		waitForXpathPresent("//*[@id='singleAppMode_warning_popup']/div/div/div/button[text()='OK']");
		sleep(2);
		singleAppMode_message_popup.click();
		sleep(1);
		single_appmode_settingsDoneBtn.click();
		Reporter.log("Clicked on SingleApplicationMode CheckBox", true);
		sleep(4);

	}

	public void VerifyOnDeviceSidesingle_application_modeEnable() throws InterruptedException {
		sleep(10);
		boolean Chrome = Initialization.driverAppium
				.findElementByXPath("//android.widget.EditText[@text='Search or type web address']").isDisplayed();
		String Pass = "PASS>> Chrome page is Launched and displayed Automatically";
		String Fail = "FAIL>> Chrome page is not Launched and displayed Automatically";
		ALib.AssertTrueMethod(Chrome, Pass, Fail);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='com.gears42.surelock:com.nix.ui.SureMdmNixSplashScreen']")
	private WebElement SureMdmNixSplashScreen;
	@FindBy(xpath = "//*[@id='com.nix:com.nix.ui.SureMdmNixSplashScreen']")
	private WebElement Com_Nix;

	public void ClickOnNixApp(String applicationName) throws InterruptedException {
		EnterSurelockAppName.clear();
		EnterSurelockAppName.sendKeys(applicationName);
		sleep(3);
		waitForXpathPresent("//*[@id='com.gears42.surelock:com.nix.ui.SureMdmNixSplashScreen']");
		SureMdmNixSplashScreen.click();
		sleep(2);
		Com_Nix.click();
		sleep(2);
		ClickOnSearchedApp.click();

	}

	public void rebootDevice() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb reboot");
		sleep(2);
		Reporter.log("Rebooting device", true);
		sleep(50);
	}

	public void LaunchSureLock() throws IOException {

		Runtime.getRuntime().exec("adb shell monkey -p com.gears42.surelock -c android.intent.category.LAUNCHER 1");

	}

	@FindBy(xpath = "//*[@id='changePasswordLi']")
	private WebElement ChangePwd;
	@FindBy(xpath = "//*[@id='password_warning_popup']/div/div/div/button[2][contains(text(),'Ok, Got It')]")
	private WebElement password_warning_popup;

	public void password_warning_popup() throws InterruptedException {
		ChangePwd.click();
		waitForXpathPresent("//*[@id='password_warning_popup']/div/div/div/button[2][contains(text(),'Ok, Got It')]");
		sleep(2);
		password_warning_popup.click();
		sleep(1);
	}

	@FindBy(xpath = "//*[@id='new_password']")
	private WebElement new_password;
	@FindBy(xpath = "//*[@id='confirm_password']")
	private WebElement confirm_password;
	@FindBy(xpath = "//*[@id='change-password-popup']/div/div/div[3]/button[2][contains(text(),'Change')]")
	private WebElement changepasswordpopupBTN;

	public void EnterNewSLpwd(String PwdSL) throws InterruptedException {
		new_password.sendKeys(PwdSL);
		confirm_password.sendKeys(PwdSL);
		sleep(1);
		changepasswordpopupBTN.click();
		Reporter.log("Clicked On Change Btn of Change Pwd", true);

	}

	public void VerifyAdminSettingsChangePwd() throws IOException, InterruptedException {
		sleep(2);
		Runtime.getRuntime().exec(
				"adb shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surelock");
		sleep(10);

		boolean flag = false;
		try {
			if (Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureLock Settings']")
					.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >>SL password is changed";
		String FailStatement = "FAIL >>SL password is NOT changed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}

	public void goToSLAdminSettingsNewPwd() throws IOException, InterruptedException {
		sleep(2);
		Runtime.getRuntime().exec(
				"adb shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"00000\" com.gears42.surelock");

		Reporter.log("Navigated to SL setting with New Pwd");
		sleep(10);

	}

	public void ClickOnWarningPopUpInChangePwdSLsetting(String Oldpwd, String NewPwd) throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK, GOT IT!']").click();
		sleep(2);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/editTextOPwd").sendKeys(Oldpwd);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/editTextNPwd").sendKeys(NewPwd);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/editTextCPwd").sendKeys(NewPwd);
		sleep(1);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/btnChangePwd").click();

	}

	@FindBy(xpath = "//*[@id='admin_users_settings']")
	private WebElement admin_users_settings;
	@FindBy(xpath = "//*[@id='allowed_admin_users']/footer/div[2]/button[1][contains(text(),'Add User')]")
	private WebElement AddUser;
	@FindBy(xpath = "//*[@id='user_name']")
	private WebElement user_name;
	@FindBy(xpath = "//*[@id='user_desc']")
	private WebElement user_desc;
	@FindBy(xpath = "//*[@id='user_pswd']")
	private WebElement user_pswd;
	@FindBy(xpath = "//*[@id='add_user_popup']/div/div/div[3]/button[2][contains(text(),'Save')]")
	private WebElement Save;
	@FindBy(xpath = "//*[@id='allowed_admin_users']/footer/div[2]/button[2][contains(text(),'Done')]")
	private WebElement DoneBtn;

	public void ClickOnadmin_users_settings() throws InterruptedException {

		admin_users_settings.click();
		sleep(1);
		AddUser.click();
		waitForXpathPresent("//p[contains(text(),'Add User')]");
		user_name.sendKeys("42Gears");
		sleep(1);
		user_desc.sendKeys("Mobility");
		sleep(1);
		user_pswd.sendKeys("00000");
		sleep(1);
		Save.click();
		sleep(1);
		DoneBtn.click();
		sleep(1);
	}

	public void VerifyDeviceSideAdminUser() throws InterruptedException {

		boolean AdminUser = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='42Gears']")
				.isDisplayed();
		String pass = "PASS>> 42Gears is displayed ";
		String fail = "FAIL>> 42Gears is Not displayed ";
		ALib.AssertTrueMethod(AdminUser, pass, fail);
		sleep(2);

		boolean AdminUserDesc = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Mobility']").isDisplayed();
		String pass1 = "PASS>> Mobility is displayed ";
		String fail1 = "FAIL>> Mobility is Not displayed ";
		ALib.AssertTrueMethod(AdminUserDesc, pass1, fail1);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='0']/div/div[3]/span/span[2][contains(text(),'Edit')]")
	private WebElement Edit;
	@FindBy(xpath = "//*[@id='add_user_popup']/div/div/div[3]/button[3][text()='Remove']")
	private WebElement Remove;

	public void VerifyRemoveAdminUser() throws InterruptedException {

		admin_users_settings.click();
		sleep(1);
		Edit.click();
		waitForXpathPresent("//p[contains(text(),'Add User')]");
		Remove.click();
		sleep(1);
		DoneBtn.click();
		sleep(1);

	}

	@FindBy(xpath = "//*[@id='wifi_mode_setting']")
	private WebElement wifi_mode_setting;
	@FindBy(xpath = "//*[@id='wifi-settings-popup']/div/div/div[3]/button[2][text()='OK']")
	private WebElement OkBtnWifi;
	@FindBy(xpath = "//*[@id='wifi_dont_care']/div/span[1]/input")
	private WebElement wifi_dont_care;

	public void ClickOnwifi_mode_setting() throws InterruptedException {

		wifi_mode_setting.click();
		waitForXpathPresent("(//p[text()='Wifi Settings'])[2]");
		boolean WifiDontCare = wifi_dont_care.isEnabled();
		String pass = "PASS>> wifi_dont_care is enbaled";
		String fail = "FAIL>> wifi_dont_care is Not enbaled";
		ALib.AssertTrueMethod(WifiDontCare, pass, fail);
		sleep(2);
		OkBtnWifi.click();

	}

	public void ClickOnCancelWifiSettings(String WifiSL) {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Cancel']").click();
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + WifiSL + "']").click();
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[@text='Always On']").click();
		Reporter.log("Clicked on WifiAlwaysOn", true);

	}

	@FindBy(xpath = "//*[@id='wifi_always_on']/div/span[1]/input")
	private WebElement wifi_always_on;

	public void ClickOnwifi_always_on() throws InterruptedException {

		wifi_mode_setting.click();
		waitForXpathPresent("(//p[text()='Wifi Settings'])[2]");
		wifi_always_on.click();
		Reporter.log("Clicked On wifi_always_on", true);
		sleep(1);
		OkBtnWifi.click();

	}

	@FindBy(xpath = "//*[@id='wifi_always_off']/div/span[1]")
	private WebElement wifi_always_off;

	public void ClickOnwifi_always_off() throws InterruptedException {

		wifi_mode_setting.click();
		waitForXpathPresent("(//p[text()='Wifi Settings'])[2]");
		wifi_always_off.click();
		Reporter.log("Clicked On wifi_always_off", true);
		sleep(1);
		OkBtnWifi.click();

	}

	public void VerifyDeviceSideWifiSL(String SLsettings, int a) throws InterruptedException {

		sleep(4);
		List<WebElement> ListOfNixCheckBox = Initialization.driverAppium.findElementsByXPath(
				"//android.widget.CheckedTextView[@text='" + SLsettings + "' and @index='" + a + "']");
		String CheckBoxStatus = ListOfNixCheckBox.get(0).getAttribute("checked");

		if (CheckBoxStatus.equals("true")) {

			Reporter.log(SLsettings + "CehckBox Is Alreay Enabled", true);
		} else {
			ALib.AssertFailMethod(SLsettings + "FAIL TO ENBALE ");
		}

	}

	@FindBy(xpath = "//*[@id='gps_settings']")
	private WebElement gps_settings;
	@FindBy(xpath = "//*[@id='gps_dont_care']/div/span[1]/span")
	private WebElement gps_dont_care;
	@FindBy(xpath = "//*[@id='gps_always_on']/div/span[1]")
	private WebElement gps_always_on;
	@FindBy(xpath = "//*[@id='gps_always_off']/div/span[1]")
	private WebElement gps_always_off;
	@FindBy(xpath = "//*[@id='gps-settings-popup']/div/div/div[3]/button[2][text()='OK']")
	private WebElement GpsOkBtn;

	public void ClickOngps_settings() throws InterruptedException {

		gps_settings.click();
		waitForXpathPresent("(//p[text()='GPS Settings'])[2]");
		boolean GpsDontCare = gps_dont_care.isEnabled();
		String pass = "PASS>> gps_dont_care is enbaled";
		String fail = "FAIL>> gps_dont_care is Not enbaled";
		ALib.AssertTrueMethod(GpsDontCare, pass, fail);
		sleep(2);
		GpsOkBtn.click();

	}

	public void ClickOngps_always_on() throws InterruptedException {

		gps_settings.click();
		waitForXpathPresent("(//p[text()='GPS Settings'])[2]");
		gps_always_on.click();
		Reporter.log("Clicked On gps_always_on", true);
		sleep(1);
		GpsOkBtn.click();

	}

	public void ClickOngps_always_off() throws InterruptedException {

		gps_settings.click();
		waitForXpathPresent("(//p[text()='GPS Settings'])[2]");
		gps_always_off.click();
		Reporter.log("Clicked On gps_always_off", true);
		sleep(1);
		GpsOkBtn.click();

	}

	@FindBy(xpath = "//*[@id='home_screenOrientation_setting']")
	private WebElement home_screenOrientation_setting;
	@FindBy(xpath = "//*[@id='dont_care_orientation']/div/span[1]/span")
	private WebElement dont_care_orientation;
	@FindBy(xpath = "//*[@id='landscape_orientation']/div/span[1]")
	private WebElement landscape_orientation;
	@FindBy(xpath = "//*[@id='portrait_orientation']/div/span[1]")
	private WebElement portrait_orientation;
	@FindBy(xpath = "//*[@id='surelock-home-screen']/div/div/div[3]/button[2][text()='OK']")
	private WebElement SLhomeScreenOkBtn;

	public void ClickOnhome_screenOrientation_setting() throws InterruptedException {

		home_screenOrientation_setting.click();
		waitForXpathPresent("(//p[text()='SureLock Home Screen Orientation'])[2]");
		boolean SLhomeScreenDontCare = dont_care_orientation.isEnabled();
		String pass = "PASS>> dont_care_orientation is enbaled";
		String fail = "FAIL>> dont_care_orientation is Not enbaled";
		ALib.AssertTrueMethod(SLhomeScreenDontCare, pass, fail);
		sleep(2);
		SLhomeScreenOkBtn.click();

	}

	public void ClickOnlandscape_orientation() throws InterruptedException {

		home_screenOrientation_setting.click();
		waitForXpathPresent("(//p[text()='SureLock Home Screen Orientation'])[2]");
		sleep(2);
		landscape_orientation.click();
		Reporter.log("Clicked On landscape_orientation", true);
		sleep(1);
		SLhomeScreenOkBtn.click();

	}

	public void ClickOnportrait_orientation() throws InterruptedException {

		home_screenOrientation_setting.click();
		waitForXpathPresent("(//p[text()='SureLock Home Screen Orientation'])[2]");
		sleep(2);
		portrait_orientation.click();
		Reporter.log("Clicked On portrait_orientation", true);
		sleep(1);
		SLhomeScreenOkBtn.click();

	}

	public void VerifyDeviceSideSLHomeScreenOreientation() throws InterruptedException {

		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='SureLock Home Screen Orientation']").click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='rotation_setting']")
	private WebElement rotation_setting;
	@FindBy(xpath = "//*[@id='rotation_dont_care']/div/span[1]")
	private WebElement rotation_dont_care;
	@FindBy(xpath = "//*[@id='rotation_always_on']/div/span[1]")
	private WebElement rotation_always_on;
	@FindBy(xpath = "//*[@id='rotation_always_off']/div/span[1]")
	private WebElement rotation_always_off;
	@FindBy(xpath = "//*[@id='rotation-settings-popup']/div/div/div[3]/button[2]")
	private WebElement SLrotation_settingScreenOkBtn;

	public void ClickOnrotation_settingDontCare() throws InterruptedException {

		rotation_setting.click();
		waitForXpathPresent("(//p[text()='Rotation Settings'])[2]");
		boolean SLrotation_settingnDontCare = rotation_dont_care.isEnabled();
		String pass = "PASS>> rotation_dont_care is enbaled";
		String fail = "FAIL>> rotation_dont_care is Not enbaled";
		ALib.AssertTrueMethod(SLrotation_settingnDontCare, pass, fail);
		sleep(2);
		SLrotation_settingScreenOkBtn.click();

	}

	public void ClickOnrotation_always_on() throws InterruptedException {

		rotation_setting.click();
		waitForXpathPresent("(//p[text()='Rotation Settings'])[2]");
		sleep(2);
		rotation_always_on.click();
		Reporter.log("Clicked On rotation_always_on", true);
		sleep(1);
		SLrotation_settingScreenOkBtn.click();

	}

	public void ClickOnrotation_always_off() throws InterruptedException {

		rotation_setting.click();
		waitForXpathPresent("(//p[text()='Rotation Settings'])[2]");
		sleep(2);
		rotation_always_off.click();
		Reporter.log("Clicked On rotation_always_off", true);
		sleep(1);
		SLrotation_settingScreenOkBtn.click();

	}

	@FindBy(xpath = "//*[@id='screenSaver_sett']")
	private WebElement screenSaver_sett;
	@FindBy(xpath = "//*[@id='enable_screen_saver']/div/div[2]/span/input")
	private WebElement enable_screen_saver;
	@FindBy(xpath = "//*[@id='use_sys_wallpaper']/div/div[2]/span/input")
	private WebElement use_sys_wallpaper;
	@FindBy(xpath = "//*[@id='screen_saver_setting']/footer/div[2]/button")
	private WebElement screen_saver_settingDoneBtn;
	@FindBy(xpath = "//*[@id='time_out_screensaver']")
	private WebElement time_out_screensaver;
	@FindBy(xpath = "//*[@id='screensaver-timeout-popup']/div/div/div[2]/div[3]/input")
	private WebElement InputScreenSaverTimeout;
	@FindBy(xpath = "//*[@id='screensaver-timeout-popup']/div/div/div[3]/button[2]")
	private WebElement OkBtnSS;
	@FindBy(xpath = "//*[@id='minutes_input']")
	private WebElement minutes_input;

	public void ClickOnscreenSaver_sett() throws InterruptedException {

		screenSaver_sett.click();
		waitForXpathPresent("//p[text()='Enable Screensaver']");
		Reporter.log("Clicked on screenSaver_sett", true);
		enable_screen_saver.click();
		waitForVisibilityOf("//p[text()='Check to use system wallpaper as screensaver']");
		Reporter.log("Clicked on enable_screen_saver", true);
		use_sys_wallpaper.click();
		Reporter.log("Clicked on use_sys_wallpaper", true);
		time_out_screensaver.click();
		waitForXpathPresent("(//p[text()='Screensaver Timeout'])[2]");
		Reporter.log("Clicked On time_out_screensaver", true);
		// minutes_input.click();
		// sleep(2);
		// Reporter.log("Clicked On minutes_input",true);
		InputScreenSaverTimeout.clear();
		sleep(2);
		InputScreenSaverTimeout.sendKeys("0");
		sleep(1);
		OkBtnSS.click();
		sleep(2);
		screen_saver_settingDoneBtn.click();
		waitForXpathPresent("//p[text()='Home Screen Settings']");
		Reporter.log("Clicked on screen_saver_settingDoneBtn", true);

	}

	public void ClickOnDisablescreenSaver_sett() throws InterruptedException {

		screenSaver_sett.click();
		waitForXpathPresent("//p[text()='Enable Screensaver']");
		Reporter.log("Clicked on screenSaver_sett", true);
		enable_screen_saver.click();
		waitForVisibilityOf("(//p[text()='Screensaver should be enabled'])[1]");
		Reporter.log("Disabled  enable_screen_saver", true);
		sleep(2);
		screen_saver_settingDoneBtn.click();
		waitForXpathPresent("//p[text()='Home Screen Settings']");
		Reporter.log("Clicked on screen_saver_settingDoneBtn", true);

	}

	public void VerifyDeviceSideTimeOfScreenSaver() {
		boolean InputScreenSaverTimeoutText = Initialization.driverAppium
				.findElementByXPath(
						"//android.widget.TextView[@text='Force Screensaver after 10 �seconds of inactivity']")
				.isDisplayed();
		String pass = "PASS>>'Force Screensaver after 10 �seconds of inactivity' is displayed";
		String fail = "FAIL>>'Force Screensaver after 10 �seconds of inactivity' is NOT displayed";
		ALib.AssertTrueMethod(InputScreenSaverTimeoutText, pass, fail);

	}

	public void WaitTenSecsForScreenSaverToDisplay() throws InterruptedException {

		sleep(12);
	}

	@FindBy(xpath = " //*[@id='use_media']")
	private WebElement use_media;
	@FindBy(xpath = "//*[@id='screensaver-media']/div/div/div[2]/div[1]/input")
	private WebElement screensaver_mediaInput;
	@FindBy(xpath = "//*[@id='screensaver-media']/div/div/div[3]/button[2]")
	private WebElement OkBtnscreensaver_mediaInput;

	public void ClickOnscreenSaver_settImage_Url(String Path) throws InterruptedException {

		screenSaver_sett.click();
		waitForXpathPresent("//p[text()='Enable Screensaver']");
		Reporter.log("Clicked on screenSaver_sett", true);
		enable_screen_saver.click();
		waitForVisibilityOf("//p[text()='Check to use system wallpaper as screensaver']");
		Reporter.log("Clicked on enable_screen_saver", true);
		use_media.click();
		waitForXpathPresent("(//p[contains(text(),'Select Screensaver Media or Webpage')])[2]");
		Reporter.log("Clicked on Select Screensaver Media or Webpage", true);
		screensaver_mediaInput.sendKeys(Path);
		sleep(2);
		OkBtnscreensaver_mediaInput.click();
		Reporter.log("Clicked On OkBtnscreensaver_mediaInput", true);
		sleep(2);
		time_out_screensaver.click();
		waitForXpathPresent("(//p[text()='Screensaver Timeout'])[2]");
		Reporter.log("Clicked On time_out_screensaver", true);
		minutes_input.click();
		sleep(2);
		Reporter.log("Clicked On minutes_input", true);
		InputScreenSaverTimeout.clear();
		sleep(2);
		InputScreenSaverTimeout.sendKeys("0");
		sleep(1);
		OkBtnSS.click();
		sleep(2);
		screen_saver_settingDoneBtn.click();
		waitForXpathPresent("//p[text()='Home Screen Settings']");
		Reporter.log("Clicked on screen_saver_settingDoneBtn", true);

	}

	public void VerifyScreenSaverImageView() throws InterruptedException {

		sleep(12);
		boolean ScreenSaverImageview = Initialization.driverAppium
				.findElementById("com.gears42.surelock:id/screensaver_imageview").isDisplayed();
		String pass = "PASS>> Image is displayed";
		String fail = "FAIL>> Image is Not displayed";
		ALib.AssertTrueMethod(ScreenSaverImageview, pass, fail);

	}

	public void VerifyMediaFileDeviceSide() {

		boolean ScreenSaverImageview = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[text='/storage/emulated/0/image.jpg']").isDisplayed();
		String pass = "PASS>> Media Path is displayed";
		String fail = "FAIL>> Media Path is Not displayed";
		ALib.AssertTrueMethod(ScreenSaverImageview, pass, fail);
	}

	public void DisableScreenSaverFromDevice() {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Enable Screensaver']").click();

	}

	@FindBy(xpath = "//*[@id='go_to_screen']")
	private WebElement go_to_screen;
	@FindBy(xpath = "//*[@id='goto_home_screen']/div/span[1]")
	private WebElement goto_home_screen;
	@FindBy(xpath = "//*[@id='go-to-home-settings-popup']/div/div/div[3]/button[2]")
	private WebElement OkBtngo_to_screen;

	public void ClickOngo_to_screen() throws InterruptedException {
		go_to_screen.click();
		waitForXpathPresent("//p[text()='Go To']");
		sleep(2);
	}

	public void VerifyHomeScreenOptn() throws InterruptedException {

		boolean goto_home_screen_Optn = goto_home_screen.isEnabled();
		String pass = "PASS>> goto_home_screen is enbaled";
		String fail = "FAIL>> goto_home_screen is Not enbaled";
		ALib.AssertTrueMethod(goto_home_screen_Optn, pass, fail);
		sleep(2);
		OkBtngo_to_screen.click();

	}

	public void ClickOnCancel() {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='CANCEL']").click();

	}

	public void LaunchPlayStoreSL() throws IOException {

		Runtime.getRuntime().exec("adb shell monkey -p com.android.vending -c android.intent.category.LAUNCHER 1");
		Reporter.log("Playstore is launched", true);

	}

	public void VerifyDeviceSideClickOnOPEN() throws InterruptedException {
		sleep(10);
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='APPS'
		// and @index='2']").click();
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SEE
		// MORE' and @index='1']").click();
		// sleep(2);
		// Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='YouTube']").click();
		// sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Open']").click();
		Reporter.log("Clicked On OPEN Btn", true);

	}

	@FindBy(xpath = "//*[@id='resume_prev_app']/div/span[1]")
	private WebElement resume_prev_app;
	@FindBy(xpath = "//*[@id='restart_prev_app']/div/span[1]")
	private WebElement restart_prev_app;

	public void Verifyresume_prev_app() throws InterruptedException {

		resume_prev_app.click();
		Reporter.log("Clicked On resume_prev_app", true);
		sleep(2);
		OkBtngo_to_screen.click();

	}

	public void Verifyrestart_prev_app() throws InterruptedException {

		restart_prev_app.click();
		Reporter.log("Clicked On restart_prev_app", true);
		sleep(2);
		OkBtngo_to_screen.click();

	}

	public void PlayStore(String value) throws IOException, InterruptedException {

		WebDriverWait wait = new WebDriverWait(Initialization.driver, 60);
		// WebDriverWait wait1=new WebDriverWait(driver, 300);
		Runtime.getRuntime()
				.exec("adb shell am start -a android.intent.action.VIEW -d 'market://details?id=" + value + "'");
		sleep(4);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
					"//android.widget.Button[contains(@resource-id, '0_resource_name_obfuscated') and @text='Open']")));

		} catch (Throwable e) {
			System.out.println("Open Button visible");
			// Initialization.driver.manage().timeouts().implicitlyWait(20,
			// TimeUnit.SECONDS);

		}
	}

	public void Verifyrestart_prev_appDeviceSide() throws InterruptedException {
		sleep(6);
		boolean AppsIcon = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Apps' and @index='1']").isDisplayed();
		String Pass = "PASS>> APPS icon is visible";
		String Fail = "FAIL>> APPS icon is not visible";
		ALib.AssertTrueMethod(AppsIcon, Pass, Fail);
		sleep(2);
	}

	public void Verifyresume_prev_appDeviceSide() throws InterruptedException {
		sleep(6);
		boolean AppsIcon = Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Open']")
				.isDisplayed();
		String Pass = "PASS>> OPEN icon is visible";
		String Fail = "FAIL>> OPEN icon is not visible";
		ALib.AssertTrueMethod(AppsIcon, Pass, Fail);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='goto_home_screen']/div/span[1]")
	private WebElement Enablegoto_home_screen;

	public void EnableHomeScreen() throws InterruptedException {

		Enablegoto_home_screen.click();
		sleep(2);
		Reporter.log("Clicked on HomeScreen", true);
		OkBtngo_to_screen.click();

	}

	@FindBy(xpath = "//*[@id='ManageShortcuts']")
	private WebElement ManageShortcuts;
	@FindBy(xpath = "//*[@id='accept_shortcuts']")
	private WebElement AllowNewApplicationShortcuts;
	@FindBy(xpath = "//*[@id='shortcuts_List_pg']/footer/div[2]/button[1]")
	private WebElement AddShortCut;

	public void ClickOnManageShortcuts() throws InterruptedException {

		ManageShortcuts.click();
		waitForXpathPresent("//*[@id='shortcuts_List_pg']/div/div[1]/div/span[2]");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='name']")
	private WebElement Shortcutname;
	@FindBy(xpath = "//*[@id='intent']")
	private WebElement Action;
	@FindBy(xpath = "//*[@id='manage_shtcut_pg']/footer/div[2]/button[1]")
	private WebElement manage_shtcut_pgOK;
	@FindBy(xpath = "//*[@id='shortcuts_List_pg']/footer/div[2]/button[3]")
	private WebElement shortcuts_List_pgDONE;

	public void ClickOnAddShortCut(String Name, String ActionName) throws InterruptedException {
		AllowNewApplicationShortcuts.click();
		sleep(2);
		AddShortCut.click();
		waitForXpathPresent("//*[@id='manage_shtcut_pg']/div/div[1]/div/div[1]/div");
		sleep(2);
		Shortcutname.clear();
		sleep(2);
		Shortcutname.sendKeys(Name);
		sleep(2);
		Action.clear();
		sleep(2);
		Action.sendKeys(Name);
		sleep(2);
		manage_shtcut_pgOK.click();
		waitForXpathPresent("//*[@id='shortcuts_List_pg']/div/div[1]/div/span[2]");
		shortcuts_List_pgDONE.click();
	}

	@FindBy(xpath = "//*[@id='MultiUserProfileSett']/div/div[2]")
	private WebElement MultiUserProfileSett;
	@FindBy(xpath = "//*[@id='enable_multiUser_profile']/div/div[2]/span/input")
	private WebElement enable_multiUser_profile;
	@FindBy(xpath = "//*[@id='profileMgr']/div/div[1]")
	private WebElement profileMgr;
	@FindBy(xpath = "//*[@id='profileManagement_pg']/div/ul/li/div/div[2]/span[1]")
	private WebElement profileManagement_pg;
	@FindBy(xpath = "//*[@id='profileManagement_pg']/footer/div[2]/button[2]")
	private WebElement profileManagement_pgDONE;
	@FindBy(xpath = "//*[@id='userMgr']/div/div[1]")
	private WebElement userMgr;
	// @FindBy(xpath="//*[@id='serverConfig']/div/div[1]")
	// private WebElement serverConfig;
	@FindBy(xpath = "//*[@id='multiUser_profile_sett_pg']/footer/div[2]/button")
	private WebElement multiUser_profile_sett_pgDONE;
	@FindBy(xpath = "//*[@id='profileManagement_pg']/footer/div[2]/button[1]")
	private WebElement profileManagement_pgOKBtn;
	@FindBy(xpath = "//*[@id='profileNmeInput']")
	private WebElement profileNmeInput;
	@FindBy(xpath = "//*[@id='add_profileNme_popup']/div/div/div[3]/button[2][text()='OK']")
	private WebElement add_profileNme_popup;
	@FindBy(xpath = "//*[@id='userMgr_pg']/footer/div[2]/button[1][text()='Add']")
	private WebElement userMgr_pgADDbtn;

	@FindBy(xpath = "//*[@id='userData_userName']")
	private WebElement userData_userName;
	@FindBy(xpath = "//*[@id='userData_password']")
	private WebElement userData_password;
	@FindBy(xpath = "//*[@id='userData_password2']")
	private WebElement userData_password2;
	@FindBy(xpath = "//*[@id='userData_profiles']")
	private WebElement userData_profiles;
	@FindBy(xpath = "//*[@id='userData_profiles']/option[1][text()='Default_Profile']")
	private WebElement Default_Profile;
	@FindBy(xpath = "//*[@id='userData_profiles']/option[2][text()='ProfileA']")
	private WebElement ProfileA;
	@FindBy(xpath = "//*[@id='userData_profiles']/option[3][text()='ProfileB']")
	private WebElement ProfileB;
	@FindBy(xpath = "//*[@id='userData_profiles']/option[4][text()='ProfileAA']")
	private WebElement ProfileAA;
	@FindBy(xpath = "//*[@id='add_userDetails_popup']/div/div/div[3]/button[2][text()='OK']")
	private WebElement add_userDetails_popup;
	@FindBy(xpath = "//*[@id='userMgr_pg']/footer/div[2]/button[2][text()='Done']")
	private WebElement userMgr_pgDONEbtn;

	@FindBy(xpath = "//*[@id='display_loggedin_user']/div/div[2]/span/input")
	private WebElement display_loggedin_user;

	public void ClickOnMultiUserProfileSett() throws InterruptedException {

		MultiUserProfileSett.click();
		waitForXpathPresent("//*[@id='enable_multiUser_profile']/div/div[1]");
		sleep(2);
		Reporter.log("Clicked on MultiUserProfileSett", true);

		enable_multiUser_profile.click();
		sleep(2);
		profileMgr.click();
		sleep(2);
		boolean DefaultProfile = profileManagement_pg.isDisplayed();
		String pass = "PASS>> Profile Management is ACTIVE";
		String fail = "FAIL>>Profile Management is Not ACTIVE";
		ALib.AssertTrueMethod(DefaultProfile, pass, fail);
		sleep(2);
		profileManagement_pgDONE.click();
		sleep(2);
		boolean MultiUseruserMgr = userMgr.isDisplayed();
		String pass1 = "PASS>> userMgr Management is ACTIVE";
		String fail1 = "FAIL>> userMgr Management is Not ACTIVE";
		ALib.AssertTrueMethod(MultiUseruserMgr, pass1, fail1);
		sleep(2);
		boolean MultiUseserverConfig = serverConfig.isDisplayed();
		String pass2 = "PASS>> Profile Management is ACTIVE";
		String fail2 = "FAIL>>Profile Management is Not ACTIVE";
		ALib.AssertTrueMethod(MultiUseserverConfig, pass2, fail2);
		sleep(2);

		profileMgr.click();
		sleep(2);
		profileManagement_pgOKBtn.click();
		waitForXpathPresent("//p[contains(text(),'Profile Name')]");
		sleep(2);
		profileNmeInput.clear();
		sleep(2);
		profileNmeInput.sendKeys("ProfileA");
		sleep(1);
		add_profileNme_popup.click();
		sleep(2);
		Reporter.log("ProfileA is created", true);
		profileManagement_pgOKBtn.click();
		waitForXpathPresent("//p[contains(text(),'Profile Name')]");
		sleep(2);
		profileNmeInput.clear();
		sleep(2);
		profileNmeInput.sendKeys("ProfileB");
		sleep(1);
		add_profileNme_popup.click();
		sleep(2);
		Reporter.log("ProfileB is created", true);

		profileManagement_pgDONE.click();
		sleep(2);
		multiUser_profile_sett_pgDONE.click();
		waitForXpathPresent("//p[text()='Allowed Applications']");
		sleep(2);

	}

	public void CreateUsers() throws InterruptedException {

		MultiUserProfileSett.click();
		waitForXpathPresent("//*[@id='enable_multiUser_profile']/div/div[1]");
		sleep(2);
		Reporter.log("Clicked on MultiUserProfileSett", true);

		userMgr.click();
		waitForXpathPresent("//*[@id='userMgr_pg']/footer/div[2]/button[1][text()='Add']");
		Reporter.log("Clicked on userMgr", true);

		userMgr_pgADDbtn.click();
		waitForXpathPresent("//p[contains(text(),'Add User')]");
		userData_userName.clear();
		sleep(1);
		userData_userName.sendKeys("UserA");
		sleep(1);
		userData_password.clear();
		sleep(1);
		userData_password.sendKeys("0");
		sleep(1);
		userData_password2.clear();
		sleep(1);
		userData_password2.sendKeys("0");
		sleep(1);
		userData_profiles.click();
		waitForXpathPresent("//*[@id='userData_profiles']/option[1][text()='Default_Profile']");
		sleep(1);
		Default_Profile.click();
		sleep(2);
		add_userDetails_popup.click();
		sleep(2);
		Reporter.log("UserA is created", true);

		userMgr_pgADDbtn.click();
		waitForXpathPresent("//p[contains(text(),'Add User')]");
		userData_userName.clear();
		sleep(1);
		userData_userName.sendKeys("UserB");
		sleep(1);
		userData_password.clear();
		sleep(1);
		userData_password.sendKeys("0");
		sleep(1);
		userData_password2.clear();
		sleep(1);
		userData_password2.sendKeys("0");
		sleep(1);
		userData_profiles.click();
		waitForXpathPresent("//*[@id='userData_profiles']/option[2][text()='ProfileA']");
		sleep(1);
		ProfileA.click();
		sleep(2);
		add_userDetails_popup.click();
		sleep(2);
		Reporter.log("UserB is created", true);

		userMgr_pgADDbtn.click();
		waitForXpathPresent("//p[contains(text(),'Add User')]");
		userData_userName.clear();
		sleep(1);
		userData_userName.sendKeys("UserC");
		sleep(1);
		userData_password.clear();
		sleep(1);
		userData_password.sendKeys("0");
		sleep(1);
		userData_password2.clear();
		sleep(1);
		userData_password2.sendKeys("0");
		sleep(1);
		userData_profiles.click();
		waitForXpathPresent("//*[@id='userData_profiles']/option[3][text()='ProfileB']");
		sleep(1);
		ProfileB.click();
		sleep(2);
		add_userDetails_popup.click();
		sleep(2);
		Reporter.log("UserC is created", true);

		userMgr_pgDONEbtn.click();
		sleep(2);
		display_loggedin_user.click();
		sleep(2);

		multiUser_profile_sett_pgDONE.click();
		waitForXpathPresent("//p[text()='Allowed Applications']");
		sleep(2);

	}

	public void CREATE_CLONE() throws InterruptedException {

		MultiUserProfileSett.click();
		waitForXpathPresent("//*[@id='enable_multiUser_profile']/div/div[1]");
		sleep(2);
		Reporter.log("Clicked on MultiUserProfileSett", true);
		profileMgr.click();
		sleep(2);
		profileManagement_pgProfileA.click();
		sleep(2);
		profileManagement_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Clone']");
		Clone.click();
		waitForXpathPresent("//*[@id='add_profileNme_popup']/div/div/div[1]/p[text()='Profile Name']");
		Reporter.log("Clicked On CLONE", true);
		profileNmeInput.clear();
		sleep(2);
		profileNmeInput.sendKeys("ProfileAA");
		sleep(1);
		add_profileNme_popup.click();
		sleep(2);
		Reporter.log("ProfileAA is created", true);

		profileManagement_pgDONE.click();
		sleep(2);
		userMgr.click();
		waitForXpathPresent("//*[@id='userMgr_pg']/footer/div[2]/button[1][text()='Add']");
		Reporter.log("Clicked on userMgr", true);

		userMgr_pgADDbtn.click();
		waitForXpathPresent("//p[contains(text(),'Add User')]");
		userData_userName.clear();
		sleep(1);
		userData_userName.sendKeys("UserD");
		sleep(1);
		userData_password.clear();
		sleep(1);
		userData_password.sendKeys("0");
		sleep(1);
		userData_password2.clear();
		sleep(1);
		userData_password2.sendKeys("0");
		sleep(1);
		userData_profiles.click();
		waitForXpathPresent("//*[@id='userData_profiles']/option[4][text()='ProfileAA']");
		sleep(1);
		ProfileAA.click();
		sleep(2);
		add_userDetails_popup.click();
		sleep(2);
		Reporter.log("UserD is created", true);

		userMgr_pgDONEbtn.click();
		sleep(2);

		multiUser_profile_sett_pgDONE.click();
		waitForXpathPresent("//p[text()='Allowed Applications']");
		sleep(2);

	}

	public void TapOnMultiUserProfileSettings(String SLsettings) {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='" + SLsettings + "']").click();

	}

	@FindBy(xpath = "//*[@id='mup_profiles_select']/input")
	private WebElement mup_profiles_select;
	@FindBy(xpath = "//*[@id='profile_list']/li[2]/span[text()='ProfileA']")
	private WebElement profile_listUserA;
	@FindBy(xpath = "//*[@id='profile_list']/li[3]/span[text()='ProfileB']")
	private WebElement profile_listUserB;

	public void ClickOnmup_profiles_selectProfileA() throws InterruptedException {

		mup_profiles_select.click();
		sleep(2);
		profile_listUserA.click();
		sleep(2);
		Reporter.log("ProfileA is selected", true);
	}

	public void ClickOnmup_profiles_selectProfileB() throws InterruptedException {

		mup_profiles_select.click();
		sleep(2);
		profile_listUserB.click();
		sleep(2);
		Reporter.log("ProfileB is selected", true);

	}

	@FindBy(xpath = "//*[@id='profile_list']/li[3]/span[text()='EditedProfileB']")
	private WebElement Edited_ProfileB;

	public void ClickOnmup_profiles_selectEditedProfileB() throws InterruptedException {

		mup_profiles_select.click();
		sleep(2);
		Edited_ProfileB.click();
		sleep(2);
		Reporter.log("Edited_ProfileB is selected", true);

	}

	@FindBy(xpath = "//*[@id='surelock_bgColor']/div/div[2]/span/input")
	private WebElement surelock_bgColor;
	@FindBy(xpath = "//*[@id='pickedColourInput']")
	private WebElement pickedColourInput;
	@FindBy(xpath = "//*[@id='colourPicker_popup']/div/div/div[3]/button[2]")
	private WebElement colourPicker_popupOKbtn;

	public void ClickOnsurelock_bgColor(String Color) throws InterruptedException {

		surelock_bgColor.click();
		waitForXpathPresent("//*[@id='colourPicker_popup']/div/div/div[1]/p");
		pickedColourInput.clear();
		sleep(2);
		pickedColourInput.sendKeys(Color);
		sleep(2);
		colourPicker_popupOKbtn.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='profileManagement_pg']/div/ul/li[2]/div/div[2]/span[2]/input")
	private WebElement profileManagement_pgProfileA;
	@FindBy(xpath = "//*[@id='profileManagement_pg']/div/ul/li[3]/div/div[2]/span[2]/input")
	private WebElement profileManagement_pgProfileB;
	@FindBy(xpath = "//*[@id='profileManagement_pg']/div/ul/li[4]/div/div[2]/span[2]/input")
	private WebElement profileManagement_pgCLONEProfileAA;
	@FindBy(xpath = "//*[@id='profileManagement_pg']/header/span[3]/ul/li[1]/a[text()='Activate']")
	private WebElement Activate;
	@FindBy(xpath = "(//span[text()='Active'])[2]")
	private WebElement Active;
	@FindBy(xpath = "//*[@id='profileManagement_pg']/header/span[3]/button/i")
	private WebElement profileManagement_pgMoreBtn;
	@FindBy(xpath = "//a[text()='Clone']")
	private WebElement Clone;
	@FindBy(xpath = "//a[text()='Select All']")
	private WebElement SelectAll;
	@FindBy(xpath = "//a[text()='Unselect All']")
	private WebElement UnselectAll;

	public void ClickOnProfiles() throws InterruptedException {
		MultiUserProfileSett.click();
		waitForXpathPresent("//*[@id='enable_multiUser_profile']/div/div[1]");
		sleep(2);
		Reporter.log("Clicked on MultiUserProfileSett", true);
		profileMgr.click();
		sleep(2);
		Reporter.log("Clicked On profileMgr", true);

		profileManagement_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Clone']");
		SelectAll.click();
		sleep(2);
		Reporter.log("Cliked On Select All", true);
		boolean CheckEnableA = profileManagement_pgProfileA.isEnabled();
		String pass = "PASS>> profileManagement_pgUserA is Enabled";
		String fail = "FAIL>> profileManagement_pgUserA is not Enabled";
		ALib.AssertTrueMethod(CheckEnableA, pass, fail);
		sleep(2);
		boolean CheckEnableB = profileManagement_pgProfileB.isEnabled();
		String pass1 = "PASS>> profileManagement_pgUserA is Enabled";
		String fail1 = "FAIL>> profileManagement_pgUserA is not Enabled";
		ALib.AssertTrueMethod(CheckEnableB, pass1, fail1);
		sleep(2);
		boolean CheckEnableAA = profileManagement_pgCLONEProfileAA.isEnabled();
		String pass2 = "PASS>> profileManagement_pgUserA is Enabled";
		String fail2 = "FAIL>> profileManagement_pgUserA is not Enabled";
		ALib.AssertTrueMethod(CheckEnableAA, pass2, fail2);
		sleep(2);
		profileManagement_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Clone']");
		UnselectAll.click();
		sleep(2);
		Reporter.log("Cliked On UnselectAll ", true);

		profileManagement_pgProfileA.click();
		sleep(2);
		profileManagement_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Clone']");
		Activate.click();
		Reporter.log("Clicked On Activate", true);
		sleep(2);

		boolean Activate = Active.isEnabled();
		String pass3 = "PASS>> Active is Displayed";
		String fail3 = "FAIL>> Active is Not Displayed";
		ALib.AssertTrueMethod(Activate, pass3, fail3);
		sleep(2);

		profileManagement_pgDONE.click();
		sleep(2);

		multiUser_profile_sett_pgDONE.click();
		waitForXpathPresent("//p[text()='Allowed Applications']");
		sleep(2);

	}

	// DEFAULT Profile

	public void VerifyDeviceDEFAULT_UserA(String User, String pwd) throws InterruptedException, IOException {
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, Config.IMP_WAIT);
		WebElement PwdTextField = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.gears42.surelock:id/password")));

		Initialization.driverAppium.findElementById("com.gears42.surelock:id/username").clear();
		sleep(2);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/username").sendKeys(User);
		Reporter.log("Entered User Name", true);
		sleep(2);
		ClickOnBackButtonInDeviceSide();
		sleep(4);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/password").click();
		sleep(2);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/password").clear();
		sleep(2);
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/password").sendKeys(pwd);
		Reporter.log("Entered Pwd", true);
		sleep(2);
		ClickOnBackButtonInDeviceSide();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='LOGIN']").click();
		Reporter.log("Login ", true);
		sleep(2);

	}

	public void WaitForUN_PWD() {

		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, Config.IMP_WAIT);
		WebElement PwdTextField = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.gears42.surelock:id/password")));

	}

	public void VerifyWaitTillSLscreenISdisplayed(String APPname) throws InterruptedException {

		boolean LoginPrompt = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Login is in progress']").isDisplayed();
		String pass = "PASS>> 'Login is in progress' is dispalyed";
		String fail = "FAIL>> 'Login is in progress' is Not dispalyed";
		ALib.AssertTrueMethod(LoginPrompt, pass, fail);
		sleep(2);

		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, Config.IMP_WAIT);
		WebElement PwdTextField = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='" + APPname + "']")));

		Reporter.log("APP is displayed,Logged in to User", true);

	}

	public void WaitForLogOutPrompt() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, Config.IMP_WAIT);
		WebElement WaitForLogOutPrompt = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//android.widget.TextView[@text='Are you sure you want to log out?']")));

		boolean LogoutPrompt = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Are you sure you want to log out?']")
				.isDisplayed();
		String pass = "PASS>> 'Are you sure you want to log out?' is dispalyed";
		String fail = "FAIL>> 'Are you sure you want to log out?' is Not dispalyed";
		ALib.AssertTrueMethod(LogoutPrompt, pass, fail);
		sleep(2);

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Log out']").click();

		boolean LogoutProgress = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Logout is in progress']").isDisplayed();
		String pass1 = "PASS>> 'Logout is in progress' is dispalyed";
		String fail1 = "FAIL>> 'Logout is in progress' is Not dispalyed";
		ALib.AssertTrueMethod(LogoutProgress, pass1, fail1);
		sleep(2);

		WebElement PwdTextField = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.gears42.surelock:id/password")));

		sleep(20);

		boolean Username = Initialization.driverAppium.findElementById("com.gears42.surelock:id/username")
				.isDisplayed();
		String pass2 = "PASS>> 'Username' is dispalyed";
		String fail2 = "FAIL>> 'Username' is Not dispalyed";
		ALib.AssertTrueMethod(Username, pass2, fail2);
		sleep(2);

	}

	public void VerifyActiveStatus() throws InterruptedException {
		boolean Active = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Active']")
				.isDisplayed();
		String pass2 = "PASS>> Active is Displayed";
		String fail2 = "FAIL>> Active is Not Displayed";
		ALib.AssertTrueMethod(Active, pass2, fail2);
		sleep(2);
		boolean ProfileA = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='ProfileA']")
				.isDisplayed();
		String pass3 = "PASS>> ProfileA is Displayed";
		String fail3 = "FAIL>> ProfileA is Not Displayed";
		ALib.AssertTrueMethod(ProfileA, pass3, fail3);
		sleep(2);
		boolean ProfileB = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='ProfileB']")
				.isDisplayed();
		String pass4 = "PASS>> ProfileB is Displayed";
		String fail4 = "FAIL>> ProfileB is Not Displayed";
		ALib.AssertTrueMethod(ProfileB, pass4, fail4);
		sleep(2);
		boolean ProfileAA = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='ProfileAA']").isDisplayed();
		String pass5 = "PASS>> ProfileAA is Displayed";
		String fail5 = "FAIL>> ProfileAA is Not Displayed";
		ALib.AssertTrueMethod(ProfileAA, pass5, fail5);
		sleep(2);

	}

	public void VerifyUsers() throws InterruptedException {
		boolean UserA = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='UserA']")
				.isDisplayed();
		String pass2 = "PASS>> UserA is Displayed";
		String fail2 = "FAIL>> UserA is Not Displayed";
		ALib.AssertTrueMethod(UserA, pass2, fail2);
		sleep(2);
		boolean UserB = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='UserB']")
				.isDisplayed();
		String pass3 = "PASS>> UserB is Displayed";
		String fail3 = "FAIL>> UserB is Not Displayed";
		ALib.AssertTrueMethod(UserB, pass3, fail3);
		sleep(2);
		boolean UserC = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='UserC']")
				.isDisplayed();
		String pass4 = "PASS>> UserC is Displayed";
		String fail4 = "FAIL>> UserC is Not Displayed";
		ALib.AssertTrueMethod(UserC, pass4, fail4);
		sleep(2);
		boolean UserD = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='UserD']")
				.isDisplayed();
		String pass5 = "PASS>> UserD is Displayed";
		String fail5 = "FAIL>> UserD is Not Displayed";
		ALib.AssertTrueMethod(UserD, pass5, fail5);
		sleep(2);

	}

	@FindBy(xpath = "//a[text()='Delete']")
	private WebElement Delete;
	@FindBy(xpath = "//a[text()='Edit']")
	private WebElement EditProfile;
	@FindBy(xpath = "//*[@id='delete_profile_popup']/div/div/div[3]/button[2]")
	private WebElement delete_profile_popup;

	public void EditandDeleteProfile() throws InterruptedException {

		MultiUserProfileSett.click();
		waitForXpathPresent("//*[@id='enable_multiUser_profile']/div/div[1]");
		sleep(2);
		Reporter.log("Clicked on MultiUserProfileSett", true);

		profileMgr.click();
		sleep(2);

		// profileManagement_pgProfileA.click();
		// sleep(2);
		// profileManagement_pgProfileB.click();
		// sleep(2);

		profileManagement_pgCLONEProfileAA.click();
		sleep(2);

		profileManagement_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Delete']");

		Delete.click();
		waitForXpathPresent("//p[text()='Are you sure you want to delete the selected profile?']");
		delete_profile_popup.click();
		sleep(2);

		// profileManagement_pgProfileA.click();
		profileManagement_pgProfileB.click();
		sleep(2);
		profileManagement_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Edit']");

		EditProfile.click();
		waitForXpathPresent("//p[contains(text(),'Profile Name')]");
		sleep(2);
		profileNmeInput.clear();
		sleep(2);
		profileNmeInput.sendKeys("EditedProfileB");
		sleep(1);
		add_profileNme_popup.click();
		sleep(2);
		Reporter.log("EditedProfileB is created", true);
		profileManagement_pgDONE.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='userMgr_pg']/div/ul/li[2]/div/div[3]/span/input")
	private WebElement userMgr_pgUserA;
	@FindBy(xpath = "//*[@id='userMgr_pg']/div/ul/li[3]/div/div[3]/span/input")
	private WebElement userMgr_pgUserB;
	@FindBy(xpath = "//*[@id='userMgr_pg']/div/ul/li[2]/div/div[3]/span/input")
	private WebElement userMgr_pgUserC;
	@FindBy(xpath = "//*[@id='userMgr_pg']/header/span[3]/button/i")
	private WebElement userMgr_pgMoreBtn;
	@FindBy(xpath = "//*[@id='userData_profiles']/option[3][text()='EditedProfileB']")
	private WebElement EditedProfileB;
	@FindBy(xpath = "//*[@id='delete_user_popup']/div/div/div[3]/button[2]")
	private WebElement delete_user_popup;

	public void EditandDeleteUserManagement() throws InterruptedException {

		userMgr.click();
		waitForXpathPresent("//*[@id='userMgr_pg']/footer/div[2]/button[1][text()='Add']");
		Reporter.log("Clicked on userMgr", true);
		userMgr_pgUserA.click();
		sleep(2);
		Reporter.log("Clicked UserA", true);
		userMgr_pgUserB.click();
		sleep(2);
		Reporter.log("Clicked UseB", true);
		userMgr_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Delete']");
		Reporter.log("Clicked On UserMgr_MoreBtn", true);
		Delete.click();
		waitForXpathPresent("//p[text()='Are you sure you want to delete the selected users?']");
		delete_user_popup.click();
		sleep(2);
		userMgr_pgUserC.click();
		sleep(2);
		Reporter.log("Clicked UserC", true);
		userMgr_pgMoreBtn.click();
		waitForXpathPresent("//a[text()='Delete']");
		Reporter.log("Clicked On UserMgr_MoreBtn", true);
		EditProfile.click();
		waitForXpathPresent("//p[contains(text(),'Add User')]");
		sleep(2);
		userData_userName.clear();
		sleep(1);
		userData_userName.sendKeys("EditedUserC");
		sleep(1);
		userData_password.clear();
		sleep(1);
		userData_password.sendKeys("0");
		sleep(1);
		userData_password2.clear();
		sleep(1);
		userData_password2.sendKeys("0");
		sleep(1);
		userData_profiles.click();
		waitForXpathPresent("//*[@id='userData_profiles']/option[3][text()='EditedProfileB']");
		sleep(1);
		EditedProfileB.click();
		sleep(2);
		add_userDetails_popup.click();
		sleep(2);
		Reporter.log("EditedUserC is created", true);
		userMgr_pgDONEbtn.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='serverConfig']/div/div[1]")
	private WebElement serverConfig;
	@FindBy(xpath = "//*[@id='server_path']")
	private WebElement server_path;
	@FindBy(xpath = "//*[@id='port_number']")
	private WebElement port_number;
	@FindBy(xpath = "//*[@id='distin_nme']")
	private WebElement distin_nme;
	@FindBy(xpath = "//*[@id='metaTag_key']")
	private WebElement metaTag_key;
	@FindBy(xpath = "//*[@id='metaTag_checkbox']")
	private WebElement metaTag_checkbox;
	@FindBy(xpath = "//*[@id='serverConfig_pg']/footer/div[2]/button[2][text()='Save']")
	private WebElement serverConfig_pg;

	public void ClickOnServerDetails() throws InterruptedException {

		serverConfig.click();
		waitForXpathPresent("//*[@id='server_path']");
		Reporter.log("Clicked On serverConfig", true);

		server_path.clear();
		sleep(2);
		server_path.sendKeys("52.88.140.72");
		sleep(2);
		Reporter.log("Entered server_path", true);

		port_number.clear();
		sleep(2);
		port_number.sendKeys("389");
		sleep(2);
		Reporter.log("Entered port_number", true);

		distin_nme.clear();
		sleep(2);
		distin_nme.sendKeys("cn=<username>,ou=People,dc=us-west-2,dc=compute,dc=internal");
		sleep(2);
		Reporter.log("Entered distin_nme", true);

		metaTag_key.clear();
		sleep(2);
		metaTag_key.sendKeys("cn");
		sleep(2);
		Reporter.log("Entered metaTag_key", true);

		metaTag_checkbox.click();
		sleep(2);
		Reporter.log("Clicked On metaTag_checkbox", true);

		serverConfig_pg.click();
		sleep(2);
		Reporter.log("Clicked On Save Btn", true);

		multiUser_profile_sett_pgDONE.click();
		waitForXpathPresent("//p[text()='Allowed Applications']");
		sleep(2);

	}

	public void VerifyAPPSforEditedProfile(String APPS) throws InterruptedException {

		boolean AllowedApps = Initialization.driver.findElement(By.xpath("//p[text()='" + APPS + "']")).isDisplayed();
		String pass5 = "PASS>> " + APPS + " is Displayed";
		String fail5 = "FAIL>> " + APPS + " is Not Displayed";
		ALib.AssertTrueMethod(AllowedApps, pass5, fail5);
		sleep(2);

	}

	public void VerifyEditedProfileUserMgr(String EditedProfileUser) throws InterruptedException {

		boolean ProfileUserMgr = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='" + EditedProfileUser + "']").isDisplayed();
		String pass5 = "PASS>> " + EditedProfileUser + " is Displayed";
		String fail5 = "FAIL>> " + EditedProfileUser + " is Not Displayed";
		ALib.AssertTrueMethod(ProfileUserMgr, pass5, fail5);
		sleep(2);

	}

	public void VerifyServerConfigDevice(String ServerConfigText) throws InterruptedException {

		boolean ServerConfig = Initialization.driverAppium
				.findElementByXPath("//android.widget.EditText[@text='" + ServerConfigText + "']").isDisplayed();
		String pass5 = "PASS>> " + ServerConfigText + " is Displayed";
		String fail5 = "FAIL>> " + ServerConfigText + " is Not Displayed";
		ALib.AssertTrueMethod(ServerConfig, pass5, fail5);
		sleep(2);

	}

	public void ClickOnValidate(String Un, String Pwd) throws InterruptedException, IOException {

		Initialization.driverAppium.findElementById("com.gears42.surelock:id/multiuser_save_btn").click();
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, Config.IMP_WAIT);
		WebElement WaitForUN_Pwd = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Validate server details']")));

		Reporter.log("Clicked On Validate", true);

		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='Enter username']").clear();

		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='Enter username']")
				.sendKeys(Un);
		sleep(2);
		Reporter.log("Entered Un", true);

		Initialization.driverAppium.findElementById("com.gears42.surelock:id/profilePasswordValue").clear();
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/profilePasswordValue").sendKeys(Pwd);
		sleep(2);
		Reporter.log("Entered Pwd", true);

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK']").click();

		WebElement WaitForSaveBtn = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@text='Save']")));

		Reporter.log("Cliked On Ok Btn", true);

		ClickOnBackButtonInDeviceSide();

		boolean AuthSuccessMessage = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Authentication Successful']").isDisplayed();
		String Pass = "PASS>> 'Authentication Successful' is displayed";
		String Fail = "FAIL>> 'Authentication Successful' is Not displayed";
		ALib.AssertTrueMethod(AuthSuccessMessage, Pass, Fail);
		sleep(2);

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Save']").click();
		sleep(2);
		Reporter.log("Cliked On Save Btn", true);

	}

	public void InvalidUn_PwdServerConfig(String Un, String Pwd) throws IOException, InterruptedException {

		Initialization.driverAppium.findElementById("com.gears42.surelock:id/multiuser_save_btn").click();
		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, Config.IMP_WAIT);
		WebElement WaitForUN_Pwd = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Validate server details']")));

		Reporter.log("Clicked On Validate", true);

		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='Enter username']").clear();

		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='Enter username']")
				.sendKeys(Un);
		sleep(2);
		Reporter.log("Entered Un", true);

		Initialization.driverAppium.findElementById("com.gears42.surelock:id/profilePasswordValue").clear();
		Initialization.driverAppium.findElementById("com.gears42.surelock:id/profilePasswordValue").sendKeys(Pwd);
		sleep(2);
		Reporter.log("Entered Pwd", true);

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK']").click();

		WebElement WaitForSaveBtn = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@text='Save']")));

		Reporter.log("Cliked On Ok Btn", true);

		ClickOnBackButtonInDeviceSide();

		boolean AuthSuccessMessage = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Server is accessible.But authentication failed']")
				.isDisplayed();
		String Pass = "PASS>> 'Server is accessible.But authentication failed' is displayed";
		String Fail = "FAIL>> 'Server is accessible.But authentication failed' is Not displayed";
		ALib.AssertTrueMethod(AuthSuccessMessage, Pass, Fail);
		sleep(2);

	}

	public void DisableMultiUserProfile() throws InterruptedException {
		MultiUserProfileSett.click();
		waitForXpathPresent("//*[@id='enable_multiUser_profile']/div/div[1]");
		sleep(2);
		Reporter.log("Clicked on MultiUserProfileSett", true);
		enable_multiUser_profile.click();
		sleep(2);

		Reporter.log("Cliked On EnableMultiProfile and disabling", true);

		multiUser_profile_sett_pgDONE.click();
		waitForXpathPresent("//p[text()='Allowed Applications']");
		sleep(2);

	}

	public void DeactivateSLDevice() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='YES']").click();

		WebDriverWait wait = new WebDriverWait(Initialization.driverAppium, Config.IMP_WAIT);
		WebElement WaitForUN_Pwd = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Deactivation Successful']")));

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK']").click();
		sleep(2);
		boolean DeactivateMessage = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Product not licensed']").isDisplayed();
		String Pass = "PASS>> 'Product not licensed' is displayed";
		String Fail = "FAIL>> 'Product not licensed' is Not displayed";
		ALib.AssertTrueMethod(DeactivateMessage, Pass, Fail);
		sleep(2);

		tapOnDoneBtn2();
		tapOnDoneBtn2();

	}

	@FindBy(xpath = "//*[@id='AboutSureLock']/div/div[2]")
	private WebElement AboutSureLock;
	@FindBy(xpath = "//*[@id='preferred_activation_id']/div/div")
	private WebElement preferred_activation_id;
	@FindBy(xpath = "(//*[@id='preferred-activation-id-popup']/div/div/div[2]/div/div/span/input)[2]")
	private WebElement preferred_activation_id_popup;
	@FindBy(xpath = "//*[@id='preferred-activation-id-popup']/div/div/div[3]/button[2]")
	private WebElement preferred_activation_id_popupDoneBtn;
	@FindBy(xpath = "(//*[@id='activation_code'])[1]")
	private WebElement activation_code;
	@FindBy(xpath = "//*[@id='about_surelock']/footer/div[2]/button[text()='Done']")
	private WebElement about_surelockDoneBTn;

	public void VerifyAboutSL_ConsoleSide_IMEI(String activationcode) throws InterruptedException {

		AboutSureLock.click();
		waitForXpathPresent("//div[text()='Activation Code']");
		sleep(2);
		Reporter.log("Clicked On AboutSureLock", true);

		preferred_activation_id.click();
		waitForXpathPresent(
				"//*[@id='preferred-activation-id-popup']/div/div/div[1]/p[text()='Preferred Activation ID']");
		sleep(2);
		Reporter.log("Clicked On preferred_activation_id", true);

		preferred_activation_id_popup.click();
		sleep(2);
		Reporter.log("IMEI is selected", true);

		preferred_activation_id_popupDoneBtn.click();
		waitForXpathPresent("//div[text()='Activation Code']");

		activation_code.clear();
		sleep(2);
		activation_code.sendKeys(activationcode);
		sleep(2);
		about_surelockDoneBTn.click();
		sleep(2);
	}

	public void ClickOnAboutSL() throws InterruptedException {

		AboutSureLock.click();
		waitForXpathPresent("//div[text()='Activation Code']");
		sleep(2);
		Reporter.log("Clicked On AboutSureLock", true);

	}

	@FindBy(xpath = "(//span[contains(text(),'This setting is not applicable for UEM Nix Agent')])[1]")
	private WebElement NoteSLActivationCode;
	@FindBy(xpath = "(//span[contains(text(),'This setting is not applicable for UEM Nix Agent')])[2]")
	private WebElement NoteSLPreferredActivationID;

	public void VerifyUIofSL_NoteAboutSL() throws InterruptedException {

		boolean NoteOfSLActivationCode = NoteSLActivationCode.isDisplayed();
		String pass = "PASS>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		String fail = "FAIL>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		ALib.AssertTrueMethod(NoteOfSLActivationCode, pass, fail);
		sleep(2);

		boolean NoteOfSLPreferredActivationID = NoteSLPreferredActivationID.isDisplayed();
		String pass1 = "PASS>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		String fail1 = "FAIL>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		ALib.AssertTrueMethod(NoteOfSLPreferredActivationID, pass1, fail1);
		sleep(2);

		about_surelockDoneBTn.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='ImportExportSetting']/div/div[2]")
	private WebElement ImportExportSetting;

	public void ClickONImportExportSetting() throws InterruptedException {

		ImportExportSetting.click();
		waitForXpathPresent("//p[contains(text(),'Export Settings')]");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='surelocksettingsversionnumber']/div/div/p[text()='SureLock Setting identifier']")
	private WebElement surelocksettingsversionnumber;

	public void SureLockSettingIdentifier() throws InterruptedException {

		JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
		je.executeScript("arguments[0].scrollIntoView()", surelocksettingsversionnumber);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='forceActivateLicense']/p/span[contains(text(),'This setting is not applicable for UEM Nix Agent')]")
	private WebElement forceActivateLicense;
	@FindBy(xpath = "(//*[@id='UEMNixAdvanceNote']/span[contains(text(),'This setting is not applicable for UEM Nix Agent')])[1]")
	private WebElement UEMNixAdvanceNote;
	@FindBy(xpath = "//*[@id='im_ex_sett_pg']/footer/div[2]/button")
	private WebElement DoneImportExportSL;

	public void VerifyUIofSL_ImportExportSL() throws InterruptedException {

		boolean UEMNixAdvanceNoteSL = UEMNixAdvanceNote.isDisplayed();
		String pass = "PASS>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		String fail = "FAIL>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		ALib.AssertTrueMethod(UEMNixAdvanceNoteSL, pass, fail);
		sleep(2);
		SureLockSettingIdentifier();
		boolean forceActivateLicenseSL = forceActivateLicense.isDisplayed();
		String pass1 = "PASS>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		String fail1 = "FAIL>> 'This setting is not applicable for UEM Nix Agent' is displayed";
		ALib.AssertTrueMethod(forceActivateLicenseSL, pass1, fail1);
		sleep(2);

		DoneImportExportSL.click();
		sleep(2);

	}
	// "+"\""+Config.TagNameDevicePlatFormName+"\")])[1]

	@FindBy(xpath = "//*[@id='sureLockAdvanceOptionNote']/span[2]")
	private WebElement sureLockAdvanceOptionNote;

	@FindBy(xpath = "//*[@id='AdanvcedOptionModal']/div/div/div[1]/button")
	private WebElement AdanvcedOptionModalSLClose;

	public void VerifyUIOfNotesAdvancedOptnSL() throws InterruptedException {

		String VerifyUIOfNotesAdvancedOptnSL = sureLockAdvanceOptionNote.getText();
		System.out.println(VerifyUIOfNotesAdvancedOptnSL);
		String Expected = VerifyUIOfNotesAdvancedOptnSL;
		String pass = "PASS>> ''�Install If Not Installed� and �Activation Code� are not applicable for UEM Nix Agent.'' is displayed";
		String fail = "FAIL>> ''�Install If Not Installed� and �Activation Code� are not applicable for UEM Nix Agent.'' is displayed";
		ALib.AssertEqualsMethod(Expected, VerifyUIOfNotesAdvancedOptnSL, pass, fail);
		sleep(2);

		AdanvcedOptionModalSLClose.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='sureLock_homePage_header']/button")
	private WebElement CloseSL;

	public void CloseSLStaticJob() throws InterruptedException {

		CloseSL.click();
		sleep(2);
	}

	@FindBy(xpath = "(//*[@id='preferred-activation-id-popup']/div/div/div[2]/div/div/span/input)[3]")
	private WebElement preferred_activation_id_popup_IMSI;

	public void VerifyAboutSL_ConsoleSide_IMSI(String activationcode) throws InterruptedException {

		AboutSureLock.click();
		waitForXpathPresent("//div[text()='Activation Code']");
		sleep(2);
		Reporter.log("Clicked On AboutSureLock", true);

		preferred_activation_id.click();
		waitForXpathPresent(
				"//*[@id='preferred-activation-id-popup']/div/div/div[1]/p[text()='Preferred Activation ID']");
		sleep(2);
		Reporter.log("Clicked On preferred_activation_id", true);

		preferred_activation_id_popup_IMSI.click();
		sleep(2);
		Reporter.log("IMSI is selected", true);

		preferred_activation_id_popupDoneBtn.click();
		waitForXpathPresent("//div[text()='Activation Code']");

		activation_code.clear();
		sleep(2);
		activation_code.sendKeys(activationcode);
		sleep(2);
		about_surelockDoneBTn.click();
		sleep(2);
	}

	@FindBy(xpath = "(//*[@id='preferred-activation-id-popup']/div/div/div[2]/div/div/span/input)[4]")
	private WebElement preferred_activation_id_popup_WIFIMAC;

	public void VerifyAboutSL_ConsoleSide_WIFI_MAC(String activationcode) throws InterruptedException {

		AboutSureLock.click();
		waitForXpathPresent("//div[text()='Activation Code']");
		sleep(2);
		Reporter.log("Clicked On AboutSureLock", true);

		preferred_activation_id.click();
		waitForXpathPresent(
				"//*[@id='preferred-activation-id-popup']/div/div/div[1]/p[text()='Preferred Activation ID']");
		sleep(2);
		Reporter.log("Clicked On preferred_activation_id", true);

		preferred_activation_id_popup_WIFIMAC.click();
		sleep(2);
		Reporter.log("WIFIMAC is selected", true);

		preferred_activation_id_popupDoneBtn.click();
		waitForXpathPresent("//div[text()='Activation Code']");

		activation_code.clear();
		sleep(2);
		activation_code.sendKeys(activationcode);
		sleep(2);
		about_surelockDoneBTn.click();
		sleep(2);
	}

	@FindBy(xpath = "(//*[@id='preferred-activation-id-popup']/div/div/div[2]/div/div/span/input)[5]")
	private WebElement preferred_activation_id_popup_BLUETOOTH_MAC;

	public void VerifyAboutSL_ConsoleSide_BLUETOOTH_MAC(String activationcode) throws InterruptedException {

		AboutSureLock.click();
		waitForXpathPresent("//div[text()='Activation Code']");
		sleep(2);
		Reporter.log("Clicked On AboutSureLock", true);

		preferred_activation_id.click();
		waitForXpathPresent(
				"//*[@id='preferred-activation-id-popup']/div/div/div[1]/p[text()='Preferred Activation ID']");
		sleep(2);
		Reporter.log("Clicked On preferred_activation_id", true);

		preferred_activation_id_popup_BLUETOOTH_MAC.click();
		sleep(2);
		Reporter.log("BLUETOOTH_MAC is selected", true);

		preferred_activation_id_popupDoneBtn.click();
		waitForXpathPresent("//div[text()='Activation Code']");

		activation_code.clear();
		sleep(2);
		activation_code.sendKeys(activationcode);
		sleep(2);
		about_surelockDoneBTn.click();
		sleep(2);
	}

	@FindBy(xpath = "(//*[@id='preferred-activation-id-popup']/div/div/div[2]/div/div/span/input)[6]")
	private WebElement preferred_activation_id_popup_GUID;

	public void VerifyAboutSL_ConsoleSide_GUID(String activationcode) throws InterruptedException {

		AboutSureLock.click();
		waitForXpathPresent("//div[text()='Activation Code']");
		sleep(2);
		Reporter.log("Clicked On AboutSureLock", true);

		preferred_activation_id.click();
		waitForXpathPresent(
				"//*[@id='preferred-activation-id-popup']/div/div/div[1]/p[text()='Preferred Activation ID']");
		sleep(2);
		Reporter.log("Clicked On preferred_activation_id", true);

		preferred_activation_id_popup_GUID.click();
		sleep(2);
		Reporter.log("GUID is selected", true);

		preferred_activation_id_popupDoneBtn.click();
		waitForXpathPresent("//div[text()='Activation Code']");

		activation_code.clear();
		sleep(2);
		activation_code.sendKeys(activationcode);
		sleep(2);
		about_surelockDoneBTn.click();
		sleep(2);
	}

	@FindBy(xpath = "(//*[@id='preferred-activation-id-popup']/div/div/div[2]/div/div/span/input)[1]")
	private WebElement preferred_activation_id_popup_NONE;

	public void VerifyAboutSL_ConsoleSide_NONE(String activationcode) throws InterruptedException {

		AboutSureLock.click();
		waitForXpathPresent("//div[text()='Activation Code']");
		sleep(2);
		Reporter.log("Clicked On AboutSureLock", true);

		preferred_activation_id.click();
		waitForXpathPresent(
				"//*[@id='preferred-activation-id-popup']/div/div/div[1]/p[text()='Preferred Activation ID']");
		sleep(2);
		Reporter.log("Clicked On preferred_activation_id", true);

		preferred_activation_id_popup_NONE.click();
		sleep(2);
		Reporter.log("NONE is selected", true);

		preferred_activation_id_popupDoneBtn.click();
		waitForXpathPresent("//div[text()='Activation Code']");

		activation_code.clear();
		sleep(2);
		activation_code.sendKeys(activationcode);
		sleep(2);
		about_surelockDoneBTn.click();
		sleep(2);
	}

	public void VerifyDeviceSideActivationSL(String PreferredActivationID) throws InterruptedException {

		boolean PreferredActivationIDText = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='" + PreferredActivationID + "']").isDisplayed();
		String Pass = "PASS>>" + PreferredActivationID + " is displayed";
		String Fail = "FAIL>>" + PreferredActivationID + " is Not displayed";
		ALib.AssertTrueMethod(PreferredActivationIDText, Pass, Fail);
		sleep(4);
	}

	public void VerifySLactivated_OR_not() throws InterruptedException {
		boolean DeactivateMsg = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Press to Deactivate device license']")
				.isDisplayed();
		String Pass = "PASS>>'Press to Deactivate device license' is displayed";
		String Fail = "FAIL>>'Press to Deactivate device license' is Not displayed";
		ALib.AssertTrueMethod(DeactivateMsg, Pass, Fail);
		sleep(2);
		Reporter.log("SureLock is Activated", true);
	}
	// new Methods

	@FindBy(id = "reset_device")
	private WebElement RebootJob;

	@FindBy(id = "job_reset_name_input")
	private WebElement enterRebootJobName;

	@FindBy(id = "job_move")
	private WebElement MoveToFolderBtn;

	@FindBy(xpath = "//*[@id='tree']/ul/li")
	private WebElement SelectFolderInFolderList;

	@FindBy(id = "job_copy")
	private WebElement JobCopyBtn;

	public void clickOniOSOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and6Icon")));
		androidOS.click();
		Reporter.log("Clicked on iOS Job", true);
		sleep(4);
	}

	public void clickOnWindowsOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and2Icon")));
		androidOS.click();
		Reporter.log("PASS >> Clicked on Windows Job", true);
		sleep(4);
	}

	public void clickOnWindowsSEOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and3Icon")));
		androidOS.click();
		Reporter.log("Clicked on WindowsSE Job", true);
		sleep(4);
	}

	public void clickOnWindowsMobileOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and4Icon")));
		androidOS.click();
		Reporter.log("Clicked on WindowsMobile Job", true);
		sleep(4);
	}

	public void clickOnAnyOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and5Icon")));
		androidOS.click();
		Reporter.log("Clicked on AnyOS Job", true);
		sleep(4);
	}

	public void clickOnAndroidWearOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and7Icon")));
		androidOS.click();
		Reporter.log("Clicked on AndroidWear Job", true);
		sleep(4);
	}

	public void clickOnLinuxOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and8Icon")));
		androidOS.click();
		Reporter.log("Clicked on Linux Job", true);
		sleep(4);
	}

	public void clickOnAndroidVR() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and10Icon")));
		androidOS.click();
		Reporter.log("Clicked on AndroidVR Job", true);
		sleep(4);
	}

	public void clickOnmacOSR() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and11Icon")));
		androidOS.click();
		Reporter.log("Clicked on AndroidVR Job", true);
		sleep(4);
	}

	public void clickOnmacOS() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement androidOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and11Icon")));
		androidOS.click();
		Reporter.log("Clicked on Mac Job", true);
		sleep(4);
	}

	public void clickOnReboot() throws InterruptedException {
		RebootJob.click();
		sleep(2);
	}

	public void enterRebootJobName(String job) throws InterruptedException {
		enterRebootJobName.sendKeys(job);
		sleep(2);
	}

	public void VerifyOfSearchOptionInJobsPage(String jobname) throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='jobDataGrid']/tbody/tr/td/p"));
		boolean flag;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			if (ActualValue.contains(jobname)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassStmt = "PASS >> Results are displayed according to the entered text.";
			String FailStmt = "Fail >> Results are not displayed according to the entered text.";
			ALib.AssertTrueMethod(flag, PassStmt, FailStmt);
		}
		try {
			if (Initialization.driver.findElement(By.id("backToParent")).isDisplayed()) {
				Initialization.driver.findElement(By.id("backToParent")).click();
				sleep(2);
			} else {
				Reporter.log("No need to click on Back to parent...");
			}

		} catch (Exception e) {

		}
	}

	public void VerifyOfFolderCreatedPopup() {
		String ActualValue = Initialization.driver.findElement(By.xpath("//span[text()='New folder created.']"))
				.getText();
		String ExpectedValue = "New folder created.";
		String PassStatement = "PASS >> New folder created PopUp is displayed." + ActualValue;
		String FailStatement = "Fail >> New folder created. PopUp isn't displayed." + ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void clickOnMoveToFolder() throws InterruptedException {
		MoveToFolderBtn.click();
		sleep(3);
		waitForidPresent("moveJob_modal");
	}

	public void clickOnCopyBtn() throws InterruptedException {
		JobCopyBtn.click();
		sleep(3);
	}

	public void SelectFolder(String folder) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("(//*[@id='tree']/ul/li[text()='" + folder + "'])[last()]")).click();
		sleep(4);
		okBtnOnFileTransferJobWindow.click();
		sleep(3);
		waitForXpathPresent("//span[contains(text(),'was moved to the folder')]");
	}

	public void ClickOnCopyJobOKButton() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='copyJob_modal']//button[text()='OK']")).click();
		waitForXpathPresent("//span[text()='Job copied successfully.']");
		sleep(2);
		Reporter.log("Job Copied Successfully", true);
	}

	public void SelectFolderInCopyJobPage(String folder) throws InterruptedException {
		Initialization.driver
				.findElement(By.xpath("(//div[@id='jobFolder_tree']/ul/li[text()='" + folder + "'])[last()]")).click();
		sleep(3);
		Initialization.driver.findElement(By.xpath("(//button[@id='okbtn'])[last()-1]")).click();
	}

	public void VerifyOfCopiedJobPopUp() {
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Job copied successfully.']")).isDisplayed();
			Reporter.log("PASS >>Job copied successfully is displayed.", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("Fail >> Job copied successfully PopUp isn't displayed.");
		}
	}

	public void VerifyOnCopiedJob(String job) {
		try {
			String a = Initialization.driver
					.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + job + "_copy']"))
					.getText();
			System.out.println(a);
			Initialization.driver
					.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + job + "_copy']"))
					.isDisplayed();
			Reporter.log("PASS >> Profile is copied successfully.", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Profile didn't copied profile successfully.");
		}
	}

	@FindBy(xpath = "//div[@id='panelBackgroundDevice']/div[1]/div[2]/div[4]/div[1]/span[1]")
	private WebElement JobsCount;

	@FindBy(xpath = "(//span[@class='page-size'])[last()]")
	private WebElement JobsAllBtn;

	@FindBy(xpath = "(//span[@class='btn-group dropup open']//a[contains(text(),'50')])[last()-1]")
	private WebElement Select50InJJobs;

	@FindBy(xpath = "(//span[@class='btn-group dropup open']//a[contains(text(),'100')])[last()]")
	private WebElement Select100InJJobs;

	@FindBy(xpath = "(//span[@class='btn-group dropup open']//a[contains(text(),'200')])[last()]")
	private WebElement Select200InJJobs;

	@FindBy(xpath = "(//span[@class='btn-group dropup open']//a[contains(text(),'500')])[last()]")
	private WebElement Select500InJJobs;

	public void VerifyOfPagination() {
		try {
			if (JobsCount.isDisplayed()) {
				String JobsCout = JobsCount.getText();
				System.out.println(JobsCout);
				String[] arrSplit_3 = JobsCout.split("\\s");
				String value1 = null;
				for (int i = 0; i < arrSplit_3.length - 1; i++) {
					value1 = arrSplit_3[i];
				}
				System.out.println(value1);
				int i = Integer.parseInt(value1);
				System.out.println(">> Total number of Jobs shown:" + i + " ");

				if (i >= 1 && i < 21) {
					System.out.println(">> jobs count is less than 20, so that there is no pagination option");
				} else if (i > 20 && i < 51) {
					JobsAllBtn.click();
					sleep(4);
					Select50InJJobs.click();
					System.out.println(">> jobs count is less than 50, so selected 50 per page");
				} else if (i > 50 && i < 101) {
					JobsAllBtn.click();
					sleep(4);
					Select100InJJobs.click();
					System.out.println(">>jobs  count is less than 100, so selected 100 per page");
				} else if (i > 100 && i < 201) {
					JobsAllBtn.click();
					sleep(4);
					Select200InJJobs.click();
					System.out.println(">> jobs count is less than 200, so selected 200 per page");
				} else {
					JobsAllBtn.click();
					sleep(4);
					Select500InJJobs.click();
					System.out.println(">> jobs count is more than 500, so selected all per page");
				}
			} else {
				ALib.AssertFailMethod("No Jobs are Present In Jobs page");
			}
		} catch (Exception e) {
			ALib.AssertFailMethod("No Jobs are present in Jobs Page");
		}
	}

	public void VerifyOfMoveJobToFolderPopUp(String job, String folder) throws InterruptedException {
		try {
			Initialization.driver.findElement(By.xpath("//span[contains(text(),'was moved to the folder')]"))
					.isDisplayed();
			Reporter.log("PASS >>Moved to folder successfully PopUp is displayed.", true);
			sleep(3);
		} catch (Exception e) {
			ALib.AssertFailMethod("Fail >>  Moved to folder successfully PopUp isn't displayed.");
			sleep(3);
		}
	}

	public void ClearSearchFieldInJobsPage() throws InterruptedException {
		SearchJobInJobList.clear();
		sleep(2);
	}

	public void ReadingConsoleMessageForJobDeployment(String JobName, String DeviceName) throws InterruptedException {
		waitForLogXpathPresent("//p[contains(text(),'The job named " + "\"" + JobName
				+ "\" has been applied to the device known as " + "\"" + DeviceName + "\"')]");
		Reporter.log("Job applied Message Is Displayed In The Console", true);
		sleep(5);
	}

	public void ClickOnYesBtnInApplyJobToTag() throws InterruptedException {
		Initialization.driver
				.findElement(By.xpath(
						"//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']"))
				.click();
		sleep(2);
	}

	public void EnableMDM() throws InterruptedException {
		EnableMDM.click();
		sleep(1);

	}

	public void clickOnTeleComMangeMentEditBtn() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//a[@id='dataUsageEditBtn']")).click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//select[@id='MobileDataUsageWarningDataType']")).click();
	}

	public void verifyOfRoamingDataOp() {
		try {
			Initialization.driver.findElement(By.xpath("//option[text()='Roaming Data']")).isDisplayed();
			ALib.AssertFailMethod("FAIL >> Roaming Data option is shown in Console");
		} catch (Exception e) {
			Reporter.log("PASS >> Roaming Data option is not shown in Console", true);
		}
	}

	@FindBy(xpath = "//span[@title='Telecom Management Policy']")
	private WebElement TelecomManagementJob;

	public void clickOnTeleComManagementJob() throws InterruptedException {
		TelecomManagementJob.click();
		sleep(2);
	}

	@FindBy(xpath = "//div[@class='modal-dialog modal-xl']//button[@aria-label='Close']")
	private WebElement TelecomManagementJobTabCloseBtn;

	public void closeTelecomManagementJob() throws InterruptedException {
		TelecomManagementJobTabCloseBtn.click();
		sleep(2);
	}

	@FindBy(id = "cancelPanel1")
	private WebElement CancelBtnInAndroidJobTab;

	public void clickOnCancelBtn_AndroidJobsTab() throws InterruptedException {
		CancelBtnInAndroidJobTab.click();
		sleep(2);
	}

	@FindBy(xpath = "//span[contains(text(),'Notification Policy')]")
	private WebElement NotificationPolicy;
	@FindBy(xpath = "//*[@id='job_ip3_input']")
	private WebElement EnableConnectionPolicy;
	@FindBy(xpath = "//*[@id='job_ip4_input']")
	private WebElement ConnectionPolicyInput;
	@FindBy(xpath = "//*[@id='job_notify_name_input']")
	private WebElement job_notify_name_input;
	@FindBy(xpath = "(//*[@id='Notification_inputs']/div/div/input)[1]")
	private WebElement EnableMDM;
	@FindBy(xpath = "//*[@id='okbtn']")
	private WebElement okbtn;

	public void ClickOnNotificationPolicy() throws InterruptedException {
		sleep(2);
		NotificationPolicy.click();
		waitForXpathPresent("//h4[contains(text(),'Notification Policy')]");
		sleep(4);
	}

	public void EnableConnectionPolicy() throws InterruptedException {
		sleep(2);
		EnableConnectionPolicy.click();
		sleep(1);
		ConnectionPolicyInput.clear();
		ConnectionPolicyInput.sendKeys("1");
		sleep(2);

	}

	public void EnterJobName(String JobName) throws InterruptedException {
		sleep(2);
		job_notify_name_input.clear();
		sleep(2);
		job_notify_name_input.sendKeys(JobName);
		sleep(4);

	}

	public void ClickOnSaveJob() throws InterruptedException {
		sleep(2);
		okbtn.click();
		sleep(2);

	}

	public void VerifyConfirmationMessageNoCompositeJobTextShouldBeDisplayed() {
		try {
			if (Initialization.driver.findElement(By.xpath(
					"//span[contains(text(),'Adding Notification Policy Job, Folder, Tags and other Fence Job is not allowed.')]"))
					.isDisplayed()) {
				Reporter.log(
						"PASS >> 'Adding Notification Policy Job, Folder, Tags and other Fence Job is not allowed.' is dispalyed ",
						true);
			}
		}

		catch (Exception e) {
			ALib.AssertFailMethod("Composite job text is displayed");
		}
	}

	@FindBy(xpath = "//*[@id='and12Icon']")
	private WebElement IntelAMT;

	public void VerifyIntelAMTjob() throws InterruptedException {

		boolean IntelAmt = IntelAMT.isDisplayed();
		String pass = "PASS>> IntelAMT Job is displayed";
		String fail = "FAIL>> IntelAMT Job is not displayed";
		ALib.AssertTrueMethod(IntelAmt, pass, fail);
		sleep(2);

	}

	public void VerifyIntelAmtJobInStandardAndPremiumAcc() {
		try {
			if (IntelAMT.isDisplayed()) {
				ALib.AssertFailMethod("FAIL >>'IntelAMT Job' is displayed");
			}
		}

		catch (Exception e) {
			Reporter.log("PASS >>'IntelAMT Job' is Not displayed", true);
		}

	}

	@FindBy(xpath = "//*[@id='data_usage_policy']")
	private WebElement data_usage_policy;

	public void ClickOndata_usage_policyTeleCom() throws InterruptedException {

		data_usage_policy.click();
		waitForXpathPresent("//h4[contains(text(),'Telecom Management Policy')]");
		sleep(2);
	}

	public void CopyNixUpgradeJob(String FolderName) throws InterruptedException {

		Initialization.driver
				.findElement(By.xpath("//*[@id='jobFolder_tree']/ul/li[contains(text(),'" + FolderName + "')]"))
				.click();
		sleep(4);

	}

	public void ClickOnOkBtnCopyFolder() throws InterruptedException {
		OkBtnCopy.click();
		waitForXpathPresent("//span[contains(text(),'Job copied successfully.')]");
		sleep(2);
		Reporter.log("Job copied successfully", true);

	}

	@FindBy(xpath = "//*[@id='jobDataGrid']/tbody/tr/td[@class='JobFolderPath']")
	private WebElement JobFolderPath;

	public void VerifyJobFolderPath(String FolderPathName) throws InterruptedException {

		String JobFolderPathVerify = JobFolderPath.getText();
		System.out.println(JobFolderPathVerify);
		String ExpectedFolderPath = "\\" + "\\" + FolderPathName;
		String pass = "PASS>> '" + ExpectedFolderPath + "' path is correct";
		String fail = "FAIL>> '" + ExpectedFolderPath + "' path is Not correct";
		ALib.AssertEqualsMethod(JobFolderPathVerify, ExpectedFolderPath, pass, fail);
		sleep(2);

	}

	public void ClickOnOkBtnCopyFolder_Folder() throws InterruptedException {
		OkBtnCopy.click();
		waitForXpathPresent("//span[contains(text(),'Folder copied successfully.')]");
		sleep(2);
		Reporter.log("Folder copied successfully", true);

	}

	@FindBy(xpath = "(//div[@class='osButton btn osIcons'])[6]")
	private WebElement AnyOs;

	@FindBy(xpath = "//*[@id='movedevice_job']")
	private WebElement movedevice_job;

	public void ClickOnAnyOs() throws InterruptedException {
		sleep(2);
		AnyOs.click();
		waitForXpathPresent("//*[@id='movedevice_job']");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='movedevicegroupname']/div/div/i")
	private WebElement movedevicegroupname;

	public void ClickOnMoveDeviceJob() throws InterruptedException {

		movedevice_job.click();
		waitForXpathPresent("//h4[contains(text(),'Move Device To Group')]");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='groupList-searchCont']/input")
	private WebElement SearchGroup;

	public void Entermovedevicegroupname() throws InterruptedException {

		movedevicegroupname.click();
		waitForXpathPresent("(//h4[text()='Group List'])[2]");
		sleep(2);

	}

	public void SearchGrp(String GrpName) throws InterruptedException {

		SearchGroup.clear();
		sleep(1);
		SearchGroup.sendKeys(GrpName);
		sleep(1);

	}

	@FindBy(xpath = "//*[@id='groupList']/ul/li[text()='" + Config.AnyOsJobGrpName + "']")
	private WebElement ChooseSourceGroup;

	@FindBy(xpath = "//*[@id='groupListModal']/div/div/div/button[text()='Ok']")
	private WebElement OkBtngroupListModal;

	public void SelectGroupFromGroupList() throws InterruptedException {

		ChooseSourceGroup.click();
		sleep(2);
		OkBtngroupListModal.click();
		sleep(2);
		// okBtnOnFileTransferJobWindow.click();
		OkButtonJob.click();

	}

	@FindBy(xpath = "//*[@id='MoveDeviceWhileDeletingGroup']")
	private WebElement MoveDeviceWhileDeletingGroup;

	public void DeleteGroup() throws InterruptedException {

		MoveDeviceWhileDeletingGroup.click();
		waitForXpathPresent("//span[conatins(text(),'" + Config.AnyOsJobGrpName
				+ "' deleted successfully. If you requested devices be moved to the Home group, they were moved before deleting '"
				+ Config.AnyOsJobGrpName + "'.");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='relayserverconfiguration_job']")
	private WebElement relayserverconfiguration_job;

	public void ClickOnRelayServerJob() throws InterruptedException {

		relayserverconfiguration_job.click();
		waitForXpathPresent("//h4[contains(text(),'Relay Server Configuration')]");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='jobname_rs_job_group']/div/span[contains(text(),'Job Name')]")
	private WebElement jobname_rs_job_group;
	@FindBy(xpath = "//*[@id='RelayServerConfigurationJobModal']/div/div/div[2]/div[2]/div/span[contains(text(),'Enable Relay Server')]")
	private WebElement EnableRelayServer;
	@FindBy(xpath = "//*[@id='enable_configure_rs_job']/div/div/span[contains(text(),'Relay URL')]")
	private WebElement Relay_URL;
	@FindBy(xpath = "//*[@id='username_rs_job_group']/div/span[contains(text(),'Relay Username')]")
	private WebElement RelayUsername;
	@FindBy(xpath = "//*[@id='password_rs_job_group']/div/span[contains(text(),'Relay Password')]")
	private WebElement RelayPassword;
	@FindBy(xpath = "//*[@id='enable_configure_rs_job']/div[4]/div/span[contains(text(),'Accept All Certificates')]")
	private WebElement AcceptAllCertificates;
	@FindBy(xpath = "//*[@id='enable_configure_rs_job']/div[5]/div/span[contains(text(),'Allow Fallback To Cloud')]")
	private WebElement AllowFallbackToCloud;
	@FindBy(xpath = "//*[@id='enable_configure_rs_job']/div[6]/span[contains(text(),'Relay TimeOut')]")
	private WebElement RelayTimeOut;
	@FindBy(xpath = "//*[@id='noteLock']/span[contains(text(),'Relay Server Configuration job will support from Nix Version >= Nix v19.92.')]")
	private WebElement noteLock;
	@FindBy(xpath = "//*[@id='RelayServerConfigurationJobModal']/div/div/div[1]/button")
	private WebElement CloseRelayServer;

	public void VerifyUIrelayServer() throws InterruptedException {

		boolean jobname_rs_job_groupRelay = jobname_rs_job_group.isDisplayed();
		String pass1 = "PASS >> jobname_rs_job_groupRelay is displayed";
		String fail1 = "FAIL >> jobname_rs_job_groupRelay is not displayed";
		ALib.AssertTrueMethod(jobname_rs_job_groupRelay, pass1, fail1);
		sleep(2);
		boolean Enable_RelayServer = EnableRelayServer.isDisplayed();
		String pass2 = "PASS >> Enable_RelayServer is displayed";
		String fail2 = "FAIL >> Enable_RelayServer is not displayed";
		ALib.AssertTrueMethod(Enable_RelayServer, pass2, fail2);
		sleep(2);
		boolean RelayURL = Relay_URL.isDisplayed();
		String pass3 = "PASS >> RelayURL is displayed";
		String fail3 = "FAIL >> RelayURL is not displayed";
		ALib.AssertTrueMethod(RelayURL, pass3, fail3);
		sleep(2);
		boolean Relay_Username = RelayUsername.isDisplayed();
		String pass4 = "PASS >> RelayUsername is displayed";
		String fail4 = "FAIL >> RelayUsername is not displayed";
		ALib.AssertTrueMethod(Relay_Username, pass4, fail4);
		sleep(2);
		boolean RelayPassword_Relay = RelayPassword.isDisplayed();
		String pass5 = "PASS >> RelayPassword is displayed";
		String fail5 = "FAIL >> RelayPassword is not displayed";
		ALib.AssertTrueMethod(RelayPassword_Relay, pass5, fail5);
		sleep(2);
		boolean AcceptAllCertificates_Relay = AcceptAllCertificates.isDisplayed();
		String pass6 = "PASS >> AcceptAllCertificates is displayed";
		String fail6 = "FAIL >> AcceptAllCertificates is not displayed";
		ALib.AssertTrueMethod(AcceptAllCertificates_Relay, pass6, fail6);
		sleep(2);
		boolean AllowFallbackToCloud_Relay = AllowFallbackToCloud.isDisplayed();
		String pass7 = "PASS >> AllowFallbackToCloud is displayed";
		String fail7 = "FAIL >> AllowFallbackToCloud is not displayed";
		ALib.AssertTrueMethod(AllowFallbackToCloud_Relay, pass7, fail7);
		sleep(2);
		boolean RelayTimeOut_Relay = RelayTimeOut.isDisplayed();
		String pass8 = "PASS >> RelayTimeOut is displayed";
		String fail8 = "FAIL >> RelayTimeOut is not displayed";
		ALib.AssertTrueMethod(RelayTimeOut_Relay, pass8, fail8);
		sleep(2);
		boolean noteLock_Relay = noteLock.isDisplayed();
		String pass9 = "PASS >> noteLock is displayed";
		String fail9 = "FAIL >> noteLock is not displayed";
		ALib.AssertTrueMethod(noteLock_Relay, pass9, fail9);
		sleep(2);
	}

	public void CloseRelayServer() throws InterruptedException {
		CloseRelayServer.click();
		sleep(2);

	}

	public void AppiumConfigurationforSettings(String appPackage, String activity)
			throws IOException, InterruptedException {

		// String apkpath="E:\\AutomationRelatedFiles\\NixBuild\\Nix.apk"; ///latest
		// appium
		// File app = new File (apkpath); ////for latest APPIUM
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "My Phone");
		caps.setCapability("appPackage", appPackage);// "com.android.settings"
		caps.setCapability("appActivity", activity);// "com.android.settings.Settings");
		caps.setCapability("noReset", "true");
		System.out.println("No Reset happened");
		caps.setCapability("adbExecTimeout", 60000);
		caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 10000);
		System.out.println("about to launch");

		try {
			Initialization.driverAppium = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		} catch (Throwable e) {
			System.out.println(e.getMessage());
		}
		sleep(5);

	}

	@FindBy(xpath = "//*[@id='WebHooks']")
	private WebElement WebHooks;

	public void ClickOnWebHooks() throws InterruptedException {

		WebHooks.click();
		waitForXpathPresent("//*[@id='WebHooks_tab']/div/div[1]/div/span[1][text()='Enable Webhooks']");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='EnableWebhook']")
	private WebElement EnableWebhook;

	public void EnableWebHooks() throws InterruptedException {
		sleep(2);
		boolean flag = EnableWebhook.isSelected();
		System.out.println(flag);
		if (flag == false) {
			EnableWebhook.click();
			Reporter.log("PASS >> Enabled Webhooks Option", true);
		}
		sleep(2);
		Reporter.log("PASS >> Enabled Webhooks Option", true);
	}

	public void DisableWebHooks() throws InterruptedException {
		boolean val = Initialization.driver.findElement(By.xpath("//*[@id='EnableWebhook']")).isSelected();
		if (val) {
			Initialization.driver.findElement(By.xpath("//*[@id='EnableWebhook']")).click();
			Reporter.log("WebHook Is Disabled Now", true);
			sleep(2);
		}

		else {

			Reporter.log("WebHook Is Already Disabled", true);
		}
	}

	@FindBy(xpath = "//*[@id='AddEndPoint']")
	private WebElement AddNewEndPoint;

	public void ClickOnAddEndPoint() throws InterruptedException {

		AddNewEndPoint.click();
		// waitForXpathPresent("//*[@id='WebHooks_tab']/div/div[1]/div/span[1][text()='Enable
		// Webhooks']");
		sleep(2);

	}

	@FindBy(xpath = "(//*[@id='dynamicEndpointID_1']/div/div)[1]/input") // dynamicEndpointID_0
	private WebElement dynamicEndpointID_0;
	@FindBy(xpath = "(//*[@id='dynamicEndpointID_1']/div/div)[2]/input")
	private WebElement WebhookEndpoint;

	@FindBy(xpath = "//*[@id='ApplyWebhookSettings']")
	private WebElement ApplyWebhookSettings;

	@FindBy(xpath = "//div[@class='newAnalyticsSection newEndpointSection']/div/span[@class='saveBtnblk']")
	private WebElement SaveWebHook;

	public void EnterWebHookName(int i, String WebHookName, int j, String Endpoint) throws InterruptedException {

		// for(int i=0;i<=1;i++) {

		ClickOnAddEndPoint();
		Initialization.driver.findElement(By.xpath(
				"(//input[@class='form-control ct-model-input analyticsName endpoint_name_input'])['" + i + "']"))
				.clear();
		sleep(2);
		Initialization.driver.findElement(By.xpath(
				"(//input[@class='form-control ct-model-input analyticsName endpoint_name_input'])['" + i + "']"))
				.sendKeys(WebHookName);
		sleep(2);
		Initialization.driver.findElement(By.xpath(
				"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['" + j
						+ "']"))
				.clear();
		sleep(2);
		Initialization.driver.findElement(By.xpath(
				"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['" + j
						+ "']"))
				.sendKeys(Endpoint);

		sleep(2);
		ApplySaveWebHooks();

	}

	// Initialization.driver.findElement(By.xpath("(//*[@id='"+EndPoint+"']/div/div)['"+i+"']/input")).clear();
	//// dynamicEndpointID_0.clear();
	// sleep(2);
	// Initialization.driver.findElement(By.xpath("(//*[@id='"+EndPoint+"']/div/div)['"+i+"']/input")).sendKeys(WebHookName);
	// sleep(2);

	public void EnterWebHookName1(String WebHookName1, String Endpoint) throws InterruptedException {

		for (int i = 0; i <= 1; i++) {

			ClickOnAddEndPoint();
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='form-control ct-model-input analyticsName endpoint_name_input'])['" + i + "']"))
					.clear();
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='form-control ct-model-input analyticsName endpoint_name_input'])['" + i + "']"))
					.sendKeys(WebHookName1);
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.clear();
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.sendKeys(Endpoint);

			sleep(2);

			System.out.println("i");
			ApplySaveWebHooks();
			// ApplyWebhookBtn();

		}
	}

	public void EnterWebHookName2(String WebHookName2, String Endpoint) throws InterruptedException {

		for (int i = 0; i <= 1; i++) {

			ClickOnAddEndPoint();
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='form-control ct-model-input analyticsName endpoint_name_input'])['" + i + "']"))
					.clear();
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='form-control ct-model-input analyticsName endpoint_name_input'])['" + i + "']"))
					.sendKeys(WebHookName2);
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.clear();
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.sendKeys(Endpoint);

			sleep(2);

			System.out.println("i");
			ApplySaveWebHooks();
			// ApplyWebhookBtn();

		}
	}

	public void EnterWebhookEndpoint(String Endpoint) throws InterruptedException {

		for (int i = 0; i <= 1; i++) {

			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.clear();
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.sendKeys(Endpoint);

			sleep(2);
			System.out.println("i");
			ApplySaveWebHooks();
			ApplyWebhookBtn();

		}

	}

	public void EnterWebhookEndpoint1(String Endpoint) throws InterruptedException {

		for (int i = 0; i <= 1; i++) {

			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.clear();
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.sendKeys(Endpoint);

			sleep(2);
			System.out.println("i");
		}

	}

	public void EnterWebhookEndpoint2(String Endpoint) throws InterruptedException {

		for (int i = 0; i <= 1; i++) {

			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.clear();
			sleep(2);
			Initialization.driver.findElement(By.xpath(
					"(//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input'])['"
							+ i + "']"))
					.sendKeys(Endpoint);

			sleep(2);
			System.out.println("i");
		}

	}

	//
	// Initialization.driver.findElement(By.xpath("(//*[@id='tagBlock']/input)['"+i+"']")).clear();
	//
	//// WebhookEndpoint.clear();
	// sleep(2);
	// Initialization.driver.findElement(By.xpath("(//*[@id='tagBlock']/input)['"+i+"']")).sendKeys(Endpoint);
	// sleep(2);
	// }

	public void ApplySaveWebHooks() throws InterruptedException {

		SaveWebHook.click();
		// waitForXpathPresent("//span[contains(text(),'Endpoint a registered
		// successfully.')]");
		sleep(6);

	}

	@FindBy(xpath = "//*[@id='webhooksdiv']/div/span/span[contains(text(),'Webhook')]")
	private WebElement WebHookOptn;

	public void VerifyWebhookOptnInNotificationPolicyDisplayed() throws InterruptedException {

		String WebHookOpt = WebHookOptn.getText();
		//System.out.println(WebHookOpt);
		String Expected = WebHookOpt;
		String pass3 = "PASS >> WebHookOptn is displayed";
		String fail3 = "FAIL >> WebHookOptn is not displayed";
		ALib.AssertEqualsMethod(Expected, WebHookOpt, pass3, fail3);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='notification_modal']/div[1]/button")
	private WebElement notification_modalClose;

	public void CloseNotificationPolicy() throws InterruptedException {

		notification_modalClose.click();
		sleep(2);

	}

	public void ApplyWebhookBtn() throws InterruptedException {

		ApplyWebhookSettings.click();
		// waitForXpathPresent("//span[contains(text(),'Settings updated
		// successfully.')]");
		sleep(6);

	}

	public void VerifyWebhookOptnNotPresentInNotificationPolicyJob() {
		boolean flag = false;
		try {
			if (WebHookOptn.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >> WebHookOptn is not displayed";
		String FailStatement = "FAIL >> WebHookOptn is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}

	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	String newWebhooks;

	public void ScrollToNewWebhooks() throws InterruptedException {

		WebElement scrollDown2 = Initialization.driver
				.findElement(By.xpath("//h3[contains(text(),'" + newWebhooks + "')]"));
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
	}

	List<String> Androidversions = new ArrayList<String>();

	public void ReadingAndroidVersionFromDropDown(String ID) {
		WebElement DropDown = Initialization.driver.findElement(By.id(ID));
		Select sel = new Select(DropDown);
		List<WebElement> Lis = sel.getOptions();
		for (int i = 0; i < Lis.size(); i++) {
			Androidversions.add(Lis.get(i).getText());
		}

	}

	public void VerifyLatestAndroidVersions(String[] ExpectedAndridVersions) {
		for (int i = 0; i < ExpectedAndridVersions.length; i++) {
			if (Androidversions.contains(ExpectedAndridVersions[i])) {
				Reporter.log("PASS>>> Lastest Android Version Is Displayed In DropDown", true);
			}

			else {
				ALib.AssertFailMethod("FAIL>>> Latest Android Version Is Not Displayed In DropDown");
			}
		}
	}

	@FindBy(xpath = "//div[@id='complianceJob_container']//button[@class='close']")
	private WebElement ComplianceJobCloseButton;

	public void ClickOnComplianceCloseButton() throws InterruptedException {
		ComplianceJobCloseButton.click();
		waitForidPresent("compliance_job");
		sleep(2);
	}

	public void GetNewWebHooksNumber() throws InterruptedException {
		newWebhooks = NewWebhooks.getText();
		System.out.println(newWebhooks);

		sleep(2);
	}

	// @FindBy(xpath = "(//*[@id='WebHooks_tab']/div/div/div/div/h3)[1]")
	// private WebElement NewWebhooks;

	public void WithoutAddingEndpoints() throws InterruptedException {

		dynamicEndpointID_0.clear();
		sleep(1);

		WebhookEndpoint.clear();
		sleep(1);

		ApplyWebhookBtn();

	}

	@FindBy(xpath = "//span[contains(text(),'Please enter an endpoint URL.')]")
	private WebElement ErrorMsg;

	public void VerifyErrorMsg() {

	}

	@FindBy(xpath = "//*[@id='webhooksdiv']/div/input")
	private WebElement webhooksdiv;
	@FindBy(xpath = "//*[@id='select_endpoint']")
	private WebElement select_endpoint;

	public void CickOnWebHookOptnNotificationPolicyJob() throws InterruptedException {

		webhooksdiv.click();

		waitForXpathPresent("//h4[text()='Endpoint URLs']");

		String DefaultWebHook = select_endpoint.getText();
		System.out.println(DefaultWebHook);
		String Expected = DefaultWebHook;
		String pass = "PASS>>Default Webhook name is displayed";
		String fail = "FAIL>>Default Webhook name is Not displayed";
		ALib.AssertEqualsMethod(Expected, DefaultWebHook, pass, fail);
		sleep(2);

		// boolean
		// ErrorMessage=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Please
		// add a WebHook Endpoint in Webhook section under Account
		// Settings.')]")).isDisplayed();
		// String pass="PASS>> 'Please add a WebHook Endpoint in Webhook section under
		// Account Settings.' is displayed";
		// String fail="FAIL>> 'Please add a WebHook Endpoint in Webhook section under
		// Account Settings.' is NOT displayed";
		// ALib.AssertTrueMethod(ErrorMessage, pass, fail);
		// sleep(2);

	}

	@FindBy(xpath = "//*[@id='webhooksPopup']/div/div/div[1]/button")
	private WebElement webhooksPopup;

	public void ClosewebhooksPopup() throws InterruptedException {

		webhooksPopup.click();
		waitForXpathPresent("//h4[contains(text(),'Notification Policy')]");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='jobName']")
	private WebElement jobNameCopied;

	public void ClearjobNameCopied(String enterjobNameCopied) throws InterruptedException {

		jobNameCopied.clear();
		sleep(2);
		jobNameCopied.sendKeys(enterjobNameCopied);
		sleep(2);

	}

	/*
	 * public void SingleSelectTagDropdownWebElement() {
	 * 
	 * Select dropdown = new Select(select_endpoint); // Get all options
	 * List<WebElement> allOptionsElement = dropdown.getOptions(); //Creating a list
	 * to store drop down options ArrayList<String> obtainedList = new
	 * ArrayList<String>(); // Storing options in list for(WebElement optionElement
	 * : allOptionsElement) { obtainedList.add(optionElement.getText()); }
	 * //Removing "Select" option as it is not actual option
	 * obtainedList.remove("Select");
	 * 
	 * 
	 * 
	 * //Default order of option in drop down
	 * System.out.println("Options in dropdown with Default order :"+obtainedList);
	 * 
	 * // Creating a temp list to sort
	 * 
	 * // List tempList = new ArrayList();&lt;&gt;(options); ArrayList<String>
	 * sortedList = new ArrayList<>(); for(String s:obtainedList){
	 * sortedList.add(s);
	 * 
	 * // Sort list ascending Collections.sort(sortedList);
	 * 
	 * 
	 * System.out.println("Sorted List "+ sortedList); //
	 * Assert.assertTrue(sortedList.equals(obtainedList));
	 * 
	 * 
	 * // equals() method checks for two lists if they contain the same elements in
	 * the same order. boolean ifSortedAscending = obtainedList.equals(sortedList);
	 * 
	 * if(ifSortedAscending) { System.out.println("List is sorted"); } else
	 * System.out.println("List is not sorted.");
	 * 
	 * }
	 * 
	 * }
	 */
	
	public void SingleSelectTagDropdownWebElement() {

		Select dropdown = new Select(select_endpoint);
		ArrayList<String> obtainedList = new ArrayList<String>();
		List<WebElement> elementList = Initialization.driver.findElements(By.xpath("//*[@id='select_endpoint']"));
		for (WebElement we : elementList) {
			obtainedList.add(we.getText());
			System.out.println(obtainedList);
		}
		ArrayList<String> sortedList = new ArrayList<>();
		for (String s : obtainedList) {
			sortedList.add(s);
			System.out.println(sortedList);

		}
		Collections.sort(sortedList);
		Assert.assertTrue(sortedList.equals(obtainedList));
	}

	public void verifyPendingJobStatusColor() {
		try {
			Initialization.driver.findElement(By.xpath("//i[@class='fa fa-circle st-jobStatus dev_st_icn ok ']"))
					.isDisplayed();
			Reporter.log("PASS >> 'Job Is In Pending State' orange color dot is displayed.", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> 'Job Is In Pending State' orange color dot is not displayed.");
		}
	}

	public void ComplianceAndroidOs_VersionFirst() throws InterruptedException {

		androidfirst.click();
		String AllAndroidVersions = androidfirst.getText();
		System.out.println(AllAndroidVersions);
		try {
			if (AllAndroidVersions.contains("Any") && AllAndroidVersions.contains("Ice Cream Sandwich(4.0)")
					&& AllAndroidVersions.contains("Ice Cream Sandwich(4.0.3)")
					&& AllAndroidVersions.contains("Jelly Bean(4.1)") && AllAndroidVersions.contains("Jelly Bean(4.2)")
					&& AllAndroidVersions.contains("Jelly Bean(4.3)")
					&& AllAndroidVersions.contains("KitKat(4.4 - 4.4.4)")
					&& AllAndroidVersions.contains("KitKat(4.4W - 4.4W.2)")
					&& AllAndroidVersions.contains("Lollipop(5.0)") && AllAndroidVersions.contains("Lollipop(5.1)")
					&& AllAndroidVersions.contains("Marshmallow(6.0 - 6.0.1)")
					&& AllAndroidVersions.contains("Nougat(7.0)") && AllAndroidVersions.contains("Nougat(7.1)")
					&& AllAndroidVersions.contains("Oreo(8.0)") && AllAndroidVersions.contains("Oreo(8.1)")
					&& AllAndroidVersions.contains("Pie(9.0)") && AllAndroidVersions.contains("Android 10")
					&& AllAndroidVersions.contains("Android 11")) {
				Reporter.log(" All Latest Android Versions are displayed ", true);

			}
		} catch (Exception e) {
			ALib.AssertFailMethod(" All Latest Android Versions are Not displayed ");
		}
	}

	public void ComplianceAndroidOs_VersionSecond() throws InterruptedException {

		androidsecond.click();
		String AllAndroidVersions = androidsecond.getText();
		System.out.println(AllAndroidVersions);
		try {
			if (AllAndroidVersions.contains("Any") && AllAndroidVersions.contains("Ice Cream Sandwich(4.0)")
					&& AllAndroidVersions.contains("Ice Cream Sandwich(4.0.3)")
					&& AllAndroidVersions.contains("Jelly Bean(4.1)") && AllAndroidVersions.contains("Jelly Bean(4.2)")
					&& AllAndroidVersions.contains("Jelly Bean(4.3)")
					&& AllAndroidVersions.contains("KitKat(4.4 - 4.4.4)")
					&& AllAndroidVersions.contains("KitKat(4.4W - 4.4W.2)")
					&& AllAndroidVersions.contains("Lollipop(5.0)") && AllAndroidVersions.contains("Lollipop(5.1)")
					&& AllAndroidVersions.contains("Marshmallow(6.0 - 6.0.1)")
					&& AllAndroidVersions.contains("Nougat(7.0)") && AllAndroidVersions.contains("Nougat(7.1)")
					&& AllAndroidVersions.contains("Oreo(8.0)") && AllAndroidVersions.contains("Oreo(8.1)")
					&& AllAndroidVersions.contains("Pie(9.0)") && AllAndroidVersions.contains("Android 10")
					&& AllAndroidVersions.contains("Android 11")) {
				Reporter.log(" All Latest Android Versions are displayed ", true);

			}
		} catch (Exception e) {
			ALib.AssertFailMethod(" All Latest Android Versions are Not displayed ");
		}
	}

	@FindBy(xpath = "//*[@id='complianceJob_container']/div[1]/button")
	private WebElement complianceJob_containerClose;

	public void ClosecomplianceJob_container() throws InterruptedException {

		complianceJob_containerClose.click();
		sleep(2);

	}

	public void CheckStatusOfappliedCompositeJob(String JobName, int MaximumTime) throws InterruptedException {

		try {

			incompleteJobRefreshButton.click();
			WebDriverWait wait1 = new WebDriverWait(Initialization.driver, MaximumTime);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='No Pending jobs']")))
					.isDisplayed();
			Reporter.log("PASS >> Job Deployed Successfully On Device", true);
			sleep(3);
			clickOnJobhistoryTab();
			System.out.println("Clicked On Job Completed Tab");
			String LatestJobName = LatestJobNameInsideHistory.getText();
			if (LatestJobName.equals(JobName)) {
				VerifyJobStatusInsideJobQueue();
			} else {
				ClickOnJobQueueCloseButton();
				ALib.AssertFailMethod("FAIL >> Job Name Is Different In Job Queue");
			}
		} catch (Exception e) {
			String CurrentIncompletedJobStatus = IncompleteJobStatus.getText();
			System.out.println("Incompleted Job Status is: " + CurrentIncompletedJobStatus);
			ClickOnJobQueueCloseButton();
			ALib.AssertFailMethod("FAIL >> Job Did not deployed within given time");
		}
		ClickOnJobQueueCloseButton();
	}

	@FindBy(xpath = "//*[@id='sureVideoRssSettings']/div/div[1]/p[1][contains(text(),'RSS and Text Feed Settings')]")
	private WebElement sureVideoRssSettings;

	public void ClickOnsureVideoRssSettings() throws InterruptedException {

		sureVideoRssSettings.click();
		waitForXpathPresent("//p[contains(text(),'Enable Text Scrolling')]");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='en_rss_feed']/div/div[2]/span/input")
	private WebElement en_rss_feedEnable;

	public void EnablesureVideoRssSettings() throws InterruptedException {

		en_rss_feedEnable.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='rss_scroll_speed']/div/div/p[text()='Scroll Speed']")
	private WebElement rss_scroll_speed;

	public void ClickOnrss_scroll_speed() throws InterruptedException {

		rss_scroll_speed.click();
		sleep(2);
		boolean RssScrollSpeedPrompt = Initialization.driver
				.findElement(By.xpath("//p[contains(text(),'RSS Scroll Speed')]")).isDisplayed();
		String pass = "PASS>> rss_scroll_speed Prompt is displayed";
		String fail = "FAIL>> rss_scroll_speed Prompt is Not displayed";
		ALib.AssertTrueMethod(RssScrollSpeedPrompt, pass, fail);

		sleep(2);

	}

	@FindBy(xpath = "//*[@id='scroll_speed']")
	private WebElement scroll_speed;
	@FindBy(xpath = "//*[@id='rss_scrollSpeed_popup']/div/div/div[3]/button[2]")
	private WebElement scroll_speedOkBtn;

	public void Inputscroll_speed(String InputSpeed) throws InterruptedException {

		scroll_speed.clear();
		sleep(1);
		scroll_speed.sendKeys(InputSpeed);
		sleep(2);
		scroll_speedOkBtn.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='rss_sett_pg']/footer/div[2]/button")
	private WebElement rss_sett_pgDoneBtn;

	public void ClickOnrss_sett_pgDoneBtn() throws InterruptedException {

		rss_sett_pgDoneBtn.click();
		sleep(2);

	}

	public void verifyAppSureVideoSettingScrollSpeed(String ScrollSpeed) throws InterruptedException {
		sleep(10);
		String GetScrollSpeed = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[contains(@text,'" + ScrollSpeed + "')]").getText();
		System.out.println(GetScrollSpeed);

		if (GetScrollSpeed.equals(ScrollSpeed)) {

			Reporter.log("Value is correct", true);
		} else {

			ALib.AssertFailMethod("Value is not correct");
		}

	}

	public void VerifyErrorMsgWebhookNotificationPolicy() throws InterruptedException {

		boolean ErrorMessage = Initialization.driver.findElement(By.xpath(
				"//span[contains(text(),'Please add a WebHook Endpoint in Webhook section under Account Settings.')]"))
				.isDisplayed();
		String pass = "PASS>> 'Please add a WebHook Endpoint in Webhook section under Account Settings.' is displayed";
		String fail = "FAIL>> 'Please add a WebHook Endpoint in Webhook section under Account Settings.' is NOT displayed";
		ALib.AssertTrueMethod(ErrorMessage, pass, fail);
		sleep(2);

	}

	@FindBy(xpath = "(//*[@id='WebHooks_tab']/div/div/div/div/h3)[1]")
	private WebElement NewWebhooks;

	@FindBy(xpath = "(//*[@id='WebHooks_tab']/div/div/div/div/h3)[1]/span/../../span[@class='cancelEndPoint']")
	private WebElement cancelEndPoint;

	public void RemoveWebhooks() throws InterruptedException {

		sleep(2);
		for (int i = 0; i <= 6; i++) {
			if (NewWebhooks.isDisplayed()) {

				cancelEndPoint.click();
				waitForXpathPresent(
						"//p[contains(text(),'Are you sure you want to delete the selected webhook endpoint?')]");
				sleep(2);
				Initialization.driver.findElement(By.xpath("//*[@id='WebHooksDeleteConfirmed']")).click();
				waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
				Reporter.log("Clicked On Remove Btn", true);
				sleep(2);

			} else {

				ALib.AssertFailMethod("Webhooks not removed");
			}
		}
	}

	public void outOfComplianceActions_UninstallApp(String complianceAction) throws InterruptedException {

		Select outOfCompliance = new Select(OutOfComplianceActionsDropDownMenu);

		try {
			outOfCompliance.selectByVisibleText(complianceAction);
			sleep(2);

			ALib.AssertFailMethod("Compliance Action is visible");

		} catch (Exception e) {

			Reporter.log("Uninstall not present", true);

		}

	}

	public void VerifyDeviceSideSureLockSettings() throws InterruptedException {
		boolean SureLockSettings = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='SureLock Settings']").isDisplayed();
		String pass1 = "PASS>>'Navigated to admin settings is displayed";
		String fail1 = "FAIL>>'Navigated to admin settings is not displayed";
		ALib.AssertTrueMethod(SureLockSettings, pass1, fail1);
		sleep(1);
		Reporter.log("Navigated to Admin SureLock Settings Device Side", true);

	}

	@FindBy(id = "notify_policy")
	private WebElement Notificationpolicyjob;

	public void ClickOnnNotificationPolicyJobs() throws InterruptedException {
		Notificationpolicyjob.click();
		waitForidPresent("job_ip1_input");
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='comp_rules_list']/li[2]/a/span[2]")
	private WebElement JailBrokenCompliance;

	public void clickOnJailBrokenCompliance() throws InterruptedException {
		JailBrokenCompliance.click();
	}

	@FindBy(xpath = "(//span[text()='Android device should be']/following-sibling::div[1]/div/span[2]/input[@name='andRoottoggle'])[1]")
	private WebElement andRoottoggle;

	public void clickOnandRoottoggle() throws InterruptedException {
		andRoottoggle.click();
	}

//Swati
	@FindBy(xpath = "//*[@id='kioskenabled']")
	private WebElement kioskenabled;

	public void EnablekioskMode() throws InterruptedException {

		boolean flag = kioskenabled.isSelected();
		if (flag) {
			Reporter.log("kioskenabled CheckBoxIs Already Enabled", true);
		} else {
			kioskenabled.click();
			Reporter.log("Enabled kioskenabled CheckBox", true);
		}
		sleep(2);
	}

	public void ScrollToDown() throws InterruptedException {

		WebElement scrollDown2 = Initialization.driver.findElement(By.xpath("(//span[text()='Previous'])[2]"));
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
	}
	
	@FindBy(xpath = "(//span[text()='Mobile Network Connectivity'])[1]")
	 private WebElement mobNetConnectivityCompliance;

	 public void clickOnMobNetConCompliance() throws InterruptedException {
		 mobNetConnectivityCompliance.click();
	 }
	 
	 @FindBy(xpath = "(//span[text()='Mobile Signal Strength'])[1]")
	 public WebElement mobileSignalStrength;

	 public void clickOnMobSignalStrength() throws InterruptedException {
		 mobileSignalStrength.click();
		 sleep(2);
	 }
	 
	 @FindBy(xpath = "//span[text()='Wifi Signal Strength']")
	 private WebElement wifiSignalStrength;

	 public void clickOnwifiSigStrength() {
		 wifiSignalStrength.click();
	 }
	 
	 @FindBy(id = "WifiSignalStrength_percent")
	 private WebElement wifiSigStrength_percent;

	 public void wifiSigStrengthPercent() {
		 wifiSigStrength_percent.sendKeys("1");
	 }
	 
	 @FindBy(id = "MobileSignalStrength_percent")
	 private WebElement mobSigStrength_percent;

	 public void mobSigStrengthPercent(String per) {
		 mobSigStrength_percent.sendKeys(per);
	 }
	 
	 public void clickOnDeviceIDinDevice() throws InterruptedException
		{
			Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Device ID']")).click();
			sleep(2);
		}
	 
	 public void clickOnYesBtnInCurrentDeviceIDTab() throws InterruptedException
	 {
		Initialization.driverAppium.findElement(By.xpath("//android.widget.Button[@text='Yes']")).click();
		sleep(2);
	 }
	 
//============================Mithilesh(2/08/2021)===============================	 

@FindBy(xpath = "//div[@id='and2Icon']")
private WebElement ClickonWindows ;	 
	 
public void clickOnWindows() throws InterruptedException {
	
     WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
	 WebElement Windows = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and2Icon")));
	 Windows.click();
	 Reporter.log("PASS >> Clicked on Windows Job", true);
	 sleep(4);
}
	 
public void clickOkBtnOnWindowsJob() throws InterruptedException {
	 sleep(2);
	 okBtnOnAndroidJobWindow.click();
	 Reporter.log("PASS >> Clicked OK button on File Transfer Page", true);
	 sleep(4);
}	 
	 
public void WindowsActivityLog() throws InterruptedException{
	
	 waitForXpathPresent("//span[contains(text(),' has been initiated on the device named')]");
	 String Msg=Initialization.driver.findElement(By.xpath("//span[contains(text(),' has been initiated on the device named')]")).getText();		
	 Reporter.log("PASS >> Message Displayed is : "+Msg,true);
	 sleep(2);
}	 
	 
@FindBy(xpath = "//div[@id='location_tracking']//i[@class='icn fa fa-map-marker']")
private WebElement ClickonLocationTracking ;	 
	 
public void ClickonLocationTracking() throws InterruptedException {
	 sleep(2);
	 ClickonLocationTracking.click();
	 Reporter.log("PASS >> Clicked on Location Tracking", true);
	 sleep(4);
}	 

@FindBy(xpath = "//input[@id='job_name_input']")
private WebElement LocationTrackingJobName ;	

@FindBy(xpath = "//input[@id='job_check']")
private WebElement EnableLocationTrackingCheckbox ;

@FindBy(xpath = "//div[@id='periodicity']//input[@id='job_text']")
private WebElement TrackingPeriodicity ;

@FindBy(xpath = "//button[@id='okbtn']")
private WebElement LocationTrackingOKButton ;
	 
public void LocationTracking() throws InterruptedException {
	 LocationTrackingJobName.clear();
	 sleep(2);
	 LocationTrackingJobName.sendKeys("Trails Location Tracking");
	 Reporter.log("PASS >> Location Tracking Job Name Entered", true);
	 sleep(2);
	 EnableLocationTrackingCheckbox.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Location Tracking CheckBox Selected", true);
	 TrackingPeriodicity.clear();
	 sleep(2);
	 TrackingPeriodicity.sendKeys("0");
	 sleep(2);
	 LocationTrackingOKButton.click();
	 Reporter.log("PASS >> Tracking Periodicity Value Entered", true);
	 waitForXpathPresent("//li[normalize-space()='Value must be greater than or equal to 1.']");
	 String Msg=Initialization.driver.findElement(By.xpath("//li[normalize-space()='Value must be greater than or equal to 1.']")).getText();		
	 Reporter.log("PASS >> Message Displayed is : "+Msg,true);
	 sleep(2);	
}	

@FindBy(xpath = "//div[@id='LocationTracking_header']//div[@class='close']")
private WebElement CloseLocationTrackingTab ;

@FindBy(xpath = "//button[@id='cancelPanel1']")
private WebElement ClickonCnacelButton ;
	 
public void CloseLocationTrackingTab() throws InterruptedException {
	 sleep(2);
	 CloseLocationTrackingTab.click();
	 Reporter.log("PASS >> Location Tracking Tab Closed",true);
	 ClickonCnacelButton.click();
	 sleep(2);
}

@FindBy(xpath = "//div[@id='compliance_job']//i[@class='icn fa fa-tasks']")
private WebElement ClickonComplianceJob ;	 
	 
public void ClickonComplianceJob() throws InterruptedException {
	 sleep(2);
	 ClickonComplianceJob.click();
	 Reporter.log("PASS >> Clicked on Compliance Job", true);
	 sleep(4);
}	

@FindBy(xpath = "//div[@class='col-xs-12 col-sm-6 col-md-6 input-group pull-left']//input[@id='job_name']")
private WebElement ComplianceJobName ;	 
	 
public void MobileThreatDefenseJobComplianceJob() throws InterruptedException {
	 sleep(2);
	 ComplianceJobName.clear();
	 ComplianceJobName.sendKeys("Trails Mobile Threat Defense Job");
	 Reporter.log("PASS >> Trails Mobile Threat Defense Job Entered ", true);
	 sleep(4);
}	

@FindBy(xpath = "//span[normalize-space()='Mobile Threat Defense']")
private WebElement SelectMobileThreatDefenseJob ;

@FindBy(xpath = "(//button[normalize-space()='Configure'])[2]")
private WebElement ClickonMobileThreatDefenseConfigure ;
	 
public void SelectMobileThreatDefenseJob() throws InterruptedException {
     sleep(2);
     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
 	 WebElement scrollDown = SelectMobileThreatDefenseJob;
 	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);	
	 Reporter.log("PASS >> Mobile Threat Defense Selected", true);
	 SelectMobileThreatDefenseJob.click();
	 sleep(2);
	 waitForXpathPresent("(//button[normalize-space()='Configure'])[2]");
	 ClickonMobileThreatDefenseConfigure.click();	 
	 sleep(4);
}	

@FindBy(xpath = "//div[@class='sec_holder']//div[@class='job_accordion_header'][normalize-space()='Anti-Virus Protection']")
private WebElement SelectAntiVirusProtection ;

@FindBy(xpath = "//div[@class='sec_holder']//input[@id='antiVirusMode']")
private WebElement AntiVirusProtectionCheckboxSelected ;
	 
public void SelectAntiVirusProtection() throws InterruptedException {
     sleep(2);
     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
 	 WebElement scrollDown = SelectAntiVirusProtection;
 	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);	
	 Reporter.log("PASS >> Mobile Threat Defense Selected", true);
	 SelectAntiVirusProtection.click();
	 waitForXpathPresent("//div[@class='sec_holder']//input[@id='antiVirusMode']");
	 AntiVirusProtectionCheckboxSelected.click();
	 sleep(2);
	 Reporter.log("PASS >> Anti Virus Protection Selected", true);
	 sleep(4);
}	

@FindBy(xpath = "//button[@id='saveBtn']")
private WebElement ComplianceJobSavebutton ;
	 
public void ComplianceJobSavebutton() throws InterruptedException {
     sleep(2);
     ComplianceJobSavebutton.click();
	 sleep(2);
}	
	 
public void AntiVirusProtectionVisible() throws InterruptedException {
     sleep(2);
     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = SelectAntiVirusProtection;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);	
	 SelectAntiVirusProtection.click();
	 sleep(2);
	 boolean flag = SelectAntiVirusProtection.isDisplayed();
	 if(flag)
		{
		  String Msg=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Anti-Virus Protection']")).getText();		
		  Reporter.log("PASS >> Option Displayed is : "+Msg,true);
		}
		else
		{
		  ALib.AssertFailMethod("FAIL >> Anti-Virus Protection is Not Displayed");
		  Reporter.log("Anti-Virus Protection is Not Displayed",true);
		}
}		 
	
@FindBy(xpath = "//div[@class='sec_holder']//li[@id='antiVirusMode_acc']//span[@title='Android'][normalize-space()='Android']")
private WebElement AndroidLabelVisible ;

public void AndroidLabelVisible() throws InterruptedException {
		 
	 boolean flag = AndroidLabelVisible.isDisplayed();
	 if(flag)
		{
		  String Msg=Initialization.driver.findElement(By.xpath("//div[@class='sec_holder']//li[@id='antiVirusMode_acc']//span[@title='Android'][normalize-space()='Android']")).getText();		
		  Reporter.log("PASS >> Label Displayed is : "+Msg,true);
		}
		else
		{
		  ALib.AssertFailMethod("FAIL >> Android Label is Not Displayed");
		  Reporter.log("Android Label is Not Displayed",true);
		}
}	
	 
@FindBy(xpath = "//div[@id='complianceJob_container']//div[@class='modal-header']//button[@type='button']")
private WebElement CloseComplianceJobTab ;
	 
public void CloseComplianceJobTab() throws InterruptedException {
     sleep(2);
     CloseComplianceJobTab.click();
	 sleep(2);
	 ClickonCnacelButton.click();
	 sleep(2);
}		 
	 	 
@FindBy(xpath = "//div[@id='app_Uninstall']//i[@class='icn fa fa-trash-o config-icon']")
private WebElement ClickonAppUninstall ;	 
	 
public void ClickonAppUninstall() throws InterruptedException {
	 sleep(2);
	 ClickonAppUninstall.click();
	 Reporter.log("PASS >> Clicked on App Uninstall", true);
	 sleep(4);
}		 
	 
@FindBy(xpath = "//input[@id='requiredJobName']")
private WebElement AppUninstallJobName ;	 
	 
public void AppUninstallJobName() throws InterruptedException {
	 sleep(2);
	 AppUninstallJobName.clear();
	 sleep(2);
	 AppUninstallJobName.sendKeys("Trails App Uninstall Job");
	 Reporter.log("PASS >> Trails App Uninstall Job Entered", true);
	 sleep(4);
}		 
	 
@FindBy(xpath = "//*[@id='AppUninstallList']/tbody/tr[1]/td[1]")
private WebElement SelectApp ;	 
	 
@FindBy(xpath = "//button[@id='ok_btn']")
private WebElement SelectAppUninstallOKbutton ;	

public void SelectAppUninstall() throws InterruptedException {
	 sleep(2);
	 SelectApp.click();
	 sleep(2);
	 Reporter.log("PASS >> App Uninstall Selected", true);
	 SelectAppUninstallOKbutton.click();
	 sleep(4);
}		 
	 
public void ModifiedAppUninstallJobName() throws InterruptedException {
	 sleep(2);
	 AppUninstallJobName.clear();
	 sleep(2);
	 AppUninstallJobName.sendKeys(Config.ModifiedAppUninstallJobName);
	 Reporter.log("PASS >> Trials Modified App Uninstall Job Entered", true);
	 sleep(4);
}		 
	 	 
@FindBy(xpath = "//p[text()='Trials Modified App Uninstall Job']")
private WebElement SelectTrialsModifiedAppUninstallJob ;	 
	 
@FindBy(xpath = "//div[@id='job_modify']//i[@class='icn fa fa-pencil-square-o']")
private WebElement ClickonModifiedButton ;

@FindBy(xpath = "//button[@id='ok_btn_modified']")
private WebElement ClickonModifiedSaveButton ;

public void SelectTrialsModifiedAppUninstallJob() throws InterruptedException {
	 sleep(2);
	 SelectTrialsModifiedAppUninstallJob.click();
	 sleep(2);
	 ClickonModifiedButton.click();
	 Reporter.log("PASS >> Clicked on Modified Button", true);
	 sleep(2);
	 AppUninstallJobName.clear();
	 sleep(2);
	 AppUninstallJobName.sendKeys(Config.ModifiedAppUninstallJobName);
	 sleep(2);
	 ClickonModifiedSaveButton.click();
	 sleep(2);
}		 
	
@FindBy(xpath = "//span[text()='Job modified successfully.']")
private WebElement JobModifiedSuccessfully ;

public void JobModifiedMessage() throws InterruptedException {
	boolean isdisplayed = true;

	try {
		JobModifiedSuccessfully.isDisplayed();

	} catch (Exception e) {
		isdisplayed = false;

	}

	Assert.assertTrue(isdisplayed, "FAIL >> 'Job Modified successfully' is not displayed");
	Reporter.log("PASS >> 'Job Modified successfully' is displayed", true);
	sleep(5);
}

public void WindowsModifiedActivityLog() throws InterruptedException{
	
	 waitForXpathPresent("//p[contains(text(),'has modified the job known as')] ");
	 String Msg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'has modified the job known as')] ")).getText();		
	 Reporter.log("PASS >> Message Displayed is : "+Msg,true);
	 sleep(2);
}	

public void funtionalityAppUninstallJobName() throws InterruptedException {
	 sleep(2);
	 AppUninstallJobName.clear();
	 sleep(2);
	 AppUninstallJobName.sendKeys("Trails funtionality App Uninstall");
	 Reporter.log("PASS >> Trails App funtionality Uninstall Entered", true);
	 sleep(4);
}	

@FindBy(xpath = "//div[@class='table_wrapper ct-noCont-wrapper']//input[@placeholder='Search']")
private WebElement SearchAppUninstallJobName ;	 
	 
public void SearchAppUninstallJobName() throws InterruptedException {
	 sleep(2);
	 SearchAppUninstallJobName.clear();
	 sleep(2);
	 SearchAppUninstallJobName.sendKeys(Config.SearchforApplicationName);
	 Reporter.log("PASS >> Search for Application", true);
	 sleep(4);
}	

@FindBy(xpath = "//span[contains(text(),'No matching result found.')]")
private WebElement SearchMessage ;

public void SearchMessageValidation() throws InterruptedException {
	boolean isdisplayed = true;

	try {
		SearchMessage.isDisplayed();

	} catch (Exception e) {
		isdisplayed = false;

	}

	Assert.assertTrue(isdisplayed, "FAIL >> 'No matching result found.' is not displayed");
	Reporter.log("PASS >> 'No matching result found.' is displayed", true);
	sleep(5);
}

@FindBy(xpath = "//button[@id='cancelPanel2']")
private WebElement CloseAppUninstallTab ;	 
	 
public void CloseAppUninstallTab() throws InterruptedException {
	 sleep(2);
	 CloseAppUninstallTab.click();
	 Reporter.log("PASS >> Closed App Uninstall Tab", true);
	 sleep(4);
}	

@FindBy(xpath="//*[@id='AppUninstallList']/tbody/tr[1]/td[2]")
private WebElement  ExpectedNumericals ;

public void ExpectedNumericals()throws InterruptedException{
	
	boolean flag=ExpectedNumericals.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//*[@id='AppUninstallList']/tbody/tr[1]/td[2]")).getText();
		String Expected = "1527c705-839a-4832-9118-54d4Bd6a0c89";
		String pass = "PASS >> Numericals Displayed is : " +option;
		String fail = "FAIL >> Numericals Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Numericals is Not Displayed");
		Reporter.log("FAIL >> Numericals is Not Displayed",true);
	}
}

@FindBy(xpath = "(//div[text()='Application Name'])[1]")
private WebElement ClickonApplicationName ;	 
	 
public void ApplicationName() throws InterruptedException {
	sleep(2);
	ClickonApplicationName.click();
	sleep(2);
	boolean flag=ClickonApplicationName.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//*[@id='AppUninstallList']/tbody/tr[1]/td[2]")).getText();
		String Expected = "WPTx64";
		String pass = "PASS >> Name Displayed is : " +option;
		String fail = "FAIL >> Name Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Application Name is Not Displayed");
		Reporter.log("FAIL >> Application Name is Not Displayed",true);
	}	
}

@FindBy(xpath = "//td[text()='A-Volute.Nahimic']")
private WebElement  VoluteNahimic ;

@FindBy(xpath = "(//td[text()='Adobe Acrobat Reader DC'])[1]")
private WebElement AdobeAcrobatReader ;

@FindBy(xpath = "(//td[text()='Adobe Photoshop Elements 2020'])[1]")
private WebElement AdobePhotoshopElements ;

@FindBy(xpath = "(//td[text()='Adobe Premiere Elements 2020'])[1]")
private WebElement AdobePremiereElements ;

public void verifyListInAppUninstall() {
	WebElement[] ele = { VoluteNahimic, AdobeAcrobatReader, AdobePhotoshopElements ,AdobePremiereElements };
	String[] Options = { "Volute Nahimic", "Adobe Acrobat Reader", "Adobe Photoshop Elements","Adobe Premiere Elements"};
	for (int i = 0; i < ele.length; i++) {
		boolean val = ele[i].isDisplayed();
		String Pass = "PASS>>>" + " " + Options[i] + "Is Displayed as Per List";
		String Fail = "FAIL>>>" + " " + Options[i] + "Is Not Displayed as Per List";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}
}

@FindBy(xpath = "(//span[text()='Location Tracking'])[2]")
private WebElement LocationTrackingVisible ;	 
	 
public void LocationTrackingVisible() throws InterruptedException {

	boolean flag=LocationTrackingVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("(//span[text()='Location Tracking'])[2]")).getText();
		String Expected = "Location Tracking";
		String pass = "PASS >> Option Displayed is : " +option;
		String fail = "FAIL >> Option Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Option is Not Displayed");
		Reporter.log("FAIL >> Option is Not Displayed",true);
	}	
}

@FindBy(xpath = "//button[@id='cancelPanel1']")
private WebElement closeSelectJobTypeTab ;
	 
public void closeSelectJobTypeTab() throws InterruptedException {
	 sleep(2);
	 ClickonCnacelButton.click();
	 Reporter.log("PASS >> Select Job Type Tab Closed",true);
}

@FindBy(xpath = "(//span[text()='Job Name'])[1]")
private WebElement  JobNameVisible ;

@FindBy(xpath = "(//span[text()='Enable Location Tracking'])[1]")
private WebElement EnableLocationTrackingVisible ;

@FindBy(xpath = "(//span[text()='Tracking periodicity (Min)'])[1]")
private WebElement TrackingperiodicityVisible ;

@FindBy(xpath = "(//button[text()='OK'])[1]")
private WebElement OKButtonVisible ;

public void verifyOptionsInLocationTracking() {
	WebElement[] ele = { JobNameVisible, EnableLocationTrackingVisible, TrackingperiodicityVisible ,OKButtonVisible };
	String[] Options = { "Job Name ", "Enable Location Tracking ", "Tracking periodicity ","OK Button "};
	for (int i = 0; i < ele.length; i++) {
		boolean val = ele[i].isDisplayed();
		String Pass = "PASS>>>" + " " + Options[i] + "Is Displayed";
		String Fail = "FAIL>>>" + " " + Options[i] + "Is Not Displayed";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}
}

public void EnableLocationTracking() throws InterruptedException {
	 LocationTrackingJobName.clear();
	 sleep(2);
	 LocationTrackingJobName.sendKeys("Enable Location Tracking");
	 Reporter.log("PASS >> Location Tracking Job Name Entered", true);
	 sleep(2);
	 EnableLocationTrackingCheckbox.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Location Tracking CheckBox Selected", true);
}	

public void TrackingPeriodicityEditable()throws InterruptedException{
	
	boolean flag=TrackingPeriodicity.isDisplayed();
	if(flag)
	{	   		
		 TrackingPeriodicity.clear();
		 sleep(2);
		 TrackingPeriodicity.sendKeys("1");
		 sleep(2);
		 LocationTrackingOKButton.click();
		 Reporter.log("PASS >> Tracking Periodicity is Editable", true);	
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Tracking Periodicity is Non Editable");
		Reporter.log("FAIL >> Tracking Periodicity is Non Editable",true);
	}
}

public void DisableLocationTracking() throws InterruptedException {
	 LocationTrackingJobName.clear();
	 sleep(2);
	 LocationTrackingJobName.sendKeys("Disable Location Tracking");
	 Reporter.log("PASS >> Location Tracking Job Name Entered", true);
	 sleep(2);
	 Reporter.log("PASS >> Disable Location Tracking CheckBox", true);
}	

public void TrackingPeriodicityNONEditable()throws InterruptedException{
	
	boolean flag=TrackingPeriodicity.isSelected();
	if(flag)
	{	   		
		 TrackingPeriodicity.clear();
		 sleep(2);
		 TrackingPeriodicity.sendKeys("1");
		 sleep(2);
		 LocationTrackingOKButton.click();
		 Reporter.log("FAIL >> Tracking Periodicity is Editable", true);	
	}
	else
	{		
		Reporter.log("PASS >> Tracking Periodicity is Grayed Out",true);
	}
}

public void LocationTrackingJOBCreated() throws InterruptedException {
	 LocationTrackingJobName.clear();
	 sleep(2);
	 LocationTrackingJobName.sendKeys("Trails Location Tracking Enabled");
	 Reporter.log("PASS >> Location Tracking Job Name Entered", true);
	 sleep(2);
	 EnableLocationTrackingCheckbox.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Location Tracking CheckBox Selected", true);
	 TrackingPeriodicity.clear();
	 sleep(2);
	 TrackingPeriodicity.sendKeys("10");
	 sleep(2);
	 LocationTrackingOKButton.click();
	 Reporter.log("PASS >> Tracking Periodicity Value Entered", true);
	 sleep(2);	
}	

public void LocationTrackingJOBCreatedDisabled() throws InterruptedException {
	 LocationTrackingJobName.clear();
	 sleep(2);
	 LocationTrackingJobName.sendKeys("Trails Location Tracking Disabled");
	 Reporter.log("PASS >> Location Tracking Job Name Entered", true);
	 sleep(2);
	 //EnableLocationTrackingCheckbox.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Location Tracking CheckBox Selected", true);
	 //TrackingPeriodicity.clear();
	 sleep(2);
	// TrackingPeriodicity.sendKeys("20");
	 sleep(2);
	 LocationTrackingOKButton.click();
	 //Reporter.log("PASS >> Tracking Periodicity Value Entered", true);
	// sleep(2);	
}	

@FindBy(xpath = "//div[@id='comp_job']//i[@class='icn fa fa-list-alt']")
private WebElement ClickonCompositeJob ;	 
	 
public void ClickonCompositeJob() throws InterruptedException {
	 sleep(2);
	 ClickonCompositeJob.click();
	 Reporter.log("PASS >> Clicked on Composite Job", true);
	 sleep(4);
}	 	 
	 
@FindBy(xpath = "//button[@id='ok_btn']")
private WebElement ClickonCompositeJobOKButton ;	 
	 
public void ClickonCompositeJobOKButton() throws InterruptedException {
	 sleep(2);
	 ClickonCompositeJobOKButton.click();
	 sleep(4);
}	  
	 
public void CompositeJobActivityLog() throws InterruptedException{
	
	 waitForXpathPresent("//p[contains(text(),'has applied a job named')]");
	 String Msg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'has applied a job named')]")).getText();		
	 Reporter.log("PASS >> Message Displayed is : "+Msg,true);
	 sleep(2);
}	 	 

public void SearchJobForCompositeMultipleJob(String Jobname) throws InterruptedException {
	SearchJobComposite.sendKeys(Jobname);
	sleep(2);
	waitForXpathPresent("//p[text()='" + Jobname + "']");
	sleep(2);
	Initialization.driver
			.findElement(By.xpath(".//*[@id='CompJobDataGrid']/tbody/tr/td[2]/p[text()='" + Jobname + "']"))
			.click();
	sleep(2);
	okBtnOnFileTransferJobWindow.click();
	waitForidPresent("comp_addDelayBtn");
	sleep(2);
}

@FindBy(xpath = "//div[@id='run_script']")
private WebElement ClickonRunScript ;	 
	 
public void ClickonRunScript() throws InterruptedException {
	 sleep(2);
	 ClickonRunScript.click();
	 Reporter.log("PASS >> Clicked on Run Script",true);
	 sleep(4);
}	

@FindBy(xpath = "//input[@id='job_name_input']")
private WebElement RunScriptJobName ;	 
	 
@FindBy(xpath = "//textarea[@id='job_script_input']")
private WebElement RunScriptTextArea ;

@FindBy(xpath = "//button[@id='okbtn']")
private WebElement RunScriptOKButton ;

public void CreateRunScriptJob() throws InterruptedException {
	 sleep(2);
	 RunScriptJobName.clear();
	 sleep(2);
	 RunScriptJobName.sendKeys(Config.RunScriptStartName);	 
	 Reporter.log("PASS >> Run Script Start Job Name Entered",true);
	 RunScriptTextArea.clear();
	 sleep(2);
	 RunScriptTextArea.sendKeys(Config.RunScriptStartText);
	 Reporter.log("PASS >> Script Text Entered",true);
	 sleep(2);
	 RunScriptOKButton.click();
	 sleep(4);
}	

public void RunScriptActivityLog() throws InterruptedException{
	
	 waitForXpathPresent("//p[contains(text(),'has applied a job named')]");
	 String Msg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'has applied a job named')]")).getText();		
	 Reporter.log("PASS >> Message Displayed is : "+Msg,true);
	 sleep(2);
}	

public void CreateRunScriptRestartJob() throws InterruptedException {
	 sleep(2);
	 RunScriptJobName.clear();
	 sleep(2);
	 RunScriptJobName.sendKeys(Config.RunScriptRestartName);	 
	 Reporter.log("PASS >> Run Script ReStart Job Name Entered",true);
	 RunScriptTextArea.clear();
	 sleep(2);
	 RunScriptTextArea.sendKeys(Config.RunScriptRestartText);
	 Reporter.log("PASS >> Script Text Entered",true);
	 sleep(2);
	 RunScriptOKButton.click();
	 sleep(4);
}	

public void CreateRunScriptStopJob() throws InterruptedException {
	 sleep(2);
	 RunScriptJobName.clear();
	 sleep(2);
	 RunScriptJobName.sendKeys(Config.RunScriptStopName);	 
	 Reporter.log("PASS >> Run Script Stop Job Name Entered",true);
	 RunScriptTextArea.clear();
	 sleep(2);
	 RunScriptTextArea.sendKeys(Config.RunScriptStopText);
	 Reporter.log("PASS >> Script Text Entered",true);
	 sleep(2);
	 RunScriptOKButton.click();
	 sleep(4);
}	

@FindBy(xpath = "//span[normalize-space()='Windows Copy Genuine Validation']")
private WebElement WindowsCopyGenuineValidation ;


public void WindowsCopyGenuineValidationVisible()throws InterruptedException{
	 sleep(2);
     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = WindowsCopyGenuineValidation ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		
	boolean flag=WindowsCopyGenuineValidation.isDisplayed();
	if(flag)
	{	 
		  String option = Initialization.driver.findElement(By.xpath("//span[normalize-space()='Windows Copy Genuine Validation']")).getText();
	      String Expected = "Windows Copy Genuine Validation";
	      String pass = "PASS >> Option Displayed in Window is : " +option;
	      String fail = "FAIL >> Option is Not Dispalyed";
	      ALib.AssertEqualsMethod(Expected, option, pass, fail);	 				 
	}
	else
	{		
		  ALib.AssertFailMethod("FAIL >> Windows Copy Genuine Validation is Not Displayed");
		  Reporter.log("FAIL >> Windows Copy Genuine Validation is Not Displayed",true);
	}
}

@FindBy(xpath = "//div[@id='windowsGenuine_BLK']//button[@type='button'][normalize-space()='Configure']")
private WebElement ClickonConfigureWindowsCopyGenuine ;


public void WindowsCopyGenuineValidation()throws InterruptedException{
	 sleep(2);
     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = WindowsCopyGenuineValidation ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		
	boolean flag=WindowsCopyGenuineValidation.isDisplayed();
	if(flag)
	{	 
		WindowsCopyGenuineValidation.click();
		sleep(2);
		waitForXpathPresent("//div[@id='windowsGenuine_BLK']//button[@type='button'][normalize-space()='Configure']");
		Reporter.log("PASS >> Clicked on Windows Copy Genuine Validation",true);
		ClickonConfigureWindowsCopyGenuine.click();
		sleep(2);
		Reporter.log("PASS >> Clicked on Configue Button Windows Copy Genuine Validation",true);
	}
	else
	{		
		  ALib.AssertFailMethod("FAIL >> Windows Copy Genuine Validation is Not Displayed");
		  Reporter.log("FAIL >> Windows Copy Genuine Validation is Not Displayed",true);
	}
}

@FindBy(xpath = "//div[@id='windowsGenuine_BLK']//span[@class='txt'][normalize-space()='Remove']")
private WebElement WindowsCopyGenuineRemoveButton ;

public void WindowsCopyGenuineValidationRemoveButtonVisible()throws InterruptedException{
		
	boolean flag=WindowsCopyGenuineRemoveButton.isDisplayed();
	if(flag)
	{	 
		WindowsCopyGenuineRemoveButton.click();
		sleep(2);
		Reporter.log("PASS >> Clicked on Remove Button Windows Copy Genuine Validation",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Remove Button Windows Copy Genuine Validation in Not Displayed");
		Reporter.log("FAIL >> Remove Button Windows Copy Genuine Validation in Not Displayed",true);
	}
}


@FindBy(xpath="//p[normalize-space()='Are you sure you want to remove the Rule?']")
private WebElement  Removebuttontext ;

@FindBy(xpath="(//button[text()='No'])[1]")
private WebElement RemoveButtonNo  ;

@FindBy(xpath="(//button[text()='Yes'])[1]")
private WebElement RemoveButtonYes  ;

public void RemoveButtonTextVisble()throws InterruptedException{
	
	boolean flag=Removebuttontext.isDisplayed();
	if(flag)
	{
		String option1 = Removebuttontext.getText();
		String Expected1 = "Are you sure you want to remove the Rule?";
		String pass1 = "PASS >> Remove Button Text Displayed is : " +option1;
		String fail1 = "FAIL >> Remove Button Text Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
		sleep(2); 
		
		String option2 = RemoveButtonNo.getText();
		String Expected2 = "No";
		String pass2 = "PASS >> Remove Button Option Displayed is : " +option2;
		String fail2 = "FAIL >> Remove Button Text Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected2, option2, pass2, fail2);	
		sleep(2);
		
		String option3 = RemoveButtonYes.getText();
		String Expected3 = "Yes";
		String pass3 = "PASS >> Remove Button Option Displayed is : " +option3;
		String fail3 = "FAIL >> Remove Button Text Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected3, option3, pass3, fail3);	
		sleep(2);
		RemoveButtonNo.click();		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Remove Button Option is Not Dispalyed");
		Reporter.log("FAIL >> Remove Button Option is Not Dispalyed",true);
	}	
}

@FindBy(xpath="//div[@id='windowsGenuine_BLK']//select[@id='ComplianceActionOption']")
private WebElement WindowsCopyGenuineOutOfComplianceActionsDropDown  ;

@FindBy(xpath="//div[@id='windowsGenuine_BLK']//button[@type='button'][normalize-space()='Add Action']")
private WebElement WindowsCopyGenuineADDButton ;

public void WindowsCopyGenuineADDButton()throws InterruptedException {
	 sleep(2);
	 JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = WindowsCopyGenuineADDButton ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);	
	 WindowsCopyGenuineADDButton.click();
	 sleep(2);
     Reporter.log("PASS >> Clicked on ADD Button ",true);
}

@FindBy(xpath="(//select[@id='ComplianceActionOption'])[2]")
private WebElement WindowsCopyGenuineOutofComplianceOne ;

public void WindowsCopyGenuineOutofComplianceOne(String complianceAction) throws InterruptedException {
	 sleep(4);
	 Select outOfCompliance = new Select(WindowsCopyGenuineOutofComplianceOne);
	 outOfCompliance.selectByVisibleText(complianceAction);
	 sleep(2);
	 Reporter.log("PASS >> Send Message Option is Selected From DropDown ",true);
}

@FindBy(xpath="(//select[@id='ComplianceActionOption'])[3]")
private WebElement WindowsCopyGenuineOutofComplianceTwo ;

@FindBy(xpath="//div[@class='act_subOpt']//input[@name='name']")
private WebElement Emailid ;

public void WindowsCopyGenuineOutofComplianceTwo(String complianceAction) throws InterruptedException {
	 sleep(2);
	 Select outOfCompliance = new Select(WindowsCopyGenuineOutofComplianceTwo);
	 outOfCompliance.selectByVisibleText(complianceAction);
	 sleep(2);
	 Emailid.clear();
	 sleep(2);
	 Emailid.sendKeys("abc@gmail.com");
	 Reporter.log("PASS >> EMail Notification Option is Selected From DropDown ",true);
}

@FindBy(xpath="(//select[@id='ComplianceActionOption'])[4]")
private WebElement WindowsCopyGenuineOutofComplianceThree ;

@FindBy(xpath="(//button[normalize-space()='Add Job'])[1]")
private WebElement ApplyJob ;

@FindBy(xpath="//div[@id='conmptableContainer']//input[@placeholder='Search']")
private WebElement SearchApplyJob ;

@FindBy(xpath="//p[@class='st-scrollTxt-left'][normalize-space()='Run Script Stop']")
private WebElement ClickonRunJob ;

@FindBy(xpath="//button[@id='okbtn']")
private WebElement ClickonOKButtonApplyJob ;

public void WindowsCopyGenuineOutofComplianceThree(String complianceAction) throws InterruptedException {
	 sleep(2);
	 Select outOfCompliance = new Select(WindowsCopyGenuineOutofComplianceThree);
	 outOfCompliance.selectByVisibleText(complianceAction);
	 sleep(2);
	 ApplyJob.click();
	 sleep(2);
	 SearchApplyJob.clear();
	 sleep(2);
	 SearchApplyJob.sendKeys("Run Script Stop");
	 sleep(2);
	 ClickonRunJob.click();
	 sleep(2);
	 ClickonOKButtonApplyJob.click();	 
	 Reporter.log("PASS >> Apply Job Option is Selected From DropDown ",true);
}

@FindBy(xpath="(//select[@id='ComplianceActionOption'])[5]")
private WebElement WindowsCopyGenuineOutofComplianceFour ;

@FindBy(xpath="//button[@id='saveBtn']")
private WebElement WindowsCopyGenuineSaveButton ;


public void WindowsCopyGenuineOutofComplianceFour(String complianceAction) throws InterruptedException {
	 sleep(2);
	 Select outOfCompliance = new Select(WindowsCopyGenuineOutofComplianceFour);
	 outOfCompliance.selectByVisibleText(complianceAction);
	 sleep(2);
	 Reporter.log("PASS >> Send Sms Option is Selected From DropDown ",true);
	 WindowsCopyGenuineSaveButton.click();
}

@FindBy(xpath="//div[@class='col-xs-12 col-sm-6 col-md-6 input-group pull-left']//input[@id='job_name']")
private WebElement WindowsCopyGenuineJobName ;

public void WindowsCopyGenuineJobName(String complianceAction) throws InterruptedException {
	 sleep(2);
	 WindowsCopyGenuineJobName.clear();
	 sleep(2);
	 WindowsCopyGenuineJobName.sendKeys("Trials Windows Copy Genuine Validation Job");
	 sleep(2);
	 Reporter.log("PASS >> Windows Copy Genuine Validation Job Name Entered",true);
}

public void WindowsCopyGeniusActivityLog() throws InterruptedException{
	
	 waitForXpathPresent("//p[contains(text(),'has applied a job named')]");
	 String Msg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'has applied a job named')]")).getText();		
	 Reporter.log("PASS >> Message Displayed is : "+Msg,true);
	 sleep(2);
}	

@FindBy(xpath = "//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
private WebElement SearchBoxModifiedJob ;

public void SearchFieldModifiedJob(String JobName) throws InterruptedException {
	SearchBoxModifiedJob.sendKeys(JobName);
	sleep(4);               
	waitForXpathPresent("//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']");
	sleep(6);
	WebElement JobSelected = Initialization.driver
			.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']"));
	JobSelected.click();
	sleep(2);
	//OkButtonApplyJob.click();
	//sleep(2);
}

public void WindowsCopyGenuineModifiedJobNameFirst(String complianceAction) throws InterruptedException {
	 sleep(2);
	 WindowsCopyGenuineJobName.clear();
	 sleep(2);
	 WindowsCopyGenuineJobName.sendKeys(complianceAction);
	 sleep(2);
	 //WindowsCopyGenuineSaveButton.click();
	 Reporter.log("PASS >> Windows Copy Genuine Validation Modified Job Name Entered",true);
}

@FindBy(xpath = "//div[@id='job_modify']//i[@class='icn fa fa-pencil-square-o']")
private WebElement ClickedTrialsWindowsCopyGenuineValidationModified ;

public void WindowsCopyGenuineModifiedJob() throws InterruptedException {
	 sleep(2);
	 ClickedTrialsWindowsCopyGenuineValidationModified.click();
	 Reporter.log("PASS >> Clicked on Modified Button",true);
	 sleep(2);	 
}

public void WindowsCopyGenuineModifiedJobName(String complianceAction) throws InterruptedException {
	 sleep(2);
	 WindowsCopyGenuineJobName.clear();
	 sleep(2);
	 WindowsCopyGenuineJobName.sendKeys(complianceAction);
	 sleep(2);
	 WindowsCopyGenuineSaveButton.click();
	 Reporter.log("PASS >> Windows Copy Genuine Validation Modified Job Name Entered",true);
}

@FindBy(xpath = "//div[@id='windowsGenuine_BLK']//i[@class='icn fa fa-trash-o']")
private WebElement ClickedonBin ;

@FindBy(xpath = "//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")
private WebElement ClickedonBinYesButton ;

public void RemovetheAction() throws InterruptedException {
	 sleep(2);
	 ClickedonBin.click();
	 Reporter.log("PASS >> Clicked on Bin Icon",true);
	 sleep(2);	
	 ClickedonBinYesButton.click();
	 sleep(2);	
	 Reporter.log("PASS >> Corresponding Action Deleted Successfully",true);
}

@FindBy(xpath="//input[@id='job_notify_name_input']")
private WebElement  EnterJobNameNotificationPloicy ;

@FindBy(xpath="//input[@id='job_ip1_input']")
private WebElement  ClickonEnableBatteryPolicy ;

@FindBy(xpath="//input[@id='job_ip1_input']")
private WebElement  ClickonRollerSlider ;

public void NotificationPolicyDetails()throws InterruptedException{
	
	sleep(4);
	EnterJobNameNotificationPloicy.sendKeys(Config.NotificationWeebookJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	ClickonEnableBatteryPolicy.click();
	Reporter.log("PASS >> Enable Battery Policy Selected",true);
	sleep(4);
	WebElement slider = Initialization.driver.findElement(By.xpath("//div[@class='slider-handle min-slider-handle round']"));
    Actions act = new Actions(Initialization.driver);
    act.dragAndDropBy(slider, 104, 0).build().perform();
    Reporter.log("PASS >> Notify when battery is below 30 Percentage",true);
}

@FindBy(xpath="//button[@onclick='SelectEndpoint()']")
private WebElement  ClickOnwebhooksPopupDoneBtn ;

public void ClickOnwebhooksPopupDoneBtn()throws InterruptedException{
	
	sleep(2);
	ClickOnwebhooksPopupDoneBtn.click();
	sleep(2);
    Reporter.log("PASS >> Clicked on Weebook Done Button",true);
}

@FindBy(xpath = "//*[@id='webhooksdiv']/div/input")
private WebElement webhooksOption;

@FindBy(xpath = "//button[@onclick='SelectEndpoint()']")
private WebElement ClickonOKButtonWeebook ;

public void WeebhookNotificationPolicy()throws InterruptedException{
	
	sleep(2);
	webhooksOption.click();
	sleep(2);
	ClickonOKButtonWeebook.click();	
    Reporter.log("PASS >> Clicked on Weebook Done Button",true);
}

@FindBy(xpath = "//div[@id='and3Icon']")
private WebElement ClickonwindowCE ;

public void ClickonwindowCE()throws InterruptedException{
	
	sleep(2);
	ClickonwindowCE.click();
	sleep(2);
    Reporter.log("PASS >> Clicked on Window CE",true);
}

@FindBy(xpath = "//div[@id='notify_policy']")
private WebElement ClickonwindowCENotificationPolicy ;

public void ClickonwindowCENotificationPolicy()throws InterruptedException{
	
	sleep(2);
	ClickonwindowCENotificationPolicy.click();
	sleep(2);
    Reporter.log("PASS >> Clicked on Notification Policy in Window CE",true);
}

@FindBy(xpath = "//div[@id='and4Icon']")
private WebElement ClickonwindowMobile ;

public void ClickonwindowMobile()throws InterruptedException{
	
	sleep(2);
	ClickonwindowMobile.click();
	sleep(2);
    Reporter.log("PASS >> Clicked on Window Mobile",true);
}

@FindBy(xpath = "//div[@id='notify_policy']")
private WebElement ClickonwindowMobileNotificationPolicy ;

public void ClickonwindowMobileNotificationPolicy()throws InterruptedException{
	
	sleep(2);
	ClickonwindowMobileNotificationPolicy.click();
	sleep(2);
    Reporter.log("PASS >> Clicked on Notification Policy in Window Mobile",true);
}

public void WebhookOptionisDisabled()throws InterruptedException{
	
	boolean flag=WebHookOptn.isSelected();
	if(flag)
	{	   		
		String option = WebHookOptn.getText();
		String Expected = option ;
		String pass = "PASS >> Option Displayed is " +option;
		String fail = "FAIL >> Option Displayed is not Displayed ";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{		
		Reporter.log("PASS >> Webhook Option is Not Displayed",true);
	}
}

@FindBy(xpath = "//input[@class='tagInputField form-control ct-model-input analyticsName endpoint_url_input']")
private WebElement WeebhookEndPoint ;

@FindBy(xpath = "//input[@class='form-control ct-model-input analyticsName endpoint_name_input']")
private WebElement WeebhookName ;

@FindBy(xpath = "//div[@onclick='testEndpointConnection(this)']")
private WebElement ClickonTestConnction ;

public void WeebhookMessageErrorVerified()throws InterruptedException{	
	 sleep(2);
	 WeebhookName.clear();
	 sleep(2);
	 WeebhookEndPoint.clear();
	 sleep(2);
	 ClickonTestConnction.click();
	 waitForXpathPresent("//span[contains(text(),'Please enter an endpoint URL.')]");
	 String Msg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Please enter an endpoint URL.')]")).getText();		
	 Reporter.log("PASS >> Message Displayed is : "+Msg,true);
}

public void LocationTrackingJOBTwentyFourCreated() throws InterruptedException {
	 LocationTrackingJobName.clear();
	 sleep(2);
	 LocationTrackingJobName.sendKeys("Trails Location Tracking Twenty Four");
	 Reporter.log("PASS >> Location Tracking Job Name Entered", true);
	 sleep(2);
	 EnableLocationTrackingCheckbox.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Location Tracking CheckBox Selected", true);
	 TrackingPeriodicity.clear();
	 sleep(2);
	 TrackingPeriodicity.sendKeys("24");
	 sleep(2);
	 LocationTrackingOKButton.click();
	 Reporter.log("PASS >> Tracking Periodicity Value Entered", true);
	 sleep(2);	
}	

@FindBy(xpath = "//div[@id='and5Icon']")
private WebElement clickOnANYOS ;	 
	 
public void clickOnANYOS() throws InterruptedException {
	
     WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
	 WebElement Windows = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and5Icon")));
	 Windows.click();
	 Reporter.log("PASS >> Clicked on Any OS", true);
	 sleep(4);
}

@FindBy(xpath = "//div[@id='compliance_job']")
private WebElement clickOnCompliancejobAnyOS ;	 
	 
public void clickOnCompliancejobAnyOS() throws InterruptedException {
	 sleep(2);
	 clickOnCompliancejobAnyOS.click();
	 Reporter.log("PASS >> Clicked on Compliance Job", true);
	 sleep(4);
}

@FindBy(xpath="//div[@class='col-xs-12 col-sm-6 col-md-6 input-group pull-left']//input[@id='job_name']")
private WebElement CompliancejobAnyOSJobName ;

public void CompliancejobAnyOSJobName(String complianceAction) throws InterruptedException {
	 sleep(2);
	 CompliancejobAnyOSJobName.clear();
	 sleep(2);
	 CompliancejobAnyOSJobName.sendKeys(complianceAction);
	 sleep(2);
	 Reporter.log("PASS >> Compliance job Any OS Job Name Entered",true);
}

@FindBy(xpath="//div[@id='osVersion_BLK']//button[@type='button'][normalize-space()='Configure']")
private WebElement CompliancejobAnyOSConfigure ;

public void CompliancejobAnyOSCreateJob() throws InterruptedException {
	 sleep(2);
	 CompliancejobAnyOSConfigure.click();
	 sleep(2);	 
	 Reporter.log("PASS >> Clicked on Configure",true);
}

@FindBy(xpath="//div[@id='osVersion_BLK']//select[@id='ComplianceActionOption']")
private WebElement ComplianceActionSendMessage ;

@FindBy(xpath="//button[@id='saveBtn']")
private WebElement ComplianceActionOKButton ;

public void ComplianceActionSendMessage(String complianceAction) throws InterruptedException {
	 sleep(4);
	 JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = ComplianceActionSendMessage ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	 sleep(2);
	 Select outOfCompliance = new Select(ComplianceActionSendMessage);
	 outOfCompliance.selectByVisibleText(complianceAction);
	 sleep(2);
	 Reporter.log("PASS >> Send Message Option is Selected From DropDown ",true);
	 ComplianceActionOKButton.click();
	 sleep(2);
}

@FindBy(xpath = "//div[@id='surelock_settings']")
private WebElement clickOnsurelocksettingsTab ;	 
	 
public void clickOnsurelocksettings() throws InterruptedException {
	 sleep(2);
	 clickOnsurelocksettingsTab.click();
	 Reporter.log("PASS >> Clicked on Surelock Settings", true);
	 sleep(4);
}

@FindBy(xpath = "//p[normalize-space()='SureLock Settings']")
private WebElement clickOnsurelocksettingsConfigure ;	 
	 
public void clickOnsurelocksettingsConfigure() throws InterruptedException {
	 sleep(2);
	 clickOnsurelocksettingsConfigure.click();
	 Reporter.log("PASS >> Clicked on Surelock Settings Configure", true);
	 sleep(4);
}

@FindBy(xpath = "//div[@class='nme_col list_col']//p[@class='tit_line'][normalize-space()='Single Application Mode']")
private WebElement clickOnSingleApplicationMode ;	 
	 
public void clickOnSingleApplicationMode() throws InterruptedException {
	 sleep(2);
	 JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = clickOnSingleApplicationMode ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	 sleep(2);	 
	 clickOnSingleApplicationMode.click();
	 Reporter.log("PASS >> Clicked on Single Application Mode", true);
	 sleep(4);
}

@FindBy(xpath = "//input[@class='ct-selbox-in st_chkBox']")
private WebElement SingleApplicationModeCheckBox ;	 
	 
public void EnableSingleApplicationModeCheckBox() throws InterruptedException {
	 sleep(2);	 
	 SingleApplicationModeCheckBox.click();
	 Reporter.log("PASS >> Enable Single Application Mode CheckBox", true);
	 sleep(4);
}

@FindBy(xpath = "//p[contains(text(),'This option is available only when you have set on')]")
private WebElement SingleApplicationModePromptMessage ;	 
	 
@FindBy(xpath = "//button[@onclick=\"closeDialog('AlertDialog')\"]")
private WebElement PromptMessageOKButton ;

@FindBy(xpath = "//div[@class='modal-dialog new_settings_slvf']//div[@class='close']")
private WebElement CloseSurelockSettingTab ;


public void SingleApplicationModePromptMessage() throws InterruptedException {
	   sleep(2);	 
	   waitForXpathPresent("//p[contains(text(),'This option is available only when you have set on')]");
	   String option =Initialization.driver.findElement(By.xpath("//p[contains(text(),'This option is available only when you have set on')]")).getText();	
	   String Expected = option ;
	   String pass = "PASS >> Message Displayed is : " +option;
	   String fail = "FAIL >> Message Displayed is not Displayed ";
	   ALib.AssertEqualsMethod(Expected, option, pass, fail);		 
	   PromptMessageOKButton.click();
	   sleep(2);
	   CloseSurelockSettingTab.click();
}

@FindBy(xpath = "//p[normalize-space()='Allowed Websites']")
private WebElement ClickonAllowedWebsites ;	 
	 
public void ClickonAllowedWebsites() throws InterruptedException {
	 sleep(2);	 
	 ClickonAllowedWebsites.click();
	 Reporter.log("PASS >> Click on Allowed Websites", true);
	 sleep(4);
}

@FindBy(xpath = "//button[normalize-space()='Add URL']")
private WebElement AllowedWebsitesDetails ;	 

@FindBy(xpath = "//input[@id='_URL']")
private WebElement URLNameDetails ;	 

@FindBy(xpath = "//button[@id='DoneBtn']")
private WebElement WebsiteInformationDoneButton ;

@FindBy(xpath = "//button[@id='done_btn']")
private WebElement AllowedWebsitesDoneButton ;
	 
public void AllowedWebsitesDetailsConfigured() throws InterruptedException {
	 sleep(2);	 
	 AllowedWebsitesDetails.click();
	 Reporter.log("PASS >> Click Add URL", true);
	 sleep(2);
	 URLNameDetails.clear();
	 sleep(2);
	 URLNameDetails.sendKeys("www.google.com");
	 Reporter.log("PASS >> URL Entered in Allowed Website", true);
	 WebsiteInformationDoneButton.click();
	 sleep(2);
	 AllowedWebsitesDoneButton.click();	 
}

@FindBy(xpath = "//button[@onclick=\"closeDialog('AlertDialog')\"]")
private WebElement ExitSingleApplicationMode ;

@FindBy(xpath = "//button[@class='opt_btn btn']")
private WebElement SureLockSettingDoneButton ;

@FindBy(xpath = "//button[@id='doneBtn']")
private WebElement SureLockSettingALLDoneButton ;
	 
public void ExitSingleApplicationMode() throws InterruptedException {
	 sleep(2);	 
	 ExitSingleApplicationMode.click();
	 Reporter.log("PASS >> Click Exit Single Application Mode OK Button ", true);
	 sleep(2);
	 SureLockSettingDoneButton.click();
	 sleep(2);
	 SureLockSettingALLDoneButton.click();
	 
}


@FindBy(xpath = "//button[@id='SaveAsBtn']")
private WebElement SureLockSettingSaveJob ;

@FindBy(xpath = "//input[@id='job_name']")
private WebElement SureLockSettingJobName ;

@FindBy(xpath = "//div[@id='saveAsJobModal']//button[@type='button'][normalize-space()='Save']")
private WebElement SureLockSettingJobNameSave ;

public void SureLockJobDetails() throws InterruptedException {
	 sleep(2);	 
	 SureLockSettingSaveJob.click();
	 Reporter.log("PASS >> Click on Save Button", true);
	 sleep(2);
	 SureLockSettingJobName.clear();
	 sleep(2);
	 SureLockSettingJobName.sendKeys(Config.SureLockJobName);
	 Reporter.log("PASS >> Surelock Settings Job Name Entered", true);
	 sleep(2);
	 SureLockSettingJobNameSave.click();	 
}

@FindBy(xpath = "//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
private WebElement SearchTextBoxModifiedJob ;

public void SearchTextBoxModifiedJob(String JobName) throws InterruptedException {
	 sleep(2);
	 SearchTextBoxModifiedJob.sendKeys(JobName);
	 sleep(4);             
	 waitForXpathPresent("//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']");
	 sleep(6);
	 WebElement JobSelected = Initialization.driver
			.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']"));
	 JobSelected.click();
	 sleep(2);
	 //OkButtonApplyJob.click();
	 sleep(2);
}

@FindBy(xpath = "//div[@id='job_modify']//i[@class='icn fa fa-pencil-square-o']")
private WebElement ClickonModifiedbutton ;

public void ClickonModifiedbutton() throws InterruptedException {
	 sleep(2);
	 ClickonModifiedbutton.click();
	 sleep(2);
}

@FindBy(xpath = "//input[@class='ct-selbox-in st_chkBox']")
private WebElement SingleApplicationModeModifiedCheckBox ;	 
	 
public void EnableSingleApplicationModeModifiedCheckBox() throws InterruptedException {
	 sleep(2);	 
	 boolean flag=SingleApplicationModeModifiedCheckBox.isSelected();
		if(flag)
		{	   		
			Reporter.log("PASS >> Single Application Mode CheckBox is Enabled",true);	
		}
		else
		{		
			ALib.AssertFailMethod("FAIL >> Single Application Mode CheckBox is Disabled");
			Reporter.log("FAIL >> Single Application Mode CheckBox is Disabled",true);
		}
	}
	 
@FindBy(xpath = "//div[@class='modal-dialog new_settings_slvf']//div[@class='close']")
private WebElement CloseSurelockModeSetting ;

public void CloseSurelockModeSetting() throws InterruptedException {
	 sleep(2);
	 CloseSurelockModeSetting.click();
	 sleep(2);
}	 
	
@FindBy(xpath = "//div[@class='modal-dialog new_settings_slvf']//div[@class='close']")
private WebElement clickOkBtnOnWindowSJob ;

public void clickOkBtnOnWindowSJob() throws InterruptedException {
	 sleep(2);
	 okBtnOnAndroidJobWindow.click();
	 Reporter.log("PASS >> Clicked OK button on File Transfer Page", true);
	 sleep(4);
}	 
	 
@FindBy(xpath = "//button[@id='ok_btn_modified']")
private WebElement clickOkBtnOnWindowModifiedJob ;

public void clickOkBtnOnWindowModifiedJob() throws InterruptedException {
	 sleep(2);
	 clickOkBtnOnWindowModifiedJob.click();
	 Reporter.log("PASS >> Clicked OK button on File Transfer Page", true);
	 sleep(4);
}	 	 
	 
public void SearchModifiedJob(String JobName) throws InterruptedException {
	 sleep(2);
	 SearchTextBoxModifiedJob.sendKeys(JobName);
	 sleep(5);
	 WebElement JobSelected = Initialization.driver
			.findElement(By.xpath("//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p"));
	 JobSelected.click();
	 sleep(2);
} 

public void SearchJobForCompositeGeoFencingJob(String Jobname) throws InterruptedException {
	 SearchJobComposite.sendKeys(Jobname);
	 sleep(2);
	 waitForXpathPresent("//p[text()='" + Jobname + "']");
	 sleep(2);
	 Initialization.driver
			.findElement(By.xpath(".//*[@id='CompJobDataGrid']/tbody/tr/td[2]/p[text()='" + Jobname + "']"))
			.click();
	 sleep(2);
	 okBtnOnFileTransferJobWindow.click();
	 waitForidPresent("comp_addDelayBtn");
	 sleep(2);
}

@FindBy(xpath = "//span[contains(text(),'Fence job cannot be added.')]")
private WebElement JobCannotbeAddedMessage ;

public void JobCannotbeAddedMessage() throws InterruptedException {
	boolean isdisplayed = true;

	try {
		JobCannotbeAddedMessage.isDisplayed();

	} catch (Exception e) {
		isdisplayed = false;

	}
	Assert.assertTrue(isdisplayed, "FAIL >> 'Fence job cannot be added.' is not displayed");
	Reporter.log("PASS >> 'Fence job cannot be added.' is displayed", true);
	sleep(5);
}

@FindBy(xpath = "//div[@id='selJob_addList_modal']//button[@aria-label='Close']")
private WebElement closeSelectJobtoAddTab ;

@FindBy(xpath = "//button[@id='cancelPanel2']")
private WebElement closeCompositeJob ;

public void closeSelectJobtoAddTab() throws InterruptedException {
	 sleep(2);
	 closeSelectJobtoAddTab.click();
	 Reporter.log("PASS >> Close Select Job to Add Tab", true);
	 closeCompositeJob.click();
	 sleep(4);
}	

@FindBy(xpath = "//div[@id='IntelAMT_config']")
private WebElement IntelAMTConfigurationVisible ;

public void IntelAMTConfigurationVisible()throws InterruptedException{
		
	boolean flag=IntelAMTConfigurationVisible.isDisplayed();
	if(flag)
	{	 
		Reporter.log("PASS >> Intel� AMT Configuration Option is Displayed in Windows",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Intel� AMT Configuration Option is Not Displayed in Windows");
		Reporter.log("FAIL >> Intel� AMT Configuration Option is Not Displayed in Windows",true);
	}
}

@FindBy(xpath = "//button[@id='cancelPanel1']")
private WebElement ClickonCancelButton ;

public void ClickonCancelButton() throws InterruptedException {
	 sleep(2);
	 ClickonCancelButton.click();
	 sleep(4);
}	

@FindBy(xpath = "//input[@id='amt_win_job_name']")
private WebElement IntelAMTConfigurationJobName ;

@FindBy(xpath = "//input[@id='amt_win_username']")
private WebElement AMTUsernameEntered ;

@FindBy(xpath = "//input[@id='amt_win_password']")
private WebElement AMTPasswordEntered ;

@FindBy(xpath = "//button[@id='amt_win_save']")
private WebElement IntelAMTConfigurationSaveButton ;

public void CreateIntelAMTConfigurationJob() throws InterruptedException {
	 sleep(2);
	 IntelAMTConfigurationVisible.click();
	 sleep(2);
	 
	 IntelAMTConfigurationJobName.clear();
	 sleep(2);
	 IntelAMTConfigurationJobName.sendKeys("Trials Intel AMT Configuration Job");
	 Reporter.log("PASS >> Trials Intel AMT Configuration Job Name Entered",true);
	 
	 AMTUsernameEntered.clear();
	 sleep(2);
	 AMTUsernameEntered.sendKeys("admin");
	 Reporter.log("PASS >> AMT Username Entered",true);
		 
	 AMTPasswordEntered.clear();
	 sleep(2);
	 AMTPasswordEntered.sendKeys("admin@123");
	 Reporter.log("PASS >> AMT Password Entered",true);	
	 
	 IntelAMTConfigurationSaveButton.click();
}	

@FindBy(xpath = "//span[@title='Install Application']")
private WebElement ClickonInstallApplication ;

public void ClickonInstallApplication() throws InterruptedException {
	 sleep(2);
	 ClickonInstallApplication.click();
	 Reporter.log("PASS >> Clicked on Install Application",true);	
	 sleep(4);
}	

@FindBy(xpath = "//div[@id='install_addBtn']//i[@class='icn fa fa-plus']")
private WebElement ClickonInstallApplicationADDButton ;

public void ClickonInstallApplicationADDButton() throws InterruptedException {
	 sleep(2);
	 ClickonInstallApplicationADDButton.click();
	 Reporter.log("PASS >> Clicked on ADD Button",true);	
	 sleep(2);	 
}	

@FindBy(xpath = "//span[normalize-space()='File Path/Url']")
private WebElement FilePathURLVisible ;

@FindBy(xpath = "//span[normalize-space()='Device Path']")
private WebElement DevicePathVisible ;

@FindBy(xpath = "//span[normalize-space()='Install After Copy']")
private WebElement InstallAfterCopyVisible ;

@FindBy(xpath = "//span[normalize-space()='Execute In Currently Logged In User']")
private WebElement ExecuteInCurrentlyLoggedInUserVisible ;

@FindBy(xpath = "//span[normalize-space()='Execute Path']")
private WebElement ExecutePathVisible ;

public void verifyOptionsInstallJob() {
	WebElement[] ele = { FilePathURLVisible , DevicePathVisible , InstallAfterCopyVisible, ExecuteInCurrentlyLoggedInUserVisible, ExecutePathVisible };
	String[] Options = { "File Path URL", "Device Path", "Install After Copy ", "Execute In Currently Logged In User", "Execute Path" };
	for (int i = 0; i < ele.length; i++) {
		boolean val = ele[i].isDisplayed();
		String Pass = "PASS >>" + " " + Options[i] + "is Displayed Successfully";
		String Fail = "FAIL >>" + " " + Options[i] + "is Not Displayed";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}
}

@FindBy(xpath = "//li[@title='The machine will be restarted without prompting after the unattended installation is complete.']")
private WebElement verysilentVisible ;

@FindBy(xpath = "//li[@title='Basic unattended installation.']")
private WebElement silentVisible ;

@FindBy(xpath = "//li[@title='Sets user interface level : no UI.']")
private WebElement qnVisible ;

@FindBy(xpath = "//li[@title='Installs or configures a product.']")
private WebElement iVisible ;

@FindBy(xpath = "//li[@title='The machine will not be restarted after the installation is complete.']")
private WebElement norestartVisible ;

@FindBy(xpath = "//li[@title='The machine will be restarted after the installation is complete.']")
private WebElement forcerestartVisible ;

public void verifyOptionsinExecutePath() {
	WebElement[] ele = { verysilentVisible , silentVisible , qnVisible, iVisible, norestartVisible,forcerestartVisible };
	String[] Options = { "/verysilent", "/silent", "/qn ", "/i", "/norestart","/forcerestart" };
	for (int i = 0; i < ele.length; i++) {
		boolean val = ele[i].isDisplayed();
		String Pass = "PASS >>" + " " + Options[i] + "is Displayed Successfully";
		String Fail = "FAIL >>" + " " + Options[i] + "is Not Displayed";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}
}

@FindBy(xpath = "//div[@id='install_add_page']//button[@aria-label='Close']")
private WebElement CloseInstallJobTab ;

@FindBy(xpath = "//button[@id='cancelPanel2']")
private WebElement ClickonCancelInstallApplication ;

public void CloseInstallJobTab() throws InterruptedException {
	 sleep(2);
	 CloseInstallJobTab.click();
	 Reporter.log("PASS >> Close Install Application Tab",true);	
	 sleep(2);	
	 ClickonCancelInstallApplication.click();
}

@FindBy(xpath = "//button[@class='uploadBtn']")
private WebElement BrowserInstallApplication ;

public void BrowserInstallApplication() throws InterruptedException {
	 sleep(2);
	 BrowserInstallApplication.click();
	 Reporter.log("PASS >> Clicked on Browser Install Application",true);	
}

public void browseFileInstallApplications(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);
	sleep(5);
	Reporter.log("PASS >> Install Application File Uploaded Successfully", true);
}

@FindBy(xpath = "//li[@title='The machine will be restarted without prompting after the unattended installation is complete.']")
private WebElement ExecutePathSelected ;

@FindBy(xpath = "//button[@id='okbtn']")
private WebElement InstallApplicationOKButton ;

@FindBy(xpath = "//button[@id='ok_btn']")
private WebElement InstallApplicationJobOKButton ;

public void ExecutePathVerysilentSelected() throws InterruptedException {
	 sleep(2);
	 ExecutePathSelected.click();
	 Reporter.log("PASS >> Execute Path Very silent Selected",true);	
	 InstallApplicationOKButton.click();
	 sleep(2);
	 InstallApplicationJobOKButton.click();	 
}

@FindBy(xpath = "//li[@title='Sets user interface level : no UI.']")
private WebElement ExecutePathqnSelected ;

public void ExecutePathqnSelected() throws InterruptedException {
	 sleep(2);
	 ExecutePathqnSelected.click();
	 Reporter.log("PASS >> Execute Path /qn Selected",true);	
	 InstallApplicationOKButton.click();
	 sleep(2);
	 InstallApplicationJobOKButton.click();	 
}

@FindBy(xpath = "//li[@title='The machine will not be restarted after the installation is complete.']")
private WebElement ExecutePathnorestartSelected ;

public void ExecutePathqnnorestartSelected() throws InterruptedException {
	 sleep(2);
	 ExecutePathqnSelected.click();
	 Reporter.log("PASS >> Execute Path /qn Selected",true);
	 ExecutePathnorestartSelected.click();
	 Reporter.log("PASS >> Execute Path /norestart Selected",true);
	 sleep(2);	 
	 InstallApplicationOKButton.click();
	 sleep(2);
	 InstallApplicationJobOKButton.click();	 
}


@FindBy(xpath = "//li[@title='The machine will be restarted after the installation is complete.']")
private WebElement ExecutePathforcerestartSelected ;

public void ExecutePathqnforcerestartSelected() throws InterruptedException {
	 sleep(2);
	 ExecutePathqnSelected.click();
	 Reporter.log("PASS >> Execute Path /qn Selected",true);
	 ExecutePathforcerestartSelected.click();
	 Reporter.log("PASS >> Execute Path /forcerestart Selected",true);
	 sleep(2);	 
	 InstallApplicationOKButton.click();
	 sleep(2);
	 InstallApplicationJobOKButton.click();	 
}

public void ExecutePathSilentnorestartSelected() throws InterruptedException {
	 sleep(2);
	 silentVisible.click();
	 Reporter.log("PASS >> Execute Path /silent Selected",true);
	 ExecutePathnorestartSelected.click();
	 Reporter.log("PASS >> Execute Path /norestart Selected",true);
	 sleep(2);	 
	 InstallApplicationOKButton.click();
	 sleep(2);
	 InstallApplicationJobOKButton.click();	 
}

public void ExecutePathSilentforcerestartSelected() throws InterruptedException {
	 sleep(2);
	 silentVisible.click();
	 Reporter.log("PASS >> Execute Path /silent Selected",true);
	 ExecutePathforcerestartSelected.click();
	 Reporter.log("PASS >> Execute Path /forcerestart Selected",true);
	 sleep(2);	 
	 InstallApplicationOKButton.click();
	 sleep(2);
	 InstallApplicationJobOKButton.click();	 
}

@FindBy(xpath = "//span[@title='SureMDM Agent Settings.']")
private WebElement ClickonSureMDMAgentSettings ;

public void ClickonSureMDMAgentSettings() throws InterruptedException {
	 sleep(2);
	 ClickonSureMDMAgentSettings.click();
	 Reporter.log("PASS >> Click on SureMDM Agent Settings",true);	
	 sleep(2);	
}

@FindBy(xpath = "//div[@class='col-xs-12 col-md-8 input-group ']//span[@class='input-group-addon mandatory labelno-bg ct-model-label'][normalize-space()='Job Name']")
private WebElement SureMDMAgentSettingsJobName ;

@FindBy(xpath = "//span[text()='Enable Device Info update']")
private WebElement SureMDMAgentSettingsEnableDeviceInfoupdate ;

@FindBy(xpath = "//span[text()='SureMDM Agent Password']")
private WebElement SureMDMAgentSettingsSureMDMAgentPassword ;

@FindBy(xpath = "//button[@id='okbtn']")
private WebElement SureMDMAgentSettingsOKButton ;

@FindBy(xpath = "//div[@id='nixAgent_modal']//div[@class='close']")
private WebElement SureMDMAgentSettingsCloseButton ;

public void VerifySureMDMAgentSettingsOptions() {
	WebElement[] ele = { SureMDMAgentSettingsJobName , SureMDMAgentSettingsEnableDeviceInfoupdate , SureMDMAgentSettingsSureMDMAgentPassword, SureMDMAgentSettingsOKButton};
	String[] Options = { "SureMDM Agent Settings JobName", "SureMDM Agent Settings Enable Device Info update", "SureMDM Agent Settings SureMDM AgentPassword", "SureMDM Agent Settings OK Button" };
	for (int i = 0; i < ele.length; i++) {
		boolean val = ele[i].isDisplayed();
		String Pass = "PASS >>" + " " + Options[i] + " is Displayed Successfully";
		String Fail = "FAIL >>" + " " + Options[i] + " is Not Displayed";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}
}

public void CloseSureMDMAgentSettings() throws InterruptedException {
	 sleep(2);
	 SureMDMAgentSettingsCloseButton.click();
	 Reporter.log("PASS >> Close SureMDM Agent Settings Tab",true);	
	 sleep(2);	 
}

@FindBy(xpath = "//div[contains(@class,'col-xs-12 col-md-8 input-group')]//input[@id='job_nixsettings_name_input']")
private WebElement SureMDMAgentSettingJobName ;

@FindBy(xpath = "//input[@id='job_nixsettings_NixPassword_input']")
private WebElement SureMDMAgentPassword ;

@FindBy(xpath = "//input[@id='job_nixsettings_EnablePeriodicUpdate_input']")
private WebElement EnableDeviceInfoupdate ;

@FindBy(xpath = "//input[@id='job_nixsettings_EnableNixPassword_input']")
private WebElement ClickonSureMDMAgentPassword ;

public void SureMDMAgentSettingDetails() throws InterruptedException {
	 sleep(2);
	 SureMDMAgentSettingJobName.clear();
	 sleep(2);
	 SureMDMAgentSettingJobName.sendKeys("Trials SureMDM Agent job");
	 Reporter.log("PASS >> SureMDM Agent job Name Entered",true);	
	 sleep(2);
	 EnableDeviceInfoupdate.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Device Info update",true);	
	 ClickonSureMDMAgentPassword.click();
	 Reporter.log("PASS >> Enable SureMDM Agent Password",true);	
	 sleep(2);
	 SureMDMAgentPassword.clear();
	 sleep(2);
	 SureMDMAgentPassword.sendKeys("0000");
	 Reporter.log("PASS >> SureMDM Agent Password Entered",true);
	 sleep(2);
	 SureMDMAgentSettingsOKButton.click();
}

public void SureMDMAgentSettingBlankPassword() throws InterruptedException {
	 sleep(2);
	 SureMDMAgentSettingJobName.clear();
	 sleep(2);
	 SureMDMAgentSettingJobName.sendKeys("Trials SureMDM Agent job");
	 Reporter.log("PASS >> SureMDM Agent job Name Entered",true);	
	 sleep(2);
	 EnableDeviceInfoupdate.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Device Info update",true);	
	 ClickonSureMDMAgentPassword.click();
	 Reporter.log("PASS >> Enable SureMDM Agent Password",true);	
	 sleep(2);
	 //SureMDMAgentPassword.clear();
	 //sleep(2);
	// SureMDMAgentPassword.sendKeys("0000");
	 Reporter.log("PASS >> SureMDM Agent Password Blank",true);
	 sleep(2);
	 SureMDMAgentSettingsOKButton.click();
}

public void SureMDMAgentSettingModifiedjob() throws InterruptedException {
	 sleep(2);
	 SureMDMAgentSettingJobName.clear();
	 sleep(2);
	 SureMDMAgentSettingJobName.sendKeys("Trials SureMDM Agent Modified job");
	 Reporter.log("PASS >> SureMDM Agent job Name Entered",true);	
	 sleep(2);
	 EnableDeviceInfoupdate.click();
	 sleep(2);
	 Reporter.log("PASS >> Enable Device Info update",true);	
	 ClickonSureMDMAgentPassword.click();
	 Reporter.log("PASS >> Enable SureMDM Agent Password",true);	
	 sleep(2);
	 SureMDMAgentPassword.clear();
	 sleep(2);
	 SureMDMAgentPassword.sendKeys("0000");
	 Reporter.log("PASS >> SureMDM Agent Password Entered",true);
	 sleep(2);
	 SureMDMAgentSettingsOKButton.click();
}

public void SureMDMAgentModifiedjob() throws InterruptedException {
	 sleep(2);
	 SureMDMAgentSettingJobName.clear();
	 sleep(2);
	 SureMDMAgentSettingJobName.sendKeys("Trials SureMDM Agent Modified job");
	 Reporter.log("PASS >> SureMDM Agent job Name Entered",true);
	 sleep(2);
	 SureMDMAgentSettingsOKButton.click();
}

//firewall

@FindBy(xpath = "//input[@id='Firewall_jobName']")
private WebElement FirewalljobName ;

@FindBy(xpath = "//input[@id='firewallPolicy_checkbox']")
private WebElement EnableFirewallPolicyCheckbox ;

public void FirewalljobName() throws InterruptedException {
	 sleep(2);
	 FirewalljobName.clear();
	 sleep(2);
	 FirewalljobName.sendKeys("Trials Firewall Job");
	 Reporter.log("PASS >> Trials Firewall Job Name Entered",true);	
	 sleep(2);
}

public void EnableFirewallPolicyCheckbox()throws InterruptedException{
	
	boolean flag=EnableFirewallPolicyCheckbox.isSelected();
	if(flag==false)
	{	
		EnableFirewallPolicyCheckbox.click();
		sleep(2);		
	}	
	Reporter.log("PASS >> Clicked on Enable Firewall Policy Checkbox",true);
}

@FindBy(xpath = "//input[@id='firewall_input']")
private WebElement FirewallPolicyDomainName ;

@FindBy(xpath = "//i[@class='icn fa fa-plus-square']")
private WebElement FirewallPolicyDomainADDButton ;

@FindBy(xpath = "//button[@id='firewall_save']")
private WebElement FirewallPolicyDomainSaveButton ;

public void FirewallPolicyDomainName() throws InterruptedException {
	 sleep(2);
	 FirewallPolicyDomainName.clear();
	 sleep(2);
	 FirewallPolicyDomainName.sendKeys("www.google.com");
	 Reporter.log("PASS >> Firewall Domain Name Entered",true);	
	 sleep(2);
	 FirewallPolicyDomainADDButton.click();
	 sleep(2);
	 FirewallPolicyDomainSaveButton.click();
}

@FindBy(xpath = "//input[@id='White_List']")
private WebElement EnableFirewallPolicyAllowlist ;

public void EnableFirewallPolicyAllowlist()throws InterruptedException{
	
	boolean flag=EnableFirewallPolicyAllowlist.isSelected();
	if(flag==false)
	{	
		EnableFirewallPolicyAllowlist.click();
		sleep(2);		
	}	
	Reporter.log("PASS >> Domain List Allowlist Enabled",true);
}

public void FirewallAllowlistjobName() throws InterruptedException {
	 sleep(2);
	 FirewalljobName.clear();
	 sleep(2);
	 FirewalljobName.sendKeys("Trials Firewall Allowlist Job");
	 Reporter.log("PASS >> Trials Firewall Job Name Entered",true);	
	 sleep(2);
}

public void FirewallBlocklistjobName() throws InterruptedException {
	 sleep(2);
	 FirewalljobName.clear();
	 sleep(2);
	 FirewalljobName.sendKeys("Trials Firewall Blocklist Job");
	 Reporter.log("PASS >> Trials Firewall Job Name Entered",true);	
	 sleep(2);
}

public void FirewallModifiedjobName() throws InterruptedException {
	 sleep(2);
	 FirewalljobName.clear();
	 sleep(2);
	 FirewalljobName.sendKeys("Trials Firewall Modified Job");
	 Reporter.log("PASS >> Trials Firewall Job Name Entered",true);	
	 sleep(2);
}

public void FirewallModifiedJobNameDetails() throws InterruptedException {
	 sleep(2);
	 FirewalljobName.clear();
	 sleep(2);
	 FirewalljobName.sendKeys("Trials Firewall Modified Job");
	 Reporter.log("PASS >> Trials Firewall Job Name Entered",true);	
	 sleep(2);
	 FirewallPolicyDomainSaveButton.click();
}

public void FirewallJobNameDetails() throws InterruptedException {
	 sleep(2);
	 FirewalljobName.clear();
	 sleep(2);
	 FirewalljobName.sendKeys("Trials Firewall Job");
	 Reporter.log("PASS >> Trials Firewall Job Name Entered",true);	
	 sleep(2);
	 FirewallPolicyDomainSaveButton.click();
}

@FindBy(xpath = "//input[@id='proxySett_jobName']")
private WebElement ProxySettingJobName ;

public void ProxySettingsManualJobName() throws InterruptedException {
	 sleep(2);
	 ProxySettingJobName.clear();
	 sleep(2);
	 ProxySettingJobName.sendKeys("Trials Proxy Settings Manual Job");
	 Reporter.log("PASS >> Trials Proxy Settings Manual Job Name Entered",true);	
	 sleep(2);
}

@FindBy(xpath = "//input[@id='proxySetting_checkbox']")
private WebElement EnableProxySettingscheckbox ;

public void EnableProxySettingscheckbox()throws InterruptedException{
	
	boolean flag=EnableProxySettingscheckbox.isSelected();
	if(flag==false)
	{	
		EnableProxySettingscheckbox.click();
		sleep(2);		
	}	
	Reporter.log("PASS >> Enable Proxy Settings",true);
}

@FindBy(xpath = "//div[@id='proxySettings_modal']//button[@aria-label='Close']")
private WebElement CloseProxySettingTab ;

@FindBy(xpath = "//button[@id='cancelPanel1']")
private WebElement CancelWindow ;

public void CloseProxySettingTab() throws InterruptedException {
	 sleep(2);
	 CloseProxySettingTab.click();
	 sleep(2);
	 CancelWindow.click();
	 Reporter.log("PASS >> Close Proxy Setting Tab",true);	
	 sleep(2);
}














@FindBy(xpath = "//select[@id='proxyType']")
private WebElement ProxyTypeDropDown ;

public void ProxyTypeDropDown()throws InterruptedException{
	
	Select sel = new Select(ProxyTypeDropDown);
	sel.selectByVisibleText("Manual");
	Reporter.log("PASS >> Proxy Type Manual Selected",true);
}

@FindBy(xpath = "//input[@id='proxy_server']")
private WebElement ProxyServersEntered ;

public void ProxyServersEntered() throws InterruptedException {
	 sleep(2);
	 ProxyServersEntered.clear();
	 sleep(2);
	 ProxyServersEntered.sendKeys("123456789");
	 Reporter.log("PASS >> Proxy Servers Entered",true);	
	 sleep(2);
}

@FindBy(xpath = "//input[@id='proxy_port']")
private WebElement ProxyPortEntered ;

public void ProxyPortEntered() throws InterruptedException {
	 sleep(2);
	 ProxyPortEntered.clear();
	 sleep(2);
	 ProxyPortEntered.sendKeys("123456789");
	 Reporter.log("PASS >> Proxy Port Entered",true);	
	 sleep(2);
}

@FindBy(xpath = "//button[@class='btn smdm_btns smdm_grn_clr addbutton sc-save-btn']")
private WebElement ProxySettingsSaveButton ;

public void ProxySettingsSaveButton() throws InterruptedException {
	 sleep(2);
	 ProxySettingsSaveButton.click();
	 Reporter.log("PASS >> Clicked on Proxy Settings Save button",true);	
	 sleep(2);
}

public void ProxySettingsAutoJobName() throws InterruptedException {
	 sleep(2);
	 ProxySettingJobName.clear();
	 sleep(2);
	 ProxySettingJobName.sendKeys("Trials Proxy Settings Auto Job");
	 Reporter.log("PASS >> Trials Proxy Settings Auto Job Name Entered",true);	
	 sleep(2);
}

public void ProxyTypeAutoDropDown()throws InterruptedException{
	
	Select sel = new Select(ProxyTypeDropDown);
	sel.selectByVisibleText("Auto");
	Reporter.log("PASS >> Proxy Type Auto Selected",true);
}

@FindBy(xpath = "//input[@id='proxy_pac_url']")
private WebElement ProxyPACURLEntered ;

public void ProxyPACURLEntered() throws InterruptedException {
	 sleep(2);
	 ProxyPACURLEntered.clear();
	 sleep(2);
	 ProxyPACURLEntered.sendKeys("www.google.com");
	 Reporter.log("PASS >> Proxy PAC URL Entered",true);	
	 sleep(2);
}

public void ProxySettingsModifiedJobName() throws InterruptedException {
	 sleep(2);
	 ProxySettingJobName.clear();
	 sleep(2);
	 ProxySettingJobName.sendKeys("Trials Proxy Settings Modified Job");
	 Reporter.log("PASS >> Trials Proxy Settings Modified Job Name Entered",true);	
	 sleep(2);
}

//wifi

@FindBy(xpath = "//span[@title='Wi-Fi Settings ']")
private WebElement WiFiSettingsVisible ;

public void WiFiSettingsVisible() throws InterruptedException {
	 sleep(2);
	 WiFiSettingsVisible.click();
	 Reporter.log("PASS >> Clicked on Wi-Fi Settings",true);	
	 sleep(2);
}

@FindBy(xpath = "//input[@id='job_wifi_name_input']")
private WebElement WiFiConfigurationSettingsJobName ;

public void WiFiConfigurationSettingsJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Configuration Settings Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

@FindBy(xpath = "//div[@id='wifi_addBtn']//i[@class='icn fa fa-plus']")
private WebElement WiFiConfigurationSettingsADDButton ;

public void WiFiConfigurationSettingsADDButton() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsADDButton.click();
	 sleep(2);
}

@FindBy(xpath = "//div[@class='input-group ct-w100']//input[@id='job_wifi_ssid_input']")
private WebElement WiFiConfigurationSettingsSSIDName ;

public void WiFiConfigurationSettingsSSIDName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsSSIDName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsSSIDName.sendKeys("42GearsUniFi");
	 Reporter.log("PASS >> WiFi Configuration Settings SSID Name Entered",true);	
	 sleep(2);
}

//Security Type

@FindBy(xpath = "//select[@id='job_wifi_security_input_windows_wifi']")
private WebElement WiFiConfigurationSettingsDropDown ;

public void SecurityTypeDropDown()throws InterruptedException{	
	Select sel = new Select(WiFiConfigurationSettingsDropDown);
	sel.selectByValue("open");
	Reporter.log("PASS >> Security Type Open Selected",true);
}

@FindBy(xpath = "//input[@id='job_wifi_autoConnect']")
private WebElement EnableWiFiConfigurationSettingsAutoConnect ;

public void EnableWiFiConfigurationSettingsAutoConnect() throws InterruptedException {
	 sleep(2);
	 EnableWiFiConfigurationSettingsAutoConnect.click();
	 Reporter.log("PASS >> Enable Auto Connect CheckBox",true);
	 sleep(2);
}

@FindBy(xpath = "//button[@id='okbtn']")
private WebElement WiFiConfigurationSettingsOKButton ;

@FindBy(xpath = "//button[@id='ok_btn']")
private WebElement WiFiConfigurationOKbutton ;

public void WiFiConfigurationSettingsOKButton() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsOKButton.click();
	 sleep(2);
	 WiFiConfigurationOKbutton.click();	 
}

//
@FindBy(xpath = "//div[@class='input-group ct-w100 ct-pwdInput-group']//input[@id='job_wifi_pass_input']")
private WebElement WiFiConfigurationWEPPassword ;

public void SecurityTypeWEPDropDown()throws InterruptedException{	
	 Select sel = new Select(WiFiConfigurationSettingsDropDown);
	 sel.selectByValue("WEP");
	 Reporter.log("PASS >> Security Type WEP Selected",true);
	 sleep(2);
	 WiFiConfigurationWEPPassword.clear();
	 sleep(2);
	 WiFiConfigurationWEPPassword.sendKeys("phobos42gears");
	 Reporter.log("PASS >> WEP Password Entered",true);
}

public void WiFiConfigurationSettingsWEPJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Configuration Settings WEP Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

public void WiFiConfigurationSettingsWPA2PersonalJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Settings WPA2Personal Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

public void SecurityTypeWPA2PersonalDropDown()throws InterruptedException{	
	 Select sel = new Select(WiFiConfigurationSettingsDropDown);
	 sel.selectByValue("WPA2PSK");
	 Reporter.log("PASS >> Security Type WPA2-Personal Selected",true);
	 sleep(2);
	 WiFiConfigurationWEPPassword.clear();
	 sleep(2);
	 WiFiConfigurationWEPPassword.sendKeys("phobos42gears");
	 Reporter.log("PASS >> WPA2Personal Password Entered",true);
}

public void WiFiConfigurationSettingsWPA2EnterpriseJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Settings WPA2Enterprise Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

@FindBy(xpath = "//input[@id='job_wifi_username']")
private WebElement WPA2EnterpriseUserName ;

public void SecurityTypeWPA2EnterpriseDropDown()throws InterruptedException{	
	 Select sel = new Select(WiFiConfigurationSettingsDropDown);
	 sel.selectByValue("WPA2");
	 Reporter.log("PASS >> Security Type WPA2-Enterprise Selected",true);
	 sleep(2);
	 
	 WPA2EnterpriseUserName.clear();
	 sleep(2);
	 WPA2EnterpriseUserName.sendKeys("admin");
	 Reporter.log("PASS >> WPA2Personal Username Entered",true);
	 	 
	 WiFiConfigurationWEPPassword.clear();
	 sleep(2);
	 WiFiConfigurationWEPPassword.sendKeys("phobos42gears");
	 Reporter.log("PASS >> WPA2Personal Password Entered",true);
}

public void WiFiConfigurationHiddenNetworkJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Hidden Network Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

public void SecurityTypeHiddenNetworkDropDown()throws InterruptedException{	
	 Select sel = new Select(WiFiConfigurationSettingsDropDown);
	 sel.selectByValue("open");
	 Reporter.log("PASS >> Security Type open Selected",true);
	 sleep(2);
}

@FindBy(xpath = "//input[@id='job_wifi_hiddenNetwork']")
private WebElement EnableWiFiConfigurationSettingsHiddenNetwork ;

public void EnableWiFiConfigurationSettingsHiddenNetwork() throws InterruptedException {
	 sleep(2);
	 EnableWiFiConfigurationSettingsHiddenNetwork.click();
	 Reporter.log("PASS >> Enable Hidden Network CheckBox",true);
	 sleep(2);
}

public void WiFiConfigurationDisableAutoConnectJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Disable Auto Connect Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

public void DisableAutoConnectcheckbox()throws InterruptedException{
	
	boolean flag=EnableWiFiConfigurationSettingsAutoConnect.isSelected();
	if(flag==true)
	{	
		EnableWiFiConfigurationSettingsAutoConnect.click();
		sleep(2);		
	}	
	Reporter.log("PASS >> Disable Auto Connect Checkbox",true);
}

public void SecurityTypeDisableAutoConnectDropDown()throws InterruptedException{	
	 Select sel = new Select(WiFiConfigurationSettingsDropDown);
	 sel.selectByValue("open");
	 Reporter.log("PASS >> Security Type open Selected",true);
	 sleep(2);
}

public void WiFiConfigurationModifiedJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Modified Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

public void SecurityTypeModifiedDropDown()throws InterruptedException{	
	 Select sel = new Select(WiFiConfigurationSettingsDropDown);
	 sel.selectByValue("open");
	 Reporter.log("PASS >> Security Type open Selected",true);
	 sleep(2);
}

@FindBy(xpath = "//button[@id='ok_btn_modified']")
private WebElement WiFiConfigurationOKbuttonModified ;

public void WiFiConfigurationOKButton() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsOKButton.click();
	 sleep(2);
	 WiFiConfigurationOKbuttonModified.click();	 
}

public void WiFiConfigurationDeleteJobName() throws InterruptedException {
	 sleep(2);
	 WiFiConfigurationSettingsJobName.clear();
	 sleep(2);
	 WiFiConfigurationSettingsJobName.sendKeys("Trials WiFi Delete Job");
	 Reporter.log("PASS >> WiFi Configuration Settings Job Name Entered",true);	
	 sleep(2);
}

@FindBy(xpath = "//div[@id='job_delete']//i[@class='icn fa fa-minus-circle']")
private WebElement ClickonDeletebutton ;

@FindBy(xpath = "//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")
private WebElement ClickonDeleteYesButton ;

public void ClickonDeletebutton() throws InterruptedException {
	 sleep(2);
	 ClickonDeletebutton.click();
	 sleep(2);
	 ClickonDeleteYesButton.click();
}

@FindBy(xpath = "//span[text()='Job deleted successfully.']")
private WebElement JobDeletedSuccessfully ;

public void JobDeletedSuccessfully() throws InterruptedException {
	boolean isdisplayed = true;

	try {
		JobDeletedSuccessfully.isDisplayed();

	} catch (Exception e) {
		isdisplayed = false;

	}

	Assert.assertTrue(isdisplayed, "FAIL >> 'Job Deleted successfully' is not displayed");
	Reporter.log("PASS >> 'Job Deleted successfully' is displayed", true);
	sleep(5);
}





























//kavya////////////////////////////////////////////////////////////////////////////////////////////////////

@FindBy(xpath = "//div[@id='compliance_job']")
private WebElement ComplainceJobWin;

@FindBy(xpath = "//button[@id='cancelPanel1']")
private WebElement WindowsJobSecCloseBtn;

@FindBy(xpath = "//span[text()='Windows Health Attestation']")
private WebElement WindowsHealthAttestationOp;

@FindBy(xpath = "//button[@id='cancBtn']")
private WebElement ComplainceJobTabCloseBtn;


@FindBy(xpath = "//span[@title='Time Fence']")
private WebElement TimeFencejob;

@FindBy(xpath = "//span[@class='tit'][normalize-space()='Select Fence']")
private WebElement SelectFenceb;

@FindBy(xpath = "//li[@class='jobIn_tab tabs']//a")
private WebElement jobIntabtabs;

@FindBy(xpath = "//li[@class='jobOut_tab tabs']//a")
private WebElement jobOuttabtabs;

@FindBy(xpath = "//button[@id='timeFenc_save']")
private WebElement timeFencsave;

@FindBy(xpath = "//div[@id='timefencing_modal']//button[@aria-label='Close']")
private WebElement CloseBtn;

@FindBy(xpath = "//span[@class='input-group-addon labelno-bg ct-model-label mandatory'][normalize-space()='Start Time']")
private WebElement StartTime;

@FindBy(xpath = "//span[@class='input-group-addon labelno-bg ct-model-label mandatory'][normalize-space()='End Time']")
private WebElement EndTime;


@FindBy(xpath = "//span[normalize-space()='Optional Time']")
private WebElement OptionalTime;

@FindBy(xpath = "//button[normalize-space()='Add Fence']")
private WebElement AddFence;

@FindBy(xpath = "//div[@id='proxy_settings']")
private WebElement ProxySettingsJob;

@FindBy(xpath = "//div[@class='modal-body proxy_type_auto']//span[@class='input-group-addon mandatory labelno-bg ct-model-label'][normalize-space()='Job Name']")
private WebElement JobNameInProxySetttings;

@FindBy(xpath = "//span[normalize-space()='Enable Proxy']")
private WebElement EnableProxy;

@FindBy(xpath = "//div[@class='modal-body proxy_type_auto']")
private WebElement proxytype;

@FindBy(xpath = "//input[@id='proxy_pac_url']")
private WebElement proxypacurl;

@FindBy(xpath = "//div[@id='proxySettings_modal']//button[@aria-label='Close']")
private WebElement proxySettingsclose;

@FindBy(xpath = "//div[@id='firewall_policy']")
private WebElement FireWallPolicyJob;

@FindBy(xpath = "//div[@class='form-group fwborderbtm']//span[@class='input-group-addon mandatory labelno-bg ct-model-label'][normalize-space()='Job Name']")
private WebElement JobNameInFireWallPolicy;

@FindBy(xpath = "//span[normalize-space()='Enable']")
private WebElement EnableOP;

@FindBy(xpath = "//span[text()='Domain List']")
private WebElement DomainList;

@FindBy(xpath = "//span[text()='Domain Names  ']")
private WebElement DomainNames  ;

@FindBy(xpath = "//div[@id='firewallPolicy_modal']//button[@aria-label='Close']")
private WebElement FireWallPolicyJobclose;


public void verifyComplainceJob_Windows() {
	try {
		ComplainceJobWin.isDisplayed();
		Reporter.log("PASS >> Compliance job is present in the  job section of windows",true);
		WindowsJobSecCloseBtn.click();
	}catch (Exception e) {
		WindowsJobSecCloseBtn.click();
		ALib.AssertFailMethod("FAIL >> Compliance job is not present in the  job section of windows");
	}
}

public void clickOnComplainceJobWin() throws InterruptedException {
	ComplainceJobWin.click();
	sleep(2);
}
JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
public void verifyWindowsHealthAttestationOp() {	
	je.executeScript("arguments[0].scrollIntoView()", WindowsHealthAttestationOp);		
	try {
		WindowsHealthAttestationOp.isDisplayed();
		Reporter.log("PASS >> Windows Health Attention is present at the bottom of compliance rule.",true);
		ComplainceJobTabCloseBtn.click();
		WindowsJobSecCloseBtn.click();
	}catch (Exception e) {
		ComplainceJobTabCloseBtn.click();
		WindowsJobSecCloseBtn.click();
		ALib.AssertFailMethod("FAIL >> Windows Health Attention is not present at the bottom of compliance rule.");
	}
}

public void clickOnWindowsHealthAttestationOp() throws InterruptedException {
	je.executeScript("arguments[0].scrollIntoView()", WindowsHealthAttestationOp);
	WindowsHealthAttestationOp.click();
	sleep(2);
}

public void clickOnTimeFencejob() throws InterruptedException
{
	TimeFencejob.click();
	sleep(2);
}

public void verifyOptionsInFenceInTimeFencejob() throws InterruptedException
{
	try {
		boolean SelectFence = SelectFenceb.isDisplayed();
		String pass1 = "PASS >> Select Fence is displayed";
		String fail1 = "FAIL >> Select Fence is not displayed";
		ALib.AssertTrueMethod(SelectFence, pass1, fail1);
		sleep(2);
		boolean jobIntabtab = jobIntabtabs.isDisplayed();
		String pass2 = "PASS >> job In tabs is displayed";
		String fail2 = "FAIL >> job In tabs is not displayed";
		ALib.AssertTrueMethod(jobIntabtab, pass2, fail2);
		sleep(2);
		boolean jobOuttabtab = jobOuttabtabs.isDisplayed();
		String pass3 = "PASS >> job Out tabs is displayed";
		String fail3 = "FAIL >> job Out tabs is not displayed";
		ALib.AssertTrueMethod(jobOuttabtab, pass3, fail3);
		sleep(2);
		boolean timeFencsaveBtn = timeFencsave.isDisplayed();
		String pass4 = "PASS >> time Fence save is displayed";
		String fail4 = "FAIL >> time Fence save is not displayed";
		ALib.AssertTrueMethod(timeFencsaveBtn, pass4, fail4);
		sleep(2);
		CloseBtn.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
	}catch (Exception e) {
		CloseBtn.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
		ALib.AssertFailMethod("FAIL >> All the Options are not displayed");
	}
}

public void clickOnSelectFence() throws InterruptedException
{
	SelectFenceb.click();
	sleep(2);
}

public void verifySelectFenceOptions() throws InterruptedException
{
	try {
		boolean StartTimeOp = StartTime.isDisplayed();
		String pass1 = "PASS >> Start Time is displayed";
		String fail1 = "FAIL >> Start Time is not displayed";
		ALib.AssertTrueMethod(StartTimeOp, pass1, fail1);
		sleep(2);
		boolean EndTimeOP = EndTime.isDisplayed();
		String pass2 = "PASS >> End Time is displayed";
		String fail2 = "FAIL >> End Time is not displayed";
		ALib.AssertTrueMethod(EndTimeOP, pass2, fail2);
		sleep(2);
		boolean OptionalTime = timeFencsave.isDisplayed();
		String pass3 = "PASS >> time Fencsave is displayed";
		String fail3 = "FAIL >> time Fencsave is not displayed";
		ALib.AssertTrueMethod(OptionalTime, pass3, fail3);
		sleep(2);
		boolean AddFenceOP = AddFence.isDisplayed();
		String pass4 = "PASS >> Add Fence is displayed";
		String fail4 = "FAIL >> Add Fence is not displayed";
		ALib.AssertTrueMethod(AddFenceOP, pass4, fail4);
		sleep(2);
		CloseBtn.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
	}catch (Exception e) {
		CloseBtn.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
		ALib.AssertFailMethod("FAIL >> All the Options are not displayed");
	}
}

public void clickOnProxySettings() throws InterruptedException {
	ProxySettingsJob.click();
	sleep(2);
}
	
public void verifyOptionInProxySettings() throws InterruptedException
{
	try {
		boolean JobName = JobNameInProxySetttings.isDisplayed();
		String pass1 = "PASS >> JobNameInProxySetttings is displayed";
		String fail1 = "FAIL >> JobNameInProxySetttings is not displayed";
		ALib.AssertTrueMethod(JobName, pass1, fail1);
		sleep(2);
		boolean EnableProxyOP = EnableProxy.isDisplayed();
		String pass2 = "PASS >> EnableProxy is displayed";
		String fail2 = "FAIL >> EnableProxy is not displayed";
		ALib.AssertTrueMethod(EnableProxyOP, pass2, fail2);
		sleep(2);
		boolean proxytypeOP = proxytype.isDisplayed();
		String pass3 = "PASS >> proxytype is displayed";
		String fail3 = "FAIL >> proxytype is not displayed";
		ALib.AssertTrueMethod(proxytypeOP, pass3, fail3);
		sleep(2);
		boolean proxypacurlOP = proxypacurl.isDisplayed();
		String pass4 = "PASS >> proxypacurl is displayed";
		String fail4 = "FAIL >> proxypacurl is not displayed";
		ALib.AssertTrueMethod(proxypacurlOP, pass4, fail4);
		sleep(2);
		proxySettingsclose.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
	}catch (Exception e) {
		proxySettingsclose.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
		ALib.AssertFailMethod("FAIL >> All the Options are not displayed");
	}
}

public void clickOnFireWallP0licyJob() throws InterruptedException
{
	FireWallPolicyJob.click();
	sleep(2);
}

public void verifyOptionInFireWallPolicyJob() throws InterruptedException
{
	try {
		boolean JobName = JobNameInFireWallPolicy.isDisplayed();
		String pass1 = "PASS >> JobNameInFireWallPolicy is displayed";
		String fail1 = "FAIL >> JobNameInFireWallPolicy is not displayed";
		ALib.AssertTrueMethod(JobName, pass1, fail1);
		sleep(2);
		boolean EnableOp = EnableOP.isDisplayed();
		String pass2 = "PASS >> Enable Option is displayed";
		String fail2 = "FAIL >> Enable Option is not displayed";
		ALib.AssertTrueMethod(EnableOp, pass2, fail2);
		sleep(2);
		boolean DomainListOP = DomainList.isDisplayed();
		String pass3 = "PASS >> DomainList is displayed";
		String fail3 = "FAIL >> DomainList is not displayed";
		ALib.AssertTrueMethod(DomainListOP, pass3, fail3);
		sleep(2);
		boolean DomainNamesOP = DomainNames.isDisplayed();
		String pass4 = "PASS >> DomainNames is displayed";
		String fail4 = "FAIL >> DomainNames is not displayed";
		ALib.AssertTrueMethod(DomainNamesOP, pass4, fail4);
		sleep(2);
		FireWallPolicyJobclose.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
	}catch (Exception e) {
		FireWallPolicyJobclose.click();
		sleep(2);
		WindowsJobSecCloseBtn.click();
		ALib.AssertFailMethod("FAIL >> All the Options are not displayed");
	}
}

 
	 
//===========================================Mithilesh
}
