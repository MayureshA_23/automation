package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class NFC_Knox extends Initialization{
	  @Test(priority=1, description="verify_DontCare_NFC_TC_AJ_116") public void
	  verify_DontCare_NFC_TC_AJ_116() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
	  commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
	  androidJOB.clickOnJobs(); androidJOB.clickNewJob();
	  androidJOB.clickOnAndroidOS(); runScript.ClickOnRunscript();
	  runScript.clickOnKnoxSection(); runScript.clickOnTurnON_OFF_NFC();
	  runScript.sendPackageNameORpath("0"); runScript.ClickOnValidateButton();
	  runScript.ScriptValidatedMessage(); runScript.ClickOnInsertButton();
	  runScript.RunScriptName("NFCDontCare"); commonmethdpage.ClickOnHomePage();
	  androidJOB.SearchDeviceInconsole(Config.DeviceName); commonmethdpage.ClickOnApplyButton();
	  androidJOB.SearchField("NFCDontCare");
	  androidJOB.CheckStatusOfappliedInstalledJob(Config.RunScriptNFCDontCare,Config.TimeToDeployRunscriptJob);
	 
	  runScript.OpenSettingsPage();
	  runScript.clickOnConnection(); 
	  runScript.DontcareNFCcondition();
	  androidJOB.ClickOnHomeButtonDeviceSide();
	  Reporter.log("Turn ON or Turn OFF NFC on device Dont care NFC condition",
	  true); }
	 
	
}
