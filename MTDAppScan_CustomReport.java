package CustomReportsScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
// Lucky Patcher and 	FlashLight Applications should be installed in device
public class MTDAppScan_CustomReport extends Initialization {
	@Test(priority = '0', description = "Enable SystemScan And MTD in DataAnalytics")
	public void VerifyEnableSystemScanAndMTDAppScan()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSystemScanButton();
		dynamicjobspage.CloseSystemScanWindow();
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnDataAnalyticsTab();
		accountsettingspage.EnableDataAnalyticsCheckBox();
		accountsettingspage.ClickOnAddAnalyticsButton();
		accountsettingspage.SendingDataAnalyticsName("MTD App Scan");
		accountsettingspage.SendingTagName("App Package Name");
		accountsettingspage.ClickOnAddFiledButton();
		accountsettingspage.SendingFieldName("App Name");
		accountsettingspage.SelectingFieldType("string");
		accountsettingspage.ClickingonAddFieldAddButton();
		accountsettingspage.ClickOnAddFiledButton();
		accountsettingspage.SendingFieldName("App Category");
		accountsettingspage.SelectingFieldType("string");
		accountsettingspage.ClickingonAddFieldAddButton();
		accountsettingspage.ClickonAnalyticsSaveButton();
		accountsettingspage.VerifyNewlyAddedAnalyticsSecretKey();
		accountsettingspage.ClickOnDataAnalyticsApplyButton();
		
	}

	@Test(priority = '1', description = "Verify MTD App Scan Custom reports")
	public void VerifyMTDAppScanForHome() throws InterruptedException {
		Reporter.log("\nVerify MTD App Scan Custom report for 'Home' Group", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("MTD App Scan");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("MTDAppScan CustomRep for Home Group", "test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("MTDAppScan CustomRep for Home Group");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("MTDAppScan CustomRep for Home Group");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("MTDAppScan CustomRep for Home Group");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("MTDAppScan CustomRep for Home Group");
		reportsPage.WindowHandle();
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.ClickOnSearchReportButtonInsidedViewedReport_MTDAppScan();
		customReports.CheckingMTDAppScanInfo();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '2', description = "Verify MTD App Scan Custom reports for Sub group")
	public void VerifyMTDAppScanForGroup() throws InterruptedException {
		Reporter.log("\nVerify MTD App Scan Custom reports for Sub group", true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("MTD App Scan");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("MTDAppScan CustomRep for Sub group", "test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("MTDAppScan CustomRep for Sub group");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("MTDAppScan CustomRep for Sub group");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("MTDAppScan CustomRep for Sub group");
		reportsPage.WindowHandle();
		customReports.ClickOnSearchReportButtonInsidedViewedReport_MTDAppScan();
		customReports.CheckingMTDAppScanInfo();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				String FailedWindow = Initialization.driver.getWindowHandle();
				if (FailedWindow.equals(customReports.PARENTWINDOW)) {
					commonmethdpage.ClickOnHomePage();
				} else {
					reportsPage.SwitchBackWindow();
				}
			} catch (Exception e) {

			}
		}
	}
}
