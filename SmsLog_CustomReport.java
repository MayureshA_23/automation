package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class SmsLog_CustomReport extends Initialization {
	
	/*@Test(priority='0',description="Enabling SMSLog DynamicJob")
	public void EnablingSMSLogDynamicJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_On();
	    dynamicjobspage.VerifyOfSMSUpdateInterval();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();
	    commonmethdpage.ClickOnAllDevicesButton();
	}*/
@Test(priority='1',description="Verify Sort by and Group by for SMS Log")
public void VerifyingSmsLogReportForAllDataAfterSorting() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for SMS Log",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSMSLogTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSMSLogTable);	
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SmsLog CustomReport for SortBy And GroupBy","test");
		customReports.Selectvaluefromsortbydropdown(Config.SelectSmsType);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SmsLog CustomReport for SortBy And GroupBy");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SmsLog CustomReport for SortBy And GroupBy");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SmsLog CustomReport for SortBy And GroupBy");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SmsLog CustomReport for SortBy And GroupBy");
		reportsPage.WindowHandle();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
			
	}
@Test(priority='2',description="Verify generating SMS Log Custom report without filters for 'Current Date'")
public void VerifyingSmsLogReportForSmsLogDataInParticularGroup() throws InterruptedException
	{Reporter.log("\nVerify generating SMS Log Custom report without filters for 'Current Date'",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSMSLogTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSMSLogTable);	
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SMSLog Customreport without filters","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("SMSLog Customreport without filters");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SMSLog Customreport without filters");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectTodayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SMSLog Customreport without filters");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SMSLog Customreport without filters");
		reportsPage.WindowHandle();
		customReports.VarifyingCurrentDateInViewReport_SmsLog();
		customReports.CheckingSmsLogData();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
@Test(priority='3',description="Verify generating SMS Log Custom report with filters for 'Current Date'")
public void VerifyingSmsLogReportForSmsLogData() throws InterruptedException
	 {Reporter.log("\nVerify generating SMS Log Custom report with filters for 'Current Date'",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("SMS Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSMSLogTable);	
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SMSLog Customreport with filters for Current Date","test");
		customReports.Selectvaluefromdropdown(Config.SelectSMSLogTable);
		customReports.SelectValueFromColumn(Config.SelectNumber);
		customReports.SelectOperatorFromDropDown(Config.SelectEqualOperator);	
		customReports.SendValueToTextfield(Config.EnterContactNumber);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("SMSLog Customreport with filters for Current Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SMSLog Customreport with filters for Current Date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectTodayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SMSLog Customreport with filters for Current Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SMSLog Customreport with filters for Current Date");
		reportsPage.WindowHandle();
		customReports.VarifyingCurrentDateInViewReport_SmsLog();
		customReports.CheckingContactNumber();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();		
}
@Test(priority='4',description="Verify generating SMS Log Custom report with filters for '1week Date'")
public void VerifyingSmsLogForOneWeekDate() throws InterruptedException
	{Reporter.log("\nVerify generating SMS Log Custom report with filters for '1week Date'",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSMSLogTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSMSLogTable);	
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SMSLog CustomRep with filters for a week Date","test");
		customReports.Selectvaluefromdropdown(Config.SelectSMSLogTable);
		customReports.SelectValueFromColumn("Datetime");
		customReports.SelectOneWeekDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("SMSLog CustomRep with filters for a week Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SMSLog CustomRep with filters for a week Date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SMSLog CustomRep with filters for a week Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SMSLog CustomRep with filters for a week Date");
		reportsPage.WindowHandle();
		customReports.VarifyingOneWeekDateInSmsLogReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='5',description="Verify generating SMS Log Custom report with filters for 'Yesterday' date")
public void VerifyingSmsLogReportForAllData() throws InterruptedException
	 {Reporter.log("\nVerify generating SMS Log Custom report with filters for 'Yesterday' date",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSMSLogTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSMSLogTable);	
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SMSLog CustomRep with filters for Yesterday date","test");
		customReports.Selectvaluefromdropdown(Config.SelectSMSLogTable);
		customReports.SelectValueFromColumn(Config.SelectName);
		customReports.SelectOperatorFromDropDown(Config.SelectEqualOperator);	
		customReports.SendValueToTextfield(Config.SendContactName);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("SMSLog CustomRep with filters for Yesterday date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SMSLog CustomRep with filters for Yesterday date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();	
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SMSLog CustomRep with filters for Yesterday date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SMSLog CustomRep with filters for Yesterday date");
		reportsPage.WindowHandle();
		customReports.VarifyingYesterDate2InViewReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}else {
					reportsPage.SwitchBackWindow();
				}
			} catch (Exception e) {
				
			}
		}
	}
}
