package OnDemandReports_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class DeviceHistory extends Initialization {

	@Test(priority = 1, description = "Generate And View the 'Device History Report'  for Selected 'Device' where report is generated for Custom Range")
	public void VefifyDeviceHistoryReportHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("1.Generate And View the 'Device History Report'  for Selected 'Sub' group where report is generated for Custom Range",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHistoryReport();
		reportsPage.VerifyDateText();
		reportsPage.VerifyDeviceHistoryReportSelectDeviceText();
		reportsPage.ClickOnSelectDeviceButton();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.BatteryPercentColumnVerificationDeviceHistoryReport();
		reportsPage.BackupBatteryPercentColumnVerificationDeviceHistoryReport();
		reportsPage.AvailablePhysicalMemoryColumnVerificationDeviceHistoryReport();
		reportsPage.AvailableStoragePercentColumnVerificationDeviceHistoryReport();
		reportsPage.WifiSignalStrengthColumnVerificationDeviceHistoryReport();
		reportsPage.DataUsageColumnVerificationDeviceHistoryReport();
		reportsPage.NetworkOperatorColumnVerificationDeviceActivityReport(Config.NetworkOperator);
		reportsPage.MobileSignalColumnVerificationDeviceHistoryReport();
		//reportsPage.CPUUsageColumnVerificationDeviceActivityReport();
		reportsPage.TemperatureColumnVerificationDeviceHistoryReport();
		reportsPage.LastSystemScanColumnVerificationDeviceActivityReport(Config.LastSystemScan);
		reportsPage.ThreatsCountColumnVerificationDeviceActivityReport(Config.threatsCount);
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 2, description = "Verify Availibility of All Columns in device history report")
	public void VerifyAvailibilityOfAllColumnsInDeviceHistoryReport() throws InterruptedException {
		Reporter.log("\n2.Verify Availibility of All Columns in device history report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHistoryReport();
		reportsPage.VerifyDateText();
		reportsPage.VerifyDeviceHistoryReportSelectDeviceText();
		reportsPage.ClickOnSelectDeviceButton();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyDeviceHistoryReportColumns();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 3, description = "Verify Availibility of All Columns in device history report")
	public void VerifyColumnsValueInDeviceHistoryReport() throws InterruptedException {
		Reporter.log("\n3.Verify Availibility of All Columns in device history report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHistoryReport();
		reportsPage.VerifyDateText();
		reportsPage.VerifyDeviceHistoryReportSelectDeviceText();
		reportsPage.ClickOnSelectDeviceButton();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.BatteryPercentColumnVerificationDeviceHistoryReport();
		reportsPage.BackupBatteryPercentColumnVerificationDeviceHistoryReport();
		reportsPage.AvailablePhysicalMemoryColumnVerificationDeviceHistoryReport();
		reportsPage.AvailableStoragePercentColumnVerificationDeviceHistoryReport();
		reportsPage.WifiSignalStrengthColumnVerificationDeviceHistoryReport();
		reportsPage.DataUsageColumnVerificationDeviceHistoryReport();
		reportsPage.NetworkOperatorColumnVerificationDeviceActivityReport(Config.NetworkOperator);
		reportsPage.MobileSignalColumnVerificationDeviceHistoryReport();
		//reportsPage.CPUUsageColumnVerificationDeviceActivityReport();
		reportsPage.TemperatureColumnVerificationDeviceHistoryReport();
		reportsPage.LastSystemScanColumnVerificationDeviceActivityReport(Config.LastSystemScan);
		reportsPage.ThreatsCountColumnVerificationDeviceActivityReport(Config.threatsCount);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 4, description = "Download for Device History Report")
	public void VerifyDownloadingDeviceActivityReport() throws InterruptedException {
		Reporter.log("\n4.Download for Device History Report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnDeviceHistoryReport();
		reportsPage.VerifyDateText();
		reportsPage.VerifyDeviceHistoryReportSelectDeviceText();
		reportsPage.ClickOnSelectDeviceButton();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}
	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();
			
		}
	}

}