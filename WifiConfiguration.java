package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class WifiConfiguration extends Initialization{
	
	@Test(priority='1',description="1.To verify clicking on Wifi Configuration") 
    public void VerifyClickingOnWifiConfiguration() throws InterruptedException {
    Reporter.log("To verify clicking on Wifi Configuration");
    profilesAndroid.ClickOnProfile();
	profilesiOS.clickOniOSOption();
	profilesiOS.AddProfile();
	profilesiOS.ClickOnWifiConfigurationProfile();
}
	
	@Test(priority='2',description="1.To Verify warning message Saving a Wifi Configuration Profile without Profile Name")
	public void VerifyWarningMessageOnSavingWifiConfigurationProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Wifi Configuration Profile without Profile Name========");
		profilesiOS.ClickOnConfigureButtonWiFiConfigurationProfile();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='3',description="1.To Verify warning message Saving a Wifi Configuration Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingWiFiConfigurationProfileWihtoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Wifi Configuration Profile without entering all the fields=========");
		profilesiOS.EnterWifiConfigurationProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	@Test(priority='4',description="to verify clicking on add button of Wifi Configuration Profile ")
	public void VerifyClickOnAddbutton_WifiConfiguration() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify clicking on add button of Configuration Profile application list without selecting an application");
		//profilesiOS.clickOnBlackListAppConfigure();
		profilesiOS.ClickOnAddButton_WifiConfiguration();
		profilesiOS.isWifiConfigurationWindowDisplayed();
		
	}
	
	@Test(priority='5',description="to verify the warning message on Saving Wifi Configuration without entering SSID")
	public void VerifyWarningMessage_SavingWithoutSSID() throws InterruptedException
	{
		Reporter.log("to verify the warning message on Saving Wifi Configuration without entering SSID");
		profilesiOS.ClickOnSaveButton_WifiConfiguration();
		profilesiOS.warningMessageWithoutSSIDorPasswordorServerURL();
	}
	
	@Test(priority='6',description="to verify the choosing Wifi Configuration ")
	public void VerifyChoosingWifiConfiguration() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("to verify the choosing Wifi Configuration");
		profilesiOS.EnterSSID_WifiConfiguration();
		profilesiOS.ClickOnSecuritytypeDropDownWifiConfigurationandChoose();
		//profilesiOS.ChooseProxySetupDropDown();
	}
	
	@Test(priority='7',description="to verify the warning message without WIFI password")
	public void VerifyWarningWithoutWifiPassword() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("to verify the saving the  Wifi Configuration profiles ");
		profilesiOS.ClickOnSaveButton_WifiConfiguration();
		profilesiOS.warningMessageWithoutSSIDorPasswordorServerURL();
        //profilesAndroid.NotificationOnProfileCreated();
	}
	@Test(priority='8',description="to verify the warning message without Server and Port")
	public void VerifyWarningWithoutServerAndPort() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("to verify the warning message without Server and Port ");
		profilesiOS.EnterPassword_WiFiConfiguration();
		profilesiOS.ClickOnSaveButton_WifiConfiguration();
		profilesiOS.warningMessageWithoutSSIDorPasswordorServerURL();
     
  }
	@Test(priority='9',description="to verify the Saving Wifi Configuration profile")
	public void VerifySavingWifiConfigurationProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("to verify the Saving Wifi Configuration profile");
		profilesiOS.ClickOnSavebutton();
		profilesAndroid.NotificationOnProfileCreated();
		
}
}
		
	
	


