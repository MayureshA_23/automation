package QuickActionToolBar_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//Pass DownloadPath in Config as per your local machine download path
public class ExportANDBlackList_MasterTestCase extends Initialization
{
     @Test(priority=1,description="Verify Export for Home group in Quick Action Tool Bar.")
     public void VerifyExportInQAT() throws EncryptedDocumentException, InvalidFormatException, InterruptedException, IOException
     {
    	 Reporter.log("1.Verify Export for Home group in Quick Action Tool Bar.",true);
    	 quickactiontoolbarpage.ClickingOnExportButtonAndVerifyingFileDownload(Config.downloadpath);
     }
     
     @Test(priority=2,description="Verify Export for sub group in Quick Action Tool Bar.")
     public void VerifyExportForSubGroupInQAT() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
     {
    	 Reporter.log("\n2.Verify Export for sub group in Quick Action Tool Bar.",true);
 		 MacOSDeviceInfo.ClickOnNewGroup();
 		 MacOSDeviceInfo.SendingGroupName("#Automation_Group");
 		 MacOSDeviceInfo.ClickOnAllDeviceTab();
 		 commonmethdpage.SearchDevice(Config.DeviceName);
 		 quickactiontoolbarpage.ClickingOnMoveToGroup(quickactiontoolbarpage.DeviceName);
 		 quickactiontoolbarpage.MovingDeviceToGroup("#Automation_Group",Config.DeviceName);
    	 quickactiontoolbarpage.ClickingOnExportButtonAndVerifyingFileDownload(Config.downloadpath); 
    	 MacOSDeviceInfo.DeletingTheGroup();
     }
     
   
}
