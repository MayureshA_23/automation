package Settings_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;


// a filetransfer job with name 'AutommationDontdeleteAndroid' should be present
// an iOS text message job with name 'AutommationDontdeleteiOS' should be present
// a windows install job with name 'AutommationDontdeleteWindows' should be present
// a MacOS reboot job with name 'AutommationDontdeletemacOS' should be present
// anyOS text message with name 'AutommationDontdeleteAnyOS' should be present

// a linux text message job with name 'AutommationDontDeleteLinux' should be present
public class Customized_ToolBar extends Initialization {
	
	
	//Comment this testcase if jobs are already created for all platforms
//    @Test(priority=1,description="Precondition: Creating jobs for all platform")
//	public void PreconditionCreatingjobsforallplatform() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.clickOnFileTransferJob();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.enterJobName(Config.Android_FileTransferJob);
//		androidJOB.clickOnAdd();
//		androidJOB.browseFile();
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.UploadcompleteStatus();
//		androidJOB.clickNewJob();
//		accountsettingspage.clickOnAnyOS();
//		androidJOB.clickOnTextMessageJob();
//		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.AnyOSTextMessageJob, Config.Customize_Sub,Config.Customize_Message);
//		androidJOB.ClickOnGetReadNotification();
//		androidJOB.ClickOnOkButton();
//		androidJOB.JobCreatedMessage();
//        androidJOB.clickNewJob();
//		accountsettingspage.clickOniOS();
//		androidJOB.clickOnTextMessageJob();
//		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.iOS_TextMessageJob, Config.Customize_Sub,Config.Customize_Message);
//		androidJOB.ClickOnOkButton();
//		androidJOB.JobCreatedMessage();
//		androidJOB.clickNewJob();
//		accountsettingspage.clickOnMacOS();
//		accountsettingspage.ClickOnRebootJob(Config.MacOS_RebootJob);
//		androidJOB.ClickOnOkButton();
//		androidJOB.JobCreatedMessage();
//		androidJOB.clickNewJob();
//		accountsettingspage.clickOnWindowsOS();
//		androidJOB.ClickOnInstallApplication();
//		androidJOB.enterJobName(Config.Windows_InstallJob);
//		androidJOB.clickOnAdd();
//		androidJOB.browseFile();
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.UploadcompleteStatus();
//		
//		androidJOB.clickNewJob();
//		accountsettingspage.clickOnLinuxOS();
//		androidJOB.clickOnTextMessageJob();
//		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.Linux_TextMessageJob, Config.Customize_Sub,Config.Customize_Message);
//		androidJOB.ClickOnOkButton();
//		androidJOB.JobCreatedMessage();
//
//	}
	
//	@Test(priority=2,description="Verify Customized tool bar Window")
//	public void VerifyCustomizedtoolbarWindow() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.verifyPredefinedJobs();
//		accountsettingspage.userdefinedAddButton();
//	}
	
//	@Test(priority=3,description="Verify Enabling predefined jobs from the Dynamic job")
//	public void VerifyEnablingpredefinedjobsfromtheDynamicjob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		accountsettingspage.verifyEnabledOfPredefinedjobs();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		accountsettingspage.verifyPredefinedJobsinConsoleAndroid();
//		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
//		accountsettingspage.verifyPredefinedJobsinConsoleWindows();
//		commonmethdpage.SearchDevice(Config.iOSDeviceName);
//		accountsettingspage.verifyPredefinedJobsinConsoleMac_Os();
//	}
	
//	@Test(priority=4,description="Verify Disabling predefined jobs from the Dynamic job")
//	public void VerifyDisablingpredefinedjobsfromtheDynamicjob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.verifySomeOfEnabledPredefinedjobsAndDisable();
//		accountsettingspage.saveCustomTool();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDeviceInconsole();
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifyUncheckPredefinedjobsinConsoleAndroid();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickOnDisabledjobsToEnable();
//		accountsettingspage.saveCustomTool();
//	}
	
		
	
//	@Test(priority=5,description="Verify Adding User-defined jobs")
//	public void VerifyAddingUserdefinedjobs() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnHomePage(); 
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickonAddButton();
//		accountsettingspage.enterTheJobName(Config.Customize_jobnameAndroid);
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.SelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		accountsettingspage.SearchJobToAddInCustomizeToolbar(Config.Android_FileTransferJob);
//		commonmethdpage.Selectjob(Config.Android_FileTransferJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifySavedCustomizeJobAndroid();
//	}

	
//	@Test(priority=6,description="Verify Adding User defined job for android.")
//	public void VerifyAddingUserdefinedjobforAndroidInConsole() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDeviceInconsole();		
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifySavedCustomizeJobInConsoleAndroid();
//	
//	}
	
	
//	@Test(priority=7,description="Verify Adding User defined job for iOS.")
//	public void VerifyAddingUserdefinedjobforiOS() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickonAddButton();
//		accountsettingspage.enterTheJobName(Config.Customize_jobname);
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.SelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		commonmethdpage.SearchJobToAddInCustomizeToolbar(Config.iOS_TextMessageJob);
//		commonmethdpage.Selectjob(Config.iOS_TextMessageJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifySavedCustomizeJob();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDevice(Config.iOSDeviceName);
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifySavedCustomizeJobInConsole();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.deleteUserDefinedjob(Config.Customize_jobname);
//	}
	

	
//	@Test(priority=8,description="Verify Adding User defined job for windows.")
//	public void VerifyAddingUserdefinedjobforwindows() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickonAddButton();
//		accountsettingspage.enterTheJobName(Config.Customize_jobname);
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.SelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		commonmethdpage.SearchJobToAddInCustomizeToolbar(Config.Windows_InstallJob);
//		commonmethdpage.Selectjob(Config.Windows_InstallJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifySavedCustomizeJob();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifySavedCustomizeJobInConsole();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.deleteUserDefinedjob(Config.Customize_jobname);
//	}
	
	
	
//	@Test(priority=9,description="Verify Adding User defined job for macOS.")
//	public void VerifyAddingUserdefinedjobformacOS() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickonAddButton();
//		accountsettingspage.enterTheJobName(Config.Customize_jobname);
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.SelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		commonmethdpage.SearchJobToAddInCustomizeToolbar(Config.MacOS_RebootJob);
//		commonmethdpage.Selectjob(Config.MacOS_RebootJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifySavedCustomizeJob();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage();
//		commonmethdpage.SearchDevice(Config.MacOSDeviceName);
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifySavedCustomizeJobInConsole();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.deleteUserDefinedjob(Config.Customize_jobname);
//	}
	
//	@Test(priority=10,description="Verify Delete userdefined job.")
//	public void VerifyDeleteuserdefinedjob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.scrolldowntoSavebuttonIncustomizetoolbar();
//		accountsettingspage.deleteUserDefinedjobWithConfirmation();
//		accountsettingspage.verifyDeletedjobIncustomizejob();
//		commonmethdpage.ClickOnHomePage();
//		//commonmethdpage.SearchDevice(Config.LinuxDeviceName);
//	}
	
	
//	@Test(priority=11,description="Verify Adding User defined job for Any OS.")
//	public void VerifyAddingUserdefinedjobforAnyOS() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickonAddButton();
//		accountsettingspage.enterTheJobName(Config.Customize_jobname);
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.SelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		commonmethdpage.SearchJobToAddInCustomizeToolbar(Config.AnyOSTextMessageJob);
//		commonmethdpage.Selectjob(Config.AnyOSTextMessageJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifySavedCustomizeJob();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifySavedCustomizeJobInConsole();
//	}
	
	
//	@Test(priority=12,description="Verify Edit userdefned job.")
//	public void VerifyEdituserdefnedjobforAnyOS() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.scrolldowntoSavebuttonIncustomizetoolbar();
//		accountsettingspage.ClickonEditUserDefinedjob();
//		accountsettingspage.UpdateJobInUserDefined();
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.UdateSelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		commonmethdpage.SearchJobToAddInCustomizeToolbar(Config.AnyOSTextMessageJob);
//		commonmethdpage.Selectjob(Config.AnyOSTextMessageJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifyModifiedCustomizeJob();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifyModifiedCustomizeJobInConsole();	
//	}
	

//	@Test(priority=13,description="Verify Enabling userdefined jobs from the Dynamic job")
//	public void VerifyEnablinguserdefinedjobsfromtheDynamicjob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.scrolldowntoSavebuttonIncustomizetoolbar();
//		accountsettingspage.verifySomeOfEnabledUserdefinedjobsAndDisable();
//		accountsettingspage.ClickOnUserDefinedJobs();
//		accountsettingspage.saveCustomTool();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifySavedCustomizeJobInConsoleAndroid();
//		accountsettingspage.verifyModifiedCustomizeJobInConsole();
//	}
	
	
	
//	@Test(priority=14,description="Verify Disabling userdefined jobs from the Dynamic job")
//	public void VerifyDisablinguserdefinedjobsfromtheDynamicjob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.scrolldowntoSavebuttonIncustomizetoolbar();
//		accountsettingspage.verifySomeOfEnabledUserdefinedjobsAndDisable();
//		accountsettingspage.saveCustomTool();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifyUncheckUserdefinedjobsinConsole();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.ClickOnUserDefinedJobs();
//		accountsettingspage.saveCustomTool();
//	}
	
//	@Test(priority=15,description="Verify Disabling all predefined and userdefined jobs from the Dynamic job")
//	public void VerifyDisablingallpredefinedanduserdefinedjobsfromtheDynamicjob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.verifySomeOfEnabledPredefinedjobsAndDisable();
//		accountsettingspage.scrolldowntoSavebuttonIncustomizetoolbar();
//		accountsettingspage.verifySomeOfEnabledUserdefinedjobsAndDisable();
//		accountsettingspage.saveCustomTool();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.refreshpage(); // this refresh is required otherwise clicking on more does not work when device is already searched
//		commonmethdpage.SearchDeviceInconsole();
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifyUncheckPredefinedjobsinConsoleAndroid();
//		accountsettingspage.verifyUncheckUserdefinedjobsinConsole();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickOnDisabledjobsToEnable();
//		accountsettingspage.ClickOnUserDefinedJobs();
//		accountsettingspage.saveCustomTool();
//		
//		accountsettingspage.deleteUserDefinedjob(Config.Customize_jobnameAndroid);
//		accountsettingspage.deleteUserDefinedjob(Config.Customize_jobname);
//	}
	
	
	
//	@Test(priority=16,description="Verify by deletingjob when that job is used in User defined toolbar")
//	public void VerifybydeletingjobwhenthatjobisusedinUserdefinedtoolbar() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickonAddButton();
//		accountsettingspage.enterTheJobName(Config.Customize_jobname);
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.SelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		commonmethdpage.SearchJobToAddInCustomizeToolbar(Config.AnyOSTextMessageJob);
//		commonmethdpage.Selectjob(Config.AnyOSTextMessageJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifySavedCustomizeJob();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.clickonjobtab();
//		commonmethdpage.jobpageholder(Config.AnyOSTextMessageJob);
//		commonmethdpage.Selectjobpageholderjob(Config.AnyOSTextMessageJob);
//		commonmethdpage.clickingondeletejob();
//		commonmethdpage.clickingonYesbuttonDeletejob();
//		commonmethdpage.verifyerrormessageWhileDeletingJob();
//		commonmethdpage.clickingoninformationOkbuttonDeletejob();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.deleteUserDefinedjob(Config.Customize_jobname);
//	}

//	@Test(priority=17,description="Verify by custom tool bar by logging in and logging out.")
//	public void Verifybycustomtoolbarbylogginginandloggingout() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
//	{	
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.clickonAddButton();
//		accountsettingspage.enterTheJobName(Config.Customize_jobname);
//		accountsettingspage.clickOnBrowseIcon();
//		accountsettingspage.SelectAnyicon();
//		accountsettingspage.ClickOnDoneButton();
//		accountsettingspage.clickOnBriefcaseicon();
//		commonmethdpage.SearchJobToAddInCustomizeToolbar(Config.Android_FileTransferJob);
//		commonmethdpage.Selectjob(Config.Android_FileTransferJob);
//		commonmethdpage.SelectjobOkButton();
//		accountsettingspage.SaveCustomdyamicJob();
//		accountsettingspage.verifySavedCustomizeJob();
//		commonmethdpage.ClickOnHomePage();
//		loginPage.logoutfromApp();
//		commonmethdpage.ClickOnLoginAgainLink();
//		loginPage.login(Config.userID, Config.password);
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDeviceInconsole();
//		accountsettingspage.clickonMorebuttonInConsole();
//		accountsettingspage.verifySavedCustomizeJobInConsole();
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.deleteUserDefinedjob(Config.Customize_jobname);
//	}
//	
	//kavya
//	@Test(priority=18,description="Verify Customized tool bar Window all option are displayed")
//	public void VerifyCustomizedtoolbarWindowalloptionaredisplayedTC_ST_279() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
//		Reporter.log("\n18.Verify Customized tool bar Window all option are displayed",true);
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnCustomizeToolbar();
//		accountsettingspage.verifyPredefinedJobs();
//		accountsettingspage.userdefinedAddButton();
//	}
	
		
	//set precondition as trial details job should be present 
	@Test(priority=19,description="Verify Logs when User Create Customize Toolbar")
	public void VerifyLogswhenUserCreateCustomizeToolbarTC_ST_619() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{	
		Reporter.log("\n19.Verify Logs when User Create Customize Toolbar",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnCustomizeToolbar();
		accountsettingspage.ADDJob();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.CreatedLogs();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnCustomizeToolbar();
		accountsettingspage.Delete42Test();		
	}
	
	@Test(priority=20,description="Verify Logs when User Modified Customize Toolbar")
	public void VerifyLogswhenUserModifiedCustomizeToolbarTC_ST_619() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{	
		Reporter.log("\n20.Verify Logs when User Modified Customize Toolbar",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnCustomizeToolbar();
		accountsettingspage.ADDJob();
		accountsettingspage.ModifyJob();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.ModifyLogs();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnCustomizeToolbar();
		accountsettingspage.Delete42Test();		
	}
	
	@Test(priority=21,description="Verify Logs when User Deleted Customize Toolbar")
	public void VerifyLogswhenUserDeletedCustomizeToolbarTC_ST_619() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{	
		Reporter.log("\n21.Verify Logs when User Deleted Customize Toolbar",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnCustomizeToolbar();
		accountsettingspage.ADDJob();
		accountsettingspage.Delete42Test();	
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DeleteActivityLogs();			
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
