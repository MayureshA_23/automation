package PageObjectRepository;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class ScheduleReports_POM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	public String PARENTWINDOW;
	String SystemLogSummary="System Log Report";
	String AssetTrackingSummary="Asset Tracking Report";
	String CallLogSummary="Call Log Tracking Report";
	String JobsDeployedSummary="Jobs Deployed Report";
	String InstalledJobSummary="Installed Job Report Description";
	String DeviceHealthSummary="Report containing device battery level, available physical memory and storage space";
	String DeviceHistorySummary="Report Containing Device Health Details";
	String DataUsageSummary="Device Data Usage Report";
	String DeviceConnectedSummary="Last Connected Time";
	String AppVersionSummary="Get version of application across devices";
	
	@FindBy(xpath="//a[text()='Reports']")
	private WebElement ReportsBtn;
	
	@FindBy(xpath="//span[text()='Schedule Reports']")
	private WebElement ScheduleReportsBtn;

	@FindBy(xpath="//span[text()='System Log']")
	private WebElement SystemLogReport;
	
	@FindBy(xpath="//span[text()='System Log']/following-sibling::span")
	private WebElement SystemLogReportSummary;
	
	@FindBy(xpath="//span[text()='Asset Tracking']")
	private WebElement AssetTrackingReport;
	
	@FindBy(xpath="//span[text()='Asset Tracking']/following-sibling::span")
	private WebElement AssetTrackingReportSummary;
	
	@FindBy(xpath="//table[@id='reportsGrid']/tbody/tr/td/div/span[text()='Call Log Tracking']")
	private WebElement CallLogTrackingReport;
	
	@FindBy(xpath="//span[text()='Call Log Tracking']/following-sibling::span")
	private WebElement CallLogTrackingReportSummary;
	
	@FindBy(xpath="//span[text()='Jobs Deployed']")
	private WebElement JobsDeployedReport;
	
	@FindBy(xpath="//span[text()='Jobs Deployed']/following-sibling::span")
	private WebElement JobsDeployedReportSummary;
	
	@FindBy(xpath="//span[text()='Installed Job Report']")
	private WebElement InstalledJobReport;
	
	@FindBy(xpath="//span[text()='Installed Job Report']/following-sibling::span")
	private WebElement InstalledJobReportSummary;
	
	@FindBy(xpath="//span[text()='Device Health Report']")
	private WebElement DeviceHealthReport;
	
	@FindBy(xpath="//span[text()='Device Health Report']/following-sibling::span")
	private WebElement DeviceHealthReportSummary;
	
	@FindBy(xpath="//span[text()='Device History']")
	private WebElement DeviceHistoryReport;
	
	@FindBy(xpath="//span[text()='Device History']/following-sibling::span")
	private WebElement DeviceHistoryReportSummary;
	
	@FindBy(xpath="//td[div[span[text()='Data Usage']]]")
	private WebElement DataUsageReport;
	
	@FindBy(xpath="//span[text()='Data Usage']/following-sibling::span")
	private WebElement DataUsageReportSummary;
	
	@FindBy(xpath="//span[text()='Device Connected']")
	private WebElement DeviceConnectedReport;
	
	@FindBy(xpath="//span[text()='Device Connected']/following-sibling::span")
	private WebElement DeviceConnectedReportSummary;
	
	@FindBy(xpath="//span[text()='App Version']")
	private WebElement AppVersionReport;
	
	@FindBy(xpath="//span[text()='App Version']/following-sibling::span")
	private WebElement AppVersionReportSummary;
	
	@FindBy(xpath="//header[@id='reportTitCont']/h5")
	private WebElement ReportHeader;
	
	@FindBy(xpath="//header[@id='reportTitCont']/p")
	private WebElement ReportSubHeader;
	
	@FindBy(id="newSchRpt_modalBtn")
	private WebElement ScheduleNewBtn;
	
	@FindBy(id="schRpt_deleteBtn")
	private WebElement ScheduleDeleteBtn;
	
	@FindBy(id="reschRpt_modalBtn")
	private WebElement ReScheduleBtn;
	
	@FindBy(xpath="//h4[contains(text(),'Schedule New')]")
	private WebElement ScheduleNewHeader;
	
	@FindBy(xpath="//h4[contains(text(),'Schedule New')]/preceding-sibling::button")
	private WebElement CloseScheduleNewHeader;
	
	@FindBy(xpath="//div[@id='scheduleReport']/span[text()='Scheduled Report Cycle']")
	private WebElement ScheduledReportCycleSection;
	
	@FindBy(id="schedule_report_type_200")
	private WebElement DailyRadioButton;
	
	@FindBy(xpath="//div[@id='scheduleReport']/div/span[text()='Daily']")
	private WebElement ScheduledReportDailySection;
	
	@FindBy(id="schedule_report_type_300")
	private WebElement WeeklyRadioButton;
	
	@FindBy(xpath="//div[@id='scheduleReport']/div/span[text()='Weekly']")
	private WebElement ScheduledReportWeeklySection;
	
	@FindBy(id="schedule_report_type_400")
	private WebElement MonthlyRadioButton;
	
	@FindBy(xpath="//div[@id='scheduleReport']/div/span[text()='Monthly']")
	private WebElement ScheduledReportMonthlySection;
	
	@FindBy(xpath="//div[@id='scheduleReport']/span[contains(@class,'schdeule_messg')]")
	private WebElement ScheduledMessage;
	
	@FindBy(xpath="//input[@id='email_input']/preceding-sibling::span")
	private WebElement MailSection;
	
	@FindBy(id="email_input")
	private WebElement EmailIdInput;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/div/span[text()='Select Group']")
	private WebElement SelectGroupSection_SystemLog;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Select Group']")
	private WebElement SelectGroupSection;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Select Device or Group']")
	private WebElement SelectGroupSection_CallLog;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Select Device']")
	private WebElement SelectDeviceSection_DeviceHistory;
	
	@FindBy(id="scheduleReportBtn")
	private WebElement ScheduleBtn;
	
	@FindBy(xpath="//h4[text()='Group List']")
	private WebElement SelectGroupHeader;
	
	@FindBy(xpath="//h4[text()='Group List']/preceding-sibling::div")
	private WebElement SelectGroupCloseBtn;
	
	@FindBy(xpath="//div[@id='groupListModal']/div/div/div[3]/button")
	private WebElement SelectGroupOkBtn;
	
	@FindBy(xpath="//h4[contains(text(),'Reschedule')]")
	private WebElement RescheduleHeader;
	
	@FindBy(xpath="//span[text()='Enter valid email address.']")
	private WebElement EmailErrorMeesage;
	
	@FindBy(xpath="//span[text()='Please select a device or group.']")
	private WebElement SelectGroupDeviceErrorMessage;
	
	@FindBy(xpath="//span[text()='Please select a device.']")
	private WebElement SelectDeviceErrorMessage_DeviceHistory;
	
	@FindBy(xpath="//span[text()='Please select a device from the list.']")
	private WebElement SelectDeviceErrorMessage;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Report Cycle']")
	private WebElement ReportCycleColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Mail To']")
	private WebElement MailToColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Group']")
	private WebElement GroupColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Device Name']")
	private WebElement DeviceNameColumn_DeviceHistory;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='All Logs']")
	private WebElement AllLogsColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Job Logs']")
	private WebElement JobLogsColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Online/Offline Logs']")
	private WebElement OnlineOfflineColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Remote Support Logs']")
	private WebElement RemoteSupportColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='App Install/Uninstall Logs']")
	private WebElement AppInstallUnistallColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Device Info Logs']")
	private WebElement DeviceInfoColumnName;
	
	@FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/span[@id='SelectGroup']")
	private WebElement SelectGroup;
	
	@FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/div/span[@id='SelectGroup']")
	private WebElement SelectGroup_SystemLog;
	
	//System Log Report
	@FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/div/span/input[@id='ShowAllLogs']")
	private WebElement ShowAllLogsCheckbox;
	
	@FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/div/span/input[@id='ShowOnlineOfflineLogs']")
	private WebElement ShowOnlineOfflineLogsCheckbox;
	
	@FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/div/span/input[@id='ShowAppInstallLogs']")
	private WebElement ShowAppInstallLogsCheckbox;
	
    @FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/div/span/input[@id='ShowJobLogs']")
	private WebElement ShowJobLogsCheckbox;
	
	@FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/div/span/input[@id='ShowRemoteSupportLogs']")
	private WebElement ShowRemoteSupportLogsCheckbox;
	
	@FindBy(xpath="//div[@class='schRpt_infoCont']/following-sibling::div/div/div/span/input[@id='ShowDeviceInfoLogs']")
	private WebElement ShowDeviceInfoLogsCheckbox;
	
	@FindBy(xpath="//span[text()='Please select type of logs for report.']")
	private WebElement LogsErrorMeesage;
	
	//DeviceHistory
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[@id='addbtn']")
	private WebElement SelectDevice_DeviceHistory;
	
	@FindBy(id="adddevicebtn")
	private WebElement addBtn;
	
	@FindBy(xpath="//h4[text()='Add Device']")
	private WebElement SelectDeviceHeader;
	
	@FindBy(xpath="//h4[text()='Add Device']/preceding-sibling::button")
	private WebElement SelectDeviceCloseBtn;
	
	@FindBy(xpath="//div[text()='Groups']")
	private WebElement GroupsSection;
	
	@FindBy(xpath="//table[@id='dGrid']/thead/tr/th[@class='DeviceName']")
	private WebElement DeviceNameSection;
	
	@FindBy(xpath="//table[@id='dGrid']/thead/tr/th[@class='DeviceModelName']")
	private WebElement DeviceModelNameSection;
	
	@FindBy(xpath="//div[@id='dGridContainer']/div/div/div/input[@placeholder='Search']")
	private WebElement SearchField;
	
	@FindBy(xpath="//span[text()='No matching result found.']")
	private WebElement SearchErrorMeesage;
	
	@FindBy(xpath="//div[@id='dGridContainer']/div/div/div[contains(text(),'No device available in this group.')]")
	private WebElement SearchGridMeesage;
	
	@FindBy(xpath="//span[text()='Please select a device from the list.']")
	private WebElement SelectDeviceErrorMeesage;
	
	//Call Log Tracking
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Call Type']")
	private WebElement CallTypeColumnName;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Call Type']")
	private WebElement CallTypeSection;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Device Name Or Group']")
	private WebElement GroupColumnName_CallLog;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[@id='addbtn']")
	private WebElement SelectGroup_CallLog;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/select[@id='CallType']")
	private WebElement CallType;
	
	@FindBy(xpath="//h4[text()='Add Device Or Group']")
	private WebElement SelectGroupHeader_CallLog;
	
	@FindBy(xpath="//h4[text()='Add Device Or Group']/preceding-sibling::button")
	private WebElement SelectGroupCloseBtn_CallLog;
	
	//DeviceHealth
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Battery <=']")
	private WebElement BatteryColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Storage Space <= ']")
	private WebElement StorageSpaceColumnName;
	
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Physical Memory <=']")
	private WebElement PhysicalMemoryColumnName;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Battery Percentage <= ']")
	private WebElement BatterySection;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Storage Space (in MB) <= ']")
	private WebElement StorageSpaceSection;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Physical Memory (in MB) <= ']")
	private WebElement PhysicalMemorySection;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/input[@id='battery']")
	private WebElement BatteryTextBox;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/input[@id='storagemb']")
	private WebElement StorageSpaceTextBox;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/input[@id='memorymb']")
	private WebElement PhysicalMemoryTextBox;
	
	//App Version
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='App Type']")
	private WebElement AppTypeColumnName;
		
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='App Name']")
	private WebElement ApplicationNameColumnName;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='App Type']")
	private WebElement AppTypeSection;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Application Name']")
	private WebElement ApplicationNameSection;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/select[@id='ApplicationType']")
	private WebElement AppTypeDropDown;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/input[@id='applicationname']")
	private WebElement AppNameTextBox;
	
	@FindBy(xpath="//span[text()='Please provide application name.']")
	private WebElement AppNameUnknownErrorMeesage;
	
	@FindBy(xpath="//span[text()='Application name cannot be less than 3 characters.']")
	private WebElement AppNameLessCharatersErrorMeesage;
	
	//Installed Job Report
	@FindBy(xpath="//table[@id='tbl_schedulereports']/thead/tr/th/div[text()='Job Name']")
	private WebElement JobsColumnName;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[text()='Select Job']")
	private WebElement JobsSection;
	
	@FindBy(xpath="//span[text()='Please provide job.']")
	private WebElement JobErrorMeesage;
	
	@FindBy(xpath="//div[@class='rpt_detailsCont']/div/span[@id='SelectJob']")
	private WebElement SelectJob;
	
	@FindBy(xpath="//span[text()='Please select a job.']")
	private WebElement SelectJobErrorMeesage;
	
	@FindBy(xpath="//h4[text()='Select Jobs To Add']")
	private WebElement SelectJobHeader;
	
	@FindBy(xpath="//h4[text()='Select Jobs To Add']/preceding-sibling::button")
	private WebElement CloseSelectJob;
	
	@FindBy(xpath="//div[@id='conmptableContainer']/div/div/div/input[@placeholder='Search']")
	private WebElement SearchFieldSelectJob;
	
	@FindBy(xpath="//div[@id='conmptableContainer']/div/div/div[contains(text(),'No jobs available.')]")
	private WebElement SearchGridMessage_Job;
	
	@FindBy(id="okbtn")
	private WebElement SelectJobOkBtn;
	
	public void ClickOnReport() throws InterruptedException
	{
		ReportsBtn.click();
		waitForXpathPresent("//span[text()='System Log']");
		sleep(5);
		Reporter.log("PASS >> Clicked on Reports , Navigates to Reports Section", true);
	}
	
	public void ClickOnScheduleReport() throws InterruptedException
	{
		ScheduleReportsBtn.click();
		waitForXpathPresent("//span[text()='System Log']");
		sleep(5);
		Reporter.log("PASS >> Clicked on Schedule Reports , Navigates to Schedule Reports Section", true);
	}
	public void ClickOnSchedulReport(String rep) throws InterruptedException
	{
		ScheduleReportsBtn.click();
		sleep(2);
		//SearchOnDemandReports.clear();
		//waitForXpathPresent("//span[text()='"+rep+"']");
		sleep(2);
	}
	public WebElement PassReportWebElement(int count)
	{
		WebElement[] PassReportWebElement = { SystemLogReport, AssetTrackingReport, CallLogTrackingReport, JobsDeployedReport,
				InstalledJobReport, DeviceHealthReport, DeviceHistoryReport, DataUsageReport, DeviceConnectedReport, AppVersionReport };
		return PassReportWebElement[count];
	}
	
	public WebElement PassReportSummaryWebElement(int count)
	{
		WebElement[] PassReportSummaryWebElement = { SystemLogReportSummary, AssetTrackingReportSummary,CallLogTrackingReportSummary, JobsDeployedReportSummary, InstalledJobReportSummary,
				DeviceHealthReportSummary, DeviceHistoryReportSummary, DataUsageReportSummary, DeviceConnectedReportSummary, AppVersionReportSummary };
		return PassReportSummaryWebElement[count];
	}
	
	public String PassReportName(int count)
	{
		String[] ExpectedValue = { "System Log", "Asset Tracking", "Call Log Tracking", "Jobs Deployed",
				"Installed Job Report", "Device Health Report", "Device History", "Data Usage", "Device Connected" ,"App Version"};
		return ExpectedValue[count];
	}
	
	public String PassReportSummary(String ReportName)
	{
		String ExpectedValue=null;
		switch (ReportName) {
		case "System Log":
			ExpectedValue = SystemLogSummary;
			break;
		case "Asset Tracking":
			ExpectedValue = AssetTrackingSummary;
			break;
		case "Call Log Tracking":
			ExpectedValue = CallLogSummary;
			break;
		case "Jobs Deployed":
			ExpectedValue = JobsDeployedSummary;
			break;
		case "Installed Job Report":
			ExpectedValue = InstalledJobSummary;
			break;
		case "Device Health Report":
			ExpectedValue = DeviceHealthSummary;
			break;
		case "Device History":
			ExpectedValue = DeviceHistorySummary;
			break;
		case "Data Usage":
			ExpectedValue = DataUsageSummary;
			break;
		case "Device Connected":
			ExpectedValue = DeviceConnectedSummary;
			break;
		case "App Version":
			ExpectedValue = AppVersionSummary;
			break;
		}
		return ExpectedValue;
	}
	
	
	public void VerifyScheduleReports()
	{
		for(int count=0;count<10;count++)
		{
		WebElement[] wb = { SystemLogReport, AssetTrackingReport, CallLogTrackingReport, JobsDeployedReport,
					InstalledJobReport, DeviceHealthReport, DeviceHistoryReport, DataUsageReport, DeviceConnectedReport,
					AppVersionReport };
		boolean value = wb[count].isDisplayed();
		String PassStatement="\nPASS >> "+PassReportName(count)+" Report block is Displayed succesfully";
		String FailStatement="FAIL >> "+PassReportName(count)+" Report block is not displayed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String ActualValue=PassReportSummaryWebElement(count).getText();
		String ExpectedValue=PassReportSummary(PassReportName(count));
		PassStatement="PASS >> "+PassReportName(count)+" Report Summary is Displayed succesfully as "+ActualValue;
		FailStatement="FAIL >> "+PassReportName(count)+" Report Summary is not displayed "+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue,PassStatement, FailStatement);
		 Reporter.log("***** PASS >> Verified "+PassReportName(count)+"*****",true);
		}
	}

	public void VerifyOfReportsPage(String ReportName) throws InterruptedException
	{
		String ActualValue=ReportHeader.getText();
		String ExpectedValue=ReportName;
		String PassStatement="PASS >> "+ReportName+" Report Header is Displayed succesfully";
		String FailStatement="FAIL >> "+ReportName+" Report Header is not displayed";
		ALib.AssertEqualsMethod(ActualValue,ExpectedValue, PassStatement, FailStatement);
		ActualValue=ReportSubHeader.getText();
		ExpectedValue=PassReportSummary(ReportName);
		PassStatement="PASS >> "+ReportName+" Report Sub-Header is Displayed succesfully as "+ActualValue;
		FailStatement="FAIL >> "+ReportName+" Report Sub-Header is not displayed "+ActualValue;
		ALib.AssertEqualsMethod(ActualValue,ExpectedValue,PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified "+ReportName+" Page *****",true);
	}
	
	public void ClickonSystemLog() throws InterruptedException
	{
		SystemLogReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on System Log Schedule Reports , Navigates to System Log Schedule Reports Section", true);
		
	}
	
	public void VerifyOfScheduleNewButtonDeleteButtonRescheduledButton() {
		String text = ScheduleNewBtn.getAttribute("class");
		boolean value = text.contains("disabled");
		String PassStatement = "PASS >> Schedule New Button is Enabled succesfully when Clicked on Report";
		String FailStatement = "FAIL >> Schedule New Button is Disabled when Clicked on Report";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified Schedule New Button *****", true);
		text = ScheduleDeleteBtn.getAttribute("class");
		value = text.contains("disabled");
		PassStatement = "PASS >> Schedule Delete Button is Disabled succesfully when Clicked on Report";
		FailStatement = "FAIL >> Schedule Delete Button is Enabled when Clicked on Report";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified Schedule Delete Button *****", true);
		text = ReScheduleBtn.getAttribute("class");
		value = text.contains("disabled");
		PassStatement = "PASS >> ReSchedule Button is Disabled succesfully when Clicked on Report";
		FailStatement = "FAIL >> ReSchedule Button is Enabled when Clicked on Report";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified ReSchedule Button *****", true);
	}

	public void VerifySystemLogReportsColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "All Logs";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Logs";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 5) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "App Install/Uninstall Logs";
				String pass = "PASS >> 6th column is present";
				String fail = "FAIL>> 6th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	@FindBy(id = "scheduleReportBtn")
	private WebElement ScheduleButton;

	public void ClickOnSchedule() throws InterruptedException {
		ScheduleButton.click();
		sleep(5);
	}
	
	public void VerifyDeviceActivityReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Name";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void EnterStorageSpace() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='storagemb']"));
		ls.get(1).clear();
		ls.get(1).sendKeys("10000");

	}

	public void VerifyTableInSelectedTableList(String TableName) throws InterruptedException {
		String ActualTableName = Initialization.driver
				.findElement(By.xpath("//div[@id='selected_tablesList_tree']/ul/li[text()='" + TableName + "']"))
				.getText();
		System.out.println(ActualTableName);
		boolean Val = ActualTableName.equals(TableName);
		String Pass = "PASS >> TableName Is Showing Correctly In Selected Table List";
		String Fail = "FAIL >> TableName Isn't Showing Correctly In Selected Table List";
		ALib.AssertTrueMethod(Val, Pass, Fail);
		sleep(2);
	}
	
	public void SelectAnyDateInScheduledCycleDate() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='reportGenerate-date-wrapper']/div/input")).click();
		sleep(1);
		Initialization.driver.findElement(By.xpath("//div[@id='reportGenerate-date-wrapper']/div/div/div[2]/ul/li[2]"))
				.click();
		
	}
	
	public void VerifyHiddenScheduledCycleTimeAndDateField() {

		if (!ScheduledCycleTime.isDisplayed() && !ScheduledCycleDate.isDisplayed()) {
			Reporter.log("PASS >> Scheduled Cycle Time and Scheduled Cycle Date field is hidden", true);
			Assert.assertTrue(true);
		} else {
			Reporter.log("FAIL >>Scheduled Cycle Time and Scheduled Cycle Date field Isn't hidden");
			Assert.assertFalse(false);
		}
	}

	public void VerifyDeviceHistoryReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Device Name";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	@FindBy(xpath = "//a[@class='report_configBtn']")
	private WebElement ClickOnChangeLink;
	public void ClickOnChange() {
		ClickOnChangeLink.click();
	}
	
	@FindBy(xpath = "(//span[text()='Scheduled Cycle Time'])[last()]")
	private WebElement ScheduledCycleTime;

	@FindBy(xpath = "(//span[text()='Scheduled Cycle Date'])[last()]")
	private WebElement ScheduledCycleDate;
	public void VerifyScheduledCycleTimeAndDateField() {

		if (ScheduledCycleTime.isDisplayed() && ScheduledCycleDate.isDisplayed()) {
			Reporter.log("PASS >> Scheduled Cycle Time and Scheduled Cycle Date field Isn't hidden", true);
			Assert.assertTrue(true);
		} else {
			Reporter.log("FAIL >>Scheduled Cycle Time and Scheduled Cycle Date field Is hidden");
			Assert.assertFalse(false);
		}
	}

	public void SelectAnyDateInScheduledCycleTime() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='reportGenerate-time-wrapper']/div/input")).click();
		sleep(1);
		Initialization.driver.findElement(By.xpath("(//div[text()='02:00'])[last()]")).click();

	}

	@FindBy(xpath = "//*[@id='report_genrateSect']/div[1]/div[1]/div[1]/div/input")
	private WebElement SearchOnDemandReports;
	
	public void SearchCustomizedReport(String reportname) throws InterruptedException {
		SearchOnDemandReports.clear();
		sleep(2);
		SearchOnDemandReports.click();
		SearchOnDemandReports.sendKeys(reportname);
		waitForVisibilityOf("(//table[@id='reportsGrid']/tbody/tr/td/div/span[1])[last()]");
		sleep(2);
	}
	public void EnterPhysicalMemory() {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='memorymb']"));
		ls.get(1).clear();
		ls.get(1).sendKeys("10000");

	}
	public void ClickOnSelectDevice() throws InterruptedException {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='addbtn']"));
		ls.get(1).click();
		sleep(5);

	}
	@FindBy(id = "email_input")
	private WebElement MailTo;
	public void EnterEmailAddress(String mail) throws InterruptedException {
		MailTo.sendKeys(mail);
		sleep(2);
	}
	public void ClickonAssetTracking() throws InterruptedException
	{
		AssetTrackingReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Asset Tracking Schedule Reports , Navigates to Asset Tracking Schedule Reports Section", true);
		
	}
	
	public void VerifyAssetTrackingReportsColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyInstalledJobReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Name";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "App Name";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}
	
	public void VerifyJobDeployedReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Job Name";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyDataUsageReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyDeviceConnectedReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void VerifyAppVersionReportColumns() throws InterruptedException {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='sch_reports_blk']/div[1]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int numberofCols = ls.size();
		for (int i = 0; i < numberofCols; i++) {
			if (i == 0) {
				String ActualFirstColumn = ls.get(i).getText();
				String ExpectedFirstColumn = "Report Cycle";
				String pass = "PASS >> 1st column is present:";
				String fail = "FAIL>> 1st column is not correct";
				ALib.AssertEqualsMethod(ExpectedFirstColumn, ActualFirstColumn, pass, fail);
			}

			if (i == 1) {
				String ActualSecondColumn = ls.get(i).getText();
				String ExpectedSecondColumn = "Mail To";
				String pass = "PASS >> 2nd column is present";
				String fail = "FAIL>> 2nd column is not correct";
				ALib.AssertEqualsMethod(ExpectedSecondColumn, ActualSecondColumn, pass, fail);
			}

			if (i == 2) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Group";
				String pass = "PASS >> 3rd column is present";
				String fail = "FAIL>> 3rd column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 3) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "Application Type";
				String pass = "PASS >> 4th column is present";
				String fail = "FAIL>> 4th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}

			if (i == 4) {
				String ActualThirdColumn = ls.get(i).getText();
				String ExpectedThirdColumn = "App Name";
				String pass = "PASS >> 5th column is present";
				String fail = "FAIL>> 5th column is not correct";
				ALib.AssertEqualsMethod(ExpectedThirdColumn, ActualThirdColumn, pass, fail);
			}
		}
	}

	public void EnterApplicationNameInstalledJobReport(String AppName) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='applicationname']"));
		ls.get(1).clear();
		ls.get(1).sendKeys(AppName);
	}


	@FindBy(xpath = "//span[text()='Device Activity']")
	private WebElement DeviceActivityReport;
	public void ClickonDeviceActivity() throws InterruptedException {
		DeviceActivityReport.click();
		sleep(4);
		Reporter.log(
				"PASS >> Clicked on Device History Schedule Reports , Navigates to Device History Schedule Reports Section",
				true);
	}
	public void ClickonJobsDeployed() throws InterruptedException
	{
		JobsDeployedReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Jobs Deployed Schedule Reports , Navigates to Jobs Deployed Schedule Reports Section", true);
		
	}
	
	public void ClickonCallLogTracking() throws InterruptedException
	{
		CallLogTrackingReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Call Log Tracking Schedule Reports , Navigates to Call Log Tracking Schedule Reports Section", true);
		
	}
	
	public void ClickonDeviceHistory() throws InterruptedException
	{
		DeviceHistoryReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Device History Schedule Reports , Navigates to Device History Schedule Reports Section", true);
		
	}
	
	public void ClickonDataUsage() throws InterruptedException
	{
		DataUsageReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Data Usage Schedule Reports , Navigates to Data Usage Schedule Reports Section", true);
		
	}
	
	public void ClickonDeviceConnected() throws InterruptedException
	{
		DeviceConnectedReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Device Connected Schedule Reports , Navigates to Device Connected Schedule Reports Section", true);
		
	}
	
	public void ClickonDeviceHealth() throws InterruptedException
	{
		DeviceHealthReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Device Health Schedule Reports , Navigates to Device Health Schedule Reports Section", true);
		
	}
	
	public void ClickonAppVersion() throws InterruptedException
	{
		AppVersionReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on App Version Schedule Reports , Navigates to App Version Schedule Reports Section", true);
		
	}
	
	public void ClickonInstalledJobReport() throws InterruptedException
	{
		InstalledJobReport.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Installed Job Schedule Reports , Navigates to Installed Job Schedule Reports Section", true);
		
	}
	
	public void VerifyOfScheduleBtn() 
	{
		String text=ScheduleNewBtn.getAttribute("class");
		boolean value=text.contains("disabled");
		String PassStatement="PASS >> Schedule New Button is Enabled succesfully when Clicked on Report";
		String FailStatement="FAIL >> Schedule New Button is Disabled when Clicked on Report";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified Schedule New Button *****",true);
		text=ScheduleDeleteBtn.getAttribute("class");
		value=text.contains("disabled");
		PassStatement="PASS >> Schedule Delete Button is Disabled succesfully when Clicked on Report";
		FailStatement="FAIL >> Schedule Delete Button is Enabled when Clicked on Report";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified Schedule Delete Button *****",true);
		text=ReScheduleBtn.getAttribute("class");
		value=text.contains("disabled");
		PassStatement="PASS >> ReSchedule Button is Disabled succesfully when Clicked on Report";
		FailStatement="FAIL >> ReSchedule Button is Enabled when Clicked on Report";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified ReSchedule Button *****",true);
	}

	public String PassScheduleNewHeader(String ReportName)
	{
		String ExpectedValue=null;
		switch (ReportName) {
		case "System Log":
			ExpectedValue = "System Log Report";
			break;
		case "Asset Tracking":
			ExpectedValue = "Asset Tracking Report";
			break;
		case "Call Log Tracking":
			ExpectedValue = "Call Log Tracking Report";
			break;
		case "Jobs Deployed":
			ExpectedValue = "Jobs Deployed Report";
			break;
		case "Installed Job Report":
			ExpectedValue = "Installed Job Report";
			break;
		case "Device Health Report":
			ExpectedValue = "Device Health Report";
			break;
		case "Device History":
			ExpectedValue = "Device History Report";
			break;
		case "Data Usage":
			ExpectedValue = "Data Usage Report";
			break;
		case "Device Connected":
			ExpectedValue = "Device Connected Report";
			break;
		case "App Version":
			ExpectedValue = "App Version Report";
			break;
		}
		return ExpectedValue;
	}
	
	public void ClickOnScheduleNew() throws InterruptedException
	{
		ScheduleNewBtn.click();
		waitForidPresent("schedule_report_type_200");
		sleep(5);
	}
	
	public void VerifyOfScheduleNew(String ReportName) throws InterruptedException
	{
		String ActualValue=ScheduleNewHeader.getText();
		String ExpectedValue="Schedule New - "+PassScheduleNewHeader(ReportName);
		String PassStatement="PASS >> Schedule New Pop is dispalyed succesfully when Clicked on Schedule New in "+ReportName;
		String FailStatement="FAIL >> Schedule New Pop is not dispalyed when Clicked on Schedule New in "+ReportName;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
		boolean value=ScheduledReportCycleSection.isDisplayed();
		PassStatement="PASS >> 'Scheduled Report Cycle' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> 'Scheduled Report Cycle' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	    value=MailSection.isDisplayed();
		PassStatement="PASS >> 'Mail To' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> 'Mail To' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
		ClickOnWeeklyRadioButon();
		
		value=ScheduledReportWeeklySection.isDisplayed();
		PassStatement="PASS >> Weekly RadioButton is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> Weekly RadioButton text is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
		ActualValue=ScheduledMessage.getText();
		ExpectedValue="Report will be generated every Sunday at 12 AM";
		PassStatement="PASS >> Schedule Message "+ActualValue+" is displayed successfully when Clicked on Weekly Radiobutton";
		FailStatement="FAIL >> Schedule Message "+ActualValue+" is not displayed when Clicked on Weekly Radiobutton";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
		ClickOnDailyRadioButon();
		
		value=ScheduledReportDailySection.isDisplayed();
		PassStatement="PASS >> Daily RadioButton is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> Daily RadioButton text is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
		ActualValue=ScheduledMessage.getText();
		ExpectedValue="Report will be generated daily at 12 AM";
		PassStatement="PASS >> Schedule Message "+ActualValue+" is displayed successfully when Clicked on Daily Radiobutton";
		FailStatement="FAIL >> Schedule Message "+ActualValue+" is not displayed when Clicked on Daily Radiobutton";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
		/*ClickOnMonthlyRadioButon();
		
		value=ScheduledReportMonthlySection.isDisplayed();
		PassStatement="PASS >> Monthly RadioButton is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> Monthly RadioButton text is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
		ActualValue=ScheduledMessage.getText();
		ExpectedValue="Report will be generated 1st day of every month at 12 AM";
		PassStatement="PASS >> Schedule Message "+ActualValue+" is displayed successfully when Clicked on Monthly Radiobutton";
		FailStatement="FAIL >> Schedule Message "+ActualValue+" is not displayed when Clicked on Monthly Radiobutton";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);*/
		
	}
	
	public void VerifyOfSelectGroupSection_SystemLog(String ReportName) throws InterruptedException
	{
		boolean value=SelectGroupSection_SystemLog.isDisplayed();
		String PassStatement="PASS >> 'Select Group' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		String FailStatement="FAIL >> 'Select Group' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfSelectGroupSection(String ReportName) throws InterruptedException
	{
		boolean value=SelectGroupSection.isDisplayed();
		String PassStatement="PASS >> 'Select Group' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		String FailStatement="FAIL >> 'Select Group' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfSelectGroupSection_CallLog(String ReportName) throws InterruptedException
	{
		boolean value=SelectGroupSection_CallLog.isDisplayed();
		String PassStatement="PASS >> 'Select Group' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		String FailStatement="FAIL >> 'Select Group' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfSelectDeviceSection_DeviceHistory(String ReportName) throws InterruptedException
	{
		boolean value=SelectDeviceSection_DeviceHistory.isDisplayed();
		String PassStatement="PASS >> 'Select Device' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		String FailStatement="FAIL >> 'Select Device' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void ClickOnWeeklyRadioButon()
	{
		WeeklyRadioButton.click();
		
	}
	
	public void ClickOnDailyRadioButon()
	{
		DailyRadioButton.click();
		
	}
	
	public void ClickOnMonthlyRadioButon()
	{
		MonthlyRadioButton.click();
		
	}
	
	public void ClickOnScheduleBtn_Error() throws InterruptedException{
		ScheduleBtn.click();
	}
	
	public void ClickOnScheduleBtn() throws InterruptedException{
		ScheduleBtn.click();
		waitForidClickable("newSchRpt_modalBtn");
		sleep(5);
	}
	
	public void ClearMailTo() throws InterruptedException{
		EmailIdInput.clear();
		
	}
	
	public void EnterMailTo() throws InterruptedException{
		EmailIdInput.sendKeys(Config.MailTo);
		
	}
	
	public void EnterMailTo(String mail) throws InterruptedException{
		EmailIdInput.sendKeys(mail);
		
	}
	
	public void ClickOnCloseSchedulePopUp() throws InterruptedException{
		CloseScheduleNewHeader.click();
		Reporter.log("Schedule New popup is closed succesfully when Clicked on Close button of Schedule New Popup",true);
		sleep(5);
		
	}
	
	public void SelectGroupHeader(){
		boolean value=SelectGroupHeader.isDisplayed();
		String PassStatement="PASS >> Group list Header is dispalyed succesfully when Clicked on Select Group";
		String FailStatement="FAIL >> Group list Header is not dispalyed when Clicked on Select Group";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	}
	
	public void ClickOnCloseSelectGroup() throws InterruptedException{
		SelectGroupCloseBtn.click();
		Reporter.log("Group List popup is closed succesfully when Clicked on Close button of Group List Popup",true);
		sleep(4);
	}
	
	public void ClickOnOkSelectGroup() throws InterruptedException{
		SelectGroupOkBtn.click();
		Reporter.log("Group List is saved succesfully when Clicked on Ok button of Group List Popup",true);
		sleep(5);
	}
	
	public void SelectGroup() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Initialization.driver.findElement(By.xpath("//div[@id='groupList']/ul/li[text()='"+ELib.getDatafromExcel("Sheet1",65,1)+"']")).click();
	}
	
	public void ClickOnReSchedule() throws InterruptedException
	{
		ReScheduleBtn.click();
		waitForidPresent("schedule_report_type_200");
		Reporter.log("Reschedule popup is opened succesfully when Clicked on Reschedule Button",true);
		sleep(5);
	}
	
	public void VerifyOfReSchedule(String ReportName) throws InterruptedException
	{
		String ActualValue=RescheduleHeader.getText();
		String ExpectedValue="ReSchedule - "+PassScheduleNewHeader(ReportName);
		String PassStatement="PASS >> ReSchedule Popup is dispalyed succesfully when Clicked on ReSchedule Button of "+ReportName;
		String FailStatement="FAIL >> ReSchedule Popup is not dispalyed when Clicked on ReSchedule Button of "+ReportName;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}
	
	public void ClickOnDelete() throws InterruptedException{
		ScheduleDeleteBtn.click();
		Reporter.log("Clicked on Delete Button",true);
		waitForidClickable("newSchRpt_modalBtn");
		sleep(6);
	}
	
	public void VerifyDelete(int rowinitial,int rowfinal) throws InterruptedException{
		boolean value=rowfinal<rowinitial;
		String PassStatement="PASS >> Daily Report is deleted successfully from the list";
		String FailStatement="FAIL >> Daily Report is not deleted from the list";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void EmailErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Enter valid email address.' is displayed successfully when schedule with Invalid Email Id";
		String FailStatement="Fail >> Error Message : 'Enter valid email address.' is not displayed when schedule with Invalid Email Id";
		Initialization.commonmethdpage.ConfirmationMessageVerify(EmailErrorMeesage, true, PassStatement, FailStatement,5);
		sleep(5);
	}
	
	public void SelectGroupDeviceErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Please select type of logs for report.' is displayed successfully when schedule with No logs selected";
		String FailStatement="Fail >> Error Message : 'Please select type of logs for report.' is not displayed when schedule with No logs selected";
		Initialization.commonmethdpage.ConfirmationMessageVerify(SelectGroupDeviceErrorMessage, true, PassStatement, FailStatement,5);
		sleep(5);
	}
	
	public int VerifyOfHowmanyRows()
	{
		List<WebElement> wb=Initialization.driver.findElements(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr"));
		int Allrows=wb.size();
		if(Allrows==1)
		{
			String value=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr")).getAttribute("class");
			if(value.contains("no-records-found"))
			{
				Allrows=0;
			}else
			{
				Allrows=1;
			}
		}
			
	    return Allrows;
	}
	
	public void ClickOnRows()
	{
		List<WebElement> wb=Initialization.driver.findElements(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr"));
		wb.get(wb.size()-1).click();
	}
	
	public void ClickOnAddButton_Error() throws InterruptedException
	{
		addBtn.click();
		sleep(2);
	}
	
	public void ClickOnAddButton() throws InterruptedException
	{
		addBtn.click();
		waitForXpathPresent("//h4[contains(text(),'Add Device')]");
		sleep(5);
	}
	
	public void VerifyOfSystemColumnName(String ReportName)
	{
		WebElement[] wb = { ReportCycleColumnName, MailToColumnName,GroupColumnName};
		String[] ExpectedValue = { "Report Cycle", "Mail To", "Group"};
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of "+ReportName;
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of "+ReportName;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		   
		}
	}
	
	public void VerifyOfColumnGroupReportMail(int row,String ReportCycle,String Group,String MailTo,String ReportName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[1]/p")).getText();
		String ExpectedValue=ReportCycle;
		String PassStatement = "PASS >> Report Cycle Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle;
		String FailStatement = "FAIL >> Report Cycle Column Name is not updated when "+ReportName+" Report is Scheduled for:"+ReportCycle;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[2]")).getText();
		ExpectedValue=MailTo;
		PassStatement = "PASS >> Mail To Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Mail To Column Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[3]")).getText();
		ExpectedValue=Group;
		PassStatement = "PASS >> Group Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Group Column Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}
	
	public void ClickOnSelectGroup() throws InterruptedException{
		SelectGroup.click();
		sleep(5);
	}
	
	public void ClickOnSelectGroup_SystemLog() throws InterruptedException{
		SelectGroup_SystemLog.click();
		sleep(5);
	}
	
	public void VerifyOfSystemColumnName_DeviceHistory()
	{
		WebElement[] wb = { ReportCycleColumnName, MailToColumnName,DeviceNameColumn_DeviceHistory};
		String[] ExpectedValue = { "Report Cycle", "Mail To", "Device Name"};
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of Device History";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of Device History";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		   
		}
	}
	
	public void SelectDeviceErrorMessage_DeviceHistory() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Please select a device.' is displayed successfully when schedule with No Device selected";
		String FailStatement="Fail >> Error Message : 'Please select a device.' is not displayed when schedule with No Device selected";
		Initialization.commonmethdpage.ConfirmationMessageVerify(SelectDeviceErrorMessage_DeviceHistory, true, PassStatement, FailStatement,5);
		sleep(5);
	}
	
	public void SelectDeviceErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Please select a device from the list.' is displayed successfully when schedule with No Device selected in Select Device Popup";
		String FailStatement="Fail >> Error Message : 'Please select a device from the list.' is not displayed when schedule with No Device selected in Select Device Popup";
		Initialization.commonmethdpage.ConfirmationMessageVerify(SelectDeviceErrorMessage, true, PassStatement, FailStatement,1);
		sleep(5);
	}
	
	public void ClickOnSelectDevice_DeviceHistory() throws InterruptedException
	{
		SelectDevice_DeviceHistory.click();
		waitForidPresent("adddevicebtn");
		sleep(5);
	}
	
	public void SelectDevice_DeviceHistory() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='dGridContainer']/div[1]/div[1]/div[1]/button")).click();
		sleep(5);
		Initialization.driver.findElement(By.xpath("//table[@id='dGrid']/tbody/tr/td/p[text()='"+Config.DeviceName+"']")).click();
		Reporter.log("Device is Selected from the List",true);
	}
	
	public void SelectDeviceHeader(){
		boolean value=SelectDeviceHeader.isDisplayed();
		String PassStatement="PASS >> Group list Header is displayed succesfully when Clicked on Select Device popup";
		String FailStatement="FAIL >> Group list Header is not displayed when Clicked on Select Device popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=GroupsSection.isDisplayed();
		PassStatement="PASS >> Groups Section is displayed succesfully when Clicked on Select Device popup";
		FailStatement="FAIL >> Groups Section is not displayed when Clicked on Select Device popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=DeviceNameSection.isDisplayed();
		PassStatement="PASS >> Device Column is displayed succesfully when Clicked on Select Device popup";
		FailStatement="FAIL >> Device Column is not displayed when Clicked on Select Device popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=DeviceModelNameSection.isDisplayed();
		PassStatement="PASS >> Device Model Column is displayed succesfully when Clicked on Select Device popup";
		FailStatement="FAIL >> Device Model Column is not displayed when Clicked on Select Device popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	
	public void ClickOnCloseSelectDevice() throws InterruptedException{
		SelectDeviceCloseBtn.click();
		waitForXpathPresent("//h4[contains(text(),'Schedule New')]/preceding-sibling::button");
		Reporter.log("Group List popup is closed succesfully when Clicked on Close button of Group List Popup", true);
		sleep(4);
	}
	
	public void EnterSearchField(String DeviceName) throws InterruptedException{
		SearchField.sendKeys(DeviceName);
		
	}
	
	public void SearchErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'No matching result found.' is displayed successfully when Searched with invalid Device Name in Select Group popup";
		String FailStatement="Fail >> Error Message : 'No matching result found.' is not displayed when Searched with invalid Device Name in Select Group popup";
		Initialization.commonmethdpage.ConfirmationMessageVerify(SearchErrorMeesage, true, PassStatement, FailStatement,5);
	}
	
	public void SearchGridMessage() throws InterruptedException
	{
		boolean value=SearchGridMeesage.isDisplayed();
		String PassStatement="PASS >> Grid Message : 'No device available in this group.' is displayed successfully when Searched with invalid Device Name in Select Group popup";
		String FailStatement="Fail >> Grid Message : 'No device available in this group.' is not displayed when Searched with invalid Device Name in Select Group popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyofSearch() throws InterruptedException
	{
		boolean value=Initialization.driver.findElement(By.xpath("//p[text()='"+Config.DeviceName+"']")).isDisplayed();
		String PassStatement="PASS >> Device is searched succesfully when Searched with valid Device Name in Select Group popup";
		String FailStatement="Fail >> Device is not searched when Searched with valid Device Name in Select Group popupp";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfColumnGroupReportMail_DeviceHistory(int row,String ReportCycle,String MailTo,String Group,String ReportName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[1]/p")).getText();
		String ExpectedValue=ReportCycle;
		String PassStatement = "PASS >> Report Cycle Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle;
		String FailStatement = "FAIL >> Report Cycle Column Name is not updated when "+ReportName+" Report is Scheduled for:"+ReportCycle;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[2]")).getText();
		ExpectedValue=MailTo;
		PassStatement = "PASS >> Mail To Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Mail To Column Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[3]")).getText();
		ExpectedValue=Group;
		PassStatement = "PASS >> Device Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Device Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}
	
	//SystemLog
	public void VerifyOfSystemColumnName()
	{
		WebElement[] wb = { ReportCycleColumnName,MailToColumnName, GroupColumnName, AllLogsColumnName, JobLogsColumnName,
				OnlineOfflineColumnName, RemoteSupportColumnName, AppInstallUnistallColumnName, DeviceInfoColumnName };
		String[] ExpectedValue = { "Report Cycle", "Mail To", "Group", "All Logs", "Job Logs", "Online/Offline Logs",
				"RemoteSupport Logs", "App Install/Uninstall Logs", "Device Info Logs" };
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of System Log";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of System Log";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		   
		}
	}
	
	public void VerifyOfLogs()
	{
		WebElement[] wb = { ShowAllLogsCheckbox, ShowOnlineOfflineLogsCheckbox, ShowAppInstallLogsCheckbox,
				ShowJobLogsCheckbox, ShowRemoteSupportLogsCheckbox, ShowDeviceInfoLogsCheckbox };
		String[] ExpectedValue = { "Show All Logs", "Show Online/Offline Logs", "Show App Install/Uninstall Logs",
				"Show Job Logs", "Show Remote Support Logs", "Show Device Info Logs" };
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of System Log";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of System Log";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		}
	}
	
	public void ClickOnShowAllLogs()
	{
		ShowAllLogsCheckbox.click();
	}
	
	public void ClickOnShowJobLogs()
	{
		ShowJobLogsCheckbox.click();
	}
	
	public void ClickOnShowOnlineOfflineLogs()
	{
		ShowOnlineOfflineLogsCheckbox.click();
	}
	
	public void ClickOnShowRemoteSupportLogs()
	{
		ShowRemoteSupportLogsCheckbox.click();
	}
	
	public void ClickOnShowAppInstallLogs()
	{
		ShowAppInstallLogsCheckbox.click();
	}
	
	public void ClickOnShowDeviceInfoLogs()
	{
		ShowDeviceInfoLogsCheckbox.click();
	}
	
	public void UncheckAllOption()
	{
		ClickOnShowAllLogs();
		ClickOnShowJobLogs();
		ClickOnShowOnlineOfflineLogs();
		ClickOnShowRemoteSupportLogs();
		ClickOnShowAppInstallLogs();
		ClickOnShowDeviceInfoLogs();
	}
	
	
	public void VerifyOfColumnAdded(int row,String AllLogs,String JobLogs,String OnlineLogs,String RemoteLogs,String AppInstallLogs,String DeviceLogs)
	{
		//Show All Logs
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[4]")).getText();
		String ExpectedValue=AllLogs;
		String PassStatement = "PASS >> Show All Logs Column Name is updated succesfully when System Log Report is Filtered with Show All Logs :"+ActualValue;
		String FailStatement = "FAIL >> Show All Logs Column Name is not updated when System Log Report is Filtered with Show All Logs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[5]")).getText();
		ExpectedValue=JobLogs;
		PassStatement = "PASS >> Show Job Logs Column Name is updated succesfully when System Log Report is Filtered with Show Job Logs :"+ActualValue;
		FailStatement = "FAIL >> Show Job Logs Column Name is not updated when System Log Report is Filtered with Show Job Logs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[6]")).getText();
		ExpectedValue=OnlineLogs;
		PassStatement = "PASS >> Show Online/Offline Logs Column Name is updated succesfully when System Log Report is Filtered with Show Online/Offline Logs :"+ActualValue;
		FailStatement = "FAIL >> Show Online/Offline Logs Column Name is not updated when System Log Report is Filtered with Show Online/Offline Logs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[7]")).getText();
		ExpectedValue=RemoteLogs;
		PassStatement = "PASS >> Show Remote Support Logs Column Name is updated succesfully when System Log Report is Filtered with Show Remote Support Logs :"+ActualValue;
		FailStatement = "FAIL >> Show Remote Support Logs Column Name is not updated when System Log Report is Filtered with Show Remote Support Logs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[8]")).getText();
		ExpectedValue=AppInstallLogs;
		PassStatement = "PASS >> Show App Install/Uninstall Logs Column Name is updated succesfully when System Log Report is Filtered with Show App Install/Uninstall Logs :"+ActualValue;
		FailStatement = "FAIL >> Show App Install/Uninstall Logs Column Name is not updated when System Log Report is Filtered with Show App Install/Uninstall Logs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[9]")).getText();
		ExpectedValue=DeviceLogs;
		PassStatement = "PASS >> Show Device Info Logs Column Name is updated succesfully when System Log Report is Filtered with Show Device Info Logs :"+ActualValue;
		FailStatement = "FAIL >> Show Device Info Logs Column Name is not updated when System Log Report is Filtered with Show Device Info Logs";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}
	
	public void VerifyOfColumnGroupReportMail(int row,String ReportCycle,String Group,String MailTo)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[1]")).getText();
		String ExpectedValue=ReportCycle;
		String PassStatement = "PASS >> Report Cycle Column Name is updated succesfully when System Log Report is Scheduled for "+ReportCycle;
		String FailStatement = "FAIL >> Report Cycle Column Name is not updated when System Log Report is Scheduled for:"+ReportCycle;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[2]")).getText();
		ExpectedValue=MailTo;
		PassStatement = "PASS >> Mail To Column Name is updated succesfully when System Log Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Mail To Column Name is not updated when System Log Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[3]")).getText();
		ExpectedValue=Group;
		PassStatement = "PASS >> Group Column Name is updated succesfully when System Log Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Group Column Name is not updated when System Log Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}

    public void LogErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Please select type of logs for report.' is displayed successfully when schedule with No logs selected";
		String FailStatement="Fail >> Error Message : 'Please select type of logs for report.' is not displayed when schedule with No logs selected";
		Initialization.commonmethdpage.ConfirmationMessageVerify(LogsErrorMeesage, true, PassStatement, FailStatement,5);
	}
    
    public void VerifyOfSystemColumnName_CallLog()
	{
		WebElement[] wb = { ReportCycleColumnName, MailToColumnName,GroupColumnName_CallLog};
		String[] ExpectedValue = { "Report Cycle", "Mail To", "Device Name Or Group", "Call Type"};
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of Call Log Tracking";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of Call Log Tracking";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		   
		}
	}
	
	public void VerifyOfCallTypeSection(String ReportName)
	{
		boolean value=CallTypeSection.isDisplayed();
		String PassStatement="PASS >> 'Call Type' Section is displayed successfully when Clicked on Schedule New in Call Log Tracking"+ReportName;
		String FailStatement="FAIL >> 'Call Type' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	}
	
	public void ClickOnSelectDeviceOrGroup() throws InterruptedException
	{
		SelectGroup_CallLog.click();
		waitForidPresent("adddevicebtn");
		sleep(5);
	}
	
	public void SelectCallType(String Call_Type)
	{
		Select sel=new Select(CallType);
		sel.selectByVisibleText(Call_Type);
	}
	
	public void VerifyOfColumnGroupReportMail_CallLog(int row,String ReportCycle,String Group,String MailTo,String Call_Type)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[1]/p")).getText();
		String ExpectedValue=ReportCycle;
		String PassStatement = "PASS >> Report Cycle Column Name is updated succesfully when Call Log Tracking Report is Scheduled for "+ReportCycle;
		String FailStatement = "FAIL >> Report Cycle Column Name is not updated when Call Log Tracking Report is Scheduled for:"+ReportCycle;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[2]")).getText();
		ExpectedValue=Group;
		PassStatement = "PASS >> Group Column Name is updated succesfully when Call Log Tracking Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Group Column Name is not updated when Call Log Tracking Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[3]")).getText();
		ExpectedValue=MailTo;
		PassStatement = "PASS >> Mail To Column Name is updated succesfully when Call Log Tracking Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Mail To Column Name is not updated when Call Log Tracking Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[4]")).getText();
		ExpectedValue=Call_Type;
		PassStatement = "PASS >> Call Type Column Name is updated succesfully when Call Log Tracking Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Call Type Column Name is not updated when Call Log Tracking Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}
	
	public void ClickOnSelectGroup_CallLog() throws InterruptedException{
		SelectGroup_CallLog.click();
		waitForidPresent("adddevicebtn");
		sleep(5);
	}
	
	public void SelectGroupHeader_CallLog(){
		boolean value=SelectGroupHeader_CallLog.isDisplayed();
		String PassStatement="PASS >> Group list Header is dispalyed succesfully when Clicked on Select Group";
		String FailStatement="FAIL >> Group list Header is not dispalyed when Clicked on Select Group";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	}
	
	public void ClickOnCloseSelectGroup_CallLog() throws InterruptedException{
		SelectGroupCloseBtn_CallLog.click();
		Reporter.log("Group List popup is closed succesfully when Clicked on Close button of Group List Popup",true);
		sleep(4);
	}
	
	public void SelectGroup_CallLog() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		
		Initialization.driver.findElement(By.xpath("//div[@id='dtree']/ul/li[text()='"+ELib.getDatafromExcel("Sheet1",65,1)+"']")).click();
	    sleep(5);
	}
	
	public void VerifyOfReportsPage_DeviceHealth(String ReportName) throws InterruptedException
	{
		String ActualValue=ReportHeader.getText();
		String ExpectedValue=ReportName;
		String PassStatement="PASS >> "+ReportName+" Report Header is Displayed succesfully";
		String FailStatement="FAIL >> "+ReportName+" Report Header is not displayed";
		ALib.AssertEqualsMethod(ActualValue,ExpectedValue, PassStatement, FailStatement);
		ActualValue=ReportSubHeader.getText();
		System.out.println(ActualValue);
		ExpectedValue="Report containing device battery level, available physical memory and storage space";
		System.out.println(ExpectedValue);
		boolean value=ExpectedValue.contains(ActualValue);
		PassStatement="PASS >> "+ReportName+" Report Sub-Header is Displayed succesfully as "+ActualValue;
		FailStatement="FAIL >> "+ReportName+" Report Sub-Header is not displayed "+ActualValue;
		ALib.AssertTrueMethod(value,PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified "+ReportName+" Page *****",true);
	}
	
	public void VerifyOfSystemColumnName_DeviceHealth()
	{
		WebElement[] wb = { ReportCycleColumnName, MailToColumnName,GroupColumnName,BatteryColumnName,StorageSpaceColumnName,PhysicalMemoryColumnName};
		String[] ExpectedValue = { "Report Cycle", "Mail To", "Group" ,"Battery <=","Storage Space <= ","Physical Memory <="};
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of Device History";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of Device History";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		   
		}
	}
	
	public void VerifyOfMemory(String ReportName)
	{
		boolean value=BatterySection.isDisplayed();
		String PassStatement="PASS >> 'Battery' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		String FailStatement="FAIL >> 'Battery' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	    value=StorageSpaceSection.isDisplayed();
		PassStatement="PASS >> 'Storage Space' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> 'Storage Space' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
		value=PhysicalMemorySection.isDisplayed();
		PassStatement="PASS >> 'Physical Memory' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> 'Physical Memory' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	}
	
	public void EnterBattery(String BatteryValue)
	{
		BatteryTextBox.sendKeys(BatteryValue);
	}
	
	public void ClearBattery()
	{
		BatteryTextBox.clear();
	}
	
	public void EnterStorageSpace(String StorageSpaceValue)
	{
		StorageSpaceTextBox.sendKeys(StorageSpaceValue);
	}
	
	public void ClearStorageSpace()
	{
		StorageSpaceTextBox.clear();
	}
	
	public void EnterPhysicalMemory(String PhysicalMemoryValue)
	{
		PhysicalMemoryTextBox.sendKeys(PhysicalMemoryValue);
	}
	
	public void ClearPhysicalMemory()
	{
		PhysicalMemoryTextBox.clear();
	}
	
	public void VerifyOfColumnGroupReportMail_DeviceHealth(int row,String ReportCycle,String Battery,String StorageSpace,String PhyscicalMemory,String ReportName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[4]")).getText();
		String ExpectedValue=Battery;
		String PassStatement = "PASS >> Battery Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle;
		String FailStatement = "FAIL >> Battery Column Name is not updated when "+ReportName+" Report is Scheduled for:"+ReportCycle;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[5]")).getText();
		ExpectedValue=StorageSpace;
		PassStatement = "PASS >> Storage Space Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Storage Space Column Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[6]")).getText();
		ExpectedValue=PhyscicalMemory;
		PassStatement = "PASS >> Physcical Memory Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> Physcical Memory Column Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}
	
	//App Version Report
	public void VerifyOfReportsPage_AppVersion(String ReportName) throws InterruptedException
	{
		String ActualValue=ReportHeader.getText();
		String ExpectedValue=ReportName;
		String PassStatement="PASS >> "+ReportName+" Report Header is Displayed succesfully";
		String FailStatement="FAIL >> "+ReportName+" Report Header is not displayed";
		ALib.AssertEqualsMethod(ActualValue,ExpectedValue, PassStatement, FailStatement);
		ActualValue=ReportSubHeader.getText();
		System.out.println(ActualValue);
		ExpectedValue="Get version of application across devices";
		System.out.println(ExpectedValue);
		boolean value=ExpectedValue.contains(ActualValue);
		PassStatement="PASS >> "+ReportName+" Report Sub-Header is Displayed succesfully as "+ActualValue;
		FailStatement="FAIL >> "+ReportName+" Report Sub-Header is not displayed "+ActualValue;
		ALib.AssertTrueMethod(value,PassStatement, FailStatement);
		Reporter.log("***** PASS >> Verified "+ReportName+" Page *****",true);
	}
	
	public void VerifyOfSystemColumnName_AppVersion(String ReportName)
	{
		WebElement[] wb = { ReportCycleColumnName, MailToColumnName,GroupColumnName,AppTypeColumnName,ApplicationNameColumnName};
		String[] ExpectedValue = { "Report Cycle", "Mail To", "Group","App Type", "App Name"};
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of "+ReportName;
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of "+ReportName;
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		   
		}
	}
	
	public void AppNameUnknownErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Please provide application name.' is displayed successfully when schedule with No application in App Version Report";
		String FailStatement="Fail >> Error Message : 'Please provide application name.' is not displayed when schedule with No application in App Version Report";
		Initialization.commonmethdpage.ConfirmationMessageVerify(AppNameUnknownErrorMeesage, true, PassStatement, FailStatement,5);
		sleep(5);
	}
	
	public void AppNameLessCharactersErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Application name cannot be less than 3 characters.' is displayed successfully when Appliaction less than 3 characters in App Version Report";
		String FailStatement="Fail >> Error Message : 'Application name cannot be less than 3 characters.' is not displayed when schedule with Appliaction less than 3 characters in App Version Report";
		Initialization.commonmethdpage.ConfirmationMessageVerify(AppNameLessCharatersErrorMeesage, true, PassStatement, FailStatement,5);
		sleep(5);
	}
	
	public void EnterApplicationName(String AppName)
	{
		AppNameTextBox.sendKeys(AppName);
	}
	
	public void ClearApplicationName()
	{
		AppNameTextBox.clear();
	}
	
	public void SelectAppType(String AppType)
	{
		
		 Select sel=new Select(AppTypeDropDown);
			sel.selectByVisibleText(AppType);
	}
	

	public void VerifyOfColumnGroupReportMail_AppVersion(int row,String ReportCycle,String AppType,String AppName,String ReportName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[4]")).getText();
		String ExpectedValue=AppType;
		ExpectedValue=ExpectedValue.replace(" Apps", "");
		String PassStatement = "PASS >> App Type Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle;
		String FailStatement = "FAIL >> App Type Column Name is not updated when "+ReportName+" Report is Scheduled for:"+ReportCycle;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[5]")).getText();
		ExpectedValue=AppName;
		PassStatement = "PASS >> App Name Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> App Name Column Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}
	
	public void AppVersionDelete() throws InterruptedException
	{
		int row=VerifyOfHowmanyRows();
		sleep(5);
		for(int i=0;i<row;i++)
		{
		ClickOnRows();
		ScheduleDeleteBtn.click();
		sleep(5);
		}
	}
	
	//installed job report
	public void VerifyOfSystemColumnName_InstalledJob()
	{
		WebElement[] wb = { ReportCycleColumnName, MailToColumnName,GroupColumnName,JobsColumnName,ApplicationNameColumnName};
		String[] ExpectedValue = { "Report Cycle", "Mail To", "Group" ,"Job Name","App Name"};
		for (int i = 0; i <= wb.length - 1; i++) {
			boolean value = wb[i].isEnabled();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Column Name is Displayed succesfully when Clicked on Schedule Reports of Device History";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Column Name is not Displayed when Clicked on Schedule Reports of Device History";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		   
		}
	}
	
	public void VerifyOfJobAppName(String ReportName)
	{
		boolean value=JobsSection.isDisplayed();
		String PassStatement="PASS >> 'Select Job' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		String FailStatement="FAIL >> 'Select Job' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	    value=ApplicationNameSection.isDisplayed();
		PassStatement="PASS >> 'Application Name' Section is displayed successfully when Clicked on Schedule New in "+ReportName;
		FailStatement="FAIL >> 'Application Name' Section is not displayed when Clicked on Schedule New in "+ReportName;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
		}
	
	public void JobErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Please provide job.' is displayed successfully when Appliaction less than 3 characters in App Version Report";
		String FailStatement="Fail >> Error Message : 'Please provide job.' is not displayed when schedule with Appliaction less than 3 characters in App Version Report";
		Initialization.commonmethdpage.ConfirmationMessageVerify(JobErrorMeesage, true, PassStatement, FailStatement,5);
		sleep(5);
	}
	
	public void ClickOnSelectJob() throws InterruptedException
	{
		SelectJob.click();
		waitForidPresent("okbtn");
		sleep(5);
	}
	
	public void SelectJobHeader(){
		boolean value=SelectJobHeader.isDisplayed();
		String PassStatement="PASS >> 'Select Jobs To Add' Header is dispalyed succesfully when Clicked on Select Group";
		String FailStatement="FAIL >> 'Select Jobs To Add' Header is not dispalyed when Clicked on Select Group";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	}
	
	public void SelectJobErrorMessage() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Please select a job.' is displayed successfully when No Job is selected";
		String FailStatement="Fail >> Error Message : 'Please select a job.' is not displayed when No Job is selected";
		Initialization.commonmethdpage.ConfirmationMessageVerify(SelectJobErrorMeesage, true, PassStatement, FailStatement,5);
		sleep(5);
	}
	
	public void ClickOnCloseSelectJob() throws InterruptedException{
		CloseSelectJob.click();
		Reporter.log("'Select Jobs to Add' popup is closed succesfully when Clicked on Close button of Select Jobs To Add Popup",true);
		sleep(4);
		
	}
	
	
	public void ClickOnSelectJobOkBtn() throws InterruptedException
	{
		SelectJobOkBtn.click();
		Reporter.log("'Select Jobs To Add' is saved succesfully when Clicked on Ok button of Select Jobs To Add Popup",true);
		sleep(2);
	}
	
	public void EnterSearchFieldSelectJob(String JobName) throws InterruptedException{
		SearchFieldSelectJob.sendKeys(JobName);
		
	}
	
	public void VerifyofSearch_SelectJob(String JobName) throws InterruptedException
	{
		boolean value=Initialization.driver.findElement(By.xpath("//p[contains(text(),'"+JobName+"')]")).isDisplayed();
		String PassStatement="PASS >> Job is searched succesfully when Searched with valid Device Name in Select Job popup";
		String FailStatement="Fail >> Job is not searched when Searched with valid Device Name in Select Job popupp";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(5);
	}
	
	public void SearchGridMessage_Job() throws InterruptedException
	{
		boolean value=SearchGridMessage_Job.isDisplayed();
		String PassStatement="PASS >> Grid Message : 'No jobs available.' is displayed successfully when Searched with invalid Device Name in Select Group popup";
		String FailStatement="Fail >> Grid Message : 'No jobs available.' is not displayed when Searched with invalid Device Name in Select Group popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void SelectJob_InstalledJob(String JobName) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		List<WebElement> wb=Initialization.driver.findElements(By.xpath("//table[@id='CompJobDataGrid']/tbody/tr/td/p[contains(text(),'"+JobName+"')]"));
	    wb.get(0).click();
		sleep(5);
	}
	
	public void VerifyOfColumnGroupReportMail_InstalledJob(int row,String ReportCycle,String JobName,String AppName,String ReportName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[4]")).getText();
		String ExpectedValue=JobName;
		String PassStatement = "PASS >> Job Name Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle;
		String FailStatement = "FAIL >> Job Name Column Name is not updated when "+ReportName+" Report is Scheduled for:"+ReportCycle;
		boolean value=ActualValue.contains(ExpectedValue);
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		ActualValue=Initialization.driver.findElement(By.xpath("//table[@id='tbl_schedulereports']/tbody/tr["+row+"]/td[5]")).getText();
		ExpectedValue=AppName;
		PassStatement = "PASS >> App Name Column Name is updated succesfully when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		FailStatement = "FAIL >> App Name Column Name is not updated when "+ReportName+" Report is Scheduled for "+ReportCycle+" as:"+ActualValue;
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		
	}
	
	public boolean CheckScheduleReportBtn()
	{
		boolean value=ScheduleNewBtn.isDisplayed();
	    return value;
	}
	
	public boolean CheckReportBtn()
	{
		boolean value = true;
		try{
			ReportsBtn.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	

	public void ClickonSystemLog_UM() throws InterruptedException
	{
		SystemLogReport.click();
		Reporter.log("PASS >> Clicked on System Log  Reports , Navigates to System Log  Reports Section", true);
		
	}
	
	public void ClickonAssetTracking_UM() throws InterruptedException
	{
		AssetTrackingReport.click();
		Reporter.log("PASS >> Clicked on Asset Tracking  Reports , Navigates to Asset Tracking  Reports Section", true);
		
	}
	
	public void ClickonJobsDeployed_UM() throws InterruptedException
	{
		JobsDeployedReport.click();
		Reporter.log("PASS >> Clicked on Jobs Deployed  Reports , Navigates to Jobs Deployed  Reports Section", true);
		
	}
	
	public void ClickonCallLogTracking_UM() throws InterruptedException
	{
		CallLogTrackingReport.click();
		Reporter.log("PASS >> Clicked on Call Log Tracking  Reports , Navigates to Call Log Tracking  Reports Section", true);
		
	}
	
	public void ClickonDeviceHistory_UM() throws InterruptedException
	{
		DeviceHistoryReport.click();
		Reporter.log("PASS >> Clicked on Device History  Reports , Navigates to Device History  Reports Section", true);
		
	}
	
	public void ClickonDataUsage_UM() throws InterruptedException
	{
		DataUsageReport.click();
		Reporter.log("PASS >> Clicked on Data Usage  Reports , Navigates to Data Usage  Reports Section", true);
		
	}
	
	public void ClickonDeviceConnected_UM() throws InterruptedException
	{
		DeviceConnectedReport.click();
		Reporter.log("PASS >> Clicked on Device Connected  Reports , Navigates to Device Connected  Reports Section", true);
		
	}
	
	public void ClickonDeviceHealth_UM() throws InterruptedException
	{
		DeviceHealthReport.click();
		Reporter.log("PASS >> Clicked on Device Health  Reports , Navigates to Device Health  Reports Section", true);
		
	}
	
	public void ClickonAppVersion_UM() throws InterruptedException
	{
		AppVersionReport.click();
		Reporter.log("PASS >> Clicked on App Version  Reports , Navigates to App Version  Reports Section", true);
		
	}
	
	public void ClickonInstalledJobReport_UM() throws InterruptedException
	{
		InstalledJobReport.click();
		Reporter.log("PASS >> Clicked on Installed Job  Reports , Navigates to Installed Job Reports Section", true);
		
	}
}
