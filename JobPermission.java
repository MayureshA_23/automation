package UserManagement_TestScripts;

//DeviceAction text job should be there

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class JobPermission extends Initialization {
	
	@Test(priority='1',description="1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException{
	Reporter.log("\n1.To Verify roles option is present",true);
	commonmethdpage.ClickOnSettings();
	usermanagement.ClickOnUserManagementOption();
	}
	@Test(priority='2',description="2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException{
	Reporter.log("\n2.To Verify default super user",true);

	usermanagement.toVerifySuperUsertemplete();
	}
	@Test(priority='3',description="3.To create a roll for Job Permission")
	public void CreateRollForJobPermission() throws InterruptedException{
		Reporter.log("\n3.To create a roll for Job Permission",true);
	
		usermanagement.JobPermissionRole();
	}
	@Test(priority='4',description="4.To create user for Job Permission")
	public void CreateUserForJobPermissions() throws InterruptedException{
		Reporter.log("\n4.To create user for Job Permission",true);
		usermanagement.CreateUserForPermissions("AutoJobPermission", "42Gears@123","AutoJobPermission", "Job Permission");  // uncomment remove later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoJobPermission","42Gears@123");
		//usermanagement.RefreshCurrentPage();
		//usermanagement.selectDeviceConsole();
		
	}
	@Test(priority='5',description="5.To check access for Job Permission")
	public void AcessToJobPermission() throws InterruptedException{
		Reporter.log("\n5.To check access for Job Permission",true);
		
		usermanagement.JobAccessable();	
	}
	@Test(priority='6',description="6.To check access for Job Permission")
	public void AcessToNEWJobPermission() throws InterruptedException{
		Reporter.log("\n6.To check access for Job Permission",true);
		
		usermanagement.NewJobPermissionAccessable();	
	}
	@Test(priority='7',description="7.To check access for Job Permission")
	public void AcessToDeleteJobPermission() throws InterruptedException{
		Reporter.log("\n7.To check access for Job Permission",true);
		usermanagement.DeleteJobPermissionAccessable();	
	}
	
	@Test(priority='8',description="8.To check access for Job Permission")
	public void AcessToModifyJobPermission() throws InterruptedException{
		Reporter.log("\n8.access granted for Modify job permissions",true);
		usermanagement.ModifyJobPermission();	
	}
	
	@Test(priority='9',description="9.Disable all the Job Permission")
	public void ModifyDisableJobPermission() throws InterruptedException{
		Reporter.log("\n9.Disable all the Job Permission",true);
		
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID ,Config.password);	
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.DisableAllJobPermission();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoJobPermission", "42Gears@123");
		/*usermanagement.RefreshCurrentPage();
		usermanagement.selectDeviceConsole();*/

	}
	@Test(priority='A',description="10.access granted for delete job permissions")
	public void DisabledNewJobPermission() throws InterruptedException{
		Reporter.log("\n10.access granted for delete job permissions",true);
		
		usermanagement.NewJodDisabled();	
		Thread.sleep(2000);
	}
	@Test(priority='B',description="6.To check access denied for Job Permission")
	public void DisabledDeleteJobPermission() throws InterruptedException{
		Reporter.log("\n11.access granted for delete job permissions",true);	
		usermanagement.DeleteJobAccessDenied();	
		Thread.sleep(2000);
	}
	@Test(priority='C',description="6.To check access denied for Job Permission")
	public void DisabledModifyJobPermission() throws InterruptedException{
		Reporter.log("\n12.access granted for delete job permissions",true);	
		usermanagement.ModifyJobAccessDenied();	
		Thread.sleep(2000);
	}
	

}
