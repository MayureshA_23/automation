package OnDemandReports_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class AppVersion extends Initialization {

	@Test(priority = 1, description = "1.Verify All On Demand Reports")
	public void VerifyAllOnDemandReports_TC_RE_31() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n1.Verify All On Demand Reports", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.verifyOfOnDemandReports();
		
	}

	@Test(priority = 2, description = "2.To Verify Clicking On App Version Report")
	public void VerifyClickingOnAppVersionReport() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n2.To Verify Clicking On App version report and its header", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
	}

	@Test(priority = 3, description = "3. Generate And View the 'App Version Report'  for 'Home' group where App Type = 'Downloaded Apps'")
	public void VerifyingGenerateReportAfterProvidingTheApplicationName() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n3.Generate And View the 'App Version Report'  for 'Home/Sub' group where App Type = 'Downloaded Apps'",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName1);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		reportsPage.ViewReportAppVersion();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.CheckApplicationVersion(Config.AppVersion1);
		reportsPage.SwitchBackWindow();
	}
	@Test(priority = 4, description = "4. Generate And View the 'App Version Report'  for 'Sub' group where App Type = 'Downloaded Apps'")
	public void VerifyingAppVersionRep_SubGroup() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n4.Generate And View the 'App Version Report'  for Sub group where App Type = 'Downloaded Apps'",true);
		commonmethdpage.ClickOnHomePage();
    	rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
    	
		commonmethdpage.ClickOnAllDevicesButton();
		rightclickDevicegrid.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
//		rightclickDevicegrid.VerifyDeviceMoved();
		
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		
		reportsPage.SelectApplicationType("Downloaded Apps");
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName1);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		reportsPage.ViewReportAppVersion();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.CheckApplicationVersion(Config.AppVersion1);
		reportsPage.SwitchBackWindow();
	}
	@Test(priority = 5, description = "5.Generate And View the 'App Version Report'  for 'Home' group where App Type = 'All Apps'")
	public void VerifyAppVersionReportHomeGroupAllApps() throws InterruptedException {
		Reporter.log("\n5.Generate And View the 'App Version Report'  for 'Home' group where App Type = 'All Apps'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.SelectApplicationType("All Apps");
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName1);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		reportsPage.ViewReportAppVersion();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.CheckApplicationVersion(Config.AppVersion1);
		reportsPage.SwitchBackWindow();
	}
	@Test(priority = 6, description = "6.Generate And View the 'App Version Report'  for 'Sub' group where App Type = 'All Apps'")
	public void VerifyAppVersionReportSubGroupAllApps() throws InterruptedException {
		Reporter.log("\n6.Generate And View the 'App Version Report'  for 'Sub' group where App Type = 'All Apps'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.SelectApplicationType("All Apps");
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName1);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		reportsPage.ViewReportAppVersion();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.CheckApplicationVersion(Config.AppVersion1);
		reportsPage.SwitchBackWindow();
		
	}

	@Test(priority = 7, description = "7.Generate And View the 'App Version Report'  for 'Home' group where App Type = 'System Apps'")
	public void VerifyAppVersionReportHomeGroupSystemApps()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n7.Generate And View the 'App Version Report'  for 'Home' group where App Type = 'System Apps'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.SelectApplicationType("System Apps");
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName2);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		reportsPage.ViewReportAppVersion();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.CheckApplicationVersion(Config.AppVersion2);
		reportsPage.SwitchBackWindow();
	}
	@Test(priority = 8, description = "8.Generate And View the 'App Version Report'  for 'Sub' group where App Type = 'System Apps'")
	public void VerifyAppVersionReportSubGroupSystemApps()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n8.Generate And View the 'App Version Report'  for 'Sub' group where App Type = 'System Apps'",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.SelectApplicationType("System Apps");
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName2);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		reportsPage.ViewReportAppVersion();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.verifyOfDeviceName(Config.DeviceName);
		reportsPage.CheckApplicationVersion(Config.AppVersion2);
		reportsPage.SwitchBackWindow();
	}
	@Test(priority = 9, description = "9.Download and Verify the data for App Version Report For 'Home' Group")
	public void VerifyDownloadingAppVersionReportForHomeGroup() throws InterruptedException {
		Reporter.log("\n9.Download and Verify the data for App Version Report For 'Home' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName1);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}
	
	@Test(priority = 10, description = "10.Download and Verify the data for App Version Report For 'Sub' Group")
	public void VerifyDownloadingAppVersionReportForSubGroup() throws InterruptedException {
		Reporter.log("\n10.Download and Verify the data for App Version Report For 'Sub' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.VerifyClickingOnAppVersionReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectTheGroup();
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.SelectApplicationType("System Apps");
		reportsPage.VerifyAppVersionReportGenerateWithApplicationName(Config.AppName2);	
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnViewReportsOption();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();

		}
	}

}