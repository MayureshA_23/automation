package Settings_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceEnrollmentRules extends Initialization {
	
	@Test(priority = 1, description = "Verify Device enrolment Rules in Account Settings")
	public void VerifyDeviceEnrolmentRulesInAccountSettings() throws InterruptedException {
		Reporter.log("1.Verify Device enrolment Rules in Account Settings", true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		accountsettingspage.VerifyDeviceEnrollmentRulesInAccSettings();
		//accountsettingspage.VerifyAuthTypesInDropDown();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 2, description = "Verify Device enrolment Rules-Device Authentication Type Required Password error message")
	public void VerifyDeviceEnrollmentRequiredPWDErrorMessage() throws InterruptedException {
		Reporter.log("\n2.Verify Device enrolment Rules-Device Authentication Type Required Password error message",
				true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		DeviceEnrollment_SCanQR.SettingPassword("12345");
		accountsettingspage
				.ClickOnDeviceEnollmentRulespplyButton_ErrorMessage(accountsettingspage.PasswordLengthErrorMessage);
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 3, description = "Verify Device enrolment Rules-Device Authentication Type as OAuth Authentication with Advanced Device Authentication enabled and keep all values as blank.")
	public void VerifyOAuthWihtADAEnabledAndKeepingValesBlank() throws InterruptedException {
		Reporter.log(
				"\n3.Verify Device enrolment Rules-Device Authentication Type as OAuth Authentication with Advanced Device Authentication enabled and keep all values as blank.",
				true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		accountsettingspage.ClickOnDeviceEnollmentRulespplyButton_ErrorMessage(accountsettingspage.OAuthEmptyErrorMessage);
		commonmethdpage.ClickOnHomePage();

	}

	@Test(priority = 4, description = "Verify Device enrolment Rules-Device Authentication Type as OAuth Authentication with Advanced Device Authentication disabled and keep all required field as blank for native and web application.")
	public void VerifyOAuthWihtADADisabledAndKeepingValesBlank() throws InterruptedException {
		Reporter.log(
				"\n4.Verify Device enrolment Rules-Device Authentication Type as OAuth Authentication with Advanced Device Authentication disabled and keep all required field as blank for native and web application.",
				true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.UncheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		DeviceEnrollment_SCanQR.SettingOAuthType_NativeappTab("4", "GSUITE");
		accountsettingspage.ClearingTextFields();
		accountsettingspage
				.ClickOnDeviceEnollmentRulespplyButton_ErrorMessage(accountsettingspage.OAuthEmptyErrorMessage);
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 5, description = "Verify entering invalid data in tenant ID field.")
	public void VerifyErrorMessageByKeepingAPPIDEmpty_TC_ST_423() throws InterruptedException {
		Reporter.log("\n5.Verify entering invalid data in tenant ID field.", true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		accountsettingspage.EnteringDataInTenantID();
		accountsettingspage.VerifyErrorMessageOnEnteringInvalidTenantID();
		commonmethdpage.ClickOnHomePage();

	}
	
	//Kavya
	@Test(priority = 6, description = "Verify error message by keeping Application ID field as empty.")
	public void VerifyErrorMessageByKeepingAPPIDEmpty_TC_ST_424() throws InterruptedException {
		Reporter.log("\n6.Verify error message by keeping Application ID field as empty.", true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();		
		accountsettingspage.enterAuthEndPoint("kSmWerq123");
		accountsettingspage.enterTokenEndpoint("kahteyfgu987");
		accountsettingspage.enterClientID("ajsakjiue");
		accountsettingspage.enterClientSecret("zjdsnfkjke");
		accountsettingspage.enterTenantid(Config.TenantId);
		accountsettingspage.enterAppIDClear();
		accountsettingspage.enterAppSecret("hdjshb");
		accountsettingspage.ClickonApplyButton();
		accountsettingspage.ErrorMessageforEmptyApplicationID();
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority = 7, description = "Verify error message by keeping Application Secret field as empty.")
	public void VerifyErrorMessageByKeepingAPPSecretEmpty_TC_ST_425() throws InterruptedException {
		Reporter.log("\n7.Verify error message by keeping Application Secret field as empty.",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_OAuthAuthentication();
		accountsettingspage.enterAuthEndPoint("kSmWerq123");
		accountsettingspage.enterTokenEndpoint("kahteyfgu987");
		accountsettingspage.enterClientID("ajsakjiue");
		accountsettingspage.enterClientSecret("zjdsnfkjke");
		accountsettingspage.enterTenantid(Config.TenantId);
		accountsettingspage.enterAppID("sakfewisj");
		accountsettingspage.ClearApplicationSecreteKey();
		accountsettingspage.ClickonApplyButton();
		accountsettingspage.ErrorMessageForEmptyApplicationSecret();
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority = 8, description = "Verify the UI when device authentication type is Require Password.")
	public void VerifyShowPwd_TC_ST_486() throws InterruptedException {
		Reporter.log("\n8.Verify the UI when device authentication type is Require Password.",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		accountsettingspage.verifyShowpassword();
		commonmethdpage.ClickOnHomePage();
	}	
	@Test(priority = 9, description = "Verify the UI when device authentication type is Require Password.")
	public void VerifyEnterPwdAndBypassPwd_TC_ST_486() throws InterruptedException {
		Reporter.log("\n9.Verify the UI when device authentication type is Require Password.",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_RequirePassword();
		accountsettingspage.verifyEnterPassword();
		accountsettingspage.verifyBypassPwd();
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority = 10, description = "verify all the prompt and pages in device enrollment with the different screen resolutions.")
	public void VerifyEnterPwdAndBypassPwd_TC_ST_739() throws InterruptedException {
		Reporter.log("\n10.verify all the prompt and pages in device enrollment with the different screen resolutions.",true);
		commonmethdpage.ClickOnSettings();
		deviceEnrollment.clickOnDeviceEnrollment();
		deviceEnrollment.verifyPromtsInDevEnrollment();
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority = 11, description = "Verify Switch to Old Console")
	public void VerifySwitchToOldConsole_TC_ST_390() throws InterruptedException {
		Reporter.log("\n11.Verify Switch to Old Console",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonTrynewconsole();
		accountsettingspage.SwitchtoOldConsole();		
	}

	//mithilesh
	@Test(priority=12,description="Verify the UPN with different accounts") 
	public void VerifytheUPNwithdifferentaccountsATC_ST_761() throws Throwable {
		Reporter.log("\n12.Verify the UPN with different accounts",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonDeviceEnrollmentRules();
		accountsettingspage.UPNOption();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.AccountNameVisible();
		commonmethdpage.ClickOnSettings();
}

	@Test(priority=13,description="Verifiy the options in the Device enrollment Rules") 
	public void VerifiytheoptionsintheDeviceenrollmentRulesTC_ST_765() throws Throwable {
		Reporter.log("\n13.Verifiy the options in the Device enrollment Rules",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonDeviceEnrollmentRules();
		accountsettingspage.StagingProvisioningOptions();				
}
	
}
