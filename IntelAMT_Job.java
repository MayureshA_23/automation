package JobsOnAndroid;

import java.io.IOException;

import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class IntelAMT_Job extends Initialization {
	
	@Test(priority=1, description="\n Intel AMT jobs")
	public void TC_JO_810() throws InterruptedException, IOException{
		
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.VerifyIntelAMTjob();

		
		
}
	@Test(priority=2, description="\n Intel AMT jobs from standard account"+"Intel AMT jobs from Premium account")
	public void TC_JO_814() throws InterruptedException, IOException{

		usermanagement.NavigateUrl(Config.StandardAccURL);
		usermanagement.LoginAsNewUser(Config.StandardAccUser,Config.StandardAccPwd);
		usermanagement.WaitForLogin();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.VerifyIntelAmtJobInStandardAndPremiumAcc();

		usermanagement.NavigateUrl(Config.PremiumAccURL);
		usermanagement.LoginAsNewUser(Config.PremiumAccUser,Config.PremiumAccPwd);
		usermanagement.WaitForLogin();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.VerifyIntelAmtJobInStandardAndPremiumAcc();


}
}
