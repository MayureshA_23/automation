package DynamicJobs_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DynamicSureVideo extends Initialization{
	
	@Test(priority='1',description="1.Verify surevideo install from XSLT")
	public void ToVerifyInstallSureVideoFromXSLT() throws InterruptedException, Throwable{
		Reporter.log("\n1.Verify surevideo install from XSLT",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSurevideoSettings();
		dynamicjobspage.MessageDuringInstallationWhenSureVideoNotInstalled();
		dynamicjobspage.ClickOnYesButtonSureVideoInstallation(); 
		dynamicjobspage.VerifySureVideoActivityLogsDeployed();
     //   dynamicjobspage.CheckSureVideoInstallationLogs();
		commonmethdpage.ClickOnDynamicRefresh();
	}

    @Test(priority='2',description="2.To Verify Dynamic SureVideo Settings Job Popup UI")
	public void VerifySureVideoSettingsPopupUI() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Dynamic SureVideo Settings Job Popup UI",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
        dynamicjobspage.ClickOnMoreOption();
        dynamicjobspage.ClickOnSurevideoSettings();
        dynamicjobspage.ClickOnMessageOnSureVideoInstallation();
	    dynamicjobspage.VerifyOfSureVideoSettings();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();

		Reporter.log("PASS>> Verification of Dynamic SureVideo Settings Job Popup UI",true);
	}
    
  
	
	@Test(priority='3',description="3.To Verify Dynamic SureVideo Settings Job when XML text area is empty")
	public void VerifySavingEmptySureVideoXMLSettings() throws InterruptedException, Throwable{
		Reporter.log("\n3.To Verify Dynamic SureVideo Settings Job when XML text area is empty",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();

		dynamicjobspage.ClickOnSurevideoSettings();
        dynamicjobspage.ClickOnEditXMLButton();
		dynamicjobspage.ClearSureVideoEditXMLArea();
		dynamicjobspage.ClickOnSaveSource();
		dynamicjobspage.SureVideoInvalidSettingsErrorMessage();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();
	}
	
	
	@Test(priority='4',description="4.Verify Edit surevideo settings and Edit XML")
	public void VerifyEditSureVideoSettingsEditXML() throws InterruptedException, Throwable{
		Reporter.log("\n4.Verify Edit surevideo settings and Edit XML",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();

		dynamicjobspage.ClickOnSurevideoSettings();
		dynamicjobspage.ClickOnEditXMLButton();
		dynamicjobspage.ClickOnSaveSource();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();

		Reporter.log("PASS>> To Verify Warning Message on SureVideo Settings Save as Job without Job Name",true);
	}
	
	 
	@Test(priority='5',description="5.To Verify Warning Message on SureVideo Settings Save as Job without Job Name")
	public void VerifyWarningMessageOfSureVideoSettingsSaveAsJobWhenSavingWithoutJobName() throws InterruptedException, Throwable{
		Reporter.log("\n5.To Verify Warning Message on SureVideo Settings Save as Job without Job Name",true);
	

		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSurevideoSettings();
        dynamicjobspage.ClickOnSureVideoSaveAsJob();
		dynamicjobspage.VerifyOfJobDetailsPopup();
		dynamicjobspage.ClickOnSaveButtonOfJobDetails();
		dynamicjobspage.SaveAsJobErrorMessageSureVideo();
		dynamicjobspage.ClickOnJobDetailsCloseBtn_SureVideo();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();

		Reporter.log("PASS>> To Verify Warning Message on SureVideo Settings Save as Job without Job Name",true);
	}

	
	@Test(priority='6',description="6.To Verify Save As Job of the SureVideo Settings")
	public void VerifySaveAsJobSureVideoSettings() throws InterruptedException, Throwable{
		Reporter.log("\n6.To Verify Save As Job of the SureVideo Settings",true);


		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSurevideoSettings();

		dynamicjobspage.ClickOnSureVideoSaveAsJob();
		dynamicjobspage.VerifyOfJobDetailsPopup();
		dynamicjobspage.EnterJobName(Config.SVSaveAsJob);
		dynamicjobspage.EnterPassword();
		dynamicjobspage.ClickOnSaveButton();
		dynamicjobspage.ClickOnSurevideoSettingsCloseBtn();
	    jobsOnAndroidpage.ClickOnJobs();
		jobsOnAndroidpage.ClearSearch();
		jobsOnAndroidpage.SearchJob(Config.SVSaveAsJob);
		dynamicjobspage.VerifyOfSureVideoSaveAsJob();
		commonmethdpage.ClickOnHomePage();
		Reporter.log("PASS>> Verification of Save As Job Button when Clicked on Close button of Job Details",true);
	}
	
	
	
	@Test(priority='7',description="7.Verify Edit surevideo settings and Refresh")
	public void VerifyEditSureVideoSettings_Refresh() throws InterruptedException, Throwable{
		Reporter.log("\n7.Verify Edit surevideo settings and Refresh",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSurevideoSettings();
		dynamicjobspage.ClickOnSureVideoSettingsMenu();
		dynamicjobspage.SureVideoRunAtStartUp();
		dynamicjobspage.DoneButtonSureVideoSettings();
		dynamicjobspage.ClickOnRefreshBtn();
		dynamicjobspage.ClickOnSureVideoSettingsMenu();
		dynamicjobspage.verifyEnablesureVideoRunAtStartUp("Run At Startup");
	//	dynamicjobspage.CheckRunAtStartUpAfterRefresh();
		dynamicjobspage.DoneButtonSureVideoSettings();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();

		
	}
	
	@Test(priority='8', description="8. Revert Settings Change and Apply")
	public void RevertSettingsChangesAndApply() throws Throwable
	{
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSurevideoSettings();
        dynamicjobspage.ClickOnSureVideoSettingsMenu();
		dynamicjobspage.SureVideoRunAtStartUp();
		dynamicjobspage.DoneButtonSureVideoSettings();
		dynamicjobspage.ClickOnSureVideoApplyButton();
		dynamicjobspage.ClickOnYesButtonApplySureVideoSettings();
		dynamicjobspage.EnterSureVideoPassword();
		dynamicjobspage.ApplyButtonSureVidePasswordPrompt();
		dynamicjobspage.ConfirmationMessageApplySettings();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();

	}
	
	
	@Test(priority='9',description="9.Verify Edit SureVideo settings")
	public void VerifyEditSureVideoSettings() throws InterruptedException, Throwable{
		Reporter.log("\n9.Verify Edit SureVideo settings and Apply",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSurevideoSettings();
    	dynamicjobspage.ClickOnSureVideoSettingsMenu();
		dynamicjobspage.SureVideoRunAtStartUp();
		dynamicjobspage.DoneButtonSureVideoSettings();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();
	}
	
	@Test(priority='A',description="10.To Verify Save As File of the SureVideo Settings")
	public void VerifySaveAsFileSureVideoSettings() throws InterruptedException, Throwable{
		Reporter.log("\n10.To Verify Save As File of the SureVideo Settings",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();

		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSurevideoSettings();
		dynamicjobspage.ClickOnSaveAsFileButton();
		dynamicjobspage.ClickOnSureVideoHomeSettingsCloseButton();
	}
	
	
	

	
}

	
		
	

