package OnDemandReports_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

import java.text.ParseException;

public class JobsDeployedReport extends Initialization {


	@Test(priority=1,description="Generate And View the 'Job Deployed'report for Selected 'Home' group")
	public void GenerateViewJobDeployedReportHomeGroup_TC_RE_40() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("1.Generate And View the 'Job Deployed'report for Selected 'Home' group",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.TextMessageJobname);
		commonmethdpage.Selectjob(Config.TextMessageJobname);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VarifyingCurrentDateInSystemLogOnDemandReport();
		reportsPage.VarifyingobDeployedTimeInJobDeployedOnDemandReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();

	}
	@Test(priority=2,description="Generate 'Yesterday's Job Deployed report for Selected 'Sub' group")
	public void GenerateViewJobDeployedReportHomeGroup_TC_RE_41() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n2.Generate 'Yesterday's Job Deployed report for Selected 'Sub' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForYesterdayInJobDeloyedReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority=3,description="Generate And View the 'Job Deployed'report for Selected 'Home' group",dependsOnMethods="GenerateViewJobDeployedReportHomeGroup_TC_RE_40")
	public void VerifyJobsDeployedRportColumns() throws InterruptedException
	{
		Reporter.log("\n3.Generate And View the 'Job Deployed'report for Selected 'Home' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyJobsDeployedReportColumns();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority=4,description="Verfy Job Status Column,Job Name Columne, Device Name Column, User ID Column",dependsOnMethods="VerifyJobsDeployedRportColumns")
	public void VerifyColumnsOfJobsStatusJobNameDeviceNameUserID() throws InterruptedException
	{
		Reporter.log("\n4.Verfy Job Status Column,Job Name Columne, Device Name Column, User ID Column",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();

	}
	@Test(priority=5,description="Generate 'Today's Job Deployed report for Selected 'Home' group")
	public void GenerateViewJobDeployedReportHomeGroup_Today() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n5.Generate 'Today's Job Deployed report for Selected 'Home' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_DeviceConnectedReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=6,description="Generate 'Today's Job Deployed report for Selected 'Sub' group")
	public void GenerateViewJobDeployedReportSubGroup_Today() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n6.Generate 'Today's Job Deployed report for Selected 'Sub' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_DeviceConnectedReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=7,description="Generate 'Last 1 Week's Job Deployed report for Selected 'Home' group")
	public void GenerateViewJobDeployedReportHomeGroup_Last1Week() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n7.Generate 'Last 1 Week's Job Deployed report for Selected 'Home' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week_DeviceConnectedReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=8,description="Generate 'Last 1 Week's Job Deployed report for Selected 'Sub' group")
	public void GenerateViewJobDeployedReportSubGroup_Last1Week() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n8.Generate 'Last 1 Week's Job Deployed report for Selected 'Sub' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week_DeviceConnectedReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=9,description="Generate 'Last 30 Day' Job Deployed report for Selected 'Home' group")
	public void GenerateViewJobDeployedReportHomeGroup_Last30Days() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n9.Generate 'Last 30 Day' Job Deployed report for Selected 'Home' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days_DeviceConnectedReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=10,description="Generate 'Last 30 Day' Job Deployed report for Selected 'Sub' group")
	public void GenerateViewJobDeployedReportSubGroup_Last30Days() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n10.Generate 'Last 30 Day' Job Deployed report for Selected 'Sub' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days_DeviceConnectedReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=11,description="Generate 'This Month' Job Deployed report for Selected 'Home' group")
	public void GenerateViewJobDeployedReportHomeGroup_ThisMonth() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n11.Generate 'This Month' Job Deployed report for Selected 'Home' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth_DeviceHealthReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=12,description="Generate 'This Month' Job Deployed report for Selected 'Sub' group")
	public void GenerateViewJobDeployedReportSubGroup_ThisMonth() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n12.Generate 'This Month' Job Deployed report for Selected 'Sub' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth_DeviceHealthReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=13,description="Generate 'Custom Range' Job Deployed report for Selected 'Home' group")
	public void GenerateViewJobDeployedReportHomeGroup_CustomRange() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n13.Generate 'Custom Range' Job Deployed report for Selected 'Home' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Custom Range");
		reportsPage.SelectingDateRange_DeviceconnectReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange_DeviceHealthReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
		
	}
	
	@Test(priority=14,description="Generate 'Custom Range' Job Deployed report for Selected 'Sub' group")
	public void GenerateViewJobDeployedReportSubGroup_CustomRange() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n14.Generate 'Custom Range' Job Deployed report for Selected 'Sub' group",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Custom Range");
		reportsPage.SelectingDateRange_DeviceconnectReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange_DeviceHealthReport();
		reportsPage.JobStatusColumnVerificationJobDeployedReport();
		reportsPage.JobNameColumnVerificationJobDeployedReport();
		reportsPage.DeviceNameColumnVerificationJobDeployedReport();
		reportsPage.UserIDColumnVerificationJobDeployedReport();
		reportsPage.SwitchBackWindow();
	}
	
	@Test(priority=15,description="Download and Verify the data for Job Deployed Report")
	public void VerifyDownloadingDeviceActivityReport() throws InterruptedException
	{
		Reporter.log("\n15.Download and Verify the data for Job Deployed Report",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnJobsDeployedReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}
	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();
			
		}
	}
}