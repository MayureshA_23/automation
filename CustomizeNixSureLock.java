package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;

public class CustomizeNixSureLock extends Initialization{

	@Test(priority=1,description="Verify the UI of customised nix") 
	public void VerifytheUIofcustomisednixTC_ST_819() throws Throwable {
		Reporter.log("\n1.Verify the UI of customised nix",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.SelectAppDropDown();
		accountsettingspage.AppDownloadURL();			
}
	
	@Test(priority=2,description="Verify that user should be able generate the Nix apk with settings") 
	public void VerifythatusershouldbeablegeneratetheNixapkwithsettingsTC_ST_820() throws Throwable {
		Reporter.log("\n2.Verify that user should be able generate the Nix apk with settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();		
		accountsettingspage.ClickonGenerateButton();		
}
	
	@Test(priority=3,description="Verify keeping Link empty and try to generate Customized apk") 
	public void VerifykeepingLinkemptyandtrytogenerateCustomizedapkTC_ST_824() throws Throwable {
		Reporter.log("\n3.Verify keeping Link empty and try to generate Customized apk",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.ClearAppDownloadUrl();
		accountsettingspage.ClickonGenerateAfterClear();		
}
	
	@Test(priority=4,description="Verify App setting, url when empty") 
	public void VerifyAppsettingurlwhenemptyTC_ST_825() throws Throwable {
		Reporter.log("\n4.Verify App setting, url when empty",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.ClearAppDownloadUrl();
		accountsettingspage.ClickonEditbutton();
		accountsettingspage.ErrormsgAfterClearAppSettings();
}
	
	@Test(priority=5,description="Verify Logs when user customizes Nix/SureLock") 
	public void VerifyLogswhenusercustomizesNixSureLockTC_ST_620() throws Throwable {
		Reporter.log("\n5.Verify Logs when user customizes Nix/SureLock",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();
		accountsettingspage.ClickonAndriodGenerateButton();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.CustomizeNixActivityLogs();		
}
	
	@Test(priority=6,description="Verify selecting  app (Nix/SureLock) under Andriod in Customize Nix/SureLock") 
	public void VerifyselectingappNixSureLockunderAndriodinCustomizeNixSureLockTC_ST_295() throws Throwable {
		Reporter.log("\n6.Verify selecting  app (Nix/SureLock) under Andriod in Customize Nix/SureLock",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();		
		accountsettingspage.SelectAppVisibleAndriod();
		accountsettingspage.AppDownloadURLVisibleAndriod();
		accountsettingspage.AppTitleVisibleAndriod();
		accountsettingspage.AppSettingsVisibleAndriod();
		accountsettingspage.GenerateVisibleAndriod();		
		accountsettingspage.DownloadVisibleAndriod();
		accountsettingspage.ImportSettingsVisibleAndriod();		
		accountsettingspage.EditSettingsVisibleAndriod();
		accountsettingspage.AppLauncherIconVisibleAndriod();
}
		
	@Test(priority=7,description="Verify selecting  app (Nix/SureLock) under select app drop down in Customize Nix/SureLock") 
	public void VerifyselectingappNixSureLockunderselectappdropdowninCustomizeNixSureLockTC_ST_296() throws Throwable {
		Reporter.log("\n7.Verify selecting  app (Nix/SureLock) under select app drop down in Customize Nix/SureLock",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.SelectAppVisible();
		accountsettingspage.AppDownloadURLVisible();
		accountsettingspage.AppSettingsVisible();
		accountsettingspage.GenerateVisible();
		accountsettingspage.DownloadVisible();		
}
	
	@Test(priority=8,description="Verify that app download url in window is present by default") 
	public void VerifythatappdownloadurlinwindowispresentbydefaultTC_ST_297() throws Throwable {
		Reporter.log("\n8.Verify that app download url in window is present by default",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.AppAppDownloadURLVisible();			
}
	
	@Test(priority=9,description="Verify that app download url in Andriod is present by default") 
	public void VerifythatappdownloadurlinAndriodispresentbydefaultTC_ST_298() throws Throwable {
		Reporter.log("\n9.Verify that app download url in Andriod is present by default",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();			
		accountsettingspage.AppAppDownloadURLVisibleAndriod();			
}
	
	@Test(priority=10,description="Verify that app download SureLock url in Andriod is present by default") 
	public void VerifythatappdownloadurlSureLockinAndriodispresentbydefaultTC_ST_299() throws Throwable {
		Reporter.log("\n10.Verify that app download SureLock url in Andriod is present by default",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();			
		accountsettingspage.AppAppDownloadURLSureLockVisibleAndriod();			
}
	
	@Test(priority=11,description="Verify that user is able to Edit Sure MDM Nix Agent App title") 
	public void VerifythatuserisabletoEditApptitleSureMDMNixAgentTC_ST_300() throws Throwable {
		Reporter.log("\n11.Verify that user is able to Edit Sure MDM Nix Agent App title",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();		
		accountsettingspage.EditAppTitleSureMDMNixAgentAndriod();			
}
	
	@Test(priority=12,description="Verify that user is able to Edit Sure Lock Andriod App title") 
	public void VerifythatuserisabletoEditApptitleSureLockAndriodTC_ST_301() throws Throwable {
		Reporter.log("\n12.Verify that user is able to Edit Sure Lock Andriod App title",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();		
		accountsettingspage.EditAppTitleSureLockAndriod();			
}
	
	
	@Test(priority=13,description="Verify that user is able to import settings file to app settings") 
	public void VerifythatuserisabletoimportsettingsfiletoappsettingsTC_ST_302() throws Throwable {
		Reporter.log("\n13.Verify that user is able to import settings file to app settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();	
		accountsettingspage.SureMDMNixAgentAndriod();
		accountsettingspage.ClickedonImportButton();
		accountsettingspage.browseFileImportSettings("./Uploads/UplaodFiles/ImportSettings/\\NixSettings.exe");
				
}
	
	@Test(priority=14,description="Verify that user is able to edit settings file in app settings") 
	public void VerifythatuserisabletoeditsettingsfileinappsettingsTC_ST_303() throws Throwable {
		Reporter.log("\n14.Verify that user is able to edit settings file in app settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();		
		accountsettingspage.SureMDMNixAgentAndriod();
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();
		accountsettingspage.browseFileImportSettings("./Uploads/UplaodFiles/ImportSettings/\\NixSettings.exe");				
}
	
	@Test(priority=15,description="Verify that user is able to add app launcher icon from remove") 
	public void VerifythatuserisabletoaddapplaunchericonTC_ST_304() throws Throwable {
		Reporter.log("\n15.Verify that user is able to add app launcher icon",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();		
		accountsettingspage.SureMDMNixAgentAndriod();
		accountsettingspage.AppLauncherIconRemovebutton();		
		accountsettingspage.AppLauncherIconBrowserbutton();
		accountsettingspage.browseFileAppLauncherIcon("./Uploads/UplaodFiles/AppLauncherIcon/\\AppImg.exe");
}
	
	@Test(priority=16,description="Verify that user should be able generate the Nix apk with settings") 
	public void VerifythatusershouldbeablegeneratetheNixapkwithsettingsTC_ST_305() throws Throwable {
		Reporter.log("\n16.Verify that user should be able generate the Nix apk with settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();		
		accountsettingspage.SureMDMNixAgentAndriod();
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();
		accountsettingspage.browseFileImportSettings("./Uploads/UplaodFiles/ImportSettings/\\NixSettings.exe");	
		accountsettingspage.ClickonAndriodGenerateButton();
			
}
	
	@Test(priority=17,description="Verify that user is able to download Nix app with settings and install the app on device.") 
	public void VerifythatuserisabletodownloadNixappwithsettingsandinstalltheappondeviceTC_ST_306() throws Throwable {
		Reporter.log("\n17.Verify that user is able to download Nix app with settings and install the app on device.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();		
		accountsettingspage.SureMDMNixAgentAndriod();
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();
		accountsettingspage.browseFileImportSettings("./Uploads/UplaodFiles/ImportSettings/\\NixSettings.exe");	
		accountsettingspage.ClickonAndriodGenerateButton();
		accountsettingspage.NixAndriodDownloadbutton();
}
	
	@Test(priority=18,description="Verify that user should be able generate the SureLock apk with settings") 
	public void VerifythatusershouldbeablegeneratetheSureLockapkwithsettingsTC_ST_307() throws Throwable {
		Reporter.log("\n18.Verify that user should be able generate the SureLock apk with settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();	
		accountsettingspage.SureLockAndriod();		
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();		
		accountsettingspage.browseFileImportSettingsSureLock("./Uploads/UplaodFiles/ImportSettingsSurelock/\\SL settings.exe");	
		accountsettingspage.ClickonAndriodGenerateButton();

}
	
	@Test(priority=19,description="Verify that user is able to download SureLock app with settings and install the app on device.") 
	public void VerifythatuserisabletodownloadSureLockappwithsettingsandinstalltheappondeviceTC_ST_308() throws Throwable {
		Reporter.log("\n19.Verify that user is able to download SureLock app with settings and install the app on device.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();	
		accountsettingspage.SureLockAndriod();		
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();		
		accountsettingspage.browseFileImportSettingsSureLock("./Uploads/UplaodFiles/ImportSettingsSurelock/\\SL settings.exe");	
		accountsettingspage.ClickonAndriodGenerateButton();
		accountsettingspage.SureLockAndriodDownloadbutton();
}
	
	@Test(priority=20,description="Verify that user is able to add app launcher icon for Surlock") 
	public void VerifythatuserisabletoaddapplaunchericonforSurlockTC_ST_309() throws Throwable {
		Reporter.log("\n20.Verify that user is able to add app launcher icon for Surlock",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();	
		accountsettingspage.SureLockAndriod();
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();
		accountsettingspage.browseFileImportSettingsSureLock("./Uploads/UplaodFiles/ImportSettingsSurelock/\\SL settings.exe");
		accountsettingspage.ClickonAndriodGenerateButton();
		accountsettingspage.AppLauncherIconRemovebutton();		
		accountsettingspage.AppLauncherIconBrowserbutton();
		accountsettingspage.browseFileAppLauncherIcon("./Uploads/UplaodFiles/AppLauncherIcon/\\AppImg.exe");
}
	
	
	@Test(priority=21,description="Verify keeping Link empty and try to generate Customized apk for SureMDM Nix Agent") 
	public void VerifykeepingLinkemptyandtrytogenerateCustomizedapkSureMDMNixAgentTC_ST_310() throws Throwable {
		Reporter.log("\n21.Verify keeping Link empty and try to generate Customized apk SureMDM Nix Agent",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();	
		accountsettingspage.SureMDMNixAgentAndriod();
		accountsettingspage.APPDownloadURLLinkbutton();			
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();		
		accountsettingspage.browseFileImportSettings("./Uploads/UplaodFiles/ImportSettings/\\NixSettings.exe");	
		accountsettingspage.AppLauncherIconRemovebutton();		
		accountsettingspage.AppLauncherIconBrowserbutton();
		accountsettingspage.browseFileAppLauncherIcon("./Uploads/UplaodFiles/AppLauncherIcon/\\AppImg.exe");		
		accountsettingspage.ClickonAndriodGenerateLinkEmpty();
}
	
	@Test(priority=22,description="Verify keeping Link empty and try to generate Customized apk for SureLock") 
	public void VerifykeepingLinkemptyandtrytogenerateCustomizedapkSureLockTC_ST_311() throws Throwable {
		Reporter.log("\n22.Verify keeping Link empty and try to generate Customized apk SureLock",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();
		accountsettingspage.SureLockAndriod();		
		accountsettingspage.APPDownloadURLLinkbutton();			
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.ClickedonImportButton();		
		accountsettingspage.browseFileImportSettingsSureLock("./Uploads/UplaodFiles/ImportSettingsSurelock/\\SL settings.exe");
		accountsettingspage.AppLauncherIconRemovebutton();		
		accountsettingspage.AppLauncherIconBrowserbutton();
		accountsettingspage.browseFileAppLauncherIcon("./Uploads/UplaodFiles/AppLauncherIcon/\\AppImg.exe");		
		accountsettingspage.ClickonAndriodGenerateLinkEmpty();
}
	
	@Test(priority=23,description="Verify keeping AppTitle,Settings, Icon keeping empty and try to generate Customized apk for SureMDM Nix Agent") 
	public void VerifykeepingAppTitleSettingsIconkeepinemptyandtrytogenerateCustomizedapkforSureMDMNixAgentTC_ST_312() throws Throwable {
		Reporter.log("\n23.Verify keeping AppTitle,Settings, Icon keeping empty and try to generate Customized apk for SureMDM Nix Agent",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();
		accountsettingspage.SureMDMNixAgentAndriod();
		accountsettingspage.APPTittleTextField();
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.AppLauncherIconRemovebutton();	
		accountsettingspage.ClickonAndriodGenerateALLEmpty();
		
}
	
	@Test(priority=24,description="Verify keeping AppTitle,Settings, Icon keeping empty and try to generate Customized apk for SureLock") 
	public void VerifykeepingAppTitleSettingsIconkeepinemptyandtrytogenerateCustomizedSureMDMNixAgentTC_ST_313() throws Throwable {
		Reporter.log("\n24.Verify keeping AppTitle,Settings, Icon keeping empty and try to generate Customized apk for SureLock",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonAndriod();
		accountsettingspage.SureLockAndriod();		
		accountsettingspage.APPTittleTextField();
		accountsettingspage.EditSettingbutton();
		accountsettingspage.EditSettingClearTextbutton();
		accountsettingspage.AppLauncherIconRemovebutton();	
		accountsettingspage.ClickonAndriodGenerateALLEmpty();
		
}
	
	@Test(priority=25,description="Verify keeping AppTitle,Settings, keeping empty SureMDM Nix Agent  app with settings and install the app on Windows") 
	public void VerifykeepingAppTitlSettingskeepingemptySureMDMNixAgentappwithsettingsandinstalltheapponWindowsTC_ST_314() throws Throwable {
		Reporter.log("\n25.Verify keeping AppTitle,Settings, keeping empty SureMDM Nix Agent  app with settings and install the app on Windows",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.APPDownloadURLEmptyWindows();
		accountsettingspage.EditSettingWindowButton();
		accountsettingspage.ClickonWindowGenerateAllLinkEmpty();
			
}
	
	@Test(priority=26,description="Verify that user is able to SureMDM Nix Agent app with import settings on Windows") 
	public void VerifythatuserisabletoSureMDMNixAgentappwithimportsettingsonWindowsTC_ST_315() throws Throwable {
		Reporter.log("\n26.Verify that user is able to SureMDM Nix Agent app with import settings on Windows",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.EditSettingWindowButton();		
		accountsettingspage.ClickedonImportWindowButton();
		accountsettingspage.browseFileImportSettings("./Uploads/UplaodFiles/ImportSettings/\\NixSettings.exe");			
		accountsettingspage.ClickonWindowsGeneButton();
			
}
	
	@Test(priority=27,description="Verify that user is able to download SureMDM Nix Agent  app with settings and install the app on Windows") 
	public void VerifythatuserisabletodownloadSureMDMNixAgentappwithsettingsandinstalltheapponWindowsTC_ST_316() throws Throwable {
		Reporter.log("\n27.Verify that user is able to download SureMDM Nix Agent  app with settings and install the app on Windows",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonCustomizeNixSureLock();
		accountsettingspage.ClickonWindows();
		accountsettingspage.EditSettingWindowButton();		
		accountsettingspage.ClickedonImportWindowButton();
		accountsettingspage.browseFileImportSettings("./Uploads/UplaodFiles/ImportSettings/\\NixSettings.exe");			
		accountsettingspage.ClickonWindowsGeneButton();
		accountsettingspage.ClickedonDownloadwindows();
			
}
	
	
	
	
	
	
	
	
}
