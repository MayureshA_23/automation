package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class TeleComManagement_Job extends Initialization {
	
	@Test(priority=1,description="Telecom Management - verify Call log and SMS log tracking in telecom management policy job")
	public void verifyCallLogAndSMSLogTrackingTC_JO_239() throws Throwable 
	{
		commonmethdpage.ClickOnAllDevicesButton();/////
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_Off();
	    dynamicjobspage.ClickOnCallLogOkBtn();

	    dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnSMSLog();
	    dynamicjobspage.SelectSMSLogStatus_Off();
	    dynamicjobspage.ClickOnSMSLogOkBtn();	

	    
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOndata_usage_policyTeleCom();
		dynamicjobspage.BillingCycle();
	    androidJOB.SendingJobName("TelecomCall&SmsLog");

	    dynamicjobspage.ClickOnTelecomManagementCallLogTrackingButton();
		dynamicjobspage.Turning_ONTelecomManagementCallLogTrackingJobs("1");
	    dynamicjobspage.SelectingCallLogsJobStatusTeleCom("15");
	    
	    dynamicjobspage.ClickOnTelecomManagementSMSLogTrackingButton();
		dynamicjobspage.Turning_ONTelecomManagementSMSLogTrackingJobs();
	    dynamicjobspage.SendingSMSLogTrackingPeriodicityTeleCom("15");
	    dynamicjobspage.ClickOnTelecomManagementJobOkButton();
	    
	    
		androidJOB.JobCreatedMessage(); 
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("TelecomCall&SmsLog"); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob("TelecomCall&SmsLog",200);
		
		dynamicjobspage.ClickOnMoreOption();	
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.ToVerifyDynamicJobStatus_ON();
	    dynamicjobspage.ClickOnCallLogOkBtn();
	    
		dynamicjobspage.ClickOnMoreOption();
		dynamicjobspage.ClickOnSMSLog();
		dynamicjobspage.ToVerifyDynamicSMSTrackingJobStatus_ON();
	    dynamicjobspage.ClickOnSMSLogCloseBtn();




	  }
	@Test(priority=2,description="Telecom Management - Verify modifying telecom Management policy job")
	public void verifyCallLogAndSMSLogTrackingTC_JO_241() throws Throwable 
	{

		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("TelecomCall&SmsLog");
		androidJOB.ClickOnOnJob("TelecomCall&SmsLog");
		androidJOB.ModifyJobButton();
	    androidJOB.SendingJobName("EditedTelecomCall&SmsLog");
	    
	    dynamicjobspage.ClickOnTelecomManagementCallLogTrackingButton();
		dynamicjobspage.Turning_ONTelecomManagementCallLogTrackingJobs("0");
		dynamicjobspage.ClickOnTelecomManagementJobOkButton();
		androidJOB.JobmodifiedMessage(); 
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EditedTelecomCall&SmsLog"); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob("EditedTelecomCall&SmsLog",200);
		
		 dynamicjobspage.ClickOnMoreOption();
		 dynamicjobspage.ClickOnCallLog();
		 dynamicjobspage.ToVerifyDynamicJobStatus_OFF();
		 dynamicjobspage.ClickOnCallLogCloseBtn();		 


		
		
		
		
	}
	@Test(priority=3,description="Telecom Management - verify disabling telecom management .")
	public void verifyDisablingTeleComTC_JO_238() throws Throwable {
		
		
	
	
	
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("EditedTelecomCall&SmsLog");
		androidJOB.ClickOnOnJob("EditedTelecomCall&SmsLog");
		androidJOB.ModifyJobButton();
	    androidJOB.SendingJobName("DisableTelecom");
	    
	    dynamicjobspage.DisableTelecomManagement();
		dynamicjobspage.ClickOnTelecomManagementJobOkButton();
		androidJOB.JobmodifiedMessage(); 
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableTelecom"); 
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob("DisableTelecom",200);

	    dynamicjobspage.ToVerifyTeleCom_OFF();
	
	
	
	
	
	}
	@Test(priority=4,description="Telecom Management - Verify Block data/Alerts for Roaming data are removed from Telecom management policy.")
	public void TC_JO_237() throws Throwable {

		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFileReset);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(false);
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingMonth);
		deviceinfopanelpage.SelectDateFromDatePickerForMonthly();
		deviceinfopanelpage.EnterDataLimitWarning();
		deviceinfopanelpage.VerifyRoamingDataUI();
		
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingWeekly);
		deviceinfopanelpage.EnterDataLimitWarning();
		deviceinfopanelpage.VerifyRoamingDataUI();



		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingDaily);
		deviceinfopanelpage.EnterDataLimitWarning();
		deviceinfopanelpage.VerifyRoamingDataUI();

		
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingCustom);
		deviceinfopanelpage.SelectCurrentDateFromDatePickerForCustomize();
		deviceinfopanelpage.ClickAnyElement();
		deviceinfopanelpage.EnterNumberOfDays(Config.CustomNumberOfDays);
		deviceinfopanelpage.EnterDataLimitWarning();
		deviceinfopanelpage.VerifyRoamingDataUI();
		deviceinfopanelpage.closetelecompopup();

		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
/*	@Test(priority=4, description="Telecom Management Policy - Verify creating telecom Management policy job by enabling block data for Monthly/Weekly/Daily and custom(Note : Block data will work only on KNOX/Rooted and signed devices)")
	public void TC_JO_234() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFileReset);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(false);
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingDaily);
		deviceinfopanelpage.EnterDataLimitWarningReset();
		deviceinfopanelpage.CheckBlockDataWarning(true);
		deviceinfopanelpage.CheckSendDeviceAlretWarning(true);
		deviceinfopanelpage.CheckSendMDMAlertWarning(true);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit1(true, Config.EmailForAlert);
		deviceinfopanelpage.CheckSendMDMAlertWarning(true);
		deviceinfopanelpage.EnterDataLimitThresholdReset();
		deviceinfopanelpage.CheckBlockDataThreshold(true);
		deviceinfopanelpage.CheckSendDeviceAlretThreshold(true);
		deviceinfopanelpage.CheckSendMDMAlertThreshold(true);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit2(true, Config.EmailForAlert);
		deviceinfopanelpage.CheckSendMDMAlertThreshold(true);
		deviceinfopanelpage.ClickOnOKButtonInTelecomManagementFromJobCreate(Config.TelecomManagementPolicyFileReset);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TelecomManagementPolicyFileReset);
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TelecomManagementPolicyFileReset, Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledFlipkart, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledimo, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledTikTok, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledTrain, true);
		
		commonmethdpage.ClickOnHomePage();
		deviceinfopanelpage.ClickOnMorebutton();
		deviceinfopanelpage.ClickODataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.GetUsedData();
		deviceinfopanelpage.ClickOnCloseDataUsage();
		
		commonmethdpage.SearchDevice();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFile);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(false);
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingDaily);
		deviceinfopanelpage.EnterDataLimitWarning();
		deviceinfopanelpage.SelectDataWarning();
		deviceinfopanelpage.SelectDataTypeWarning();
		deviceinfopanelpage.CheckBlockDataWarning(false);
		deviceinfopanelpage.CheckSendDeviceAlretWarning(false);
		deviceinfopanelpage.ClickOnOKButtonInTelecomManagementFromJobCreate(Config.TelecomManagementPolicyFile);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TelecomManagementPolicyFile);
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TelecomManagementPolicyFile, Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledFlipkart, Config.SubTitleFlipkart);
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledimo, "imo free video calls and chat");
		deviceinfopanelpage.VerifyAlertMessageFromDeviceLimit1(Config.DeviceName, 600);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
//		deviceinfopanelpage.ClickOnBackButton();
//		androidJOB.AppiumConfigurationforNIX();
		deviceinfopanelpage.VerifyNixStatus("Offline");
		androidJOB.AppiumConfigurationforSettings("com.android.settings","com.android.settings.Settings");
		deviceinfopanelpage.ClickOnConnections();
		deviceinfopanelpage.ClickOnDataUsage();
		deviceinfopanelpage.VerifyMobileDataIsEnabled(false);
	}
	@Test(priority=5, description="Telecom Management policy - verify Creating telecom management policy with 2 threshold limits and alert messages.")
	public void TC_JO_235() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		deviceinfopanelpage.DeleteFileFromDevice();
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledSureFox, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledFlipkart, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledimo, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledTikTok, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledTrain, true);
		
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnDynamicRefresh();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFileReset);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(false);
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingMonth);
		deviceinfopanelpage.SelectDateFromDatePickerForMonthly();
		deviceinfopanelpage.EnterDataLimitWarningReset();
		deviceinfopanelpage.CheckBlockDataWarning(true);
		deviceinfopanelpage.CheckSendDeviceAlretWarning(true);
		deviceinfopanelpage.CheckSendMDMAlertWarning(true);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit1(true, Config.EmailForAlert);
		deviceinfopanelpage.CheckSendMDMAlertWarning(true);
		deviceinfopanelpage.EnterDataLimitThresholdReset();
		deviceinfopanelpage.CheckBlockDataThreshold(true);
		deviceinfopanelpage.CheckSendDeviceAlretThreshold(true);
		deviceinfopanelpage.CheckSendMDMAlertThreshold(true);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit2(true, Config.EmailForAlert);
		deviceinfopanelpage.CheckSendMDMAlertThreshold(true);
		deviceinfopanelpage.ClickOnOKButtonInTelecomManagementFromJobCreate(Config.TelecomManagementPolicyFileReset);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TelecomManagementPolicyFileReset);
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TelecomManagementPolicyFileReset, Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();
		
		commonmethdpage.ClickOnHomePage();
		deviceinfopanelpage.ClickOnMorebutton();
		deviceinfopanelpage.ClickODataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.GetUsedData();
		deviceinfopanelpage.ClickOnCloseDataUsage();
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFile);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(false);
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingMonth);
		deviceinfopanelpage.SelectDateFromDatePickerForMonthly();
		deviceinfopanelpage.EnterDataLimitWarning();
		deviceinfopanelpage.SelectDataWarning();
		deviceinfopanelpage.SelectDataTypeWarning();
		deviceinfopanelpage.CheckSendDeviceAlretWarning(false);
		deviceinfopanelpage.CheckSendMDMAlertWarning(false);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit1(false, Config.EmailForAlert);
		deviceinfopanelpage.EnterDataLimitThreshold();
		deviceinfopanelpage.SelectDataThreshold();
		deviceinfopanelpage.SelectDataTypeThreshold();
		deviceinfopanelpage.CheckSendDeviceAlretThreshold(false);
		deviceinfopanelpage.CheckSendMDMAlertThreshold(false);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit2(false, Config.EmailForAlert);
		deviceinfopanelpage.ClickOnOKButtonInTelecomManagementFromJobCreate(Config.TelecomManagementPolicyFile);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TelecomManagementPolicyFile);
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TelecomManagementPolicyFile, Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.VerifyTelecomManagementStatus(Config.TelecomStatusOn);
		
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledFlipkart, Config.SubTitleFlipkart);
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledimo, "imo free video calls and chat");
		deviceinfopanelpage.VerifyAlertMessageFromDeviceLimit1(Config.DeviceName, 600);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
		
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledTrain, Config.SubTitleWhereIsMyTrain);
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledTikTok, Config.SubTitleTikTok);
		deviceinfopanelpage.VerifyAlertMessageFromDeviceLimit2(Config.DeviceName, 600);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
		
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.ClickOnInbox();
		accountsettingspage.VerifyTextMessageFromConsoleForDataUsage(Config.DeviceName);
		accountsettingspage.VerifyTextMessageFromEmailDataUsage(Config.DeviceName);
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFileDisable);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(true);
		deviceinfopanelpage.ClickOnOKButtonInTelecomManagementFromJobCreate(Config.TelecomManagementPolicyFileDisable);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TelecomManagementPolicyFileDisable);
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TelecomManagementPolicyFileDisable, Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();
	}
	@Test(priority=6,description="Telecom Management Policy - Verify Apply Jobs/profile in telecom management policy job.")
	public void TC_JO_236() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnDynamicRefresh();
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFileReset);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(false);
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingCustom);
		deviceinfopanelpage.SelectCurrentDateFromDatePickerForCustomize();
		deviceinfopanelpage.ClickAnyElement();
		deviceinfopanelpage.EnterNumberOfDays(Config.CustomNumberOfDays);
		deviceinfopanelpage.EnterDataLimitWarningReset();
		deviceinfopanelpage.CheckBlockDataWarning(true);
		deviceinfopanelpage.CheckSendDeviceAlretWarning(true);
		deviceinfopanelpage.CheckSendMDMAlertWarning(true);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit1(true, Config.EmailForAlert);
		deviceinfopanelpage.CheckSendMDMAlertWarning(true);
		deviceinfopanelpage.EnterDataLimitThresholdReset();
		deviceinfopanelpage.CheckBlockDataThreshold(true);
		deviceinfopanelpage.CheckSendDeviceAlretThreshold(true);
		deviceinfopanelpage.CheckSendMDMAlertThreshold(true);
		deviceinfopanelpage.ClickAndEnterEmailForAlertLimit2(true, Config.EmailForAlert);
		deviceinfopanelpage.CheckSendMDMAlertThreshold(true);
		deviceinfopanelpage.ClickOnOKButtonInTelecomManagementFromJobCreate(Config.TelecomManagementPolicyFileReset);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TelecomManagementPolicyFileReset);
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TelecomManagementPolicyFileReset, Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		deviceinfopanelpage.DeleteFileFromDevice();
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledSureFox, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledFlipkart, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledimo, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledTikTok, true);
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledTrain, true);
		
		commonmethdpage.ClickOnHomePage();
		deviceinfopanelpage.ClickOnMorebutton();
		deviceinfopanelpage.ClickODataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.RefreshDataUsage();
		deviceinfopanelpage.GetUsedData();
		deviceinfopanelpage.ClickOnCloseDataUsage();
		
		commonmethdpage.SearchDevice();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTelecomManagentPolicy();
		deviceinfopanelpage.EnterTelecomManagementName(Config.TelecomManagementPolicyFile);
		deviceinfopanelpage.ClickOnEnableTelecomManagement(false);
		deviceinfopanelpage.SelectConfigureBilligCycle(Config.ConfigureBillingCustom);
		deviceinfopanelpage.SelectCurrentDateFromDatePickerForCustomize();
		deviceinfopanelpage.ClickAnyElement();
		deviceinfopanelpage.EnterNumberOfDays(Config.CustomNumberOfDays);
		deviceinfopanelpage.EnterDataLimitWarning();
		deviceinfopanelpage.SelectDataWarning();
		deviceinfopanelpage.SelectDataTypeWarning();
		deviceinfopanelpage.CheckSendDeviceAlretWarning(false);
		deviceinfopanelpage.CheckOnApplyJobOrProfileWarning(false);
		deviceinfopanelpage.ClickOnAddButtonjobsTelecom();
		deviceinfopanelpage.SearchJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectsearchedJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.OkButtonAfterSearch();
		deviceinfopanelpage.OkButtonDevInfoJob();
		
		deviceinfopanelpage.EnterDataLimitThreshold();
		deviceinfopanelpage.SelectDataThreshold();
		deviceinfopanelpage.SelectDataTypeThreshold();
		deviceinfopanelpage.CheckSendDeviceAlretThreshold(false);
		deviceinfopanelpage.CheckOnApplyJobOrProfileThreshold(false);
		deviceinfopanelpage.ClickOnAddButtonjobsTelecom();
		deviceinfopanelpage.SearchFenceJob(Config.InstallJob);
		deviceinfopanelpage.SelectFenceJob(Config.InstallJob);
		deviceinfopanelpage.OkButtonAfterSearch();
		deviceinfopanelpage.OkButtonDevInfoJob();
		deviceinfopanelpage.ClickOnOKButtonInTelecomManagementFromJobCreate(Config.TelecomManagementPolicyFile);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TelecomManagementPolicyFile);
		androidJOB.JobInitiatedOnDevice(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TelecomManagementPolicyFile, Config.DeploymentTime);
		androidJOB.ClickOnJobQueueCloseButton();
		
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledFlipkart, Config.SubTitleFlipkart);
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledimo, "imo free video calls and chat");
		deviceinfopanelpage.VerifyAlertMessageFromDeviceLimit1(Config.DeviceName, 600);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		deviceinfopanelpage.VerifyFileNameInRemoteSupport();
		remotesupportpage.SwitchToMainWindow();
		
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledTikTok, Config.SubTitleTikTok);
		commonmethdpage.InstallApplicationsFromPlayStore(Config.AppBundledTrain, Config.SubTitleWhereIsMyTrain);
		deviceinfopanelpage.VerifyAlertMessageFromDeviceLimit2ApplyJob(Config.DeviceName, 600);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();
		deviceinfopanelpage.VerifyAppIsInstalled(Config.AppBundledSureFox, false);
		
	}*/
}
