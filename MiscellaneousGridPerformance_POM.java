package PageObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;

public class MiscellaneousGridPerformance_POM extends WebDriverCommonLib
{
	JavascriptExecutor jc=(JavascriptExecutor) Initialization.driver;
	AssertLib ALib=new AssertLib();
	
	
	
    @FindBy(xpath="//input[@id='useadvgrid']")
    private WebElement GridPerformanceCheckBox;
    
    @FindBy(xpath="//input[@id='miscellaneous_apply']")
    private WebElement MiscellaneousApplyButton;
    
    @FindBy(xpath="//span[text()='Settings updated successfully.']")
    private WebElement NotificationOnUpdatedSettings;
    
    @FindBy(xpath="//select[@id='temperature_unit']")
    private WebElement TemperatureUnitDropDown;
    
    @FindBy(id="homeSection")
    private WebElement HomeSection;
    
    @FindBy(xpath="//li[@id='eulaDisclaimer']")
    private WebElement EULADisclaimerpolicyButton;
    
    @FindBy(xpath="//*[@id='chkbox_eulaDisclaimer']")
    private WebElement EULADisclaimerCheckBox;
    
    @FindBy(xpath="//*[@id='eulaDisclaimerPloicyText']")
    private WebElement EULADisclaimerTextBox;
    
    public void verifyMiscellaneousGridPerformance()
    {
    	if(GridPerformanceCheckBox.isSelected())
    	{
    	    GridPerformanceCheckBox.click();
    	    Reporter.log(">>>>>>>CheckBox is Deselected",true);
    	}
    	else
    	{
    		GridPerformanceCheckBox.click();
    		Reporter.log(">>>>>>>CheckBox is Selected",true);
    	}
    	
    	jc.executeScript("arguments[0].scrollIntoView()",MiscellaneousApplyButton );
    	MiscellaneousApplyButton.click();
    	waitForXpathPresent("//span[text()='Settings updated successfully.']");
    	if(NotificationOnUpdatedSettings.isDisplayed())
    	{
    		Reporter.log(">>>>>>>>Settings Updated Successfully",true);
    	}
    	else
    	{
    	  Reporter.log(">>>>>>>Settings are not Updated",true);	
    	}
    	  	
    }
  //----------------------------------------------------------------------------------------------------------------
    public void VerifyTemperatureUnitDefaultValueInMicellaneousSettings() throws InterruptedException
    {
  	  jc.executeScript("arguments[0].scrollIntoView()",TemperatureUnitDropDown);
  	  Select select=new Select(TemperatureUnitDropDown);
  	  String DefaultUnit = select.getFirstSelectedOption().getText();
  	  if(DefaultUnit.equals("Celsius (�C)"))
  	  {
  		  Reporter.log("PASS >>Default Value is Celsius (�C)",true);
  	  }
  	  else
  	  {
  		  Reporter.log("FAIL >>Default value Is Not Celsius",true);
  	  }
  	  
    }
    public void ChangingCelciusToFahrenheit() 
    {
    	Select select=new Select(TemperatureUnitDropDown);
    	select.selectByValue("1");
    }
    public void ClickingOnApplyButton() throws InterruptedException
    {
    	MiscellaneousApplyButton.click();
  	  waitForXpathPresent("//span[text()='Settings updated successfully.']");
  	  sleep(1);
  	  if(NotificationOnUpdatedSettings.isDisplayed())
  	  {
  		  Reporter.log("PASS >>Settings Applied Successfully",true);
  	  }
  	  else  
  	  {
  		  Reporter.log("FAIL >>Settings Are Not Updated",true);
  	  } 
  	 while(Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[3]")).isDisplayed())
  	 {
  	 }
  	 sleep(3);
    }
    public void VerifyingUnitChangeInConsoleFromCelsiusToFahrenheit() throws InterruptedException
    {
    	boolean flag;
    	String DeviceTemperature = Initialization.driver.findElement(By.xpath("//td[@class='Temperature']")).getText();
    	if(DeviceTemperature.contains("�F"))
    	{
    		flag=true;
    	}
    	else
    	{
    		flag=false;
    	}
    	String PassStatement="PASS >> Device Temperature Has Been Changed From Celsius To Fahrenheit"+" : "+DeviceTemperature;
    	String FailStatement="FAIL >> Device Temperature Has Not Been Changed"+" : "+DeviceTemperature;
    	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
    	waitForXpathPresent("//p[text()='"+Config.Miscellaneous_DeviceName+"']");
    	sleep(7);
    			
    }
    public void ChangingFahrenheitToCelcius() 
    {
    	Select select=new Select(TemperatureUnitDropDown);
    	select.selectByValue("0");
    }
    public void VerifyingUnitChangeInConsoleFormFahrenheitToCelsius()
    {
    	boolean flag;
    	String DeviceTemperature = Initialization.driver.findElement(By.xpath("//td[@class='Temperature']")).getText();
    	if(DeviceTemperature.contains("�C"))
    	{
    		flag=true;
    	}
    	else
    	{
    		flag=false;
    	}
      String PassStatement="PASS >> Device Temperature Has Been Changed From Fahrenheit To Celsius"+" : "+DeviceTemperature;
  	  String FailStatement="FAIL >> Device Temperature Has Not Been Chnaged"+" : "+DeviceTemperature;
  	  ALib.AssertTrueMethod(flag, PassStatement, FailStatement);    			
    }
    public void ClickingOnEulaDisclaimerPolicy() throws InterruptedException
    {
    	EULADisclaimerpolicyButton.click();
    	waitForVisibilityOf("//p[text()='EULA Disclaimer Policy']");
    	sleep(10);
    }
    public void VerifyEulaDisclaimerDefaultStatus()
    {
    	boolean flag = EULADisclaimerCheckBox.isSelected();
    	if(flag==false)
    	{
    		flag=false;
    	}
    	else
    	{
    		flag=true;
    		Reporter.log("EULA Disclaimer Policy Is Enabled Default",true);
    	}
    	String PassStatement="PASS >> EULA Disclaimer Policy Is Disbled Default";
    	String FailStatement="FAIL >> EULA Disclaimer Policy Is Enabled Default";
    	ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
    	
    }
    /*public void VerifyEulaDisclaimerTextBoxWhenCheckBoxIsDisabled() 
    {
    	EULADisclaimerTextBox.sendKeys("42 Gears EULA Testing");
    	String text = EULADisclaimerTextBox.getAttribute("value");
    	System.out.println(text);
    	
    }
    */
    public void VerifyEulaDisclaimerTextBoxWhenCheckBoxISEnabled() throws InterruptedException
    {
    	boolean value;
    	boolean flag = EULADisclaimerCheckBox.isSelected();
    	if(flag==true)
    	{
    		Reporter.log("EULA Disclaimer Policy Is Enabled Default",true);
    	}
    	else
    	{
    		EULADisclaimerCheckBox.click();
    		sleep(2);
    	}
    	EULADisclaimerTextBox.clear();
    	sleep(2);
    	EULADisclaimerTextBox.sendKeys("42 Gears EULA Testing");
    	String text = EULADisclaimerTextBox.getAttribute("value");
    	if(text.equals("42 Gears EULA Testing"))
    	{
    		value=true;
    	}
    	else
    	{
    		value=false;
    	}
    	String PassStatement="PASS >> EULA Disclaimer Policy Text Field Is Enabled While CheckBox is Selected";
    	String FailStatement="FAIL >> EULA Disclaimer Policy Text Field Is Disabled Though CheckBox Is Selected";
    	ALib.AssertTrueMethod(value, PassStatement, FailStatement);
    }
    public void ClickingOnEulaDisclaimerSaveButton() throws InterruptedException
    {
    	Initialization.driver.findElement(By.xpath("//input[@id='eula_policy']")).click();
    	waitForVisibilityOf("//span[text()='Updated successfully.']");
    	sleep(3);
    }
}
