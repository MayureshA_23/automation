package Profiles_Windows_Testscripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class EnterPriseDataProtection extends Initialization
{
	@Test(priority='1',description="UI validation EDP profile .") 
    public void VerifyUIforEnterpriseDataProtection() throws InterruptedException
	{
		Reporter.log("1.UI validation EDP profile ",true);
		profilesAndroid.ClickOnProfile();
		profilesWindows.ClickOnWindowsOption();
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.VarifyingEnterpriseApplicationIsPresent();
		profilesWindows.VarifyingExemptApplicationIsPresent();
		profilesWindows.VarifyingAppliactionDataProtectionlevel();
		profilesWindows.VarifyingAdvancesettings();
		profilesWindows.ClickOnBackBtn();
	}
	@Test(priority='2',description="Verify by applying EDP profile to device with different UWP applications.") 
    public void VarifyByApplyingEDPProfileWithDifferentUWPapplication() throws InterruptedException
    {
		Reporter.log("\n2.Verify by applying EDP profile to device with different UWP applications",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForUWPApp);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectStoreApps);
		profilesWindows.EnterPublisher_Name(Config.Enter_PublisherNameForSkypeApplication);
		profilesWindows.EnterPackage_Name();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.VarifyAddedProfileOfTypeStoreAppsInEnterpriseApp();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForBlock);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();
		
    }
	@Test(priority='3',description="Verify by applying EDP profile to device with .exe and win 32 apps with publisher .")
	public void VarifyByApplyingEDPProfileWithEXEAndWin32() throws InterruptedException
	{
		Reporter.log("\n3.Verify by applying EDP profile to device with different UWP applications.",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForEXEandWin32App);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectEXE);
		profilesWindows.EnterPublisher_Name(Config.Enter_PublisherNameForNotePad);
		profilesWindows.EnterProductNameEnterpriseApp();
		profilesWindows.EnterBinaryNameEnterpriseApp();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.VarifyAddedProfileForEXEapps();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForBlock);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();	
	}
	@Test(priority='4',description="Verify by applying EDP profile to device with .exe and win 32 apps with path .")
	public void VarifyByApplyingEDPProfileWithPath() throws InterruptedException
	{
		Reporter.log("\n4.Verify by applying EDP profile to device with .exe and win 32 apps with path .",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForEXE);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectEXE);
		profilesWindows.ClickOnPathButton();
		profilesWindows.EnterPath();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForBlock);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();		
	}
	@Test(priority='5',description="Verify EDP profile by applying profile with Exempt applications.")
	public void VarifyEDPprofileWithExemptApplication() throws InterruptedException
	{
		Reporter.log("\n5.Verify EDP profile by applying profile with Exempt applications.",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameforExtempApp);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectEXE);
		profilesWindows.ClickOnPathButton();
		profilesWindows.EnterPath();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.ClickOnExemptAppAddBtn();
		profilesWindows.ClickontypeconditionStoreAppInExemptApplications(Config.SelectEXE);
		profilesWindows.EnterPublisherNameForExeemptApplication(Config.Enter_PublisherNameForNotePad);
		profilesWindows.EnterProductNameForExemptApplication();
		profilesWindows.EnterBinaryNameForExemptApplication();
		profilesWindows.ClickOnAddExemptApplicationBtn();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForBlock);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();		
	}
	@Test(priority='6',description="Verify by opening encryped file without extemp application.")
	public void VarifyEDPprofileWithoutExemptApplication() throws InterruptedException
	{
		Reporter.log("\n6.Verify by opening encryped file without extemp application.",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameforExtempApp);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectEXE);
		profilesWindows.ClickOnPathButton();
		profilesWindows.EnterPath();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.ClickOnExemptAppAddBtn();
		profilesWindows.ClickontypeconditionStoreAppInExemptApplications(Config.SelectEXE);
		profilesWindows.EnterPublisherNameForExeemptApplication(Config.Enter_PublisherNameForNotePad);
		profilesWindows.EnterProductNameForExemptApplication();
		profilesWindows.EnterBinaryNameForExemptApplication();
		profilesWindows.ClickOnAddExemptApplicationBtn();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForBlock);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();	
	
	}
	@Test(priority='7',description="Verify by enabling Prevent Corporate Data From Being Accessed By Apps in advance settings")
	public void VerifybyenablingPreventCorporateData() throws InterruptedException
	{
		Reporter.log("\n7.Verify by enabling Prevent Corporate Data From Being Accessed By Apps in advance settings",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForUWPApp);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectStoreApps);
		profilesWindows.EnterPublisher_Name(Config.Enter_PublisherNameForSkypeApplication);
		profilesWindows.EnterPackage_Name();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.EnablingPreventCorporateDataFromBeingAccessedByApps();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForBlock);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();	
		profilesWindows.VarifingCreatedProfile();
	}
	@Test(priority='8',description="Verify by sending message to managed applications.")
	public void Verifybysendingmessagetomanagedapp() throws InterruptedException
	{
		Reporter.log("\n8.Verify by sending message to managed applications.",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForEXEandWin32App);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectEXE);
		profilesWindows.EnterPublisher_Name(Config.Enter_PublisherNameForNotePad);
		profilesWindows.EnterProductNameEnterpriseApp();
		profilesWindows.EnterBinaryNameEnterpriseApp();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.VarifyAddedProfileForEXEapps();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForBlock);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();
	}
	@Test(priority='9',description="Verify by opening encrypted file if override mode is applied.")
	public void VerifybyopeningencryptedfileInOverrideMode() throws InterruptedException
	{
		Reporter.log("\n9.Verify by opening encrypted file if override mode is applied.",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForEXE);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectEXE);
		profilesWindows.ClickOnPathButton();
		profilesWindows.EnterPath();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.EnablingPreventCorporateDataFromBeingAccessedByApps();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForOverride);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();	
		profilesWindows.VarifingCreatedProfile();	
	}
	@Test(priority='A',description="Verify by sharing encrypted file to other device through ftp")
	public void Verifybysharingencryptedfiletootherdevicethroughftp() throws InterruptedException
	{
		Reporter.log("\n10.Verify by sharing encrypted file to other device through ftp",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForUWPApp);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectStoreApps);
		profilesWindows.EnterPublisher_Name(Config.Enter_PublisherNameForSkypeApplication);
		profilesWindows.EnterPackage_Name();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.VarifyAddedProfileOfTypeStoreAppsInEnterpriseApp();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForOverride);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();	
	}
	@Test(priority='B',description="Verify by copy & Paste data from encrypted app to non encrypted app")
	public void VerifybycopyAndPastedatafromencryptedapptononencryptedapp() throws InterruptedException
	{
		Reporter.log("\n11.Verify by copy & Paste data from encrypted app to non encrypted app",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForUWPApp);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectStoreApps);
		profilesWindows.EnterPublisher_Name(Config.Enter_PublisherNameForSkypeApplication);
		profilesWindows.EnterPackage_Name();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.VarifyAddedProfileOfTypeStoreAppsInEnterpriseApp();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForOverride);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();
		profilesWindows.VarifingCreatedProfile();
	}
	@Test(priority='C',description="Verify by converting work file to personal file.")
	public void Verifybyconvertingworkfiletopersonalfile() throws InterruptedException
	{
		Reporter.log("\n12.Verify by converting work file to personal file.",true);
		profilesWindows.AddProfile();
		profilesWindows.ClickOnEnterPriseDataProtection();
		profilesWindows.clickOnConfigureButton_EnterPriseDataProtection();
		profilesWindows.EnterProfileName(Config.Enter_ProfileNameForEXE);
		profilesWindows.ClickOnEnterpriseAppAddBtn();
		profilesWindows.ClickontypeconditionStoreApp(Config.SelectEXE);
		profilesWindows.ClickOnPathButton();
		profilesWindows.EnterPath();
		profilesWindows.ClickOnAddEnterpriseAppBtn();
		profilesWindows.EnablingPreventCorporateDataFromBeingAccessedByApps();
		profilesWindows.EnterPrimarydomain();
		profilesWindows.SelectApplicationProtectionlevel(Config.ApplicationlevelForOverride);
		profilesWindows.ClickOnSaveEnterpriseAppProfileBtn();	
		profilesWindows.VarifingCreatedProfile();
		
	}
	
}


