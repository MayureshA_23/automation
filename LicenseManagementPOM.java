package PageObjectRepository;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.PassScreenshot;
import Library.Utility;
import Library.WebDriverCommonLib;

public class LicenseManagementPOM extends WebDriverCommonLib{
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(id="licenseManagement")
	private WebElement ClickOnLicenseManagement;
	
	@FindBy(xpath ="//h4[text()='License Management']")
	private WebElement HeaderOfLicenseManagement;

	@FindBy(id="okbtn_licensedetail")
	private WebElement ShowHistoryButton;
	
	@FindBy(xpath = ".//*[@id='commonModal']/div/div/div[1]/button")
	private WebElement CloseWindow;
	
	
	public void VerifyClickOnLicenseManagementWindow() throws InterruptedException
	{
		ClickOnLicenseManagement.click();
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 10);
	     wait1.until(ExpectedConditions.elementToBeClickable(ShowHistoryButton));
	     sleep(3);
	    
			boolean isdisplayed=true;
			 
			  try{
				  HeaderOfLicenseManagement.isDisplayed();
					   	 
					    }catch(Exception e)
					    {
					    	isdisplayed=false;
					   	 
					    }
			   Assert.assertTrue(isdisplayed,"FAIL >> : License Management Window display failed");
			   Reporter.log("PASS >> Alert Message : 'License Management Window' is displayed",true );		
			   sleep(3);		
			
			
	}

	
	public void VerifyLicenseMgmtWindow() throws InterruptedException
	{
		//verify size of the cols
	    List<WebElement> ColumnHeaderCount = Initialization.driver.findElements(By.xpath(".//*[@id='LicenseGridContainer']/div[2]/div[2]/div[1]/table/thead/tr/th/div[1]"));
		int countOfColumns = ColumnHeaderCount.size();
		Reporter.log("Total Column Count is : "+countOfColumns);
		int expectedColSize = 3;
		String PassStatement = "PASS >>"+expectedColSize+" columns displayed";
		String FailStatement = "FAIL >>"+expectedColSize+ " All 3 cols not available" ;
		ALib.AssertEqualsMethodInt(expectedColSize, countOfColumns, PassStatement, FailStatement);
		Reporter.log("");
		//verify name of the cols
		for(int i=0;i<countOfColumns;i++){
			  String actual = ColumnHeaderCount.get(i).getText();
			  Reporter.log("Column Name Found --" +actual);
			  Reporter.log("");
			  if(i==0)
			  {
				String expectedValue = "Device Count";
				String PassStatement1 = "PASS >>"+expectedValue+" -- 1st columnName is correct";
				String FailStatement1 = "FAIL >>"+expectedValue+ "-- 1st Column Name is not correct" ;
				ALib.AssertEqualsMethod(expectedValue, actual, PassStatement1, FailStatement1);
				Reporter.log("");
			  }
			  if(i==1)
			  {
				String expectedValue = "Purchased On";
				String PassStatement1 = "PASS >>"+expectedValue+" -- 2nd columnName is correct";
				String FailStatement1 = "FAIL >>"+expectedValue+ "-- 2nd Column Name is not correct" ;
				ALib.AssertEqualsMethod(expectedValue, actual, PassStatement1, FailStatement1);
				Reporter.log("");
			  }
			  if(i==2)
			  {
				String expectedValue = "Expires On";
				String PassStatement1 = "PASS >>"+expectedValue+" -- 3rd columnName is correct";
				String FailStatement1 = "FAIL >>"+expectedValue+ "-- 3rd Column Name is not correct" ;
				ALib.AssertEqualsMethod(expectedValue, actual, PassStatement1, FailStatement1);
				Reporter.log("");
				sleep(3);
			  }
			  
			  
			 
					
	}
		
		PassScreenshot.captureScreenshot(Initialization.driver, "LicenseManagementScreenshot");	  
		sleep(3);
		CloseWindow.click();
			 	  
			 
	}
	
	
		}
	

