package JobsOnAndroid;
import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Library.Config;
import Common.Initialization;

public class WindowsCE extends Initialization{
	
//	@Test(priority=1,description="Verfiy Webhooks is displayed in Notification policy Job in Windows CE") 
//	public void VerfiyWebhooksisdisplayedinNotificationpolicyJobinWindowsCETC_JO_857() throws Throwable {
//		Reporter.log("\n1.Verfiy Webhooks is displayed in Notification policy Job in Windows CE",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonWebhooksButton();
//		androidJOB.EnableWebHooks();
//		androidJOB.EnterWebHookName(1,"Webhooks",2,Config.WrbhookEndPoint);
//		androidJOB.ApplyWebhookBtn();
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.ClickonwindowCE();		
//		androidJOB.ClickonwindowCENotificationPolicy();
//		androidJOB.EnableConnectionPolicy();
//		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
//		androidJOB.CloseNotificationPolicy();
//		commonmethdpage.ClickOnHomePage();		
//}
	
	
//	@Test(priority=2,description="Verfiy Webhooks is displayed in Notification policy Job in Windows Mobile") 
//	public void VerfiyWebhooksisdisplayedinNotificationpolicyJobinWindowsMobileTC_JO_859() throws Throwable {
//		Reporter.log("\n2.Verfiy Webhooks is displayed in Notification policy Job in Windows Mobile",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonWebhooksButton();
//		androidJOB.EnableWebHooks();
//		androidJOB.EnterWebHookName(1,"Webhooks",2,Config.WrbhookEndPoint);
//		androidJOB.ApplyWebhookBtn();
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.ClickonwindowMobile();		
//		androidJOB.ClickonwindowMobileNotificationPolicy();		
//		androidJOB.EnableConnectionPolicy();
//		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
//		androidJOB.CloseNotificationPolicy();
//		commonmethdpage.ClickOnHomePage();		
//}
	
//	@Test(priority=3,description="Verify Webhooks option in Notification policy when webhooks is disabled in account settings") 
//	public void VerifyWebhooksoptioninNotificationpolicywhenwebhooksisdisabledinaccountsettingsTC_JO_865() throws Throwable {
//		Reporter.log("\n3.Verify Webhooks option in Notification policy when webhooks is disabled in account settings",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonWebhooksButton();		
//		androidJOB.DisableWebHooks();
//		androidJOB.ApplyWebhookBtn();	
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.ClickonwindowCE();			
//		androidJOB.ClickonwindowCENotificationPolicy();				
//		androidJOB.EnableConnectionPolicy();
//		androidJOB.WebhookOptionisDisabled();
//		androidJOB.CloseNotificationPolicy();
//		commonmethdpage.ClickOnHomePage();		
//}
	
//	@Test(priority=4,description="Verify error message when we click Done without adding any endpoint url in the popup") 
//	public void VerifyerrormessagewhenweclickDonewithoutaddinganyendpointurlpopupTC_JO_875() throws Throwable {
//		Reporter.log("\n4.Verify error message when we click Done without adding any endpoint url in the popup",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonWebhooksButton();
//		androidJOB.EnableWebHooks();
//		androidJOB.WeebhookMessageErrorVerified();
//		commonmethdpage.ClickOnHomePage();		
//}
	
//	@Test(priority=5,description="Location tracking -Verify device info update for Tewnty Four hrs periodicty in window") 
//	public void LocationtrackingVerifydeviceinfoupdateforTewntyFourhrsperiodictyinwindowTC_JO_852() throws Throwable {
//		Reporter.log("\n5.Location tracking -Verify device info update for Tewnty Four hrs periodicty in window",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonLocationTracking();
//		androidJOB.LocationTrackingJOBTwentyFourCreated();
//		androidJOB.JobCreatedMessage();		
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.TrackingperiodicityDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trails Location Tracking Twenty Four"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=6,description="Verify the prompt message while enabling Single app mode in Windows") 
//	public void VerifythepromptmessagewhileenablingSingleappmodeinWindowsTC_JO_838() throws Throwable {
//		Reporter.log("\n6.Verify the prompt message while enabling Single app mode in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.clickOnsurelocksettings();
//		androidJOB.clickOnsurelocksettingsConfigure();
//		androidJOB.clickOnSingleApplicationMode();
//		androidJOB.EnableSingleApplicationModeCheckBox();
//		androidJOB.SingleApplicationModePromptMessage();
//		commonmethdpage.ClickOnHomePage();				
//}
	
//	@Test(priority=7,description="Verify the prompt message while enabling Single app mode in Windows") 
//	public void VerifythepromptmessagewhileenablingSingleappmodeinWindowsTC_JO_838() throws Throwable {
//		Reporter.log("\n7.Verify the prompt message while enabling Single app mode in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.clickOnsurelocksettings();
//		androidJOB.clickOnsurelocksettingsConfigure();
//		androidJOB.clickOnSingleApplicationMode();
//		androidJOB.EnableSingleApplicationModeCheckBox();
//		androidJOB.SingleApplicationModePromptMessage();
//		commonmethdpage.ClickOnHomePage();				
//}	
	
//	@Test(priority=8,description="Verify Single app mode in SureLock Settings from Windows Static job") 
//	public void Verify SingleappmodeinSureLockSettingsfromWindowsStaticjobTC_JO_836() throws Throwable {
//		Reporter.log("\n8.Verify Single app mode in SureLock Settings from Windows Static job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.clickOnsurelocksettings();
//		androidJOB.ClickonAllowedWebsites();
//		androidJOB.AllowedWebsitesDetailsConfigured();
//		androidJOB.clickOnsurelocksettingsConfigure();
//		androidJOB.clickOnSingleApplicationMode();
//		androidJOB.EnableSingleApplicationModeCheckBox();
//		androidJOB.ExitSingleApplicationMode();
//		androidJOB.SureLockJobDetails();		
//		androidJOB.JobCreatedMessage();		
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.SureLockDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Surelock Settings Job"); 
//		androidJOB.WindowsActivityLog();					
//}	
	
//	@Test(priority=9,description="Verify Single app mode remains enabled when SureLock settings job is modified") 
//	public void VerifySingleappmoderemainsenabledwhenSureLocksettingsjobismodifiedTC_JO_837() throws Throwable {
//		Reporter.log("\n9.Verify Single app mode remains enabled when SureLock settings job is modified",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.clickOnsurelocksettings();
//		androidJOB.ClickonAllowedWebsites();
//		androidJOB.AllowedWebsitesDetailsConfigured();
//		androidJOB.clickOnsurelocksettingsConfigure();
//		androidJOB.clickOnSingleApplicationMode();
//		androidJOB.EnableSingleApplicationModeCheckBox();
//		androidJOB.ExitSingleApplicationMode();
//		androidJOB.SureLockJobDetails();		
//		androidJOB.JobCreatedMessage();	
//		androidJOB.SearchTextBoxModifiedJob("Trials Surelock Settings Job");
//		androidJOB.ClickonModifiedbutton();
//		androidJOB.clickOnsurelocksettingsConfigure();
//		androidJOB.clickOnSingleApplicationMode();
//		androidJOB.EnableSingleApplicationModeModifiedCheckBox();
//		androidJOB.CloseSurelockModeSetting();
//		commonmethdpage.ClickOnHomePage();				
//}	
	
//	@Test(priority=10,description="Verify creating file transfer job with single multiple with foreign character in file name for Windows") 
//	public void VerifycreatingfiletransferjobwithsinglemultiplewithforeigncharacterinfilenameforWindowsTC_JO_823() throws Throwable {
//		Reporter.log("\n10.Verify creating file transfer job with single multiple with foreign character in file name for Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.clickOnFileTransferJob();		
//		androidJOB.enterJobName("c�mo est�s");
//		androidJOB.clickOnAdd();
//		androidJOB.enterFilePathURL(Config.MultipleFileTransferURL);
//		androidJOB.EnterDevicePath("/sdcard/Download");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.JobCreatedMessage(); 
//		commonmethdpage.ClickOnHomePage();					
//}	
	
//	@Test(priority=11,description="Verify modifying the file transfer job with foregin character in file name for Windows") 
//	public void VerifymodifyingthefiletransferjobwithforegincharacterinfilenameforWindowsTC_JO_824() throws Throwable {
//		Reporter.log("\n11.Verify modifying the file transfer job with foregin character in file name for Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.clickOnFileTransferJob();		
//		androidJOB.enterJobName("c�mo est�s");
//		androidJOB.clickOnAdd();
//		androidJOB.enterFilePathURL(Config.MultipleFileTransferURL);
//		androidJOB.EnterDevicePath("/sdcard/Download");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnWindowSJob();
//		androidJOB.JobCreatedMessage();		
//		androidJOB.SearchModifiedJob("c");
//		androidJOB.ClickonModifiedbutton();
//		androidJOB.enterJobName("c�mo est�s");
//		androidJOB.clickOkBtnOnWindowModifiedJob();
//		androidJOB.JobModifiedMessage();	
//		commonmethdpage.ClickOnHomePage();					
//}	
	
	
//	@Test(priority=12,description="Verify adding composite job in Windows Geo fence job") 
//	public void VerifyaddingcompositejobinWindowsGeofencejobTC_JO_805() throws Throwable {
//		Reporter.log("\n12.Verify adding composite job in Windows Geo fence job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickOnCompositeJob();
//		androidJOB.CompositeJobName("Geo Fence Job");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeGeoFencingJob("Trials Geo Fencing");
//		androidJOB.JobCannotbeAddedMessage();
//		androidJOB.closeSelectJobtoAddTab();
//		commonmethdpage.ClickOnHomePage();	
//}	
//	
//	@Test(priority=13,description="Verify adding composite job in Windows Time fence job") 
//	public void VerifyaddingcompositejobinWindowsTimefencejobTC_JO_806() throws Throwable {
//		Reporter.log("\n13.Verify adding composite job in Windows Time fence job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickOnCompositeJob();
//		androidJOB.CompositeJobName("Time Fence Job");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeGeoFencingJob("Trials Time Fencing");
//		androidJOB.JobCannotbeAddedMessage();
//		androidJOB.closeSelectJobtoAddTab();
//		commonmethdpage.ClickOnHomePage();	
//}		
	
//	@Test(priority=14,description="Intel AMT job configuration in windows platform") 
//	public void IntelAMTjobconfigurationinwindowsplatformTC_JO_809() throws Throwable {
//		Reporter.log("\n14.Intel AMT job configuration in windows platform",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();		
//		androidJOB.IntelAMTConfigurationVisible();
//		androidJOB.ClickonCancelButton();
//		commonmethdpage.ClickOnHomePage();			
//}			
	
//	@Test(priority=15,description="Create an Intel AMT configuration job") 
//	public void CreateanIntelAMTconfigurationjobTC_JO_811() throws Throwable {
//		Reporter.log("\n15.Create an Intel AMT configuration job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();		
//		androidJOB.IntelAMTConfigurationVisible();
//		androidJOB.CreateIntelAMTConfigurationJob();
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();			
//}		
	
//	@Test(priority=16,description="Verify UI of install Application job job in windows") 
//	public void VerifyUIofinstallApplicationjobinwindowsTC_JO_744() throws Throwable {
//		Reporter.log("\n16.Verify UI of install Application job in windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonInstallApplication();
//		androidJOB.ClickonInstallApplicationADDButton();
//		androidJOB.verifyOptionsInstallJob();
//		androidJOB.verifyOptionsinExecutePath();
//		androidJOB.CloseInstallJobTab();
//		commonmethdpage.ClickOnHomePage();				
//}		
	
//	@Test(priority=17,description="Verify silent install of exe file by executing verysilent in Windows") 
//	public void VerifysilentinstallofexefilebyexecutingverysilentinWindowsTC_JO_745() throws Throwable {
//		Reporter.log("\n17.Verify silent install of exe file by executing verysilent in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonInstallApplication();
//		androidJOB.enterJobName("Trials Install Application exe Job");
//		androidJOB.ClickonInstallApplicationADDButton();
//		androidJOB.BrowserInstallApplication();
//		androidJOB.browseFileInstallApplications("./Uploads/UplaodFiles/Install Application/\\App.exe");
//		androidJOB.ExecutePathVerysilentSelected();	
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.InstallApplicationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Install Application exe Job"); 
//		androidJOB.WindowsActivityLog();			
//}		
	
//	@Test(priority=18,description="Verify silent install of msi file by executing verysilent in Windows") 
//	public void VerifysilentinstallofmsifilebyexecutingverysilentinWindowsTC_JO_746() throws Throwable {
//		Reporter.log("\n18.Verify silent install of msi file by executing verysilent in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonInstallApplication();
//		androidJOB.enterJobName("Trials Install Application msi Job");
//		androidJOB.ClickonInstallApplicationADDButton();
//		androidJOB.BrowserInstallApplication();		
//		androidJOB.browseFileInstallApplications("./Uploads/UplaodFiles/Install Application/\\Appmsi.exe");		
//		androidJOB.ExecutePathqnSelected();			
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.InstallApplicationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Install Application msi Job"); 
//		androidJOB.WindowsActivityLog();			
//}		
	
	
//	@Test(priority=19,description="Verify Silently install the msi package without reboot in Windows") 
//	public void VerifySilentlyinstallthemsipackagewithoutrebootinWindowsTC_JO_747() throws Throwable {
//		Reporter.log("\n19.Verify Silently install the msi package without reboot in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonInstallApplication();
//		androidJOB.enterJobName("Trials Install Application msi Job");
//		androidJOB.ClickonInstallApplicationADDButton();
//		androidJOB.BrowserInstallApplication();		
//		androidJOB.browseFileInstallApplications("./Uploads/UplaodFiles/Install Application/\\Appmsi.exe");			
//		androidJOB.ExecutePathqnnorestartSelected();//			
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.InstallApplicationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Install Application msi Job"); 
//		androidJOB.WindowsActivityLog();			
//}		
	
	
//	@Test(priority=20,description="Verify  Silently install the msi package with reboot in Windows") 
//	public void VerifySilentlyinstallthemsipackagewithrebootTC_JO_748() throws Throwable {
//		Reporter.log("\n20.Verify Silently install the msi package with reboot in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonInstallApplication();
//		androidJOB.enterJobName("Trials Install Application msi Job");
//		androidJOB.ClickonInstallApplicationADDButton();
//		androidJOB.BrowserInstallApplication();		
//		androidJOB.browseFileInstallApplications("./Uploads/UplaodFiles/Install Application/\\Appmsi.exe");			
//		androidJOB.ExecutePathSilentnorestartSelected();//			
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.InstallApplicationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Install Application msi Job"); 
//		androidJOB.WindowsActivityLog();			
//}		
	
	
//	@Test(priority=21,description="Verify Silently install the msi package without reboot in Windows") 
//	public void VerifySilentlyinstallthemsipackagewithoutrebootinWindowsTC_JO_749() throws Throwable {
//		Reporter.log("\n21.Verify Silently install the msi package with reboot in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonInstallApplication();
//		androidJOB.enterJobName("Trials Install Application msi Job");
//		androidJOB.ClickonInstallApplicationADDButton();
//		androidJOB.BrowserInstallApplication();		
//		androidJOB.browseFileInstallApplications("./Uploads/UplaodFiles/Install Application/\\Appmsi.exe");			
//		androidJOB.ExecutePathSilentforcerestartSelected();//			
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.InstallApplicationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Install Application msi Job"); 
//		androidJOB.WindowsActivityLog();			
//}		
	
//	@Test(priority=22,description="Verify  Silently install the exe package with reboot in Windows") 
//	public void VerifySilentlyinstalltheexepackagewithrebootinWindowsTC_JO_750() throws Throwable {
//		Reporter.log("\n22.Verify Silently install the exe package with reboot in windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonInstallApplication();
//		androidJOB.enterJobName("Trials Install Application exe Job");
//		androidJOB.ClickonInstallApplicationADDButton();
//		androidJOB.BrowserInstallApplication();		
//		androidJOB.browseFileInstallApplications("./Uploads/UplaodFiles/Install Application/\\App.exe");		
//		androidJOB.ExecutePathSilentforcerestartSelected();//			
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.InstallApplicationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Install Application exe Job"); 
//		androidJOB.WindowsActivityLog();			
//}	
	
//	@Test(priority=23,description="Verify UI Validation of SureMDM Agent Settings jobs in Windows") 
//	public void VerifyUIValidationofSureMDMAgentSettingsjobsinWindowsTC_JO_732() throws Throwable {
//		Reporter.log("\n23.Verify UI Validation of SureMDM Agent Settings jobs in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonSureMDMAgentSettings();
//		androidJOB.VerifySureMDMAgentSettingsOptions();
//		androidJOB.CloseSureMDMAgentSettings();
//		androidJOB.ClickonCancelButton();
//		commonmethdpage.ClickOnHomePage();			
//}	
	
	
//	@Test(priority=24,description="Verify creating and deploying SureMDM agent settings job in Windows") 
//	public void VerifycreatinganddeployingSureMDMagentsettingsjobinWindowsTC_JO_733() throws Throwable {
//		Reporter.log("\n24.Verify creating and deploying SureMDM agent settings job in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonSureMDMAgentSettings();		
//		androidJOB.SureMDMAgentSettingDetails();
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.SureMDMNixSettings);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials SureMDM Agent job"); 
//		androidJOB.WindowsActivityLog();						
//}	
	
//	@Test(priority=25,description="Verify creating and deploying SureMDM agent settings job with blank password in Windows") 
//	public void VerifycreatinganddeployingSureMDMagentsettingsjobwithblankpasswordinWindowsTC_JO_734() throws Throwable {
//		Reporter.log("\n25.Verify creating and deploying SureMDM agent settings job with blank password in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonSureMDMAgentSettings();		
//		androidJOB.SureMDMAgentSettingBlankPassword();
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.SureMDMNixSettings);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials SureMDM Agent job"); 
//		androidJOB.WindowsActivityLog();						
//}	
	
//	@Test(priority=26,description="Verify modifying the SureMDM Agent settings job in Windows") 
//	public void VerifymodifyingtheSureMDMAgentsettingsjobinWindowsTC_JO_735() throws Throwable {
//		Reporter.log("\n26.Verify modifying the SureMDM Agent settings job in Windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonSureMDMAgentSettings();		
//		androidJOB.SureMDMAgentSettingModifiedjob();
//		androidJOB.JobCreatedMessage();
//		androidJOB.SearchTextBoxModifiedJob("Trials SureMDM Agent Modified job");
//		androidJOB.ClickonModifiedbutton();
//		androidJOB.SureMDMAgentModifiedjob();
//		androidJOB.JobModifiedMessage();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.SureMDMNixSettings);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials SureMDM Agent Modified job"); 
//		androidJOB.WindowsActivityLog();						
//}	
	
//	@Test(priority =27,description = "Verify UI of Firewall Policy Job in Windows")
//	public void VerifyUIofFirewallPolicyJobinWindowsTC_JO_727() throws InterruptedException {
//		Reporter.log("\n27.Verify UI of Firewall Policy Job in Windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnFireWallP0licyJob();
//		androidJOB.verifyOptionInFireWallPolicyJob();
//}
	
//	@Test(priority =28,description = "Verify Creating and Applying Firewall Policy Job Blocklist URL in Windows")
//	public void VerifyCreatingandApplyingFirewallPolicyJobBlocklistURLinWindowsTC_JO_728() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n28.Verify Creating and Applying Firewall Policy Job Blocklist URL in Windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnFireWallP0licyJob();
//		androidJOB.FirewallBlocklistjobName();
//		androidJOB.EnableFirewallPolicyCheckbox();
//		androidJOB.FirewallPolicyDomainName();
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.FirewallpolicyDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Firewall Blocklist Job"); 
//		androidJOB.WindowsActivityLog();		
//}
//	
//	@Test(priority =29,description = "Verify Creating and Applying Firewall Policy Job Allowlist URL in Windows")
//	public void VerifyCreatingandApplyingFirewallPolicyJobAllowlistURLinWindowsTC_JO_729() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n29.Verify Creating and Applying Firewall Policy Job Allowlist URL in Windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnFireWallP0licyJob();
//		androidJOB.FirewallAllowlistjobName();
//		androidJOB.EnableFirewallPolicyCheckbox();
//		androidJOB.EnableFirewallPolicyAllowlist();		
//		androidJOB.FirewallPolicyDomainName();
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.FirewallpolicyDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Firewall Allowlist Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority =29,description = "Verify already created job modify and Apply to Device in Windows")
//	public void VerifyalreadyCreatedjobmodifyandApplytoDeviceTC_JO_730() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n29.Verify already created job modify and Apply to Device in Windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnFireWallP0licyJob();		
//		androidJOB.FirewallModifiedjobName();
//		androidJOB.EnableFirewallPolicyCheckbox();
//		androidJOB.EnableFirewallPolicyAllowlist();		
//		androidJOB.FirewallPolicyDomainName();
//		androidJOB.JobCreatedMessage();	
//		androidJOB.SearchTextBoxModifiedJob("Trials Firewall Modified Job");
//		androidJOB.ClickonModifiedbutton();
//		androidJOB.FirewallModifiedJobNameDetails();
//		androidJOB.JobModifiedMessage();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.FirewallpolicyDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Firewall Modified Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority =30,description = "Verify Disabling Firewall Policy Job in Windows")
//	public void VerifyDisablingFirewallPolicyJobinWindowsTC_JO_731() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n30.Verify Disabling Firewall Policy Job in Windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnFireWallP0licyJob();			
//		androidJOB.FirewallJobNameDetails();		
//		androidJOB.JobCreatedMessage();				
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.FirewallpolicyDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Firewall Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
