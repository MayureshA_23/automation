package CustomReportsScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class CallLog_CustomReport extends Initialization {
@Test(priority='0',description="Enabling CallLog DynamicJob")
public void EnablingCallLogDynamicJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnCallLog();
	    dynamicjobspage.SelectCallLogStatus_On();
	    dynamicjobspage.VerifyOfUpdateInterval();
	    dynamicjobspage.ClickOnCallLogCloseBtn();
	    commonmethdpage.ClickOnAllDevicesButton(); 
	}
@Test(priority='1',description="Verify generating Call Log Custom report with filters for 'Current Date'")
public void VerifyingCallLogForSomeDuration() throws InterruptedException
	{Reporter.log("\nVerify generating Call Log Custom report with filters for 'Current Date'",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Call Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectCallLogTable);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CallLog Custrep with filters for Current Date","Test");
		customReports.Selectvaluefromdropdown(Config.SelectCallLogTable);
		customReports.SelectValueFromColumn(Config.SelectCallDuration);
		customReports.SelectOperatorFromDropDown("!=");	
		customReports.SendValueToTextfield(Config.EnterCallDuration);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("CallLog Custrep with filters for Current Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectTodayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CallLog Custrep with filters for Current Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CallLog Custrep with filters for Current Date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingCurrentDateInViewReport();
		customReports.Varifycallduration();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();		
	}
@Test(priority='2',description="Verify generating Call Log Custom report with filters for '1week Date'")
public void VerifyingCallLogForOneWeekDate() throws InterruptedException
	{Reporter.log("\nVerify generating Call Log Custom report with filters for '1week Date'",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Call Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectCallLogTable);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CallLog Custrepor with filters for one week Date","Test");
		customReports.Selectvaluefromdropdown(Config.SelectCallLogTable);
		customReports.SelectValueFromColumn("Datetime");
		customReports.SelectOneWeekDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("CallLog Custrepor with filters for one week Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CallLog Custrepor with filters for one week Date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CallLog Custrepor with filters for one week Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CallLog Custrepor with filters for one week Date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingOneWeekDateInCallLogReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
    }
@Test(priority='3',description="Verify generating Call Log Custom report with filters for 'Yesterday' date")
public void VerifyingCallLogReportOnlyForIncomingCalls() throws InterruptedException
	{Reporter.log("\nVerify generating Call Log Custom report with filters for 'Yesterday' date",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Call Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectCallLogTable);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CallLog Custrepor with filters as like Op for Yesterdays date","This is Custom report");
		customReports.Selectvaluefromdropdown(Config.SelectCallLogTable);
		customReports.SelectValueFromColumn(Config.SelectCallTypeInColumn);
		customReports.SelectOperatorFromDropDown("like");	
		customReports.SendValueToTextfield(Config.EnterCallType);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("CallLog Custrepor with filters as like Op for Yesterdays date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CallLog Custrepor with filters as like Op for Yesterdays date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CallLog Custrepor with filters as like Op for Yesterdays date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CallLog Custrepor with filters as like Op for Yesterdays date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingCalltype_Calllog();
		customReports.VarifyingYesterDateInViewReport_Calllog();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
@Test(priority='4',description="Verify generating Call Log Custom report without filters for 'Current Date'")
public void VerifyingCallLogReportForAllData() throws InterruptedException
	{Reporter.log("\nVerify generating Call Log Custom report without filters for 'Current Date'",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Call Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectCallLogTable);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CallLog Custrepor without filters for Current Date","Test");
		customReports.Selectvaluefromdropdown(Config.SelectCallLogTable);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("CallLog Custrepor without filters for Current Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CallLog Custrepor without filters for Current Date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectTodayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CallLog Custrepor without filters for Current Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CallLog Custrepor without filters for Current Date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingCurrentDateInViewReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='5',description="Verify Sort by and Group by for Call Log")
public void VerifyingCallLogReportAfterSorting_TC_RE_104() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for Call Log",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Call Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectCallLogTable);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CallLog Custrepor with Sortby and Groupby","Test");
		customReports.Selectvaluefromsortbydropdown(Config.SelectCallTypeInColumn);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CallLog Custrepor with Sortby and Groupby");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CallLog Custrepor with Sortby and Groupby");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CallLog Custrepor with Sortby and Groupby");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CallLog Custrepor with Sortby and Groupby");
		reportsPage.WindowHandle();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
}
}
