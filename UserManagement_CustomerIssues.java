package UserManagement_TestScripts;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.tools.ant.taskdefs.WaitFor;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class UserManagement_CustomerIssues extends Initialization
{

    @Test(priority=1,description="77479/Nathalie Jutzi/Smart Liberty/Unable to delete user 'MDR' using account admin credential for on prem/17-04-2020")
	public void DeleteUser() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser("DeleteUser");
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails("DeleteUser","42Gears@123","dontDelete","abc@gmail.com");
		usermanagement.ClickOnCreateUserButton();
		usermanagement.SelectandDeleteUser("DeleteUser");
		usermanagement.SearchDeletedUser("DeleteUser");
	}

	@Test(priority=2,description="Issue/75983/User is able to log in with certain credentials even if the account is disabled or if the credentials are changed/22nd may, 2020\"")
	public void VerifySubUserIsUnableToLoginWhenDisabled() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.DemoUserDisabled);
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.DemoUserDisabled,Config.pwd3,Config.DemoUserDisabled,"abc@gmail.com");
		usermanagement.ClickOnCreateUserButton();
		usermanagement.SearchUser(Config.DemoUserDisabled);
		usermanagement.selectUserDisable();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginwithDisablesUser(Config.DemoUserDisabled,Config.pwd3);
		loginPage.login(Config.userID,Config.password);		
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.SearchUser(Config.DemoUserDisabled);
		usermanagement.selectUserInUsersList(Config.DemoUserDisabled);
		usermanagement.ChangingPasswordForSubUser("Password@123");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginwithDisablesUser(Config.DemoUserDisabled,"Password@123");
		loginPage.login(Config.userID,Config.password);		
	}

	@Test(priority=3,description="DIDIGlobal/\"Access Denied. Please Contact Administrator\" issue EVEN WHEN the user has role permissions")
	public void VerifyUserCanAccessRemoteWhenUserHasRolePermission() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.RemotePermissionUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Automation Allow Remote Role");
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName("Automation Allow Remote Role");
		usermanagement.OpenPermissionsDropDown("Device Action Permissions");
		usermanagement.SelectingPermission("Remote Device");
		usermanagement.OpenPermissionsDropDown("Device Management Permissions");
		usermanagement.SelectingPermission("Allow Remote Screen");
		usermanagement.SelectingPermission("Allow Remote File Manager");
		usermanagement.SelectingPermission("Allow Remote Clipboard");
		usermanagement.SelectingPermission("Allow Remote Task Manager");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.RemotePermissionUser,Config.pwd3,Config.RemotePermissionUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Automation Allow Remote Role");
		usermanagement.AssigningGroupSetToUser("Super User");
		usermanagement.ClickOnCreateUserButton();
		usermanagement.SearchUser(Config.RemotePermissionUser);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.RemotePermissionUser,Config.pwd3);
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfRemoteSupportLaunched();
		remotesupportpage.SwitchToMainWindow();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);		
		
	}

	@Test(priority=4,description="Issue/Adam (Command Alkon)/Not able to create jobs. Shows Access denied even though having admin user role/Oct 16, 2020")
	public void VerifyUserCanCreateJobsWhenUserHasRolePermission() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.JobPermissionUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Automation Allow Job Roles");   
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName("Automation Allow Job Roles");
		usermanagement.OpenPermissionsDropDown("Job Permissions");
		usermanagement.SelectingPermission("New Job");
		usermanagement.SelectingPermission("Delete Job");
		usermanagement.SelectingPermission("Modify Job");
		usermanagement.SelectingPermission("Move Jobs To Folder");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.JobPermissionUser,Config.pwd3,Config.JobPermissionUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Automation Allow Job Roles");
		usermanagement.ClickOnCreateUserButton();
		usermanagement.SearchUser(Config.JobPermissionUser);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.JobPermissionUser,Config.pwd3);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("AutomationSubUserJob", "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("AutomationSubUserJob");
		Reporter.log("PASS>> Job Successfully Created By Sub User",true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);		
	}
	
	@Test(priority=5,description="84613/Daniel(Consec group)/When logged into SureMDM with a particular user account, 'Roles' field shows blank in User Management/ 09-12-2020")
	public void VerifyUserRoleFieldIsnotEmpty() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.Superuser);
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.Superuser,Config.pwd3,Config.Superuser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Super User");
		usermanagement.ClickOnCreateUserButton();
		usermanagement.SearchUser(Config.Superuser);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.Superuser,Config.pwd3);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.SearchUser(Config.Superuser);
		usermanagement.selectUserInUsersList(Config.Superuser);
		usermanagement.ClickOnEdituserButton();
		usermanagement.VerifyRoleFieldIsNotEmptyWhenLoggedInasSubUser("Super User");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);	
	}
	
	@Test(priority=6,description="Issue/85523/Zakrij/Suremdm logs out when Push file/Push application feature is used/2nd Feb, 2021")
	public void VerifySubUserAbleToPushFileAndApplications() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.PushFileApplication);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Role With Push Permission");   
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName("Role With Push Permission");
		usermanagement.OpenPermissionsDropDown("Device Action Permissions");
		usermanagement.SelectingPermission("Apply Job");
		usermanagement.OpenPermissionsDropDown("Job Permissions");
		usermanagement.SelectingPermission("New Job");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.PushFileApplication,Config.pwd3,Config.PushFileApplication,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Role With Push Permission");
		usermanagement.AssigningGroupSetToUser("Super User");
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.PushFileApplication,Config.pwd3);
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickonPushApplication();
		rightclickDevicegrid.UploadAPK(Config.Pushapk,Config.apkname);
		rightclickDevicegrid.ClickOnNextBtnPushApplicationPopup();
	    rightclickDevicegrid.VerifyingSuccessMsg();
	    rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
		Reporter.log("PASS>>> Sub User Is Able To Push Application",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);	
	}
	
	@Test(priority=7,description="Issue/85523/Zakrij/Able to add and assign device to tags when not permitted in roles/2nd Feb,2021")
	public void VerifyUserIsNotAbleToAddTagWhenNotpermitted() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.TagsPermissionUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Role Without Tag Permission");
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName("Role Without Tag Permission");
		usermanagement.OpenPermissionsDropDown("Device Action Permissions");
		usermanagement.SelectingPermission("Apply Job");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.TagsPermissionUser,Config.pwd3,Config.TagsPermissionUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser("Role Without Tag Permission");
		usermanagement.AssigningGroupSetToUser("Super User");
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.TagsPermissionUser,Config.pwd3);
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnTagOptions_RightClick("Add Tags");
		usermanagement.VerifyAddingTagWhenNotPermitted("Automation_Tag");
		Tags.ClikOnTaglistCloseButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);		
	}
	@Test(priority=8,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_MarkAsRead() throws InterruptedException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		commonmethdpage.enterSubInComposeMessage(4,"AutomationsubUserTest", "test");
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.SelectingAllPermissions("Inbox Permissions");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingUnreadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("read");
	}
	
	@Test(priority=9,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios"
			,dependsOnMethods="VerifyUMInboxPermission_MarkAsRead")
    public void VerifyUMInboxPermission_MarkAsUnRead() throws InterruptedException
    {
		inboxpage.SelectingReadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("unread");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
    }
	
	@Test(priority=10,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReplyUnread() throws InterruptedException, IOException
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		commonmethdpage.enterSubInComposeMessage(4,"AutomationsubUserTest", "test");
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Reply","Mark As Unread"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingReadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("unread");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=11,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReadUnread() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Mark As Read","Mark As Unread"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingUnreadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("read");
		inboxpage.SelectingReadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("unread");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=12,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_UnReadCleanUp() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Mark As Unread","Delete"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingReadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("unread");
	    commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=13,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReadCleanUp() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Mark As Read","Delete"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingUnreadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("read");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	@Test(priority=14,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReplyRead() throws InterruptedException, IOException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Reply","Mark As Read"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingUnreadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("read");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	
	@Test(priority=15,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReadUnreadCleanUp() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Mark As Read","Mark As Unread","Delete"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingReadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("unread");
		inboxpage.SelectingUnreadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("read");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=16,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReplyReadUnread() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Reply","Mark As Read","Mark As Unread"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingReadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("unread");
		inboxpage.SelectingUnreadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("read");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=17,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReplyReadCleanUp() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Reply","Mark As Read","Delete"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingUnreadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("read");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=18,description="User Managment> Inbox permissions>Mark as read and Mark as unread are not working in the below mentioned scenarios")
	public void VerifyUMInboxPermission_ReplyUnReadCleanUp() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Reply","Mark As Unread","Delete"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		inboxpage.ClickOnInbox();
		inboxpage.SelectingReadMessage();
		inboxpage.ClickOnMarkAsReadYesButton();
		inboxpage.VerfySuccessMessageOnMarkAsRead("unread");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=19,description="UI issue in User Management>Inbox permissions")
	public void VerifuInboxPermissionUI() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.OpenPermissionsDropDown("Inbox Permissions");
		String[] permissions={"Reply","Delete"};
		usermanagement.VerifyInboxPermissionUI(permissions);
		usermanagement.ClickOnCloseRoleWindow();
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority=20,description="Mobile Email Management is shown for sub user even though permission is not given")
	public void VerifyEmailManagementForSubuser() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Account Settings");
		usermanagement.SelectingPermission("Branding Info");
		usermanagement.SelectingPermission("Alert Template");
		usermanagement.SelectingPermission("Data Analytics");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		usermanagement.VerifyMEMIsNotDisplayedForSubUserWhenNotPermitted();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=21,description="Sub user is not able to access plugins.")
	public void VerifySubUserIsAbletoAccessPlugins() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Account Settings");
		String[] permissions={"Branding Info","Alert Template","Data Analytics","Firmware Updates","License Management"};
		usermanagement.SelectingMultiplePermissons(permissions);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		usermanagement.verifySubUserIsableToaccessPlugin();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
		
	}
	
	@Test(priority=22,description="Even if Apply profile permission is given,Sub user is not able to apply profile to device")
	public void VerifySubUserIsAbleToApplyProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Device Action Permissions");
		usermanagement.SelectingPermission("Apply Profile");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.AssigningGroupSetToUser("Super User");
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		Reporter.log("Sub user is able to apply profiles on device.",true);
		quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
		
	}
	
	@Test(priority=23,description="Screen will freeze for sometime when we click on edit Device Group set")
	public void VerifyScreenOnClickingeditGroupSet() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnDeviceGroupSet();
		usermanagement.SelectingDeviceGroupSet();
		usermanagement.ClickOnDeviceGridEditButton();
		usermanagement.ClickOnDevieGroupSaveButton("modified");
		Reporter.log("Console Is Not Freezed when We Click on edit device group set",true);
	}
	
	@Test(priority=24,description="Screen will freeze for sometime when we click on edit Device Group set")
	public void VerifModifyingDeviceGrouSetGroupSet() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnDeviceGroupSet();
		usermanagement.SelectingDeviceGroupSet();
		usermanagement.ClickOnDeviceGridEditButton();
		usermanagement.GettingCheckBoxStatusBeforeModification();
		usermanagement.Enabling_DisablingCheckBox();
		usermanagement.ClickOnDevieGroupSaveButton("modified");
		usermanagement.SelectingDeviceGroupSet();
		usermanagement.ClickOnDeviceGridEditButton();
		usermanagement.VerifyDeviceGroupSetModification();	
		usermanagement.ClosePremissionPopup();
		commonmethdpage.ClickOnHomePage();
		
	}
	
	@Test(priority=25,description="Linux Profile Permission issues in user management")
	public void VerifyDeleteLinuxProfilePermission() throws InterruptedException
	{
		profilesAndroid.ClickOnProfiles();
		profilespage.ClickOnLinuxProfiles();
		profilespage.ClickOnAddButton_Linux();
		profilespage.ClickOnProfileType("Blocklist URLs");
		profilespage.ClickOnBlockListURLConfigButton();
		profilespage.ClickOnAddBlackURLList();
		profilespage.SendingURL_Name_Linux("Google","https://google.com");
		profilespage.ClickOnNameURLAddButton_Linux();
		profilespage.SendingLinuxProfileName(Config.LinuxProfileName);
		profilespage.ClickOnLinuxProfileSaveButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Profile Permissions");
		usermanagement.SelectingPermission("Delete Linux Profile");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		profilesAndroid.ClickOnProfiles();
		profilespage.ClickOnLinuxProfiles();
		profilespage.Searchprofile(Config.LinuxProfileName);
		profilespage.ClickOnProfiledeleteButton();
		profilespage.ClickOnDeleteProfileYesButton();
		usermanagement.VerifyProfileDeltedSuccessfully();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=26,description="Linux Profile Permission issues in user management")
	public void VerifySetAsDefaultLinuxProfilePermission() throws InterruptedException
	{
		profilesAndroid.ClickOnProfiles();
		profilespage.ClickOnLinuxProfiles();
		profilespage.ClickOnAddButton_Linux();
		profilespage.ClickOnProfileType("Blocklist URLs");
		profilespage.ClickOnBlockListURLConfigButton();
		profilespage.ClickOnAddBlackURLList();
		profilespage.SendingURL_Name_Linux("Google","https://google.com");
		profilespage.ClickOnNameURLAddButton_Linux();
		profilespage.SendingLinuxProfileName(Config.LinuxProfileName);
		profilespage.ClickOnLinuxProfileSaveButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Job Permissions");
		usermanagement.SelectingPermission("Modify Job");
		usermanagement.OpenPermissionsDropDown("Profile Permissions");
		usermanagement.SelectingPermission("Set Default Linux Profile");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		profilesAndroid.ClickOnProfiles();
		profilespage.ClickOnLinuxProfiles();
		profilespage.Searchprofile(Config.LinuxProfileName);
		profilespage.ClickOnSetAsDefaultButton(Config.LinuxProfileName);
		Reporter.log("PASS>>> User Can Set Profile As Default",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=27,description="UI changes in User Mangement")
	public void VerifyUMMessage() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.VerifyUMMessage(Config.UMMessage);
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority=28,description="Application Policy Compliance text from user mangement>>roles should be changed to Blocklisted Applications")
	public void VerifyBlockListedApplicationsunderDashboardPermission() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.OpenPermissionsDropDown("Dashboard Permissions");
		usermanagement.VerifyPermissionUI("Blocklisted Applications");
		usermanagement.ClickOnCloseRoleWindow();
		commonmethdpage.ClickOnHomePage();		
	}
	
	@Test(priority=29,description="Last Logged In User and Host name is not added in UserManagement>>Deviece Grid Column set")
	public void VerifyLasLoggedINUserInDeviceGridColumnSet() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.clickOnDeviceGridColumn();
		usermanagement.ClickOnDevicgridColumnSetAddButton();
		usermanagement.VerifyPermissionUI("Last Logged In User");
		usermanagement.VerifyPermissionUI("Hostname");
		usermanagement.VerifyPermissionUI("DEP Server");
		usermanagement.ClickOnGridColumnCloseButton();
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority=30,description="Console is logging out and logging in when only new Job permission is given for sub user")
	public void VerifyConsoleWhenSubUserIsPermittednewJobPermission() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Job Permissions");
		usermanagement.SelectingPermission("New Job");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		dynamicjobspage.SearchJobInSearchField(Config.TextMessageJobname);
    	androidJOB.ClickOnOnJob(Config.TextMessageJobname);
    	androidJOB.clickOnCopyBtn();
    	androidJOB.ClickOnCopyJobOKButton();
		commonmethdpage.ClickOnHomePage();	
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
		
	@Test(priority=31,description="Issues in sub-user, when some permissions are given from super user")
	public void VeirfyRightClick_Groups_TagsForSubUserWhenNotPermitted() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Job Permissions");
		usermanagement.SelectingPermission("New Job");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.AssigningGroupSetToUser("Super User");
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnTagOptions_RightClick("Add Tags");
		Tags.EnterCustomTagName("Sample Tags");
		Tags.AddButtonCustomTag();
		usermanagement.VerifyAccessDenied();
		Tags.CloseAddingNewTagPopup();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		usermanagement.ClikcOnMovToGroup();
		usermanagement.VerifyAccessDenied();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickOnTagOptions_RightClick("Remove Tags");
		usermanagement.ClikcOnRemoveTagSaveButton();
		usermanagement.VerifyAccessDenied();
		Tags.CloseAddingNewTagPopup();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=32,description="Issues in sub-user, when some permissions are given from super user")
	public void VerifyAccountSettingsOptionForSubUserallowedonePermission() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.SelectingAllPermissions("Job Permissions");
		usermanagement.OpenPermissionsDropDown("Account Settings");
		usermanagement.SelectingPermission("Branding Info");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		commonmethdpage.ClickOnSettings();
		usermanagement.VerifyOptionForsubuser(usermanagement.AccountSettingsOption,"Account Settings");
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=33,description="Issues in sub-user, when some permissions are given from super user")
	public void VerifydeletingAndroidProfile_SubUser() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnWifiConfigurationPolicy();
		profilesAndroid.ClickOnWifiConfigurationPolicyConfigButton();
		profilesAndroid.EnterSSID_PasswordValues_WifiConfigurationWAP2();
		profilesAndroid.EnableAutoConnect();
		profilesAndroid.EnterWifiConfigurationProfileName(Config.AndroidProfileName);
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Profile Permissions");
		usermanagement.SelectingPermission("Delete Android Profile");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		profilesAndroid.ClickOnProfiles();
		profilespage.Searchprofile(Config.AndroidProfileName);
		profilespage.ClickOnProfiledeleteButton();
		profilespage.ClickOnDeleteProfileYesButton();
		usermanagement.VerifyProfileDeltedSuccessfully();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=34,description="Issues in sub-user, when some permissions are given from super user")
	public void VerifydeletingWindowsProfile_SubUser() throws InterruptedException, AWTException
	{
		profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
        profilesWindows.ClickOnApplicationPolicy();
        profilesWindows.ClickOnConfigureButton_ApplicationPolicyProfile();
        profilesWindows.EnterApplicationPolicyProfileName();
        profilesWindows.ClickOnAddButton_ApplicationPolicy();
        profilesWindows.ClickOnChooseApp();
		profilesWindows.EnterApplicationName();
		profilesWindows.ClickOnAddButtonInAddAppWindow();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.NotificationOnProfileCreated();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Profile Permissions");
		usermanagement.SelectingPermission("Delete Windows Profile");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		profilesAndroid.ClickOnProfiles();
		profilesWindows.ClickOnWindowsOption();
		profilespage.Searchprofile(Config.EnterAppLockerProfileName);
		profilespage.ClickOnProfiledeleteButton();
		profilespage.ClickOnDeleteProfileYesButton();
		usermanagement.VerifyProfileDeltedSuccessfully();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=35,description="Issues in sub-user, when some permissions are given from super user")
	public void VerifydeletingIOSProfile_SubUser() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		    profilesAndroid.ClickOnProfile();
			profilesiOS.clickOniOSOption();
			profilesiOS.AddProfile();
			profilesiOS.ClickOnVPN();
			profilesiOS.ClickOnConfigureButtonVPNProfile();
			profilesiOS.EnterVPNProfileName();
			profilesiOS.EnterConnectionName();
			profilesiOS.EnterServer_VPN();
			profilesiOS.EnterPasswordVPN();
			profilesiOS.ChooseConnectionType();
			profilesiOS.ClickOnSavebutton();
			profilesWindows.NotificationOnProfileCreated();
			commonmethdpage.ClickOnSettings();
			usermanagement.ClickOnUserManagementOption();
			usermanagement.VerifyUserManagementIsDisplayed();
			usermanagement.DeletingExistingUser(Config.AutomationUser);
			usermanagement.ClickOnRolesOption();
			usermanagement.DeletingExistingRole(Config.Automationrole);
			usermanagement.ClickOnAddButtonRoles();
			usermanagement.SendingRoleName(Config.Automationrole);
			usermanagement.OpenPermissionsDropDown("Profile Permissions");
			usermanagement.SelectingPermission("Delete iOS Profile");
			usermanagement.ClickOnRoleSaveButton();
			usermanagement.ClickOnUsertabInUM();
			usermanagement.ClickOnAddUserButton();
			usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
			usermanagement.AssigningRoleToUser(Config.Automationrole);
			usermanagement.ClickOnCreateUserButton();
			commonmethdpage.ClickOnSettings();
			usermanagement.logoutAsAdmin();
			loginPage.ClickOnLoginAgainLink();
			loginPage.login(Config.AutomationUser,Config.pwd3);
			profilesAndroid.ClickOnProfiles();
			profilesiOS.clickOniOSOption();
			profilespage.Searchprofile(Config.EnterVPNProfileName);
			profilespage.ClickOnProfiledeleteButton();
			profilespage.ClickOnDeleteProfileYesButton();
			usermanagement.VerifyProfileDeltedSuccessfully();
			commonmethdpage.ClickOnSettings();
			usermanagement.logoutAsAdmin();
			loginPage.ClickOnLoginAgainLink();
			loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=36,description="Issues in sub-user, when some permissions are given from super user")
	public void VerifydeletingMacOsProfile_SubUser() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		profilesAndroid.ClickOnProfile();
		profilespage.ClikconMacOSProfiles();
		profilespage.ClickOnAddButton_MacOS();
		profilespage.ClickONRestrictionProfileConfigureButton();
		profilespage.SendingMacOsProfileName(Config.MacOSProfileName);
		profilespage.ClickOnmacOSprofileSaveButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.OpenPermissionsDropDown("Profile Permissions");
		usermanagement.SelectingPermission("Delete macOS Profile");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		profilesAndroid.ClickOnProfiles();
		profilespage.ClikconMacOSProfiles();
		profilespage.Searchprofile(Config.MacOSProfileName);
		profilespage.ClickOnProfiledeleteButton();
		profilespage.ClickOnDeleteProfileYesButton();
		usermanagement.VerifyProfileDeltedSuccessfully();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
	@Test(priority=37,description="Issues in sub-user, when some permissions are given from super user")
	public void VerifyDeleteGroupAlongWithDeviceWhenSubuserNotPermitted() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		rightclickDevicegrid.ClickonNewGroup();
		rightclickDevicegrid.CreateNewGroup("##AutomationDeleteGroup");
		Filter.ClickOnAllDevices();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup("##AutomationDeleteGroup");
		rightclickDevicegrid.VerifyDeviceMoved();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.SelectingAllPermissions("Group/Tag/Filter Permissions");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		groupspage.ClickOnGroup("##AutomationDeleteGroup");
		groupspage.ClickOnDeleteGroup();
		groupspage.ClickonYesDeleteAlongWithDevices();
		usermanagement.VerifyAccessDenied();
		groupspage.ClickOnDeleteGroupCancelButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);
	}
	
/*	@Test(priority=37,description="Hide Parent Group When No Access To Child Groups' is not working in User management.")
	public void VerifyHideParentGroupWhenNoAccessToChildGroups() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.SelectingAllPermissions("Other Permissions");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.EnableHideParentGroupCheckBox();
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
	}*/
	
	@Test(priority=38,description="Register endpoints to use this Option when subUser clicks on Webhooks in Notification policy job when Webhooks feature is disabled for Sub User.")
	public void VerifyWebhooksDisabledForSubUser() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnWebhookstab();
		accountsettingspage.DisableWebhooks();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.SelectingAllPermissions("Job Permissions");
		usermanagement.OpenPermissionsDropDown("Account Settings");
		String permission[]={"Branding Info","Device Enrollment Rules","Customize Toolbar","Account Management"};
		usermanagement.SelectingMultiplePermissons(permission);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.AutomationUser,Config.pwd3);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnnNotificationPolicyJobs();
		usermanagement.VerifyWebHooksIsNotDisplayed();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		loginPage.login(Config.userID,Config.password);	
	}
	
	@Test(priority=39,description="SIEM integration option present in roles is not getting disabled.")
	public void VerifySIEMIntegration() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.SelectingAllPermissions("Job Permissions");
		usermanagement.OpenPermissionsDropDown("Account Settings");
		String permission[]={"Branding Info","Enterprise Integrations","Customize Toolbar","Account Management"};
		usermanagement.SelectingMultiplePermissons(permission);
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.SearchRole(Config.Automationrole);
		usermanagement.ModifyRole(Config.Automationrole);
		usermanagement.SelectingPermission("Enterprise Integrations");
		usermanagement.ClickOnRoleSaveButton_Modify();
		usermanagement.SearchRole(Config.Automationrole);
		usermanagement.SelectingRole(Config.Automationrole);
		usermanagement.ClickOnEdit();
		usermanagement.VerifyPermissionStatusAfterDisabling("Enterprise Integrations");
		usermanagement.ClickOnRoleSaveButton_Modify();
		commonmethdpage.ClickOnHomePage();
		
	}
	
	@Test(priority=40,description="Error message is thrown when we reset password from user management, but password gets reset successfully.")
	public void VerifyErrorMessageWhenPwdisresetted() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		usermanagement.selectUserInUsersList(Config.AutomationUser);
		usermanagement.ChangingPasswordForSubUser("42Gears@789");
		Reporter.log("No error message displayed on changing password",true);	
	}
	
	@Test(priority=41,description="Message upon deleting user assigned role is showing Role deleted successfully.")
	public void VerifyMessageOnDeletingRoleWhenAssignedtoUser() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole(Config.Automationrole);
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.SendingRoleName(Config.Automationrole);
		usermanagement.SelectingAllPermissions("Job Permissions");
		usermanagement.ClickOnRoleSaveButton();
		usermanagement.ClickOnUsertabInUM();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,Config.pwd3,Config.AutomationUser,"abc@gmail.com");
		usermanagement.AssigningRoleToUser(Config.Automationrole);
		usermanagement.ClickOnCreateUserButton();
		usermanagement.ClickOnRolesOption();
		usermanagement.SearchRole(Config.Automationrole);
		usermanagement.SelectingRole(Config.Automationrole);
		usermanagement.DeleteExistingRolesWhenAssignedToUser();
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority=42,description="Error message is displaying wrong when password does not meet the requirment complexity while creating the user.")
	public void VerifyErrorMessageWhenPasswordIsNotComplex() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.DeletingExistingUser(Config.AutomationUser);
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails(Config.AutomationUser,"test",Config.AutomationUser,"abc@gmail.com");
		usermanagement.VerifyPwdComplextiyErrorMessage();
		commonmethdpage.ClickOnHomePage();
	}
	
	
	@AfterMethod
	public void RefreshingPage(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
			usermanagement.RefreshConsole();
		
		    
	}
}
