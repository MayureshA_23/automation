package DeviceInfoPanel;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Runscript extends Initialization{
	
	
	@Test(priority='Z',description="Creating Runscript")
	public void CreatingRunscript() throws InterruptedException
	{
		Reporter.log("\n33.Creating Runscript",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		accountsettingspage.sendingRunScript(Config.NixPermission_RunScript);
		runScript.RunScriptName("Nix Permission_Automation");
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority = 'a', description = "Verify SureMDM Nix Permission checklist on Device Info Panel")
	public void VerifySureMDMNixPermissionCheckList()
			throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n34.Verify SureMDM Nix Permission checklist on Device Info Panel",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		commonmethdpage.commonScrollMethod("Nix Permission Checklist");
		deviceinfopanelpage.ClickOnNixPermissionCheckListOption();
		deviceinfopanelpage.ReadingStatusInNixPermissionCheckList("Configure Runtime Permissions");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceinfopanelpage.ClickOnDeviceInfoNixPermissionShowButton();
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Telephone Permission");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Location Permission");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Contacts Permission");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Camera Permission");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Storage Permission");
		deviceinfopanelpage.ReadingStatusInNixPermissionCheckList("Activate Device Admin");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Device Administrator");
		deviceinfopanelpage.ReadingStatusInNixPermissionCheckList("Enable Samsung KNOX");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Samsung KNOX");
		deviceinfopanelpage.ReadingStatusInNixPermissionCheckList("Enable Usage Access");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Apps With Usage Access");
		deviceinfopanelpage.ReadingStatusInNixPermissionCheckList("Ignore Battery Optimization");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Ignore Battery Optimization");
		deviceinfopanelpage.ReadingStatusInNixPermissionCheckList("Configure System Permission");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Configure System Permission");
		deviceinfopanelpage.ReadingStatusInNixPermissionCheckList("Enable Display Over Other Apps");
		deviceinfopanelpage.VerifyingNixPermissionCheckList("Draw Over Other Apps");
		deviceinfopanelpage.ClosingNixPermissionStatusInConsole();
	}
	@Test(priority='b',description="Modify SureMDM Nix permissions on the device")
	public void VerifyModifiedSureMDMNixPermissionOnDevice() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n35.Modify SureMDM Nix permissions on the device",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClikOnNixSettings();
		deviceinfopanelpage.DisablingDeviceAdminInNix("Enable Admin");
		commonmethdpage.commonScrollMethod("Nix Permission Checklist");
		deviceinfopanelpage.ClickOnNixPermissionCheckListOption();
		deviceinfopanelpage.AllowingPermissionsInNixpermissionCheckList("Ignore Battery Optimization");
		deviceinfopanelpage.ClickOnAllowButtonInBatteryUsagePopup();
		deviceinfopanelpage.AllowingPermissionsInNixpermissionCheckList("Configure Unknown Sources");
		deviceinfopanelpage.ClickOnConfigUnsourceToggleButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Nix Permission_Automation");
		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.JobInitiatedMessage();
		accountsettingspage.ReadingConsoleMessageForJobDeployment("Nix Permission_Automation",Config.DeviceName);
		deviceinfopanelpage.ClickOnDeviceInfoNixPermissionShowButton();
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterDenyingPermissionInNix("Device Administrator");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Ignore Battery Optimization");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Configure Unknown sources");
		deviceinfopanelpage.ClosingNixPermissionStatusInConsole();
	}
	@Test(priority='c',description="Validate SureMDM Nix permissions on device side and console side ")
	public void ValidateSureMDMNixPermission() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n36.Validate SureMDM Nix permissions on device side and console side ",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		PendingDelete.clickOnDeleteDevice();
		PendingDelete.ClickingOnDeleteDeviceConfirmationYesButton();
		DeviceEnrollment_SCanQR.DelrtingDeviceFromPendingDelete(Config.Enrolled_DeviceIMEI);
		deviceinfopanelpage.UnInstallingNix();
		deviceinfopanelpage.InstallingNix("adb install C:\\Users\\karthik.bs\\Desktop\\Nix_16.0.apk");
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		DeviceEnrollment_SCanQR.ClickOnDeviceEnrollmentRules();
		DeviceEnrollment_SCanQR.CheckingAdvancedDeviceAuthentication();
		DeviceEnrollment_SCanQR.SettingDeviceAuthenticationType_NoAuthentication();
		DeviceEnrollment_SCanQR.ClickOnApplyButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		DeviceEnrollment_SCanQR.ClickOnGetStartedButton();
		deviceinfopanelpage.AllowNixRunPermission();
		deviceinfopanelpage.ClickOnCancelButtonIAcctivateDeviceAdminPrompt();
		deviceinfopanelpage.AllowingUsageAccessPermission();
		deviceinfopanelpage.AllowingBatteryUsagePermission();
		DeviceEnrollment_SCanQR.ClickOnContinueButton();
		DeviceEnrollment_SCanQR.SendingAccountID(Config.AccountId);
		DeviceEnrollment_SCanQR.ClickOnRegisterButton();
		DeviceEnrollment_SCanQR.SendingDeviceName(Config.DeviceName);
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("Nix Permission_Automation");
		androidJOB.ClickOnApplyJobOkButton();
		accountsettingspage.ReadingConsoleMessageForJobDeployment("Nix Permission_Automation",Config.DeviceName);
		deviceinfopanelpage.ClickOnDeviceInfoNixPermissionShowButton();
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Sms Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Camera Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Contacts Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Calllog Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Location Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Telephone Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Storage Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Apps With Usage Access");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterAllowingPermissionInNix("Ignore Battery Optimization");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterDenyingPermissionInNix("Device Administrator");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterDenyingPermissionInNix("Configure System Permission");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterDenyingPermissionInNix("Configure Unknown sources");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterDenyingPermissionInNix("Samsung KNOX");
		deviceinfopanelpage.VerifyPermissionstatusInConsoleAfterDenyingPermissionInNix("Draw Over Other Apps");
		deviceinfopanelpage.ClosingNixPermissionStatusInConsole();
	}




}

