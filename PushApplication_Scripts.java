package RightClick;
import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class PushApplication_Scripts extends Initialization {

	@Test(priority='0',description="1.To Verify Push Application Right Click On DeviceGrid Single app ")
	public void VerifyRightClickOperation() throws Throwable {
	//	rightclickDevicegrid.AppiumPart();
	//	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		Reporter.log("\n1.To Verify Push Application Right Click On DeviceGrid Single app ",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickonPushApplication();
		rightclickDevicegrid.UploadAPK(Config.Pushapk,Config.apkname);
		rightclickDevicegrid.ClickOnNextBtnPushApplicationPopup();
	    rightclickDevicegrid.VerifyingSuccessMsg();
	    rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	}
	
	@Test(priority='2',description="2.To Verify Push Application Right Click On DeviceGrid Multiple app ")
	public void VerifyPushApplicationMultipeApp() throws Throwable {
		Reporter.log("\n2.To Verify Push Application Right Click On DeviceGrid Multiple app ",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickonPushApplication();
		rightclickDevicegrid.UploadAPK(Config.Pushapk1,Config.apkname1);
		rightclickDevicegrid.UploadAPK(Config.Pushapk2,Config.apkname2);
		rightclickDevicegrid.ClickOnNextBtnPushApplicationPopup();
	    rightclickDevicegrid.VerifyingSuccessMsg();
	    rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	 //  rightclickDevicegrid.VerifySuccessfullyDeployedStatus();
	}
	
	@Test(priority='4',description="3.To Verify Adding Push Application with URL")
	public void VerifyAddingPushApplicationURL() throws Throwable {
		Reporter.log("\n3.To Verify Adding Push Application with URL",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickonPushApplication();
		rightclickDevicegrid.PushApplicationURL(Config.PushApp);
		rightclickDevicegrid.ClickOnNextBtnPushApplicationPopup();
	    rightclickDevicegrid.VerifyingSuccessMsg();
	    rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	}	
	@Test(priority='5',description="4.Verify Push Application from Jobs section")
	public void VerifyPushAppFromJobsSection_TC_DG_085() throws Throwable {
		Reporter.log("\n4.Verify Push Application from Jobs section",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickonPushApplication();
		rightclickDevicegrid.UploadAPK(Config.Pushapk3,Config.apkname3);
		rightclickDevicegrid.ClickOnNextBtnPushApplicationPopup();
	    rightclickDevicegrid.VerifyingSuccessMsg();
	    rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	    
	    androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PushApp_astrocontacts.apk"); 
		androidJOB.JobInitiatedOnDevice();
		accountsettingspage.ReadingConsoleMessageForJobDeployment("PushApp_astrocontacts.apk",Config.DeviceName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("astrocontacts.apk",Config.DeviceName);
		androidJOB.clickOnInstallBtnInDevice();
		androidJOB.CheckApplicationIsInstalledDeviceSide(Config.BundleID);
		androidJOB.unInstallMultipleApplication(Config.BundleID);
	}
	@Test(priority='6',description="5.Verify Push Application by exiting the dialogue box.")
	public void VerifyPushAppByExitingDialogBox_TC_DG_083() throws Throwable {
		Reporter.log("\n5.Verify Push Application by exiting the dialogue box.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.ClickonPushApplication();
		rightclickDevicegrid.UploadAPK(Config.Pushapk,Config.apkname);
		rightclickDevicegrid.ClickOnNextBtnPushApplicationPopup();
		 rightclickDevicegrid.VerifyingSuccessMsg();
		rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
		accountsettingspage.ReadingConsoleMessageForJobDeployment("PushApp_GPS Test.apk",Config.DeviceName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("GPS Test.apk",Config.DeviceName);
		androidJOB.CheckStatusOfappliedInstalledJob("PushApp_GPS Test.apk",360);
		androidJOB.clickOnInstallBtnInDevice();
		androidJOB.CheckApplicationIsInstalledDeviceSide(Config.GPSTestApp_BundleID);
		androidJOB.unInstallMultipleApplication(Config.GPSTestApp_BundleID);
		
	}
}
