package Settings_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class EULADisclaimerPolicy extends Initialization {
	
	@Test(priority = 1, description = "Verify Eula Disclaimer Policy in Account Settings")
	public void VerifyEULAdisclaimerOptions()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n1.Verify Eula Disclaimer Policy in Account Settings",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnEulaDisclaimer();
		accountsettingspage.verifyUseEulaDisclaimer();
		accountsettingspage.VerifytheDisclaimerPolicyText();
		accountsettingspage.VerifySaveButton();
	}

	@Test(priority = 2, description = "Verify Eula Disclaimer Policy in Account Settings by Leaving EULA Disclaimer Policy Text Empty")
	public void VerifyEULADisclaimerPolicyByLeavingBlankText()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n2.Verify Eula Disclaimer Policy in Account Settings by Leaving EULA Disclaimer Policy Text Empty",true);
		accountsettingspage.selectUseEulaDisclaimer();
		accountsettingspage.ClearDisclaimerPolicyText();
		accountsettingspage.ClickOnSave();
		accountsettingspage.verifyTheErrormessgewitoutEulaDisclaimerText();
	}

	@Test(priority = 3, description = "Verify Eula Disclaimer Policy in Account Settings by adding EULA Disclaimer Policy Text Empty")
	public void EULADisclaimerPolicyFunctionality()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n3.Verify Eula Disclaimer Policy in Account Settings by adding EULA Disclaimer Policy Text Empty",true);
		accountsettingspage.selectUseEulaDisclaimer();
		accountsettingspage.ClearDisclaimerPolicyText();
		accountsettingspage.entertheDisclaimerPolicyText();
		accountsettingspage.ClickOnSave();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickOnHomePage();

	}

	@Test(priority = 4, description = "Verify Eula Disclaimer Policy in Account Settings by modifying EULA Disclaimer Policy Text Empty")
	public void VerifyEULADisclaimerPolicyByModifyingText()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n4.Verify Eula Disclaimer Policy in Account Settings by modifying EULA Disclaimer Policy Text Empty",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnEulaDisclaimer();
		accountsettingspage.selectUseEulaDisclaimer();
		accountsettingspage.ModifyDisclaimerPolicyText();
		accountsettingspage.ClickOnSave();
		accountsettingspage.SuccessMessageAfterClickOnSaveButton();
		commonmethdpage.ClickOnHomePage();

	}

	@Test(priority = 5, description = "Verify Disabling Eula Disclaimer Policy in Account Settings")
	public void VerifyDisableEULADisclaimerPolicy()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n5.Verify Disabling Eula Disclaimer Policy in Account Settings",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnEulaDisclaimer();
		accountsettingspage.UnselectUseEulaDisclaimer();
		accountsettingspage.ClickOnSave();
		accountsettingspage.SuccessMessageAfterClickOnSaveButton();
		commonmethdpage.ClickOnHomePage();

	}
	
	//mithilesh
	@Test(priority = 6 , description = "Verify Logs when admin Enables EULA")
	public void VerifyLogswhenadminEnablesEULATC_ST_601()
	throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("\n6.Verify Logs when admin Enables EULA",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnEulaDisclaimer();
		accountsettingspage.EnabledEULA();
		accountsettingspage.ClickonEULASave();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.EULAEnabledActivityLog();	
	}
	
	@Test(priority = 7 , description = "Verify Logs when admin Disables EULA")
	public void VerifyLogswhenadminDisablesEULATC_ST_602()
	throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	Reporter.log("\n7.Verify Logs when admin Disables EULA",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnEulaDisclaimer();
		accountsettingspage.DisabledEULA();		
		accountsettingspage.ClickonEULASave();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.EULADisabledActivityLog();		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
