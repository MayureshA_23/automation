package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ComplianceJobOSVersion extends Initialization{


	@Test(priority=1, description="Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Send Message.")
	public void ComplianceJobOsVersionSendMessage() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("Send Message",1,"osVersion_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionSendMessage");
	//	androidJOB.enterJobName("ComplianceJobOsVersionSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobOsVersionSendMessage",100);
		Thread.sleep(200);
		androidJOB.verifyOutOfComplianceMsgOnDevice();
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
		Reporter.log("Pass >> Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Send Message.",true); }



	@Test(priority=2, description="Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Email Notification")
	public void ComplianceJobOsVersionEmailNotification() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("E-Mail Notification",1,"osVersion_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionEmailNotifications");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionEmailNotifications");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobOsVersionEmailNotifications",200);
		Thread.sleep(120);
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");
		Reporter.log("Pass >> Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Email Notification",true); }


	@Test(priority=3, description="Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Apply job.")
	public void ComplianceJobOsVersionApplyJob() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("JobCreatedForComplianceApplyJob","ComplianceJob","ComplianceJob");
		androidJOB.ClickOnForceReadMessage();
		androidJOB.ClickOnOkButton();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("Apply Job",1,"osVersion_BLK");
		androidJOB.addComplianceJob("osVersion_BLK");
		androidJOB.SearchFieldComplianceJob("JobCreatedForComplianceApplyJob");
		androidJOB.EnterJobNameTextField("JobOSVersionApplyJob");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("JobOSVersionApplyJob");
		androidJOB.CheckStatusOfappliedInstalledJob("JobOSVersionApplyJob",200);
		Thread.sleep(150);
		androidJOB.verifyComplianceApplyJob();
		Reporter.log("Pass >> Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Apply job.",true); }	

	@Test(priority=4, description="Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Move to blacklist")
	public void ComplianceJobOsVersionMovetoBlackList() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("Move to Blocklist",1,"osVersion_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionMovetoBlackList");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionMovetoBlackList");
		Thread.sleep(2000);
		blacklistedpage.ClickOnBlacklisted();
		blacklistedpage.ClickOnRefreshButton();
//		blacklistedpage.ClickOnRefreshButton();
        blacklistedpage.ClickOnDeviceToWhiteList();
		blacklistedpage.ClickOnWhitelistButton();
		blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
		blacklistedpage.ClickOnHomeGroup();
		Reporter.log("Pass >> Compliance job - Verify creating Compliance job with OS version and out of Compliance action as Move to blacklist",true); }

	@Test(priority=5, description="Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions")
	public void ComplianceJobOnlineDeviceSendMessage() throws Throwable{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnOnlineDeviceConnectivity();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnConfigureButton("conn_BLK");
		androidJOB.ComplianceDurationDeviceShouldBeoffLine("1");
		androidJOB.outOfComplianceActions("Send Message",2,"conn_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobDeviceConectivitySendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobDeviceConectivitySendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceConectivitySendMessage",60);
		androidJOB.makeDeviceOffForGivenTime(250);
		androidJOB.verifyOutOfComplianceMsgOnDevice();
		androidJOB.makeDeviceOnLine();
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
		Reporter.log("Pass >>Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions",true); }	


	@Test(priority=6, description="Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions")
	public void ComplianceJobOnlineDeviceEmailNotification() throws Throwable{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnOnlineDeviceConnectivity();
		androidJOB.ClickOnConfigureButton("conn_BLK");
		androidJOB.ComplianceDurationDeviceShouldBeoffLine("1");
		androidJOB.outOfComplianceActions("E-Mail Notification",2,"conn_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobDeviceConectivityEmailNotification");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobDeviceConectivityEmailNotification");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceConectivityEmailNotification",300);
		androidJOB.makeDeviceOffForGivenTime(180);
		androidJOB.makeDeviceOnLine();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		Thread.sleep(6);
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");
		Reporter.log("Pass >>Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions",true); }	

	@Test(priority=7, description="Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions")
	public void ComplianceJobDeviceConnectivityApplyJob() throws Throwable{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("JobCreatedForComplianceApplyJob","ComplianceJob","ComplianceJob");
		androidJOB.ClickOnForceReadMessage();
		androidJOB.ClickOnOkButton();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnOnlineDeviceConnectivity();
		androidJOB.ClickOnConfigureButton("conn_BLK");
		androidJOB.ComplianceDurationDeviceShouldBeoffLine("1");
		androidJOB.outOfComplianceActions("Apply Job",2,"conn_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobDeviceConectivityApplyJob");
		androidJOB.addComplianceJob("conn_BLK");
		androidJOB.SearchFieldComplianceJob("JobCreatedForComplianceApplyJob");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobDeviceConectivityApplyJob");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceConectivityApplyJob",400);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.makeDeviceOffForGivenTime(180);
		androidJOB.makeDeviceOnLine();
		Thread.sleep(200);
		androidJOB.verifyComplianceApplyJob();
		Reporter.log("Pass >>Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions",true); }	

	@Test(priority=8, description="Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions")
	public void ComplianceJobOnlineDeviceBlackList() throws Throwable{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.ClickOnOnlineDeviceConnectivity();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnConfigureButton("conn_BLK");
		androidJOB.ComplianceDurationDeviceShouldBeoffLine("1");
		androidJOB.outOfComplianceActions("Move to Blocklist",2,"conn_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobDeviceConectivityBlacklist");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobDeviceConectivityBlacklist");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceConectivityBlacklist",200);
		androidJOB.makeDeviceOffForGivenTime(200);
		androidJOB.makeDeviceOnLine();
		Thread.sleep(300);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		blacklistedpage.ClickOnBlacklisted();
		blacklistedpage.ClickOnRefreshButton();
		blacklistedpage.ClickOnDeviceToWhiteList();
		blacklistedpage.ClickOnWhitelistButton();
		blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
		blacklistedpage.ClickOnHomeGroup();
		Reporter.log("Pass >>Compliance Job - verify creating compliance job with Online Device connectivity and verify all out of Compliance actions",true); 
		}	

	@Test(priority=9, description="Compliance Job _ verify adding delay for out of compliance actions.")
	public void ComplianceDelayJobOsVersionSendMessage() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("Send Message",1,"osVersion_BLK");
		androidJOB.SelectDelayForCompliance("1","Minutes");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionSendMessageDelay");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionSendMessageDelay");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobOsVersionSendMessageDelay",200);
		androidJOB.complianceDelay(200);
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
		androidJOB.verifyOutOfComplianceMsgOnDevice();
		Reporter.log("Pass >>Compliance Job _ verify adding delay for out of compliance actions.",true); }

	@Test(priority='A', description="Compliance Job _ verify adding delay for out of compliance actions.")
	public void ComplianceDelayJobOsVersionMovetoBlackList() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("Move to Blocklist",1,"osVersion_BLK");
		androidJOB.SelectDelayForCompliance("1","Minutes");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionMovetoBlackListDelay");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionMovetoBlackListDelay");
		androidJOB.complianceDelay(200);
		Thread.sleep(300);
		blacklistedpage.ClickOnBlacklisted();
		blacklistedpage.ClickOnRefreshButton();
		blacklistedpage.ClickOnDeviceToWhiteList();
		blacklistedpage.ClickOnWhitelistButton();
		blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
		blacklistedpage.ClickOnHomeGroup();
		Reporter.log("Pass >>Compliance Job _ verify adding delay for out of compliance actions.",true); }




	@Test(priority='B', description="Compliance Job _ verify adding delay for out of compliance actions.")
	public void ComplianceDelayJobOsVersionApplyJob() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("JobCreatedForComplianceApplyJobdelay","ComplianceJob","ComplianceJob");
		androidJOB.ClickOnForceReadMessage();
		androidJOB.ClickOnOkButton();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("Apply Job",1,"osVersion_BLK");
		androidJOB.SelectDelayForCompliance("1","Minutes");
		androidJOB.addComplianceJob("osVersion_BLK");
		androidJOB.SearchFieldComplianceJob("JobCreatedForComplianceApplyJobDelay");
		androidJOB.EnterJobNameTextField("JobOSVersionComplianceApplyJobDelay");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("JobOSVersionComplianceApplyJobDelay");
		androidJOB.CheckStatusOfappliedInstalledJob("JobOSVersionComplianceApplyJobDelay",200);
		androidJOB.complianceDelay(250);
		androidJOB.verifyComplianceApplyJob();
		Reporter.log("Pass >>Compliance Job _ verify adding delay for out of compliance actions.",true);
		}
	@Test(priority='C', description="Compliance Job _ verify adding delay for out of compliance actions.")
	public void ComplianceDelayJobOsVersionEmailNotification() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.SelectAndriodVersionFirst("KitKat(4.4 - 4.4.4)");
		androidJOB.SelectAndriodVersionSecond("Lollipop(5.1)");
		androidJOB.outOfComplianceActions("E-Mail Notification",1,"osVersion_BLK");
		androidJOB.SelectDelayForCompliance("1","Minutes");
		androidJOB.EnterJobNameTextField("ComplianceJobOsVersionEmailNotificationsDelay");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobOsVersionEmailNotificationsDelay");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobOsVersionEmailNotificationsDelay",200);
		androidJOB.complianceDelay(200);  
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");
		Reporter.log("Pass >> Compliance Job _ verify adding delay for out of compliance actions.",true); }

	@Test(priority='D', description="Verify latest OS version for Android  is added in compliance job ")
	public void TC_JO_01() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");
		androidJOB.ComplianceAndroidOs_VersionFirst();
		androidJOB.ComplianceAndroidOs_VersionSecond();
		androidJOB.ClosecomplianceJob_container();


		
		
		
		
}
	//Dont Run
	
/*	@Test(priority='E', description="Compliance Job _ Verify \"Uninstall App\" out of compliance actions in compliance should be listed only for Blocklisted Applications.")
	public void VerifyOutOfComplianceActions_UninstallAppTC_JO_845() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{

		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.clickOnOsVersionCompliance();
		androidJOB.ClickOnConfigureButton("osVersion_BLK");

		androidJOB.outOfComplianceActions_UninstallApp("Uninstall App");   



		
		
		
		
}
*/


}
