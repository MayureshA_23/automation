package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class RunScriptSureLockAdminSettings extends Initialization {

	//Pre cond- Keep SL installed,Activated and launched In Device
	
	@Test(priority=1, description="Verify Runscript for \"Run script to navigate to SureLock Admin settings.\"")
	public void TC_JO_05() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

	 commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock","com.gears42.surelock.HomeScreen");


		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnGenericSection();
		runScript.ClickOnNavigateSurelockAdminSettings();
		runScript.ClickOnscriptmodalpopInput("0000");
		runScript.ClickOnValidateButton();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("SureLockAdminSettings");
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureLockAdminSettings");
		androidJOB.CheckStatusOfappliedInstalledJob("SureLockAdminSettings",100);
	    androidJOB.VerifyDeviceSideSureLockSettings();





	
	
	
	
	
	
	
	
	
}
}
