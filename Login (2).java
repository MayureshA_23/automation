package Login;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import PageObjectRepository.CommonMethods_POM;
import PageObjectRepository.LoginLogoutPOM;

public class Login {
	
	public static WebDriver driver;
	LoginLogoutPOM loginPage;
	CommonMethods_POM commonmethdpage;
	ExcelLib eLib;
	
	@BeforeSuite
	public void BeforeSuiteMtd() throws InterruptedException
	{
		driver = Driver.getBrowser();
		loginPage = PageFactory.initElements(driver, LoginLogoutPOM.class);
		//loginPage.loginToAPP(Config.userID, Config.password, Config.url);
		loginPage.loginToAPP(Config.url);
		loginPage.TrailMessageClick();
		
	}
	
	@AfterSuite
	public void AfterMtd()
	{
		driver.quit();
	}

}
