package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class GroupAssignmentRules  extends Initialization{

	@Test(priority=1,description="Verify the UI of SSID option in group assignment rule") 
	public void VerifytheUIofSSIDoptioningroupassignmentruleTC_ST_898() throws Throwable {
		Reporter.log("\n1.Verify the UI of SSID option in group assignment rule",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.ADDRulesPageVisible();
		accountsettingspage.SSIDDropDownOption();
		accountsettingspage.CloseADDRuleTab();		
}
	
	//add a device in group SourceGroup001 with SSID_Name
	@Test(priority=2,description="Create group assignment rule with single SSID ") 
	public void CreategroupassignmentrulewithsingleSSIDTC_ST_899() throws Throwable {
		Reporter.log("\n2.Create group assignment rule with single SSID ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.FillSSIDRule();
}	
	
	@Test(priority=3,description="Create group assignment rule with multiple SSID and multiple conditions") 
	public void CreategroupassignmentrulewithmultipleSSIDandmultipleconditionsTC_ST_900() throws Throwable {
		Reporter.log("\n3.Create group assignment rule with multiple SSID and multiple conditions",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.MultipleSSIDRules();		
}	
	
	@Test(priority=4,description="Verify group assignment rules with SSID and Device Model number") 
	public void VerifygroupassignmentruleswithSSIDandDeviceModelnumberTC_ST_901() throws Throwable {
		Reporter.log("\n4.Verify group assignment rules with SSID and Device Model number",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.SSIDDeviceModel();		
}	
	
	@Test(priority=5,description="Verify Group assignment rules with SSID and IP address global and local") 
	public void VerifyGroupassignmentruleswithSSIDandIPaddressglobalandlocalTC_ST_902() throws Throwable {
		Reporter.log("\n5.Verify Group assignment rules with SSID and IP address global and local",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.SSIDIPaddress();
}	
	
	@Test(priority=6,description="Verify by adding multiple group assignment rule with SSID") 
	public void VerifybyaddingmultiplegroupassignmentrulewithSSIDTC_ST_903() throws Throwable {
		Reporter.log("\n6.Verify by adding multiple group assignment rule with SSID",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.SSIDEqualSSIDValue();
}	
	
	@Test(priority=7,description="Verify the logs for Creating rules ") 
	public void VerifythelogsforCreatingrulesTC_ST_905() throws Throwable {
		Reporter.log("\n7.Verify the logs for Creating rules ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.SSIDSingle();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.AcitvityLogInformation();		
}	
		
	//check weather Edit Rule is already present in the Group Assignment Rule list 
	@Test(priority=8,description="Verify Editing existing group assignment rule") 
	public void VerifyEditingexistinggroupassignmentruleTC_ST_907() throws Throwable {
		Reporter.log("\n8.Verify Editing existing group assignment rule",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
	    accountsettingspage.EnableGroupAssignmentRule();
	    accountsettingspage.ClickonADDRulesbutton();	    
		accountsettingspage.EditRule();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.RulesModifiedLogInfo();			
}	
	
	@Test(priority=9,description="Verify Logs when user Deletes a Group Assignment Rule") 
	public void VerifyLogswhenuserDeletesaGroupAssignmentRuleTC_ST_617() throws Throwable {
		Reporter.log("\n9.Verify Logs when user Deletes a Group Assignment Rule",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.DeleteNewRule();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.GroupDeletedActivityLog();			
}	
	
	
	@Test(priority=10,description="Verify Logs when user Disables Group Assignment Rule") 
	public void VerifyLogswhenuserDisablesGroupAssignmentRuleTC_ST_618() throws Throwable {
		Reporter.log("\n10.Verify Logs when user Disables Group Assignment Rule",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.DisabledGroupRule();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DisabledActivityLog();
}
	
	@Test(priority=11,description="Verify group assignment rules by adding more than 10 groups in source group") 
	public void Verifygroupassignmentrulesbyaddingmorethan10groupsinsourcegroupTC_ST_599() throws Throwable {
		Reporter.log("\n11.Verify group assignment rules by adding more than 10 groups in source group",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.MultipleSource();
		accountsettingspage.ADDIPAddress();		
}	
	
	//check weather device name AutomationDevice_A51 is present in SourceGroup001
	@Test(priority=12,description="Verify the logs while the device moves from one group to another ") 
	public void VerifythelogswhilethedevicemovesfromonegrouptoanotherTC_ST_906() throws Throwable {
		Reporter.log("\n12.Verify the logs while the device moves from one group to another ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnGroupAssigmentRule();
		accountsettingspage.EnableGroupAssignmentRule();
		accountsettingspage.ClickonADDRulesbutton();
		accountsettingspage.SSIDDeviceMove();		
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.AcitvityLogInformationSSID();		
}	
	
	
	
	
	
	
	
	
	
	
}
