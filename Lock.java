package DynamicJobs_TestScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Utility;

public class Lock extends Initialization{

	//Keep pwd as six zeroes
	@Test(priority='1',description="1.To Verify Dynamic Lock Job pop UI and functionality by Clicking On No button")
	public void DynamicLockJob_TC1() throws InterruptedException, Throwable{
		Reporter.log("\n1.To Verify Dynamic Lock Job pop UI and functionality by Clicking On No button",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnLockDevice();
	    dynamicjobspage.VerifyOfLockDevicePopup();
	    dynamicjobspage.ClickOnLockDeviceNoBtn();
	    dynamicjobspage.ConfirmationMessageLockDeviceVerify_False();
		Reporter.log("PASS>>To Verify Dynamic Lock Job pop UI and functionality by Clicking On No button",true);
	}
	
	@Test(priority='2',description="2.To Verify Dynamic Lock Job pop up UI and functionality by clicking on YES Button and validate Unlock")
	public void DynamicLockJob_TC_DJ_03() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Dynamic Lock Job pop up UI and functionality by clicking on YES Button and validate Unlock",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDeviceInconsole();
        dynamicjobspage.ClickOnMoreOption();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
	    dynamicjobspage.ClickOnLockDevice();
	    dynamicjobspage.VerifyOfLockDevicePopup();
	    dynamicjobspage.ClickOnLockDeviceYesBtn();
	    dynamicjobspage.ConfirmationMessageLockDeviceVerify();
        dynamicjobspage.UnlockDeviceAndVAlidate();
        androidJOB.ClickOnHomeButtonDeviceSide();
 //      dynamicjobspage.LaunchContactsSystemApp();
	    dynamicjobspage.VerifyOfLockActivityLogs();
	    Reporter.log("PASS>> To Verify Dynamic Lock Job pop up UI and functionality by clicking on YES Button and validate Unlock",true);
	    Reporter.log("PASS>>Unlocked the device and successfully launched Contact Application",true);
	}
	
	@AfterMethod
	public void aftermethod(ITestResult result) throws InterruptedException, Throwable
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			Utility.captureScreenshot(driver, result.getName());
			commonmethdpage.refreshpage();
			commonmethdpage.SearchDevice();
		}
	}
}
