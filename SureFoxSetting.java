package JobsOnAndroid;

import org.testng.annotations.Test;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;

import Common.Initialization;
import Library.Config;

public class SureFoxSetting extends Initialization {

	@Test(priority=1,description="SureFox Settings job - verify creating SureFox settings job.")
	public void VerifyModifydeployInstall() throws InterruptedException, IOException{
		
		androidJOB.LaunchSureFox();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureFoxSettings();
		androidJOB.verifySureFoxPrompt();
		Reporter.log("Pass >>SureFox Settings job - verify creating SureFox settings job.",true); }
	
	@Test(priority=2,description="SureFox Settings job - verify creating and edit SureFox settings.")
	public void SureFoxAddWebsites() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnsurefoxAllowedWebsite();
		androidJOB.clickOnsurefoxAddURL();
		androidJOB.EnterWebSite("www.google.com/","Google");
		androidJOB.clickOnSureFoxAllowWebsitesDone();
		androidJOB.clickOnsurefoxAddURL();
		androidJOB.EnterWebSite("www.youtube.com/","YouTube");
		androidJOB.clickOnSureFoxAllowWebsitesDone();
		androidJOB.clickOnDoneAddingAllowWebsites();
		androidJOB.aboutSureFox();
		androidJOB.clickOnSureFoxBrowserPreferences();
		androidJOB.clickOnJavaScriptCheckBox();
		androidJOB.clickOnDoneSureFoxPreferences();
		androidJOB.clickOnsaveSureFoxSetting();
		androidJOB.SureFoxSettingName("SureFoxCreatingSettings");
		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox","com.gears42.surefox.SurefoxBrowserScreen");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureFoxCreatingSettings");
		androidJOB.CheckStatusOfappliedInstalledJob("SureFoxCreatingSettings",200);
		androidJOB.verifyAppsOnSureFox("Google,YouTube");
		androidJOB.SureFoxAdminDeviceSide();
		androidJOB.clickOnSureFoxSetting();
		androidJOB.verifyJavaScript("false");
		Reporter.log("Pass >>SureFox Settings job - verify creating and edit SureFox settings.",true); }
	
	@Test(priority=3,description="SureFox Settings job - verify modifying and editing surefox settings.")
	public void SureFoxModifying() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("SureFoxCreatingSettings"); 
		androidJOB.ClickOnOnJob("SureFoxCreatingSettings");
		androidJOB.clickOnModifyOption(); 
		androidJOB.clickOnSureFoxBrowserPreferences();
		androidJOB.clickOnJavaScriptCheckBox();
		androidJOB.clickOnDoneSureFoxPreferences();
		androidJOB.clickOnsaveSureFoxSetting();
		androidJOB.SureFoxSettingName("SureFoxCreatingSettingsModified");
		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox","com.gears42.surefox.SurefoxBrowserScreen");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureFoxCreatingSettingsModified");
		androidJOB.CheckStatusOfappliedInstalledJob("SureFoxCreatingSettingsModified",200);
		androidJOB.verifyAppsOnSureFox("Google,YouTube");
		androidJOB.SureFoxAdminDeviceSide();
		androidJOB.clickOnSureFoxSetting();
		androidJOB.verifyJavaScript("true");
		Reporter.log("Pass >>SureFox Settings job - verify modifying and editing surefox settings.",true); }
	
	@Test(priority=4,description="SureFox Settings job - Verify SureFox Settings job with Save AS File.")
	public void SureFoxSaveAsFile() throws InterruptedException, IOException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureFoxSettings();
		androidJOB.clickOnSaveAsFile();
		androidJOB.isSureLockFileDownloaded(Config.FileDownloaded,"surefox");
		Reporter.log("Pass >>SureFox Settings job - Verify SureFox Settings job with Save AS File.",true); }
	
	@Test(priority=5,description="SureFox Settings Job - Verify SureFox settings job with Edit XML Option.")
	public void SureFoxEditXml() throws Throwable{
		androidJOB.clickOnEditXML();
		androidJOB.ClearEditXMLTextArea();
		androidJOB.EnterEditXMLTextArea("./Uploads/UplaodFiles/SureLock Settings/\\EditXMLOptionSureFox.txt");
		androidJOB.ClickOnEditXMLSaveBtn();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("SureFoxEditXML");
		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox","com.gears42.surefox.SurefoxBrowserScreen");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureFoxEditXML");
		androidJOB.CheckStatusOfappliedInstalledJob("SureFoxEditXML",200);
		androidJOB.SureFoxAdminDeviceSide();
		androidJOB.clickOnSureFoxSetting();
		androidJOB.verifyPluginSupport();
		Reporter.log("Pass >>SureFox Settings job - Verify SureFox Settings job with Save AS File.",true); }
	
	@Test(priority=6,description="SureFox Settings Job - Verify SureFox Settings job with Advanced option.")
	public void SureFoxAdvanceOption() throws Throwable{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureFoxSettings();
		androidJOB.clickOnAdvanceOption();
		androidJOB.enableAdvanceOption();
		androidJOB.EnterActivationodeSureFox();
		androidJOB.ClickOnSaveEnableAdvncSureLckFox();
		androidJOB.clickOnSureFoxBrowserPreferences();
		androidJOB.ClickOnBackBtnBrwsngCheckBox();
		androidJOB.clickOnDoneSureFoxPreferences();
		androidJOB.clickOnsaveSureFoxSetting();
		androidJOB.SureFoxSettingName("SureFoxAdvaceoption");
		commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox","com.gears42.surefox.SurefoxBrowserScreen");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("SureFoxAdvaceoption");
		androidJOB.CheckStatusOfappliedInstalledJob("SureFoxAdvaceoption",200);
		androidJOB.SureFoxAdminDeviceSide();
		androidJOB.clickOnSureFoxSetting();
		androidJOB.verifyBackButtonBrowisng("false");
		androidJOB.unInstallSureFox();

		Reporter.log("Pass >>SureFox Settings Job - Verify SureFox Settings job with Advanced option.",true); }
	
	
	
	@Test(priority=7,description="Verify by modifying SureFox Static Job")
	public void VerifyModifyingSureFoxStaticJob() throws Throwable{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnSureFoxSettings();
		androidJOB.clickOnsurefoxAllowedWebsite();
		androidJOB.clickOnsurefoxAddURL();
		androidJOB.EnterWebSite(" https://3575084.app.netsuite.com/app/login/secure/enterpriselogin.nl?c=3575084&redirect=%2Fapp%2Fsite%2Fhosting%2Fscriptlet.nl%3Fscript%3D396%26deploy%3D1%26compid%3D3575084%26whence%3D&whence=","URL");
		androidJOB.clickOnSureFoxAllowWebsitesDone();
		androidJOB.clickOnDoneAddingAllowWebsites();
		androidJOB.clickOnsaveSureFoxSetting();
		androidJOB.SureFoxSettingName("SureFoxCreatingSettings");
		dynamicjobspage.SearchJobInSearchField("SureFoxCreatingSettings"); 
		androidJOB.ClickOnOnJob("SureFoxCreatingSettings");
		androidJOB.clickOnModifyOption(); 
		androidJOB.VerifySurefoxSettingsJobModifiySuccessFully();
		androidJOB.clickOnDoneAddingAllowWebsites();
		androidJOB.clickOnsaveSureFoxSetting();
		androidJOB.SureFoxSettingName("SureFoxCreatingSettings");





	
	
	
	
	
	
	}
	
	
	
	
	
	
	
	
}
