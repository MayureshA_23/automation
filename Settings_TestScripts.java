package NewUI_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class Settings_TestScripts  extends Initialization{
	
	@Test(priority=0,description="Verify Selecting all permissions in Roles .")
	public void SelectingAllPermissionInRoles() throws InterruptedException
	{Reporter.log("=====1.VerifySelecting all permissions in Roles .=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnRolesOption();
		newUIScriptsPage.DeletingExistingRole("Demo567");
		newUIScriptsPage.RoleWithAllPermissions("Demo567","Demo567");
		newUIScriptsPage.SearchRole("Demo567");
		newUIScriptsPage.ValidatingCreatedRole("Demo567");
	}

}
