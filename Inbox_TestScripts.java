package NewUI_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Inbox_TestScripts extends Initialization{
	@Test(priority=0,description="Inbox-Verify delete message by confirming yes")
	public void DeleteMessage() throws InterruptedException, IOException
	{
		Reporter.log("=====1.Inbox-Verify delete message by confirming yes=====",true);
		//newUIScriptsPage.ClickOnTryNewConsole();
		//inboxpage.ClickOnInbox();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		//commonmethdpage.clickOnInboxEditBtn();
		commonmethdpage.enterSubInComposeMessage(1,"DeleteMail", "test");
		//commonmethdpage.clicOnSendBtnInMailBox();
		inboxpage.SearchMailInInbox("DeleteMail");
		newUIScriptsPage.ClickOnDeleteButton(); 
		inboxpage.ClickOnYesDeleteMessage();	
		inboxpage.SearchMailInInbox("DeleteMail");
		inboxpage.VerifyOnDeletedMail("DeleteMail");
	}
	/*@Test(priority=1,description="Inbox-Verify reply message for get read notification")
	public void ReplyMsgForGetReadNotification() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("=====2.Inbox-Verify reply message for get read notification=====",true);
		newUIScriptsPage.ClickOnTryNewConsole();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		commonmethdpage.clickOnInboxEditBtn();
		commonmethdpage.enterSubInComposeMessage("Automation TestMail", "test");
		commonmethdpage.clicOnSendBtnInMailBox();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.Device_Name);
		inboxpage.ClickOnInbox();
		inboxpage.SearchMailInInbox("Automation TestMail");
		inboxpage.isTheFirstMessageSelectedByDefault();
		inboxpage.ClickOnMailReplyButton();
		inboxpage.ClickOnSendButton();
		inboxpage.warningWhenNoMessageBody();
		inboxpage.verifyRichTextFields();
		inboxpage.VerifyVersionSupportMessage();
		inboxpage.clikOnPlainText();
		inboxpage.EnterMessageBody();
		inboxpage.GetReadNotificaiton();
		inboxpage.ClickOnSendButton();
		inboxpage.NotificationMessageSent();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
	    androidJOB.ClickOnMailBoxOfNix();
		inboxpage.clickOnTextMessageInMailBoxReadNotify("Automation TestMail","test");
		
	}
*/
}
