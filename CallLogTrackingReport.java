package OnDemandReports_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import java.text.ParseException;

import Common.Initialization;
import Library.Config;
import Library.Utility;

//Enable call logs 
//need d\
public class CallLogTrackingReport extends Initialization {
	@Test(priority = 1, description = "1.Verify Call Log Tracking report for 'Home' group for 'Outgoing' Call")
	public void GenerateCallLogTrackingReportHomeGroup_Outgoing_TC_RE_38() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("1.GenerateCallLogTrackingReportHomeGroup_Outgoing", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();		
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyOfCallLogTrackingReportDeviceName(Config.DeviceName);
		reportsPage.SearchDeviceInReportsView("Outgoing");
		reportsPage.VerifyOfCallType("Outgoing");
		reportsPage.VarifyingCurrentDateInCallLogTrackingOnDemandReport();
		customReports.Varifycallduration();
		reportsPage.SearchDeviceInReportsView(Config.EnterContactNumber);
		reportsPage.VerifyOfContactNumber(Config.EnterContactNumber);
		reportsPage.SearchDeviceInReportsView(Config.SendContactName);
		reportsPage.VerifyOfContactName(Config.SendContactName);
		reportsPage.SwitchBackWindow();
		reportsPage.ClickOnOnDemandReport();
	}
	
	@Test(priority = 2, description = "2.Verify Call Log Tracking report for 'Sub' group for 'Outgoing' Call")
	public void GenerateCallLogTrackingReportSubGroup_Outgoing() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n2.Verify Call Log Tracking report for 'Sub' group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		reportsPage.clickOnSelectGroupInODRep();
		reportsPage.selectGrpAndAdd(Config.GroupName_Reports);
		accountsettingspage.ClickONRequestReportButton();
		
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();	
		reportsPage.ClickOnView();
		
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyOfCallLogTrackingReportDeviceName(Config.DeviceName);
		reportsPage.SearchDeviceInReportsView("Outgoing");
		reportsPage.VerifyOfCallType("Outgoing");
		reportsPage.VarifyingCurrentDateInCallLogTrackingOnDemandReport();
		customReports.Varifycallduration();
		reportsPage.SearchDeviceInReportsView(Config.EnterContactNumber);
		reportsPage.VerifyOfContactNumber(Config.EnterContactNumber);
		reportsPage.SearchDeviceInReportsView(Config.SendContactName);
		reportsPage.VerifyOfContactName(Config.SendContactName);
		reportsPage.SwitchBackWindow();
		reportsPage.ClickOnOnDemandReport();
	}

	@Test(priority = 3, description = "3.Verify Call Log Tracking report for 'Home' group for 'Incoming' Call")
	public void GenerateCallLogTrackingReportHomeGroup_Incoming_TC_RE_38() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n3.Verify Call Log Tracking report for 'Home' group for 'Incoming' Call", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		accountsettingspage.ClickONRequestReportButton();
		
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyOfCallLogTrackingReportDeviceName(Config.DeviceName);
		reportsPage.SearchDeviceInReportsView("Incoming");
		reportsPage.VerifyOfCallType("Incoming");
		reportsPage.VarifyingCurrentDateInCallLogTrackingOnDemandReport();
		customReports.Varifycallduration();
		reportsPage.SearchDeviceInReportsView(Config.EnterContactNumber);
		reportsPage.VerifyOfContactNumber(Config.EnterContactNumber);
		reportsPage.SearchDeviceInReportsView(Config.SendContactName);
		reportsPage.VerifyOfContactName(Config.SendContactName);
		reportsPage.SwitchBackWindow();
		reportsPage.ClickOnOnDemandReport();
	}
	
	@Test(priority = 4, description = "4.Verify Call Log Tracking report for 'Sub' group for Incoming calls")
	public void GenerateCallLogTrackingReportSubGroup_Incoming() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n4.Verify Call Log Tracking report for 'Sub' group for Incoming calls", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		reportsPage.clickOnSelectGroupInODRep();
		reportsPage.selectGrpAndAdd(Config.GroupName_Reports);
		accountsettingspage.ClickONRequestReportButton();
		
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyOfCallLogTrackingReportDeviceName(Config.DeviceName);
		reportsPage.SearchDeviceInReportsView("Incoming");
		reportsPage.VerifyOfCallType("Incoming");
		reportsPage.VarifyingCurrentDateInCallLogTrackingOnDemandReport();
		customReports.Varifycallduration();
		reportsPage.SearchDeviceInReportsView(Config.EnterContactNumber);
		reportsPage.VerifyOfContactNumber(Config.EnterContactNumber);
		reportsPage.SearchDeviceInReportsView(Config.SendContactName);
		reportsPage.VerifyOfContactName(Config.SendContactName);
		reportsPage.SwitchBackWindow();
		reportsPage.ClickOnOnDemandReport();
	}

	@Test(priority = 5, description = "5.Verify Call Log Tracking report for 'Home' group")
	public void GenerateCallLogTrackingReportHomeGroup_Missed_TC_RE_38() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n5.Verify Call Log Tracking report for 'Home' group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyOfCallLogTrackingReportDeviceName(Config.DeviceName);
		reportsPage.SearchDeviceInReportsView("Missed");
		reportsPage.VerifyOfCallType("Missed");
		reportsPage.VarifyingCurrentDateInCallLogTrackingOnDemandReport();
		customReports.Varifycallduration();
		reportsPage.SearchDeviceInReportsView(Config.EnterContactNumber);
		reportsPage.VerifyOfContactNumber(Config.EnterContactNumber);
		reportsPage.SearchDeviceInReportsView(Config.SendContactName);
		reportsPage.VerifyOfContactName(Config.SendContactName);
		reportsPage.SwitchBackWindow();
		reportsPage.ClickOnOnDemandReport();
	}

	@Test(priority = 6, description = "6.Verify Call Log Tracking report for 'Home' group")
	public void GenerateCallLogTrackingReportHomeGroup_Rejected_TC_RE_38() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n6.Verify Call Log Tracking report for 'Home' grou", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.VerifyOfCallLogTrackingReportDeviceName(Config.DeviceName);
		reportsPage.SearchDeviceInReportsView("Rejected");
		reportsPage.VerifyOfCallType("Rejected");
		reportsPage.VarifyingCurrentDateInCallLogTrackingOnDemandReport();
		customReports.Varifycallduration();
		reportsPage.SearchDeviceInReportsView(Config.EnterContactNumber);
		reportsPage.VerifyOfContactNumber(Config.EnterContactNumber);
		reportsPage.SearchDeviceInReportsView(Config.SendContactName);
		reportsPage.VerifyOfContactName(Config.SendContactName);
		reportsPage.SwitchBackWindow();
		reportsPage.ClickOnOnDemandReport();
	}

	@Test(priority = 7, description = "7.Generate And View the Incoming call log report for 'Last 30 Days '")
	public void GenerateCallLogTrackingReportHomeGroup_TC_RE_39() throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException,ParseException{
		Reporter.log("\n7.Generate And View the Incoming call log report for 'Last 30 Days '", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		reportsPage.clickOnSelectGroupInODRep();
		reportsPage.selectGrpAndAdd(Config.GroupName_Reports);
		reportsPage.SelectCallTypeInOnDemandReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifyOfCallType("Incoming");
		reportsPage.verifySelectedDateForLast30DaysInCallLogTrackingODRep();
		reportsPage.SwitchBackWindow();
		reportsPage.ClickOnOnDemandReport();
	}
	@Test(priority = 8, description = "8.Download for Call Log Tracking report for 'Home' Group")
	public void VerifyDownloadingDataUsageReportHomeGroup() throws InterruptedException {
		Reporter.log("\n8.Download for Call Log Tracking report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
	    accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}
	
	@Test(priority = 9, description = "9.Download for Call Log Tracking report for 'Sub' Group")
	public void VerifyDownloadingDataUsageReportSubGroup() throws InterruptedException {
		Reporter.log("\n9.Download for Call Log Tracking report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnCallLogTrackingReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
	    accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}
	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();

		}
	}
}
