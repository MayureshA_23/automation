package PageObjectRepository;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.Helper;
import Library.WebDriverCommonLib;

public class InboxPOM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	Actions act=new Actions(Initialization.driver);
	
	@FindBy(id="mailSection")
	private WebElement inbox;
	
	@FindBy(id="mailDeleteButton")
	private WebElement deleteButton;
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[1]")
	private WebElement alertMessageOnMessageDelete;
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement yes_button;
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement no_button;
	
	@FindBy(xpath="//span[text()='Message deleted successfully.']")
	private WebElement isMessageDeletedToast;
	
	@FindBy(id="mailReplyButton")
	private WebElement MailReplyButton;
	
	@FindBy(id="mailDeleteButton")
	private WebElement MailDeleteButton;
	
	@FindBy(xpath="//span[text()='Please select a message.']")
	private WebElement WarningMessageOnDeleteWhenNoMessageSelected;
	
	@FindBy(xpath=".//*[@id='mailGrid']/tbody/tr/td[2]")
	private WebElement MessageRows;
	
	@FindBy(xpath=".//*[@id='mailGrid']/tbody/tr[1]/td[1]/span/input")
	private WebElement FirstMessageCheckBox;
	
	@FindBy(xpath=".//*[@id='mailGrid']/tbody/tr[3]/td[1]/span/input")
	private WebElement ThirdMessageCheckBox;
	
	@FindBy(xpath="//h4[text()='Send Message']")
	private WebElement TextMessageWindow;
	
	@FindBy(id="deviceMessageBody")
	private WebElement messageBody;
	
	@FindBy(id="deviceMessageReadNotification")
	private WebElement getReadNotification;
	
	@FindBy(id="deviceMessageForceRead")
	private WebElement ForceReadMessage;
	
	@FindBy(xpath=".//*[@id='message_modal']/div/div/div[3]/input")
    private WebElement sendButton;
	
	@FindBy(xpath="//span[contains(text(),'Message sent to device successfully.')]")
    private WebElement NotificationSendMessage;
	
	@FindBy(xpath="//span[text()='Cannot send message without body.']")
	private WebElement WarningMessageWhenNoMessageBody;
	
	@FindBy(id="mailMarkAsUnReadBtn")
	private WebElement markAsUnread;
	
	@FindBy(xpath="//p[text()='Do you want to mark the message as unread?']")
	private WebElement warningMessageWhenMarkAsUnread;
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement Cancel_MarkAsUnread;
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YES_MarkAsUnread;

	@FindBy(xpath="//span[text()='Message marked as unread successfully.']")
	private WebElement confirmiationNotificaiotnAfterMarkAsUnread;
	
	@FindBy(xpath="//span[text()='Please select a message.']")
	private WebElement warningOnDeleteMessageWhenNoMessageisSelected;
	
	@FindBy(id="settPopupBtn")
	private WebElement CleanupInbox;
	
	@FindBy(xpath="//h4[text()='Cleanup Inbox Options']")
	private WebElement CleanupInboxOptionsWindow;
	
	@FindBy(xpath="//span[text()='Delete messages older than']")
	private WebElement isDeleteMessageOlderThanPresent;
	
	@FindBy(xpath=".//*[@id='mail_main_pg']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement MailSearchBox;
	
	@FindBy(id="messg_textarea")
	private WebElement messageTextArea;
	
	@FindBy(xpath="(//button/span[@class='page-size'])[2]")
	private WebElement PagesizeButton;
	
	@FindBy(id="mailMarkAsReadBtn")
	private WebElement MarkasRead;
	
	@FindBy(id="mailMarkAsUnReadBtn")
	private WebElement MarkAsUnread;
	
	
			
	WebDriver driver;
	public void ClickOnDeleteButton() throws InterruptedException
	{
		deleteButton.click();
		sleep(2);
	}
	
	public void VerifySingleMessageDeleteWarningMessageOnThePopUp() throws InterruptedException{
		deleteButton.click();
		sleep(2);
		//verifying the warning message on the pop up
		String Actualvalue = alertMessageOnMessageDelete.getText();	
		String Expectedvalue = "This message will be deleted. Do you wish to continue?";
		String PassStatement = "PASS >>'Selected message will be deleted. Do you wish to continue?- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in delete window or Failed to open delete button window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		
		
		boolean displayed;
		
		try {
			yes_button.isDisplayed();
			displayed = true;
		} catch (Throwable T) 
		{
			displayed = false;
			
		}
		String PassStatement1 = "PASS >> Warning message has YES button";
		String FailStatement1 = "FAIL >> Warning message does not have yes";
		ALib.AssertTrueMethod(displayed, PassStatement1, FailStatement1);
		sleep(3);
		
	}
	
	public void VerifyMultipleMessagesDeleteWarningMessageOnThePopUp() throws InterruptedException{
		deleteButton.click();
		sleep(2);
		//verifying multiple message delete warning message on the pop up
		String Actualvalue = alertMessageOnMessageDelete.getText();	
		String Expectedvalue = "All selected messages will be deleted. Do you wish to continue?";
		String PassStatement = "PASS >>'Selected message will be deleted. Do you wish to continue?- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in delete window or Failed to open delete button window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}
	
	//Navigate to In box
	public void ClickOnInbox() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, inbox);
		
		inbox.click();
		waitForidPresent("mailDeleteButton");
		sleep(2);

	boolean displayed;
	
	try {
		deleteButton.isDisplayed();
		displayed = true;
	} catch (Throwable T) 
	{
		displayed = false;
		
	}
	String PassStatement = "PASS >> Navigated to inbox page successfully";
	String FailStatement = "FAIL >> Navigation to inbox page failed";
	ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
	
	}
	
	public void DeselctingCheckBox() throws InterruptedException
	{
		FirstMessageCheckBox.click();
		sleep(2);
	}
	
public void ClickOnInboxActions() throws InterruptedException {
		
		markAsUnread.click();
		sleep(2);
		String Actualvalue = AccessDEniedMoveToFolder.getText();	
		String Expectedvalue = "Access denied. Please contact your administrator.";
		String PassStatement = "PASS >>'"+markAsUnread+"'error Message displayed";
		String FailStatement = "FAIL >> '"+markAsUnread+"'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
	   sleep(4);
		
		MailDeleteButton.click();
        sleep(2);
		String Actualvalue1 = AccessDEniedMoveToFolder.getText();	
		String Expectedvalue1 = "Access denied. Please contact your administrator.";
		String PassStatement1 = "PASS >>'"+MailDeleteButton+"'error Message displayed'";
		String FailStatement1 = "FAIL >> '"+MailDeleteButton+"'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
	    sleep(4);
		
		CleanupInbox.click();
		sleep(2);
		String Actualvalue2 = AccessDEniedMoveToFolder.getText();	
		String Expectedvalue2 = "Access denied. Please contact your administrator.";
		String PassStatement2 = "PASS >>'"+CleanupInbox+"'error Message displayed'";
		String FailStatement2 = "FAIL >>'"+CleanupInbox+"'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue2, Actualvalue2, PassStatement2, FailStatement2);
	    sleep(4);

		MailReplyButton.click();
        sleep(2);
		String Actualvalue3 = AccessDEniedMoveToFolder.getText();	
		String Expectedvalue3 = "Access denied. Please contact your administrator.";
		String PassStatement3 = "PASS >>'"+MailReplyButton+"'error Message displayed'";
		String FailStatement3 = "FAIL >>'"+MailReplyButton+"'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue3, Actualvalue3, PassStatement3, FailStatement3);
	    sleep(4);
	}
	
	//Click NO 
	public void ClickOnNoButonOnDeleteWarningPopUp() throws InterruptedException{
		no_button.click();
		boolean displayed;
		
		try {
			deleteButton.isDisplayed();
			displayed = true;
		} catch (Throwable T) 
		{
			displayed = false;
			
		}
		String PassStatement = "PASS >> Clicked on NO and Pop up dismissed";
		String FailStatement = "FAIL >> Delete warning message unable to click NO";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
	    sleep(2);
		
		}
	
	//Click YES
	public void ClickOnYesDeleteMessage() throws InterruptedException{
		yes_button.click();
		sleep(2);
		
		boolean displayed;
		try {
			
			isMessageDeletedToast.isDisplayed();
			displayed = true;
		} catch (Exception e) {
			displayed = false;
		}
		String PassStatement = "PASS >> Mail deleted message displayed-- Message delete Successfully";
		String FailStatement = "FAIL >> No warning Message or Unable to delete Message";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(2);
		
	}
		
	public void ClickOnMailReplyButton_UM() throws InterruptedException
	{
		Helper.highLightElement(Initialization.driver, MailReplyButton);
		
		MailReplyButton.click();
		sleep(3);
	}
	
	public void ClickOnMailDeleteButton_UM()
	{
		MailDeleteButton.click();
	}
	
	public void ClickOnMailReplyButton() throws InterruptedException
    {
            MailReplyButton.click();
            sleep(3);
            waitForidPresent("deviceMessageSubject");
            sleep(1);
            boolean isDisplayed= TextMessageWindow.isDisplayed();
            String pass = "PASS >> Text message window is displayed- Clicked on Reply button";
            String fail = "FAIL >> text message window is NOT displayed - NOT clicked on Reply button";
            ALib.AssertTrueMethod(isDisplayed, pass, fail);
            sleep(3);

    }
	
	public void ClickOnMailDeleteButton() throws InterruptedException
	{
		MailDeleteButton.click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div[2]/button[1]");
		sleep(2);
	}
	
	public void ClickOnDeleteMessageNoBtn() throws InterruptedException
	{
		no_button.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(1);
	}
	
	public void isTheFirstMessageSelectedByDefault()
	{
		Helper.highLightElement(Initialization.driver, FirstMessageCheckBox);
		
		boolean value = FirstMessageCheckBox.isEnabled();
		String pass = "PASS >>Enabled -  first message is selected by default";
		String fail = "FAIL >> Not Enabled - first messge is NOT selected by default";
		ALib.AssertTrueMethod(value, pass, fail);
	}
	
	//unable to handle this - reply button always return true though it's disabled
	public boolean UncheckFirstMessage() throws InterruptedException
	{
		FirstMessageCheckBox.click();
		sleep(4);
		boolean val = MailReplyButton.isEnabled();
		System.out.println(val);
		return val;
	}
	public void SelectFirstMessage() throws InterruptedException
	{
		FirstMessageCheckBox.click();
		sleep(2);
	}
	// Verify all the options of Text Message window
	
	public void VerifyAllTheOptionsOfTextMessageWindow() throws InterruptedException
	{
		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='mess_model_body']/div/div[1]"));
		
		int Actualcount = ls.size();
		System.out.println(Actualcount);
		int expectedCount = 4;
		String pass = "PASS >> 4 options are available";
		String fail = "FAIL >> 4 options are NOT available";
		ALib.AssertEqualsMethodInt(expectedCount, Actualcount, pass, fail);
		
		// checking options
		for(int i=0;i<Actualcount;i++)
		{
			if(i==0)
			{
				String firstOption = ls.get(i).getText();
				String expected = "Subject";
				String pass1 = "PASS >> first option is correct and available";
				String fail1 = "FAIL >> first option either NOT correct or Not available";
				ALib.AssertEqualsMethod(expected, firstOption, pass1, fail1);
			}
			
			if(i==1)
			{
				String secondOption = ls.get(i).getText();
				String expected = "Message";
				String pass1 = "PASS >> second option is correct and available";
				String fail1 = "FAIL >> second option either NOT correct or Not available";
				ALib.AssertEqualsMethod(expected, secondOption, pass1, fail1);
			}
			
			if(i==2)
			{
				String thirdOption = ls.get(i).getText();
				System.out.println(thirdOption);
				String expected = "Get Read Notification";
				String pass1 = "PASS >> third option is correct and available";
				String fail1 = "FAIL >> third option either NOT correct or Not available";
				ALib.AssertEqualsMethod(expected, thirdOption, pass1, fail1);
			}
			
			if(i==3)
			{
				String fourthOption = ls.get(i).getText();
				String expected = "Force Read Message";
				String pass1 = "PASS >> fourth option is correct and available";
				String fail1 = "FAIL >> fourth option either NOT correct or Not available";
				ALib.AssertEqualsMethod(expected, fourthOption, pass1, fail1);
				
			}
		}
	}
	
	 public void EnterMessageBody(String MsgBody)
	 {
		 messageBody.sendKeys(MsgBody);
	 }
	
		@FindBy(xpath="//input[@value='richtext']")
		private WebElement richTextTab;
		
		public void verifyRichTextFields() throws InterruptedException
		{
			richTextTab.click();
			sleep(2);
		}
		
		@FindBy(xpath="//input[@value='simpletext']")
		private WebElement plainTextTab;
		public void clikOnPlainText() throws InterruptedException
		{
			plainTextTab.click();
			sleep(2);
		}
		
		@FindBy(xpath="//span[contains(text(),'Enable Buzz and Close Message Popup works on Android Nix Version 13.29 onwards.')]")
		private WebElement RemoteBuzzSupportVersion;
		public void VerifyBuzzSupportVersion()
		{
			boolean ispresent = RemoteBuzzSupportVersion.isDisplayed();
			String pass1= "PASS >> Buzz version support message is displayed";
			String fail1 ="FAIL >> Buzz version Support message is not displayed";
			ALib.AssertTrueMethod(ispresent, pass1, fail1);
		}
		@FindBy(xpath="//span[contains(text(),'Rich Text Message works on Android Nix Version 13.')]")
		private WebElement VersionsupportMessage;
		public void VerifyVersionSupportMessage() throws InterruptedException
		{
			
			boolean ispresent = VersionsupportMessage.isDisplayed();
			String pass1= "PASS >> version support message is dispalyed";
			String fail1 ="FAIL >> version support message is not dispalyed";
			ALib.AssertTrueMethod(ispresent, pass1, fail1);
		}
	 public void GetReadNotificaiton() throws InterruptedException
	 {
		 getReadNotification.click();
		 sleep(1);
	 }
	 
	 public void ForceReadMessage() throws InterruptedException
	 {
		 ForceReadMessage.click();
		 sleep(1);
	 }
	 public void ClickOnSendButton() throws InterruptedException
	 {
		 sendButton.click();
		 sleep(5);
	 }
	 
	 public void NotificationMessageSent() throws InterruptedException
	 {
        boolean isdisplayed =true;
		 
		 try{
			 
			 NotificationSendMessage.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> NOT displayed message sent confirmation");
	   Reporter.log("PASS >> 'Message sent successfully.' is received",true );	
	   try {
		sleep(2);
	} catch (Exception e) {
		
		e.printStackTrace();
	}
	 
	   
	 }
	 public void warningWhenNoMessageBody()
	 {
         boolean isdisplayed =true;
		 
		 try{
			 WarningMessageWhenNoMessageBody.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> NOT displayed warning");
	   Reporter.log("PASS >> 'Cannot send message without body.' is received",true );	
	 }
	 
	 @FindBy(xpath="//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	 private WebElement NOmarkUnread;

	 public void CancelBtnClickMarkUnread() throws InterruptedException {
	 	
	 	NOmarkUnread.click();
	 	sleep(2);
	 }
	 
	 @FindBy(xpath="//span[text()='Access denied. Please contact your administrator.']")
	  private WebElement AccessDEniedMoveToFolder;
	 public void VerifySomeOptionsInInbox() throws InterruptedException {
			
			MailReplyButton.click();
		    sleep(2);
			String Actualvalue3 = AccessDEniedMoveToFolder.getText();	
			String Expectedvalue3 = "Access denied. Please contact your administrator.";
			String PassStatement3 = "PASS >>'"+MailReplyButton+"'error Message displayed'";
			String FailStatement3 = "FAIL >>'"+MailReplyButton+"'error Message not displayed'";
			ALib.AssertEqualsMethod(Expectedvalue3, Actualvalue3, PassStatement3, FailStatement3);
		    sleep(4);
		}
	 
	 @FindBy(xpath="//*[@id='mailGrid']/tbody/tr[1]")
	 private WebElement mailGrid;

	 public void VerifyMessagesShouldBeDisplayedInInbox() throws InterruptedException {
	 	
	 	
	 	String Message=mailGrid.getText();
	 	System.out.println(Message);
	 	String Expectedvalue = Message;
	 	String PassStatement = "PASS >>'"+mailGrid+"' is displayed";
	 	String FailStatement = "FAIL >>'"+mailGrid+"' is not displayed";
	 	ALib.AssertEqualsMethod(Message, Expectedvalue, PassStatement, FailStatement);
	 	sleep(2);
	 }
	 
	 @FindBy(xpath="//*[@id='inboxSettings_modal']/div/div/div[1]/button")
	 private WebElement CloseBtnCleanupInnbox;

	 public void CloseBtnCleanUpInbox() throws InterruptedException {
	 	
	 	CloseBtnCleanupInnbox.click();
	 	sleep(2);
	 	
	 }
	 @FindBy(xpath="//*[@id='message_modal']/div/div/div[1]/button")
	 private WebElement CloseReplyBtn;

	 public void ClickOnReplyCloseBtn() throws InterruptedException {
	 	
	 	CloseReplyBtn.click();
	 	sleep(2);
	 	
	 }

	 public void WarningPopUpWhenMarkAsUnread()
	 {
		 
       boolean isdisplayed =true;
		 
		 try{
			 warningMessageWhenMarkAsUnread.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> mark as unread warning NOT displayed");
	   Reporter.log("PASS >> 'Do you want to mark the message as unread?' is received",true );	
	   waitForPageToLoad();
	 }
	 
	 public void Cancelling_MarkAsUnread() throws InterruptedException
	 {
		 Helper.highLightElement(Initialization.driver, Cancel_MarkAsUnread);
		 
		 Cancel_MarkAsUnread.click();
		 waitForidPresent("mailReplyButton");
		 sleep(2);
		 
	 }
	 
	 public void Yes_MarkAsUnread()
	 {
		 Initialization.driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS );
		
		Helper.highLightElement(Initialization.driver, YES_MarkAsUnread);
		
		 YES_MarkAsUnread.click();
		 
	 }
	 
	 public void ConfirmationNotificationOnMarkAsUnread() throws InterruptedException
	 {
		 
       boolean isdisplayed =true;
		 
		 try{
			 confirmiationNotificaiotnAfterMarkAsUnread.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> Notifcation did not displayed- Did not mark as unread");
	   Reporter.log("PASS >> 'Message marked as unread successfully.' is received - Marked message unread",true );	
	   sleep(1);
	 }
	 
	 public void DeleteMessageWithoutSelectingAnyMessage() throws InterruptedException
	 {
        boolean isdisplayed =true;
		 
		 try{
			 warningOnDeleteMessageWhenNoMessageisSelected.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> Notifcation did not displayed");
	   Reporter.log("PASS >> 'Please select a message.' is received",true );	
	   sleep(1);
		 
	 }
	 public void SelectThirdMessage() throws InterruptedException
		{
		 ThirdMessageCheckBox.click();
	     sleep(2);
		}
	 public void clickOnCleanupInbox() throws InterruptedException
	 {
		 CleanupInbox.click();
		 sleep(2);
	 }
	 public void VerifyCleanupInboxOptionsWindow()
	 {
		 boolean isCleanupInboxWindowDisplayed = CleanupInboxOptionsWindow.isDisplayed();
		 String pass = "PASS >> 'Cleanup window option' window is displayed";
		 String fail ="FAIL >> 'Cleanup window option' window is NOT displayed";
		 ALib.AssertTrueMethod(isCleanupInboxWindowDisplayed, pass, fail);
		 
		 boolean ispresent = isDeleteMessageOlderThanPresent.isDisplayed();
		 String pass1= "PASS >> 'Delete Messages Older Than'is present";
		 String fail1 ="FAIL >> 'Delete Messages Older Than' window is NOT present";
		 ALib.AssertTrueMethod(ispresent, pass1, fail1);
	 }
	 
	 //search mails using different keywords
	 
	 public void SearchMailsUsingFrom() throws InterruptedException
	 {
		 //search using from
		 MailSearchBox.sendKeys(Config.searchFrom);
		 sleep(2);
		 List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='mailGrid']/tbody/tr/td[2]/span"));
		
		 
		 int count =4; // 4 mails in inbox with "from"
		 for(int i=0;i<count;i++)
		 {
			Helper.highLightElement(Initialization.driver, ls.get(i));
			 
			 String Actual_from = ls.get(i).getText();
			 String Expected_from = Config.searchFrom;
		     String pass = "PASS >> from returned: "+Actual_from+" is correct - mail search using 'from' is correct";
			 String fail = "FAIL >> from returned: "+Actual_from+" is NOT correct-mail search using 'from' is NOT correct";
			 ALib.AssertEqualsMethod(Expected_from, Actual_from, pass, fail);
			 sleep(3);
		 }
	 }
		 
		 public void ClearMailSearchBox() throws InterruptedException
		 {
			 MailSearchBox.clear();
			 sleep(2);
		 }
		 
		 
		//search using subject
		 
		 public void SearchMailsUsingSubject() throws InterruptedException
		 
		 {
		 MailSearchBox.sendKeys(Config.searchSubject);
		 sleep(2);
	List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='mailGrid']/tbody/tr/td[3]/span"));
		
		 int count =1; //only 1 mail in inbox with subject suremdm
		 for(int i=0;i<count;i++)
		 {
			Helper.highLightElement(Initialization.driver, ls.get(i));
			 
			 String Actualsubject = ls.get(i).getText();
			 String ExpectedSubject = Config.searchSubject;
		     String pass = "PASS >> subject returned: "+Actualsubject+" is correct - mail search using 'subject' is correct";
			 String fail = "FAIL >> subject returned: "+Actualsubject+" is NOT correct-mail search using 'subject' is NOT correct";
			 ALib.AssertEqualsMethod(ExpectedSubject, Actualsubject, pass, fail);
			 sleep(2);
			 
		}
}
		 public void VerifyTextMessageInBodyAfterSearch()
		 {
			 String actualText = messageTextArea.getText();
			 String expectedText = Config.messageBodySentFromDeviceSie;
			 String pass = "PASS >> Text Message in body returned is: "+actualText+" correct";
			 String fail = "FAIL >> Text Message in body returned is: "+actualText+" NOT correct";
			 ALib.AssertEqualsMethod(expectedText, actualText, pass, fail);
		 }
			 
		//Inbox Pagination
		 
		 public void VerifyPagintion() throws InterruptedException
		 {
			 boolean mark;
			 PagesizeButton.click();
			 waitForXpathPresent("(//a[text()='100'])[2]");
			 WebElement Ele50 = Initialization.driver.findElement(By.xpath("(//a[text()='"+Config.Select100+"'])[2]"));
			 act.click(Ele50).perform();
			 sleep(2);
			 List<WebElement> List = Initialization.driver.findElements(By.xpath("//*[@id='mailGrid']/tbody/tr/td[2]"));     
			 if(List.size()==Config.Select100)
			 {
				 mark=true;
			 }
			 else
			 {
				 mark=false;
			 }
			 String PassStatement="PASS >> Number of Mails in curent page is"+" "+Config.Select100;
		   	 String FailStatement="FAIL >> Number of Mails in curent page isn't"+" "+Config.Select100;
		   	 ALib.AssertTrueMethod(mark, PassStatement, FailStatement);
		 }
		 public void SearchMailInInbox(String mail) throws InterruptedException
		 {
			 Initialization.driver.findElement(By.xpath("(//input[@placeholder='Search'])[last()-1]")).clear();
			 sleep(2);
			 Initialization.driver.findElement(By.xpath("(//input[@placeholder='Search'])[last()-1]")).sendKeys(mail);
			 sleep(2);
		 }
		 
		 public void VerifyOnDeletedMail(String mail)
		 {
			 boolean MailFound=false;
			 try {
				 Initialization.driver.findElement(By.xpath("//tr[@class='selected']//span[contains(text(),'"+mail+"')]")).isDisplayed();
				 MailFound=true;
			 }catch (Exception e) {
				Assert.assertFalse(false, "Mail isn't present......");
				Reporter.log("Mail isn't present......",true);
			}
		 }
		 
		 public void clickOnTextMessageInMailBoxReadNotify(String msg,String body) throws InterruptedException
	        {
	                Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Subject: '"+msg+"'']").click();
	                sleep(3);
	                boolean MessageInMaliBox = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+body+"']").isDisplayed();
	                String pass = "PASS >> Mail has been recieved from console successfully...";
	                String fail = "FAIL >> Mail isn't been recieved from console successfully";
	                ALib.AssertTrueMethod(MessageInMaliBox, pass, fail);
	                sleep(10);
	        }
		 public void clickOnCloseSendMessageTab() throws InterruptedException
		 {
			 Initialization.driver.findElement(By.xpath("//div[@id='message_modal']//button[@type='button']")).click();
			 sleep(3);
		 }
		 
		 public void SelectingUnreadMessage() throws InterruptedException
		 {
			 try
			 {
				 Initialization.driver.findElement(By.xpath("//table[@id='mailGrid']//td[2]//span[@style='font-weight:900']//ancestor::tr/td[@class='bs-checkbox']")).click();
				 sleep(2);
				 ClickOnMarkAsRead();
			 }
			 
			 catch (Exception e) 
			 {
				 Initialization.driver.findElement(By.xpath("(//table[@id='mailGrid']//td[2]//span[@style='font-weight:900']//ancestor::tr/td[@class='bs-checkbox'])[2]")).click();
			    sleep(2);
				 ClickOnMarkAsRead();
			}
			 	 
		 }
		 
		 
		 public void SelectingReadMessage() throws InterruptedException
		 {
			 try
			 {
				 Initialization.driver.findElement(By.xpath("//table[@id='mailGrid']//td[2]//span[@style='font-weight:normal']//ancestor::tr/td[@class='bs-checkbox']")).click();
					sleep(2);
					ClickOnMarkAsUnread();
			 }
			 catch (Exception e) {
			Initialization.driver.findElement(By.xpath("(//table[@id='mailGrid']//td[2]//span[@style='font-weight:normal']//ancestor::tr/td[@class='bs-checkbox'])[2]")).click();
			sleep(2);
			ClickOnMarkAsUnread();
			}
		 }
		 
		 public void ClickOnMarkAsRead() throws InterruptedException
		 {
			 MarkasRead.click();
			 waitForXpathPresent("//div[@id='ConfirmationDialog']//button[text()='Yes']");
			 sleep(2);
		 }
		 
		 public void ClickOnMarkAsUnread() throws InterruptedException
		 {
			 MarkAsUnread.click();
			 waitForXpathPresent("//div[@id='ConfirmationDialog']//button[text()='Yes']");
			 sleep(2);
		 }
		 
		 public void ClickOnMarkAsReadYesButton() throws InterruptedException
		 {
			 Initialization.driver.findElement(By.xpath("//div[@id='ConfirmationDialog']//button[text()='Yes']")).click();
			 sleep(3);
		 }
		 
		 public void VerfySuccessMessageOnMarkAsRead(String Action)
		 {
			 try
			 {
				 Initialization.driver.findElement(By.xpath("//span[text()='Message marked as "+Action+" successfully.']")).isDisplayed();
				 Reporter.log("Success Message Displayed Successfully",true);
			 }
			 
			 catch (Exception e) 
			 {
				 ALib.AssertFailMethod("FAIL>>> Success Message Not Displayed");
			}
		 }
	 }