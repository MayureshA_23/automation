package ProfilesAndroid_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class WifiConfiguration extends Initialization {

	@Test(priority = '1', description = "1.To Verify clicking on wifi Configuration profile")
	public void VerifyClickingOnWifiConfigurationProfile() throws InterruptedException {
		Reporter.log("1.To Verify clicking on wifi configuration profile");
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnWifiConfigurationPolicy();
	}

	@Test(priority = '2', description = "2.To Verify Clicking on Wifi Configuration profile config button")
	public void VerifyWifiConfigurationProfileConfigButton() throws InterruptedException {

		profilesAndroid.ClickOnWifiConfigurationPolicyConfigButton();
	}

	@Test(priority = '3', description = "3.Verify default value of Security Type and Security Type Open")
	public void VerifyDefaultValueOfSecurityType()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("3.Verify default value of Security Type and Security Type Open");

		profilesAndroid.DefaultValueSecurityType();
		profilesAndroid.SecurityTypeOpen();
	}

	@Test(priority = '4', description = "4.Verify Parameters Of wifi Configuration Policy Window")
	public void VerifyParametersOfWifiConfigurationPolicyWindow()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("4.Verify Parameters Of wifi Configuration Policy Window");
		profilesAndroid.VerifyParametersOfWifiConfigurationPolicyWindow();
	}

	@Test(priority = '5', description = "5.Verify Entering parameters value of SSID and Password wifi Configuration Policy Window - Open")
	public void VerifyEnteringParametersValueOfWifiConfigurationPolicyOPEN()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("5.Verify Entering parameters value of wifi Configuration Policy Window");
		profilesAndroid.EnterSSIDWifiConfigurationOpen();
	}

	@Test(priority = '6', description = "6.Verify Enabling 'Auto Connect'")
	public void EnablingAutoConnect_HiddenNetwork()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("6.Verify Enabling 'Auto Connect'");
		profilesAndroid.EnableAutoConnect();

	}

	@Test(priority = '7', description = "7.verify Enter Wifi configuration profile Name and Save")
	public void VerifyEnterNameOfWifiConfigurationProfileAndSave()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("7.verify Enter Wifi configuration profile Name and Save");
		profilesAndroid.EnterWifiConfigurationProfileName(Config.EnterWifiConfigurationProfileNameHotSpot);
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
	}

	@Test(priority = '8', description = "8.verify the Navigation to Home and Apply prfoile")
	public void NavigationToHomeAndApply()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("8. Verify the Navigation to Home and Apply prfoile");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		profilesAndroid.SelectWifiConfigProfile1();
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();

	}

	@Test(priority = '9', description = "9.verify Wifi configuration on Device")
	public void VerifyingTheWifiProfileOndevice() throws IOException, InterruptedException 
	{
		commonmethdpage.AppiumConfigurationCommonMethod("com.android.settings","com.android.settings.wifi.WifiSettings");
		profilesAndroid.verfiyWifiHotspotConnectedAfterApplyingTheProfile();

	}
}
