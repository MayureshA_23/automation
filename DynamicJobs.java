package SanityTestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DynamicJobs extends Initialization {
	@Test(priority='0',description="Validate Force Read Message in Dynamic Message")
	public void DynamicMessageJob_TC_DJ_40() throws InterruptedException, Throwable{
		Reporter.log("\n1.Validate Force Read Message in Dynamic Message",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.EnterSubjectSMS("DynamicFR");
	    dynamicjobspage.EnterBodySMS("Have a good day");
	    dynamicjobspage.ClickOnForceReadMessage();
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ConfirmationMessageVerify();
	    dynamicjobspage.VerifyOfMessageActivityLogs(Config.DeviceName);
	    dynamicjobspage.SMSForceReadMessage();
	    androidJOB.ClickOnHomeButtonDeviceSide();
		Reporter.log("Pass >> Validate Force Read Message in Dynamic Message",true);
	}
	@Test(priority='1',description="Validate Get Read Notification in Dynamic Message")
	public void DynamicMessageJob_TC_DJ_38() throws InterruptedException, Throwable{
		Reporter.log("\n2.Validate Get Read Notification in Dynamic Message",true);
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.EnterSubjectSMS("DynamicRN");
	    dynamicjobspage.EnterBodySMS("Wssup??");
	    dynamicjobspage.ClickOnGetReadNotification();
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ConfirmationMessageVerify();
	    dynamicjobspage.VerifyOfMessageActivityLogs(Config.DeviceName);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		dynamicjobspage.clickOnTextMessageInMailBoxReadNotify();
		Reporter.log("PASS>> Validate Get Read Notification in Dynamic Message",true);
	}
	
}
