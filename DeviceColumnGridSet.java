package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
//'AllowAllColumn' and 'AllowFewColumn' Device Grid Column must be deleted
// 0UsermanagementEnable must be present
//0DonotDeleteUMEnable premission should be present in the console

public class DeviceColumnGridSet extends Initialization {

	@Test(priority = '1', description = "1.On clicking on Device grid Column set page should be displayed ")
	public void verifyClickingonDeviceGridColumnSetInUserManagement() throws InterruptedException {
		Reporter.log("\n1.On clicking on Device grid Column set page should be displayed ", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.clickOnDeviceGridColumn();
	}

	@Test(priority = '2', description = "2.Create device Column Grid and successfully saved message should be displayed")
	public void verifyCreateDevicegridColumTemplateAndCreateSuccessfullyMessageShouldbeDisplayed()
			throws InterruptedException {
		Reporter.log("\n2.Create device Column Grid and successfully saved message should be displayed", true);
		usermanagement.CreateNewDeviceGridColumn();
	}

	@Test(priority = '3', description = "3.User should be created successfully and all the coluns must be displayed ")
	public void verifyCreateSubUserAssignDeviceGroupSetAllowAllColumns() throws InterruptedException {
		Reporter.log("\n3.User should be created successfully and all the coluns must be displayed ", true);
		usermanagement.CreateUserForDeviceColumnGrid("AutoDeviceGridColumnSet", "42Gears@123",
				"AutoDeviceGridColumnSet", "0DonotDeleteUMEnable", "AllowAllColumn");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoDeviceGridColumnSet", "42Gears@123");
		// usermanagement.CheckCountOfColoumnsDeviceGrid();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.clickOnDeviceGridColumn();
	}

	@Test(priority = '4', description = "4.Few Enabled column must be displayed")
	public void verifyfewAllowedColumnsShouldBeVisibleInSubUser() throws InterruptedException {
		Reporter.log("\n4.Few Enabled column must be displayed", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.clickOnDeviceGridColumn();
		usermanagement.CreateNewDeviceGridColumnAllowOnlyFew();
		usermanagement.CreateUserForDeviceColumnGrid("AutoUser2NameDeviceGridColumnSet", "42Gears@123",
				"AutoUser2NameDeviceGridColumnSet", "0DonotDeleteUMEnable", "AllowFewColumn");
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoUser2NameDeviceGridColumnSet", "42Gears@123");
		// usermanagement.CheckallowedVisibleColoumn();

	}
}
