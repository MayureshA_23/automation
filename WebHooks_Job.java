package JobsOnAndroid;

import java.io.IOException;

import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class WebHooks_Job extends Initialization{
	
	//Precondition- All Webhook must be removed first
	
	@Test(priority=0,description="Enable Webhook")
    public void EnableWebHook() throws InterruptedException, IOException
    {
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.VerifyWebHooksOptnInAccountSettingsSubUser();
		androidJOB.ClickOnWebHooks();
		androidJOB.EnableWebHooks();
		androidJOB.EnterWebHookName(1,"Webhooks",2,Config.WrbhookEndPoint);
		androidJOB.EnterWebHookName(1,"GWebhooks",2,"https://webhook.site/eb3b1869-9625-4db6-84ea-09807fc0e09p");
		androidJOB.EnterWebHookName(1,"AWebhooks",2,"https://webhook.site/eb3b1869-9625-4db6-84ea-09807fc0e09r");
		androidJOB.EnterWebHookName(1,"FWebhooks",2,"https://webhook.site/eb3b1869-9625-4db6-84ea-09807fc0e09n");
		androidJOB.EnterWebHookName(1,"@@Webhooks",2,"https://webhook.site/eb3b1869-9625-4db6-84ea-09807fc0e09g");
		androidJOB.EnterWebHookName(1,"123Webhooks",2,"https://webhook.site/eb3b1869-9625-4db6-84ea-09807fc0e09h");
		androidJOB.EnterWebHookName(1,"WebhooksNotificationJob",2,"https://webhook.site/6535bd0e-c987-4909-9f56-55bcedef99c9");

		androidJOB.ApplyWebhookBtn();
		


  }
	@Test(priority=1,description="Verfiy Webhooks is displayed in Notification policy Job in Android")
    public void TC_JO_853() throws InterruptedException, IOException
    {
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();

		androidJOB.ClickOnNotificationPolicy();
		androidJOB.EnableConnectionPolicy();
		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
		androidJOB.CloseNotificationPolicy();
		commonmethdpage.refesh();


  }
	@Test(priority=2,description="Verfiy Webhooks is displayed in Notification policy Job in Android Wear")
    public void TC_JO_861() throws InterruptedException, IOException
    {
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidWearOS();

		androidJOB.ClickOnNotificationPolicy();
        androidJOB.EnableConnectionPolicy();
		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
		androidJOB.CloseNotificationPolicy();
		commonmethdpage.refesh();


  }
	
	@Test(priority=3,description="Verify default  Endpoint URLS in Notificaton polciy job for webhooks")
    public void DefaultEndpointsURLs_WebHooks() throws InterruptedException, IOException
    {
		
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnNotificationPolicy();
		androidJOB.EnableConnectionPolicy();
		androidJOB.CickOnWebHookOptnNotificationPolicyJob();
		androidJOB.ClosewebhooksPopup();
		androidJOB.CloseNotificationPolicy();
		commonmethdpage.refesh();

  }
	@Test(priority=4,description="Verify Endpoint URLS are displaying in Alphabeticall order in Notificaton polciy job for webhooks")
    public void AlphabeticallOrderInNotificatonPolciyJobForWebhooks() throws InterruptedException, IOException
    {

		
		

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();

		androidJOB.ClickOnNotificationPolicy();
		androidJOB.EnableConnectionPolicy();
		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
		androidJOB.CickOnWebHookOptnNotificationPolicyJob();

		
		androidJOB.SingleSelectTagDropdownWebElement();
		
		androidJOB.ClosewebhooksPopup();
		androidJOB.CloseNotificationPolicy();
		commonmethdpage.refesh();

		
    }
	//DONT RUN
/*	@Test(priority=5,description="Verify applying notification policy job to android device by enabling webhooks ")
    public void TC_JO_854() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
    {

		
		
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();

		androidJOB.ClickOnNotificationPolicy();
		androidJOB.EnterJobName("EnableBatttery_WebHook");
		androidJOB.EnableBatteryPolicy();
		androidJOB.EnableMDM();
		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
		androidJOB.CickOnWebHookOptnNotificationPolicyJob();
     	androidJOB.SelectTheEndPointUrl("WebhooksNotificationJob");
		androidJOB.ClickOnwebhooksPopupDoneBtn();
		androidJOB.ClickOnSaveJob();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnableBatttery_WebHook");
		androidJOB.CheckStatusOfappliedInstalledJob("EnableBatttery_WebHook", Config.TotalTimeForJobName1ToGetDeployedOnDevice);

		}
*/		
	@Test(priority=6,description="Verify Webhooks option in Notification policy job without adding endpoints ")
    public void TC_JO_869() throws InterruptedException, IOException
    {
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.VerifyWebHooksOptnInAccountSettingsSubUser();
		androidJOB.ClickOnWebHooks();
		androidJOB.EnableWebHooks();
		androidJOB.RemoveWebhooks();
		androidJOB.ClickOnAddEndPoint();
		androidJOB.ApplyWebhookBtn();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();

		androidJOB.ClickOnNotificationPolicy();
		androidJOB.EnableConnectionPolicy();
		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
		androidJOB.CickOnWebHookOptnNotificationPolicyJob();

		androidJOB.VerifyErrorMsgWebhookNotificationPolicy();
		androidJOB.CloseNotificationPolicy();
		commonmethdpage.refesh();
}


	@Test(priority=7,description="Verify Webhooks option in Notification policy when webhooks is disabled in account settings")
    public void TC_JO_863() throws InterruptedException, IOException
    {
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.VerifyWebHooksOptnInAccountSettingsSubUser();
		androidJOB.ClickOnWebHooks();
		androidJOB.DisableWebHooks();
		androidJOB.ApplyWebhookBtn();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();

		androidJOB.ClickOnNotificationPolicy();
		androidJOB.EnableConnectionPolicy();
		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
		androidJOB.CloseNotificationPolicy();
		androidJOB.VerifyWebhookOptnNotPresentInNotificationPolicyJob();

    }
	

}
