package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Enable_Disable_logger extends Initialization{
	
	@Test(priority=1, description="create a runscript to Disable Uninstall")
	public void ToDisableLogger() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnDisableLogger();
		runScript.ClickOnValidateButton();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("DisableLogger");
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableLogger");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.RunScriptDisableLogger,Config.TimeToDeployRunscriptJob);
		runScript.ClickOnSettingsDeviceSide();
		
		
	}

}
