package DeviceGrid_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//pass the device name which is offline

public class DeviceGrid_TestCases extends Initialization{
	@Test(priority = '0', description = "Verify OS Build number in device grid")
	public void VerifyOSBuildNumber()throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("1.Validation of Last Device Time column in Device Grid.",true);
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		deviceGrid.verifyDeviceOSBuildNumber(Config.Windows_OSBuildNumber);
	}

	@Test(priority = '1', description = "Verify OS Build number in custom device detail report")
	public void VerifyOSBuildNumberInCustomDeviceDetailsreport()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n2.Verify OS Build number in custom device detail report",true);
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		deviceGrid.ReadingOSBuildNumber();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.OSBuildNu_CustomReportName,
				Config.OSBuildNu_CustomReportDescription);
		deviceGrid.checkingTableInReports("Device Details");
		accountsettingspage.ClickOnAddSelectedTable();
		accountsettingspage.ClickOnCustomReportSaveButton();
		accountsettingspage.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport(Config.OSBuildNu_CustomReportName);
		accountsettingspage.ClickOnSelectDeviceButtonInReports();
		accountsettingspage.SearchingForDeviceInOnDemandReport(Config.WindowsDeviceName);
		accountsettingspage.ClickOnAddDeviceButton();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView(Config.OSBuildNu_CustomReportName);
		deviceGrid.WindowHandle();
		deviceGrid.VerifyOSBuildNumberInReport(66);
		deviceGrid.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = '2', description = "Verify OS Build number in Asset Tracking report")
	public void VerifyOSBuildNumberInAssetTrackingReport()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n3.Verify OS Build number in Asset Tracking report",true);
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		deviceGrid.ReadingOSBuildNumber();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Asset Tracking");
		deviceGrid.WindowHandle();
		quickactiontoolbarpage.SearchingInsideReport(Config.WindowsDeviceName);
		deviceGrid.VerifyOSBuildNumberInReport(42);
		deviceGrid.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = '3', description = "Verify custom column values after refresh.")
	public void VerifyCustomColumnValueAfterRefresh()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n4.Verify custom column values after refresh.",true);
		deviceGrid.DisablingAllCustomColumns();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.AdvanceSearch_CustomColumn);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.AdvanceSearch_CustomColumn);
		deviceGrid.ClickOnCustomColumnsCloseButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.AdvanceSearch_CustomColumn);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.AdvanceSearch_CustomColumn);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.AdvanceSearch_CustomColumn);
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.AddingValuesToCustomColumn(Config.AdvanceSearch_CustomColumnValue);
		deviceGrid.ClickOnSaveButton_CustomColumnValue(deviceGrid.Message);
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.AdvanceSearch_CustomColumnValue);
		DeviceEnrollment_SCanQR.clicOnGridRefresh();
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.AdvanceSearch_CustomColumnValue);
		deviceGrid.ClicOnDynamicRefreshButton();
		deviceGrid.VerifyUniqueValueInDeviceInfoPanelAndDeviceGrid(Config.AdvanceSearch_CustomColumnValue);

	}

	//*************************************************Blocked*******************************************//
	@Test(priority = '4', description = "Verify the device count within asset tracking report, device grid,Dashboard and licence count. ",enabled=false)
	public void VerifyDeviceCount() throws InterruptedException {
		Reporter.log("\n5.Verify the device count within asset tracking report, device grid,Dashboard and licence count.",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();
		deviceGrid.ReadingDeviceCountInAssettrackingReport();
		accountsettingspage.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
		MacOSDeviceInfo.ClickOnAllDeviceTab();
		Filter.ClickOnDevicesPerPagebutton();
		Filter.chooseMaxDevicesPerPage();
		deviceGrid.ReadingDeviceCountInDeviceGrid();
		commonmethdpage.ClickOnSettings();
		deviceGrid.ReadingLicenseCount("devicesused");
		deviceGrid.ReadingLicenseCount("thingsdevicesused");
		deviceGrid.ReadingLicenseCount("vrLicenseUsed");
		commonmethdpage.ClosingSettings();
		dashboardpage.ClickOnDashBoard();
		deviceGrid.ReadingDeviceCountInOnlineOfflineDashBoard();
		commonmethdpage.ClickOnHomePage();
		deviceGrid.VerifyDeviceCountMatchingInGrid_AssetReport_DashBoard_LicenseUsedSection();
	}

	@Test(priority = '5', description = "Verify Free Storage Memory in Device Grid for Windows devices enrolled through Nix")
	public void VerifyStorageMemoryInDeviceGridEnrolledThroughNix()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n6.Verify Free Storage Memory in Device Grid for Windows devices enrolled through Nix",true);
		commonmethdpage.SearchDevice(Config.WindowsDeviceName);
		devicegridwindows.verifyDeviceFreestorageMemeory("49");
	}

	@Test(priority = '6', description = "Error message should be shown when we try to save custom column with dot(.)")
	public void VerifyErrorMessageWhenTriedToSaveColumnWithDot() throws InterruptedException {
		Reporter.log("\n7.Error message should be shown when we try to save custom column with dot(.)",true);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn("VerifyChanging Name");
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.SendingCustomColumnName(".");
		deviceGrid.ClikOnAddNewColumnButtonWhenColmnNameisDot();
		deviceGrid.SendingCustomColumnName("VerifyChanging Name");
		deviceGrid.ClickOnAddNewColumnOkButton();
	}

	@Test(priority = '7', description = "Edit a column with give the new name with the dot in it or . in middle or . at the begining or . at the last ",dependsOnMethods = "VerifyErrorMessageWhenTriedToSaveColumnWithDot")
	public void VerifyEditColumnName() throws InterruptedException {
		Reporter.log("\n8.Edit a column with give the new name with the dot in it or . in middle or . at the begining or . at the last ",true);
		deviceGrid.ModifyingcustomColumn(".Modify", "VerifyChanging Name");
		deviceGrid.VerifyErrorMessagewhenColumnNameIncludesDot();
		deviceGrid.ClickOnAddCustomColumnCloseButton();
		deviceGrid.ModifyingcustomColumn("Mod.ify", "VerifyChanging Name");
		deviceGrid.VerifyErrorMessagewhenColumnNameIncludesDot();
		deviceGrid.ClickOnAddCustomColumnCloseButton();
		deviceGrid.ModifyingcustomColumn("Modify.", "VerifyChanging Name");
		deviceGrid.VerifyErrorMessagewhenColumnNameIncludesDot();
		deviceGrid.ClickOnAddCustomColumnCloseButton();
		deviceGrid.ClickOnCustomColumnsCloseButton();
	}

	@Test(priority = '8', description = "Verify the OS Build Number on Device grid ")
	public void VerifyOSBuildNumberInGrid()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n9.Verify the OS Build Number on Device grid ",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.verifyDeviceOSBuildNumber(Config.Android_OSBuildNumber);
		deviceGrid.ReadingOSBuildNumberInConsole();
	}

	@Test(priority = '9', description = "Verify Searching any value for OS Build Number in Device Grid ", dependsOnMethods = "VerifyOSBuildNumberInGrid")
	public void VerifySearchingOSBuildNumberInDeviceGrid() throws InterruptedException {
		Reporter.log("\n10.Verify Searching any value for OS Build Number in Device Grid ",true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ScrollToColumn("OsBuildNumber");
		deviceGrid.EnterValueInAdvanceSearchColumn("OsBuildNumber", deviceGrid.OSBuildNumberInConsole);
		deviceGrid.VerifySearchedvaluesInGrid("OsBuildNumber", deviceGrid.OSBuildNumberInConsole);
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 'A', description = "Verify Searching N/A in OS Build number in Device grid ")
	public void VerifySearchingNAInOSBuildNumber() throws InterruptedException {
		Reporter.log("\n11.Verify Searching N/A in OS Build number in Device grid ",true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ScrollToColumn("OsBuildNumber");
		deviceGrid.EnterValueInAdvanceSearchColumn("OsBuildNumber", "N/A");
		deviceGrid.VerifySearchedvaluesInGrid("OsBuildNumber", "N/A");
		deviceGrid.SelectSearchOption("Basic Search");
	}

	/*@Test(priority = 'B', description = "Verify Free Storage Memory in Device Grid for Windows devices for Dual Enrollment")
	public void VerifyStorageMemoryInGrid_WindowsDualEnrollment()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n12.Verify Free Storage Memory in Device Grid for Windows devices for Dual Enrollment",true);
		commonmethdpage.SearchDevice(Config.DualEnrolledWindows_DeviceName);
		devicegridwindows.verifyDeviceFreestorageMemeory("N/A");
	}*/

	@Test(priority = 'C', description = "Verify Device Info Panel for Memory Management of Windows devices enrolled with Dual Enrollment.")
	public void VerifyMemoryManagement_WindowsDualEnrollment()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n13.Verify Device Info Panel for Memory Management of Windows devices enrolled with Dual Enrollment.",true);
		commonmethdpage.SearchDevice(Config.DualEnrolledWindows_DeviceName);
		deviceGrid.VerifymemorymanagementInInfoPanel(deviceGrid.StorageMemory_Memorymanagement1);
		deviceGrid.VerifymemorymanagementInInfoPanel(deviceGrid.StorageMemory_Memorymanagement2);
		deviceGrid.VerifymemorymanagementInInfoPanel(deviceGrid.ProgramMemory_Memorymanagement);
	}

	@Test(priority = 'D', description = "Verify Device Info Panel for Memory Management of Windows devices enrolled through Nix")
	public void VerifyMemorymanagement_WindowsNixEnrollment()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n14.Verify Device Info Panel for Memory Management of Windows devices enrolled through Nix",true);
		commonmethdpage.SearchDevice(Config.Windows_DeviceName);
		deviceGrid.VerifymemorymanagementInInfoPanel(deviceGrid.StorageMemory_Memorymanagement1);
		deviceGrid.VerifymemorymanagementInInfoPanel_Nix(deviceGrid.StorageMemory_Memorymanagement2);
		deviceGrid.VerifymemorymanagementInInfoPanel(deviceGrid.ProgramMemory_Memorymanagement);
	}

	@Test(priority = 'E', description = "Validation of Location Tracking icon.")
	public void ValidationOfLocationTracking()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n15.Validation of Location Tracking icon.",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.CheckingLocationTrackingStatus();
		quickactiontoolbarpage.ClickOnLocate_UM();
		quickactiontoolbarpage.ClickingOnTurnOnButton();
		quickactiontoolbarpage.TurningOnLocationTracking("1");
		//deviceGrid.VerifyLocationTrackingICOnWhenLocationIsNotAvailable(Config.DeviceName);
		quickactiontoolbarpage.WaitingForUpdate(130);
		deviceGrid.VerifyLocationTrackingIconWhenLocationIsAvailableButLocationTrackingIsOFF();

	}

	@Test(priority = 'F', description = "Intel� AMT Status on device grid")
	public void VerifyAMTStatusInGrid() {
		Reporter.log("\n16.Intel� AMT Status on device grid",true);
		deviceGrid.VerifyIntelAMTStatusInGrid();
	}
	
	@Test(priority='H',description="8.Verify the status of the offline device after editing the device note",dependsOnMethods="RemoveTagsToMultipleDevices_TC_DG_232") 
	public void VerifyEditedNote_TC_DG_231() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n18.Verify the status of the offline device after editing the device note",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.OfflineAndroid_Device);
		deviceinfopanelpage.ClickOnEditdeviceNotes();
		deviceinfopanelpage.SelectAllNotesAndEnterNotes("AutomationDevice");
		deviceinfopanelpage.ClickOnSaveButtonAddNotes();
		deviceinfopanelpage.VerifyEditedNote();
		commonmethdpage.ClickOnAllDevicesButton();
	}
	@Test(priority='I',description="Verify On clicking on Success job status in device grid navigating to job history Complete jobs tab")
	public void VerifyOnClickingSuccessJob_NavigationtoCompletedJosTab() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n19.Verify On clicking on Success job status in device grid navigating to job history Complete jobs tab",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobSuccessStateSymbol);
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideCopletedJobTab(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}
	@Test(priority='J',description="Verify status is changing from Pending to Inprogress state once job when to In Progress.")
	public void VerifyJobStatusChangeFrompendingToInprogress() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n20.Verify status is changing from Pending to Inprogress state once job when to In Progress.",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
		quickactiontoolbarpage.VerifyJobStatusChangeFromPendingToInprogressInDevieGrid();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
	}
	@Test(priority='K',description="Verify status is changing from Pending to Inprogress and error if jobs to go to error state.")
	public void VerifyJobStatusChangeFromPendingToInprogressAndError() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n21.Verify status is changing from Pending to Inprogress and error if jobs to go to error state.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.VerifyJObStatusChangeFromPendingToinprogressAndError();
		
	}
	
	@Test(priority='L',description="Verify On Mouse hover on the pending Job status in device Grid displaying pending Job count.")
	public void VerifyMouseHoverOnPendingJobStatusInDeviceGridDisplayingJobCount() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n22.Verify On Mouse hover on the pending Job status in device Grid displaying pending Job count.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobPendingStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Pending JobsTab",1);
		deviceGrid.RefreshGrid();
		//quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
	}
	@Test(priority='M',description="Verify On Mouse hover on the Inprogress Job status in device Grid displaying Inprogress Job count.",dependsOnMethods="VerifyMouseHoverOnPendingJobStatusInDeviceGridDisplayingJobCount")
	public void VerifyMouseHoverOnInprogressJobStatusInDeviceGridDisplayingInprogressJobCount() throws InterruptedException, IOException
	{
		Reporter.log("\n23.Verify On Mouse hover on the Inprogress Job status in device Grid displaying Inprogress Job count.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobInprogressStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Inprogress JobsTab",2);	   
	}
	@Test(priority='N',description="Verify On Mouse hover on the Error Job status in device Grid displaying Error Job count.")
	public void VerifyMouseHoverOnErrorJobStatusInDeviceGriddisplayingErrorJobCount() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n24.Verify On Mouse hover on the Error Job status in device Grid displaying Error Job count.",true);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobErrorStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Error JobsTab",3);
	}
	@Test(priority='O',description="Verify On Mouse hover on the Success Job status in device Grid displaying Success Job count.")
	public void VerifyMouseHoveroNSuccessjobStatusInDeviceGridDisplsyingSuccessJobCount() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n25.Verify On Mouse hover on the Success Job status in device Grid displaying Success Job count.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobSuccessStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Success jobsTab",4);
	}
	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException {
			if(ITestResult.FAILURE==result.getStatus())
			{
				deviceGrid.RefreshGrid();
			}
		}

}
