package PageObjectRepository;

import static java.time.Duration.ofSeconds;

import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;

import io.appium.java_client.touch.LongPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;
public class Profiles_Android_POM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();
	ExcelLib ELib = new ExcelLib();

	@FindBy(id = "byodSection")
	private WebElement profilesOption;

	@FindBy(id = "addPolicyBtn")
	private WebElement Addbutton;

	@FindBy(id = "nrollafwaccount")
	private WebElement enrollAFWbutton;

	@FindBy(id = "enrollgoogleplayaccount")
	private WebElement EnrollButton;

	@FindBy(xpath = "//h4[text()='AFW Enrollment']")
	private WebElement AFWEnrollmentWindow;

	@FindBy(xpath = "//h5[text()='Profile is empty']")
	private WebElement profileIsEmptyText;

	@FindBy(xpath = "//p[text()='Click Add to create a new profile']")
	private WebElement ClickAddToCreateANewProfile;

	@FindBy(xpath = "//p[text()='Work Profile']")
	private WebElement WorkProfileText;

	@FindBy(xpath = "//p[text()='Use AFW if available']")
	private WebElement UseAFWIfAvailableText;

	@FindBy(xpath = "//span[text()='Profile Name']")
	private WebElement ProfileNameText;

	@FindBy(id = "passwordpolicyAndroidProfileConfig")
	private WebElement passwordPolicyConfigurebutton;

	@FindBy(id = "systemsettingsAndroidProfileConfig")
	private WebElement systemSettingsPolicyConfigurebutton;

	@FindBy(id = "applicationPolicyAndroidProfileConfig")
	private WebElement applicationSettingsPolicyConfigurebutton;

	@FindBy(id = "globalHTTPproxyAndroidProfileConfig")
	private WebElement networkSettingsPolicyConfigurebutton;

	@FindBy(id = "andr-mailProfileConfig")
	private WebElement mailConfigurationPolicyConfigurebutton;

	@FindBy(id = "andr-wifiProfileConfig")
	private WebElement wifiConfigurationPolicyConfigurebutton;

	@FindBy(id = "certificateProfileConfig")
	private WebElement CertificatePolicyConfigurebutton;

	@FindBy(id = "filesharingPolicyAndroidProfileConfig")
	private WebElement FileSharingPolicyConfigurebutton;

	@FindBy(xpath = ".//*[@id='policy_item_box']/div[4]/div/ul/li/span")
	private WebElement TypesOfPolicies;

	@FindBy(xpath = "//h5[text()='Use this section to configure Password Policy.']")
	private WebElement PasswordPolicySummary;

	@FindBy(xpath = "//h5[text()='Use this section to configure System Settings.']")
	private WebElement SystemSettingsPolicySummary;

	@FindBy(xpath = "//h5[text()='Use this section to configure Application Policy.']")
	private WebElement ApplicationPolicySummary;

	@FindBy(xpath = "//h5[text()='Use this section to configure Network Settings.']")
	private WebElement NetworkSettingsPolicySummary;

	@FindBy(xpath = "//h5[text()='Use this section to specify the certificates you want to install on the device. And your corporate certificate and other certificates necessary to authenticate the device access to your network.']")
	private WebElement certificatePolicySummary;

	@FindBy(xpath = "//h5[text()='Use this section to configure Exchange Mail on device via Gmail App']")
	private WebElement MailConfigurationPolicySummary;

	@FindBy(xpath = "//h5[text()='Use this section to configure your device to connect to wireless network.']")
	private WebElement WifiConfigurationPolicySummary;

	@FindBy(xpath = "//h5[text()='Use this section to configure File Sharing Policy.']")
	private WebElement FileSharingPolicySummary;

	@FindBy(xpath = ".//*[@id='androidPolicyDetails']/div/div[2]/div/div[1]/div/div[2]/button[1]")
	private WebElement BackButton;

	@FindBy(id = "saveAfwProfile")
	private WebElement saveButton;

	@FindBy(xpath = "//span[text()='Please enter a Profile Name.']")
	private WebElement warningMessageOnSavingWithoutProfileName;

	@FindBy(id = "policyName")
	private WebElement EnterPolicyName;

	@FindBy(xpath = "//span[text()='Please add at least one payload to save the profile.']")
	private WebElement warningMessageOnSavingWithoutPayload;

	@FindBy(xpath = "//span[text()='Password Policy']")
	private WebElement ClickOnPasswordPolicy;

	@FindBy(xpath = "//span[text()='Password Policy']")
	private WebElement PasswordPolicyText;

	@FindBy(xpath = "//span[text()='Application Policy']")
	private WebElement ApplicationPolicyText;

	@FindBy(xpath = "//span[text()='Network Settings']")
	private WebElement NetworkSettingsPolicyText;

	@FindBy(xpath = "//span[text()='Mail Configuration']")
	private WebElement MailConfigurationPolicyText;

	@FindBy(xpath = "//span[text()='File Sharing Policy']")
	private WebElement FileSharingPolicyText;

	@FindBy(xpath = "//span[text()='Wi-Fi Configuration']")
	private WebElement WifiConfigurationPolicyText;

	@FindBy(xpath = "//span[text()='System Settings']")
	private WebElement SystemSettingsText;

	@FindBy(xpath = "//span[text()='Unspecified']")
	private WebElement Unspecified;

	@FindBy(xpath = "//span[text()='Alphabetic']")
	private WebElement Alphabetic;

	@FindBy(xpath = "//span[text()='Alphanumeric']")
	private WebElement Alphanumeric;

	@FindBy(xpath = "//span[text()='Numeric']")
	private WebElement Numeric;

	@FindBy(xpath = "//span[text()='Complex']")
	private WebElement Complex;

	@FindBy(xpath = "//span[text()='Any']")
	private WebElement Any;

	@FindBy(xpath = "//span[text()='Manual']")
	private WebElement Manual;

	@FindBy(xpath = "//span[text()='Auto']")
	private WebElement Auto;

	@FindBy(id = "passwordPolicyPasscodeLength")
	private WebElement MinimumPasswordLength;

	@FindBy(id = "passwordPolicyMaximumFailedAttempt")
	private WebElement MaximumFailedAttempt;

	@FindBy(id = "passwordPolicyExpirationTimeout")
	private WebElement passwordTimeout;

	@FindBy(id = "passwordPolicyHistoryLength")
	private WebElement passwordHistorylength;

	@FindBy(id = "passwordPolicyMaximumTimeToLock")
	private WebElement maxiumumTimeToLock;

	@FindBy(xpath = "//span[text()='Maximum time to lock can not be less than 10 second.']")
	private WebElement WarningMessageOnMaxTimeToLock;

	@FindBy(xpath = "//span[text()='Profile created successfully.']")
	private WebElement NotificationOnProfileCreated;

	@FindBy(xpath = "//span[text()='Profile modified successfully.']")
	private WebElement NotificationOnProfileUpdated;

	@FindBy(xpath = "//span[text()='Password length should be minimum 4.']")
	private WebElement WarningMessageOnPasswordLength;

	@FindBy(xpath = "//span[text()='Enforce password history can not be more than 10.']")
	private WebElement WarningMsgOnPasswordHistory;

	@FindBy(xpath = "//span[text()='Maximum failed attempts can not be more than 6.']")
	private WebElement WarningMsgOnMaxFailedAttempts;

	@FindBy(xpath = "//td[text()='@#$PRAC7267(*)']")
	private WebElement FirstcreatedProfile;

	@FindBy(xpath = "//td[text()='Automation SystemSettingsProfile']")
	private WebElement systemSettingsCreatedProfile;

	@FindBy(xpath = "//td[text()='SamplePasswordPolicyDevice']")
	private WebElement SecondCreatedProfile;

	@FindBy(xpath = "//span[text()='System Settings']")
	private WebElement systemSettingsPolicyOption;

	@FindBy(xpath = "//span[text()='Application Policy']")
	private WebElement applicationSettingsPolicyOption;

	@FindBy(xpath = "//span[text()='Network Settings']")
	private WebElement networkSettingsPolicyOption;

	@FindBy(xpath = "//span[text()='Mail Configuration']")
	private WebElement mailConfigurationPolicyOption;

	@FindBy(xpath = "//span[text()='Wi-Fi Configuration']")
	private WebElement wifiConfigurationPolicyOption;

	@FindBy(id = "editPolicyBtn")
	private WebElement EditProfileButton;

	@FindBy(id = "policylistRefresh")
	private WebElement EMMWindowRefresh;

	@FindBy(id = "setDefaultPolicy")
	private WebElement SetAsDefault;

	@FindBy(xpath = "//span[text()='Changed default profile']")
	private WebElement SetDefaultProfileNotification;

	@FindBy(xpath = "//li[text()='Default']")
	private WebElement DefaultMark;

	@FindBy(xpath = ".//*[@id='policyTable']/tbody/tr[1]/td[1]/div")
	private WebElement DeviceWithDefaultMark;

	@FindBy(xpath = ".//*[@id='setDefaultPolicy']")
	private WebElement RemoveDefaultOption;

	@FindBy(xpath = "//span[text()='Removed default profile']")
	private WebElement NotificationOnRemovingDefaultProfile;

	@FindBy(id = "settingsPolicyBtn")
	private WebElement isSettingsButtonVisible;

	@FindBy(id = "systemSettingsDisableSafeMode")
	private WebElement SysSettingsDisableSafemode_CheckBox;

	@FindBy(id = "systemSettingsDisableSafeMode")
	private WebElement DisableCrossProfileCopyPaste_CheckBox;

	@FindBy(xpath = "//span[text()='Prompt']")
	private WebElement DefauleValue_DefaultApplicationPermission;

	@FindBy(xpath = "//*[@id='systemSettingsUnknownSources_AM_chosen']")
	private WebElement DefauleValue_UnknownSources;

	@FindBy(xpath = ".//*[@id='systemSettingsUSBDebugging_chosen']/a/span") // keeping this since previous xpath value
																			// is also same and it creates problem in
																			// element hightlight
	private WebElement DefauleValue_USBDebugging;

	@FindBy(xpath = ".//*[@id='systemSettingsSystemUpdatePolicy_chosen']/a/span") // keeping this because with don't
																					// care span xpath does not work.
																					// single/double quotes problem
	private WebElement DefauleValue_SystemUpdatePolicy;

	@FindBy(id = "applicationPolicyDefaultPermission_chosen")
	private WebElement Choose_AppPolicyDefaultPermssion;

	@FindBy(id = "systemSettingsUnknownSources_chosen")
	private WebElement Choose_UnknownSourcesDropdown;

	@FindBy(id = "systemSettingsUSBDebugging_chosen")
	private WebElement Choose_USBDebuggingDropdown;

	@FindBy(id = "systemSettingsSystemUpdatePolicy_chosen")
	private WebElement Choose_SystemUpdatePolicyDropDown;

	@FindBy(xpath = "//span[text()='Prompt']")
	private WebElement prompt;

	@FindBy(xpath = "//span[text()='Grant']")
	private WebElement Grant;

	@FindBy(xpath = "//span[text()='Deny']")
	private WebElement Deny;

	@FindBy(xpath = "//span[text()='Enable']")
	private WebElement Enable;

	@FindBy(xpath = "//span[text()='Automatic']")
	private WebElement Automatic;

	@FindBy(xpath = "//span[text()='Windowed']")
	private WebElement Windowed;

	@FindBy(xpath = "//span[text()='Postpone']")
	private WebElement Postpone;

	@FindBy(xpath = "//*[@id='systemsettingsAndroidProfileTail']/ul/li[2]/div[text()='Sync and Storage']")
	private WebElement Scroll;

	@FindBy(xpath = "//div[text()='Default Application Permission']")
	private WebElement ScrollToDefaultApplicationPermission;

	@FindBy(id = "systemSettingsDisableFactoryReset")
	private WebElement CheckBox_DisableFactoryReset;

	@FindBy(id = "systemSettingsDisableVolume")
	private WebElement CheckBox_DisableVolume;

	@FindBy(id = "systemSettingsDisableAppControl")
	private WebElement CheckBox_DisableAppControl;

	@FindBy(id = "systemSettingsDisableAppsUninstallation")
	private WebElement CheckBox_DisableAppsUninstallation;

	@FindBy(id = "systemSettingsDisableBluetoothConfiguration")
	private WebElement CheckBox_DisableBluetoothConfiguration;

	@FindBy(id = "systemSettingsDisableCredentialsConfiguration")
	private WebElement CheckBox_DisableCredentialsConfiguration;

	@FindBy(id = "systemSettingsDisableMobileNetworksConfiguration")
	private WebElement CheckBox_DisableMobileNetworksConfiguration;

	@FindBy(id = "systemSettingsDisableTetheringConfiguration")
	private WebElement CheckBox_DisableMTetheringConfiguration;

	@FindBy(id = "systemSettingsDisableVPNConfiguration")
	private WebElement CheckBox_DisableDisableVPNConfiguration;

	@FindBy(id = "systemSettingsDisableWifiConfiguration")
	private WebElement CheckBox_DisableWifiConfiguration;

	@FindBy(id = "systemSettingsDisableAppsInstallation")
	private WebElement CheckBox_DisableAppsInstallation;

	@FindBy(id = "systemSettingsDisableAccounts")
	private WebElement CheckBox_DisableAccounts;

	@FindBy(id = "systemSettingsDisableOutgoingCall")
	private WebElement CheckBox_DisableOutgoingCall;

	@FindBy(id = "systemSettingsDisableLocationSharing")
	private WebElement CheckBox_DisableLocationSharing;

	@FindBy(id = "systemSettingsDisableSMS")
	private WebElement CheckBox_DisableSMS;

	@FindBy(id = "systemSettingsDisableMicrophone")
	private WebElement CheckBox_DisableMicrophone;

	@FindBy(id = "systemSettingsDisableUSBFileTransfer")
	private WebElement CheckBox_DisableUSBFileTransfer;

	@FindBy(id = "systemSettingsDisableAddUser")
	private WebElement CheckBox_DisableAddUserr;

	@FindBy(id = "systemSettingsDisableRemoveUser")
	private WebElement CheckBox_DisableRemoveUser;

	@FindBy(id = "systemSettingsDisableDataRoaming")
	private WebElement CheckBox_DisableDataRoaming;

	@FindBy(id = "systemSettingsDisableNetworkReset")
	private WebElement CheckBox_DisableNetworkReset;

	@FindBy(id = "systemSettingsDisableOutgoingBeam")
	private WebElement CheckBox_DisableOutgoingBeam;

	@FindBy(id = "systemSettingsDisableWallpaper")
	private WebElement CheckBox_DisableWallpaper;

	@FindBy(xpath = ".//*[@id='applicationPolicyAndroidProfileTail']/ul/li/div/div[3]/div[3]/div/div")
	private WebElement MessageOnApplicationPolicyWindow;

	@FindBy(id = "addApplicationPolicyBtn")
	private WebElement AppPolicyAddButton;

	@FindBy(xpath = "//h4[text()='Select Application Source']")
	private WebElement SelectAppSourceWindow_Header;

	@FindBy(xpath = ".//*[@id='fileSystemModal']/div/div/div[1]/h4")
	private WebElement FileStoreWindow_Header;

	@FindBy(xpath = "//h4[text()='Enterprise App Store']")
	private WebElement EnterpriseAppWindow_Header;

	@FindBy(id = "addEnterpriseStoreApplicationBtn")
	private WebElement icon_SureMDMAppStore;

	@FindBy(id = "addSysApplicationPolicyBtn")
	private WebElement icon_ConfigureSystemApps;

	@FindBy(xpath = "//div[@id='enterpriseApplicationPopup']/div/div/div[2]/div[1]/div/a/div/b")
	private WebElement ClickOnAppName;

	@FindBy(xpath = ".//*[@id='add_new_app_to_store']")
	private WebElement AddNewApp;

	@FindBy(xpath = "//p[text()='New application first needs to be added to App Store. Would you like to add now?']")
	private WebElement WarningMessageOnAddNewPopUp;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement ClickOnNoButtonOnWarningDialog;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement ClickOnYesButtonOnWarningDialog;

	@FindBy(xpath = "//h4[text()='Select Options']")
	private WebElement SelectOptionWindow_Header;

	@FindBy(xpath = "//p[text()='Upload APK']")
	private WebElement ClickOnUploaAPK;

	@FindBy(xpath = "//p[text()='APK link']")
	private WebElement ClickOnAPKLink;

	@FindBy(xpath = "//span[text()='APK Link']")
	private WebElement APKLinkParameter;

	@FindBy(xpath = ".//*[@id='apkLinkMam']")
	private WebElement Enterapklink;

	@FindBy(id = "appApkLinAddBtn")
	private WebElement APKLinkAddButton;

	@FindBy(xpath = "//*[@id='addNewAppBtn']")
	private WebElement ClickOnAddNewAppButton;

	@FindBy(id = "appVersion")
	private WebElement app_version;

	@FindBy(xpath = ".//*[@id='UploadWebAppMam']/div")
	private WebElement ClickOnWebApp;

	@FindBy(xpath = ".//*[@id='appWebAppTitle']")
	private WebElement EnterWebAppTitle;

	@FindBy(xpath = ".//*[@id='appWebAppWebLink']")
	private WebElement EnterWebAppLink;

	@FindBy(xpath = ".//*[@id='appWebAppCategory']")
	private WebElement EnterWebAppCategory;

	@FindBy(xpath = ".//*[@id='appWebAppDescription']")
	private WebElement EnterWebAppDescription;

	@FindBy(id = "appWebAppAddBtn")
	private WebElement WebAppAddButton;

	@FindBy(id = "appWebAppBackBtn")
	private WebElement WebAppBackButton;

	@FindBy(xpath = "//span[text()='Web App Added Successfully.']")
	private WebElement ConfirmationMessageOnAddingWebApp;

	@FindBy(xpath = "//span[text()='App Added Successfully.']")
	private WebElement ConfirmationMessageOnAddingApk;

	@FindBy(xpath = "//span[text()='App has been already added!']")
	private WebElement WarningMessageOnAddingSameWebApp;

	@FindBy(xpath = ".//*[@id='AddNewAppPopup']/div/div/div[1]/button")
	private WebElement CloseSelectOptionsWindow;

	@FindBy(xpath = ".//*[@id='enterpriseApplicationPopup']/div/div/div[3]/button")
	private WebElement AddButton_EnterpriseAppStoreWindow;

	@FindBy(xpath = "//td[text()='https://es-file-explorer.en.uptodown.com/android/download']")
	private WebElement PackageNameofESFileExplorer;

	@FindBy(xpath = "//div[text()='Proxy Server and Port']")
	private WebElement ParameterOnSelecting_Manual;

	@FindBy(xpath = "//div[text()='Proxy PAC URL']")
	private WebElement ParameterOnSelecting_Auto;

	@FindBy(id = "globalproxyserver")
	private WebElement EnterproxyServer;

	@FindBy(id = "globalproxyport")
	private WebElement EnterproxyServerPort;

	@FindBy(id = "globalproxyPACUrl")
	private WebElement EnterProxyPACURL;

	@FindBy(id = "vpnSettingsPackageName")
	private WebElement EnterVPNPackageName;

	@FindBy(id = "vpnSettingsDisallowNetworkingWhenVpnOn")
	private WebElement DisableNetworkWhenVPNNotConnected;

	@FindBy(id = "emailMailConfigAndroid")
	private WebElement MailConfiguration_EmailAddress;

	@FindBy(id = "hostNameMailConfigAndroid")
	private WebElement MailConfiguration_HostName;

	@FindBy(id = "userNameConfigAndroid")
	private WebElement MailConfiguration_UserName;

	@FindBy(id = "deviceIdentifierMailConfigAndroid")
	private WebElement MailConfiguration_DeviceIdentifier;

	@FindBy(xpath = "(//span[text()='Choose App'])[1]")
	private WebElement ChooseAppConfigSys;

	@FindBy(xpath = "(//*[@id='appList_chosen']/div/div/input)[1]")
	private WebElement SearchPlayStoreInChooseApps;

	@FindBy(xpath = "//em[text()='Chrome']")
	private WebElement ChromeFromDropDown;

	@FindBy(xpath = "//span[text()='Default']")
	private WebElement DefaultApppermission;

	@FindBy(xpath = "(//button[text()='Add'])[5]")
	private WebElement AddButtonCongifSys;

	@FindBy(id = "sslMailConfigAndroid_chosen")
	private WebElement MailConfiguration_SSLRequired;

	@FindBy(id = "'trustCertificateMailConfigAndroid_chosen")
	private WebElement MailConfiguration_TrustallCertificates;

	@FindBy(id = "loginCertificateMailConfigAndroid")
	private WebElement MailConfiguration_LoginCertificateAlias;

	@FindBy(id = "emailSignatureMailConfigAndroid")
	private WebElement MailConfiguration_DefaultEmailSignature;

	@FindBy(id = "syncWindowMailConfigAndroid")
	private WebElement MailConfiguration_DefaultSyncWindow;

	@FindBy(id = "ssidWifiConfigAndroid")
	private WebElement WifiConfiguration_SSID;

	@FindBy(id = "passwordWifiConfigAndroid")
	private WebElement WifiConfiguration_password;

	@FindBy(id = "securityTypeWifiConfigAndroid_chosen")
	private WebElement WifiConfiguration_securityTypeDropdown;

	@FindBy(id = "autoConnectWifiConfigAndroid")
	private WebElement WifiConfiguration_autoConnect;

	@FindBy(id = "hiddenNetworkWifiConfigAndroid")
	private WebElement WifiConfiguration_hiddenNetwork;

	@FindBy(xpath = "//*[@id='securityTypeWifiConfigAndroid_chosen']/a/span[text()='WPA PSK']")
	private WebElement DefauleValue_SecurityType;

	@FindBy(xpath = "//span[text()='Open']")
	private WebElement Open;

	@FindBy(xpath = "//span[text()='WEP']")
	private WebElement WEP;

	@FindBy(id = "certFileInputField")
	private WebElement certBrowsebutton;

	@FindBy(xpath = "//span[text()='File Sharing Policy']")
	private WebElement fileSharingPolicy;

	@FindBy(id = "android_filesharing_add")
	private WebElement ClickOnFileSharing_AddButton;

	@FindBy(xpath = ".//*[@id='policy_item_box']/div[4]/div/ul/li[5]") // span xpath does not work
	private WebElement ClickOnCertificatePolicy;

	@FindBy(xpath = ".//*[@id='certificateAddPopup']/div/div/div[3]/button[1]")
	private WebElement ClickOnAddButton_CertWindow;

	@FindBy(id = "android_cert_add_btn")
	private WebElement ClickOn_AddButton_CertificateWindow;

	@FindBy(xpath = "//td[text()='cert.p12']")
	private WebElement isCertpresent;

	@FindBy(id = "android_cert_edit_btn")
	private WebElement ClickOnCert_EditButton;

	@FindBy(id = "android_cert_delete_btn")
	private WebElement ClickOnCert_DeleteButton;

	@FindBy(xpath = ".//*[@id='certificateAddPopup']/div/div/div[3]/button[2]")
	private WebElement ClickOnCancelButtonOnCertficateWindow;

	@FindBy(xpath = "//h4[text()='Certificate']")
	private WebElement certficateWindow_Header;

	@FindBy(xpath = "//h4[text()='Profile Settings']")
	private WebElement profileSettings_WindowHeader;

	@FindBy(xpath = "//h4[text()='Store Layout']")
	private WebElement storeLayout_WindowHeader;

	@FindBy(xpath = "//h4[text()='Add Page Title']")
	private WebElement AddPageTitle_WindowHeader;

	@FindBy(id = "storeLayoutManagement_li")
	private WebElement StoreLayout;

	@FindBy(id = "appLicenseData_li")
	private WebElement ApplicationLicenses;

	@FindBy(id = "unenrol_li")
	private WebElement Unenroll;

	@FindBy(xpath = "//p[text()='Manage your Google Play for Work layout']")
	private WebElement summaryStoreLayout;

	@FindBy(xpath = "//p[text()='See the summary of your application licenses']")
	private WebElement summaryApplicationLicenses;

	@FindBy(xpath = "//p[text()='Unenroll your existing Google Managed Domain']")
	private WebElement summaryUnEnroll;

	@FindBy(xpath = ".//*[@id='playlayoutusingangularjs']/div/div/div[2]/div/div/div[2]/button[1]")
	private WebElement AddPage_Button;

	@FindBy(id = "btn_storelayout_deletepage")
	private WebElement DeletePage_Button;

	@FindBy(id = "txt_page_name_add")
	private WebElement EnterPageName;

	@FindBy(xpath = "//a[text()='Profiles']")
	private WebElement Profiles;

	@FindBy(id = "btn_page_name_add")
	private WebElement AddPageTitle_AddButton;

	@FindBy(id = "appLicenseData_li")
	private WebElement applicationLicense;

	@FindBy(xpath = "//h4[text()='App License Data']")
	private WebElement appLicenseWindow;

	@FindBy(xpath = ".//*[@id='license_appList_modal']/div/div/div[2]/div/div[1]/div[3]/div/div")
	private WebElement TextMessageInLicensWhenNoApplication;

	@FindBy(id = "unenrol_li")
	private WebElement UnenrollAFW;

	@FindBy(xpath = ".//*[@id='license_appList_modal']/div/div/div[1]/button")
	private WebElement CloseButton_AppLicensWindow;

	@FindBy(xpath = ".//*[@id='deleteConfPopup']/div/div/div[1]")
	private WebElement WarningMessageOnAFWUnenroll;

	@FindBy(xpath = ".//*[@id='deleteConfPopup']/div/div/div[2]/button[1]")
	private WebElement ClickOnNoButton_UnEnrollAFW;

	@FindBy(xpath = ".//*[@id='deleteConfPopup']/div/div/div[2]/button[2]")
	private WebElement ClickOnYES_Button_UnEnrollAFW;

	@FindBy(xpath = "//span[text()='Successfully unregistered EMM account']")
	private WebElement ConformationNotificationOn_UnEnrollAFWAccount;

	@FindBy(xpath = ".//*[@id='fileSystemModal']/div/div/div[1]/button")
	private WebElement CloseFileSharingWindow;

	@FindBy(xpath = "//p[text()='@#$PRAC7267(*)']")
	private WebElement SelectPassswordPolicyProfile;

	@FindBy(xpath = "(.//*[@id='v1_UmVUZXN0Rm9sZGVyXA2'])[1]")
	private WebElement SelectingFolder;

	@FindBy(xpath = ".//*[@id='v1_SW1hZ2UuanBn0']/div[1]/div")
	private WebElement SelectingImg_file;

	@FindBy(xpath = "(.//*[@id='v1_UmVUZXN0Rm9sZGVyXA2'])[1]")
	private WebElement SelectingAudio_File;

	@FindBy(id = "fileSystemModal_addbtn")
	private WebElement selectingAddBotton;

	@FindBy(id = "fileSystemModal_savebtn")
	private WebElement doneBotton;

	@FindBy(xpath = ".//div[text()='Video.mp4']")
	private WebElement SelectingVideo_File;

	@FindBy(xpath = ".//div[text()='File.xlsx']")
	private WebElement Selectingxl_File;

	@FindBy(xpath = ".//div[text()='Audio.mp3']")
	private WebElement SelectingAudio;

	@FindBy(xpath = "(//div[text()='Game.apk'])[1]")
	private WebElement SelectingApk;

	@FindBy(xpath = ".//*[@id='v1_VGVzdEZvbGRlclw1']/div[2]")
	private WebElement NestedFolder;

	@FindBy(id = "systemSettingsEnableAllSystemApps")
	private WebElement Checkbox_EnableAllSystemApps;

	public void SelectApk() {
		SelectingApk.click();
	}

	public void SelectingXl_file() {
		Selectingxl_File.click();
	}

	public void SelectingAudio_File() {
		SelectingAudio.click();
	}

	public void select_video() throws InterruptedException {
		SelectingVideo_File.click();
		sleep(2);
	}

	public void Select_IMG() throws InterruptedException {
		SelectingImg_file.click();
		sleep(2);
	}

	public void SelectFolder() throws InterruptedException {

		SelectingFolder.click();
	}

	public void SelectNestedFolder() {
		NestedFolder.click();
	}

	public void ClickOnAddBottonAfterselectingfile() throws InterruptedException {
		selectingAddBotton.click();
		sleep(3);
	}

	public void ClickOnDone() throws InterruptedException {
		doneBotton.click();
		sleep(5);
		waitForidPresent("policyName");
		sleep(3);
	}

	public void clickOnProfileTextfield(String ProfileName) throws InterruptedException {

		EnterPolicyName.sendKeys(ProfileName);
	}

	public void clickOnSave() throws InterruptedException {
		saveButton.click();
		sleep(3);
	}

	@FindBy(xpath = "//p[text()='" + Config.ApplicationSettingsProfileName + "']")
	private WebElement ChooseTheProfileToAppstore;

	public void ChooseApplicationSettingProfile() throws InterruptedException {
		ChooseTheProfileToAppstore.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile1 + "']")
	private WebElement ChooseTheProfile1ToAppstore1;

	public void ChooseApplicationSettingProfile1() throws InterruptedException {
		ChooseTheProfile1ToAppstore1.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile2 + "']")
	private WebElement ChooseTheProfile1ToAppstore2;

	public void ChooseApplicationSettingProfile2() throws InterruptedException {
		ChooseTheProfile1ToAppstore2.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile3 + "']")
	private WebElement ChooseTheProfile1ToAppstore3;

	public void ChooseApplicationSettingProfile3() throws InterruptedException {
		ChooseTheProfile1ToAppstore3.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile4 + "']")
	private WebElement ChooseTheProfile1ToAppstore4;

	public void ChooseApplicationSettingProfile4() throws InterruptedException {
		ChooseTheProfile1ToAppstore4.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile5 + "']")
	private WebElement ChooseTheProfile1ToAppstore5;

	public void ChooseApplicationSettingProfile5() throws InterruptedException {
		ChooseTheProfile1ToAppstore5.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile6 + "']")
	private WebElement ChooseTheProfile1ToAppstore6;

	public void ChooseApplicationSettingProfile6() throws InterruptedException {
		ChooseTheProfile1ToAppstore6.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile7 + "']")
	private WebElement ChooseTheProfile1ToAppstore7;

	public void ChooseApplicationSettingProfile7() throws InterruptedException {
		ChooseTheProfile1ToAppstore7.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.Appstore_Profile8 + "']")
	private WebElement ChooseTheProfile1ToAppstore8;

	public void ChooseApplicationSettingProfile8() throws InterruptedException {
		ChooseTheProfile1ToAppstore8.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.SystemSettingsProfileName + "']")
	private WebElement ChooseTheProfileSystemSettingsLenovo;

	public void ChooseSystemSettingProfile() throws InterruptedException {
		ChooseTheProfileSystemSettingsLenovo.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.SystemSettingsProfileName1 + "']")
	private WebElement ChooseTheProfileSystemSettingsSamsung;

	public void ChooseSystemSettingProfilesamsung() throws InterruptedException {
		ChooseTheProfileSystemSettingsSamsung.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='" + Config.SystemSettingsProfileName3 + "']")
	private WebElement ChooseTheProfileSystemSettingskiosk;

	public void ChooseSystemSettingProfilekiosk() throws InterruptedException {
		ChooseTheProfileSystemSettingskiosk.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='Automation SystemSettingsProfile SamsungBYOD']")
	private WebElement ChooseTheProfileSystemSettingsSamsungBYOD;

	public void ChooseSystemSettingProfileBYOD() throws InterruptedException {
		ChooseTheProfileSystemSettingsSamsungBYOD.click();
		sleep(2);
	}

	public void SelectSearchedJoborProfile() {
		List<WebElement> searchedJobOrProfile = Initialization.driver
				.findElements(By.xpath(".//*[@id='applyJobDataGrid']/tbody/tr/td[2]/p"));
		searchedJobOrProfile.get(0).click();
	}

	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	public void ClickOnProfiles() throws InterruptedException {
		profilesOption.click();
		waitForidPresent("addPolicyBtn");
		sleep(4);

	}

	public void commonScrollClick(String FileNameText) throws InterruptedException {
		Initialization.driverAppium.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + FileNameText + "\").instance(0))")).click();
	}
	
	 public void clickOnDeleteOKBUTTON() throws InterruptedException {
			Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK']").click();
			sleep(2);
		}
		
	 
	public void LongPressOnFolder(String FolderName) throws InterruptedException {
		sleep(10);
		WebElement FolderElement = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]");
		TouchAction action = new TouchAction(Initialization.driverAppium).longPress(longPressOptions().withElement(element(FolderElement)).withDuration(Duration.ofMillis(10000))).release().perform();
		action.longPress(longPressOptions().withElement(element(FolderElement)).withDuration(ofSeconds(2))).release().perform();	
	}
	
	public void verifyFolderIsDownloadedInDevice(String DownloadedFolder) throws InterruptedException   {
		
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.ImageView[@content-desc='Storage']").click();
		sleep(10);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Android']").click();
		sleep(4);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='data']").click();
		commonScrollClick("com.nix");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='files']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='EFSS']").click();
		sleep(1);
//		Initialization.driverAppium.findElementByXPath("//android.view.ViewGroup[@index='2']").click();//android.view.ViewGroup
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='012100052']").click();  //android.view.ViewGroup
        sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='File Store']").click();
		try {
			boolean DownloadedFolderValue = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+DownloadedFolder+"']").isDisplayed();
			String PassStatement1 = "PASS >> Folder is Present ";
			String FailStatement1 = "FAIL >>Folder is not Present ";
			ALib.AssertTrueMethod(DownloadedFolderValue,PassStatement1, FailStatement1);}
		catch(Exception e) {
			ALib.AssertFailMethod("Folder is not displayed");
		}
	}
	
	public void ClickOnProfiles_Nav() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, profilesOption);
		profilesOption.click();
		sleep(2);
	}

	public void verifyFolderOnDevice(String FolderName) throws InterruptedException   {
		sleep(10);
		boolean folderIsDisplayed = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").isDisplayed();
		String PassStatement1 = "PASS >> Folder is Present ";
		String FailStatement1 = "FAIL >>Folder is not Present ";
		ALib.AssertTrueMethod(folderIsDisplayed,PassStatement1, FailStatement1);
	}
	
	// common method
	public void IsAddButtonDisplayed() throws InterruptedException {
		boolean value = Addbutton.isDisplayed();
		String PassStatement = "PASS >> 'Add button' is displayed";
		String FailStatement = "FAIL >> 'Add button' is NOT displayed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	/*
	 * public void IsEnrollAFWPresent(){
	 * 
	 * boolean isPresent; enrollAFWbutton.isDisplayed(); if(isPresent=true){
	 * enrollAFWbutton.click(); } WebDriverWait wait = new
	 * WebDriverWait(Initialization.driver, 10);
	 * wait.until(ExpectedConditions.visibilityOf(EnrollButton)); Boolean
	 * Actualvalue = AFWEnrollmentWindow.isDisplayed(); String PassStatement1 =
	 * "PASS >> 'AFW Enrollment' window  displayed successfully"; String
	 * FailStatement1 =
	 * "FAIL >> 'AFW Enrollment' window NOT displayed successfully";
	 * ALib.AssertTrueMethod(Actualvalue,PassStatement1, FailStatement1); //verify
	 * if two options are present
	 * 
	 * List<WebElement> list = Initialization.driver.findElements(By.xpath(
	 * ".//*[@id='enroll_AFW_pop']/div/div/div[2]/div/div/span[1]")); for(int
	 * i=0;i<2;i++){ if(i==0){ String actual = list.get(i).getText(); String
	 * expected = "Enroll Using Your Gmail ID"; String PassStatement
	 * ="PASS >> 1st option is present - correct"; String FailStatement
	 * ="FAIL >> 1st option is NOT present - incorrect";
	 * ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement); }
	 * if(i==1){ String actual = list.get(i).getText(); String expected =
	 * "Enroll Using Your G Suite Account"; String PassStatement
	 * ="PASS >> 2nd option is present - correct"; String FailStatement
	 * ="FAIL >> 2nd option is NOT present - incorrect";
	 * ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement); }
	 */

	public void isEmptyTextPresent() {
		Reporter.log("====Verify the message when No profile is created=========");
		String text1 = profileIsEmptyText.getText();
		Reporter.log(text1);
		String text2 = ClickAddToCreateANewProfile.getText();
		Reporter.log(text2);
		boolean v1 = profileIsEmptyText.isDisplayed() && ClickAddToCreateANewProfile.isDisplayed();
		String PassStatement1 = "PASS >> Empty Message Text displayed successfully";
		String FailStatement1 = "FAIL >> Empty Message Text NOT displayed successfully";
		ALib.AssertTrueMethod(v1, PassStatement1, FailStatement1);
	}

	public void AddProfile() throws InterruptedException {

		Addbutton.click();
		sleep(4);

	}

	public void ClickOnPasswordPolicy() {
		ClickOnPasswordPolicy.click();
	}

	public void VerifyPasswordPolicySaveWithoutProfileName() throws InterruptedException {
		saveButton.click();
		sleep(2);
		boolean value = true;

		try {
			warningMessageOnSavingWithoutProfileName.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Please enter a Profile Name.'is displayed", true);
		sleep(3);
	}

	public void VerifyPasswordPolicyEnter1stProfileName() throws InterruptedException {
		EnterPolicyName.sendKeys(Config.FirstProfileName);
		sleep(2);

	}

	public void VerifyPasswordPolicyEnter2ndProfileName() throws InterruptedException {
		EnterPolicyName.sendKeys(Config.SecondProfileName);
		sleep(2);

	}

	public void VerifyPasswordPolicySaveWithoutProfileNameAndPayload() throws InterruptedException {
		saveButton.click();
		sleep(2);
		boolean value = true;

		try {
			warningMessageOnSavingWithoutPayload.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Please add at least one payload to save the profile.'is displayed", true);
		sleep(3);
	}

	public void ClickOnPasswordPolicyConfigButton() throws InterruptedException {
		passwordPolicyConfigurebutton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(PasswordPolicyText));
		boolean value = PasswordPolicyText.isDisplayed();
		String pass = "PASS >> 'Password Policy' page displayed successfully";
		String fail = "FAIL >> 'Password Policy' page NOT displayed successfully";
		ALib.AssertTrueMethod(value, pass, fail);
		
		sleep(4);

	}

	public void VerifyPasswordPolicyParameters() {

		// Profile Type is newly added need to write
		Reporter.log("======Verify Total parameter count of Password Policy======");
		List<WebElement> parameters = Initialization.driver
				.findElements(By.xpath(".//*[@id='passwordPolicyParentProfile']/ul/li/div[1]"));
		// verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 6;
		String PassStatement2 = "PASS >> Total parameter count is: " + expectedParameters + "  ";
		String FailStatement2 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		// verifying parameters one by one
		Reporter.log("========Verify Password Policy parameters one by one=========");
		for (int i = 0; i < totalParameters; i++) {
			if (i == 0) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Minimum Password Quality";
				String PassStatement = "PASS >> 1st option is present - correct";
				String FailStatement = "FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 1) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Minimum Password Length";
				String PassStatement = "PASS >> 2nd option is present - correct";
				String FailStatement = "FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 2) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Maximum Failed Attempts";
				String PassStatement = "PASS >> 3rd option is present - correct";
				String FailStatement = "FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 3) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Maximum Password Age (In hours)";
				String PassStatement = "PASS >> 4th option is present - correct";
				String FailStatement = "FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 4) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Enforce Password History";
				String PassStatement = "PASS >> 5th option is present - correct";
				String FailStatement = "FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 5) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Maximum Time To Lock (In seconds)";
				String PassStatement = "PASS >> 6th option is present - correct";
				String FailStatement = "FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
		}
	}

	public void VerifyAllOptionsOfMinimumPasswordQuality()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify if all the options present in Minimum Password Quality dropdown ====");
		// verify if all the options present in dropdown
		for (int i = 0; i < 6; i++) // total 6 options
		{
			if (i == 0) {
				Initialization.driver.findElement(By.id("passwordPolicyQuality_chosen")).click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 60, 2) + "']"))
						.click();
				;
				sleep(1);
				boolean value = Unspecified.isDisplayed();
				String pass = "PASS >> 'UnSpecified' is available";
				String fail = "FAIL >> 'Unspecified' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if (i == 1) {
				Initialization.driver.findElement(By.id("passwordPolicyQuality_chosen")).click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 60, 2) + "']"))
						.click();
				;
				sleep(1);
				boolean value = Alphabetic.isDisplayed();
				String pass = "PASS >> 'Alphabetic' is available";
				String fail = "FAIL >> 'Alphabetic' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if (i == 2) {
				Initialization.driver.findElement(By.id("passwordPolicyQuality_chosen")).click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 60, 2) + "']"))
						.click();
				;
				sleep(1);
				boolean value = Alphanumeric.isDisplayed();
				String pass = "PASS >> 'Alphanumeric' is available";
				String fail = "FAIL >> 'Alphanumeric' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if (i == 3) {
				Initialization.driver.findElement(By.id("passwordPolicyQuality_chosen")).click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 60, 2) + "']"))
						.click();
				;
				sleep(1);
				boolean value = Numeric.isDisplayed();
				String pass = "PASS >> 'Numeric' is available";
				String fail = "FAIL >> 'Numeric' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}
			if (i == 4) {
				Initialization.driver.findElement(By.id("passwordPolicyQuality_chosen")).click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 60, 2) + "']"))
						.click();
				;
				sleep(1);
				boolean value = Complex.isDisplayed();
				String pass = "PASS >> 'Complex' is available";
				String fail = "FAIL >> 'Complex' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}

			if (i == 5) {
				Initialization.driver.findElement(By.id("passwordPolicyQuality_chosen")).click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 60, 2) + "']"))
						.click();
				;
				sleep(1);
				boolean value = Any.isDisplayed();
				String pass = "PASS >> 'Any' is available";
				String fail = "FAIL >> 'Any' is NOT available";
				ALib.AssertTrueMethod(value, pass, fail);
			}
		}

	}

	public void ChooseUnspecified()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Initialization.driver.findElement(By.id("passwordPolicyQuality_chosen")).click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 60, 2) + "']"))
				.click();
		;
		sleep(2);
	}

	public void EnterMinimumPasswordLength() {
		MinimumPasswordLength.sendKeys(Config.MiniumPasswordLength);
	}

	public void EnterMaximumFailedAttempts() {
		MaximumFailedAttempt.sendKeys(Config.MaximumFailedAttempts);
	}

	public void EnterMaximumPasswordAge() {
		passwordTimeout.sendKeys(Config.MaximumPasswordAge);
	}

	public void EnterPasswordHistoryLength() {
		passwordHistorylength.sendKeys(Config.PasswordHistoryLength);
	}

	public void EnterMaxiumumTimeToLock() {
		maxiumumTimeToLock.sendKeys(Config.maxiumumTimeToLock);
	}

	public void ClearMaximumTimeToLock() {
		maxiumumTimeToLock.clear();
	}

	public void ReEnterMaximumTimeToLock() {
		maxiumumTimeToLock.sendKeys(Config.ReEnterMaxiumumTimeToLock);
	}

	public void WarningMessageOnMaxTimeToLock() throws InterruptedException {

		boolean value = true;

		try {
			WarningMessageOnMaxTimeToLock.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Maximum time to lock can not be less than 10 second.'is displayed", true);
		sleep(4);

	}

	public void ClickOnSaveButton() throws InterruptedException {

		saveButton.click();
		sleep(3);
	}

	public void NotificationOnProfileCreated() throws InterruptedException {
		boolean value = true;

		try {
			NotificationOnProfileCreated.isDisplayed();

		} catch (Exception e) {
			value = false;

		}
		Assert.assertTrue(value, "FAIL >> Profile Created Notification not displayed ");
		Reporter.log("PASS >> Notification 'Profile created successfully.'is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);

	}

	public void NotificationOnProfileUpdate() throws InterruptedException {
		boolean value = true;

		try {
			NotificationOnProfileUpdated.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Profile Update Notification not displayed");
		Reporter.log("PASS >> Notification 'Profile updated successfully.'is displayed", true);
		sleep(4);
		waitForidPresent("addPolicyBtn");
	}

	public void IsFirstProfilePresent() throws InterruptedException {

		boolean value = FirstcreatedProfile.isDisplayed();
		String pass = "PASS >> 1st Profile Name  displayed - profile present ";
		String fail = "FAIL >> 2nd Profile Name  NOT displayed - profile Not Available";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(2);

	}

	public void Is2ndProfilePresent() {

		boolean value = SecondCreatedProfile.isDisplayed();
		String pass = "PASS >> Profile Name  displayed - profile present ";
		String fail = "FAIL >> Profile Name  NOT displayed - profile Not Available";
		ALib.AssertTrueMethod(value, pass, fail);

	}

	public void WarningMsgOnMinimumPasswordLength() throws InterruptedException {

		boolean value = true;

		try {
			WarningMessageOnPasswordLength.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Password length should be minimum 4.'is displayed", true);
		sleep(4);
	}

	public void ClearMinimumPasswordLength() {
		MinimumPasswordLength.clear();
	}

	public void ReEnterMinimumPasswordLength() {
		MinimumPasswordLength.sendKeys(Config.ReEnterMiniumPasswordLength);
	}

	public void WarningMsgOnPasswordhistory() throws InterruptedException {

		boolean value = true;

		try {
			WarningMsgOnPasswordHistory.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Enforce password history can not be more than 10.'is displayed", true);
		sleep(4);
	}

	public void ClearPasswordHistory() {
		passwordHistorylength.clear();
	}

	public void ReEnterPasswordHistory() {
		passwordHistorylength.sendKeys(Config.ReEnterPasswordHistoryLength);
	}

	public void WarningMsgOnFailedAttempts() throws InterruptedException {

		boolean value = true;

		try {
			WarningMsgOnMaxFailedAttempts.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Maximum failed attempts can not be more than 6.'is displayed", true);
		sleep(4);
	}

	public void ClearMaxiumFailedAttempts() {
		MaximumFailedAttempt.clear();
	}

	public void ReEnterMaxFailedAttempts() {
		MaximumFailedAttempt.sendKeys(Config.ReEnterMaximumFailedAttempts);
	}

	@FindBy(id = "passwordPolicyProfileType_chosen")
	private WebElement profiletype_dropdown;

	public void VerifyProfiletypeDropDownPasswordpolicy() throws InterruptedException {
		profiletype_dropdown.click();
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath("//*[@id='passwordPolicyProfileType_chosen']/div/ul/li"));
		for (int i = 0; i < ls.size(); i++) {
			if (i == 0) {
				String Actual = ls.get(i).getText();
				String Expected = "Device Security";
				String pass = "PASS >> 'Device Security' 1st Option present";
				String fail = "FAIL >> 'Device Security' 1st Option Not present";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}
			if (i == 1) {
				String Actual = ls.get(i).getText();
				String Expected = "Work Security";
				String pass = "PASS >> 'Work Security' 2nd Option present";
				String fail = "FAIL >> 'Work Security' 2nd Option Not present";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}
			if (i == 2) {
				String Actual = ls.get(i).getText();
				String Expected = "Device & Work Security";
				String pass = "PASS >> 'Device & Work Security' 3rd Option present";
				String fail = "FAIL >> 'Device & Work Security' 3rd Option Not present";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}
		}
		Initialization.driver.findElement(By.xpath("//*[@id='passwordPolicyProfileType_chosen']/div/ul/li[2]")).click();
		sleep(2);
	}

	public void ClickOnSystemSettingsPolicy() {
		Helper.highLightElement(Initialization.driver, systemSettingsPolicyOption);
		systemSettingsPolicyOption.click();
	}

	public void VerifyEditingTheFirstProfile() throws InterruptedException {
		FirstcreatedProfile.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(EditProfileButton));
		EditProfileButton.click();
		waitForidPresent("saveAfwProfile");
		sleep(2);

		String actual = MinimumPasswordLength.getAttribute("value");
		Reporter.log(actual);
		String Expected = "4";
		String PassStatement = "PASS >> Minimum Password Length value " + Expected + " is retained on edit";
		String FailStatement = "FAIL >> Minimum Password Length value NOT retained on edit";
		ALib.AssertEqualsMethod(Expected, actual, PassStatement, FailStatement);

		// verifying Maximum Failed Attempt value retention after edit
		String actual1 = MaximumFailedAttempt.getAttribute("value");
		Reporter.log(actual1);
		String Expected1 = "5";
		String PassStatement1 = "PASS >> Maximum Failed Attempt value: " + Expected1 + " is retained on edit";
		String FailStatement1 = "FAIL >> Maximum Failed Attempt value NOT retained on edit";
		ALib.AssertEqualsMethod(Expected1, actual1, PassStatement1, FailStatement1);

		// verifying password Policy Expiration Timeout value retention after edit
		String actual2 = passwordTimeout.getAttribute("value");
		Reporter.log(actual2);
		String Expected2 = "2";
		String PassStatement2 = "PASS >> password Policy Expiration Timeoutt value: " + Expected2
				+ " is retained on edit";
		String FailStatement2 = "FAIL >> password Policy Expiration Timeout value NOT retained on edit";
		ALib.AssertEqualsMethod(Expected2, actual2, PassStatement2, FailStatement2);

		// verifying password history length value retention after edit
		String actual3 = passwordHistorylength.getAttribute("value");
		Reporter.log(actual3);
		String Expected3 = "8";
		String PassStatement3 = "PASS >> password History length value: " + Expected3 + " is retained on edit";
		String FailStatement3 = "FAIL >> password History length value NOT retained on edit";
		ALib.AssertEqualsMethod(Expected3, actual3, PassStatement3, FailStatement3);

		// verifying maximum Time To Lock value retention after edit
		String actual4 = maxiumumTimeToLock.getAttribute("value");
		Reporter.log(actual3);
		String Expected4 = "10";
		String PassStatement4 = "PASS >> maximum Time To Lock value: " + Expected4 + " is retained on edit";
		String FailStatement4 = "FAIL >> maximum Time To Lock value NOT retained on edit";
		ALib.AssertEqualsMethod(Expected4, actual4, PassStatement4, FailStatement4);
	}

	public void VerifyRefreshingEMMWindow() {
		EMMWindowRefresh.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(Addbutton));

	}

	public void ClickOnSetDefaultProfile() throws InterruptedException {
		Reporter.log("Clicking On Set As Default profile");
		FirstcreatedProfile.click();
		SetAsDefault.click();
		sleep(2);
	}

	public void NotificationOnSetAsDefaultprofile() throws InterruptedException {
		Reporter.log("Notification on Set as Default Profile");
		sleep(2);
		boolean value = true;

		try {
			SetDefaultProfileNotification.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> No notification came - Set Default profile failed");
		Reporter.log("PASS >> Notification 'Changed default profile'is displayed", true);
		sleep(4);
	}

	public void DefaultMark() {
		boolean value = DefaultMark.isDisplayed();
		String pass = "PASS >> Default Mark is Visible - profile set as 'Default' ";
		String fail = "FAIL >> Default Mark NOT Visible - profile NOT set as 'Default'";
		ALib.AssertTrueMethod(value, pass, fail);

	}

	public void isRemoveDefaultVisible() throws InterruptedException {
		DeviceWithDefaultMark.click();
		sleep(2);
		String actual = RemoveDefaultOption.getText();
		String expected = "Remove Default";
		String pass = "PASS >> Set as default button changed to 'Remove default'successfully";
		String fail = "FAIL >> Set as default button NOT changed to 'Remove default' ";
		ALib.AssertEqualsMethod(expected, actual, pass, fail);
	}

	public void RemovingDefaultProfile() throws InterruptedException {
		RemoveDefaultOption.click();
		sleep(2);
	}

	public void NotificationOnRemovingDefaultProfile() throws InterruptedException {
		boolean value = true;

		try {
			NotificationOnRemovingDefaultProfile.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> 'Removed Default profile' Notification not displayed ");
		Reporter.log("PASS >> Notification 'Removed default profile 'is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
	}

	public void IsSettingsButtonAvailable() {
		Reporter.log("====Verify if Settings option is available on EMM window=====");
		boolean value = isSettingsButtonVisible.isDisplayed();
		String pass = "PASS >> 'Settings' button is available on EMM window";
		String fail = "FAIL >> 'Settings' button is NOT available on EMM window";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void EnterSystemSettingsProfileName(String Profilename) throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(Profilename);
		sleep(2);

	}

	public void ClickOnSystemSettingPolicyConfigButton() {
		Helper.highLightElement(Initialization.driver, systemSettingsPolicyConfigurebutton);
		systemSettingsPolicyConfigurebutton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(PasswordPolicyText));
		boolean value = SystemSettingsText.isDisplayed();
		String pass = "PASS >> 'System Settings' page displayed successfully";
		String fail = "FAIL >> 'System Settings' page NOT displayed successfully";
		ALib.AssertTrueMethod(value, pass, fail);

	}

	// Verify all parameters of System Settings

	public void VerifySystemSettingsParameters() {
		Reporter.log("======Verify Total parameter count of System Settings Policy======");

		List<WebElement> parameters = Initialization.driver
				.findElements(By.xpath(".//*[@id='systemsettingsAndroidProfileTail']/ul/li/div[1]"));
		// verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 12;
		String PassStatement2 = "PASS >> Total System Settings parameter count is: " + expectedParameters + "  ";
		String FailStatement2 = "FAIL >> Total System Settings parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		// verifying parameters one by one
		Reporter.log("========Verify System Policy parameters one by one=========");

		for (int i = 0; i < totalParameters; i++) {
			if (i == 0) {

				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Functionality";
				String PassStatement = "PASS >> 1st option is present - correct";
				String FailStatement = "FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 1) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Sync and Storage";
				String PassStatement = "PASS >> 3rd option is present - correct";
				String FailStatement = "FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 2) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Application(s)";
				String PassStatement = "PASS >> 4th option is present - correct";
				String FailStatement = "FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
	}

	public void VerifyDefaultValueOfDisableSafeMode() {
		Helper.highLightElement(Initialization.driver, SysSettingsDisableSafemode_CheckBox);
		boolean isEnabled = SysSettingsDisableSafemode_CheckBox.isEnabled();
		String pass = " PASS >> 'Disable Safe Mode' checkbox is enabled by default";
		String fail = " FAIL >> 'Disable Safe Mode' checkbox is NOT enabled by default";
		ALib.AssertTrueMethod(isEnabled, pass, fail);
	}

	public void VerifyDefaultValueOf_DisableCrossProfileCopyPaste() {
		Helper.highLightElement(Initialization.driver, DisableCrossProfileCopyPaste_CheckBox);
		boolean isEnabled = DisableCrossProfileCopyPaste_CheckBox.isEnabled();
		String pass = " PASS >> 'Disable Cross Profile Copy Paste_CheckBox' checkbox is enabled by default";
		String fail = " FAIL >> 'Disable Cross Profile Copy Paste_CheckBox' checkbox is NOT enabled by default";
		ALib.AssertTrueMethod(isEnabled, pass, fail);
	}

	// Verify Default value of 'Default Application Permission', 'Unknown
	// Source','USB Debugging',System Update Policy

	public void VerifyDefaultValuesof_AppPermission_UnknownSources_USBDebugging_SystemUpdatePolicy() {

		for (int i = 0; i < 4; i++) {
			if (i == 0) {
				Reporter.log(
						"=====Verify Default value of 'Default Application Permission', 'Unknown Source','USB Debugging',System Update Policy======");
				Helper.highLightElement(Initialization.driver, DefauleValue_DefaultApplicationPermission);
				boolean isDisplayed = DefauleValue_DefaultApplicationPermission.isDisplayed();
				String pass = "PASS >> Default value of 'Default Application Permission': Prompt";
				String fail = "PASS >> Default value of 'Default Application Permission' is NOT displayed";
				ALib.AssertTrueMethod(isDisplayed, pass, fail);
			}
			if (i == 1) {
				Helper.highLightElement(Initialization.driver, DefauleValue_UnknownSources);
				boolean isDisplayed = DefauleValue_UnknownSources.isDisplayed();
				String pass = "PASS >> Default value of 'Unknown Sources' :Disable";
				String fail = "PASS >> Default value of 'Unknown Sources' is NOT displayed";
				ALib.AssertTrueMethod(isDisplayed, pass, fail);
			}
			if (i == 2) {
				Helper.highLightElement(Initialization.driver, DefauleValue_USBDebugging);
				String actual = DefauleValue_USBDebugging.getText();
				String expected = "Disable";
				String pass = "PASS >> Default value of 'USB Debugging' :Disable";
				String fail = "PASS >> Default value of 'USB Debugging' is NOT displayed";
				ALib.AssertEqualsMethod(expected, actual, pass, fail);
			}
			if (i == 3) {
				Helper.highLightElement(Initialization.driver, DefauleValue_SystemUpdatePolicy);
				boolean isDisplayed = DefauleValue_SystemUpdatePolicy.isDisplayed();
				String pass = "PASS >> Default value of 'System Update Policy' :Don't Care";
				String fail = "PASS >> Default value of 'System Update Policy' is NOT displayed";
				ALib.AssertTrueMethod(isDisplayed, pass, fail);
			}
		}
	}
	// verify options present in Default Application Permission.

	public void VerifyOptionsOf_DefaultApplicationPermissionDropDown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify if all the options present in 'Default Application Permission' dropdown ====");
		// verify if all the options present in DefaultApplicationPermission dropdown
		Helper.highLightElement(Initialization.driver, prompt);
		boolean val = prompt.isDisplayed();
		String pass = "PASS >> 'Prompt' is available";
		String fail = "FAIL >> 'Prompt' is NOT available";
		ALib.AssertTrueMethod(val, pass, fail);
		for (int i = 0; i < 2; i++) {
			if (i == 0) {
				Helper.highLightElement(Initialization.driver, Choose_AppPolicyDefaultPermssion);
				Choose_AppPolicyDefaultPermssion.click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 127, 2) + "']"))
						.click();
				;
				sleep(1);
				Helper.highLightElement(Initialization.driver, Grant);
				boolean value = Grant.isDisplayed();
				String pass1 = "PASS >> 'Grant' is available";
				String fail1 = "FAIL >> 'Grant' is NOT available";
				ALib.AssertTrueMethod(value, pass1, fail1);
			}

			if (i == 1) {
				Helper.highLightElement(Initialization.driver, Choose_AppPolicyDefaultPermssion);
				Choose_AppPolicyDefaultPermssion.click();
				Initialization.driver
						.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", i + 127, 2) + "']"))
						.click();
				;
				sleep(1);
				Helper.highLightElement(Initialization.driver, Deny);
				boolean value = Deny.isDisplayed();
				String pass1 = "PASS >> 'Deny' is available";
				String fail1 = "FAIL >> 'Deny' is NOT available";
				ALib.AssertTrueMethod(value, pass1, fail1);
			}
		}
		Choose_AppPolicyDefaultPermssion.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 127, 2) + "']"))
				.click();

	}

	public void EnableSystemApps() {
		Checkbox_EnableAllSystemApps.click();
	}

	// verify options present in Unknown Sources.

	public void VerifyOptionsOf_UnknownSources_DropDown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify if all the options present in 'Unknown Sources' dropdown ====");
		// verify if all the options present in unknown sources DropDown
		Helper.highLightElement(Initialization.driver, DefauleValue_UnknownSources);
		boolean val = DefauleValue_UnknownSources.isDisplayed();
		String pass = "PASS >> 'Disable' is available";
		String fail = "FAIL >> 'Disable' is NOT available";
		ALib.AssertTrueMethod(val, pass, fail);
		for (int i = 0; i < 1; i++) {
			if (i == 0) {
				Helper.highLightElement(Initialization.driver, Choose_UnknownSourcesDropdown);
				Choose_UnknownSourcesDropdown.click();
				Initialization.driver.findElement(By.xpath("//li[text()='Enable']")).click();
				sleep(1);
				Helper.highLightElement(Initialization.driver, Enable);
				boolean value = Enable.isDisplayed();
				String pass1 = "PASS >> 'Enable' is available";
				String fail1 = "FAIL >> 'Enable' is NOT available";
				ALib.AssertTrueMethod(value, pass1, fail1);
			}
		}
	}

	@FindBy(id = "systemSettingsUSBDebugging_chosen")
	private WebElement USBDebuggChoosen;

	@FindBy(xpath = "//*[@id='systemSettingsUSBDebugging_chosen']/div/ul/li[2]")
	private WebElement USBEnable;

	public void USBDebuggingEnable() throws InterruptedException {
		USBDebuggChoosen.click();
		USBEnable.click();
		sleep(3);
	}

	public void VerifyOptionsOf_SystemUpdatePolicy_DropDown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify if all the options present in 'System Update Policy' dropdown ====");
		// verify if all the options present in system update policy DropDown
		Helper.highLightElement(Initialization.driver, DefauleValue_SystemUpdatePolicy);
		boolean val = DefauleValue_SystemUpdatePolicy.isDisplayed();
		String pass = "PASS >> 'Don't Care' is available";
		String fail = "FAIL >> 'Don't Care' is NOT available";
		ALib.AssertTrueMethod(val, pass, fail);
		for (int i = 0; i < 3; i++) {
			if (i == 0) {
				Helper.highLightElement(Initialization.driver, Choose_SystemUpdatePolicyDropDown);
				Choose_SystemUpdatePolicyDropDown.click();
				Initialization.driver.findElement(By.xpath("//li[text()='Automatic']")).click();
				Helper.highLightElement(Initialization.driver, Automatic);
				boolean value = Automatic.isDisplayed();
				String pass1 = "PASS >> 'Automatic' is available";
				String fail1 = "FAIL >> 'Automatic' is NOT available";
				ALib.AssertTrueMethod(value, pass1, fail1);
			}
			if (i == 1) {
				Helper.highLightElement(Initialization.driver, Choose_UnknownSourcesDropdown);
				Choose_SystemUpdatePolicyDropDown.click();
				Initialization.driver.findElement(By.xpath("//li[text()='Windowed']")).click();
				Helper.highLightElement(Initialization.driver, Windowed);
				boolean value = Windowed.isDisplayed();
				String pass1 = "PASS >> 'Windowed' is available";
				String fail1 = "FAIL >> 'Windowed' is NOT available";
				ALib.AssertTrueMethod(value, pass1, fail1);
			}
			if (i == 2) {
				Helper.highLightElement(Initialization.driver, Choose_UnknownSourcesDropdown);
				Choose_SystemUpdatePolicyDropDown.click();
				Initialization.driver.findElement(By.xpath("//li[text()='Postpone']")).click();
				Helper.highLightElement(Initialization.driver, Postpone);
				boolean value = Postpone.isDisplayed();
				String pass1 = "PASS >> 'Postpone' is available";
				String fail1 = "FAIL >> 'Postpone' is NOT available";
				ALib.AssertTrueMethod(value, pass1, fail1);
			}
		}
	}

	public void EnableUSBDebugging() throws InterruptedException {

		WebElement scrollToElement = Scroll;
		js.executeScript("arguments[0].scrollIntoView();", scrollToElement);
		Scroll.click();
		Initialization.driver.findElement(By.id("systemSettingsUSBDebugging_chosen")).click();
		sleep(3);
		USBEnable.click();
		sleep(2);

	}

	public void SelectChromeFromConfigSystem() throws InterruptedException {
		icon_ConfigureSystemApps.click();
		waitForXpathPresent("(//span[text()='Choose App'])[2]");
		sleep(3);
		ChooseAppConfigSys.click();
		sleep(3);
		SearchPlayStoreInChooseApps.sendKeys("Chrome");
		ChromeFromDropDown.click();
		sleep(5);
		Helper.highLightElement(Initialization.driver, DefaultApppermission);
		AllowInKioskModecheckbox.click();
		waitForXpathPresent("(//button[text()='Add'])[5]");
		AddButtonCongifSys.click();
		waitForidPresent("policyName");
		sleep(5);
	}

	@FindBy(xpath = "(//*[@id='allowInKioskMode'])[1]")
	private WebElement AllowInKioskModecheckbox;

	public void SelectParametersforBYOD() {
		CheckBox_DisableAccounts.click();
		CheckBox_DisableAppsInstallation.click();
		CheckBox_DisableLocationSharing.click();
		CheckBox_DisableAppsUninstallation.click();
	}

	// Enabling the required parameters of System Settings Policy
	public void SelectParametersofSystemSettings() {
		Reporter.log("======Enabling required checkboxes=====");

		CheckBox_DisableVolume.click();
		CheckBox_DisableFactoryReset.click();
		CheckBox_DisableWallpaper.click();

	}

	public void SelectParametersofSystemSettings1() {
		Reporter.log("======Enabling required checkboxes=====");
		CheckBox_DisableVolume.click();
		CheckBox_DisableFactoryReset.click();
		CheckBox_DisableWallpaper.click();

	}

	public void SelectAppPermisssionDeny()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Choose_AppPolicyDefaultPermssion.click();
		Initialization.driver
				.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 1 + 127, 2) + "']")).click();
		;
		sleep(1);
	}

	public void DeSelectParametersofSystemSettings() {
		Reporter.log("======Disabling required checkboxes=====");
		CheckBox_DisableVolume.click();
		CheckBox_DisableFactoryReset.click();
		CheckBox_DisableWallpaper.click();

	}

	public void DeSelectParametersofSystemSettingsSamsung() {
		Reporter.log("======Disabling required checkboxes=====");
		CheckBox_DisableVolume.click();
		CheckBox_DisableAppControl.click();
		CheckBox_DisableBluetoothConfiguration.click();
		CheckBox_DisableCredentialsConfiguration.click();
		CheckBox_DisableMTetheringConfiguration.click();
		CheckBox_DisableMobileNetworksConfiguration.click();
		CheckBox_DisableDisableVPNConfiguration.click();
		CheckBox_DisableWifiConfiguration.click();
		CheckBox_DisableAccounts.click();
		CheckBox_DisableSMS.click();
		CheckBox_DisableOutgoingCall.click();
		CheckBox_DisableLocationSharing.click();
		CheckBox_DisableMicrophone.click();
		CheckBox_DisableUSBFileTransfer.click();
		CheckBox_DisableFactoryReset.click();
		CheckBox_DisableNetworkReset.click();
		CheckBox_DisableWallpaper.click();
		CheckBox_DisableOutgoingBeam.click();

	}

	@FindBy(id = "systemSettingsKioskMode")
	private WebElement checkBox_KioskMode;

	public void EnableKioskMode() {
		checkBox_KioskMode.click();
	}

	@FindBy(id = "systemSettingsKioskExitPassword")
	private WebElement password_KioskMode;

	public void EnterKioskModeExitPassowrd() {
		password_KioskMode.sendKeys("0000");
	}

	public void IsSystemSettingsProfilePresent() {

		boolean value = systemSettingsCreatedProfile.isDisplayed();
		String pass = "PASS >> 'System Settings profile Name' displayed - profile created ";
		String fail = "FAIL >> 'System Settings profile Name' NOT displayed - profile Not Available";
		ALib.AssertTrueMethod(value, pass, fail);

	}

	@FindBy(xpath = "//td[text()='Automation SystemSettingsProfile']")
	private WebElement AutomationSystemSettingsProfile;

	public void SelectAutomationSystemSettingsprofile() throws InterruptedException {
		AutomationSystemSettingsProfile.click();
		EditProfileButton.click();
		waitForidPresent("passwordpolicyAndroidProfileConfig");
		sleep(2);
	}

	@FindBy(xpath = "//td[text()='Automation SystemSettingsProfile Samsung']")
	private WebElement AutomationSystemSettingsProfilesam;

	public void SelectAutomationSystemSettingsprofileSamsung() throws InterruptedException {
		AutomationSystemSettingsProfilesam.click();
		EditProfileButton.click();
		waitForidPresent("passwordpolicyAndroidProfileConfig");
		sleep(2);
	}

	// For Application Policy

	public void ClickOnApplicationSettingsPolicy() {
		Helper.highLightElement(Initialization.driver, applicationSettingsPolicyOption);
		applicationSettingsPolicyOption.click();

	}

	public void EnterApplicationSettingsProfileName() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(Config.ApplicationSettingsProfileName);
		sleep(2);

	}

	public void ClickOnApplicationSettingPolicyConfigButton() {
		Helper.highLightElement(Initialization.driver, applicationSettingsPolicyConfigurebutton);
		applicationSettingsPolicyConfigurebutton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(ApplicationPolicyText));
		boolean value = ApplicationPolicyText.isDisplayed();
		String pass = "PASS >> 'Application Settings' page displayed successfully";
		String fail = "FAIL >> 'Application Settings' page NOT displayed successfully";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void VerifyTextMessageOfApplicationPolicyWindow() {
		String textMessage = MessageOnApplicationPolicyWindow.getText();
		boolean value = MessageOnApplicationPolicyWindow.isDisplayed();
		String pass = "PASS >> Text Message displayed is : " + textMessage + "  ";
		String fail = "FAIL >> Text message displayed is wrong";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void VerifyClickingOnApplicationPolicyAddButtton() {
		AppPolicyAddButton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[text()='Select Application Source']")));
	}

	@FindBy(id = "android_filesharing_add")
	private WebElement FileAddButton;

	public void VerifyClickingOnFilsharingPolicyAddButtton() throws InterruptedException {
		FileAddButton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[text()='FileSharing12345'])[1]")));
		sleep(2);
	}

	public void verifyNESTEDFolderIsDownloadedInDevice(String DownloadedFolder) throws InterruptedException   {
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.ImageView[@content-desc='Storage']").click();
		sleep(10);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Android']").click();
		sleep(4);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='data']").click();
		commonScrollClick("com.nix");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='files']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='EFSS']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='012100052']").click();  //android.view.ViewGroup
        sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='File Store']").click();
		sleep(1);
        Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='NestedFolder01']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='NestedFolder02']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='NestedFolder03']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='NestedFolder04']").click();
		try {
			boolean DownloadedFolderValue = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+DownloadedFolder+"']").isDisplayed();
			String PassStatement1 = "PASS >> Folder is Present ";
			String FailStatement1 = "FAIL >>Folder is not Present ";
			ALib.AssertTrueMethod(DownloadedFolderValue,PassStatement1, FailStatement1);}
		catch(Exception e) {
			ALib.AssertFailMethod("Folder is not displayed");
		}
	}
	
	public void verifySharedFolderOnDevice(String FolderName) throws InterruptedException   {
		sleep(10);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").click();
		//Initialization.driverAppium.waitForXpathPresent("//android.widget.TextView[contains(@text,'"+Config.FileStore_SubFolder+"')]");
		sleep(4);
		boolean folderIsDisplayed = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").isDisplayed();
		String PassStatement1 = "PASS >> Folder is Present ";
		String FailStatement1 = "FAIL >>Folder is not Present ";
		ALib.AssertTrueMethod(folderIsDisplayed,PassStatement1, FailStatement1);
		sleep(6);
		
		}
	public void verifyFolderIsDownloadedInDeviceSUBfolders(String DownloadedFolder) throws InterruptedException   {
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.ImageView[@content-desc='Storage']").click();
		sleep(10);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Android']").click();
		sleep(4);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='data']").click();
		commonScrollClick("com.nix");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='files']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='EFSS']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='012100052']").click();  //android.view.ViewGroup
        sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='File Store']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Shared']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Folder1']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Folder2']").click();
		
		try {
			boolean DownloadedFolderValue = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+DownloadedFolder+"']").isDisplayed();
			String PassStatement1 = "PASS >> Folder is Present ";
			String FailStatement1 = "FAIL >>Folder is not Present ";
			ALib.AssertTrueMethod(DownloadedFolderValue,PassStatement1, FailStatement1);}
		catch(Exception e) {
			ALib.AssertFailMethod("Folder is not displayed");
		}
	}
	
	@FindBy(xpath="//*[@id='filesharingPolicyAndroidProfileRemoveTail']/span")
	private WebElement removeProfile;

	public void CliCkOnRemoveButton() throws InterruptedException
	{
		removeProfile.click();
		sleep(2);
	}
	
	@FindBy(id="systemSettingsBackBtn")
	private WebElement backButton;
	public void clickOnBackButton() throws InterruptedException
	{
		backButton.click();
		sleep(2);
	}
	
	public void verifyRemoveMessage() throws InterruptedException
	{
			Initialization.driver.findElement(By.xpath("//p[contains(text(),'Are you sure you want to remove the File Sharing Policy?')]")).isDisplayed();
			Initialization.driver.findElement(By.xpath("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")).click();
			clickOnBackButton();
		}
	
	@FindBy(xpath="//*[@id='fileSystemModal']/div/div/div[1]/button")
	private WebElement closeAddProfileWindow;
	public void VerifyFileAddedIsremoved(String FolderName) throws InterruptedException {
		try {
			if(Initialization.driver.findElement(By.xpath("(//span[contains(text(),'"+FolderName+"')])[1]/../i")).isDisplayed()) {
				closeAddProfileWindow.click();
				ALib.AssertFailMethod("Selected file is not removed");
			}
		}catch(Exception e){
			Reporter.log("Selected file is removed");
			closeAddProfileWindow.click();
		}
	}
	
	public void RemoveSelectedFile(String FolderName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("(//span[contains(text(),'"+FolderName+"')])[1]/../i")).click();
		sleep(2);	
	}
	
	public void verifyFolderNotOnDevice(String FolderName) throws InterruptedException   {
		sleep(10);try {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").isDisplayed();
	    }catch(Exception e) {
			Reporter.log("Folder is not present");
		}
	}
	
	public void VerifyAppStoreIsNotPresent() throws IOException, InterruptedException {
		sleep(2);
		try {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text=']").isDisplayed(); 
		}catch(Exception e){
			Reporter.log("App Store is not present");
		}
	}
	
	@FindBy(xpath="//*[@id=\"fileSystemModal\"]/div/div/div[1]/button")
	private WebElement CloseFileStoreProfile;
	public void verifyShareAnduserFolderNotPresent() throws InterruptedException{
		int NumberofFolderPresent = Initialization.driver.findElements(By.xpath("(//span[contains(text(),'File Store')]/..)[1]/div/div/span")).size();
		for(int i=1; i<=NumberofFolderPresent;i++) {
			String folderName = Initialization.driver.findElement(By.xpath("(//span[contains(text(),'File Store')]/..)[1]/div/div["+i+"]/span")).getText();
			if (folderName.equalsIgnoreCase("user")||folderName.equalsIgnoreCase("Shared")) {
				CloseFileStoreProfile.click();
				ALib.AssertFailMethod(folderName+" is present in Profile");
			}
		}
		CloseFileStoreProfile.click();
		sleep(10);
	}
	
	public void VerifyFileLoading() throws InterruptedException{
		boolean loadingPOPupDisplayed = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Adding to upload queue..')]").isDisplayed();
		String PassStatement1 = "PASS >> Folder is Present ";
		String FailStatement1 = "FAIL >>Folder is not Present ";
		ALib.AssertTrueMethod(loadingPOPupDisplayed,PassStatement1, FailStatement1);
		sleep(6);
	}
	
	public void commonScrollMethod(String NameText) throws InterruptedException {
		boolean Element=Initialization.driverAppium.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + NameText + "\").instance(0))")).isDisplayed();
		String pass = NameText+"PASS >> is displayed";
		String fail = NameText+" is not displayed";
		ALib.AssertTrueMethod(Element, pass, fail);
	}
	public void uploadFile(String FileNmame) throws InterruptedException {
		sleep(30);
		Initialization.driverAppium.findElementById("com.nix:id/imageViewUpload").click();
		sleep(3);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'sdcard')]").click();
		sleep(3);
		commonScrollMethod("Download");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'Download')]").click();
		sleep(3);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FileNmame+"')]").click();
		sleep(3);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[contains(@text,'UPLOAD')]").click();
	}
	
	public void VerifyFolderUploaded(String FolderName) {
		boolean folderIsDisplayed=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").isDisplayed();
		String PassStatement1 = "PASS >> Folder is Present ";
		String FailStatement1 = "FAIL >>Folder is not Present ";
		ALib.AssertTrueMethod(folderIsDisplayed,PassStatement1, FailStatement1);	
	}
	
	public void OnDeviceOnClickOnFolder(String FolderName) throws InterruptedException {
		sleep(10);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").click();
		sleep(10);
	} 
	
	public void verifySharedSubFolderOnDevice(String FolderName) throws InterruptedException   {
		sleep(10);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").click();
		sleep(2);
		boolean folderIsDisplayed = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+FolderName+"')]").isDisplayed();
		String PassStatement1 = "PASS >> subFolder is Present ";
		String FailStatement1 = "FAIL >>subFolder is not Present ";
		ALib.AssertTrueMethod(folderIsDisplayed,PassStatement1, FailStatement1);
		sleep(2);
		
		}
	
	public void ClickOnSaveApplicationPolicy(){
		saveButton.click();
	}
	
public void VerifyAppIsPresentInHometab(String AppName) throws IOException, InterruptedException {
		
		sleep(20);
		String[] ApplicationsName=AppName.split(",");
		for(String appName:ApplicationsName) {
			if(Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+appName+"')]").isDisplayed()) {
				Reporter.log(appName +" AppStore is displayed",true);
			}else {
				ALib.AssertFailMethod(appName+" App Store is not displayed");
			}
	}}

	public void ClickOnStoreTabInAppStore() throws IOException, InterruptedException {
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Store']").click();
		sleep(2);
	}
	
	public void ClickOnUpdateButton() throws IOException, InterruptedException {
		AppiumwaitForPresenceOfElement(Initialization.driverAppium, 200, "//android.widget.TextView[@text='Update']");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Update']").click();
		Reporter.log("Clicked on Update button",true);
		//AppiumwaitForPresenceOfElement(Initialization.driverAppium, 200, "//android.widget.TextView[@text='Uninstall']");
	}
	
	public void IsCountBadgeDisplayed() throws IOException, InterruptedException {
		AppiumwaitForPresenceOfElement(Initialization.driverAppium, 200, "//android.widget.TextView[@text='Update']");
		Initialization.driverAppium.findElementById("com.nix:id/count_txt").click();
		Reporter.log("A badge (with count) is displayed on Store Tab",true);
	}
	
	public void VerifyAppStoreUpdate() throws IOException, InterruptedException {
		AppiumwaitForPresenceOfElement(Initialization.driverAppium, 200, "//android.widget.TextView[@text='Update']");
		if(Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Update']").isDisplayed()) {
			Reporter.log("AppStore Update button is displayed",true);
		}else {
			ALib.AssertFailMethod("App Store Update button is not displayed");
		}
	}
	
	public void VerifyAppStore() throws IOException, InterruptedException {
		sleep(30);
		if(Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureMDM App Store']").isDisplayed()) {
			Reporter.log("AppStore is displayed",true);
		}else {
			ALib.AssertFailMethod("App Store is not displayed");
		}
	}
	
	public void SelectingTheCreatedAppDropdown(String AppStore) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='"+AppStore+"']")).click();
		sleep(2);
	}
	
	public void ClickOnEditSymbol(String App) throws InterruptedException{
		Initialization.driver.findElement(By.xpath("//div[p[text()='"+App+"']]/following-sibling::div/div/ul/li/a[text()='Edit']")).click();
		waitForidPresent("appTitleMam");
		sleep(5);
		Reporter.log("Clicked on Edit , Navigates successfully to Edit App Details Page",true);
	}
	
	public void AppiumwaitForPresenceOfElement(AndroidDriver<WebElement> Driver,int time,String Xpath){
		WebDriverWait wait = new WebDriverWait(Driver,time);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xpath)));
	}
	public void VerifyAppInstall(String AppName) throws IOException, InterruptedException {
		sleep(20);
		String[] ApplicationsName=AppName.split(",");
		for(String appName:ApplicationsName) {
			AppiumwaitForPresenceOfElement(Initialization.driverAppium, 200, "//android.widget.TextView[contains(@text,'"+appName+"')]");
			if(Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+appName+"')]").isDisplayed()) {
				Reporter.log(AppName +" AppStore is displayed",true);
			}else {
				ALib.AssertFailMethod("App Store is not displayed");
			}
		}
		sleep(2);
	}
	
	public void VerifyAppNotPresentInAppStore(String AppName) throws IOException, InterruptedException {
		sleep(20);
		String[] ApplicationsName=AppName.split(",");
		for(String appName:ApplicationsName) {
			try {
			if(Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@text,'"+appName+"')]").isDisplayed()) {
				
				ALib.AssertFailMethod("App is still Present in App Store");
			}}catch(Exception e) {
				Reporter.log(AppName +" AppStore is Not Present AnyMore",true);
			}
		}
		sleep(2);
	}
	
	public void ClickOnSureMDMAppStore() throws InterruptedException
	{
		icon_SureMDMAppStore.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[text()='Select Application Source']")));
		sleep(4);
		String Actualvalue = EnterpriseAppWindow_Header.getText();
		String Expectedvalue = "Enterprise App Store";
		String Pass = "PASS >> "+Expectedvalue+" windows  displayed successfully";
		String Fail = "FAIL >> "+Expectedvalue+ " window is not displayed" ;
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, Pass, Fail);
		sleep(2);
	}
	
	public void SelectFolder(String FolderName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[text()='"+FolderName+"']/..")).click();
		sleep(2);
	}
	
	public void IsSelectApplicationSourceWindowAndItsIconsDisplayed() {
		String Actualvalue = SelectAppSourceWindow_Header.getText();
		String Expectedvalue = "Select Application Source";
		String Pass = "PASS >> " + Expectedvalue + " windows  displayed successfully";
		String Fail = "FAIL >> " + Expectedvalue + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, Pass, Fail);

		// verify SureMDM AppStore and Configure systemApps button

		boolean isIconPresent = icon_SureMDMAppStore.isDisplayed() && icon_ConfigureSystemApps.isDisplayed();
		String pass = "PASS >> 'SureMDM App Store' and 'Configure System Apps' are available on the 'Select Application Source Window' ";
		String fail = "FAIL >> icons are not available ";
		ALib.AssertTrueMethod(isIconPresent, pass, fail);
	}

	public void ClickiOnSureMDMAppStore() throws InterruptedException {
		icon_SureMDMAppStore.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[text()='Select Application Source']")));
		sleep(4);
		String Actualvalue = EnterpriseAppWindow_Header.getText();
		String Expectedvalue = "Enterprise App Store";
		String Pass = "PASS >> " + Expectedvalue + " windows  displayed successfully";
		String Fail = "FAIL >> " + Expectedvalue + " window is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, Pass, Fail);
		sleep(2);
	}

	public void EnterVersion() {
		app_version.sendKeys("testing");
	}

	public void ChoosingApp() throws InterruptedException {
		ClickOnAppName.click();
		sleep(2);
	}

	public void AddNewApp() {
		AddNewApp.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(WarningMessageOnAddNewPopUp));
	}

	public void WarningMessageWindowOnClickingAddNewApp() throws InterruptedException {

		boolean value = true;

		try {
			WarningMessageOnAddNewPopUp.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> No warning pop up displayed");
		Reporter.log(
				"PASS >> warning dialog with message 'New application first needs to be added to App Store. Would you like to add now? is displayed",
				true);
		sleep(4);

	}

	public void ClickOnYesButtonWarningDialog() throws InterruptedException {
		ClickOnYesButtonOnWarningDialog.click();
		waitForidPresent("addNewAppBtn");
		Reporter.log("Clicking On App Store, Navigated to App Store Page", true);
		sleep(4);
	}

	public void ClickOnAddNewApp() throws InterruptedException {
		waitForidPresent("byodSection");
		ClickOnAddNewAppButton.click();
		sleep(4);
		try {
			sleep(4);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

	}

	public void IsSelectOptionsWindowDisplayed() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='UploadApkMam']/p")));
		boolean isVisible = SelectOptionWindow_Header.isDisplayed();
		String pass = "PASS >> 'Select Options' window is displayed";
		String fail = "FAIL >> 'Select Options' window is NOT displayed";
		ALib.AssertTrueMethod(isVisible, pass, fail);
		sleep(4);
	}

	public void ClickOnUploaAPKOption() {
		ClickOnUploaAPK.click();
	}

	public void ClickOnAPKLink() throws InterruptedException {
		ClickOnAPKLink.click();
		sleep(6);
		boolean value = APKLinkParameter.isDisplayed();
		String pass = "PASS >> the parameter is 'APK Link'";
		String fail = "FAIL >> the parameter is not Shown or Wrong";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(5);
		Initialization.driver.findElement(By.xpath(".//*[@id='apkLinkMam']")).sendKeys("http://test.com");

	}

	public void VerifyClickingOnWebApp() throws InterruptedException {
		ClickOnWebApp.click();
		sleep(2);
	}

	public void EnterWebAppDetails(String Tittle,String WebAppLink) throws InterruptedException{

		EnterWebAppTitle.sendKeys(Tittle);
		EnterWebAppLink.sendKeys(WebAppLink);
		EnterWebAppCategory.sendKeys(Config.EnterWebAppCatergory);
		EnterWebAppDescription.sendKeys(Config.EnterWebAppDesc);
		Initialization.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		sleep(2); 

	}
	
	public void EnterWebAppDetails() throws InterruptedException {

		EnterWebAppTitle.sendKeys(Config.EnterWebAppTitle);
		EnterWebAppLink.sendKeys(Config.EnterWebLink);
		EnterWebAppCategory.sendKeys(Config.EnterWebAppCatergory);
		EnterWebAppDescription.sendKeys(Config.EnterWebAppDesc);
		Initialization.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		sleep(2);

	}

	@FindBy(xpath="//span[text()='App has been already added!']")
	private WebElement AppAlreadyAdded;
	
	@FindBy(xpath="//*[@id='UploadWebAppPopup']/div/div/div[1]/button")
	private WebElement CloseAppUpload;
	public void ClickOnWebAppAddButton() throws InterruptedException
	{
		WebAppAddButton.click();
		Initialization.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		sleep(1);
		try{
		if (AppAlreadyAdded.isDisplayed()) {
			sleep(5);
			CloseAppUpload.click();
		}}catch(Exception e) {
		}sleep(10);
	}

	public void ClickOnWebAppBackButton() throws InterruptedException {
		WebAppBackButton.click();
		Initialization.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		sleep(2);
	}

	public void CloseButton_SelectOptionsWindow() throws InterruptedException {
		CloseSelectOptionsWindow.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(profilesOption));
		sleep(2);

	}

	public void VerifyConformationMessageOnAddingWebApp() throws InterruptedException {

		sleep(2);
		boolean value = true;

		try {
			ConfirmationMessageOnAddingWebApp.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> No confirmation message displayed");
		Reporter.log("PASS >> Confirmation message  'Web App Added Successfully.'  is displayed", true);
		Initialization.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		sleep(5);
	}

	public void WarningMessageOnAddingSameWebApp() throws InterruptedException {

		sleep(2);
		boolean value = true;

		try {
			WarningMessageOnAddingSameWebApp.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> No warning message displayed");
		Reporter.log("PASS >> Warning message  'App has been already added!'  is displayed", true);
		sleep(4);

	}

	public void NotificationMessageOnAddingApk() throws InterruptedException {

		sleep(2);
		boolean value = true;

		try {
			ConfirmationMessageOnAddingApk.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> No message displayed");
		Reporter.log("PASS >> message  'App added succesfully. 'is displayed", true);
		sleep(4);

	}

	public void SelectingTheCreatedWebAppFromDrowpdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 141, 2) + "']"))
				.click();
		;
		sleep(1);
	}

	public void VerifyPackageNameOftheAddedWebApp() throws InterruptedException {
		boolean isdisplayed = PackageNameofESFileExplorer.isDisplayed();
		String pass = "PASS >> Package Name of 'ES file explorer' is displayed correctly";
		String fail = "FAIL >>  Package Name of 'ES file explorer' is NOT correct";
		ALib.AssertTrueMethod(isdisplayed, pass, fail);
		sleep(2);

	}

	@FindBy(xpath = "(//div[@id='appList_chosen'])[3]")
	private WebElement chooseApplist;

	public void SelectingTheCreatedAppFromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 255, 2) + "']"))
				.click();
		sleep(2);
	}

	public void SelectingTheCreatedApp1FromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 258, 2) + "']"))
				.click();
		sleep(3);
	}

	@FindBy(xpath = "(.//input[@id='requiresApp'])[1]")
	private WebElement InstallSilently;

	public void ClickOnInstallSilentlyCheck_Box() throws InterruptedException {

		List<WebElement> ls = Initialization.driver.findElements(By.xpath(".//input[@id='requiresApp']"));
		ls.get(1).click();

	}

	public void SelectingTheCreatedApp2FromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 264, 2) + "']"))
				.click();
		sleep(3);
	}

	public void SelectingTheCreatedApp2FromDropdownSIM()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 261, 2) + "']"))
				.click();
		sleep(3);
	}

	public void SelectingTheCreatedWebApp1FromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 267, 2) + "']"))
				.click();
		sleep(3);
	}

	public void SelectingTheCreatedWebApp2FromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 270, 2) + "']"))
				.click();
		sleep(3);
	}

	public void SelectingTheCreatedAppMultiprofileFromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		sleep(2);
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 273, 2) + "']"))
				.click();
		sleep(3);

	}

	public void SelectingTheCreatedWebAppMultiProfileFromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 276, 2) + "']"))
				.click();
		sleep(3);
	}

	public void SelectingTheCreatedAppLinkMultiProfileFromDropdown()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		chooseApplist.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 278, 2) + "']"))
				.click();
		sleep(3);
	}

	public void ClickOnAddButton_EnterpriseAppStore() throws InterruptedException {
		AddButton_EnterpriseAppStoreWindow.click();
		sleep(3);
	}

	public void ClickOnAPKLinkAddButton() {
		APKLinkAddButton.click();
	}

	public void ClickOnNoButtonWaningPopUp() throws InterruptedException {
		ClickOnNoButtonOnWarningDialog.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(AppPolicyAddButton));
		boolean isAddbuttonVisible = AppPolicyAddButton.isDisplayed();
		String pass = "PASS >> 'Add button' is visible - Warning dialog dismissed ";
		String fail = "FAIL >> 'Add button' is not visible- Warning dialog didn't dismiss";
		ALib.AssertTrueMethod(isAddbuttonVisible, pass, fail);
	}

	// Network Settings
	public void ClickOnNetworkSettingsPolicy() {
		Helper.highLightElement(Initialization.driver, networkSettingsPolicyOption);
		networkSettingsPolicyOption.click();

	}

	public void EnterNetworkSettingsProfileName() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(Config.NetworkSettingsProfileName);
		sleep(2);
	}

	public void ClickOnNetworkSettingPolicyConfigButton() {

		Helper.highLightElement(Initialization.driver, networkSettingsPolicyConfigurebutton);
		networkSettingsPolicyConfigurebutton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(NetworkSettingsPolicyText));
		boolean value = NetworkSettingsPolicyText.isDisplayed();
		String pass = "PASS >> 'Network Settings' page displayed successfully";
		String fail = "FAIL >> 'Network Settings' page NOT displayed successfully";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void VerifyParametersOfNetworkSettingPolicyWindow() {

		Reporter.log("======Verify Total parameter count of Network Settings Policy======");
		List<WebElement> parameters = Initialization.driver
				.findElements(By.xpath(".//*[@id='globalHTTPproxyAndroidProfileTail']/ul/li/div[1]"));
		// verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 2;
		String PassStatement2 = "PASS >> Total parameter count is: " + expectedParameters + "  ";
		String FailStatement2 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement2, FailStatement2);

		// verifying parameters one by one
		Reporter.log("========Verify Network Settings Policy parameters one by one=========");
		for (int i = 0; i < totalParameters; i++) {
			if (i == 0) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Proxy Type";
				String PassStatement = "PASS >> 1st option is present - correct";
				String FailStatement = "FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 1) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Set always-on VPN";
				String PassStatement = "PASS >> 2nd option is present - correct";
				String FailStatement = "FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
		Reporter.log("======Verify Total parameter count of Network Settings Policy - 2nd set======");
		List<WebElement> parameters1 = Initialization.driver
				.findElements(By.xpath(".//*[@id='globalHTTPproxyAndroidProfileTail']/ul/li[5]/ul/li/div[1]"));
		// verifying total parameters of 2nd set

		int totalParameters1 = parameters1.size();
		int expectedParameters1 = 2;
		String PassStatement3 = "PASS >> Total parameter count is: " + expectedParameters1 + "  ";
		String FailStatement3 = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters1, totalParameters1, PassStatement3, FailStatement3);

		// verifying parameters one by one - 2nd set
		Reporter.log("========Verify Network Settings Policy parameters one by one in the 2nd set=========");
		for (int i = 0; i < totalParameters1; i++) {
			if (i == 0) {
				String actual = parameters1.get(i).getText();
				Reporter.log(actual);
				String expected = "Package Name";
				String PassStatement = "PASS >> 3rd option is present - correct";
				String FailStatement = "FAIL >> 3rd  option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 1) {
				String actual = parameters1.get(i).getText();
				Reporter.log(actual);
				String expected = "Disable network access when VPN is not connected";
				String PassStatement = "PASS >> 4th option is present - correct";
				String FailStatement = "FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}

		}
	}

	public void VerifyProxyType_Manual()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify checking if Proxy Type- Manual is available in the dropdown ====");
		// verify if all the options present in dropdown

		Initialization.driver.findElement(By.id("globalproxyselectAFW_chosen")).click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 144, 2) + "']"))
				.click();
		;
		sleep(1);
		Helper.highLightElement(Initialization.driver, Manual);
		boolean value = Manual.isDisplayed();
		String pass = "PASS >> 'Manual' is available";
		String fail = "FAIL >> 'Manual' is NOT available";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(1);

	}

	public void VerifyProxyType_Auto()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify checking if Proxy Type- Auto is available in the dropdown ====");

		Initialization.driver.findElement(By.id("globalproxyselectAFW_chosen")).click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 145, 2) + "']"))
				.click();
		;
		sleep(1);
		boolean value = Auto.isDisplayed();
		String pass = "PASS >> 'Auto' is available";
		String fail = "FAIL >> 'Auto' is NOT available";
		ALib.AssertTrueMethod(value, pass, fail);
		sleep(1);
	}

	public void VerifyParameterFor_Manual() {
		Reporter.log("=======Verify parameter of Manual========");
		boolean value = ParameterOnSelecting_Manual.isDisplayed();
		String pass = "PASS >> Parameter 'Proxy Server and Port'  displayed on selecting 'Manual' ";
		String fail = "FAIL >> Parameter 'Proxy Server and Port' NOT displayed on selecting 'Manual' ";
		ALib.AssertTrueMethod(value, pass, fail);
		// enter server and port
		Helper.highLightElement(Initialization.driver, EnterproxyServer);
		EnterproxyServer.sendKeys(Config.EnterproxyServer);
		Helper.highLightElement(Initialization.driver, EnterproxyServerPort);
		EnterproxyServerPort.sendKeys(Config.EnterProxyPort);
	}

	public void VerifyParameterFor_Auto() {
		Reporter.log("=======Verify parameter of Auto========");
		boolean value = ParameterOnSelecting_Auto.isDisplayed();
		String pass = "PASS >> Parameter 'Proxy PAC URL'  displayed on selecting 'Auto' ";
		String fail = "FAIL >> Parameter 'Proxy PAC URL' NOT displayed on selecting 'Auto' ";
		ALib.AssertTrueMethod(value, pass, fail);
		// enter proxyPAC URL
		Helper.highLightElement(Initialization.driver, EnterProxyPACURL);
		EnterProxyPACURL.sendKeys(Config.EnterProxyPACURL);
	}

	public void VerifySetAlwaysOnVPN() {
		Helper.highLightElement(Initialization.driver, EnterVPNPackageName);
		EnterVPNPackageName.sendKeys(Config.EnterVPNPackageName);
		Helper.highLightElement(Initialization.driver, DisableNetworkWhenVPNNotConnected);
		DisableNetworkWhenVPNNotConnected.click();
	}

	// *********** Mail Configuration****************

	public void ClickOnMailConfigurationPolicy() {
		Helper.highLightElement(Initialization.driver, mailConfigurationPolicyOption);
		mailConfigurationPolicyOption.click();

	}

	public void EnterMailConfigurationProfileName() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(Config.EnterMailConfigurationProfileName);
		sleep(2);
	}

	public void EnterMailConfigurationProfileName1() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(Config.EnterMailConfigurationProfileName1);
		sleep(2);
	}

	public void ClickOnMailConfigurationPolicyConfigButton() {

		Helper.highLightElement(Initialization.driver, mailConfigurationPolicyConfigurebutton);
		mailConfigurationPolicyConfigurebutton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(MailConfigurationPolicyText));
		boolean value = MailConfigurationPolicyText.isDisplayed();
		String pass = "PASS >> 'Mail Configuration' policy page displayed successfully";
		String fail = "FAIL >> 'Mail Configuration' policy page NOT displayed successfully";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void VerifyParametersOfMailConfigurationPolicyWindow() {

		Reporter.log("======Verify Total parameter count of Mail Configuration Policy======");
		List<WebElement> parameters = Initialization.driver
				.findElements(By.xpath(".//*[@id='andr-mailProfileTail']/ul[1]/li/div[1]"));

		// verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 9;
		String PassStatement = "PASS >> Total parameter count is: " + expectedParameters + "  ";
		String FailStatement = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement, FailStatement);

		// verifying parameters one by one
		Reporter.log("========Verify Mail Configuration Policy parameters one by one=========");
		for (int i = 0; i < totalParameters; i++) {
			if (i == 0) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Email Address";
				String PassStatement1 = "PASS >> 1st option is present - correct";
				String FailStatement1 = "FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement1, FailStatement1);
			}
			if (i == 1) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Hostname or Host";
				String PassStatement2 = "PASS >> 2nd option is present - correct";
				String FailStatement2 = "FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 2) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Username";
				String PassStatement2 = "PASS >>3rd option is present - correct";
				String FailStatement2 = "FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 3) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Identifier";
				String PassStatement2 = "PASS >> 4th option is present - correct";
				String FailStatement2 = "FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 4) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "SSL Required";
				String PassStatement2 = "PASS >> 5th option is present - correct";
				String FailStatement2 = "FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 5) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Trust all Certificates";
				String PassStatement2 = "PASS >> 6th option is present - correct";
				String FailStatement2 = "FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 6) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Login Certificate Alias";
				String PassStatement2 = "PASS >> 7th option is present - correct";
				String FailStatement2 = "FAIL >> 7th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 7) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Default Email Signature";
				String PassStatement2 = "PASS >> 8th option is present - correct";
				String FailStatement2 = "FAIL >> 8th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 8) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Default Sync Window";
				String PassStatement2 = "PASS >> 9th option is present - correct";
				String FailStatement2 = "FAIL >> 9th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
		}
	}

	public void EnterAllParameterValues_MailConfiguration() {
		MailConfiguration_EmailAddress.sendKeys(Config.EnterEmailAddress);
		MailConfiguration_HostName.sendKeys(Config.EnterHostname);
		MailConfiguration_UserName.sendKeys(Config.EnterUserName);
		MailConfiguration_DeviceIdentifier.sendKeys(Config.EnterDeviceIdentifier);
		MailConfiguration_LoginCertificateAlias.sendKeys(Config.EnterLoginCertificateAlias);
		MailConfiguration_DefaultEmailSignature.sendKeys(Config.EnterDefaultEmailSignature);
		MailConfiguration_DefaultSyncWindow.sendKeys(Config.EnterDefaultSyncWindow);

	}

	public void EnterAllParameterValues_MailConfiguration1() {
		MailConfiguration_EmailAddress.sendKeys(Config.EnterEmailAddress1);
		MailConfiguration_HostName.sendKeys(Config.EnterHostname1);
		MailConfiguration_UserName.sendKeys(Config.EnterUserName1);
		MailConfiguration_DeviceIdentifier.sendKeys(Config.EnterDeviceIdentifier);
		MailConfiguration_LoginCertificateAlias.sendKeys(Config.EnterLoginCertificateAlias);
		MailConfiguration_DefaultEmailSignature.sendKeys(Config.EnterDefaultEmailSignature);
		MailConfiguration_DefaultSyncWindow.sendKeys(Config.EnterDefaultSyncWindow);

	}

	// *********** Wifi Configuration****************

	public void ClickOnWifiConfigurationPolicy() {
		Helper.highLightElement(Initialization.driver, wifiConfigurationPolicyOption);
		wifiConfigurationPolicyOption.click();

	}

	public void EnterWifiConfigurationProfileName(String name) throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(name);
		sleep(2);
	}

	public void ClickOnWifiConfigurationPolicyConfigButton() throws InterruptedException {

		Helper.highLightElement(Initialization.driver, wifiConfigurationPolicyConfigurebutton);
		wifiConfigurationPolicyConfigurebutton.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(2);
	}

	public void VerifyParametersOfWifiConfigurationPolicyWindow() {

		Reporter.log("======Verify Total parameter count of wifi Configuration Policy======");
		List<WebElement> parameters = Initialization.driver
				.findElements(By.xpath(".//*[@id='andr-wifiProfileTail']/ul/li/div[1]"));

		// verifying total parameters

		int totalParameters = parameters.size();

		// verifying parameters one by one
		Reporter.log("========Verify wifi Configuration Policy parameters one by one=========");
		for (int i = 1; i < totalParameters; i++) {

			if (i == 1) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "SSID";
				String PassStatement1 = "PASS >> 1st option is present - correct";
				String FailStatement1 = "FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement1, FailStatement1);
			}

			if (i == 2) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Security Type";
				String PassStatement1 = "PASS >> 2nd option is present - correct";
				String FailStatement1 = "FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement1, FailStatement1);
			}

		}
		
	}

	public void EnterSSIDWifiConfigurationOpen() {
		WifiConfiguration_SSID.sendKeys(Config.EnterWifiConfigurationSSIDOpen);

	}

	public void EnterSSID_PasswordValues_WifiConfigurationWAP2() {
		WifiConfiguration_SSID.sendKeys(Config.EnterWifiConfigurationSSIDWAP2);
		WifiConfiguration_password.sendKeys(Config.EnterWifiConfigurationPasswordWAP2);

	}

	public void DefaultValueSecurityType()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		boolean val = DefauleValue_SecurityType.isDisplayed();
		String pass = "PASS >> 'WPA PSK' is available";
		String fail = "FAIL >> 'WPA PSK' is NOT available";
		ALib.AssertTrueMethod(val, pass, fail);

	}

	public void SecurityTypeOpen() throws InterruptedException {

		Helper.highLightElement(Initialization.driver, WifiConfiguration_securityTypeDropdown);
		WifiConfiguration_securityTypeDropdown.click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//li[text()='Open']")).click();
		sleep(1);
		boolean value = Open.isDisplayed();
		String pass1 = "PASS >> 'Open' is available";
		String fail1 = "FAIL >> 'Open' is NOT available";
		ALib.AssertTrueMethod(value, pass1, fail1);
	}

	public void EnableAutoConnect() {
		WifiConfiguration_autoConnect.click();

	}

	public void EnableHiddenNetwork() {
		WifiConfiguration_hiddenNetwork.click();
	}

	// ***************** file sharing policy****************

	public void clickOnFileSharingPolicy() throws InterruptedException {
		sleep(2);
		Helper.highLightElement(Initialization.driver, fileSharingPolicy);
		fileSharingPolicy.click();
	}

	public void ClickOnFileSharingPolicyConfigButton() {

		Helper.highLightElement(Initialization.driver, FileSharingPolicyConfigurebutton);
		FileSharingPolicyConfigurebutton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(FileSharingPolicyText));
		boolean value = FileSharingPolicyText.isDisplayed();
		String pass = "PASS >> 'File Sharing' policy page displayed successfully";
		String fail = "FAIL >> 'File Sharing' policy page NOT displayed successfully";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void ClickOnFileSharingAddButton() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, ClickOnFileSharing_AddButton);
		ClickOnFileSharing_AddButton.click();
		while (Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div"))
				.isDisplayed()) {
		}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		Reporter.log("Clicking On File Store, Navigated to File Store Page", true);
		sleep(10);

	}

	public void IsFileStoreWindowDisplayed() throws InterruptedException {

		boolean isDisplayed = FileStoreWindow_Header.isDisplayed();
		String Pass = "PASS >> 'File Store' window is displayed";
		String Fail = "FAIL >> 'File Store' window is NOT displayed";
		ALib.AssertTrueMethod(isDisplayed, Pass, Fail);
		sleep(6);
	}

	public void rightclick() {
		Initialization.driver.findElement(By.xpath(".//*[@id='v1_MS5qcGc1']/div[1]/div")).click();

	}

	// *****certificate*************

	public void ClickOnCertificatePolicy() {
		Helper.highLightElement(Initialization.driver, ClickOnCertificatePolicy);
		ClickOnCertificatePolicy.click();
	}

	public void EnterCertificateProfileName() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(Config.EnterCertificateProfileName);
		sleep(2);
	}

	public void EnterCertificateProfileName(String value) throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(value);
		sleep(2);
	}

	public void isCertUploaded(String value) {
// 	    	boolean flag = isCertpresent.isDisplayed();
		waitForXpathPresent("//table[@id='android_certificateTable']/tbody/tr/td");
		boolean flag = Initialization.driver
				.findElement(By.xpath("//table[@id='android_certificateTable']/tbody/tr/td[text()='" + value + "']"))
				.isDisplayed();
		String pass = "PASS >> cert is displayed- Cert Uploaded";
		String fail = "FAIL >> cert is not displayed-Cert NOT uploaded";
		ALib.AssertTrueMethod(flag, pass, fail);
	}

	public void ClickOnCertificatePolicyConfigButton() throws InterruptedException {

		Helper.highLightElement(Initialization.driver, CertificatePolicyConfigurebutton);
		CertificatePolicyConfigurebutton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[text()='Certificate']")));
		sleep(4);
	}

	public void ClickOnCertificateBrowseButton() throws InterruptedException {
		certBrowsebutton.click();
		sleep(2);
	}

	public void RunAutoItFileOne() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("C:\\Users\\admin\\Documents\\Automation_Related\\fileupload\\upload.exe");
		sleep(3);
	}

	public void RunAutoItFileTwo() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("C:\\Users\\admin\\Documents\\Automation_Related\\fileupload\\uploadTwo.exe");
		sleep(3);
	}

	public void ClickOnAddButtonOnCertWindow() throws InterruptedException {
		ClickOnAddButton_CertWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(4);
	}

	public void ClickOnAddButtonToAddCert() throws InterruptedException {
		ClickOn_AddButton_CertificateWindow.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[text()='Certificate']")));
		sleep(4);
	}

	public void isCertUploaded() {
		boolean value = isCertpresent.isDisplayed();
		String pass = "PASS >> cert is displayed- Cert Uploaded";
		String fail = "FAIL >> cert is not displayed-Cert NOT uploaded";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void EditUploadedCert() throws InterruptedException {
		isCertpresent.click();// clicking the uploaded the cert
		ClickOnCert_EditButton.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[text()='Certificate']")));
		sleep(4);
		boolean iscertWindowDisplayed = certficateWindow_Header.isDisplayed();
		String pass = "PASS >> Certificate Window is displayed- Clicked on Edit";
		String fail = "FAIL >> Certificate Window is NOT displayed- Not clicked on Edit";
		ALib.AssertTrueMethod(iscertWindowDisplayed, pass, fail);

		ClickOnCancelButtonOnCertficateWindow.click();
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 20);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='android_cert_edit_btn']")));
		sleep(4);
	}

	public void DeleteUploadedCert() throws InterruptedException {
		ClickOnCert_DeleteButton.click();
		sleep(1);

	}

	public void ConfirmIfCertDeleted() {
		List<WebElement> ls = Initialization.driver
				.findElements(By.xpath(".//*[@id='android_certificateTable']/tbody/tr/td"));
		int actual = ls.size();
		int expected = 1;
		String pass = "PASS >> deleted";
		String fail = "FAIL >> Not deleted";
		ALib.AssertEqualsMethodInt(expected, actual, pass, fail);
	}

	// ******************profile settings****************

	public void isProfileSettingsWindowDisplayed() throws InterruptedException {
		System.out.println("Clicking on Settings button");
		isSettingsButtonVisible.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[text()='Profile Settings']")));
		sleep(3);
		System.out.println("checking if profile settings window displayed");
		boolean isdisplayed = profileSettings_WindowHeader.isDisplayed();
		String pass = "PASS >> Profile settings window is displayed";
		String fail = "FAIL >> Profile settings window is NOT displayed";
		ALib.AssertTrueMethod(isdisplayed, pass, fail);
	}

	// verify elements of profile settings window
	public void isStoreLayoutPresent() {
		System.out.println("checking if store layout present");
		boolean value = StoreLayout.isDisplayed();
		String pass = "PASS >> Store Layout is present in profile settings window";
		String fail = "FAIL >> Store layout is NOT present in profile settings window";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void isApplicationLicensesPresent() {
		System.out.println("checking if application license present");
		boolean value = ApplicationLicenses.isDisplayed();
		String pass = "PASS >> Application Licenses is present in profile settings window";
		String fail = "FAIL >> Application Licenses is NOT present in profile settings window";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void IsUnEnrollPresent() {
		System.out.println("checking if Unenroll present");
		boolean value = Unenroll.isDisplayed();
		String pass = "PASS >> Unenroll is present in profile settings window";
		String fail = "FAIL >> Unenroll is NOT present in profile settings window";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	// verify summary of profile settings options
	public void VerifyStoreLayoutSummary() {
		System.out.println("checking summary of store layout");
		boolean value = summaryStoreLayout.isDisplayed();
		String pass = "PASS >> Store Layout Summary is present in profile settings window";
		String fail = "FAIL >> Store layout summary is NOT present in profile settings window";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void VerifyApplicationLicenseSummary() {
		System.out.println("checking summary of application license");
		boolean value = summaryApplicationLicenses.isDisplayed();
		String pass = "PASS >> Application Licenses Summary is correct in profile settings window";
		String fail = "FAIL >> Application Licenses summary is NOT present in profile settings window";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	public void VerifyUnenrollSummary() {
		System.out.println("checking summary of unenroll");
		boolean value = summaryUnEnroll.isDisplayed();
		String pass = "PASS >> Unenroll summary is present in profile settings window";
		String fail = "FAIL >> Unenroll summary is NOT present in profile settings window";
		ALib.AssertTrueMethod(value, pass, fail);
	}

	// configure Store Layout in profile settings
	public void ConfigureStoreLayout() {
		System.out.println("clicking on store layout");
		StoreLayout.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[text()='Store Layout']")));
		try {
			sleep(3);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public void isStoreLayoutWindowDisplayed() throws InterruptedException {

		boolean isdisplayed = storeLayout_WindowHeader.isDisplayed();
		String pass = "PASS >> store layout window is displayed";
		String fail = "FAIL >> Store Layout window is NOT displayed";
		ALib.AssertTrueMethod(isdisplayed, pass, fail);
	}

	public void clickOnAddPage() {
		AddPage_Button.click();
		boolean isdisplayed = AddPageTitle_WindowHeader.isDisplayed();
		String pass = "PASS >> 'Add Page Title' window is displayed - Clicked On Add Page button";
		String fail = "FAIL >> 'Add Page Title' window is NOT displayed- Not Clicked On Add Page button";
		ALib.AssertTrueMethod(isdisplayed, pass, fail);
	}

	public boolean CheckProfilesBtn() {
		boolean value = true;
		try {
			Profiles.isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void EnterPageName() {
		EnterPageName.sendKeys(Config.EnterPageName);
	}

	public void ClickOnAddButton_AddPageTitle() {
		AddPageTitle_AddButton.click();
	}

	public void ClickOnProfile() throws InterruptedException {
		try {
			waitForXpathPresent("//a[text()='Profiles']");
			sleep(4);
			Profiles.click();
			waitForidPresent("addPolicyBtn");
			sleep(2);
			Reporter.log("PASS>> Clicking On Profiles, Navigated to Profiles Page", true);
		} catch (Exception e) {
			Initialization.driver.findElement(By.xpath("//li[@class='nav-subgrp actAsSubGrp']/p/span[text()='More']"))
			.click();
			sleep(2);
			Profiles.click();
			waitForidPresent("addPolicyBtn");
			sleep(2);
			Reporter.log("PASS>> Clicking On Profiles, Navigated to Profiles Page", true);
		}
	}

	public void ClickOnApplicationLicense() throws InterruptedException {
		applicationLicense.click();
		sleep(2);
	}

	public void isAppLicenseDataWindowDisplayed() {
		boolean isDisplayed = appLicenseWindow.isDisplayed();
		String pass = "PASS >> App license window is displayed";
		String fail = "FAIL >>  App licens window is NOT displayed";
		ALib.AssertTrueMethod(isDisplayed, pass, fail);
	}

	public void TextMessageWhenNoApplicationLicenseAvailable() throws InterruptedException {
		boolean isTextMessageDisplayed = TextMessageInLicensWhenNoApplication.isDisplayed();
		String pass = "PASS >> Text Message when no license available is displayed";
		String fail = "FAIL >> Text Message when no license available is NOT displayed";
		ALib.AssertTrueMethod(isTextMessageDisplayed, pass, fail);
		sleep(2);
	}

	public void ClickOnCloseButton() throws InterruptedException {
		CloseButton_AppLicensWindow.click();
		sleep(1);
	}

	public void ClickOnUnenrollButton() throws InterruptedException {
		UnenrollAFW.click();
		sleep(1);
	}

	public void VerifyWarningPopupOnAFWEnroll() throws InterruptedException {
		boolean isWarningDisplayed = WarningMessageOnAFWUnenroll.isDisplayed();
		String pass = "PASS >> Warning message on AFW enroll displayed";
		String fail = "FAIL >> warning message on AFW enroll NOT displayed";
		ALib.AssertTrueMethod(isWarningDisplayed, pass, fail);
		sleep(1);
	}

	public void ClickOnNO_UnenrollAFW() throws InterruptedException {
		ClickOnNoButton_UnEnrollAFW.click();
		Reporter.log("Clicked On No Button- warning popup dismissed");
		sleep(1);
	}

	public void ClickOnYES_EnenrollAFW() throws InterruptedException {
		ClickOnYES_Button_UnEnrollAFW.click();
		sleep(2);
	}

	public void ConfirmationMessageOnUnenrollAFW() throws InterruptedException {
		boolean value = true;

		try {
			ConformationNotificationOn_UnEnrollAFWAccount.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> Did not recieve confirmation message");
		Reporter.log("PASS >> confirmation notification 'Successfully unregistered EMM account'is displayed", true);
		sleep(3);
	}

	public void CloseFileSharingWindow() throws InterruptedException {
		CloseFileSharingWindow.click();
		Initialization.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		sleep(3);
		Reporter.log("PASS >> File Sharing Window Closed Successfully");
	}

	public void EnterFileSharingProfileName() throws InterruptedException {
		Helper.highLightElement(Initialization.driver, EnterPolicyName);
		EnterPolicyName.sendKeys(Config.EnterFileSharingProfileName);
		sleep(2);
	}

	public void ClickOnPasswordPolicyProfile() {
		SelectPassswordPolicyProfile.click();
	}

	public void VerifyAutomaticallyLockText() throws InterruptedException {
		boolean isSubTextDisplayed = Initialization.driverAppium.findElementByXPath(
				"//android.widget.TextView[@text='30 minutes after sleep, except when kept unlocked by Smart Lock']")
				.isDisplayed();
		String pass = "PASS >> sub text is displayed correctly according to applied timeout";
		String fail = "FAIL >> sub text is wrong";
		ALib.AssertTrueMethod(isSubTextDisplayed, pass, fail);
		sleep(6);
	}

	public void ClickOnScreenLock() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Screen lock']").click();
		sleep(6);
	}

	@FindBy(id = "com.android.settings:id/password_entry")
	private WebElement ConfirmYourPINEntry;

	public void EnterConfirmYourPIN() throws InterruptedException {
		for (int i = 0; i < 4; i++) {
			Initialization.driverAppium.getKeyboard().sendKeys("0");
		}
		sleep(3);
	}

	public void ClickOnNextButtonInConfirmYourPINPage() throws InterruptedException {

		List<WebElement> ls = Initialization.driverAppium.findElementsById("com.android.settings:id/next_button");

		int count = ls.size();
		System.out.println("Total next buttons:" + count);
		ls.get(0).click();
		sleep(3);
	}

	public void ChoosePINFromChooseScreenLock() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='PIN']").click();

	}

	@FindBy(xpath = "//p[text()='" + Config.EnterMailConfigurationProfileName + "']")
	private WebElement MailCongfigurationProfile;

	public void SelectMailConfigProfile() throws InterruptedException {
		MailCongfigurationProfile.click();
		sleep(4);
	}

	@FindBy(xpath = "//p[text()='" + Config.EnterMailConfigurationProfileName1 + "']")
	private WebElement MailCongfigurationProfile1;

	public void SelectMailConfigProfile1() throws InterruptedException {
		MailCongfigurationProfile1.click();
		sleep(4);
	}

	@FindBy(xpath = "//p[text()='" + Config.EnterWifiConfigurationProfileNameHotSpot + "']")
	private WebElement WifiConfigProfile;

	public void SelectWifiConfigProfile1() throws InterruptedException {
		WifiConfigProfile.click();
		sleep(4);
	}

	// ################ Astro Mail #####################
	@FindBy(id = "andr-astroMailProfileConfig")
	private WebElement AstroMailConfigButton;

	@FindBy(xpath = "//span[text()='Please fill all required fields to save profile']")
	private WebElement ErrorMSG;

	@FindBy(id = "astroMail_email")
	private WebElement AstromailEmail;

	@FindBy(id = "astroMail_Password")
	private WebElement AstroMailPasssword;

	@FindBy(id = "astromail_type_chosen")
	private WebElement TypeDropDown;

	@FindBy(id = "astromail_security_chosen")
	private WebElement Security;

	@FindBy(id = "astromail_exchangeserver")
	private WebElement ExchangeServer;

	@FindBy(id = "astromail_port")
	private WebElement Port;

	@FindBy(id = "astromail_userName")
	private WebElement UserNameastromail;

	@FindBy(xpath = "//p[text()='" + Config.ProfilePOP + "']")
	private WebElement AstromailProfile1;

	@FindBy(xpath = "//p[text()='" + Config.ProfileIMAP + "']")
	private WebElement AstromailProfile2;

	@FindBy(xpath = "//p[text()='" + Config.ProfileActive + "']")
	private WebElement AstromailProfile3;

	@FindBy(xpath = "//td[text()='" + Config.ProfilePOP + "']")
	private WebElement PoP3profile;

	@FindBy(xpath = "//td[text()='" + Config.ProfileIMAP + "']")
	private WebElement IMAPprofile;

	@FindBy(xpath = "//td[text()='" + Config.ProfileActive + "']")
	private WebElement ActiveSyncprofile;

	@FindBy(id = "astromailrestrict_incomingemail")
	private WebElement ReplayToIncomingMailCheckBox;

	@FindBy(id = "astromailrestrict_forwardemail")
	private WebElement ForwardMailRestrictionCheckBox;

	@FindBy(id = "astromailrestrict_downloadAttachment")
	private WebElement RestrictionDownloadAttachment;

	@FindBy(id = "astromailrestrict_screenshot")
	private WebElement RestrictionScreenShoot;

	@FindBy(id = "astromailrestrict_copyClipboard")
	private WebElement RestrictionCopyToclipBoard;

	public void SelectingPOP3()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=======verify selecting the App from the dropdown ====");
		TypeDropDown.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 255, 2) + "']"))
				.click();
		sleep(2);
	}

	public void clickONAstroMailConfig() throws InterruptedException {
		AstroMailConfigButton.click();
		sleep(3);
	}

	public void VerifyParametersOfAstroMailConfigurationPolicyWindow() {

		Reporter.log("======Verify Total parameter count of Mail Configuration Policy======");
		List<WebElement> parameters = Initialization.driver
				.findElements(By.xpath("//*[@id='astroMailConfig_feilds']/li/div[1]"));

		// verifying total parameters

		int totalParameters = parameters.size();
		int expectedParameters = 18;
		String PassStatement = "PASS >> Total parameter count is: " + expectedParameters + "  ";
		String FailStatement = "FAIL >> Total parameter count is Wrong";
		ALib.AssertEqualsMethodInt(expectedParameters, totalParameters, PassStatement, FailStatement);

		// verifying parameters one by one
		Reporter.log("========Verify Mail Configuration Policy parameters one by one=========");
		for (int i = 0; i < totalParameters; i++) {
			if (i == 0) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Email Address";
				String PassStatement1 = "PASS >> 1st option is present - correct";
				String FailStatement1 = "FAIL >> 1st option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement1, FailStatement1);
			}
			if (i == 1) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Password";
				String PassStatement2 = "PASS >> 2nd option is present - correct";
				String FailStatement2 = "FAIL >> 2nd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 2) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Type";
				String PassStatement2 = "PASS >>3rd option is present - correct";
				String FailStatement2 = "FAIL >> 3rd option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 3) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Server Path";
				String PassStatement2 = "PASS >> 4th option is present - correct";
				String FailStatement2 = "FAIL >> 4th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 4) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Security";
				String PassStatement2 = "PASS >> 5th option is present - correct";
				String FailStatement2 = "FAIL >> 5th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 5) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Port";
				String PassStatement2 = "PASS >> 6th option is present - correct";
				String FailStatement2 = "FAIL >> 6th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 6) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Username";
				String PassStatement2 = "PASS >> 7th option is present - correct";
				String FailStatement2 = "FAIL >> 7th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 9) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Restriction On Replying To Email To Outside Domain";
				String PassStatement2 = "PASS >> 8th option is present - correct";
				String FailStatement2 = "FAIL >> 8th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);

			}
			js.executeScript("arguments[0].scrollIntoView();", parameters.get(9));
			if (i == 10) {

				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Restriction On Replying To Email Within Domain";
				String PassStatement2 = "PASS >> 9th option is present - correct";
				String FailStatement2 = "FAIL >> 9th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

			if (i == 11) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Restriction On Forwarding Email To Outside Domain";
				String PassStatement2 = "PASS >> 10th option is present - correct";
				String FailStatement2 = "FAIL >> 10th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 12) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Restriction On Forwarding Email Within Domain";
				String PassStatement2 = "PASS >> 11th option is present - correct";
				String FailStatement2 = "FAIL >> 11th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 13) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Restriction With Forwarding Attachment";
				String PassStatement2 = "PASS >> 12th option is present - correct";
				String FailStatement2 = "FAIL >> 12th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 14) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Restriction On Downloading Attachment";
				String PassStatement2 = "PASS >> 13th option is present - correct";
				String FailStatement2 = "FAIL >> 13th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 15) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Restriction On Viewing Attachment";
				String PassStatement2 = "PASS >> 14th option is present - correct";
				String FailStatement2 = "FAIL >> 14th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 16) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Screenshot Restriction";
				String PassStatement2 = "PASS >> 15th option is present - correct";
				String FailStatement2 = "FAIL >> 15th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}
			if (i == 17) {
				String actual = parameters.get(i).getText();
				Reporter.log(actual);
				String expected = "Disallow Copy To Clipboard";
				String PassStatement2 = "PASS >> 16th option is present - correct";
				String FailStatement2 = "FAIL >> 16th option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement2, FailStatement2);
			}

		}
	}

	public void ErrorMessagePleasefillallrequiredfieldstosaveprofile() throws InterruptedException {

		boolean value = true;

		try {
			ErrorMSG.isDisplayed();

		} catch (Exception e) {
			value = false;

		}
		Assert.assertTrue(value, "FAIL >> Please fill all required fields to save profile not displayed ");
		Reporter.log("PASS >> Notification 'Please fill all required fields to save profile.'is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
	}

	public void EnterAllParameterValuesAstroMailConfigurationPOP3()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		AstromailEmail.sendKeys(Config.AstroMailEmail);
		AstroMailPasssword.sendKeys(Config.PasswordAstro);
		TypeDropDown.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 282, 2) + "']"))
				.click();
		sleep(2);
		ExchangeServer.sendKeys(Config.ExchangeServer);
		Security.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 284, 2) + "']"))
				.click();
		Port.sendKeys(Config.Port);
		UserNameastromail.sendKeys(Config.UserNmaeAstro);

	}

	public void EnterAllParameterValuesAstroMailConfigurationIMAP()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		AstromailEmail.sendKeys(Config.AstroMailEmail1);
		AstroMailPasssword.sendKeys(Config.PasswordAstro1);
		TypeDropDown.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 286, 2) + "']"))
				.click();
		sleep(2);
		ExchangeServer.sendKeys(Config.ExchangeServer1);
		Security.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 284, 2) + "']"))
				.click();
		sleep(2);
		Port.sendKeys(Config.Port1);
		UserNameastromail.sendKeys(Config.UserNmaeAstro1);

	}

	public void EnterAllParameterValuesAstroMailConfigurationActiveSync()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		AstromailEmail.sendKeys(Config.AstroMailEmail3);
		AstroMailPasssword.sendKeys(Config.PasswordAstro3);
		TypeDropDown.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 288, 2) + "']"))
				.click();
		sleep(2);
		ExchangeServer.sendKeys(Config.ExchangeServer3);
		Security.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet15", 284, 2) + "']"))
				.click();
		sleep(2);
		Port.sendKeys(Config.Port3);
		UserNameastromail.sendKeys(Config.UserNmaeAstro3);

	}

	public void SelectPOP3profile() throws InterruptedException {
		AstromailProfile1.click();
		sleep(2);
	}

	public void SelectIMAPprofile() throws InterruptedException {
		AstromailProfile2.click();
		sleep(2);
	}

	public void SelectActiveSyncprofile() throws InterruptedException {
		AstromailProfile3.click();
		sleep(2);
	}

	public void ClickOnPOP3ProfileEdit() throws InterruptedException {
		PoP3profile.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(EditProfileButton));
		EditProfileButton.click();
		waitForidPresent("saveAfwProfile");
		sleep(2);
	}

	public void ClickOnIMAPProfileEdit() throws InterruptedException {
		IMAPprofile.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(EditProfileButton));
		EditProfileButton.click();
		waitForidPresent("saveAfwProfile");
		sleep(2);
	}

	public void ClickOnActiveSyncProfileEdit() throws InterruptedException {
		ActiveSyncprofile.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(EditProfileButton));
		EditProfileButton.click();
		waitForidPresent("saveAfwProfile");
		sleep(2);
	}

	public void ModifyTheProfile() throws InterruptedException {
		ReplayToIncomingMailCheckBox.click();
		// RestrictionCopyToclipBoard.click();
		ForwardMailRestrictionCheckBox.click();
		RestrictionDownloadAttachment.click();
		RestrictionScreenShoot.click();

	}

	public void ModifyActiveSyncProfileToOffice360() {
		AstromailEmail.clear();
		AstromailEmail.sendKeys(Config.AstroMailEmail3);
		AstroMailPasssword.clear();
		AstroMailPasssword.sendKeys(Config.PasswordAstro3);
		ExchangeServer.clear();
		ExchangeServer.sendKeys(Config.ExchangeServer3);
		UserNameastromail.clear();
		UserNameastromail.sendKeys(Config.UserNmaeAstro3);
		Port.clear();
		Port.sendKeys(Config.Port3);
	}

	public void VerifyExistingProfiles() {
		boolean status = ReplayToIncomingMailCheckBox.isSelected();
		String Pass = " ReplayToIncomingMailCheckBox is Remained Enabled " + status + "";
		String Fail = " ReplayToIncomingMailCheckBox is Remained Enabled " + status + "";
		ALib.AssertTrueMethod(status, Pass, Fail);

		boolean status1 = ForwardMailRestrictionCheckBox.isSelected();
		String Pass1 = " ForwardMailRestrictionCheckBox is Remained Enabled " + status + "";
		String Fail1 = " ForwardMailRestrictionCheckBox is Remained Enabled " + status + "";
		ALib.AssertTrueMethod(status1, Pass1, Fail1);

		boolean status2 = RestrictionScreenShoot.isSelected();
		String Pass2 = " RestrictionScreenShoot is Remained Enabled " + status + "";
		String Fail2 = " RestrictionScreenShoot is Remained Enabled " + status + "";
		ALib.AssertTrueMethod(status2, Pass2, Fail2);

		boolean status3 = RestrictionDownloadAttachment.isSelected();
		String Pass3 = "RestrictionDownloadAttachment is Remained Enabled " + status + "";
		String Fail3 = "RestrictionDownloadAttachment is Remained Enabled " + status + "";
		ALib.AssertTrueMethod(status3, Pass3, Fail3);

	}


	



	
	// ############################ Wifi configuration #################

	

	public void verfiyWifiHotspotConnectedAfterApplyingTheProfile() throws IOException, InterruptedException {
		Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").click();
		sleep(6);

		Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").click();
		sleep(10);
try {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Redmi' and @index='0']")
				.click();
		sleep(3);
		String ActualValue = Initialization.driverAppium.findElementById("com.android.settings:id/value").getText();
		String ExpectedValue = "Connected";
		String pass = "PASS >> Autoconnect to wifi hotspot is successful " + ActualValue + "";
		String fail = "FAIL >> Autoconnect is not successful";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, pass, fail);
		Initialization.driverAppium.findElementById("android:id/button2").click();
		sleep(3);}catch(Exception e) {
			String ActualValue = Initialization.driverAppium.findElementById("android:id/summary").getText();
			String ExpectedValue = "Connected";
			String pass = "PASS >> Autoconnect to wifi hotspot is successful " + ActualValue + "";
			String fail = "FAIL >> Autoconnect is not successful";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue, pass, fail);
			
		}
	}

	public void VerifywifiConnectedAfterApplyingTheProfile() throws InterruptedException {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='42GearsUniFi']").click();
		sleep(3);
		String ActualValue = Initialization.driverAppium.findElementById("com.android.settings:id/value").getText();
		String ExpectedValue = "Connected";
		String pass = "PASS >> Autoconnect to wifi hotspot is successful " + ActualValue + "";
		String fail = "FAIL >> Autoconnect is not successful";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, pass, fail);
		Initialization.driverAppium.findElementById("android:id/button2").click();
		sleep(3);
	}

	public void TurnOffWifiAndVerifyAutoConnect1() throws InterruptedException, IOException {
		Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").click();
		sleep(2);
		Initialization.driverAppium.findElementById("com.android.settings:id/switch_widget").click();
		sleep(12);
		VerifywifiConnectedAfterApplyingTheProfile();
	}

	// ##################### System Setting Profile ###########################

	


	CommonMethods_POM common=new CommonMethods_POM();
	public void VerifyVolumeSettigsAreDisabled() throws IOException, InterruptedException {

		//common.commonScrollMethod("Sound");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Media volume']").click();
		boolean bln = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass = "PASS >> Media volume disabled";
		String fail = "FAIL >> Media volume NOT disabled";
		ALib.AssertTrueMethod(bln, Pass, fail);

		Initialization.driverAppium.findElementById("android:id/button1").click();
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Alarm volume']").click();
		boolean bln1 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass1 = "PASS >> Alarm volume disabled";
		String fail1 = "FAIL >> Alarm volume NOT disabled";
		ALib.AssertTrueMethod(bln1, Pass1, fail1);

		Initialization.driverAppium.findElementById("android:id/button1").click();
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Ring volume']").click();
		boolean bln2 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass2 = "PASS >> Ring volume disabled";
		String fail2 = "FAIL >> Ring volume NOT disabled";
		ALib.AssertTrueMethod(bln2, Pass2, fail2);
		Initialization.driverAppium.findElementById("android:id/button1").click();
		System.out.println("Going back");

	}

	public void verifyFolderNotInDevice(String DownloadedFolder) throws InterruptedException   {
		Initialization.driverAppium.findElementByXPath("//android.widget.ImageView[@content-desc='Storage']").click();
		sleep(8);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Android']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='data']").click();
		commonScrollClick("com.nix");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='files']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='EFSS']").click();
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='012100052']").click();  //android.view.ViewGroup
        sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='File Store']").click();
		try {
			boolean DownloadedFolderValue = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+DownloadedFolder+"']").isDisplayed();
			ALib.AssertFailMethod("Folder is displayed");}
		catch(Exception e) {
			Reporter.log("Folder is deleted successfully");
		}
	}
	public void GoBack() throws IOException, InterruptedException {

		Runtime.getRuntime().exec("adb shell input keyevent 4");
		sleep(4);

	}





	

	public void VerifyNetworkSetting() {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='More']").click();
		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Tethering & portable hotspot']").click();
		boolean bln = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass = "PASS >> Tethering & portable hotspot disabled";
		String fail = "FAIL >> Tethering & portable hotspot NOT disabled";
		ALib.AssertTrueMethod(bln, Pass, fail);
		Initialization.driverAppium.findElementById("android:id/button1").click();

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='VPN']").click();
		boolean bln1 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass1 = "PASS >> VPN disabled";
		String fail1 = "FAIL >> VPN NOT disabled";
		ALib.AssertTrueMethod(bln1, Pass1, fail1);
		Initialization.driverAppium.findElementById("android:id/button1").click();

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Cellular networks']").click();
		boolean bln2 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass2 = "PASS >> Cellular networks hotspot disabled";
		String fail2 = "FAIL >> Cellular networks hotspot NOT disabled";
		ALib.AssertTrueMethod(bln2, Pass2, fail2);
		Initialization.driverAppium.findElementById("android:id/button1").click();

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Network settings reset']")
				.click();
		boolean bln3 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass3 = "PASS >> Network settings reset  disabled";
		String fail3 = "FAIL >> Network settings reset  NOT disabled";
		ALib.AssertTrueMethod(bln3, Pass3, fail3);
		Initialization.driverAppium.findElementById("android:id/button1").click();
	}

	public void VerifyWifiSettings() throws IOException, InterruptedException {

		Runtime.getRuntime().exec("adb shell am start -n com.android.settings/com.android.settings.wifi.WifiSettings");
		sleep(3);
		String actual = Initialization.driverAppium.findElementById("com.android.settings:id/admin_support_msg")
				.getText();
		String expected = "This action is disabled. Contact your organization's administrator to learn more.";
		ALib.AssertEqualsMethod(expected, actual, "PASS >> WIFI disabled", "FAIL >> WIFI not disabled");

	}

	public void VerifyFactoryDataReset() throws InterruptedException 
	{
		common.commonScrollMethod("Backup & reset");
		common.ClickOnAppiumButtons("Backup & reset");
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Factory data reset']").click();
		boolean bln3 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass3 = "PASS >> Factory reset  disabled";
		String fail3 = "FAIL >> Factory reset  NOT disabled";
		ALib.AssertTrueMethod(bln3, Pass3, fail3);
		Initialization.driverAppium.findElementById("android:id/button1").click();
	}

	public void VerifyWallpaperDisabled() throws InterruptedException 
	{
		common.commonScrollMethod("Display");
		common.ClickOnAppiumText("Display");
		 Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Wallpaper']").click();
		 boolean bln= Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		 String Pass="PASS >> Wallpaper  disabled";
	     String fail="FAIL >> Wallpaper  NOT disabled";
	     ALib.AssertTrueMethod(bln, Pass, fail); 
        Initialization.driverAppium.findElementById("android:id/button1").click();
	 }
	public void USBFileTransfer() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell cmd statusbar expand-notifications");
		sleep(2);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='USB charging this device' and @index='0']")
				.click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[@text='Transfer files']")
				.click();
		boolean bln = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass = "PASS>>Filetransfer  disabled";
		String fail = "FAIL>>Filetransfer  NOT disabled";
		ALib.AssertTrueMethod(bln, Pass, fail);
		Initialization.driverAppium.findElementById("android:id/button1").click();
		Initialization.driverAppium
				.findElementByXPath("//android.widget.CheckedTextView[@text='Transfer photos (PTP)']").click();
		boolean bln1 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Action not allowed']").isDisplayed();
		String Pass1 = "PASS >> Filetransfer  disabled";
		String fail1 = "FAIL >> Filetransfer  NOT disabled";
		ALib.AssertTrueMethod(bln1, Pass1, fail1);
		Initialization.driverAppium.findElementById("android:id/button1").click();
	}

	public void USBfiletransferDisable() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell cmd statusbar expand-notifications");
		sleep(2);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Charging via USB' and @index='0']").click();
		sleep(2);
		Initialization.driverAppium
				.findElementByXPath("//android.widget.CheckedTextView[@text='Transfer files' and @index='0']").click();
		boolean bln = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Unable to perform action']").isDisplayed();
		String Pass = "PASS >> Filetransfer  disabled";
		String fail = "FAIL >> Filetransfer  NOT disabled";
		ALib.AssertTrueMethod(bln, Pass, fail);
		Initialization.driverAppium.findElementById("android:id/button1").click();
		Initialization.driverAppium
				.findElementByXPath("//android.widget.CheckedTextView[@text='Transfer images' and @index='0']").click();
		boolean bln1 = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Unable to perform action']").isDisplayed();
		String Pass1 = "PASS >> Filetransfer  disabled";
		String fail1 = "FAIL >> Filetransfer  NOT disabled";
		ALib.AssertTrueMethod(bln1, Pass1, fail1);
		Initialization.driverAppium.findElementById("android:id/button1").click();
	}

	public void VerifyMediaVolumeSettigsAreNOTDisabled() throws IOException, InterruptedException {
 
		common.commonScrollMethod("Sound");
		common.ClickOnAppiumText("Sound");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Media volume']").click();

		boolean bln = false;
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Action not allowed']")
					.isDisplayed();
		} catch (Exception e) {
			bln = true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> MediaVolume not disabled", "FAIL >> MediaVolume disabled");

	}

	public void AlarmVolumeNOTDisabled() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Alarm volume']").click();
		boolean bln = false;
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Action not allowed']")
					.isDisplayed();
		} catch (Exception e) {
			bln = true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> AlarmVolume not disabled", "FAIL >> AlarmVolume disabled");
	}

	public void RingVolumeNotDisabled() {

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Ring volume']").click();
		boolean bln = false;
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Action not allowed']")
					.isDisplayed();
		} catch (Exception e) {
			bln = true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> RingVolume not disabled", "FAIL >> RingVolume disabled");

	}

	public void VerifyFactoryDataResetNotDisabled() throws InterruptedException, IOException
	{
		common.commonScrollMethod("Backup & reset");
		common.ClickOnAppiumText("Backup & reset");
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Factory data reset']").click();
		boolean bln = false;
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Action not allowed']")
					.isDisplayed();
		} catch (Exception e) {
			bln = true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> FactoryDataReset not disabled", "FAIL >> FactoryDataReset disabled");
		;
		GoBack();
		GoBack();
	}


	public void VerifyWallpaperNOTDisabled() throws IOException, InterruptedException 
	{
		common.commonScrollMethod("Display");
		common.ClickOnAppiumText("Display");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Wallpaper']").click();
		boolean bln = false;
		try {
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Action not allowed']")
					.isDisplayed();
		} catch (Exception e) {
			bln = true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> Wallpaper not disabled", "FAIL >> Wallpaper disabled");
		GoBack();

	}

	public void VerifyWiFiConfiguration() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Wi-Fi']").click();
		String Actual = Initialization.driverAppium.findElementById("com.android.settings:id/admin_support_msg")
				.getText();
		String Expected = "You do not have permission to perform this action. Contact your organisation's IT administrator for more information.";
		ALib.AssertEqualsMethod(Expected, Actual, "PASS >> Wifi Disabled", "FAIL >> Wifi Not Disabled");
	}

	public void VerifyWifiNotDisabled() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Wi-Fi']").click();
		boolean bln = false;
		try {
			Initialization.driverAppium.findElementById("com.android.settings:id/admin_support_msg").isDisplayed();
		} catch (Exception e) {
			bln = true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> Wifi not Disabled", "FAIL >> Wifi Disabled");
	}

	public void VerifyTeetheringHotspot() throws InterruptedException {
		Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Mobile Hotspot and tethering']").click();
		sleep(1);
		boolean bln = Initialization.driverAppium
				.findElementByXPath("//android.widget.TextView[@text='Unable to perform action']").isDisplayed();
		String Pass = "PASS >> Teethering Hotspot disabled";
		String fail = "FAIL >> Teethering Hotspot NOT disabled";
		ALib.AssertTrueMethod(bln, Pass, fail);
		Initialization.driverAppium.findElementById("android:id/button1").click();
	}
	
	public void VerifyingFactoryDataResetNotDisabled() throws InterruptedException 
	{
		common.commonScrollMethod("General management");
		common.ClickOnAppiumText("General management");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Reset']").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Factory data reset']").click();
		boolean bln = false;
		try {
			Initialization.driverAppium.findElementById("com.android.settings:id/admin_support_msg").isDisplayed();
		} catch (Exception e) {
			bln = true;
		}
		ALib.AssertTrueMethod(bln, "PASS >> Factory Data Reset Not Disabled", "FAIL >> Factorydatareset disabled");
	}


	public void VerifyKioskMode() {
		boolean bln = Initialization.driverAppium
				.findElement(By.xpath("//android.widget.TextView[@text='Stop kiosk mode']")).isDisplayed();
		String pass = "PASS >> 'Stop kiosk mode' is displayed " + bln + "";
		String fail = "FAIL >> 'Stop kiosk mode' is displayed " + bln + "";
		ALib.AssertTrueMethod(bln, pass, fail);
	}

	public void VerifyAllowedAppsKioskMode() throws IOException, InterruptedException {
		Initialization.driverAppium.findElement(By.id("com.nix:id/pkg_icon")).click();
		boolean bln = Initialization.driverAppium.findElement(By.id("com.android.chrome:id/terms_accept"))
				.isDisplayed();
		String pass = "PASS >> 'Allowed App Chrome' is displayed " + bln + "";
		String fail = "FAIL >> 'Allowed App Chrome' is displayed " + bln + "";
		ALib.AssertTrueMethod(bln, pass, fail);
		GoBack();
	}

	public void ClickOnExitKioskMode() {
		Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Stop kiosk mode']")).click();
		boolean bln = Initialization.driverAppium
				.findElement(By.xpath("//android.widget.TextView[@text='KIOSK PASSWORD']")).isDisplayed();
		String pass = "PASS >> 'Enter Kiosk Password' Prompt Displayed " + bln + "";
		String fail = "FAIL >> 'Enter Kiosk Password' Prompt Displayed " + bln + "";
		ALib.AssertTrueMethod(bln, pass, fail);
		Initialization.driverAppium.findElement(By.xpath("//android.widget.EditText[@bounds='[50,653][670,744]']"))
				.click();
	}

	public void ClickOnExitKioskModesamsung() {
		Initialization.driverAppium.findElement(By.xpath("//android.widget.TextView[@text='Stop kiosk mode']")).click();
		boolean bln = Initialization.driverAppium
				.findElement(By.xpath("//android.widget.TextView[@text='KIOSK PASSWORD']")).isDisplayed();
		String pass = "PASS >> 'Enter Kiosk Password' Prompt Displayed " + bln + "";
		String fail = "FAIL >> 'Enter Kiosk Password' Prompt Displayed  " + bln + "";
		ALib.AssertTrueMethod(bln, pass, fail);
		Initialization.driverAppium.findElement(By.xpath("//android.widget.EditText[@bounds='[75,979][1005,1115]']"))
				.click();
	}

	public void EnterPasswordkioskMode() throws IOException {
		Runtime.getRuntime().exec("adb shell input keyevent 7");
		Runtime.getRuntime().exec("adb shell input keyevent 7");
		Runtime.getRuntime().exec("adb shell input keyevent 7");
		Runtime.getRuntime().exec("adb shell input keyevent 7");
		Initialization.driverAppium.findElementById("android:id/button1").click();
	}

//dummy code

	@FindBy(xpath = "(//div[text()='Device Registered'])[1]")
	private WebElement scroll;

	public void Verifyscrolling() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
		js.executeScript("arguments[0].scrollIntoView();", scroll);
		sleep(20);
	}
// new methods
	
	@FindBy(xpath="//*[@id='profileList_tableCont']/div/div[2]/div[1]/div[2]/div[1]/div/input")
	private WebElement SearchFiledProfile;
	
	@FindBy(xpath = "//div[@class='sec_body hideIf_Office365']//div[@id='profileList_tableCont']//input[@placeholder='Search']")
	private WebElement SearchProfiles;
	
	@FindBy(xpath="//span[normalize-space()='Enroll using your Gmail Account']")
	private WebElement EnrollUsingYourGmailAccount;
	
	@FindBy(xpath="//*[@id='manageByGooglePlay']/div[2]/div/span[2]")
	private WebElement EnrollUsingGsuiteID;
	
	@FindBy(xpath="//span[normalize-space()='Google Managed Domain']")
	private WebElement GoogleManagedDomain;
	
	@FindBy(xpath="//span[normalize-space()='EMM Token']")
	private WebElement EMMToken;
	
	public void SelectProfile(String profile) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td[text()='"+profile+"']")).click();
		sleep(2);
	}
	
	public void clickOnDeleteBtn() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//button[@id='deletePolicyBtn']")).click();
		sleep(3);
	}
	public void clickOnDeleteYESBtn() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")).click();
		sleep(3);
	}

	public void DeleteCreatedProfile(String profile) throws InterruptedException {
		    SearchFiledProfile.clear();
		    sleep(2);
		    SearchFiledProfile.sendKeys(profile);
			int profiles = Initialization.driver.findElements(By.xpath("//table[@id='policyTable']/tbody/tr/td[text()='" + profile + "']")).size();
			sleep(2);
			for(int i= 0;i<profiles;i++ ){
			SelectProfile(profile);
			sleep(2);
			clickOnDeleteBtn();
			sleep(2);
			clickOnDeleteYESBtn();
			sleep(2);}
	}
	public void clickOnEditInProfilePage() throws InterruptedException
	{
		EditProfileButton.click();
		waitForidPresent("passwordpolicyAndroidProfileConfig");
		sleep(2);
	}
	
	public void NotificationOnProfileDelete() throws InterruptedException {
		boolean value = true;

		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Profile deleted successfully.']")).isDisplayed();
			sleep(2);
		} catch (Exception e) {
			value = false;

		}
		Assert.assertTrue(value, "FAIL >> Profile Deletion Notification not displayed");
		Reporter.log("PASS >> Notification 'Profile deleted successfully.'is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
	}

	public void NotificationOnProfileCopied() throws InterruptedException {
		boolean value = true;

		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Profile copied successfully.']")).isDisplayed();

		} catch (Exception e) {
			value = false;
		}
		Assert.assertTrue(value, "FAIL >> Profile copied Notification not displayed");
		Reporter.log("PASS >> Notification 'Profile copied successfully.'is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
	}
	public void VerifyOfCopiedProfileName(String profile)
	{
		try
		{
			String a = Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td[text()='"+profile+"_copy']")).getText();
			System.out.println(a);
			Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td[text()='"+profile+"_copy']")).isDisplayed();
			Reporter.log("PASS >> Profile is copied successfully.",true);
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Profile didn't copied profile successfully.");
		}


	}
	public void SearchProfileInProfilePage(String profile) throws InterruptedException {
		SearchProfiles.clear();
		sleep(2);
		SearchProfiles.sendKeys(profile);
		sleep(3);
	}

	public void clickOnRefreshBtnInProfilePAge() throws InterruptedException {
		Initialization.driver
				.findElement(By.xpath(
						"//button[@id='policylistRefresh']//span[@class='nme txt'][normalize-space()='Refresh']"))
				.click();
		sleep(3);
	}

	public void VerifyOfExpectedProfileName(String profile) {
		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//table[@id='policyTable']/tbody/tr/td[2]"));
		boolean flag;
		for (int i = 0; i < ls.size(); i++) {
			String ActualValue = ls.get(i).getText();
			if (ActualValue.contains(profile)) {
				flag = true;
			} else {
				flag = false;
			}
			String PassStmt = "PASS >> Results are displayed according to the entered text.";
			String FailStmt = "Fail >> Results are not displayed according to the entered text.";
			ALib.AssertTrueMethod(flag, PassStmt, FailStmt);
		}
		
		
	}
	public void clickOnCopyBtn() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//button[@id='copyPolicyBtn']")).click();
		sleep(3);
		Initialization.driver.findElement(By.xpath("//button[@id='CopyProfile_OkBtn']")).click();
		sleep(2);
	}
	public void clickOnDefaultBtn() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//button[@id='setDefaultPolicy']")).click();
		sleep(2);
	}
	public void clickOnAddFolder() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//button[@id='addPolicyFolderBtn']")).click();
		sleep(5);
	}
	public void EnterFolderNameInProfile(String name) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//input[@id='jobFolderName']")).sendKeys(name);
		sleep(3);
		Initialization.driver.findElement(By.xpath("//button[@id='foldername_ok']")).click();
		waitForXpathPresent("//span[text()='New folder created.']");
	}
	public void NotificationOnCreatedFolderInProfile() throws InterruptedException {
		boolean value = true;

		try {
			Initialization.driver.findElement(By.xpath("//span[text()='New folder created.']")).isDisplayed();

		} catch (Exception e) {
			value = false;
		}
		Assert.assertTrue(value, "FAIL >> 'New folder created.' Notification not displayed");
		Reporter.log("PASS >>  'New folder created.' Notification is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
	}
	public void clickOnCreatedFolder() throws InterruptedException
	{
		Actions action=new Actions(Initialization.driver);
		action.doubleClick(Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td[2]")));
		action.build().perform();
		sleep(3);
	}
	
	public void clickOnSettingInProfile() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//button[@id='settingsPolicyBtn']//span[@class='nme'][normalize-space()='Settings']")).click();
		sleep(2);
	}
	public void clickOnUnenroll() throws InterruptedException
	{
		Initialization.driver.findElement(By.id("unenrol_li")).click();
		sleep(2);
		Initialization.driver.findElement(By.xpath("//button[@id='unenrollUser']")).click();
		waitForXpathPresent("//span[normalize-space()='Successfully unregistered EMM account.']");
	}
	public void UnEnrollAndroidEnterprise() throws InterruptedException
	{
		try {
			Initialization.driver.findElement(By.xpath("//button[@id='settingsPolicyBtn']//span[@class='nme'][normalize-space()='Settings']")).isDisplayed();
			clickOnSettingInProfile();
			sleep(2);
			clickOnUnenroll();
		}catch (Exception e) {
			clickOnEnrollAndroidEnterpriseBtn();
		}
	}
	public void clickOnEnrollAndroidEnterpriseBtn() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//button[normalize-space()='Enroll Android Enterprise']")).click();
		sleep(2);
	}
	public void NotificationOnFolderDeleteInProfile() throws InterruptedException {
		boolean value = true;

		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Folder(s) deleted successfully.']")).isDisplayed();
			sleep(2);
		} catch (Exception e) {
			value = false;

		}
		Assert.assertTrue(value, "FAIL >> Folder Deletion Notification not displayed");
		Reporter.log("PASS >> Notification 'Folder(s) deleted successfully.'is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
	}
	public void NotificationOnProfileDefualt() throws InterruptedException {
		try
		{
			Initialization.driver.findElement(By.xpath("//span[contains(text(),'Changed default profile. The default profile is now')]")).isDisplayed();
			sleep(2);
			Reporter.log("PASS >> Notification 'Changed default profile.'is displayed",true);
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Profile set as defualt Notification not displayed");
		}
	}
	public void VerifyOfOptionsInEAE() throws InterruptedException
	{
		boolean flag = EnrollUsingYourGmailAccount.isDisplayed();
		String PassStatement = "PASS >>  Enroll Using Your Gmail Account is displayed successfully when clicked on EAE Button";
		String FailStatement = "Fail >>  Enroll Using Your Gmail Account is not displayed when clicked on EAE Button";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		
		sleep(2);
		
		boolean flag1 = EnrollUsingGsuiteID.isDisplayed();
		String PassStatement1 = "PASS >>  Enroll Using Your Managed Google Account is displayed successfully when clicked on EAE Button";
		String FailStatement1 = "Fail >>  Enroll Using Your Managed Google Account is not displayed when clicked on EAE Button";
		ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
		
		sleep(2);
		
		Initialization.driver.findElement(By.xpath("//input[@id='GoogleAccounts']")).click();
		
		boolean flag2 = GoogleManagedDomain.isDisplayed();
		String PassStatement2 = "PASS >>  Google Managed Domain is displayed successfully when clicked on Enroll Using Gsuite ID Radio Button";
		String FailStatement2 = "Fail >>  Google Managed Domain Account is not displayed when clicked on Enroll Using Gsuite ID Radio Button";
		ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);
		
		sleep(2);
		
		boolean flag3 = EMMToken.isDisplayed();
		String PassStatement3 = "PASS >>  EMM Token is displayed successfully when clicked on Enroll Using Gsuite ID Radio Button";
		String FailStatement3 = "Fail >>  EMM Token is not displayed when clicked on Enroll Using Gsuite ID Radio Button";
		ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);
		
		sleep(2);
		
		Initialization.driver.findElement(By.xpath("//div[@id='enroll_AFW_pop']//button[@aria-label='Close']")).click();
	}
	public void VerifyOfDeleteFolderOrProfile(String profile)
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//table[@id='policyTable']/tbody/tr/td[text()='"+profile+"']")).isDisplayed();
			sleep(2);
		}catch (Exception e) {
			Assert.assertFalse(false, "Folder/Profile isn't present......");
			Reporter.log("Folder/Profile isn't present......",true);
		}
	}
	@FindBy(xpath="//*[@id='filesharingPolicyAndroidProfileRemoveTail']")
	private WebElement filesharingPolicyAndroidProfileRemoveTail;
	@FindBy(xpath="//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement ConfirmationDialogYes;
	
public void ClickOnRemoveFileTransferButton() throws InterruptedException {
	
	filesharingPolicyAndroidProfileRemoveTail.click();
	waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
	ConfirmationDialogYes.click();
	sleep(2);
	Reporter.log("FileTransfer Policy Removed",true);
	
	
	
}
//SWATI
@FindBy(xpath="//*[@id='enrollafwaccount']")
private WebElement enrollafwaccount;
@FindBy(xpath="//*[@id='enrollgoogleplayaccount']")
private WebElement enrollgoogleplayaccount;
@FindBy(xpath="//*[@id='afw_appsConfig_btn']/span[text()='Android Enterprise Apps']")
private WebElement afw_appsConfig_btn;

public void ClickOnEnrollAfwAcc() throws InterruptedException {
	
//	boolean value = true;

	
		if(afw_appsConfig_btn.isDisplayed()) {
		Initialization.driver.findElement(By.xpath("//*[@id='settingsPolicyBtn']/span[text()='Settings']")).click();
		waitForXpathPresent("(//p[text()='Unenroll'])[1]");
		Initialization.driver.findElement(By.xpath("(//p[text()='Unenroll'])[1]")).click();
		Initialization.driver.findElement(By.xpath("//*[@id='moreSettings_modal']/div/div/div[1]/button[@class='close']")).click();
		sleep(2);
		waitForXpathPresent("//p[text()='Do you really want to unenroll your Android Enterprise account from SureMDM?']");
		Initialization.driver.findElement(By.xpath("//*[@id='unenrollUser']")).click();
		waitForXpathPresent("//span[text()='You have successfully unregistered your Android Enterprise account.']");
		sleep(2);
		}
		else {
			enrollafwaccount.click();
			sleep(2);
			enrollgoogleplayaccount.click();
			waitForVisibilityOf("(//*[@title='Google Play Logo'])[2]");
			boolean GooglePlayLogo=Initialization.driver.findElement(By.xpath("(//*[@title='Google Play Logo'])[2]")).isDisplayed();
			String Pass="PASS>> 'user is redirected to Google Play for work website.'is displayed";
			String Fail="FAIL>> 'user is not redirected to Google Play for work website.'is displayed";
			ALib.AssertTrueMethod(GooglePlayLogo, Pass, Fail);
			sleep(2);

		}
}

@FindBy(xpath="//*[@id='yDmH0d']/c-wiz[2]/c-wiz/div/div[1]/div/div/div/div[2]/div[3]/div/div[2]/div/div[2]")
private WebElement ClickOnConfirm;

		public void EnrollAFW(String MailId,String Pwd) throws InterruptedException {   ////a[text()='Re-enroll']
			boolean value = true;
                                              
		
			try {
				Initialization.driver.findElement(By.xpath("//span[text()='Sign in']")).isDisplayed(); ////span[text()='Sign in']
		    	Initialization.driver.findElement(By.xpath("//span[text()='Sign in']")).click();
		    	waitForXpathPresent("//input[@type='email']");
		    	Initialization.driver.findElement(By.xpath("//input[@type='email']")).sendKeys(MailId);
		    	sleep(2);
		    	Initialization.driver.findElement(By.xpath("//*[@id='identifierNext']/div/button")).click();
		    	waitForXpathPresent("//input[@type='password']");
		    	sleep(2);
		    	Initialization.driver.findElement(By.xpath("//input[@type='password']")).sendKeys(Pwd);
		    	sleep(2);
		    	Initialization.driver.findElement(By.xpath("//*[@id='passwordNext']/div/button")).click();
			}
				catch (Exception e) {

					Reporter.log("Mail and pwd entered successfully",true);
				}
		}
		
		public void ConfirmPopUp() throws InterruptedException {
	//		boolean value = true;

			
	//		if(ClickOnConfirm.isDisplayed()) {
				
	//			ClickOnConfirm.click();
			
			    sleep(2);
				waitForXpathPresent("//span[text()='Get started']");
				sleep(2);
				Initialization.driver.findElement(By.xpath("//span[text()='Get started']")).click();
      	    	waitForXpathPresent("//h1[text()='Business name']");

                Reporter.log("Clicked On GetStarted",true);

				
			}
//			else {
//			//	if(Initialization.driver.findElement(By.xpath("//span[text()='Get started']")).isDisplayed()) {
//				Initialization.driver.findElement(By.xpath("//span[text()='Get started']")).click();
//                Reporter.log("Clicked On GetStarted",true);
//				}
//			}
//				catch (Exception e) {
//
//        		Assert.assertTrue(value, "Pass >> 'Navigated to next page'");
//        		Reporter.log("PASS >> 'Navigated to next page'", true);
//        		waitForXpathPresent("//h1[text()='Business name']");
//        		boolean BusinessNamePage=Initialization.driver.findElement(By.xpath("//h1[text()='Business name']")).isDisplayed();
//        		String Pass="PASS>> 'user is redirected to Business name website.'is displayed";
//        		String Fail="FAIL>> 'user is not redirected to Business name website.'is displayed";
//        		ALib.AssertTrueMethod(BusinessNamePage, Pass, Fail);
//        		sleep(2);
//        		
//        			}	
//
//		    	
//			
//		
//			}

		

@FindBy(xpath="//*[@id='yDmH0d']/c-wiz/div/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div/div[2]/label")
private WebElement BusinessNameInputBox;
public void InputAnyName(String AntName) throws InterruptedException {
	sleep(2);
	boolean BusinessNamePage=Initialization.driver.findElement(By.xpath("//h1[text()='Business name']")).isDisplayed();
	String Pass="PASS>> 'user is redirected to Business name website.'is displayed";
	String Fail="FAIL>> 'user is not redirected to Business name website.'is displayed";
	ALib.AssertTrueMethod(BusinessNamePage, Pass, Fail);
	sleep(2);

	BusinessNameInputBox.sendKeys(AntName);
	sleep(2);
	Initialization.driver.findElement(By.xpath("//span[text()='Next']")).click();
	waitForXpathPresent("//h1[text()='Contact details']");
	Reporter.log("Navigated to Contact details page",true);
}
		
public void ClickOnCheckBox() throws InterruptedException {
	
	Initialization.driver.findElement(By.xpath("//input[@type='checkbox']")).click();
	waitForXpathPresent("//span[text()='Confirm']");
	Initialization.driver.findElement(By.xpath("//span[text()='Confirm']")).click();
	waitForXpathPresent("//*[@id='yDmH0d']/c-wiz/div/div/div[2]/div/div[2]/div/div[3]/div/div[2]/div/div/button/div");
	sleep(2);
	boolean CompleteReg=Initialization.driver.findElement(By.xpath("//*[@id='yDmH0d']/c-wiz/div/div/div[2]/div/div[2]/div/div[3]/div/div[2]/div/div/button/div")).isDisplayed();
	String Pass="PASS>> 'Complete Registration'is displayed";
	String Fail="FAIL>> 'Complete Registration'is displayed";
	ALib.AssertTrueMethod(CompleteReg, Pass, Fail);
	sleep(2);

	
	
	}
		
		
public void ClickOnCompleteReg() throws InterruptedException {
		
		
	Initialization.driver.findElement(By.xpath("//span[text()='Complete Registration']")).click();
	waitForVisibilityOf("//span[text()='Successfully enrolled your enterprise']");
	boolean SuccessMsg=Initialization.driver.findElement(By.xpath("//span[text()='Successfully enrolled your enterprise']")).isDisplayed();
	String Pass="PASS>> 'Successfully enrolled your enterprise' is displayed";
	String Fail="FAIL>> 'Successfully enrolled your enterprise' is disaplayed";
	ALib.AssertTrueMethod(SuccessMsg, Pass, Fail);
	sleep(2);

		
		}
public void VerifyRedirectedToConsole() throws InterruptedException {
	
	boolean SettingsIcon=Initialization.driver.findElement(By.xpath("//*[@id='settingsPolicyBtn']/span[text()='Settings']")).isDisplayed();
	String Pass="PASS>> 'SettingsIcon' is displayed";
	String Fail="FAIL>> 'SettingsIcon' is disaplayed";
	ALib.AssertTrueMethod(SettingsIcon, Pass, Fail);
	sleep(2);
	boolean AFW=afw_appsConfig_btn.isDisplayed();
	String Pass2="PASS>> 'afw_appsConfig_btn' is displayed";
	String Fail2="FAIL>> 'afw_appsConfig_btn' is disaplayed";
	ALib.AssertTrueMethod(AFW, Pass2, Fail2);
	sleep(2);
Reporter.log("Successfully redirected to console",true);
}
public void WindowHandle() throws InterruptedException {
	String originalHandle = Initialization.driver.getWindowHandle();
	ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
	Initialization.driver.switchTo().window(tabs.get(1));
	sleep(10);
}

public void ClickOnAFWEnterprise() throws InterruptedException {
	
	afw_appsConfig_btn.click();
	waitForVisibilityOf("//h4[text()='Android Enterprise Apps']");
	Initialization.driver.findElement(By.xpath("//*[@id='afw_apps_modal']/div/div/div[2]/ul/li[1]/div[2]/a")).click();
	WindowHandle();
	waitForXpathPresent("//a[text()='My Managed Apps']");
	boolean GooglePlayWebsite=	Initialization.driver.findElement(By.xpath("//a[text()='My Managed Apps']")).isDisplayed();
	String Pass="PASS>> 'User redirected to Google play for work website.'is displayed";
	String Fail="FAIL>> 'User redirected to Google play for work website.'is Not displayed";
	ALib.AssertTrueMethod(GooglePlayWebsite, Pass, Fail);
	sleep(2);


	
}
@FindBy(xpath="//input[@placeholder='Search']")
private WebElement SearchTextBoxGooglePlay;

@FindBy(xpath="//button[@aria-label='Google Search']")
private WebElement GoogleSearch;

public void SerachApp(String AppName) throws InterruptedException {
	
	SearchTextBoxGooglePlay.sendKeys(AppName);
	sleep(2);
	GoogleSearch.click();
	waitForXpathPresent("//div[text()='Google Chrome: Fast & Secure']");
	sleep(2);
	
}
public void ClickOnApp() throws InterruptedException {
	
	Initialization.driver.findElement(By.xpath("(//div[text()='Google Chrome: Fast & Secure'])[3]")).click();
	waitForVisibilityOf("//span[text()='Approve']");
	sleep(2);
	
	
}
public void ClickOnApprove() throws InterruptedException {
	
	boolean ApproveApp=	Initialization.driver.findElement(By.xpath("//span[text()='Approve']")).isDisplayed();
	String Pass="PASS>> 'Approve'is displayed";
	String Fail="FAIL>> 'Approve'is Not displayed";
	ALib.AssertTrueMethod(ApproveApp, Pass, Fail);
	sleep(2);

	Initialization.driver.findElement(By.xpath("//span[text()='Approve']")).click();
	waitForXpathPresent("//div[text()='Showing permissions for all versions of this app']");
	Initialization.driver.findElement(By.xpath("(//span[text()='Approve'])[2]")).click();
	

	
}

// SWATI

@FindBy(xpath = "//select[@id='passwordPolicyProfileType']")   //  //passwordPolicyQuality
private WebElement passwordPolicyQuality;

@FindBy(xpath = "//select[@id='passwordPolicyProfileType']") //id=""  //passwordPolicyQuality_chosen
private WebElement passwordPolicyQuality_chosen;

public void SelectDeviceMinimumPasswordQuality(String value) throws InterruptedException {
	/*
	 * sleep(5); passwordPolicyQuality_chosen.click(); sleep(5);
	 */
	Initialization.driver.findElement(By.id("passwordPolicyProfileType_chosen")).click();
	
	  Select dropOption = new Select(Initialization.driver.findElement(By.id("passwordPolicyProfileType_chosen")));
	  dropOption.selectByValue(value); 
	  sleep(2);
	 
	 
	
	
	
	

}



}


	






