package SanityTestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Jobs_TestScripts extends Initialization{
	
	@Test(priority=0,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnEachPlatform() throws InterruptedException
	{
	  //android job
		Reporter.log("=====1.Android TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Android TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
		@Test(priority=1,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOniOSPlattform() throws InterruptedException
	{
		//iOS job
		Reporter.log("=====2.iOS TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOniOSOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("iOS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=2,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
		public void CreateOneJobOnWindowsOSPlatform() throws InterruptedException
		{
		//WindowsOS job
		Reporter.log("=====3.Windows TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Windows TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		}
	@Test(priority=3,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnWindowsSEPlatform() throws InterruptedException
	{
		//WindowsSE OS Job
		Reporter.log("=====4.WindowsSE TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsSEOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("WindowsSE TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=4,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnWindowsMobilePlatform() throws InterruptedException
	{
		//WindowsMobile OS Job
		Reporter.log("=====5.WindowsMobile TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsMobileOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("WindowsMobile TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=5,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnAnyOSPlatform() throws InterruptedException
	{
		//AnyOS Job
		Reporter.log("=====6.AnyOS TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAnyOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("AnyOS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}	
	@Test(priority=6,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnAndroidwearPlatform() throws InterruptedException
	{
		//Android wear OS Job
		Reporter.log("=====7.Android wear OS TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidWearOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Android wear OS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=7,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnLinuxPlatform() throws InterruptedException
	{
		//Linux OS Job
		Reporter.log("=====8.LinuxOS TextMessageJob=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnLinuxOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("LinuxOS TextMessageJob","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	
	@Test(priority=8,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnAndroidVRPlatform() throws InterruptedException
	{
		//Android VR OS
		Reporter.log("=====9.Android VR OS Install Application job=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidVR();
		androidJOB.ClickOnInstallApplication();
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterJobName(Config.EnterJobWithApkUrl);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL(Config.EnterApkUrl);
		androidJOB.EnterDevicePath("Home");
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
		
	}
	@Test(priority=9,description="Verify Creating a job from the NEW UI for all Platforms(Each Platform one job)")
	public void CreateOneJobOnmacOSPlatform() throws InterruptedException
	{
		//macOS 
		Reporter.log("=====10.macOS Reboot Job=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnmacOSR();
		androidJOB.clickOnReboot();
		androidJOB.enterRebootJobName("macOS Reboot Job");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority='A',description="Jobs - verify Search option in jobs page.")
	public void VerifyOfSeachOptionInJobsPAge() throws InterruptedException
	{
		Reporter.log("=====11.verify Search option in jobs page.=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname,"TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.SearchJobInList(Config.TextMessageJobname);
		androidJOB.VerifyOfSearchOptionInJobsPage(Config.TextMessageJobname);
	}
	@Test(priority='B',description="Jobs - Verify creating New Folder in jobs.")
	public void VerifyOfCreatingNewFolderInJobs() throws InterruptedException
	{
		Reporter.log("=====12.erify creating New Folder in jobs.=====",true);
		androidJOB.clickOnJobs();
		androidJOB.ClickOnNewFolder();
		androidJOB.NamingFolder("Auto");
		androidJOB.VerifyOfFolderCreatedPopup();
		androidJOB.SearchJobInList("Auto");
		androidJOB.VerifyOfSearchOptionInJobsPage("Auto");
	}
	@Test(priority='C',description="Jobs - Verify Move to Folder in Jobs page.")
	public void VerifyOfMoveToFolderInJobs() throws InterruptedException
	{
		Reporter.log("=====13.Verify Move to Folder in Jobs page.=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname,"TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.ClickOnNewFolder();
		androidJOB.NamingFolder("Folder000");
		androidJOB.SearchJobInList(Config.TextMessageJobname);
		androidJOB.clickOnMoveToFolder();
		androidJOB.SelectFolder("Folder000");
		androidJOB.VerifyOfMoveJobToFolderPopUp(Config.TextMessageJobname,"Folder000");
	}
	@Test(priority='D',description="Jobs - Verify Pagination in Jobs Page")
	public void VerifyOfPaginationInJobsPage() throws InterruptedException
	{
		Reporter.log("=====14. Verify Pagination in Jobs Page=====",true);
		commonmethdpage.ClickOnHomePage();
		androidJOB.clickOnJobs();
		androidJOB.ClearSearchFieldInJobsPage();
		androidJOB.VerifyOfPagination();
	}
	@Test(priority='E',description="Jobs - verify Copy in jobs page.")
	public void VerifyOfCopyInJobsPage() throws InterruptedException
	{
		Reporter.log("=====15.verify Copy in jobs page.=====",true);
		androidJOB.clickOnJobs();
		androidJOB.ClickOnNewFolder();
		androidJOB.NamingFolder("Folder000");
		//androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("@TextMessage","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.SearchJobInList("@TextMessage");
		androidJOB.clickOnCopyBtn();
		androidJOB.SelectFolderInCopyJobPage("Folder000");
		androidJOB.VerifyOfCopiedJobPopUp();
		androidJOB.SearchJobInList("copy");
		androidJOB.VerifyOnCopiedJob("@TextMessage");
	}
	@Test(priority='F',description="Jobs - Verify applying copied job to device")
	public void ApplyingCopiedJobToDevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====16.Verify applying copied job to device=====",true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("@TextMessage_copy");
		androidJOB.JobInitiatedOnDevice();
		newUIScriptsPage.ReadingConsoleMessageForJobDeployment("0TextMessage_copy",Config.DeviceName);
	}
	@Test(priority='G',description="Jobs - Verify delete in jobs page")
	public void VerifyOfDeleteJobInJobsPage() throws InterruptedException
	{
		Reporter.log("=====17. Verify delete in jobs page=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("Delete 0Job","TextVerifyJob","notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.SearchJobInList("Delete 0Job");
		androidJOB.deleteCreatedJob();
		androidJOB.JobDeletedMessage();
		androidJOB.SearchJobInList("@Folder");
		androidJOB.VerifyOfSearchOptionInJobsPage("@Folder");
	}
	@Test(priority='H',description="Validate if Install Job is working as expected")
	public void VerifyOfInstallJob() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("=====18.Validate if Install Job is working as expected=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.EnterJobWithApkUrl);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL(Config.EnterApkUrl);
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.clickOkBtnOnAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.EnterJobWithApkUrl);
		androidJOB.JobInitiatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnterJobWithApkUrl,360);
		androidJOB.CheckApplicationIsInstalledDeviceSide(Config.BundleID);
		androidJOB.unInstallMultipleApplication(Config.BundleID);
		Reporter.log("Pass >>Validate if Install Job is working as expected",true);
	}
	@Test(priority='I',description="Validate if Multiple Install Job is working as expected")
	public void VerifyOfMultipleInstallJob() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("=====19.Validate if Multiple Install Job is working as expected=====",true);
		androidJOB.clickOnJobs(); 
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS(); 
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName("EnterJobWithMulipleApkUrl");
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL(Config.EnterMultipleApkUrl);
		androidJOB.clickOkBtnOnIstallJob(); 
		androidJOB.clickOkBtnOnAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnterJobWithMulipleApkUrl");
		androidJOB.JobInitiatedMessage();
		androidJOB.CheckStatusOfappliedInstalledJob("EnterJobWithMulipleApkUrl",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckApplicationsInstalledDeviceSide("com.gears42.astrocontacts,com.gears42.surelock");
		androidJOB.unInstallMultipleApplication(Config.MultipleBundleID); 
		Reporter.log("Pass >>Validate if Multiple Install Job is working as expected",true);
	}
		
}
