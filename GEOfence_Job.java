package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;

public class GEOfence_Job extends Initialization {
	
	
	
	   @Test(priority=3,description="Geo Fence - UI Validation")
	    public void verifyGEOFenceUI() throws InterruptedException
	    {
	    	 androidJOB.clickOnJobs();
	 		androidJOB.clickNewJob();
	 		androidJOB.clickOnAndroidOS();
	 		androidJOB.ClickOnGeoFenceJobType("geo_fencing_job");	 
	 		androidJOB.VerifyGeoFenceUI();
	    }
	    
	    @Test(priority=4,description="Geo Fence - Verify download templates in Select Fence in Geo fence job")
	    public void VerifyDownloadTemplate() throws EncryptedDocumentException, InvalidFormatException, InterruptedException, IOException
	    {
	    	androidJOB.VerifydownloadedFileInLocation("GeoFences_Template",androidJOB.DownloadGeoFenceTemplateButton);
	    	androidJOB.ClosingGeoFenceTab();
	    	commonmethdpage.ClickOnHomePage();
	    }



}

