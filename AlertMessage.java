package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class AlertMessage extends Initialization{

	@Test(priority=1, description="\n New job in android/iOS/windows  platform ")
	public void TC_JO_903() throws InterruptedException, IOException{
		Reporter.log("\n New job in android/iOS/windows  platform ",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.Verifyalert_messageJobPresent();
//	   androidJOB.ClickOnCloseAlertMessage();
}
	@Test(priority=2, description="\n Make sure when we modify a built in tool alert job built in tool should only open ")
	public void TC_JO_904Create() throws InterruptedException, IOException{
		Reporter.log("\n Make sure when we modify a built in tool alert job built in tool should only open ",true);
		androidJOB.ClickOnalert_messageJob();
		androidJOB.CreateNewAlertimg_option();
		androidJOB.InputJobNameAlertMessage("AlertMessageJobType");
		androidJOB.alertTitle_Input("Alert");
		androidJOB.alertTitleColorClick();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#e0cd1f");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.InputAlertDescription("DescriptionAlert");
		androidJOB.alertDescriptionColorClick();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#37de13");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.alertIconSelectionInput();
		androidJOB.selectDropDownPredefined_icons();
		androidJOB.ClickOnUploadIcon();
		androidJOB.SelectPredefinedIcon();
		androidJOB.ClickOnalertBackgroundColor();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#204cc8");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.Alert_Display_typeClick();
		androidJOB.SelectDropDownAlert_Display_type();
		androidJOB.EnableAlertBuzz();
		androidJOB.InputAlertBuzzTime("20");
		androidJOB.EnableCloseAlertPopupEnable();
		androidJOB.InputCloseAlertPopup("30");
		androidJOB.ClickOnSaveOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority=3, description="\n Make sure when we modify a built in tool alert job built in tool should only open ")
	public void TC_JO_904() throws InterruptedException, IOException{

		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("AlertMessageJobType");
		androidJOB.ClickOnOnJob("AlertMessageJobType");
		androidJOB.ModifyJobButton();
		androidJOB.WaitFor_AlertMessagePopUp("Create Alert Message");
		androidJOB.ScrollToPreviewButton();
		androidJOB.ClickOnPreviewButton();
		androidJOB.previewBlkModalCloseBtn_Click();
		androidJOB.ClickOnSaveOkButton();
		androidJOB.VerfyModificationSuccessMessage();



	
}
	
	@Test(priority=4, description="\n Create a alert job with create new alert with built in tool ")
	public void TC_JO_906() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
	
		Reporter.log("\n Create a alert job with create new alert with built in tool ",true);
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("AlertMessageJobType");
		commonmethdpage.Selectjob("AlertMessageJobType");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
        androidJOB.CheckStatusOfappliedInstalledJob("AlertMessageJobType",600);
        androidJOB.VerifyDeviceSide();
        
        
        }
	@Test(priority=5, description="\n Verify Preview functionality in Alert message job with \"!\" charcter in title and decription"+"verify close button for the preview prompt in Alert message ")
	public void PreviewFunctionalityValidate() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		
		Reporter.log("\n Verify Preview functionality in Alert message job with \"!\" charcter in title and decription",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		androidJOB.CreateNewAlertimg_option();
		androidJOB.InputJobNameAlertMessage("AlertMessageJobType");
		androidJOB.alertTitle_Input("Alert!!!");
		androidJOB.InputAlertDescription("TestAlert!!!");
        androidJOB.ScrollToPreviewButton();
        androidJOB.ClickOnRightAlignment();
		androidJOB.ClickOnPreviewButton();
		androidJOB.VerifyPreviewFunctionTitle();
		androidJOB.VerifyOnlyPreviewPopUpCloses();
		androidJOB.CloseBtn();
		androidJOB.ClickOnNewJobBackButton();
		androidJOB.osPanel_cancel_btnClick();
		commonmethdpage.ClickOnHomePage(); 



}
	@Test(priority=6, description="\n Verify Default background color after modifying the alert message job.")
	public void DefaultBackgroundColor() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		
		
		Reporter.log("\n Verify Default background color after modifying the alert message job.",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		androidJOB.CreateNewAlertimg_option();
        androidJOB.ScrollToPreviewButton();
//    	androidJOB.ClickOnalertBackgroundColor();
//		androidJOB.WaitForSelectColorPopUp();

        androidJOB.VerifyDefaultBackgrndColor();
		androidJOB.CloseBtn();
		androidJOB.ClickOnNewJobBackButton();
		androidJOB.osPanel_cancel_btnClick();
		commonmethdpage.ClickOnHomePage(); 

		
		
		
		
}
	
	@Test(priority=7, description="\n check the validation for alert message job with built in tool ")
	public void TC_JO_905() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
	
		Reporter.log("\n check the validation for alert message job with built in tool",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		androidJOB.CreateNewAlertimg_option();
		androidJOB.ClickOnSaveOkButton();
		androidJOB.VerifySaveWithoutFillingTheData();
		androidJOB.InputJobNameAlertMessage("AlertMessageJobType");
		androidJOB.ClickOnSaveOkButton();
        androidJOB.VerifySaveWithoutFillingTheData();
		androidJOB.alertTitle_Input("Alert");
		androidJOB.ClickOnSaveOkButton();
		androidJOB.VerifySaveWithoutFillingAlertDescription();
		androidJOB.CloseBtn();
		androidJOB.ClickOnNewJobBackButton();
		androidJOB.osPanel_cancel_btnClick();
		commonmethdpage.ClickOnHomePage(); 

		
	}

	@Test(priority=8,description="\n Modify a alert message job from built in tool ")
	public void TC_JO_907() throws Throwable{
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		androidJOB.clickOnJobs();
		androidJOB.ClearSearchJobInSearchField();
		dynamicjobspage.SearchJobInSearchField("AlertMessageJobType");
		androidJOB.ClickOnOnJob("AlertMessageJobType");
		androidJOB.ModifyJobButton();
		androidJOB.WaitFor_AlertMessagePopUp("Create Alert Message");
		androidJOB.ClearInputJobNameAlertMessage();
		androidJOB.InputJobNameAlertMessage("Modify_AlertMessageJobType");
		
    	androidJOB.ScrollToUploadButton();
		androidJOB.selectDropDownPredefined_icons();
        androidJOB.UploadCustomIcon_Select();
    	androidJOB.ClickOnalertBackgroundColor();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#204cc8");
		androidJOB.ClickOnoKbtnAlertTitleColor();
        androidJOB.clickOnuploadalertFile("./Uploads/UplaodFiles/FileStore/\\Image.exe");
        
        androidJOB.ScrollToPreviewButton();
		androidJOB.ClickOnPreviewButton();
		androidJOB.previewBlkModalCloseBtn_Click();
		androidJOB.Alert_Display_typeClick();
		androidJOB.SelectDropDownAlert_Display_type2();
		androidJOB.EnableSnoozeCheckBox();
		androidJOB.InputSnoozeDuration("4");
       
		androidJOB.ClickOnSaveOkButton();
		androidJOB.VerfyModificationSuccessMessage();
		
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("Modify_AlertMessageJobType");
		commonmethdpage.Selectjob("Modify_AlertMessageJobType");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();

        androidJOB.CheckStatusOfappliedInstalledJob("Modify_AlertMessageJobType",600);
        
        androidJOB.VerifyDeviceSideSnoozePopup();
        androidJOB.ClickOnOpenSnoozePopUp();
		
	}
		
	@Test(priority=9,description="\n Create a alert job with Upload an existing alert image from your system")
	public void TC_JO_908Create() throws Throwable{
		

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		
		androidJOB.ClickOnUploadExistingAlert();
		androidJOB.InputJobNameAlertMessage("UploadExistingAlert");
		androidJOB.ClickOnSelect_File_Type();
		androidJOB.clickOnuploadalert("./Uploads/UplaodFiles/FileStore/\\Image.exe");
		
		androidJOB.ClickOnPreviewButton();
		androidJOB.previewBlkModalCloseBtn_Click();
		androidJOB.Alert_Display_typeClick();
		
		androidJOB.SelectDropDownAlert_Display_type();
		androidJOB.EnableAlertBuzz();
		androidJOB.InputAlertBuzzTime("20");
		androidJOB.EnableCloseAlertPopupEnable();
		androidJOB.InputCloseAlertPopup("30");
		androidJOB.ClickOnSaveOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority='A',description="\n Create a alert job with Upload an existing alert image from your system")
	public void TC_JO_908() throws Throwable{
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("UploadExistingAlert");
		commonmethdpage.Selectjob("UploadExistingAlert");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
        androidJOB.CheckStatusOfappliedInstalledJob("UploadExistingAlert",600);
        
        androidJOB.VerifyDeviceSide();
        
		}
	@Test(priority='B',description="\n Verify on Adding GIF images in Alert template message job is getting deployed to device")
	public void GIFimageCreate() throws Throwable{	
		

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		
		androidJOB.ClickOnUploadExistingAlert();
		androidJOB.InputJobNameAlertMessage("UploadExistingAlert");
		androidJOB.ClickOnSelect_File_Type();
		androidJOB.clickOnuploadalert("./Uploads/UplaodFiles/Job On Android/FileTransferSingle/\\File5.exe");
		
		androidJOB.ClickOnPreviewButton();
		androidJOB.previewBlkModalCloseBtn_Click();
		androidJOB.Alert_Display_typeClick();
		
		androidJOB.SelectDropDownAlert_Display_type();
		androidJOB.EnableAlertBuzz();
		androidJOB.InputAlertBuzzTime("20");
		androidJOB.EnableCloseAlertPopupEnable();
		androidJOB.InputCloseAlertPopup("30");
		androidJOB.ClickOnSaveOkButton();
		androidJOB.JobCreatedMessage();
	}
	@Test(priority='C',description="\n Verify on Adding GIF images in Alert template message job is getting deployed to device")
	public void GIFimage() throws Throwable{	
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("UploadExistingAlert");
		commonmethdpage.Selectjob("UploadExistingAlert");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
        androidJOB.CheckStatusOfappliedInstalledJob("UploadExistingAlert",600);
        
        androidJOB.VerifyDeviceSide();
 





}
	
	@Test(priority='D',description="\n Modify a alert message job from Upload an existing alert image from your system")
	public void TC_JO_909() throws Throwable{
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.ClearSearchJobInSearchField();
		dynamicjobspage.SearchJobInSearchField("UploadExistingAlert");
		androidJOB.ClickOnOnJob("UploadExistingAlert");
		androidJOB.ModifyJobButton();
		androidJOB.WaitFor_AlertMessagePopUp("Upload Existing Alert");
		androidJOB.ClearInputJobNameAlertMessage();
		androidJOB.InputJobNameAlertMessage("Modified_UploadExistingAlert");
        androidJOB.SelectVideoFile();
		androidJOB.AlertVideoUrl("./Uploads/UplaodFiles/FileStore/\\Video.exe");
		
		androidJOB.Alert_Display_typeClick();
		androidJOB.SelectDropDownAlert_Display_type2();
		androidJOB.EnableSnoozeCheckBox();
		androidJOB.InputSnoozeDuration("4");
       
		androidJOB.ClickOnSaveOkButton();
		androidJOB.VerfyModificationSuccessMessage();
		
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("Modified_UploadExistingAlert");
		commonmethdpage.Selectjob("Modified_UploadExistingAlert");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();

        androidJOB.CheckStatusOfappliedInstalledJob("Modified_UploadExistingAlert",600);
        
        androidJOB.VerifyDeviceSideSnoozePopup();
        androidJOB.ClickOnOpenSnoozePopUpVideo();
		
		
		}

	@Test(priority='E',description="\n check the validation for alert message job with Existing alert image ")
	public void TC_JO_110() throws Throwable{
		
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		androidJOB.ClickOnUploadExistingAlert();
		androidJOB.ClickOnSaveOkButton();
		androidJOB.VerfyErrorMessageAlertImage();
		androidJOB.InputJobNameAlertMessage("UploadExistingAlert");
		androidJOB.ClickOnSaveOkButton();
		androidJOB.VerfyErrorMessageAlertImage();
		androidJOB.CloseButtonClick();


		
		
	}
		
	@Test(priority='F',description="\n Verify customization of the popup notification of alert messages.")
	public void TC_001() throws Throwable{	
		
		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		androidJOB.CreateNewAlertimg_option();
        androidJOB.ScrollToPreviewButton();
		androidJOB.selectAlert_Display_typeDropDown();
		androidJOB.GetDefaultValueAndMessageTextBox();

		androidJOB.verifyCustomizingValueTextField("Automation");

		androidJOB.verifyCustomizingMessageTextField("Automation Test");
		androidJOB.CloseBtnAlertMessage_modal();
		
		
		
}
	
	@Test(priority='G',description="\n Verify Snooze option even if the user closes the popup notification window.")
	public void TC_002Create() throws Throwable{	
		
		androidJOB.ClickOnalert_messageJob();
		androidJOB.CreateNewAlertimg_option();
		androidJOB.InputJobNameAlertMessage("AlertMessageJobType2");
		androidJOB.alertTitle_Input("Alert");
		androidJOB.alertTitleColorClick();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#e0cd1f");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.InputAlertDescription("DescriptionAlert");
		androidJOB.alertDescriptionColorClick();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#37de13");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.alertIconSelectionInput();
		androidJOB.selectDropDownPredefined_icons();
		androidJOB.ClickOnUploadIcon();
		androidJOB.SelectPredefinedIcon();
		androidJOB.ClickOnalertBackgroundColor();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#204cc8");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.ScrollToPreviewButton();
		androidJOB.selectAlert_Display_typeDropDown();
		androidJOB.ScrollToEnableSnooze();
		androidJOB.EnableSnoozeCheckBox();
		androidJOB.InputSnoozeDuration("4");
        androidJOB.ClickOnSaveOkButton();
		androidJOB.JobCreatedMessage();
		
	}
	@Test(priority='H',description="\n Verify Snooze option even if the user closes the popup notification window.")
	public void TC_002() throws Throwable{	

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("AlertMessageJobType2");
		commonmethdpage.Selectjob("AlertMessageJobType2");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
        androidJOB.CheckStatusOfappliedInstalledJob("AlertMessageJobType2",600);

        androidJOB.VerifyDeviceSideSnoozePopupAgain();
        androidJOB.ClickOnHomeButtonDeviceSide();
        androidJOB.VerifyDeviceSideSnoozePopupAgain();
        androidJOB.ClickOnOpenAlertPopUp();


		
		
		
		
		
	}
	@Test(priority='I',description="\n Verify applying a Alert message job with modified  Message.")
	public void TC_012Create() throws Throwable{	
		

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnalert_messageJob();
		androidJOB.CreateNewAlertimg_option();

		androidJOB.InputJobNameAlertMessage("AlertMessageJobType3");
		androidJOB.alertTitle_Input("Alert");
		androidJOB.alertTitleColorClick();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#e0cd1f");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.InputAlertDescription("DescriptionAlert");
		androidJOB.alertDescriptionColorClick();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#37de13");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.alertIconSelectionInput();
		androidJOB.selectDropDownPredefined_icons();
		androidJOB.ClickOnUploadIcon();
		androidJOB.SelectPredefinedIcon();
		androidJOB.ClickOnalertBackgroundColor();
		androidJOB.WaitForSelectColorPopUp();
		androidJOB.InputColorAlertTitle("#204cc8");
		androidJOB.ClickOnoKbtnAlertTitleColor();
		androidJOB.ScrollToPreviewButton();
		androidJOB.selectAlert_Display_typeDropDown();
		androidJOB.MessageTextField("Automation Alert Message");
		androidJOB.ScrollToEnableSnooze();
		androidJOB.EnableSnoozeCheckBox();
		androidJOB.InputSnoozeDuration("4");
        androidJOB.ClickOnSaveOkButton();
		androidJOB.JobCreatedMessage();
		
	}
	@Test(priority='J',description="\n Verify applying a Alert message job with modified  Message.")
	public void TC_012() throws Throwable{	

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("AlertMessageJobType3");
		commonmethdpage.Selectjob("AlertMessageJobType3");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
        androidJOB.CheckStatusOfappliedInstalledJob("AlertMessageJobType3",600);
        androidJOB.VerifyDeviceSideSnoozePopupAgain();
        androidJOB.VerifyAlertPopUp3("Automation Alert Message");
        androidJOB.ClickOnHomeButtonDeviceSide();
        androidJOB.VerifyDeviceSideSnoozePopupAgain();
        androidJOB.ClickOnOpenAlertPopUp();






}
	@Test(priority='k',description="\n Verify  Runscript is created when user Enables Alert message Analytics.")
	public void TC_004() throws Throwable{	
		
		
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();
		accountsettingspage.EnableDataAnalyticsCheckBox();
		accountsettingspage.DisableAlertMessageAnalyticsCheckBox();
		accountsettingspage.ClickOnDataAnalyticsApplyButton();
        accountsettingspage.EnableAlertAnalytics();
		accountsettingspage.ClickOnDataAnalyticsApplyButton();
        accountsettingspage.ClcikOnAlertAnalyticsEnablePopUpYesButton();
		androidJOB.clickOnJobs();
		commonmethdpage.SearchJobInSearchField("Enable Alert Message Analytics");
		commonmethdpage.VerifyAlertRunscriptInJob("Enable Alert Message Analytics");
		commonmethdpage.SearchDevice(Config.DeviceName);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.AnalyticsJobName,360);

		





		
				
	}
}
		
		
		

