package GridPaginationTest;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

//No of Devices in console should be more than 100
public class GridPagination extends Initialization {
	
	@Test(priority='1',description="1.Verify Device count on the 1st,2nd and 3rd page when max number of device selected 20")
	public void VerifyDeviceCountOnFor20() throws InterruptedException, IOException {
	Reporter.log("1.Verify Device count on the 1st,2nd and 3rd page when max number of device selected 20");
	commonmethdpage.ClickOnAllDevicesButton();
    gridpagination.SelectLastPagination();
    gridpagination.GetDeviceCount();
	gridpagination.ClickOnDevicesPerPagebutton();
	gridpagination.chooseDevicesPerPage20();
	}
	 
	@Test(priority='2',description="2.Verify Device count on the 1st,2nd and 3rd page when max number of device selected 50")
	public void VerifyDeviceCountOnFor50() throws InterruptedException, IOException {
	Reporter.log("2.Verify Device count on the 1st,2nd and 3rd page when max number of device selected 50");
	gridpagination.ClickOnDevicesPerPagebutton();
	gridpagination.chooseDevicesPerPage50();
	}

	@Test(priority='3',description="3.Verify pagination after action")
	public void VerifyPaginationAfterAction() throws InterruptedException, IOException {
	gridpagination.logoutfromApp();
	gridpagination.LoginAgain();
	gridpagination.VerifyNumberOfDevicesAfterActions();
	gridpagination.ClickOnReports_Button();
	commonmethdpage.ClickOnHomePage();
	gridpagination.VerifyNumberOfDevicesAfterActions();
	}

	@Test(priority='4',description="4.Verify Device count on the 1st,2nd and 3rd page when max number of device selected 100")
	public void VerifyDeviceCountOnFor100() throws InterruptedException, IOException {
	Reporter.log("4.Verify Device count on the 1st,2nd and 3rd page when max number of device selected 100");
	gridpagination.ClickOnDevicesPerPagebutton();
	gridpagination.chooseDevicesPerPage100();
	gridpagination.ClickOnDevicesPerPagebutton(); // going back to 20
	gridpagination.chooseDevicesPerPage20();
	}


}
