package PageObjectRepository;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class Blacklisted_POM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(id="blacklistedSection")
	private WebElement BlacklistedSectionBtn;
	
	@FindBy(id="blWhitelistBtn")
	private WebElement WhiteListDeviceBtn;
	
	@FindBy(id="blDeleteBtn")
	private WebElement BlacklistDeviceDeleteBtn;
	
	@FindBy(xpath="//p[text()='"+Config.DeviceName+"']") 
	private WebElement BlacklistDevice1;

	
	@FindBy(xpath="//p[text()='775']") 
	private WebElement BlacklistNewDevice;
	
	@FindBy(xpath="//p[text()='819']") 
	private WebElement BlacklistDevice2;
	
	@FindBy(id="blacklistDeviceBtn")
	private WebElement ClickOnBlacklistButtonOnTop;
	
	@FindBy(xpath="//p[text()='Are you sure you want to blocklist the selected device{s}?']")
	private WebElement warningMessageBlacklistDevices;
	
	@FindBy(xpath="//p[text()='Are you sure you want to allowlist the device(s)?']")
	 private WebElement warningMessageWhitelistDevices;
	
	@FindBy(xpath="//p[text()='Are you sure you want to remove the device from the list of blocklisted devices?']")
	private WebElement warningMessageDeleteDevices;
	
	@FindBy(xpath=".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='No']")
	private WebElement ClickOnNoButtonBlacklistWarningDialog;
	
	@FindBy(xpath=".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement ClickOnYesButtonBlacklistWarningDialog;
	
	@FindBy(xpath="//span[text()='2 devices were blocklisted.']")
	private WebElement deviceBlacklistedSuccessfulMessage;
	
	@FindBy(xpath="//span[text()='Device(s) allowlisted successfully.']")
	private WebElement deviceWhitelistSuccessfulMessage;
	
	@FindBy(xpath="//td[text()='"+Config.DeviceName+"']")
	private WebElement isBlacklistedDevice1Displayed;
	
	@FindBy(xpath="//td[text()='"+Config.DeviceName1+"']") 
    private WebElement isBlacklistedDevice2Displayed;
	
	@FindBy(xpath="//span[text()='Please select a device.']")
	private WebElement warningMessageOnWhitelistWithoutSelectingDevice;
	
	@FindBy(xpath="//td[text()='"+Config.DeviceName+"']")
	private WebElement whitelistDevice;
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='No']")
	private WebElement NoButton_WarningDialogWhitelistDevice;
	
	@FindBy(xpath=".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesButton_WarningDialogWhitelistDevice;
	
	@FindBy(xpath="//div [@id='groupstree']/ul/li[text()='Home']")
	private WebElement ClickOnHomeGroup;
	
	@FindBy(xpath="//span[text()='Device(s) deleted successfully.']")
	private WebElement Message_deviceDeleteFromBlackisted;
	
	@FindBy(xpath="//div[contains(text(),'No Device available.')]")
	private WebElement MessageWhenNoDeviceAvailableInBlacklistedWindow;
	
	@FindBy(xpath=".//*[@id='panelBackgroundDevice']/div[2]/div[2]/div[1]/table/thead/tr/th[1]/div[1]")
	private WebElement ClickOnDeviceNameColumnInBlacklistedWindow;
	
	@FindBy(xpath=".//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")
	private WebElement ClickOnSearchBlacklistedWindow;
	
	@FindBy(xpath="//td[text()='"+Config.DeviceName+"']")
	private WebElement isTheSearchedDeviceDisplayed;
	
	@FindBy(id="blRefreshBtn")
	private WebElement RefreshButtonBlacklist;
	
	
	
	Actions action=new Actions(Initialization.driver);
	
	public void ClickOnBlacklisted() throws InterruptedException
	{
		BlacklistedSectionBtn.click();
		waitForidPresent("blDeleteBtn");
		sleep(2);
		Reporter.log("Clicked on Blacklisted section",true);
		sleep(10);
		
	}
	
	public boolean VerifyOfWhiteListDeviceBtnEnabled()
	{
		String ActualValue=WhiteListDeviceBtn.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		return value;
	}
	
	public void ClickOnWhitelistButton() throws InterruptedException
	{
		WhiteListDeviceBtn.click();
		sleep(2);
		
	}
	
	public void ClickOnRefreshButton() throws InterruptedException
	{
		RefreshButtonBlacklist.click();
		sleep(2);
	}
	
	public boolean VerifyOfBlacklistDeviceDeleteBtnEnabled()
	{
		String ActualValue=BlacklistDeviceDeleteBtn.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		return value;
	}
	
	public void ClickOnDevice1ToBlackList(String DeviceName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//p[text()='"+Config.DeviceName+"']")).click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    sleep(4);
	}
	
	public void ClickOnBlacklistButton() throws InterruptedException
	{
		ClickOnBlacklistButtonOnTop.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    sleep(1);
	}
	
	public void WarningMessageOnBlacklistingADevice() throws InterruptedException
  	{
  			 
  	       boolean isdisplayed =true;
  			 
  			 try{
  				warningMessageBlacklistDevices.isDisplayed();
  				   	 
  				    }catch(Exception e)
  				    {
  				    	isdisplayed=false;
  				   	 
  				    }
  		 
  		   Assert.assertTrue(isdisplayed,"FAIL >> blacklis warning NOT displayed");
  		   Reporter.log("PASS >> 'Are you sure you want to blacklist selected device(s)?' is displayed",true );	
  		   waitForPageToLoad();
  		   Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
 	       sleep(1);
  		 }
	  
	 public void ClickinOnNoButtonWarningDialogBlacklist() throws InterruptedException
	 {
		 ClickOnNoButtonBlacklistWarningDialog.click();
		 boolean isblacklistButtondisplayed = ClickOnBlacklistButtonOnTop.isDisplayed();
		 String pass = "PASS >> Blacklist button is displayed - Cancelled the warning dialog";
		 String fail = "FAIL >> Blacklist button is NOT displayed- did not cancel warning dialog";
		 ALib.AssertTrueMethod(isblacklistButtondisplayed, pass, fail);
		 Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	     sleep(1);
	 }
	 
	 public void ClickOnYesButtonWarningDialogBlacklist(String DeviceName) throws InterruptedException
	 {
		 ClickOnYesButtonBlacklistWarningDialog.click();
		 sleep(2);
		 
		 try
		 {
			 Initialization.driver.findElement(By.xpath("//span[text()='The device named " +"\""+DeviceName+"\" is now blocklisted.']"));
		     Reporter.log("Devcie BlockLited Message Is Displayed Successfully",true);
		 }
		 
		 catch (Exception e) 
		 {
			 ALib.AssertFailMethod("Devcie BlockLited Message Is Not Displayed");
		}
		    
	 }
	 public void ClickOnYesButtonWarningDialogBlacklistForMultipleDevices() throws InterruptedException
	 {
		 ClickOnYesButtonBlacklistWarningDialog.click();
		 sleep(2);
		 
		 try
		 {
			 Initialization.driver.findElement(By.xpath("//span[text()='2 devices were blocklisted.']"));
		     Reporter.log("Devcie BlockLited Message Is Displayed Successfully",true);
		 }
		 
		 catch (Exception e) 
		 {
			 ALib.AssertFailMethod("Devcie BlockLited Message Is Not Displayed");
		}
		    
	 }
	 
	 public void IstheBlacklistedDevicePresent(String DeviceName) throws InterruptedException
	 {
		 boolean isDevice1Present = Initialization.driver.findElement(By.xpath("//td[text()='"+DeviceName+"']")).isDisplayed();
		 String pass = "PASS >> blacklisted device 1 is present";
		 String fail = "FAIL >> blacklisted device  1 is NOT present";
		 ALib.AssertTrueMethod(isDevice1Present, pass, fail);
		 Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	     sleep(1);
	 }
	 
	 public void ClickOnWhitelistDevice1() throws InterruptedException
	 {
		 isBlacklistedDevice1Displayed.click();
		 sleep(1);
	 }
	 
	 public void ClickOnWhitelistDevice2() throws InterruptedException
	 {
		 isBlacklistedDevice2Displayed.click();
		 sleep(1);
	 }
	 
	 public void WarningMessageOnWhitelistingWithoutSelectingDevice() throws InterruptedException
	  	{
	  			 
	  	       boolean isdisplayed =true;
	  			 
	  			 try{
	  				warningMessageOnWhitelistWithoutSelectingDevice.isDisplayed();
	  				   	 
	  				    }catch(Exception e)
	  				    {
	  				    	isdisplayed=false;
	  				   	 
	  				    }
	  		 
	  		   Assert.assertTrue(isdisplayed,"FAIL >> whitelist warning NOT displayed");
	  		   Reporter.log("PASS >> 'Please select a device.' is displayed",true );	
	  		   waitForPageToLoad();
	  		   Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	 	       sleep(1);
	    }
	 
	 public void WarningMessageOnWhitelistingADevice() throws InterruptedException
	  	{
	  			 
	  	       boolean isdisplayed =true;
	  			 
	  			 try{
	  				warningMessageWhitelistDevices.isDisplayed();
	  				   	 
	  				    }catch(Exception e)
	  				    {
	  				    	isdisplayed=false;
	  				   	 
	  				    }
	  		 
	  		   Assert.assertTrue(isdisplayed,"FAIL >> whitelist warning NOT displayed");
	  		   Reporter.log("PASS >> 'Are you sure you want to whitelist selected device(s)?' is displayed",true );	
	  		   Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	 	       sleep(2);
	  }
	 
	 public void WarningMessageOnDeletingADeviceFromBlacklistedlist() throws InterruptedException
	  	{
	  			 
	  	       boolean isdisplayed =true;
	  			 
	  			 try{
	  				warningMessageDeleteDevices.isDisplayed();
	  				   	 
	  				    }catch(Exception e)
	  				    {
	  				    	isdisplayed=false;
	  				   	 
	  				    }
	  		 
	  		   Assert.assertTrue(isdisplayed,"FAIL >> delete warning NOT displayed");
	  		   Reporter.log("PASS >> 'Are you sure you want to delete the device(s)?' is displayed",true );	
	  		   Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	 	       sleep(2);
	  		 }
	 
	 
	 public void ClickOnDeviceToWhiteList() throws InterruptedException
	 {
		 whitelistDevice.click();
		 sleep(2);
	 }
	 
	 public void ClickingOnNoButtonWarningDialogWhitelist() throws InterruptedException
	 {
		 NoButton_WarningDialogWhitelistDevice.click();
		 boolean isblacklistButtondisplayed = WhiteListDeviceBtn.isDisplayed();
		 String pass = "PASS >> Whitelist button is displayed - Cancelled the warning dialog";
		 String fail = "FAIL >> Whitelist button is NOT displayed- did not cancel warning dialog";
		 ALib.AssertTrueMethod(isblacklistButtondisplayed, pass, fail);
		 Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	     sleep(1);
	 }
	 
	 public void ClickOnYesButtonWarningDialogWhitelist() throws InterruptedException
	 {
		 YesButton_WarningDialogWhitelistDevice.click();
		 sleep(1);
		 boolean isdisplayed =true;
		 
		 try{
			 deviceWhitelistSuccessfulMessage.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> Successfull Message displayed");
	   Reporter.log("PASS >> 'Device(s) whitelisted successfully' is received",true );	
	   waitForPageToLoad();
	   
	 }
	 
	 public void ClickOnHomeGroup() throws InterruptedException
	 {
		 ClickOnHomeGroup.click();
		 waitForidPresent("deleteDeviceBtn");
		 sleep(7);
	 }
	 
	
	 
	 public void SelectMultipleDevicesToBlacklist() throws InterruptedException
	 {
		 
	//	 ClickOnDevice1ToBlackList();
		 action.keyDown(Keys.CONTROL).build().perform();
		 sleep(10);
		 BlacklistDevice2.click();
		 sleep(10);
		 action.keyUp(Keys.CONTROL).build().perform();
	 }
	 
	 public void SelectMultipleDevicesToWhitelist(String Device1,String Device2) throws InterruptedException
	 {
		      
	     WebElement dev1 = Initialization.driver.findElement(By.xpath("//td[text()='"+Device1+"']"));
	 	 WebElement dev2 = Initialization.driver.findElement(By.xpath("//td[text()='"+Device2+"']"));
	 	 action.keyDown(Keys.CONTROL).click(dev1).click(dev2).keyUp(Keys.CONTROL).build().perform();
	 	 sleep(2);
	 }
	 
	 public void SelectMultipleDevicesToDelete() throws InterruptedException
	 {
		 List <WebElement> ls = Initialization.driver.findElements(By.xpath(".//*[@id='blacklistGrid']/tbody/tr/td[1]"));
		 ls.get(0).click(); //clicking the 1st device
		 action.keyDown(Keys.CONTROL).build().perform();
		 ls.get(1).click(); //clicking the 2nd device
		 action.keyUp(Keys.CONTROL).build().perform();
		 Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	     sleep(1);
	 }
	 
	
	 
	 public void ClickOnDeleteButton_Blacklist() throws InterruptedException
	 {
		 BlacklistDeviceDeleteBtn.click();
		 sleep(1);
	 }
	 
	 public void MessageOnSuccessfullDeletionOfDevicesFromBlacklistedList() throws InterruptedException
	 {
         boolean isdisplayed =true;
		 
		 try{
			 Message_deviceDeleteFromBlackisted.isDisplayed();
			   	 
			    }catch(Exception e)
			    {
			    	isdisplayed=false;
			   	 
			    }
	 
	   Assert.assertTrue(isdisplayed,"FAIL >> Successfull Message displayed");
	   Reporter.log("PASS >> 'Device(s) deleted successfully.' is received",true );	
	   sleep(2);
	 }
	 
	 public void ClickOnYesButtonWarningDialogDelete() throws InterruptedException
	 {
		 YesButton_WarningDialogWhitelistDevice.click();
		 sleep(6);
	 
	 }
	 
	 public void VerifyDeviceCountAfterDeletingASingleDeviceFromBlacklisted() throws InterruptedException
	 {
		 List <WebElement> count = Initialization.driver.findElements(By.xpath(".//*[@id='blacklistGrid']/tbody/tr/td[1]"));
		 int actualCount = count.size();
		 int expectedCount =1;
		 String pass = "One device deleted successfully from the blacklisted list";
		 String fail = "failed to delete device from the blacklisted lise";
		 ALib.AssertEqualsMethodInt(expectedCount, actualCount, pass, fail);
		 Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	     sleep(2);
	 }
	 
	 public void ClickOnNewdeviceToBlackList() throws InterruptedException
	 {
		 BlacklistNewDevice.click(); // new device to blacklist
		 Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 sleep(4);
	 }
	 
	 @FindBy(xpath="//table[@id='blacklistGrid']//tr/td[2]")
	 private List<WebElement> BlacklistDevciedata;
	 
	 public void MessageWhenNoDeviceAvailableInBlacklistedWindow()
	 {
		int NoOFDevices=0;
		 for(int i=0;i<BlacklistDevciedata.size();i++)
		 {
			 NoOFDevices++;
		 }
		 if(NoOFDevices==0)
		 {
		 String expectedMessage = MessageWhenNoDeviceAvailableInBlacklistedWindow.getText();
		 String actualValue ="No Device available.";
		 String pass = "PASS >>  message: "+expectedMessage+" is displayed when No device is available in blacklisted window";
		 String fail = "FAIL >> message is not displayed in the blacklisted window when no device is avilable";
		 ALib.AssertEqualsMethod(expectedMessage, actualValue, pass, fail);
		 }
		 else
		 {
			 Reporter.log("Device Is Present In BlackList i.e no Message Is Displayed",true);
		 }
	 }
	 
	 public void GetJobDevicesNameInBlacklistedWindow() //before and after sorting
		{
			//Getting Total count devices in blackliste
		    List<WebElement> NoOfDevices = Initialization.driver.findElements(By.xpath(".//*[@id='blacklistGrid']/tbody/tr/td[1]"));
			int count = NoOfDevices.size(); // Getting count devices available in Blacklisted window
			System.out.println(count);
		    for(int i=0;i<count;i++){
	   		    String actual =NoOfDevices.get(i).getText();
			    Reporter.log(actual);
	   	}
		}
	 
	//***Clicking On the Device Name Column of Blacklisted window**
		public void ClickOnDeviceNameColumnColumnOfBlacklisteWindow() throws InterruptedException
		{
			ClickOnDeviceNameColumnInBlacklistedWindow.click();
			sleep(2);
		}
		
		public void ClickOnSearchFieldBlacklistedWindow(String DeviceName) throws InterruptedException
		{
			ClickOnSearchBlacklistedWindow.sendKeys(DeviceName);
			boolean isdisplayed = isTheSearchedDeviceDisplayed.isDisplayed();
			String deviceName = Config.DeviceName;
			String pass = "PASS >> Device "+deviceName+" is searched successfully";
			String fail = "FAIl >> Device is not searched";
			ALib.AssertTrueMethod(isdisplayed, pass, fail);
			sleep(1);
			ClickOnSearchBlacklistedWindow.clear();
			sleep(1);
			ClickOnSearchBlacklistedWindow.sendKeys(Keys.ENTER);
			sleep(2);
			
		}
					
				
		
		    

			
			
		}
	 
	 
	 
	 
	
		  
		  
	 
	 

