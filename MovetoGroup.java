package RightClick;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class MovetoGroup extends Initialization
{

	@Test(priority='1',description="1.To verify Creating Group and SubGroup")
	public void VerifyCreatingGroupAndSubGroup() throws InterruptedException {
		Reporter.log("\n1.To verify Creating Group and SubGroup",true);
	    rightclickDevicegrid.ClickonNewGroup();
		rightclickDevicegrid.CreateNewGroup(Config.GroupName);
		rightclickDevicegrid.CreatedGroupisEmpty();
		Reporter.log("PASS >> Group Created Sucessfully",true);
		rightclickDevicegrid.VerifyCreatingSubGroup(Config.GroupName1);
		rightclickDevicegrid.CreatedGroupisEmpty();
		Reporter.log("PASS >> SubGroup Created Sucessfully",true);
	}
	
  @Test(priority='2',description="2.To Verify Adding Single device to group")
	public void VerifyAddingDeviceToGroup() throws Exception{
	  Reporter.log("\n2.To Verify Adding Single device to group",true);
		rightclickDevicegrid.ScrollToHome();
		rightclickDevicegrid.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName);
		rightclickDevicegrid.VerifyDeviceMoved();
		
	}
	
	@Test(priority='3',description="3.To Verify Adding Single device to Subgroup")
	public void VerifyAddingDevicetoSubgroup() throws Exception{
		Reporter.log("\n3.To Verify Adding Single device to Subgroup",true);
		rightclickDevicegrid.ScrollToHome();
		rightclickDevicegrid.SearchDevice(Config.MultipleDevices1);
		rightclickDevicegrid.RightClickOndevice(Config.MultipleDevices1);
		rightclickDevicegrid.MoveToSubGroup(Config.GroupName1);
		rightclickDevicegrid.VerifyDeviceMoved();
		
	}
	
	@Test(priority='4',description="4.To Verify Deleting Group By Moving DeviceTo HomeGroup")
	public void VerifyDeletingGroupByMovingDeviceToHomeGroup() throws InterruptedException
	{   
		Reporter.log("\n4.To Verify Deleting Group By Moving DeviceTo HomeGroup",true);
		rightclickDevicegrid.ClickOnGroup(Config.GroupName);
		rightclickDevicegrid.ClickOnDeleteGroup(Config.GroupName);
		rightclickDevicegrid.DeleteGroupByMovingDeviceToHome();
		rightclickDevicegrid.GroupDeletedSuccessfullyMsg();
	}
	
	
	@Test(priority='5',description="5.To Verify Moving Multiple Devices To Groups")
	public void VerifyMovingMultipleDeviestoGroup() throws Exception 
	{
		Reporter.log("\n5.To Verify Moving Multiple Devices To Groups",true);
		rightclickDevicegrid.ClickonNewGroup();
		rightclickDevicegrid.CreateNewGroup(Config.GroupName);
		rightclickDevicegrid.CreatedGroupisEmpty();
		rightclickDevicegrid.ScrollToHome();
		groupspage.ClickOnHomeGrup();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
		rightclickDevicegrid.SelectMultipleDevice();
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroupMultiple(Config.GroupName);
		Filter.ClickOnAllDevices();
		rightclickDevicegrid.ClickOnGroup(Config.GroupName);
     	rightclickDevicegrid.VerifyMultipleDevicesAdded();
     	rightclickDevicegrid.ClickOnDeleteGroup(Config.GroupName);
		rightclickDevicegrid.DeleteGroupByMovingDeviceToHome();
		rightclickDevicegrid.GroupDeletedSuccessfullyMsg();
	}

}
