package Settings_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Utility;


public class BrandingInfo_AccountSettings extends Initialization{
	
	@Test(priority=1,description="1.To Verify Account Settings Window's UI")
	public void VerifyAccountSettingWindowUI() throws InterruptedException{
		Reporter.log("\n1.To Verify Account Settings Window's UI",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.VerifyOfAccountSettingsPage();
		accountsettingspage.BrandingInfoSectionVisible();
		accountsettingspage.DeviceEnrollmentSectionVisible();
		accountsettingspage.MiscellaneousSettingsSectionVisible();
		accountsettingspage.SSOSectionVisible();
		accountsettingspage.AlertTemplateSectionVisible();
		accountsettingspage.CustomizeToolbarSectionVisible();
		Reporter.log("PASS>> Verification of Account Settings Window's UI",true);
	}
	
	@Test(priority=2,description="2.To Verify Branding Info Window's UI when Use Logo is Unchecked")
	public void VerifyBrandingInfoWindowUIUseLogIsUnchecked() throws InterruptedException {
		Reporter.log("\n2.To Verify Branding Info Window's UI when Use Logo is Unchecked",true);
		accountsettingspage.UseLogoIsSelected(true);
		accountsettingspage.UseLogoCheckedOptionVisible();
		accountsettingspage.UseLogoMessageFooterOptionVisible();
		Reporter.log("PASS>> Verification of Branding Info Window's UI when Use Logo is Unchecked",true);
	}
	
	@Test(priority=3,description="3.To Verify Branding Info when Title and SubTitle are empty")
	public void VerifyBrandingInfoWhenTitleAndSubtitleEmpty() throws InterruptedException {
		Reporter.log("\n3.To Verify Branding Info when Title and SubTitle are empty",true);
		accountsettingspage.ClearTitle();
		accountsettingspage.ClearSubTitle();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		accountsettingspage.VerifyErrorMessageTitle(true);
		Reporter.log("PASS>> Verification of Branding Info when Title and SubTitle are empty",true);
	}
	
	@Test(priority=4,description="4.To Verify Branding Info when Title is empty and Valid date entered in SubTitle.")
	public void VerifyBrandingInfoWhenTitleEmptryValidDateEnteredInSubTitle() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n4.To Verify Branding Info when Title is empty and Valid date entered in SubTitle.",true);
		accountsettingspage.ClearTitle();
		accountsettingspage.ClearSubTitle();
		accountsettingspage.EnterSubTitle();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		accountsettingspage.VerifyErrorMessageTitle(true);
		Reporter.log("PASS>> Verification of Branding Info when Title is empty and Valid date entered in SubTitle.",true);
		}
	
	@Test(priority=5,description="5.To Verify Branding Info when SubTitle is empty and Valid date entered in Title.")
	public void VerifyBrandignInfoWhenSubTitleIsEmptyAndValidDateEnteredInTitle() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n5.To Verify Branding Info when SubTitle is empty and Valid date entered in Title.",true);
		accountsettingspage.ClearTitle();
		accountsettingspage.ClearSubTitle();
		accountsettingspage.EnterTitle();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		accountsettingspage.VerifyErrorMessageSubTitle();
		Reporter.log("PASS>> Verification of Branding Info when SubTitle is empty and Valid date entered in Title.",true);
		}
	
	@Test(priority=6,description="6.To Verify Branding Info when Valid date entered in both Title and SubTitle is empty.")
	public void VerifyBrandingInfoWhenValidDateEnteredInBothTitleSubtitle() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n6.To Verify Branding Info when Valid date entered in both Title and SubTitle is empty.",true);
		accountsettingspage.ClearTitle();
		accountsettingspage.ClearSubTitle();
		accountsettingspage.EnterTitle();
		accountsettingspage.EnterSubTitle();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.VerifyHomePageTitle();
		accountsettingspage.VerifyHomePageSubTitle();
		Reporter.log("PASS>> Verification of Branding Info when Valid date entered in both Title and SubTitle is empty.",true);
	
		}

	@Test(priority=7,description="7.To Verify Branding Info when Message Footer is empty with Use Logo Unchecked.")
	public void VerifyBrandingInfoWhenMessageFooterisEmptyUseLogoUnchecked() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n7.To Verify Branding Info when Message Footer is empty with Use Logo Unchecked.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClearMessageFooter();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		dynamicmessagepage.ClickOnMore();
		dynamicmessagepage.ClickOnMessage();
		dynamicmessagepage.EnterMessageSubject();
		dynamicmessagepage.EnterMessageBody();
		dynamicmessagepage.ClickOnForceReadMessage();
		dynamicmessagepage.ClickOnSend();
		Reporter.log("PASS>> Verification of Branding Info when Message Footer is empty with Use Logo Unchecked.",true);
		
	}
	
	@Test(priority=8,description="8.To Verify Branding Info with Use Logo Unchecked when Valid date entered in Message Footer.")
	public void VerifyBrandingInfoUseLogUncheckedWHenValidDateEnteredInMessageFooter() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n8.To Verify Branding Info with Use Logo Unchecked when Valid date entered in Message Footer.",true);
		accountsettingspage.TestFailureBrandingInfoMthd2();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClearMessageFooter();
		accountsettingspage.EnterMessageFooter();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClearSearchDevice();
		commonmethdpage.SearchDevice();
		dynamicmessagepage.ClickOnMore();
		dynamicmessagepage.ClickOnMessage();
		dynamicmessagepage.EnterMessageSubject();
		dynamicmessagepage.EnterMessageBody();
		dynamicmessagepage.ClickOnForceReadMessage();
		dynamicmessagepage.ClickOnSend();
		Reporter.log("PASS>> Verification of Branding Info with Use Logo Unchecked when Valid date entered in Message Footer.",true);
	}
	
	@Test(priority=9,description="9.To Verify Branding Info Window's UI when Use Logo is Checked")
	public void VerifyBrandingInfoWindowUIWhenUseLogoIsChecked() throws InterruptedException {
		Reporter.log("\n9.To Verify Branding Info Window's UI when Use Logo is Checked",true);
		accountsettingspage.TestFailureBrandingInfoMthd2();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.UseLogoIsSelected(false);
		accountsettingspage.UseLogoUnCheckedOptionVisible();
		accountsettingspage.UseLogoMessageFooterOptionVisible();
		Reporter.log("PASS>> Verification of Branding Info Window's UI when Use Logo is Checked",true);
	}
	
	@Test(priority=10,description="10.To Verify Branding Info when Logo Path empty")
	public void VerifyBrandingInfoWhenLogoPathEmpty() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n10.To Verify Branding Info when Logo Path empty",true);
		accountsettingspage.TestFailureBrandingInfoMthd1();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		accountsettingspage.VerifyErrorMessageLogoPathFile(true);
		Reporter.log("PASS>> Verification of Branding Info when Logo Path empty",true);
	}
	
	@Test(priority=11,description="11.To Verify Branding Info when Logo Path image size is greater than 1MB.")
	public void VerifyBrandingInfoWhenLogoImageIsGreaterThan1MB() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n11.To Verify Branding Info when Logo Path image size is greater than 1MB.",true);
		accountsettingspage.TestFailureBrandingInfoMthd1();
		accountsettingspage.BrowseLogoPath("./uploads/UplaodFiles/Branding Info/\\File2.exe");
		accountsettingspage.VerifyErrorMessageLogoPathSize(true);
		Reporter.log("PASS>> Verification of Branding Info when Logo Path image size is greater than 1MB",true);
	}
	
	@Test(priority=12,description="12.To Verify Branding Info when Vaild Logo Path iis Browsed" )
	public void VerifyBrandingInfoWhenValidLogoPathisBrowsed() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n12.To Verify Branding Info when Vaild Logo Path iis Browsed",true);
		accountsettingspage.TestFailureBrandingInfoMthd1();
		accountsettingspage.BrowseLogoPath("./uploads/UplaodFiles/Branding Info/\\File1.exe");
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		accountsettingspage.ConfirmationMessageVerify(true);
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.VerifyOfLogoDisplyedOnHomePage();
		Reporter.log("PASS>> Verification of Branding Info when Vaild Logo Path iis Browsed",true);
	}
	
	@Test(priority=13,description="13.To Verify Branding Info when Message Footer is empty with Use Logo Checked.")
	public void VerifyBrandingInfoWhenMessageFooterIsEmptryWithUseLogoChecked() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n13.To Verify Branding Info when Message Footer is empty with Use Logo Checked.",true);
		accountsettingspage.TestFailureBrandingInfoMthd1();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClearMessageFooter();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		accountsettingspage.ConfirmationMessageVerify(true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClearSearchDevice();
		commonmethdpage.SearchDevice();
		dynamicmessagepage.ClickOnMore();
		dynamicmessagepage.ClickOnMessage();
		dynamicmessagepage.EnterMessageSubject();
		dynamicmessagepage.EnterMessageBody();
		dynamicmessagepage.ClickOnForceReadMessage();
		dynamicmessagepage.ClickOnSend();
		Reporter.log("PASS>> Verification of Branding Info when Message Footer is empty with Use Logo Checked.",true);
	}
	
	@Test(priority=14,description="14.To Verify Branding Info with Use Logo Checked when Valid date entered in Message Footer.")
	public void VerifyBrandingInfoWithUseLogoCheckedWhenValidDateEnteredInMessageFooter() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n14.To Verify Branding Info with Use Logo Checked when Valid date entered in Message Footer.",true);
		accountsettingspage.TestFailureBrandingInfoMthd1();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClearMessageFooter();
		accountsettingspage.EnterMessageFooter();
		accountsettingspage.ClickOnBrandingInfoApplyBtn();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClearSearchDevice();
		commonmethdpage.SearchDevice();
		dynamicmessagepage.ClickOnMore();
		dynamicmessagepage.ClickOnMessage();
		dynamicmessagepage.EnterMessageSubject();
		dynamicmessagepage.EnterMessageBody();
		dynamicmessagepage.ClickOnForceReadMessage();
		dynamicmessagepage.ClickOnSend();
		Reporter.log("PASS>> Verification of Branding Info with Use Logo Checked when Valid date entered in Message Footer.",true);
	}
	
	
	@AfterMethod
	public void AfterMthd(ITestResult result)
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
		   Utility.captureScreenshot(driver, result.getName());
		}
		
	}
}

