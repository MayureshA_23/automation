package PageObjectRepository;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;


public class ChangePassword_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	String XpathforPageAfterClickOnChngePwd = "//h4[text()='Change Your Account Password']";
	String IdforPageAfterClickOnSettings = "generateQRCode";
	String IdforConsolePageToLoad = "deleteDeviceBtn";
	
	
    @FindBy(id="changePassword")
	private WebElement ChangePasswordBtn;
	
	@FindBy(id="okbtn")
	private WebElement ChangePasswordokbtn;
	
	@FindBy(id="oldpassword")
	private WebElement OldPasswordtextbox;
	
	@FindBy(id="newpassword")
	private WebElement NewPasswordtextbox;
	
	@FindBy(xpath="//span[text()='Retype New Password']/following-sibling::input")
	private WebElement RetypeNewPasswordtextbox;
	
	@FindBy(xpath="//span[ul[li[text()='Enter old Password']]]/preceding-sibling::div/span")
	private WebElement ErrorMessageOldPassword;
	
	@FindBy(xpath="//span[@id='incorrect_oldpassword']/preceding-sibling::div/span")
	private WebElement ErrorMessageOldPasswordIncorrect;
	
	@FindBy(xpath="//span[ul[li[text()='Password too short. Minimum 4 characters']]]/preceding-sibling::div/span")
	private WebElement ErrorMessageNewPassword;
	
	@FindBy(xpath="//span[ul[li[text()='Password do not match']]]/preceding-sibling::div/span")
	private WebElement ErrorMessageRetypeNewPassword;
	
	@FindBy(xpath="//h4[text()='Change Your Account Password']/preceding-sibling::button")
	private WebElement ChngePasswordCloseBtn;
	
	@FindBy(xpath="//h4[text()='Change Your Account Password']")
	private WebElement titleChngePwd;
	
	@FindBy(id="warningArea")
	private WebElement LoginErrorMessage;
	
	@FindBy(xpath="//span[text()='Password reset successful.']")
	private WebElement ConfirmationMessage;
	
	
	public void ClickOnChangePasword() throws InterruptedException{
		
		ChangePasswordBtn.click();
		waitForXpathPresent(XpathforPageAfterClickOnChngePwd);
		sleep(5);
	
	}
	
	public void ClickOnChangePasword_UM()
	{
		ChangePasswordBtn.click();
	}
	
	public void titleCheckChangePassword()
	{
		String Actualvalue=titleChngePwd.getText();
		String Expectedvalue="Change Your Account Password";
		String PassStatement="PASS >> Navigated to 'Change Your Account Password' page";
		String FailStatement="FAIL >> Navigation to Change Your Account Password failed";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
		
	}
	
	public void ErrorMessageOldPaswordMtd(){
		String Actualvalue=ErrorMessageOldPassword.getText();
		String Expectedvalue="Old Password";
		String PassStatement="PASS >> "+Expectedvalue+ "is mandatory field. Highlighted with Incorrect Old Password";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Old Password";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void ErrorMessageOldPaswordIncorrectMtd(){
		String Actualvalue=ErrorMessageOldPasswordIncorrect.getText();
		String Expectedvalue="Old Password";
		String PassStatement="PASS >> "+Expectedvalue+ " is mandatory field. Highlighted with Incorrect Old Password";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Old Password";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void ErrorMessageNewPaswordMtd(){
		String Actualvalue=ErrorMessageNewPassword.getText();
		String Expectedvalue="New Password";
		String PassStatement="PASS >> "+Expectedvalue+ " is mandatory field. Highlighted with Password too short. Minimum 4 characters";
		String FailStatement="FAIL >> Mandatory field is not highlighted : New Password";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void ErrorMessageRetypeNewPaswordMtd(){
		String Actualvalue=ErrorMessageRetypeNewPassword.getText();
		String Expectedvalue="Retype New Password";
		String PassStatement="PASS >> "+Expectedvalue+ " is mandatory field. Highlighted with Password do not match";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Retype New Password";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	
	}
	
	public void ChangePwdCloseMtd() throws InterruptedException{
		ChngePasswordCloseBtn.click();
		waitForidPresent(IdforConsolePageToLoad);
	    sleep(5);
	}
	
	
	public void ClickOnChangePaswordOkbtn(){
		ChangePasswordokbtn.click();
	
	}
	
	public void EnterDataOldPassword(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException{
		OldPasswordtextbox.sendKeys(ELib.getDatafromExcel("Sheet10", row, col));
	}
	
	public void EnterDataNewPassword(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException{
		NewPasswordtextbox.sendKeys(ELib.getDatafromExcel("Sheet10", row, col));
	}
	
	public void EnterDataRetypeNewPassword(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException{
		RetypeNewPasswordtextbox.sendKeys(ELib.getDatafromExcel("Sheet10", row, col));
	}
	
	public void TestFailureToRecover() throws InterruptedException{
		try {
			Initialization.commonmethdpage.ClickOnSettings();
		} catch (Exception e) {
			refesh();
			waitForidPresent(IdforConsolePageToLoad);
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();
		}
		sleep(2);
	}
	
	public void ValuationOfLoginTrueMtd(boolean value) throws InterruptedException{
		  String PassStatement="PASS >> Password is not changed after Cancellation";
		  String FailStatement="Fail >> Password is changed after Cancellation";
		sleep(2);
		try {
			LoginErrorMessage.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	
	public void ValuationOfLoginFalseMtd(boolean value) throws InterruptedException{
		String PassStatement = "PASS >> New Password is changed successfully";
		String FailStatement = "Fail >> New Password is not applied";
		sleep(2);
		try {
			LoginErrorMessage.isDisplayed();
			value = false;
		} catch (Exception e) {
			value = true;
		}
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void ConfirmationMessageVerify(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Password reset successful.' Message is displayed successfully";
		String FailStatement = "Fail >> 'Password reset successful.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessage, value, PassStatement, FailStatement,3);
	}
}
