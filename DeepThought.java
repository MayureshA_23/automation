package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class DeepThought extends Initialization{

	@Test(priority='1',description="Verify working of Deep Thought and commands in device side.") 
	public void VerifyworkingofDeepThoughtandcommandsindevicesideTC_ST_386() throws Throwable {
		Reporter.log("\n1.Verify working of Deep Thought and commands in device side.",true);
		
		accountsettingspage.ClickonMoreOption();
		accountsettingspage.ClickonDeepThought();
		accountsettingspage.ClickonShowmeMyAccountDetails();
		accountsettingspage.ClickonShowmeAllOnlineDevices();
		accountsettingspage.lockitOptionVisible();
		accountsettingspage.wipeitOptionVisible();
		accountsettingspage.rebootitOptionVisible();
		accountsettingspage.refreshitOptionVisible();
		accountsettingspage.sendmessageOptionVisible();
}
	
	
	@Test(priority='2',description="Verify Disable DeepThought option for BYOD Users.") 
	public void VerifyworkingofDeepThoughtandcommandsindevicesideTC_ST_389() throws Throwable {
		Reporter.log("\n2.Verify Disable DeepThought option for BYOD Users.",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonDeepThoughtSettings();
		accountsettingspage.EnableDeepThoughtforBYODUsers();
		accountsettingspage.EnableDeepThoughtSettingsSuccessfully(); 
		
}
	
	
}
