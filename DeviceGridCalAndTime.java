package DeviceGrid_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import Library.Config;

import Common.Initialization;

public class DeviceGridCalAndTime extends Initialization {
	@Test(priority = 0, description = "Validation of Last Connected column in Device Grid.")
	public void VerifySortingOfLastConnectedColumnAndToday() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("1.Validation of Last Connected column in Device Grid...", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Last Connected");
		deviceGrid.Enable_DisbleColumn("Last Connected", "Enable");
		deviceGrid.VerifyDecendingDateTimeSorting("Last Connected");
		deviceGrid.VerifyAscendingDateTimeSorting("Last Connected");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingDateTime("2", "Last Connected");
		deviceGrid.selectPageAndVerifySortingDateTime("1", "Last Connected");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedDateTime("Last Connected");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("LastTimeStamp", "Last Connected");
		deviceGrid.SelectDateFromCal("Today");
		deviceGrid.verifySelectedTimeForToday("Last Connected");

		Reporter.log("Pass >>Validation of Last Connected column in Device Grid.", true);
	}

	@Test(priority = 1, description = "Validation of Last Connected column in Device Grid.")
	public void VerifySortingOfLastConnectedYesterday() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n2.Validation of Last Connected column in Device Grid.",true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("LastTimeStamp", "Last Connected");
		deviceGrid.SelectDateFromCal("Yesterday");
		deviceGrid.verifySelectedTimeForYesterday("Last Connected");
		Reporter.log("Pass >>Validation of Last Connected column in Device Grid.", true);
	}

	@Test(priority = 2, description = "Validation of Last Connected column in Device Grid.")
	public void VerifySortingOfLastConnectedLastWeek() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n3.Validation of Last Connected column in Device Grid.",true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("LastTimeStamp", "Last Connected");
		deviceGrid.SelectDateFromCal("Last 1 Week");
		deviceGrid.verifySelectedTimeForLastWeek("Last Connected");
		Reporter.log("Pass >>Validation of Last Connected column in Device Grid.", true);
	}

	@Test(priority = 3, description = "Validation of Last Connected column in Device Grid.")
	public void VerifySortingOfLastConnected30Days() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n4.Validation of Last Connected column in Device Grid.",true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("LastTimeStamp", "Last Connected");
		deviceGrid.SelectDateFromCal("Last 30 Days");
		deviceGrid.verifySelectedTimeForLast30Days("Last Connected");
		Reporter.log("Pass >>Validation of Last Connected column in Device Grid.", true);
	}

	@Test(priority = 4, description = "Validation of Last Connected column in Device Grid.")
	public void VerifySortingOfLastConnectedThisMonth() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n5.Validation of Last Connected column in Device Grid.");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("LastTimeStamp", "Last Connected");
		deviceGrid.SelectDateFromCal("This Month");
		deviceGrid.verifySelectedTimeForThisMonth("Last Connected");
		Reporter.log("Pass >>Validation of Last Connected column in Device Grid.", true);
	}

	@Test(priority = 5, description = "Validation of Last Device Time column in Device Grid.")
	public void VerifySortingOfLastDeviceTimeColumnAndToday() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n6.Validation of Last Device Time column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Last Device Time");
		deviceGrid.Enable_DisbleColumn("Last Device Time", "Enable");
		deviceGrid.VerifyDecendingDateTimeSorting("Last Device Time");
		deviceGrid.VerifyAscendingDateTimeSorting("Last Device Time");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingDateTime("2", "Last Device Time");
		deviceGrid.selectPageAndVerifySortingDateTime("1", "Last Device Time");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedDateTime("Last Device Time");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceTimeStamp", "Last Device Time");
		deviceGrid.SelectDateFromCal("Today");
		deviceGrid.verifySelectedTimeForToday("Last Device Time");

		Reporter.log("Pass >>Validation of Last Device Time column in Device Grid.", true);
	}

	@Test(priority = 6, description = "Validation of Last Device Time column in Device Grid.")
	public void VerifySortingOfLastDeviceTimeYesterday() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n7.Validation of Last Device Time column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceTimeStamp", "Last Device Time");
		deviceGrid.SelectDateFromCal("Yesterday");
		deviceGrid.verifySelectedTimeForYesterday("Last Device Time");
		Reporter.log("Pass >>Validation of Last Device Time column in Device Grid.", true);
	}

	@Test(priority = 7, description = "Validation of Last Device Time column in Device Grid.")
	public void VerifySortingOfLastDeviceTimeLastWeek() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n8.Validation of Last Device Time column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceTimeStamp", "Last Device Time");
		deviceGrid.SelectDateFromCal("Last 1 Week");
		deviceGrid.verifySelectedTimeForLastWeek("Last Device Time");
		Reporter.log("Pass >>Validation of Last Device Time column in Device Grid.", true);
	}

	@Test(priority = 8, description = "Validation of Last Device Time column in Device Grid.")
	public void VerifySortingOfLastDeviceTime30Days() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n9.Validation of Last Device Time column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceTimeStamp", "Last Device Time");
		deviceGrid.SelectDateFromCal("Last 30 Days");
		deviceGrid.verifySelectedTimeForLast30Days("Last Device Time");
		Reporter.log("Pass >>Validation of Last Device Time column in Device Grid.", true);
	}

	@Test(priority = 9, description = "Validation of Last Device Time column in Device Grid.")
	public void VerifySortingOfLastDeviceTimeThisMonth() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n10.Validation of Last Device Time column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceTimeStamp", "Last Device Time");
		deviceGrid.SelectDateFromCal("This Month");
		deviceGrid.verifySelectedTimeForThisMonth("Last Device Time");
		Reporter.log("Pass >>Validation of Last Device Time column in Device Grid.", true);
	}

	@Test(priority = 'A', description = "Validation of Device Registered column in Device Grid.")
	public void VerifySortingOfDeviceRegisteredColumnAndToday() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n11.Validation of Device Registered column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device Registered");
		deviceGrid.Enable_DisbleColumn("Device Registered", "Enable");
		deviceGrid.VerifyDecendingDateTimeSorting("Device Registered");
		deviceGrid.VerifyAscendingDateTimeSorting("Device Registered");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingDateTime("2", "Device Registered");
		deviceGrid.selectPageAndVerifySortingDateTime("1", "Device Registered");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedDateTime("Device Registered");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceRegistered", "Device Registered");
		deviceGrid.SelectDateFromCal("Today");
		deviceGrid.verifySelectedTimeForToday("Device Registered");

		Reporter.log("Pass >>Validation of Device Registered column in Device Grid.", true);
	}

	@Test(priority = 'B', description = "Validation of Device Registered column in Device Grid.")
	public void VerifySortingOfDeviceRegisteredYesterday() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n12.Validation of Device Registered column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceRegistered", "Device Registered");
		deviceGrid.SelectDateFromCal("Yesterday");
		deviceGrid.verifySelectedTimeForYesterday("Device Registered");
		Reporter.log("Pass >>Validation of Device Registered column in Device Grid.", true);
	}

	@Test(priority = 'C', description = "Validation of Device Registered column in Device Grid.")
	public void VerifySortingOfDeviceRegisteredLastWeek() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n13.Validation of Device Registered column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceRegistered", "Device Registered");
		deviceGrid.SelectDateFromCal("Last 1 Week");
		deviceGrid.verifySelectedTimeForLastWeek("Device Registered");
		Reporter.log("Pass >>Validation of Device Registered column in Device Grid.", true);
	}

	@Test(priority = 'D', description = "Validation of Last Device Time column in Device Grid.")
	public void VerifySortingOfDeviceReigstered30Days() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n14.Validation of Last Device Time column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceRegistered", "Device Registered");
		deviceGrid.SelectDateFromCal("Last 30 Days");
		deviceGrid.verifySelectedTimeForLast30Days("Device Registered");
		Reporter.log("Pass >>Validation of Last Device Time column in Device Grid.", true);
	}

	@Test(priority = 'E', description = "Validation of Last Device Time column in Device Grid.")
	public void VerifySortingOfDeviceReigsteredThisMonth() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n15.Validation of Last Device Time column in Device Grid.", true);
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.ClickOnAdvanceSeach("DeviceRegistered", "Device Registered");
		deviceGrid.SelectDateFromCal("This Month");
		deviceGrid.verifySelectedTimeForThisMonth("Device Registered");
		Reporter.log("Pass >>Validation of Last Device Time column in Device Grid.", true);
	}

	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException {
		
		/*commonmethdpage.HardWait(2);
		deviceGrid.RefreshGrid();
		commonmethdpage.HardWait(2);
		deviceGrid.RefreshDeviceGrid();*/
				if(ITestResult.FAILURE==result.getStatus())
				{
					deviceGrid.RefreshGrid();
				}
			}
}
