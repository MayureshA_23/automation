package SanityTestScripts;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class Inbox_TestScripts extends Initialization{
	@Test(priority=1,description="Inbox-Verify delete message by confirming yes")
	public void DeleteMessage() throws InterruptedException, IOException
	{
		Reporter.log("=====1.Inbox-Verify delete message by confirming yes=====");
		commonmethdpage.ClickOnHomePage();
		inboxpage.ClickOnInbox();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		commonmethdpage.enterSubInComposeMessage(1,"DeleteMail", "test");
		inboxpage.SearchMailInInbox("DeleteMail");
		inboxpage.ClickOnDeleteButton();
		inboxpage.ClickOnYesDeleteMessage();	
		inboxpage.SearchMailInInbox("DeleteMail");
		inboxpage.VerifyOnDeletedMail("DeleteMail");
		//inboxpage.ClickOnMailDeleteButton_UM(); 
	}
	@Test(priority=0,description="Validate mail read/unread mails in Inbox by sending/receiving messages")
	public void VerifyOfReadAndUnreadMails() throws InterruptedException, IOException
	{
		Reporter.log("\n=====2.Validate mail read/unread mails in Inbox by sending/receiving messages=====",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		commonmethdpage.enterSubInComposeMessage(6,"suremdm", "test");
		inboxpage.ClickOnInbox();
		inboxpage.isTheFirstMessageSelectedByDefault();
		inboxpage.ClickOnMailReplyButton();
		inboxpage.EnterMessageBody("Hi");
		inboxpage.GetReadNotificaiton();
		inboxpage.ClickOnSendButton();
		inboxpage.NotificationMessageSent();
		inboxpage.ClickOnMarkAsUnread();
		inboxpage.WarningPopUpWhenMarkAsUnread();
		inboxpage.Yes_MarkAsUnread();
		inboxpage.ConfirmationNotificationOnMarkAsUnread();
		
	}
}
