package Settings_TestScripts;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

public class ManageSubscription extends Initialization{

	
	//set precondition as
	//String url = "https://worldoftesting.eu.suremdm.io/console/"; String userID ="abdulemithilesh@gmail.com"; String password = "Mithilesh@123";
	@Test(priority=1 ,description="Verify the Suscription plan details for intal AMT ") 
	public void VerifytheSuscriptionplandetailsforintalAMTTC_ST_656() throws Throwable {
		Reporter.log("\n1.Verify the Suscription plan details for intal AMT ",true);
		
		accountsettingspage.Closewindow();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.BuySubscription();
		accountsettingspage.IntelActiveManagementTechnologyVisible();
		accountsettingspage.CloseSubscriptionWindow();				
}
	@Test(priority=2 ,description="Verify the Suscription plan details for intal AMT Should be shown Cross mark for standard") 
	public void VerifytheSuscriptionplandetailsforintalAMTShouldbeshownCrossmarkforstandardTC_ST_657() throws Throwable {
		Reporter.log("\n1.Verify the Suscription plan details for intal AMT Should be shown Cross mark for standard",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.BuySubscription();		
		accountsettingspage.IntelActiveManagementTechnologyVisible();
		accountsettingspage.IntelAMTForStandardVisible();
		accountsettingspage.CloseSubscriptionWindow();				
}
	@Test(priority=3 ,description="Verify the Suscription plan details for intal AMT Should be shown Cross mark for Premium") 
	public void VerifytheSuscriptionplandetailsforintalAMTShouldbeshownCrossmarkforPremiumTC_ST_657() throws Throwable {
		Reporter.log("\n3.Verify the Suscription plan details for intal AMT Should be shown Cross mark for Premium",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.BuySubscription();		
		accountsettingspage.IntelActiveManagementTechnologyVisible();
		accountsettingspage.IntelAMTForPremiumVisible();
		accountsettingspage.CloseSubscriptionWindow();				
}
	
	@Test(priority=4 ,description="Verify the Suscription plan details for intal AMT Should be shown Cross mark for Enterprise") 
	public void VerifytheSuscriptionplandetailsforintalAMTShouldbeshownCrossmarkforEnterpriseTC_ST_657() throws Throwable {
		Reporter.log("\n4.Verify the Suscription plan details for intal AMT Should be shown Cross mark for Enterprise",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.BuySubscription();		
		accountsettingspage.IntelActiveManagementTechnologyVisible();
		accountsettingspage.IntelAMTForEnterpriseVisible();
		accountsettingspage.CloseSubscriptionWindow();				
}
	
	@Test(priority=5 ,description="Verify VR license used field is displaying in Settings dropdown.") 
	public void VerifyVRlicenseusedfieldisdisplayinginSettingsdropdownTC_ST_644() throws Throwable {
		Reporter.log("\n5.Verify VR license used field is displaying in Settings dropdown.",true);
		
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.VRLicensesUsed();
		commonmethdpage.ClickOnSettings();
}
	
	@Test(priority=6 ,description="Verify Things devices used used field is displaying in Settings dropdown.") 
	public void VerifyThingsdevicesisdisplayinginSettingsdropdownTC_ST_645() throws Throwable {
		Reporter.log("\n6.Verify Things devices field is displaying in Settings dropdown.",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.Thingsdevicesused();
		commonmethdpage.ClickOnSettings();
}
	
	
	
	
	
	
	
	
	
}
