package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;

public class EnterpriseIntegrations extends Initialization{
	
	
	@Test(priority=1,description="Intel� AMT Device Management from account settings") 
	public void IntelAMTDeviceManagementfromaccountsettingsTC_ST_625() throws Throwable {
		Reporter.log("\n1.Intel� AMT Device Management from account settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonEnterpriseIntegrations();
		accountsettingspage.IntelAMTdevicemanagementVisible();	
}
	

	@Test(priority=2,description="Check for help text for splunk host url") 
	public void CheckforhelptextforsplunkhosturlTC_ST_781() throws Throwable {
		Reporter.log("\n2.Check for help text for splunk host url",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonEnterpriseIntegrations();
		accountsettingspage.HostUrlExampleVisible();		
}
	
	@Test(priority=3,description="Cisco ISE must be integrated in Account settings ") 
	public void CiscoISEmustbeintegratedinAccountsettingsTC_ST_784() throws Throwable {
		Reporter.log("\n3.Cisco ISE must be integrated in Account settings ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonEnterpriseIntegrations();
		accountsettingspage.CiscoISEIntegrationVisible();				
}
	
	@Test(priority=4,description="Enable Cisco ISE and enter username and password") 
	public void EnableCiscoISEandenterusernameandpasswordTC_ST_785() throws Throwable {
		Reporter.log("\n4.Enable Cisco ISE and enter username and password",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonEnterpriseIntegrations();
		accountsettingspage.CiscoISEIntegrationVisible();
		accountsettingspage.EnableCiscoISE();
		accountsettingspage.CiscoUserName();
		accountsettingspage.CiscoPassword();
		accountsettingspage.ClickonCiscoSaveButton();
		accountsettingspage.CiscoISEIntegrationSavedmsg();		
}
	
	@Test(priority=5,description="Verfy the error message when UID and Password is not entered in MDM ") 
	public void VerfytheerrormessagewhenUIDandPasswordisnotenteredinMDMTC_ST_787() throws Throwable {
		Reporter.log("\n5.Verfy the error message when UID and Password is not entered in MDM ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonEnterpriseIntegrations();
		accountsettingspage.CiscoISEIntegrationVisible();
		accountsettingspage.EnableCiscoISE();
		accountsettingspage.CiscoUserNameClear();
		accountsettingspage.CiscoPasswordClear();
		accountsettingspage.ClickonCiscoSaveButton();
		accountsettingspage.CiscoISEIntegrationErrorMsg();	
}
	
	@Test(priority=6,description="Verify NAC integrations on Enterprise account") 
	public void VerifyNACintegrationsonEnterpriseaccountTC_ST_804() throws Throwable {
		Reporter.log("\n6.Verify NAC integrations on Enterprise account",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonEnterpriseIntegrations();
		accountsettingspage.NACIntegrationVisible();		
}
	
	@Test(priority=7,description="Verify the Enterprise Integration") 
	public void VerifytheEnterpriseIntegrationTC_ST_805() throws Throwable {
		Reporter.log("\n7.Verify the Enterprise Integration",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonEnterpriseIntegrations();
		accountsettingspage.NACIntegrationVisible();
		accountsettingspage.EnableNACIntegration();
}
	
	//vinimanoj
	
	@Test(priority=8,description="Verify if the NAC template is downloaded or not TC_ST_806")
   	public void VerifyiftheNACtemplateisdownloadedornotTC_ST_806() throws InterruptedException
   	{
   		Reporter.log("\n8.Verify if the NAC template is downloaded or not TC_ST_806",true);
   		commonmethdpage.ClickOnSettings();
   		commonmethdpage.ClickonAccsettings();
   		accountsettingspage.EnterpriseIntegrations();
   		accountsettingspage.NACIntegration();
   		accountsettingspage.NACIntegrationDownloadTemplate();
   		accountsettingspage.clickHome();
   	}
	
	@Test(priority=9,description="Verify if the NAC template is been imported or not TC_ST_807")
   	public void VerifyiftheNACtemplateisbeenimportedornotTC_ST_807() throws InterruptedException, Exception {
   		Reporter.log("\n9.Verify if the NAC template is been imported or not TC_ST_807",true);
   		commonmethdpage.ClickOnSettings();
   		commonmethdpage.ClickonAccsettings();
   		accountsettingspage.EnterpriseIntegrations();
   		accountsettingspage.NACIntegration();
   		accountsettingspage.NACIntegrationImpport();
   		accountsettingspage.browserIntegrationImport("./Uploads/UplaodFiles/NAC Integration/\\NAC.exe"); 
   		accountsettingspage.clickHome();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
