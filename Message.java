package DynamicJobs_TestScripts;


import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class Message extends Initialization{

	@Test(priority='1',description="1.To Verify Dynamic Send Message Job Window's UI")
	public void DynamicMessageJob_TC_DJ_35() throws InterruptedException, Throwable{
	Reporter.log("\n1.To Verify Dynamic Send Message Job Window's UI",true);
	
	commonmethdpage.ClickOnAllDevicesButton();
	commonmethdpage.SearchDevice(Config.DeviceName);
	 dynamicjobspage.ClickOnMoreOption();
     dynamicjobspage.ClickOnMessage();
     dynamicjobspage.VerifyOfSendMessageWindow();
     dynamicjobspage.ClickOnMessageCloseBtn();
	Reporter.log("PASS>> Verification of Dynamic Send Message Job Window's UI",true);
	}
	
	@Test(priority='2',description="2.To Verify Dynamic Send Message Job when Subject,Message fields are empty")
	public void DynamicMessageJob_TC_DJ_36() throws InterruptedException, Throwable{
		Reporter.log("\n2.To Verify Dynamic Send Message Job when Subject,Message fields are empty",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ErrorMessageBodyVerify();
	    dynamicjobspage.ClickOnMessageCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Send Message Job when Subject,Message fields are empty",true);
	}
	
	@Test(priority='3',description="3.To Verify Dynamic Send Message Job when Message field is empty")
	public void DynamicMessageJob_TC_DJ_37() throws InterruptedException, Throwable{
		Reporter.log("\n3.To Verify Dynamic Send Message Job when Message field is empty",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.EnterMessageSubject();
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ErrorMessageBodyVerify();
	    dynamicjobspage.ClickOnMessageCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Send Message Job when Message field is empty",true);
	}
	
	@Test(priority='4',description="4.To Verify Dynamic Send Message Job when Subject field is empty")
	public void DynamicMessageJob_TC_04() throws InterruptedException, Throwable{
		Reporter.log("\n4.To Verify Dynamic Send Message Job when Message field is empty",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.EnterMessageBody();
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ErrorMessageSubjectVerify();
	    dynamicjobspage.ClickOnMessageCloseBtn();
		Reporter.log("PASS>> Verification of Dynamic Send Message Job when Message fields is empty",true);
	}
	
	@Test(priority='5',description="5.To Verify Dynamic Send Message Job with subject,body entered valid data")
	public void DynamicMessageJob_TC_DJ_39() throws InterruptedException, Throwable{
		Reporter.log("\n5.To Verify Dynamic Send Message Job with subject,body entered valid data",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.EnterSubjectSMS("Dynamic");
	    dynamicjobspage.EnterBodySMS("meetat6pm");
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ConfirmationMessageVerify();
	    dynamicjobspage.VerifyOfMessageActivityLogs(Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
	    dynamicjobspage.clickOnTextMessageInMailBox();
		
		Reporter.log("PASS>> Verification of Dynamic Send Message Job with subject,body entered valid data",true);
	}
	
	@Test(priority='6',description="6.To Verify Dynamic Send Message Job with subject,body entered valid data and Get Read Notification Enabled")
	public void DynamicMessageJob_TC_DJ_38() throws InterruptedException, Throwable{
		Reporter.log("\n6.To Verify Dynamic Send Message Job with subject,body entered valid data and Get Read Notification Enabled",true);
		
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);

		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.EnterSubjectSMS("DynamicRN");
	    dynamicjobspage.EnterBodySMS("Wssup??");
	    dynamicjobspage.ClickOnGetReadNotification();
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ConfirmationMessageVerify();
	    dynamicjobspage.VerifyOfMessageActivityLogs(Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClickOnMailBoxOfNix();
		dynamicjobspage.clickOnTextMessageInMailBoxReadNotify();
		Reporter.log("PASS>> Verification of Dynamic Send Message Job with subject,body entered valid data and Get Read Notification Enabled",true);
	}
	
	@Test(priority='7',description="7.To Verify Dynamic Send Message Job with subject,body entered valid data and Force Read Message Enabled")
	public void DynamicMessageJob_TC_DJ_40() throws InterruptedException, Throwable{
		Reporter.log("\n7.To Verify Dynamic Send Message Job with subject,body entered valid data and Force Read Message Enabled",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);

		dynamicjobspage.ClickOnMoreOption();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
	    dynamicjobspage.ClickOnMessage();
	    dynamicjobspage.EnterSubjectSMS("DynamicFR");
	    dynamicjobspage.EnterBodySMS("Have a good day");
	    dynamicjobspage.ClickOnForceReadMessage();
	    dynamicjobspage.ClickOnSend();
	    dynamicjobspage.ConfirmationMessageVerify();
	    dynamicjobspage.VerifyOfMessageActivityLogs(Config.DeviceName);
	    dynamicjobspage.SMSForceReadMessage();
	    androidJOB.ClickOnHomeButtonDeviceSide();
		Reporter.log("PASS>> Verification of Dynamic Send Message Job with subject,body entered valid data and Force Read Message Enabled",true);
	}
	
	// DONT Run
	
/*	@Test(priority='8',description="7.To Verify Dynamic Send Message Job when mailbox is disabled")
	public void DynamicMessageJobDisabledMailbox_TC_DJ_40() throws InterruptedException, Throwable{
		Reporter.log("\n7.To Verify error message when mailbox is disabled in nix agent settings ",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		 dynamicjobspage.disableMailBoxDeviceSide();
		 dynamicjobspage.ClickOnMoreOption();
		 dynamicjobspage.ClickOnMessage();
		 dynamicjobspage.EnterSubjectSMS("error");
		 dynamicjobspage.EnterBodySMS("this message should not be sent on device");
		 dynamicjobspage.ClickOnSend();
		 Thread.sleep(3000);
		 dynamicjobspage.ErrorMessageWhenMailBoxIsDisabled();
		 
		Reporter.log("PASS>> Disabled mailbox in nix settings and error message captured 'Error delivering message Reason: Mailbox disabled on device'",true);
	}
*/	
	/*@AfterMethod
	public void aftermethod(ITestResult result) throws InterruptedException, Throwable
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			Utility.captureScreenshot(driver, result.getName());
			commonmethdpage.refreshpage();
			commonmethdpage.SearchDevice();
		}
	}*/
}
