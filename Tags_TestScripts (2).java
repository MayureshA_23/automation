package SanityTestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Tags_TestScripts extends Initialization {
	@Test(priority = 0, description = "Verify deleting the tag")
	public void DeleteTag() throws InterruptedException {
		Reporter.log("=====1.Verify deleting the tag=====", true);
		Tags.ClickOnTags1();
		Tags.CreatTag("#Test5");
		Tags.DeleteTag("#Test5");
	}

	@Test(priority = 1, description = "Verify creation of new tags and move devices to tags")
	public void CreateTag()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n=====2.Verify creation of new tags and move devices to tags=====", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName("NixSingleFileTransfer");
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe");
		androidJOB.EnterDevicePath("/sdcard/Download");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
		
		commonmethdpage.ClickOnHomePage();
		Tags.ClickOnTags1();
		Tags.CreatTag("@Automaton");
		Tags.ClickOnGroup();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.SearchTag("@Automaton");
		Tags.EnableTagCheckbox(" @Automaton");
		Tags.ClickOnSaveTagButton();
		Tags.SuccessfulMessageOnTagMove();
		Tags.ClickOnTags1();
		Tags.ApplyJobOnTag("@Automaton");
		androidJOB.SearchField("NixSingleFileTransfer");
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();	
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("FileTransfer.jpg",Config.DeviceName);	
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("NixSingleFileTransfer",200);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();
		
	}

}
