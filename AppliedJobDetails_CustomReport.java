package CustomReportsScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class AppliedJobDetails_CustomReport extends Initialization
{@Test(priority='0',description="Applying Job") 
public void ApplyingJobs() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("TextMessage", "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDeviceInconsole();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.Job_Name);
		commonmethdpage.Selectjob(Config.Job_Name);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
	}
@Test(priority='1',description="Verify generating Applied Job Details Custom report without filters") 
public void VerifyOfAppliedJobDetailsReport_TC_RE_128() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify generating Applied Job Details Custom report without filters",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectAppliedJobDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppliedJobDetails);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Applied Job Details CRep without filters","Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Applied Job Details CRep without filters");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Applied Job Details CRep without filters");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Applied Job Details CRep without filters");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Applied Job Details CRep without filters");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.SearchBoxInsideViewRep(Config.Job_Name);
		customReports.VarifyingAppliedJob();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='1',description="Verify generating Applied Job Detail Custom report with filters") 
public void VerifactionOfAppliedJobDetailsReport_TC_RE_129() throws InterruptedException
	{Reporter.log("\nVerify generating Applied Job Detail Custom report with filters",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectAppliedJobDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppliedJobDetails);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Applied Job Details CRep with filter as equal operator","Test");
		customReports.Selectvaluefromdropdown(Config.SelectAppliedJobDetails);
		customReports.SelectValueFromColumn(Config.SelectJobName);
		customReports.SendValueToTextfield(Config.EnterJobNameAsTextMessage);		
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Applied Job Details CRep with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("Applied Job Details CRep with filter as equal operator");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Applied Job Details CRep with filter as equal operator");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Applied Job Details CRep with filter as equal operator");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.VarifyingAppliedJob();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
@Test(priority='2',description="Verify generating Applied Job Detail Custom report with filters- The report should contain the Deployed job details only") 
public void VerifyAppliedJobDetailsForColumn__TC_RE_130() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerification of The report should contain the Deployed job details only",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectAppliedJobDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppliedJobDetails);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("AppJobDet CustRep with filters as equal OP for Status Col","Test");
		customReports.Selectvaluefromdropdown(Config.SelectAppliedJobDetails);
		customReports.SelectValueFromColumn(Config.SelectStatus);
		customReports.SendValueToTextfield(Config.EnterJobStatusAsDeployed);	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppJobDet CustRep with filters as equal OP for Status Col");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("AppJobDet CustRep with filters as equal OP for Status Col");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppJobDet CustRep with filters as equal OP for Status Col");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppJobDet CustRep with filters as equal OP for Status Col");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.VarifyingAppliedJobStatus();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='3',description="Verify generating Applied Job Detail Custom report with filters") 
public void VerifactionOfAppliedJobDetailsForColumnNameAsJobNameForSubGroup() throws InterruptedException
	{Reporter.log("\nVerify generating Applied Job Detail Custom report with filters",true);	
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectAppliedJobDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppliedJobDetails);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("AppJobDet CustRep with filters as equal OP for Job Name Col","Test");
		customReports.Selectvaluefromdropdown(Config.SelectAppliedJobDetails);
		customReports.SelectValueFromColumn(Config.SelectJobName);
		customReports.SendValueToTextfield(Config.EnterJobNameAsTextMessage);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppJobDet CustRep with filters as equal OP for Job Name Col");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("AppJobDet CustRep with filters as equal OP for Job Name Col");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppJobDet CustRep with filters as equal OP for Job Name Col");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppJobDet CustRep with filters as equal OP for Job Name Col");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.VarifyingAppliedJob();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();		
	}
@Test(priority='4',description="Verify Sort by and Group by for Applied Job Detail") 
public void VerifactionOfColsOfAppJobDetRep_TC_RE_132() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for Applied Job Detail",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectAppliedJobDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppliedJobDetails);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("AppJobDet CustRep for SortBy And GroupBy","Test");
		customReports.Selectvaluefromdropdown(Config.SelectAppliedJobDetails);	
		customReports.Selectvaluefromsortbydropdown(Config.SortBy_DeviceName);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName(Config.EnterAliasNameAsMyDevice);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppJobDet CustRep for SortBy And GroupBy");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppJobDet CustRep for SortBy And GroupBy");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppJobDet CustRep for SortBy And GroupBy");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}

}
