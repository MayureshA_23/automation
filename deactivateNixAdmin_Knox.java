package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class deactivateNixAdmin_Knox extends Initialization {
	@Test(priority=1, description="create a runscript to deactivate Nix admin Rrunscript")
		public void ToDeactivateNixAdminOnDevice() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

			androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			runScript.ClickOnRunscript();
			runScript.clickOnKnoxSection();
			runScript.ClickOnDeactivateDeviceAdmin();
			runScript.ClickOnValidateButton();
			runScript.ScriptValidatedMessage();
			runScript.ClickOnInsertButton();
			runScript.RunScriptName("DeactivateAdmin");
			commonmethdpage.ClickOnHomePage();
			androidJOB.SearchDeviceInconsole(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("DeactivateAdmin");
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
			androidJOB.CheckStatusOfappliedInstalledJob("DeactivateAdmin",60);
			runScript.ClickOnSettingsDeviceSide();
			runScript.CheckAdminDeactivatedOnDevice();
		}
}
