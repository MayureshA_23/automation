package CustomReportsScripts;

import java.io.IOException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;

//Pass IP address of any device that should be there in device grid.
//Pass LocalIP address of any device that should be there in device grid.
//Pass Model name of any device ------------''----------------------
//pass MAC Address of device and that should be there in device grid.
//pass AgentVersion Address of device and that should be there in device grid.
//pass IMEI of device and that should be there in device grid.
//pass SureLock Version of device and that should be there in device grid.
//pass SureFox Version of device and that should be there in device grid.
//pass SureVideo Version of device and that should be there in device grid.
//pass Default Launcher of device and that should be there in device grid.
//pass Group Path of any device and that should be there in device grid.
//pass Tag of any device and that should be there in device grid.DeviceUser_Name
//pass DeviceUser_Name of any device and that should be there in device grid.
//pass OSBuildNumber of any device and that should be there in device grid.
//pass AndroidID of any device and that should be there in device grid.
//pass HashCode of any device and that should be there in device grid.70659

public class DeviceDetails_Columns extends Initialization {
	@Test(priority=-200,description="Verify Data in Device IP Address column with filter as Equal operator")
	public void VerifyDataInDeviceIPAddressCol_EqualOP() throws InterruptedException
	{
		Reporter.log("==========1.Verify Data in Device IP Address column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceIPAddress col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Device IP Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device IP Address");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.IP_Address);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceIPAddress col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceIPAddress col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceIPAddressCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-199,description="Verify Data in Device IP Address column with filter as NotEqualTo operator")
	public void VerifyDataInDeviceIPAddressCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n============2.Verify Data in Device IP Address column with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceIPAddress col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Device IP Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device IP Address");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.IP_Address);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceIPAddress col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceIPAddress col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.IP_Address);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-198,description="Verify Data in Device IP Address column with filter as Like operator")
	public void VerifyDataInDeviceIPAddressCol_LikeOp() throws InterruptedException
	{
		Reporter.log("\n============3.Verify Data in Device IP Address column with filter as Like operator======",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceIPAddress col with filter as like Operator","test");
		customReports.ClickOnColumnInTable("Device IP Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device IP Address");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.IPAdd_Like);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceIPAddress col with filter as like Operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceIPAddress col with filter as like Operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceIPAddressCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-197,description="Verify Last Connected Time column with filter as Range")
	public void VerifyLastConnectedTimeColumn_RangeOP() throws InterruptedException
	{
		Reporter.log("\n======4.Verify Last Connected Time column with filter as Range====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDetail Report for LastConnectedTime col with filter as Range","test");
		customReports.ClickOnColumnInTable("Last Connected Time");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Last Connected Time");
		customReports.SelectTodayDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevDetail Report for LastConnectedTime col with filter as Range");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevDetail Report for LastConnectedTime col with filter as Range");
	    reportsPage.WindowHandle();
		customReports.VarifyingLastConnectedDate();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-196,description="Verify Device Local IP Address col with filter as equal operator")
	public void VerifyDeviceLocalIPAddressCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n======5.Verify Device Local IP Address col with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceLocalIPAdd col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Device Local IP Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Local IP Address");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.LocalIP_Address);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceLocalIPAdd col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceLocalIPAdd col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceLocalIPAddressCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		
	}
	@Test(priority=-195,description="Verify Device Local IP Address col with filter as NotEqualTo operator")
	public void VerifyDeviceLocalIPAddressCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n======6.Verify Device Local IP Address col with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceLocalIPAdd col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Device Local IP Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Local IP Address");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.LocalIP_Address);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceLocalIPAdd col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceLocalIPAdd col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.LocalIP_Address);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-194,description="Verify Device Local IP Address col with filter as Like operator")
	public void VerifyDeviceLocalIPAddressCol_LikeOp() throws InterruptedException
	{
		Reporter.log("\n=======7.Verify Device Local IP Address col with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceLocalIPAdd col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Device Local IP Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Local IP Address");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.LocalIP_Like);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceLocalIPAdd col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceLocalIPAdd col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceLocalIPAddressCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-193,description="Verify Notes column with filter as equal operator")
	public void VerifyNotesColumn_EqualOP() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n======8.Verify Notes column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice("moto g(7)");
		Thread.sleep(1000);
		deviceinfopanelpage.ClickOnEditdeviceNotes_UM(); 
		deviceinfopanelpage.TitleAddNotesWindow();
		deviceinfopanelpage.SelectAllNotesAndEnterNotes("AutomationDevice");
		deviceinfopanelpage.ClickOnSaveButtonAddNotes();
		commonmethdpage.ClickOnAllDevicesButton();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Notes col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Notes");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Notes");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("AutomationDevice");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Notes col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Notes col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingNotesCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-192,description="Verify Notes column with filter as NotEqualTo operator")
	public void VerifyNotesColumn_NotEqualToOP() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n====9.Verify Notes column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Notes col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Notes");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Notes");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("AutomationDevice");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Notes col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Notes col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("AutomationDevice");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority=-191,description="Verify Notes column with filter as Like operator")
	public void VerifyNotesColumn_LikeOp() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n====10.Verify Notes column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Notes col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Notes");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Notes");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Automation");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Notes col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Notes col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingNotesCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority=-190,description="Verify Platform column with filter as equal operator")
	public void VerifyPlatformColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n=======11.Verify Platform column with filter as equal operator=======",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Platform col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Platform");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Platform");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Android");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Platform col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Platform col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDevicePlatform__EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-189,description="Verify Platform column with filter as NotEqualTo operator")
	public void VerifyPlatformColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n======12.Verify Platform column with filter as NotEqualTo operator======",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Platform col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Platform");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Platform");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("iOS");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Platform col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Platform col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("ioS");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-188,description="Verify Platform column with filter as Like operator")
	public void VerifyPlatformColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n=======13.Verify Platform column with filter as Like operator==========",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Platform col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Platform");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Platform");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Win");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Platform col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Platform col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDevicePlatform__LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-187,description="Verify Model column with filter as equal operator")
	public void VerifyModelColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====14.Verify Model column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Model col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Model");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Model");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Model_Name);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Model col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Model col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceModel_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-186,description="Verify Model column with filter as NotEqualTo operator")
	public void VerifyModelColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====15.Verify Model column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Model col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Model");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Model");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Model_Name);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Model col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Model col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Model_Name);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-185,description="Verify Model column with filter as Like operator")
	public void VerifyModelColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====16.Verify Model column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Model col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Model");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Model");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.Model);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Model col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Model col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceModel_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-184,description="Verify DeviceRegistered column with filter as Range")
	public void VerifyDeviceRegisteredColumn_RangeOP() throws InterruptedException
	{
		Reporter.log("\n====17.Verify DeviceRegistered column with filter as Range=====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceRegistered col with filter as Range","test");
		customReports.ClickOnColumnInTable("Device Registered");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Registered");
		customReports.SelectTodayDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceRegistered col with filter as Range");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceRegistered col with filter as Range");
	    reportsPage.WindowHandle();
		customReports.VerifyingRegisteredDeviceCol();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-183,description="Verify BatteryPercent column with filter as equal operator")
	public void VerifyBatteryPercentColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n=====18.Verify BatteryPercent column with filter as equal operator======",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryPercent col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Battery Percent");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Percent");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("50");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryPercent col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryPercent col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyBatteryPercentageCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-182,description="Verify BatteryPercent column with filter as NotEqualTo operator")
	public void VerifyBatteryPercentColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n======19.Verify BatteryPercent column with filter as NotEqualTo operator======",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryPercent col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Battery Percent");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Percent");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryPercent col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryPercent col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("100");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-181,description="Verify BatteryPercent column with filter as less than or equal to operator")
	public void VerifyBatteryPercentColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n=====20.Verify BatteryPercent column with filter as less than or equal to operator=====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryPercent col with filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Battery Percent");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Percent");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryPercent col with filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryPercent col with filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyBatteryPercentageCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-180,description="Verify BatteryPercent column with filter as greater than or equal to opeartor")
	public void VerifyBatteryPercentColumn4() throws InterruptedException
	{
		Reporter.log("\n====21.Verify BatteryPercent column with filter as greater than or equal to opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryPercent col with filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Battery Percent");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Percent");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryPercent col with filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryPercent col with filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyBatteryPercentageCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-179,description="Verify BatteryState column with filter as equal operator")
	public void VerifyBatteryStateColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n=====22.Verify BatteryState column with filter as equal operator=====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryState col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Battery State");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery State");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Charging");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryState col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryState col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingBatteryState_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority=-178,description="Verify BatteryState column with filter as NotEqualTo operator")
	public void VerifyBatteryStateColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n=====23.Verify BatteryState column with filter as NotEqualTo operator=====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryState col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Battery State");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery State");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Charging");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryState col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryState col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Charging");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority=-177,description="Verify BatteryState column with filter as Like operator")
	public void VerifyBatteryStateColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n=====24.Verify BatteryState column with filter as Like operator=====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryState col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Battery State");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery State");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Nor");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryState col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryState col with filter as Like operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();   
	    customReports.VerifyingBatteryState__LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority=-176,description="Verify  BatteryChemistry column with filter as equal operator")
	public void VerifyBatteryChemistryColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====25.Verify  BatteryChemistry column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryChemistry col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Battery Chemistry");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Chemistry");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Li-ion");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryChemistry col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryChemistry col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingBatteryChemistryCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-175,description="Verify  BatteryChemistry column with filter as NotEqualTo operator")
	public void VerifyBatteryChemistryColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====26.Verify  BatteryChemistry column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryChemistry col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Battery Chemistry");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Chemistry");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Li-ion");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryChemistry col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryChemistry col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Li-ion");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-174,description="Verify  BatteryChemistry column with filter as Like operator")
	public void VerifyBatteryChemistryColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====27.Verify  BatteryChemistry column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BatteryChemistry col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Battery Chemistry");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Chemistry");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Li");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BatteryChemistry col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BatteryChemistry col with filter as Like operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Li-ion");
	    customReports.VerifyingBatteryChemistryCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	/*@Test(priority=-173,description="Verify  Total Physical Memory(MB) column with filter as equal operator")
	public void VerifyTotalPhysicalMemoryColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n=====28.Verify  Total Physical Memory(MB) column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalPhysicalMemory(MB)Col With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Total Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown("=");
		//customReports.SendValueToTextfield(Config.TotalPhysicalMemory);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalPhysicalMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Report For TotalPhysicalMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnViewReportLinkButton();
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyingTotalPhysicalMemoryReport_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}*/
	@Test(priority=-172,description="Verify  Total Physical Memory(MB)  column with filter as NotEqualTo operator")
	public void VerifyTotalPhysicalMemoryColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n=====29.Verify  Total Physical Memory(MB)  column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalPhysicalMemory(MB)Col With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Total Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("5000");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalPhysicalMemory(MB)Col With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For TotalPhysicalMemory(MB)Col With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("5000");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-170,description="Verify  Total Physical Memory(MB)  column with filter as less than or equal to operator")
	public void VerifyTotalPhysicalMemoryColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n=====30.Verify  Total Physical Memory(MB)  column with filter as less than or equal to operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalPhysicalMemory(MB)Col With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Total Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalPhysicalMemory(MB)Col With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For TotalPhysicalMemory(MB)Col With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-169,description="Verify  Total Physical Memory(MB) column with filter as greater than or equal to opeartor")
	public void VerifyTotalPhysicalMemoryColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n=====31.Verify  Total Physical Memory(MB) column with filter as greater than or equal to opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalPhysicalMemory(MB)Col With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Total Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalPhysicalMemory(MB)Col With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For TotalPhysicalMemory(MB)Col With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	/*@Test(priority=-168,description="Verify  Available Physical Memory(MB) column with filter as equal operator")
	public void VerifyAvailablePhysicalMemoryColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====32.Verify  Available Physical Memory(MB) column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailablePhysicalMemory(MB)Col With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Available Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown("=");
		//customReports.SendValueToTextfield(Config.AvailablePhysicalMemory);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailablePhysicalMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Report For AvailablePhysicalMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnViewReportLinkButton();
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingAvailablePhysicalMemoryReport_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}*/
	@Test(priority=-167,description="Verify  Available Physical Memory(MB) column with filter as NotEqualTo operator")
	public void VerifyAvailablePhysicalMemoryColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n=====33.Verify  Available Physical Memory(MB) column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailablePhysicalMemory(MB)Col With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Available Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("5000");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailablePhysicalMemory(MB)Col With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For AvailablePhysicalMemory(MB)Col With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("5000");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-166,description="Verify  Available Physical Memory(MB) column with filter as less than or equal to operator")
	public void VerifyAvailablePhysicalMemoryColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n=====34.Verify  Total Available Physical Memory(MB) column with filter as less than or equal to operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailablePhysicalMemory(MB)Col With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Available Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailablePhysicalMemory(MB)Col With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For AvailablePhysicalMemory(MB)Col With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-165,description="Verify  Available Physical Memory(MB)  column with filter as greater than or equal to opeartor")
	public void VerifyAvailablePhysicalMemoryColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n=====35.Verify  Available Physical Memory(MB) column with filter as greater than or equal to opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailablePhysicalMemory(MB)Col With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Available Physical Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Physical Memory (MB)");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailablePhysicalMemory(MB)Col With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For AvailablePhysicalMemory(MB)Col With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	/*@Test(priority=-164,description="Verify  Total Storage Memory(MB)  column with filter as equal operator")
	public void VerifyTotalStorageMemoryColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====36.Verify  Total Storage Memory(MB) column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalStorageMemory(MB)Col With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Total Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown("=");
		//customReports.SendValueToTextfield(Config.TotalStorageMemory);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalStorageMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Report For TotalStorageMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnViewReportLinkButton();
	    reportsPage.WindowHandle();
	   // customReports.ChooseDevicesPerPage();
	    customReports.VerifyingTotalStorageMemoryReport_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}*/
	@Test(priority=-163,description="Verify  Total Storage Memory(MB)  column with filter as NotEqualTo operator")
	public void VerifyTotalStorageMemoryColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====37.Verify  Total Storage Memory(MB) column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalStorageMemory(MB)Col With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Total Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("50000");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalStorageMemory(MB)Col With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For TotalStorageMemory(MB)Col With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	   // customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("50000");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-162,description="Verify  Total Storage Memory(MB)  column with filter as less than or equal to operator")
	public void VerifyTotalStorageMemoryColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====38.Verify  Total Storage Memory(MB) column with filter as less than or equal to operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalStorageMemory(MB)Col With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Total Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalStorageMemory(MB)Col With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For TotalStorageMemory(MB)Col With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-161,description="Verify  Total Storage Memory(MB)  column with filter as greater than or equal to opeartor")
	public void VerifyTotalStorageMemoryColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====39.Verify  Total Storage Memory(MB) column with filter as greater than or equal to opeartor===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For TotalStorageMemory(MB)Col With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Total Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Total Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For TotalStorageMemory(MB)Col With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For TotalStorageMemory(MB)Col With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	/*@Test(priority=-160,description="Verify  Available Storage Memory (MB) column with filter as equal operator")
	public void VerifyAvailableStorageMemoryColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====40.Verify  Available Storage Memory (MB) column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailableStorageMemory(MB)Col With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Available Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown("=");
		//customReports.SendValueToTextfield(Config.AvailableStorageMemory);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailableStorageMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("Report For AvailableStorageMemory(MB)Col With Filter as equal operator");
		customReports.ClickOnViewReportLinkButton();
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingAvailableStorageMemoryReport_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}*/
	@Test(priority=-159,description="Verify  Available Storage Memory (MB) column with filter as NotEqualTo operator")
	public void VerifyAvailableStorageMemoryColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====41.Verify  Available Storage Memory (MB) column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailableStorageMemory(MB)Col With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Available Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("50000");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailableStorageMemory(MB)Col With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For AvailableStorageMemory(MB)Col With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("50000");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-158,description="Verify  Available Storage Memory (MB) column with filter as less than or equal to operator")
	public void VerifyAvailableStorageMemoryColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====42.Verify  Available Storage Memory (MB) column with filter as less than or equal to operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailableStorageMemory(MB)Col With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Available Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("50000");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailableStorageMemory(MB)Col With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For AvailableStorageMemory(MB)Col With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-157,description="Verify  Available Storage Memory (MB) column with filter as greater than or equal to opeartor")
	public void VerifyAvailableStorageMemoryColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====43.Verify  Available Storage Memory (MB) column with filter as greater than or equal to opeartor===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For AvailableStorageMemory(MB)Col With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Available Storage Memory (MB)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Available Storage Memory (MB)");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For AvailableStorageMemory(MB)Col With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For AvailableStorageMemory(MB)Col With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	   // customReports.ChooseDevicesPerPage();
	    customReports.VerifyingReport_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-156,description="Verify Operating System column with filter as equal operator")
	public void VerifyOperatingSystemColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====44.Verify Operating System column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Operating System With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Operating System");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Operating System");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("ANDROID 10");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Operating System With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Operating System With Filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingOperatingSystemCol_EqualOp(); 
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-155,description="Verify Operating System column with filter as NotEqualTo operator")
	public void VerifyOperatingSystemColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====45.Verify Operating System column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Operating System With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Operating System");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Operating System");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("LOLLIPOP");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Operating System With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Operating System With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("LOLLIPOP");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-154,description="Verify Operating System column with filter as Like operator")
	public void VerifyOperatingSystemColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====46.Verify Operating System column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Operating System With Filter as Like operator","test");
		customReports.ClickOnColumnInTable("Operating System");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Operating System");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("AND");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Operating System With Filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Operating System With Filter as Like operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingOperatingSystemCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-153,description="Verify MAC Address column with filter as equal operator")
	public void VerifyMACAddressColumn_EqualOP() throws InterruptedException
	{

		Reporter.log("\n====47.Verify MAC Address column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For MAC Address With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("MAC Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("MAC Address");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.MAC_Address);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For MAC Address With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For MAC Address With Filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingMacAddressCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-152,description="Verify MAC Address column with filter as NotEqualTo operator")
	public void VerifyMACAddressColumn_NotEqualToOP() throws InterruptedException
	{

		Reporter.log("\n====48.Verify MAC Address column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For MAC Address With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("MAC Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("MAC Address");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.MAC_Address);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For MAC Address With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For MAC Address With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.MAC_Address);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-151,description="Verify MAC Address column with filter as Like operator")
	public void VerifyMACAddressColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====49.Verify MAC Address column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For MAC Address With Filter as Like operator","test");
		customReports.ClickOnColumnInTable("MAC Address");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("MAC Address");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("70");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For MAC Address With Filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For MAC Address With Filter as Like operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingMacAddressCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-150,description="Verify Network Operator column with filter as equal operator")
	public void VerifyNetworkOperatorColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====50.Verify Network Operator column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Network Operator With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Network Operator");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Network Operator");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Network_OPerator);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Network Operator With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Network Operator With Filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingNetworkOperatorCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-149,description="Verify Network Operator column with filter as NotEqualTo operator")
	public void VerifyNetworkOperatorColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====51.Verify Network Operator column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Network Operator With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Network Operator");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Network Operator");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Network_OPerator);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Network Operator With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Network Operator With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Network_OPerator);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		
	}
	@Test(priority=-148,description="Verify Network Operator column with filter as Like operator")
	public void VerifyNetworkOperatorColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====52.Verify Network Operator column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Network Operator With Filter as Like operator","test");
		customReports.ClickOnColumnInTable("Network Operator");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Network Operator");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Jio");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Network Operator With Filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Network Operator With Filter as Like operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingNetworkOperatorCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-147,description="Verify Phone Signal column with filter as equal operator")
	public void VerifyPhoneSignalColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====53.Verify Phone Signal column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Phone Signal With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Phone Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Phone Signal");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Phone Signal With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Phone Signal With Filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-146,description="Verify Phone Signal column with filter as NotEqualTo operator")
	public void VerifyPhoneSignalColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====54.Verify Phone Signal column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Phone Signal With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Phone Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Phone Signal");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Phone Signal With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Phone Signal With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("100");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-145,description="Verify Phone Signal column with filter as less than or equal to operator")
	public void VerifyPhoneSignalColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====55.Verify Phone Signal column with filter as less than or equal to operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Phone Signal With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Phone Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Phone Signal");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Phone Signal With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Phone Signal With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-144,description="Verify Phone Signal column with filter as greater than or equal to opeartor")
	public void VerifyPhoneSignalColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====56.Verify Phone Signal column with filter as greater than or equal to opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Phone Signal With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Phone Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Phone Signal");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("50");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Phone Signal With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Phone Signal With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-143,description="Verify SSID column with filter as equal operator")
	public void VerifySSIDColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====57.Verify SSID column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For SSID With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("SSID");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SSID");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.SSID);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For SSID With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For SSID With Filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingSSIDCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		
	}
	@Test(priority=-142,description="Verify SSID column with filter as NotEqualTo operator")
	public void VerifySSIDColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====58.Verify SSID column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For SSID With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SSID");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SSID");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.SSID);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For SSID With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For SSID With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.SSID);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-141,description="Verify SSID column with filter as Like operator")
	public void VerifySSIDColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====59.Verify SSID column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For SSID With Filter as Like operator","test");
		customReports.ClickOnColumnInTable("SSID");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SSID");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.SSID);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For SSID With Filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For SSID With Filter as Like operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingSSIDCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-140,description="Verify Agent Version column with filter as equal operator")
	public void VerifyAgentVersionColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====60.Verify Agent Version column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Agent Version With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Agent Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Agent Version");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.AgentVersion);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Agent Version With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Agent Version With Filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingAgentVersionCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-139,description="Verify Agent Version column with filter as NotEqualTo operator")
	public void VerifyAgentVersionColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====61.Verify Agent Version column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Agent Version With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Agent Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Agent Version");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.AgentVersion);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Agent Version With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Agent Version With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.AgentVersion);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-138,description="Verify Agent Version column with filter as less than or equal to operator")
	public void VerifyAgentVersionColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====62.Verify Agent Version column with filter as less than or equal to operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Agent Version With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Agent Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Agent Version");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield(Config.AgentVersion);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Agent Version With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Agent Version With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingAgentVersionCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-137,description="Verify Agent Version column with filter as greater than or equal to opeartor")
	public void VerifyAgentVersionColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====63.Verify Agent Version column with filter as greater than or equal to opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For Agent Version With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Agent Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Agent Version");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield(Config.AgentVersion);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For Agent Version With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For Agent Version With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyingAgentVersionCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-136,description="Verify IMEI column with filter as equal operator")
	public void VerifyIMEIColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====64.Verify IMEI column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IMEI col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("IMEI");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("IMEI");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.IMEI);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IMEI col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IMEI col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingIMEICol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-135,description="Verify IMEI column with filter as NotEqualTo operator")
	public void VerifyIMEIColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====65.Verify IMEI column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IMEI col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("IMEI");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("IMEI");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.IMEI);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IMEI col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IMEI col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.IMEI);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-134,description="Verify IMEI column with filter as Like operator")
	public void VerifyIMEIColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====66.Verify IMEI column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IMEI col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("IMEI");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("IMEI");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("8698");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IMEI col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IMEI col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingIMEICol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-133,description="Verify IMSI column with filter as equal operator")
	public void VerifyIMSIColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===67.Verify IMSI column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IMSI col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("IMSI");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("IMSI");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Unknown");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IMSI col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IMSI col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyIMSICol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-132,description="Verify IMSI column with filter as NotEqualTo operator")
	public void VerifyIMSIColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===68.Verify IMSI column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IMSI col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("IMSI");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("IMSI");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Unknown");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IMSI col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IMSI col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Unknown");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-131,description="Verify IMSI column with filter as Like operator")
	public void VerifyIMSIColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n===69.Verify IMSI column with filter as Like operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IMSI col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("IMSI");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("IMSI");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("4044");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IMSI col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IMSI col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyIMSICol__LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-130,description="Verify Protocol column with filter as equal operator")
	public void VerifyProtocolColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n==70.Verify Protocol column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Protocol col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Protocol");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Protocol");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Secured");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Protocol col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Protocol col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingProtocalCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-129,description="Verify Protocol column with filter as NotEqualTo operator")
	public void VerifyProtocolColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n==71.Verify Protocol column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Protocol col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Protocol");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Protocol");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Unsecured");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Protocol col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Protocol col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Unsecured");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-128,description="Verify Protocol column with filter as Like operator")
	public void VerifyProtocolColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n=====72.Verify Protocol column with filter as Like operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Protocol col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Protocol");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Protocol");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Unsecure");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Protocol col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Protocol col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingProtocalCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-127,description="Verify Wi-Fi Signal column with filter as equal operator")
	public void VerifyWiFiSignalColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n======73.Verify Wi-Fi Signal column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Wi-Fi Signal col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Wi-Fi Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Wi-Fi Signal");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Wi-Fi Signal col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Wi-Fi Signal col with filter as equal operator");
	    reportsPage.WindowHandle();
        customReports.VerifyingPhoneSignalCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-126,description="Verify Wi-Fi Signal column with filter as NotEqualTo operator")
	public void VerifyWiFiSignalColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n======74.Verify Wi-Fi Signal column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Wi-Fi Signal col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Wi-Fi Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Wi-Fi Signal");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Wi-Fi Signal col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Wi-Fi Signal col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("100");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-125,description="Verify Wi-Fi Signal column with filter as less than or equal to operator")
	public void VerifyWiFiSignalColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n=======75.Verify Wi-Fi Signal column with filter as less than or equal to operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Wi-Fi Signal col with filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Wi-Fi Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Wi-Fi Signal");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Wi-Fi Signal col with filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Wi-Fi Signal col with filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingPhoneSignalCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-124,description="Verify Wi-Fi Signal column with filter as greater than or equal to opeartor")
	public void VerifyWiFiSignalColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n======76.Verify Wi-Fi Signal column with filter as greater than or equal to opeartor===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Wi-Fi Signal col with filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Wi-Fi Signal");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Wi-Fi Signal");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("50");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Wi-Fi Signal col with filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Wi-Fi Signal col with filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyingPhoneSignalCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-123,description="Verify Root Status column with filter as equal operator")
	public void VerifyRootStatusColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n======77.Verify Root Status column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Root Status col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Root Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Root Status");
		customReports.SelectRootStatus("4");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Root Status col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Root Status col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingRootStatusCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-122,description="Verify Root Status column with filter as NotEqualTo operator")
	public void VerifyRootStatusColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====78.Verify Root Status column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Root Status col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Root Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Root Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SelectRootStatus("1");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Root Status col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Root Status col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Granted");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-121,description="Verify KNOX Status column with filter as equal operator")
	public void VerifyKNOXStatusColumn_EqualOP() throws InterruptedException
	{//not working
		Reporter.log("\n====79.Verify KNOX Status column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for KNOX Status col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("KNOX Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("KNOX Status");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for KNOX Status col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for KNOX Status col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		
	}
	@Test(priority=-120,description="Verify KNOX Status column with filter as NotEqualTo operator")
	public void VerifyKNOXStatusColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====80.Verify KNOX Status column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for KNOX Status col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("KNOX Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("KNOX Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("No");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for KNOX Status col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for KNOX Status col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("No");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-119,description="Verify SureLock Version column with filter as equal operator")
	public void VerifySureLockVersionColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====81.Verify SureLock Version column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLockVersion col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureLock Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Version");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.SureLock_version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLockVersion col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLockVersion col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifySureLockVersionCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-118,description="Verify SureLock Version column with filter as NotEqualTo operator")
	public void VerifySureLockVersionColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====82.Verify SureLock Version column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLock Version col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureLock Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Version");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.SureLock_version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLock Version col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLock Version col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.SureLock_version);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-117,description="Verify SureLock Version column with filter as less than or equal to operator")
	public void VerifySureLockVersionColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====83.Verify SureLock Version column with filter as lessthan or equalto operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLock Version col with filter as lessthan or equalto operator","test");
		customReports.ClickOnColumnInTable("SureLock Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Version");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLock Version col with filter as lessthan or equalto operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLock Version col with filter as lessthan or equalto operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyVersionCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority=-116,description="Verify SureLock Version column with filter as greater than or equal to opeartor")
	public void VerifySureLockVersionColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====84.Verify SureLock Version column with filter as greaterthan or equalto opeartor===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLock Version col with filter as greaterthan or equalto opeartor","test");
		customReports.ClickOnColumnInTable("SureLock Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Version");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLock Version col with filter as greaterthan or equalto opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLock Version col with filter as greaterthan or equalto opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyVersionCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-115,description="Verify SureFox Version column with filter as equal operator")
	public void VerifySureFoxVersionColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===85.Verify SureFox Version column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxVersion col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureFox Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Version");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.SureFox_version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxVersion col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxVersion col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifySureFoxVersionCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-114,description="Verify SureFox Version column with filter as NotEqualTo operator")
	public void VerifySureFoxVersionColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===86.Verify SureFox Version column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxVersion col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureFox Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Version");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.SureFox_version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxVersion col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxVersion col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.SureFox_version);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-113,description="Verify SureFox Version column with filter as lessthan or equalto operator")
	public void VerifySureFoxVersionColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===87.Verify SureFox Version column with filter as lessthan or equalto operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxVersion col with filter as lessthan or equalto operator","test");
		customReports.ClickOnColumnInTable("SureFox Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Version");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxVersion col with filter as lessthan or equalto operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxVersion col with filter as lessthan or equalto operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyVersionCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-112,description="Verify SureFox Version column with filter as greaterthan or equalto opeartor")
	public void VerifySureFoxVersionColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===88.Verify SureFox Version column with filter as greaterthan or equalto opeartor===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxVersion col with filter as greaterthan or equalto opeartor","test");
		customReports.ClickOnColumnInTable("SureFox Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Version");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxVersion col with filter as greaterthan or equalto opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxVersion col with filter as greaterthan or equalto opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyVersionCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-111,description="Verify SureVideo Version column with filter as equal operator")
	public void VerifySureVideoVersionColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===89.Verify SureVideoVersion column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoVersion col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureVideo Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Version");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.SureVideo_version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoVersion col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoVersion col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifySureVideoVersionCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-110,description="Verify SureVideo Version column with filter as NotEqualTo operator")
	public void VerifySureVideoVersionColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===90.Verify SureVideoVersion column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoVersion col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureVideo Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Version");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.SureVideo_version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoVersion col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoVersion col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.SureVideo_version);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-109,description="Verify SureVideo Version column with filter as lessthan or equalto operator")
	public void VerifySureVideoVersionColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===91.Verify SureVideoVersion column with filter as lessthan or equalto operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoVersion col with filter as lessthan or equalto operator","test");
		customReports.ClickOnColumnInTable("SureVideo Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Version");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoVersion col with filter as lessthan or equalto operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoVersion col with filter as lessthan or equalto operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyVersionCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-108,description="Verify SureVideo Version column with filter as greaterthan or equalto opeartor")
	public void VerifySureVideoVersionColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===92.Verify SureVideoVersion column with filter as greaterthan or equalto opeartor===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoVersion col with filter as greaterthan or equalto opeartor","test");
		customReports.ClickOnColumnInTable("SureVideo Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Version");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoVersion col with filter as greaterthan or equalto opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoVersion col with filter as greaterthan or equalto opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyVersionCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-107,description="Verify Default Launcher column with filter as equal operator")
	public void VerifyDefaultLauncherColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===93.Verify Default Launcher column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Default Launcher col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Default Launcher");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Default Launcher");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.DefaultLauncher);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Default Launcher col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Default Launcher col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyDefaultLauncherCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-106,description="Verify Default Launcher column with filter as NotEqualTo operator")
	public void VerifyDefaultLauncherColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===94.Verify Default Launcher column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Default Launcher col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Default Launcher");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Default Launcher");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.DefaultLauncher);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Default Launcher col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Default Launcher col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.DefaultLauncher);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-105,description="Verify Default Launcher column with filter as Like operator")
	public void VerifyDefaultLauncherColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n===95.Verify Default Launcher column with filter as Like operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Default Launcher col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Default Launcher");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Default Launcher");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("android");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Default Launcher col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Default Launcher col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyDefaultLauncherCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-104,description="Verify Serial Number column with filter as equal operator")
	public void VerifySerialNumberColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====96.Verify Serial Number column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Serial Number col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Serial Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Serial Number");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.SerialNumber);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Serial Number col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Serial Number col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifySerialNumberCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-103,description="Verify Serial Number column with filter as NotEqualTo operator")
	public void VerifySerialNumberColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====97.Verify Serial Number column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Serial Number col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Serial Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Serial Number");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.SerialNumber);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Serial Number col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Serial Number col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.SerialNumber);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-102,description="Verify Serial Number column with filter as Like operator")
	public void VerifySerialNumberColumn1_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====98.Verify Serial Number column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Serial Number col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Serial Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Serial Number");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("RZ");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Serial Number col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Serial Number col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifySerialNumberCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-101,description="Verify Device Phone Number column with filter as equal operator")
	public void VerifyDevicePhoneNumberColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====99.Verify Device Phone Number column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DevicePhoneNumber col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Device Phone Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Phone Number");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Phone_Number);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DevicePhoneNumber col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DevicePhoneNumber col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyDevicePhoneNumberCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-100,description="Verify Device Phone Number column with filter as NotEqualTo operator")
	public void VerifyDevicePhoneNumberColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====100.Verify Device Phone Number column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DevicePhoneNumber col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Device Phone Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Phone Number");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Phone_Number);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DevicePhoneNumber col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DevicePhoneNumber col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Phone_Number);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-99,description="Verify Device Phone Number column with filter as Like operator")
	public void VerifyDevicePhoneNumberColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====101.Verify Device Phone Number column with filter as Like operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DevicePhoneNumber col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Device Phone Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Phone Number");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("496");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DevicePhoneNumber col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DevicePhoneNumber col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyDevicePhoneNumberCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-98,description="Verify Sim Serial Number column with filter as equal operator")
	public void VerifySimSerialNumberColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====102.Verify Sim Serial Number column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SimSerialNumber col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Sim Serial Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Sim Serial Number");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.SimSerialNumber);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SimSerialNumber col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SimSerialNumber col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifySimSerialNumberCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-97,description="Verify Sim Serial Number column with filter as NotEqualTo operator")
	public void VerifySimSerialNumberColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====103.Verify Sim Serial Number column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SimSerialNumber col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Sim Serial Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Sim Serial Number");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.SimSerialNumber);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SimSerialNumber col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SimSerialNumber col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.SimSerialNumber);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-96,description="Verify Sim Serial Number column with filter as Like operator")
	public void VerifySimSerialNumberColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n====104.Verify Sim Serial Number column with filter as Like operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SimSerialNumber col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Sim Serial Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Sim Serial Number");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("2477");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SimSerialNumber col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SimSerialNumber col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.VerifySimSerialNumberCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-95,description="Verify Roaming column with filter as equal operator")
	public void VerifyRoamingColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====105.Verify Roaming column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Roaming col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Roaming");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Roaming");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Roaming_Status);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Roaming col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Roaming col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyRoamingCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-94,description="Verify Roaming column with filter as NotEqualTo operator")
	public void VerifyRoamingColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====106.Verify Roaming column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Roaming col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Roaming");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Roaming");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("N/A");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Roaming col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Roaming col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("N/A");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-93,description="Verify OS Version column with filter as equal operator")
	public void VerifyOSVersionColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===107.Verify OS Version column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OSVersion col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("OS Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("OS Version");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("9");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OSVersion col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OSVersion col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyOSVersionCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-92,description="Verify OS Version column with filter as NotEqualTo operator")
	public void VerifyOSVersionColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===108.Verify OS Version column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OSVersion col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("OS Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("OS Version");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("8.0.0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OSVersion col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OSVersion col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("8.0.0");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-91,description="Verify OS Version column with filter as lessthan or equalto operator")
	public void VerifyOSVersionColumn3() throws InterruptedException
	{
		Reporter.log("\n===109.Verify OS Version column with filter as lessthan or equalto operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OSVersion col with filter as lessthan or equalto operator","test");
		customReports.ClickOnColumnInTable("OS Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("OS Version");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OSVersion col with filter as lessthan or equalto operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OSVersion col with filter as lessthan or equalto operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyOSVersionCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-90,description="Verify OS Version column with filter as greaterthan or equalto opeartor")
	public void VerifyOSVersionColumn4() throws InterruptedException
	{
		Reporter.log("\n===110.Verify OS Version column with filter as greaterthan or equalto opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OSVersion col with filter as greaterthan or equalto opeartor","test");
		customReports.ClickOnColumnInTable("OS Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("OS Version");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("10");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OSVersion col with filter as greaterthan or equalto opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OSVersion col with filter as greaterthan or equalto opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyOSVersionCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-89,description="Verify Last Device Time column with filter as Range operator")
	public void VerifyLastDeviceTimeColumn_RangeOP() throws InterruptedException
	{
		Reporter.log("\n===111.Verify Last Device Time column with filter as Range operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for LastDeviceTime col with filter as Range operator","test");
		customReports.ClickOnColumnInTable("Last Device Time");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Last Device Time");
		customReports.SelectTodayDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for LastDeviceTime col with filter as Range operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for LastDeviceTime col with filter as Range operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyLastDeviceTimeCol();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-88,description="Verify DataUsage column with filter as equal operator")
	public void VerifyDataUsageColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===112.Verify DataUsage column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DataUsage col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Data Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Data Usage");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("5000");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DataUsage col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DataUsage col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.VerifyingAvailableStorageMemoryReport_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-87,description="Verify DataUsage column with filter as NotEqualTo operator")
	public void VerifyDataUsageColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===113.Verify DataUsage column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DataUsage col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Data Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Data Usage");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("5000");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DataUsage col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DataUsage col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("5000");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-86,description="Verify DataUsage column with filter as lessthan or equalto operator")
	public void VerifyDataUsageColumn_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===114.Verify DataUsage column with filter as lessthan or equalto operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DataUsage col with filter as lessthan or equalto operator","test");
		customReports.ClickOnColumnInTable("Data Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Data Usage");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DataUsage col with filter as lessthan or equalto operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DataUsage col with filter as lessthan or equalto operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingReport_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-85,description="Verify DataUsage column with filter as greaterthan or equalto opeartor")
	public void VerifyDataUsageColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===115.Verify DataUsage column with filter as greaterthan or equalto opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DataUsage col with filter as greaterthan or equalto opeartor","test");
		customReports.ClickOnColumnInTable("Data Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Data Usage");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DataUsage col with filter as greaterthan or equalto opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DataUsage col with filter as greaterthan or equalto opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyingReport_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-84,description="Verify CPU Usage column with filter as equal operator")
	public void VerifyCPUusageColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===116.Verify CPU Usage column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for CPU Usage col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("CPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("CPU Usage");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for CPU Usage col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for CPU Usage col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingReportForUsage_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-83,description="Verify CPU Usage column with filter as NotEqualTo operator")
	public void VerifyCPUusageColumn__NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===117.Verify CPU Usage column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for CPU Usage col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("CPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("CPU Usage");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("-1");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for CPU Usage col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for CPU Usage col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("-1");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-82,description="Verify CPU Usage column with filter as lessthan or equalto operator")
	public void VerifyCPUusageColumn__LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===118.Verify CPU Usage column with filter as lessthan or equalto operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for CPU Usage col with filter as lessthan or equalto operator","test");
		customReports.ClickOnColumnInTable("CPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("CPU Usage");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for CPU Usage col with filter as lessthan or equalto operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for CPU Usage col with filter as lessthan or equalto operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyDataUsageCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-81,description="Verify CPU Usage column with filter as greaterthan or equalto opeartor")
	public void VerifyCPUusageColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===119.Verify CPU Usage column with filter as greaterthan or equalto opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for CPU Usage col with filter as greaterthan or equalto opeartor","test");
		customReports.ClickOnColumnInTable("CPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("CPU Usage");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for CPU Usage col with filter as greaterthan or equalto opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for CPU Usage col with filter as greaterthan or equalto opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyDataUageCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-80,description="Verify GPU Usage column with filter as equal operator")
	public void VerifyGPUusageColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===120.Verify GPU Usage column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GPU Usage col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("GPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("GPU Usage");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GPU Usage col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GPU Usage col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingReportForUsage_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority=-79,description="Verify GPU Usage column with filter as NotEqualTo operator")
	public void VerifyGPUusageColumn__NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===121.Verify GPU Usage column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GPU Usage col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("GPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("GPU Usage");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("-1");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GPU Usage col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GPU Usage col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("-1");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-78,description="Verify GPU Usage column with filter as lessthan or equalto operator")
	public void VerifyGPUusageColumn__LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===122.Verify GPU Usage column with filter as lessthan or equalto operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GPU Usage col with filter as lessthan or equalto operator","test");
		customReports.ClickOnColumnInTable("GPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("GPU Usage");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GPU Usage col with filter as lessthan or equalto operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GPU Usage col with filter as lessthan or equalto operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyDataUsageCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-77,description="Verify GPU Usage column with filter as greaterthan or equalto opeartor")
	public void VerifyGPUusageColumn_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n===123.Verify GPU Usage column with filter as greaterthan or equalto opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GPU Usage col with filter as greaterthan or equalto opeartor","test");
		customReports.ClickOnColumnInTable("GPU Usage");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("GPU Usage");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("5");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GPU Usage col with filter as greaterthan or equalto opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GPU Usage col with filter as greaterthan or equalto opeartor");
	    reportsPage.WindowHandle();
	    customReports.VerifyDataUageCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	/*@Test(priority=-75,description="Verify Battery Temperature column with filter as equal operator")
	public void VerifyBatteryTempColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===124.Verify Battery Temperature column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Battery Temperature col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Battery Temperature");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Temperature");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Battery Temperature col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Battery Temperature col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyBatteryTemp_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}*/
	@Test(priority=-75,description="Verify Battery Temperature column with filter as NotEqualTo operator")
	public void VerifyBatteryTempColumn__NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===125.Verify Battery Temperature column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Battery Temperature col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Battery Temperature");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Battery Temperature");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("N/A");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Battery Temperature col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
	    customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Battery Temperature col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("N/A");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-74,description="Verify Is Supervised(IOS) column with filter as equal operator")
	public void VerifyIsSupervisedColumn__EqualOP() throws InterruptedException
	{
		Reporter.log("\n===126.Verify Is Supervised(IOS) column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IsSupervised(IOS) col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Is Supervised(iOS)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Is Supervised(iOS)");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IsSupervised(IOS) col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IsSupervised(IOS) col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-73,description="Verify Is Supervised(IOS) column with filter as NotEqualTo operator")
	public void VerifyIsSupervisedColumn__NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===127.Verify Is Supervised(IOS) column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for IsSupervised(IOS) col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Is Supervised(iOS)");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Is Supervised(iOS)");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for IsSupervised(IOS) col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for IsSupervised(IOS) col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-72,description="Verify SureLock Licensed column with filter as equal operator")
	public void VerifySureLockLicensedColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===128.Verify SureLock Licensed column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLockLicensed col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureLock Licensed");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Licensed");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLockLicensed col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLockLicensed col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-71,description="Verify SureLock Licensed column with filter as NotEqualTo operator")
	public void VerifySureLockLicensedColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===129.Verify SureLock Licensed column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLockLicensed col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureLock Licensed");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Licensed");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLockLicensed col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLockLicensed col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-70,description="Verify SureFox Licensed column with filter as equal operator")
	public void VerifySureFoxLicensedColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===130.Verify SureFox Licensed column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxLicensed col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureFox Licensed");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Licensed");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxLicensed col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxLicensed col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-69,description="Verify SureFox Licensed column with filter as NotEqualTo operator")
	public void VerifySureFoxLicensedColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===131.Verify SureFox Licensed column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxLicensed col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureFox Licensed");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Licensed");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxLicensed col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxLicensed col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-68,description="Verify SureVideo Licensed column with filter as equal operator")
	public void VerifySureVideoLicensedColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===132.Verify SureVideo Licensed column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoLicensed col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureVideo Licensed");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Licensed");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoLicensed col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoLicensed col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-67,description="Verify SureVideo Licensed column with filter as NotEqualTo operator")
	public void VerifySureVideoLicensedColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===133.Verify SureVideo Licensed column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoLicensed col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureVideo Licensed");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Licensed");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoLicensed col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoLicensed col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-66,description="Verify SureLock Admin column with filter as equal operator")
	public void VerifySureLockAdminColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===134.Verify SureLock Admin column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLockAdmin col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureLock Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Admin");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLockAdmin col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLockAdmin col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-65,description="Verify SureLock Admin column with filter as NotEqualTo operator")
	public void VerifySureLockAdminColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===135.Verify SureLock Admin column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureLockAdmin col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureLock Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureLock Admin");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureLockAdmin col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureLockAdmin col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	  //  customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-64,description="Verify SureFox Admin column with filter as equal operator")
	public void VerifySureFoxAdminColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===136.Verify SureFox Admin column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxAdmin col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureFox Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Admin");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxAdmin col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxAdmin col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-63,description="Verify SSureFox Admin column with filter as NotEqualTo operator")
	public void VerifySureFoxAdminColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===137.Verify SureFox Admin column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureFoxAdmin col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureFox Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureFox Admin");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureFoxAdmin col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureFoxAdmin col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-62,description="Verify SureVideo Admin column with filter as equal operator")
	public void VerifySureVideoAdminColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===138.Verify SureVideo Admin column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoAdmin col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("SureVideo Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Admin");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoAdmin col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoAdmin col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-61,description="Verify SureVideo Admin column with filter as NotEqualTo operator")
	public void VerifySureVideoAdminColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===140.Verify SureVideo Admin column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SureVideoAdmin col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("SureVideo Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("SureVideo Admin");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SureVideoAdmin col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SureVideoAdmin col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-60,description="Verify Nix Admin column with filter as equal operator")
	public void VerifyNixAdminColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===141.Verify Nix Admin column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for NixAdmin col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Nix Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Nix Admin");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for NixAdmin col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for NixAdmin col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-59,description="Verify Nix Admin column with filter as NotEqualTo operator")
	public void VerifyNixAdminColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===142.Verify Nix Admin column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for NixAdmin col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Nix Admin");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Nix Admin");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for NixAdmin col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for NixAdmin col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-58,description="Verify Nix Polling column with filter as equal operator")
	public void VerifyNixPollingColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===143.Verify Nix Polling column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for NixPolling col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Nix Polling");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Nix Polling");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("FCM");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for NixPolling col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for NixPolling col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyNixPollingStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-57,description="Verify Nix Polling column with filter as NotEqualTo operator")
	public void VerifyNixPollingColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===144.Verify Nix Admin column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for NixPolling col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Nix Polling");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Nix Polling");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("FCM");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for NixPolling col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for NixPolling col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("FCM");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-56,description="Verify Network Type column with filter as equal operator")
	public void VerifyNetworkTypeColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===145.Verify NetworkType column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for NetworkType col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Network Type");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Network Type");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Wi-Fi");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for NetworkType col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for NetworkType col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyNetworkTypeStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-55,description="Verify Network Type column with filter as NotEqualTo operator")
	public void VerifyNetworkTypeColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n=====146.Verify Network Type column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for NetworkType col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Network Type");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Network Type");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Wi-Fi");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for NetworkType col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for NetworkType col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Wi-Fi");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-55,description="Verify GPS Enabled column with filter as equal operator")
	public void VerifyGPSEnabledColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===147.Verify GPS Enabled column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GPSEnabled col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("GPS Enabled");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("GPS Enabled");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Enabled");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GPSEnabled col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GPSEnabled col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyGPSEnabledStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-54,description="Verify GPS Enabled column with filter as NotEqualTo operator")
	public void VerifyGPSEnabledColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n=====148.Verify GPS Enabled column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GPSEnabled col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("GPS Enabled");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("GPS Enabled");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Disabled");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GPSEnabled col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GPSEnabled col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Disabled");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-53,description="Verify Group Path column with filter as equal operator")
	public void VerifyGroupPathColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n=====149.Verify Group Path column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GroupPath col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Group Path");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Group Path");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Group_Path);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GroupPath col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GroupPath col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyGroupPathStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-52,description="Verify Group Path column with filter as NotEqualTo operator")
	public void VerifyGroupPathColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===150.Verify Group Path column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GroupPath col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Group Path");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Group Path");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Group_Path);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GroupPath col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GroupPath col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Group_Path);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-51,description="Verify Group Path column with filter as Like operator")
	public void VerifyGroupPathColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n===151.Verify Group Path column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for GroupPath col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Group Path");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Group Path");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.GroupName_Like);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for GroupPath col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for GroupPath col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyGroupPathStatus_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-50,description="Verify Tags column with filter as equal operator")
	public void VerifyTagsColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===152.Verify Tags column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Tags col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Tags");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Tags");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Tag);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Tags col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Tags col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyTag_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-49,description="Verify Tags column with filter as NotEqualTo operator")
	public void VerifyTagsColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===153.Verify Tags Path column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Tags col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Tags");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Tags");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Tag);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Tags col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Tags col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Tag);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-48,description="Verify Tags column with filter as Like operator")
	public void VerifyTagsColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n===154.Verify Tags Path column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Tags col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Tags");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Tags");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.TagName_Like);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Tags col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Tags col with filter as Like operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyTag_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-47,description="Verify Online Status column with filter as equal operator")
	public void VerifyOnlineStatusColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===155.Verify Online Status column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OnlineStatus col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Online Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Online Status");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Online");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OnlineStatus col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OnlineStatus col with filter as equal operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.VerifyOnlineStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-46,description="Verify OnlineStatus column with filter as NotEqualTo operator")
	public void VerifyOnlineStatusColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===156.Verify OnlineStatus column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OnlineStatus col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Online Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Online Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Online");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OnlineStatus col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OnlineStatus col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    //customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Online");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-45,description="Verify Enrolled column with filter as equal operator")
	public void VerifyEnrolledColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===157.Verify Enrolled column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Enrolled col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Enrolled");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Enrolled");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Enrolled col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Enrolled col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-44,description="Verify Enrolled column with filter as NotEqualTo operator")
	public void VerifyEnrolledColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===158.Verify Enrolled column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Enrolled col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Enrolled");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Enrolled");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Enrolled col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Enrolled col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Yes");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	/*@Test(priority=-43,description="Verify DeviceUserName column with filter as equal operator")
	public void VerifyDeviceUserNameColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===159.Verify DeviceUserName column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceUserName col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Device User Name");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device User Name");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.DeviceUser_Name);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceUserName col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceUserName col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyDeviceUserNameCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	*/
	@Test(priority=-42,description="Verify DeviceUserName column with filter as NotEqualTo operator")
	public void VerifyDeviceUserNameColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===160.Verify DeviceUserName column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceUserName col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Device User Name");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device User Name");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.DeviceUser_Name);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceUserName col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceUserName col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.DeviceUser_Name);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-41,description="Verify DeviceUserName column with filter as Like operator")
	public void VerifyDeviceUserNameColumn_LikeOp() throws InterruptedException
	{
		Reporter.log("\n===161.Verify DeviceUserName column with filter as Like operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceUserName col with filter as Like operator","test");
		customReports.ClickOnColumnInTable("Device User Name");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device User Name");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.DeviceUserName_Like);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceUserName col with filter as Like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceUserName col with filter as Like operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyDeviceUserNameCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-40,description="Verify Bluetooth Status column with filter as equal operator")
	public void VerifyBluetoothStatusColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===162.Verify Bluetooth Status column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BluetoothStatus col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Bluetooth Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Bluetooth Status");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("On");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BluetoothStatus col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BluetoothStatus col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyBluetoothStatusCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-39,description="Verify BluetoothStatus column with filter as NotEqualTo operator")
	public void VerifyBluetoothStatusColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===163.Verify Bluetooth Status column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BluetoothStatus col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Bluetooth Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Bluetooth Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Off");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BluetoothStatus col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BluetoothStatus col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Off");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-38,description="Verify USB Status column with filter as equal operator")
	public void VerifyUSBStatusColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===164.Verify USB Status column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for USBStatus col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("USB Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("USB Status");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Plugged In");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for USBStatus col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for USBStatus col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyUsbStatusCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-37,description="Verify USB Status column with filter as NotEqualTo operator")
	public void VerifyUSBStatusColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===165.Verify USB Status column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for USBStatus col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("USB Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("USB Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Plugged Out");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for USBStatus col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for USBStatus col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Plugged Out");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-36,description="Verify Bluetooth SSID column with filter as equal operator")
	public void VerifyBluetoothSSIDColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===166.Verify Bluetooth SSID column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BluetoothSSID col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Bluetooth SSID");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Bluetooth SSID");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.BluetoothSSID);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BluetoothSSID col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BluetoothSSID col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyBluetoothSSIDCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-35,description="Verify Bluetooth SSID column with filter as NotEqualTo operator")
	public void VerifyBluetoothSSIDColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===167.Verify Bluetooth SSID column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BluetoothSSID col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Bluetooth SSID");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Bluetooth SSID");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.BluetoothSSID);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BluetoothSSID col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BluetoothSSID col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.BluetoothSSID);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-34,description="Verify OS Build Number column with filter as equal operator")
	public void VerifyOSBuildNumberColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===168.Verify OS Build Number column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OSBuildNumber col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("OS Build Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("OS Build Number");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Android_OSBuildNumber);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OSBuildNumber col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OSBuildNumber col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyOSBuildNumberCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}	
	@Test(priority=-33,description="Verify OS Build Number column with filter as NotEqualTo operator")
	public void VerifyOSBuildNumberColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n===169.Verify OS Build Number column with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for OSBuildNumber col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("OS Build Number");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("OS Build Number");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Android_OSBuildNumber);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for OSBuildNumber col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for OSBuildNumber col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Android_OSBuildNumber);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-32,description="Verify Free Storage Memory column with filter as equal operator")
	public void VerifyFreeStorageMemoryColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====170.Verify Free Storage Memory column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeStorageMemoryCol With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Free Storage Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Storage Memory");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeStorageMemoryCol With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeStorageMemoryCol With Filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-31,description="Verify FreeStorageMemoryCol with filter as NotEqualTo operator")
	public void VerifyFreeStorageMemoryCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====171.Verify FreeStorageMemoryCol with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeStorageMemoryCol With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Free Storage Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Storage Memory");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeStorageMemoryCol With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeStorageMemoryCol With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("0");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-30,description="Verify FreeStorageMemoryCol with filter as less than or equal to operator")
	public void VerifyFreeStorageMemoryCol_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====172.Verify Phone FreeStorageMemoryCol with filter as less than or equal to operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeStorageMemoryCol With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Free Storage Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Storage Memory");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeStorageMemoryCol With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeStorageMemoryCol With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-29,description="Verify FreeStorageMemoryCol with filter as greater than or equal to opeartor")
	public void VerifyFreeStorageMemoryCol_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====173.Verify FreeStorageMemoryCol with filter as greater than or equal to opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeStorageMemoryCol With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Free Storage Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Storage Memory");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("50");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeStorageMemoryCol With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeStorageMemoryCol With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-28,description="Verify Free Program Memory column with filter as equal operator")
	public void VerifyFreeProgramMemoryCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====174.Verify Free Storage Memory column with filter as equal operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeStorageMemoryCol With Filter as equal operator","test");
		customReports.ClickOnColumnInTable("Free Program Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Program Memory");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("0");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeStorageMemoryCol With Filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeStorageMemoryCol With Filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-27,description="Verify FreeProgramMemoryCol with filter as NotEqualTo operator")
	public void VerifyFreeProgramMemoryCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====175.Verify FreeProgramMemoryCol with filter as NotEqualTo operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeProgramMemoryCol With Filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("Free Program Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Program Memory");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeProgramMemoryCol With Filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeProgramMemoryCol With Filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("100");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-26,description="Verify FreeProgramMemoryCol with filter as less than or equal to operator")
	public void VerifyFreeProgramMemoryCol_LessThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====176.Verify Phone FreeProgramMemoryCol with filter as less than or equal to operator====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeProgramMemoryCol With Filter as less than or equal to operator","test");
		customReports.ClickOnColumnInTable("Free Program Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Program Memory");
		customReports.SelectOperatorFromDropDown("<=");
		customReports.SendValueToTextfield("100");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeProgramMemoryCol With Filter as less than or equal to operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeProgramMemoryCol With Filter as less than or equal to operator");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_LessThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-25,description="Verify FreeProgramMemoryCol with filter as greater than or equal to opeartor")
	public void VerifyFreeProgramMemoryCol_GreaterThanorEqualtoOP() throws InterruptedException
	{
		Reporter.log("\n====177.Verify FreeProgramMemoryCol with filter as greater than or equal to opeartor====",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report For FreeProgramMemoryCol With Filter as greater than or equal to opeartor","test");
		customReports.ClickOnColumnInTable("Free Program Memory");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Free Program Memory");
		customReports.SelectOperatorFromDropDown(">=");
		customReports.SendValueToTextfield("50");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report For FreeProgramMemoryCol With Filter as greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report For FreeProgramMemoryCol With Filter as greater than or equal to opeartor");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
	    customReports.VerifyingPhoneSignalCol_GreaterThanOrEqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-24,description="Verify Bluetooth Name column with filter as Equal operator")
	public void VerifyBluetoothNameCol_EqualOP() throws InterruptedException
	{
		Reporter.log("============178.Verify Bluetooth Name column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BluetoothName col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Bluetooth Name");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Bluetooth Name");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Model_Name);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BluetoothName col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BluetoothName col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceModel_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-23,description="Verify Bluetooth Name col with filter as NotEqualTo operator")
	public void VerifyBluetoothNameCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n============179.Verify Bluetooth Name col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BluetoothName col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Bluetooth Name");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Bluetooth Name");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Model_Name);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BluetoothName col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BluetoothName col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Model_Name);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-22,description="Verify BluetoothName col with filter as Like operator")
	public void VerifyBluetoothNameCol_LikeOp() throws InterruptedException
	{
		Reporter.log("\n============180.Verify BluetoothName col with filter as Like operator======",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for BluetoothName col with filter as like Operator","test");
		customReports.ClickOnColumnInTable("Bluetooth Name");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Bluetooth Name");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.Model);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for BluetoothName col with filter as like Operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for BluetoothName col with filter as like Operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceModel_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-21,description="Verify SecurityPatchDate col with filter as Range operator")
	public void VerifySecurityPatchDateCol_RangeOp() throws InterruptedException
	{
		Reporter.log("\n========181.Verify Security Patch Date col with filter as Range operator======",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for SecurityPatchDate col with filter as Range Operator","test");
		customReports.ClickOnColumnInTable("Security Patch Date");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Security Patch Date");
		customReports.SelectLast30DaysInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for SecurityPatchDate col with filter as Range Operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for SecurityPatchDate col with filter as Range Operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingSecurityPatchDate();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-20,description="Verify Android Enterprise Status column with filter as Equal operator")
	public void VerifyAndroidEnterpriseStatusCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===========182.Verify Android Enterprise Status column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for AndroidEnterpriseStatus col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Android Enterprise Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Android Enterprise Status");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.AndroidEnterpriseStatus);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for AndroidEnterpriseStatus col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for AndroidEnterpriseStatus col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingAndroidEnterpriseStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-19,description="Verify Bluetooth Name col with filter as NotEqualTo operator")
	public void VerifyAndroidEnterpriseStatusCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n============183.Verify AndroidEnterpriseStatus col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for AndroidEnterpriseStatus col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Android Enterprise Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Android Enterprise Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("Not Enrolled");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for AndroidEnterpriseStatus col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for AndroidEnterpriseStatus col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("Not Enrolled");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-18,description="Verify Android Enterprise Status col with filter as Like operator")
	public void VerifyAndroidEnterpriseStatusCol_LikeOp() throws InterruptedException
	{   //not working
		Reporter.log("\n=======184.Verify Android Enterprise Status col with filter as Like operator======",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for AndroidEnterpriseStatus col with filter as like Operator","test");
		customReports.ClickOnColumnInTable("Android Enterprise Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Android Enterprise Status");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield("Enroll");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for AndroidEnterpriseStatus col with filter as like Operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for AndroidEnterpriseStatus col with filter as like Operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceModel_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-17,description="Verify Wi-Fi Hotspot Status column with filter as Equal operator")
	public void VerifyWiFiHotspotStatusCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n========185.Verify Wi-Fi Hotspot Status column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Wi-FiHotspotStatus col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Wi-Fi Hotspot Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Wi-Fi Hotspot Status");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("ON");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Wi-FiHotspotStatus col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Wi-FiHotspotStatus col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingWiFiHotspotStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-16,description="Verify Wi-Fi Hotspot Status col with filter as NotEqualTo operator")
	public void VerifyWiFiHotspotStatusCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n==========186.Verify Wi-Fi Hotspot Status col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for Wi-FiHotspotStatuscol with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Wi-Fi Hotspot Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Wi-Fi Hotspot Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("OFF");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for Wi-FiHotspotStatuscol with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for Wi-FiHotspotStatuscol with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("OFF");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-15,description="Verify Encryption Status column with filter as equal operator")
	public void VerifyEncryptionStatusColumn_EqualOP() throws InterruptedException
	{
		Reporter.log("\n====187.Verify Encryption Status column with filter as equal operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for EncryptionStatus col with filter as equal operator","test");
		customReports.ClickOnColumnInTable("Encryption Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Encryption Status");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield("Yes");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for EncryptionStatus col with filter as equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for EncryptionStatus col with filter as equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyStatus_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		
	}
	@Test(priority=-14,description="Verify Encryption Status column with filter as NotEqualTo operator")
	public void VerifyEncryptionStatusColumn_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n====188.Verify Encryption Status column with filter as NotEqualTo operator===",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for EncryptionStatus col with filter as NotEqualTo operator","test");
		customReports.ClickOnColumnInTable("KNOX Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("KNOX Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield("NO");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for EncryptionStatus col with filter as NotEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for EncryptionStatus col with filter as NotEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport("NO");
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-13,description="Verify DeviceTimeZone column with filter as Equal operator")
	public void VerifyDeviceTimeZoneCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n==========189.Verify DeviceTimeZone column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceTimeZone col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Device Time Zone");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Time Zone");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Device_Time_Zone);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceTimeZone col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceTimeZone col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceTimeZoneCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-12,description="Verify DeviceTimeZone col with filter as NotEqualTo operator")
	public void VerifyDeviceTimeZoneCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n=========190.Verify DeviceTimeZone col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceTimeZone col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Device Time Zone");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Time Zone");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Device_Time_Zone);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceTimeZone col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceTimeZone col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Device_Time_Zone);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-11,description="Verify DeviceTimeZone col with filter as Like operator")
	public void VerifyDeviceTimeZoneCol_LikeOp() throws InterruptedException
	{
		Reporter.log("\n==========191.Verify DeviceTimeZone col with filter as Like operator======",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for DeviceTimeZone col with filter as like Operator","test");
		customReports.ClickOnColumnInTable("Device Time Zone");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Time Zone");
		customReports.SelectOperatorFromDropDown("like");
		customReports.SendValueToTextfield(Config.DeviceTimeZone_Like);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for DeviceTimeZone col with filter as like Operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for DeviceTimeZone col with filter as like Operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceTimeZoneCol_LikeOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-10,description="Verify AndroidID column with filter as Equal operator")
	public void VerifyAndroidIDCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===========192.Verify AndroidID column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for AndroidID col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Android ID");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Android ID");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.AndroidID);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for AndroidID col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for AndroidID col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceAndroidIDCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-9,description="Verify AndroidID col with filter as NotEqualTo operator")
	public void VerifyAndroidIDCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n============193.Verify AndroidID col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for AndroidID col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Android ID");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Android ID");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.AndroidID);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for AndroidID col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for AndroidID col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.AndroidID);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-8,description="Verify HashCode column with filter as Equal operator")
	public void VerifyHashCodeCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===========194.Verify HashCode column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for HashCode col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Hash Code");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Hash Code");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.HashCode);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for HashCode col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for HashCode col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingDeviceHashCodeCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-7,description="Verify HashCode col with filter as NotEqualTo operator")
	public void VerifyHashCodeCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n============195.Verify HashCode col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for HashCode col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Hash Code");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Hash Code");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.HashCode);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for HashCode col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for HashCode col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.HashCode);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-6,description="Verify FOTA Registration Status column with filter as Equal operator")
	public void VerifyFOTARegistrationStatusCol_EqualOP() throws InterruptedException
	{
		Reporter.log("\n===========196.Verify FOTA Registration Status column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for FOTARegistrationStatus col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("FOTA Registration Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("FOTA Registration Status");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.FOTARegistration_Status);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for FOTARegistrationStatus col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for FOTARegistrationStatus col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingFOTARegistrationStatusCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-5,description="Verify FOTA Registration Status col with filter as NotEqualTo operator")
	public void VerifyFOTARegistrationStatusCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n==========197.Verify FOTA Registration Status col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for FOTARegistrationStatus col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("FOTA Registration Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("FOTA Registration Status");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.FOTARegistration_Status);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for FOTARegistrationStatus col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for FOTARegistrationStatus col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.FOTARegistration_Status);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-4,description="Verify Firmware Version column with filter as Equal operator")
	public void VerifyFirmwareVersionCol_EqualOP() throws InterruptedException
	{
		Reporter.log("============198.Verify Firmware Version column with filter as Equal operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for FirmwareVersion col with filter as Equal operator","test");
		customReports.ClickOnColumnInTable("Firmware Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Firmware Version");
		customReports.SelectOperatorFromDropDown("=");
		customReports.SendValueToTextfield(Config.Firmware_Version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for FirmwareVersion col with filter as Equal operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for FirmwareVersion col with filter as Equal operator");
	    reportsPage.WindowHandle();
	    customReports.VerifyingFirmwareVersionCol_EqualOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority=-3,description="Verify FirmwareVersion col with filter as NotEqualTo operator")
	public void VerifyFirmwareVersionCol_NotEqualToOP() throws InterruptedException
	{
		Reporter.log("\n============199.Verify FirmwareVersion col with filter as NotEqualTo operator=============",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("Report for FirmwareVersion col with filter as NOTEqualTo operator","test");
		customReports.ClickOnColumnInTable("Firmware Version");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Firmware Version");
		customReports.SelectOperatorFromDropDown("!=");
		customReports.SendValueToTextfield(Config.Firmware_Version);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("Report for FirmwareVersion col with filter as NOTEqualTo operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("Report for FirmwareVersion col with filter as NOTEqualTo operator");
	    reportsPage.WindowHandle();
	    customReports.ClickOnSearchReportButtonInsidedViewedReport(Config.Firmware_Version);
	    customReports.VerifyingReport_NotEqualToOp();
	    reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	
	//######################## Veifying all the single column with filter##############################//
	
			// bug is raised
		/*@Test(priority='B',description="Verify BatteryLevel column with filter")
		public void VerifyBatteryLevelColumn() throws InterruptedException
		{
			commonmethdpage.ClickOnHomePage();
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDetail Report for BatteryLevel col with filter","This is Custom report");
			customReports.ClickOnColumnInTable("Battery Level");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.Selectvaluefromdropdown("Device Details");
			customReports.SelectValueFromColumn("Battery Level");
			customReports.SelectOperatorFromDropDown("<=");
			customReports.SendValueToTextfield("50");
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("DevDetail Report for BatteryLevel col with filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("DevDetail Report for BatteryLevel col with filter");
		   	customReports.clickOnRefreshButton();
			customReports.ClickOnViewReportLinkButton();
		    reportsPage.WindowHandle();
		    //customReports.VerifyingDeviceModel();
		    reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
		
		@Test(priority='D',description="Verify  BatteryChemistry column with filter")
		public void VerifyBatteryChemistryColumn() throws InterruptedException
		{
			commonmethdpage.ClickOnHomePage();
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDetail Report for BatteryChemistry col with filter","This is Custom report");
			customReports.ClickOnColumnInTable("Battery Chemistry");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.Selectvaluefromdropdown("Device Details");
			customReports.SelectValueFromColumn("Battery Chemistry");
			customReports.SelectOperatorFromDropDown("=");
			customReports.SendValueToTextfield("Li-ion");
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("DevDetail Report for BatteryChemistry col with filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("DevDetail Report for BatteryChemistry col with filter");
		   	customReports.clickOnRefreshButton();
			customReports.ClickOnViewReportLinkButton();
		    reportsPage.WindowHandle();
		    customReports.ChooseDevicesPerPage();
		    customReports.VerifyingBatteryChemistryCol();
		    reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
		//bug raised
		@Test(priority='E',description="Verify  BackupBatteryPercent column with filter")
		public void VerifyBackupBatteryPercentColumn() throws InterruptedException
		{
			commonmethdpage.ClickOnHomePage();
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDetail Report For BackupBatteryPercentcol With Filter","This is Custom report");
			customReports.ClickOnColumnInTable("Backup Battery Percent");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.Selectvaluefromdropdown("Device Details");
			customReports.SelectValueFromColumn("Backup Battery Percent");
			customReports.SelectOperatorFromDropDown("=");
			customReports.SendValueToTextfield("NA");
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("DevDetail Report For BackupBatteryPercentcol With Filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("DevDetail Report For BackupBatteryPercentcol With Filter");
		   	customReports.clickOnRefreshButton();
			customReports.ClickOnViewReportLinkButton();
		    reportsPage.WindowHandle();
		    customReports.ChooseDevicesPerPage();
		    customReports.VerifyingBatteryChemistryCol();
		    reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	*/
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
			
			}
		}
	}
}
