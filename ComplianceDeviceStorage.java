package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ComplianceDeviceStorage extends Initialization {
	
	
	@Test(priority=1, description="Compliance job - Verify creating Compliance job with Device storage and out of Compliance action as Send Message.")
	public void TC_JO_879() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{


		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnComplianceJob();
		androidJOB.SelctingComplianceJobsRule("Device Storage");
		androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
		androidJOB.devicestorage_percent("90");
		androidJOB.outOfComplianceActions("Send Message",2,"DeviceStorage_BLK");
		androidJOB.EnterJobNameTextField("ComplianceJobDeviceStorageSendMessage");
		androidJOB.ClickOnSaveButtonJobWindow();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("ComplianceJobDeviceStorageSendMessage");
		androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceStorageSendMessage",100);
		Thread.sleep(200);
		androidJOB.VerifyTextMessageWhenDeviceIsOutOfComplianceDeviceStorage();
		inboxpage.ClickOnInbox();
		androidJOB.ClickOnFirstEmail();
		androidJOB.readFirstMsg();
		Reporter.log("Pass >> Compliance job - Verify creating Compliance job with Device storage and out of Compliance action as Send Message.",true);
		}



		
		
		
	
@Test(priority=2, description="Compliance job - Verify creating Compliance job with  Device storage and out of Compliance action as Email Notification")
public void TC_JO_883() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
	androidJOB.clickOnJobs();
	androidJOB.clickNewJob();
	androidJOB.clickOnAndroidOS();
	androidJOB.ClickOnComplianceJob();
	androidJOB.SelctingComplianceJobsRule("Device Storage");
	androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
	androidJOB.devicestorage_percent("90");
	androidJOB.outOfComplianceActions("E-Mail Notification",2,"DeviceStorage_BLK");
	androidJOB.EnterJobNameTextField("ComplianceJobDeviceStorageEmailNotifications");
	androidJOB.ClickOnSaveButtonJobWindow();
	commonmethdpage.ClickOnHomePage();
	androidJOB.SearchDeviceInconsole(Config.DeviceName); 
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("ComplianceJobDeviceStorageEmailNotifications");
	androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceStorageEmailNotifications",200);
	Thread.sleep(120);
	androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");
	Reporter.log("Pass >> Compliance job - Verify creating Compliance job with  Device storage and out of Compliance action as Email Notification",true); }

@Test(priority=3, description="Compliance job - Verify creating Compliance job with Device storage  and out of Compliance action as Move to Blocklist")
public void TC_JO_880() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{



	androidJOB.clickOnJobs();
	androidJOB.clickNewJob();
	androidJOB.clickOnAndroidOS();
	androidJOB.ClickOnComplianceJob();
	androidJOB.SelctingComplianceJobsRule("Device Storage");
	androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
	androidJOB.devicestorage_percent("90");
	androidJOB.outOfComplianceActions("Move to Blocklist",2,"DeviceStorage_BLK");
	androidJOB.EnterJobNameTextField("ComplianceJobDeviceStorageMoveToBlockList");
	androidJOB.ClickOnSaveButtonJobWindow();
	commonmethdpage.ClickOnHomePage();
	androidJOB.SearchDeviceInconsole(Config.DeviceName); 
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("ComplianceJobDeviceStorageMoveToBlockList");
	androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceStorageMoveToBlockList",100);
	Thread.sleep(200);
	blacklistedpage.ClickOnBlacklisted();
	blacklistedpage.ClickOnRefreshButton();
	blacklistedpage.ClickOnDeviceToWhiteList();
	blacklistedpage.ClickOnWhitelistButton();
	blacklistedpage.ClickOnYesButtonWarningDialogWhitelist();
	blacklistedpage.ClickOnHomeGroup();
	Reporter.log("Pass >> Compliance job - Verify creating Compliance job with Device storage  and out of Compliance action as Move to Blocklist",true);
	}


@Test(priority=4, description="Compliance job - Verify creating Compliance job with Device storage and out of Compliance action as Apply job.")
public void TC_JO_884() throws InterruptedException, IOException, AWTException, EncryptedDocumentException, InvalidFormatException{
	androidJOB.clickOnJobs();
	androidJOB.clickNewJob();
	androidJOB.clickOnAndroidOS();
	androidJOB.clickOnTextMessageJob();
	androidJOB.textMessageJobWhenNoFieldIsEmpty("JobCreatedForComplianceApplyJob","ComplianceJob","ComplianceJob");
	androidJOB.ClickOnForceReadMessage();
	androidJOB.ClickOnOkButton();
	androidJOB.clickNewJob();
	androidJOB.clickOnAndroidOS();
	androidJOB.ClickOnComplianceJob();
	androidJOB.SelctingComplianceJobsRule("Device Storage");
	androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
	androidJOB.devicestorage_percent("90");
	androidJOB.outOfComplianceActions("Apply Job",2,"DeviceStorage_BLK");
	androidJOB.addComplianceJob("DeviceStorage_BLK");
	androidJOB.SearchFieldComplianceJob("JobCreatedForComplianceApplyJob");
	androidJOB.EnterJobNameTextField("JobDeviceStorageApplyJob");
	androidJOB.ClickOnSaveButtonJobWindow();
	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

	commonmethdpage.ClickOnHomePage();
	androidJOB.SearchDeviceInconsole(Config.DeviceName); 
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("JobDeviceStorageApplyJob");
	androidJOB.CheckStatusOfappliedInstalledJob("JobDeviceStorageApplyJob",200);
	Thread.sleep(150);
	androidJOB.verifyComplianceApplyJob();
	Reporter.log("Pass >>Compliance job - Verify creating Compliance job with Device storage and out of Compliance action as Apply job.",true); }	



@Test(priority=5,description="Compliance job - Verify creating Compliance job with Device storage and out of Compliance action as Lock device")
public void TC_JO_882() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
{
	commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
	androidJOB.clickOnJobs();
	androidJOB.clickNewJob();
	androidJOB.clickOnAndroidOS();
	androidJOB.ClickOnComplianceJob();
	androidJOB.SendingComplianceJobName("Automation_DeviceStorageLockDeviceAction");
	androidJOB.SelctingComplianceJobsRule("Device Storage");
	androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
	androidJOB.devicestorage_percent("90");
	androidJOB.outOfComplianceActions("Lock Device (Android / iOS/iPadOS)");
	androidJOB.Delay_OutOfComplianceActions("Immediately");
	androidJOB.ClickOnComplianceJobSaveButton();
	commonmethdpage.ClickOnHomePage();
	commonmethdpage.SearchDevice(Config.DeviceName);
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("Automation_DeviceStorageLockDeviceAction"); 
	androidJOB.JobInitiatedMessage(); 
    androidJOB.CheckStatusOfappliedInstalledJob("Automation_DeviceStorageLockDeviceAction",600);
	androidJOB.VerifyIsDeviceLocked();
	dynamicjobspage.UnLockingDeviceUsingADB();

}
@Test(priority=6, description="Compliance job - Verify creating Compliance job with  Device storage and out of Compliance action as  Send Message and Send SMS")
public void TC_JO_886() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{


	commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
	androidJOB.clickOnJobs();
	androidJOB.clickNewJob();
	androidJOB.clickOnAndroidOS();
	androidJOB.ClickOnComplianceJob();
	androidJOB.SelctingComplianceJobsRule("Device Storage");
	androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
	androidJOB.devicestorage_percent("90");
	androidJOB.OutOFComplianceDeviceStorage("Send Message",1);
	androidJOB.Delay_OutOfComplianceActions("Immediately");
    androidJOB.ClickOnAddActionBtnDeviceStorage();
	androidJOB.OutOFComplianceDeviceStorage("Send Sms",2);
	androidJOB.Delay_OutOfComplianceActions("Immediately");

    androidJOB.SendingphoneNumber(Config.OutOfCompliancePhoneNumber);
    androidJOB.EnterJobNameTextField("ComplianceJobDeviceStorageSendMessage_SendSms");
	androidJOB.ClickOnSaveButtonJobWindow();
	commonmethdpage.ClickOnHomePage();
	androidJOB.SearchDeviceInconsole(Config.DeviceName); 
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("ComplianceJobDeviceStorageSendMessage_SendSms");
	androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceStorageSendMessage_SendSms",100);
	Thread.sleep(200);
	androidJOB.VerifyTextMessageWhenDeviceIsOutOfComplianceDeviceStorage();
	inboxpage.ClickOnInbox();
	androidJOB.ClickOnFirstEmail();
	androidJOB.readFirstMsg();
	Reporter.log("Pass >> Compliance job - Verify creating Compliance job with  Device storage and out of Compliance action as  Send Message and Send SMS",true);
	}



@Test(priority=7, description="Compliance job - Verify creating Compliance job with  Device storage and out of Compliance action as Send SMS")
public void TC_JO_885() throws Throwable{
	androidJOB.clickOnJobs();
	androidJOB.clickNewJob();
	androidJOB.clickOnAndroidOS();
	androidJOB.ClickOnComplianceJob();
	androidJOB.SelctingComplianceJobsRule("Device Storage");
	androidJOB.ClickOnConfigureButton("DeviceStorage_BLK");
	androidJOB.devicestorage_percent("90");
	androidJOB.outOfComplianceActions("Send Sms",2,"DeviceStorage_BLK");
	androidJOB.Delay_OutOfComplianceActions("Immediately");
    androidJOB.SendingphoneNumber(Config.OutOfCompliancePhoneNumber);
    androidJOB.EnterJobNameTextField("ComplianceJobDeviceStorageSendSms");
	androidJOB.ClickOnSaveButtonJobWindow();
	commonmethdpage.ClickOnHomePage();
	androidJOB.SearchDeviceInconsole(Config.DeviceName); 
	commonmethdpage.ClickOnApplyButton();
	androidJOB.SearchField("ComplianceJobDeviceStorageSendSms");
	androidJOB.CheckStatusOfappliedInstalledJob("ComplianceJobDeviceStorageSendSms",100);
	Reporter.log("Pass >>Compliance job - Verify creating Compliance job with  Device storage and out of Compliance action as Send SMS",true); }	
































}

		

