package OnDemandReports_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

// Pass 
// Config.GPSTestVersion, Config.SureMDMNix, 
public class InstalledJobReport extends Initialization {

	@Test(priority = 0, description = "Creating Job")
	public void CreatingJob() throws InterruptedException, IOException {
		Reporter.log("Creating Jobs", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.ClickOnInstallApplication();
//		androidJOB.enterJobName(Config.JobName);
//		androidJOB.clickOnAddInstallApplication();// androidJOB. androidJOB.
//		androidJOB.browseFileInstallApplication("./Uploads/UplaodFiles/Job On Android/InstallJobMultiple/\\File3.exe");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnAndroidJob();
//		androidJOB.UploadcompleteStatus();
//
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.clickOnNixUpgrade();
//		androidJOB.VerifyNixUpgrade();
//		androidJOB.enterNixAgentLink(Config.nixLatestAPK);
//		androidJOB.enterNixDevicePath();
//		androidJOB.nixUpgradeOkButton();
//		androidJOB.EmptyJobNameMessage();
//		androidJOB.enterJobNameNixUpgrade("NixUpgradeJob");
//		androidJOB.enterNixAgentLink(Config.nixLatestAPK);
//		androidJOB.enterNixDevicePath();
//		androidJOB.nixUpgradeOkButton();
//		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 1, description = "Generate And View the 'Installed Job Report'  for Selected 'Home' group")
	public void GenerateViewInstalledJobReportHomeGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n1.Generate And View the 'Installed Job Report'  for Selected 'Home' group", true);

		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.JobName);
		commonmethdpage.Selectjob(Config.JobName);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("GPS Test.apk", Config.DeviceName);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport(Config.JobName);
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.ApplicationName);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport(Config.JobName);
		reportsPage.GPSTestColumnVerificationInstalledJobReport(Config.GPSTestVersion);
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 2, description = "2.Verify of Job Status column in Installed Job Report")
	public void VerifyofJobStatusCol_Today()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n2.Verify of Job Status column in Installed Job Report",true);

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("NixUpgradeJob");
		commonmethdpage.Selectjob("NixUpgradeJob");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("NixUpgradeJob", Config.DeviceName);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport("NixUpgradeJob");
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.EnterSureMDMNix);
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport("NixUpgradeJob");
		reportsPage.verifyofJobScheduleTime();
		reportsPage.verifyofJobDeployedTime();
		reportsPage.verifyOfUserId(Config.userID);
		reportsPage.verifyOfSureMDMNix(Config.SureMDMNix);
		reportsPage.verifyOfSureMDMNixVR(Config.SureMDMNix);
		reportsPage.verifyOfUninstallSureMDMNix(Config.SureMDMNix);
		reportsPage.SwitchBackWindow();
	}
	
	@Test(priority = 3, description = "3.Generate And View the 'Installed Job Report'  for Selected 'Sub' group")
	public void GenerateViewInstalledJobReportSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n3.Generate And View the 'Installed Job Report'  for Selected 'Sub' group", true);

		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport(Config.JobName);
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.ApplicationName);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport(Config.JobName);
		reportsPage.GPSTestColumnVerificationInstalledJobReport(Config.GPSTestVersion);
		reportsPage.SwitchBackWindow();
	}

	

	@Test(priority = 4, description = "4.Verify of Job Schedule Time column in Installed Job Report")
	public void VerifyofJobScheduleTime_Yesterday() throws InterruptedException, ParseException {
		Reporter.log("\n4.Verify of Job Schedule Time column in Installed Job Report",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport("NixUpgradeJob");
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.EnterSureMDMNix);
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport("NixUpgradeJob");
		reportsPage.verifySelectedDateForYesterday_InstalledJobReport();

		reportsPage.verifyOfUserId(Config.userID);
		reportsPage.verifyOfSureMDMNix(Config.SureMDMNix);
		reportsPage.verifyOfSureMDMNixVR(Config.SureMDMNix);
		reportsPage.verifyOfUninstallSureMDMNix(Config.SureMDMNix);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 5, description = "5.Verify of Job Deployed Time column in Installed Job Report")
	public void VerifyofJobDeployedTime_Last1Week() throws InterruptedException, ParseException {
		Reporter.log("\n5.Verify of Job Deployed Time column in Installed Job Report",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport("NixUpgradeJob");
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.EnterSureMDMNix);
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport("NixUpgradeJob");
		reportsPage.verifySelectedDateForLast1week_InstalledJobReport();
		reportsPage.verifyOfUserId(Config.userID);
		reportsPage.verifyOfSureMDMNix(Config.SureMDMNix);
		reportsPage.verifyOfSureMDMNixVR(Config.SureMDMNix);
		reportsPage.verifyOfUninstallSureMDMNix(Config.SureMDMNix);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 6, description = "6.Verify of User ID column in Installed Job Report")
	public void VerifyofUserID_Last30Days() throws InterruptedException, ParseException {
		Reporter.log("\n6.Verify of User ID column in Installed Job Report",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport("NixUpgradeJob");
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.EnterSureMDMNix);
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport("NixUpgradeJob");

		reportsPage.verifySelectedDateForLast30Days_InstalledJobReport();
		reportsPage.verifyOfUserId(Config.userID);
		reportsPage.verifyOfSureMDMNix(Config.SureMDMNix);
		reportsPage.verifyOfSureMDMNixVR(Config.SureMDMNix);
		reportsPage.verifyOfUninstallSureMDMNix(Config.SureMDMNix);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 7, description = "7.Verify of SureMDM Nix column in Installed Job Report")
	public void VerifyofSureMDMNix_ThisMonth() throws InterruptedException, ParseException {
		Reporter.log("\n7.Verify of SureMDM Nix column in Installed Job Report",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport("NixUpgradeJob");
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.EnterSureMDMNix);
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport("NixUpgradeJob");

		reportsPage.verifySelectedDateForThisMonth_InstalledJobReport();
		reportsPage.verifyOfUserId(Config.userID);
		reportsPage.verifyOfSureMDMNix(Config.SureMDMNix);
		reportsPage.verifyOfSureMDMNixVR(Config.SureMDMNix);
		reportsPage.verifyOfUninstallSureMDMNix(Config.SureMDMNix);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 8, description = "8.Verify of SureMDM Nix VR column in Installed Job Report")
	public void VerifyofSureMDMNixVR_CustomRange() throws InterruptedException, ParseException {
		Reporter.log("\n8.Verify of SureMDM Nix VR column in Installed Job Report",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange_InstalledJob();
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport("NixUpgradeJob");
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.EnterSureMDMNix);
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport("NixUpgradeJob");

		reportsPage.verifySelectedDateForCustomRange_InstalledJobReport();
		reportsPage.verifyOfUserId(Config.userID);
		reportsPage.verifyOfSureMDMNix(Config.SureMDMNix);
		reportsPage.verifyOfSureMDMNixVR(Config.SureMDMNix);
		reportsPage.verifyOfUninstallSureMDMNix(Config.SureMDMNix);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 9, description = "9.Verify of Uninstall SureMDM Nix column in Installed Job Report")
	public void VerifyofUninstallSureMDMNix() throws InterruptedException {
		Reporter.log("\n9.Verify of Uninstall SureMDM Nix column in Installed Job Report",true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport("NixUpgradeJob");
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.EnterSureMDMNix);
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyInstallJobReportColumns();
		reportsPage.SearchDeviceInReportsView(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceHealthReport(Config.DeviceName); // common
		reportsPage.JobNameColumnVerificationInstalledJobReport("NixUpgradeJob");
		reportsPage.verifyofJobScheduleTime();
		reportsPage.verifyofJobDeployedTime();
		reportsPage.verifyOfUserId(Config.userID);
		reportsPage.verifyOfSureMDMNix(Config.SureMDMNix);
		reportsPage.verifyOfSureMDMNixVR(Config.SureMDMNix);
		reportsPage.verifyOfUninstallSureMDMNix(Config.SureMDMNix);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 10, description = "10.Download for Installed Job Report")
	public void VerifyDownloadingDeviceActivityReport() throws InterruptedException {
		Reporter.log("\n10.Download and Verify the data for Installed Job Report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickOnInstallJobReport();
		reportsPage.ClickOnSelectJobOption();
		reportsPage.SearchJobInReport(Config.JobName);
		reportsPage.SelectSearchedJobInReport();
		reportsPage.ClickOnOkButtonSelectJobToAddInReport();
		reportsPage.EnterApplicationNameInstalledJobReport(Config.ApplicationName);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();

		}
	}

}
