package Settings_TestScripts;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;
import Common.Initialization;

public class ThingsLicenses extends Initialization{

	@Test(priority=1,description="Verify things license used field is displaying in Settings dropdown.") 
	public void VerifythingslicenseusedfieldisdisplayinginSettingsdropdownTC_ST_487() throws Throwable {
		Reporter.log("\n1.Verify things license used field is displaying in Settings dropdown.",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ThingsLicensesUsedOptionVisible();
		accountsettingspage.ThingsLicensesCount();				
}
	
	
	//set precondition as 
	//String url = "https://worldoftesting.eu.suremdm.io/console/"; String userID ="abdulemithilesh@gmail.com"; String password = "Mithilesh@123";
//	@Test(priority=2,description="Verify things license for the Trial Account") 
//	public void VerifythingslicensefortheTrialAccountTC_ST_488() throws Throwable {
//		Reporter.log("\n2.Verify things license for the Trial Account",true);
//		
//		//accountsettingspage.CloseWindow();
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ThingsLicensesUsedOptionVisible();
//		accountsettingspage.ThingsLicensesCount();				
//}
	
}
