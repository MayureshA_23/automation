package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;
/*
 * Fake GPS (com.lexa.fakegps) and astrocontactapk in App Store
 */
//JobInitiatedMessage()
public class InstallApplication extends Initialization {

	// Two Testcases are merged
	@Test(priority = 1, description = "Install Application- Verify all options in the install job."+ "Install Application - Verify creating install job with all the details and Apply to the device.")

	public void VerifyJobsPage() throws InterruptedException, IOException {
		androidJOB.clickOnJobs();
		androidJOB.verifyElementsOnJobsPage();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.verifyElementsOnCreateJobPage();
		androidJOB.ClickOnInstallApplication();
		Reporter.log("Pass >>Install Application- Verify all options in the install job."+"Install Application - Verify creating install job with all the details and Apply to the device.",true);
	}

	@Test(priority = 2, description = "Install Application - Verify creating and deploying single install job with URL (Silent install will work on Knox,rooted and signed devices.)")
	public void VerifyJobWithURL() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.ClickOnInstallApplication();

		androidJOB.enterJobName(Config.EnterJobWithApkUrl);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL(Config.EnterApkUrl);
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.clickOkBtnOnAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDeviceInconsole();
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfInCompleteJobsInJobQueue("In Progress Job",2,600);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfJobsInJobQueue("Completed Job", 4);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfFailedJobsInJobQueue("Failed Job", 3);
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.EnterJobWithApkUrl);
		androidJOB.JobInitiatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnterJobWithApkUrl,360);
		androidJOB.CheckApplicationIsInstalledDeviceSide(Config.BundleID);
		androidJOB.unInstallMultipleApplication(Config.BundleID);
		Reporter.log("Pass >>Install Application - Verify creating and deploying single install job with URL (Silent install will work on Knox,rooted and signed devices.)",true);
	}

	@Test(priority=3,description="Install Application - Verify creating and deploying multiple install job with Multiple APK URL'S(Silent install should work on Knox, rooted and signed devices)" )
	public void VerifyJobWithMultipleURL() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs(); 
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS(); 
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName("EnterJobWithMulipleApkUrl");
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL(Config.EnterMultipleApkUrl);
		androidJOB.clickOkBtnOnIstallJob(); 
		androidJOB.clickOkBtnOnAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDeviceInconsole();
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfInCompleteJobsInJobQueue("In Progress Job",2,600);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfJobsInJobQueue("Completed Job", 4);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfFailedJobsInJobQueue("Failed Job", 3);
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnterJobWithMulipleApkUrl");
		androidJOB.JobInitiatedMessage();
		androidJOB.CheckStatusOfappliedInstalledJob("EnterJobWithMulipleApkUrl",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckApplicationsInstalledDeviceSide("com.gears42.astrocontacts,com.gears42.surelock");
		androidJOB.unInstallMultipleApplication(Config.MultipleBundleID); 
		Reporter.log("Pass >>Install Application - Verify creating and deploying multiple install job with Multiple APK URL'S(Silent install should work on Knox, rooted and signed devices)",true); }


/*	@Test(priority=4,description="Verify Install job- File path/URL-with special character in the URL" )
	public void VerifyJobWithSplCharURL() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs(); 
		 androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS(); 
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName("EnterJobWithSplCharUrl");
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL("https://mars.42gears.com/support/inout/qa/sur*efo&xv7.86.apk");
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage(); 
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDeviceInconsole();
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfInCompleteJobsInJobQueue("In Progress Job",2,600);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfJobsInJobQueue("Completed Job", 4);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfFailedJobsInJobQueue("Failed Job", 3);
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("EnterJobWithSplCharUrl");
		androidJOB.JobInitiatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("EnterJobWithSplCharUrl",600);
		androidJOB.CheckApplicationsInstalledDeviceSide("com.gears42.surefox");
		androidJOB.unInstallMultipleApplication("com.gears42.surefox"); 
		Reporter.log("Pass >>Verify Install job- File path/URL-with special character in the URL",true); }
*/
	@Test(priority=6,description="Verify Install job- File path/URL-when Install after copy is checked") 
	public void VerifyJobCopyIsChecked() throws InterruptedException,IOException, EncryptedDocumentException, InvalidFormatException{ 
		androidJOB.clickOnJobs();  
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS(); 
		androidJOB.ClickOnInstallApplication(); 
		androidJOB.enterJobName(Config.EnterJobNameWithApkUrlCopyClick); 
		androidJOB.clickOnAddInstallApplication(); 
		androidJOB.enterFilePathURL(Config.EnterApkUrl); 
		androidJOB.clickOkBtnOnIstallJob(); 
		androidJOB.clickOkBtnOnAndroidJob();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDeviceInconsole();
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfInCompleteJobsInJobQueue("In Progress Job",2,600);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfJobsInJobQueue("Completed Job", 4);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfFailedJobsInJobQueue("Failed Job", 3);
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.EnterJobNameWithApkUrlCopyClick);
		androidJOB.JobInitiatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnterJobNameWithApkUrlCopyClick,500); 
		androidJOB.CheckApplicationIsInstalledDeviceSide(Config.BundleID); 
		androidJOB.unInstallApplication(Config.BundleID);
		Reporter.log("Pass >>Verify Install job- File path/URL-when Install after copy is checked" ); }

	@Test(priority=7,description="Verify Install job- File path/URL-when Install after copy is unchecked")
	public void VerifyJobCopyisUnChecked() throws InterruptedException,IOException, EncryptedDocumentException, InvalidFormatException{ 
		androidJOB.clickOnJobs();  
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication(); 
		androidJOB.enterJobName(Config.EnterJobNameWithApkUrlCopyUnchecked); 
		androidJOB.clickOnAddInstallApplication(); 
		androidJOB.enterFilePathURL(Config.EnterApkUrl); 
		androidJOB.unCheckInstallCopy();  androidJOB.clickOkBtnOnIstallJob(); 
		androidJOB.clickOkBtnOnAndroidJob(); 
		androidJOB.JobCreatedMessage(); 
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDeviceInconsole();
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfInCompleteJobsInJobQueue("In Progress Job",2,600);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfJobsInJobQueue("Completed Job", 4);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfFailedJobsInJobQueue("Failed Job", 3);
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.EnterJobNameWithApkUrlCopyUnchecked);
		androidJOB.JobInitiatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnterJobNameWithApkUrlCopyUnchecked,360); 
		androidJOB.ClickOnHomeButtonDeviceSide(); 
		androidJOB.CheckApplicationIsNotInstalledDeviceSide(Config.BundleID); 
		Reporter.log("Pass >>Verify Install job- File path/URL-when Install after copy is unchecked" ); }

	@Test(priority=8,description="Install Application - Verify install job with Use Apps from Appstore option.") 
	public void VerifyinstallJobUseAppsfromAppstore() throws
	InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{ 
		androidJOB.clickOnJobs(); 
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS(); 
		androidJOB.ClickOnInstallApplication(); 
		androidJOB.enterJobName(Config.EnterJobWNameUseAppsfromAppstore); 
		androidJOB.clickOnAddInstallApplication(); 
		androidJOB.clickOnUseAppsFromAppStoreCheckBox();
		androidJOB.SelectAppFromAppsDropDown("Fake GPS (com.lexa.fakegps)"); 
		androidJOB.clickOkBtnOnIstallJob(); 
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage(); 
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDeviceInconsole();
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfInCompleteJobsInJobQueue("In Progress Job",2,600);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfJobsInJobQueue("Completed Job", 4);
		androidJOB.clickonJobQueue();
		androidJOB.GettingInitialCountOfFailedJobsInJobQueue("Failed Job", 3);
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.EnterJobWNameUseAppsfromAppstore);
		androidJOB.JobInitiatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnterJobWNameUseAppsfromAppstore,600); 
		androidJOB.CheckApplicationIsInstalledDeviceSide("com.lexa.fakegps"); 
		androidJOB.unInstallApplication("com.lexa.fakegps");
		Reporter.log("Pass >>Install Application - Verify install job with Use Apps from Appstore option." ,true); }

	@Test(priority=9, description="Install Application - Verify creating and deploying single install job with Apk file(Silent install will work on Knox,rooted and signed devices.)") 
	public void VerifyJobWithAPKFile() throws InterruptedException,IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();  
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS(); 
		androidJOB.ClickOnInstallApplication(); 
		androidJOB.enterJobName(Config.EnterJobWNameFileApk); 
		androidJOB.clickOnAddInstallApplication(); 
		androidJOB.browseAPK("./Uploads/apk/\\FileAPK.exe"); 
		androidJOB.clickOkBtnOnIstallJob(); 
		androidJOB.clickOkBtnOnAndroidJob();
		commonmethdpage.ClickOnHomePage(); 
		androidJOB.SearchDeviceInconsole(Config.DeviceName); 
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.EnterJobWNameFileApk); 
		androidJOB.JobInitiatedMessage(); 
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnterJobWNameFileApk,600); 
		androidJOB.CheckApplicationIsInstalledDeviceSide("com.lexa.fakegps"); 
		androidJOB.unInstallApplication("com.lexa.fakegps");
		Reporter.log("Pass >>Install Application - Verify creating and deploying single install job with URL (Silent install will work on Knox,rooted and signed devices.)" ,true); } 

	@Test(priority = 'A', description = "Install Application - Verify creating and deploying multiple install job with Apk files.(Silent install should work on Knox, rooted and signed devices)")
	public void VerifyCreateDeployMultipleAPK() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		androidJOB.clickOnJobs();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName("CreateDeployMultipleAPK");
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseMultipleAPK();
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
//		androidJOB.UploadcompleteStatus();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("CreateDeployMultipleAPK");
		androidJOB.JobInitiatedMessage();
		androidJOB.CheckStatusOfappliedInstalledJob("CreateDeployMultipleAPK", 900);
		androidJOB.CheckApplicationsInstalledDeviceSide("fit.cult.android,com.myntra.android");
		androidJOB.unInstallMultipleApplication("fit.cult.android,com.myntra.android");
		Reporter.log("Pass >>Install Application - Verify creating and deploying multiple install job with Apk files.(Silent install should work on Knox, rooted and signed devices)",true);
	}

	@Test(priority = 'B', description = "Install Application - Verify advanced settings option in Install job with App version filter with conditions (=,>=,<=)")
	public void VerifyAdvanceSettingAppVersion() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		androidJOB.clickOnJobs();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.EnterJobWNameAdvanceSetting);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.clickOnUseAppsFromAppStoreCheckBox();
		androidJOB.SelectAppFromAppsDropDown("AstroContacts (com.gears42.astrocontacts)");
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.clickOnAdvancedSettingBtn();
		androidJOB.selectAndroidlogic(Config.greaterThanEquals);
		androidJOB.selectAndroidNum(Config.Andriodversion);
		androidJOB.selectappName(Config.appName);
		androidJOB.selectappCompOperator(Config.greaterThanEquals);
		androidJOB.selectappVersion(Config.appversion);
		androidJOB.clickOnDoneAdvancedSettingsBtn();
		androidJOB.verifyAdvancedSettingsMsg();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.EnterJobWNameAdvanceSetting);
		androidJOB.JobInitiatedMessage();
		androidJOB.CheckStatusOfappliedInstalledJob(Config.EnterJobWNameAdvanceSetting, 600);
		androidJOB.CheckApplicationIsInstalledDeviceSide(Config.BundleID);
		Reporter.log("Pass >>Install Application - Verify advanced settings option in Install job with App version filter with conditions (=,>=,<=)",true);
	}

	// Merged three cases
	@Test(priority = 'C', description = "Install Application - Verify Moveup/MoveDown in install job when multiple APKS are added."
			+ "Install Application - Verify delete in Install job."
			+ "Install Application - Verify modify option in Install job.")

	public void VerifyMoveUpMoveDown() throws InterruptedException, IOException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName("JobMoveupMoveDown");
		androidJOB.clickOnAddInstallApplication();
		androidJOB.enterFilePathURL(Config.EnterMultipleApkUrl);
		androidJOB.clickOkBtnOnIstallJob();
		androidJOB.selectAPKOnConfigurationJob();
		androidJOB.verifyMoveDown();
		androidJOB.verifyMoveUp();
		androidJOB.verifyModifyOption();
		androidJOB.verifyDeleteInInstallJob();
		commonmethdpage.refreshpage();

		Reporter.log("Pass >>Install Application - Verify advanced settings option in Install job with App version filter with conditions (=,>=,<=)"
				+ "Install Application - Verify delete in Install job."
				+ "Install Application - Verify modify option in Install job.",
				true);
	}
	    
	 
	
	   @Test(priority='D',description="Verify job deployed status on plugged in charging state")
	    public void VerifyJobDeploymentStatusONPluggedInChargingState() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	    {
	    	androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnInstallApplication();
			androidJOB.enterJobName(Config.InstallJObName);   
			androidJOB.clickOnAddInstallApplication();
			androidJOB.browseFileInstallApplication("./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\File1.exe");
			androidJOB.clickOkBtnOnBrowseFileWindow();
			androidJOB.clickOkBtnOnAndroidJob();
//			androidJOB.UploadcompleteStatus();
			commonmethdpage.ClickOnHomePage();
			commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchJob(Config.InstallJObName);
			androidJOB.SelectingChargingStateInApplyJob("Plugged In");
			androidJOB.ClickOnApplyJobOkButton();
			quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
			androidJOB.clickonJobQueue();
			androidJOB.VerifyChargingPluggeStatusInsidJobQueue("Plugged In");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("surevideo.apk",Config.DeviceName);
			Reporter.log("Job Is Deployed Successfully As The Device Is Plugged In");
	    }
	    @Test(priority='E',description="Verify Job deploy on Don't care state in charging state")
	    public void VerifyJobDeploymentStatusONDontCareChargingState() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	    {
	    	commonmethdpage.SearchDevice(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
	    	androidJOB.SearchJob(Config.InstallJObName);
			androidJOB.SelectingChargingStateInApplyJob("Don't Care");
			androidJOB.ClickOnApplyJobOkButton();
			quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
			androidJOB.clickonJobQueue();
			androidJOB.VerifyChargingPluggeStatusInsidJobQueue("Any");
			accountsettingspage.ReadingConsoleMessageForJobDeployment("surevideo.apk",Config.DeviceName);
			Reporter.log("Job Is Deployed Successfully As The Charging State Was Don't Care");
	    }
	    @Test(priority='F',description="Verify device charging status with combination of other platform device")
	    public void VerifyDeviceChargeStatusWithCombinationOfOtherPlatfomDevice() throws InterruptedException, AWTException
	    {
	    	commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.WindowsDeviceName);
			quickactiontoolbarpage.SelectingMultipleDevices();
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchJob(Config.InstallJObName);
			androidJOB.VerifyDeviceChargingStateOptionInApplyJob();
			quickactiontoolbarpage.ClickOnApplyJobCloseBtn();
	    }
/*		@Test(priority ='G', description = "Install Job - Verify Saving install and file transfer job with foreign key.")
		public void VerifyFileTransferforeignKeyTC_JO_844() throws InterruptedException, IOException, ParseException, EncryptedDocumentException, InvalidFormatException {

			
			
			androidJOB.clickOnJobs();  
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
			androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS(); 
			androidJOB.ClickOnInstallApplication(); 
			androidJOB.enterJobName("##InstallJobWithForeignKey$$"); 
			androidJOB.clickOnAddInstallApplication(); 
			androidJOB.browseAPK("./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\File5.exe"); 
			androidJOB.EnterDevicePath("/sdcard/Download");

			androidJOB.clickOkBtnOnIstallJob(); 
			androidJOB.clickOkBtnOnAndroidJob();
			commonmethdpage.ClickOnHomePage(); 
			androidJOB.SearchDeviceInconsole(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("##InstallJobWithForeignKey$$"); 
			androidJOB.JobInitiatedOnDevice(); 
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
			androidJOB.CheckStatusOfappliedInstalledJob("##InstallJobWithForeignKey$$",360);
			commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");

			androidJOB.verifyFolderIsDownloadedInDevice("@@Fake GPS location##.apk");
			androidJOB.ClickOnHomeButtonDeviceSide();
			androidJOB.CheckApplicationIsInstalledDeviceSide("com.lexa.fakegps"); 

			androidJOB.unInstallApplication("com.lexa.fakegps");

			Reporter.log("File Transfer - Verify Saving install and file transfer job with foreign character.",true);


		}
*/
	    @Test(priority='H',description="Verify Pushing application upgrades from the SureMDM")
	    public void PushingLowerVersionApp() throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException{
	    	
	    	
			androidJOB.clickOnJobs(); 
			androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS(); 
			androidJOB.ClickOnInstallApplication(); 
			androidJOB.enterJobName(Config.NameOfJob); 
			androidJOB.clickOnAddInstallApplication(); 
			androidJOB.browseAPK("./Uploads/apk/\\FileTouch.exe"); 
			androidJOB.clickOkBtnOnIstallJob(); 
			androidJOB.clickOkBtnOnAndroidJob();
			commonmethdpage.ClickOnHomePage(); 
			androidJOB.SearchDeviceInconsole(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField(Config.NameOfJob); 
			androidJOB.JobInitiatedOnDevice(); 
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
			androidJOB.CheckStatusOfappliedInstalledJob(Config.NameOfJob,360);
			androidJOB.CheckApplicationIsInstalledDeviceSide(Config.PackageNameInstallJobApp); 
			commonmethdpage.ClickOnAllDevicesButton();
			commonmethdpage.SearchDevice(Config.DeviceName);
			dynamicjobspage.ClickOnApps();
			dynamicjobspage.EnterSearchOfDownloadedApps(Config.AppName);
			dynamicjobspage.VerifyLowerVersionOfApp(Config.LowerVersion);
			dynamicjobspage.CloseDynamicApp();



	    	
	       }	
	    @Test(priority='I',description="Verify Pushing application upgrades from the SureMDM")
	    public void PushingUpgradeVersionApp() throws InterruptedException, AWTException, IOException, EncryptedDocumentException, InvalidFormatException{
	    	
	    	
			androidJOB.clickOnJobs(); 
			androidJOB.clickNewJob(); 
			androidJOB.clickOnAndroidOS(); 
			androidJOB.ClickOnInstallApplication(); 
			androidJOB.enterJobName("UpgradedVersionApp"); 
			androidJOB.clickOnAddInstallApplication(); 
			androidJOB.browseAPK("./Uploads/apk/\\JTouchHighVersion.exe"); 
			androidJOB.clickOkBtnOnIstallJob(); 
			androidJOB.clickOkBtnOnAndroidJob();
			commonmethdpage.ClickOnHomePage(); 
			androidJOB.SearchDeviceInconsole(Config.DeviceName);
			commonmethdpage.ClickOnApplyButton();
			androidJOB.SearchField("UpgradedVersionApp"); 
			androidJOB.JobInitiatedOnDevice(); 
			commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
			androidJOB.CheckStatusOfappliedInstalledJob("UpgradedVersionApp",360);
			androidJOB.CheckApplicationIsInstalledDeviceSide(Config.PackageNameInstallJobApp); 
			commonmethdpage.ClickOnAllDevicesButton();
			commonmethdpage.SearchDevice(Config.DeviceName);
			dynamicjobspage.ClickOnApps();
			dynamicjobspage.EnterSearchOfDownloadedApps(Config.AppName);
			dynamicjobspage.VerifyUpgradedVersionOfApp(Config.LowerVersion,Config.UpgradedVersion);
			dynamicjobspage.CloseDynamicApp();

	    	
	    	
	    	
	    }
	    @Test(priority='J',description="Verify created jobs shows correct size.")
	    public void VerifyCreatedJobsShowsCorrectSize() throws Throwable{

	    	
			androidJOB.clickOnJobs();
			androidJOB.clickNewJob();
			androidJOB.clickOnAndroidOS();
			androidJOB.ClickOnInstallApplication(); 
			androidJOB.enterJobName(Config.EnterJobWNameFileApk); 
			androidJOB.clickOnAddInstallApplication(); 
			androidJOB.browseAPK("./Uploads/UplaodFiles/FileStore/\\Game.exe"); 
			androidJOB.clickOkBtnOnIstallJob(); 
			androidJOB.clickOkBtnOnAndroidJob();
		    androidJOB.JobCreatedMessage();
			androidJOB.UploadcompleteStatus();
			androidJOB.CheckFileSize("40.236 MB");
			commonmethdpage.ClickOnHomePage(); 


	    	
	 
	    
}
	    
	    
}
