package QuickActionToolBar_TestScripts;


import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Locate_MasterTestCase extends Initialization
{
	//Multiple Devices Should Be Enrolled And ScreenShots should be seen in PassedCasesScreenshots for TC 7&8
	//Pass DownloadPath in Config as per your local machine download path
	
    @Test(priority=1,description="Verify locate in Quick Action Tool Bar.")	
    public void VerifyingLocateInQAT() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	Reporter.log("1.Verify locate in Quick Action Tool Bar.",true);
    	commonmethdpage.SearchDevice(Config.DeviceName);
    	quickactiontoolbarpage.CheckingLocationTrackingStatus();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.ClickingOnTurnOnButton();
    	quickactiontoolbarpage.TurningOnLocationTracking("2");
    	quickactiontoolbarpage.WaitingForUpdate(180);
    	commonmethdpage.SearchDevice(Config.MultipleDevices1);
    	quickactiontoolbarpage.CheckingLocationTrackingStatus();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.ClickingOnTurnOnButton();
    	quickactiontoolbarpage.TurningOnLocationTracking("1");
    }
    @Test(priority=2,description="Verify real time location tracking of the device.")
    public void VerifyRealTimeLocationTracking() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	Reporter.log("\n2.Verify real time location tracking of the device.",true);
    	commonmethdpage.SearchDevice(Config.DeviceName);
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.VerifyingDeviceNameInRealTimeLocationTracking(Config.DeviceName);
    	quickactiontoolbarpage.VerifyingDeviceAddressInRealTimeLocationTracking(Config.LoctionTrackingAddress,Config.PinCode);
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();
    }
    @Test(priority=3,description="Verify Export button for history in location tracking.")
    public void VerifyExportButtonInLocationTracking() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
    {
    	Reporter.log("\n3.Verify Export button for history in location tracking.",true);
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.ClickingonLocationTrackingHistoryTab();
    	quickactiontoolbarpage.ClickingOnExportButton(Config.downloadpath);
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();
    }
     @Test(priority=4,description="Verify History in Location tracking.")
    public void VerifyHistoryInLocationTracking() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	 Reporter.log("\n4.Verify History in Location tracking.",true);
    	 commonmethdpage.SearchDevice(Config.DeviceName);
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.ClickingonLocationTrackingHistoryTab();
    	quickactiontoolbarpage.ClickingOnOpenHistoryTogglerButton();
    	quickactiontoolbarpage.VerifyLocationHistroy();
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();    	
    }

    
    @Test(priority=6,description="Verify Clear button for history in location tracking.")
    public void VerifyingClearHistoryButton() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	Reporter.log("\n6.Verify Clear button for history in location tracking.",true);
    	commonmethdpage.SearchDevice(Config.DeviceName);  //Should be removed
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.ClickingonLocationTrackingHistoryTab();
    	quickactiontoolbarpage.ClickingOnClearHistoryButton();
    	quickactiontoolbarpage.ClickingOnClearLocationHistoryConfirmationOKButton();
    	quickactiontoolbarpage.VerifyingHistoryTabAfetrClearingHistory();
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();    	
    }
    
    @Test(priority=5,description="Verify Location tracking for multiple devices.")
    public void VerifyLocationTrackingForMultipleDevices() throws InterruptedException, AWTException
    {
    	Reporter.log("\n5.Verify Location tracking for multiple devices.",true);
    	commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
    	quickactiontoolbarpage.SelectingMultipleDevices();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.VerifyingMultipleDevicesInsideLocationTrackingPage();
    	quickactiontoolbarpage.VerifyingDeviceAddressInRealTimeLocationTracking(Config.LoctionTrackingAddress,Config.PinCode);
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();    	

    }
    
    @Test(priority=7,description="Verify search the device in device list .")
    public void VerifySearchDeviceInLocationTrackingDeviceList() throws InterruptedException, AWTException
    {
    	Reporter.log("\7.Verify search the device in device list .",true);
    	commonmethdpage.SearchForMultipleDevice(Config.DeviceName,Config.MultipleDevices1);
    	quickactiontoolbarpage.SelectingMultipleDevices();
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.VerifyingMultipleDevicesInsideLocationTrackingPage();
    	quickactiontoolbarpage.SearchingForDeviceInLocationTrackingPage(Config.DeviceName);
    	quickactiontoolbarpage.VerifyingDeviceNameInRealTimeLocationTracking(Config.DeviceName);
    	quickactiontoolbarpage.VerifyingDeviceAddressInRealTimeLocationTracking(Config.LoctionTrackingAddress,Config.PinCode);
    	quickactiontoolbarpage.SearchingForDeviceInLocationTrackingPage(Config.MultipleDevices1);
    	quickactiontoolbarpage.VerifyingDeviceNameInRealTimeLocationTracking(Config.MultipleDevices1);
    	quickactiontoolbarpage.VerifyingDeviceAddressInRealTimeLocationTracking(Config.LoctionTrackingAddress,Config.PinCode);
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();    	

    }
    
    @Test(priority=8,description="Verify '+' and '-' buttons in location tracking page.")
    public void VerifyZoomInZoomOutButtons() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	Reporter.log("\n8.Verify '+' and '-' buttons in location tracking page.",true);
    	commonmethdpage.SearchDevice(Config.DeviceName); 
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.ClickOnZoomInButton();
    	quickactiontoolbarpage.ClickOnZoomOutButton();
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();    	

    }
    
    @Test(priority=9,description="Verify Satellite view in location tracking.")
    public void VerifySatelliteViewInLocationTracking() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	Reporter.log("\n9.Verify Satellite view in location tracking.",true);
    	commonmethdpage.SearchDevice(Config.DeviceName); 
    	quickactiontoolbarpage.ClickOnLocate_UM();
    	quickactiontoolbarpage.windowhandles();
    	quickactiontoolbarpage.SwitchToLocateWindow();
    	quickactiontoolbarpage.TakingScreenShotOfNormalMap();
    	quickactiontoolbarpage.ClickOnSatelliteButtonInLocationTrackingPage();
    	quickactiontoolbarpage.ClosingLoctionTrackingWindow();   
    	deviceGrid.VerifyLocationTrackingIconWhenLocationIsAvailable();
    	quickactiontoolbarpage.CheckingLocationTrackingStatus();
    }
    
    @AfterMethod
    public void CollectDeviceLogs(ITestResult result) throws InterruptedException
   	{
   		if(ITestResult.FAILURE==result.getStatus())
   		{
   			try
   			{
   				String FailedWindow = Initialization.driver.getWindowHandle();	
   				if(FailedWindow.equals(quickactiontoolbarpage.PARENTWINDOW))
   				{
   				    commonmethdpage.ClickOnHomePage();
   				}
   				else
   				{
   					quickactiontoolbarpage.ClosingLoctionTrackingWindow();
   				}
   			} 
   			catch (Exception e) 
   			{
   				
   			}
   		}
   	
       
      }
	
    
   }
