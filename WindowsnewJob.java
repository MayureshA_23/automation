package JobsOnAndroid;
import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Library.Config;
import Common.Initialization;

public class WindowsnewJob extends Initialization{
	
//	@Test(priority=1,description="UI Validation of Proxy Settings job in windows")
//	public void UIValidationofProxySettingsjobinwindowsTC_JO_723() throws InterruptedException {
//		Reporter.log("\n1.UI Validation of Proxy Settings job in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnProxySettings();
//		androidJOB.verifyOptionInProxySettings();
//	}
	
//	@Test(priority=2,description="Verify Enable proxy in Proxy settings job in windows")
//	public void VerifyEnableproxyinProxysettingsjobinwindowsTC_JO_722() throws InterruptedException {
//		Reporter.log("\n2.Verify Enable proxy in Proxy settings job in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnProxySettings();
//		androidJOB.EnableProxySettingscheckbox();
//		androidJOB.CloseProxySettingTab();
//		commonmethdpage.ClickOnHomePage();	
//	}
	
	
//	@Test(priority=3,description="Verify Proxy Settings with proxy type as Manual in windows")
//	public void VerifyProxySettingswithproxytypeasManualinwindowsTC_JO_724() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n3.Verify Proxy Settings with proxy type as Manual in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnProxySettings();
//		androidJOB.ProxySettingsManualJobName();
//		androidJOB.EnableProxySettingscheckbox();
//		androidJOB.ProxyTypeDropDown();
//		androidJOB.ProxyServersEntered();
//		androidJOB.ProxyPortEntered();
//		androidJOB.ProxySettingsSaveButton();
//		androidJOB.JobCreatedMessage();			
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.ProxypolicyDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Proxy Settings Manual Job"); 
//		androidJOB.WindowsActivityLog();		
//	}
	

//	@Test(priority=4,description="Verify Proxy Settings with proxy type as Auto and with proxy URL in windows")
//	public void VerifyProxySettingswithproxytypeasAutoandwithproxyURLinwindowsTC_JO_725() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n4.Verify Proxy Settings with proxy type as Auto and with proxy URL in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnProxySettings();		
//		androidJOB.ProxySettingsAutoJobName();		
//		androidJOB.EnableProxySettingscheckbox();
//		androidJOB.ProxyTypeAutoDropDown();		
//		androidJOB.ProxyPACURLEntered();		
//		androidJOB.ProxySettingsSaveButton();
//		androidJOB.JobCreatedMessage();			
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.ProxypolicyDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Proxy Settings Auto Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=5,description="Verify modifying Proxy Settings job in windows")
//	public void VerifymodifyingProxySettingsjobinwindowsTC_JO_726() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n5.Verify modifying Proxy Settings job in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnProxySettings();		
//		androidJOB.ProxySettingsModifiedJobName();
//		androidJOB.EnableProxySettingscheckbox();
//		androidJOB.ProxyTypeDropDown();
//		androidJOB.ProxyServersEntered();
//		androidJOB.ProxyPortEntered();
//		androidJOB.ProxySettingsSaveButton();
//		androidJOB.JobCreatedMessage();			
//		androidJOB.SearchTextBoxModifiedJob("Trials Proxy Settings Modified Job");
//		androidJOB.ClickonModifiedbutton();
//		androidJOB.ProxySettingsModifiedJobName();
//		androidJOB.ProxySettingsSaveButton();
//		androidJOB.JobModifiedMessage();				
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.ProxypolicyDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Proxy Settings Modified Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=6,description="Verify Creating Wifi with Security type as Open and Auto connect in windows")
//	public void VerifyCreatingWifiwithSecuritytypeasOpenandAutoconnectinwindowsTC_JO_713() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n6.Verify Creating Wifi with Security type as Open and Auto connect in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();
//		androidJOB.WiFiConfigurationSettingsJobName();
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();
//		androidJOB.SecurityTypeDropDown();
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();
//		androidJOB.WiFiConfigurationSettingsOKButton();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Configuration Settings Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
	
//	@Test(priority=7,description="Verify Creating Wifi with Security type as Open and Auto connect in windows")
//	public void VerifyCreatingWifiwithSecuritytypeasOpenandAutoconnectinwindowsTC_JO_713() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n7.Verify Creating Wifi with Security type as Open and Auto connect in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();
//		androidJOB.WiFiConfigurationSettingsWEPJobName();
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();		
//		androidJOB.SecurityTypeWEPDropDown();		
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();
//		androidJOB.WiFiConfigurationSettingsOKButton();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Configuration Settings WEP Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=8,description="Verify Creating Wi-Fi settings job with security type as WPA2-Personal (AES) and Auto connect in windows")
//	public void VerifyCreatingWiFisettingsjobwithsecuritytypeasWPA2PersonalandAutoconnectinwindowsTC_JO_715() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n8.Verify Creating Wi-Fi settings job with security type as WPA2-Personal (AES) and Auto connect in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();		
//		androidJOB.WiFiConfigurationSettingsWPA2PersonalJobName();		
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();			
//		androidJOB.SecurityTypeWPA2PersonalDropDown();		
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();
//		androidJOB.WiFiConfigurationSettingsOKButton();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Settings WPA2Personal Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=9,description="Verify Creating Wi-Fi settings job with security type as WPA2-Enterprise (AES - PEAP) and Auto connect in windows")
//	public void VerifyCreatingWiFisettingsjobwithsecuritytypeasWPA2EnterpriseandAutoconnectinwindowsTC_JO_716() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n9.Verify Creating Wi-Fi settings job with security type as WPA2-Enterprise (AES - PEAP) and Auto connect in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();		
//		androidJOB.WiFiConfigurationSettingsWPA2EnterpriseJobName();		
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();			
//		androidJOB.SecurityTypeWPA2EnterpriseDropDown();		
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();
//		androidJOB.WiFiConfigurationSettingsOKButton();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Settings WPA2Enterprise Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=10,description="Verify Wi-Fi Settings job when Hidden Network is enabled in windows")
//	public void VerifyWiFiSettingsjobwhenHiddenNetworkisenabledinwindowsTC_JO_717() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n10.Verify Wi-Fi Settings job when Hidden Network is enabled in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();		
//		androidJOB.WiFiConfigurationHiddenNetworkJobName();		
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();			
//		androidJOB.SecurityTypeHiddenNetworkDropDown();		
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();
//		androidJOB.EnableWiFiConfigurationSettingsHiddenNetwork();		
//		androidJOB.WiFiConfigurationSettingsOKButton();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Hidden Network Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
	
//	@Test(priority=11,description="Verify disabling Auto connect in Wi-Fi settings job in windows")
//	public void VerifydisablingAutoconnectinWiFisettingsjobinwindowsTC_JO_718() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n11.Verify disabling Auto connect in Wi-Fi settings job in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();		
//		androidJOB.WiFiConfigurationDisableAutoConnectJobName();			
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();		
//		androidJOB.SecurityTypeDisableAutoConnectDropDown();			
//		androidJOB.DisableAutoConnectcheckbox();		
//		androidJOB.WiFiConfigurationSettingsOKButton();		
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Disable Auto Connect Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
	
//	@Test(priority=12,description="Verify Modify in WiFi Settings job in windows")
//	public void VerifyModifyinWiFiSettingsjobinwindowsTC_JO_719() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n12.Verify Modify in WiFi Settings job in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();		
//		androidJOB.WiFiConfigurationModifiedJobName();		
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();			
//		androidJOB.SecurityTypeModifiedDropDown();		
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();		
//		androidJOB.WiFiConfigurationSettingsOKButton();			
//		androidJOB.JobCreatedMessage();			
//		androidJOB.SearchTextBoxModifiedJob("Trials WiFi Modified Job");
//		androidJOB.ClickonModifiedbutton();
//		androidJOB.WiFiConfigurationModifiedJobName();
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();	
//		androidJOB.WiFiConfigurationOKButton();
//		androidJOB.JobModifiedMessage();					
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Modified Job"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=13,description="Verify delete in WiFi Settings job in windows")
//	public void VerifydeleteinWiFiSettingsjobinwindowsTC_JO_720() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n13.Verify delete in WiFi Settings job in windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();		
//		androidJOB.WiFiConfigurationDeleteJobName();		
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();			
//		androidJOB.SecurityTypeModifiedDropDown();		
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();		
//		androidJOB.WiFiConfigurationSettingsOKButton();			
//		androidJOB.JobCreatedMessage();			
//		androidJOB.SearchTextBoxModifiedJob("Trials WiFi Delete Job");		
//		androidJOB.ClickonDeletebutton();		
//		androidJOB.JobDeletedSuccessfully();					
//		commonmethdpage.ClickOnHomePage();	
//}
	
	
//	@Test(priority=14,description="Verify Modifying and deploy the WiFi Settings job in windows device")
//	public void VerifyModifyinganddeploytheWiFiSettingsjobinwindowsdeviceTC_JO_721() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n14.Verify Modifying and deploy the WiFi Settings job in windows device", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.WiFiSettingsVisible();
//		androidJOB.WiFiConfigurationModifiedJobName();		
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();			
//		androidJOB.SecurityTypeModifiedDropDown();		
//		androidJOB.EnableWiFiConfigurationSettingsAutoConnect();		
//		androidJOB.WiFiConfigurationSettingsOKButton();			
//		androidJOB.JobCreatedMessage();			
//		androidJOB.SearchTextBoxModifiedJob("Trials WiFi Modified Job");
//		androidJOB.ClickonModifiedbutton();
//		androidJOB.WiFiConfigurationModifiedJobName();
//		androidJOB.WiFiConfigurationSettingsADDButton();
//		androidJOB.WiFiConfigurationSettingsSSIDName();	
//		androidJOB.WiFiConfigurationOKButton();
//		androidJOB.JobModifiedMessage();					
//		commonmethdpage.ClickOnHomePage();	
//		androidJOB.SearchDeviceInconsole(Config.WiFIConfigurationDevice);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials WiFi Modified Job"); 
//		androidJOB.WindowsActivityLog();					
//}
		
//	@Test(priority =15,description ="UI Validation of Time Fence job in Windows")
//	public void UIValidationofTimeFencejobinWindowsTC_JO_702() throws InterruptedException {
//		Reporter.log("\n15.UI Validation of Time Fence job in Windows", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnTimeFencejob();
//		androidJOB.verifyOptionsInFenceInTimeFencejob();
//	}
//
//	@Test(priority =16,description = "UI Validation of 'Select Fence' in Time fence job in Windows")
//	public void UIValidationofSelectFenceinTimefencejobinWindowsTC_JO_703() throws InterruptedException {
//		Reporter.log("\n16.UI Validation of 'Select Fence' in Time fence job.", true);
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnWindowsOS();
//		androidJOB.clickOnTimeFencejob();
//		androidJOB.clickOnSelectFence();
//		androidJOB.verifySelectFenceOptions();
//	}

	@Test(priority =17,description = "UI Validation of 'Select Fence' in Time fence job in Windows")
	public void UIValidationofSelectFenceinTimefencejobinWindowsTC_JO_703() throws InterruptedException {
		Reporter.log("\n17.UI Validation of 'Select Fence' in Time fence job.", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnWindowsOS();
		androidJOB.clickOnTimeFencejob();		
		androidJOB.clickOnSelectFence();
		
//		deviceinfopanelpage.ClickOnEnableTimeFence(false);
//		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1");
//		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2");
//		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();
		
		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyTimeFenceone);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyTimeFenceone);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("JobNameToApplyTimeFence");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
