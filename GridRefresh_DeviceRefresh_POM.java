package PageObjectRepository;


import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.WebDriverCommonLib;

public class GridRefresh_DeviceRefresh_POM extends WebDriverCommonLib
{
	AssertLib ALib=new AssertLib();
     @FindBy(xpath="//div[@id='main_div_id']/div[3]/div[4]/div[1]/div[1]/button")  
     private WebElement GridRefreshButton;
     
     @FindBy(id="refreshButton")
     private WebElement DeviceRefreshButton;
     
     @FindBy(xpath="//i[@class='st-jobStatus dev_st_icn good fa fa-check-circle']")
     private WebElement JobSuccessIndicator;
     
     @FindBy(xpath="//i[@class='st-jobStatus dev_st_icn poor fa fa-exclamation-circle']")
     private WebElement JobErrorIndicator;
     
     public void clicOnGridRefresh() throws InterruptedException
     {
    	 GridRefreshButton.click();
    	 waitForXpathPresent("//div[@id='deleteDeviceBtn']");
    	 sleep(4);
     }
     public void clickOnDeviceRefresh() throws InterruptedException
     {
    	 DeviceRefreshButton.click();
    	 sleep(25);
     }

 	public void CheckStatusOfappliedJob1(int DeploymentTime,String Jobname) throws InterruptedException
 	{
 		WebDriverWait wait3 = new WebDriverWait(Initialization.driver,DeploymentTime); 
 		 boolean bool = wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//i[@class='st-jobStatus dev_st_icn good fa fa-check-circle' or @class='st-jobStatus dev_st_icn poor fa fa-exclamation-circle']"))).isDisplayed();
 		 try
 		 {
 		 
 			if(JobSuccessIndicator.isEnabled())
 			{
 				Reporter.log("Main Job Deployed Successfully On The Device",true);
 				Reporter.log("Clicking on job Queue",true);
 				Initialization.driver.findElement(By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']")).click();
 				sleep(2);
 				waitForidPresent("jobqueuetitle");
 				sleep(2);
// 				Initialization.driver.findElement(By.xpath("//*[@id=\"jobQueueDataGrid\"]/tbody/tr/td/p[text()='"+Jobname+"']")).click();
 				Initialization.driver.findElement(By.xpath("//*[@id='jobQueueDataGrid']/tbody/tr[1]/td/p/i[@class='fa fa-arrow-circle-o-right jobqueue_expandclass']")).click();


 				sleep(3);
 				ArrayList<WebElement> List =(ArrayList<WebElement>) Initialization.driver.findElements(By.xpath("//*[@id='subjobQueueDataGrid']/tbody/tr/td[5]"));
 				for(WebElement e:List )
 				{
 					System.out.println(e.getText()); 				
 					if(e.getText().equals("Error"))
 					{
 						String FailMessage="Main job status is Deployed sub job Status is Error";
 						ALib.AssertFailMethod(FailMessage);
 					}
 					else if(e.getText().equals("In progress"))
 					{
 						String FailMessage="Main job status is Deployed sub job Status is Inprogressr";
 						ALib.AssertFailMethod(FailMessage);
 					}
 					else
 					{
 					
 						Reporter.log("Sub Job staus is Deployed",true);
 					}
 					
 				}
 				sleep(2);
 				Initialization.driver.findElement(By.xpath("//*[@id=\"subjobQueueModel\"]/div/div/div[1]/button")).click();
 				sleep(3);
 				Initialization.driver.findElement(By.xpath("//*[@id=\"device_jobQ_modal\"]/div[1]/button")).click();
 				sleep(2);
 				
 			}	
 		 }
 		 catch(Throwable t)
 		 {
 			 if(JobErrorIndicator.isDisplayed())
 				
 				{
 					Reporter.log("Job remains in Error Status",true);
 					System.out.println("clicking On Main Job Status");
 					Initialization.driver.findElement(By.xpath("//i[@class='st-jobStatus dev_st_icn poor fa fa-exclamation-circle']")).click();
 					waitForXpathPresent("//h4[@id='jobqueuetitle']");
 					sleep(3);
 					System.out.println("Clicking On JobQueue");
 					Initialization.driver.findElement(By.xpath("(//i[@class='fa fa-arrow-circle-o-right jobqueue_expandclass'])[1]")).click();
 					waitForXpathPresent("//*[@id='subjobQueueModel']/div/div/div[1]/h4");
 					sleep(4);
 					System.out.println("Reading The Status Of Jobs in Sub Job Queue");
 					List<WebElement> List1 = Initialization.driver.findElements(By.xpath("//*[@id='subjobQueueDataGrid']/tbody/tr/td[5]"));
 					for(WebElement el:List1)
 					{
 						String JobStatus = el.getText();
 						if(JobStatus.equals("Error"))
 						{
 							String StatusMessage = Initialization.driver.findElement(By.xpath("//*[@id='subjobQueueDataGrid']//p[contains(text(),'Cannot') or contains(text(),'Failed')]")).getText();
 						    Reporter.log("Job Status Message is"+" "+StatusMessage,true);
 						}
 					}
 					Initialization.driver.findElement(By.xpath("//*[@id=\"subjobQueueModel\"]/div/div/div[1]/button")).click();
 					sleep(3);
 					Initialization.driver.findElement(By.xpath("//*[@id=\"device_jobQ_modal\"]/div[1]/button")).click();
 					sleep(2);
 					String FailMessage="Job Remains In Error Status";
 					ALib.AssertFailMethod(FailMessage);
 					
 				}
 		 }
 	
 	}
 	
     
     
}
