package PageObjectRepository;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class ActivityLog_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(xpath="//div[@id='logContainer']")
	private WebElement ActivityLog;
	
	public boolean CheckActivityLog()
	{
		boolean value;
		try {
			ActivityLog.isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		return value;
	}
	@FindBy(xpath=".//*[@id='logContent']/ul/li/p")
	private WebElement LogContainer;
	@FindBy(xpath=".//*[@id='logsOperationMenu']/li/span[text()='Clear Log']")
	private WebElement ClearLog;
	@FindBy(xpath=".//*[@id='logsOperationMenu']/li/span[text()='Export']")
	private WebElement Export;
	public void RightclickClearLog() throws InterruptedException
	{
		 Actions act = new Actions(Initialization.driver);
		 act.moveToElement(LogContainer).contextClick(LogContainer).perform();
		 sleep(2);
		 ClearLog.click();
		 sleep(4);
	}
	@FindBy(xpath=".//*[@id='logContainer']/div[2]/button")
	private WebElement LogsMenu;
	@FindBy(xpath=".//*[@id='logsOperationMenu']/li[1]")
	private WebElement filterOption;
	@FindBy(xpath=".//input[@id='enable_logfilter']")
	private WebElement EnableLogFilter;
	@FindBy(xpath=".//*[@id='logFilter_deviceName']")
	private WebElement DeviceNameFilter;
	@FindBy(xpath=".//*[@id='LogFilter_okbtn']")
	private WebElement OkButtonFilter;
	@FindBy(id="logFilter_jobName")
	private WebElement jobname;
	
	public void FilterCondition(String DeviceName, String JobName) throws InterruptedException
	{
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(LogContainer).contextClick(LogContainer).perform();
	    sleep(2);
		filterOption.click();
		waitForidPresent("logsFilter_modal");
		sleep(2);
		EnableLogFilter.click();
		DeviceNameFilter.sendKeys(DeviceName);
		jobname.sendKeys(JobName);
	}
	
	public void ClickOnOkButtonLogFilter() throws InterruptedException
	{
		OkButtonFilter.click();
		waitForidPresent("logContainer");
		sleep(5);
	}
	
	@FindBy(xpath=".//*[@id='logContent']/ul/li[1]/p")
	private WebElement FirstLog;
	public void CheckForFiltercondition() throws InterruptedException
	{
	
		String LogContain = FirstLog.getText();
		System.out.println(LogContain);
		sleep(2);
		boolean value = LogContain.contains("Job("+Config.Job1+") applied to device "+Config.DeviceName+"");
		String PassStatement3 = "PASS >> Filter is working";
	    String FailStatement3 = "FAIL >> Filter condition not working";
	    ALib.AssertTrueMethod(value, PassStatement3, FailStatement3);
	    sleep(4);
	}
	
	public void LogClearVerification(String user) throws InterruptedException
	{
	String LogContain = FirstLog.getText();
	System.out.println(LogContain);
	sleep(2);
	boolean value = LogContain.contains("Log cleared by "+user+"");
	String PassStatement3 = "PASS >> Filter is working";
    String FailStatement3 = "FAIL >> Filter condition not working";
    ALib.AssertTrueMethod(value, PassStatement3, FailStatement3);
    sleep(4);
	}
	
	public void ExportLog() throws InterruptedException
	{
		 Actions act = new Actions(Initialization.driver);
		 act.moveToElement(LogContainer).contextClick(LogContainer).perform();
		 sleep(2);
		 Export.click();
		 sleep(4);
	}
	@FindBy(xpath="//*[@id='logContainer']/div[2]/button")
	private WebElement LogMenu;
	public void FilterMenu(String DeviceName, String JobName) throws InterruptedException
	{
		LogMenu.click();
		sleep(3);
		filterOption.click();
		waitForidPresent("logsFilter_modal");
		sleep(2);
		DeviceNameFilter.clear();
		DeviceNameFilter.sendKeys(DeviceName);
		jobname.clear();
		jobname.sendKeys(JobName);
	}

	
	
}

















