package SanityTestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Groups_TestScripts extends Initialization{
	@Test(priority=0,description="Verify delting the Group")
	public void DeletingGroup() throws InterruptedException
	{
		Reporter.log("=====1.Verify delting the Group=====",true);
		
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName("Test@123");
		groupspage.ClickOnNewGroupOkBtn();
		groupspage.ClickOnDeleteGroup();
		groupspage.Verifydeletegroup();
	}
	@Test(priority=1,description="verify applying job to group.")
	public void VerifyOfApplyJobToGroup() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("=====2.verify applying job to group.=====",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName("NixSingleFileTransfer");
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe");
		androidJOB.EnterDevicePath("/sdcard/Download");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage(); 
		
		commonmethdpage.ClickOnHomePage();
		groupspage.ClickOnHomeGrup();
		groupspage.ClickOnNewGroup();
		groupspage.EnterGroupName(Config.GroupName);
		groupspage.ClickOnNewGroupOkBtn();
		rightclickDevicegrid.VerifyGroupPresence("dontouch");
		MacOSDeviceInfo.ClickOnAllDeviceTab();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.ClickingOnMoveToGroup(quickactiontoolbarpage.DeviceName);
		quickactiontoolbarpage.MovingDeviceToGroup(Config.GroupName,Config.DeviceName);
		quickactiontoolbarpage.ClickOnGroup_ApplyButton();
		androidJOB.SearchField("NixSingleFileTransfer");
		quickactiontoolbarpage.ClickOnApplyJobYesButton_Group();
		
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("FileTransfer.jpg",Config.DeviceName);
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("NixSingleFileTransfer",200);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();
		
	}
}
