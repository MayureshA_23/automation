package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ErrorMessagesUserName_PwdNewCases extends Initialization {
	// Demouser10 should be deleted
	// Delte Role ALLRolespermissions1

	@Test(priority = '1', description = "1.Verify the error message when password does not meet the requirment complexity while creating the user.")
	public void TC_ST_632() throws InterruptedException {
		Reporter.log(
				"\n1.Verify the error message when password does not meet the requirment complexity while creating the user.",
				true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.RoleWithAllPermissions("ALLRolespermissions1", "user Roles"); // ROLES
		usermanagement.ClickOnUserTab();
		usermanagement.VerifyCreateUserWraningMessageWhenPwdIsWrong("Demouser5", "42gears", "ALLRolespermissions1");
	}

	@Test(priority = '2', description = "2.Verify password field while adding user")
	public void TC_ST_634() throws InterruptedException {
		Reporter.log("\n2.Verify password field while adding user", true);

		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.VerifyWraningMessageWhenPwdIsWrongInConfirmTexfield("Demo", "42Gears@123", "42Gears",
				"ALLRolespermissions1");
		usermanagement.VerifyWraningMessageWhenPwdIsCorrectInConfirmTexfield("42Gears@123");

	}

	@Test(priority = '3', description = "1.Verify password field while editing the  user password.")
	public void TC_ST_635() throws InterruptedException {
		Reporter.log("\n3.Verify password field while editing the  user password.", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnRolesOption();
		usermanagement.CreateDemoUser1("Demouser10", "42Gears@123", "42Gears@123", "ALLRolespermissions1");
		usermanagement.SearchUserTextFieldDemouser1("Demouser10");
		usermanagement.VerifyResettingWrongPwd_confirm_pwd_textbox("Demouser10", "42Gears@123", "42Gears");
		usermanagement.VerifyResettingCorrectPwd_confirm_pwd_textbox("42Gears@123");
		usermanagement.ClickOnCloseResetPasswordButton();

	}

	@Test(priority = '4', description = "1.Verify Error message for username field while creating the user in usermanagement when username field is left blank.")
	public void TC_ST_637() throws InterruptedException {
		Reporter.log(
				"\n4.Verify Error message for username field while creating the user in usermanagement when username field is left blank.",
				true);

		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.VerifyErrorMessageForUsernamefield("42Gears@123");

	}

}