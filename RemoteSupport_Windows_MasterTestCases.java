package RemoteSupport_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;


//NotePad should not be opened  
public class RemoteSupport_Windows_MasterTestCases extends Initialization
{
	
	@Test(priority=0,description="PreCondtions For Windows Remote Support")
	public void PreConditions() throws InterruptedException
	{
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
 		accountsettingspage.ClickOnMiscellaneousSettings();
 		remotesupportpage.UncheckingZipAllDowlaodCheckBox();
 		remotesupportpage.UncheckingDontPauseScreenCheckBox();
 		remotesupportpage.ClickingOnAccSettingsApply();
 		commonmethdpage.ClickOnHomePage();
 		
	}
	
	  @Test(priority='1',description="Verify Remote support UI for Windows device .")
    public void VerifyingWindwsRemoteSupportWindowUI() throws Throwable
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
    	remotesupportpage.VerifyOfRemoteSupportwindow_Windows();
    	remotesupportpage.SwitchToMainWindow();	
    }
    
    @Test(priority='2',description="Verify online status on remote screen for Windows device .")
    public void VerifyOnlinrStatusOnRemoteScreen() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
    	remotesupportpage.VerifyWindowsDeviceOnlineStatus();
    	remotesupportpage.SwitchToMainWindow();
    	
    }
   
    @Test(priority='3',description="Verify functionality of Play button on remote support for Windows device .")
    public void VerifuFunctionalityOfPlayButton() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
    	remotesupportpage.ClickingOnPlayPauseButton();
    	remotesupportpage.VerifyPlayPauseSymbol();
		remotesupportpage.VerifyScreenAfterClickingPauseButton();
    }
    
    @Test(priority='4',description="Verify functionality of Play button on remote screen for Windows device .")
    public void VerifyFunctionalityOfpauseButton() throws InterruptedException
    {
    	remotesupportpage.ClickingOnPlayPauseButton();
    	remotesupportpage.VerifyPlayPauseSymbol();
    	remotesupportpage.VerifyRemoteScreenAfterClickingOnPlayButton();
    }
    
    @Test(priority='5',description="Verify functionality back button on the files tab on remote support for Windows device .")
    public void VerifyFunctionalityOfBackButton() throws Throwable
    {
    	remotesupportpage.ClickingOnCDrive();
    	String FolderName="TestFolder";
		remotesupportpage.RenameNewFolder(FolderName);
		remotesupportpage.DoubleClickOperation_Windows("TestFolder");
		remotesupportpage.ClickOnBack_Windows();
		remotesupportpage.SwitchToMainWindow();	
    }
    
    @Test(priority='6',description="Verify functionality of upload button under the files tab on remote support for Windows device .")
    public void VerifyUploadButtonInRemoteSupport() throws Throwable
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnCDrive();
    	remotesupportpage.ClickingOnUploadButtonInRemoteSupport();
		remotesupportpage.ClickingOnFileBrowseButton();
		remotesupportpage.UploadingFileInRemoteSupport("./Uploads/UplaodFiles/Remote Support/File3.exe");
		remotesupportpage.VerifyUploadStatusAfetrUploadingTheFile();
		remotesupportpage.SwitchToMainWindow();	
		
    }
    
    @Test(priority='7',description="Verify upload cancel functionality .")
    public void VerifyingUploadCancelButton() throws Throwable
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnCDrive();
    	remotesupportpage.ClickingOnUploadButtonInRemoteSupport();
		remotesupportpage.ClickingOnFileBrowseButton();
		remotesupportpage.UploadingFileInRemoteSupport("./Uploads/UplaodFiles/Remote Support/File3.exe");
		remotesupportpage.ClickingOnUploadCancelButton();
    }
    
 
    
    @Test(priority='8',description="Verify functionality of view button under the files tab on remote support for Windows device .")
    public void VerifyFunctionalityofViewButton() throws Throwable
    {
    	remotesupportpage.ClickOnIconView();
        remotesupportpage.VerifyOfIconViewColumn();
        remotesupportpage.VerifyViewAsIconButtonInRemoteSupport();
        remotesupportpage.SwitchToMainWindow();
    }
    
    @Test(priority='9',description="Verify the Remote device screen capture by pause manually for Windows device")
    public void VerifyScreenPauseByClickingOnPause() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
    	remotesupportpage.ClickingOnPlayPauseButton();
    	remotesupportpage.VerifyScreenAfterClickingPauseButton();
    }
    
    @Test(priority='A',description="Verify the Remote device screen capture by Play manually for Windows device.")
    public void VerifyScreenResumeByClickingOnPlay() throws InterruptedException
    {
    	remotesupportpage.ClickingOnPlayPauseButton();
    	remotesupportpage.VerifyRemoteScreenAfterClickingOnPlayButton();
    	remotesupportpage.SwitchToMainWindow();

    }
    
    @Test(priority='B',description="Verify the Remote device screen capture by resume manually for Windows device.")
    public void VerifyScreenResumeByClickingResume() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
    	remotesupportpage.ClickingOnPlayPauseButton();
    	remotesupportpage.ClickingOnResumeScreenCaptureButton();
    	remotesupportpage.SwitchToMainWindow();
    }
    
    @Test(priority='C',description="Verify the Remote device screen capture by pause with idle time out for Windows device")
    public void VerifyScreenPauseWithIdealTimeOut() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.SwitchToMainWindow();
    }
    
    @Test(priority='D',description="Verify the Remote device screen capture by resume from ideal time out for Windows device .")
    public void VerifyScreenResumeWithIdealTimeOutbyClickingResume() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.ClickingOnResumeScreenCaptureButton();
		remotesupportpage.SwitchToMainWindow();		
    }
    
    @Test(priority='E',description="Verify the Remote device screen capture by play from ideal time out for Windows device .")
    public void VerifyRemoteDeviceCaptureByPlayFromIdealTimeOut() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.CheckingDontPauseScreenCheckBox();
		remotesupportpage.ClickingOnResumeScreenCaptureButton();
		remotesupportpage.WaitingForScreenPauseMessageAferClickingOnResumeButton();
		remotesupportpage.SwitchToMainWindow();		
    }
    
    @Test(priority='F',description="Verify the Remote device screen capture by closing the remote support tab and validate capture screen for Windows device .")
    public void VerifyRemoteSupportScreenByClosingRemoteSupport() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.CheckingDontPauseScreenCheckBox();
		remotesupportpage.ClickingOnResumeScreenCaptureButton();
		remotesupportpage.SwitchToMainWindow();		
		commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.WaitingForScreenPauseMessage();
		remotesupportpage.SwitchToMainWindow();		
    }
    
    @Test(priority='G',description="Verify device name on remote support window for Windows device .")
    public void VerifyDeviceNameInRemoteSupport() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.DeviceNameVerification(Config.Windows_DeviceName1);
    }
    
    @Test(priority='H',description="Verify downloaded screenshot should be on .webp format")
    public void VerifyDownloadedScreenShot() throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
    {
    	remotesupportpage.CheckScreenshots("screenshot.webp",Config.downloadpath);
		remotesupportpage.SwitchToMainWindow();	
    }
    
    @Test(priority='I',description="Verify functionality of download button under the files tab on remote support for Windows device .")
    public void VerifyingDownloadButton() throws Throwable
    {
        commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnCDrive();
    	remotesupportpage.ClickOnFile("user.xlsx");
    	remotesupportpage.VeirfyFileDownloadingInWindowsRemoteSuppport("user.xlsx",Config.downloadpath);
    	remotesupportpage.SwitchToMainWindow();
    }
    
    @Test(priority='J',description="Verify task manager tab for Windows device .")
    public void VerifyTaskManagerTab() throws Throwable
    {
    	commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.LaunchingNotePadApplication();
		remotesupportpage.ClickOnTaskManagerTab();
		remotesupportpage.ClickOnRefreshList();
		remotesupportpage.VerifyCurrentRunningTaskInsideRemoteSupportTaskManager("notepad.exe");
    }
    
    @Test(priority='K',description="Verify functionality of kill process in task manger for Windows device .")
    public void VerifyKillProcessInTaskManager() throws Throwable
    {
   
    	remotesupportpage.ClickOnApplication("notepad.exe");
    	remotesupportpage.ClickOnKillProcess();
    	remotesupportpage.VerifyKillProcessTaskInsideRemoteSupportTaskManager("notepad.exe");
    }
    
    @Test(priority='L',description="19.To Verify Remote support - Send To Device ClipBoard")
	public void RemoteSupport_TCJ() throws Throwable{
		Reporter.log("\n19.To Verify Remote support - Send To Device ClipBoard",true);
		//remotesupportpage.ClickOnReload();
        remotesupportpage.ClickOnClipBoardTab();
        remotesupportpage.ClearClipBoardTextbox();
        remotesupportpage.EnterClipBoardTextbox();
        remotesupportpage.ClickOnSendToDeviceClipboard();
        Reporter.log("PASS >> Verification of Remote support - Send To Device ClipBoard",true);
	}
	
    
    @Test(priority='N',description="To Verify Remote support - Clear from Device ClipBoard")
	public void VerifyClearFromDeviceClipBoard() throws Throwable
    {
		remotesupportpage.ClickOnClipBoardTab();
		remotesupportpage.ClearClipBoardTextbox();
		remotesupportpage.EnterClipBoardTextbox();
        remotesupportpage.ClickOnSendToDeviceClipboard();
        remotesupportpage.ClickOnClearFromDeviceClipboard();
        remotesupportpage.VerifyOfClearFromDeviceClipBoard();
        remotesupportpage.ClickOnReadfromClipboard();
        remotesupportpage.VerifyOfClearFromDeviceClipBoard();
        remotesupportpage.SwitchToMainWindow();
        
	}
    
    @Test(priority='O',description="12.To Verify Remote support - Delete Popup when clicked on Cancel Button")
	public void RemoteSupport_TCC() throws Throwable
    {
		Reporter.log("\n12.To Verify Remote support - Delete Popup when clicked on Cancel Button",true);
		commonmethdpage.SearchDevice(Config.Windows_DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnCDrive();
        remotesupportpage.ClickOnFile(Config.DownloadFile1Name_Windows_Underscore);
        remotesupportpage.ClickOnDelete();
        remotesupportpage.VerifyOfDeleteFileHeader();
        remotesupportpage.VerifyOfCancelButton_DeleteFile();
        remotesupportpage.VerifyOfOkButton_DeleteFile();
        remotesupportpage.VerifyOfDeleteSummary();
        remotesupportpage.ClickOnCancelButton_DeleteFile();
        remotesupportpage.VerifyOfDeleteFile_True(Config.DownloadFile1Name_Windows_Underscore);
		Reporter.log("PASS >> Verification of Remote support - Delete Popup when clicked on Cancel Button",true);
	}
	
	@Test(priority='P',description="13.To Verify Remote support - Delete File")
	public void RemoteSupport_TCD() throws Throwable{
		Reporter.log("\n13.To Verify Remote support - Delete File",true);
        remotesupportpage.ClickOnFile(Config.DownloadFile1Name_Windows_Underscore);
        remotesupportpage.ClickOnDelete();
        remotesupportpage.VerifyOfDeleteFileHeader();
        remotesupportpage.VerifyOfCancelButton_DeleteFile();
        remotesupportpage.VerifyOfOkButton_DeleteFile();
        remotesupportpage.VerifyOfDeleteSummary();
        remotesupportpage.ClickOnOkButton_DeleteFile();
        remotesupportpage.VerifyOfDeleteFile_False(Config.DownloadFile1Name_Windows_Underscore);
        Reporter.log("PASS >> Verification of Remote support - Delete File",true);
	}
	
	@Test(priority='Q',description="14.To Verify Remote support - Delete Folder")
	public void RemoteSupport_TCE() throws Throwable{
		Reporter.log("\n14.To Verify Remote support - Delete Folder",true);
        remotesupportpage.ClickOnFile("TestFolder");
        remotesupportpage.ClickOnDelete();
        remotesupportpage.VerifyOfDeleteFileHeader();
        remotesupportpage.VerifyOfCancelButton_DeleteFile();
        remotesupportpage.VerifyOfOkButton_DeleteFile();
        remotesupportpage.VerifyOfDeleteSummary();
        remotesupportpage.ClickOnOkButton_DeleteFile();
        remotesupportpage.VerifyOfDeleteFile_False("TestFolder");
		Reporter.log("PASS >> Verification of Remote support - Delete Folder",true);
	}
	

    
    @AfterMethod
    public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(remotesupportpage.PARENTWINDOW))
				{
				    commonmethdpage.ClickOnHomePage();
				}
				else
				{
					remotesupportpage.SwitchToMainWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}

    
    
}
