package Settings_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class Help extends Initialization  {
	
	@Test(priority=1,description="1.To Verify Help window launch")
	public void VerifyLaunchingHelpWindow() throws InterruptedException{
		Reporter.log("\n1.To Verify Help window Open in New Tab",true);
		commonmethdpage.ClickOnSettings();
		helpPage.ClickOnHelp();
		helpPage.VerifyCommunity();
		accountsettingspage.CloseHelpTab();
}
	
	@Test(priority=2,description="Verify Help section in sureMDM console") 
	public void VerifyHelpsectioninsureMDMconsoleTC_ST_700() throws Throwable {
		Reporter.log("\n2.Verify Help section in sureMDM console",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonHelpButton();
	    accountsettingspage.HelpOptionsVisible();
	    accountsettingspage.CloseHelpTab();
	    commonmethdpage.ClickOnSettings();
}
	
	@Test(priority=3,description="Verify community link in sureMDM console Help section") 
	public void VerifycommunitylinkinsureMDMconsoleHelpsectionTC_ST_701() throws Throwable {
		Reporter.log("\n3.Verify community link in sureMDM console Help section",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonHelpButton();
		accountsettingspage.ClickonCommunityLink();	
		accountsettingspage.CloseHelpTab();
		//commonmethdpage.ClickOnSettings();
}
	
	@Test(priority=4,description="Verify Documentation  link in sureMDM console Help section") 
	public void VerifyDocumentationlinkinsureMDMconsoleHelpsectionTC_ST_702() throws Throwable {
		Reporter.log("\n4.Verify Documentation  link in sureMDM console Help section",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonHelpButton();
		accountsettingspage.ClickonDocumentationLink();
		accountsettingspage.CloseHelpTab();
		//commonmethdpage.ClickOnSettings();
}
	
	@Test(priority=5,description="Verify Release notes link in sureMDM console Help section") 
	public void VerifyReleasenoteslinkinsureMDMconsoleHelpsectionTC_ST_703() throws Throwable {
		Reporter.log("\n5.Verify Release notes link in sureMDM console Help section",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonHelpButton();
		accountsettingspage.ClickonReleaseNotesLink();
		accountsettingspage.CloseHelpTab();
		//commonmethdpage.ClickOnSettings();
}
	
	@Test(priority=6,description="Verify Training  link in sureMDM console Help section ") 
	public void VerifyTraininglinkinsureMDMconsoleHelpsectionTC_ST_704() throws Throwable {
		Reporter.log("\n6.Verify Training  link in sureMDM console Help section ",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonHelpButton();
		accountsettingspage.ClickonTrainingLink();
		accountsettingspage.CloseHelpTab();
		//commonmethdpage.ClickOnSettings();
}	
	
	
	
	
	
	
	
	
	
	
	
}
