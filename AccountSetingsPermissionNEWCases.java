package UserManagement_TestScripts;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class AccountSetingsPermissionNEWCases extends Initialization {

	// WEBHOOKS SETTINGS ROLES IN ACCOUNT SETTINGS

	// AllRoleWebhookSettingsPermission
	// DemouserWebhookSettingsPermission
	// AllRoleDataAnalyticsSettingsPermission
	// Account settings>> AutomationDataAnalytics

	@Test(priority = '1', description = "1.Verify webhooks option is added in roles in user management")
	public void TC_ST_671() throws InterruptedException {
		Reporter.log("\n1.Verify webhooks option is added in roles in user management", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.ClickOnExpandButtonAccountSettingsRoles();
		usermanagement.VerifyWebhooksOptionIsAddedInRolesAccountSettings();
		usermanagement.RolesClose();
	}

	@Test(priority = '2', description = "2.Verify enabling Webhooks options in roles and assigning to sub user")
	public void TC_ST_672() throws InterruptedException {
		Reporter.log("\n2.Verify enabling Webhooks options in roles and assigning to sub user", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.RoleWithAllPermissions(Config.RolesFoeWebhookSettings, "User All");
		usermanagement.CreateDemoUser1(Config.SubUser3, Config.pwd3, Config.pwd3, Config.RolesFoeWebhookSettings);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.VerifyWebHooksOptnInAccountSettingsSubUser();

	}

	@Test(priority = '3', description = "3.Verify disabling Webhooks options in roles and assigning to sub user")
	public void TC_ST_673() throws InterruptedException {
		Reporter.log("\n3.Verify disabling Webhooks options in roles and assigning to sub user", true);
		// SUPER USER
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.SearchRole(Config.RolesFoeWebhookSettings);
		usermanagement.selectRoleWebhookSettings();
		usermanagement.ClickOnEdit();
		usermanagement.ClickOnExpandButtonAccountSettingsRoles();
		usermanagement.VerifyWebhooksOptionIsAddedInRolesAccountSettings();
		usermanagement.DisableWebhookSettings();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.VerifyWebHooksOptnNotPresentInAccountSettingsSubUser();

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);

		commonmethdpage.ClickOnSettings();

		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnUserTab();

		usermanagement.SelectandDeleteUser(Config.SubUser3);

	}
	// ****************** DATA ANALYTICS IN ACCOUNT SETTINGS******************

	@Test(priority = '4', description = "4.Verify data analytics in account setting by  giving access to sub-user ")
	public void Verify_Enable_data_analytics_in_account_settingRole() throws InterruptedException {
		Reporter.log("\n4.Verify data analytics in account setting by  giving access to sub-user ", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.ClickOnAddButtonRoles();
		usermanagement.ClickOnExpandButtonAccountSettingsRoles();
		usermanagement.VerifyDataAnalyticsOptn();
		usermanagement.RolesClose();
		usermanagement.RoleWithAllPermissionsDA(Config.DataAnaRoles, " User DataAnalytics");
		usermanagement.CreateDemoUser1(Config.SubUser3, Config.pwd3, Config.pwd3, Config.DataAnaRoles);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.VerifyDataAnalyticsOptnInAccountSettingsSubUser();

	}

	@Test(priority = '5', description = "5.Verify the analytics added in admin user should visible to other sub-users .")
	public void Verify_data_analytics_in_accountsettingsADMIN() throws InterruptedException {
		Reporter.log("\n5.Verify the analytics added in admin user should visible to other sub-users .", true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.ClickOnAddAnalyticsButton();
		usermanagement.GetNewAnalyticsNumber();
		usermanagement.SendingDataAnalyticsName(Config.InputDataAnaName);
		usermanagement.SendingTagName(Config.InputDataAnaTagName);
		usermanagement.ClickOnAddFieldBtn();
		usermanagement.InputFieldName(Config.InputFieldName);
		usermanagement.ClickOnAddButton();
		usermanagement.ClickonAnalyticsSaveButton();
		usermanagement.ClickonAnalyticsAPPLYButton();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.ScrollToNewAnalytics();
		usermanagement.VerifyNewlyCreatedDataAnalyticsVisibleToSubUserWhenDataAnalyticsRoleIsEnabled();
	}

	@Test(priority = '6', description = "6.Verify removing the analytics from admin user")
	public void Verify_Removing_data_analytics_in_accountsettingsADMIN() throws InterruptedException {
		Reporter.log("\n6.Verify removing the analytics from admin user", true);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();

		usermanagement.ScrollToNewAnalytics();
		usermanagement.ClickOnRemoveBtn();

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.VerifyAnalyticsShouldNotBeVisibleInAccountSettings();

	}

	@Test(priority = '7', description = "7.Verify the analytics added in sub-users user should visible to the admin user.")
	public void Verify_data_analytics_in_accountsettingsSubUser() throws InterruptedException {
		Reporter.log("\n7.Verify the analytics added in sub-users user should visible to the admin user.", true);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.ClickOnAddAnalyticsButton();
		usermanagement.GetNewAnalyticsNumber();
		usermanagement.SendingDataAnalyticsName(Config.InputDataAnaNameSubUser);
		usermanagement.SendingTagName(Config.InputDataAnaTagNameSubUser);
		usermanagement.ClickOnAddFieldBtn();
		usermanagement.InputFieldName(Config.InputDataAnaFieldNameSubUser);
		usermanagement.ClickOnAddButton();
		usermanagement.ClickonAnalyticsSaveButton();
		usermanagement.ClickonAnalyticsAPPLYButton();

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.ScrollToNewAnalytics();
		usermanagement.VerifyNewlyCreatedDataAnalyticsVisibleToADMINWhenDataAnalyticsRoleIsEnabled();

	}

	@Test(priority = '8', description = "8.verify by removing the analytics in sub-users.")
	public void Verify_Removing_data_analytics_in_accountsettingsSubUser() throws InterruptedException {
		Reporter.log("\n8.verify by removing the analytics in sub-users.", true);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();

		usermanagement.ScrollToNewAnalytics();
		usermanagement.ClickOnRemoveBtn1();

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.VerifyAnalyticsShouldNotBeVisibleInAccountSettings();

	}

	@Test(priority = '9', description = "9.Verify the analytics added in user should visible to other sub-users.")
	public void Verify_data_analytics_in_accountsettingsUSERS() throws InterruptedException {
		Reporter.log("\n9.Verify the analytics added in user should visible to other sub-users.", true);

		// Superuser creating User
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.CreateDemoUser1(Config.DataAnaUser, Config.pwd3, Config.pwd3, Config.DataAnaRoles);

		// Sub-USER
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);

		// Sub-USER Creating DataAnalytics
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.ClickOnAddAnalyticsButton();
		usermanagement.GetNewAnalyticsNumber();
		usermanagement.SendingDataAnalyticsName(Config.InputDataAnaNameUser);
		usermanagement.SendingTagName(Config.InputDataAnaTagNameUser);
		usermanagement.ClickOnAddFieldBtn();
		usermanagement.InputFieldName(Config.InputDataAnaFieldNameUser);
		usermanagement.ClickOnAddButton();
		usermanagement.ClickonAnalyticsSaveButton();
		usermanagement.ClickonAnalyticsAPPLYButton();

		// USER
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.DataAnaUser, Config.pwd3);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.ScrollToNewAnalytics();
		usermanagement.VerifyNewlyCreatedDataAnalyticsVisibleToUSERSWhenDataAnalyticsRoleIsEnabled();

	}

	@Test(priority = 'A', description = "10.verify by removing the analytics in sub-users.")
	public void Verify_Removing_data_analytics_in_accountsettingsSubUserForUSER() throws InterruptedException {
		Reporter.log("\n10.verify by removing the analytics in sub-users.", true);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();

		usermanagement.ScrollToNewAnalytics();
		usermanagement.ClickOnRemoveBtn2();

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.DataAnaUser, Config.pwd3);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.ClickOnDataAnalyticsTab();
		usermanagement.VerifyAnalyticsShouldNotBeVisibleInAccountSettings();

	}

	@Test(priority = 'B', description = "11.Verify data analytics in account setting by not giving access to sub-user")
	public void Verify_Disable_data_analytics_in_account_settingRole() throws InterruptedException {

		Reporter.log("\n11.Verify data analytics in account setting by not giving access to sub-user", true);

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.userID, Config.password);

		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.ClickOnRolesOption();
		usermanagement.SearchRole(Config.DataAnaRoles);
		usermanagement.SelectDataAnalyticsRole();
		usermanagement.ClickOnEdit();
		usermanagement.ClickOnExpandButtonAccountSettingsRoles();
		usermanagement.VerifyDisablingDataAnalyticsOptn();

		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		loginPage.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser(Config.SubUser3, Config.pwd3);

		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		usermanagement.VerifyDataAnalyticsOptnNotPresentInAccountSettingsSubUser();

	}

}
