package Profiles_iOS_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class ConfigurationProfile extends Initialization{
	
	@Test(priority='1',description="1.To verify clicking on Configuration Profile") 
    public void VerifyClickingOnConfigurationProfile() throws InterruptedException {
    Reporter.log("To verify clicking on Configuration Profile");
    profilesAndroid.ClickOnProfile();
	profilesiOS.clickOniOSOption();
	profilesiOS.AddProfile();
	profilesiOS.ClickOnConfigurationProfile();
}
	
	@Test(priority='2',description="2.To verify summary of Configuration Profile")
	public void VerifySummaryofConfigurationProfile() throws InterruptedException
	{
		profilesiOS.VerifySummaryofConfigurationProfile();
	}
	
	@Test(priority='3',description="3.To Verify warning message Saving a Configuration Profile without Profile Name")
	public void VerifyWarningMessageOnSavingConfiguratioProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Configuration Profile without Profile Name=========");
		profilesiOS.ClickOnConfigureButtonConfigurationProfile();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='4',description="4.To Verify warning message Saving a Configuration Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingConfigurationProfileWihtoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Configuration Profile without entering all the fields=========");
		profilesiOS.EnterConfigurationProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	
	
	/*
	@Test(priority='5',description="To verify saving the Configuration profile")
	public void Verify_SavingConfigurationProfile() throws InterruptedException{
		Reporter.log("To verify saving the Configuration profile ",true);
	    profilesiOS.ClickOnSavebutton();
	    profilesAndroid.NotificationOnProfileCreated();
	}*/
	

}
