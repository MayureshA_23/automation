package SanityTestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Settings_TestScripts extends Initialization{
	
	@Test(priority=0,description="Verify options in  Creat User Window UI")
	public void VerifyOfUserCreatWindowUI() throws InterruptedException
	{Reporter.log("=====1.Verify options in  Creat User Window UI=====",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyOfOptionsInAddUserUI();
	}
	@Test(priority=1,description="VerifySelecting all permissions in Roles .")
	public void SelectingAllPermissionInRoles() throws InterruptedException
	{Reporter.log("=====2.VerifySelecting all permissions in Roles .=====",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("All Permission123");
		usermanagement.RoleWithAllPermissions("All Permission123","All Permission123");
	}
	@Test(priority=2,description="User Management_Verify User Management- Edit User-select details")
	public void VerifyOfEditUserInUserManagement() throws InterruptedException
	{Reporter.log("=====3.Verify options in  Creat User Window UI=====",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.SearchUserUM("Kavya1");
		usermanagement.DeletingExistingUser("Kavya1");
		
		usermanagement.ClickOnRolesOption();
		usermanagement.DeletingExistingRole("Permissions");
		usermanagement.RoleWithAllPermissions("Permissions","Permissions");
		
		usermanagement.ClickOnUserTab();
		usermanagement.clickOnAddUserBtn();
		usermanagement.EnterUserName("Kavya1");
		usermanagement.EnterUserPasswordAndConfirm("42Gears@123", "42Gears@123");
		usermanagement.EnterFirstName("Demo");
		usermanagement.EnterLastName("xyz");
		usermanagement.EnterUserEmail("test@gmail.com");
		usermanagement.EnterPhoneNo("9872345671");
		usermanagement.clicOnCreatBtn();
		usermanagement.SearchUserUM("Kavya1");
		usermanagement.selectUserInUsersList("Kavya1");
		usermanagement.ClickOnEditUser();
		usermanagement.EnterFirstName("Test123");
		usermanagement.EnterLastName("xxx");
		usermanagement.EnterUserEmail("zet@gmail.com");
		usermanagement.EnterPhoneNo("9235221267");
		usermanagement.selectRoleForUser("Permissions");
		usermanagement.VerifyOfUserModified();
	}
	/*@Test(priority=3,description="Verify resetting password for suremdm console")
	public void ResettingPassword() throws InterruptedException
	{Reporter.log("=====4.Verify resetting password for suremdm console=====",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.clickOnSecurity();
		usermanagement.EnterOldPwd("42Gears@654", "42Gears@567", "42Gears@567");
		usermanagement.clickOnOkBtnInresetPwd();
		usermanagement.NotificationOnResettingPwd();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.clickOnGridrefreshbutton();
		customReports.logintoApp(Config.userID,"42Gears@567");
		customReports.ClickOnLogin();
	}*/
}
