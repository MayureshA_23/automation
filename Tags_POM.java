package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.seleniumhq.jetty9.io.ClientConnectionFactory.Helper;
import org.seleniumhq.jetty9.server.handler.ContextHandler.AliasCheck;
import org.testng.Assert;
import org.testng.Reporter;

//import com.thoughtworks.selenium.webdriven.commands.KeyEvent;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.WebDriverCommonLib;

public class Tags_POM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();

	@FindBy(xpath = "//*[@id='groupsbuttonpanel']/div/ul/li[2]/a")
	private WebElement Tags;

	@FindBy(id = "newGroup")
	private WebElement NewTag;

	@FindBy(id = "renameGroup")
	private WebElement RenameTag;

	@FindBy(id = "deleteGroup")
	private WebElement DeletTag;

	@FindBy(id = "ApplyJobtoGroup")
	private WebElement ApplyJobTag;

	@FindBy(xpath = ".//*[@id='groupForm']/div/input")
	private WebElement NewTagName;

	@FindBy(xpath = ".//*[@id='groupokbtn']")
	private WebElement OK;

	@FindBy(xpath = "(//li[text()='AutomationTag'])[last()]")
	private WebElement TagName;

	@FindBy(xpath = "//li[text()='" + Config.FirstTagName + "']")
	private WebElement FirstTagName;

	@FindBy(xpath = "//li[text()='" + Config.RenameTag + "']")
	private WebElement ReNamedTag;

	@FindBy(xpath = ".//*[@id='deletegroupbtn']/a")
	private WebElement DeleteTagYes;

	@FindBy(xpath = "//span[contains(text(),'deleted successfully.')]")
	private WebElement DeleteTagMsg;

	@FindBy(xpath = "//span[text()='Groups']")
	private WebElement Group;

	@FindBy(xpath = ".//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDevice;

	@FindBy(xpath = "//p[text()='" + Config.DeviceName + "']")
	private WebElement Device;

	@FindBy(xpath = ".//*[@id='gridMenu']/li[6]")
	private WebElement TagOption;

	@FindBy(xpath = ".//*[@id='masterBody']/ul[1]/li[5]")
	private WebElement MultipleTagOption;

	@FindBy(xpath = "//span[text()='Add Tags']")
	private WebElement AddTag;

	@FindBy(xpath = "//span[text()='Add Tags']")
	private WebElement MultipleAddTag;

	@FindBy(xpath = ".//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[1]/div/input")
	private WebElement SearchTag;

	@FindBy(xpath = "//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul")
	private WebElement CheckboxTag;

	@FindBy(xpath = ".//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[4]/a/label/input")
	private WebElement CheckboxTag2;

	@FindBy(id = "tagsDeviceSaveBtn")
	private WebElement SaveTag;

	@FindBy(xpath = "//p[text()='Create Custom Tag']")
	private WebElement CreateCustomTag;

	@FindBy(xpath = "//span[contains(text(),'Device(s) added to tag(s) successfully.')]")
	private WebElement DeviceAddedTagMsg;

	@FindBy(xpath = "//span[contains(text(),'Device(s) removed from tag(s) successfully.')]")
	private WebElement DeviceRemoveTagMsg;

	@FindBy(xpath = ".//*[@id='JobGridContainer']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement SearchJob;

	@FindBy(xpath = "//p[text()='Text']")
	private WebElement SearchedJob;

	@FindBy(id = "applyJob_okbtn")
	private WebElement ApplyJob;

	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YesApply;

	@FindBy(xpath = "//span[text()='Remove Tags']")
	private WebElement RemoveTag;

	@FindBy(id = "all_ava_devices")
	private WebElement AllDevices;

	@FindBy(xpath = "//p[text()='" + Config.DeviceName + "']")
	private WebElement devicename;

	@FindBy(id = "NewTagCutom")
	private WebElement customTag;

	@FindBy(id = "newcustomtagadd")
	private WebElement CustomTagAdd;

	@FindBy(xpath = ".//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[5]/a/label/input")
	private WebElement CheckboxTag3;

	@FindBy(xpath = ".//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[6]/a/label/input")
	private WebElement CheckboxTag4;

	@FindBy(xpath = ".//*[@id='groupstree']/ul/li[text()='Home']")
	private WebElement Home;

	@FindBy(xpath = "//span[text()='Move to Group']")
	private WebElement MoveTOGroup;

	@FindBy(xpath = ".//*[@id='groupList']/ul/li[text()='Mayuri']")
	private WebElement SelectGroup;

	@FindBy(xpath = "//button[text()='Move']")
	private WebElement Move;

	@FindBy(xpath = ".//*[@id='dataGrid']/tbody/tr[1]/td[1]")
	private WebElement GroupDevice;

	public void ClickOnTags() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("(//span[text()='Tags'])[last()]")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(15);
		boolean value = NewTag.isDisplayed();
		String PassStatement = "PASS >> 'NewTag button' is displayed  when Clicked on Tags";
		String FailStatement = "Fail >> 'NewTag button' is not displayed  when Clicked on Tags";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		boolean value1 = RenameTag.isDisplayed();
		String PassStatement1 = "PASS >> 'RenameTag button' is displayed  when Clicked on Tags";
		String FailStatement1 = "Fail >> 'RenameTag button' is not displayed  when Clicked on Tags";
		ALib.AssertTrueMethod(value1, PassStatement1, FailStatement1);

		boolean value2 = DeletTag.isDisplayed();
		String PassStatement2 = "PASS >> 'DeletTag button' is displayed  when Clicked on Tags";
		String FailStatement2 = "Fail >>  'DeletTag button' is not displayed  when Clicked on Tags";
		ALib.AssertTrueMethod(value2, PassStatement2, FailStatement2);

		boolean value3 = ApplyJobTag.isDisplayed();
		String PassStatement3 = "PASS >> 'ApplyJobTag button' is displayed  when Clicked on Tags";
		String FailStatement3 = "Fail >> 'ApplyJobTag button' is not displayed  when Clicked on Tags";
		ALib.AssertTrueMethod(value3, PassStatement3, FailStatement3);

		sleep(3);

	}

	// Kavya
	public void CreatTag(String tagName) throws InterruptedException {

		try {
			Initialization.driver
					.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + tagName + "']"))
					.isDisplayed();
			sleep(4);
			Initialization.driver
					.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + tagName + "']")).click();
			sleep(2);
			DeletTag.click();
			waitForXpathPresent(".//*[@id='deleteGroupConf']/div/div/div[1]/p");
			sleep(2);
			DeleteTagYes.click(); 
			sleep(2);
			waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
			sleep(4);
		} catch (Exception e) {
			Reporter.log("No Tags are exist with expected Name", true);
		}
		sleep(3);
		NewTag.click();
		waitForidPresent("groupDialog");
		sleep(3);
		NewTagName.sendKeys(tagName);
		OK.click();
		sleep(10);
	}

	// Kavya
	public void verifyOfCreatedTag(String tagName) throws InterruptedException {
		Boolean value = Initialization.driver
				.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + tagName + "']")).isDisplayed();
		String PassStatement = "PASS >> 'Tag Created successfully..'";
		String FailStatement = "Fail >> 'Tag is not Created successfully..'";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='generateQRCode']")
	private WebElement generateQRCode;

	public void ClickOnEnroll() throws InterruptedException {

		generateQRCode.click();
		sleep(120);
	}

	public void RenamingTag(String tag, String renamedTag) throws InterruptedException {
		// TagName.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + tag + "']")).click();
		sleep(4);
		RenameTag.click();
		waitForXpathPresent("//h4[text()='Rename existing Tag']");
		sleep(3);
		NewTagName.clear();
		NewTagName.sendKeys(renamedTag);
		sleep(2);
		OK.click();
		waitForXpathPresent("//li[text()='" + renamedTag + "']");
		sleep(2);
	}

	public void verifyOfRenamedTag(String tag) {
		js.executeScript("arguments[0].scrollIntoView(true);",
				Initialization.driver.findElement(By.xpath("//li[text()='" + tag + "']")));
		boolean value = Initialization.driver.findElement(By.xpath("//li[text()='" + tag + "']")).isDisplayed();
		String PassStatement = "PASS >> 'Renamed tag' is displayed ";
		String FailStatement = "Fail >> 'Renamed tag' is not displayed ";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void DeletingTag(String tag) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//li[text()='" + tag + "']")).click();
		sleep(4);
		DeletTag.click();
		waitForXpathPresent(".//*[@id='deleteGroupConf']/div/div/div[1]/p");
		sleep(2);
		DeleteTagYes.click();
		sleep(2);
		waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
	}

	public void verifyOfDeletedTag(String tag) throws InterruptedException {
		try {
			if (Initialization.driver.findElement(By.xpath("//li[text()='" + tag + "']")).isDisplayed()) {
				ALib.AssertFailMethod("FAIL >>  Tag is not deleted successfully..");
			}
		} catch (Exception e) {
			Reporter.log("PASS >> Tag is deleted successfully..", true);
		}
		sleep(3);
	}

	public void ClickOnGroup() throws InterruptedException {
		Group.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(20);
	}

	public void RightClickTag(String device) throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.contextClick(Initialization.driver.findElement(By.xpath("//p[text()='" + device + "']"))).perform();
		sleep(6);
		act.moveToElement(TagOption).perform();
		// act.moveToElement(AddTag).perform();
		sleep(3);
		AddTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);
	}

	@FindBy(xpath = "//ul[@id='customMenu']/li/span[text()='Tags']")
	private WebElement TagOptionForMultipleDevices;

	public void RightClickTag_MultipleDevice() throws InterruptedException {
		Actions act = new Actions(Initialization.driver);
		act.contextClick(Device).perform();
		sleep(6);
		act.moveToElement(TagOptionForMultipleDevices).perform();
		// act.moveToElement(AddTag).perform();
		sleep(3);
		AddTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);
	}

	public void SearchTag(String tag) throws InterruptedException {
		SearchTag.sendKeys(tag);
		sleep(5);
	}

	public void EnableTagCheckbox(String tag) throws InterruptedException {
		// CheckboxTag.click();
		Initialization.driver
				.findElement(By.xpath("(//label[text()='" + tag + "']//preceding::input[@type='checkbox'])[last()]"))
				.click();
		sleep(2);
	}

	public void AvailibilityOfCreateCustomTag() throws InterruptedException {

		boolean value = CreateCustomTag.isDisplayed();
		String PassStatement = "PASS >> 'Create Custom Tag' is displayed ";
		String FailStatement = "Fail >> 'Create Custom Tag' is not displayed ";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(3);
	}

	@FindBy(xpath = "//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[3]/a/label")
	private WebElement FirstTag;

	public void EnableTheFirstTagFromTheList() throws InterruptedException {
		String a = FirstTag.getText();
		System.out.println(a);
		FirstTag.click();
		sleep(2);
	}

	public void ClickOnSaveTagButton() throws InterruptedException {
		SaveTag.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
		// Reporter.log("'Device(s) added to tag(s) successfully.", true);
	}

	public void SuccessfulMessageOnTagMove() throws InterruptedException {
		try {
			DeviceAddedTagMsg.isDisplayed();
			Reporter.log("PASS >> Move to tag message displayed successfully", true);
			sleep(5);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Move to tag message not displayed successfully");
		}

	}

	@FindBy(xpath = "//p[text()='" + Config.DeviceName + "']")
	private WebElement CheckDeviceInTag;

	public void ClickOnTagWithDevices(String tag, String device) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//li[text()='" + tag + "']")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(8);
		boolean a = Initialization.driver.findElement(By.xpath("//p[text()='" + device + "']")).isDisplayed();
		String pass = "PASS >> Device move to tag successfully, device is present inside tag";
		String fail = "FAIL >> Device is not present inside tag, device not moved to tag";
		ALib.AssertTrueMethod(a, pass, fail);

	}

	public void ClickOnSaveTagButtonForRemove() throws InterruptedException {
		SaveTag.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(3);
	}

	public void CheckStatusOfcomplianceJob() throws InterruptedException {

		WebDriverWait wait3 = new WebDriverWait(Initialization.driver, 1500);
		try {
			wait3.until(ExpectedConditions.presenceOfElementLocated(
					By.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']")))
					.isDisplayed();
			System.out.println("PASS>>> Job is deployed successfully to device");
		} catch (Throwable e) {
			System.out.println("job in queue refresh device");
			Initialization.driver.findElement(By.xpath(".//*[@id='refreshButton']/i")).click();
			Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			sleep(5);
			try {
				wait3.until(ExpectedConditions.presenceOfElementLocated(By
						.xpath("//i[contains(@class,'st-jobStatus dev') and @title='All jobs successfully deployed']")))
						.isDisplayed();
				System.out.println("PASS>>> Job is deployed successfully to device after device refresh");
			} catch (Throwable e1) {
				System.out.println("FAIL>>> job not deployed to device");

			}
		}
		sleep(4);

	}

	public void ApplyJobToTag() throws InterruptedException {
		Tags.click();
		waitForXpathPresent("//li[text()='Tag1']");
		sleep(2);
		TagName.click();
		sleep(2);
		ApplyJobTag.click();
		waitForXpathPresent(".//*[@id='apply_job_header']");
		sleep(2);
		SearchJob.sendKeys("Text");
		waitForXpathPresent("//p[text()='Text']");
		SearchedJob.click();
		ApplyJob.click();
		waitForidPresent("deviceConfirmationDialog");
		YesApply.click();
		// CheckStatusOfcomplianceJob();

	}

	@FindBy(xpath = "//p[text()='" + Config.SecondDeviceNameFilter + "']")
	private WebElement SecondDeviceName;

	public void ClickOnTagWithMultipleDevices() throws InterruptedException {
		FirstTagName.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(8);
		boolean a = CheckDeviceInTag.isDisplayed();
		String pass = "First Device move to tag successfully, device is present inside tag";
		String fail = "First Device is not present inside tag, device not moved to tag";
		ALib.AssertTrueMethod(a, pass, fail);

		boolean a1 = SecondDeviceName.isDisplayed();
		String pass1 = "2nd Device move to tag successfully, device is present inside tag";
		String fail1 = "2nd Device is not present inside tag, device not moved to tag";
		ALib.AssertTrueMethod(a1, pass1, fail1);

	}

	public void RemoveDeviceFromTag() throws InterruptedException {

		Actions act = new Actions(Initialization.driver);
		act.contextClick(Device).perform();
		sleep(1);
		act.moveToElement(TagOption).perform();
		sleep(1);
		act.moveToElement(RemoveTag).perform();

		RemoveTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);
	}

	public void RightClickMultipleDevice() throws InterruptedException, AWTException {
		Actions actionObj = new Actions(Initialization.driver);
		actionObj.keyDown(Keys.CONTROL).sendKeys(Keys.chord("A")).keyUp(Keys.CONTROL).perform();

		Actions act = new Actions(Initialization.driver);
		act.contextClick(AnyFirstDevice).perform();
		sleep(4);
		act.moveToElement(MultipleTagOption).perform();
		sleep(3);
		act.moveToElement(MultipleAddTag).perform();
		sleep(3);
		MultipleAddTag.click();
		waitForidPresent("tagsDeviceSaveBtn");
		sleep(6);

	}

	@FindBy(xpath = "//*[@id='dataGrid']/tbody/tr[2]/td[2]")
	private WebElement AnySecondDevice;

	public void RemoveMultipleDevicesFromTag() throws InterruptedException, AWTException {

		Actions actionObj = new Actions(Initialization.driver);
		actionObj.keyDown(Keys.CONTROL);
		AnySecondDevice.click();

		Actions act = new Actions(Initialization.driver);
		act.contextClick(AnySecondDevice).perform();
		sleep(4);
		act.moveToElement(MultipleTagOption).perform();
		sleep(4);
		act.moveToElement(RemoveTag).perform();
		sleep(4);
		RemoveTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);

	}

	public void EnterCustomTagName(String name) throws InterruptedException, AWTException {

		customTag.sendKeys(name);
		sleep(3);
	}

	public void AddButtonCustomTag() throws InterruptedException {

		CustomTagAdd.click();
		sleep(3);
	}

	public void AddTagsToSubGroup() throws InterruptedException, AWTException {
		waitForidPresent("newGroup");
		sleep(3);
		Group.click();

		sleep(4);
		NewTag.click();
		waitForidPresent("groupDialog");
		sleep(3);
		NewTagName.sendKeys("Mayuri");
		OK.click();
		waitForXpathPresent(".//*[@id='groupstree']/ul/li[text()='Mayuri']");
		sleep(2);
		Home.click();
		waitForXpathPresent("//p[text()='" + Config.DeviceName + "']");
		sleep(2);
		Device.click();
		sleep(3);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		devicename.click();

		Actions act = new Actions(Initialization.driver);
		act.contextClick(Device).perform();
		sleep(4);
		act.moveToElement(MultipleTagOption).perform();
		MoveTOGroup.click();
		waitForXpathPresent("//button[text()='Move']");
		sleep(3);
		// waitForidPresent("groupListModal");
		SelectGroup.click();

		Move.click();
		rb.keyRelease(KeyEvent.VK_CONTROL);
		sleep(3);
		waitForXpathPresent("//p[text()='" + Config.DeviceName + "']");
		sleep(5);
		Device.click();
		sleep(2);
		Robot rb1 = new Robot();
		rb1.keyPress(KeyEvent.VK_CONTROL);
		devicename.click();
		rb1.keyRelease(KeyEvent.VK_CONTROL);

		Actions act1 = new Actions(Initialization.driver);
		act1.contextClick(Device).perform();
		sleep(4);
		act1.moveToElement(MultipleTagOption).perform();
		act1.moveToElement(MultipleAddTag).perform();
		sleep(2);
		MultipleAddTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);
		// SearchTag.sendKeys("Tag1");
		CheckboxTag.click();
		CheckboxTag2.click();
		CheckboxTag3.click();
		sleep(3);
		SaveTag.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			DeviceAddedTagMsg.isDisplayed();
		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed, "PASS:'Device(s) added to tag(s) successfully.. is  displayed");
		Reporter.log("FAIL>> Alert Message :'Device(s) added to tag(s) successfully.' is not displayed", true);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='dataGrid']/tbody/tr[1]/td[2]")
	private WebElement AnyFirstDevice;

	public void RightClickAllDevices() throws InterruptedException, AWTException {

		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_A);
		sleep(4);
		Actions act = new Actions(Initialization.driver);
		act.contextClick(AnyFirstDevice).perform();
		sleep(4);
		act.moveToElement(MultipleTagOption).perform();
		act.moveToElement(MultipleAddTag).perform();
		sleep(2);
		MultipleAddTag.click();
		waitForidPresent("tagsDeviceSaveBtn");
		sleep(6);

	}

	public void RemoveAllDevicesFromTag() throws InterruptedException, AWTException {

		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_A);
		sleep(4);
		Actions act = new Actions(Initialization.driver);
		act.contextClick(AnyFirstDevice).perform();
		sleep(4);
		act.moveToElement(MultipleTagOption).perform();
		act.moveToElement(RemoveTag).perform();
		sleep(2);
		RemoveTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);

	}

	public void RemoveTagsFromSubGroup() throws InterruptedException, AWTException {
		waitForXpathPresent("//p[text()='" + Config.DeviceName + "']");
		sleep(2);
		Device.click();
		sleep(2);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		devicename.click();
		rb.keyRelease(KeyEvent.VK_CONTROL);

		Actions act = new Actions(Initialization.driver);
		act.contextClick(Device).perform();
		sleep(4);
		act.moveToElement(MultipleTagOption).perform();
		act.moveToElement(RemoveTag).perform();
		sleep(2);
		RemoveTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);
		// SearchTag.sendKeys("Tag1");
		CheckboxTag.click();
		CheckboxTag2.click();
		CheckboxTag3.click();
		sleep(3);
		SaveTag.click();
		sleep(1);
		waitForXpathPresent("//span[text()='Device(s) removed from tag(s) successfully.']");
		boolean isdisplayed = true;
		try {
			DeviceRemoveTagMsg.isDisplayed();
		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed, "PASS:'Device(s) Removed to tag(s) successfully.. is  displayed");
		Reporter.log("FAIL>> Alert Message :'Device(s) Removed to tag(s) successfully.' is not displayed", true);
		sleep(2);
	}

	public void ClickOnAllDevicesButton() throws InterruptedException, AWTException {

		AllDevices.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
	}

	public void RemoveTagsFromAllDevices() throws InterruptedException, AWTException {
		/* waitForXpathPresent(".//*[@id='groupstree']/ul/li[text()='Home']"); */

		sleep(6);
		WebDriverWait wait3 = new WebDriverWait(Initialization.driver, 1200);
		try {
			wait3.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath(".//*[@id='groupstree']/ul/li[text()='Home']"))).isDisplayed();
			System.out.println("PASS>>>Devices displayed ");

		} catch (Throwable e1) {
			System.out.println("FAIL>>>Devices not dispalyed");

		}
		sleep(4);
		// Home.click();
		sleep(4);
		Robot rb1 = new Robot();
		rb1.keyPress(KeyEvent.VK_CONTROL);
		rb1.keyPress(KeyEvent.VK_A);

		Actions act1 = new Actions(Initialization.driver);
		act1.contextClick(GroupDevice).perform();
		sleep(4);
		act1.moveToElement(MultipleTagOption).perform();
		act1.moveToElement(RemoveTag).perform();
		sleep(2);
		RemoveTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);
		// SearchTag.sendKeys("Tag1");
		CheckboxTag.click();
		CheckboxTag2.click();
		CheckboxTag3.click();
		CheckboxTag4.click();
		sleep(3);
		SaveTag.click();
		waitForXpathPresent("//span[text()='Device(s) removed from tag(s) successfully.']");
		sleep(2);
		boolean isdisplayed = true;
		try {
			DeviceRemoveTagMsg.isDisplayed();
		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed, "PASS:'Device(s) Removed to tag(s) successfully.. is  displayed");
		Reporter.log("FAIL>> Alert Message :'Device(s) Removed to tag(s) successfully.' is not displayed", true);
		sleep(2);
		rb1.keyRelease(KeyEvent.VK_A);
		rb1.keyRelease(KeyEvent.VK_CONTROL);
		sleep(3);

	}

	public void CreateCustomTagToSubGroupDevices() throws InterruptedException, AWTException {
		/*
		 * waitForXpathPresent(".//*[@id='groupstree']/ul/li[text()='Home']"); sleep(5);
		 * Home.click(); WebDriverWait wait3 = new
		 * WebDriverWait(Initialization.driver,1200); try{
		 * wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
		 * ".//*[@id='groupstree']/ul/li[text()='Home']"))).isDisplayed();
		 * System.out.println("PASS>>>Devices displayed ");
		 * 
		 * } catch(Throwable e1){ System.out.println("FAIL>>>Devices not dispalyed");
		 * 
		 * } sleep(4); sleep(4); Robot rb1 = new Robot();
		 * rb1.keyPress(KeyEvent.VK_CONTROL); rb1.keyPress(KeyEvent.VK_A);
		 * 
		 * Actions act1 = new Actions(Initialization.driver);
		 * act1.contextClick(GroupDevice).perform(); sleep(4);
		 * act1.moveToElement(MultipleTagOption).perform();
		 * act1.moveToElement(MultipleAddTag).perform(); sleep(2);
		 * MultipleAddTag.click(); waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		 * sleep(4); Device.click(); sleep(2); Robot rb = new Robot();
		 * rb.keyPress(KeyEvent.VK_CONTROL); Device2.click();
		 * rb.keyRelease(KeyEvent.VK_CONTROL);
		 * 
		 * Actions act = new Actions(Initialization.driver);
		 * act.contextClick(Device).perform(); sleep(4);
		 * act.moveToElement(MultipleTagOption).perform();
		 * act.moveToElement(RemoveTag).perform(); sleep(2); RemoveTag.click();
		 * waitForXpathPresent("(//h4[text()='Tag List'])[1]"); sleep(4);
		 */
		waitForXpathPresent("//p[text()='vinod']");
		sleep(2);
		Device.click();
		sleep(2);
		Robot rb1 = new Robot();
		rb1.keyPress(KeyEvent.VK_CONTROL);
		devicename.click();
		rb1.keyRelease(KeyEvent.VK_CONTROL);

		Actions act1 = new Actions(Initialization.driver);
		act1.contextClick(Device).perform();
		sleep(4);
		act1.moveToElement(MultipleTagOption).perform();
		act1.moveToElement(MultipleAddTag).perform();
		sleep(2);
		MultipleAddTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);

		customTag.sendKeys("Tag4Coustm");
		CustomTagAdd.click();
		sleep(3);
		CheckboxTag.click();
		CheckboxTag2.click();
		CheckboxTag3.click();
		CheckboxTag4.click();
		sleep(3);
		SaveTag.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			DeviceAddedTagMsg.isDisplayed();
		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed, "PASS:'Device(s) added to tag(s) successfully.. is  displayed");
		Reporter.log("FAIL>> Alert Message :'Device(s) added to tag(s) successfully.' is not displayed", true);
		sleep(2);
		rb1.keyRelease(KeyEvent.VK_A);
		rb1.keyRelease(KeyEvent.VK_CONTROL);

	}

	// TAGS

	// Tag_ verify adding of Job/profiile in tag properties

	@FindBy(xpath = "//*[@id='groupProperties']")
	private WebElement PropertiesTag;

	@FindBy(xpath = "//*[@id='Tag_properties_addBtn']/i")
	private WebElement AddButtonTagProperties;

	@FindBy(xpath = "//*[@id='compjob_JobGridContainer']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement SearchTextFieldTag;

	@FindBy(xpath = "//*[text()='" + Config.TagNameDeviceModelName + "']")
	private WebElement ClickDeviceTag;

	@FindBy(xpath = "//div[@id='tagsListMainBlock']/ul//li[text()='" + Config.TagNameDeviceModelName + "']")
	private WebElement ClickOnCreatedTag;

	public void ClickOnCreatedTag() throws InterruptedException {

		ClickOnCreatedTag.click();
		Reporter.log("Clicked on Created tag", true);
		sleep(4);

		if (ClickOnCreatedTag.isDisplayed()) {
			System.out.println("PASS>>TagName '" + Config.TagNameDeviceModelName + "' Present");
		} else {
			System.out.println("FAIL>>TagName '" + Config.TagNameDeviceModelName + "' NOT Present");
		}

		// ClickDeviceTag.click();
		// sleep(4);

	}

	public void ClickOnCreatedTag2(String TagName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + TagName + "']"))
				.click();
		Reporter.log(">> Clicked on Created tag", true);
		sleep(2);
	}

	@FindBy(xpath = "//div[@id='device_jobQ_modal']/div[1]/button")
	private WebElement JobQueueCloseButton;

	public void ClickOnJobQueueCloseButton() throws InterruptedException {
		try {
			JobQueueCloseButton.click();
			waitForidPresent("deleteDeviceBtn");
			sleep(2);
		} catch (Exception e) {

		}
	}

	@FindBy(xpath = "//*[@id='jobQueueButton']/i")
	private WebElement jobQueueButton;

	public void clickOnJobhistoryTabTAGS() throws InterruptedException {
		jobQueueButton.click();
		waitForXpathPresent("//*[@id='jobQueueRefreshBtn']/i");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='jobQueueHistoryTab']/a")
	private WebElement LatestJobNameInsideHistoryCompletedJobs;

	public void CompletedJobsSection() throws InterruptedException {
		LatestJobNameInsideHistoryCompletedJobs.click();
		waitForXpathPresent("//*[@id='jobQueueRefreshBtn']/i");
		sleep(2);
	}

	public void VerifyDefaultTag(String devicemodel) throws InterruptedException {

		boolean DevicePresent = Initialization.driver.findElement(By.xpath("//*[@id='dataGrid']/tbody/tr/td[2]/p[contains(text(),' " + devicemodel + "')]")).isDisplayed();
		String Pass = "PASS>>Android Device is present inside created Android Device Model";
		String Fail = "FAIL>>Android Device is NOT present inside created Android Device Model";
		ALib.AssertTrueMethod(DevicePresent, Pass, Fail);
		sleep(2);
	}

	public void ClickOnTagPropertiesButton() throws InterruptedException {

		PropertiesTag.click();
		sleep(2);
		waitForXpathPresent("//*[@id='tagPropertiesDialog']/div/div/div[1]/h4[text()='Tag Properties - \"']");
	}

	// Kavya
	public void SearchJob(String TagJob, String TagName) throws InterruptedException {
		SearchTextFieldTag.sendKeys(TagJob);
		sleep(4);
		// refreshButtonTags.click();
		waitForXpathPresent("//*[@id='GroupJobDataGrid']/tbody/tr/td[2]/p[text()='" + TagJob + "']");
		sleep(2);
		// FileJob.click();
		Initialization.driver
				.findElement(By.xpath("//*[@id='GroupJobDataGrid']/tbody/tr/td[2]/p[text()='" + TagJob + "']")).click();
		sleep(2);
		OkButton.click();
		sleep(2);
		waitForXpathPresent("(//span[contains(text()," + "\"" + TagName + "\")])[1]"); // contains
		sleep(1);
	}

	public void verifyOfJobInTagsProperty(String Job) throws InterruptedException {
		try {
			Initialization.driver.findElement(By.xpath("//table[@id='TagJobGrid']/tbody/tr/td/p[text()='" + Job + "']"))
					.isDisplayed();
			sleep(2);
			Reporter.log("PASS >> Job Added Succesfully in Tag Properties Tab...", true);
		} catch (Exception e) {
			tagpropertiesokbtn.click();
			sleep(2);
			ALib.AssertFailMethod("FAIL >> Job didn't Added Succesfully in Tag Properties Tab...");
		}
		tagpropertiesokbtn.click();
		sleep(2);
	}

	public void AddButtonClickTagProperties() throws InterruptedException {
		AddButtonTagProperties.click();
		sleep(2);
		waitForXpathPresent("//*[@id='groupAddJob_modal']/div[1]/h4");
	}

	@FindBy(xpath = "//*[@id='GroupJobDataGrid']/tbody/tr/td[2]/p[text()='FileJob']")
	private WebElement FileJob;

	@FindBy(xpath = "//*[@id='groupAddJob_modal']/div[3]/button[text()='OK']")
	private WebElement OkButton;

	@FindBy(xpath = "//*[@id='tagpropertiesokbtn']")
	private WebElement tagpropertiesokbtn;

	@FindBy(xpath = "//*[@id='compjob_JobGridContainer']/div[2]/div[1]/div[1]/div[1]/button[@name='refresh']")
	private WebElement refreshButtonTags;

	public void SearchJob() throws InterruptedException {
		SearchTextFieldTag.sendKeys("FileJob");
		sleep(4);
		refreshButtonTags.click();
		waitForXpathPresent("//*[@id='GroupJobDataGrid']/tbody/tr/td[2]/p[text()='FileJob']");
		sleep(2);
		FileJob.click();
		sleep(6);
		OkButton.click();
		sleep(2);
		waitForXpathPresent("//span[text()='Job FileJob added successfully to KITKAT [Android]']"); // contains
		sleep(1);
		tagpropertiesokbtn.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[1]/div/input")
	private WebElement taglistpopupSearchTextField;

	@FindBy(xpath = "//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li[167]/a/label[text()='"
			+ Config.TagNameDeviceModelName + "']")
	private WebElement SelectSearchedTagName1;

//	@FindBy(xpath="//*[@id='taglistpopup']/div/div/div/div/span/div/ul/li/a/label[contains(text(),'"+Config.TagNameDevicePlatFormNameRightClick+"')]")
//	private WebElement SelectSearchedTagName2;

	/*
	 * @FindBy(xpath =
	 * "//*[@id='taglistpopup']/div/div/div/div/span/div/ul/li/a/label[contains(text(),'"+
	 * Config.TagNameDevicePlatFormName + "')]") private WebElement
	 * SelectSearchedTagName2;
	 */

	@FindBy(xpath = "//*[@id='taglistpopup']/div/div/div[2]/div/span/div/ul/li/a/label[text()='"
			+ Config.TagNameRightClick + "']")
	private WebElement SelectSearchedTagName3;

	/*
	 * public void taglistpopupSearchTextField1() throws InterruptedException {
	 * taglistpopupSearchTextField.sendKeys(Config.TagNameDeviceModelName);
	 * sleep(2);
	 * 
	 * }
	 */
	public void taglistpopupSearchTextField2(String TagNameSearch) throws InterruptedException {
		taglistpopupSearchTextField.sendKeys(TagNameSearch); // MARSHMALLOW [Android]
		sleep(2);

	}

	public void EnableTheSearchedTagNameFromTheList1() throws InterruptedException {
		String a = SelectSearchedTagName1.getText();
		System.out.println(a);
		sleep(2);
		SelectSearchedTagName1.click();
		sleep(4);
	}

	public void EnableTheSearchedTagNameFromTheList2(String tag) throws InterruptedException {
		String a = Initialization.driver.findElement(By.xpath("//label[contains(text(),'" + tag + "')]//input"))
				.getText();
		System.out.println(a);
		sleep(2);
		Initialization.driver.findElement(By.xpath("//label[contains(text(),'" + tag + "')]//input")).click();
		sleep(4);
	}

	public void EnableTheSearchedTagNameFromTheList3() throws InterruptedException {
		String a = SelectSearchedTagName3.getText();
		System.out.println(a);
		sleep(2);
		SelectSearchedTagName3.click();
		sleep(4);
	}

	// Clicking On Enrollment button
	@FindBy(xpath = "//*[@id='enrollmentDevices']/a/p")
	private WebElement QRCodeEnrollment;

	public void ClickQRCodeEnrollment() throws InterruptedException {
		QRCodeEnrollment.click();
		sleep(120);
	}

	// delete JObIn TAG

	@FindBy(xpath = "//*[@id='TagJobGrid']/tbody/tr/td[2]/p[text()='FileJob']")
	private WebElement FileJobDelete;

	@FindBy(xpath = "//*[@id='Tag_properties_dltbtn']/i")
	private WebElement DeleteBtn;

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']")
	private WebElement YesButton;

	@FindBy(xpath = "//*[@id='tagPropertiesDialog']/div/div/div[1]/button")
	private WebElement CloseButtonInTagPropertiesTab;

	@FindBy(xpath = "//*[@id='TagJobGrid']/tbody/tr/td[9]/span/input")
	private WebElement ClickOnForceApplyOnReRegistration;

	public void DeleteFileJobFromTagProperties(String Job, String tag) throws InterruptedException {
		// FileJobDelete.click();
		try {
			Initialization.driver.findElement(By.xpath("//*[@id='TagJobGrid']/tbody/tr/td[2]/p[text()='" + Job + "']"))
					.isDisplayed();

			Initialization.driver.findElement(By.xpath("//*[@id='TagJobGrid']/tbody/tr/td[2]/p[text()='" + Job + "']"))
					.click();

			sleep(2);
			DeleteBtn.click();
			YesButton.click();
			waitForXpathPresent("//span[text()='Job deleted successfully.']");
			sleep(2);

		} catch (Exception e) {
			CloseButtonInTagPropertiesTab.click();
			ALib.AssertFailMethod("FAIL >> Job is not present in Tag Properties Tab");
		}

		CloseButtonInTagPropertiesTab.click();
		sleep(2);
		waitForXpathPresent("//*[@id='applyJobButton']/i");
	}

//Kavya
	public void EnableForcereapplyOnReregistartaion() throws InterruptedException {
		ClickOnForceApplyOnReRegistration.click();
		sleep(2);
		try {
			ClickOnForceApplyOnReRegistration.isEnabled();
			Reporter.log("PASS >> Force reapply on re-registartaion checkbox enabled is successfully..", true);
		} catch (Exception e) {
			tagpropertiesokbtn.click();
			sleep(2);
			ALib.AssertFailMethod("FAIL >> Force reapply on re-registartaion checkbox is not enabled successfully..");
		}
		tagpropertiesokbtn.click();
		sleep(2);
	}

//Kavya
	@FindBy(xpath = "//*[@id='groupsbuttonpanel']/div/ul/li[2]/a")
	private WebElement Tags1;

	public void ClickOnTags1() throws InterruptedException {
		Tags1.click();
		sleep(4);
		Reporter.log("PASS>>Navigated To Tags Tab", true);
		try {
			Initialization.driver.findElement(By.xpath("(//span[text()='Tags'])[last()]")).isDisplayed();
			Reporter.log("PASS >> 'Tag' button is displayed correctly...", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> 'Tag' button is not displayed correctly...");
		}
	}

	@FindBy(xpath = "//*[@id='tagsListMainBlock']/ul/li[contains(text(),'#Auto0001')]")
	private WebElement ScrollToTagName;

	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	public void ScrollToCreatedTagName() throws InterruptedException {

		WebElement scrollDown = ScrollToTagName;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

	}

	@FindBy(xpath = "//*[@id='tableContainer']/div[7]/div/div[contains(text(),'No device available in this tag.')]")
	private WebElement NodeviceAvailableMessage;

	@FindBy(xpath = "//*[@id='dataGrid']/tbody/tr/td/p[contains(text(),'" + Config.DeviceNameTag + "')]")
	private WebElement DeviceModelName;

	public void VerifyDeviceNotPresentInsideTags_UM() throws InterruptedException {

		ScrollToTagName.click();
		sleep(2);

		String result = NodeviceAvailableMessage.getText();
		String Expected = "No device available in this tag.";
		String Pass = "PASS>> 'No device available in this tag.'- is Displayed";
		String Fail = "FAIL>>'No device available in this tag.' -is NOt Displayed";
		ALib.AssertEqualsMethod(result, Expected, Pass, Fail);
		sleep(2);

	}

	public void VerifyDevicePresentInsideTags_UM() throws InterruptedException {

		ScrollToTagName.click();
		sleep(2);

		String result = DeviceModelName.getText();
		String Expected = Config.DeviceNameTag;
		String Pass = "PASS>> 'Device available in this tag.'- is Displayed";
		String Fail = "FAIL>>'No device available in this tag.' -is NOt Displayed";
		ALib.AssertEqualsMethod(result, Expected, Pass, Fail);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='taglistpopup']/div/div/div/div/span/div/ul/li/a/label[contains(text(),' A37fw')]")
	private WebElement TagEnable;

	public void EnableTag() throws InterruptedException {
		TagEnable.click();
		sleep(2);

	}

	public void ClickOnTagMultipleDevices(String tag) throws InterruptedException {
		// FirstTagName.click();
		Initialization.driver.findElement(By.xpath("//li[text()='" + tag + "']")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);

	}

	public void verifyDeviceInTag(String Dev) {
		boolean a = Initialization.driver.findElement(By.xpath("//p[text()='" + Dev + "']")).isDisplayed();
		String pass = "Device move to tag successfully, device is present inside tag";
		String fail = "Device is not present inside tag, device not moved to tag";
		ALib.AssertTrueMethod(a, pass, fail);

	}

	public void RemoveMultipleDeviceFromTag() throws InterruptedException {

		Actions act = new Actions(Initialization.driver);
		act.contextClick(Device).perform();
		sleep(1);
		act.moveToElement(Initialization.driver.findElement(By.xpath("//ul[@id='customMenu']//span[text()='Tags']")))
				.perform();
		sleep(1);
		act.moveToElement(RemoveTag).perform();

		RemoveTag.click();
		waitForXpathPresent("(//h4[text()='Tag List'])[1]");
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[1]/div[1]/button")
	private WebElement RefreshBtnTags;

	public void VerifyDeviceNotPresentInTags() {

		RefreshBtnTags.click();
		waitForVisibilityOf("//div[text()='No device available in this tag.']");
		Reporter.log("'No device available in this tag.'", true);

	}

	public void DeleteTagFirstTagName() throws InterruptedException {
		FirstTagName.click();
		sleep(4);
		DeletTag.click();
		waitForXpathPresent(".//*[@id='deleteGroupConf']/div/div/div[1]/p");
		sleep(2);
		DeleteTagYes.click();
		// sleep(4);
		waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
		// waitForVisibilityOf("Deleted tag successfully.");
		boolean isdisplayed = true;
		try {
			DeleteTagMsg.isDisplayed();
		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed, "PASS:Tag Deleted successfully. is  displayed");
		Reporter.log("FAIL>> Alert Message : 'Tag Deleted successfully.' is not displayed", true);
		sleep(3);
	}

	@FindBy(xpath = "//li[text()='" + Config.TagNameDevicePlatFormName + "']")
	private WebElement TagNameDevicePlatFormName;

	public void DeleteTag_TagNameDevicePlatFormName() throws InterruptedException {
		TagNameDevicePlatFormName.click();
		sleep(4);
		DeletTag.click();
		waitForXpathPresent(".//*[@id='deleteGroupConf']/div/div/div[1]/p");
		sleep(2);
		DeleteTagYes.click();
		// sleep(4);
		waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
		// waitForVisibilityOf("Deleted tag successfully.");
		boolean isdisplayed = true;
		try {
			DeleteTagMsg.isDisplayed();
		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed, "PASS:Tag Deleted successfully. is  displayed");
		Reporter.log("FAIL>> Alert Message : 'Tag Deleted successfully.' is not displayed", true);
		sleep(3);
	}

//new methods 
	public void ApplyJobOnTag(String tag) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + tag + "']"))
				.click();
		sleep(4);
		Initialization.driver.findElement(By.xpath("//div[@id='ApplyJobtoGroup']")).click();

	}

	public void DeleteTag(String tag) throws InterruptedException {
		/*
		 * Initialization.driver.findElement(By.xpath(
		 * "//div[@id='tagsListMainBlock']/ul//li[text()='"+tag+"']")).click();
		 * sleep(4); DeletTag.click();
		 * waitForXpathPresent(".//*[@id='deleteGroupConf']/div/div/div[1]/p");
		 * sleep(2); DeleteTagYes.click(); //sleep(4);
		 * waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
		 * boolean isdisplayed=true; try{ Initialization.driver.findElement(By.xpath(
		 * "//div[@id='tagsListMainBlock']/ul//li[text()='"+tag+"']")).isDisplayed();
		 * }catch(Exception e) { isdisplayed=false; }
		 * Assert.assertTrue(isdisplayed,"PASS:"+
		 * tag+" Deleted successfully. is  displayed");
		 * Reporter.log("FAIL>> Alert Message : '"
		 * +tag+" Deleted successfully.' is not displayed",true ); sleep(3);
		 */

		Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + tag + "']"))
				.click();
		sleep(4);
		DeletTag.click();
		waitForXpathPresent(".//*[@id='deleteGroupConf']/div/div/div[1]/p");
		sleep(2);
		DeleteTagYes.click();
		sleep(2);
		waitForXpathPresent("//span[contains(text(),'deleted successfully.')]");
		try {

			Initialization.driver
					.findElement(By.xpath("//span[contains(text(),'[" + tag + "] deleted successfully.')]"))
					.isDisplayed();
			Reporter.log("PASS >> deleted successfully. is  displayed", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("deleted successfully. is not displayed");
			sleep(5);
		}

	}

	public void selectTag(String tag) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + tag + "']"))
				.click();
		sleep(4);
	}

	public void SuccessfulMessageOnRemovingDeviceFromTag() throws InterruptedException {
		try {
			DeviceRemoveTagMsg.isDisplayed();
			Reporter.log("PASS >> 'Device(s) removed from tag(s) successfully.' message displayed successfully", true);
			sleep(2);
			waitForidPresent("deleteDeviceBtn");
		} catch (Exception e) {
			ALib.AssertFailMethod(
					"FAIL >> 'Device(s) removed from tag(s) successfully.' message not displayed successfully");
			sleep(2);
		}
	}

	public void VerifyErrorMessageForZLGOTAAndEFOTA(String Tagname1, String Tagname2) throws InterruptedException {
		try {
		NewTag.click();
		waitForidPresent("groupDialog");
		sleep(3);
		NewTagName.sendKeys(Tagname1);
		OK.click();
		sleep(2);
		boolean value = Initialization.driver
				.findElement(By.xpath("//span[text()='Tag name should not contain ZLGOTA text.']")).isDisplayed();
		String PassStatement = "PASS >> 'Tag name should not contain ZLGOTA text' is displayed ";
		String FailStatement = "Fail >> 'Tag name should not contain ZLGOTA text.' is not displayed ";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(5);
		NewTagName.clear();
		sleep(2);
		NewTagName.sendKeys(Tagname2);
		OK.click();
		sleep(2);
		boolean value1 = Initialization.driver
				.findElement(By.xpath("//span[text()='Tag name should not contain EFOTA text.']")).isDisplayed();
		String PassStatement1 = "PASS >> 'Tag name should not contain EFOTA text' is displayed ";
		String FailStatement1 = "Fail >> 'Tag name should not contain EFOTA text' is not displayed ";
		ALib.AssertTrueMethod(value1, PassStatement1, FailStatement1);
		sleep(3);
		}catch (Exception e) {
			Initialization.driver.findElement(By.xpath("//div[@id='groupDialog']//button[@aria-label='Close']")).click();
			waitForidClickable("deleteDeviceBtn");
			ALib.AssertFailMethod(" 'Tag name should not contain EFOTA/ZLGOTA text.' is not displayed. ");
			sleep(2);
		}

	}

	public void CloseAddingNewTagPopup() throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//div[@id='groupDialog']//button[@aria-label='Close']")).click();
		waitForidClickable("deleteDeviceBtn");
		sleep(2);
	}

	@FindBy(xpath = "//div[@id='taglistpopup']/div/div/div[1]/button")
	private WebElement tagListCloseButton;

	public void ClikOnTaglistCloseButton() throws InterruptedException {

		tagListCloseButton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
	}

	/*
	 * @FindBy(xpath="//*[@id='deleteGroup']") private WebElement deleteTag;
	 * 
	 * @FindBy(xpath="//*[@id='deletegroupbtn']") private WebElement deleteTagbtn;
	 * 
	 * public void DeleteCreatedTag() throws InterruptedException {
	 * deleteTag.click();
	 * waitForXpathPresent("//*[@id='deleteGroupConf']/div/div/div[1]/p[text()='Are you sure you want to delete the tag?']"
	 * ); sleep(2); deleteTagbtn.click(); sleep(2); }
	 */

	// Kavya

	public void clickOnEFOTAtagAndEdit(String TagName) {
		try {
			if (Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[contains(text(),'" + TagName + "')]")).isDisplayed()) {
				Reporter.log(">> EFOTA Tags are available in console", true);
				Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[contains(text(),'" + TagName + "')]")).click();
				try {
					Initialization.driver.findElement(By.xpath("//div[@id='renameGroup' and @class='col-xs-3 col-md-3 groupsactionbutton disabledclass']")).isDisplayed();
					Reporter.log("PASS >> User is not be able to rename the tag created.", true);
				} catch (Exception e) {
					ALib.AssertFailMethod("FAIL >> User is be able to rename the tag created.");
				}
			} else {
				ALib.AssertFailMethod("FAIL >> >> EFOTA Tags are not available in console");
			}
		} catch (Exception e) {
			Reporter.log(">> EFOTA Tags are not available in console", true);
		}
	}

	public void clickOnEFOTAorZLGOTAtagAndDelete(String TagName) {
		try {
			if (Initialization.driver
					.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[contains(text(),'" + TagName + "')]"))
					.isDisplayed()) {
				Reporter.log(">> EFOTA/ZLGOTA Tags are available in console", true);
				Initialization.driver
						.findElement(
								By.xpath("//div[@id='tagsListMainBlock']/ul//li[contains(text(),'" + TagName + "')]"))
						.click();
				try {
					Initialization.driver.findElement(By.xpath(
							"//div[@id='deleteGroup' and @class='col-xs-3 col-md-3 groupsactionbutton disabledclass']"))
							.isDisplayed();
					Reporter.log("PASS >> Delete tag button is greyed out.", true);
				} catch (Exception e) {
					ALib.AssertFailMethod("FAIL >> Delete tag button is greyed out.");
				}
			} else {
				ALib.AssertFailMethod("FAIL >> >> EFOTA/ZLGOTA Tags are not available in console");
			}
		} catch (Exception e) {
			Reporter.log(">> EFOTA/ZLGOTA Tags are not available in console", true);
		}
	}

	public void verifyDevicesForMulValSearch(String Dev1, String Dev2) throws InterruptedException {
		boolean a = Initialization.driver.findElement(By.xpath("//p[normalize-space()='" + Dev1 + "']")).isDisplayed();
		String pass = "Device/Model/Note is displayed in the expected way...";
		String fail = "Device/Model/Note is not displayed in the expected way...";
		ALib.AssertTrueMethod(a, pass, fail);

		sleep(2);

		boolean b = Initialization.driver.findElement(By.xpath("//p[normalize-space()='" + Dev2 + "']")).isDisplayed();
		String pass1 = "Device/Model/Note is displayed in the expected way...";
		String fail1 = "Device/Model/Note is not displayed in the expected way...";
		ALib.AssertTrueMethod(b, pass1, fail1);

	}

	public void verifyOfErrorMsgForNoDeviceInTag() {
		try {
			Initialization.driver
					.findElement(By.xpath(
							"//span[text()='There are no devices that meet the filter criteria you have selected.']"))
					.isDisplayed();
			Reporter.log(
					"PASS >> 'There are no devices that meet the filter criteria you have selected.' is displayed....",
					true);
		} catch (Exception e) {
			ALib.AssertFailMethod(
					"FAIL >> 'There are no devices that meet the filter criteria you have selected.' is not displayed....");
		}
	}

	public void verifyofenablingAdvancessearchinTags() {
		try {
			Initialization.driver.findElement(By.xpath("//*[@id='tableContainer']//tr[@class='advSearchRow']"))
					.isDisplayed();
			Reporter.log("PASS >> Search text field on Top of each column is appeared.", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL >>  Search text field on Top of each column is not appeared.");
		}
	}

	ArrayList<String> taglist1 = new ArrayList<>();

	ArrayList<String> taglist2 = new ArrayList<>();

	@FindBy(xpath = "//div[@id='tagsListMainBlock']/ul/li")
	private List<WebElement> tags;

	public void getTagsListFromTagsSection() {
		for (int i = 0; i < tags.size(); i++) {
			taglist1.add(tags.get(i).getText());

		}
		// System.out.println(taglist1);
	}

	@FindBy(xpath = "//*[@id='taglistpopup']//ul[@class='multiselect-container dropdown-menu']/li/a/label")
	private List<WebElement> tags1;

	public void getTagsListFromTagsSection_RightClick() {
		for (int i = 1; i < tags1.size(); i++) {
			taglist2.add(tags1.get(i).getText());

		}
		// System.out.println(taglist2);
	}

	public void compareTagsList() {
		for (int i = 0; i < taglist2.size(); i++) {
			if (taglist1.equals(taglist2)) {			
				Reporter.log("PASS >> Order of the tags in the tags list pop up is matched with order of tags in the tags section.");
			} else {
				tagListCloseButton.click();
				ALib.AssertFailMethod("FAIL >> Order of the tags in the tags list pop up is not matched with order of tags in the tags section.");
			}
			
			//Initialization.driver.findElement(By.xpath("//div[@id='taglistpopup']//button[@aria-label='Close']")).click();
		}
		tagListCloseButton.click();
		Reporter.log("PASS >> Order of the tags in the tags list pop up is matched with order of tags in the tags section.",true);
		
	}
	
	
	public void VerifyDefaultTagInTagsSection(String devicemodel) throws InterruptedException {
			try {
				Initialization.driver.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + devicemodel + "']"))
						.isDisplayed();
				sleep(4);
				Initialization.driver
						.findElement(By.xpath("//div[@id='tagsListMainBlock']/ul//li[text()='" + devicemodel + "']")).click();
				sleep(2);
			Reporter.log("PASS>>Default Tags is created with device model name and Platform Name in Tags section.",true);
			sleep(2);
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL>> Default Tags is not created with device model name and Platform Name in Tags section.");
		}
	
	}
}
