package PageObjectRepository;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Common.Initialization;
import Library.AssertLib;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class Message_POM extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(id="appBtn2")
	private WebElement DynamicAppsMoreBtn;
	
	@FindBy(id="messageDeviceBtn")
	private WebElement DynamicMessageBtn;
	
	@FindBy(id="deviceMessageSubject")
	private WebElement SubjectTextField;
	
	@FindBy(id="deviceMessageBody")
	private WebElement BodyTextField;
	
	@FindBy(id="deviceMessageForceRead")
	private WebElement ForceReadMessageRadioBtn;
	
	@FindBy(xpath="//input[@value='Send']")
	private WebElement SendBtn;
	
	@FindBy(xpath="//span[text()='Successfully delivered Message to device.']")
	private WebElement ConfirmationMessage;
	
	public void ClickOnMore() throws InterruptedException
	{
		DynamicAppsMoreBtn.click();
		waitForidPresent("messageDeviceBtn");
		sleep(2);
	}
	
	public void ClickOnMessage() throws InterruptedException
	{
		DynamicMessageBtn.click();
		waitForidPresent("deviceMessageSubject");
		sleep(2);
	}
	
	public void EnterMessageSubject() throws EncryptedDocumentException, InvalidFormatException, IOException{
		SubjectTextField.sendKeys("SureMDM");
	}
	
	public void EnterMessageBody() throws EncryptedDocumentException, InvalidFormatException, IOException{
		BodyTextField.sendKeys("SureMDM");
	}
	
	public void ClickOnForceReadMessage()
	{
		ForceReadMessageRadioBtn.click();
		
	}
	
	public void ClickOnSend()
	{
		SendBtn.click();
		
	}
	
	public void ConfirmationMessageVerify(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Successfully delivered Message to device.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Successfully delivered Message to device.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessage, value, PassStatement, FailStatement,5);
	}
	
}
