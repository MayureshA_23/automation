package SanityTestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class RemoteSupportAndroid_TestScripts extends Initialization {
	@Test(priority=0,description="Verify Android remote support")
	public void RemoteSupport_Android() throws Throwable
	{
		Reporter.log("=====1.Verify Android remote support=====",true);
		accountsettingspage.GoToMiscSettings();
		accountsettingspage.UnSelectUseOldRemoteSupport();
		accountsettingspage.ClickOnMiscellaneousApplyBtn();
		accountsettingspage.ConfirmationMessageVerify(true);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.VerifyOfRemoteSupportLaunched();
		System.out.println("");
		remotesupportpage.SwitchToMainWindow();
	}
	@Test(priority=1,description="Verify uploading and downloading of files in remote support (Validate for multiple files )")
	public void RemoteSupport_UploadAndDownloadFiles() throws Throwable
	{
		Reporter.log("=====2.Verify uploading and downloading of files in remote support (Validate for multiple files )=====",true);
		commonmethdpage.SearchDevice(Config.DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.ClickingOnUploadButtonInRemoteSupport();
		remotesupportpage.ClickingOnFileBrowseButton();
		remotesupportpage.UploadingFileInRemoteSupport("D:\\UplaodFiles\\Remote Support\\File1.exe");
		remotesupportpage.ClickingOnUploadCancelButton();
		remotesupportpage.SwitchToMainWindow();
	}
	@Test(priority='2',description="Verify uploading and downloading of files in remote support (Validate for multiple files )")
	public void VerifyDownloadingMultipeFilesInRemoteSupport() throws Throwable
	{
		Reporter.log("=====3.Verify uploading and downloading of files in remote support (Validate for multiple files )=====",true);
		commonmethdpage.SearchDevice(Config.DeviceName1);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.SelectingMultipleFilesInRemoteSupport("Samsung","Android");      
		remotesupportpage.SwitchToMainWindow();
	}
	
}
