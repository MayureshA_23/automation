package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//import org.apache.poi.EncryptedDocumentException;
//import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import Common.Initialization;
import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
//import Library.ExcelLib;
import Library.Helper;
import Library.PassScreenshot;
import Library.WebDriverCommonLib;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class GridPaginationPOM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();

	@FindBy(xpath = "//a[text()='20']")
	private WebElement Select20;
	int totalDevicesConsole;
	public void chooseDevicesPerPage20() throws InterruptedException {
		try {
		Select20.click();
		sleep(7);
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 90);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		if(totalDevices<=DeviceCountInGrid) {
		int expectedDeviceCount = 20;
		String PassStatement2 = "PASS >> Total device count for page when 20 per is selected is: " + expectedDeviceCount
				+ "  ";
		String FailStatement2 = "FAIL >> Total device count for page when 20 per is selected is Wrong";
		ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
		sleep(5);}else {
			Reporter.log("Device Count in grid is less than 20", true);
		}}catch(Exception e) {
			Reporter.log("Pagination is not displayed", true);
		}

	}

	// ############################################################################
	@FindBy(xpath = "//a[text()='1']")
	private WebElement PageNumber1;

	@FindBy(xpath = "//a[text()='2']")
	private WebElement PageNumber2;

	@FindBy(xpath = "//a[text()='3']")
	private WebElement PageNumber3;

	@FindBy(xpath = "//a[text()='100']")
	private WebElement Select100;

	@FindBy(xpath = "//a[text()='500']")
	private WebElement Select500;

	@FindBy(xpath = "//a[text()='50']")
	private WebElement Select50;

	@FindBy(xpath = ".//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/button")
	private WebElement DevicesPerPageButton;

	@FindBy(xpath = "//a[text()='Reports']")
	private WebElement reportButton;

	@FindBy(id = "userProfileButton")
	private WebElement settings;

	@FindBy(xpath = "//div[@class='cover']/h3")
	private WebElement toastWhenNoOptionSelected;

	public void ClickOnDevicesPerPagebutton() {
		DevicesPerPageButton.click();

	}

	public void chooseDevicesPerPage50() throws InterruptedException {
		try {
		Select50.click();
		sleep(10);
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		
		
		
		if(totalDevices<=DeviceCountInGrid) {
			int expectedDeviceCount = 50;
			String PassStatement2 = "PASS >> Total device count for page is: " + expectedDeviceCount + "  ";
			String FailStatement2 = "FAIL >> Total device count for page is Wrong";
			ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
			sleep(5);}else {
				Reporter.log("Device Count in grid is less than 50", true);
			}
	}catch(Exception e) {
		Reporter.log("Pagination option 50 is not displayed", true);
	}
	}

	public void logoutfromApp() throws InterruptedException {
		settings.click();
		waitForidPresent("generateQRCode");
		sleep(5);
		logoutBtn.click();
		sleep(4);

	}

	@FindBy(xpath = "//a[text()='New User? Sign Up']")
	private WebElement ClickOnNewUserSignUpLink;

	@FindBy(id = "logoutButton")
	private WebElement logoutBtn;

	@FindBy(id = "uName")
	private WebElement userNAmeEdt;

	@FindBy(id = "pass")
	private WebElement passwordEdt;

	@FindBy(id = "loginBtn")
	private WebElement loginBtn;

	public void logoutAsAdmin() throws InterruptedException {
		logoutBtn.click();
		waitForidPresent("forgot_password");
		sleep(2);
	}

	public void LoginAgain() throws InterruptedException {

		Initialization.driver.findElement(By.xpath("//*[@id=\"newdnsmessage\"]/a")).click();
		sleep(4);
		System.out.println("Entering UN PWD");
		Helper.highLightElement(Initialization.driver, userNAmeEdt);
		userNAmeEdt.sendKeys(Config.userID);
		Helper.highLightElement(Initialization.driver, passwordEdt);
		passwordEdt.sendKeys(Config.password);
		Helper.highLightElement(Initialization.driver, loginBtn);
		loginBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		System.out.println("Logged in to SureMDM successfully");
	}

	public void chooseDevicesPerPage100() throws InterruptedException {
		try {
		Select100.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dataGrid']/tbody/tr[1]")));
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		Reporter.log("Total number of devices being shown when 100 per page is selected:" + totalDevices + " ",true);
		sleep(5);}catch(Exception e) {
			Reporter.log("Pagination 100 is ");
		}
	}

	public void VerifyNumberOfDevicesAfterActions() throws InterruptedException {
		// Checking the number of devices after logout and login and other actions
		List<WebElement> deviceCount = Initialization.driver.findElements(By.xpath(".//*[@id='dataGrid']/tbody/tr"));
		int totalDevices = deviceCount.size();
		int expectedDeviceCount = 50;
		String PassStatement2 = "PASS >> Total device count for page is: " + expectedDeviceCount + "  ";
		String FailStatement2 = "FAIL >> Total device count for page is Wrong";
		ALib.AssertEqualsMethodInt(expectedDeviceCount, totalDevices, PassStatement2, FailStatement2);
		sleep(5);
	}

	public void ClickOnReports_Button() throws InterruptedException {
		reportButton.click();

		sleep(5);
		String Actualvalue = toastWhenNoOptionSelected.getText();
		String Expectedvalue = "No option is selected";
		String PassStatement = "PASS >> Clicked on Reports and " + Expectedvalue + "  displayed successfully";
		String FailStatement = "FAIL >> Either Reports not clicked or " + Expectedvalue + "is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		Reporter.log("");
		sleep(2);

	}
	
	@FindBy(xpath="(//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/ul/li)[last()]")
	private WebElement SelectLastPagination;
	
	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[2]/div[4]/div[1]/span[2]/span[1]/button")
	private WebElement Pagination;
	public void SelectLastPagination() throws InterruptedException {
		if(Pagination.isDisplayed()) {
			Pagination.click();
			SelectLastPagination.click();
		}}
	
	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[2]/div[4]/span")
	private WebElement deviceCountOnGrid;
	int DeviceCountInGrid;
	public void GetDeviceCount() throws InterruptedException {
		String[] devicountOnlineOffline = deviceCountOnGrid.getText().split(",");
		 DeviceCountInGrid=Integer.parseInt(devicountOnlineOffline[0].replaceAll("\\D", ""));
	}
	
}
