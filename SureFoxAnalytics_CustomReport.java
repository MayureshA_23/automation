package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class SureFoxAnalytics_CustomReport extends Initialization {
	@Test(priority='0',description="Verify Duration column in SureFox Anaytics Report should be displayed as HH:MM:SS") 
	public void VerifySureFoxAnalytics_TC_RE_223() throws InterruptedException
		{Reporter.log("\n1.Verify Duration column in SureFox Anaytics Report should be displayed as HH:MM:SS",true);
			commonmethdpage.ClickOnSettings();
			commonmethdpage.ClickonAccsettings();
			accountsettingspage.ClickOnDataAnalyticsTab();
			accountsettingspage.EnableDataAnalyticsCheckBox();
			accountsettingspage.DisablingSureFoxAnalytics();
			accountsettingspage.EnablingSureFoxAnalytics();
			accountsettingspage.ClickOnDataAnalyticsApplyButton();
			accountsettingspage.VerifySureFoxAnalyticsEnablePopup();
			accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
			commonmethdpage.ClickOnHomePage();
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureFoxAnalytics CustRep to verify DurationCol In ViewRep","test");
			customReports.ClickOnColumnInTable("SureFox Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureFoxAnalytics CustRep to verify DurationCol In ViewRep");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureFoxAnalytics CustRep to verify DurationCol In ViewRep");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureFoxAnalytics CustRep to verify DurationCol In ViewRep");
			reportsPage.WindowHandle();
			customReports.ChooseDevicesPerPage();
			customReports.CheckingDurationColDateFormat_InSureFoxAnalytics();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	@Test(priority='1',description="Verify duration column filter for the SureFox Analytics report") 
	public void VerifySureFoxAnalytics_TC_RE_227() throws InterruptedException
		{Reporter.log("\n2.Verify duration column filter for the SureFox Analytics report",true);
			/*commonmethdpage.ClickOnSettings();
			commonmethdpage.ClickonAccsettings();
			accountsettingspage.ClickOnDataAnalyticsTab();
			accountsettingspage.EnableDataAnalyticsCheckBox();
			accountsettingspage.DisablingSureVideoAnalytics();
			accountsettingspage.EnablingSureVideoAnalytics();		
			accountsettingspage.ClickOnDataAnalyticsApplyButton();	
			accountsettingspage.VerifySureVideoAnalyticsEnablePopup();
			accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();*/
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.ClickOnColumnInTable("SureFox Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureFoxAnalytics CustRep duration column filter","test");
			customReports.Selectvaluefromdropdown("SureFox Analytics");
			customReports.SelectValueFromColumn("Duration");
			customReports.SelectOperatorFromDropDown("<=");
			customReports.SendValueToTextfield(Config.DurationFormat3);
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureFoxAnalytics CustRep duration column filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureFoxAnalytics CustRep duration column filter");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureFoxAnalytics CustRep duration column filter");
			reportsPage.WindowHandle();
			customReports.ChooseDevicesPerPage();
			customReports.CheckingDurationColDateFormat_InSureFoxAnalytics();
			customReports.VerifyingDurationColForGivenRange_InSureVideoAnalytics();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	@Test(priority='2',description="Verify Modifying the SureLock/SureFox/SureVideo Analytics and verify Duration column") 
	public void VerifySureFoxAnalytics_TC_RE_229() throws InterruptedException
		{Reporter.log("\nVerify Modifying the SureLock/SureFox/SureVideo Analytics and verify Duration column",true);
			commonmethdpage.ClickOnHomePage();
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.SearchingReportInCustomReportsList("SureFoxAnalytics CustRep to verify DurationCol In ViewRep");
			customReports.ClickOnModifyButton();
			customReports.ClickOnAddMoreButton();
			customReports.Selectvaluefromdropdown("SureFox Analytics");
			customReports.SelectValueFromColumn("Duration");
			customReports.SelectOperatorFromDropDown("<=");
			customReports.SendValueToTextfield(Config.DurationFormat3);
			customReports.ClickOnUpdateButton();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureFoxAnalytics CustRep duration column filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureFoxAnalytics CustRep duration column filter");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureFoxAnalytics CustRep duration column filter");
			reportsPage.WindowHandle();
			customReports.ChooseDevicesPerPage();
			customReports.CheckingDurationColDateFormat_InSureFoxAnalytics();
			customReports.VerifyingDurationColForGivenRange_InSureVideoAnalytics();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
