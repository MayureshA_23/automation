package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class LockDevice extends Initialization  {

	@Test(priority=1, description="Verify error message while creating lock Device Job with all fields are empty.")
	public void CreateInstallApplicationJobWithAllFieldsEmpty() throws InterruptedException, IOException{

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnLockDevice();
		androidJOB.ClickOnOkButton();
		androidJOB.verifyBlankfieldTextmessage();
		androidJOB.ClickOnCloseBtnLockDeviceJob();

	}
	@Test(priority=2, description="Create Lock job by selecting LockDevice")
	public void VerifyCreateLock() throws InterruptedException, IOException{

		androidJOB.clickOnLockDevice();
        androidJOB.CreateLockJobForLockDeviceOption();
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();

	}
	@Test(priority=3, description="Create change password Lock Device job with empty fields  ")
	public void VerifyChangePasswordLockJobErrorMessage() throws InterruptedException, IOException{

		androidJOB.clickOnJobs();

		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnLockDevice();
		androidJOB.CreateLockJobChangePasswordButton();
		androidJOB.ClickOnOkButton();
		androidJOB.verifyBlankfieldTextmessage();
		androidJOB.LockJobEnterDevicePasswordErrorCapture();
		androidJOB.ClickOnCloseBtnLockDeviceJob();

	}
	@Test(priority=4, description="Create Lock job by selecting Change Password")
	public void VerifyCreateLockJobChangePassword() throws InterruptedException, IOException{

		androidJOB.clickOnLockDevice();

		androidJOB.CreateLockJobChangePassword(Config.Pwd);  //000000
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
	}

	@Test(priority=5, description="Apply Lock Device Job")
	public void ApplyLockJobLockDevice() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");

		commonmethdpage.ClickOnHomePage();
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
       commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("0lockJobChangePassword");
		androidJOB.CheckStatusOfappliedInstalledJob("0lockJobChangePassword",360);	
		androidJOB.ClearSearchFieldConsole();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("0lockJobLockDevice");
		androidJOB.CheckStatusOfappliedInstalledJob("0lockJobLockDevice",360);	
		commonmethdpage.VerifyIsDeviceLocked();
		dynamicjobspage.UnLockingDeviceUsingADB();
		androidJOB.LaunchNix();
		androidJOB.ClickOnHomeButtonDeviceSide();
	}	

}
