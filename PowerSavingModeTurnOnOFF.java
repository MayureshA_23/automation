package RunScript_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class PowerSavingModeTurnOnOFF extends Initialization{
	@Test(priority='1', description="Power saving mode should be disable .") 
	public void VerifyThatRunsciptToControlPowerSaverModeDeviceWithTrue_TC_AJ_236() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.clickOnPowerSavingModeOption();
		runScript.sendPackageNameORpath("true");
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("TurnOffPowerSaving");
		commonmethdpage.ClickOnHomePage();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("TurnOffPowerSaving");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("TurnOffPowerSaving",60);
		runScript.ScrollStatusbarOnDeviceSide();
		runScript.Powersaving_Disabled();
		runScript.DisabledPowerSaving();
		androidJOB.ClickOnHomeButtonDeviceSide();
	}
	@Test(priority='2', description="Power saving mode should be disable .") 
	public void VerifyThatRunsciptToControlPowerSaverModeAndroidSettingsOfTheDeviceWithFalse_TC_AJ_237() throws InterruptedException, IOException{
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		runScript.ClickOnRunscript();
		runScript.clickOnKnoxSection();
		runScript.clickOnPowerSavingModeOption();
		runScript.sendPackageNameORpath("false");
		runScript.ClickOnValidateButton();
		runScript.ScriptValidatedMessage();
		runScript.ClickOnInsertButton();
		runScript.RunScriptName("TurnOnPowerSaving");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("TurnOnPowerSaving");
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("TurnOnPowerSaving",60);
		runScript.ScrollStatusbarOnDeviceSide();
		runScript.Powersaving_Disabled();
		runScript.CheckEnablePowerSavingMode();
		androidJOB.ClickOnHomeButtonDeviceSide();
		
	}
}
