package OnDemandReports_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class SystemLogs extends Initialization {

	@Test(priority = 0, description = "PreCondition")
	public void PreCondition()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("PreCondition -Applying Job and Installing Application", true);

//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();
//		androidJOB.clickOnAndroidOS();
//		androidJOB.clickOnTextMessageJob();
//		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text", "Normal Text");
//		androidJOB.ClickOnOkButton();
//		androidJOB.JobCreatedMessage();
//		commonmethdpage.ClickOnHomePage();
//		commonmethdpage.SearchDevice(Config.DeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		commonmethdpage.SearchJob(Config.TextMessageJobname);
//		commonmethdpage.Selectjob(Config.TextMessageJobname);
//		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
//		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,
//				Config.DeviceName);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
//		reportsPage.installingApplicationOnDevice(
//				"./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\LED Flashlight Small size_v1.3_apkpure.com.apk");
//		reportsPage.RemoveAppFromDevice("com.bhanu.torch");

	}

	@Test(priority = 1, description = "1.Verify On Demand System Log report for 'Home' group for 'Current date'")
	public void Reports_SystemLogs_VerifyGeneratingReportsHomeGroup() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
//		Reporter.log("\n1.Verify On Demand System Log report for 'Home' group for 'Current date'", true);
//		reportsPage.ClickOnReports_Button();
//		reportsPage.ClickOnOnDemandReport();
//		reportsPage.systemreports();
//		reportsPage.CountOfUnreadReports();
//		reportsPage.ClickOnView();
//		reportsPage.WindowHandle();
//		reportsPage.VerifyOfReportOpenedInNextTab();
//		reportsPage.VarifyingCurrentDateInSystemLogOnDemandReport();
//		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
//		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
//		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
//		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
//		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 2, description = "2.Verify System Logs Reports Column Availibility")
	public void VerifySystemLogsReportsColumnsAvailibility() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n2.Verify On Demand System Log report for 'Home' group for 'Current date'", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifySystemLogReportColumns();
		reportsPage.VarifyingCurrentDateInSystemLogOnDemandReport();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 3, description = "3.Verify System Logs Reports column values")
	public void VerifySystemLogsReportsColumnsValue() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n3.Verify System Logs Reports column values", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 4, description = "4.Verify On Demand System Log report for 'Sub' group for 'Current date'")
	public void Reports_SystemLogs_VerifyGeneratingReportsSubGroup() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n4.Verify On Demand System Log report for 'Sub' group for 'Current date'", true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.VerifySystemLogReportColumns();
		reportsPage.VarifyingCurrentDateInSystemLogOnDemandReport();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 5, description = "5. Download for System Log Report")
	public void VerifyDownloadingDeviceConnectedReport() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n5.Download for System Log Report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@Test(priority = 6, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportHomeGroup_30Days() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n6.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully.");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 7, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportHomeGroup_Today() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n7.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_SystemLogRep();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 8, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportHomeGroup_Yesterday() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n8.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForYesterday();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 9, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportHomeGroup_Last1week() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n9.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 10, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportHomeGroup_ThisMonth() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n10.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 11, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportHomeGroup_CustomRange() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n11.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for home group",
				true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 12, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportSubGroup_Today() throws InterruptedException,
			ParseException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log(
				"\n12.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group",
				true);
		commonmethdpage.ClickOnHomePage();
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();

		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_SystemLogRep();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 13, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportSubGroup_Yesterday() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n13.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForYesterday();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 14, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportSubGroup_Last1week() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n14.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 15, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportSubGroup_30Days() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n15.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
//		reportsPage.VerifyOfUserNameInSystemLogReport(Config.userID);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 16, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportSubGroup_ThisMonth() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n16.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		// reportsPage.VerifyOfUserNameInSystemLogReport(Config.userID);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
//		reportsPage.VerifyOfUserNameInSystemLogReport(Config.userID);
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 17, description = "Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group")
	public void VerifyGenerationOfSessionLogsInOnDemandReportSubGroup_CustomRange() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log(
				"\n17.Verify generation of Session Logs in On Demand Reports with 'show all logs' filter for sub group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 18, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsHomeGroup_Today()
			throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, InterruptedException,
			IOException, ParseException {
		Reporter.log(
				"\n18.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_SystemLogRep();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 19, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsHomeGroup_Yesterday()
			throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, InterruptedException,
			IOException, ParseException {
		Reporter.log(
				"\n19.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForYesterday();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 20, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsHomeGroup_Last1week()
			throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, InterruptedException,
			IOException, ParseException {
		Reporter.log(
				"\n20.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 21, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsHomeGroup_30Days()
			throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, InterruptedException,
			IOException, ParseException {
		Reporter.log(
				"\n21.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 22, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsHomeGroup_ThisMonth()
			throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, InterruptedException,
			IOException, ParseException {
		Reporter.log(
				"\n22.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 23, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsHomeGroup_CustomRange()
			throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, InterruptedException,
			IOException, ParseException {
		Reporter.log(
				"\n23.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for HOME group",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 24, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsSubGroup_Today() throws InterruptedException,
			ParseException, NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log(
				"\n24.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_SystemLogRep();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 25, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsSubGroup_Yesterday()
			throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log(
				"\n25.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForYesterday();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 26, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsSubGroup_Last1week()
			throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log(
				"\n26.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 27, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsSubGroup_30Days()
			throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log(
				"\n27.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 28, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsSubGroup_ThisMonth()
			throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log(
				"\n28.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 29, description = "Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.")
	public void VerifyGenerationOfSessionLogsCombiningjobLogsAndSessionLogsSubGroup_CustomRange()
			throws InterruptedException, NumberFormatException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log(
				"\n29.Verify generation of Session Logs in On Demand Reports with combinations 'Show job logs' and 'Show user session logs' for Sub-group.",
				true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.DisablingCheckBox("ShowAllLogs");
		reportsPage.DisablingCheckBox("ShowAppInstallLogs");
		reportsPage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("System Log");
		accountsettingspage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange();
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_LogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfUserNameInSystemLogReport("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Device Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_LogMessage);
		reportsPage.Search_SystemLogTextBox(Config.SystemLogReport_SystemLogMessage);
		reportsPage.VerifyOfSystemLogReportDeviceName(Config.DeviceName);
		reportsPage.VerifyOfSystemLogReportLogType("System Log");
		reportsPage.VerifyOfSystemLogReportLogMessage(Config.SystemLogReport_SystemLogMessage);
		reportsPage.Search_SystemLogTextBox("User " + Config.userID + " logged in successfully");
		reportsPage.VerifyOfSystemLogReportDeviceName("N/A");
		reportsPage.VerifyOfSystemLogReportLogType("Session Log");
		reportsPage.VerifyOfSystemLogReportLogMessage("User " + Config.userID + " logged in successfully");
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 30, description = "Verify Session Logs filter is present in On Demand Reports ")
	public void VerifySessionLogFilterInOnDemandReports() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n30.Verify Session Logs filter is present in On Demand Reports ", true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		reportsPage.systemreports();
		reportsPage.VerifySessionLogFiltersInOnDemandReports();
		reportsPage.VerifySessionLogsFilterIsEnabledByDefaultInOnDemandReports();
	}

	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();

		}
	}

}
