package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class TimeFence_Job extends Initialization {

	
/*	@Test(priority = 1, description = "Precondition to create jobs")
	public void PreconditionForTimeFence()throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		androidJOB.clickOnJobs();
		deviceinfopanelpage.ClickOnNewFolder();
		deviceinfopanelpage.EnterAndSaveNewFolder("AutomationDontDeleteJobFolder");
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnNotificationPolicyJob();
		deviceinfopanelpage.CheckDisableNotificationPolicy(false);
		deviceinfopanelpage.EnterAndSaveNotificationPolicyJob("AutomationDontDeleteNotificationPolicy");
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.InstallJob);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileInstallApplication();
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
		androidJOB.clickNewJob();
		accountsettingspage.clickOnAnyOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("AutomationDontDeleteTextMessage", Config.Customize_Sub,Config.Customize_Message);
		androidJOB.ClickOnGetReadNotification();
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.enterJobName(Config.JobNameToApplyMiscSettings);
		androidJOB.clickOnAdd();
		androidJOB.browseFile();
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
	}
*/
	@Test(priority = 2, description = "Time Fence - UI Validation")
	public void TimeFenceUIValidation()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.VerifyEnableTimeFenceIsSelected(true);
		deviceinfopanelpage.VerifyTimeFenceTabs();
		deviceinfopanelpage.VerifySaveButtonInRightTopCorner();
	}

	@Test(priority = 3, description = "Time Fence - 'Select Fence' UI Validation.")
	public void TimeFenceSelectFenceUIValidation()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		deviceinfopanelpage.VerifySelectFenceUIValidation1();
		deviceinfopanelpage.VerifySelectFenceUIValidation2();
	}

	@Test(priority = 4, description = "Time Fence - Verify creating Time Fence Job with Fence entered and Fence exited.")
	public void TimeFenceVerifycreatingTimeFenceJobwithFenceenteredandFenceexited()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {

		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1");
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2");
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();

		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);

		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("TimeFenceJob");

		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("TimeFenceJob");
		androidJOB.Selectjob("TimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("TimeFenceJob", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
       deviceinfopanelpage.VerifyTimeFenceStatus("ON");

		deviceinfopanelpage.VerifyFenceExitAlertForDevice(Config.DeviceName, "Time fence", 500);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		deviceinfopanelpage.VerifyFenceEntryAlertForDevice(Config.DeviceName, "Time fence", 520);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Time fence");

		deviceinfopanelpage.InboxEnteryMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Time fence");
		commonmethdpage.ClickOnHomePage();

	}

	@Test(priority = 5, description = "Time Fence - Verify Disable enable Time Fence")
	public void TimeFenceVerifyDisableenableTimeFence()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		commonmethdpage.ClickOnHomePage();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(true);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("TimeFenceJobDisable");

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("TimeFenceJobDisable");
		androidJOB.Selectjob("TimeFenceJobDisable");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("TimeFenceJobDisable", 120);
		androidJOB.ClickOnJobQueueCloseButton();
        deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);


		deviceinfopanelpage.VerifyTimeFenceStatus("OFF");
//		deviceinfopanelpage.SleepFor40Seconds();
//      deviceinfopanelpage.refesh();

	}



	@Test(priority = 6, description = "Time Fence - Verify Creating time fence job by selecting Console Time.")
	public void TimeFenceVerifyCreatingtimefencejobbyselectingConsoleTime()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
//	    commonmethdpage.ClickOnHomePage();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.SelectDeviceTime(Config.DeviceTime);  //sleep(4);
    	deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1");
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2");
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();

		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.SelectFenceJob(Config.JobNameToApplyMiscSettings);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);

		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("TimeFenceJob");

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		deviceinfopanelpage.ClickOnBackButton();

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("TimeFenceJob");
		androidJOB.Selectjob("TimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("TimeFenceJob", 300);
		androidJOB.ClickOnJobQueueCloseButton();
		
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
       deviceinfopanelpage.VerifyTimeFenceStatus("ON");

		deviceinfopanelpage.VerifyFenceExitAlertForDevice(Config.DeviceName, "Time fence", 500);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		deviceinfopanelpage.VerifyFenceEntryAlertForDevice(Config.DeviceName, "Time fence", 520);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Time fence");

		deviceinfopanelpage.InboxEnteryMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Time fence");
		
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("TimeFenceJobDisable");
		androidJOB.Selectjob("TimeFenceJobDisable");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("TimeFenceJobDisable", 300);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);

		deviceinfopanelpage.VerifyTimeFenceStatus("OFF");

		deviceinfopanelpage.SleepFor40Seconds();
	}

	
	@Test(priority = 7, description = "Time Fence - Verify not adding jobs in Fence Entered and Fence Exited.")
	public void TimeFenceVerifynotaddingjobsinFenceEnteredandFenceExited()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		deviceinfopanelpage.ClickOnBackButton();

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.SelectDeviceTime(Config.ConsoleTime);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1");
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 14
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();

		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.VerifyErrorMessageWithoutJobAndAlert();

	}

	@Test(priority = 8, description = "8. Time Fence - Verify Delete fence in Time fence job")
	public void TimeFenceVerifyDeletefenceinTimefencejob()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		deviceinfopanelpage.ClickOnFenceDeleteButton();
		deviceinfopanelpage.ClickOnFenceDeleteConfirmationYesButton();
		deviceinfopanelpage.VerifyDeletedFence();
		deviceinfopanelpage.ClickOnCloseButtonOfTimeFence();
	}

	@Test(priority = 9, description = "9. Time Fence - Verify Modifying the Time fence job.")
	public void TimeFenceVerifyModifyingtheTimefencejob()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		deviceinfopanelpage.ClickOnSelectJobTypeBackButton();
		deviceinfopanelpage.ClickOnSelectOSCancelButton();
		androidJOB.clickOnJobs();
		jobsOnAndroidpage.SearchJobFolder("TimeFenceJob");
		jobsOnAndroidpage.ClickOnJob("TimeFenceJob");
		deviceinfopanelpage.ClickOnModifyJob_TimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(true);
		deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.ModifyTimeFenceAndJobName("Modified", "TimeFenceJobModified");

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		dynamicjobspage.ClickOnrefreshButtonDeviceInfo();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("TimeFenceJobModified");
		androidJOB.Selectjob("TimeFenceJobModified");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("TimeFenceJobModified", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();

		commonmethdpage.SearchDevice(Config.DeviceName);

		deviceinfopanelpage.VerifyTimeFenceStatus("OFF");
		deviceinfopanelpage.SleepFor40Seconds();
		
	}
	
/*	@Test(priority = 'A', description = "10. Time Fence - Verify Creating Time fence job with Composite job added in Fence Exited")
	public void CreatingCompositeJob()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		
       androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.FileTransferJobName);
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe");
		androidJOB.EnterDevicePath("/sdcard/Download");
        androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("comp_job");
		androidJOB.CompositeJobName(Config.CompositeJobName);
		androidJOB.ClickOnAddButtonCompositeJob();
		androidJOB.SearchJobForCompositeAction(Config.TextMessageJobname);
		androidJOB.ClickOnAddButtonCompositeJob();
        androidJOB.SearchJobForCompositeAction(Config.FileTransferJobName);
		androidJOB.ClickOnOnOkButtonAndroidJob();
		androidJOB.JobCreatedMessage(); 
        commonmethdpage.ClickOnHomePage();
        
	}
        
    	@Test(priority = 'B', description = "11. Time Fence - Verify Creating Time fence job with Composite job added in Fence Exited")
    	public void TC_JO_793()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
    		


		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1"); // 19
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 20
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();
		


		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();

		deviceinfopanelpage.CheckFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceExited(false, "complianceEmailID@mailinator.com");
        deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("ExitTimeFenceJob");

		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("ExitTimeFenceJob");
		androidJOB.Selectjob("ExitTimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("ExitTimeFenceJob", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
        deviceinfopanelpage.VerifyTimeFenceStatus("ON");

		deviceinfopanelpage.VerifyFenceExitAlertForDevice(Config.DeviceName, "Time fence", 520);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Time fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();


		}
    	@Test(priority = 'C', description =" 12. Time Fence -Verify creating time fence job with composite job along with some other jobs like install/Text message added in fence exited")
    	public void TC_JO_797()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
    		


		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1"); // 19
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 20
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();
		


		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
        deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();


		deviceinfopanelpage.CheckFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceExited(false, "complianceEmailID@mailinator.com");
        deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("ExitTimeFenceJob");

		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("ExitTimeFenceJob");
		androidJOB.Selectjob("ExitTimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("ExitTimeFenceJob", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
        deviceinfopanelpage.VerifyTimeFenceStatus("ON");

		deviceinfopanelpage.VerifyFenceExitAlertForDevice(Config.DeviceName, "Time fence", 520);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Time fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();


		}

	@Test(priority = 'D', description = "13. Time Fence - Verify Creating Time fence job with Composite job(with delay added) added in Fence Exited.")
	public void TC_JO_795_creatingDelayCompositeJob()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {

        androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.FileTransferJobName);
		androidJOB.clickOnAdd();
		androidJOB.browseFile("./Uploads/UplaodFiles/FileTransfer/\\FileTransfer.exe");
		androidJOB.EnterDevicePath("/sdcard/Download");
        androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.JobCreatedMessage();
    	androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnJobType("comp_job");
		androidJOB.CompositeJobName(Config.CompositeJobNameEdited);
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.TextMessageJobname);
		deviceinfopanelpage.ClickOnDelayCompositeJob("10");
        androidJOB.SelectingJobToAddOutOfComplianceAction(Config.FileTransferJobName);
    	androidJOB.ClickOnOnOkButtonAndroidJob();
		androidJOB.JobCreatedMessage(); 
        commonmethdpage.ClickOnHomePage();
	}

    	@Test(priority = 'E', description = "14. Time Fence - Verify Creating Time fence job with Composite job(with delay added) added in Fence Exited.")
    	public void TC_JO_795()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {

//		androidJOB.clickOnJobs();
//		dynamicjobspage.SearchJobInSearchField(Config.CompositeJobName);
//		androidJOB.ClickOnOnJob(Config.CompositeJobNameEdited);
//		androidJOB.clickOnModifyOption(); 
//		deviceinfopanelpage.ClickOnDelayCompositeJob("1");
//		androidJOB.JobCreatedMessage(); 
//       commonmethdpage.ClickOnHomePage();

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1"); // 19
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 20
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();
		


		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
        deviceinfopanelpage.SearchFenceJob(Config.CompositeJobNameEdited);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobNameEdited);
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		
		deviceinfopanelpage.CheckFenceExitedDeviceAlert(false);
		deviceinfopanelpage.CheckFenceExitedMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceExited(false, "complianceEmailID@mailinator.com");
        deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("ExitTimeFenceJob");

		
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("ExitTimeFenceJob");
		androidJOB.Selectjob("ExitTimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("ExitTimeFenceJob", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
        deviceinfopanelpage.VerifyTimeFenceStatus("ON");

		deviceinfopanelpage.VerifyFenceExitAlertForDevice(Config.DeviceName, "Time fence", 520);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
		deviceinfopanelpage.InboxExitMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "exit", "Time fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();


		
		}
	@Test(priority = 'E', description = "14. Time Fence - Verify Creating Time fence job with Composite job added in Fence Entered."+"Verify creating time fence job with composite job along with some other jobs like install/Text message added in fence entered")
	public void TC_JO_792()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {


		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1"); // 19
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 20
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();
		
	
		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
        deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceEntered(false,"complianceEmailID@mailinator.com");
        deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("EnterTimeFenceJob");

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		
		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("EnterTimeFenceJob");
		androidJOB.Selectjob("EnterTimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("EnterTimeFenceJob", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
       deviceinfopanelpage.VerifyTimeFenceStatus("ON");


		deviceinfopanelpage.VerifyFenceEntryAlertForDevice(Config.DeviceName, "Time fence", 550);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
        deviceinfopanelpage.InboxEnteryMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Time fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();


		
	}
	@Test(priority = 'F', description = "15. Time Fence - Verify creating time fence job with composite job along with some other jobs like install/Text message added in fence entered")
	public void TC_JO_796()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {


		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1"); // 19
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 20
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();
		
	
		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteTextMessage");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobName);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobName);
        deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceEntered(false,"complianceEmailID@mailinator.com");
        deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("EnterTimeFenceJob");

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		
		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("EnterTimeFenceJob");
		androidJOB.Selectjob("EnterTimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("EnterTimeFenceJob", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
       deviceinfopanelpage.VerifyTimeFenceStatus("ON");


		deviceinfopanelpage.VerifyFenceEntryAlertForDevice(Config.DeviceName, "Time fence", 550);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
        deviceinfopanelpage.InboxEnteryMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Time fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();


		
	}

	@Test(priority = 'G', description = "16. Time Fence - Verify Creating Time fence job with Composite job(with delay added) added in Fence Entered.")
	public void TC_JO_794()throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {

		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1"); // 19
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 20
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();
		
	
		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob(Config.CompositeJobNameEdited);
		deviceinfopanelpage.SelectFenceJob(Config.CompositeJobNameEdited);
        deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.CheckFenceEnteredDeviceAlert(false);
		deviceinfopanelpage.CheckFenceEnteredMDMAlert(false);
		deviceinfopanelpage.ClickAndEnterEmailForFenceEntered(false,"complianceEmailID@mailinator.com");
        deviceinfopanelpage.ClickedOnTimeFenceJobSave();
		deviceinfopanelpage.EnterJobnameFenceOkButton("EnterTimeFenceJob");

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");

		
		deviceinfopanelpage.ClickOnBackButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJob("EnterTimeFenceJob");
		androidJOB.Selectjob("EnterTimeFenceJob");
		androidJOB.ClickOnApplyButtonApplyJobWindow();
		androidJOB.CheckStatusOfappliedInstalledJobTimeFence("EnterTimeFenceJob", 120);
		androidJOB.ClickOnJobQueueCloseButton();
		deviceinfopanelpage.refesh();
		commonmethdpage.SearchDevice(Config.DeviceName);
       deviceinfopanelpage.VerifyTimeFenceStatus("ON");


		deviceinfopanelpage.VerifyFenceEntryAlertForDevice(Config.DeviceName, "Time fence", 520);
		deviceinfopanelpage.ClickOnCloseButtonOfAlertMsg();

		androidJOB.ClickOnInbox();
        deviceinfopanelpage.InboxEnteryMessage();
		deviceinfopanelpage.VerifyFenceAlertFromConsole(Config.DeviceName, "entry", "Time fence");
		androidJOB.VerifyEmailNotification("https://www.mailinator.com","complianceEmailID@mailinator.com");

		commonmethdpage.ClickOnHomePage();

		commonmethdpage.AppiumConfigurationCommonMethod("com.mi.android.globalFileexplorer","com.android.fileexplorer.FileExplorerTabActivity");
		androidJOB.verifyFolderIsDownloadedInDevice("FileTransfer.jpg");
		androidJOB.ClickOnHomeButtonDeviceSide();


		}*/
	

	@Test(priority = 'H', description = "17. Time Fence - Verify adding Fence jobs /Composite jobs/Notification policy/job folders in Fence Entered and Fence Exited.")
	public void TimeFenceVerifyaddingFencejobsCompositejobsNotificationpolicyjobfoldersinFenceEnteredandFenceExited()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException, AWTException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		deviceinfopanelpage.ClickOnTimeFence();
		deviceinfopanelpage.ClickOnEnableTimeFence(false);
		deviceinfopanelpage.ClickAndSetStartTime(deviceinfopanelpage.TimeFenceStartTime1, 5, "1"); // 19
		deviceinfopanelpage.ClickAndSetEndTime(deviceinfopanelpage.TimeFenceEndTime1, 7, "2"); // 20
		deviceinfopanelpage.ClickOnTimeFenceAllDaysOption();

		deviceinfopanelpage.ClickOnFenceEnteredTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("TimeFenceJob");
		deviceinfopanelpage.SelectFenceJob("TimeFenceJob");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.VerifyErrorMsgForNotificationNCompositeNFenceJobs();

		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteJobFolder");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteJobFolder");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.VerifyErrorMsgForNotificationNCompositeNFenceJobs();

		deviceinfopanelpage.ClickOnTimeFenceAddJobInButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteNotificationPolicy");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteNotificationPolicy");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.VerifyErrorMsgForNotificationNCompositeNFenceJobs();

		deviceinfopanelpage.ClickOnFenceExiedTab();
		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("TimeFenceJob");
		deviceinfopanelpage.SelectFenceJob("TimeFenceJob");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.VerifyErrorMsgForNotificationNCompositeNFenceJobs();

		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteJobFolder");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteJobFolder");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.VerifyErrorMsgForNotificationNCompositeNFenceJobs();

		deviceinfopanelpage.ClickOnTimeFenceAddJobOutButton();
		deviceinfopanelpage.SearchFenceJob("AutomationDontDeleteNotificationPolicy");
		deviceinfopanelpage.SelectFenceJob("AutomationDontDeleteNotificationPolicy");
		deviceinfopanelpage.ClickOnTimeFenceAddJobOkButton();
		deviceinfopanelpage.VerifyErrorMsgForNotificationNCompositeNFenceJobs();
		deviceinfopanelpage.ClickOnCloseButtonOfTimeFence();
	}

}
