package Profiles_iOS_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class GlobalHTTPProxy extends Initialization{
	
	@Test(priority='1',description="1.To verify clicking on Global HTTP Proxy") 
    public void VerifyClickingOnGlobalHTTPProxy() throws InterruptedException {
    Reporter.log("To verify clicking on Global HTTP Proxy");
    profilesAndroid.ClickOnProfile();
	profilesiOS.clickOniOSOption();
	profilesiOS.AddProfile();
	profilesiOS.ClickOnGlobalHTTPProxy();
}
	@Test(priority='2',description="1.To verify summary of Global HTTP proxy")
	public void VerifySummaryofGlobalHTTPProxy() throws InterruptedException
	{
		Reporter.log("To verify summary of Global HTTP proxy");
		profilesiOS.VerifySummaryofGlobalHTTPProxy();
	}
	
	@Test(priority='3',description="1.To Verify warning message Saving a Global HTTP Proxy without Profile Name")
	public void VerifyWarningMessageOnSavingGlobalHTTPProxyWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Global HTTP Proxy without Profile Name========");
		profilesiOS.ClickOnConfigureButtonGlobalHTTPProxy();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='4',description="1.To Verify warning message Saving a Global HTTP Proxy without entering all the fields")
	public void VerifyWarningMessageOnSavingGlobalHTTPProxyWihtoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Global HTTP Proxy without entering all the fields=========");
		profilesiOS.EnterGlobalHTTPProxyProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	@Test(priority='5',description="1.To Verify warning message Saving a Global HTTP Proxy without Proxy Server And Port")
	public void VerifyWarningMessageOnSavingGlobalHTTPProxyWihtoutProxyServerAndPort() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a Global HTTP Proxy without Proxy Server And Port=========");
		profilesiOS.EnterProxyServerPort_GlobalHTTPProxy();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.warningWithoutProyServerAndPort();
}
	@Test(priority='6',description="1.To Verify saving the Global HTTP Proxy profile")
	public void VerifyGlobalHTTPProxyProfile() throws InterruptedException{
		Reporter.log("=======To Verify saving the Global HTTP Proxy profile=========");
		profilesiOS.ProxyServerURL_GlobalHTTPProxy();
		profilesiOS.ClickOnSavebutton();
		profilesAndroid.NotificationOnProfileCreated();
}
}
