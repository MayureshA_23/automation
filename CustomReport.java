package CustomReportsScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class CustomReport extends Initialization{
@Test(priority='0',description="Verifi modification / Deletion of custom reports")
public void VerifyingUserIsAbletodeleteCustomReport_TC_RE_153() throws InterruptedException
	{Reporter.log("\nVerifi modification / Deletion of custom reports",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.SearchingReportInCustomReportsList("Devdetails CustRep With Filters As OS Version Is NotequalTo 6.0.1");
		customReports.ClickOnModifyButton();
		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnUpdateButton();
		customReports.CompareValueInBothTableAfterModify();
		customReports.ClickOnCustomReports();
		customReports.DeleteReport("Devdetails CustRep With Filters As OS Version Is NotequalTo 6.0.1");
		customReports.VarifyDeletedReport();
	}
@Test(priority='1',description="Verify Access To Other Users in ALL Custom reports")
public void VerifyingCustomReportInSubUserAccount_TC_RE_154() throws InterruptedException
	{Reporter.log("\nVerify Access To Other Users in ALL Custom reports",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnAllowAccessToUSerCheckBox();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustRep accessable by SubUser","Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustRep accessable by SubUser");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();	
		loginPage.logoutfromApp();
		usermanagement.ClickOnLoginLink();
		customReports.logintoApp(Config.subuser_name,Config.subuser_passwrd);
		customReports.ClickOnLogin();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustRep accessable by SubUser");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustRep accessable by SubUser");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep(Config.DeviceName);
		customReports.CheckingCustomreportAtSubUserAccount();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
		loginPage.logoutfromApp();
	}
@Test(priority='2',description="Verify that Alias name for selected column in Custom reports")
public void VerifyingCustomReportForAliasName_TC_RE_155() throws InterruptedException
	{Reporter.log("\nVerify that Alias name for selected column in Custom reports",true);
		usermanagement.ClickOnLoginLink();
		customReports.logintoApp(Config.userID,Config.password);
		customReports.ClickOnLogin();
	    commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustRep to Verify the Alias name for selected col","Test");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceDetails);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustRep to Verify the Alias name for selected col");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CustRep to Verify the Alias name for selected col");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustRep to Verify the Alias name for selected col");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustRep to Verify the Alias name for selected col");
		reportsPage.WindowHandle();
		customReports.CheckingAlisaNameForDeviceNameCol();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='3',description="Verify that Alias name for selected multiple column in Custom reports")
public void VerifyingCustomReportInSubUserAccount_TC_RE_156() throws InterruptedException
	{Reporter.log("\nVerify that Alias name for selected multiple column in Custom reports",true);	
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Applied Job Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustRep to Verify the Alias name for selected multiple col","Test");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceDetails);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.AggregateOptionCount);
		customReports.SendingAliasName("My Device");
		customReports.SendingAliasName_JobName(Config.Alias_NAme_JobName);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustRep to Verify the Alias name for selected multiple col");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("CustRep to Verify the Alias name for selected multiple col");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustRep to Verify the Alias name for selected multiple col");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustRep to Verify the Alias name for selected multiple col");
		reportsPage.WindowHandle();
		customReports.CheckingAlisaNameForDeviceNameCol();
		customReports.CheckingAlisaNameForJobNameCol();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='4',description="Verify OS Update custom Report")
public void VerifyingCustomReport_TC_RE_26() throws InterruptedException
	{Reporter.log("\nVerify OS Update custom Report",true);	
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Windows OS Update Status");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("WindowsOSUpdate Status CustomReport ToCheckColumns in report","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("WindowsOSUpdate Status CustomReport ToCheckColumns in report");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("WindowsOSUpdate Status CustomReport ToCheckColumns in report");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("WindowsOSUpdate Status CustomReport ToCheckColumns in report");
		reportsPage.WindowHandle();
		customReports.VerifyColumnsInOSUpdateReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='5',description="Verify Custom Reports by selecting date time zone as Today")
public void VerifyingCustomReport_TC_RE_191() throws InterruptedException
	{Reporter.log("\nVerify Custom Reports by selecting date time zone as Today",true);	
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("System Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustomReports by selecting DateTimeZone as Today","test");
		customReports.Selectvaluefromdropdown("System Log");
		customReports.SelectValueFromColumn("Log Time");
		customReports.SelectTodayDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustomReports by selecting DateTimeZone as Today");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustomReports by selecting DateTimeZone as Today");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustomReports by selecting DateTimeZone as Today");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingCurrentDateInViewReport_AppliedJobDetails();
		customReports.VerifyTimeZoneForTodaysDate();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority='6',description="Verify Custom Reports by selecting date time zone as Yesterday / Last 1 Week / Last 30 Days / This Month ")
	public void VerifyingCustomReport_TC_RE_192() throws InterruptedException
	{Reporter.log("\nVerify Custom Reports by selecting date time zone as Today",true);	
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("System Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustomReports by selecting DateTimeZone as Yesterday","test");
		customReports.Selectvaluefromdropdown("System Log");
		customReports.SelectValueFromColumn("Log Time");
		customReports.SelectYesterdayDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustomReports by selecting DateTimeZone as Yesterday");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustomReports by selecting DateTimeZone as Yesterday");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustomReports by selecting DateTimeZone as Yesterday");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingYesterDateInViewReport_SystemLog();
		customReports.VerifyTimeZoneForYesterday();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='7',description="Verify Custom Reports by selecting different date time zone as Today/Yesterday/Last 1Week/Last 30Days/This Month in report filter")
public void VerifyingCustomReport_TC_RE_194() throws InterruptedException
	{Reporter.log("\nVerify Custom Reports by selecting different date time zone as Today/Yesterday/Last 1Week/Last 30Days/This Month in report filter",true);	
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Call Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustomReports by selecting DateTimeZone as Last one Week","test");
		customReports.Selectvaluefromdropdown("Call Log");
		customReports.SelectValueFromColumn("Datetime");
		customReports.SelectOneWeekDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustomReports by selecting DateTimeZone as Last one Week");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustomReports by selecting DateTimeZone as Last one Week");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustomReports by selecting DateTimeZone as Last one Week");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingOneWeekDateInCallLogReport();
		customReports.VerifyTimeZoneFor1WeekDateInCalLogsRep();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='8',description="Verify Custom Reports by selecting different date time zone as Custom Range in report filter for any date-time range/period")
public void VerifyingCustomReport_TC_RE_195() throws InterruptedException, AWTException
	{Reporter.log("\nVerify Custom Reports by selecting different date time zone as Custom Range in report filter for any date-time range/period=====",true);	
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceLocation);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustRep for different date time zone as Custom Range","test");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceLocation);
		customReports.SelectValueFromColumn("Datetime");
		customReports.SelectingRangeInDatePicker();
		customReports.ClickOnApplyButton();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustRep for different date time zone as Custom Range");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustRep for different date time zone as Custom Range");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustRep for different date time zone as Custom Range");
	    reportsPage.WindowHandle();
	    customReports.ChooseDevicesPerPage();
		customReports.VarifyingOneWeekDateInDevLocationReport();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='9',description="Verify date picker Ui in custom report")
public void VerifyingCustomReport_TC_RE_202() throws InterruptedException, AWTException
	{Reporter.log("\nVerify date picker Ui in custom report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("Call Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Call Log");
		customReports.SelectValueFromColumn("Datetime");
		customReports.VerifingDataPickerMoment();
		customReports.ClickOnCancelButton();
	}
@Test(priority='A',description="Verify 'Created date' column is added in custom reports.")
public void VerifyingCustomReport_TC_RE_213() throws InterruptedException
	{Reporter.log("\nVerify 'Created date' column is added in custom reports.",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.CheckingReportCreatedDateFormat();
	}

@Test(priority='C',description="Verify search in the Customn Reports")
public void VerifyingCustomReport_TC_RE_216() throws InterruptedException
	{Reporter.log("\nVerify search in the Customn Reports",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable("System Log");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustomReports to validate SearchBox","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.SendRepToSearchBox("CustomReports to validate SearchBox");
		customReports.VerifySearchBox("CustomReports to validate SearchBox");	
	}
@Test(priority='D',description="Verify the OS Build Number in Custom Reports")
public void VerifyingCustomReport_TC_RE_231() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify the OS Build Number in Custom Reports",true);
		commonmethdpage.SearchDeviceInconsole();
		deviceGrid.ScrollToColumn("OS Build Number");
		customReports.GetDeviceOSBuildNumber();
		commonmethdpage.ClickOnAllDevicesButton();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustRep to Verify OS Build Number from DevDetails table","test");
	    customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustRep to Verify OS Build Number from DevDetails table");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustRep to Verify OS Build Number from DevDetails table");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustRep to Verify OS Build Number from DevDetails table");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		deviceGrid.VerifyOSBuildNumberInReport(66);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='E',description="Verify the OS Build Number in Custom Reports")
public void VerifyingCustomReport_TC_RE_232() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify the OS Build Number in Custom Reports",true);	
		commonmethdpage.SearchDeviceInconsole();
		deviceGrid.ScrollToColumn("OS Build Number");
		customReports.GetDeviceOSBuildNumber();
		commonmethdpage.ClickOnAllDevicesButton();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("CustRep to Verify OS Build Number from DeHistory table","test");
		customReports.ClickOnColumnInTable("Device History");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("CustRep to Verify OS Build Number from DeHistory table");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("CustRep to Verify OS Build Number from DeHistory table");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("CustRep to Verify OS Build Number from DeHistory table");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		deviceGrid.VerifyOSBuildNumberInReport(26);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
@Test(priority='F',description="Verify date/time in date picker for custom reports with filter.")
public void VerifyingCustomReport_TC_RE() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify date/time in date picker for custom reports with filter.",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDet Rep to Verify date/time in date picker for CustRep with filter.","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Last Connected Time");
		customReports.GetDateAndTimeFormat();
		customReports.ClickOnAddMoreButton();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Registered");
		customReports.GetDateAndTimeFormat();
		customReports.ClickOnAddMoreButton();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Last Device Time");
		customReports.GetDateAndTimeFormat();
		customReports.ClickOnCancelButton();
	}
@Test(priority='G',description="Verify the pagination of Custom Report page")
public void VerifyingCustomReport_TC_RE_215() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\nVerify the pagination of Custom Report page",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.VerifyColumnsInCustReport();	
	}
@Test(priority='H',description="verify Date picker when user modifies the previously created custom device detail report")
public void VerifyingCustomReport_TC_RE2() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{Reporter.log("\n==verify Date picker when user modifies the previously created custom device detail report=========",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevDetRep to Verify Datepicker for modified Rep","test");
		customReports.ClickOnColumnInTable("Device Details");
		customReports.ClickOnAddButtonInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.SearchingReportInCustomReportsList("DevDetRep to Verify Datepicker for modified Rep");
		customReports.ClickOnModifyButton();
		customReports.ClickOnAddMoreButton();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Last Device Time");
		customReports.GetDateAndTimeFormat();
		customReports.ClickOnAddMoreButton();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Device Registered");
		customReports.GetDateAndTimeFormat();
		customReports.ClickOnAddMoreButton();
		customReports.Selectvaluefromdropdown("Device Details");
		customReports.SelectValueFromColumn("Last Device Time");
		customReports.GetDateAndTimeFormat();
		customReports.ClickOnCancelButton();
	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				} else {
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
