package ActivityLog_TestScript;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

/*Precondition
Create below jobs and update AcccountUser According to Account User Name
String Job1 = "Text1";
String Job2 = "Text2";
String AcccountUser */


public class ActivityLog extends Initialization{

	@Test(priority='1',description="create job and Apply  on the device to generate the log")
	public void CreateJobAndApply() throws InterruptedException{
		Reporter.log("\ncreate job and Apply  on the device to generate the log",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.Job1,"test","Nowhere");
		androidJOB.ClickOnOkButton();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.Job2,"test","hi how is your day");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		try {
			commonmethdpage.SearchDevice(Config.DeviceName);
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			e.printStackTrace();
		}
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.Job1);
		commonmethdpage.Selectjob(Config.Job1);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.Job2);
		commonmethdpage.Selectjob(Config.Job2);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
	}
	
	@Test(priority='2',description="Log Filter should be applied and logs should be displayed only for the selected device and Job Name")
	public void VerifyActivityLogsFilter_TC_AL_01() throws InterruptedException{
		Reporter.log("\nLog Filter is applied and logs should be displayed only for the selected device and Job Name",true);
		activitylogpage.FilterCondition(Config.DeviceName,Config.Job1 );
		activitylogpage.ClickOnOkButtonLogFilter();
		activitylogpage.CheckForFiltercondition();
	}
	
	@Test(priority='3',description="Logs should be cleared")
	public void ActivityLogsVerifyClearLog_TC_AL_02() throws InterruptedException{
		Reporter.log("\nLogs should be cleared",true);
		activitylogpage.RightclickClearLog();
		activitylogpage.LogClearVerification(Config.AcccountUser);
	}
	
	@Test(priority='4',description="Activity Logs_Verify Log Filter by clicking on menu button")
	public void ActivityLogFilterFromMeny() throws InterruptedException{
		Reporter.log("\nActivity Logs_Verify Log Filter by clicking on menu button",true);
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.Job1);
		commonmethdpage.Selectjob(Config.Job1);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.Job2);
		commonmethdpage.Selectjob(Config.Job2);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		activitylogpage.FilterMenu(Config.DeviceName,Config.Job1 );
		activitylogpage.ClickOnOkButtonLogFilter();
		activitylogpage.CheckForFiltercondition();
	}
	
	@Test(priority='5',description="Activity Logs_Verify Export Log")
	public void ActivityLogsVerifyExportLog() throws InterruptedException{
		Reporter.log("\nActivity Logs_Verify Export Log",true);
		activitylogpage.ExportLog();
	}
	@Test(priority='6',description="Activity Logs_Verify Activity Logs are printed")
	public void ActivityLogsPrinted_TC_DG_155() throws InterruptedException,IOException,InvalidFormatException
	{
		Reporter.log("\nActivity Logs_Verify Activity Logs are printed",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname,"test","Nowhere");
		androidJOB.ClickOnOkButton();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob(Config.TextMessageJobname);
		commonmethdpage.Selectjob(Config.TextMessageJobname);
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,Config.DeviceName);
		
	}

}
