package JobsOnAndroid;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.Test;

import Common.Initialization;

public class SureVideo_Job extends Initialization {
	

	@Test(priority=1,description="Verify enabling \"Mute audio while playing screensaver\" in SureVideo Settings Statis jobs")
	public void TC_JO_911() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{


		androidJOB.LaunchSureVideo();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnsurevideo_settings();
		androidJOB.ClickOnsurevideoSettings();
		androidJOB.ClickOn_surevideoSettingsSearch("Screensaver Settings");
		androidJOB.ClickOnScreenSaverSettings();
		androidJOB.EnableScreenSaver();
		androidJOB.muteAudioScreenSaverSV();
		androidJOB.screen_sav_pg();
		androidJOB.surevideo_sett_pg();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("MuteAudioWhilePlayingScreensaver SV");
		commonmethdpage.ClickOnHomePage();
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surevideo","com.gears42.surevideo.SureVideoScreen");

		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("MuteAudioWhilePlayingScreensaver SV");
		androidJOB.CheckStatusOfappliedInstalledJob("MuteAudioWhilePlayingScreensaver SV",360);
		androidJOB.TapDevice();
	    androidJOB.TapDevice();
		androidJOB.goToSVAdminSettings();
		androidJOB.DeviceSideClickOnSureVideoSettings();
		androidJOB.DeviceSideSVSettingClickOnSearchBar("Screensaver Settings");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Screensaver Settings");
		androidJOB.verifyEnableScreensaverSettings_MuteAudioSV("Enable Screensaver");
		androidJOB.verifyEnableScreensaverSettings_MuteAudioSV("Mute audio while playing screensaver");


		
		}
	@Test(priority=2,description="Verify disabling \"Mute audio while playing screensaver\" in SureVideo Settings Statis jobs")
	public void TC_JO_912() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

		androidJOB.TapDevice();

		androidJOB.clickOnJobs();
		dynamicjobspage.SearchJobInSearchField("MuteAudioWhilePlayingScreensaver SV");
		androidJOB.ClickOnOnJob("MuteAudioWhilePlayingScreensaver SV");
		androidJOB.ModifyJobButton();
		androidJOB.ClickOnsurevideoSettings();
		androidJOB.ClickOn_surevideoSettingsSearch("Screensaver Settings");
		androidJOB.ClickOnScreenSaverSettings();
		androidJOB.muteAudioScreenSaverSV();
		androidJOB.screen_sav_pg();
		androidJOB.surevideo_sett_pg();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("DisableMuteAudioWhilePlayingScreensaver SV");
		androidJOB.TapDevice();
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surevideo","com.gears42.surevideo.SureVideoScreen");

		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("DisableMuteAudioWhilePlayingScreensaver SV");
		androidJOB.CheckStatusOfappliedInstalledJob("DisableMuteAudioWhilePlayingScreensaver SV",360);
		androidJOB.TapDevice();
	    androidJOB.TapDevice();
		androidJOB.goToSVAdminSettings();
		androidJOB.DeviceSideClickOnSureVideoSettings();
		androidJOB.DeviceSideSVSettingClickOnSearchBar("Screensaver Settings");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("Screensaver Settings");
		androidJOB.verifyDisableScreensaverSettings_MuteAudioSV("Mute audio while playing screensaver");
	}
	
	// Feature Not supported on Pie and above devices.
	
	@Test(priority=3,description="Verify Configuring the scroll speed in SureVideo Settings Static job")
	public void TC_JO_913() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException{

	
	
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob(); 
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnsurevideo_settings();
		androidJOB.ClickOnsurevideoSettings();
		androidJOB.ClickOn_surevideoSettingsSearch("RSS and Text Feed Settings");
		androidJOB.ClickOnsureVideoRssSettings();
		androidJOB.EnablesureVideoRssSettings();
		androidJOB.ClickOnrss_scroll_speed();
		androidJOB.Inputscroll_speed("52");
		androidJOB.ClickOnrss_sett_pgDoneBtn();
		androidJOB.surevideo_sett_pg();
		androidJOB.clickOnsaveSureLockSetting();
		androidJOB.SurelockSettingName("RSSAndTextFeedSettings_SV");
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("RSSAndTextFeedSettings_SV");
		androidJOB.CheckStatusOfappliedInstalledJob("RSSAndTextFeedSettings_SV",360);
	    commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surevideo","com.gears42.surevideo.SureVideoScreen");
		androidJOB.goToSVAdminSettings();
		androidJOB.DeviceSideClickOnSureVideoSettings();
		androidJOB.DeviceSideSVSettingClickOnSearchBar("RSS and Text Feed Settings");
		androidJOB.TwoTimeClickOnSLsettingsAfterSearchDeviceSide("RSS and Text Feed Settings");
		androidJOB.DeviceSideSVSettingClickOnSearchBar("Scroll Speed");
		androidJOB.OneTimeClickAfterSearchSLsettings("Scroll Speed");
		androidJOB.verifyAppSureVideoSettingScrollSpeed("52");


	
	
	
	
	
	
	
	
	}	
}
