package Schedule_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ScheduleReports extends Initialization {
	@Test(priority='0',description="Pre condition-Creating a group")
	public void CreateGroup() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		rightclickDevicegrid.VerifyGroupPresence(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
		rightclickDevicegrid.MoveToGroup(Config.GroupName_Reports);
		commonmethdpage.ClickOnAllDevicesButton();
	}
	
	@Test(priority='1',description="Verify 'Daily' Scheduled Report for System Log - Home Group")
	public void ScheduledReportSystemLogDaily_HomeGroup() throws InterruptedException{
		Reporter.log("\n1.Verify 'Daily' Scheduled Report for System Log ",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonSystemLog();
		schedulereportpage.VerifySystemLogReportsColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.ClickOnSchedule();	
	}
	
	@Test(priority='2',description="Verify 'Daily' Scheduled Report for Asset Tracking Log - Home Group")
	public void ScheduledReportAssetTrackingLogDaily() throws InterruptedException{
		Reporter.log("\n2.Verify 'Daily' Scheduled Report for System Log ",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonAssetTracking();
		schedulereportpage.VerifyAssetTrackingReportsColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.ClickOnSchedule();
		
	}
	
	@Test(priority='3',description="Verify 'Daily' Scheduled Report for Device Activity")
	public void ScheduledReportDeviceActivityDaily() throws InterruptedException{
		Reporter.log("\n3.Verify 'Daily' Scheduled Report for Device Activity",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonDeviceActivity();
		schedulereportpage.VerifyDeviceActivityReportColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.ClickOnSelectDevice();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		schedulereportpage.ClickOnSchedule();
	}
	
	@Test(priority='4',description="Verify 'Daily' Job Deployed Report for Device Activity - Home Group")
	public void ScheduledReportJobsDeployedDaily() throws InterruptedException{
		Reporter.log("\n4.Verify 'Daily' Job Deployed Report for Device Activity - Home Group",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonJobsDeployed();
		schedulereportpage.VerifyJobDeployedReportColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.ClickOnSchedule();
	}
	
	@Test(priority='5',description="Verify 'Daily' Job Deployed Report for Installed Job Report - Home Group")
    public void ScheduledReportInstalledJobReportDaily() throws InterruptedException, IOException{
            Reporter.log("\n5.Verify 'Daily' Job Deployed Report for Installed Job Report - Home Group",true);
            
            androidJOB.clickOnJobs();
            androidJOB.clickNewJob();
            androidJOB.clickOnAndroidOS();
            androidJOB.ClickOnInstallApplication();
            androidJOB.enterJobName(Config.JobName);
            androidJOB.clickOnAddInstallApplication();
            androidJOB.browseFileInstallApplication("./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\File3.exe");
            androidJOB.clickOkBtnOnBrowseFileWindow();
            androidJOB.clickOkBtnOnAndroidJob();
            androidJOB.UploadcompleteStatus();
            commonmethdpage.ClickOnHomePage();
            commonmethdpage.ClickOnAllDevicesButton();
            commonmethdpage.SearchDeviceInconsole();
            commonmethdpage.ClickOnApplyButton();
            commonmethdpage.SearchJob(Config.JobName);
            commonmethdpage.Selectjob(Config.JobName);
            commonmethdpage.ClickOnApplyButtonApplyJobWindow();
            reportsPage.ClickOnReports_Button();
            schedulereportpage.ClickOnScheduleReport();
            schedulereportpage.ClickonInstalledJobReport();
            schedulereportpage.VerifyInstalledJobReportColumns();
            schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
            schedulereportpage.ClickOnScheduleNew();
            schedulereportpage.EnterEmailAddress(Config.Email);
            schedulereportpage.ClickOnSelectJob();
            reportsPage.SearchJobInReport(Config.JobName);
            reportsPage.SelectSearchedJobInReport();
            reportsPage.ClickOnOkButtonSelectJobToAddInReport();
            schedulereportpage.EnterApplicationNameInstalledJobReport(Config.ApplicationName);
            schedulereportpage.ClickOnSchedule();
    }
	
	@Test(priority='6',description="Verify 'Daily' Scheduled Report for Device Health - Sub Group")
	public void ScheduledReportDeviceHealthReportDailySubGroup() throws InterruptedException{
		Reporter.log("\n6.Verify 'Daily' Scheduled Report for Device Health - Sub Group",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonDeviceHealth();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.EnterStorageSpace();
		schedulereportpage.EnterPhysicalMemory();
		schedulereportpage.ClickOnSelectGroup();
		reportsPage.SearchGroup(Config.GroupName_Reports);
		reportsPage.SelectSearchedGroupInReport(Config.GroupName_Reports);
		reportsPage.ClickOnOkButtonGroupList();
		schedulereportpage.ClickOnSchedule();
		
	}
	
	@Test(priority='7',description="7. Verify 'Daily' Scheduled Report for Device History Report - Home Group")
	public void ScheduledReportDeviceHistoryReportDailyHomeGroup() throws InterruptedException{
		Reporter.log("\n7.Verify 'Daily' Scheduled Report for Device Health - Home Group",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonDeviceHistory();
		schedulereportpage.VerifyDeviceHistoryReportColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.ClickOnSelectDevice();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		schedulereportpage.ClickOnSchedule();
		
	}
	
	@Test(priority='8',description="Verify 'Daily' Scheduled Report for Data Usage Report - Home Group")
	public void ScheduledReportDataUsageReportDailyHomeGroup() throws InterruptedException{
		Reporter.log("\n8.Verify 'Daily' Scheduled Report for Data Usage Report - Home Group",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonDataUsage();
		schedulereportpage.VerifyDataUsageReportColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.ClickOnSchedule();
		
	}
	
	@Test(priority='9',description="Verify 'Daily' Scheduled Report for Device Connected - Home Group")
	public void ScheduledReportDeviceConnectedReportDailyHomeGroup() throws InterruptedException{
		Reporter.log("\n9.Verify 'Daily' Scheduled Report for Device Health- Home Group",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonDeviceConnected();
		schedulereportpage.VerifyDeviceConnectedReportColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.ClickOnSchedule();
		
	}
	@Test(priority='A',description="Verify 'Daily' Scheduled Report for App Version - Home Group - All Apps")
	public void ScheduledReportAppVersionReportDailyHomeGroup_AllApps() throws InterruptedException{
		Reporter.log("\n10.Verify 'Daily' Scheduled Report for Device Health- Home Group",true);
		reportsPage.ClickOnReports_Button();
		schedulereportpage.ClickOnScheduleReport();
		schedulereportpage.ClickonAppVersion();
		schedulereportpage.VerifyAppVersionReportColumns();
		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
		schedulereportpage.ClickOnScheduleNew();
		schedulereportpage.EnterEmailAddress(Config.Email);
		schedulereportpage.EnterApplicationNameInstalledJobReport("ES File Explorer");
		schedulereportpage.ClickOnSchedule();
	}
//	@Test(priority='B',description="Verify Scheduled Jobs count in the Asset Tracking report")
//	public void ScheduledAssetTrackingCustReportDailyHomeGroup_TC_RE_161() throws InterruptedException{
//		Reporter.log("\n11.Verify Scheduled Jobs count in the Asset Tracking report",true);
//		//TC_RE_162-Verify InProgress Jobs count column in the Asset Tracking report
//		//TC_RE_163-Verify Failed Jobs count column in the Asset Tracking report
//		//TC_RE_164-Verify Deployed Jobs count column in the Asset Tracking report
//		
//		reportsPage.ClickOnReports_Button();
//		schedulereportpage.ClickOnScheduleReport();
//		schedulereportpage.ClickonAssetTracking();
//		schedulereportpage.VerifyAssetTrackingReportsColumns();
//		schedulereportpage.VerifyOfScheduleNewButtonDeleteButtonRescheduledButton();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='C',description="Schedule Device details custom report")
//	public void ScheduledDevdetailsCustReportDailyHomeGroup_TC_RE_77() throws InterruptedException{
//		Reporter.log("\n12.Schedule Device details custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule Devdetails CustRep","test");
//		customReports.ClickOnColumnInTable("Device Details");
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList("Device Details");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule Devdetails CustRep");
//		customReports.SearchCustomizedReport("Schedule Devdetails CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='D',description="Schedule Call Log Tracking custom report")
//	public void ScheduledCallLogCustReportDailyHomeGroup_TC_RE_78() throws InterruptedException{
//		Reporter.log("\n13.Schedule Call Log Tracking custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectCallLogTable);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectCallLogTable);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule CallLog CustRep","Test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule CallLog CustRep");
//		customReports.SearchCustomizedReport("Schedule CallLog CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='E',description="Schedule SMS Log custom report")
//	public void ScheduledSmsLogCustReportDailyHomeGroup_TC_RE_79() throws InterruptedException{
//		Reporter.log("\n14.Schedule SMS Log custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectSMSLogTable);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectSMSLogTable);		
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule SmsLog CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule SmsLog CustRep");
//		customReports.SearchCustomizedReport("Schedule SmsLog CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='F',description="Schedule Data usage custom report")
//	public void ScheduledDataUsageCustReportDailyHomeGroup_TC_RE_80() throws InterruptedException{
//		Reporter.log("\n15.Schedule Data usage custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnDataUsageCol();
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectDataUsage);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule DataUsage CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule DataUsage CustRep");
//		customReports.SearchCustomizedReport("Schedule DataUsage CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='G',description="Schedule App Data usage custom report")
//	public void ScheduledAppDataUsageCustReportDailyHomeGroup_TC_RE_81() throws InterruptedException{
//		Reporter.log("\n16.Schedule App Data usage custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule AppDataUsage CustRep","test");
//		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule AppDataUsage CustRep");
//		customReports.SearchCustomizedReport("Schedule AppDataUsage CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='H',description="Schedule Location tracking custom report")
//	public void ScheduledLocationTrackingCustReportDailyHomeGroup_TC_RE_82() throws InterruptedException{
//		Reporter.log("\n17.Schedule Location tracking custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectDeviceLocation);	
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule DevLocation CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule DevLocation CustRep");
//		customReports.SearchCustomizedReport("Schedule DevLocation CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='I',description="Schedule applied Job detail custom report")
//	public void ScheduledAppJobDetCustReportDailyHomeGroup_TC_RE_83() throws InterruptedException{
//		Reporter.log("\n18.Schedule applied Job detail custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable("Applied Job Details");
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectAppliedJobDetails);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule AppJobDet CustRep","Test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule AppJobDet CustRep");
//		customReports.SearchCustomizedReport("Schedule AppJobDet CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='J',description="Schedule Installed Job detail custom report")
//	public void ScheduledInstAppDetCustReportDailyHomeGroup_TC_RE_84() throws InterruptedException{
//		Reporter.log("\n21.Schedule Installed Job detail custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectInstalledAppDetails);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectInstalledAppDetails);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule InstAppDetails CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule InstAppDetails CustRep");
//		customReports.SearchCustomizedReport("Schedule InstAppDetails CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='K',description="Schedule System Log custom report")
//	public void ScheduledReportSystemLogDaily_TC_RE_85() throws InterruptedException{
//		Reporter.log("\n22.Schedule System Log custom report",true);	
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectSystemLog);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectSystemLog);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule SystemLog CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule SystemLog CustRep");
//		customReports.SearchCustomizedReport("Schedule SystemLog CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();	
//	}
//	@Test(priority='L',description="Schedule SureLock Checklist custom report")
//	public void ScheduledSureLockChecklistCustReportDailyHomeGroup_TC_RE_87() throws InterruptedException{
//		Reporter.log("\n23.Schedule SureLock Checklist custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable("SureLock CheckList");
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule SureLockChecklist CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule SureLockChecklist CustRep");
//		customReports.SearchCustomizedReport("Schedule SureLockChecklist CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='M',description="Schedule SureLock Analytics custom report")
//	public void ScheduledSureLockAnalyticsCustReportDailyHomeGroup_TC_RE_89() throws InterruptedException{
//		Reporter.log("\n24.Schedule SureLock Analytics custom report",true);
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnDataAnalyticsTab();
//		accountsettingspage.EnableDataAnalyticsCheckBox();
//		accountsettingspage.DisableSureLockAnalyticsCheckBox();
//		accountsettingspage.ClickOnDataAnalyticsApplyButton();
//		accountsettingspage.EnablingSureLockAnalyticsChckBox();
//		accountsettingspage.ClickOnDataAnalyticsApplyButton();
//		accountsettingspage.VerifySureLockAnalyticsEnablePopUp();
//		accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule SureLockAnalytics CustRep","test");
//		customReports.ClickOnColumnInTable("SureLock Analytics");
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule SureLockAnalytics CustRep");
//		customReports.SearchCustomizedReport("Schedule SureLockAnalytics CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='N',description="Schedule MTD Scan custom report")
//	public void ScheduledSMTDScanCustReportDailyHomeGroup_TC_RE_90() throws InterruptedException{
//		Reporter.log("\n25.Schedule MTD Scan custom report",true);
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnDataAnalyticsTab();	
//		accountsettingspage.EnableDataAnalyticsCheckBox();
//		accountsettingspage.DisableMTDAnalyticsCheckBox();
//		accountsettingspage.ClickOnDataAnalyticsApplyButton();
//		accountsettingspage.EnablingMTDAppScanAnalyticsChckBox();
//		accountsettingspage.ClickOnDataAnalyticsApplyButton();
//		accountsettingspage.VerifyMTDAppScanAnalyticsEnablePopUp();
//		accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable("MTD App Scan");
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule MTDAppScan CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule MTDAppScan CustRep");
//		customReports.SearchCustomizedReport("Schedule MTDAppScan CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='O',description="Verify Schedule Reports for SureMDM Nix Permission checklist ")
//	public void ScheduledSureMDMNixPerCustReportDailyHomeGroup_TC_RE_182() throws InterruptedException{
//		Reporter.log("\n26.Verify Schedule Reports for SureMDM Nix Permission checklist ",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable("Application Permission");
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("Schedule ApplicationPermission CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("Schedule ApplicationPermission CustRep");
//		customReports.SearchCustomizedReport("Schedule ApplicationPermission CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	
//	@Test(priority='P',description="Verify 'monthly' Schedule Call log tracking custom report")
//	public void ScheduledMonthlyCalLogCustReportDailyHomeGroup_TC_RE_06() throws InterruptedException{
//		Reporter.log("\n27.Verify 'monthly' Schedule Call log tracking custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectCallLogTable);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectCallLogTable);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("'monthly' Schedule CallLog CustRep","Test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("'monthly' Schedule CallLog CustRep");
//		customReports.SearchCustomizedReport("'monthly' Schedule CallLog CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		//case-1
//		schedulereportpage.ClickOnMonthlyRadioButon();
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyHiddenScheduledCycleTimeAndDateField();
//		//case-2
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyScheduledCycleTimeAndDateField();
//		schedulereportpage.SelectAnyDateInScheduledCycleTime();
//		schedulereportpage.SelectAnyDateInScheduledCycleDate();
//		//case-3
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='Q',description="Verify 'monthly' Schedule Sms log tracking custom report")
//	public void ScheduledMonthlySmsLogCustReportDailyHomeGroup_TC_RE_07() throws InterruptedException{
//		Reporter.log("\n28.Verify 'monthly' Schedule Sms log tracking custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectSMSLogTable);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectSMSLogTable);		
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("'monthly' Schedule SmsLog CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("'monthly' Schedule CallLog CustRep");
//		customReports.SearchCustomizedReport("'monthly' Schedule CallLog CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		//case-1
//		schedulereportpage.ClickOnMonthlyRadioButon();
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyHiddenScheduledCycleTimeAndDateField();
//		//case-2
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyScheduledCycleTimeAndDateField();
//		schedulereportpage.SelectAnyDateInScheduledCycleTime();
//		schedulereportpage.SelectAnyDateInScheduledCycleDate();
//		//case-3
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='R',description="Verify 'monthly' Schedule App data usage custom report")
//	public void ScheduledMonthlyAppdatausageCustReportDailyHomeGroup_TC_RE_09() throws InterruptedException{
//		Reporter.log("\n29.Verify 'monthly' Schedule App data usage custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("'monthly' Schedule AppDataUsage CustRep","test");
//		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("'monthly' Schedule AppDataUsage CustRep");
//		customReports.SearchCustomizedReport("'monthly' Schedule AppDataUsage CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		//case-1
//		schedulereportpage.ClickOnMonthlyRadioButon();
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyHiddenScheduledCycleTimeAndDateField();
//		//case-2
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyScheduledCycleTimeAndDateField();
//		schedulereportpage.SelectAnyDateInScheduledCycleTime();
//		schedulereportpage.SelectAnyDateInScheduledCycleDate();
//		//case-3
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='S',description="Verify 'Monthly' Schedule Location tracking custom report")
//	public void ScheduledMonthlyLocationtrackingCustReportDailyHomeGroup_TC_RE_10() throws InterruptedException{
//		Reporter.log("\n30.Verify 'Monthly' Schedule Location tracking custom report",true);
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectDeviceLocation);	
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectDeviceLocation);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("'monthly' Schedule DevLocation CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("'monthly' Schedule DevLocation CustRep");
//		customReports.SearchCustomizedReport("'monthly' Schedule DevLocation CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		//case-1
//		schedulereportpage.ClickOnMonthlyRadioButon();
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyHiddenScheduledCycleTimeAndDateField();
//		//case-2
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyScheduledCycleTimeAndDateField();
//		schedulereportpage.SelectAnyDateInScheduledCycleTime();
//		schedulereportpage.SelectAnyDateInScheduledCycleDate();
//		//case-3
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
//	@Test(priority='T',description="Verify 'Monthly' Schedule System Log custom report")
//	public void ScheduledMonthlyLocationtrackingCustReportDailyHomeGroup_TC_RE_11() throws InterruptedException{
//		Reporter.log("\n31.Verify 'Monthly' Schedule System Log custom report",true);	
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.ClickOnColumnInTable(Config.SelectSystemLog);
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.VerifyTableInSelectedTableList(Config.SelectSystemLog);
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("'monthly' Schedule SystemLog CustRep","test");
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("'monthly' Schedule SystemLog CustRep");
//		customReports.SearchCustomizedReport("'monthly' Schedule SystemLog CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		//case-1
//		schedulereportpage.ClickOnMonthlyRadioButon();
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyHiddenScheduledCycleTimeAndDateField();
//		//case-2
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyScheduledCycleTimeAndDateField();
//		schedulereportpage.SelectAnyDateInScheduledCycleTime();
//		schedulereportpage.SelectAnyDateInScheduledCycleDate();
//		//case-3
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//}
//	@Test(priority='U',description="Verify 'Monthly' Schedule SureLock Analytics custom report")
//	public void ScheduledMonthlySureLockAnalyticsCustReportDailyHomeGroup_TC_RE_13() throws InterruptedException{
//		Reporter.log("\n32.Verify 'Monthly' Schedule SureLock Analytics custom report",true);
//		commonmethdpage.ClickOnSettings();
//		commonmethdpage.ClickonAccsettings();
//		accountsettingspage.ClickOnDataAnalyticsTab();
//		accountsettingspage.EnableDataAnalyticsCheckBox();
//		accountsettingspage.DisableSureLockAnalyticsCheckBox();
//		accountsettingspage.ClickOnDataAnalyticsApplyButton();
//		accountsettingspage.EnablingSureLockAnalyticsChckBox();
//		accountsettingspage.ClickOnDataAnalyticsApplyButton();
//		accountsettingspage.VerifySureLockAnalyticsEnablePopUp();
//		accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
//		reportsPage.ClickOnReports_Button();
//		customReports.ClickOnCustomReports();
//		customReports.ClickOnAddButtonCustomReport();
//		customReports.EnterCustomReportsNameDescription_DeviceDetails("'monthly' Schedule SureLockAnalytics CustRep","test");
//		customReports.ClickOnColumnInTable("SureLock Analytics");
//		customReports.ClickOnAddButtonInCustomReport();
//		customReports.ClickOnSaveButton_CustomReports();
//		schedulereportpage.ClickOnSchedulReport("'monthly' Schedule SureLockAnalytics CustRep");
//		customReports.SearchCustomizedReport("'monthly' Schedule SureLockAnalytics CustRep");
//		customReports.ClickOnCustomizedReportNameInOndemand();
//		schedulereportpage.ClickOnScheduleNew();
//		//case-1
//		schedulereportpage.ClickOnMonthlyRadioButon();
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyHiddenScheduledCycleTimeAndDateField();
//		//case-2
//		schedulereportpage.ClickOnChange();
//		schedulereportpage.VerifyScheduledCycleTimeAndDateField();
//		schedulereportpage.SelectAnyDateInScheduledCycleTime();
//		schedulereportpage.SelectAnyDateInScheduledCycleDate();
//		//case-3
//		schedulereportpage.EnterEmailAddress(Config.Email);
//		schedulereportpage.ClickOnSchedule();
//	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				String FailedWindow = Initialization.driver.getWindowHandle();
				if (FailedWindow.equals(customReports.PARENTWINDOW)) {
					commonmethdpage.ClickOnHomePage();
				} else {
					reportsPage.SwitchBackWindow();
				}
			} catch (Exception e) {

			}
		}
	}
}
