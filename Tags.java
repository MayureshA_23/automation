package Tags_TestScript;

// make sure to change the name of the first tag name in config
// ensure two device names are mentioned in config
// make sure "CustomTag" is removed

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Tags extends Initialization {

	@Test(priority = 1, description = "1.Precondition Create FileTransfer Job")
	public void CreateINstallJobForTAGS() throws InterruptedException, IOException {

		Reporter.log("1.Precondition Create FileTransfer Job", true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnFileTransferJob();
		androidJOB.enterJobName(Config.FileTransferJobTAGS);
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileTRansferJobTAGS("./Uploads/UplaodFiles/FileStore/\\File.exe");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 2, description = "2.Pre condition Creste TextMessage Job")
	public void createTextMessage() throws InterruptedException {
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.verifyTextMessagePrompt();
		androidJOB.textMessageJobWhenNoFieldIsEmpty("TextJobToAddInTagsProperties", "TextVerifyJob", "notify Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority = 2, description = "2.Navigate to Tags Module")
	public void NavigateTags_TC_T_01() throws InterruptedException {
		Reporter.log("\n2.Navigate to Tags Module", true);
		Tags.ClickOnTags1();
	}
	
	@Test(priority = 2, description = "15.Verify Default tag is creating in Tags when devices are enrolled to console.")
	public void DefaultTagCreatingTnTagswhendevicesareEnrolledToConsole_TC_TG_011() throws InterruptedException {
		Reporter.log("\n15.Verify Default tag is creating in Tags when devices are enrolled to console.", true);
		Tags.ClickOnTags1();
		Tags.VerifyDefaultTagInTagsSection(Config.ModelName);
		Tags.ClickOnTagWithDevices(Config.ModelName, Config.DeviceName);

	}

	// Creating One new Tag

	@Test(priority = 3, description = "3.Create new tag")
	public void CreateTag_TC_T_02()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n3.Create new tag", true);
		Tags.ClickOnTags1();
		Tags.CreatTag(Config.TagNameDevice);
		Tags.verifyOfCreatedTag(Config.TagNameDevice);
	}

	// Adding Jobs/Profiles to Tag Properties Section

	@Test(priority = 4, description = "4.verify adding of Job/profiile in tag properties")
	public void TagverifyAddingOfJobProfiileInTagProperties() throws InterruptedException {
		Reporter.log("\n4.verify adding of Job/profiile in tag properties", true);
		Tags.ClickOnTags1();
		Tags.CreatTag("Kavya");
		Tags.ClickOnCreatedTag2("Kavya");
		Tags.ClickOnTagPropertiesButton();
		Tags.AddButtonClickTagProperties();
		Tags.SearchJob(Config.FileTransferJobTAGS, "Kavya");
		Tags.verifyOfJobInTagsProperty(Config.FileTransferJobTAGS);
		Tags.ClickOnTags1();

	}

	// Doing Rt.Click And TAGGING the device to multiple tag

	@Test(priority = 5, description = "5.verify applaying defualt job on Tag", dependsOnMethods = "TagverifyAddingOfJobProfiileInTagProperties")
	public void Tagverifyaddingofdevicetotag()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n5.verify applaying defualt job on Tag", true);

		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.taglistpopupSearchTextField2("Kavya");
		Tags.EnableTheSearchedTagNameFromTheList2("Kavya");
		Tags.ClickOnSaveTagButton();
		Tags.SuccessfulMessageOnTagMove();
		Tags.ClickOnTags1();
		Tags.ClickOnCreatedTag2("Kavya");
		commonmethdpage.clickOnGridrefreshbutton();
		androidJOB.clickOnJobhistoryTabTAGS();
		androidJOB.CheckStatusOfappliedInstalledJobTAGS(Config.FileTransferJobTAGS, 360);

	} // Tag_verify enabling of Force reapply on re-registartaion

	@Test(priority = 6, description = "6.verify enabling of Force reapply on re-registartaion")
	public void reapplyOnReRegistartaion() throws InterruptedException {
		Reporter.log("\n6.verify enabling of Force reapply on re-registartaion", true);
		Tags.ClickOnTags1();
		Tags.ClickOnTagPropertiesButton();
		Tags.EnableForcereapplyOnReregistartaion();

	}

	// Deleting the jobs/Profile from Tag sections

	@Test(priority = 7, description = "7.verify deletion of job/profile from tag")
	public void DeleteJobFromTagList() throws InterruptedException {
		Reporter.log("\n7.verify deletion of job/profile from tag", true);

		Tags.ClickOnTags1();
		Tags.CreatTag("AutomationTag");
		Tags.ClickOnCreatedTag2("AutomationTag");
		Tags.ClickOnTagPropertiesButton();
		Tags.AddButtonClickTagProperties();
		Tags.SearchJob("TextJobToAddInTagsProperties", "AutomationTag");
		Tags.verifyOfJobInTagsProperty("TextJobToAddInTagsProperties");

		Tags.ClickOnCreatedTag2("AutomationTag");
		Tags.ClickOnTagPropertiesButton();
		Tags.DeleteFileJobFromTagProperties("TextJobToAddInTagsProperties", "AutomationTag");

	}

	@Test(priority = 8, description = "8.verify Rename tag")
	public void RenameTag_TC_T_03() throws InterruptedException {
		Reporter.log("\n8.Verify Rename tag", true);
		Tags.ClickOnTags1();
		Tags.CreatTag("!!!AutomationTag");
		Tags.RenamingTag("!!!AutomationTag", "RenamedTag");
		Tags.verifyOfRenamedTag("RenamedTag");

	}

	@Test(priority = 9, description = "9.verify Delete tag")
	public void DeleteTag_TC_T_04() throws InterruptedException {
		Reporter.log("\n9.Verify delete tag", true);
		Tags.ClickOnTags1();
		Tags.CreatTag("@TagToDelete");
		Tags.DeletingTag("@TagToDelete");
		Tags.verifyOfDeletedTag("@TagToDelete");
	}

	@Test(priority = 10, description = "10. Assign Tag To Device")
	public void AssignTagToDevice_TC_T_05()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n10.Assign Tag To Device", true);
		Tags.ClickOnTags1();
		Tags.CreatTag("!0123");
		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.taglistpopupSearchTextField2("!0123");
		Tags.AvailibilityOfCreateCustomTag();
		Tags.EnableTheSearchedTagNameFromTheList2("!0123");
		Tags.ClickOnSaveTagButton();
		Tags.ClickOnTags1();
		Tags.ClickOnTagWithDevices("!0123", Config.DeviceName);
	}

	@Test(priority = 11, description = "11.Remove Device From Tag", dependsOnMethods = "AssignTagToDevice_TC_T_05")
	public void RemoveDeviceFromTag_TC_T_07() throws InterruptedException {
		Reporter.log("\n11.Remove Device From Tag", true);
		Tags.ClickOnTags1();
		Tags.ClickOnTagWithDevices("!0123", Config.DeviceName);
		Tags.RemoveDeviceFromTag();
		Tags.taglistpopupSearchTextField2("!0123");
		Tags.EnableTheSearchedTagNameFromTheList2("!0123");
		Tags.ClickOnSaveTagButtonForRemove();
		Tags.SuccessfulMessageOnRemovingDeviceFromTag();
		Tags.ClickOnGroup();

	}

	@Test(priority = 12, description = "12.Add Tags To Multiple Devices")
	public void AddTagsToMultipleDevices_TC_T_08()
			throws InterruptedException, AWTException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n12.Add Tags To Multiple Devices", true);
		Tags.ClickOnTags1();
		Tags.CreatTag(Config.FirstTagName);
		Tags.ClickOnGroup();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName, Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		Tags.RightClickTag_MultipleDevice();
		Tags.AvailibilityOfCreateCustomTag();
		Tags.taglistpopupSearchTextField2(Config.FirstTagName);
		Tags.EnableTheSearchedTagNameFromTheList2(Config.FirstTagName);
		Tags.ClickOnSaveTagButton();
		Tags.ClickOnTags();
		Tags.ClickOnTagMultipleDevices(Config.FirstTagName);
		commonmethdpage.SearchDevice(Config.DeviceName);
		Tags.verifyDeviceInTag(Config.DeviceName);
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		Tags.verifyDeviceInTag(Config.MultipleDevices1);
	}

	@Test(priority = 13, description = "13.Remove Tags To Multiple Devices")
	public void RemoveTagsToMultipleDevices_TC_T_09() throws InterruptedException, AWTException {
		Reporter.log("\n13.Remove Tags To Multiple Devices", true);
		Tags.ClickOnTags();
		Tags.ClickOnTagMultipleDevices(Config.FirstTagName);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName, Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		Tags.RemoveMultipleDeviceFromTag(); 
		Tags.taglistpopupSearchTextField2(Config.FirstTagName);
		Tags.EnableTheSearchedTagNameFromTheList2(Config.FirstTagName);
		Tags.ClickOnSaveTagButtonForRemove();
		Tags.VerifyDeviceNotPresentInTags(); 
		Tags.DeleteTagFirstTagName();
		Tags.ClickOnTags1();
		Tags.DeleteTag_TagNameDevicePlatFormName();

	}

	// Kavya

	@Test(priority = 16, description = "16.Verify force reapply on re-registration in tags")
	public void Verifyforcereapplyonreregistration_TC_TG_012()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n16.Verify force reapply on re-registration in tags", true);

		Tags.ClickOnTags1();
		Tags.CreatTag("@!Test");

		Tags.ClickOnCreatedTag2("@!Test");
		Tags.ClickOnTagPropertiesButton();
		Tags.AddButtonClickTagProperties();
		Tags.SearchJob(Config.FileTransferJobTAGS, "@!Test");
		Tags.verifyOfJobInTagsProperty(Config.FileTransferJobTAGS);
		Tags.ClickOnCreatedTag2("@!Test");
		Tags.ClickOnTagPropertiesButton();
		Tags.EnableForcereapplyOnReregistartaion();

		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.MultipleDevices1);
		Tags.RightClickTag(Config.MultipleDevices1);
		Tags.taglistpopupSearchTextField2("@!Test");
		Tags.EnableTheSearchedTagNameFromTheList2("@!Test");
		Tags.ClickOnSaveTagButton();
		Tags.SuccessfulMessageOnTagMove();

		commonmethdpage.AppiumConfigurationCommonMethod("com.nix", "com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDeviceIDinDevice();
		androidJOB.clickOnYesBtnInCurrentDeviceIDTab();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();

		androidJOB.clickOnJobhistoryTabTAGS();
		androidJOB.CheckStatusOfappliedInstalledJobTAGS(Config.FileTransferJobTAGS, 360);

	}

	@Test(priority = 17, description = "17.Verify creating new tags with ZLGOTA and EFOTA text in it")
	public void RemoveTagsToMultipleDevices_TC_DG_232() throws InterruptedException {
		Reporter.log("\n17.Verify creating new tags with ZLGOTA and EFOTA text in it", true);
		Tags.ClickOnTags1();
		Tags.VerifyErrorMessageForZLGOTAAndEFOTA("ZLGOTA", "EFOTA");
		Tags.CloseAddingNewTagPopup();
		Filter.ClickOnGroups();
	}

	@Test(priority = 18, description = "18.Verify editing Efota tag name created")
	public void verifyOfEditEFOTAtag_TC_TG_026() throws InterruptedException {
		Reporter.log("\n18.Verify editing Efota tag name created", true);
		Tags.ClickOnTags1();
		Tags.clickOnEFOTAtagAndEdit("EFOTA");

	}

	@Test(priority = 19, description = "19.To verify basic search in Tags")
	public void verifyBasicSearchInTags_TC_TG_028()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n19.To verify basic search in Tags");
		// Adding Note to thedevice
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnNotesEdit();
		deviceGrid.clearAddNotesField();
		deviceGrid.enterNotefordevice(Config.EnterNotes);
		deviceGrid.clickOnNoteSaveButton();

		Tags.ClickOnTags1();
		Tags.CreatTag("@!Test");
		Tags.ClickOnCreatedTag2("@!Test");
		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.taglistpopupSearchTextField2("@!Test");
		Tags.EnableTheSearchedTagNameFromTheList2("@!Test");
		Tags.ClickOnSaveTagButton();
		Tags.SuccessfulMessageOnTagMove();
		Tags.ClickOnTags1();
		Tags.ClickOnCreatedTag2("@!Test");
		commonmethdpage.SearchDevice(Config.DeviceName);
		Tags.verifyDeviceInTag(Config.DeviceName);

		commonmethdpage.SearchDevice(Config.ModelName);
		Tags.verifyDeviceInTag(Config.ModelName);

		commonmethdpage.SearchDevice(Config.EnterNotes);
		Tags.verifyDeviceInTag(Config.EnterNotes);
	}

	@Test(priority = 20, description = "20.Verify basic search by entering multiple values.")
	public void verifyBasicSearchByMultipleValuesInTags_TC_TG_029()
			throws InterruptedException, AWTException, EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n20.Verify basic search by entering multiple values.", true);
		// Adding Note to the device commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.clickOnNotesEdit();
		deviceGrid.clearAddNotesField();
		deviceGrid.enterNotefordevice(Config.EnterNotes);
		deviceGrid.clickOnNoteSaveButton();

		// Adding Note to the device
		commonmethdpage.SearchDevice(Config.MultipleDevices1);
		deviceGrid.clickOnNotesEdit();
		deviceGrid.clearAddNotesField();
		deviceGrid.enterNotefordevice(Config.EnterNotes1);
		deviceGrid.clickOnNoteSaveButton();

		Tags.ClickOnTags1();
		Tags.CreatTag(Config.FirstTagName);
		Tags.ClickOnGroup();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName, Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		Tags.RightClickTag_MultipleDevice();
		Tags.AvailibilityOfCreateCustomTag();
		Tags.taglistpopupSearchTextField2(Config.FirstTagName);
		Tags.EnableTheSearchedTagNameFromTheList2(Config.FirstTagName);
		Tags.ClickOnSaveTagButton();
		Tags.ClickOnTags();
		Tags.ClickOnTagMultipleDevices(Config.FirstTagName);
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName, Config.MultipleDevices1);
		Tags.verifyDevicesForMulValSearch(Config.DeviceName, Config.MultipleDevices1);

		commonmethdpage.SearchForMultipleDevice(Config.ModelName, Config.ModelName1);
		Tags.verifyDevicesForMulValSearch(Config.ModelName, Config.ModelName1);

		commonmethdpage.SearchForMultipleDevice(Config.EnterNotes, Config.EnterNotes1);
		Tags.verifyDevicesForMulValSearch(Config.EnterNotes, Config.EnterNotes1);

	}

	@Test(priority = 21, description = "21.Verify Error message when no device with searched values are present in the Tag")
	public void verifyBasicSearchByMultipleValuesInTags_TC_TG_030()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n21.Verify Error message when no device with searched values are present in the Tag", true);
		Tags.ClickOnTags1();
		Tags.CreatTag(Config.FirstTagName);
		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.taglistpopupSearchTextField2(Config.FirstTagName);
		Tags.AvailibilityOfCreateCustomTag();
		Tags.EnableTheSearchedTagNameFromTheList2(Config.FirstTagName);
		Tags.ClickOnSaveTagButton();
		Tags.ClickOnTags1();
		Tags.ClickOnCreatedTag2(Config.FirstTagName);
		commonmethdpage.SearchDevice("@RegressionDevice");
		Tags.verifyOfErrorMsgForNoDeviceInTag();

	}

	@Test(priority = 22, description = "22.Verify enabling Advance ssearch in Tags")
	public void verifyBasicSearchByMultipleValuesInTags_TC_TG_031()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n22.Verify enabling Advance ssearch in Tags", true);
		Tags.ClickOnTags1();
		Tags.CreatTag("!Tag_TestAutomation");
		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.taglistpopupSearchTextField2("!Tag_TestAutomation");
		Tags.AvailibilityOfCreateCustomTag();
		Tags.EnableTheSearchedTagNameFromTheList2("!Tag_TestAutomation");
		Tags.ClickOnSaveTagButton();
		Tags.ClickOnTags1();
		Tags.ClickOnCreatedTag2("!Tag_TestAutomation");
		deviceGrid.SelectSearchOption("Advanced Search");
		Tags.verifyofenablingAdvancessearchinTags();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 23, description = "23.Verify Deleting EFOTA/ZLGOTA tags")
	public void verifyBasicSearchByMultipleValuesInTags_TC_TG_034() throws InterruptedException {
		Reporter.log("\n23.Verify Deleting EFOTA/ZLGOTA tags", true);
		Tags.ClickOnTags1();
		Tags.clickOnEFOTAorZLGOTAtagAndDelete("EFOTA");
		Tags.clickOnEFOTAorZLGOTAtagAndDelete("ZLGOTA");
	}

	@Test(priority = 24, description = "24.Verify functionality of Advance search")
	public void verifyBasicSearchByModelInTags_TC_TG_032()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n24.Verify functionality of Advance search", true);

		Tags.ClickOnTags1();
		Tags.CreatTag("!Tag_TestAutomation");
		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.taglistpopupSearchTextField2("!Tag_TestAutomation");
		Tags.AvailibilityOfCreateCustomTag();
		Tags.EnableTheSearchedTagNameFromTheList2("!Tag_TestAutomation");
		Tags.ClickOnSaveTagButton();

		Tags.ClickOnTags1();
		Tags.ClickOnCreatedTag2("!Tag_TestAutomation");
		deviceGrid.SelectSearchOption("Advanced Search");

		deviceGrid.enterValueInModelFieldForAdvSearch(Config.ModelName);
		deviceGrid.verifyEnrolledDeviceAvailable();
		deviceGrid.SelectSearchOption("Basic Search");
	}

	@Test(priority = 25, description = "25.Verify functionality of Advance search by searching multiple values")
	public void verifyAdvanceSearchByMultipleValuesInTags_TC_TG_033()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException {
		Reporter.log("\n25.Verify functionality of Advance search by searching multiple values", true);
		Tags.ClickOnTags1();
		Tags.CreatTag("!Tag_TestAutomation");
		Tags.ClickOnGroup();
		commonmethdpage.SearchForMultipleDevice(Config.DeviceName, Config.MultipleDevices1);
		quickactiontoolbarpage.SelectingMultipleDevices();
		Tags.RightClickTag_MultipleDevice();
		Tags.AvailibilityOfCreateCustomTag();
		Tags.taglistpopupSearchTextField2("!Tag_TestAutomation");
		Tags.EnableTheSearchedTagNameFromTheList2("!Tag_TestAutomation");
		Tags.ClickOnSaveTagButton();
		Tags.ClickOnTags();
		Tags.ClickOnTagMultipleDevices("!Tag_TestAutomation");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.MultipleDeviceName);
		deviceGrid.verifyEnrolledDeviceForMultiSearch(Config.DeviceName, Config.MultipleDevices1);

	}

	@Test(priority = 26, description = "26.verify order of tags is consistent in both tags sections and right click tags pop up")
	public void verifyOfTagsSections_TC_TG_039()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("26.verify order of tags is consistent in both tags sections and right click tags pop up", true);

		Tags.ClickOnTags1();
		Tags.getTagsListFromTagsSection();
		Tags.ClickOnGroup();
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		Tags.RightClickTag(Config.DeviceName);
		Tags.getTagsListFromTagsSection_RightClick();
		Tags.compareTagsList();
	}

	
}
