package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.PassScreenshot;
import Library.WebDriverCommonLib;

public class QuickActionToolBar_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	PassScreenshot ps=new PassScreenshot();
	Actions act=new Actions(Initialization.driver);
	String parentwinID;
	String childwinID;
	String URL;
	String Time;
	String Schedule_Time;
	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
	String originalHandle = Initialization.driver.getWindowHandle();

	@FindBy(id="applyJobButton")
	private WebElement ApplyJobButton;

	@FindBy(xpath="//h4[contains(text(),'Apply Job')]/preceding-sibling::button")
	private WebElement applyJobCloseBtn;

	@FindBy(id="apply_job_header")
	private WebElement applyJobTitle;

	@FindBy(xpath="//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
	private WebElement ApplyJobSearchButton;

	@FindBy(xpath="//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Job/Folder Name']")
	private WebElement ApplyJobFolderNameColumn;

	@FindBy(xpath="//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Type']")
	private WebElement ApplyJobTypeColumn;

	@FindBy(xpath="//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Platform']")
	private WebElement ApplyJobPlatformColumn;

	@FindBy(xpath="//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Size']")
	private WebElement ApplyJobSizeColumn;

	@FindBy(xpath="//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Last Modified']")
	private WebElement ApplyJobLastModifiedColumn;

	@FindBy(id="scheduleJob")
	private WebElement ScheduleJobButton;

	@FindBy(id="pushjob")
	private WebElement ScheduleJobSummary;

	@FindBy(id="applyJob_okbtn")
	private WebElement applyJobApplyButton;

	@FindBy(xpath="//*[@id='applyJob_table_cover']/div[1]/div[1]/div[1]/button")
	private WebElement applyJobRefreshBtn;

	@FindBy(xpath="//span[text()='Please select a device.']")
	private WebElement ErrorMessageSelectDevice;

	@FindBy(xpath="//span[text()='Please select a job.']")
	private WebElement ApplyJobErrorMessage;

	@FindBy(xpath="//div[@id='scheduleJob_modal']/div/h4")
	private WebElement ScheduleJobHeader;

	@FindBy(id="immediateSchedule")
	private WebElement ScheduleJobImmediatelyButton;

	@FindBy(xpath="//span[text()='Immediately']")
	private WebElement ScheduleJobImmediatelyMessage;

	@FindBy(id="recursiveScheduletime")
	private WebElement ScheduleJobPeriodicallyButton;

	@FindBy(xpath="//span[text()='Periodically']")
	private WebElement ScheduleJobPeriodicallyMessage;

	@FindBy(id="recursiveIntervaltime")
	private WebElement ScheduleJobPeriodicallyTextbox;

	@FindBy(xpath="//span[text()='Value should be greater than 4 minutes.' and @data-notify-text='']")
	private WebElement ScheduleJobPeriodicallyErrorMessage;

	@FindBy(id="recursiveSchedule")
	private WebElement ScheduleJobScheduleDayTimeButton;

	@FindBy(xpath="//div[@id='recursiveInterval_div']/div/input[contains(@class,'srtTime')]")
	private WebElement ScheduleDayTimeInterval;

	@FindBy(xpath="//div[@id='recursiveInterval_div']/div/div[contains(@class,'days_list')]/div/span")
	private List<WebElement> ScheduleDayInterval;

	@FindBy(xpath="//span[text()='Please select time.' and @data-notify-text='']")
	private WebElement ScheduleDaysAndTime_TimeErrorMessage;

	@FindBy(xpath="//span[text()='Please select atleast one day.' and @data-notify-text='']")
	private WebElement ScheduleDaysAndTime_DayErrorMessage;

	@FindBy(xpath="//span[text()='All Days']/preceding-sibling::input")
	private WebElement ScheduleDay_AllDay;

	@FindBy(xpath="//span[text()='Sun']/preceding-sibling::input")
	private WebElement ScheduleDay_SunDay;

	@FindBy(xpath="//span[text()='Mon']/preceding-sibling::input")
	private WebElement ScheduleDay_MonDay;

	@FindBy(xpath="//span[text()='Tue']/preceding-sibling::input")
	private WebElement ScheduleDay_TueDay;

	@FindBy(xpath="//span[text()='Wed']/preceding-sibling::input")
	private WebElement ScheduleDay_WedDay;

	@FindBy(xpath="//span[text()='Thur']/preceding-sibling::input")
	private WebElement ScheduleDay_ThurDay;

	@FindBy(xpath="//span[text()='Fri']/preceding-sibling::input")
	private WebElement ScheduleDay_FriDay;

	@FindBy(xpath="//span[text()='Sat']/preceding-sibling::input")
	private WebElement ScheduleDay_SatDay;

	@FindBy(xpath="//input[contains(@class,'periodic ') and @placeholder='HH:mm']")
	private WebElement ScheduleTime_TextBox;

	@FindBy(xpath="//span[text()='Schedule Days And Time']")
	private WebElement ScheduleJobScheduleDayTimeMessage;

	@FindBy(xpath="//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_' and contains(@style,'display: block')]/div/button[@class='xdsoft_next']")
	private WebElement ScheduleJobScheduleDay;

	@FindBy(id="ScheduleWithDateAndTime")
	private WebElement ScheduleJobSelectDateTimeButton;

	@FindBy(xpath="//span[text()='Select date and time']")
	private WebElement ScheduleJobSelectDateTimeMessage;

	@FindBy(id="okbutton")
	private WebElement ScheduleJobOkButton;

	@FindBy(xpath="//div[@id='scheduleJob_modal']/div/button[@class='close']")
	private WebElement ScheduleJobCloseButton;

	@FindBy(xpath="//span[text()='Initiated apply job on selected device.']")
	private WebElement ApplyJobConfirmationMessage;

	@FindBy(xpath="//div[@id='JobGridContainer']/div/div/div/div/input")
	private WebElement ApplyJobSearchTextField;

	@FindBy(xpath="//div[@id='JobGridContainer']/div/div/div/div/button")
	private WebElement ApplyJobrefreshbutton;

	@FindBy(id="datetimepicker")
	private WebElement SelectDateTime;

	@FindBy(xpath="//div[contains(@class,'xdsoft_datetimepicker') and contains(@style,'display: block')]")
	private WebElement SelectDateTimeInterval;

	@FindBy(xpath="//div[contains(@class,'xdsoft_datetimepicker') and contains(@style,'display: block')]/div/div/table")
	private WebElement SelectDateTimeIntervaltable;

	@FindBy(xpath="//div[contains(@class,'xdsoft_datetimepicker') and contains(@style,'display: block')]/div/div/div/span[text()='"+Config.SelectDate_ApplyJobMonth+"']")
	private WebElement SelectDateTimeIntervalMonth;

	@FindBy(xpath="//div[contains(@class,'xdsoft_datetimepicker') and contains(@style,'display: block')]/div/div/div/span[text()='"+Config.SelectDate_ApplyJobYear+"']")
	private WebElement SelectDateTimeIntervalYear;

	@FindBy(xpath="//div[contains(@class,'xdsoft_datetimepicker') and contains(@style,'display: block')]/div[@class='xdsoft_timepicker active']")
	private WebElement SelectDateTimeIntervalTime;

	@FindBy(xpath="//div[@id='download_job_via_div']/div[text()='Deploy job on']")
	private WebElement ConnectivityText;

	@FindBy(xpath="//select[@id='download_job_via']/option")
	private List<WebElement> ConnectivityOptions;

	@FindBy(id="download_job_via")
	private WebElement ConnectivityDropDown;
	
	@FindBy(id="DeviceChargingState")
	private WebElement DevicechargingStateDropDown;

	public void ClickOnApplyJob() throws InterruptedException
	{
		ApplyJobButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForidPresent("scheduleJob");
		sleep(5);
		Reporter.log("PASS >> Clicked on Apply Job Button ,Navigates successfully to Apply Job button",true);
	}

	public void ClickOnApplyJobCloseBtn() throws InterruptedException {
		applyJobCloseBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
		Reporter.log("PASS >> Clicked on Close Button of Apply Job", true);
	}

	public void VerifyOfApplyJobPage() {
		boolean flag = applyJobTitle.isDisplayed();
		String PassStatement = "PASS >>  title is displayed successfully when clicked on Apply Job Button";
		String FailStatement = "Fail >>  title is not displayed when clicked on Apply Job Button";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		WebElement[] ApplyJobElement = { ApplyJobSearchButton, applyJobRefreshBtn, ApplyJobFolderNameColumn,
				ApplyJobTypeColumn, ApplyJobPlatformColumn, ApplyJobSizeColumn, ApplyJobLastModifiedColumn,
				applyJobApplyButton, };
		String[] ApplyJobText = { "Search Field", "Refresh Button", "Job/Folder Name Column", "Type Column",
				"Platform Column", "Size Column", "Last Modified Column", "Apply Job Ok Button"};
		for (int i = 0; i < ApplyJobElement.length - 1; i++) 
		{
			boolean value=ApplyJobElement[i].isDisplayed();
			PassStatement = "PASS >> " + ApplyJobText[i]+ " is displayed successfully when clicked on Apply Job Button";
			FailStatement = "Fail >> " + ApplyJobText[i] + " is not displayed even when clicked on Apply Job Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

		boolean value=ScheduleJobButton.isDisplayed();
		PassStatement = "PASS >>  button is displayed successfully when clicked on Apply Job Button";
		FailStatement = "Fail >>  button is not displayed when clicked on Apply Job Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		boolean value1 = ScheduleJobSummary.isDisplayed();
		PassStatement = "PASS >>  Summary is displayed successfully when clicked on Apply Job Button";
		FailStatement = "Fail >>  Summary is not displayed when clicked on Apply Job Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}
	
	public void VerifyChargingUIInJobApplyJob()
	{
		boolean DeviceChargingStateDrop = DevicechargingStateDropDown.isDisplayed();
		String PassStatement="Device Charging State Drop Down Is Displayed";
		String FailStatement="Device Charging State Drop Down Is not Displayed";
		ALib.AssertTrueMethod(DeviceChargingStateDrop,PassStatement,FailStatement);
	}
	
	public void VerifyChargingStateDropDownOptions()
	{
		
	    String[] ExpectedOptions={"Don't Care","Plugged In"};
		Select sel=new Select(DevicechargingStateDropDown);
		List<WebElement> Options = sel.getOptions();
		for(int i=0;i<Options.size();i++)
		{
			if(Options.get(i).getText().equals(ExpectedOptions[i]))
			{
				Reporter.log("Options are shown correctly",true);
			}
			
			else
			{
				ALib.AssertFailMethod("Options are not shown correctly");
			}
		}
	}

	public void ClickOnApplyJob_UM()
	{
		ApplyJobButton.click();
	}

	public void ClickOnApplyJobOkButton_Error()
	{
		applyJobApplyButton.click();
		Reporter.log("Clicked on Apply Job Ok Button",true);
	}

	public void ClickOnApplyJobOkButton() throws Throwable
	{
		applyJobApplyButton.click();
		Reporter.log("Clciked on Apply Button");
	}

	public void ErrorMessage_SelectDevice() throws Throwable
	{
		String PassStatement = "PASS >> Alert Message : 'Please select a device.' is displayed successfully when clicked on ApplyJob without selecting device";
		String FailStatement = "Fail >> Alert Message : 'Please select a device.' is not displayed even when clicked on ApplyJob without selecting device";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageSelectDevice, true, PassStatement,
				FailStatement, 5);
	}

	public void ApplyJobErrorMessage() throws Throwable
	{
		String PassStatement = "PASS >> Alert Message : 'Please select a job.' is displayed successfully when clicked on Apply button without selecting job";
		String FailStatement = "Fail >> Alert Message : 'Please select a job.' is not displayed even when clicked on Apply button without selecting job";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ApplyJobErrorMessage, true, PassStatement,
				FailStatement, 5);
	}

	public void ClickOnConfigureScheduleTime() throws Throwable
	{
		ScheduleJobButton.click();
		waitForidPresent("okbutton");
		sleep(2);
		Reporter.log("Click on Configure Schedule Time",true);
	}

	public void VerifyOfScheduleTime()
	{
		String ActualValue = ScheduleJobHeader.getText();
		String ExpectedValue = "Schedule Job";
		String PassStatement = "PASS >> " + ActualValue+ " title is displayed successfully when clicked on Configure Schedule Time Button";
		String FailStatement = "Fail >> " + ActualValue + " title is not displayed when clicked on Configure Schedule Time Button";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		WebElement[] ScheduleJobElement = { ScheduleJobImmediatelyButton, ScheduleJobPeriodicallyButton, ScheduleJobScheduleDayTimeButton,
				ScheduleJobSelectDateTimeButton};
		String[] ScheduleJobActualText = { ScheduleJobImmediatelyMessage.getText(), ScheduleJobPeriodicallyMessage.getText(), ScheduleJobScheduleDayTimeMessage.getText(),
				ScheduleJobSelectDateTimeMessage.getText() };
		String[] ScheduleJobExpectedText = { "Immediately", "Periodically", "Schedule Days And Time","Select date and time"};
		for (int i = 0; i < ScheduleJobElement.length - 1; i++) 
		{
			boolean value=ScheduleJobElement[i].isEnabled() && ScheduleJobActualText[i].equals(ScheduleJobExpectedText[i]);
			PassStatement = "PASS >> " + ScheduleJobExpectedText[i]+ " is displayed successfully when clicked on Configure Schedule Time Button";
			FailStatement = "Fail >> " + ScheduleJobExpectedText[i] + " is not displayed even when clicked on Configure Schedule Time Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		boolean value=ScheduleJobOkButton.isDisplayed();
		PassStatement = "PASS >> Schedule Job Ok button is displayed successfully when clicked on Configure Schedule Time Button";
		FailStatement = "Fail >> Schedule Job Ok button is not displayed even when clicked on Configure Schedule Time Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnScheduleJobCloseBtn() throws InterruptedException {
		ScheduleJobCloseButton.click();
		waitForidPresent("applyJobButton");
		sleep(2);
		Reporter.log("PASS >> Clicked on Close Button of Schedule Job", true);
	}

	public void ClickOnPeriodically() throws Throwable
	{
		ScheduleJobPeriodicallyButton.click();
		sleep(2);
	}

	public void VerifyOfPeriodicallybutton()
	{
		boolean value=ScheduleJobPeriodicallyTextbox.isDisplayed();
		String PassStatement = "PASS >> ScheduleJob Periodically Textbox is displayed successfully when clicked on Periodically Button";
		String FailStatement = "Fail >> ScheduleJob Periodically Textbox is not displayed even when clicked on Periodically Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnScheduleJobOkBtn() throws InterruptedException {
		ScheduleJobOkButton.click();
		waitForidPresent("applyJobButton");
		sleep(2);
		Reporter.log("PASS >> Clicked on Ok Button of Schedule Job", true);
	}

	public void ClickOnScheduleJobOkBtn_Error() throws InterruptedException {
		ScheduleJobOkButton.click();
		Reporter.log("PASS >> Clicked on Ok Button of Schedule Job", true);
	}

	public void ScheduleJobPeriodicallyErrorMessage() throws Throwable
	{
		String PassStatement = "PASS >> Alert Message : 'Value should be greater than 4 minutes.' is displayed successfully when clicked on Ok button without selecting time for periodically Configure Schedule Time";
		String FailStatement = "Fail >> Alert Message : 'Value should be greater than 4 minutes.' is not displayed even when clicked on Ok button without selecting time for periodically Configure Schedule Time";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ScheduleJobPeriodicallyErrorMessage, true, PassStatement,
				FailStatement, 5);
	}

	public void EnterPeriodicallyTime(String Time)
	{
		ScheduleJobPeriodicallyTextbox.sendKeys(Time);
	}

	public void VerifyOfPeriodicallySummary(String Time)
	{
		String ActualValue = ScheduleJobSummary.getText();
		String ExpectedValue = "Job will be pushed for the device after every "+Time+" minutes.";
		boolean value=ScheduleJobSummary.isDisplayed() && ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> " + ActualValue+ " Summary is displayed successfully when Periodically option is selected with time "+Time+" minutes";
		String FailStatement = "Fail >> " + ActualValue + " Summary is not displayed when Periodically option is selected with time "+Time+" minutes";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ApplyJob(String JobName) throws Throwable
	{
		ApplyJobButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForidPresent("applyJob_okbtn");
		sleep(4);
		Reporter.log("Clicked on Apply Job,Navigates successfully to Apply Job popup",true);
		ApplyJobSearchTextField.sendKeys(JobName);
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
		{}
		sleep(4);
		ApplyJobrefreshbutton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
		{}
		sleep(2);
		Initialization.driver.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr/td/p[text()='"+JobName+"']")).click();
		ClickOnApplyJobOkButton();
		sleep(6);
	}

	public void ApplyJob_Schedule(String JobName) throws Throwable
	{
		ApplyJobSearchTextField.sendKeys(JobName);
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
		{}
		sleep(4);
		ApplyJobrefreshbutton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
		{}
		sleep(2);
		Initialization.driver.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr/td/p[text()='"+JobName+"']")).click();
		ClickOnApplyJobOkButton();
	}

	public void ApplyJobConfirmationMessage() throws Throwable
	{
		String PassStatement = "PASS >> Confirmation Message : 'Initiated apply job on selected device.' is displayed successfully when job is applied to device";
		String FailStatement = "Fail >> Confirmation Message : 'Initiated apply job on selected device.' is not displayed even when job is applied to device";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ApplyJobConfirmationMessage, true, PassStatement,FailStatement, 5);
	}

	public void VerifyOfPeridically_Queue(String JobName, String Time)
	{
		boolean value=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(text(),'Job will be applied every "+Time+" minutes.')]")).isDisplayed();
		String PassStatement = "PASS >> " + JobName+ " is applied successfully with Periodic time of "+Time+" minutes when Periodically option is selected with time "+Time+" minutes";
		String FailStatement = "Fail >> " + JobName + " Summary is not applied when Periodically option is selected with time "+Time+" minutes";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnScheduleDaysAndTime() throws Throwable
	{
		ScheduleJobScheduleDayTimeButton.click();
		sleep(5);
		Reporter.log("Selecting Schedule Days and Time",true);
	}

	public void VerifyOfScheduleDaysAndTimebutton()
	{
		boolean value=ScheduleDayTimeInterval.isDisplayed();
		String PassStatement = "PASS >> Schedule Days And Time ,Interval textbox is displayed successfully when clicked on Schedule Days and Time Button";
		String FailStatement = "Fail >> Schedule Days And Time, Interval textbox is not displayed even when clicked on Schedule Days and Time Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String[] ScheduleDaysAndTimeText={"Sun","Mon","Tue","Wed","Thur","Fri","Sat","All Days"};
		for(int i=0;i<ScheduleDaysAndTimeText.length;i++)
		{
			String ActualValue=ScheduleDayInterval.get(i).getText();
			String ExpectedValue=ScheduleDaysAndTimeText[i];
			PassStatement="PASS >> Schedule Days And Time Interval, Days List is displayed successfully when clicked on Schedule Days and Time Button "+ActualValue;
			FailStatement="Fail >> Schedule Days And Time Interval, Days List is not displayed even when clicked on Schedule Days and Time Button "+ActualValue;
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		}
	}

	public void ScheduleDaysAndTime_TimeErrorMessage() throws Throwable
	{
		String PassStatement = "PASS >> Alert Message : 'Please select time.' is displayed successfully when clicked on Ok button without selecting time for Schedule Days and Time";
		String FailStatement = "Fail >> Alert Message : 'Please select time.' is not displayed even when clicked on Ok button without selecting time for Schedule Days and Time";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ScheduleDaysAndTime_TimeErrorMessage, true, PassStatement,
				FailStatement, 5);
	}

	public void ClickOnAllDay() throws Throwable
	{
		ScheduleDay_AllDay.click();
		sleep(2);
	}

	public void ClickOnSunday() throws Throwable
	{
		ScheduleDay_SunDay.click();
		sleep(2);
	}

	public void ClickOnMonday() throws Throwable
	{
		ScheduleDay_MonDay.click();
		sleep(2);
	}

	public void ClickOnTuesday() throws Throwable
	{
		ScheduleDay_TueDay.click();
		sleep(2);
	}

	public void ClickOnWednesday() throws Throwable
	{
		ScheduleDay_WedDay.click();
		sleep(2);
	}

	public void ClickOnThursday() throws Throwable
	{
		ScheduleDay_ThurDay.click();
		sleep(2);
	}

	public void ClickOnFriday() throws Throwable
	{
		ScheduleDay_FriDay.click();
		sleep(2);
	}

	public void ClickOnSaturday() throws Throwable
	{
		ScheduleDay_SatDay.click();
		sleep(2);
	}

	public void ScheduleDaysAndTime_DayErrorMessage() throws Throwable
	{
		String PassStatement = "PASS >> Alert Message : 'Please select atleast one day.' is displayed successfully when clicked on Ok button without selecting Days for Schedule Days and Time";
		String FailStatement = "Fail >> Alert Message : 'Please select atleast one day.' is not displayed even when clicked on Ok button without selecting Days for Schedule Days and Time";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ScheduleDaysAndTime_DayErrorMessage, true, PassStatement,
				FailStatement, 5);
	}

	public void ScheduleTime() throws Throwable
	{
		ScheduleTime_TextBox.click();
		sleep(5);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		System.out.println( sdf.format(cal.getTime()) );
		Schedule_Time=sdf.format(cal.getTime());
		String[] timeElement=sdf.format(cal.getTime()).split(":");
		System.out.println(timeElement[0]);System.out.println(timeElement[1]);
		if(timeElement[0].startsWith("0"))
		{
			timeElement[0]=timeElement[0].replaceFirst("0", "");
		}
		if(timeElement[1].startsWith("0"))
		{
			timeElement[1]=timeElement[1].replaceFirst("0", "");
		}
		WebElement mobile = Initialization.driver.findElement(By.xpath("//div[contains(@style,'block')]/div/div/div/div[@data-minute='"+timeElement[1]+"' and @data-hour='"+timeElement[0]+"']"));
		mobile.click();
	}

	public void VerifyOfScheduleDayAndTimeSummary_Single(String Day)
	{
		String ActualValue = ScheduleJobSummary.getText();
		String ExpectedValue = "Job will be applied at "+Schedule_Time+" on every "+Day+".";
		boolean value=ScheduleJobSummary.isDisplayed() && ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> " + ActualValue+ " Summary is displayed successfully when Schedule Days and Time option is selected with Single Day '"+Day+"'";
		String FailStatement = "Fail >> " + ActualValue + " Summary is not displayed when Schedule Days and Time option is selected with Single Day '"+Day+"'";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfScheduleDayAndTimeSingle_Queue(String JobName, String Day) throws Throwable
	{
		DateFormat formatterIST = new SimpleDateFormat("HH:mm");
		formatterIST.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // better than using IST
		Date date = formatterIST.parse(Schedule_Time);
		System.out.println(formatterIST.format(date)); // output: 15-05-2014 00:00:00
		DateFormat formatterUTC = new SimpleDateFormat("HH:mm a");
		formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC")); // UTC timezone
		System.out.println(formatterUTC.format(date)); // output: 14-05-2014 18:30:00
		boolean value=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(text(),'Job will be applied at ["+formatterUTC.format(date)+" (UTC)] on every "+Day+".')]")).isDisplayed();
		String PassStatement = "PASS >> " + JobName+ " is applied successfully with Schedule day and time of "+formatterIST.format(date)+" and "+Day+" when Schedule Days and time option is selected";
		String FailStatement = "Fail >> " + JobName + " Summary is not applied when Schedule day and time of "+formatterIST.format(date)+" and "+Day+" when Schedule Days and time option is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfScheduleDayAndTimeAll_Queue(String JobName, String Day) throws Throwable
	{
		DateFormat formatterIST = new SimpleDateFormat("HH:mm");
		formatterIST.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // better than using IST
		Date date = formatterIST.parse(Schedule_Time);
		System.out.println(formatterIST.format(date)); // output: 15-05-2014 00:00:00
		DateFormat formatterUTC = new SimpleDateFormat("HH:mm a");
		formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC")); // UTC timezone
		System.out.println(formatterUTC.format(date)); // output: 14-05-2014 18:30:00
		boolean value=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(text(),'Job will be applied at ["+formatterUTC.format(date)+" (UTC)] every "+Day+".')]")).isDisplayed();
		String PassStatement = "PASS >> " + JobName+ " is applied successfully with Schedule day and time of "+formatterIST.format(date)+" and "+Day+" when Schedule Days and time option is selected";
		String FailStatement = "Fail >> " + JobName + " Summary is not applied when Schedule day and time of "+formatterIST.format(date)+" and "+Day+" when Schedule Days and time option is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnSelectDateAndTime() throws Throwable
	{
		ScheduleJobSelectDateTimeButton.click();
		sleep(5);
		Reporter.log("Selecting Select Date and Time",true);
	}

	public void ClickOnDateTime() throws Throwable
	{
		SelectDateTime.click();
		sleep(5);
	}

	public void VerifyOfSelectDateAndTimebutton() throws Throwable
	{
		boolean value=SelectDateTime.isDisplayed();
		String PassStatement = "PASS >> Select Date And Time ,Interval textbox is displayed successfully when clicked on Select Date and Time Button";
		String FailStatement = "Fail >> Select Date And Time, Interval textbox is not displayed even when clicked on Select Date and Time Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		ClickOnDateTime();
		WebElement[] SelectDateAndTimeElement={SelectDateTimeInterval,SelectDateTimeIntervaltable,SelectDateTimeIntervalMonth,SelectDateTimeIntervalYear,SelectDateTimeIntervalTime};
		String[] SelectDateAndTimeText={"Select Date and Time Interval","Select Date and Time Interval Table","Select Date and Time - Month","Select Date and Time - Year","Select Date and Time - Interval"};
		for(int i=0;i<SelectDateAndTimeElement.length;i++)
		{
			value=SelectDateAndTimeElement[i].isDisplayed();
			PassStatement="PASS >> "+SelectDateAndTimeText[i]+" is displayed successfully when clicked on Select Days and Time Button ";
			FailStatement="Fail >> "+SelectDateAndTimeText[i]+" is not displayed even when clicked on Schedule Days and Time Button ";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

	}

	public void SelectTime() throws Throwable
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		System.out.println( sdf.format(cal.getTime()) );
		Date d = sdf.parse(sdf.format(cal.getTime())); 
		cal.setTime(d);
		cal.add(Calendar.MINUTE, 2);
		String newTime = sdf.format(cal.getTime());
		System.out.println(newTime);
		Time=newTime;
		Initialization.driver.findElement(By.xpath("//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_' and contains(@style,'display: block')]/div/div/div/div[text()='"+newTime+"']")).click();
		sleep(2);
	}

	public void VerifyOfSelectDateAndTimeSummary()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("M/d/YYYY");
		LocalDate localDate = LocalDate.now();
		System.out.println(dtf.format(localDate)); 
		String time=Time;
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			final Date dateObj = sdf.parse(time);
			time = new SimpleDateFormat("h:mm aa").format(dateObj);
			System.out.println(time);
		} catch (final ParseException e) {
			e.printStackTrace();
		}
		String[] timeElement=time.split(" ");
		String ActualValue = ScheduleJobSummary.getText();
		String ExpectedValue = "Job shall be pushed on device at "+dtf.format(localDate)+" "+timeElement[0]+":00 "+timeElement[1];
		boolean value=ScheduleJobSummary.isDisplayed() && ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> " + ActualValue+ " Summary is displayed successfully when Select Days and Time option is selected ";
		String FailStatement = "Fail >> " + ActualValue + " Summary is not displayed when Select Days and Time option is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfSelectDayAndTimeQueue(String JobName) throws Throwable
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		LocalDate localDate = LocalDate.now();
		System.out.println(dtf.format(localDate)); 
		DateFormat formatterIST = new SimpleDateFormat("HH:mm");
		formatterIST.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // better than using IST
		Date date = formatterIST.parse(Time);
		System.out.println(formatterIST.format(date)); // output: 15-05-2014 00:00:00
		DateFormat formatterUTC = new SimpleDateFormat("HH:mm");
		formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC")); // UTC timezone
		System.out.println(formatterUTC.format(date)); // output: 14-05-2014 18:30:00
		boolean value=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(text(),'Job scheduled to be applied at "+dtf.format(localDate)+" "+formatterUTC.format(date)+" (UTC).')]")).isDisplayed();
		System.out.println(Initialization.driver.findElement(By.xpath("//p[contains(@id,'jobComments')]")).getText());
		String PassStatement = "PASS >> " + JobName+ " is applied successfully with Select date and time of "+dtf.format(localDate)+" "+formatterIST.format(date)+" is selected";
		String FailStatement = "Fail >> " + JobName + " Summary is not applied when Select date and time of "+dtf.format(localDate)+" "+formatterIST.format(date)+" is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnImmediately() throws Throwable
	{
		ScheduleJobImmediatelyButton.click();
		sleep(5);
		Reporter.log("Selecting Immediately Button",true);
	}

	public void VerifyOfImmediatelySummary()
	{
		String ActualValue = ScheduleJobSummary.getText();
		String ExpectedValue = "Job shall be pushed on device immediately";
		boolean value=ScheduleJobSummary.isDisplayed() && ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> " + ActualValue+ " Summary is displayed successfully when Immediately is selected ";
		String FailStatement = "Fail >> " + ActualValue + " Summary is not displayed when Immediately is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfImmediatelyQueue(String JobName) throws Throwable
	{
		boolean value=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(text(),'Job pending to be applied.')]")).isDisplayed();
		System.out.println(Initialization.driver.findElement(By.xpath("//p[contains(@id,'jobComments')]")).getText());
		String PassStatement = "PASS >> " + JobName+ " is applied successfully with Immediately option is selected";
		String FailStatement = "Fail >> " + JobName + " Summary is not applied even when Immediately option is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfPeriodicallySummary_Cancel(String Time)
	{
		String ActualValue = ScheduleJobSummary.getText();
		String ExpectedValue = "Job will be pushed for the device after every "+Time+" minutes.";
		boolean value=ScheduleJobSummary.isDisplayed() && ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> " + ActualValue+ " Summary is displayed successfully when clicked on Close Button of Configure Schedule Time by selecting other option";
		String FailStatement = "Fail >> " + ActualValue + " Summary is not displayed when clicked on Close Button of Configure Schedule Time by selecting other option";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfScheduleDayAndTimeSummary_Ok(String Day)
	{
		String ActualValue = ScheduleJobSummary.getText();
		String ExpectedValue = "Job will be applied at "+Schedule_Time+" on every "+Day+".";
		boolean value=ScheduleJobSummary.isDisplayed() && ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> " + ActualValue+ " Summary is displayed successfully when clicked on Ok button by changing option of Configure Schedule Time from Periodically to Schedule Time and Day";
		String FailStatement = "Fail >> " + ActualValue + " Summary is not displayed even when clicked on Ok button by changing option of Configure Schedule Time from Periodically to Schedule Time and Day";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void SelectJob_Connectivity(String JobName) throws Throwable
	{
		ApplyJobSearchTextField.sendKeys(JobName);
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
		{}
		sleep(5);
		ApplyJobrefreshbutton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
		{}
		sleep(5);
		Initialization.driver.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr/td/p[text()='"+JobName+"']")).click();
	}

	public void ClearSearchApplyJob() throws Throwable
	{
		ApplyJobSearchTextField.clear();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div")).isDisplayed())
		{}
		sleep(4);
	}

	public void VerifyOfConnectivity()
	{
		boolean value=ConnectivityText.isDisplayed();
		String ActualValue=ConnectivityText.getText();
		String ExpectedValue="Deploy job on";
		value= value && ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> " + ActualValue+ " Summary is displayed successfully when clicked Install/FileTransfer Job in Apply Job Window";
		String FailStatement = "Fail >> " + ActualValue + " Summary is not displayed even when clicked on Install/FileTransfer Job in Apply Job Window";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String[] ConnectionText = {"Wi-Fi Only","Mobile Data Only","Any Network"};
		for(int i=0;i<ConnectionText.length;i++)
		{
			ActualValue=ConnectivityOptions.get(i).getText();
			ExpectedValue=ConnectionText[i];
			PassStatement = "PASS >> " + ActualValue+ " option is displayed successfully when clicked Install/FileTransfer Job in Apply Job Window";
			FailStatement = "Fail >> " + ActualValue + " option is not displayed even when clicked on Install/FileTransfer Job in Apply Job Window";
		}

	}

	public void SelectWiFi()
	{
		Select ConnmectivitySel=new Select(ConnectivityDropDown);
		ConnmectivitySel.selectByVisibleText("Wi-Fi Only");
	}

	public void SelectMobile()
	{
		Select ConnmectivitySel=new Select(ConnectivityDropDown);
		ConnmectivitySel.selectByVisibleText("Mobile Data Only");
	}

	public void SelectAny()
	{
		Select ConnmectivitySel=new Select(ConnectivityDropDown);
		ConnmectivitySel.selectByVisibleText("Any Network");
	}

	public void VerifyOfTypeColumn_TextMessage(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td[1]")).getText();
		String ExpectedValue="TextMessage";
		String PassStatement = "PASS >> Type of Message job is displayed successfully as "+ActualValue+" when clicked on Apply Job";
		String FailStatement = "Fail >> Type of Message job is not displayed as "+ActualValue+" even when clicked on Apply Job";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfTypeColumn_FileTransfer(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td[1]")).getText();
		String ExpectedValue="File Transfer";
		String PassStatement = "PASS >> Type of File Transfer job is displayed successfully as "+ActualValue+" when clicked on Apply Job";
		String FailStatement = "Fail >> Type of File Transfer job is not displayed as "+ActualValue+" even when clicked on Apply Job";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfTypeColumn_Install(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td[1]")).getText();
		String ExpectedValue="Install";
		String PassStatement = "PASS >> Type of Install job is displayed successfully as "+ActualValue+" when clicked on Apply Job";
		String FailStatement = "Fail >> Type of Install job is not displayed as "+ActualValue+" even when clicked on Apply Job";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfPlatformColumn_TextMessage(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td[2]")).getText();
		String ExpectedValue="Any";
		String PassStatement = "PASS >> Type of Platform is displayed successfully for TextMessage job as "+ActualValue+" when clicked on Apply Job";
		String FailStatement = "Fail >> Type of Platform is not displayed for TextMessage job as "+ActualValue+" even when clicked on Apply Job";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfPlatformColumn_Install(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td[2]")).getText();
		String ExpectedValue="Android";
		String PassStatement = "PASS >> Type of Platform is displayed successfully for Install/FileTransfer job as "+ActualValue+" when clicked on Apply Job";
		String FailStatement = "Failss >> Type of Platform is not displayed for Install/FileTransfer job as "+ActualValue+" even when clicked on Apply Job";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}



	//BlackList

	@FindBy(id="deleteDeviceBtn")
	private WebElement DeleteDeviceBtn;

	@FindBy(id="blacklistDeviceBtn")
	private WebElement BlacklistDeviceBtn;

	@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement DeleteBlacklistDeviceNoBtn;

	@FindBy(xpath="//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement BlacklistDeviceYesBtn;

	@FindBy(id="blacklistedSection")
	private WebElement BlackListedTab;

	@FindBy(xpath="//div[@id='panelBackgroundDevice']/div[2]/div/div/input")
	private WebElement BlackListDeviceSearchField;



	public void ClickOnDeleteDevice_UM()
	{
		DeleteDeviceBtn.click();
	}

	public void ClickOnDeleteDevice() throws InterruptedException
	{
		DeleteDeviceBtn.click();
		waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]");
		sleep(2);
	}

	public void ClickOnDeleteBlacklistDeviceNoBtn() throws InterruptedException
	{
		DeleteBlacklistDeviceNoBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(1);
	}

	public void ClickOnBlacklistDevice_UM()
	{
		BlacklistDeviceBtn.click();
	}

	public void ClickOnBlacklistDevice() throws InterruptedException
	{
		BlacklistDeviceBtn.click();
		waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]");
		sleep(5);
	}

	public void ClickOnBlackListedTab() throws InterruptedException
	{
		BlackListedTab.click();
		sleep(10);
		Reporter.log("Clicked On BlackListed Tab",true);
	}

	public void SearchDeviceInBlackList() throws InterruptedException
	{
		boolean flag;
		BlackListDeviceSearchField.sendKeys(Config.DeviceName);
		try
		{
			waitForXpathPresent("//td[text()='"+Config.DeviceName+"']");	
			flag=Initialization.driver.findElement(By.xpath("//td[text()='"+Config.DeviceName+"']")).isDisplayed();

		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="Device Is present Inside BlackListTab";
		String Fail="Device Is Not Present Inside BlackListTab";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}


	//Device Logs
	@FindBy(id="logDeviceBtn")
	private WebElement DeviceLogButton;

	@FindBy(id="device_log_title")
	private WebElement DeviceLogHeader;

	@FindBy(id="device_logs_export")
	private WebElement DeviceLogExportButton;

	@FindBy(xpath="//h4[@id='device_log_title']/preceding-sibling::button")
	private WebElement DeviceLogCloseButton;

	@FindBy(xpath="//div[@id='dev_log_model_body']/div/div/div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Device Activity Logs']")
	private WebElement DeviceLogColumn;

	@FindBy(xpath="//div[@id='dev_log_model_body']/div/input")
	private WebElement DeviceLogOkButton;

	@FindBy(xpath="//span[contains(text(),'New device') and contains(text(),'registered.')]")
	private WebElement DeviceRegisteredLogs;

	@FindBy(xpath="//li[@id='action_log']")
	private WebElement DeviceLogsTab;

	@FindBy(xpath="//li[@id='info_log']")
	private WebElement DeviceActivityTab;

	public void ClickOnDeviceLogs_UM() throws Throwable
	{
		DeviceLogButton.click();
		Reporter.log("PASS >> Clicked on Device Logs Button ,Navigates successfully to Device Logs popup",true);
	}

	public void ClickOnDeviceLogs() throws Throwable
	{
		DeviceLogButton.click();
		waitForidPresent("device_logs_export");
		sleep(5);
		Reporter.log("PASS >> Clicked on Device Logs Button ,Navigates successfully to Device Logs popup",true);
	}

	public void ClickOnMultipleDevicesLogs()
	{
		boolean flag;
		DeviceLogButton.click();
		waitForXpathPresent("//span[text()='Please select only one device.']");
		try
		{
			Initialization.driver.findElement(By.xpath("//span[text()='Please select only one device.']")).isDisplayed();
			flag=true;
		}
		catch (Exception e) 
		{
			flag=false;
		}
		String Pass="PASS >>>Please select only one device Message Is Displayed";
		String Fail="FAIL >>>Please select only one device Message Is Not Displayed";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifyOfDeviceLogPage()
	{
		String ActualValue=DeviceLogHeader.getText();
		String ExpectedValue="Device Activity Logs - "+Config.DeviceName;
		String PassStatement = "PASS >> " + ActualValue+ " title is displayed successfully when clicked on Device Logs Button";
		String FailStatement = "Fail >> " + ActualValue + " title is not displayed when clicked on Device Logs Button";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		WebElement[] DeviceLogElement = {DeviceLogsTab,DeviceActivityTab, DeviceLogExportButton, DeviceLogCloseButton, DeviceLogColumn, DeviceLogOkButton };
		String[] DeviceLogText = {"DeviceLogs Tab","DeviceActivity Tab", "Device Log Export Button", "Device Log Close Button", "Device Activity Logs Column Name", "Device Log Ok Button" };
		for (int i = 0; i < DeviceLogText.length - 1; i++) 
		{
			boolean value=DeviceLogElement[i].isDisplayed();
			PassStatement = "PASS >> " + DeviceLogText[i]+ " is displayed successfully when clicked on Device Logs Button";
			FailStatement = "Fail >> " + DeviceLogText[i] + " is not displayed even when clicked on Device Logs Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void ClickOnDeviceLogCloseBtn() throws InterruptedException {
		DeviceLogCloseButton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
		Reporter.log("PASS >> Clicked on Close Button of Device Log", true);
	}

	public void VerifyOfRegisteredLogs()
	{
		boolean value=DeviceRegisteredLogs.isDisplayed();
		String Actualtext=DeviceRegisteredLogs.getText();
		String PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		String FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfAddedLogs()
	{
		WebElement AddedLogs=Initialization.driver.findElement(By.xpath("//span[contains(text(),'New Device') and contains(text(),'added by "+Config.AccountUserName+".')]"));
		boolean value=AddedLogs.isDisplayed();
		String Actualtext=AddedLogs.getText();
		String PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		String FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfOnlineLogs()
	{
		WebElement OnlineLogs=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Device("+Config.DeviceName+") is online.')]"));
		boolean value=OnlineLogs.isDisplayed();
		String Actualtext=OnlineLogs.getText();
		String PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		String FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfUpdatedLogs()
	{
		WebElement UpdatedLogs=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Device("+Config.DeviceName+") updated its info.')]"));
		boolean value=UpdatedLogs.isDisplayed();
		String Actualtext=UpdatedLogs.getText();
		String PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		String FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfRemoteLogs()
	{
		WebElement RemoteLogs=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Remote support initiated on device("+Config.DeviceName+") by user')]"));
		boolean value=RemoteLogs.isDisplayed();
		String Actualtext=RemoteLogs.getText();
		String PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		String FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		RemoteLogs=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Remote support closed on device("+Config.DeviceName+") by user')]"));
		value=RemoteLogs.isDisplayed();
		Actualtext=RemoteLogs.getText();
		PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfJobLogs(String JobName)
	{
		WebElement JobLogs=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Job("+JobName+") applied to device "+Config.DeviceName+"')]"));
		boolean value=JobLogs.isDisplayed();
		String Actualtext=JobLogs.getText();
		String PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		String FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		JobLogs=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Job("+JobName+") picked by "+Config.DeviceName+".')]"));
		value=JobLogs.isDisplayed();
		Actualtext=JobLogs.getText();
		PassStatement = "PASS >> '" + Actualtext+ "' is displayed successfully when clicked on Device Logs Button";
		FailStatement = "Fail >> '" + Actualtext+ "' is not displayed even when clicked on Device Logs Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void CheckExport() throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
	{
		String downloadPath = Config.downloadpath;
		String fileName = "Device Logs.csv";
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {
				initial++;
			}
		}

		DeviceLogExportButton.click();
		sleep(5);
		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=(result==initial);
		String PassStatement="PASS >> Device Logs is exported successfully in .csv format when clicked on Export button of Device Logs";
		String FailStatement="Fail >> Device Logs is not exported in .csv format even when clicked on Export button of Device Logs";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void VerifyAbsenceOfOnlineOfflineStatusInDeviceLogsTab()
	{
		boolean flag;
		List<WebElement> Logs = Initialization.driver.findElements(By.xpath("//table[@id='devicelogtable']/tbody/tr"));
		System.out.println(Logs.size());
		for(int i=0;i<=Logs.size()-1;i++)
		{
			//		boolean flag;
			String LogsMessage = Logs.get(i).getText();
			if(LogsMessage.contains("is online.")||LogsMessage.contains("is offline."))
			{
				flag=false;
			}
			else
			{
				flag=true;
			}
			String Pass="PASS >>> Online Offline Status Have not been Updated In DeviceLogs Tab";
			String Fail="FAIL >>> Online Offline Status Updated In DeviceLogs Tab";
			ALib.AssertTrueMethod(flag, Pass, Fail);
		}

	}

	//Queue
	@FindBy(id="jobQueueButton")
	private WebElement JobQueueBtn;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr[1]/td/p[contains(text(),'Pending')]")
	private WebElement PendingJobFirstRow;

	@FindBy(id="jobqueueremovebtn")
	private WebElement JobQueueRemoveBtn;

	@FindBy(xpath="//h4[@id='jobqueuetitle']/preceding-sibling::button")
	private WebElement JobQueueCloseBtn;

	@FindBy(xpath="//div[@id='multiDev-JobQ_modal']/div/button")
	private WebElement JobQueueCloseButtonForMultipleDevices;

	@FindBy(id="ShowAll")
	private WebElement PastBtn;

	@FindBy(id="jobqueuetitle")
	private WebElement QueueHeader;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/thead/tr/th/div[text()='Job Name']")
	private WebElement JobNameColumn;


	@FindBy(xpath="//table[@id='jobQueueDataGrid']/thead/tr/th/div[text()='Type']")
	private WebElement TypeColumn;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/thead/tr/th/div[text()='Size']")
	private WebElement SizeColumn;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/thead/tr/th/div[text()='Scheduled To Run At']")
	private WebElement ScheduledToRunAtColumn;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/thead/tr/th/div[text()='Status']")
	private WebElement StatusColumn;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/thead/tr/th/div[text()='Status Message']")
	private WebElement StatusMessageColumn;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/thead/tr/th/div[text()='Network Type']")
	private WebElement NetworkTypeColumn;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Pending')]")
	private List<WebElement> PendingJobsStatus;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Pending')]/i[contains(@class,'exclamation-triangle')]")
	private List<WebElement> PendingJobsStatusImage;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Deployed')]")
	private List<WebElement> DeployedJobsStatus;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Deployed')]/i[contains(@class,'check-circle')]")
	private List<WebElement> DeployedJobsStatusImage;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Error')]")
	private List<WebElement> ErrorJobsStatus;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Error')]/i[contains(@class,'exclamation-circle')]")
	private List<WebElement> ErrorJobsStatusImage;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Error')]/i[contains(@class,'retry_icon')]")
	private List<WebElement> ErrorReapplyIcon;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr/td/p[contains(text(),'Pending')]/i[contains(@class,'retry_icon')]")
	private List<WebElement> PendingReapplyIcon;

	@FindBy(id="summaryText")
	private WebElement QueueSummary;

	@FindBy(xpath="//h4[contains(text(),'Sub Job Status')]")
	private WebElement SubJobStatusHeader;

	@FindBy(xpath="//h4[contains(text(),'Sub Job Status')]/preceding-sibling::button")
	private WebElement SubJobStatusCloseButton;

	@FindBy(xpath="//table[@id='subjobQueueDataGrid']/thead/tr/th/div[text()='Job Name']")
	private WebElement SubJobStatusJobNameColumn;

	@FindBy(xpath="//table[@id='subjobQueueDataGrid']/thead/tr/th/div[text()='Type']")
	private WebElement SubJobStatusTypeColumn;

	@FindBy(xpath="//table[@id='subjobQueueDataGrid']/thead/tr/th/div[text()='Scheduled To Run At']")
	private WebElement SubJobStatusScheduledColumn;

	@FindBy(xpath="//table[@id='subjobQueueDataGrid']/thead/tr/th/div[text()='Status']")
	private WebElement SubJobStatusColumn;

	@FindBy(xpath="//table[@id='subjobQueueDataGrid']/thead/tr/th/div[text()='Status Message']")
	private WebElement SubJobStatusMessageColumn;

	@FindBy(xpath="//div[@id='SubjobQueueRefreshBtn']")
	private WebElement SubJobStatusRefreshButton;

	@FindBy(xpath="//button[contains(@onclick,'subjobQueueModel') and text()='Ok']")
	private WebElement SubJobStatusOkButton;

	@FindBy(xpath="//a[text()='Failed Jobs']")
	private WebElement FailedJobsTab;

	@FindBy(xpath="//a[text()='Incomplete Jobs']")
	private WebElement IncompleteJobsTab;

	@FindBy(xpath="//a[text()='Completed Jobs']")
	private WebElement CompletedHistoryTab;

	@FindBy(xpath="//a[text()='Quick Action']")
	private WebElement QuickActionTab;

	@FindBy(xpath="//table[@id='jobQueueHistoryGrid']/tbody/tr/td[7]")
	private WebElement StatusMessage_CompletedHistorTab;

	@FindBy(xpath="//div[@id='jobQ-info-container']/div[@class='info_block']//span[text()='Pending Job']")
	private WebElement PendingJobsCountFiled;

	@FindBy(xpath="//div[@id='jobQ-info-container']/div[@class='info_block']//span[text()='In Progress Job']")
	private WebElement InprogressJobsCountFiled;

	@FindBy(xpath="//div[@id='jobQ-info-container']/div[@class='info_block']//span[text()='Failed Job']")
	private WebElement FailedJobsCountField;

	@FindBy(xpath="//div[@id='jobQ-info-container']/div[@class='info_block']//span[text()='Completed Job']")
	private WebElement CompletedJobsCountFiled;

	public void clickOnJobHistoryButton() throws InterruptedException
	{
		JobQueueBtn.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForidPresent("ShowAll");
		sleep(5);
		Reporter.log("Clicked on Job Queue , Navigates successfully to Job Queue Page");
	}

	public void ClickOnJobQueue_UM() throws InterruptedException
	{
		JobQueueBtn.click();
		Reporter.log("Clicked on Job Queue , Navigates successfully to Job Queue Page");
	}

	public void ClickOnPastShowButton() throws Throwable
	{
		PastBtn.click();
		sleep(5);
	}

	public void ClickOnJobQueuePendingFirstrow() throws InterruptedException
	{
		PendingJobFirstRow.click();
		sleep(2);
	}

	public boolean verifyOfRemoveJobQueueEnabled()
	{
		String ActualValue=JobQueueRemoveBtn.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		return value;
	}

	public void ClickOnJobQueueCloseBtn() throws InterruptedException
	{
		JobQueueCloseBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		Reporter.log("Clicked on Close Button of Job Queue", true);
	}

	public void VerifyOfQueuePage()
	{
		String ActualValue=QueueHeader.getText();
		String ExpectedValue="Job History - "+Config.DeviceName;
		String PassStatement = "PASS >> " + ActualValue+ " title is displayed successfully when clicked on Queue Button";
		String FailStatement = "Fail >> " + ActualValue + " title is not displayed when clicked on Queue Button";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		WebElement[] QueueElement = {IncompleteJobsTab,FailedJobsTab,CompletedHistoryTab,QuickActionTab,JobNameColumn, TypeColumn, SizeColumn, ScheduledToRunAtColumn, StatusColumn, StatusMessageColumn, NetworkTypeColumn,PendingJobsCountFiled,InprogressJobsCountFiled,FailedJobsCountField,CompletedJobsCountFiled};
		String[] QueueText = {"'IncompleteJobsTab'","'FailedJobsTab'","'CompletedHistoryTab'","'QuickActionTab'",	"'Job Name' Column", "'Type' Column", "'Size' Column", "'Scheduled To Run At' Column", "'Status' Column", "'Status Message' Column", "'Network Type' Column","'PendingDelete Jobs CountFiled'",
				"'Inprogress Jobs CountField'","'Failed Jobs CountField'","'Completed Jobs CountFiled'"       };
		for (int i = 0; i < QueueText.length; i++) 
		{
			boolean value=QueueElement[i].isEnabled();
			PassStatement = "PASS >> " + QueueText[i]+ " is displayed successfully when clicked on Queue Button";
			FailStatement = "Fail >> " + QueueText[i] + " is not displayed even when clicked on Queue Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyOfNetworkTypeMessage(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(@id,'jobNetworkType')]")).getText();
		String ExpectedValue="N/A";
		String PassStatement = "PASS >> Network Type is displayed successfully as " + ActualValue+ " when Message job is applied";
		String FailStatement = "Fail >> Network Type is not displayed as " + ActualValue+ " even when Message job is applied";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfNetworkTypeInstall_WiFi(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(@id,'jobNetworkType')]")).getText();
		String ExpectedValue="Wi-Fi Data";
		String PassStatement = "PASS >> Network Type is displayed successfully as " + ActualValue+ " when Install job is applied with WiFi as Connectivity";
		String FailStatement = "Fail >> Network Type is not displayed as " + ActualValue+ " even when Install job is applied with WiFi as Connectivity";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfNetworkTypeFile_WiFi(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(@id,'jobNetworkType')]")).getText();
		String ExpectedValue="Wi-Fi Data";
		String PassStatement = "PASS >> Network Type is displayed successfully as " + ActualValue+ " when FileTransfer job is applied with WiFi as Connectivity";
		String FailStatement = "Fail >> Network Type is not displayed as " + ActualValue+ " even when FileTransfer job is applied with WiFi as Connectivity";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}


	public void VerifyOfNetworkTypeInstall_Mobile(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(@id,'jobNetworkType')]")).getText();
		String ExpectedValue="Mobile Data";
		String PassStatement = "PASS >> Network Type is displayed successfully as " + ActualValue+ " when Install job is applied with Mobile as Connectivity";
		String FailStatement = "Fail >> Network Type is not displayed as " + ActualValue+ " even when Install job is applied with Mobile as Connectivity";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfNetworkTypeFile_Mobile(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(@id,'jobNetworkType')]")).getText();
		String ExpectedValue="Mobile Data";
		String PassStatement = "PASS >> Network Type is displayed successfully as " + ActualValue+ " when FileTransfer job is applied with Mobile as Connectivity";
		String FailStatement = "Fail >> Network Type is not displayed as " + ActualValue+ " even when FileTransfer job is applied with Mobile as Connectivity";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfNetworkTypeInstall_Any(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(@id,'jobNetworkType')]")).getText();
		String ExpectedValue="Any Network";
		String PassStatement = "PASS >> Network Type is displayed successfully as " + ActualValue+ " when Install job is applied with Any Network as Connectivity";
		String FailStatement = "Fail >> Network Type is not displayed as " + ActualValue+ " even when Install job is applied with Any Network as Connectivity";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfNetworkTypeFile_Any(String JobName)
	{
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/following-sibling::td/p[contains(@id,'jobNetworkType')]")).getText();
		String ExpectedValue="Any Network";
		String PassStatement = "PASS >> Network Type is displayed successfully as " + ActualValue+ " when FileTransfer job is applied with Any Network as Connectivity";
		String FailStatement = "Fail >> Network Type is not displayed as " + ActualValue+ " even when FileTransfer job is applied with Any Network as Connectivity";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfPendingJobs()
	{
		int NoOfPendingJobs =PendingJobsStatus.size();
		String ActualValue=QueueSummary.getText();
		String ExpectedValue= NoOfPendingJobs+" job(s) pending in queue";
		String PassStatement = "PASS >> Summary Text for Pending Jobs is displayed successfully when clicked on Queue";
		String FailStatement = "Fail >> Summary Text for Pending Jobs is not displayed successfully even when clicked on Queue";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		for(int i=0;i<PendingJobsStatus.size();i++)
		{
			ActualValue=PendingJobsStatus.get(i).getText();
			ExpectedValue="Pending";
			PassStatement = "PASS >> Only Pending Job is displayed successfully when clicked on Queue with Show Past jobs disabled";
			FailStatement = "Fail >> Only Pending Job is not displayed even when clicked on Queue with Show Past jobs disabled";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=PendingJobsStatusImage.get(i).isDisplayed();
			PassStatement = "PASS >> Pending Image is displayed successfully for Pending Jobs";
			FailStatement = "Fail >> Pending Image is not displayed for Pending Jobs";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=PendingReapplyIcon.get(i).isDisplayed();
			PassStatement = "PASS >> Re-apply icon for Pending jobs is displayed successfully when clicked on Queue with Show Past jobs disabled";
			FailStatement = "Fail >> Re-apply icon for Pending jobs is not displayed when clicked on Queue with Show Past jobs disabled";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyOfPendingJobs_Enable()
	{
		int NoOfPendingJobs =PendingJobsStatus.size();
		String ActualValue=QueueSummary.getText();
		String ExpectedValue= NoOfPendingJobs+" job(s) pending in queue";
		String PassStatement = "PASS >> Summary Text for Pending Jobs is displayed successfully when clicked on Queue";
		String FailStatement = "Fail >> Summary Text for Pending Jobs is not displayed successfully even when clicked on Queue";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		for(int i=0;i<PendingJobsStatus.size();i++)
		{
			ActualValue=PendingJobsStatus.get(i).getText();
			ExpectedValue="Pending";
			PassStatement = "PASS >> Only Pending Job is displayed successfully when clicked on Queue with Show Past jobs Enabled";
			FailStatement = "Fail >> Only Pending Job is not displayed even when clicked on Queue with Show Past jobs disEnabledabled";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=PendingJobsStatusImage.get(i).isDisplayed();
			PassStatement = "PASS >> Pending Image is displayed successfully for Pending Jobs";
			FailStatement = "Fail >> Pending Image is not displayed for Pending Jobs";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=PendingReapplyIcon.get(i).isDisplayed();
			PassStatement = "PASS >> Re-apply icon for Pending jobs is displayed successfully when clicked on Queue with Show Past jobs Enabled";
			FailStatement = "Fail >> Re-apply icon for Pending jobs is not displayed when clicked on Queue with Show Past jobs Enabled";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyOfDeployedJobs()
	{
		for(int i=0;i<DeployedJobsStatus.size();i++)
		{
			String ActualValue=DeployedJobsStatus.get(i).getText();
			String ExpectedValue="Deployed";
			String PassStatement = "PASS >> Deployed Job is displayed successfully when clicked on Queue with Show Past jobs enabled";
			String FailStatement = "Fail >> Deployed Job is not displayed even when clicked on Queue with Show Past jobs enabled";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=DeployedJobsStatusImage.get(i).isDisplayed();
			PassStatement = "PASS >> Deployed Image is displayed successfully for Deployed Jobs";
			FailStatement = "Fail >> Deployed Image is not displayed for Deployed Jobs";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

	}

	public void VerifyOfErrorJobs()
	{
		for(int i=0;i<ErrorJobsStatus.size();i++)
		{
			String ActualValue=ErrorJobsStatus.get(i).getText();
			String ExpectedValue="Error";
			String PassStatement = "PASS >> Error Job is displayed successfully when clicked on Queue with Show Past jobs enabled";
			String FailStatement = "Fail >> Error Job is not displayed even when clicked on Queue with Show Past jobs enabled";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			boolean value=ErrorJobsStatusImage.get(i).isDisplayed();
			PassStatement = "PASS >> Error Image is displayed successfully for Error Jobs";
			FailStatement = "Fail >> Error Image is not displayed for Error Jobs";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			value=ErrorReapplyIcon.get(i).isDisplayed();
			PassStatement = "PASS >> Re-apply icon for Pending jobs is displayed successfully when clicked on Queue with Show Past jobs enabled";
			FailStatement = "Fail >> Re-apply icon for Pending jobs is not displayed when clicked on Queue with Show Past jobs enabled";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

	}

	public boolean VerifyOfRemoveJob() throws InterruptedException
	{
		String ActualValue=JobQueueRemoveBtn.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		return value;
	}

	public void VerifyOfRemoveJob_UnSelect() throws InterruptedException
	{
		boolean value=VerifyOfRemoveJob();
		String PassStatement = "PASS >> Remove Job Button is grayed out successfully when clicked on No Pending/Error job is selected ";
		String FailStatement = "Fail >> Remove Job Button is not grayed out even when clicked on No Pending/Error job is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfRemoveJobPending_Select() throws InterruptedException
	{
		boolean value=VerifyOfRemoveJob();
		String PassStatement = "PASS >> Remove Job Button is enabled successfully when clicked on Pending job is selected ";
		String FailStatement = "Fail >> Remove Job Button is grayed out even when clicked on Pending job is selected";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfRemoveJobError_Select() throws InterruptedException
	{
		boolean value=VerifyOfRemoveJob();
		String PassStatement = "PASS >> Remove Job Button is enabled successfully when clicked on Error job is selected ";
		String FailStatement = "Fail >> Remove Job Button is grayed out even when clicked on Error job is selected";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnRemoveJobButton() throws Throwable
	{
		JobQueueRemoveBtn.click();
		try
		{
			waitForXpathPresent("//p[text()='Do you wish to continue?']");
			sleep(3);
			Reporter.log("Clciked on Remove Job Button",true);
		}
		catch(Exception e)
		{
			Reporter.log("Clciked on Remove Job Button",true);
		}
	}

	public void ClickOnDeleteProgressJobYesButton()
	{
		Initialization.driver.findElement(By.xpath("(//button[text()='Yes'])[2]")).click();
		waitForXpathPresent("//h4[@id='jobqueuetitle']/preceding-sibling::button");
	}

	public void VerifyOfPendingRemove() throws Throwable
	{
		int initialcount =PendingJobsStatus.size();
		System.out.println(initialcount);
		ClickOnRemoveJobButton();
		int finalcount =0;
		try{
			PendingJobsStatus.size();
			finalcount=PendingJobsStatus.size();
		}catch(Exception e)
		{}
		boolean value = finalcount ==(initialcount-1);
		System.out.println(value);
		String PassStatement = "PASS >> Pending Job is removed successfully when clicked on Remove Job Button";
		String FailStatement = "Fail >> Pending Job is not removed even when clicked on Remove Job Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfErrorRemove() throws Throwable
	{
		int initialcount =ErrorJobsStatus.size();
		ClickOnRemoveJobButton();
		int finalcount =0;
		try{
			ErrorJobsStatus.size();
			finalcount=ErrorJobsStatus.size();
		}catch(Exception e)
		{}
		boolean value = finalcount ==(initialcount-1);
		String PassStatement = "PASS >> Error Job is removed successfully when clicked on Remove Job Button";
		String FailStatement = "Fail >> Error Job is not removed even when clicked on Remove Job Button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnJobQueueErrorFirstrow() throws Throwable
	{ 
		ErrorJobsStatus.get(0).click();
		sleep(2);
	}

	public void SubJobStatusOfInstallJob(String JobName, String SubJobName) throws Throwable
	{
		WebElement QueueElement=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/preceding-sibling::td/p/i[@title='View sub job status']"));
		boolean value=QueueElement.isDisplayed();
		String PassStatement = "PASS >> Sub Job Status is displayed successfully for Install Job when Clicked on Queue";
		String FailStatement = "Fail >> Sub Job Status is not displayed for Install Job even when Clicked on Queue";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		QueueElement.click();
		waitForXpathPresent("//button[contains(@onclick,'subjobQueueModel') and text()='Ok']");
		sleep(5);
		VerifyOfSubjobStatusPageUI(JobName);
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+SubJobName+"']]/following-sibling::td/p[contains(@id,'jobType')]")).getText();
		String ExpectedValue="Install";
		PassStatement = "PASS >> Type of Install job is displayed successfully as "+ActualValue+" when clicked on Sub Job Status";
		FailStatement = "Fail >> Type of Install job is not displayed as "+ActualValue+" even when clicked on Sub Job Status";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void SubJobStatusOfFileTransferJob(String JobName, String SubJobName) throws Throwable
	{
		WebElement QueueElement=Initialization.driver.findElement(By.xpath("//td[p[text()='"+JobName+"']]/preceding-sibling::td/p/i[@title='View sub job status']"));
		boolean value=QueueElement.isDisplayed();
		String PassStatement = "PASS >> Sub Job Status is displayed successfully for FileTransfer Job when Clicked on Queue";
		String FailStatement = "Fail >> Sub Job Status is not displayed for FileTransfer Job even when Clicked on Queue";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		QueueElement.click();
		waitForXpathPresent("//button[contains(@onclick,'subjobQueueModel') and text()='Ok']");
		sleep(5);
		VerifyOfSubjobStatusPageUI(JobName);
		String ActualValue=Initialization.driver.findElement(By.xpath("//td[p[text()='"+SubJobName+"']]/following-sibling::td/p[contains(@id,'jobType')]")).getText();
		String ExpectedValue="File Transfer";
		PassStatement = "PASS >> Type of FileTransfer job is displayed successfully as "+ActualValue+" when clicked on Sub Job Status";
		FailStatement = "Fail >> Type of FileTransfer job is not displayed as "+ActualValue+" even when clicked on Sub Job Status";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void VerifyOfSubjobStatusPageUI(String JobName)
	{
		String ActualValue=SubJobStatusHeader.getText();
		String ExpectedValue="Sub Job Status Of "+JobName;
		String PassStatement = "PASS >> Sub Job Status Header is displayed successfully when Clicked on Queue on Install/File Transfer Job Sub Status";
		String FailStatement = "Fail >> Sub Job Status Header is not displayed even when Clicked on Queue on Install/File Transfer Job Sub Status";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		WebElement[] SubJobElement = { SubJobStatusOkButton,SubJobStatusJobNameColumn, SubJobStatusTypeColumn, SubJobStatusScheduledColumn, SubJobStatusColumn, SubJobStatusMessageColumn, SubJobStatusCloseButton,SubJobStatusRefreshButton};
		String[] QueueText = { "'Sub Job Status Ok button'","'Sub Job Status Job Name' Column", "'Sub Job Status Type' Column", "'Sub Job Status Scheduled To Run At' Column", "'Sub Job Status' Column", "'Sub Job Status Message' Column", "'Sub Job Status Close Button'","Refresh Button"};
		for (int i = 0; i <= SubJobElement.length - 1; i++) 
		{
			boolean value=SubJobElement[i].isEnabled();
			PassStatement = "PASS >> " + QueueText[i]+ " is displayed successfully when clicked on Queue Button";
			FailStatement = "Fail >> " + QueueText[i] + " is not displayed even when clicked on Queue Button";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	@FindBy(xpath="//p[text()='In progress ']")
	private WebElement InprogressJob;

	@FindBy(xpath="(//div[contains(text(),'No Job')])[1]")
	private WebElement NoJObsConfirmationMessage;

	@FindBy(xpath="//div[@class='actionbuttonheader']/div[1]/child::i")
	private WebElement DynamicJobsRefreshButton;

	@FindBy(xpath="//table[@id='jobQueueDataDynamicGrid']/tbody/tr[1]/td[1]")
	private WebElement DynamicJobName;

	@FindBy(xpath="//div[@id='jobQueueRefreshBtn']")
	private WebElement JobQueueRefreshButton;

	@FindBy(xpath="//p[@class='jobStat']/child::i")
	private WebElement JobStatusSymbol;

	int BeforeApplyingDynamicJobSize;

	@FindBy(xpath="//table[@id='jobQueueHistoryGrid']/tbody/tr/td[2]")
	private List<WebElement> JobNames;

	@FindBy(xpath="//table[@id='jobQueueHistoryGrid']/tbody/tr/td[6]")
	private List<WebElement> JobStatus;

	@FindBy(xpath="//i[@title='View sub job status']")
	private WebElement SubJobQueueButton;

	@FindBy(xpath="//div[@id='subJobQ_tableCover']/div[1]/div[2]/div/table/thead/tr/th/div[text()='Job Name']")
	private WebElement SubJobQueueJobNameColumn;

	@FindBy(xpath="//div[@id='subJobQ_tableCover']/div[1]/div[2]/div/table/thead/tr/th/div[text()='Type']")
	private WebElement SubJobQueueTypeColumn;

	@FindBy(xpath="//table[@id='installConfigTable']/tbody/tr/td[1]")
	private List<WebElement> FileNames;

	@FindBy(xpath="//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]")
	private List <WebElement> SubJobStatus;

	@FindBy(xpath="//div[@id='SubjobQueueRefreshBtn']")
	private WebElement SubJobQueueRefreshButton;

	@FindBy(xpath="//table[@id='subjobQueueDataGrid']/tbody/tr/td[7]")
	private List<WebElement> DownloadProgressColumn;

	public void ClickOnSubStatusJobCloseButton() throws Throwable
	{
		SubJobStatusCloseButton.click();
		sleep(3);
		Reporter.log("Clicked on Close button of Sub Job Status",true);
	}

	public void ClickOnCompletedHistoryJobsTab() throws InterruptedException
	{
		CompletedHistoryTab.click();
		waitForXpathPresent("//div[@id='jobQueueHistoryRefreshBtn']");
		sleep(5);
	}

	public void ClickOnQuickActionTab() throws InterruptedException
	{
		QuickActionTab.click();
		waitForXpathPresent("//div[@id='jobQueueRefreshDynamicBtn']");
		sleep(5);
	}
	

	public void SizeOFDynamicJobsBeforeApplying()
	{
		List<WebElement> AllDynamicJobs = Initialization.driver.findElements(By.xpath("//table[@id='jobQueueDataDynamicGrid']/tbody/tr"));
		BeforeApplyingDynamicJobSize=AllDynamicJobs.size();
		System.out.println(BeforeApplyingDynamicJobSize);
	}

	public void VerifyingDynamicJobsSizeAfterApplyingJob()
	{
		boolean flag;
		String JobName = DynamicJobName.getText();
		List<WebElement> AllDynamicJobs = Initialization.driver.findElements(By.xpath("//table[@id='jobQueueDataDynamicGrid']/tbody/tr"));
		int AfterApplyingDynamicJob=AllDynamicJobs.size();
		if(BeforeApplyingDynamicJobSize<AfterApplyingDynamicJob && JobName.equals("Refresh") )
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="PASS >> Dynamic Job Applied Successfully";
		String Fail="FAIL >> Dynamic Job Not Applied";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifyStatusMessageInCompletedHistoryJobsTab()
	{
		boolean flag;
		List<WebElement> StatusMessages = Initialization.driver.findElements(By.xpath("//table[@id='jobQueueHistoryGrid']/tbody/tr/td[7]"));
		for(int i=0;i<=StatusMessages.size()-1;i++)
		{
			String StatusMessage = StatusMessages.get(i).getText();
			if(StatusMessage.contains("Dynamic job"))
			{
				flag=false;
			}
			else
			{
				flag=true;
			}
			String Pass="PASS >> Only Static Jobs Present Inside CompletedHistoryJobsTab";
			String Fail="FAIL >> Dynamic Jobs Present Inside CompletedHistoryJobsTab";
			ALib.AssertTrueMethod(flag, Pass, Fail);
		}
	}

	public void ClickingOnInprogressJob()
	{
		waitForXpathPresent("//p[text()='In progress ']");
		try 
		{
			InprogressJob.click();
		}
		catch (Exception e) 
		{
			Reporter.log("No Inprogress Job Present Inside Incomplete Jobs Tab");	
		}
	}

	public void VerifyRemovedJobInsideinCompleteJobs(String InprogressJobName) throws InterruptedException
	{
		boolean flag;
		try
		{
			flag=Initialization.driver.findElement(By.xpath("(//div[contains(text(),'No Job')])[1]")).isDisplayed();
			Reporter.log("No Jobs Available Message Is Displayed",true);

		}
		catch(Exception e)
		{
			flag=true;
			Reporter.log("No Jobs Available Message Is Not Displayed",true);

		}
		String Pass="PASS >> Deleted Job Is Not Present Inside Incomplete Jobs ";
		String Fail="FAIL >> Deleted Job Is  Present Inside Incomplete Jobs";
		ALib.AssertFalseMethod(flag, Pass, Fail);
		sleep(5);
	}

	public void SelectingMultipleJobsRandomly(String Xpath) throws AWTException
	{
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		List<WebElement> Jobs=Initialization.driver.findElements(By.xpath(Xpath));
		for(int i=0;i<=5;i++)
		{
			Jobs.get(i).click();
		}
		r.keyRelease(KeyEvent.VK_CONTROL);
	}

	public void SelectingMultipleInprogress_PendingJobs() throws AWTException, InterruptedException
	{
		try
		{
			Robot ro=new Robot();
			List<WebElement> PendingJobs = Initialization.driver.findElements(By.xpath("//table[@id='jobQueueDataGrid']/tbody/tr"));
			ro.keyPress(KeyEvent.VK_CONTROL);
			for(int i=0;i<=PendingJobs.size()-1;i++)
			{
				PendingJobs.get(i).click();
			}

			ro.keyRelease(KeyEvent.VK_CONTROL);
		}
		catch(Exception e)
		{
			Robot ro=new Robot();
			ro.keyPress(KeyEvent.VK_CONTROL);
			sleep(3);
			ro.keyPress(KeyEvent.VK_A);
			sleep(3);
			ro.keyRelease(KeyEvent.VK_CONTROL);
		}

	}

	public void VerifyingNoJobsInIncompleteJobsTab()
	{
		boolean flag;
		if(NoJObsConfirmationMessage.isDisplayed())
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="No Jobs Prsent Inside IncompleteJobsTab";
		String Fail="Jobs Are Still Present Insdie IncompleteJobsTab";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void ClcikOnDynamicRefreshButton() throws InterruptedException
	{
		DynamicJobsRefreshButton.click();
		while(Initialization.driver.findElement(By.xpath("//i[@class='st-jobStatus dev_st_icn good fa fa-check-circle']/following-sibling::div/child::div[4]")).isDisplayed())
		{
			sleep(1);
		}
	}
	public void ClickingOnJobsRefreshButton() throws InterruptedException
	{
		JobQueueRefreshButton.click();
		waitForidPresent("jobqueueremovebtn");
		sleep(3);
	}

	public void ClickingOnJobRefresgButtonWhileJobIsPendingOrInprogress() throws InterruptedException
	{
		try
		{
			while(JobStatusSymbol.isDisplayed())
			{
				Reporter.log("Inprogress/Pending Job Present Inside Incomplete Jobs Tab",true);
				ClickingOnInprogressJob();
				ClickingOnJobsRefreshButton();
			}
		}
		catch(Exception E)
		{
			Reporter.log("Inprogress/Pending Job Is Not Present Inside Incomplete Jobs Tab",true);
		}
	}

	public void VerifyingPending_InprogressJobsForMultipleDevices()
	{
		boolean flag;
		List<WebElement> JobStatus = Initialization.driver.findElements(By.xpath("//p[contains(text(),'Pending')]|//p[contains(text(),'In progress')]"));
		List<WebElement> JobstatsText = Initialization.driver.findElements(By.xpath("//p[@class='jobStat']"));
		for(int i=0;i<JobstatsText.size();i++)
		{
			Reporter.log("Job Status Is"+" "+JobstatsText.get(i).getText());
		}
		if(JobStatus.size()>=2)
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="PASS >> Pending/Inprogress Jobs For Multiple Devices Is Displayed";
		String Fail="FAIL >> Pending/Inprogress Jobs For Multiple Devices Is Not Displayed";
		ALib.AssertTrueMethod(flag, Pass, Fail);

	}

	public void VerifyingDeployedJobsForMultipleDevices()
	{
		boolean flag;
		for(int i=0;i<2;i++)
		{   			
			if(JobNames.get(i).getText().equals("0FileTransfer") &&JobStatus.get(i).getText().equals("Deployed"))
			{
				flag=true;	
			}
			else
			{
				flag=false;
			}
			String Pass="PASS >>"+JobNames.get(i).getText()+" "+" "+JobStatus.get(i).getText()+" "+"On The Device ";
			String Fail="FAIL >>"+JobNames.get(i).getText()+" "+" "+JobStatus.get(i).getText()+" "+"On The Device";
			ALib.AssertTrueMethod(flag, Pass, Fail);
		}
	}

	public void ClickingOnSubJobQueue() throws InterruptedException
	{
		SubJobQueueButton.click();
		waitForXpathPresent("//h4[contains(text(),'Sub Job Status')]");
		sleep(3);
	}
	public void VerifySequenceOfSubJobs(String SubJobName) throws InterruptedException {
		
		SubJobStatusRefreshButton.click();

		String Composite_SubJobName=Initialization.driver.findElement(By.xpath("//p[text()='"+SubJobName+"']")).getText();
		System.out.println(Composite_SubJobName);
		String ExpectedSubJobName=SubJobName;
		String Pass="PASS>> Sub Job qeueue sequence is correct";
		String Fail="FAIL>> Sub Job qeueue sequence is correct";
		ALib.AssertEqualsMethod(Composite_SubJobName, ExpectedSubJobName, Pass, Fail);
		SubJobStatusRefreshButton.click();

		sleep(2);


		
	}
	public void SelectCompositeJob(String CompJob) throws InterruptedException {
		
		
	WebElement CompJobName=Initialization.driver.findElement(By.xpath("(//table[@id='jobQueueHistoryGrid']/tbody/tr/td[2]/p[contains(text(),'"+CompJob+"')])[1]"));	
	CompJobName.click();
	sleep(2);
		
	}
	public void VerifyAlertRunscriptJob(String CompJob) throws InterruptedException {
	
		boolean CompJobName=Initialization.driver.findElement(By.xpath("(//table[@id='jobQueueHistoryGrid']/tbody/tr/td[2]/p[contains(text(),'"+CompJob+"')])[1]")).isDisplayed();
		String pass="PASS>> Alert Message Runscript is displayed";
		String fail="FAIL>> Alert Message Runscript is displayed";
		ALib.AssertTrueMethod(CompJobName, pass, fail);
		sleep(2);


		
	}
	public void VerifyInJobHistoryCompleted_WithDelayCompositeJob(String SubJobName1,String SubJobName2,String SubJobName3,String SubJobName4) throws Throwable {
		

	    WebElement Composite_SubJobName = Initialization.driver.findElement(By.xpath("//*[@id='subjobQueueDataGrid']/tbody/tr/td[2]/p"));
		String CompJobName=Composite_SubJobName.getText();
		String status = Initialization.driver.findElement(By.xpath("//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p")).getText();
	   	 if(CompJobName.contains(SubJobName1)&&CompJobName.contains(SubJobName2)&&CompJobName.contains(SubJobName3)&&CompJobName.contains(SubJobName4))
	   	 {
	   	   	 if(status.contains("Deployed")&&CompJobName.contains("Deployed")&&CompJobName.contains("Deployed")&&CompJobName.contains("Deployed"))
	   	   	 {

	   		Reporter.log("PASS>>> Sub Job status is shown Correctly ",true);
	   	 }
	   	 
	   	 else
	   	 {
	   		 ALib.AssertFailMethod("FAIL>>> Sub Job status is Not shown Correctly");
	   	 }
	    }
		}
	public void VerifyInJobHistoryCompleted_CompositeJob(String SubJobName1,String SubJobName2) throws Throwable {
		

	    WebElement Composite_SubJobName = Initialization.driver.findElement(By.xpath("//*[@id='subjobQueueDataGrid']/tbody/tr/td[2]/p"));
		String CompJobName=Composite_SubJobName.getText();
		String status = Initialization.driver.findElement(By.xpath("//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p")).getText();
	   	 if(CompJobName.contains(SubJobName1)&&CompJobName.contains(SubJobName2))
	   	 {
	   	   	 if(status.contains("Deployed")&&CompJobName.contains("Deployed"))
	   	   	 {

	   		Reporter.log("PASS>>> Sub Job status is shown Correctly ",true);
	   	 }
	   	 
	   	 else
	   	 {
	   		 ALib.AssertFailMethod("FAIL>>> Sub Job status is Not shown Correctly");
	   	 
	    
	   	 }
	   	 }
	}
	
	public void SelectCompositeJobError(String ErrorCompJob) throws InterruptedException {
		
		
	WebElement CompJobName=Initialization.driver.findElement(By.xpath("(//table[@id='jobQueueErrorGrid']/tbody/tr/td[2]/p[contains(text(),'"+ErrorCompJob+"')])[1]"));	
	CompJobName.click();
	sleep(2);
		
	}

	public void VerifyInJobHistoryErrorTab_CompositeJob(String SubJobName1,String SubJobName2,String SubJobName3) throws Throwable {
		

	    WebElement Composite_SubJobName = Initialization.driver.findElement(By.xpath("//*[@id='subjobQueueDataGrid']/tbody/tr/td[2]/p"));
		String CompJobName=Composite_SubJobName.getText();
		String status = Initialization.driver.findElement(By.xpath("//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p")).getText();
	   	 if(CompJobName.contains(SubJobName1)&&CompJobName.contains(SubJobName2)&&CompJobName.contains(SubJobName3))
	   	 {
	   	   	 if(status.contains("Deployed")&&CompJobName.contains("Deployed")&&CompJobName.contains("Error"))
	   	   	 {

	   		Reporter.log("PASS>>> Sub Job status is shown Correctly ",true);
	   	 }
	   	 
	   	 else
	   	 {
	   		 ALib.AssertFailMethod("FAIL>>> Sub Job status is Not shown Correctly");
	   	 }
	    }
		}
	


	public void VerifyingDeployedJobsForSubJobQueue_Composite(String SubJobName,int MaximumTime,String SubJobStatus,int SubJobStatusIndex) throws InterruptedException
	{
		
		SubJobStatusRefreshButton.click();
		sleep(2);

		boolean flag;
		

        String Composite_SubJobName = Initialization.driver.findElement(By.xpath("//p[text()='"+SubJobName+"']")).getText();

		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, MaximumTime);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p[contains(text(),'"+SubJobStatus+"')])['"+SubJobStatusIndex+"']"))).isDisplayed();
		Reporter.log("PASS >> Job Deployed Successfully On Device", true);
		sleep(3);
        String Composite_SubJobstatus = Initialization.driver.findElement(By.xpath("(//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p[contains(text(),'"+SubJobStatus+"')])['"+SubJobStatusIndex+"']")).getText();

		if(Composite_SubJobName.equals(SubJobName)&&Composite_SubJobstatus.equals(SubJobStatus))
		 {
				flag=true;	
			}
			else
			{
				flag=false;
			}
		String Pass="PASS >>"+Composite_SubJobName+" "+" "+Composite_SubJobstatus+" "+"On The Device ";
		String Fail="FAIL >>"+Composite_SubJobName+" "+" "+Composite_SubJobstatus+" "+"On The Device";
		ALib.AssertTrueMethod(flag, Pass, Fail);
		}
	
	public void VerifyingProgressedJobsForSubJobQueue_Composite(String SubJobName,int MaximumTime,String SubJobStatus,int SubJobStatusIndex,String SubJobStatusDeployed) throws InterruptedException
	{
		boolean flag;
//		SubJobStatusRefreshButton.click();

        String Composite_SubJobName = Initialization.driver.findElement(By.xpath("//p[text()='"+SubJobName+"']")).getText();
        String Composite_SubJobstatus = Initialization.driver.findElement(By.xpath("(//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p[contains(text(),'"+SubJobStatus+"')])['"+SubJobStatusIndex+"']")).getText();

		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, MaximumTime);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p[contains(text(),'"+SubJobStatus+"')])['"+SubJobStatusIndex+"']"))).isDisplayed();
		Reporter.log("PASS >> Job Deployed Successfully On Device", true);
		sleep(3);
		
		if(Composite_SubJobName.equals(SubJobName)&&Composite_SubJobstatus.equals(SubJobStatus))
		 {
				flag=true;	
//				SubJobStatusRefreshButton.click();
				wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p[contains(text(),'"+SubJobStatusDeployed+"')])['"+SubJobStatusIndex+"']"))).isDisplayed();
				sleep(3);
				
			

			}
			else
			{
				flag=false;
			}
		String Pass="PASS >>"+Composite_SubJobName+" "+" "+Composite_SubJobstatus+" "+"On The Device ";
		String Fail="FAIL >>"+Composite_SubJobName+" "+" "+Composite_SubJobstatus+" "+"On The Device";
		ALib.AssertTrueMethod(flag, Pass, Fail);
		}
	


	public void VerifyingMultipleFileTransferFileNamesInSubJobQueue()
	{
		boolean flag;
		List<WebElement> SubJobQueueFileNames = Initialization.driver.findElements(By.xpath("//table[@id='subjobQueueDataGrid']/tbody/tr/td[2]"));
		for(int i=0;i<=SubJobQueueFileNames.size()-1;i++)
		{
			String FileName = SubJobQueueFileNames.get(i).getText();
			switch (FileName) 
			{
			case Config.MultipleFileTransferFile1:
				Reporter.log(Config.MultipleFileTransferFile1+" "+"is displayed",true);
				flag=true;
				break;

			case Config.MultipleFileTransferFile2:
				Reporter.log(Config.MultipleFileTransferFile2+" "+"is displayed",true);
				flag=true;
				break;

			case Config.MultipleFileTransferFile3:
				Reporter.log(Config.MultipleFileTransferFile3+" "+"is displayed",true);
				flag=true;
				break;

			case Config.MultipleFileTransferFile4:
				Reporter.log(Config.MultipleFileTransferFile4+" "+"is displayed",true);
				flag=true;
				break;

			default:
				flag=false;
				break;
			}
			String Pass="PASS >> All The Files Are Present Inside SubJob Queue";
			String Fail="FAIL >> Files Are Not Present Inside SubJob Queue";
			ALib.AssertTrueMethod(flag, Pass, Fail);

		}
	}
	
	public void VerifyMultipleJobsSubJobQueue(String JObName) throws Throwable
    {
    	try
    	{
    	Initialization.driver.findElement(By.xpath("//table[@id='subjobQueueDataGrid']/tbody/tr/td[2]/p[text()='"+JObName+"']")).isDisplayed();
    	Reporter.log("FAIL>>> "+JObName+ "Job Is  Present inside SubJob Queue Queue",true);
    	}
    	catch (Exception e) 
    	{
    		ClickOnSubStatusJobCloseButton();
    		ClickOnJobQueueCloseBtn();
    		ALib.AssertFailMethod("FAIL>>> "+JObName+ "Job Is Not Found inside SubJob Queue Queue");
		}
    }

	public void VerifySubJobStatus()
	{
		boolean flag;
		for(int i=0;i<=SubJobStatus.size()-1;i++)
		{   			
			if(SubJobStatus.get(i).getText().equals("In progress"))
			{
				flag=true;
				Reporter.log("SubJob Status Is Inprogress Status",true);
			}
			else
			{
				flag=false;
				Reporter.log("Subjob Status Is"+" : "+SubJobStatus.get(i).getText(),true);
			}
			String Pass="PASS >>Job Is in Inprogress State";
			String Fail="FAIL >>Job Is Not In Inprogress State";
			SoftAssert softAssertion = new SoftAssert();
			softAssertion.assertTrue(flag);
			if(flag==true)
			{
				System.err.println(Pass);
				Reporter.log(Pass);
			}
			else{
				System.err.println(Fail);
				Reporter.log(Fail);
			}
		}
	}

	public void WaitForErrorStatus()
	{
		boolean flag;
		try
		{
			waitForXpathPresent("//i[@class='st-jobStatus dev_st_icn poor fa fa-exclamation-circle']");
			sleep(3);
			flag=true;

		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS >> Job Deployed With Error Status";
		String Fail="FAIL >> Job Not Deployed With Error Status";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}
	public void VerifyErrorStatusInSubJobQueue()
	{
		boolean flag;
		try
		{
			flag=Initialization.driver.findElement(By.xpath("//p[text()='Error']")).isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS>> Sub Job Status Is Error i.e Over All Job Status Is Error";
		String Fail="FAIL>> Sub Job Status Is Not Error Even Over All Job Status Is Error";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void ClickOnSubJobQueueRefreshButton() throws InterruptedException
	{
		SubJobQueueRefreshButton.click();
		waitForXpathPresent("(//div[@id='subjobQueueModel']//button)[2]");
		sleep(3);

	}

	public void WaitForInprogressJob() throws InterruptedException
	{
		waitForXpathPresent("//i[@class='st-jobStatus dev_st_icn fa fa-spinner']");
		sleep(5);

	}

	public void VerifyDownloadProgress() throws InterruptedException
	{
		boolean flag;
		WebDriverWait wait=new WebDriverWait(Initialization.driver,90);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[contains(text(),'%')]")));
		for(int i=0;i<DownloadProgressColumn.size();i++)
		{
			String DownloadProgress = DownloadProgressColumn.get(i).getText();
			if(DownloadProgress.contains("%"))
			{
				flag=true;
				SubJobQueueRefreshButton.click();
				sleep(3);
			}
			else
			{
				flag=false;
			}
			String Pass="Download Progress Is Shown In SubJob Queue"+" "+DownloadProgressColumn.get(i).getText();
			String Fail="Download Progress Isn't Shown In SubJob Queue"+" "+DownloadProgressColumn.get(i).getText();
			SoftAssert soft=new SoftAssert();
			soft.assertTrue(flag);
			if(flag==true)
			{
				System.err.println(Pass);
				Reporter.log(Pass,true);
			}
			else
			{
				System.err.println(Fail);
				Reporter.log(Fail,true);

			}
		}
	}

	@FindBy(id="miscellaneous_apply")
	private WebElement MiscellaneousSettingApplyButton;

	@FindBy(id="deleteDeviceBtn")
	private WebElement QuickActionDeleteButton;

	@FindBy(xpath="//*[@id='deviceConfirmationDialog']/div/div/div[1]")
	private WebElement DeleteDeviceConfirmationMessage;

	@FindBy(xpath="(//button[text()='No'])[2]")
	private WebElement DeleteDeviceConfirmationMessage_No_Button;

	@FindBy(xpath="(//button[text()='Yes'])[2]")
	private WebElement DeleteDeviceConfirmationMessage_Yes_Button;

	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;

	@FindBy(xpath="//button[@id='historyjobqueueremovebtn']")
	private WebElement CompletedHistoryTabRemoveButton;




	public void ClickOnMiscellaneousApplyButton() throws InterruptedException
	{
		MiscellaneousSettingApplyButton.click();
		try
		{
			waitForXpathPresent("//span[text()='Settings updated successfully.']");
		}
		catch(Exception e)
		{
			Reporter.log("Settings Update Message Not Displayed",true);
		}
		sleep(3);
	}

	public void ClickOnQuickAction_DeleteButton() throws InterruptedException
	{
		QuickActionDeleteButton.click();
		waitForXpathPresent("//*[@id='deviceConfirmationDialog']/div/div/div[1]");
		sleep(3);
	}

	public void ClickOnDeployedJobAndVerifyRemoveButton(String Xpath) throws InterruptedException
	{
		boolean flag;
		Initialization.driver.findElement(By.xpath(Xpath)).click();
		String Status = CompletedHistoryTabRemoveButton.getCssValue("color");
		if(Status.equals("rgba(115, 115, 115, 1)"))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="Deployed Job Cannot Be Deleted";
		String Fail="Deployed Job Can Be Deleted";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}



	public void VerifyDeleteDeviceConfirmationMessageUI()
	{
		try
		{
			DeleteDeviceConfirmationMessage.isDisplayed();
			Reporter.log("Confirmation Message Is Displayed",true);
		}
		catch(Exception e)
		{
			Reporter.log("Confirmation Message Isn't Displayed");
		}
		WebElement[]	ConfirmationMessageElements= {DeleteDeviceConfirmationMessage_Yes_Button,DeleteDeviceConfirmationMessage_No_Button};
		String[] Text= {"DeleteDeviceConfirmationMessage_Yes_Button","DeleteDeviceConfirmationMessage_No_Button"};
		for(int i=0;i<=ConfirmationMessageElements.length-1;i++)
		{
			boolean val=ConfirmationMessageElements[i].isDisplayed();
			String Pass="PASS >>>"+Text[i]+"Button Is Displayed In Confirmation Message";
			String Fail="FAIL >>>"+Text[i]+"Button Isn't Displayed In Confirmation Message";
		}

	}
	public void ClickOnDeleteDevice_YesButton() throws InterruptedException
	{
		DeleteDeviceConfirmationMessage_Yes_Button.click();
		System.out.println("Clicked On Yes");
		waitForXpathPresent("//span[text()='Device(s) deleted successfully.']");
		while(Initialization.driver.findElement(By.xpath("(//div[text()='Loading...'])[3]")).isDisplayed())
		{

		}
		sleep(20);
	}
	public void VerifyDeletedDeviceInConsole(String DeviceName) throws InterruptedException
	{
		boolean flag;
		SearchDeviceTextField.clear();
		SearchDeviceTextField.sendKeys(DeviceName);
		try
		{
			flag=Initialization.driver.findElement(By.xpath("//p[text()='"+DeviceName+"']")).isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS >> Deleted Device Is Not present In The Console";
		String Fail="FAIL >> Deleted Device Is Present In The Console";
		ALib.AssertFalseMethod(flag, Pass, Fail);
	}

	Actions action=new Actions(Initialization.driver);
	public void SelectingMultipleDevices() throws AWTException, InterruptedException
	{
		WebElement dev1 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[1]/td[2]"));
 		WebElement dev2 = Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr[2]/td[2]"));
 		action.keyDown(Keys.CONTROL).click(dev1).click(dev2).keyUp(Keys.CONTROL).build().perform();
 		sleep(2);
 	}
	

	
	//Location Tracking
	@FindBy(id="locateBtn")
	private WebElement LocateBtn;

	@FindBy(xpath="//a[@id='locationTrackingEditBtn']/preceding-sibling::span")
	private WebElement DeviceINfoLocationTrackingStatus;

	@FindBy(xpath="//a[@id='locationTrackingEditBtn']")
	private WebElement EditLocationButton;

	@FindBy(xpath="//button[@onclick='locationTrackingOkClick()']")
	private WebElement ApplyLocationTracking;

	@FindBy(xpath="//*[@id='locationTrackingModal']/div/div/div[2]/div[1]/div/label")
	private WebElement LocationSwitch;

	@FindBy(xpath="//button[text()='Turn On']")
	private WebElement LocationTrackingTurnOnButton;

	@FindBy(xpath="//input[@id='job_text']")
	private WebElement TrackingPerodicity;

	@FindBy(xpath="(//div[@id='addGeoTagSection']/div/p)[1]")
	private WebElement RealTimeLocationTrackingDeviceName;

	@FindBy(xpath="(//div[@id='addGeoTagSection']/div/p)[6]")
	private WebElement RealTimeLoctionTrackingAddress;

	@FindBy(id="historytab")
	private WebElement LocationTrackingHistroryTab;

	@FindBy(xpath="//span[text()='Export']")
	private WebElement LocationTrackingHistoryTabExportButton;

	@FindBy(id="clearBtn")
	private WebElement LocationTrackingClearHistoryButton;

	@FindBy(xpath="(//button[text()='OK'])[4]")
	private WebElement LocationTrackingClearHistoryConfirmationOKButton;

	@FindBy(xpath="//div[@id='location_left_side_bar']/div/div/div/div/input")
	private WebElement LocationTrackingDeviceSearchTextBox;

	@FindBy(xpath="//button[@title='Zoom in']")
	private WebElement LocationTrackingZoomInButton;

	@FindBy(xpath="//button[@title='Zoom out']")
	private WebElement LocationTrackingZoomOutButton;


@FindBy(xpath="//button[text()='Satellite']")
private WebElement LocationTrackingSatelliteButton;

	@FindBy(id="exportGridView")
	private WebElement QuickActionTollbarExportButton;

	@FindBy(xpath="//p[text()='"+Config.DeviceName+"']")
	public WebElement DeviceName;
	
	@FindBy(xpath="//p[text()='"+Config.MultipleDevices1+"']")
	public  WebElement MultipleDevices1;

	@FindBy(xpath="//*[@id='gridMenu']/li[5]")
	private WebElement RightClickMoveToGroup;

	@FindBy(xpath="(//input[@placeholder='Search By Group Name'])[2]")
	private WebElement SearchGroupsButton;

	@FindBy(xpath="//li[@class='list-group-item node-groupList search-result']")
	private WebElement SearchedGroupName;

	@FindBy(xpath="//div[@id='groupListModal']//button[@type='button'][normalize-space()='Move']")
	private WebElement MoveDeviceButton;

	@FindBy(xpath="//div[@id='righttoggler']")
	private WebElement HistoryTabToggleButton;

	public String  PARENTWINDOW;

	public void ClickOnLocate_UM() throws InterruptedException
	{
		Reporter.log("Clicking On Locate Button",true);
		PARENTWINDOW=Initialization.driver.getWindowHandle();
		LocateBtn.click();
		sleep(5);
	}

	public void windowhandles()
	{
		Set<String> set=Initialization.driver.getWindowHandles();
		Iterator<String> id=set.iterator();
		parentwinID =id.next();
		childwinID =id.next();
	}

	public void SwitchToLocateWindow() throws InterruptedException
	{
		Initialization.driver.switchTo().window(childwinID);
		waitForXpathPresent("//button[@id='refreshbtn']");
		sleep(3);
		URL=Initialization.driver.getCurrentUrl();
	}

	public void SwitchToMainWindow() throws InterruptedException{
		Initialization.driver.close();
		Initialization.driver.switchTo().window(parentwinID);
		sleep(5);
	}

	public void CheckingLocationTrackingStatus() throws InterruptedException
	{
		if(DeviceINfoLocationTrackingStatus.getText().equals("OFF"))
		{
			Reporter.log("Location Tracking Is OFF ",true);
		}
		else
		{	
			EditLocationButton.click();
			waitForidPresent("isLocationTrackingOn");
			sleep(3);
			LocationSwitch.click();
			ApplyLocationTracking.click();
			sleep(4);
			Reporter.log("Location Tacking Updated Message Displayed Successfully",true);
		}
	}

	public void ClickingOnTurnOnButton() throws InterruptedException
	{
		LocationTrackingTurnOnButton.click();
		//   waitForXpathPresent("//h4[text()='Location Tracking ']");
		sleep(3);
		Reporter.log("Enable Location Tracking Pop Up Is Displayed",true);
	}

	public void TurningOnLocationTracking(String TrackingPeriodicity) throws InterruptedException
	{
		TrackingPerodicity.clear();
		TrackingPerodicity.sendKeys(TrackingPeriodicity);
		ApplyLocationTracking.click();
		//waitForXpathPresent("//span[text()='Device tracking updated successfully.']");
		sleep(5);
		Reporter.log("Location Tracking Updated Successfully Message Displayed",true);
	}

	public void WaitingForUpdate(int waittime) throws InterruptedException
	{
		Reporter.log("Waiting For Console Updatation",true);
		sleep(waittime);
	}

	public void VerifyingDeviceNameInRealTimeLocationTracking(String DeviceName)
	{

		boolean flag;
		String LocationTrackingDeviceName = RealTimeLocationTrackingDeviceName.getText();
		System.out.println(LocationTrackingDeviceName);
		if(LocationTrackingDeviceName.contains(DeviceName))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="Device Name Is Displyed Correctly ";
		String Fail="Device Name Isn't Displayed Correctly";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifyingDeviceAddressInRealTimeLocationTracking(String Location,String PinCode)
	{
		boolean flag;
		String LoctionTrackingAddress=RealTimeLoctionTrackingAddress.getText();
		if(LoctionTrackingAddress.contains(Location)||LoctionTrackingAddress.contains(PinCode))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="Location Tracking Address Is Displayed Correctly"+" : "+LoctionTrackingAddress;
		String Fail="Location Tracking Address Isn'rt Displayed"+" : "+LoctionTrackingAddress;
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void ClosingLoctionTrackingWindow() throws InterruptedException
	{
		Reporter.log("Closing Location Tracking Window",true);
		Initialization.driver.close();
		sleep(5);
		Initialization.driver.switchTo().window(parentwinID);
	}

	public void ClickingonLocationTrackingHistoryTab() throws InterruptedException
	{
		Reporter.log("Clicking On Location Tracking HitoryTab",true);
		LocationTrackingHistroryTab.click();
		waitForVisibilityOf("//span[text()='Export']");
		sleep(2);
	}
	public void ClickingOnExportButton(String Downloadpath) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
    {        
            String downloadPath = Downloadpath;
            String fileName =Config.DeviceName+".csv";
            String dd = fileName.replace(".", "  ");
            String[] text = dd.split("  ");
            File dir = new File(downloadPath);
            File[] dir_contents = dir.listFiles();
            int initial = 0;
            for (int i = 0; i < dir_contents.length; i++)
            {
                    if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) 
                    {

                            initial++;
                    }
            }

            LocationTrackingHistoryTabExportButton.click();
            sleep(10);

            dir = new File(downloadPath);
            dir_contents = dir.listFiles();
            int result=0;
            for (int i = 0; i < dir_contents.length; i++)
            {
                    if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) 
                    {
                            result++;

                    }
            }
            boolean value=result>initial;
            String PassStatement="PASS >> Location Tracking History CSV File is Downloaded successfully";
            String FailStatement="Fail >> Location Tracking History CSV File is not Downloaded";
            ALib.AssertTrueMethod(value, PassStatement, FailStatement);
    }
	
	public void ClickingOnClearHistoryButton() throws InterruptedException
	{
		Reporter.log("Clicking On Clear History Button",true);
		LocationTrackingClearHistoryButton.click();
		waitForXpathPresent("//p[text()='Are you sure you want to clear History?']");
		sleep(2);
	}

	public void ClickingOnClearLocationHistoryConfirmationOKButton() throws InterruptedException
	{
		Reporter.log("Clicking On LocationHistoryConfirmationOKButton",true);
		LocationTrackingClearHistoryConfirmationOKButton.click();
		waitForXpathPresent("//span[text()='Device history cleared successfully']");
		sleep(4);
		Reporter.log("Device history Cleared Successfully Message Displayed Successfully",true);
	}

	public void VerifyingHistoryTabAfetrClearingHistory()
	{
		boolean flag;
		try
		{
			flag=Initialization.driver.findElement(By.xpath("//div[contains(text(),'No Location available for these device(s)')]")).isDisplayed();
			Reporter.log("No History Is Available In History Tab",true);
		}
		catch(Exception e)
		{
			flag=false;
			Reporter.log("History Tab Is Not Empty Even After Clearing History",true);
		}
	}

	public void VerifyingMultipleDevicesInsideLocationTrackingPage() throws InterruptedException
	{
		boolean flag;
		sleep(4);
		List<WebElement> DeviceNamesInsideLocationTrackingPage = Initialization.driver.findElements(By.xpath("//div[@id='location_left_side_bar']/div/div/div[2]/div/table[@id='devicetable']/tbody/tr/td[1]"));

		int count = DeviceNamesInsideLocationTrackingPage.size();
		System.out.println(count);
		for(int i=0;i<count;i++)
		{
			System.out.println(DeviceNamesInsideLocationTrackingPage.get(i).getText());
			if(DeviceNamesInsideLocationTrackingPage.get(i).getText().equals(Config.DeviceName)||DeviceNamesInsideLocationTrackingPage.get(i).getText().equals(Config.MultipleDevices1))
			{
				flag=true;
				String DeviceNameInsideLocationTracking = DeviceNamesInsideLocationTrackingPage.get(i).getText();
				Reporter.log(DeviceNameInsideLocationTracking+" "+"Is Present Inside Location History Tab",true);
			}
			else
			{
				flag=false;
			}
			String Pass="PASS >>> Device Is Present Inside Location HIstory Tab";
			String Fail="FAIL >>> Device Isn't Present Inside Location History Tab";
			ALib.AssertTrueMethod(flag, Pass,Fail);
		}
	}

	public void SearchingForDeviceInLocationTrackingPage(String DeviceName) throws InterruptedException
	{
		LocationTrackingDeviceSearchTextBox.clear();
		sleep(2);
		LocationTrackingDeviceSearchTextBox.sendKeys(DeviceName);
		waitForXpathPresent("//p[text()='"+DeviceName+"']");
		sleep(2);
	}

	public void ClickOnZoomInButton() throws InterruptedException
	{
		Reporter.log("Clicking On Location Tracking Map ZoomIn Button",true);
		LocationTrackingZoomInButton.click();
		LocationTrackingZoomInButton.click();
		LocationTrackingZoomInButton.click();
		sleep(2);
		PassScreenshot.captureScreenshot(Initialization.driver, "ZoomIN");	  
	}

	public void ClickOnZoomOutButton() throws InterruptedException
	{
		Reporter.log("Clicking On Location Tracking Map Zoom Out Button",true);
		LocationTrackingZoomOutButton.click();
		LocationTrackingZoomOutButton.click();
		LocationTrackingZoomOutButton.click();
		sleep(2);
		PassScreenshot.captureScreenshot(Initialization.driver, "ZoomOut");	  
	}

	public void TakingScreenShotOfNormalMap()
	{
		PassScreenshot.captureScreenshot(Initialization.driver, "Maps Normal View");	  
	}

	public void ClickOnSatelliteButtonInLocationTrackingPage() throws InterruptedException
	{
		LocationTrackingSatelliteButton.click();
		sleep(3);
		PassScreenshot.captureScreenshot(Initialization.driver, "Maps Satellite View");	  

	}

	public void ClickingOnOpenHistoryTogglerButton() throws InterruptedException
	{
		HistoryTabToggleButton.click();
		waitForXpathPresent("//select[@id='devices']");
		sleep(2);
		PassScreenshot.captureScreenshot(Initialization.driver, "Loaction History");	  

	}

	public void VerifyLocationHistroy()
	{		
		boolean flag;
		List<WebElement> history = Initialization.driver.findElements(By.xpath("//table[@id='locationtable']/tbody/tr/td[2]"));
		if(history.size()>0)
		{
			flag=true;
			for(int i=0;i<history.size()-1;i++)
			{
				System.out.println(history.get(i).getText());
			}
		}
		else
		{
			flag=false;
		}
		String Pass="Device History Is upadted";
		String Fail="Device History Is Not Updated";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void ClickingOnExportButtonAndVerifyingFileDownload(String Downloadpath) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		String downloadPath =Downloadpath;
		String fileName = "Export.csv";
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		QuickActionTollbarExportButton.click();
		sleep(10);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=result>initial;
		String PassStatement="PASS >> Export.CSV Downloaded successfully";
		String FailStatement="Fail >> Export.CSV is not Downloaded";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickingOnMoveToGroup(WebElement ele) throws InterruptedException
	{
		act.contextClick(ele).perform();
		waitForXpathPresent("//li[@data-action='refresh']");
		sleep(2);
		RightClickMoveToGroup.click();
		sleep(2);
	}

	public void MovingDeviceToGroup(String GroupName,String DeviceName) throws InterruptedException
	{
		SearchGroupsButton.sendKeys(GroupName);
		waitForXpathPresent("//li[@class='list-group-item node-groupList search-result']");
		sleep(3);
		SearchedGroupName.click();
		sleep(3);
		MoveDeviceButton.click();	
		sleep(3);
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")).click();
		sleep(2);
		Reporter.log("Devices moved successfully",true);
	}

	@FindBy(xpath="//button[@class='btn logsMenu_openBtn ct-noActive']")
	private WebElement LogsHamburgerButton;

	@FindBy(xpath="//span[text()='Clear Log']")
	private WebElement ClearLogsButton;

	public void ClearingConsoleLogs() throws InterruptedException
	{
		LogsHamburgerButton.click();
		waitForXpathPresent("//span[text()='Clear Log']");
		sleep(2);
		ClearLogsButton.click();
		sleep(3);

	}

	/////////////////////////////////////Job Queue Updated ////////////////////////////////////////////////////////////////////////
	public String ErrorjobName="SureFoxSettings_Automation";


	@FindBy(xpath="//i[@class='fa fa-circle st-jobStatus dev_st_icn ok ']")
	public WebElement JobPendingStateSymbol;

	@FindBy(xpath="//i[@class='fa fa-circle st-jobStatus dev_st_icn ']")
	public WebElement JobInprogressStateSymbol;

	@FindBy(xpath="//i[@class='fa fa-circle st-jobStatus dev_st_icn poor ']")
	public WebElement JobErrorStateSymbol;

	@FindBy(xpath="//i[@class='fa fa-circle st-jobStatus dev_st_icn good ']")
	public WebElement JobSuccessStateSymbol;

	@FindBy(xpath="//table[@id='jobQueueHistoryGrid']/tbody/tr[1]/td[2]/p")
	private WebElement LatestDeployedJobName;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[2]/p")
	public WebElement LatestPendingJobName;

	@FindBy(xpath="//table[@id='jobQueueErrorGrid']/tbody/tr[1]/td[2]/p")
	private WebElement LatestErrorJobName;

	@FindBy(xpath="//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[2]/p")
	private WebElement LatestInprogressJobName;

	@FindBy(xpath="//div[@id='errorhistoryjobqueueremovebtn']")
	private WebElement ErrorJobRemoveButton;

	@FindBy(css="div#jobqueueremovebtn")
	public WebElement IncompleteJobRemoveButton;
	
	@FindBy(id="jobqueueremoveDynamicbtn")
	private WebElement QuickActionQueueRemoveButton;


	int NumbofJobsInJobHistory;

	public void GettingInitialCountOfJobsInJobQueue(String TabName,int TabNumber) throws InterruptedException
	{
		clickOnJobHistoryButton();
		String NumberofJobsBeforeApplyingJob=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+TabNumber+"]/div[2]")).getText();
		NumbofJobsInJobHistory = Integer.parseInt(NumberofJobsBeforeApplyingJob);
		Reporter.log("Number Of jobs Present In"+ " " +TabName+" "+ "Is" + " " +NumbofJobsInJobHistory,true);
		ClickOnJobQueueCloseBtn();
	}

	public void WaitingForJobStatusUpdationInConsole(String JobStatus,String DeviceName) throws InterruptedException
	{
		switch (JobStatus) 
		{
		case "Pending" :

			waitForElementPresent("//i[@class='fa fa-circle st-jobStatus dev_st_icn ok ']");
			Reporter.log("Job Is In Pending State",true);
			break;	

		case "Inprogress" :
			waitForElementPresent("//i[@class='fa fa-circle st-jobStatus dev_st_icn ']");
			Reporter.log("Job Is In Inprogress State",true);
			break;

		case "Error" :
			waitForElementPresent("//i[@class='fa fa-circle st-jobStatus dev_st_icn poor ']");
			waitForLogXpathPresent("//p[contains(text(),'Job("+ErrorjobName+") deployed with error on "+DeviceName+"')]");
			Reporter.log("Job Is In Error State",true);
			sleep(3);
			break;
		}
	}

	public void VerifyAppliedJobIsPresentInsideIncompleteJobsTab(String JobName)
	{
		String JobNameInIncompleteJobsTab =LatestPendingJobName.getText();
		if(JobNameInIncompleteJobsTab.equals(JobName))
		{
			Reporter.log("Applied Job Is Present Inside Incomplete Jobs Tab",true);
		}
		else
		{
			ALib.AssertFailMethod("Applied Job Is Not Present Inside Incomplete jobs Tab");
		}
	}

	public void VerifyAppliedJobIsPresentInsideFailedJobsTab(String JobName)
	{
		String JobNameInFailedJobsTab=LatestErrorJobName.getText();
		String JobStatusInsideFailedJobsTab = Initialization.driver.findElement(By.xpath("//table[@id='jobQueueErrorGrid']/tbody/tr[1]/td[6]")).getText();
		if(JobNameInFailedJobsTab.equals(JobName)&&JobStatusInsideFailedJobsTab.equals("Error"))
		{
			Reporter.log("Applied job Is Present Inside Failed Jobs Tab",true);
		}
		else
		{
			ALib.AssertFailMethod("Applied job Is Not Present Inside Failed jobs Tab");
		}
	}

	public void VerifyAppliedJobIsPresentInsideCopletedJobTab(String JobNAme)
	{
		
		String LatestDeployedJObName =LatestDeployedJobName.getText();
		if(LatestDeployedJObName.equals(JobNAme))
		{
			Reporter.log("Applied Job IS successfully Deployed into device And Present Under Completed JobTab",true);
		}

		else
		{
			ALib.AssertFailMethod("Applide Job Is Not Deployed Into Device");
		}		
	}
	public void VerifyJobsCountInsideJobsQueue_AfterApplyingJob(String TabName,int TabNumber) throws InterruptedException
	{
		String NumberofJobsAfterApplyingJob=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+TabNumber+"]/div[2]")).getText();
		int FinalNumberOfJobs=Integer.parseInt(NumberofJobsAfterApplyingJob);
		ClickOnJobQueueCloseBtn();
		Reporter.log("Number Of jobs Present In"+" " +TabName+" " + "Is" +" "+ FinalNumberOfJobs,true);
		if(FinalNumberOfJobs==NumbofJobsInJobHistory+1)
		{
			Reporter.log("PASS>>> jobs Count Is Updated Successfully",true);
		}
		else
		{
			ClickOnJobQueueCloseBtn();
			ALib.AssertFailMethod("Jobs Count Is Not Updted");
		}
	}

	public void ClickOnSureFoxSettingsJob() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='surefox_settings']")).click();
		waitForidPresent("SaveAsBtn");
		sleep(2);
	}

	public void CreatingSureFoxSettingsJob(String JobName) throws InterruptedException
	{
		ClickOnSureFoxSettingsJob();
		Initialization.driver.findElement(By.id("SaveAsBtn")).click();
		waitForLogXpathPresent("//div[@id='saveAsJobModal']/div/div/div[3]/button[text()='Save']");
		sleep(3);
		Initialization.driver.findElement(By.id("job_name")).sendKeys(JobName);
		sleep(2);
		Initialization.driver.findElement(By.xpath("//div[@id='saveAsJobModal']/div/div/div[3]/button[text()='Save']")).click();
		waitForXpathPresent("//span[text()='Job created successfully.']");
		sleep(4);
	}

	public void ClickOnFailedJobsTab() throws InterruptedException
	{
		FailedJobsTab.click();
		waitForidPresent("jobQueueErrrHistoryRefreshBtn");
		sleep(4);
	}

	public void DisablingNixService() throws InterruptedException
	{
		/*String val = Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox[@index='0']").getAttribute("checked");
		if(val.equals("true"))
		{
			Reporter.log("Nix Service Is Enabled Hence Disabling",true);
			Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox[@index='0']").click();
		}else{
			Reporter.log("Nix Service IS Already Disabled",true);
		}*/
		//waitForXpathPresent("//android.widget.CheckBox");
		//String nixcheckbox=Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox").getAttribute("checked");	
		String nixcheckbox=Initialization.driverAppium.findElementByXPath("(//android.widget.CheckBox)[1]").getAttribute("checked");
		sleep(2);
		if(nixcheckbox.equals("true"))
		{
			Reporter.log("Nix Service Is Enabled Hence Disabling",true);
			Initialization.driverAppium.findElementByXPath("(//android.widget.CheckBox)[1]").click();
		}else{
			Reporter.log("Nix Service IS Already Disabled",true);
		}
	}

	public void ClickOnJobStatus(WebElement ele) throws InterruptedException
	{
		ele.click();
		waitForidPresent("jobqueuetitle");
		sleep(3);
	}

	public void ClickOnNixSettings() throws InterruptedException
	{
		/*try{ Initialization.driver.findElement(By.xpath("//android.widget.Button[@text='Settings' and @index='0']")).click();
		}
		
		catch(Exception e)
		{
			Initialization.driverAppium.tap(1, Initialization.driver.findElement(By.xpath("//android.widget.Button[@text='Settings' and @index='0']")), 500);
		}*/
		
		try
		{
		WebElement NixSettingsButton = Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Settings' and @index='0']");
	    NixSettingsButton.click();
		}
		catch(Exception e) 
		{
		  Initialization.driverAppium.findElementById("com.nix:id/button2").click();
		}
	}
	
	public void goBack() throws InterruptedException{
		sleep(1);
		Initialization.driverAppium.navigate().back();
		sleep(1);
		Reporter.log("Tapped on Back Button", true);
	}

	public void EnablingCheckBoxes(String Text,int a) throws InterruptedException
	{

		List<WebElement> ListOfNixCheckBox = Initialization.driverAppium.findElementsByClassName("android.widget.CheckBox");
		String CheckBoxStatus = ListOfNixCheckBox.get(a).getAttribute("checked");
		if(CheckBoxStatus.equals("true"))
		{

			Reporter.log("CehckBox Is Alreay Enabled",true);
		}
		else
		{
			Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Text+"']").click();
			Reporter.log("Enabled"+" "+Text+"Check Box",true);
			sleep(3);
		}    	
	}

	public void VerifyJobStatusChangeFromPendingToInprogressInDevieGrid() throws InterruptedException
	{
		WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		Reporter.log("PASS>>> Job Status Is Successfully Changed From Pending To Inprogress",true);
	}

	@FindBy(xpath="//table[@id='jobQueueErrorGrid']/tbody/tr")
	private List<WebElement> NumberOfFailedJobs;


	public void RemovingExistingFailedJobsInDevice() throws InterruptedException
	{
		clickOnJobHistoryButton();
		ClickOnFailedJobsTab();
		System.out.println(NumberOfFailedJobs.size());
		if(NumberOfFailedJobs.size()>=1)
		{
			for(int i=0;i<=NumberOfFailedJobs.size();i++)
			{
				NumberOfFailedJobs.get(0).click();
				sleep(2);
				ErrorJobRemoveButton.click();
				waitForXpathPresent("//div[@id='errorhistoryjobqueueremovebtn']");
				sleep(3);
			}
		}
		else
		{
			Reporter.log("No job Is Present Inside Failed Jobs Tab",true);
		}
		ClickOnJobQueueCloseBtn();
	}

	public void VerifyJObStatusChangeFromPendingToinprogressAndError() throws InterruptedException
	{
		WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		//WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		Reporter.log("PASS>>> Job Status Is Successfully Changed From Pending To Inprogress And Error",true);
	}

	int JobCountInDeviceGrid;
	public void ReadingToolTipMessageForJobStatusInDeviceGrid(WebElement JobStatus)
	{
		String JobStatusToolTipMessage = JobStatus.getAttribute("title");
		String[] el = JobStatusToolTipMessage.split(" ");
		JobCountInDeviceGrid =Integer.parseInt(el[0]);
		System.out.println(JobStatusToolTipMessage);
	}
	
	

	public void VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame(String TabName,int TabNumber) throws InterruptedException
	{
		GettingInitialCountOfJobsInJobQueue(TabName,TabNumber);
		if(NumbofJobsInJobHistory==JobCountInDeviceGrid)
		{
			Reporter.log("PASS>>> Number Of Jobs In Device Grid And Job History Are Matching",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Number Of Jobs In Device Grid And Job History Are not Matching");
		}
	}

	int NumberOfjobsBeforeDeletingJobsInJobHistory;
	public void ReadingJobsCountBeforeDeletingJobsInJobHistory(String TabName,int TabNumber) throws InterruptedException
	{
		String NumberofJobsBeforeApplyingJob=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+TabNumber+"]/div[2]")).getText();
		NumberOfjobsBeforeDeletingJobsInJobHistory = Integer.parseInt(NumberofJobsBeforeApplyingJob);
		Reporter.log("Number Of jobs Present In"+ " " +TabName+" "+ "Is" + " " +NumberOfjobsBeforeDeletingJobsInJobHistory,true);
	}
	public void VerifyJobsCountafterDeletingjobsInJobHistory(String TabName,int TabNumber,WebElement LatestJob,WebElement RemoveButton,String jobStatus) throws InterruptedException
	{
		LatestJob.click();
		sleep(2);
		if(jobStatus.equals("Inprogress"))
		{
			RemoveButton.click();
			waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']");
			sleep(2);
			Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[2][text()='Yes']")).click();
			waitForidPresent("jobqueuetitle");
			sleep(4);

		}
		else 
		{
			RemoveButton.click();
			waitForidPresent("jobqueuetitle");
			sleep(4);
		}
		String NumberofJobsAfterDeletingJob=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+TabNumber+"]/div[2]")).getText();
		int NumberOfjobsAfterDeletingJobsInJobHistory = Integer.parseInt(NumberofJobsAfterDeletingJob);
		VerifyMessageInsideJobHistoryTabsWhenNoJobsArePresent("Pending");
		if(NumberOfjobsAfterDeletingJobsInJobHistory==NumberOfjobsBeforeDeletingJobsInJobHistory-1)
		{

			Reporter.log("Number Of jobs Present In"+ " " +TabName+" "+ "Is" + " " +NumberOfjobsAfterDeletingJobsInJobHistory,true);
			Reporter.log("PASS>>> Job Count Is Updated Successfully After Deleting The Job In"+" "+TabName,true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Job Count Is Not Updated After Deleting The Job In "+" "+TabName);
		}
	}

	public void DeletingMultipleJobsInJobHistory() throws InterruptedException, AWTException
	{
		Robot r=new Robot();
		ClickOnFailedJobsTab();
		for(int i=0;i<NumberOfFailedJobs.size();i++)
		{
			NumberOfFailedJobs.get(i).click();
			r.keyPress(KeyEvent.VK_CONTROL);	
			sleep(2);
			r.keyPress(KeyEvent.VK_DOWN);
		}
		r.keyRelease(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_DOWN);
		ErrorJobRemoveButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicator']/div/div/div[text()='Loading...']")).isDisplayed())
		{}
		waitForXpathPresent("//div[text()='No Failed jobs']");
		sleep(5);
	}

	public void VerifyJobsCountafterDeletingFailedjobsInJobHistory(String TabName,int TabNumber) throws InterruptedException
	{
		String NumberofJobsAfterDeletingFailedJob=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+TabNumber+"]/div[2]")).getText();
		int NumberOfjobsAfterDeletingFailedJobsInJobHistory = Integer.parseInt(NumberofJobsAfterDeletingFailedJob);
		System.out.println(NumberOfjobsAfterDeletingFailedJobsInJobHistory);
		VerifyMessageInsideJobHistoryTabsWhenNoJobsArePresent("Failed");
		if(NumberOfjobsAfterDeletingFailedJobsInJobHistory==0)
		{
			Reporter.log("PASS>>> Multiple Failed Jobs Deleted Successfully",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Unable To Delete Multiple Failed Jobs In Job History");
		}
	}

	public void VerifyMessageInsideJobHistoryTabsWhenNoJobsArePresent(String JobStatus) throws InterruptedException
	{
		boolean Val=Initialization.driver.findElement(By.xpath("//div[text()='No "+JobStatus+" jobs']")).isDisplayed();
		String Pass="PASS>>> No "+JobStatus+" Jobs Message Is Shown When No Jobs Are Present";
		String Fail="FAIL>>> No "+JobStatus+" Jobs Message Is Not Shown When No Jobs Are Present";
		ClickOnJobQueueCloseBtn();
		ALib.AssertTrueMethod(Val, Pass, Fail);
	}

	@FindBy(xpath="//div[@id='reportList_tableCont']/section/div[2]/div/div[2]/input")
	private WebElement SearchFieldInsideReport;

	@FindBy(xpath="//div[text()='Deployed Jobs Count']")
	private WebElement CompletedJobsCountColumnInReports;

	@FindBy(xpath="//div[@id='jobqueueRetryBtn']")
	private WebElement InprogressAndPendingJobRetryButton;

	@FindBy(xpath="//div[@id='jobQueueRefreshBtn']")
	private WebElement IncompletejobsRefreshButton;

	@FindBy(xpath="//div[@id='errorhistoryjobqueueRetryBtn']")
	private WebElement FailedJobsRetrybutton;

	@FindBy(xpath="//div[@id='jobQueueButton']")
	private WebElement JobQueueButton;

	@FindBy(xpath="//i[@class=' icn fa fa-plus']")
	private WebElement ScheduleReportAddButton;

	@FindBy(xpath="//div[@id='reportGenerate-time-wrapper']/div/input")
	private WebElement ScheduleReportTimeField;

	@FindBy(xpath="//button[@id='scheduleReportBtn']")
	private WebElement ReportScheduleButton;

	@FindBy(xpath=".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[2]")
	private WebElement viewReportButton;

	public void SearchingInsideReport(String DeviceName) throws InterruptedException
	{
		waitForXpathPresent("//input[@placeholder='Search']");
		SearchFieldInsideReport.clear();
		sleep(2);
		SearchFieldInsideReport.sendKeys(DeviceName);
		waitForXpathPresent("//p[text()='"+DeviceName+"']");
		sleep(3);
	}
	public void SwitchBackWindow() throws InterruptedException {

		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);
		sleep(3);
	}
	
	String jobCountInString;

	public void VerifyScheduledjobsCountInsideReport(String Column,int ColumnNumber) throws InterruptedException
	{
		js.executeScript("arguments[0].scrollIntoView()",Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[62]/p")));
		sleep(4);
		jobCountInString = Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td["+ColumnNumber+"]/p")).getText();
		int jobCountInsideReport = Integer.parseInt(jobCountInString);
		if(jobCountInsideReport==NumbofJobsInJobHistory)
		{
			Reporter.log("Job Count Inside "+Column+" In Report Is"+" "+jobCountInsideReport,true );
			Reporter.log("PASS>>> Job Count Inside "+Column+" In Report Is Equal To Job Count In Device Grid",true);
		}
		else
		{
			SwitchBackWindow();
			Reporter.log("Job Count Inside "+Column+" In Report Is"+" "+jobCountInsideReport,true);
			ALib.AssertFailMethod("FAIL>>> Job Count Inside "+Column+" In Report Is Not Equal To Job Count In Device Grid");
		}
	}

	public void RetryingJobPresentInIncompleteJobsTab() throws InterruptedException
	{
		LatestInprogressJobName.click();
		//		IncompletejobsRefreshButton.click();
		sleep(7);
		InprogressAndPendingJobRetryButton.click();
		System.out.println("Clicked On Retry Button");
		sleep(2);
	}

	int NumberOfjobsAfterRetryingJobsInJobHistory;
	public void ReadingJobsCountAfterRetryingJobsInJobHistory(String TabName,int TabNumber) throws InterruptedException
	{
		String NumberofJobsAfterRetringJob=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+TabNumber+"]/div[2]")).getText();
		NumberOfjobsAfterRetryingJobsInJobHistory = Integer.parseInt(NumberofJobsAfterRetringJob);
		Reporter.log("Number Of jobs Present In"+ " " +TabName+" "+ "Is" + " " +NumberOfjobsAfterRetryingJobsInJobHistory,true);
	}

	public void clickonJobQueue() throws InterruptedException
	{
		JobQueueButton.click();
		waitForXpathPresent("//div[@id='jobQueueRefreshBtn']");
		sleep(3);
	}

	public void VerifyJobCountAndJobStatusAfterRetryingInprogressJob() throws InterruptedException
	{
		IncompletejobsRefreshButton.click();
		sleep(3);
		String JobStatusInIncompletejobsTab = Initialization.driver.findElement(By.xpath("//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[6]")).getText();
		System.out.println(JobStatusInIncompletejobsTab);
		if(JobStatusInIncompletejobsTab.contains("Pending")||JobStatusInIncompletejobsTab.contains("-")||JobStatusInIncompletejobsTab.contains("JobStatusInIncompletejobsTab")||JobStatusInIncompletejobsTab.contains("Scheduled")&&NumberOfjobsAfterRetryingJobsInJobHistory==NumbofJobsInJobHistory-1)
		{
			ClickOnJobQueueCloseBtn();
			Reporter.log("PASS>>> Job Status Is Pending After retrying Inprogress Job",true);
		}
		else
		{
			ClickOnJobQueueCloseBtn();
			ALib.AssertFailMethod("FAIL>>> Retrying Inprogress Job Is Failed");
		}
	}

	public void ClickOnIncompleteJobsTab() throws InterruptedException
	{
		IncompleteJobsTab.click();
		waitForXpathPresent("//div[@id='jobQueueRefreshBtn']");
		sleep(1);
	}

	public void RetryingFailedJob() throws InterruptedException
	{
		LatestErrorJobName.click();
		sleep(2);
		FailedJobsRetrybutton.click();
		waitForXpathPresent("//div[@id='errorhistoryjobqueueRetryBtn']");
		sleep(3);
	}

	public void VerifyJobCountAndJobStatusAfterRetryingFailedJob() throws InterruptedException
	{
		ClickOnIncompleteJobsTab();
		/*String JobStatusInIncompletejobsTab = Initialization.driver.findElement(By.xpath("//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[6]")).getText();
		String PendingJobName = LatestPendingJobName.getText();*/
		if(NumberOfjobsAfterRetryingJobsInJobHistory==NumbofJobsInJobHistory-1)
		{
			ClickOnJobQueueCloseBtn();
			Reporter.log("PASS>>> Job Status Is Pending After retrying Error Job",true);
		}

		else
		{
			ClickOnJobQueueCloseBtn();
			ALib.AssertFailMethod("FAIL>>> Retrying Error Job Is Failed");
		}
	}

	int TimeBefore;
	int TimeAfter;

	public  void GettingTimeBeforeRetryingPendingJob() throws InterruptedException
	{
		String JobStatusDetails = Initialization.driver.findElement(By.xpath("//table[@id='jobQueueDataGrid']/tbody/tr/td[7]")).getText();
		String[] Ele = JobStatusDetails.split(" ");
		String[] Ele1 = Ele[3].split(":");
		TimeBefore=Integer.parseInt(Ele1[2]);
	}

	public void GettingTimeAfterRetryingPendingJob() throws InterruptedException
	{
		String JobStatusDetails = Initialization.driver.findElement(By.xpath("//table[@id='jobQueueDataGrid']/tbody/tr/td[7]")).getText();
		String[] Ele = JobStatusDetails.split(" ");
		String[] Ele1 = Ele[3].split(":");
		TimeAfter=Integer.parseInt(Ele1[2]);

		ClickOnJobQueueCloseBtn();
	}

	public void VerifyRetryingPendingJob()
	{
		if(TimeBefore==TimeAfter)
		{
			ALib.AssertFailMethod("Retry Of Pending Job Is Failed");	
		}

		else
		{
			Reporter.log("Retry Of Pending Job Is Successfull",true);
		}
	}

	int NumbofJobsInJobHistoryAfterDeviceIsOffline;
	int NumbofJobsInJobHistoryForOnlineDevice;

	ArrayList<Integer> al=new ArrayList<Integer>();
	ArrayList<Integer> al1=new ArrayList<Integer>();

	public void GettingJobCountsForAnOnlineDevice() throws InterruptedException
	{
		clickOnJobHistoryButton();
		for(int i=1;i<5;i++)
		{
			String NumberofJobsInHistory=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+i+"]/div[2]")).getText();
			NumbofJobsInJobHistoryForOnlineDevice = Integer.parseInt(NumberofJobsInHistory);
			al.add(NumbofJobsInJobHistoryForOnlineDevice);
		}
		ClickOnJobQueueCloseBtn();

	}

	public void GettingAllJobCounts() throws InterruptedException
	{
		clickOnJobHistoryButton();
		for(int i=1;i<5;i++)
		{
			String NumberofJobsInHistory=Initialization.driver.findElement(By.xpath("//div[@id='jobQ-info-container']/div["+i+"]/div[2]")).getText();
			NumbofJobsInJobHistoryAfterDeviceIsOffline = Integer.parseInt(NumberofJobsInHistory);
			al1.add(NumbofJobsInJobHistoryAfterDeviceIsOffline);
			
		}
		System.out.println(al1);
		ClickOnJobQueueCloseBtn();
	}

	public void VerifyingJobCountForOnlineAndOfflineDevice()
	{
		for(int i=0;i<al.size();i++)
		{
			if(al.get(i)==al1.get(i))
			{
				Reporter.log("Job Count Are Showing Correctly For Offline Device",true);
			}
			else
			{
				ALib.AssertFailMethod("Job Count Are Not Showing Correctly For Offline Device");
			}
		}
	}

	public void SchedulingReport() throws InterruptedException
	{
		ScheduleReportAddButton.click();
		waitForXpathPresent("//button[@id='scheduleReportBtn']");
		sleep(3);
	}

	int CurrentMinute;
	int CurrenTime;

	public void FetchingCurrentTime()
	{
		SimpleDateFormat dateformat=new SimpleDateFormat("HH:mm");
		Date time=new Date();
		String CurrentTime = dateformat.format(time);
		System.out.println(CurrentTime);
		String[] Arr = CurrentTime.split(":");
		CurrenTime=Integer.parseInt(Arr[0]);
		CurrentMinute = Integer.parseInt(Arr[1]);
	}

	public void ClickOnScheduleButton() 
	{
		ReportScheduleButton.click();
		waitForXpathPresent("//i[@class=' icn fa fa-plus']");
	}

	public void SchedulingReportTimings() throws InterruptedException
	{
		FetchingCurrentTime();
		ScheduleReportTimeField.click();
		sleep(5);
		if(CurrentMinute>=30)
		{
			int TimeToBeClicked = CurrenTime+1;
			WebElement 	ele=Initialization.driver.findElement(By.xpath("(//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_'])[3]/div[2]/div/div[1]/div[text()='"+TimeToBeClicked+":00"+"']"));
			js.executeScript("arguments[0].scrollIntoView()", ele);
			ele.click();
		}
		else if(CurrentMinute<30)
		{
			WebElement 	ele1=Initialization.driver.findElement(By.xpath("(//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_'])[3]/div[2]/div/div[1]/div[text()='"+CurrenTime+":30"+"']"));
			js.executeScript("arguments[0].scrollIntoView()", ele1);
			ele1.click();
		}
		ClickOnScheduleButton();
		sleep(3);
	}

	public void WaitingForReportGeneration() throws InterruptedException
	{
		Reporter.log("Waiting For Report Generation",true);
		sleep(210);
		Initialization.driver.findElement(By.xpath("//section[@id='report_viewSect']//button[@name='refresh']")).click();
		sleep(5);
	}

	public void SearchingForScheduledReport() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@class='rptTable_cont ct-noCont-wrapper']/div/div[1]/div[2]/input")).sendKeys("Asset Tracking");
		String ReportName = Initialization.driver.findElement(By.xpath(".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[1]/p")).getText();
		sleep(4);
		if(ReportName.equals("Asset Tracking"))
		{
			viewReportButton.click();
		}
		else
		{
			ALib.AssertFailMethod("Scheduled Report Is Not Yet Generated");
		}		
		sleep(3);
	}

	ArrayList<Integer>  al2=new ArrayList<Integer>();
	public void GettingJobsCountInsideScheduledReport() throws InterruptedException
	{
		js.executeScript("arguments[0].scrollIntoView()",Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td[62]/p")));
		sleep(4);
		for(int i=59;i<63;i++)
		{
			jobCountInString = Initialization.driver.findElement(By.xpath("//table[@id='devicetable']/tbody/tr/td["+i+"]/p")).getText();
			int jobCountInsideScheduleReport = Integer.parseInt(jobCountInString);
			System.out.println(jobCountInsideScheduleReport);
			al2.add(jobCountInsideScheduleReport);
		}	
	}

	public void VerifyJobCountInsideJobHistoryAndScheduleReport()
	{
		for(int i=0;i<al.size();i++)
		{
			if(al.get(i)==al2.get(i))
			{
				Reporter.log("PASS>>> Job Count Inside Report Is Equal To Job Count In Device Grid",true);
			}

			else
			{
				ALib.AssertFailMethod("FAIL>>> Job Count Inside Report Is Not Equal To Job Count In Device Grid");
			}
		}
	}

	public void VerifyDeletingDeployedDynamicJob(String JobName) throws InterruptedException
	{
		String job=Initialization.driver.findElement(By.xpath("//table[@id='jobQueueDataDynamicGrid']/tbody/tr[1]/td[1]/p[1]")).getText();
		if(job.equals("Remote support"))
			Reporter.log("PASS>>> Applied Dynamic Job Is Listed In QuickAction tab",true);
		else
		{
			ClickOnJobQueueCloseBtn();
	    	ALib.AssertFailMethod("FAIL>>> Applied Dynamic Job Is Not Listed In QuickAction tab");	
		}
		Initialization.driver.findElement(By.xpath("(//table[@id='jobQueueDataDynamicGrid']/tbody/tr/td/p[text()='"+JobName+"'])[1]")).click();
		String Status = QuickActionQueueRemoveButton.getAttribute("class");
	    if(Status.contains("disabledclass"))
	    	Reporter.log("PASS>>> Remove Button Is Greyed out");
	    else
	    {
	    	ClickOnJobQueueCloseBtn();
	    	ALib.AssertFailMethod("FAIL>>> Remove Button Is Not Greyed out");
	    }	
	}
	
	public void VerifysubJobqueuePromptIsDisplyed() throws Throwable
	{
		try
		{
			SubJobStatusRefreshButton.isDisplayed();
			Reporter.log("PASS>>> SubJobqueue Prompt Is Displyed Successfully with Refresh Button",true);
		}
		catch(Exception e)
		{
			ClickOnSubStatusJobCloseButton();
			ClickOnJobQueueCloseBtn();
			ALib.AssertFailMethod("FAIL>>> SubJobqueue Prompt Is Not Displyed with Refresh Button");
			
		}
	}
	
	public void SwitchingWifiStatus(String Status) throws InterruptedException
	{
		Initialization.driverAppium.toggleWifi();
		Reporter.log("Wifi Status Is "+Status,true);
		sleep(15);
		
	}
	
	public void VerifyJobStaysInPendingStatewhenWifiIsOFF(String JobStatus,String DeviceName) throws InterruptedException
	{
		//sleep(15);
		try
		{
			WaitingForJobStatusUpdationInConsole(JobStatus,DeviceName);
			Reporter.log("PASS>>> Job Is In Pending State As Wifi Connection Is Off",true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Job Is Not In Pening State");
		}
	}
	
	public void VerifySubJobststus(String jobstatus) throws Throwable
	{
		try
		{
		  Initialization.driver.findElement(By.xpath("//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p[text()='"+jobstatus+"']")).isDisplayed();
	      Reporter.log("PASS>>> Sub Job Status Is "+jobstatus,true);
		}
		catch (Exception e) 
		{
			ClickOnSubStatusJobCloseButton();
    		ClickOnJobQueueCloseBtn();
    		ALib.AssertFailMethod("FAIL>>> "+jobstatus+ "Job Is Not Found inside SubJob Queue Queue");
		}
	}
	
	public void VerifyMultipleSubJobStatus() throws Throwable
	{
		List<WebElement> status = Initialization.driver.findElements(By.xpath("//table[@id='subjobQueueDataGrid']/tbody/tr/td[5]/p"));	
	     for(int i=0;i<status.size();i++)
	     {
	    	 String jobStatus=status.get(i).getText();
	    	 if(jobStatus.equals("In progress")||jobStatus.equals("Deployed"))
	    	 {
	    		Reporter.log("PASS>>> Sub Job status is shown Correctly when main job status is InProgress state",true);
	    	 }
	    	 
	    	 else
	    	 {
	    		 ClickOnSubStatusJobCloseButton();
	    	     ClickOnJobQueueCloseBtn();
	    		 ALib.AssertFailMethod("FAIL>>> Sub Job status is Not shown Correctly when main job status is InProgress state");
	    	 }
	     }
	
	}
	@FindBy(id="recursiveScheduletime")
	private WebElement PeriodicallyRadioBtn;

	@FindBy(id="recursiveIntervaltime")
	private WebElement EnterTime;
	
    @FindBy(id="recursiveSchedule")
	private WebElement ScheduleRadioButton;

    @FindBy(xpath="//input[@id='recursiveInterval']/../input[2]")
	private WebElement ScheduleDaysTimeTextBox;

    @FindBy(id="ScheduleWithDateAndTime")
	private WebElement SelectDateAndtimeRadioBtn;

	@FindBy(id="datetimepicker")
	private WebElement SelectDateAndtimeEdit;
	
    @FindBy(xpath=".//*[@id='okbutton']")
	private WebElement ScheduleJobOkBtn;


    
	public void SelectDayFromDayCal() throws InterruptedException {
		Date date = new Date( ); // Instantiate a Date object  
		System.out.println(date);
		String[] s1 = date.toString().split(" ");
		String day = s1[0];
		System.out.println(day);
		sleep(1);
		WebElement ClickingOnDay= Initialization.driver.findElement(By.xpath("//*[@id='recursiveInterval_div']/div[2]/div/div/span[contains(text(),'"+day+"')]"));
		Actions actions = new Actions(Initialization.driver);
		actions.moveToElement(ClickingOnDay).click().build().perform();
		sleep(2);
	}
	
	public void SearchFieldApplyJobAndScheduleTime(String JobName,String RadioButton) throws InterruptedException, AWTException
	{
		ScheduleJobButton.click();
		waitForidPresent("recursiveScheduletime");
		sleep(1);
		if(RadioButton.equals("Periodically")) {

			PeriodicallyRadioBtn.click();
			waitForidPresent("recursiveIntervaltime");
			EnterTime.sendKeys("5");}

		else if(RadioButton.equals("ScheduleDayAndTime")) {
			ScheduleRadioButton.click();
			ScheduleDaysTimeTextBox.click();
			Date date = new Date( ); // Instantiate a Date object  
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MINUTE,3);
			String IncreasedfiveMins =cal.getTime().toString();
			System.out.println(IncreasedfiveMins);
			String hour = IncreasedfiveMins.substring(11, 16);

			int i=0;
			List<WebElement> ShdeuledDaysAndTimeDropDown = Initialization.driver.findElements(By.xpath("//*[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_']"));
			for (WebElement values:ShdeuledDaysAndTimeDropDown) {
				String styleval = values.getAttribute("style");
				if (!styleval.isEmpty()) {
					i=i+1;
					break;
				}
				i=i+1;
			}
			WebElement hourScheduledHour = Initialization.driver.findElement(By.xpath("(//*[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_'])["+i+"]/div[2]/div/div[1]/div[contains(text(),'"+hour+"')]"));
			JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
			je.executeScript("arguments[0].scrollIntoView()",hourScheduledHour);
			sleep(4);
			hourScheduledHour.click();
			sleep(2);
			SelectDayFromDayCal();}
		else {
			SelectDateAndtimeRadioBtn.click();
			SelectDateAndtimeEdit.click();

			Date date = new Date( ); // Instantiate a Date object  
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MINUTE,3);
			String IncreasedfiveMins =cal.getTime().toString();
			System.out.println(IncreasedfiveMins);
			String hour = IncreasedfiveMins.substring(11, 16);

			int i=0;
			List<WebElement> ShdeuledDaysAndTimeDropDown = Initialization.driver.findElements(By.xpath("//*[@class='xdsoft_time_variant']"));
			for (WebElement values:ShdeuledDaysAndTimeDropDown) {
				String styleval = values.getAttribute("style");
				if (!styleval.isEmpty()) {
					i=i+1;
					break;
				}
				i=i+1;
			}
			WebElement hourScheduledHour = Initialization.driver.findElement(By.xpath("((//*[@class='xdsoft_time_variant'])[\"+i+\"]/div[contains(text(),'"+hour+"')])[2]"));
			JavascriptExecutor je = (JavascriptExecutor) Initialization.driver;
			je.executeScript("arguments[0].scrollIntoView()",hourScheduledHour);
			sleep(6);
			hourScheduledHour.click();

		}

		ScheduleJobOkBtn.click();		
		sleep(4);
	}
	
	public void VerifyJobStatusScheduled() throws InterruptedException
	{
		try 
		{
		String JobStatusInIncompletejobsTab = Initialization.driver.findElement(By.xpath("//table[@id='jobQueueDataGrid']/tbody/tr[1]/td[6]")).getText();
		Assert.assertTrue(JobStatusInIncompletejobsTab.equals("Scheduled"));
		Reporter.log("Job Status Is Scheduled",true);
		ClickOnJobQueueCloseBtn();
		
		}
	catch(Exception e)
	{
	   ClickOnJobQueueCloseBtn();
	}

	}
	//Job Queue For Multiple Devices

	@FindBy(xpath="//a[text()='Pending']")
	private WebElement PendingJobsTab_MutipleDevice;

	@FindBy(xpath="//a[text()='In Progress']")
	private WebElement InprogressJobsTab_MultipleDevices;

	@FindBy(xpath="//a[text()='Failed']")
	private WebElement FailedJobsTab_MultipldeDevices;

	@FindBy(xpath="//a[text()='Completed']")
	private WebElement CompletedJobstab_MultipleDevices;

	@FindBy(xpath="//div[@id='multiDev-jobQueueGridContainer']/div[1]/div[2]/div[1]/table/thead/tr/th/div[text()='Job Name']")
	private WebElement JobNameColumn_MultipleDevices;

	@FindBy(xpath="//div[@id='multiDev-jobQueueGridContainer']/div[1]/div[2]/div[1]/table/thead/tr/th/div[text()='Type']")
	private WebElement jobTypeColumn_MultipleDevices;

	@FindBy(xpath="//div[@id='multiDev-jobQueueGridContainer']/div[1]/div[2]/div[1]/table/thead/tr/th/div[text()='Size']")
	private WebElement JobSizeColumn_MultipleDevices;

	@FindBy(xpath="//div[@id='multiDev-jobQueueGridContainer']/div[1]/div[2]/div[1]/table/thead/tr/th/div[text()='Number of devices']")
	private WebElement NumberOfDevicesColumn_MultipleDevices;
	
	@FindBy(id="multiDev-jobqueueremovebtn")
    private WebElement JobRemoveButton_MultipleDevices;
	
	@FindBy(id="multiDev-jobqueueretrybtn")
	private WebElement JobRetryButton_MultipleDevices;
	
	@FindBy(xpath="(//div[contains(text(),'No Job available')])[1]")
	private WebElement NoJobsMessage;

	public void VerifyJobHistoryForMultileDevices()
	{
		WebElement[] eles= {PendingJobsTab_MutipleDevice,InprogressJobsTab_MultipleDevices,FailedJobsTab_MultipldeDevices,CompletedJobstab_MultipleDevices,JobNameColumn_MultipleDevices,jobTypeColumn_MultipleDevices,JobSizeColumn_MultipleDevices,NumberOfDevicesColumn_MultipleDevices};
		String[] Names= {"Pending Jobs Tab","inprogress Jobs Tab","Failed Jobs Tab","Completed Jobs Tab","Job Name Column","Job Type Column","Job Size Column","Number Of Devices Column"};
		for(int i=0;i<eles.length;i++)
		{
			boolean val = eles[i].isDisplayed();
			String Pass="PASS>>>" + " "+Names[i]+ "Is Displayed Successfully";
			String Fail="FAIL>>>" + " "+Names[i]+ "Is Not Displayed";
			ALib.AssertTrueMethod(val, Pass, Fail);
		}
	}

	public void ClickOnJobQueueCloseButton_MultipleDevices() throws InterruptedException
	{
		JobQueueCloseButtonForMultipleDevices.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(3);
	}


	public void VerifyJobsTabForMultipleDevices(String jobName,String jobType,String NumberOfDevices) throws InterruptedException
	{
		ArrayList<String> al=new ArrayList<>();
		String[] a= new String[]{jobName,jobType,NumberOfDevices};
		String [] b= {"JobName","JobType","NumberOfDevices"};
		for(int i=2;i<6;i++)
		{
			if(i!=4)
			{
				String val = Initialization.driver.findElement(By.xpath("//table[@id='multiDev-jobQueueDataGrid']/tbody/tr/td["+i+"]")).getText();
				al.add(val);
			}
		}

		for(int j=0;j<al.size();j++)
		{
			if(al.get(j).equals(a[j]))
			{
				Reporter.log("PASS>>>" +b[j]+" "+"Is Shown Correctly",true);
			}

			else
			{
				ClickOnJobQueueCloseButton_MultipleDevices();
				ALib.AssertFailMethod("FAIL>>>" +b[j]+" "+ "Is Not Shown Correctly");
			}
		}
	}

	public void ClickOnJobTabsInsidejobHistoryForMultipleDevices(String TabName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//a[text()='"+TabName+"']")).click();
		sleep(4);   
	}

	public void VerifyCompletedJobsTabForMultipleDevices(String JobName,String jobType,String NumberOfDevices) throws InterruptedException
	{
		WebElement ele = Initialization.driver.findElement(By.xpath("//td[text()='"+JobName+"']"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		sleep(3);
		ArrayList<String> al=new ArrayList<>();
		String[] a= new String[]{jobType,NumberOfDevices};
		String [] b= {"JobType","NumberOfDevices"};
		for(int i=1;i<4;i++)
		{
			if(i!=2)
			{
				String val = Initialization.driver.findElement(By.xpath("//td[text()='"+JobName+"']/following-sibling::td["+i+"]")).getText();
				System.out.println(val);
				al.add(val);
			}
		}

		for(int j=0;j<al.size();j++)
		{
			if(al.get(j).equals(a[j]))
			{
				Reporter.log("PASS>>>" +b[j]+" "+"Is Shown Correctly",true);
			}

			else
			{
				ClickOnJobQueueCloseButton_MultipleDevices();
				ALib.AssertFailMethod("FAIL>>>" +b[j]+" "+ "Is Not Shown Correctly");
			}
		}
	}

	public void SelectingJobInsideJobQueue(String JobName) throws InterruptedException
	{
		try
		{
		Initialization.driver.findElement(By.xpath("//td[text()='"+JobName+"']")).click();
		sleep(3);
		}
		catch (Exception e) 
		{
		   Reporter.log("Job Is Not Present Inside Job Queue",true);
		}
	}
	
	public void ClickonJobRemoveButton() throws InterruptedException
	{
		JobRemoveButton_MultipleDevices.click();
		waitForXpathPresent("//span[text()='Job deletion initiated. It may take some time to reflect the changes.']");
		sleep(2);
	}
	
	public void ClickOnJobRetryButton() throws InterruptedException
	{
		JobRetryButton_MultipleDevices.click();
		waitForXpathPresent("//span[text()='Reapplied job successfully.']");
		Reporter.log("PASS>>> Reapplied job successfully Message Is Displayed Successfully",true);
		sleep(2);
		
	}
	
	public void VerifyJobsDeletionInMultipleDevicesJobQueue()
	{
		try 
		{
			NoJobsMessage.isDisplayed();
			sleep(2);
			Reporter.log("PASS>>> No Jobs are Present Inside Job Queue",true);
			sleep(3);
		}
		
		catch (Exception e) 
		{
		    ALib.AssertFailMethod("FAIL>>> Job Are Still Present Inside Job Queue");
		}
	}
	
	public void SelectingDeployedJObInsideCompletedJobsTab(String JobName) throws InterruptedException
	{
		WebElement ele = Initialization.driver.findElement(By.xpath("//td[text()='"+JobName+"']"));
		js.executeScript("arguments[0].scrollIntoView()", ele);
		Initialization.driver.findElement(By.xpath("//td[text()='"+JobName+"']")).click();
		sleep(3);
	}
	
	public void VerifyRemoveButtonIsGrayedOutWhenTriedToDeleteCompletedJobs()
	{
		String State = JobRemoveButton_MultipleDevices.getAttribute("class");
		if(State.equals("btn smdm_btns smdm_red_clr deletebutton disabledclass"))
		{
			Reporter.log("PASS>>> Remove Button Have Been Grayed Out In Completed Jobs Tab",true);
		}
		else
		{
			ALib.AssertFailMethod("FAIL>>> Remove Button Have Not Been Grayed Out In Completed JObs Tab");
		}
	}
	
	public void WaitForMultipleDevice_JobInprogressStatus()
	{
		waitForXpathPresent("(//i[@class='fa fa-circle st-jobStatus dev_st_icn '])[1]");
		waitForXpathPresent("(//i[@class='fa fa-circle st-jobStatus dev_st_icn '])[2]");
	}
	
	public void VerifyRetryOfFailedJobs() throws InterruptedException
	{
		WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		WaitingForJobStatusUpdationInConsole("Error",Config.MultipleDevices1);
		Reporter.log("PASS>>> Failed Jobs Applied On Device On Retrying",true);
		
	}
	
	public void VerifyPendingJobForMultipleDevice(String JobName) throws InterruptedException
	{
		boolean Bool = Initialization.driver.findElement(By.xpath("//table[@id='multiDev-jobQueueDataGrid']/tbody/tr/td[text()='"+JobName+"']")).isDisplayed();
	    String Pass="PASS>>> User Can See The Pending Jobs For Multiple Device";
	    String Fail="FAIL>>> User Is Not Able to See Pending Jobs For Multiple Devices";
	    ClickOnJobQueueCloseButton_MultipleDevices();
	    ALib.AssertTrueMethod(Bool, Pass, Fail);
	}
	
	


//////////////////////////Group JobQueue////////////////////////////
	
	@FindBy(id="groupjobqueue")
	private WebElement GroupJobQueueButton;
	
	@FindBy(id="ApplyJobtoGroup")
	private WebElement GroupApplyjobButton;
	
	@FindBy(id="groupjobqueueretrybtn")
	private WebElement GroupjobQueueRetryButton;
	
	@FindBy(id="groupjobqueueremovebtn")
	private WebElement GroupjobQueueRemoveButton;
	
	@FindBy(xpath="//ul[@id='grpOperationMenu']/li/span[text()='Job History']")
	private WebElement GroupJobhistoryButton_RightClick;
	
	
	public void ClickOnGroups(String GroupName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul/li[text()='"+GroupName+"']")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(3);
	}
	
	public void ClickOnGroupJobQueueButton() throws InterruptedException
	{
		GroupJobQueueButton.click();
		waitForidPresent("groupjobqueuetitle");
		sleep(3);
	}
	
	public void VerifyGroupJobQueueUI()
	{
		WebElement GroupjobQueueTitle=Initialization.driver.findElement(By.xpath("//h4[text()='Job History - "+Config.GroupName+"']"));
		WebElement[] eles= {GroupjobQueueTitle,PendingJobsTab_MutipleDevice,InprogressJobsTab_MultipleDevices,FailedJobsTab_MultipldeDevices,CompletedJobstab_MultipleDevices};
		String[] Names= {"Group Job Queue Title","Pending Jobs Tab","inprogress Jobs Tab","Failed Jobs Tab","Completed Jobs Tab"};
		for(int i=0;i<eles.length;i++)
		{
			boolean val = eles[i].isDisplayed();
			String Pass="PASS>>>" + " "+Names[i]+ " Is Displayed Successfully";
			String Fail="FAIL>>>" + " "+Names[i]+ " Is Not Displayed";
			ALib.AssertTrueMethod(val, Pass, Fail);
		}
	}
	
	public void ClickOnGroupJobQueueCloseButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='groupJobQ_modal']/div/button")).click();
		Reporter.log("Clicked On job Queue Button",true);
		sleep(3);
	}
	
	public void ClickOnGroup_ApplyButton() throws InterruptedException
	{
		sleep(3);
		GroupApplyjobButton.click();
		waitForidPresent("scheduleJob");
		sleep(3);
	}
	
	public void VerifyJobInsideTab(String Job,String Tabname) throws InterruptedException
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//table[@id='groupjobQueueDataGrid']/tbody/tr/td[text()='"+Job+"']")).isDisplayed();
			ClickOnGroupJobQueueCloseButton();
			Reporter.log("PASS>>> Job Is Present Inside " +Tabname+ " job Tab",true);
		}
		
		catch(Exception e)
		{
			ClickOnGroupJobQueueCloseButton();
	    	ALib.AssertFailMethod("Job Is not Present Inside " +Tabname+ " job Tab");
		}
		
	}
	
	public void ClickOnApplyJobOkButton_Group() throws InterruptedException
	{
		applyJobApplyButton.click();
		waitForXpathPresent("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='Yes']");
		Reporter.log("Clciked on Apply Button");
		sleep(4);
	}
	
	public void ClickOnApplyJobYesButton_Group() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='Yes']")).click();
		waitForidPresent("deleteDeviceBtn");
		sleep(4);
	}
	
	public void ClickOnJobInsideGroupJobQueue() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//table[@id='groupjobQueueDataGrid']/tbody/tr/td[2]")).click();
		sleep(4);
	}
	
	public void ClickOnRetryButtonInGroupJobQueue() throws InterruptedException
	{
		ClickOnJobInsideGroupJobQueue();
		GroupjobQueueRetryButton.click();
		waitForXpathPresent("//span[text()='Reapplied job successfully.']");
		Reporter.log("Reapplied job successfully Is Displayed",true);
		sleep(3);
	}
	
	public void ClickOnJobTabsInsidejobHistoryForGroups(String TabName,int sleeptime) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//a[text()='"+TabName+"']")).click();
		waitForXpathPresent("//table[@id='groupjobQueueDataGrid']/tbody/tr/td");
		sleep(sleeptime);   
		System.out.println("Clicked On "+TabName+" Tab");
	}
	
	public void RightClickOnGroup(String GroupName) throws InterruptedException
	{
		WebElement Group = Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul/li[text()='"+GroupName+"']"));
	    act.contextClick(Group).build().perform();
	    sleep(3);
	}
	
	public void ClickOnGroupJobHistory_RightClick() throws InterruptedException
	{
		GroupJobhistoryButton_RightClick.click();
		waitForidPresent("groupjobqueuetitle");
		sleep(3);
	}
	
	public void VerifyAppliedJobInsideSubGroupjobQueue(String Job) throws InterruptedException
	{
		try
		{
			Initialization.driver.findElement(By.xpath("//table[@id='groupjobQueueDataGrid']/tbody/tr/td[text()='"+Job+"']")).isDisplayed();
			ClickOnGroupJobQueueCloseButton();
			Reporter.log("PASS>>> Job Is Present Inside Pending Tab",true);
			Reporter.log("Job Applied To Sub Group Successfully",true);
		}
		
		catch(Exception e)
		{
			try
			{
		      ClickOnJobTabsInsidejobHistoryForGroups("In Progress",5);
		      Initialization.driver.findElement(By.xpath("//table[@id='groupjobQueueDataGrid']/tbody/tr/td[text()='"+Job+"']")).isDisplayed();
		      ClickOnGroupJobQueueCloseButton();
				Reporter.log("PASS>>> Job Is Present Inside In Progress Tab",true);
				Reporter.log("PASS>>> Job Applied To Sub Group Successfully",true);
			}
			
			catch(Exception a)
			{
				ClickOnGroupJobQueueCloseButton();
				ALib.AssertFailMethod("FAIL>>> Job Is Not Applied To Sub Group ");
			}
		}
	}
	
	public void ClickOnRemoveButton_GroupJobQueue() throws InterruptedException
	{
		ClickOnJobInsideGroupJobQueue();
		GroupjobQueueRemoveButton.click();
		waitForXpathPresent("//span[text()='Job deleted successfully.']");
		VerifyJobsDeletionInMultipleDevicesJobQueue();
	}
	//new Methods 
		@FindBy(xpath="//h4[normalize-space()='Apply Job/Profile To Device']")
		private WebElement ApplyJobWindow;
		public void VerifyOfApplyJobWindow()
		{
			System.out.println(ApplyJobWindow.getText());
			boolean flag = ApplyJobWindow.isDisplayed();
			String PassStatement = "Pass >> Apply Job Window is displayed successfully..";
			String FailStatement = "Fail >> Apply Job Window isn't displayed successfully..";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		
		public void VerifyOfUpdatedPeridicity(String TrackingPeriodicity,String LocationTrackPeriodicity) throws InterruptedException
		{
			TrackingPerodicity.clear();
			TrackingPerodicity.sendKeys(TrackingPeriodicity);
			ApplyLocationTracking.click();
			sleep(2);
			Reporter.log("Location Tracking Peridicity Updated Successfully",true);
			sleep(4);
			String ActualResult=Initialization.driver.findElement(By.xpath("(//*[@id='dis-card']/div[text()='Location Tracking']/following-sibling::div/span)[1]")).getText();
			Reporter.log(ActualResult,true);
			String ExpectedResult=LocationTrackPeriodicity ;
			String Pass="PASS>> Loctaion Tracking "+ActualResult+" Peridicity is Displayed";
			String Fail="FAIL>> Loctaion Tracking "+ActualResult+" Peridicity isn't Displayed";
			ALib.AssertEqualsMethod(ExpectedResult,ActualResult,Pass,Fail);
			sleep(2);
		}

		//Madhu
				public void ClickOnApplyJobNoButton_Group() throws InterruptedException
				{
					Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='No']")).click();
					waitForidPresent("deleteDeviceBtn");
					sleep(4);
				}
			
				public void VerifyJobNoPresentInsideTab(String Job) throws InterruptedException
				{
					try {
						Initialization.driver.findElement(By.xpath("//table[@id='groupjobQueueDataGrid']/tbody/tr/td[text()='"+Job+"']")).isDisplayed();
						ALib.AssertFailMethod("Job Is Present Inside job Tab");
						ClickOnGroupJobQueueCloseButton();
					}
					
					catch(Exception e)
					{
//						ClickOnGroupJobQueueCloseButton();
						Reporter.log("PASS>>> Job Is Not Present Inside job Tab",true);
					}
				}

	}



