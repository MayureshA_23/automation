package Profiles_Windows_Testscripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class WifiConfiguration  extends Initialization{
	
	@Test(priority='1',description="To verify clicking on WifiConfugration") 
    public void VerifyClickingOnWifiConfiguration() throws InterruptedException
	{
        Reporter.log("1.To verify clicking on WifiConfugration",true);
        profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
        profilesWindows.ClickOnWifiConfigurationPolicy();
    }
	@Test(priority='2',description="To Verify warning message Saving wifi configuration profile without profile Name")
	public void VerifyWarningMessageOnSavingWifiConfigurationProfileWithoutName() throws InterruptedException{
		Reporter.log("\n2.To Verify warning message Saving wifi configuration profile without profile Name",true);
		profilesWindows.ClickOnConfigureButton_WifiConfigurationProfile();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	
	@Test(priority='3',description="To Verify warning message Saving Wifi Configuration Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingWifiConfigurationProfileWithoutAllFielfs() throws InterruptedException{
		Reporter.log("\n3.To Verify warning message Saving Wifi Configuration Profile without entering all the fields",true);
		profilesWindows.EnterWifiConfigurationPolicyProfileName();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	
	@Test(priority='4',description="To Verify the parameters of Wifi Configuration Window")
	public void VerifyParamentersOfWifiConfigurationWindow() throws InterruptedException{
		Reporter.log("\n4.To Verify the parameters of Wifi Configuration Window",true);
		profilesWindows.ClickOnAddButton_WifiConfiguration();
		profilesWindows.VerifyAlltheParametersOfWifiConfigurationWindow();
	}
	
	@Test(priority='5',description="To Verify warning message while clicking Add Button of Wifi Configuration Window without entering all the fields")
	public void VerifyWarningMessageOnClickingOnAddButtonOfWifiConfigurationWindowWithoutAllFields() throws InterruptedException{
		Reporter.log("\n5.To Verify warning message while clicking Add Button of Wifi Configuration Window without entering all the fields",true);
		profilesWindows.AddButton_WifiConfigurationWindow();
		profilesWindows.WarningMessageWithoutOutgoing_MailConfiguration();//common
	}
	
	@Test(priority='6',description="To Verify cancelling the Wifi Configuration Window")
	public void VerifyCancellingWifiConfigurationWindow() throws InterruptedException{
		Reporter.log("\n6.To Verify cancelling the Wifi Configuration Window",true);
		profilesWindows.ClickOnCancelButton_WifiConfigurationWindow();
	}
	
	@Test(priority='7',description="To Verify all the parameters of Security Type Dropdown")
	public void VerifyParametersOfSecurityTypeDropdown() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n7.To Verify all the parameters of Security Type Dropdown",true);
		profilesWindows.ClickOnAddButton_WifiConfiguration();
		profilesWindows.VerifyAllOptionsOfSecurityTypeDropDown();
		
	}
	
	@Test(priority='8',description="To Verify adding the Wifi Configuration profiles ")
	public void VerifyAddingWifiConfigurationProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n8.To Verify all the parameters of Security Type Dropdown",true);
        profilesWindows.EnterSSID_WifiConfiguration();
		profilesWindows.AddButton_WifiConfigurationWindow();
	}
	
	@Test(priority='9',description="To Verify Row data of Wifi Configuration after adding")
	public void VerifyRowData_WifiConfigurationAfterAdding() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("\n9.To Verify Row data of Wifi Configuration after adding",true);
		profilesWindows.VerifySSID_SecurityTypeAfterAdding();
	}
	
	@Test(priority='A',description="To Verify Saving Wifi Configuration profile")
	public void VerifySavingWifiConfigurationProfile() throws InterruptedException{
		Reporter.log("\n10.To Verify Saving AppLocker profile",true);
	    profilesWindows.ClickOnSaveButton();
		profilesWindows.NotificationOnProfileCreated();
	}
	
	
	
	
}
