package PageObjectRepository;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.WebDriverCommonLib;

public class PreApproved_POM extends WebDriverCommonLib
{
	AssertLib ALib=new AssertLib();
      @FindBy(xpath="//p[@class='btn_txt_cover']/i[@class='fa fa-thumbs-up icon_color']")
      private WebElement PreApproved_Button;
      
      public void VerifyingPreApprovedButton()
      {
    	  boolean mark;
    	   if(PreApproved_Button.isDisplayed() && PreApproved_Button.isEnabled() )
    	   {
    		   mark=true;
    	   }
    	   else
    	   {
    		   mark=false;
    	   }
    	 String PassStatement="PASS >> preApproved Button is Displayed When Logged Into SureMDM Console";
      	 String FailStatement="FAIL >> preApproved Button is not Displayed When Logged Into SureMDM Console ";
      	 ALib.AssertTrueMethod(mark, PassStatement, FailStatement);
      }
      
 //Mithilesh=========================================================================================================
      
     
      
@FindBy(xpath="//*[@id='DeletedDeviceGrid']/tbody/tr[1]/td[1]")
private WebElement ClickonDevicetoDelete ;

@FindBy(xpath="//div[@id='ddDeleteBtn']//i[@class='icn fa fa-minus-circle']")
private WebElement ClickonForceDelete ;

@FindBy(xpath="//button[@onclick=\"closeDialog('ConfirmationDialog')\"]")
private WebElement PromptMessageNoButton ;

@FindBy(xpath="//p[contains(text(),'The device (or devices) you are about to')]")
private WebElement PromptMessageVisible ;


public void PromptMessageDisplayed() throws InterruptedException{
	 sleep(3);
     ClickonDevicetoDelete.click();
     sleep(3);
     ClickonForceDelete.click();                                
     sleep(3);
     
     boolean flag=PromptMessageVisible.isDisplayed();
 	if(flag)
 	{
 		String Value=Initialization.driver.findElement(By.xpath("//p[contains(text(),'The device (or devices) you are about to')]")).getText();
        Reporter.log("PASS >> Message Displayed "+Value,true);			
 	}
 	else
 	{
 		ALib.AssertFailMethod("FAIL >> Prompt Message is Not Displayed");
 		Reporter.log("FAIL >> Prompt Message is Not Displayed",true);
 	}
 	 sleep(2);
     PromptMessageNoButton.click();
     sleep(2);
     Reporter.log("PASS >> Prompt Window Closed",true);
}      
      
     
@FindBy(xpath="//*[@id='deletedSection']/a/p ")
private WebElement ClickonPendingDelete ;      
      
public void ClickonPendingDelete() throws InterruptedException{
	sleep(2);
	ClickonPendingDelete.click();
	sleep(2);
    Reporter.log("PASS >> Clicked on Pending Delete",true);
  
}          
      
@FindBy(xpath="//*[@id='preapprovedSection']/a/p")
private WebElement ClickonPreapproved ;      
      
public void ClickonPreapproved() throws InterruptedException{
	sleep(2);
	ClickonPreapproved.click();
	sleep(2);
    Reporter.log("PASS >> Clicked on Pending Delete",true); 
}               
      
@FindBy(xpath="//*[@id='preapprovedGrid']/tbody/tr[1]/td[1]/span/input")
private WebElement Checkboxselected ;  

@FindBy(xpath="//span[text()='Block Enrollment']")
private WebElement BlockedEnrollmentSelected ;  
      
public void BlockEnrollment() throws InterruptedException{
	sleep(2);
	Checkboxselected.click();
	sleep(2);
    Reporter.log("PASS >> Check Box is Selected",true);
    BlockedEnrollmentSelected.click();
    sleep(2);
    Reporter.log("PASS >> Block Enrollment is Selected",true);  
}        
      
@FindBy(xpath="//span[text()=' Allow Enrollment']")
private WebElement AllowEnrollmentSelected ;  
      
public void AllowEnrollment() throws InterruptedException{
	sleep(2);
	Checkboxselected.click();
	sleep(2);
    Reporter.log("PASS >> Check Box is Selected",true);
    AllowEnrollmentSelected.click();
    sleep(2);
    Reporter.log("PASS >> Allow Enrollment is Selected",true);  
}         
 
@FindBy(xpath="//*[@id='preapprovedGrid']/tbody/tr[1]/td[10]/p")
private WebElement VerifyBlockedinfirstColumn ; 

public void VerifyBlockEnrollment() throws InterruptedException{
	
	boolean flag=VerifyBlockedinfirstColumn.isDisplayed();
 	if(flag)
 	{
 		String Value=Initialization.driver.findElement(By.xpath("//*[@id='preapprovedGrid']/tbody/tr[1]/td[10]/p")).getText();
        Reporter.log("PASS >> Enrollment Status Displayed is : "+Value,true);			
 	}
 	else
 	{
 		ALib.AssertFailMethod("FAIL >> Enrollment Status Displayed is Not Displayed");
 		Reporter.log("FAIL >> Enrollment Status Displayed is Not Displayed",true);
 	}		
}    
    
@FindBy(xpath="//span[text()='Modified successfully.']")
private WebElement ModifiedSuccessfully ; 
  
public void ConfirmationMessageVerify(boolean value) throws InterruptedException{
	String PassStatement="PASS >> 'Modified successfully.' Message is displayed successfully";
	String FailStatement="FAIL >> 'Modified successfully.' Message is not displayed";
	Initialization.commonmethdpage.ConfirmationMessageVerify(ModifiedSuccessfully, value, PassStatement, FailStatement,3);
}
      
@FindBy(xpath="//*[@id='preapprovedGrid']/tbody/tr[1]/td[10]/p")
private WebElement VerifyAllowinfirstColumn ; 

public void VerifyAllowEnrollment() throws InterruptedException{
	
	boolean flag=VerifyAllowinfirstColumn.isDisplayed();
 	if(flag)
 	{
 		String Value=Initialization.driver.findElement(By.xpath("//*[@id='preapprovedGrid']/tbody/tr[1]/td[10]/p")).getText();
        Reporter.log("PASS >> Enrollment Status Displayed is : "+Value,true);			
 	}
 	else
 	{
 		ALib.AssertFailMethod("FAIL >> Enrollment Status Displayed is Not Displayed");
 		Reporter.log("FAIL >> Enrollment Status Displayed is Not Displayed",true);
 	}		
}       
      
@FindBy(xpath="//span[text()='Import']")
private WebElement ClickonImportFileButton ;
      
public void ClickonImportButton() throws InterruptedException{
	sleep(2);
	ClickonImportFileButton.click();
	sleep(2);
    Reporter.log("PASS >> Click on Import Button",true);
}   

public void browseImportFile(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);
	Reporter.log("PASS >> File Imported Successfully", true);
	sleep(2);
}

@FindBy(xpath="//span[text()='Import failed. A device with identical information is already enrolled.']")
private WebElement ImportfailedMessage ; 
  
public void ConfirmationErrorMessageVerify(boolean value) throws InterruptedException{
	
	String PassStatement="PASS >> 'Import failed. A device with identical information is already enrolled.' Message is displayed successfully";
	String FailStatement="FAIL >> 'Import failed. A device with identical information is already enrolled.' Message is not displayed";
	Initialization.commonmethdpage.ConfirmationMessageVerify(ImportfailedMessage, value, PassStatement, FailStatement,3);
}

@FindBy(xpath="//span[text()='Invite User']")
private WebElement InviteUserVisible ; 

public void InviteUserVisible() throws InterruptedException{
	
	boolean flag=InviteUserVisible.isDisplayed();
 	if(flag)
 	{		
        Reporter.log("PASS >> Invite User is Displayed ",true);			
 	}
 	else
 	{
 		ALib.AssertFailMethod("FAIL >> Invite User is Not Displayed");
 		Reporter.log("FAIL >> Invite User is Not Displayed",true);
 	}		
}      

public void ClickonInviteUser() throws InterruptedException{
	 sleep(2);
	 InviteUserVisible.click();
	 sleep(2);
	 Reporter.log("PASS >> Clicked on Invite User",true);		
}      

@FindBy(xpath="//h4[normalize-space()='Invite User']")
private WebElement InviteUserTittleVisible ; 

@FindBy(xpath="//span[normalize-space()='Invite User by Email']")
private WebElement InviteUserbyEmailVisible ; 

@FindBy(xpath="//span[normalize-space()='Invite User by SMS']")
private WebElement InviteUserbySMSVisible ; 


public void verifyOptionsInInviteUser() {
	WebElement[] ele = { InviteUserTittleVisible , InviteUserbyEmailVisible, InviteUserbySMSVisible };
	String[] Options = { "Invite User Tittle" , "Invite User by Email", "Invite User by SMS" };
	for (int i = 0; i < ele.length; i++) {
		boolean val = ele[i].isDisplayed();
		String Pass = "PASS >>" + " " + Options[i] + " Is Displayed Successfully";
		String Fail = "FAIL >>" + " " + Options[i] + " Is Not Displayed";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}
}

@FindBy(xpath="//*[@id='preapprovedGrid']/tbody/tr[1]/td[1]/span/input")
private WebElement SelectDevicePresent ; 

public void SelectDevicePresent() throws InterruptedException{
			
 		sleep(3);
 		SelectDevicePresent.click();
 		sleep(3);
        Reporter.log("PASS >> Device Selected",true);				
}      

@FindBy(xpath="//div[@id='getNix_appOpt_modal']//button[@aria-label='Close']")
private WebElement CloseInviteUserTab ; 

public void CloseInviteUserTab() throws InterruptedException{
			
 		sleep(3);
 		CloseInviteUserTab.click();
 		sleep(3);
        Reporter.log("PASS >> Invite User Tab Closed",true);				
}

@FindBy(xpath="//input[@id='emailnixapk']")
private WebElement InviteUserbyEmailCheckboxSelected ;

@FindBy(xpath="(//button[text()='OK'])[6]")
private WebElement InviteUserTabOKButton ;

public void InviteUserbyEmailCheckboxSelected()throws InterruptedException{
	
	boolean flag=InviteUserbyEmailCheckboxSelected.isSelected();
	if(flag==false)
	{	
		sleep(2);
		InviteUserbyEmailCheckboxSelected.click();
		sleep(2);		
	}	
	Reporter.log("PASS >> Invite User by Email Check Box Selected",true);
}

public void InviteUserTabOKButton() throws InterruptedException{
			
 		sleep(3);
 		InviteUserTabOKButton.click();
        Reporter.log("PASS >> Invite User Tab Closed",true);				
}

@FindBy(xpath="//span[contains(text(),'An invitation to join SureMDM has been sent to this device.')]")
private WebElement InviteUserMessage ; 
  
public void InviteUserMessage(boolean value) throws InterruptedException{
	
	String PassStatement="PASS >> 'An invitation to join SureMDM has been sent to this device.' Message is displayed successfully";
	String FailStatement="FAIL >> 'An invitation to join SureMDM has been sent to this device.' Message is not displayed";
	Initialization.commonmethdpage.ConfirmationMessageVerify(InviteUserMessage, value, PassStatement, FailStatement,3);
}

@FindBy(xpath="//input[@id='phonenixapk']")
private WebElement InviteUserbySMSCheckboxSelected ;

public void InviteUserbySMSCheckboxSelected()throws InterruptedException{
	
	boolean flag=InviteUserbySMSCheckboxSelected.isSelected();
	if(flag==false)
	{	
		sleep(2);
		InviteUserbySMSCheckboxSelected.click();
		sleep(2);		
	}	
	Reporter.log("PASS >> Invite User by SMS Check Box Selected",true);
}









      
      
      
      
      
      
      
      
      
      
      
      
      //Mithilesh=========================================================================================================
}
