package PageObjectRepository;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;

public class UserManagement_POMold extends WebDriverCommonLib{
	
	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	
	@FindBy(id="userManagement")
	private WebElement UserManagement;
	
	@FindBy(xpath="//h4[text()='User Management']")
	private WebElement UserManagementHeader;
	
	@FindBy(id="addUser")
	private WebElement AddUser;
	
	@FindBy(id="editUser")
	private WebElement EditUser;
	
	@FindBy(id="uneditablemessage")
	private WebElement UnEditableMessage;
	
	@FindBy(id="deleteUser")
	private WebElement DeleteUser;
	
	@FindBy(xpath="//h4[text()='Create New User']")
	private WebElement CreateNewUserHeader;
	
	@FindBy(xpath="//h4[text()='Edit User Settings']")
	private WebElement EditUserHeader;
	
	@FindBy(xpath="//span[text()='User Name']")
	private WebElement UserNameField;
	
	@FindBy(xpath="//span[text()='Password']")
	private WebElement PasswordField;
	
	@FindBy(xpath="//span[text()='Confirm Password']")
	private WebElement ConfirmPasswordField;
	
	@FindBy(xpath="//span[text()='First Name']")
	private WebElement FirstNameField;
	
	@FindBy(xpath="//span[text()='Last Name']")
	private WebElement LastNameField;
	
	@FindBy(xpath="//div[@class='userDetail_form']/div/div/span[text()='Email']")
	private WebElement EmailField;
	
	@FindBy(xpath="//span[text()='Phone Number']")
	private WebElement PhoneNumberField;
	
	@FindBy(xpath="//span[text()='Permission Type']")
	private WebElement PermissionTypeField;
	
	@FindBy(id="userid")
	private WebElement UserNameTextBox;
	
	@FindBy(id="password")
	private WebElement PasswordTextBox;
	
	@FindBy(id="confirmpassword")
	private WebElement ConfirmPasswordTextBox;
	
	@FindBy(id="firstname")
	private WebElement FirstNameTextBox;
	
	@FindBy(id="lastname")
	private WebElement LastNameTextBox;
	
	@FindBy(id="email")
	private WebElement EmailTextBox;
	
	@FindBy(id="phone")
	private WebElement PhoneNumberTextBox;
	
	@FindBy(xpath="//span[text()='Enter valid Email Address']")
	private WebElement ErrorMessageEmail;
	
	@FindBy(xpath="//span[contains(text(),'User ID already exists.')]")
	private WebElement ErrorMessageUser;
	
	@FindBy(id="modifypermissions")
	private WebElement UserPermission;
	
	@FindBy(xpath="//h4[text()='User Permissions']")
	private WebElement UserPermissionHeader;
	
	@FindBy(xpath="//span[text()='Role']")
	private WebElement RoleText;
	
	@FindBy(id="permissiontype")
	private WebElement RoleType;
	
   @FindBy(xpath="//div[text()='Rules']")
   private WebElement RulesSection;
   
   @FindBy(xpath="//span[contains(text(),'Hide parent group')]")
   private WebElement HideParentGroupText;
   
   @FindBy(xpath="//label[@for='hide_parent_if_child_not_allowed']")
  	private WebElement HideParentGroupRadioBtn;
   
   @FindBy(xpath="//div[text()='Feature Permissions']")
   private WebElement FeaturePermissionsSection;
   
   @FindBy(xpath="//li[text()='Group Permissions']")
   private WebElement GroupPermssions;
   
   @FindBy(xpath="//li[text()='Group Permissions']/span[3]")
   private WebElement GroupPermssionsCheckbox;
   
   @FindBy(xpath="//li[text()='Device Action Permissions']")
   private WebElement DeviceActionPermissions;
   
   @FindBy(xpath="//li[text()='Device Action Permissions']/span[3]")
   private WebElement DeviceActionPermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='Device Management Permissions']")
   private WebElement DeviceManagementPermissions;
   
   @FindBy(xpath="//li[text()='Device Management Permissions']/span[3]")
   private WebElement DeviceManagementPermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='Application Settings Permissions']")
   private WebElement ApplicationSettingsPermissions;
   
   @FindBy(xpath="//li[text()='Application Settings Permissions']/span[3]")
   private WebElement ApplicationSettingsPermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='Job Permissions']")
   private WebElement JobPermissions;
   
   @FindBy(xpath="//li[text()='Job Permissions']/span[3]")
   private WebElement JobPermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='Report Permissions']")
   private WebElement ReportPermissions;
   
   @FindBy(xpath="//li[text()='Report Permissions']/span[3]")
   private WebElement ReportPermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='User Management Permissions']")
   private WebElement UserManagementPermissions;
   
   @FindBy(xpath="//li[text()='User Management Permissions']/span[3]")
   private WebElement UserManagementPermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='Dashboard Permissions']")
   private WebElement DashboardPermissions;
   
   @FindBy(xpath="//li[text()='Dashboard Permissions']/span[3]")
   private WebElement DashboardPermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='File Store Permissions']")
   private WebElement FileStorePermissions;
   
   @FindBy(xpath="//li[text()='File Store Permissions']/span[3]")
   private WebElement FileStorePermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='Profile Permissions']")
   private WebElement ProfilePermissions;
   
   @FindBy(xpath="//li[text()='Profile Permissions']/span[3]")
   private WebElement ProfilePermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='App Store Permissions']")
   private WebElement AppStorePermissions;
   
   @FindBy(xpath="//li[text()='App Store Permissions']/span[3]")
   private WebElement AppStorePermissionsCheckbox;
   
   @FindBy(xpath="//li[text()='Other Permissions']")
   private WebElement OtherPermissions;
   
   @FindBy(xpath="//li[text()='Other Permissions']/span[3]")
   private WebElement OtherPermissionsCheckbox;
 
   @FindBy(xpath="//div[text()='Allowed Device Groups']")
   private WebElement AllowedDeviceGroupsSection;
   
   @FindBy(xpath="//div[@id='groupsTree']/ul/li[text()='Home']")
   private WebElement AllowedDeviceGroupsHome;
   
   @FindBy(xpath="//div[text()='Allowed Job Folders']")
   private WebElement AllowedJobFoldersSection;
   
   @FindBy(xpath="//div[@id='jobfoldersTree']/ul/li[contains(text(),'Home')]")
   private WebElement AllowedJobFoldersHome;
   
   @FindBy(id="ImportPermissionsBtn")
   private WebElement ImportPermissionsBtn;
   
   @FindBy(id="ExportPermissionsBtn")
   private WebElement ExportPermissionsBtn;
   
   @FindBy(id="okbtn")
   private WebElement UserPermissionOkBtn;
   
   @FindBy(xpath="//button[@id='ExportPermissionsBtn']/following-sibling::button")
   private WebElement UserPermissionOkBtn_UM;
   
   @FindBy(id="cancelbtn")
   private WebElement CreateUserCancelBtn;
   
   @FindBy(xpath="//h4[text()='User Permissions']/preceding-sibling::button")
   private WebElement CancelBtn;
   
   @FindBy(xpath="//h4[text()='User Management']/preceding-sibling::button")
   private WebElement CloseUserManagementBtn;

   @FindBy(xpath="//span[text()='User created successfully.']")
   private WebElement ConfirmationMessageUserCreated;
   
   @FindBy(xpath="//span[text()='Permissions saved successfully.']")
   private WebElement ConfirmationMessagePermissionSaved;
   
   @FindBy(xpath="//span[text()='User modified successfully.']")
   private WebElement ConfirmationMessageUserModified;
   
   @FindBy(xpath="//span[@class='user-nme']")
   private WebElement UserName;
   
   @FindBy(xpath="//span[@class='user-id selectable']/span[2]")
   private WebElement AccountId;
 
   @FindBy(xpath="//span[text()='Access denied. Please contact your administrator.']")
   private WebElement AccessDeniedErrorMessage;
   
   @FindBy(xpath="//h4[text()='Permissions']")
   private WebElement ImportPermissionPopup;
   
   @FindBy(id="upload_permission_error")
   private WebElement ImportPermissionErrorMessage;
   
   @FindBy(xpath="//div[@id='BrowsePermissionsFile']/div/div/div/button[text()='Add']")
   private WebElement ImportPermissionAddButton;
   
   @FindBy(xpath="//div[@id='BrowsePermissionsFile']/div/div/div/button[text()='Cancel']")
   private WebElement ImportPermissionCancelButton;
   
   @FindBy(xpath="//h4[text()='Permissions']/preceding-sibling::button")
   private WebElement ImportPermissionCloseButton;
   
   @FindBy(xpath="//span[text()='Permissions imported successfully.']")
   private WebElement ImportPermissionConfirmationmessage;
   
   @FindBy(id="permissionsFileInputField")
   private WebElement ImportPermissionBrowseFile;
 
   @FindBy(xpath="//td[text()='Account Admin']/preceding-sibling::td/span/input")
   private WebElement SelectAdminAccount;
   
   @FindBy(xpath="//span[text()='You cannot modify Super User permission.']")
   private WebElement EditAdminAccountErrorMessage;
 
   @FindBy(xpath="//div[@class='userMgr_userDetails_wrapper']/div/div/div/input")
   private WebElement SearchUserManagement;
 
   @FindBy(xpath="//p[contains(text(),'Selected User(s) will be deleted')]")
   private WebElement DeleteUserPopUp;
 
   @FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div/button[text()='No']")
   private WebElement DeleteUserNoButton;
   
   @FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div/button[text()='Yes']")
   private WebElement DeleteUserYesButton;
   
   @FindBy(xpath="//span[text()='You cannot delete your own account.']")
   private WebElement DeleteUserAlertmessage;
   
   @FindBy(xpath="//span[text()='User deleted successfully.']")
   private WebElement DeleteUserConfirmationmessage;
   
   public void ClickOnUserManagement() throws InterruptedException
   {
	   UserManagement.click();
	   waitForidPresent("addUser");
	   sleep(3);
	   Reporter.log("Clicked On User Management in Settings, Navigated to User Management Page",true);
   }
   
   public void VerifyOfUserPermissionHeader()
   {
		boolean value = UserPermissionHeader.isDisplayed();
		String PassStatement = "PASS >> 'User Permissions' Header is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'User Permissions' Header is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
   }
   
	public void VerifyOfRoleSection()
   {
		boolean value = RoleText.isDisplayed();
		String PassStatement = "\nPASS >> 'Role' Section is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'Role' Section is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Select sel = new Select(RoleType);
		String[] text = {"Custom","All Permissions","Administrator","Help Desk"};
		List<WebElement> lst = sel.getOptions();
		for (int i = 0; i < lst.size(); i++) {
			String ActualValue = lst.get(i).getText();
			String ExpectedValue = text[i];
			PassStatement = "PASS >> " + ActualValue + " is displayed successfully in Role Dropdown";
			FailStatement = "FAIL >> " + ActualValue + " is not successfully in Role Dropdown";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
       }
	}
   
	public void VerifyOfRulesSection()
	{
		boolean value = RulesSection.isDisplayed();
		String PassStatement = "\nPASS >> 'Rules' Section is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'Rules' Section is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = HideParentGroupText.isDisplayed();
		PassStatement = "PASS >> 'Hide parent group when no access to child groups' text is Displayed succesfully when Clicked on Default User Permission";
		FailStatement = "FAIL >> 'Hide parent group when no access to child groups' text is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = HideParentGroupRadioBtn.isEnabled();
		PassStatement = "PASS >> 'Hide parent group when no access to child groups' RadioButton is Displayed succesfully when Clicked on Default User Permission";
		FailStatement = "FAIL >> 'Hide parent group when no access to child groups' RadioButton is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	}
   
   
	public void VerifyOfFeaturePermission()
	{
		boolean value = FeaturePermissionsSection.isDisplayed();
		String PassStatement = "\nPASS >> 'Feature Permissions' Section is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'Feature Permissions' Section is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		WebElement[] wb = { GroupPermssions, DeviceActionPermissions, DeviceManagementPermissions,
				ApplicationSettingsPermissions, JobPermissions, ReportPermissions, UserManagementPermissions,
				DashboardPermissions, FileStorePermissions, ProfilePermissions, AppStorePermissions, OtherPermissions };
		
		String[] text = { "GroupPermssions", "DeviceActionPermissions", "DeviceManagementPermissions",
				"ApplicationSettingsPermissions", "JobPermissions", "ReportPermissions", "UserManagementPermissions",
				"DashboardPermissions", "FileStorePermissions", "ProfilePermissions", "AppStorePermissions", "OtherPermissions" };
		for( int i = 0; i <= wb.length - 1; i++)
		{
			value = wb[i].isDisplayed();
			PassStatement = "PASS >> '"+text[i]+"' Checkbox is Displayed succesfully under Feature Permission List";
			FailStatement = "FAIL >> '"+text[i]+"' Checkbox is not Displayed under Feature Permission List";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

	}
   
	public void VerifyOfAllowedGroups()
	{
		boolean value = AllowedDeviceGroupsSection.isDisplayed();
		String PassStatement = "\nPASS >> 'Allowed Device Groups' Section is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'Allowed Device Groups' Section is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		AllowedDeviceGroupsSection.click();
		value = AllowedDeviceGroupsHome.isDisplayed();
		PassStatement = "PASS >> 'Home Group' is Displayed succesfully when Clicked on Allowed Device Groups";
		FailStatement = "FAIL >> 'Home Group' is not Displayed when Clicked on Allowed Device Group";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfAllowedJobFolders()
	{
		boolean value = AllowedJobFoldersSection.isDisplayed();
		String PassStatement = "\nPASS >> 'Allowed Job Folders' Section is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'Allowed Job Folders' Section is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		AllowedJobFoldersSection.click();
		value = AllowedJobFoldersHome.isDisplayed();
		PassStatement = "PASS >> 'Home Group' is Displayed succesfully when Clicked on Allowed Job Folders";
		FailStatement = "FAIL >> 'Home Group' is not Displayed when Clicked on Allowed Job Folders";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfImportExportPermission()
	{
		boolean value = ImportPermissionsBtn.isDisplayed();
		String PassStatement = "\nPASS >> 'Import Permissions' Button is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'Import Permissions' Button is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = ExportPermissionsBtn.isDisplayed();
		PassStatement = "PASS >> 'Export Permissions' Button is Displayed succesfully when Clicked on Allowed Job Folders";
		FailStatement = "FAIL >> 'Export Permissions' Button is not Displayed when Clicked on Allowed Job Folders";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void SelectRole(String Role) throws InterruptedException{
		Select sel = new Select(RoleType);
		sel.selectByVisibleText(Role);
		sleep(2);
	}
	
	public void VerifyOfSelectedRole(String Role) throws InterruptedException
	{
		Select sel = new Select(RoleType);
		WebElement wb=sel.getFirstSelectedOption();
		String ActualValue=wb.getText();
		String PassStatement = "PASS >> Default USer Permission is saved successfully with Role as "+Role;
		String FailStatement = "FAIL >> Default USer Permission is not saved with Role as "+Role;
		ALib.AssertEqualsMethod(ActualValue, Role, PassStatement, FailStatement);
		sleep(2);
	}
	
	public void ClickOnOkBtnSSO()
	{
		UserPermissionOkBtn.click();
		Reporter.log("Clicked On Ok Button of User Permission",true);
	}
	
	public void ClickOnOkBtnUserPermission() throws InterruptedException
	{
		UserPermissionOkBtn_UM.click();
		String PassStatement="PASS >> Confirmation Message : 'Permissions saved successfully.' is displayed successfully when a new user created";
		String FailStatement="Fail >> Confirmation Message : 'Permissions saved successfully.' is not displayed when a new user created";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessagePermissionSaved, true, PassStatement, FailStatement,1);
		sleep(3);
		Reporter.log("Clicked On Ok Button of User Permission",true);
	}
	
	public void ClickOnCancelBtnSSO() throws InterruptedException
	{
		CancelBtn.click();
		sleep(3);
		Reporter.log("Navigates to SSO Settings Page When clicked on Cancel Button",true);
	}
	
	public void ClickOnCreateButton() throws InterruptedException
	{
		UserPermissionOkBtn.click();
		Reporter.log("Clicking of Create Button on Create New User Page",true);
	}
	
	public void ClickOnHideParentGroupBtn()
	{
		HideParentGroupRadioBtn.click();
		Reporter.log("Hide parent group when no access to child groups is enabled",true);
	}
	
	public void VerifyOfHideParentGroupBtn()
	{
		boolean value=HideParentGroupRadioBtn.isEnabled();
		String PassStatement = "PASS >> Hide parent group when no access to child groups settings is saved successfully ";
		String FailStatement = "FAIL >> Hide parent group when no access to child groups settings is not saved";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void ClickOnCancelBtnCreateNewUser() throws InterruptedException {
		CreateUserCancelBtn.click();
		waitForidPresent("addUser");
		sleep(3);
		Reporter.log("Navigates to User Mangement Page When clicked on Cancel Button", true);
	}

	public void ClickOnAddUser() throws InterruptedException
	{
		AddUser.click();
		waitForidPresent("userid");
		sleep(2);
		Reporter.log("Navigates to Create New User Page when clicked on Add User",true);
	}
	
	public void VerifyOfUserManagementHeader()
	   {
			boolean value = UserManagementHeader.isDisplayed();
			String PassStatement = "PASS >> 'User Management' Header is Displayed succesfully when Clicked on User Management in Settings";
			String FailStatement = "FAIL >> 'User Management' Header is not Displayed when Clicked on User Management in Settings";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	   }
	
	public void VerifyUserManagementPage()
	{
		boolean value=AddUser.isEnabled();
		String PassStatement="PASS >> Add User Button is displayed succesfully when Clicked on User Management";
		String FailStatement="FAIL >> Add User Button is not displayed when Clicked on User Management";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=EditUser.isEnabled();
		PassStatement="PASS >> Edit User Button is displayed succesfully when Clicked on User Management";
		FailStatement="FAIL >> Edit User Button is not displayed when Clicked on User Management";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=DeleteUser.isEnabled();
		PassStatement="PASS >> Delete User Button is displayed succesfully when Clicked on User Management";
		FailStatement="FAIL >> Delete User Button is not displayed when Clicked on User Management";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		
	}
	
	public void VerifyCreateNewUserPage()
	{
		WebElement[] wb = { UserNameField, PasswordField, ConfirmPasswordField, FirstNameField, LastNameField,
				EmailField, PhoneNumberField, PermissionTypeField };
		String[] ExpectedValue = { "User Name", "Password", "Confirm Password", "First Name", "Last Name",
				"Email", "Phone Number", "Permission Type" };
		for (int i = 0; i <= wb.length - 1; i++) 
		{
			boolean value = wb[i].isDisplayed();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Filed is Displayed succesfully when Clicked on Add User";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Field is not Displayed when Clicked on Add User";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		}
		
	}
	
	public void UserPermissionsMandatorySign() throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		for (int i = 0; i < 5; i++) {
			WebElement wb = Initialization.driver.findElement(By.xpath("//span[ul[li[text()='"+ ELib.getDatafromExcel("Sheet9", i, 1) + "']]]/preceding-sibling::div/span"));
			boolean value=wb.isDisplayed();
			String name = wb.getText();
			String PassStatement="'" + name + "' is a mandatory field shows mandatory text as "+ELib.getDatafromExcel("Sheet9", i, 1)+" when Clicked on Create without entering details";
			String FailStatement="'" + name + "' is a mandatory field doesnot shows mandatory text as "+ELib.getDatafromExcel("Sheet9", i, 1)+" when Clicked on Create without entering details";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}
	
	public void ClickOnCloseUserManagement() throws InterruptedException
	{
		CloseUserManagementBtn.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		Reporter.log("Navigates to Home Page When clicked on Close Button of User Management Popup", true);
	}
	
	public void VerifyOfCreateNewUserHeader()
	   {
			boolean value = CreateNewUserHeader.isDisplayed();
			String PassStatement = "PASS >> 'Create New User' Header is Displayed succesfully when Clicked on Add User";
			String FailStatement = "FAIL >> 'Create New User' Header is not Displayed when Clicked on Add User";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	   }
	
	public void EnterUserName(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		UserNameTextBox.sendKeys(ELib.getDatafromExcel("Sheet9", row, col));
	}
	
	public void EnterPassword(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		PasswordTextBox.sendKeys(ELib.getDatafromExcel("Sheet9", row, col));
	}
	
	public void EnterConfirmPassword(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		ConfirmPasswordTextBox.sendKeys(ELib.getDatafromExcel("Sheet9", row, col));
	}
	
	public void EnterFirstName(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		FirstNameTextBox.sendKeys(ELib.getDatafromExcel("Sheet9", row, col));
	}
	
	public void EnterLastName(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		LastNameTextBox.sendKeys(ELib.getDatafromExcel("Sheet9", row, col));
	}
	
	public void EnterEmail(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EmailTextBox.sendKeys(ELib.getDatafromExcel("Sheet9", row, col));
	}
	
	public void EnterPhoneNumber(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		PhoneNumberTextBox.sendKeys(ELib.getDatafromExcel("Sheet9", row, col));
	}
	
	public void ClearFirstName() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		FirstNameTextBox.clear();
	}
	
	public void ClearLastName() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		LastNameTextBox.clear();
	}
	
	public void ClearEmail() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		EmailTextBox.clear();
	}
	
	public void ClearPhoneNumber() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		PhoneNumberTextBox.clear();
	}
	
	public void MandatoryForPassword() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		boolean value=Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet9", 1, 1)+ "']")).isDisplayed();
		String PassStatement="Mandatory text Shows successfully as "+ELib.getDatafromExcel("Sheet9", 1, 1)+" when Clicked on Create without entering details";
		String FailStatement="Mandatory text is not shown as "+ELib.getDatafromExcel("Sheet9", 1, 1)+" when Clicked on Create without entering details";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String name = Initialization.driver.findElement(By.xpath("//span[ul[li[text()='" + ELib.getDatafromExcel("Sheet9", 1, 1)+ "']]]/preceding-sibling::div/span")).getText();
		PassStatement="'" + name + "' is a mandatory field shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 1, 1)+" when Clicked on Create without entering details";
		FailStatement="'" + name + "' is a mandatory field doesnot shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 1, 1)+" when Clicked on Create without entering details";
		ALib.AssertEqualsMethod(name, "Password", PassStatement, FailStatement);
		}
	
	public void MandatoryForConfirmPassword() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		boolean value=Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet9", 2, 1)+ "']")).isDisplayed();
		String PassStatement="Mandatory text Shows successfully as "+ELib.getDatafromExcel("Sheet9", 2, 1)+" when Clicked on Create without entering details";
		String FailStatement="Mandatory text is not shown as "+ELib.getDatafromExcel("Sheet9", 2, 1)+" when Clicked on Create without entering details";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String name = Initialization.driver.findElement(By.xpath("//span[ul[li[text()='" + ELib.getDatafromExcel("Sheet9", 2, 1)+ "']]]/preceding-sibling::div/span")).getText();
		PassStatement="'" + name + "' is a mandatory field shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 2, 1)+" when Clicked on Create without entering details";
		FailStatement="'" + name + "' is a mandatory field doesnot shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 2, 1)+" when Clicked on Create without entering details";
		ALib.AssertEqualsMethod(name, "Confirm Password", PassStatement, FailStatement);
	}
	
	public void MandatoryForFirstName() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		boolean value=Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet9", 3, 1)+ "']")).isDisplayed();
		String PassStatement="Mandatory text Shows successfully as "+ELib.getDatafromExcel("Sheet9", 3, 1)+" when Clicked on Create without entering details";
		String FailStatement="Mandatory text is not shown as "+ELib.getDatafromExcel("Sheet9", 3, 1)+" when Clicked on Create without entering details";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String name = Initialization.driver.findElement(By.xpath("//span[ul[li[text()='" + ELib.getDatafromExcel("Sheet9", 3, 1)+ "']]]/preceding-sibling::div/span")).getText();
		PassStatement="'" + name + "' is a mandatory field shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 3, 1)+" when Clicked on Create without entering details";
		FailStatement="'" + name + "' is a mandatory field doesnot shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 3, 1)+" when Clicked on Create without entering details";
		ALib.AssertEqualsMethod(name, "First Name", PassStatement, FailStatement);
	}
	
	public void MandatoryForEmail() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		boolean value=Initialization.driver.findElement(By.xpath("//li[text()='" + ELib.getDatafromExcel("Sheet9", 4, 1)+ "']")).isDisplayed();
		String PassStatement="Mandatory text Shows successfully as "+ELib.getDatafromExcel("Sheet9", 4, 1)+" when Clicked on Create without entering details";
		String FailStatement="Mandatory text is not shown as "+ELib.getDatafromExcel("Sheet9", 4, 1)+" when Clicked on Create without entering details";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String name = Initialization.driver.findElement(By.xpath("//span[ul[li[text()='" + ELib.getDatafromExcel("Sheet9", 4, 1)+ "']]]/preceding-sibling::div/span")).getText();
		PassStatement="'" + name + "' is a mandatory field shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 4, 1)+" when Clicked on Create without entering details";
		FailStatement="'" + name + "' is a mandatory field doesnot shows mandatory text as "+ELib.getDatafromExcel("Sheet9", 4, 1)+" when Clicked on Create without entering details";
		ALib.AssertEqualsMethod(name, "Email", PassStatement, FailStatement);
	}
	
	public void ErrorMessageEmail() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'Enter valid Email Address' is displayed successfully when Email ID not valid ";
		String FailStatement="Fail >> Error Message : 'Enter valid Email Address' is not displayed when Email ID not valid";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageEmail, true, PassStatement, FailStatement,3);
		
	}
	
	public void ErrorMessageUser() throws InterruptedException
	{
		String PassStatement="PASS >> Error Message : 'User creation failed. User ID already exists' is displayed successfully when User is already exist";
		String FailStatement="Fail >> Error Message : 'User creation failed. User ID already exists' is not displayed when User is already exist";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageUser, true, PassStatement, FailStatement,1);
		sleep(3);
	}
	
	public void ClickOnUserPermission() throws InterruptedException
	{
		UserPermission.click();
		waitForidPresent("permissiontype");
		sleep(3);
		Reporter.log("Clicking On User Permission in Create New USer Page, Navigates to User Permission Page",true);
	}
	
	public void ClickOnUserPermission_UM() throws InterruptedException
	{
		UserPermission.click();
	}
	
	public void ClickOnCancelBtnUserPermission() throws InterruptedException
	{
		CancelBtn.click();
		waitForidPresent("userid");
		sleep(3);
		Reporter.log("Navigates to User Mangement - Create New User Page When clicked on Cancel Button", true);
	}
	
	public void ConfirmationMessageUserCreated() throws InterruptedException
	{
		String PassStatement="PASS >> Confirmation Message : 'User created successfully.' is displayed successfully when a new user created";
		String FailStatement="Fail >> Confirmation Message : 'User created successfully.' is not displayed when a new user created";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessageUserCreated, true, PassStatement, FailStatement,1);
		sleep(3);
	}
	
	public void ConfirmationMessageUserModified() throws InterruptedException
	{
		String PassStatement="PASS >> Confirmation Message : 'User modified successfully.' is displayed successfully when a new user edited";
		String FailStatement="Fail >> Confirmation Message : 'User modified successfully.' is not displayed when a new user edited";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessageUserModified, true, PassStatement, FailStatement,1);
		sleep(3);
	}
	
	public void VerifyOfUserCreated(int UsernameRow,int UserNamecol) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		String ActualValue = UserName.getText();
		String ExpectedValue= ELib.getDatafromExcel("Sheet9", UsernameRow, UserNamecol);
		ExpectedValue = ExpectedValue.substring(0, 1).toUpperCase() + ExpectedValue.substring(1);
		String PassStatement="UserName is displayed succesfully for the created user in Settings";
		String FailStatement="UserName is not displayed for the created user in Settings";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		
		ActualValue = AccountId.getText();
		ExpectedValue= Config.AccountId;
		PassStatement="Account Id is displayed succesfully for the created user in Settings";
		FailStatement="Account Id is not displayed for the created user in Settings";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
	}
	
	public void AccessDeniedErrorMessage(String Permission) throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Access denied. Please contact your administrator.' is displayed successfully when "
				+ Permission + " is denied for that user";
		String FailStatement = "Fail >> Alert Message : 'Access denied. Please contact your administrator.' is not displayed when "
				+ Permission + " is denied for that user";
		Initialization.commonmethdpage.ConfirmationMessageVerify(AccessDeniedErrorMessage, true, PassStatement,
				FailStatement, 5);
		
	}

	public void GrantMessage(String Permission) throws InterruptedException
	{
		String PassStatement = "PASS >> "+ Permission + " is granted for that user";
		String FailStatement = "Fail >> Alert Message : 'Access denied. Please contact your administrator.' is displayed when "
				+ Permission + " is granted for that user";
		Initialization.commonmethdpage.ConfirmationMessageVerifyFalse(AccessDeniedErrorMessage, false, PassStatement,
				FailStatement, 1);
		sleep(3);
	}
	
	public void CheckAssignGroup(String Permission)
	{
		boolean value = false;
		try {
			Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr/td[@class='DeviceName']/p[text()='" +Config.DeviceName + "']")).click();
		} catch (Throwable t) {
			value = true;
		}
		String PassStatement = "PASS >> Device is not moved when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Device is moved successfully when "+ Permission + " permission is denied for that user";
	   ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	
	}
	
	public void CheckAssignGroup_Grant(String Permission)
	{
		boolean value = false;
		try {
			Initialization.driver.findElement(By.xpath("//table[@id='dataGrid']/tbody/tr/td[@class='DeviceName']/p[text()='" +Config.DeviceName + "']")).click();
		} catch (Throwable t) {
			value = true;
		}
		String PassStatement = "PASS >> Device is moved when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Device is not moved when "+ Permission + " permission is granted for that user";
	   ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	
	}
	
	public void CheckAllowHomeGroup(String Permission)
	{
		boolean value = false;
		try {
			Initialization.groupspage.ClickHomeGroupFirstRow();
		} catch (Throwable t) {
			value = true;
		}
		String PassStatement = "PASS >> Device is not present in Home Group when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Device is present in Home Group when "+ Permission + " permission is denied for that user";
	   ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	
	}
	
	public void CheckAllowHomeGroup_Grant(String Permission)
	{
		boolean value = false;
		try {
			Initialization.groupspage.ClickHomeGroupFirstRow();
		} catch (Throwable t) {
			value = true;
		}
		String PassStatement = "PASS >> Device is present in Home Group when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Device is not present in Home Group when "+ Permission + " permission is granted for that user";
	   ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	
	}
	
	public void CheckAllowNewDeviceGroup(String Permission,String GroupName)
	{
		boolean value = false;
		try {
			Initialization.groupspage.ClickOnGroup(GroupName);
		} catch (Throwable t) {
			value = true;
		}
		String PassStatement = "PASS >> Group is not present which created in othe user when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Group is present which created in othe user when "+ Permission + " permission is denied for that user";
	   ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	
	}
	
	public void CheckAllowNewDeviceGroup_Grant(String Permission,String GroupName)
	{
		boolean value = false;
		try {
			Initialization.groupspage.ClickOnGroup(GroupName);
		} catch (Throwable t) {
			value = true;
		}
		String PassStatement = "PASS >> Group is present which created in othe user when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Group is not present which created in othe user when "+ Permission + " permission is granted for that user";
	   ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	
	}
	
	public void SelectUser(int row,int col) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException 
	{
		Initialization.driver.findElement(By.xpath("//td[text()='" + ELib.getDatafromExcel("Sheet9", row, col)+"']/preceding-sibling::td[1]/span/input")).click();
	    sleep(2);
	}
	
	public void ClickOnEditUser() throws InterruptedException
	{
		EditUser.click();
		waitForidPresent("modifypermissions");
		sleep(7);
	}
	
	public void ClickOnDeleteUser() throws InterruptedException
	{
		DeleteUser.click();
		waitForXpathPresent("//div[@id='ConfirmationDialog']/div/div/div/button[text()='No']");
		sleep(3);
	}
	
	public void refresh() throws InterruptedException
	{
		refesh();
		waitForidPresent("deleteDeviceBtn");
		sleep(5);
		
	}
	
	public void CheckRemoveJobQueue(String Permission) throws InterruptedException
	{
		boolean value = true;
		try {
			Initialization.quickactiontoolbarpage.ClickOnJobQueuePendingFirstrow();
		} catch (Throwable t) {
			value = false;
		}
		if(value==true)
		{
			value=Initialization.quickactiontoolbarpage.verifyOfRemoveJobQueueEnabled();
			String PassStatement = "PASS >> Remove Job Queue is disabled while removing pendig/error jobs when "+ Permission + " permission is denied for that user";
			String FailStatement = "Fail >> Remove Job Queue is not disabled while removing pendig/error jobs when "+ Permission + " permission is denied for that user";
		   ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}else
		{
			Reporter.log("No Pending/Error Jobs found in the list", true);
		}
		Initialization.quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}
	
	public void CheckRemoveJobQueue_Grant(String Permission) throws InterruptedException
	{
		boolean value = true;
		try {
			Initialization.quickactiontoolbarpage.ClickOnJobQueuePendingFirstrow();
		} catch (Throwable t) {
			value = false;
		}
		if(value==true)
		{
			value=Initialization.quickactiontoolbarpage.verifyOfRemoveJobQueueEnabled();
			String PassStatement = "PASS >> Remove Job Queue is not disabled while removing pendig/error jobs when "+ Permission + " permission is granted for that user";
			String FailStatement = "Fail >> Remove Job Queue is disabled while removing pendig/error jobs when "+ Permission + " permission is granted for that user";
		   ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}else
		{
			Reporter.log("No Pending/Error Jobs found in the list", true);
		}
		Initialization.quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}
	
	public void CheckWhiteListButton(String Permission) throws InterruptedException
	{
		boolean value=Initialization.blacklistedpage.VerifyOfWhiteListDeviceBtnEnabled();
		String PassStatement = "PASS >> WhiteList Device button is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> WhiteList Device button is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckBlacklistedDeleteButton(String Permission) throws InterruptedException
	{
		boolean value=Initialization.blacklistedpage.VerifyOfBlacklistDeviceDeleteBtnEnabled();
		String PassStatement = "PASS >> Blacklist Device Delete button is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Blacklist Device Delete button is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckApproveButton(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfApproveDeviceButtonEnabled();
		String PassStatement = "PASS >> Approve Device button is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Approve Device button is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUnapprovedDeleteButton(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfUnapprovedDeviceDeleteButtonEnabled();
		String PassStatement = "PASS >> Unapproved Delete button is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Unapproved Delete button is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAutomaticallyApproveAllDeviceButton(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfAutomaticallyApproveAllDeviceCheckboxEnabled();
		String PassStatement = "PASS >> Automatically Approve All Device button is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Automatically Approve All Device button is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckPreapprovedDeleteButtonButton(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfPreapprovedDeleteButtonEnabled();
		String PassStatement = "PASS >> Preapproved Delete button is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Preapproved Delete button is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckWhiteListButton_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.blacklistedpage.VerifyOfWhiteListDeviceBtnEnabled();
		String PassStatement = "PASS >> WhiteList Device button is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> WhiteList Device button is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckBlacklistedDeleteButton_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.blacklistedpage.VerifyOfBlacklistDeviceDeleteBtnEnabled();
		String PassStatement = "PASS >> Blacklist Delete button is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Blacklist Delete button is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckApproveButton_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfApproveDeviceButtonEnabled();
		String PassStatement = "PASS >> Approve Device button is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Approve Device button is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUnapprovedDeleteButton_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfUnapprovedDeviceDeleteButtonEnabled();
		String PassStatement = "PASS >> Unapproved Delete button is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Unapproved Delete button is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAutomaticallyApproveAllDeviceButton_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfAutomaticallyApproveAllDeviceCheckboxEnabled();
		String PassStatement = "PASS >> Automatically Approve All Device button is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Automatically Approve All Device button is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckPreapprovedDeleteButtonButton_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.UnApproved.VerifyOfPreapprovedDeleteButtonEnabled();
		String PassStatement = "PASS >> Preapproved Delete button is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Preapproved Delete button is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckLockApps(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dynamicjobspage.VerifyOfLockApplicationEnabled();
		String PassStatement = "PASS >> Lock Application Checkbox is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Lock Application Checkbox is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckRunAtStartupApps(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dynamicjobspage.VerifyOfRunAtStartupApplicationEnabled();
		String PassStatement = "PASS >> Run At Startup Checkbox is disabled when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Run At Startup Checkbox is enabled when "+ Permission + " permission is denied for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckLockApps_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dynamicjobspage.VerifyOfLockApplicationEnabled();
		String PassStatement = "PASS >> Lock Application Checkbox is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Lock Application Checkbox is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckRunAtStartupApps_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dynamicjobspage.VerifyOfRunAtStartupApplicationEnabled();
		String PassStatement = "PASS >> Run At Startup Checkbox is disabled when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Run At Startup Checkbox is enabled when "+ Permission + " permission is granted for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAllowNewFolder(String Permission,String FolderName) throws InterruptedException
	{
		boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(FolderName);
		String PassStatement = "PASS >> "+FolderName+" Folder is not present when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> "+FolderName+"Folder is present when "+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAllowNewFolder_Grant(String Permission,String FolderName) throws InterruptedException
	{
		boolean value=Initialization.jobsOnAndroidpage.CheckAllowNewFolder(FolderName);
		String PassStatement = "PASS >> Folder is present when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Folder is not present when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckOnDemandReportsPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.reportsPage.CheckRequestReportBtn();
		String PassStatement = "PASS >> Request Report option is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Request Report option is displayed even when "+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckOnDemandReportsPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.reportsPage.CheckRequestReportBtn();
		String PassStatement = "PASS >> Request Report option is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Request Report option is not displayed even when"+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	    sleep(3);
	}
	
	public void CheckScheduleReportsPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.schedulereportpage.CheckScheduleReportBtn();
		String PassStatement = "PASS >> Schedule New option is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Schedule New option is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckScheduleReportsPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.schedulereportpage.CheckScheduleReportBtn();
		String PassStatement = "PASS >> Schedule New option is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Schedule New option is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public boolean CheckUserManagement(){
		boolean value;
		try{
		UserManagement.isDisplayed();
		value=true;
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}
	
	public void CheckUserManagementPermission(String Permission) throws InterruptedException
	{
		boolean value=CheckUserManagement();
		String PassStatement = "PASS >> User Management is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> User Management is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUserManagementPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=CheckUserManagement();
		String PassStatement = "PASS >> User Management is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> User Management is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckOSPlatformsDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckOSPlatformspopup();
		String PassStatement = "PASS >> OS Platforms DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> OS Platforms DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckOSPlatformsDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckOSPlatformspopup();
		String PassStatement = "PASS >> OS Platforms DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> OS Platforms DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAvailableBatteryDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckAvailableBatterypopup();
		String PassStatement = "PASS >> Available Battery DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Available Battery DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAvailableBatteryDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckAvailableBatterypopup();
		String PassStatement = "PASS >> Available Battery DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Available Battery DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAvailableRAMDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckAvailableRAMpopup();
		String PassStatement = "PASS >> Available RAM DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Available RAM DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAvailableRAMDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckAvailableRAMpopup();
		String PassStatement = "PASS >> Available RAM DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Available RAM DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAvailableStorageDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckAvailableStoragepopup();
		String PassStatement = "PASS >> Available Storage DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Available Storage DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAvailableStorageDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckAvailableStoragepopup();
		String PassStatement = "PASS >> Available Storage DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Available Storage DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckLastConnectedDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckLastConnectedpopup();
		String PassStatement = "PASS >> Last Connected DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Last Connected DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckLastConnectedDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckLastConnectedpopup();
		String PassStatement = "PASS >> Last Connected DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Last Connected DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckOnlineOfflineDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckOnlineOfflinepopup();
		String PassStatement = "PASS >> Online / Offline DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Online / Offline DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckOnlineOfflineDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckOnlineOfflinepopup();
		String PassStatement = "PASS >> Online / Offline DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Online / Offline DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckDeviceSIMStatusDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckDeviceSIMStatuspopup();
		String PassStatement = "PASS >> Device SIM Status DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Device SIM Status DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckDeviceSIMStatusDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckDeviceSIMStatuspopup();
		String PassStatement = "PASS >> Device SIM Status DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Device SIM Status DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckRoamingStatusDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckRoamingStatuspopup();
		String PassStatement = "PASS >> Roaming Status DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Roaming Status DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckRoamingStatusDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckRoamingStatuspopup();
		String PassStatement = "PASS >> Roaming Status DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Roaming Status DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUnreadMailDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckUnreadMailpopup();
		String PassStatement = "PASS >> Unread Mail DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Unread Mail DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUnreadMailDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckUnreadMailpopup();
		String PassStatement = "PASS >> Unread Mail DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Unread Mail DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUnapprovedDevicesDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckUnapprovedDevicespopup();
		String PassStatement = "PASS >> Unapproved Devices DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Unapproved Devices DashBoard is displayed even when "+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUnapprovedDevicesDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckUnapprovedDevicespopup();
		String PassStatement = "PASS >> Unapproved Devices DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Unapproved Devices DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckJobsDashBoardPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckJobspopup();
		String PassStatement = "PASS >> Jobs DashBoard is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Jobs DashBoard is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckJobsDashBoardPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckJobspopup();
		String PassStatement = "PASS >> Jobs DashBoard is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Jobs DashBoard is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckNewFolderFileStorePermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckNewFolder();
		String PassStatement = "PASS >> New Folder Permission of File Store is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> New Folder Permission of File Store is displayed even when"+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckNewFolderFileStorePermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckNewFolder();
		String PassStatement = "PASS >> New Folder Permission of File Store is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> New Folder Permission of File Store is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUploadFilesFileStorePermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckUploadFiles();
		String PassStatement = "PASS >> Upload Files Permission of File Store is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Upload Files Permission of File Store is displayed even when "+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckUploadFilesFileStorePermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckUploadFiles();
		String PassStatement = "PASS >> Upload Files Permission of File Store is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Upload Files Permission of File Store is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckDeleteFolderFileStorePermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckDeleteFolder();
		String PassStatement = "PASS >> Delete Files/Folder Permission of File Store is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Delete Files/Folder Permission of File Store is displayed even when "+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckDeleteFolderFileStorePermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckDeleteFolder();
		String PassStatement = "PASS >> Delete Files/Folder Permission of File Store is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Delete Files/Folder Permission of File Store is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckActivityLogPermission(String Permission) throws InterruptedException
	{
		boolean value=Initialization.activitylogpage.CheckActivityLog();
		String PassStatement = "PASS >> Activity Log is not displayed when "+ Permission + " permission is denied for that user";
		String FailStatement = "Fail >> Activity Log is displayed even when "+ Permission + " permission is denied for that user";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckActivityLogPermission_Grant(String Permission) throws InterruptedException
	{
		boolean value=Initialization.activitylogpage.CheckActivityLog();
		String PassStatement = "PASS >> Activity Log is displayed when "+ Permission + " permission is granted for that user";
		String FailStatement = "Fail >> Activity Log is not displayed even when "+ Permission + " permission is granted for that user";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void AllowedGroupsPermission() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
		int NoOfGroups = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 23, 1));

		for (int i = 0; i < NoOfGroups; i++) 
		{
			Initialization.driver.findElement(By.xpath("//div[@id='groupsTree']/ul/li[text()='"+ ELib.getDatafromExcel("Sheet9", 24+i, 1) + "']/span[4]")).click();
			sleep(2);
		}
	}
 
	public void DenyAllowedGroupsPermission() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
		int NoOfGroups = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 23, 1));
		boolean value;

		for (int i = 0; i < NoOfGroups; i++) 
		{
			try{
			Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul/li[text()='"+ ELib.getDatafromExcel("Sheet9", 24+i, 1) + "']")).click();
			value=true;
			}catch(Exception e)
			{
				value=false;
			}
			String PassStatement = "PASS >> "+ELib.getDatafromExcel("Sheet9", 24+i, 1)+" Group is not displayed when allowed group permission is denied for that user";
			String FailStatement = "Fail >> "+ELib.getDatafromExcel("Sheet9", 24+i, 1)+" Group is displayed even when allowed group permission is denied for that user";
		    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}
	
	public void GrantAllowedGroupsPermission() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
		int NoOfGroups = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 23, 1));
		boolean value;
		for (int i = 0; i < NoOfGroups; i++) 
		{
			try{
				Initialization.driver.findElement(By.xpath("//div[@id='groupstree']/ul/li[text()='"+ ELib.getDatafromExcel("Sheet9", 24+i, 1) + "']")).click();
				sleep(5);
				value=true;
				}catch(Exception e)
				{
					value=false;
				}
				String PassStatement = "PASS >> "+ELib.getDatafromExcel("Sheet9", 24+i, 1)+" Group is displayed when allowed group permission is granted for that user";
				String FailStatement = "Fail >> "+ELib.getDatafromExcel("Sheet9", 24+i, 1)+" Group is not displayed even when allowed group permission is granted for that user";
			    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}
	
	public void AllowedFoldersPermission() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
		int NoOfFolders = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 27, 1));

		for (int i = 0; i < NoOfFolders; i++) 
		{
			Initialization.driver.findElement(By.xpath("//div[@id='jobfoldersTree']/ul/li[text()='"
						+ ELib.getDatafromExcel("Sheet9", 28+i, 1) + "']/span[4]")).click();
			sleep(2);
		}
	}
	
	public void CheckReportsSection_HideParentEnabled() throws InterruptedException
	{
		boolean value=Initialization.schedulereportpage.CheckReportBtn();
		String PassStatement = "PASS >> Reports Section is not visible when Hide parent group when no access to child groups Permission is enabled";
		String FailStatement = "FAIL >> Reports Section is still visible even when Hide parent group when no access to child groups Permission is enabled";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckReportsSection_HideParentDisabled() throws InterruptedException
	{
		boolean value=Initialization.schedulereportpage.CheckReportBtn();
		String PassStatement = "PASS >> Reports Section is visible when Hide parent group when no access to child groups Permission is disabled";
		String FailStatement = "FAIL >> Reports Section is not visible even when Hide parent group when no access to child groups Permission is disabled";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAppStoreSection_HideParentEnabled() throws InterruptedException
	{
		boolean value=Initialization.appstorepage.CheckAppStoreBtn();
		String PassStatement = "PASS >> AppStore Section is not visible when Hide parent group when no access to child groups Permission is enabled";
		String FailStatement = "FAIL >> AppStore Section is still visible even when Hide parent group when no access to child groups Permission is enabled";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckAppStoreSection_HideParentDisabled() throws InterruptedException
	{
		boolean value=Initialization.appstorepage.CheckAppStoreBtn();
		String PassStatement = "PASS >> AppStore Section is visible when Hide parent group when no access to child groups Permission is disabled";
		String FailStatement = "FAIL >> AppStore Section is not visible even when Hide parent group when no access to child groups Permission is disabled";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckFileStoreSection_HideParentEnabled() throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckFileStoreBtn();
		String PassStatement = "PASS >> FileStore Section is not visible when Hide parent group when no access to child groups Permission is enabled";
		String FailStatement = "FAIL >> FileStore Section is still visible even when Hide parent group when no access to child groups Permission is enabled";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckFileStoreSection_HideParentDisabled() throws InterruptedException
	{
		boolean value=Initialization.filestorepage.CheckFileStoreBtn();
		String PassStatement = "PASS >> FileStore Section is visible when Hide parent group when no access to child groups Permission is disabled";
		String FailStatement = "FAIL >> FileStore Section is not visible even when Hide parent group when no access to child groups Permission is disabled";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckDashboardSection_HideParentEnabled() throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckDashBoardBtn();
		String PassStatement = "PASS >> Dashboard Section is not visible when Hide parent group when no access to child groups Permission is enabled";
		String FailStatement = "FAIL >> Dashboard Section is still visible even when Hide parent group when no access to child groups Permission is enabled";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckDashboardSection_HideParentDisabled() throws InterruptedException
	{
		boolean value=Initialization.dashboardpage.CheckDashBoardBtn();
		String PassStatement = "PASS >> Dashboard Section is visible when Hide parent group when no access to child groups Permission is disabled";
		String FailStatement = "FAIL >> Dashboard Section is not visible even when Hide parent group when no access to child groups Permission is disabled";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckProfilesSection_HideParentEnabled() throws InterruptedException
	{
		boolean value=Initialization.profilespage.CheckProfilesBtn();
		String PassStatement = "PASS >> Profiles Section is not visible when Hide parent group when no access to child groups Permission is enabled";
		String FailStatement = "FAIL >> Profiles Section is still visible even when Hide parent group when no access to child groups Permission is enabled";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckProfilesSection_HideParentDisabled() throws InterruptedException
	{
		boolean value=Initialization.profilespage.CheckProfilesBtn();
		String PassStatement = "PASS >> Profiles Section is visible when Hide parent group when no access to child groups Permission is disabled";
		String FailStatement = "FAIL >> Profiles Section is not visible even when Hide parent group when no access to child groups Permission is disabled";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckJobsSection_HideParentEnabled() throws InterruptedException
	{
		boolean value=Initialization.jobsOnAndroidpage.CheckJobsBtn();
		String PassStatement = "PASS >> Jobs Section is not visible when Hide parent group when no access to child groups Permission is enabled";
		String FailStatement = "FAIL >> Jobs Section is still visible even when Hide parent group when no access to child groups Permission is enabled";
	    ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	public void CheckJobsSection_HideParentDisabled() throws InterruptedException
	{
		boolean value=Initialization.jobsOnAndroidpage.CheckJobsBtn();
		String PassStatement = "PASS >> Jobs Section is visible when Hide parent group when no access to child groups Permission is disabled";
		String FailStatement = "FAIL >> Jobs Section is not visible even when Hide parent group when no access to child groups Permission is disabled";
	    ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void ClickOnImportPermission() throws InterruptedException
	{
		ImportPermissionsBtn.click();
		waitForidPresent("permissionsFileInputField");
		sleep(2);
	}
	
	public void CheckImportPemission() throws InterruptedException
	{
		boolean value = ImportPermissionPopup.isDisplayed();
		String PassStatement = "PASS >> 'Permissions' Header is Displayed succesfully when Clicked on Import Permission in User Permission page";
		String FailStatement = "FAIL >> 'Permissions' Header is not Displayed when Clicked on Import Permission in User Permission page";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void ClickOnAddButtonOfImportPermission() throws InterruptedException
	{
	   ImportPermissionAddButton.click();
	}
	
	public void ClickOnCancelButtonOfImportPermission() throws InterruptedException
	{
	   ImportPermissionCancelButton.click();
	   waitForidPresent("ExportPermissionsBtn");
	   sleep(2);
	   Reporter.log("Navigates to USer Permission PAge when clicked on Cancel button Of Import Permission", true);
	 }
	
	public void ClickOnCloseButtonOfImportPermission() throws InterruptedException
	{
		ImportPermissionCloseButton.click();
	   waitForidPresent("ExportPermissionsBtn");
	   sleep(2);
	   Reporter.log("Navigates to USer Permission PAge when clicked on Close button Of Import Permission", true);
	 }
	
	public void ImportPermissionBrowseFile() throws InterruptedException, IOException
	{
		ImportPermissionBrowseFile.click();
		sleep(1);
		Runtime.getRuntime().exec("C:\\Users\\admin\\Documents\\automation related\\fileupload\\Branding Info\\File1.exe");
		sleep(1);
	}
	
	public void ImportExportErrorMessage() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'Please upload permissions file.' is displayed successfully when No file permission is imported";
		String FailStatement = "Fail >> Alert Message : 'Please upload permissions file.' is not displayed when No file permission is imported";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ImportPermissionErrorMessage, true, PassStatement,
				FailStatement, 1);
		sleep(3);
	}
	
	public void ImportExportConfirmationMessage() throws InterruptedException
	{
		String PassStatement = "PASS >> Confirmation Message : 'Permissions imported successfully.' is displayed successfully when Clicked on Add with File permission is imported";
		String FailStatement = "Fail >> Confirmation Message : 'Permissions imported successfully.' is not displayed when Clicked on Add with File permission is imported";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ImportPermissionConfirmationmessage, true, PassStatement,
				FailStatement, 1);
		sleep(3);
	}
	
  
	public void PermissionCheckforImportExport()
	{
		WebElement[] wbEnabled ={GroupPermssionsCheckbox,DeviceManagementPermissionsCheckbox,
				JobPermissionsCheckbox,UserManagementPermissionsCheckbox,FileStorePermissionsCheckbox,AppStorePermissionsCheckbox};
		WebElement[] wbDisabled ={DeviceActionPermissionsCheckbox,ApplicationSettingsPermissionsCheckbox,
				ReportPermissionsCheckbox,DashboardPermissionsCheckbox,ProfilePermissionsCheckbox,OtherPermissionsCheckbox};
		String[] ElementsEnabled={"Group Permssions","DeviceManagementPermissions",
				"JobPermissions","UserManagementPermissions","FileStorePermissions","AppStorePermissions"};
		String[] ElementsDisabled ={"DeviceAction  Permissions","ApplicationSettings  Permissions",
				"Report  Permissions","Dashboard  Permissions","Profile  Permissions","Other  Permissions"};
		for(int i=0;i<wbEnabled.length;i++)
		{
			String Expected=wbEnabled[i].getAttribute("class");
			boolean value = Expected.contains("unchked");
			String PassStatement = "PASS >> Imported Permissions for "+ElementsEnabled[i]+" is saved succesfully when Imported the Permission";
			String FailStatement = "FAIL >> Imported Permissions for "+ElementsEnabled[i]+" is not saved when Imported the Permission";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			
			Expected=wbDisabled[i].getAttribute("class");
			value = Expected.contains("unchked");
			PassStatement = "PASS >> Imported Permissions for "+ElementsDisabled[i]+" is saved succesfully when Imported the Permission";
			FailStatement = "FAIL >> Imported Permissions for "+ElementsDisabled[i]+" is not saved when Imported the Permission";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}
	
	public void PermissionCheckforImportExport_Cancel()
	{
		WebElement[] wbEnabled ={GroupPermssionsCheckbox,DeviceManagementPermissionsCheckbox,
				JobPermissionsCheckbox,UserManagementPermissionsCheckbox,FileStorePermissionsCheckbox,AppStorePermissionsCheckbox,DeviceActionPermissionsCheckbox,ApplicationSettingsPermissionsCheckbox,
				ReportPermissionsCheckbox,DashboardPermissionsCheckbox,ProfilePermissionsCheckbox,OtherPermissionsCheckbox};
		String[] ElementsEnabled={"Group Permssions","DeviceManagementPermissions",
				"JobPermissions","UserManagementPermissions","FileStorePermissions","AppStorePermissions","DeviceAction  Permissions","ApplicationSettings  Permissions",
				"Report  Permissions","Dashboard  Permissions","Profile  Permissions","Other  Permissions"};
		for(int i=0;i<wbEnabled.length;i++)
		{
			String Expected=wbEnabled[i].getAttribute("class");
			boolean value = Expected.contains("unchked");
			String PassStatement = "PASS >> Imported Permissions for "+ElementsEnabled[i]+" is not saved when Clicked on Cancel Button of Import Permission";
			String FailStatement = "FAIL >> Imported Permissions for "+ElementsEnabled[i]+" is saved succesfully when Clicked on Cancel Button of Import Permission";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
			
		}
	}
	
	public void ClickOnExportPermission() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		String downloadPath = ELib.getDatafromExcel("Sheet1", 19, 1);
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith("Permissions") && dir_contents[i].getName().endsWith(".csv")) {
                initial++;
			}
		}
		
		ExportPermissionsBtn.click();
		sleep(10);
		
		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith("Permissions") && dir_contents[i].getName().endsWith(".csv")) {
                result++;
			}
		}
		boolean value=result>initial;
		String PassStatement="PASS >> Permission is Exported successfully in csv Format";
		String FailStatement="Fail >> File is not Exported in csv Format";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	
	public void VerifyOfEditUserHeader()
	   {
			boolean value = EditUserHeader.isDisplayed();
			String PassStatement = "PASS >> 'Edit User Settings' Header is Displayed succesfully when Clicked on Edit User";
			String FailStatement = "FAIL >> 'Edit User Settings' Header is not Displayed when Clicked on Edit User";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	   }
	
	public void VerifyEditUserPage()
	{
		WebElement[] wb = { UserNameField, FirstNameField, LastNameField,
				EmailField, PhoneNumberField, PermissionTypeField };
		String[] ExpectedValue = { "User Name", "First Name", "Last Name",
				"Email", "Phone Number", "Permission Type" };
		for (int i = 0; i <= wb.length - 1; i++) 
		{
			boolean value = wb[i].isDisplayed();
			String PassStatement = "\nPASS >> " + ExpectedValue[i]+ " Filed is Displayed succesfully when Clicked on Edit User";
			String FailStatement = "FAIL >> " + ExpectedValue[i]+ " Field is not Displayed when Clicked on Edit User";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
			Reporter.log("***** PASS >> Verified " + ExpectedValue[i] + "*****", true);
		}
		
	}
	
	public void CheckUneditableMessageForUserName()
	{
		String ActualValue=UnEditableMessage.getText();
		String ExpectedValue="Note: User Name cannot be edited.";
		String PassStatement = "PASS >> 'User Name cannot be edited.' Note is Displayed succesfully when Clicked on Edit User";
		String FailStatement = "FAIL >> 'User Name cannot be edited.' Note is not Displayed when Clicked on Edit User";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
	}
	
	public void CheckFirstName(int row, int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		String ActualValue=FirstNameTextBox.getAttribute("value");
		String ExpectedValue=ELib.getDatafromExcel("Sheet9", row, col);
		String PassStatement = "PASS >> 'First Name' is saved succesfully when user is edited";
		String FailStatement = "FAIL >> 'First Name' is not saved when user is edited";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
	}
	
	public void CheckLastName(int row, int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		String ActualValue=LastNameTextBox.getAttribute("value");
		String ExpectedValue=ELib.getDatafromExcel("Sheet9", row, col);
		String PassStatement = "PASS >> 'Last Name' is saved succesfully when user is edited";
		String FailStatement = "FAIL >> 'Last Name' is not saved when user is edited";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
	}
	
	public void CheckEmail(int row, int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		String ActualValue=EmailTextBox.getAttribute("value");
		String ExpectedValue=ELib.getDatafromExcel("Sheet9", row, col);
		String PassStatement = "PASS >> 'Email' is saved succesfully when user is edited";
		String FailStatement = "FAIL >> 'Email' is not saved when user is edited";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
	}
	
	public void CheckPhoneNumber(int row, int col) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		String ActualValue=PhoneNumberTextBox.getAttribute("value");
		String ExpectedValue=ELib.getDatafromExcel("Sheet9", row, col);
		String PassStatement = "PASS >> 'Phone Number' is saved succesfully when user is edited";
		String FailStatement = "FAIL >> 'Phone Number' is not saved when user is edited";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
	}
	
	public void CheckRole(String Role)
	{
		Select sel = new Select(RoleType);
		String ActualValue=sel.getFirstSelectedOption().getText();
		String PassStatement = "PASS >> 'Role Type' is saved succesfully when a user created "+Role;
		String FailStatement = "FAIL >> 'Role Type' is not saved when a user created "+Role;
		ALib.AssertEqualsMethod(Role, ActualValue,PassStatement, FailStatement);
	}
	
	public void SelectAdminAccount() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		SelectAdminAccount.click();
	}
	
	
	public void EditAdminAccountErrorMessage() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'You cannot modify Super User permission.' is displayed successfully while trying to edit Account Admin";
		String FailStatement = "Fail >> Alert Message : 'You cannot modify Super User permission.' is not displayed while trying to edit Account Admin";
		Initialization.commonmethdpage.ConfirmationMessageVerify(EditAdminAccountErrorMessage, true, PassStatement,
				FailStatement, 1);
	}
	
	public void VerifyEditUser()
	{
		String ExpectedValue=EditUser.getAttribute("class");
		boolean value=ExpectedValue.contains("disabled");
		String PassStatement="PASS >> Edit User Button is grayed out succesfully when Multiple User is selected";
		String FailStatement="FAIL >> Edit User Button is visible even when Multiple User is selected";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void EnterSearchUserManagement(String Search)
	{
		SearchUserManagement.sendKeys(Search);
	}
	
	public void ClearSearchUserManagement()
	{
		SearchUserManagement.clear();
	}
	
	public void VerifyOfSearch_UserName(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//td[text()='"+Search+"']")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" is Searched succesfully in User Management Page when searched with UserName";
		String FailStatement="FAIL >> "+Search+" is not Searched in User Management Page when searched with UserName";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfSearch_CreatedDate(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//td[contains(text(),'"+Search+"')]")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" is Searched succesfully in User Management Page when searched with Created Date";
		String FailStatement="FAIL >> "+Search+" is not Searched in User Management Page when searched with Created Date";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	
	public void VerifyOfSearch_UserType(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//td[text()='"+Search+"']")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" is Searched succesfully in User Management Page when searched with UserType";
		String FailStatement="FAIL >> "+Search+" is not Searched in User Management Page when searched with UserType";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
	
	public void VerifyOfSearch_UserTypeFalse(String Search)
	{
		boolean value;
		try {
			Initialization.driver.findElement(By.xpath("//td[text()='"+Search+"']")).isDisplayed();
			value = true;
		} catch (Exception e) {
			value = false;
		}
		String PassStatement="PASS >> "+Search+" is Searched succesfully in User Management Page when searched with UserType";
		String FailStatement="FAIL >> "+Search+" is not Searched in User Management Page when searched with UserType";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}
	
	
	
	public void VerfiyDeleteUserPopUp()
	{
		String ActualValue=DeleteUserPopUp.getText();
		String ExpectedValue="Selected User(s) will be deleted. Do you wish to continue?";
		String PassStatement = "PASS >> 'Selected User(s) will be deleted. Do you wish to continue?' is displayed successfully when Clicked Delete User";
		String FailStatement = "FAIL >> 'Selected User(s) will be deleted. Do you wish to continue?' is not displayed when Clicked Delete User";
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue,PassStatement, FailStatement);
	}
	
	public void ClickOnDeleteUserNoButton() throws InterruptedException
	{
		DeleteUserNoButton.click();
		waitForidPresent("addUser");
		sleep(2);
	}
	
	public void ClickOnDeleteUserYesButton() throws InterruptedException
	{
		DeleteUserYesButton.click();
		
	}
	
	public void DeleteUserErrorMessage() throws InterruptedException
	{
		String PassStatement = "PASS >> Alert Message : 'You cannot delete your own account.' is displayed successfully when Deleting of own account";
		String FailStatement = "Fail >> Alert Message : 'You cannot delete your own account.' is not displayed when Deleting of own account";
		Initialization.commonmethdpage.ConfirmationMessageVerify(DeleteUserAlertmessage, true, PassStatement,
				FailStatement, 1);
		sleep(3);
	}
	
	public void DeleteUserConfirmationMessage() throws InterruptedException
	{
		String PassStatement = "PASS >> Confirmation Message : 'User deleted successfully.' is displayed successfully when Clicked on Delete User Yes Button";
		String FailStatement = "Fail >> Confirmation Message : 'User deleted successfully.' is not displayed when Clicked on Delete User Yes Button";
		Initialization.commonmethdpage.ConfirmationMessageVerify(DeleteUserConfirmationmessage, true, PassStatement,
				FailStatement, 1);
		sleep(3);
	}
	
	public void ClickOnAllowedDeviceGroups() throws InterruptedException
	{
		AllowedDeviceGroupsSection.click();
		sleep(5);
	}
	
	public void ClickOnAllowedJobFoldersSection() throws InterruptedException
	{
		AllowedJobFoldersSection.click();
		sleep(5);
	}
	
	public void ClickonGroupPermission_Deny() {
		String Expected = GroupPermssionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			GroupPermssionsCheckbox.click();
		}
		Reporter.log("Group Permission is Denied by Unchecking on Group Permission Checkbox", true);
	}

	public void ClickonGroupPermission_Grant() {
		String Expected = GroupPermssionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			GroupPermssionsCheckbox.click();
		}
		Reporter.log("Group Permission is Granted by Checking on Group Permission Checkbox", true);
	}

	public void ClickonDeviceActionPermissions_Deny() {
		String Expected = DeviceActionPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			DeviceActionPermissionsCheckbox.click();
		}
		Reporter.log("Device Action Permission is Denied by Unchecking on Device Action Permission Checkbox", true);
	}

	public void ClickonDeviceActionPermissions_Grant() {
		String Expected = DeviceActionPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			DeviceActionPermissionsCheckbox.click();
		}
		Reporter.log("Device Action Permission is Granted by Checking on Device Action Permission Checkbox", true);
	}

	public void ClickonDeviceManagementPermissions_Deny() {
		String Expected = DeviceManagementPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			DeviceManagementPermissionsCheckbox.click();
		}
		Reporter.log("Device Management Permission is Denied by Unchecking on Device Management Permission Checkbox",
				true);
	}

	public void ClickonDeviceManagementPermissions_Grant() {
		String Expected = DeviceManagementPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			DeviceManagementPermissionsCheckbox.click();
		}
		Reporter.log("Device Management Permission is Granted by Checking on Device Management Permission Checkbox",
				true);
	}

	public void ClickonApplicationSettingsPermissions_Deny() {
		String Expected = ApplicationSettingsPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			ApplicationSettingsPermissionsCheckbox.click();
		}
		Reporter.log(
				"Application Settings Permission is Denied by Unchecking on Application Settings Permission Checkbox",
				true);
	}

	public void ClickonApplicationSettingsPermissions_Grant() {
		String Expected = ApplicationSettingsPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			ApplicationSettingsPermissionsCheckbox.click();
		}
		Reporter.log(
				"Application Settings Permission is Granted by Checking on Application Settings Permission Checkbox",
				true);
	}
	
	public void ClickonJobPermissions_Deny() {
		String Expected = JobPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			JobPermissionsCheckbox.click();
		}
		Reporter.log("Job Permission is Denied by Unchecking on Job Permission Checkbox", true);
	}

	public void ClickonJobPermissions_Grant() {
		String Expected = JobPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			JobPermissionsCheckbox.click();
		}
		Reporter.log("Job Permission is Granted by Checking on Job Permission Checkbox", true);
	}
	
	public void ClickonReportPermissions_Deny() {
		String Expected = ReportPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			ReportPermissionsCheckbox.click();
		}
		Reporter.log("Report Permission is Denied by Unchecking on Report Permission Checkbox", true);
	}

	public void ClickonReportPermissions_Grant() {
		String Expected = ReportPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			ReportPermissionsCheckbox.click();
		}
		Reporter.log("Report Permission is Granted by Checking on Report Permission Checkbox", true);
	}
	
	public void ClickonDashboardPermissions_Deny() {
		String Expected = DashboardPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			DashboardPermissionsCheckbox.click();
		}
		Reporter.log("Dashboard Permission is Denied by Unchecking on Dashboard Permission Checkbox", true);
	}

	public void ClickonDashboardPermissions_Grant() {
		String Expected = DashboardPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			DashboardPermissionsCheckbox.click();
		}
		Reporter.log("Dashboard Permission is Granted by Checking on Dashboard Permission Checkbox", true);
	}
	
	public void ClickonFileStorePermissions_Deny() {
		String Expected = FileStorePermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			FileStorePermissionsCheckbox.click();
		}
		Reporter.log("FileStore Permission is Denied by Unchecking on FileStore Permission Checkbox", true);
	}

	public void ClickonFileStorePermissions_Grant() {
		String Expected = FileStorePermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			FileStorePermissionsCheckbox.click();
		}
		Reporter.log("FileStore Permission is Granted by Checking on FileStore Permission Checkbox", true);
	}
	
	public void ClickonProfilePermissions_Deny() {
		String Expected = ProfilePermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			ProfilePermissionsCheckbox.click();
		}
		Reporter.log("Profile Permission is Denied by Unchecking on Profile Permission Checkbox", true);
	}

	public void ClickonProfilePermissions_Grant() {
		String Expected = ProfilePermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			ProfilePermissionsCheckbox.click();
		}
		Reporter.log("Profile Permission is Granted by Checking on Profile Permission Checkbox", true);
	}
	
	public void ClickonAppStorePermissions_Deny() {
		String Expected = AppStorePermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			AppStorePermissionsCheckbox.click();
		}
		Reporter.log("AppStore Permission is Denied by Unchecking on AppStore Permission Checkbox", true);
	}

	public void ClickonAppStorePermissions_Grant() {
		String Expected = AppStorePermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			AppStorePermissionsCheckbox.click();
		}
		Reporter.log("AppStore Permission is Granted by Checking on AppStore Permission Checkbox", true);
	}
	
	public void ClickonUserManagementPermissions_Deny() {
		String Expected = UserManagementPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			UserManagementPermissionsCheckbox.click();
		}
		Reporter.log("UserManagement Permission is Denied by Unchecking on UserManagement Permission Checkbox", true);
	}

	public void ClickonUserManagementPermissions_Grant() {
		String Expected = UserManagementPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			UserManagementPermissionsCheckbox.click();
		}
		Reporter.log("UserManagement Permission is Granted by Checking on UserManagement Permission Checkbox", true);
	}
	
	public void ClickonOtherPermissions_Deny() {
		String Expected = OtherPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == false) {
			OtherPermissionsCheckbox.click();
		}
		Reporter.log("Other Permission is Denied by Unchecking on Other Permission Checkbox", true);
	}

	public void ClickonOtherPermissions_Grant() {
		String Expected = OtherPermissionsCheckbox.getAttribute("class");
		boolean value = Expected.contains("unchked");
		if (value == true) {
			OtherPermissionsCheckbox.click();
		}
		Reporter.log("Other Permission is Granted by Checking on Other Permission Checkbox", true);
	}
	
	public void AllowedGroupsPermission_Deny() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
	    int NoOfFolders = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 27, 1));
		for (int i = 0; i < NoOfFolders; i++) 
		{
		    WebElement GroupWebElementCheckbox = Initialization.driver.findElement(By.xpath("//div[@id='groupsTree']/ul/li[text()='"+ ELib.getDatafromExcel("Sheet9", 24+i, 1) + "']/span[4]"));
		    String Expected = GroupWebElementCheckbox.getAttribute("class");
			boolean value = Expected.contains("unchked");
		    if (value == false) {
			GroupWebElementCheckbox.click();
		    }
		    Reporter.log(ELib.getDatafromExcel("Sheet9", 28+i, 1)+" Permission is Granted by Checking on "+ELib.getDatafromExcel("Sheet9", 28+i, 1)+" Permission Checkbox", true);
			sleep(2);
		}
	}
	
	public void AllowedGroupsPermission_Grant() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
	    int NoOfFolders = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 27, 1));
		for (int i = 0; i < NoOfFolders; i++) 
		{
		    WebElement GroupWebElementCheckbox = Initialization.driver.findElement(By.xpath("//div[@id='groupsTree']/ul/li[text()='"+ ELib.getDatafromExcel("Sheet9", 24+i, 1) + "']/span[4]"));
		    String Expected = GroupWebElementCheckbox.getAttribute("class");
			boolean value = Expected.contains("unchked");
		    if (value == true) {
			GroupWebElementCheckbox.click();
		    }
		     Reporter.log(ELib.getDatafromExcel("Sheet9", 24+i, 1)+" Permission is Denied by Unchecking on "+ELib.getDatafromExcel("Sheet9", 24+i, 1)+" Permission Checkbox", true);
			sleep(2);
		}
	}
	
	public void AllowedFoldersPermission_Deny() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
	    int NoOfFolders = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 27, 1));
		for (int i = 0; i < NoOfFolders; i++) 
		{
		    WebElement FolderWebElementCheckbox = Initialization.driver.findElement(By.xpath("//div[@id='jobfoldersTree']/ul/li[text()='"
						+ ELib.getDatafromExcel("Sheet9", 28+i, 1) + "']/span[4]"));
		    String Expected = FolderWebElementCheckbox.getAttribute("class");
			boolean value = Expected.contains("unchked");
		    if (value == false) {
			FolderWebElementCheckbox.click();
		    }
		    Reporter.log(ELib.getDatafromExcel("Sheet9", 28+i, 1)+" Permission is Denied by Unchecking on "+ELib.getDatafromExcel("Sheet9", 28+i, 1)+" Permission Checkbox", true);
			sleep(2);
		}
	}
	
	public void AllowedFoldersPermission_Grant() throws NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException 
	{
	    int NoOfFolders = Integer.parseInt(ELib.getDatafromExcel("Sheet9", 27, 1));
		for (int i = 0; i < NoOfFolders; i++) 
		{
		    WebElement FolderWebElementCheckbox = Initialization.driver.findElement(By.xpath("//div[@id='jobfoldersTree']/ul/li[text()='"
						+ ELib.getDatafromExcel("Sheet9", 28+i, 1) + "']/span[4]"));
		    String Expected = FolderWebElementCheckbox.getAttribute("class");
			boolean value = Expected.contains("unchked");
		    if (value == true) {
			FolderWebElementCheckbox.click();
		    }
		    Reporter.log(ELib.getDatafromExcel("Sheet9", 28+i, 1)+" Permission is Granted by Checking on "+ELib.getDatafromExcel("Sheet9", 28+i, 1)+" Permission Checkbox", true);
			sleep(2);
		}
	}
	
	public void VerifyUserManagementPage_HelpDesk()
	{
		String ExpectedValue=AddUser.getAttribute("class");
		boolean value=ExpectedValue.contains("notAllowed");
		String PassStatement="PASS >> Add User Button is grayed out succesfully when Permission of Add User is denied";
		String FailStatement="FAIL >> Add User Button is visible even when Permission of Add User is denied";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		ExpectedValue=EditUser.getAttribute("class");
		value=ExpectedValue.contains("notAllowed");
		PassStatement="PASS >> Edit User Button is grayed out succesfully when Permission of Edit User is denied";
		FailStatement="FAIL >> Edit User Button is visible even when Permission of Edit User is denied";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		ExpectedValue=DeleteUser.getAttribute("class");
		value=ExpectedValue.contains("notAllowed");
		PassStatement="PASS >> Delete User Button is grayed out succesfully when Permission of Delete User is denied";
		FailStatement="FAIL >> Delete User Button is visible even when Permission of Delete User is denied";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}
}
