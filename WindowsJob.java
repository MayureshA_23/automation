package JobsOnAndroid;

import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Library.Config;
import Common.Initialization;

public class WindowsJob extends Initialization{
	
	
//	@Test(priority=1,description="Verify the install or file transfer jobs on Windows") 
//	public void VerifytheinstallorfiletransferjobsonWindowsTC_JO_1060() throws Throwable {
//		Reporter.log("\n1.Verify the install or file transfer jobs on Windows",true);
//		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.clickOnFileTransferJob();
//		androidJOB.enterJobName("Trials File Transfer Job Windows");
//		androidJOB.clickOnAdd();
//		androidJOB.enterFilePathURL(Config.MultipleFileTransferURL);
//		androidJOB.EnterDevicePath("/sdcard/Download");
//		androidJOB.clickOkBtnOnBrowseFileWindow();
//		androidJOB.clickOkBtnOnWindowsJob();
//		androidJOB.JobCreatedMessage(); 
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials File Transfer Job Windows"); 
//		androidJOB.WindowsActivityLog(); 				
//}
//
//	@Test(priority=2,description="Verify the error message when location tracking job is modified and periodicity is given value as 0") 
//	public void VerifytheerrormessagewhenlocationtrackingjobismodifiedandperiodicityisgivenvalueasTC_JO_1059() throws Throwable {
//		Reporter.log("\n2.Verify the error message when location tracking job is modified and periodicity is given value as 0",true);
//		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonLocationTracking();
//		androidJOB.LocationTracking();
//		androidJOB.CloseLocationTrackingTab();						
//}
//	
//	@Test(priority=3,description="Verify the deployment of compliance job") 
//	public void VerifythedeploymentofcompliancejobTC_JO_1058() throws Throwable {
//		Reporter.log("\n3.Verify the deployment of compliance job",true);
//		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();
//		androidJOB.MobileThreatDefenseJobComplianceJob();
//		androidJOB.SelectMobileThreatDefenseJob();
//		androidJOB.SelectAntiVirusProtection();
//		androidJOB.ComplianceJobSavebutton();
//		androidJOB.JobCreatedMessage();		
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trails Mobile Threat Defense Job"); 
//		androidJOB.WindowsActivityLog();		
//}
//	
//	@Test(priority=4,description="Verify UI of MTD in windows compliance Job") 
//	public void VerifyUIofMTDinwindowscomplianceJobTC_JO_1057() throws Throwable {
//		Reporter.log("\n4.Verify UI of MTD in windows compliance Job",true);
//		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();
//		androidJOB.MobileThreatDefenseJobComplianceJob();
//		androidJOB.SelectMobileThreatDefenseJob();
//		androidJOB.AntiVirusProtectionVisible();		
//		androidJOB.AndroidLabelVisible();
//		androidJOB.CloseComplianceJobTab();		
//}
//	
//	@Test(priority=5,description="Verify Selecting Multiple Applications in App Uninstall job") 
//	public void VerifySelectingMultipleApplicationsinAppUninstalljobTC_JO_1056() throws Throwable {
//		Reporter.log("\n5.Verify Selecting Multiple Applications in App Uninstall job",true);
//		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonAppUninstall();
//		androidJOB.AppUninstallJobName();
//		androidJOB.SelectAppUninstall();
//		androidJOB.JobCreatedMessage();		
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trails App Uninstall Job"); 
//		androidJOB.WindowsActivityLog();
//		
//}
//	
//	@Test(priority=6,description="Verify the funtionality of App Uninstall Job on windows") 
//	public void VerifythefuntionalityofAppUninstallJobonwindowsTC_JO_1054() throws Throwable {
//		Reporter.log("\n6.Verify the funtionality of App Uninstall Job on windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonAppUninstall();		
//		androidJOB.funtionalityAppUninstallJobName();
//		androidJOB.SelectAppUninstall();
//		androidJOB.JobCreatedMessage();		
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trails funtionality App Uninstall"); 
//		androidJOB.WindowsActivityLog();	
//}
//	
//	@Test(priority=7,description="Verify Modifying App Uninstall Job in windows") 
//	public void VerifyModifyingAppUninstallJobinwindowsTC_JO_1055() throws Throwable {
//		Reporter.log("\n7.Verify Modifying App Uninstall Job in windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonAppUninstall();
//		androidJOB.ModifiedAppUninstallJobName();
//		androidJOB.SelectAppUninstall();
//		androidJOB.JobCreatedMessage();	
//		androidJOB.SelectTrialsModifiedAppUninstallJob();		
//		androidJOB.JobModifiedMessage();		
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.WindowsModifiedActivityLog();		
//}
	
//	@Test(priority=8,description="Verify the Error message when User searches for an application which is not Present on the list") 
//	public void VerifytheErrormessagewhenUsersearchesforanapplicationwhichisnotPresentonthelistTC_JO_1053() throws Throwable {
//		Reporter.log("\n8.Verify the Error message when User searches for an application which is not Present on the list",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonAppUninstall();		
//		androidJOB.AppUninstallJobName();
//		androidJOB.SearchAppUninstallJobName();
//		androidJOB.SearchMessageValidation();
//		androidJOB.CloseAppUninstallTab();		
//}
	
//	@Test(priority=9,description="Verify Sorting of all the columns Present in App Uninstall Job") 
//	public void VerifysortingofallthecolumnsPresentinAppUninstallJobTC_JO_1052() throws Throwable {
//		Reporter.log("\n9.Verify Sorting of all the columns Present in App Uninstall Job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonAppUninstall();		
//		androidJOB.AppUninstallJobName();
//		androidJOB.ExpectedNumericals();
//		androidJOB.ApplicationName();
//		androidJOB.CloseAppUninstallTab();		
//}
	
	
//	@Test(priority=10,description="Verify the applist shown in App Uninstall Job") 
//	public void VerifytheapplistshowninAppUninstallJobTC_JO_1051() throws Throwable {
//		Reporter.log("\n10.Verify the applist shown in App Uninstall Job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonAppUninstall();		
//		androidJOB.AppUninstallJobName();
//		androidJOB.verifyListInAppUninstall();
//		androidJOB.CloseAppUninstallTab();			
//}
	
//	@Test(priority=11,description="Verify location tracking job Option in windows") 
//	public void VerifylocationtrackingjobOptioninwindowsTC_JO_1032() throws Throwable {
//		Reporter.log("\n11.Verify location tracking job Option in windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.LocationTrackingVisible();
//		androidJOB.closeSelectJobTypeTab();
//}
	
//	@Test(priority=12,description="Verify UI of location tracking job in windows") 
//	public void VerifyUIoflocationtrackingjobinwindowsTC_JO_1033() throws Throwable {
//		Reporter.log("\n12.Verify UI of location tracking job in windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.LocationTrackingVisible();
//		androidJOB.ClickonLocationTracking();
//		androidJOB.verifyOptionsInLocationTracking();
//		androidJOB.CloseLocationTrackingTab();			
//}
	
//	@Test(priority=13,description="Verify that Tracking Periodicity (Min) is getting enabled when Enable Location Tracking is enabled") 
//	public void VerifythatTrackingPeriodicityMinisgettingenabledwhenEnableLocationTrackingisenabledTC_JO_1034() throws Throwable {
//		Reporter.log("\n13.Verify that Tracking Periodicity (Min) is getting enabled when Enable Location Tracking is enabled",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.LocationTrackingVisible();
//		androidJOB.ClickonLocationTracking();		
//		androidJOB.EnableLocationTracking();
//		androidJOB.TrackingPeriodicityEditable();		
//}
	
//	@Test(priority=14,description="Verify that Tracking Periodicity (Min) not enabled when Enable Location Tracking is disabled") 
//	public void VerifythatTrackingPeriodicityMinnotenabledwhenEnableLocationTrackingisdisabledTC_JO_1035() throws Throwable {
//		Reporter.log("\n14.Verify that Tracking Periodicity (Min) not enabled when Enable Location Tracking is disabled",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.LocationTrackingVisible();
//		androidJOB.ClickonLocationTracking();		
//		androidJOB.DisableLocationTracking();
//		androidJOB.TrackingPeriodicityNONEditable();
//		androidJOB.CloseLocationTrackingTab();
//}
	
//	@Test(priority=15,description="Location tracking - verify creating and deploying location tracking job.") 
//	public void LocationtrackingverifycreatinganddeployinglocationtrackingjobTC_JO_1036() throws Throwable {
//		Reporter.log("\n15.Location tracking - verify creating and deploying location tracking job.",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonLocationTracking();
//		androidJOB.LocationTrackingJOBCreated();
//		androidJOB.JobCreatedMessage();		
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trails Location Tracking Enabled"); 
//		androidJOB.WindowsActivityLog();		
//}
//	
//	@Test(priority=16,description="Location Tracking - Verify disabling location tracking") 
//	public void LocationTrackingVerifydisablinglocationtrackingTC_JO_1037() throws Throwable {
//		Reporter.log("\n16.Location Tracking - Verify disabling location tracking",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonLocationTracking();
//		androidJOB.LocationTrackingJOBCreatedDisabled();
//		androidJOB.JobCreatedMessage();		
//		commonmethdpage.ClickOnHomePage();		
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceName);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trails Location Tracking Disabled"); 
//		androidJOB.WindowsActivityLog();		
//}
	
//	@Test(priority=17,description="Composite Job - Verify jobs added in Composite job deploying one after the other") 
//	public void CompositeJobVerifyjobsaddedinCompositejobdeployingoneaftertheotherTC_JO_890() throws Throwable {
//		Reporter.log("\n17.Composite Job - Verify jobs added in Composite job deploying one after the other",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();		
//		androidJOB.ClickOnCompositeJob();
//		androidJOB.CompositeJobName("Multiple Job Deploy");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeMultipleJob("Composite Job A");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeMultipleJob("Composite Job B");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeMultipleJob("Composite Job C");
//		androidJOB.ClickonCompositeJobOKButton();
//		androidJOB.JobCreatedMessage();	
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceNameCompositejob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Multiple Job Deploy"); 
//		androidJOB.CompositeJobActivityLog();
//}
	
//	@Test(priority=18,description="Verify multiple composite job sequence on windows") 
//	public void VerifymultiplecompositejobsequenceonwindowsTC_JO_918() throws Throwable {
//		Reporter.log("\n18.Verify multiple composite job sequence on windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();		
//		androidJOB.ClickOnCompositeJob();
//		androidJOB.CompositeJobName("Multiple Job Deploy Sequence");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeMultipleJob("Composite Job A");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeMultipleJob("Composite Job B");
//		androidJOB.ClickOnAddButtonCompositeJob();
//		androidJOB.SearchJobForCompositeMultipleJob("Composite Job C");
//		androidJOB.ClickonCompositeJobOKButton();
//		androidJOB.JobCreatedMessage();	
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceNameCompositejob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Multiple Job Deploy Sequence"); 
//		androidJOB.CompositeJobActivityLog();
//}
	
//	@Test(priority=19,description="Verify turning on of Location tracking on windows") 
//	public void VerifyturningonofLocationtrackingonwindowsTC_JO_1018() throws Throwable {
//		Reporter.log("\n19.Verify turning on of Location tracking on windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonRunScript(); 
//		androidJOB.CreateRunScriptJob();
//		androidJOB.JobCreatedMessage();	
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceNameCompositejob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Run Script Start"); 
//		androidJOB.RunScriptActivityLog();		
//}
	
	
//	@Test(priority=20,description="Verify restart of Location tracking on windows") 
//	public void VerifyrestartofLocationtrackingonwindowsTC_JO_1019() throws Throwable {
//		Reporter.log("\n20.Verify restart of Location tracking on windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonRunScript();		
//		androidJOB.CreateRunScriptRestartJob();
//		androidJOB.JobCreatedMessage();	
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceNameCompositejob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Run Script Restart"); 
//		androidJOB.RunScriptActivityLog();		
//}
	
//	@Test(priority=20,description="Verify Stop of Location tracking on windows") 
//	public void VerifyStopofLocationtrackingonwindowsTC_JO_1020() throws Throwable {
//		Reporter.log("\n20.Verify Stop of Location tracking on windows",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonRunScript();		
//		androidJOB.CreateRunScriptStopJob();
//		androidJOB.JobCreatedMessage();	
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowDeviceNameCompositejob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Run Script Stop"); 
//		androidJOB.RunScriptActivityLog();		
//}
	
//	@Test(priority=21,description="Verify windows copy genuine validation option") 
//	public void VerifywindowscopygenuinevalidationoptionTC_JO_1024() throws Throwable {
//		Reporter.log("\n21.Verify windows copy genuine validation option",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();
//		androidJOB.WindowsCopyGenuineValidationVisible();
//		androidJOB.CloseComplianceJobTab();		
//}
	
//	@Test(priority=22,description="Verify remove option from windows copy genuine validation rule") 
//	public void VerifyremoveoptionfromwindowscopygenuinevalidationruleTC_JO_1025() throws Throwable {
//		Reporter.log("\n22.Verify remove option from windows copy genuine validation rule",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();		
//		androidJOB.WindowsCopyGenuineValidation();
//		androidJOB.WindowsCopyGenuineValidationRemoveButtonVisible();
//		androidJOB.RemoveButtonTextVisble();	
//		androidJOB.CloseComplianceJobTab();		
//}
	
//	@Test(priority=23,description="Verify the deployment of windows copy genuine validation job") 
//	public void VerifythedeploymentofwindowscopygenuinevalidationjobTC_JO_1026() throws Throwable {
//		Reporter.log("\n23.Verify the deployment of windows copy genuine validation job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();
//		androidJOB.WindowsCopyGenuineJobName("Trials Windows Copy Genuine Validation Job");		
//		androidJOB.WindowsCopyGenuineValidation();
//		androidJOB.WindowsCopyGenuineOutofComplianceOne("Send Message");
//		androidJOB.Delay_OutOfComplianceActions("Immediately");		
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceTwo("E-Mail Notification");
//		androidJOB.Delay_OutOfComplianceActions("Immediately");		
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceThree("Apply Job");
//		androidJOB.Delay_OutOfComplianceActions("Immediately");			
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceFour("Send Sms");
//		androidJOB.Delay_OutOfComplianceActions("Immediately");
//		androidJOB.JobCreatedMessage();	
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowComplianceJob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Windows Copy Genuine Validation Job"); 
//		androidJOB.WindowsCopyGeniusActivityLog();		
//}
	
//	@Test(priority=24,description="Verify the deployment of modified job") 
//	public void VerifythedeploymentofmodifiedjobTC_JO_1027() throws Throwable {
//		Reporter.log("\n24.Verify the deployment of modified job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();
//		
//		androidJOB.WindowsCopyGenuineModifiedJobNameFirst("Trials Windows Copy Genuine Validation Modified Job");		
//		androidJOB.WindowsCopyGenuineValidation();
//		
//		androidJOB.WindowsCopyGenuineOutofComplianceOne("Send Message");
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceTwo("E-Mail Notification");	
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceThree("Apply Job");		
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceFour("Send Sms");
//		androidJOB.JobCreatedMessage();	
//		androidJOB.SearchFieldModifiedJob("Trials Windows Copy Genuine Validation Modified Job");
//		androidJOB.WindowsCopyGenuineModifiedJob();
//		androidJOB.WindowsCopyGenuineModifiedJobName("Trials Windows Copy Genuine Validation Modified Job");
//		androidJOB.JobModifiedMessage();		
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowComplianceJob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Windows Copy Genuine Validation Modified Job"); 
//		androidJOB.WindowsCopyGeniusActivityLog();		
//}
	
//	@Test(priority=25,description="Verify the deployment of older compliance job ") 
//	public void VerifythedeploymentofoldercompliancejobTC_JO_1028() throws Throwable {
//		Reporter.log("\n25.Verify the deployment of older compliance job ",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();
//		
//		androidJOB.WindowsCopyGenuineModifiedJobNameFirst("Trials Windows Copy Genuine Validation Job");		
//		androidJOB.WindowsCopyGenuineValidation();
//		
//		androidJOB.WindowsCopyGenuineOutofComplianceOne("Send Message");
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceTwo("E-Mail Notification");	
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceThree("Apply Job");		
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceFour("Send Sms");
//		androidJOB.JobCreatedMessage();	
//		
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowComplianceJob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Windows Copy Genuine Validation Job"); 
//		androidJOB.WindowsCopyGeniusActivityLog();		
//}
	
	
//	@Test(priority=26,description="Verify the deployment of older compliance job by modifying the compliance job") 
//	public void VerifythedeploymentofoldercompliancejobbymodifyingthecompliancejobTC_JO_1029() throws Throwable {
//		Reporter.log("\n26.Verify the deployment of older compliance job by modifying the compliance job",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();		
//		androidJOB.WindowsCopyGenuineModifiedJobNameFirst("Trials Windows Copy Genuine Validation Modified Job");		
//		androidJOB.WindowsCopyGenuineValidation();		
//		androidJOB.WindowsCopyGenuineOutofComplianceOne("Send Message");
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceTwo("E-Mail Notification");	
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceThree("Apply Job");		
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceFour("Send Sms");
//		androidJOB.JobCreatedMessage();		
//		androidJOB.SearchFieldModifiedJob("Trials Windows Copy Genuine Validation Modified Job");
//		androidJOB.WindowsCopyGenuineModifiedJob();
//		androidJOB.WindowsCopyGenuineModifiedJobName("Trials Windows Copy Genuine Validation Modified Job");
//		androidJOB.JobModifiedMessage();					
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowComplianceJob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Windows Copy Genuine Validation Modified Job"); 
//		androidJOB.WindowsCopyGeniusActivityLog();		
//}
	
//	@Test(priority=27,description="Verify 'bin' option out of compliance rule") 
//	public void VerifybinoptionoutofcomplianceruleTC_JO_1030() throws Throwable {
//		Reporter.log("\n27.Verify 'bin' option of compliance rule",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();		
//		androidJOB.WindowsCopyGenuineModifiedJobNameFirst("Trials Windows Copy Genuine Validation Modified Job");		
//		androidJOB.WindowsCopyGenuineValidation();		
//		androidJOB.WindowsCopyGenuineOutofComplianceOne("Send Message");
//		androidJOB.RemovetheAction();
//		androidJOB.CloseComplianceJobTab();			
//}
	
//	@Test(priority=28,description="Verify add action option for out compliance rules") 
//	public void VerifyaddactionoptionforoutcompliancerulesTC_JO_1031() throws Throwable {
//		Reporter.log("\n28.Verify add action option for out compliance rules",true);		
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickonComplianceJob();		
//		androidJOB.WindowsCopyGenuineModifiedJobNameFirst("Trials Windows Copy Genuine Validation ADD Job");		
//		androidJOB.WindowsCopyGenuineValidation();		
//		androidJOB.WindowsCopyGenuineOutofComplianceOne("Send Message");
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceTwo("E-Mail Notification");	
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceThree("Apply Job");		
//		androidJOB.WindowsCopyGenuineADDButton();
//		androidJOB.WindowsCopyGenuineOutofComplianceFour("Send Sms");
//		androidJOB.JobCreatedMessage();			
//}
	
	
//	@Test(priority=29,description="Verfiy Webhooks is displayed in Notification policy Job in Windows") 
//	public void VerfiyWebhooksisdisplayedinNotificationpolicyJobinWindowsTC_JO_855() throws Throwable {
//		Reporter.log("\n29.Verfiy Webhooks is displayed in Notification policy Job in Windows",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonWebhooksButton();
//		androidJOB.EnableWebHooks();
//		androidJOB.EnterWebHookName(1,"Webhooks",2,Config.WrbhookEndPoint);
//		androidJOB.ApplyWebhookBtn();
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickOnNotificationPolicy();
//		androidJOB.EnableConnectionPolicy();
//		androidJOB.VerifyWebhookOptnInNotificationPolicyDisplayed();
//		androidJOB.CloseNotificationPolicy();
//		commonmethdpage.ClickOnHomePage();
//}
	
//	@Test(priority=30,description="Verify applying notification policy job to windows device by enabling webhooks") 
//	public void VerifyapplyingnotificationpolicyjobtowindowsdevicebyenablingwebhooksTC_JO_856() throws Throwable {
//		Reporter.log("\n30.Verify applying notification policy job to windows device by enabling webhooks",true);
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonWebhooksButton();
//		androidJOB.EnableWebHooks();
//		androidJOB.EnterWebHookName(1,"Webhooks",2,Config.WrbhookEndPoint);
//		androidJOB.ApplyWebhookBtn();
//		androidJOB.clickOnJobs();
//		androidJOB.clickNewJob();		
//		androidJOB.clickOnWindows();
//		androidJOB.ClickOnNotificationPolicy();		
//		androidJOB.NotificationPolicyDetails();
//		accountsettingspage.SendAlertToMDM();
//		accountsettingspage.SendAlertToDevice();
//		androidJOB.WeebhookNotificationPolicy();
//		androidJOB.ClickOnSaveJob();		
//		commonmethdpage.ClickOnHomePage();
//		androidJOB.SearchDeviceInconsole(Config.WindowComplianceJob);
//		commonmethdpage.ClickOnApplyButton();
//		androidJOB.SearchField("Trials Weehbook Job"); 
//		androidJOB.WindowsCopyGeniusActivityLog();	
//}
	
	
	
	
}
