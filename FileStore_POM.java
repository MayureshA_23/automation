package PageObjectRepository;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.ExcelLib;
import Library.WebDriverCommonLib;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class FileStore_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();

	@FindBy(xpath="//li[@id='efssSection']/a[text()='File Store']")
	private WebElement FileStore;

	@FindBy(xpath="//android.widget.TextView[@text='Image.jpg']")
	private WebElement IMG_applied;

	@FindBy(xpath="//div[text()='Shared' and contains(@class,'filename')]")
	private WebElement SharedFolder;

	@FindBy(xpath="//div[text()='Users' and contains(@class,'filename')]")
	private WebElement UsersFolder;

	@FindBy(xpath="//div[@title='Back']")
	private WebElement BackButton;

	@FindBy(xpath="//div[@title='Forward']")
	private WebElement ForwardButton;

	@FindBy(xpath="//div[@title='New folder']")
	private WebElement NewFolder;

	@FindBy(xpath="//div[@title='Upload files']")
	private WebElement UploadFiles;

	@FindBy(xpath="//div[@title='Download']")
	private WebElement DownloadButton;

	@FindBy(xpath="//div[@title='Delete' and contains(@class,'elfinder')]")
	private WebElement DeleteFolder;

	@FindBy(xpath="//div[@title='List view']")
	private WebElement ListviewButton;

	@FindBy(xpath="//div[@title='Icons view']")
	private WebElement IconsviewButton;

	@FindBy(xpath="//div[@title='Sort']")
	private WebElement SortButton;

	@FindBy(xpath="//input[@id='EnableFileStore']")
	private WebElement EnableFileStoreButton;

	@FindBy(xpath="//span[text()='File Store']")
	private WebElement FileStoreOptiondroppable;

	@FindBy(xpath="//span[text()='Shared']")
	private WebElement SharedFolderOptiondroppable;

	@FindBy(xpath="//span[text()='Users']")
	private WebElement UsersFolderOptiondroppable;

	@FindBy(xpath="//div[@data-type='directory']")
	private List<WebElement> NoOfFolders;

	@FindBy(xpath="//div[contains(@title,'untitled folder')]/input")
	private WebElement RenameFolder;

	@FindBy(xpath="//div[@class='elfinder-cwd-wrapper']")
	private WebElement FileStoreSpace;

	@FindBy(xpath="//div[@class='elfinder-path']/a")
	private List<WebElement> FileStorePath;

	@FindBy(xpath="//div[contains(text(),'File named') and contains(text(),'already exists.')]")
	private WebElement FileStoreSameFolder;

	@FindBy(xpath="//span[text()='Close']")
	private WebElement CloseButton;

	@FindBy(xpath="//div[@id='effs_upload_loader']/div/div[text()='Uploading']")
	private WebElement UploadingFiles;

	@FindBy(xpath="//div[@id='elfinder']/div/div[contains(text(),'Are you sure that you want to permanently delete')]")
	private WebElement DeleteFolderPopoup;

	@FindBy(xpath="//div[@id='elfinder']/div/div/div/button/span[text()='Yes']")
	private WebElement DeleteFolderYesButton;

	@FindBy(xpath="//div[@id='elfinder']/div/div/div/button/span[text()='No']")
	private WebElement DeleteFolderNoButton;

	@FindBy(xpath="//td[contains(@class,'name')]")
	private WebElement ListViewName;

	@FindBy(xpath="//td[contains(@class,'date')]")
	private WebElement ListViewDate;

	@FindBy(xpath="//td[contains(@class,'size')]")
	private WebElement ListViewSize;

	@FindBy(xpath="//td[contains(@class,'kind')]")
	private WebElement ListViewType;

	@FindBy(xpath="//div[contains(text(),'Deleted User') and contains(text(),'Data')]")
	private WebElement DeleteUsersDataFolder;

	@FindBy(xpath="//div[text()='by name']")
	private WebElement SortByNameButton;

	@FindBy(xpath="//div[text()='by size']")
	private WebElement SortBysizeButton;

	@FindBy(xpath="//div[text()='by type']")
	private WebElement SortBytypeButton;

	@FindBy(xpath="//div[text()='by date']")
	private WebElement SortBydateButton;

	@FindBy(xpath="//div[text()='Folders first']")
	private WebElement SortByFoldersfirstButton;

	@FindBy(xpath="//span[text()='File store for all the devices [ENABLED]']")
	private WebElement EnableFileStoreMessage;

	@FindBy(xpath="//span[text()='File store for all the devices [DISABLED]']")
	private WebElement DisableFileStoreMessage;

	@FindBy(xpath="//span[text()='You are not allowed to delete users and shared folders']")
	private WebElement DeleteErrorMessage;

	@FindBy(xpath="//div[@class='elfinder-stat-selected']")
	private WebElement SizePath;

	@FindBy(xpath="")
	private WebElement EnableFileStrore;

	public void VerifyEnableFilestore() throws InterruptedException {

		waitForXpathPresent("//input[@id='EnableFileStore']");
		if(EnableFileStoreButton.isSelected()) {
			Reporter.log("FileStore Enabled for all the devices",true);  	
		}else {
			EnableFileStoreButton.click();
		}

	}

	@FindBy(xpath="//span[text()='File Store']")
	private WebElement FileStoreDroppable;
	
	public void ClickOnFileStoreDropdown() throws InterruptedException {
		FileStoreDroppable.click();
		sleep(4);
	}

/*	public void ClickOnFileStore() throws InterruptedException
	{
		FileStore.click();
		while(Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[3]")).isDisplayed())
		{
			sleep(10);
		}

		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		Reporter.log("Clicking On File Store, Navigated to File Store Page",true);
		sleep(15);
	}
*/	public void ClickOnFileStore() throws InterruptedException
	{
		try {
			waitForXpathPresent("//li[@id='efssSection']/a[text()='File Store']");
			sleep(4);
			FileStore.click();
			sleep(25);
			waitForXpathPresent("//div[@title='New folder']");
			sleep(2);
			Reporter.log("PASS>> Clicking On File Store, Navigated to File Store Page", true);
		} catch (Exception e) {
			Initialization.driver.findElement(By.xpath("//li[@class='nav-subgrp actAsSubGrp']/p/span[text()='More']"))
			.click();
			sleep(2);
			FileStore.click();
			sleep(25);
			waitForXpathPresent("//div[@title='New folder']");
			sleep(2);
			Reporter.log("PASS >> Clicking On File Store, Navigated to File Store Page", true);
		}
	}


	@FindBy(xpath="//p/span[text()='More']")
	private WebElement More_BTN_NewDns;
	public void ClickOnMreBtn_NewDns() throws InterruptedException {

		More_BTN_NewDns.click();
		waitForXpathPresent("//li[@id='efssSection']/a[text()='File Store']");
		sleep(4);

	}

	public boolean CheckNewFolder() 
	{
		boolean value=NewFolder.isDisplayed();
		return value;
	}

	public boolean CheckUploadFiles()
	{
		boolean value=UploadFiles.isDisplayed();
		return value;
	}

	public boolean CheckDeleteFolder()
	{
		boolean value=DeleteFolder.isDisplayed();
		return value;
	}

	public void ClickOnSharedFolder_UM() throws InterruptedException
	{
		SharedFolder.click();
		sleep(5);
		Reporter.log("Clicking On Shared Folder",true);
	}

	public boolean CheckFileStoreBtn()
	{
		boolean value = true;
		try{
			FileStore.isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}

	public void VerifyFileStoreButton()
	{
		boolean value=CheckFileStoreBtn();
		String PassStatement = "PASS >> 'File Store' is displayed successfully when logged into SureMDM Console";
		String FailStatement = "Fail >> File Store' as is not displayed even when logged into SureMDM Console";	
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfFileStore() throws InterruptedException
	{
		WebElement[] FileStoreElement={SharedFolder,UsersFolder,BackButton,ForwardButton,NewFolder,UploadFiles,DownloadButton,
				DeleteFolder,ListviewButton,SortButton,EnableFileStoreButton,FileStoreOptiondroppable,SharedFolderOptiondroppable,UsersFolderOptiondroppable};
		String[] ElementsText={"Shared Folder","Users Folder","Back Button","Forward Button","NewFolder Button","Upload Files Button","Download Button",
				"Delete Folder Button","View Button","Sort Button","Enable File Store for all devices button","FileStore option(Right side-droppable)","Shared option(Right side-droppable)","Users option(Right side-droppable)"};
		for(int i=0;i<FileStoreElement.length;i++)
		{
			boolean value=FileStoreElement[i].isDisplayed();
			String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on File Store";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
		sleep(4);
	}

	public void VerifyOfFileStore_Shared()
	{
		WebElement[] FileStoreElement={BackButton,ForwardButton,NewFolder,UploadFiles,DownloadButton,
				DeleteFolder,ListviewButton,SortButton,EnableFileStoreButton,FileStoreOptiondroppable,SharedFolderOptiondroppable,UsersFolderOptiondroppable};
		String[] ElementsText={"Back Button","Forward Button","NewFolder Button","Upload Files Button","Download Button",
				"Delete Folder Button","View Button","Sort Button","Enable File Store for all devices button","FileStore option(Right side-droppable)","Shared option(Right side-droppable)","Users option(Right side-droppable)"};
		for(int i=0;i<FileStoreElement.length;i++)
		{
			boolean value=FileStoreElement[i].isDisplayed();
			String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on File Store";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyOfFileStore_Users()
	{
		WebElement[] FileStoreElement={DownloadButton,ListviewButton,SortButton,EnableFileStoreButton,FileStoreOptiondroppable,SharedFolderOptiondroppable,UsersFolderOptiondroppable,DeleteUsersDataFolder};
		String[] ElementsText={"Download Button","View Button","Sort Button","Enable File Store for all devices button","FileStore option(Right side-droppable)","Shared option(Right side-droppable)","Users option(Right side-droppable)","Deleted USer's Data Folder"};
		for(int i=0;i<FileStoreElement.length;i++)
		{
			boolean value=FileStoreElement[i].isDisplayed();
			String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on File Store";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void ClickOnNewFolder() throws Throwable
	{
		NewFolder.click();
		waitForXpathPresent("//div[@title='New folder']");
		sleep(3);
		Reporter.log("Clicking on New Folder",true);
	}

	public int CountNoOfFolders()
	{
		return NoOfFolders.size();
	}

	public void CreationofNewFolder() throws Throwable
	{
		int initialFoldervalue=CountNoOfFolders();
		ClickOnNewFolder();
		FileStoreSpace.click();
		sleep(5);
		int ActualfinalFoldervalue=CountNoOfFolders();
		int ExpectedFinalFoldervalue=initialFoldervalue+1;
		boolean value=(ActualfinalFoldervalue==ExpectedFinalFoldervalue);
		String PassStatement="PASS >> New folder is created successfully when clicked on NewFolder Button";
		String FailStatement="FAIL >> New folder is not created even when clicked on NewFolder Button";;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void RenameFolder(String FolderName) throws Throwable
	{
		int initialFoldervalue=CountNoOfFolders();
		ClickOnNewFolder();
		sleep(5);
		RenameFolder.sendKeys(FolderName);
		Reporter.log("Renaming New Folder",true);
		sleep(5);
		FileStoreSpace.click();
		sleep(5);
		int ActualfinalFoldervalue=CountNoOfFolders();
		int ExpectedFinalFoldervalue=initialFoldervalue+1;
		boolean value=(ActualfinalFoldervalue==ExpectedFinalFoldervalue);
		String PassStatement="PASS >> New folder is created successfully when clicked on NewFolder Button";
		String FailStatement="FAIL >> New folder is not created even when clicked on NewFolder Button";;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=Initialization.driver.findElement(By.xpath("//div[@title='"+FolderName+"']")).isDisplayed();
		PassStatement="PASS >> Folder is renamed successfully when Newfolder is created";
		FailStatement="FAIL >> Folder is not renamed even when Newfolder is created";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void DoubleClickOperationOnSharedFolder() throws Throwable
	{
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(SharedFolder).doubleClick().perform();
		Reporter.log("Double Click operation on New Folder Created", true);
		sleep(5);
		String ActualValue = VerifyOfPath();
		String ExpectedValue = "File Store/Shared";
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the Shared folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the Shared folder is failed even when double click operation on folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void DoubleClickOperationOnNewFolder(String FolderName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']")).click();
		sleep(5);
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']"));
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Folder).build().perform();
		Reporter.log("Double Click operation on New Folder Created", true);
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[3]")).isDisplayed()){}
		sleep(2);
		String ActualValue = VerifyOfPath();
		String ExpectedValue = "File Store/" + FolderName;
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the folder is failed even when double click operation on folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}


public void DoubleClickOperationOnNewFolder_Nested(String FolderName1,String FolderName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']")).click();
		sleep(5);
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']"));
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Folder).build().perform();
		Reporter.log("Double Click operation on New Folder Created", true);
		sleep(5);
		String ActualValue = VerifyOfPath();
		String ExpectedValue = "File Store/" + FolderName1 + "/" + FolderName;
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the folder is failed even when double click operation on folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(5);
	}

	public void ClickOnBackButton() throws Throwable
	{
		BackButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		FileStoreSpace.click();
		sleep(5);
		Reporter.log("Clicking On Back Button of File Store",true);
	}

	public void RenameFolder_Error(String FolderName) throws Throwable
	{
		ClickOnNewFolder();
		sleep(5);
		RenameFolder.sendKeys(FolderName);
		Reporter.log("Renaming New Folder",true);
		sleep(5);
		FileStoreSpace.click();
		sleep(5);
		boolean value=FileStoreSameFolder.isDisplayed();
		String text=FileStoreSameFolder.getText();
		String PassStatement="PASS >> Error Message is displayed successfully when Creating folders with name already exist as "+text;
		String FailStatement="FAIL >> Error Message is not displayed even when Creating folders with name already exist as "+text;

		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnCloseButton() throws Throwable
	{
		CloseButton.click();
		sleep(5);
		Reporter.log("Clicking On Close Button",true);
	}

	public void ClickOnUploadFile(String FileName) throws Throwable
	{
		UploadFiles.click();
		sleep(8);
		Runtime.getRuntime().exec(FileName);
		while (UploadingFiles.isDisplayed()){}
		while (UploadingFiles.isDisplayed()){}
		while (UploadingFiles.isDisplayed()){}
		int i=10;
		while(i>=10)
		{
			try {
				Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed();
				i=0;
			}catch (Exception e) {
				i++;
				if(i==5500)
				{
					i=0;
				}
			}
		}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		sleep(20);
		Reporter.log("Clicked on Upload Files",true);
	}

	public void DoubleClickOperationOnRenamedFolder(String FolderName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']")).click();
		sleep(5);
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']"));
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Folder).build().perform();
		Reporter.log("Double Click operation on New Folder Created", true);
		sleep(5);
		String ActualValue = VerifyOfPath();
		String ExpectedValue = "File Store/" + FolderName;
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the renamed folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the folder is failed even when double click operation on renamed folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void NamingFolderWhileCreation(String FolderName) throws Throwable
	{   
		sleep(3);
		int initialFoldervalue=CountNoOfFolders();
		ClickOnNewFolder();
		sleep(5);
		RenameFolder.sendKeys(FolderName);
		Reporter.log("Renaming New Folder",true);
		sleep(5);
		FileStoreSpace.click();
		sleep(5);
		int ActualfinalFoldervalue=CountNoOfFolders();
		int ExpectedFinalFoldervalue=initialFoldervalue+1;
		boolean value=(ActualfinalFoldervalue==ExpectedFinalFoldervalue);
		String PassStatement="PASS >> New folder is created successfully when clicked on NewFolder Button";
		String FailStatement="FAIL >> New folder is not created even when clicked on NewFolder Button";;
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=Initialization.driver.findElement(By.xpath("//div[@title='"+FolderName+"']")).isDisplayed();
		PassStatement="PASS >> Folder is created successfully with name";
		FailStatement="FAIL >> Folder is not created successfully with name";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		sleep(2);
	}

	public void DoubleClickOperationOnRenamedFolderSubGroup(String FolderName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']")).click();
		sleep(5);
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']"));
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Folder).build().perform();
		Reporter.log("Double Click operation on New Folder Created", true);
		sleep(5);
	}

	@FindBy(xpath="//*[@id='nav-v1_U2hhcmVkXA2']/span[2]")
	private WebElement SharedFolderClick;

	public void clickonSharedFolder() {
		SharedFolderClick.click();


	}
	public void isProgressBarDisplayed() throws InterruptedException {
		sleep(2);
		boolean progressBar =  Initialization.driverAppium.findElementById("com.nix:id/progressBar").isDisplayed();
		String pass="progressBar button Displayed ";
		String fail="Cancle Button Not Displayed";
		ALib.AssertTrueMethod(progressBar, pass, fail);
		sleep(25);
	}
	public void VerifyDownloadbuttonIsNotPresent() throws InterruptedException {
		try {
			if(Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Download']").isDisplayed()) {
				ALib.AssertFailMethod("Download button is not displayed");
			}}catch(Exception e) {
				Reporter.log("Download button is not displayed");
			}
		//Initialization.driverAppium.findElementByXPath("com.nix:id/textViewDownloadSizeDetails[contains(@text,'(100%)')]").click();
		sleep(1);
	}
	public void ClickOnbackButton() throws InterruptedException {
		Initialization.driverAppium.findElementById("com.nix:id/toolbar_Back").click();
	}
	public void SearchFolder(String FolderName) throws InterruptedException {
		Initialization.driverAppium.findElementById("com.nix:id/search_button").click();
		sleep(20);
		Initialization.driverAppium.findElementById("com.nix:id/search_src_text").sendKeys(FolderName);
	}
	public void VerifyFolderPresentFolder(String FolderName) throws Throwable
	{ 
		sleep(2);
		boolean FolderPresent = Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']")).isDisplayed();
		String PassStatement="PASS >> Folder is present";
		String FailStatement="FAIL >>Folder is not present"; 	
		ALib.AssertTrueMethod(FolderPresent, PassStatement, FailStatement);
		sleep(5);
	}
	@FindBy(xpath="//div[contains(text(),'"+Config.DeviceName+"')]")
	private WebElement DeviceFolder;

	public void DoubleClickUserFolder() throws Throwable
	{
		sleep(10);
		JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;		
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		sleep(10);
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(DeviceFolder).doubleClick().perform();
		sleep(10);
	}
	@FindBy(xpath="//span[contains(text(),'Users')]")
	private WebElement UserFolder;
	public void ClickOnUserFolder() throws Throwable
	{sleep(3);
	waitForXpathPresent("//span[contains(text(),'Users')]");
	UserFolder.click();
	}


	public void VerifyOfUploadedFile(String FileName) throws Throwable
	{
		boolean value = true;
		try {
			Initialization.driver.findElement(By.xpath("//div[text()='" + FileName + "']")).isDisplayed();
		} catch (Exception e) {
			Initialization.commonmethdpage.refreshpage();
			ClickOnFileStore();
			try {
				value = Initialization.driver.findElement(By.xpath("//div[text()='" + FileName + "']")).isDisplayed();
			} catch (Exception e1) {
				value = false;
			}
		}
		String PassStatement = "PASS >> " + FileName
				+ " is uploaded successfully when files are uploaded using Upload Files option";
		String FailStatement = "FAIL >> " + FileName
				+ " is not uploaded even when files are uploaded using Upload Files option";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfSize(String FileName, String Size) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[text()='"+FileName+"']")).click();
		String ActualValue=SizePath.getText();
		String ExpectedValue=FileName+", "+Size;
		String PassStatement = "PASS >> Size of the file is displayed correctly when files are uploaded using Upload Files option as "+Size;
		String FailStatement = "FAIL >> Size of the file is not displayed correct when files are uploaded using Upload Files option as "+Size;
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		sleep(3);
	}

	public void ClickOnDelete(String FileName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//div[text()='"+FileName+"']")).click();
		DeleteFolder.click();
		waitForXpathPresent("//div[@id='elfinder']/div/div/div/button/span[text()='Yes']");
		sleep(1);
		Reporter.log("Clicked on Delete button , Navigates to delete popup successfully",true);
	}

	public void VerifyOfDeletePopup(String FileName)
	{
		String ActualValue=DeleteFolderPopoup.getText();
		String ExpectedValue="Are you sure that you want to permanently delete\n"+FileName+"?";
		String Text="Are you sure that you want to permanently delete "+FileName+"?";
		String PassStatement="PASS >> "+Text+" Summary is displayed successfully when clicked on Delete button of File Store";
		String FailStatement="FAIL >> "+Text+" Summary is not displayed even when clicked on Delete button of File Store"; 	
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		boolean value=DeleteFolderYesButton.isDisplayed();
		PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Delete button of File Store";
		FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Delete button of File Store"; 	
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=DeleteFolderNoButton.isDisplayed();
		PassStatement="PASS >> 'No' button is displayed successfully when clicked on Delete button of File Store";
		FailStatement="FAIL >> 'No' button is not displayed even when clicked on Delete button of File Store"; 	
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnDeleteNoButton() throws Throwable
	{
		DeleteFolderNoButton.click();
		waitForXpathPresent("//div[text()='Shared' and contains(@class,'filename')]");
		sleep(2);
		Reporter.log("Clicked on No button of Delete popup of File Store",true);	
	}

	public void ClickOnDeleteYesButton() throws Throwable
	{
		DeleteFolderYesButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='New folder']");
		sleep(2);
		Reporter.log("Clicked on Yes button of Delete popup of File Store",true);	
	}

	public boolean VerifyOfFileOrFolder(String FileName)
	{
		boolean value=true;
		try{
			Initialization.driver.findElement(By.xpath("//div[text()='"+FileName+"']")).isDisplayed();
		}catch(Exception e)
		{
			value=false;
		}
		return value;
	}

	public void VerifyOfDeleteFile_True(String FileName)
	{
		boolean value=VerifyOfFileOrFolder(FileName);
		String PassStatement="PASS >> "+FileName+" is not deleted when Clicked on No Button of Delete Popup";
		String FailStatement="FAIL >> "+FileName+" is deleted even when Clicked on No Button of Delete Popup";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfDeleteFile_False(String FileName)
	{
		boolean value=VerifyOfFileOrFolder(FileName);
		String PassStatement="PASS >> "+FileName+" is deleted successfully when Clicked on Yes Button of Delete Popup";
		String FailStatement="FAIL >> "+FileName+" is not deleted even when Clicked on YEs Button of Delete Popup";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public String VerifyOfPath()
	{
		String FinalPath = "";
		for (int i = 0; i < FileStorePath.size(); i++) {
			String ExpectedValue = FileStorePath.get(i).getText();
			if (i == FileStorePath.size() - 1) {
				FinalPath = FinalPath + ExpectedValue;
			} else {
				FinalPath = FinalPath + ExpectedValue+ "/";
			}
		}
		return FinalPath;
	}

	public void DoubleClickOperationOnUserFolder() throws Throwable
	{
		Actions act = new Actions(Initialization.driver);
		act.moveToElement(UsersFolder).doubleClick().perform();
		Reporter.log("Double Click operation on Users Folder", true);
		sleep(5);
		while(Initialization.driver.findElement(By.xpath("//*[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed()) {}
		sleep(5);
		String ActualValue = VerifyOfPath();
		String ExpectedValue = "File Store/Users";
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the Users folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the Users folder is failed even when double click operation on folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	@FindBy(xpath="//div[text()='vinod']")
	private WebElement devicefolder;


	@FindBy(xpath="//div[@class='ui-helper-clearfix elfinder-cwd ui-selectable ui-droppable elfinder-cwd-view-icons']")
	private WebElement Fileses;

	public void ScrollToDeviceFolder() {
		Fileses.click();
		JavascriptExecutor js=(JavascriptExecutor)Initialization.driver;
		js.executeScript("arguments[0].scrollIntoView();",devicefolder );
		devicefolder.click();
	}


	public void DoubleClickOperationOnDeviceFolder() throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//div[@title='" + Config.DeviceName + "']")).click();
		sleep(5);
		WebElement Folder = Initialization.driver.findElement(By.xpath("//div[@title='" + Config.DeviceName + "']"));
		Actions act = new Actions(Initialization.driver);
		act.doubleClick(Folder).build().perform();
		Reporter.log("Double Click operation on Device Folders", true);
		sleep(5);
		VerifyOfFileStore_Shared();
		String ActualValue = VerifyOfPath();
		System.out.println(ActualValue);
		String ExpectedValue = "File Store/Users/"+Config.DeviceID;
		System.out.println(ExpectedValue);
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully inside the Device folder when double click operation on folder is performed";
		String FailStatement = "FAIL >> Navigation inside the Device folder is failed even when double click operation on folder is performed";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnFileStoreDroppable() throws Throwable
	{

		FileStoreOptiondroppable.click();
		System.out.println("Hello");
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		Reporter.log("Clicking On File Store, Navigated to File Store Page",true);
		sleep(10);
	}
	@FindBy(xpath="//span[text()='File Store']")
	private WebElement File_Store;


	public void ClickOnFile_Store() throws Throwable
	{

		File_Store.click();
		System.out.println("Hello");
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		Reporter.log("Clicking On File Store, Navigated to File Store Page",true);
		sleep(10);
	}

	public void ClickOnFileStoreDroppable_Right() throws Throwable
	{
		if(!(FileStoreOptiondroppable.getAttribute("class").contains("expanded")))
			FileStoreOptiondroppable.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		sleep(10);
	}

	public void VerifyofBackButton()
	{
		String ActualValue=VerifyOfPath();
		String ExpectedValue="File Store";
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully to previous page when Clicked on Back button";
		String FailStatement = "FAIL >> Navigation to previous page is failed even when Clicked on Back button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnForwardButton() throws Throwable
	{
		ForwardButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		FileStoreSpace.click();
		sleep(5);
		Reporter.log("Clicking On Forward Button of File Store",true);
	}

	public void VerifyofForwardButton()
	{
		String ActualValue=VerifyOfPath();
		String ExpectedValue="File Store/Shared";
		boolean value = ActualValue.equals(ExpectedValue);
		String PassStatement = "PASS >> Navigated succesfully to next page when Clicked on Forward button";
		String FailStatement = "FAIL >> Navigation to next page is failed even when Clicked on Forward button";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnListView()
	{
		ListviewButton.click();
	}

	public void VerifyOfListViewColumn()
	{
		WebElement[] FileStoreElement={ListViewName,ListViewDate,ListViewSize,ListViewType};
		String[] ElementsText={ListViewName.getText(),ListViewDate.getText(),ListViewSize.getText(),ListViewType.getText()};
		for(int i=0;i<FileStoreElement.length;i++)
		{
			boolean value=FileStoreElement[i].isDisplayed();
			String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on List View Button of File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on List View Button of File Store";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	public void ClickOnIconView()
	{
		IconsviewButton.click();
	}

	public void VerifyOfIconViewColumn()
	{
		WebElement[] FileStoreElement={ListViewName,ListViewDate,ListViewSize,ListViewType};
		String[] ElementsText={"Name","Modified","Size","Type"};
		for(int i=0;i<FileStoreElement.length;i++)
		{
			boolean value = true;
			try{
				FileStoreElement[i].isDisplayed();
			}catch(Exception e)
			{
				value=false;
			}
			String PassStatement="PASS >> "+ElementsText[i]+" is not displayed when clicked on Icon View Button of File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is displayed even when clicked on Icon View Button of File Store";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}

	public void VerifyOfTooltip()
	{
		WebElement[] FileStoreElement={BackButton,ForwardButton,NewFolder,UploadFiles,DownloadButton,
				DeleteFolder,ListviewButton,SortButton};
		String[] FileStoreTooltip={"Back","Forward","New folder","Upload files","Download",
				"Delete","List view","Sort"};
		String[] ElementsText={"Back Button","Forward Button","NewFolder Button","Upload Files Button","Download Button",
				"Delete Folder Button","View Button","Sort Button"};
		for(int i=0;i<FileStoreElement.length;i++)
		{
			Actions act =new Actions(Initialization.driver);
			act.moveToElement(FileStoreElement[i]).build().perform();
			String ActualValue=FileStoreElement[i].getAttribute("title");
			String ExpectedValue=FileStoreTooltip[i];
			String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on File Store";
			ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		}
	}

	public void CheckDownloads() throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
	{
		String downloadPath = Config.downloadpath;
		String fileName = Config.FileStore_ImageName;
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {
				initial++;
			}
		}

		Initialization.driver.findElement(By.xpath("//div[@title='" + Config.FileStore_ImageName + "']")).click();
		sleep(2);
		DownloadButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		sleep(5);
		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=(result==initial);
		String PassStatement="PASS >> File is Downloaded successfully when clicked on Download button";
		String FailStatement="Fail >> File is not Downloaded even when clicked on Download button";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void MultipleDownload() throws Throwable
	{
		String[] Downloadtext = { Config.FileStore_ImageName, Config.FileStore_DocName };
		String downloadPath = Config.downloadpath;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		int[] initialrequest = { 0, 0 };
		for (int j = 0; j < Downloadtext.length; j++) {
			String fileName = Downloadtext[j];
			String dd = fileName.replace(".", "  ");
			String[] text = dd.split("  ");
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {
					initial++;
				}
			}
			switch (j) {
			case 0:
				initialrequest[0] = initial;
			case 1:
				initialrequest[1] = initial;
			}
		}

		for (int i = 0; i < Downloadtext.length; i++) {
			WebElement wb = Initialization.driver.findElement(By.xpath("//div[@title='" + Downloadtext[i] + "']"));
			sleep(2);
			new Actions(Initialization.driver).keyDown(Keys.CONTROL).click(wb).keyUp(Keys.CONTROL).perform();
		}
		sleep(2);
		DownloadButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		sleep(15);

		for(int j=0;j<Downloadtext.length;j++)
		{
			String fileName = Downloadtext[j];
			String dd= fileName.replace(".", "  ");
			String[] text = dd.split("  ");
			dir = new File(downloadPath);
			dir_contents = dir.listFiles();
			int result=0;
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
					result++;
				}
			}
			switch(j)
			{
			case 0: initialrequest[0]=initial;
			case 1: initialrequest[1]=initial;
			}
			boolean value=(result==initial);
			String PassStatement="PASS >> File is Downloaded successfully when clicked on Download button";
			String FailStatement="Fail >> File is not Downloaded even when clicked on Download button";
			ALib.AssertFalseMethod(value, PassStatement, FailStatement);
		}
	}

	public void ClickOnFolder(String FolderName) throws Throwable
	{
		Initialization.driver.findElement(By.xpath("//div[@title='" + FolderName + "']")).click();
		sleep(5);
	}

	public void VerifyOfDownload_Folder()
	{
		String ActualValue=DownloadButton.getAttribute("class");
		String ExpectedValue="disabled";
		boolean value=ActualValue.contains(ExpectedValue);
		String PassStatement="PASS >> Download option is grayed out successfully when clicked on Folder of File Store";
		String FailStatement="FAIL >> Download option is not grayed out when clicked on Folder of File Store"; 	
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}


	public void ClickOnSharedDroppable() throws Throwable
	{
		sleep(4);
		SharedFolderOptiondroppable.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='New folder']");
		waitForXpathPresent("//div[@title='New folder']");
		Reporter.log("Clicking On Shared, Navigated to Shared Folder Page",true);
		sleep(10);
	}


	public void ClickOnUsersDroppable() throws Throwable
	{
		UsersFolderOptiondroppable.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		waitForXpathPresent("//div[@title='Download']");
		waitForXpathPresent("//div[@title='Download']");
		Reporter.log("Clicking On Users, Navigated to Users Folder Page",true);
		sleep(13);
	}

	public void ClickOnSortButton() throws Throwable
	{
		SortButton.click();
		while(Initialization.driver.findElement(By.xpath("//div[@id='busyIndicatorMailLoad']/div/div/div")).isDisplayed())
		{}
		Reporter.log("Clicking On Sort Button of File Store Page",true);
	}

	public void VerifyOfSort()
	{
		WebElement[] FileStoreElement={SortByNameButton,SortBysizeButton,SortBytypeButton,SortBydateButton,SortByFoldersfirstButton};
		String[] ElementsText={"Sort By Name Button","Sort By Size Button","Sort By Type Button","Sort By Date Button","Sort By Folders First Button"};
		for(int i=0;i<FileStoreElement.length;i++)
		{
			boolean value=FileStoreElement[i].isDisplayed();
			String PassStatement="PASS >> "+ElementsText[i]+" is displayed successfully when clicked on Sort Button Of File Store";
			String FailStatement="FAIL >> "+ElementsText[i]+" is not displayed even when clicked on Sort Button of File Store";
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}

	}

	public void ClickOnEnableFileStore()
	{
		EnableFileStoreButton.click();
	}

	public void EnableFileStoreMessage() throws Throwable
	{
		String PassStatement="PASS >> 'File store for all the devices [ENABLED].' Message is displayed successfully when Clicked on Enable File Store for all devices";
		String FailStatement="FAIL >> 'File store for all the devices [ENABLED]' Message is not displayed even when Clicked on Enable File Store for all devices";
		Initialization.commonmethdpage.ConfirmationMessageVerify(EnableFileStoreMessage, true, PassStatement, FailStatement,4);
	}

	public void DisableFileStoreMessage() throws Throwable
	{
		String PassStatement="PASS >> 'File store for all the devices [DISABLED].' Message is displayed successfully when Clicked on Enable File Store for all devices";
		String FailStatement="FAIL >> 'File store for all the devices [DISABLED]' Message is not displayed even when Clicked on Enable File Store for all devices";
		Initialization.commonmethdpage.ConfirmationMessageVerify(DisableFileStoreMessage, true, PassStatement, FailStatement,4);
	}

	public void MultipleDelete(String[] Downloadtext) throws Throwable
	{
		for (int i = 0; i < Downloadtext.length; i++) {
			WebElement wb = Initialization.driver.findElement(By.xpath("//div[@title='" + Downloadtext[i] + "']"));
			sleep(2);
			new Actions(Initialization.driver).keyDown(Keys.CONTROL).click(wb).keyUp(Keys.CONTROL).perform();
		}
		sleep(2);
		DeleteFolder.click();
		waitForXpathPresent("//div[@id='elfinder']/div/div/div/button/span[text()='Yes']");
		sleep(1);
		Reporter.log("Clicked on Delete button , Navigates to delete popup successfully",true);
	}

	public void VerifyOfDeletePopup_Multiple()
	{
		String ActualValue=DeleteFolderPopoup.getText();
		System.out.println(ActualValue);
		String ExpectedValue="Are you sure that you want to permanently delete\nthese 3 items?";
		System.out.println(ExpectedValue);
		String Text="Are you sure that you want to permanently delete these 3 items?";
		String PassStatement="PASS >> "+Text+" Summary is displayed successfully when clicked on Delete button of File Store";
		String FailStatement="FAIL >> "+Text+" Summary is not displayed even when clicked on Delete button of File Store"; 	
		ALib.AssertEqualsMethod(ExpectedValue, ActualValue, PassStatement, FailStatement);
		boolean value=DeleteFolderYesButton.isDisplayed();
		PassStatement="PASS >> 'Yes' button is displayed successfully when clicked on Delete button of File Store";
		FailStatement="FAIL >> 'Yes' button is not displayed even when clicked on Delete button of File Store"; 	
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value=DeleteFolderNoButton.isDisplayed();
		PassStatement="PASS >> 'No' button is displayed successfully when clicked on Delete button of File Store";
		FailStatement="FAIL >> 'No' button is not displayed even when clicked on Delete button of File Store"; 	
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnSharedFolder() throws Throwable
	{
		SharedFolder.click();
		waitForXpathPresent("//div[@title='New folder']");
		sleep(3);
		Reporter.log("Clicking on Shared Folder",true);
	}

	public void ClickOnUsersFolder() throws Throwable
	{
		UsersFolder.click();
		waitForXpathPresent("//div[@title='New folder']");
		sleep(3);
		Reporter.log("Clicking on Users Folder",true);
	}

	public void ClickOnDelete()
	{
		DeleteFolder.click();
	}

	public void DeleteErrorMessage() throws Throwable
	{
		String PassStatement="PASS >> 'You are not allowed to delete users and shared folders' Message is displayed successfully when Deleting of Shared/Users folders";
		String FailStatement="FAIL >> 'You are not allowed to delete users and shared folders' Message is not displayed even when Deleting of Shared/Users folders";
		Initialization.commonmethdpage.ConfirmationMessageVerify(DeleteErrorMessage, true, PassStatement, FailStatement,4);
	}

	@FindBy(xpath="//div[text()='Video.mp4']")
	private WebElement video;

	public void ClickOnUploadedVideo() throws InterruptedException {
		video.click();
		sleep(2);
	}



	/************************APPIUM PART************************/



	public void LaunchFileStore() throws InterruptedException, IOException    
	{            
		System.out.println("Launching File Store");                 
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.efss.splashscreen.EFSSSplashScreen");// to launch FileStore on Device                       
		sleep(15);                  
	}          

	public void clickOnfolder() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='ReTestFolder']").click();
	}

	public void clickOnDelete() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Delete']").click();
	}

	public void clickOnDeleteOKBUTTON() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK']").click();
		sleep(2);
	}

	public void clickOnCancle() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='CANCEL']").click();
	}

	public void clickOnSelectAllCheck_Box() throws InterruptedException {
		boolean selectcheck_box = Initialization.driverAppium.findElementById("com.nix:id/efss_toolbar_checkbox").isDisplayed();
		String pass="SelectAll CheckBox is present "+selectcheck_box +"";
		String fail="SelectAll CheckBox is present "+selectcheck_box +"";
		ALib.AssertTrueMethod(selectcheck_box, pass, fail);

		Initialization.driverAppium.findElementById("com.nix:id/efss_toolbar_checkbox").click();
		sleep(4);
		boolean selectcheck_box1 = Initialization.driverAppium.findElementById("com.nix:id/efss_toolbar_checkbox").isEnabled();
		String pass1="SelectAll CheckBox is Got Enabled "+selectcheck_box +"";
		String fail1="SelectAll CheckBox is present "+selectcheck_box +"";
		ALib.AssertTrueMethod(selectcheck_box1, pass1, fail1);

	}

	public void clickOnDownloadButton() throws InterruptedException {
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Download']").click();
		sleep(1);
	}

	public void isApkpresent() throws InterruptedException 
	{
		boolean apk = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Game.apk']").isDisplayed();
		String pass="apk is present "+ apk +"";
		String fail="apk is present "+ apk +"";
		ALib.AssertTrueMethod( apk, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Game.apk']").click();

	}
	public void isImagePresent() {
		boolean image = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Image.jpg']").isDisplayed();
		String pass="image is present "+ image +"";
		String fail="image is present "+ image +"";
		ALib.AssertTrueMethod( image, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Image.jpg']").click();

	}

	public void isImagePresentinside() {
		boolean image = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Image.jpg']").isDisplayed();
		String pass="image is present "+ image +"";
		String fail="image is present "+ image +"";
		ALib.AssertTrueMethod( image, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Image.jpg']").click();
	}

	public void isFolderPesent() {
		boolean folder = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='ReTestFolder']").isDisplayed();
		String pass="folder is present "+ folder +"";
		String fail="folder is present "+ folder +"";
		ALib.AssertTrueMethod( folder, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='ReTestFolder']").click();
	}

	public void isNestedFolderPesent() {

		boolean folder = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Config.FileStore_foldername1+"']").isDisplayed();
		String pass="folder is present "+ folder +"";
		String fail="folder is present "+ folder +"";
		ALib.AssertTrueMethod( folder, pass, fail);

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Config.FileStore_foldername1+"']").click();

		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Config.FileStore_foldername2+"']").click();		

	}

	public void isAudioPresent() {
		boolean status=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Audio.mp3']").isDisplayed();
		String pass="AudioPresent is present "+ status +"";
		String fail="AudioPresent is present "+ status +"";
		ALib.AssertTrueMethod( status, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Audio.mp3']").click();

	}

	public void isVideoPresent() throws InterruptedException {
		boolean Video=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Video.mp4']").isDisplayed();
		String pass="VideoPresent is present "+ Video +"";
		String fail="VideoPresent is present "+ Video +"";
		ALib.AssertTrueMethod( Video, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Video.mp4']").click();
	}

	public void IsVideoDowloadingAfterDelete() {
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Video.mp4']").click();
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Download']").click();
	}

	public void isVideoPresentinNested() {
		boolean status=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Video.mp4']").isDisplayed();
		String pass="VideoPresent is present "+ status +"";
		String fail="VideoPresent is present "+ status +"";
		ALib.AssertTrueMethod( status, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Video.mp4']").click();

	}

	public void isDocPresent() {
		boolean doc = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='File.xlsx']").isDisplayed();
		String pass="DocPresent is present "+ doc +"";
		String fail="DocPresentt is present "+ doc +"";
		ALib.AssertTrueMethod( doc, pass, fail);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='File.xlsx']").click();

	}

	public void isHomefolderPresent() 
	{
		System.out.println("Hello");
		boolean home = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Home']").isDisplayed();
		String pass="Homefolder is present "+ home +"";
		String fail="Homefolder is present "+ home +"";
		ALib.AssertTrueMethod( home, pass, fail);

	}

	public void isSharedFolderPresent() {
		boolean status=Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Shared']").isDisplayed();
		String pass="status is present "+ status +"";
		String fail="status is present "+ status +"";
		ALib.AssertTrueMethod( status, pass, fail);

	}

	public void isCancelButtonpresent() throws InterruptedException {

		boolean cancel = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='cancel']").isDisplayed();
		String pass="Cancle button Displayed "+ cancel +"";
		String fail="Cancle Button Not Displayed "+ cancel +"";
		ALib.AssertTrueMethod(cancel, pass, fail);
		sleep(25);

	}

	public void LaunchNix() throws IOException, InterruptedException {
		Runtime.getRuntime().exec("adb shell am start -n com.nix/com.nix.MainFrm"); 
		sleep(4); 
	}
	public void DisableFileStore() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//label[@for='EnableFileStore']")).click();
		sleep(10);
	}
	public void EnableFileStore() throws InterruptedException {
		try {
			Initialization.driver.findElement(By.xpath("//label[@for='EnableFileStore']")).isEnabled();
			Reporter.log("Enable File Store for all device is already Enabled",true);

		}catch (Exception e) {
			Initialization.driver.findElement(By.xpath("//label[@for='EnableFileStore']")).click();
			Reporter.log("Enable File Store for all device is Not Enabled Hence Enabling now",true);
		}
		
	}
	
	public void VerifyOfEnabledFileStoreCheckBoxMsg()
	{
		try {
			Initialization.driver.findElement(By.xpath("//span[contains(text(),'File store for all devices [ENABLED]')]")).isDisplayed();
			Reporter.log("PASS >> Enable File Store for all device is displayed",true);
		}catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Enable File Store for all device isn't displayed");
		}
	}
}