package Settings_TestScripts;
import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;

public class iOSiPadOSmacOSSettings extends Initialization{

	@Test(priority=1,description="Verify Logs when user Modifies a DEP profile") 
	public void VerifyLogswhenuserModifiesaDEPprofileTC_ST_605() throws Throwable {
		Reporter.log("\n1.Verify Logs when user Modifies a DEP profile",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.ClickonDEPSettings();
		accountsettingspage.EditProfile();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DEPmodifiedActivityLogs();		
}
	
	@Test(priority=2,description="Verify Logs when user Deletes a DEP profile") 
	public void VerifyLogswhenuserDeletesaDEPprofileTC_ST_606() throws Throwable {
		Reporter.log("\n2.Verify Logs when user Deletes a DEP profile",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.ClickonDEPSettings();
		accountsettingspage.CreateDeleteProfile();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DEPProfileDeleteActivityLog();	
}
	
	@Test(priority=3,description="Verify Logs when user creates a DEP server") 
	public void VerifyLogswhenusercreatesaDEPserverTC_ST_607() throws Throwable {
		Reporter.log("\n3.Verify Logs when user creates a DEP server",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.ClickonDEPSettings();
		accountsettingspage.CreateDEPServers();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DEPServersActivityLog();
}
	
	@Test(priority=4,description="Verify Logs when user Modifies a DEP server") 
	public void VerifyLogswhenuserModifiesaDEPserverTC_ST_608() throws Throwable {
		Reporter.log("\n4.Verify Logs when user Modifies a DEP server",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.ClickonDEPSettings();
		accountsettingspage.ModifiedDEPServer();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.ModifiedActivityLog();
}
	
	@Test(priority=5,description="Verify Logs when user Delete a DEP server") 
	public void VerifyLogswhenuserDeleteaDEPserverTC_ST_609() throws Throwable {
		Reporter.log("\n5.Verify Logs when user Delete a DEP server",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.ClickonDEPSettings();
		accountsettingspage.CreateDeleteDEPServer();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DEPServerDeletedActivityLog();
}
	
	@Test(priority=6,description="Verify Logs when user Deletes a VPP token") 
	public void VerifyLogswhenuserDeletesaVPPtokenTC_ST_613() throws Throwable {
		Reporter.log("\n6.Verify Logs when user Deletes a VPP token",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.VPP();
		accountsettingspage.ModifyVisible();
		accountsettingspage.BrowserVisible();
		accountsettingspage.browseFile("./Uploads/UplaodFiles/VPP/\\VPPM.exe");
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.VPPTokenDeletedActivityLog();		
}
	
	@Test(priority=7,description="Verify Logs when user makes any changes in Miscellaneous settings") 
	public void VerifyLogswhenusermakesanychangesinMiscellaneoussettingsTC_ST_614() throws Throwable {
		Reporter.log("\n7.Verify Logs when user makes any changes in Miscellaneous settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.IOSMiscellaneous();		
		accountsettingspage.EnableDeviceWipe();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.MiscellaneousModifiedActivityLog();
}
	
	@Test(priority=8,description="Verify whether Recovery Pin text field accepts only 6 digits") 
	public void VerifywhetherRecoveryPintextfieldacceptsonlydigitsTC_ST_531() throws Throwable {
		Reporter.log("\n8.Verify whether Recovery Pin text field accepts only 6 digits",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonIOSSettings();
		accountsettingspage.IOSMiscellaneous();		
		accountsettingspage.EnableDeviceWipeCheckbox();
		accountsettingspage.EnterSIXDigitPin();
}
	
	@Test(priority = 9, description = "Verify the following options are present in iOS/macOS settings")
	public void VerifyOptionsIniOSmacOSMiscelleaneousTC_ST_527() throws InterruptedException {
		Reporter.log("\n9.Verify whether Recovery Pin text field accepts only 6 digits",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.clickOniOSmacTab();
		accountsettingspage.clickOnMiscellaneous();
		accountsettingspage.verifyOptionsInMiscelleaneous();
		commonmethdpage.ClickOnHomePage();
	}
	@Test(priority = 10, description = "Verify If UI Click here to see instructions to renew the existing APNS Certificate.")
	public void VerifyCertificateUITC_ST_744() throws InterruptedException {
		Reporter.log("\n10.Verify If UI Click here to see instructions to renew the existing APNS Certificate.",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.clickOniOSmacTab();
		accountsettingspage.verifyInstructionStmt();
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority = 11, description = "Verify the UI of Certificate Details in iOS/iPad iOS/macOS Settings")
	public void VerifyOptionsInAPNS() throws InterruptedException {
		Reporter.log("\n11.\"Verify the UI of Certificate Details in iOS/iPad iOS/macOS Settings",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.clickOniOSmacTab();
		accountsettingspage.clickOnAPNS();
		accountsettingspage.verifyOptionsInAPNS();
		commonmethdpage.ClickOnHomePage();
	}
	
	@Test(priority = 12, description = "Verify success message by uploading valid push certificate in Upload push certificate page")
	public void VerifysuccessmessagebyuploadingvalidpushcertificateinUploadpushcertificatepageTC_ST_920() throws InterruptedException, IOException {
		Reporter.log("\n12.Verify success message by uploading valid push certificate in Upload push certificate page",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.clickOniOSmacTab();
		accountsettingspage.clickOnAPNS();
		accountsettingspage.UploadpemCertificatebutton();
		accountsettingspage.ClickonbrowserbuttonAPNS();
		accountsettingspage.browseFileWrongAPNS("./Uploads/UplaodFiles/APNS Wrong/\\APNSW.exe");
		accountsettingspage.EnterAppleIDforWrongCertificate();		
	}
	
	@Test(priority = 13, description = "Verify the Upload button without adding  Certificate ")
	public void VerifytheUploadbuttonwithoutaddingCertificate() throws InterruptedException, IOException {
		Reporter.log("\n13.Verify the Upload button without adding  Certificate ",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.clickOniOSmacTab();
		accountsettingspage.clickOnAPNS();
		accountsettingspage.UploadpemCertificatebutton();
		accountsettingspage.EnterAppleIDWithoutCertificate();
		accountsettingspage.CloseAPNSWindow();			
	}
	
	@Test(priority = 14, description = "Verify the Upload button without adding Apple ID")
	public void VerifytheUploadbuttonwithoutaddingAppleID() throws InterruptedException, IOException {
		Reporter.log("\n14.Verify the Upload button without adding Apple ID",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.clickOniOSmacTab();
		accountsettingspage.clickOnAPNS();
		accountsettingspage.UploadpemCertificatebutton();
		accountsettingspage.ClickonbrowserbuttonAPNS();
		accountsettingspage.browseFileAPNS("./Uploads/UplaodFiles/APNS/\\APNS.exe");
		accountsettingspage.EnterCertificateWithoutAppleID();
		accountsettingspage.CloseAPNSWindow();			
	}
	
	
	//vinimanoj
	
	@Test(priority = 15, description = "Verify If UI Click here to see instructions to renew the existing APNS Certificate is present")
	public void VerifyIfUIispresentTC_ST_743() throws InterruptedException {
	Reporter.log("\n15.Verify If UI Click here to see instructions to renew the existing APNS Certificate is present",true);
	commonmethdpage.ClickOnSettings();
	accountsettingspage.AccountSettings();
	accountsettingspage.clickOniOSmacTab();
	accountsettingspage.VerifyTextIsPresentIniOSiPadOSmacOSSettings();
	accountsettingspage.ClickOnIniOSiPadOSmacOSSettingsClickHere();
	accountsettingspage.SwitchToIniOSiPadOSmacOSSettingsToAnotherWindow();
	accountsettingspage.clickHome();
	}
		
	@Test(priority=16,description="Verify the 'Enable Shared iPad' option is added in DEP profiles under account settings")
   	public void VerifytheEnableSharediPadoptionisaddedinDEPprofilesunderAccountsettingsTC_ST_850() throws InterruptedException, Exception {
   		Reporter.log("\n16.Verify the 'Enable Shared iPad' option is added in DEP profiles under account settings",true);
   		commonmethdpage.ClickOnSettings();
   		accountsettingspage.AccountSettings();
   		accountsettingspage.clickOniOSmacTab();
   		accountsettingspage.iOSmacTabDepTab();
   		accountsettingspage.DepProfileAddButton();
   		accountsettingspage.EnableSharediPadRbutton();
   		accountsettingspage.verifyIfSharediPadValueAndValuedisplayed();
   		accountsettingspage.DEEPprofilePupupClosebutton();
   		accountsettingspage.clickHome();
	}
	
	@Test(priority=17,description="Verify the 'Enable Shared iPad' option gest grayed out when Supervision option is disabled TC_ST_851")
   	public void VerifytheEnableSharediPaoptiongestgrayedoutwhenSupervisionOptionisDisabledTC_ST_851() throws InterruptedException, Exception {
   		Reporter.log("\n17.Verify the 'Enable Shared iPad' option gest grayed out when Supervision option is disabled TC_ST_851",true);
   		commonmethdpage.ClickOnSettings();
   		accountsettingspage.AccountSettings();
   		accountsettingspage.clickOniOSmacTab();
   		accountsettingspage.iOSmacTabDepTab();
   		accountsettingspage.DepProfileAddButton();
   		accountsettingspage.SupervisionEnableButton();
   		accountsettingspage.verifyMDMProfileRemovableTextAredisable();
   		accountsettingspage.verifyEnableSharediPadTextAredisable();
   		accountsettingspage.DEEPprofilePupupClosebutton();
   		accountsettingspage.clickHome();
	}
	
	@Test(priority=18,description="Verify Expiration date for VPP token TC_ST_866")
   	public void VerifyExpirationdateforVPPtokenTC_ST_866() throws InterruptedException, Exception {
   		Reporter.log("\n18.Verify Expiration date for VPP token TC_ST_866",true);
   		commonmethdpage.ClickOnSettings();
   		accountsettingspage.AccountSettings();
   		accountsettingspage.clickOniOSmacTab();
   		accountsettingspage.IOSvppButton();
   		accountsettingspage.vppDeleteTokenBtn();
   		accountsettingspage.clickbeowser();
   		accountsettingspage.browserIntegrationImport("./Uploads/UplaodFiles/VPP file/\\VPP.exe"); 
   		accountsettingspage.uploadVppSerTokenBtn();
   		accountsettingspage.uploadVppSucessfulNotficationMessagen();
   		accountsettingspage.VerifyExpirationdateforVPPtoken();
   		accountsettingspage.clickHome();
   		
	}
	
	@Test(priority=19,description="Verify that expiration date is not shown when VPP token is not uploaded TC_ST_867")
   	public void VerifyThatExpirationDateisNotshownWhenVPPtokenisnotuploadedTC_ST_867() throws InterruptedException, Exception {
   		Reporter.log("\n19.Verify that expiration date is not shown when VPP token is not uploaded TC_ST_867",true);
   		commonmethdpage.ClickOnSettings();
   		accountsettingspage.AccountSettings();
   		accountsettingspage.clickOniOSmacTab();
   		accountsettingspage.IOSvppButton();
   		accountsettingspage.checkIsUploadButtonisPresent("./Uploads/UplaodFiles/VPP file/\\VPP.exe");
   		accountsettingspage.vppDeleteTokenButton();
   		accountsettingspage.vppDeleteYesBtn();
   		accountsettingspage.vppDeleteTokenNotification();
   		accountsettingspage.VerifyExpirationdateNotShownAfterdelete();
   		accountsettingspage.clickHome();
	}
	
	@Test(priority=20,description="Verify the UI of \"View device\" in DEP Settings  TC_ST_875")
   	public void VerifytheUIofViewdeviceinDEPSettingsTC_ST_875() throws InterruptedException, Exception {
   		Reporter.log("\n20.Verify the UI of \"View device\" in DEP Settings  TC_ST_875",true);
   		commonmethdpage.ClickOnSettings();
   		accountsettingspage.AccountSettings();
   		accountsettingspage.clickOniOSmacTab();
   		accountsettingspage.iOSmacTabDepTab();
   		accountsettingspage.DEPServers();
   		accountsettingspage.selectViewDevicesFirstdevice();
   		accountsettingspage.ViewDevices();
   		accountsettingspage.diviceListExport();
   		accountsettingspage.deviceListDeviceName();
   		accountsettingspage.deviceListSerialNumber();
   		accountsettingspage.deviceListsearch();
   		accountsettingspage.deviceListpopupCloseBtn();
   		accountsettingspage.clickHome();
	}
	
	@Test(priority=21,description="Verify the error message in View Devices page when there is no device present  TC_ST_877")
   	public void VerifytheErrormessageInViewDevicesPagewhenThereisnoDevicePresentTC_ST_877() throws InterruptedException {
   		Reporter.log("\n21.Verify the error message in View Devices page when there is no device present TC_ST_877",true);
   		commonmethdpage.ClickOnSettings();
   		accountsettingspage.AccountSettings();
   		accountsettingspage.clickOniOSmacTab();
   		accountsettingspage.iOSmacTabDepTab();
   		accountsettingspage.DEPServers();
   		accountsettingspage.selectViewDevicesFirstdevice();
   		accountsettingspage.ViewDevices();
   		accountsettingspage.NoDataavailableNotificationMessage();
   		accountsettingspage.deviceListpopupCloseBtn();
   		accountsettingspage.clickHome();
	}
		
	@Test(priority=22,description="Verify the Export option in View Devices Page TC_ST_878")
   	public void VerifyTheExportOptioninViewDevicesPageTC_ST_878() throws InterruptedException {
   		Reporter.log("\n22.VVerify the Export option in View Devices Page TC_ST_878",true);
   		commonmethdpage.ClickOnSettings();
   		accountsettingspage.AccountSettings();
   		accountsettingspage.clickOniOSmacTab();
   		accountsettingspage.iOSmacTabDepTab();
   		accountsettingspage.DEPServers();
   		accountsettingspage.selectViewDevicesFirstdevice();
   		accountsettingspage.ViewDevices();
   		accountsettingspage.clickDiviceListExport();
   		accountsettingspage.deviceListpopupCloseBtn();
   		accountsettingspage.clickHome();
	}
	
	//mithilesh
	@Test(priority = 23, description = "Verify success message by uploading valid push certificate in Upload push certificate page")
	public void VerifysuccessmessagebyuploadingvalidpushcertificateinUploadpushcertificatepageTC_ST_919() throws InterruptedException, IOException {
		Reporter.log("\n23.Verify success message by uploading valid push certificate in Upload push certificate page",true);
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.clickOniOSmacTab();
		accountsettingspage.clickOnAPNS();
		accountsettingspage.UploadpemCertificatebutton();
		accountsettingspage.ClickonbrowserbuttonAPNS();
		accountsettingspage.browseFileAPNS("./Uploads/UplaodFiles/APNS/\\APNS.exe");
		accountsettingspage.EnterAppleID();		
	}
	
	
	
	
	
}
