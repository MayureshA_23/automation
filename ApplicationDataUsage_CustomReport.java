package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ApplicationDataUsage_CustomReport extends Initialization {

	@Test(priority = '0', description = "Verify Sort by and Group by for Application Datausage")
	public void VerifyingAppDataUsageReport_TC_RE_116() throws InterruptedException {
		Reporter.log("\nVerify Sort by and Group by for Application Datausage", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("AppDataUsage CustomRep with Sortby and Groupby condition", "test");
		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
		customReports.Selectvaluefromsortbydropdown(Config.SelectPackageName);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.DeviceNameValue);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName(Config.EnterAliasNameAsMyDevice);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppDataUsage CustomRep with Sortby and Groupby condition");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("AppDataUsage CustomRep with Sortby and Groupby condition");
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppDataUsage CustomRep with Sortby and Groupby condition");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppDataUsage CustomRep with Sortby and Groupby condition");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '1', description = "Verify generating Application Data Usage Custom report without filters")
	public void VerifyingAppDataUsageReport_TC_RE_117() throws InterruptedException {
		Reporter.log("\nVerify generating Application Data Usage Custom report without filters", true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("AppDataUsage CustomReport without filter",
				"test");
		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppDataUsage CustomReport without filter");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppDataUsage CustomReport without filter");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppDataUsage CustomReport without filter");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingOneWeekDateInAppDataUsageViewReport();
		//accountsettingspage.SearchUsedApplicationORWebSiteInReport(Config.EnterSureMDMNix);
		customReports.VerifyPackageNameInAppDataUsageReport(Config.EnterPackageId);
		customReports.VerifyAppNameInAppDataUsageReport(Config.EnterSureMDMNix);
		accountsettingspage.SearchUsedApplicationORWebSiteInReport(Config.Enter_App1);
		customReports.VerifyPackageNameInAppDataUsageReport("com.google.android.gms");
		customReports.VerifyAppNameInAppDataUsageReport(Config.Enter_App1);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '2', description = "Verify generating Application Data Usage Custom report with filters for 'Current Date' & Home group")
	public void VerifyingAppDataUsageReport_TC_RE_118() throws InterruptedException {
		Reporter.log(
				"\nVerify generating Application Data Usage Custom report with filters for 'Current Date' & Home group",
				true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(
				"AppDataUsage CRep with filter for greater than or equal to opeartor", "test");
		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
		customReports.Selectvaluefromdropdown(Config.SelectAppDataUSage);
		customReports.SelectValueFromColumn(Config.SelectMobileData);
		customReports.SelectOperatorFromDropDown(Config.SelectGreaterThanOrEqualtoOperator);
		customReports.SendValueToTextfield("50");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppDataUsage CRep with filter for greater than or equal to opeartor");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection(
				"AppDataUsage CRep with filter for greater than or equal to opeartor");
		customReports.SelectOneWeekDateInOndemandReport();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppDataUsage CRep with filter for greater than or equal to opeartor");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppDataUsage CRep with filter for greater than or equal to opeartor");
		reportsPage.WindowHandle();
		customReports.VerifyingMobileDataUsageColumn();
		customReports.VarifyingOneWeekDateInAppDataUsageViewReport();
		accountsettingspage.SearchUsedApplicationORWebSiteInReport(Config.EnterSureMDMNix);
		customReports.VerifyPackageNameInAppDataUsageReport(Config.EnterPackageId);
		customReports.VerifyAppNameInAppDataUsageReport(Config.EnterSureMDMNix);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '3', description = "Verify generating Application Data Usage Custom report with filters for '1week Date'")
	public void VerifyingAppDataUsageReport_TC_RE_119() throws InterruptedException {
		Reporter.log("\nVerify generating Application Data Usage Custom report with filters for '1week Date'", true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("AppDataUsage CRep with filter for one week Date",
				"test");
		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
		customReports.Selectvaluefromdropdown(Config.SelectAppDataUSage);
		customReports.SelectValueFromColumn(Config.SelectDateTime);
		customReports.SelectOneWeekDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppDataUsage CRep with filter for one week Date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("AppDataUsage CRep with filter for one week Date");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppDataUsage CRep with filter for one week Date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppDataUsage CRep with filter for one week Date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingOneWeekDateInAppDataUsageViewReport();
		accountsettingspage.SearchUsedApplicationORWebSiteInReport(Config.EnterSureMDMNix);
		customReports.VerifyPackageNameInAppDataUsageReport(Config.EnterPackageId);
		customReports.VerifyAppNameInAppDataUsageReport(Config.EnterSureMDMNix);
		accountsettingspage.SearchUsedApplicationORWebSiteInReport(Config.Enter_App1);
		customReports.VerifyPackageNameInAppDataUsageReport(Config.SelectPackageName1);
		customReports.VerifyAppNameInAppDataUsageReport(Config.Enter_App1);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '4', description = "Verify generating Application Data usage Custom report with filters for 'Yesterday' date")
	public void VerifyingAppDataUsageReport_TC_RE_120() throws InterruptedException {
		Reporter.log("\nVerify generating Application Data usage Custom report with filters for 'Yesterday' date",
				true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(
				"AppDataUsage CRep with filter for Yesterday and equal Op", "test");
		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
		customReports.Selectvaluefromdropdown(Config.SelectAppDataUSage);
		customReports.SelectValueFromColumn(Config.SelectWifiData);
		customReports.SelectOperatorFromDropDown(Config.SelectGreaterThanOrEqualtoOperator);
		customReports.SendValueToTextfield("500");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppDataUsage CRep with filter for Yesterday and equal Op");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppDataUsage CRep with filter for Yesterday and equal Op");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppDataUsage CRep with filter for Yesterday and equal Op");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingYestrDate_APPDataUsage();
		customReports.VerifyingWifiDataUsageColumn();
		accountsettingspage.SearchUsedApplicationORWebSiteInReport(Config.Enter_App1);
		customReports.VerifyPackageNameInAppDataUsageReport(Config.SelectPackageName1);
		customReports.VerifyAppNameInAppDataUsageReport(Config.Enter_App1);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '5', description = "Verify generating Application Data Usage Custom report with filters for 'Current Date' & Home group")
	public void VerifyingAppDataUsageReport_TC_RE_121() throws InterruptedException {
		Reporter.log(
				"\nVerify generating Application Data Usage Custom report with filters for 'Current Date' & Home group",
				true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(
				"AppDataUsage CustRep with filter for Current Date and like Operator", "test");
		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
		customReports.Selectvaluefromdropdown(Config.SelectAppDataUSage);
		customReports.SelectValueFromColumn(Config.SelectApplicationName);
		customReports.SelectOperatorFromDropDown(Config.SelectlikeOperator);
		customReports.SendValueToTextfield("MDM Nix");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppDataUsage CustRep with filter for Current Date and like Operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppDataUsage CustRep with filter for Current Date and like Operator");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppDataUsage CustRep with filter for Current Date and like Operator");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingYestrDate_APPDataUsage();
		customReports.VerifyPackageNameInAppDataUsageReport(Config.EnterPackageId);
		customReports.VerifyAppNameInAppDataUsageReport(Config.EnterSureMDMNix);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@Test(priority = '6', description = "Verify generating Application Data Usage Custom report with filters for 'Current Date' & Home group- The report contain only the specified app wrt filter applied i.e com.nix")
	public void VerifyingAppDataUsageReport_TC_RE_122() throws InterruptedException {
		Reporter.log(
				"\nVerify generating Application Data Usage Custom report with filters for 'Current Date' & Home group- The report contain only the specified app wrt filter applied i.e com.nix",
				true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("AppDataUsage CRep with filter for like Operator",
				"test");
		customReports.ClickOnColumnInTable(Config.SelectAppDataUSage);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectAppDataUSage);
		customReports.Selectvaluefromdropdown(Config.SelectAppDataUSage);
		customReports.SelectValueFromColumn(Config.SelectPackageName);
		customReports.SelectOperatorFromDropDown(Config.SelectlikeOperator);
		customReports.SendValueToTextfield(Config.EnterPackageId);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("AppDataUsage CRep with filter for like Operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("AppDataUsage CRep with filter for like Operator");
		customReports.SelectTodayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("AppDataUsage CRep with filter for like Operator");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("AppDataUsage CRep with filter for like Operator");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VarifyingCurrentDate_APPDataUsage();
		customReports.VerifyPackageNameInAppDataUsageReport(Config.EnterPackageId);
		customReports.VerifyAppNameInAppDataUsageReport(Config.EnterSureMDMNix);
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}

	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				String FailedWindow = Initialization.driver.getWindowHandle();
				if (FailedWindow.equals(customReports.PARENTWINDOW)) {
					commonmethdpage.ClickOnHomePage();
				} else {
					reportsPage.SwitchBackWindow();
				}
			} catch (Exception e) {

			}
		}
	}
}
