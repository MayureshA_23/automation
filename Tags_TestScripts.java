package NewUI_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class Tags_TestScripts extends Initialization{
	@Test(priority = 0, description = "Verify deleting the tag")
	public void DeleteTag() throws InterruptedException {
		Reporter.log("=====1.Verify deleting the tag=====", true);
		newUIScriptsPage.ClickOnTryNewConsole();
		Tags.ClickOnTags1();
		Tags.CreatTag("AutomationTag1");
		newUIScriptsPage.DeleteTag("AutomationTag1");
		newUIScriptsPage.VerifyOnDeletedTag("AutomationTag1");
		
	}
	/*@Test(priority = 1, description = "verify applying job to tag")
	public void ApplyJobToTag()throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("=====2.verify applying job to tag=====", true);
		newUIScriptsPage.ClickOnTryNewConsole();
		Tags.ClickOnTags1();
		Tags.CreatTag("@AutomationGrp");
		Tags.ClickOnGroup();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		Tags.RightClickTag();
		Tags.SearchTag("@AutomationGrp");
		Tags.EnableTagCheckbox(" @AutomationGrp");
		Tags.ClickOnSaveTagButton();
		Tags.SuccessfulMessageOnTagMove();
		Tags.ClickOnTags1();
		Tags.ApplyJobOnTag("@AutomationGrp");
		androidJOB.SearchField("0TextMessage"); 
		newUIScriptsPage.clickOnYesBtnInApplyJobToGroup();
		newUIScriptsPage.ReadingConsoleMessageForJobDeployment("0TextMessage",Config.DeviceName);
		Tags.DeleteTag("@AutomationGrp");
	}*/

}
