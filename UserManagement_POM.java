package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.gargoylesoftware.htmlunit.javascript.host.media.webkitAudioContext;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.PassScreenshot;
import Library.WebDriverCommonLib;

public class UserManagement_POM extends WebDriverCommonLib {

	AssertLib ALib = new AssertLib();
	ExcelLib ELib = new ExcelLib();
	// Robot robot = new Robot();
	// JavascriptExecutor js=(JavascriptExecutor)Initialization.driver;

	@FindBy(xpath = ".//*[@id='userManagement']")
	private WebElement usermanagementlink;
	@FindBy(xpath = ".//*[@id='userManagement_pg']/section/div[2]/header/span[1]")
	private WebElement usermanagementtext;

	public void VerifyRolesOptionIsPresent() throws InterruptedException {
		usermanagementlink.click();
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 20);
		wait1.until(ExpectedConditions.elementToBeClickable(usermanagementtext));
		boolean isdisplayed = true;

		try {
			usermanagementtext.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail: Unable to display usermanagent page");
		Reporter.log("PASS >> user management page launched successfully", true);
		sleep(4);
		PassScreenshot.captureScreenshot(Initialization.driver, "DeviceEnrollmentScreenshot");
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='userManagement_pg']/section/div[1]/ul/li[2]/a[text()='Roles']")
	private WebElement clickOnRolesOptions;
	
	@FindBy(xpath = "//p[text()='Super User']")
	private WebElement superusertemplete;

	public void toVerifySuperUsertemplete() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(5);

		boolean displayed;

		try {
			sleep(5);
			superusertemplete.isDisplayed();
			sleep(5);
			displayed = true;
			sleep(5);
		} catch (Throwable T) {
			displayed = true;

		}
		String PassStatement = "PASS >> default super user is present";
		String FailStatement = "FAIL >> default super user is not present ";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(2);

	}

	public void ClickOnCreateButton() throws InterruptedException {
		OkButton.click();
		sleep(3);
	}

	@FindBy(id = "cancelbtn")
	private WebElement CancelButton;

	public void ClickOnCancelButton() throws InterruptedException {
		CancelButton.click();
		sleep(4);
	}

	@FindBy(xpath = "//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.InputUserDemouser1 + "']")
	private WebElement InputUserDemouser1;

	@FindBy(xpath = "//*[@id='newpassword_forreset']")
	private WebElement newpassword_forreset;

	@FindBy(xpath = "//*[@id='retypePasswordreset']")
	private WebElement retypePasswordreset;

	@FindBy(xpath = "//*[@id='resetpasswordusermanagement']")
	private WebElement OKButton_resetpasswordusermanagement;

	public void selectDemoUser1(String SelectSubUser, String NewPwd, String RetypePwd) throws InterruptedException {
		InputUserDemouser1.click();
		sleep(2);
		ResetPasswordButton.click();
		sleep(2);
		newpassword_forreset.sendKeys(NewPwd);
		sleep(2);
		retypePasswordreset.sendKeys(RetypePwd);
		sleep(2);
		OKButton_resetpasswordusermanagement.click();
		waitForXpathPresent("//span[text()='New Password saved successfully']");
		sleep(30);
	}

	public void UsermanagementWindowsSwitch() throws InterruptedException {

		js.executeScript("window.open()");
		String Mainwindow = Initialization.driver.getWindowHandle();
		Initialization.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		Initialization.driver.get(Config.url);
		Reporter.log("Clicked On Tab", true);
		sleep(2);
	}

	public void SearchUserTextField(String InputUser) throws InterruptedException {

		SearchUser.sendKeys(InputUser);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + InputUser + "']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.InputUser + "']")
	private WebElement InputUser;

	public void selectUser(String SelectSubUser, String NewPwd, String RetypePwd) throws InterruptedException {
		InputUser.click();
		sleep(2);
		ResetPasswordButton.click();
		sleep(2);
		newpassword_forreset.sendKeys(NewPwd);
		sleep(2);
		retypePasswordreset.sendKeys(RetypePwd);
		sleep(2);
		OKButton_resetpasswordusermanagement.click();
		waitForXpathPresent("//span[text()='New Password saved successfully']");
		sleep(30);
	}

	public void SearchSuperUserTextField(String InputSuperUser) throws InterruptedException {

		SearchUser.sendKeys(InputSuperUser);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.userID + "']");
		sleep(2);
	}

	public void SearchUserTextField1(String InputUser) throws InterruptedException {

		SearchUser.sendKeys(InputUser);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + InputUser + "']");
		sleep(2);
	}

	public void SwitchToChildWindow() throws InterruptedException {
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		waitForXpathPresent("//*[@id='all_ava_devices']");
		Reporter.log("Switched To Child Windows", true);
		sleep(10);
	}

	@FindBy(xpath = "//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.userID + "']")
	private WebElement InputUserSuperuser;

	public void selectSuperUser(String NewPwd, String RetypePwd) throws InterruptedException {
		InputUserSuperuser.click();
		sleep(2);
		ResetPasswordButton.click();
		sleep(2);
		newpassword_forreset.sendKeys(NewPwd);
		sleep(2);
		retypePasswordreset.sendKeys(RetypePwd);
		sleep(2);
		OKButton_resetpasswordusermanagement.click();
		waitForXpathPresent("//span[text()='New Password saved successfully']");
		sleep(30);
	}

	public void CloseBrowsers() throws InterruptedException {

		Initialization.driver.close();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='all_ava_devices']")
	private WebElement all_ava_devices;

	public void ClickONallDevices() throws InterruptedException {

		all_ava_devices.click();
		// waitForidPresent("forgot_password");
		sleep(4);

		Boolean SubUserLoggedOut = userNAmeEdt.isDisplayed();
		String Pass = "PASS>> userNAmeEdt is Displayed";
		String Fail = "FAIL>> userNAmeEdt is Not Displayed";
		ALib.AssertTrueMethod(SubUserLoggedOut, Pass, Fail);
		sleep(2);
	}

	public void SearchUserTextFieldDemouser1(String InputUser) throws InterruptedException {

		SearchUser.sendKeys(InputUser);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + InputUser + "']");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='userManagementDialog']/div/div[1]/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement SearchUser;

	public void SearchUserTextFieldDemouser10(String InputUser) throws InterruptedException {

		SearchUser.sendKeys(InputUser);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.InputUserDemouser10 + "']");
		sleep(2);
	}

	public void SwitchToMainWindow() throws InterruptedException {
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(0));
		Reporter.log("Switched To MainWindows", true);
		sleep(10);
	}

	public void CreateDemoUser2(String InputUser, String pwd, String RetypePwd, String SelectRoles)
			throws InterruptedException {
		User.click();
		sleep(2);
		AddUserButton.click();
		sleep(3);
		UserName.sendKeys(InputUser);
		UserPasswoard.sendKeys(pwd);
		ConfirmPasswoard.sendKeys(RetypePwd);
		FirstName.sendKeys(Config.FirstName);
		Email.sendKeys(Config.EmailID);
		sleep(3);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(2);
		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(SelectRoles);
		sleep(2);
		OkButton.click();
		waitForXpathPresent("//span[text()='New user created successfully.']");
		sleep(2);
		String Actualvalue = UserSuccessfullyCreatedPopUp.getText();
		String Expectedvalue = "New user created successfully.";
		String PassStatement = "PASS >>'User created successfully.- displayed'";
		String FailStatement = "FAIL >>  User created successfully.not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}

	public void CreateDemoUser1(String InputUser, String pwd, String RetypePwd, String SelectRoles)
			throws InterruptedException {
		User.click();
		sleep(2);
		AddUserButton.click();
		sleep(4);
		UserName.sendKeys(InputUser);
		sleep(2);
		UserPasswoard.sendKeys(pwd);
		ConfirmPasswoard.sendKeys(RetypePwd);
		FirstName.sendKeys(Config.FirstName);
		Email.sendKeys(Config.EmailID);
		sleep(3);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(2);
		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(SelectRoles);
		sleep(2);
		OkButton.click();
		waitForXpathPresent("//span[text()='New user created successfully.']");
		sleep(2);
		String Actualvalue = UserSuccessfullyCreatedPopUp.getText();
		String Expectedvalue = "New user created successfully.";
		String PassStatement = "PASS >>'User created successfully.- displayed'";
		String FailStatement = "FAIL >>  User created successfully.not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}


	public void RoleWithAllPermissions(String RoleName, String RoleDescriptionName) throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys(RoleName);
		sleep(2);

		descriptionTestField.sendKeys(RoleDescriptionName);
		sleep(2);

		selectAllRolesPermissions.click();
		sleep(2);

		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Role created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(6);
	}

	@FindBy(xpath = "//td[text()= '" + Config.user + "']")
	private WebElement SelectUserDemoUser;

	public void SelectDemoUser() throws InterruptedException {
		SelectUserDemoUser.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='editUser']/span[text()='Edit User']")
	private WebElement EditUserButton;

	public void ClickOnEditUser() throws InterruptedException {
		EditUserButton.click();
		waitForXpathPresent("//h4[text()='Edit User Settings']");
		sleep(3);
	}

	@FindBy(id = "uneditablemessage")
	private WebElement note;

	public void VerifyEditUserNote() throws InterruptedException {
		String Actual = note.getText();
		String Expected = Config.EditUserNote;
		String pass = "PASS >> Edit User Note is correct: " + note;
		String fail = "FAIL >> Edit User note is not correct";
		ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
	}

	public void WarningMessagesblankFieldInUser() throws InterruptedException {
		AddUserButton.click();
		sleep(3);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
		OkButton.click();
		sleep(2);

		boolean displayValidMail = EnterValidEmailAddress.isDisplayed();
		String PassStatement1 = "PASS >> Warning message enter valid Email ID has been displayed ";
		String FailStatement1 = "FAIL >> Warning message  enter valid Email ID has not been displayed";
		ALib.AssertTrueMethod(displayValidMail, PassStatement1, FailStatement1);

		boolean displayValidFirstNmae = EnterValidFirstName.isDisplayed();
		String PassStatement2 = "PASS >> Warning message enter valid First Name has been displayed ";
		String FailStatement2 = "FAIL >> Warning message  enter valid First Name has not been displayed";
		ALib.AssertTrueMethod(displayValidFirstNmae, PassStatement2, FailStatement2);

		WebElement scrollup = UserName;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollup);
		sleep(2);

		boolean displayed = EnterValiduserName.isDisplayed();
		sleep(2);
		String PassStatement = "PASS >> Warning message enter valid user name has been displayed ";
		String FailStatement = "FAIL >> Warning message  enter valid user name has not been displayed";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(3);

		boolean displayedPassword = PasswordLengthIsTooShort.isDisplayed();
		String PassStatement3 = "PASS >> Warning message Password length is too short has been displayed ";
		String FailStatement3 = "FAIL >> Warning message  Password length is too short name has not been displayed";
		ALib.AssertTrueMethod(displayedPassword, PassStatement3, FailStatement3);

		boolean displayedPasswordDonotMatch = PasswordDonotMatch.isDisplayed();
		String PassStatement4 = "PASS >> Warning message enter valid user name has been displayed ";
		String FailStatement4 = "FAIL >> Warning message  enter valid user name has not been displayed";
		ALib.AssertTrueMethod(displayedPasswordDonotMatch, PassStatement4, FailStatement4);

	}

	public void VerifyUserManagementIsDisplayed() throws InterruptedException {

		boolean isdisplayed = true;

		try {
			usermanagementtext.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail: Unable to display usermanagent page");
		Reporter.log("PASS >> user management page launched successfully", true);
		sleep(4);
		PassScreenshot.captureScreenshot(Initialization.driver, "DeviceEnrollmentScreenshot");
		sleep(3);
	}

	public void Disable_SelectUnapprovedAndroidEnterpriseApplications() throws InterruptedException {

		WebElement scrollDown = Enable_SelectUnapprovedAndroidEnterpriseApplications;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		Enable_SelectUnapprovedAndroidEnterpriseApplications.click();
		sleep(2);

		saveButton.click();
		sleep(4);

		String Actualvalue = RoleEditedSuccessfully.getText();
		String Expectedvalue = "Roles modified successfully.";
		String PassStatement = "PASS >>'Roles modified successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to modify in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='disableUser']")
	private WebElement disableUser;

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement ConfirmationDialog;

	public void selectUserDisable() throws InterruptedException {
		InputDemouser.click();
		sleep(2);
		disableUser.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']");
		sleep(1);
		ConfirmationDialog.click();
		waitForXpathPresent("//span[contains(text(),'User disabled successfully.')]");
		Reporter.log("Selected user(s) disabled", true);
		sleep(2);

	}

	public void RolesClose() throws InterruptedException {

		RolesCloseButton.click();
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='PermissionsRolesGrid']/tbody/tr/td/p[text()='AllRoleDataAnalyticsSettingsPermission']")
	private WebElement AccountSettingsDataAnalyticsRole;

	public void SelectDataAnalyticsRole() throws InterruptedException {

		AccountSettingsDataAnalyticsRole.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='WebHooks']")
	private WebElement WebHooks;

	public void VerifyWebHooksOptnInAccountSettingsSubUser() throws InterruptedException {

		WebElement scrollDown = WebHooks;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		Boolean ProfilePermissionRole = WebHooks.isDisplayed();
		String pass = "PASS>> Webhook Settings is Displayed";
		String fail = "FAIL>> Webhook Settings  is Not Displayed";
		ALib.AssertTrueMethod(ProfilePermissionRole, pass, fail);
		sleep(2);
	}

	public void ClickonAnalyticsAPPLYButton() {
		analytics_apply.click();
		try {
			waitForLogXpathPresent("//span[text()='Settings updated successfully.']");
			Reporter.log("Settings updated successfully.", true);
			sleep(4);
		} catch (Exception e) {
			String FailMessage = "'Settings updated successfully.' Isn't Displayed";
			ALib.AssertFailMethod(FailMessage);
		}
	}

	@FindBy(xpath = "(//button[contains(text(),'Add')])[3]")
	private WebElement AddButton;

	public void ClickOnAddButton() throws InterruptedException {

		AddButton.click();
		sleep(2);

	}

	@FindBy(xpath = "//span[contains(text(),'" + Config.FieldNameDataAnalyticsUsers + "')]")
	private WebElement FieldNameDataAnalyticsUsers;

	public void ClickOnRemoveBtn2() throws InterruptedException {

		if (FieldNameDataAnalyticsUsers.isDisplayed()) {
			cancelAnalyticsSec.click();
			waitForXpathPresent("//p[text()='Are you sure you want to delete these analytics?']");
			sleep(1);
			YesBtn.click();
			waitForXpathPresent("//span[contains(text(),'Analytics Settings deleted successfully.')]");

			Reporter.log("'Analytics Settings deleted successfully.'", true);
			sleep(1);

		} else {

			ALib.AssertFailMethod("'Analytics Settings deleted successfully.' is not displayed");
		}
	}

	public void ClickOnRemoveBtn() throws InterruptedException {

		if (FieldNameDataAnalytics.isDisplayed()) {
			cancelAnalyticsSec.click();
			// waitForXpathPresent("//p[text()='Are you sure you want to delete this
			// Analytics?']");
			sleep(2);
			YesBtn.click();
			waitForXpathPresent("//span[contains(text(),'Analytics Settings deleted successfully.')]");

			Reporter.log("'Analytics Settings deleted successfully.'", true);
			sleep(1);

		} else {

			ALib.AssertFailMethod("'Analytics Settings deleted successfully.' is not displayed");
		}
	}

	@FindBy(xpath = "//*[@id='field_name_input']")
	private WebElement field_name_input;

	public void InputFieldName(String FieldName) throws InterruptedException {

		field_name_input.clear();
		sleep(1);
		field_name_input.sendKeys(FieldName);
		sleep(1);
	}

	public void ClickOnAddFieldBtn() throws InterruptedException {

		AddFieldsButton.click();
		waitForXpathPresent("//h4[contains(text(),'Add Field')]");
		sleep(1);

	}

	public void GetNewAnalyticsNumber() throws InterruptedException {

		newAnalytics = NewAnalytics.getText();
		System.out.println(newAnalytics);

		sleep(2);
	}

	@FindBy(xpath = "//span[contains(text(),'" + Config.FieldNameDataAnalytics + "')]")
	private WebElement FieldNameDataAnalytics;

	public void VerifyNewlyCreatedDataAnalyticsVisibleToSubUserWhenDataAnalyticsRoleIsEnabled()
			throws InterruptedException {

		Boolean ProfilePermissionRole = Initialization.driver
				.findElement(By.xpath("//h3[contains(text(),'" + newAnalytics + "')]")).isDisplayed();
		String pass = "PASS>> '" + newAnalytics + "'is Displayed for SubUser";
		String fail = "FAIL>> '" + newAnalytics + "' is Not Displayed for SubUser";
		ALib.AssertTrueMethod(ProfilePermissionRole, pass, fail);
		sleep(2);

		Boolean ProfilePermissionRole1 = FieldNameDataAnalytics.isDisplayed();
		String pass1 = "PASS>> '" + FieldNameDataAnalytics + "'is Displayed for SubUser";
		String fail1 = "FAIL>> '" + FieldNameDataAnalytics + "' is Not Displayed for SubUser";
		ALib.AssertTrueMethod(ProfilePermissionRole1, pass1, fail1);
		sleep(2);
	}

	public void ClickonAnalyticsSaveButton() {
		NewlyCreatedAnalyticsSaveButton.click();
		try {
			waitForLogXpathPresent("//span[text()='Analytics Settings saved successfully.']");
			Reporter.log("Analytics Settings saved successfully Message Displayed", true);
			waitForXpathPresent(
					"//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[5]/div[2]/div/span[1]");
			sleep(5);
		} catch (Exception e) {
			String FailMessage = "Analytics Settings saved successfully Message Isn't Displayed";
			ALib.AssertFailMethod(FailMessage);
		}
	}

	@FindBy(xpath = "//*[@id='Miscellaneous_Settings']")
	private WebElement Miscellaneous_Settings;

	@FindBy(xpath = "//div[@id='newanalytics']")
	private WebElement AddAnalyticsButton;
	@FindBy(xpath = "//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[2]/div/input")
	private WebElement NewlyCreatedAnalyticsNameField;
	@FindBy(xpath = "//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[3]/div/input")
	private WebElement NewlyCreatedAnalyticsTagNameField;
	@FindBy(xpath = "//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[4]/div/div[2]/div")
	private WebElement AddFieldsButton;
	@FindBy(xpath = "//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[1]/span[2]/a")
	private WebElement NewlyCreatedAnalyticsSaveButton;
	@FindBy(xpath = "//*[@id='analytics_apply']")
	private WebElement analytics_apply;
	@FindBy(xpath = "//span[text()='Settings updated successfully.']")
	private WebElement SettingUpdtedSuccessMessage;

	public void ClickOnAddAnalyticsButton() {
		AddAnalyticsButton.click();
	}

	public void SendingDataAnalyticsName(String DataAnalyticsName) throws InterruptedException {
		NewlyCreatedAnalyticsNameField.sendKeys(DataAnalyticsName);
		sleep(2);
	}

	public void SendingTagName(String TagName) throws InterruptedException {
		NewlyCreatedAnalyticsTagNameField.sendKeys(TagName);
		sleep(2);
	}

	public void VerifyWebHooksOptnNotPresentInAccountSettingsSubUser() throws InterruptedException {

		WebElement scrollDown = Miscellaneous_Settings;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		boolean flag = false;
		try {
			if (WebHooks.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >> " + WebHooks + " is NOT displayed";
		String FailStatement = "FAIL >> " + WebHooks + " is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);

	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[28][text()='Data Analytics']")
	private WebElement DataAnalytics;

	@FindBy(xpath = "//*[@id='umTree']/ul/li[28]/span[4]")
	private WebElement EnableCheckDataAnalytics;

	public void VerifyDisablingDataAnalyticsOptn() throws InterruptedException {

		WebElement scrollDown = DataAnalytics;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		EnableCheckDataAnalytics.click();
		sleep(1);

		saveButton.click();
		sleep(4);

		String Actualvalue = RoleEditedSuccessfully.getText();
		String Expectedvalue = "Role modified successfully.";
		String PassStatement = "PASS >>'Roles modified successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to modify in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}

	public void VerifyDataAnalyticsOptnNotPresentInAccountSettingsSubUser() throws InterruptedException {

		boolean flag = false;
		try {
			if (DataAnalyticsSubUser.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >> " + DataAnalyticsSubUser + " is NOT displayed";
		String FailStatement = "FAIL >> " + DataAnalyticsSubUser + " is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}

	public void VerifyNewlyCreatedDataAnalyticsVisibleToADMINWhenDataAnalyticsRoleIsEnabled()
			throws InterruptedException {

		Boolean ProfilePermissionRole = Initialization.driver
				.findElement(By.xpath("//h3[contains(text(),'" + newAnalytics + "')]")).isDisplayed();
		String pass = "PASS>> '" + newAnalytics + "' is Displayed for ADMIN";
		String fail = "FAIL>> '" + newAnalytics + "' is Not Displayed for ADMIN";
		ALib.AssertTrueMethod(ProfilePermissionRole, pass, fail);
		sleep(2);

		Boolean ProfilePermissionRole1 = FieldNameDataAnalyticsSubUser.isDisplayed();
		String pass1 = "PASS>> '" + FieldNameDataAnalyticsSubUser + "'is Displayed for SubUser";
		String fail1 = "FAIL>> '" + FieldNameDataAnalyticsSubUser + "' is Not Displayed for SubUser";
		ALib.AssertTrueMethod(ProfilePermissionRole1, pass1, fail1);
		sleep(2);
	}

	String newAnalytics;

	@FindBy(xpath = "(//span[contains(text(),'Add Analytics')]/../../../div[4]/div/div/h3)[1]")
	private WebElement NewAnalytics;

	public void VerifyAnalyticsShouldNotBeVisibleInAccountSettings() {

		boolean flag = false;
		try {
			if (Initialization.driver.findElement(By.xpath("//h3[contains(text(),'" + newAnalytics + "')]"))
					.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >> " + newAnalytics + " is NOT displayed";
		String FailStatement = "FAIL >> " + newAnalytics + " is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}

	public void ScrollToNewAnalytics() throws InterruptedException {

		WebElement scrollDown2 = Initialization.driver
				.findElement(By.xpath("//h3[contains(text(),'" + newAnalytics + "')]"));
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
	}

	public void VerifyDataAnalyticsOptnInAccountSettingsSubUser() throws InterruptedException {

		Boolean ProfilePermissionRole = DataAnalyticsSubUser.isDisplayed();
		String pass = "PASS>> DataAnalyticsSubUser Settings is Displayed";
		String fail = "FAIL>> DataAnalyticsSubUser Settings  is Not Displayed";
		ALib.AssertTrueMethod(ProfilePermissionRole, pass, fail);
		sleep(2);
	}

	public void ClickOnSaveButtonRoles() throws InterruptedException {
		saveButton.click();
		sleep(5);
	}

	public void ClickOnUserOption() throws InterruptedException {
		User.click();
		sleep(5);
	}

	public void ClickOnMoreIOS(String appname) throws InterruptedException {
		try {
			Initialization.driver
			.findElement(By.xpath("(//div[p[text()='" + appname + "']]/following-sibling::div/div/span)[1]"))
			.click();
			sleep(5);
			Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove", true);
		} catch (Exception e) {
			Initialization.driver
			.findElement(By.xpath("(//div[p[text()='" + appname + "']]/following-sibling::div/div/span)[2]"))
			.click();
			sleep(5);
			Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove", true);

		}
	}

	@FindBy(id = "ListFeaturePermissions")
	private WebElement RolesDropdown;

	public void SelectRolesInUser() {
		Select roles = new Select(RolesDropdown);
		roles.selectByVisibleText(Config.RoleName);
	}

	@FindBy(xpath = "//*[@id='permissionSelection_popup']/div/div/div[1]/button")
	private WebElement CloseRoleWindow;

	public void ClickOnCloseRoleWindow() throws InterruptedException {
		CloseRoleWindow.click();
		waitForidPresent("userProfileButton");
		sleep(2);
	}

	public void RolesCreatedSuccessfullyMessage() {
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

	}

	public void EnterRoleDescriptio(String RoleDescription) throws InterruptedException {

		descriptionTestField.sendKeys(RoleDescription);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[15]/span[3]")
	private WebElement AccountSettingsPermissionCheckbox;

	public void ClickOnAccountSettingsPermission() {
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);

		AccountSettingsPermissionCheckbox.click();
	}

	public void EnterRolesName(String RoleName) throws InterruptedException {
		NameTextField.sendKeys(RoleName);
		sleep(4);
	}

	@FindBy(xpath = "//*[@id=\"umTree\"]/ul/li[17]")
	private WebElement ScrollDownToAccountSettings;

	public void AvailibityOfAllFeaturesOfAccountSettings() throws InterruptedException {
		WebElement scrollDown = ScrollDownToAccountSettings;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);

		List<WebElement> ls = Initialization.driver.findElements(By.xpath("//*[@id='umTree']/ul/li"));
		int count = ls.size();
		for (int i = 17; i < count; i++) {
			if (i == 17) {
				String Actual = ls.get(i).getText();
				String Expected = "Branding Info";
				String pass = "Feature 1 is available: " + Actual;
				String fail = "Feature 1 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 18) {
				String Actual = ls.get(i).getText();
				String Expected = "Device Enrollment Rules";
				String pass = "Feature 2 is available: " + Actual;
				String fail = "Feature 2 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 19) {
				String Actual = ls.get(i).getText();
				String Expected = "EULA Disclaimer Policy";
				String pass = "Feature 3 is available: " + Actual;
				String fail = "Feature 3 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 20) {
				String Actual = ls.get(i).getText();
				String Expected = "iOS/iPadOS/macOS Settings";
				String pass = "Feature 4 is available: " + Actual;
				String fail = "Feature 4 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 21) {
				String Actual = ls.get(i).getText();
				String Expected = "Group Assignment Rules";
				String pass = "Feature 5 is available: " + Actual;
				String fail = "Feature 5 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 22) {
				String Actual = ls.get(i).getText();
				String Expected = "SAML Single Sign-On";
				String pass = "Feature 6 is available: " + Actual;
				String fail = "Feature 6 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 23) {
				String Actual = ls.get(i).getText();
				String Expected = "Alert Template";
				String pass = "Feature 7 is available: " + Actual;
				String fail = "Feature 7 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}
			if (i == 24) {
				String Actual = ls.get(i).getText();
				String Expected = "Customize Toolbar";
				String pass = "Feature 8 is available: " + Actual;
				String fail = "Feature 8 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 25) {
				String Actual = ls.get(i).getText();
				String Expected = "Customize Nix/SureLock";
				String pass = "Feature 9 is available: " + Actual;
				String fail = "Feature 9 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 26) {
				String Actual = ls.get(i).getText();
				String Expected = "Certificate Management";
				String pass = "Feature 10 is available: " + Actual;
				String fail = "Feature 10 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 27) {
				String Actual = ls.get(i).getText();
				String Expected = "Data Analytics";
				String pass = "Feature 11 is available: " + Actual;
				String fail = "Feature 11 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 28) {
				String Actual = ls.get(i).getText();
				String Expected = "Miscellaneous Settings";
				String pass = "Feature 12 is available: " + Actual;
				String fail = "Feature 12 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 29) {
				String Actual = ls.get(i).getText();
				String Expected = "Mobile Email Management";
				String pass = "Feature 13 is available: " + Actual;
				String fail = "Feature 13 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 30) {
				String Actual = ls.get(i).getText();
				String Expected = "Account Management";
				String pass = "Feature 14 is available: " + Actual;
				String fail = "Feature 14 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 31) {
				String Actual = ls.get(i).getText();
				String Expected = "Enterprise Integrations";
				String pass = "Feature 15 is available: " + Actual;
				String fail = "Feature 15 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 32) {
				String Actual = ls.get(i).getText();
				String Expected = "Chrome OS Device Management";
				String pass = "Feature 16 is available: " + Actual;
				String fail = "Feature 16 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 33) {
				String Actual = ls.get(i).getText();
				String Expected = "Office365 Settings";
				String pass = "Feature 17 is available: " + Actual;
				String fail = "Feature 17 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 35) {
				String Actual = ls.get(i).getText();
				String Expected = "Verified Emails";
				String pass = "Feature 18 is available: " + Actual;
				String fail = "Feature 18 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 36) {
				String Actual = ls.get(i).getText();
				String Expected = "Plugins settings";
				String pass = "Feature 19 is available: " + Actual;
				String fail = "Feature 19 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 37) {
				String Actual = ls.get(i).getText();
				String Expected = "Firmware Updates";
				String pass = "Feature 20 is available: " + Actual;
				String fail = "Feature 20 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}

			if (i == 38) {
				String Actual = ls.get(i).getText();
				String Expected = "License Management";
				String pass = "Feature 21 is available: " + Actual;
				String fail = "Feature 21 is not available";
				ALib.AssertEqualsMethod(Expected, Actual, pass, fail);
			}
		}
	}

	public void VerifyDataAnalyticsOptn() throws InterruptedException {

		WebElement scrollDown = DataAnalytics;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		Boolean ProfilePermissionRole = DataAnalytics.isDisplayed();
		String pass = "PASS>> DataAnalytics Settings is Displayed";
		String fail = "FAIL>> DataAnalytics Settings  is Not Displayed";
		ALib.AssertTrueMethod(ProfilePermissionRole, pass, fail);
		sleep(2);
	}

	public void VerifyNewlyCreatedDataAnalyticsVisibleToUSERSWhenDataAnalyticsRoleIsEnabled()
			throws InterruptedException {

		Boolean ProfilePermissionRole = Initialization.driver
				.findElement(By.xpath("//h3[contains(text(),'" + newAnalytics + "')]")).isDisplayed();
		String pass = "PASS>> '" + newAnalytics + "' is Displayed for USER";
		String fail = "FAIL>> '" + newAnalytics + "' is Not Displayed for USER";
		ALib.AssertTrueMethod(ProfilePermissionRole, pass, fail);
		sleep(2);

		Boolean ProfilePermissionRole1 = FieldNameDataAnalyticsUsers.isDisplayed();
		String pass1 = "PASS>> '" + FieldNameDataAnalyticsUsers + "'is Displayed for SubUser";
		String fail1 = "FAIL>> '" + FieldNameDataAnalyticsUsers + "' is Not Displayed for SubUser";
		ALib.AssertTrueMethod(ProfilePermissionRole1, pass1, fail1);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='PermissionsRolesGrid']/tbody/tr/td/p[text()='" + Config.SelectCreatedRoleWebhookSettings
			+ "']")
	private WebElement AccountSettingsWebhookSettings;

	public void selectRoleWebhookSettings() throws InterruptedException {

		AccountSettingsWebhookSettings.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[35]/span[4]")
	private WebElement EnableWebhookSettings;

	public void DisableWebhookSettings() throws InterruptedException {

		EnableWebhookSettings.click();
		sleep(2);
		saveButton.click();
		sleep(4);

		String Actualvalue = RoleEditedSuccessfully.getText();
		String Expectedvalue = "Role modified successfully.";
		String PassStatement = "PASS >>'Roles modified successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to modify in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}

	public void RoleWithAllPermissionsDA(String RoleName, String RoleDescriptionName) throws InterruptedException {

		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys(RoleName);
		sleep(2);

		descriptionTestField.sendKeys(RoleDescriptionName);
		sleep(2);

		selectAllRolesPermissions.click();
		sleep(2);

		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(6);
	}

	@FindBy(xpath = "//span[contains(text(),'" + Config.FieldNameDataAnalyticsSubUser + "')]")
	private WebElement FieldNameDataAnalyticsSubUser;

	@FindBy(xpath = "//span[@class='cancelAnalyticsSec']")
	private WebElement cancelAnalyticsSec;

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesBtn;

	public void ClickOnRemoveBtn1() throws InterruptedException {

		if (FieldNameDataAnalyticsSubUser.isDisplayed()) {
			cancelAnalyticsSec.click();
			waitForXpathPresent("//p[text()='Are you sure you want to delete these analytics?']");
			sleep(1);
			YesBtn.click();
			waitForXpathPresent("//span[contains(text(),'Analytics Settings deleted successfully.')]");

			Reporter.log("'Analytics Settings deleted successfully.'", true);
			sleep(1);

		} else {

			ALib.AssertFailMethod("'Analytics Settings deleted successfully.' is not displayed");
		}
	}

	@FindBy(xpath = "//a[contains(text(),'Data Analytics')]")
	private WebElement DataAnalyticsSubUser;

	public void ClickOnDataAnalyticsTab() throws InterruptedException {

		DataAnalyticsSubUser.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='featPerm_tabCont']/div[1]/div/div[1]/span[text()='Add']")
	private WebElement AddButtonRoles;

	public void ClickOnAddButtonRoles() throws InterruptedException {
		AddButtonRoles.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Webhooks Settings']")
	private WebElement WebhooksSettings;

	public void VerifyWebhooksOptionIsAddedInRolesAccountSettings() throws InterruptedException {

		WebElement scrollDown2 = WebhooksSettings;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(2);

		Boolean WebhooksSettingsOptn = WebhooksSettings.isDisplayed();
		String Pass = "PASS>> " + WebhooksSettings + " is displayed";
		String Fail = "FAIL>> " + WebhooksSettings + " is NOT displayed";
		ALib.AssertTrueMethod(WebhooksSettingsOptn, Pass, Fail);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[18]/span[1]")
	private WebElement ExpandButtonAccountSettings;

	public void ClickOnExpandButtonAccountSettingsRoles() throws InterruptedException {
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);

		ExpandButtonAccountSettings.click();
		sleep(5);
	}

	public void ClickOnRolesOption() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
	}

	@FindBy(xpath = "//span[text()='User is disabled. Please contact your Admin.'] ")
	private WebElement UserdisabledWarningMessage;

	public void verifyWarningMessage() throws InterruptedException {

		String WarningMessage = UserdisabledWarningMessage.getText();
		System.out.println(WarningMessage);
		String Expected = WarningMessage;
		String Pass = "PASS>> 'User is disabled. Please contact your Admin.' is Displayed ";
		String Fail = "FAIL>> 'User is disabled. Please contact your Admin.' is Not Displayed";
		ALib.AssertEqualsMethod(Expected, WarningMessage, Pass, Fail);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.DemoUserDisabled
			+ "']//parent::tr/td[@class='bs-checkbox']")
	private WebElement InputDemouser;

	public void SearchDemouserToBeDisabled(String InputSuperUser) throws InterruptedException {

		SearchUser.sendKeys(InputSuperUser);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.DemoUserDisabled + "']");
		sleep(2);
	}

	public void SearchUser(String User) throws InterruptedException {
		SearchUser.clear();
		sleep(3);
		SearchUser.sendKeys(User);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + User + "']");
		sleep(2);
	}

	public void SearchDeletedUser(String User) throws InterruptedException {
		SearchUser.clear();
		sleep(3);
		SearchUser.sendKeys(User);
		sleep(5);
		try {
			Initialization.driver
			.findElement(By.xpath("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + User + "']"))
			.isDisplayed();
			ALib.AssertFailMethod("Delted User Still Exist");
		} catch (Exception e) {
			Reporter.log("Deleted user doesn't exist", true);
		}
	}

	public void VerifyProfilePermissionrolesto_move_folder_MacOs() throws InterruptedException {

		clickOnRolesOptions.click();
		sleep(2);

		clickOnAddButtonUserManagement.click();
		sleep(4);
		ExpandProfilePermission.click();
		sleep(2);
		WebElement scrollDown = MoveToFolderMacOs;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		String Option1 = MoveToFolderMacOs.getText();
		String ExpectedOption1 = "Move to Folder macOS Profile";
		String Pass = "PASS>>'Move to Folder macOS Profile' -  is Displayed";
		String Fail = "FAIL>>'Move to Folder macOS Profile' -  is Not Displayed";
		ALib.AssertEqualsMethod(ExpectedOption1, Option1, Pass, Fail);
		sleep(2);

		RolesCloseButton.click();
		sleep(5);
	}

	public void VerifyProfilePermissionrolesto_move_folder_Window() throws InterruptedException {

		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		ExpandProfilePermission.click();
		sleep(2);
		WebElement scrollDown = MoveToFolderWindow;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);
		String Option1 = MoveToFolderWindow.getText();
		String ExpectedOption1 = "Move to Folder Windows Profile";
		String Pass = "PASS>>'Move to Folder Windows Profile' -  is Displayed";
		String Fail = "FAIL>>'Move to Folder Windows Profile' -  is Not Displayed";
		ALib.AssertEqualsMethod(ExpectedOption1, Option1, Pass, Fail);
		sleep(2);
		RolesCloseButton.click();
		sleep(5);
	}

	public void VerifyProfilePermissionrolesto_move_folder_IOS() throws InterruptedException {

		clickOnRolesOptions.click();
		sleep(2);

		clickOnAddButtonUserManagement.click();
		sleep(4);
		ExpandProfilePermission.click();
		sleep(2);
		WebElement scrollDown = MoveToFolderIOS;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		String Option1 = MoveToFolderIOS.getText();
		String ExpectedOption1 = "Move to Folder iOS Profile";
		String Pass = "PASS>> 'Move to Folder iOS Profile' -  is Displayed";
		String Fail = "FAIL>>'Move to Folder iOS Profile'-  is Not Displayed";
		ALib.AssertEqualsMethod(ExpectedOption1, Option1, Pass, Fail);
		sleep(2);

		RolesCloseButton.click();
		sleep(5);
	}

	@FindBy(xpath = "//li[text()='" + Config.MoveToFolderAndroid + "']")
	private WebElement MoveToFolderAndroid;

	@FindBy(xpath = "//li[text()='" + Config.MoveToFolderIOS + "']")
	private WebElement MoveToFolderIOS;

	@FindBy(xpath = "//li[text()='" + Config.MoveToFolderWindow + "']")
	private WebElement MoveToFolderWindow;

	@FindBy(xpath = "//li[text()='" + Config.MoveToFolderMacOs + "']")
	private WebElement MoveToFolderMacOs;

	public void VerifyProfilePermissionrolesto_move_folder_Android() throws InterruptedException {

		clickOnRolesOptions.click();
		sleep(2);

		clickOnAddButtonUserManagement.click();
		sleep(4);
		ExpandProfilePermission.click();
		sleep(2);
		WebElement scrollDown = MoveToFolderAndroid;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		String Option1 = MoveToFolderAndroid.getText();
		String ExpectedOption1 = "Move to Folder Android Profile";
		String Pass = "PASS>> Move to Folder Android Profile -  is Displayed";
		String Fail = "FAIL>> Move to Folder Android Profile -  is Not Displayed";
		ALib.AssertEqualsMethod(ExpectedOption1, Option1, Pass, Fail);
		sleep(2);

		RolesCloseButton.click();
		sleep(5);
	}

	public void VerifyDisable_SelectUnapprovedAndroidEnterpriseApplications() throws InterruptedException {

		ExpandProfilePermission.click();
		sleep(2);

		WebElement scrollDown = Enable_SelectUnapprovedAndroidEnterpriseApplications;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		if (!Enable_SelectUnapprovedAndroidEnterpriseApplications.isSelected()) {
			Reporter.log("Is Disbaled", true);
		} else {
			ALib.AssertFailMethod("Fail");
		}
		RolesCloseButton.click();
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='PermissionsRolesGrid']/tbody/tr/td/p[text()='" + Config.SelectCreatedRoleInboxPermission
			+ "']")
	private WebElement InboxPermission;

	public void selectRoleInboxPermission() throws InterruptedException {

		InboxPermission.click();
		sleep(2);

	}

	public void EnableInboxPermissionRole() throws InterruptedException {

		ScrollToInboxPermissionRole();

		ClickOnInboxPermissionCheckBx.click();
		sleep(2);

		saveButton.click();
		sleep(5);

		String Actualvalue = RoleEditedSuccessfully.getText();
		String Expectedvalue = "Roles modified successfully.";
		String PassStatement = "PASS >>'Roles modified successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to modify in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='featPerm_tabCont']//span[text()='Edit']")
	private WebElement EditRole;

	public void ClickOnEdit() throws InterruptedException {

		EditRole.click();
		waitForXpathPresent("//h4[text()='Roles']");
		sleep(2);
	}

	public void RoleWithAllPermissionsWithSomeInboxPermission(String RoleName, String RoleDescriptionName)
			throws InterruptedException {
		// clickOnRolesOptions.click();
		// sleep(2);
		// clickOnAddButtonUserManagement.click();
		// sleep(4);
		NameTextField.sendKeys(RoleName);
		sleep(2);
		descriptionTestField.sendKeys(RoleDescriptionName);
		sleep(2);
		selectAllRolesPermissions.click();
		sleep(2);
		ScrollToInboxPermissionRole();
		ExpandInboxPermisson();
		Initialization.driver.findElement(By.xpath("//*[@id='umTree']/ul/li[11]/span[4]")).click(); // Disabling Reply
		// option inbox
		sleep(2);
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		// OtherPermissionCheckBox.click();
		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Roles created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(6);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[1]/span[3]")
	private WebElement GroupPermission;

	public void ClickOnGroupPermission() {
		GroupPermission.click();
	}

	public void RightClickOndeviceInsideGroups(String name) throws InterruptedException {
		Actions action = new Actions(Initialization.driver);
		JavascriptExecutor js1 = (JavascriptExecutor) Initialization.driver;
		action.contextClick(
				Initialization.driver.findElement(By.xpath("//p[text()='" + Config.DeviceInsideGroups + "']")))
		.perform();
		sleep(2);
	}

	@FindBy(xpath = "(//span[contains(text(),'Password do not match.')])[1]")
	private WebElement errorMessagePwdConfirmTexfield;

	public void VerifyWraningMessageWhenPwdIsWrongInConfirmTexfield(String InputUser, String Correctpswd,
			String Wrongpswd, String SelectRoles) throws InterruptedException {

		AddUserButton.click();
		sleep(3);
		UserName.sendKeys(InputUser);
		UserPasswoard.sendKeys(Correctpswd);
		ConfirmPasswoard.sendKeys(Wrongpswd);
		sleep(1);
		UserPasswoard.click();
		sleep(2);
		ConfirmPasswoard.click();
		sleep(1);

		Boolean ErrorPwdMessage = errorMessagePwdConfirmTexfield.isDisplayed();
		String Pass = "PASS>> " + errorMessagePwdConfirmTexfield + " is displayed";
		String Fail = "FAIL>> " + errorMessagePwdConfirmTexfield + " is NOT displayed";
		ALib.AssertTrueMethod(ErrorPwdMessage, Pass, Fail);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='usermg_resetPassword_popup']/div/div/div[1]/button")
	private WebElement CloseResetPwdBtn;

	public void ClickOnCloseResetPasswordButton() throws InterruptedException {

		CloseResetPwdBtn.click();
		sleep(2);
	}

	@FindBy(xpath = "//span[contains(text(),'Please provide a User Name.')]")
	private WebElement ErrorMessageForUsernamefield;

	public void VerifyErrorMessageForUsernamefield(String pswd) throws InterruptedException {

		AddUserButton.click();
		sleep(3);
		UserPasswoard.sendKeys(pswd);
		sleep(2);
		UserName.click();
		sleep(2);

		Boolean ErrorPwdMessage = ErrorMessageForUsernamefield.isDisplayed();
		String Pass = "PASS>> " + ErrorMessageForUsernamefield + " is displayed";
		String Fail = "FAIL>> " + ErrorMessageForUsernamefield + " is NOT displayed";
		ALib.AssertTrueMethod(ErrorPwdMessage, Pass, Fail);
		sleep(2);

		ConfirmPasswoard.sendKeys(pswd);
		sleep(2);

	}

	public void VerifyResettingCorrectPwd_confirm_pwd_textbox(String RetypePwd) throws InterruptedException {

		retypePasswordreset.clear();
		sleep(1);
		retypePasswordreset.sendKeys(RetypePwd);
		sleep(2);

		boolean flag = false;
		try {
			if (ResetPwdWarningMsg.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >> " + ResetPwdWarningMsg + " is NOT displayed";
		String FailStatement = "FAIL >> " + ResetPwdWarningMsg + " is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);

	}

	@FindBy(xpath = "//span[contains(text(),'Password do not match.')]")
	private WebElement ResetPwdWarningMsg;

	@FindBy(xpath = "//*[@id='userManagementGrid']/tbody/tr/td[text()='" + Config.InputUserDemouser10 + "']")
	private WebElement InputUserDemouser10;

	public void VerifyResettingWrongPwd_confirm_pwd_textbox(String SelectSubUser, String NewPwd, String RetypePwd)
			throws InterruptedException {
		InputUserDemouser10.click();
		sleep(2);
		ResetPasswordButton.click();
		sleep(2);
		newpassword_forreset.sendKeys(NewPwd);
		sleep(2);
		retypePasswordreset.sendKeys(RetypePwd);
		sleep(2);

		Boolean ErrorPwdMessage = ResetPwdWarningMsg.isDisplayed();
		String Pass = "PASS>> " + ResetPwdWarningMsg + " is displayed";
		String Fail = "FAIL>> " + ResetPwdWarningMsg + " is NOT displayed";
		ALib.AssertTrueMethod(ErrorPwdMessage, Pass, Fail);
		sleep(2);
	}

	public void VerifyWraningMessageWhenPwdIsCorrectInConfirmTexfield(String pswd) throws InterruptedException {

		ConfirmPasswoard.clear();
		sleep(1);
		ConfirmPasswoard.sendKeys(pswd);
		sleep(2);
		UserPasswoard.click();
		sleep(1);

		boolean flag = false;
		try {
			if (errorMessagePwdConfirmTexfield.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >> " + errorMessagePwdConfirmTexfield + " is NOT displayed";
		String FailStatement = "FAIL >> " + errorMessagePwdConfirmTexfield + " is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}

	public void NavigateUrl(String URL) throws InterruptedException {

		Initialization.driver.navigate().to(URL);
		sleep(10);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='File Store Permissions']")
	private WebElement FileStorePermissions;

	public void VerifyFileStorePermissionRole() throws InterruptedException {

		WebElement scrollDown = FileStorePermissions;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		Boolean FileStorePermissionRole = FileStorePermissions.isDisplayed();
		String pass = "PASS>> FileStorePermissions is Displayed";
		String fail = "FAIL>> FileStorePermissions is Not Displayed";
		ALib.AssertTrueMethod(FileStorePermissionRole, pass, fail);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='passworderr']/ul/li[text()='Passwords must have at least 8 characters and contain following: uppercase letters, lowercase letters, numbers and symbols.']")
	private WebElement ErrorMessagePasswordUserTab;

	public void VerifyCreateUserWraningMessageWhenPwdIsWrong(String InputUser, String pswd, String SelectRoles)
			throws InterruptedException {

		AddUserButton.click();
		sleep(3);
		UserName.sendKeys(InputUser);
		UserPasswoard.sendKeys(pswd);
		ConfirmPasswoard.sendKeys(pswd);
		FirstName.sendKeys(Config.FirstName);
		Email.sendKeys(Config.EmailID);
		sleep(3);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(2);
		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(SelectRoles);
		sleep(2);
		OkButton.click();
		sleep(5);
		WebElement scrollUp2 = UserPasswoard;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollUp2);
		sleep(2);
		Boolean ErrorPwdMessage = ErrorMessagePasswordUserTab.isDisplayed();
		String Pass = "PASS>> " + ErrorMessagePasswordUserTab + " is displayed";
		String Fail = "FAIL>> " + ErrorMessagePasswordUserTab + " is NOT displayed";
		ALib.AssertTrueMethod(ErrorPwdMessage, Pass, Fail);
		sleep(2);

	}

	public void ClickOnUserTab() throws InterruptedException {
		User.click();
		sleep(5);

	}

	public void VerifyFileStorePermissionInSatndardAccount() throws InterruptedException {

		boolean flag = false;
		try {
			if (FileStorePermissions.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			flag = false;
		}
		String PassStatement = "PASS >> " + FileStorePermissions + " is NOT displayed";
		String FailStatement = "FAIL >> " + FileStorePermissions + " is displayed";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}

	public void SelectRemotePermissionsInUser() {
		Select roles = new Select(RolesDropdown);
		roles.selectByVisibleText(Config.RoleName1);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[10]/span[4]")
	private WebElement AllowRemoteScreen;

	@FindBy(xpath = "//*[@id='umTree']/ul/li[11]/span[4]")
	private WebElement AllowRemoteFileExplorer;

	@FindBy(xpath = "//*[@id='umTree']/ul/li[12]/span[4]")
	private WebElement AllowRemoteClipboard;

	public void AllowRemoteScreen()

	{
		AllowRemoteScreen.click();
	}

	public void AllowRemoteFileExplorer()

	{
		AllowRemoteFileExplorer.click();
	}

	public void AllowRemoteClipboard()

	{
		AllowRemoteClipboard.click();
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[2]/span[1]")
	private WebElement ExpandDeviceActionPermission;

	public void ClickOnExpandDeviceActionPermission() throws InterruptedException {
		ExpandDeviceActionPermission.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[3]/span[1]")
	private WebElement ExpandDeviceManagementPermission;

	public void ClickOnExpandDeviceManagementPermission() throws InterruptedException {
		ExpandDeviceManagementPermission.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[6]/span[4]")
	private WebElement RemoteDeviceCheckbox;

	public void ClickOnRemoteDevice() throws InterruptedException {
		RemoteDeviceCheckbox.click();
		sleep(1);
	}

	public void EnableSomeOptionsUnderInbox() throws InterruptedException {

		ScrollToInboxPermissionRole();
		ExpandInboxPermisson();
		Initialization.driver.findElement(By.xpath("//*[@id='umTree']/ul/li[11]/span[4]")).click(); // Disabling Reply
		// option inbox
		sleep(2);
		saveButton.click();
		sleep(5);
		String Actualvalue = RoleEditedSuccessfully.getText();
		String Expectedvalue = "Roles modified successfully.";
		String PassStatement = "PASS >>'Roles modified successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to modify in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='permissionSelection_popup']/div/div/div[1]/button")
	private WebElement RolesCloseButton;

	public void VerifyEnable_SelectUnapprovedAndroidEnterpriseApplications() throws InterruptedException {

		WebElement scrollDown = Enable_SelectUnapprovedAndroidEnterpriseApplications;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		Boolean ProfilePermissionRole = Enable_SelectUnapprovedAndroidEnterpriseApplications.isEnabled();
		String pass = "PASS>> Enable_SelectUnapprovedAndroidEnterpriseApplications is Enabled";
		String fail = "FAIL>>Enable_SelectUnapprovedAndroidEnterpriseApplications is Not Enabled";
		ALib.AssertTrueMethod(ProfilePermissionRole, pass, fail);
		sleep(2);

		RolesCloseButton.click();
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='featPerm_tabCont']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement RolesSearchTextBox;

	public void ClickOnRoles() throws InterruptedException {

		clickOnRolesOptions.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='PermissionsRolesGrid']/tbody/tr/td/p[text()='" + Config.RoleSelect + "']")
	private WebElement SelectCreatedRole;

	public void selectRole() throws InterruptedException {

		SelectCreatedRole.click();
		sleep(2);
	}

	public void SearchRole(String InputRole) throws InterruptedException {

		RolesSearchTextBox.clear();
		sleep(2);
		RolesSearchTextBox.sendKeys(InputRole);
		sleep(4);
	}

	public void InputRolesDescription(String RoleName, String DescriptionName) throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys(RoleName);
		sleep(2);
		descriptionTestField.sendKeys(DescriptionName);
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='profileList_tableCont']/div/div[2]/div[1]/div[2]/div[1]/div/input")
	private WebElement SearchProfile;

	public void SearchProfile(String Profile) throws InterruptedException {

		SearchProfile.sendKeys(Profile);

		sleep(2);
	}

	public void ClickOnLocateDevice() throws InterruptedException {

		String originalHandle = Initialization.driver.getWindowHandle();
		sleep(10);
		LocateButton.click();
		sleep(7);
		Initialization.driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		sleep(5);

		boolean isdisplayed = true;

		try {
			LoctePageNewTAb.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL: 'Locte Device is not displayed'");
		Reporter.log("PASS>> 'Locate' page launched succesfully", true);

		waitForidPresent("realtimetab");
		sleep(8);
		HistoryButton.click();
		sleep(5);
		ClearHistoryLocation.click();
		sleep(5);
		Initialization.driver.findElement(By.xpath("//*[@id='deviceHistClear']/div/div/div[2]/button[text()='OK']"))
		.click();
		sleep(5);
		boolean AddAndroid = DeviceHistoryClearedPopUP.isDisplayed();
		String PassStatement1 = "PASS >>Device history cleared displayed";
		String FailStatement1 = "FAIL >> Device history cleared not displayed";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);
		modifyButtonLocation.click();
		waitForidPresent("modifyTable");
		sleep(2);
		CloseButtonModifyPopUP.click();
		sleep(3);

		sleep(2);
		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}
		sleep(5);

		Initialization.driver.switchTo().window(originalHandle);

		sleep(7);
	}

	@FindBy(xpath = "//*[@id='jobDataGrid']/tbody/tr/td/p[text()='donotdelete']")
	private WebElement donotdeleteJob;

	public void SelectTheJob() throws InterruptedException {

		donotdeleteJob.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[10]/span[3]")
	private WebElement ClickOnInboxPermissionCheckBx;

	public void RoleWithAllPermissionsExceptInboxPermission(String RoleName, String RoleDescriptionName)
			throws InterruptedException {
		// clickOnRolesOptions.click();
		// sleep(2);
		// clickOnAddButtonUserManagement.click();
		// sleep(4);
		NameTextField.sendKeys(RoleName);
		sleep(2);
		descriptionTestField.sendKeys(RoleDescriptionName);
		sleep(2);
		selectAllRolesPermissions.click();
		sleep(2);
		ScrollToInboxPermissionRole();
		ClickOnInboxPermissionCheckBx.click();
		sleep(2);
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		// OtherPermissionCheckBox.click();
		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Roles created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(6);

	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Reply']")
	private WebElement ReplyOptn;
	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Mark As Read']")
	private WebElement MarkAsReadOptn;
	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Mark As Unread']")
	private WebElement MarkAsUnReadOptn;
	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Delete']")
	private WebElement DeleteOptn;
	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Cleanup Inbox']")
	private WebElement CleanupInboxOptn;

	public void VerifyTheInboxPermissionOptns() throws InterruptedException {

		boolean replyOption = ReplyOptn.isDisplayed();
		String pass = "PASS>> ReplyOptn is displayed";
		String fail = "FAIL>> ReplyOptn is NOT displayed";
		ALib.AssertTrueMethod(replyOption, pass, fail);
		sleep(2);

		boolean MarkAsRead_Optn = MarkAsReadOptn.isDisplayed();
		String pass1 = "PASS>> MarkAsReadOptn is displayed";
		String fail1 = "FAIL>> MarkAsReadOptn is NOT displayed";
		ALib.AssertTrueMethod(MarkAsRead_Optn, pass1, fail1);
		sleep(2);

		boolean MarkAsUnRead_Optn = MarkAsUnReadOptn.isDisplayed();
		String pass2 = "PASS>> MarkAsUnReadOptn is displayed";
		String fail2 = "FAIL>> MarkAsUnReadOptn is NOT displayed";
		ALib.AssertTrueMethod(MarkAsUnRead_Optn, pass2, fail2);
		sleep(2);

		boolean Delete_Optn = DeleteOptn.isDisplayed();
		String pass3 = "PASS>> DeleteOptn is displayed";
		String fail3 = "FAIL>> DeleteOptn is NOT displayed";
		ALib.AssertTrueMethod(Delete_Optn, pass3, fail3);
		sleep(2);

		boolean CleanupInbox_Optn = CleanupInboxOptn.isDisplayed();
		String pass4 = "PASS>> CleanupInboxOptn is displayed";
		String fail4 = "FAIL>> CleanupInboxOptn is NOT displayed";
		ALib.AssertTrueMethod(CleanupInbox_Optn, pass4, fail4);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Inbox Permissions']")
	private WebElement ScrollToInboxPermission;

	@FindBy(xpath = "//*[@id='umTree']/ul/li[10]/span[1]")
	private WebElement ClickONExpandInboxPermission;

	public void ScrollToInboxPermissionRole() throws InterruptedException {

		WebElement scrollDownInbox = ScrollToInboxPermission;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDownInbox);
		sleep(4);
	}

	public void ClickOnAddButtonUM() throws InterruptedException {

		clickOnAddButtonUserManagement.click();

		sleep(2);
	}

	public void ExpandInboxPermisson() throws InterruptedException {

		ClickONExpandInboxPermission.click();
		waitForXpathPresent("//*[@id='umTree']/ul/li[text()='Reply']");
		sleep(2);

	}

	@FindBy(xpath = "//span[text()='Access denied. Please contact your administrator.']")
	private WebElement AccessDEniedMoveToFolder;

	public void VerifyAccessMeesage_OnClickOnNewFolder() throws InterruptedException {

		job_moveToFolder.click();
		waitForXpathPresent("//span[text()='Access denied. Please contact your administrator.']");
		sleep(2);
		boolean displayRoleCreated = AccessDEniedMoveToFolder.isDisplayed();
		String PassStatement3 = "PASS >> 'Access denied. Please contact your administrator.'is displayed";
		String FailStatement3 = "FAIL >> 'Access denied. Please contact your administrator.' Not displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void DeleteFolder(String FolderName) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//p[text()='" + FolderName + "']")).click();
		JobDeleteButton.click();
		sleep(2);
		YesDialogueBoxJobDelete.click();
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[9]/span[4]")
	private WebElement DisableMovetOfolder;

	public void DisableJobPermissionRole_Movejobstofolder() throws InterruptedException {
		ExpandJobPermission.click();
		waitForXpathPresent("//*[@id='umTree']/ul/li[text()='Move Jobs To Folder']");
		sleep(2);

		boolean displayRoleCreated = MoveToFolderPermission.isDisplayed();
		String PassStatement3 = "PASS >> Move Jobs To Folder is displayed";
		String FailStatement3 = "FAIL >> Move Jobs To Folder is Not displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);

		DisableMovetOfolder.click();
		sleep(2);

		saveButton.click();
		sleep(5);
		String Actualvalue = RoleEditedSuccessfully.getText();
		String Expectedvalue = "Role modified successfully.";
		String PassStatement = "PASS >>'Roles modified successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to modify in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='PermissionsRolesGrid']/tbody/tr/td/p[text()='" + Config.RoleSelectJobPermission + "']")
	private WebElement SelectCreatedRoleJobPermission;

	public void SelectCreatedRoleJobPermission() throws InterruptedException {

		SelectCreatedRoleJobPermission.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='moveJob_modal']/div[2]/div/ul/li[text()='DonotdeleteFolder']")
	private WebElement DonotdeleteFolder;

	public void ClickOnFolder() throws InterruptedException {
		DonotdeleteFolder.click();
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='okbtn']")
	private WebElement okbtn;

	public void ClickOnOkbTnMoveToFolder() throws InterruptedException {
		okbtn.click();
		waitForXpathPresent("//span[contains(text(),'successfully')]");
		System.out.println("Moved to folder successfully.");
		sleep(2);
	}

	@FindBy(xpath = "//*[@id='job_move']/span[text()='Move to Folder']")
	private WebElement job_moveToFolder;

	public void ClickOnjob_moveToFolder() throws InterruptedException {
		job_moveToFolder.click();
		waitForXpathPresent("//h4[text()='Move To']");
		sleep(2);
	}

	@FindBy(xpath = "(//*[@id='umTree']/ul/li[5]/span)[1]")
	private WebElement ExpandJobPermission;

	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Move Jobs To Folder']")
	private WebElement MoveToFolderPermission;

	public void EnableJobPermissionRole_Movejobstofolder() throws InterruptedException {
		ExpandJobPermission.click();
		waitForXpathPresent("//*[@id='umTree']/ul/li[text()='Move Jobs To Folder']");
		sleep(2);

		boolean displayRoleCreated = MoveToFolderPermission.isDisplayed();
		String PassStatement3 = "PASS >> Move Jobs To Folder is displayed";
		String FailStatement3 = "FAIL >> Move Jobs To Folder is Not displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		selectAllRolesPermissions.click();
		sleep(2);
		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(6);
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[1]/span[1]")
	private WebElement ClickOnGroupExpandButton;

	public void CreateRoleProfilePermission(String InputRole, String InputDescp) throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys(InputRole); // Profile Permission
		sleep(2);

		descriptionTestField.sendKeys(InputDescp);// FOR USER pij
		sleep(2);
		// clickonGroupPermissionUM.click();
		ClickOnGroupExpandButton.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		// clickonGroupPermissionUM.click();
		ClickOnGroupExpandButton.click();
		sleep(2);
		ProfilePermissionCheckBox.click();
		saveButton.click();
		sleep(4);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[12]/span[1]")
	private WebElement ExpandProfilePermission;

	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='Select unapproved Android Enterprise applications']/span[4]")
	private WebElement Enable_SelectUnapprovedAndroidEnterpriseApplications;

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[12]")
	private WebElement ScrollDownToElementProfile;

	public void CreateRoleProfilePermission_SelectUnapprovedAndroidEnterpriseApplications()
			throws InterruptedException {

		UsermanagemnetCheckBox.click();
		sleep(4);

		ExpandProfilePermission.click();
		sleep(2);
		WebElement scrollDown = Enable_SelectUnapprovedAndroidEnterpriseApplications;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);
		Enable_SelectUnapprovedAndroidEnterpriseApplications.click();
		sleep(2);
		GroupPermissionCheckBox.click();
		sleep(2);
		saveButton.click();
		sleep(4);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
	}

	@FindBy(linkText = "Login again")
	private WebElement LoginAgain;

	public void ClickOnLoginAgainLink() throws InterruptedException {
		try {
			LoginAgain.click();
			waitForidPresent("uName");
			System.out.println("Clicked on Login again link");
			sleep(2);
		} catch (Exception e) {

		}
	}

	@FindBy(xpath = "//a[text()='Login again']")
	private WebElement LoginLink;

	public void ClickOnLoginLink() throws InterruptedException {
		LoginLink.click();
		Reporter.log("PASS >> clicked on login link", true);
		sleep(4);
		waitForXpathPresent("//p[text()=' � 2010-2020. 42Gears Mobility Systems Pvt Ltd. All rights reserved']");
		sleep(2);

	}

	public void UserManagementOptionPresentOrNot() throws InterruptedException {
		boolean var;
		try {
			usermanagementlink.isDisplayed();
			var = false;
		} catch (Exception e) {
			var = true;

		}
		String PassStatement = "PASS >> User Management option is not available";
		String FailStatement = "User Management option is available";
		ALib.AssertTrueMethod(var, PassStatement, FailStatement);

	}

	public void ClickOnUserManagementOption() {
		usermanagementlink.click();
		WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 20);
		wait1.until(ExpectedConditions.elementToBeClickable(usermanagementtext));
	}

	@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceTextField;

	public void SearchDeviceInconsole()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {

		SearchDeviceTextField.clear();
		sleep(3);
		SearchDeviceTextField.sendKeys(Config.DeviceName);
		waitForXpathPresent("//p[text()='" + Config.DeviceName + "']");
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='featPerm_tabCont']/div[1]/div/div[1]/i")
	private WebElement clickOnAddButtonUserManagement;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[1]/span[1]")
	private WebElement clickonGroupPermissionUM;
	@FindBy(xpath = ".//*[@id='permissionSelection_popup']/div/div/div[2]/div[3]/div[1]/div/span[2]")
	private WebElement waituptoselect;

	public void clickOnAddButtonUserManagement() throws InterruptedException {

		clickOnAddButtonUserManagement.click();
		Initialization.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// List<WebElement> optionvalues=
		// Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(5);
	}

	public void clickOnGroupPermissions() throws InterruptedException {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		// WebDriverWait wait1 = new WebDriverWait(Initialization.driver, 20);
		// wait1.until(ExpectedConditions.elementToBeClickable(waituptoselect));
		// boolean isdisplayed=true;
		//
		// try{
		// waituptoselect.isDisplayed();
		//
		// }catch(Throwable e)
		// {
		// isdisplayed=false;
		//
		// }
		// clickonGroupPermissionUM.click();
		//
		//
		//// List<WebElement> optionvalues=
		// Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		// Initialization.driver.manage().timeouts().implicitlyWait(20,
		// TimeUnit.SECONDS);
		//
		// Reporter.log("========Verify 'Group Permission' parameters one by
		// one=========")clickonGroupPermissionUM.click();

		for (int i = 1; i < 6; i++) {
			if (i == 1) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "New Group/Tag/Filter";
				String PassStatement = "PASS >> new group option is present - correct";
				String FailStatement = "FAIL >> new group option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 2) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Rename Group/Tag/Filter";
				String PassStatement = "PASS >> Rename Group option is present - correct";
				String FailStatement = "FAIL >> Rename Group option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 3) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete Group/Tag/Filter";
				String PassStatement = "PASS >> Delete Group option is present - correct";
				String FailStatement = "FAIL >> Delete Group option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 4) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add/Delete Default Group/Tag Job";
				String PassStatement = "PASS >> Add/Delete Default Group Job option is present - correct";
				String FailStatement = "FAIL >> Add/Delete Default Group Job option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 5) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Assign Group";
				String PassStatement = "PASS >> Assign Group option is present - correct";
				String FailStatement = "FAIL >> Assign Group option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 6) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Allow Home Group";
				String PassStatement = "PASS >> Allow Home Group option is present - correct";
				String FailStatement = "FAIL >> Allow Home Group option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
		clickonGroupPermissionUM.click();
		sleep(5);

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[2]/span[1]")
	private WebElement deviceactiondropdown;

	public void DeviceAction() throws InterruptedException {
		deviceactiondropdown.click();
		sleep(2);
	}

	public void VerifyDeviceActionPermission() throws InterruptedException {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		for (int i = 2; i < 21; i++) {
			if (i == 2) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Apply Job";
				String PassStatement = "PASS >> Apply Job OPtion is present - correct";
				String FailStatement = "FAIL >> Apply job option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 3) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Apply Profile";
				String PassStatement = "PASS >>Apply Profile OPtion is present - correct";
				String FailStatement = "FAIL >> Apply Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 4) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Remove Queued Job";
				String PassStatement = "PASS >> Remove Queued Job option is present - correct";
				String FailStatement = "FAIL >> Remove Queued Job option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 5) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Remote Device";
				String PassStatement = "PASS >> Remote Device option is present - correct";
				String FailStatement = "FAIL >> Remote Deviceoption is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 6) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Locate Device";
				String PassStatement = "PASS >> Locate Device option is present - correct";
				String FailStatement = "FAIL >> Locate Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 7) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Send Message to Device";
				String PassStatement = "PASS >> Send Message to Device option is present - correct";
				String FailStatement = "FAIL >> Send Message to Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 8) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Reboot Device";
				String PassStatement = "PASS >> Reboot Device OPtion is present - correct";
				String FailStatement = "FAIL >>Reboot Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 9) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Lock Device";
				String PassStatement = "PASS >> Lock Device option is present - correct";
				String FailStatement = "FAIL >> Lock Device  option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 10) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Wipe Device";
				String PassStatement = "PASS >> Wipe Device option is present - correct";
				String FailStatement = "FAIL >> Wipe Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			// if(i==11)
			// {
			// String actual =optionvalues.get(i).getText();
			// Reporter.log(actual);
			// String expected = "Delete Message";
			// String PassStatement ="PASS >>Delete Message option is present - correct";
			// String FailStatement ="FAIL >>Delete Message option is NOT present -
			// incorrect";
			// ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			// }
			// if(i==12)
			// {
			// String actual =optionvalues.get(i).getText();
			// Reporter.log(actual);
			// String expected = "Reply Message";
			// String PassStatement ="PASS >> Reply Message option is present - correct";
			// String FailStatement ="FAIL >>Reply Message option is NOT present -
			// incorrect";
			// ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			// }

			if (i == 11) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Owner Name Change";
				String PassStatement = "PASS >>Device Owner Name Change OPtion is present - correct";
				String FailStatement = "FAIL >> Device Owner Name Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 12) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Notes Change";
				String PassStatement = "PASS >> Device Notes Change option is present - correct";
				String FailStatement = "FAIL >> Device Notes Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 13) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Call Log Device";
				String PassStatement = "PASS >> Call Log Device option is present - correct";
				String FailStatement = "FAIL >> Call Log Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 14) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "SureLock Settings Change";
				String PassStatement = "PASS >> SureLock Settings Change option is present - correct";
				String FailStatement = "FAIL >> SureLock Settings Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 15) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "SureFox Settings Change";
				String PassStatement = "PASS >> SureFox Settings Change option is present - correct";
				String FailStatement = "FAIL >> SureFox Settings Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 16) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "SureVideo Settings Change";
				String PassStatement = "PASS >>SureVideo Settings Change OPtion is present - correct";
				String FailStatement = "FAIL >>SureVideo Settings Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 17) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "SMS Logs";
				String PassStatement = "PASS >> SMS Logs option is present - correct";
				String FailStatement = "FAIL >> SMS Logs option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 18) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Data Usage";
				String PassStatement = "PASS >>Data Usage option is present - correct";
				String FailStatement = "FAIL >>Data Usage option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 19) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Remote Buzz";
				String PassStatement = "PASS >>Remote Buzz option is present - correct";
				String FailStatement = "FAIL >>Remote Buzz option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
		deviceactiondropdown.click();
		sleep(2);

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[3]/span[1]")
	private WebElement clickOnDeviceManagemengtPermission;

	public void clickDeviceManagementPermission() throws InterruptedException {
		clickOnDeviceManagemengtPermission.click();
		sleep(2);
	}

	public void verifyDeviceManagementPermission() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		for (int i = 3; i < 8; i++) {

			if (i == 3) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete Device";
				String PassStatement = "PASS >> Delete Device option is present - correct";
				String FailStatement = "FAIL >>Delete Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 4) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Blocklist Device";
				String PassStatement = "PASS >> Blacklist Device option is present - correct";
				String FailStatement = "FAIL >> Blacklist Device is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 5) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Allowlist Device";
				String PassStatement = "PASS >> Whitelist Device option is present - correct";
				String FailStatement = "FAIL >> Whitelist Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 6) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Approve Device";
				String PassStatement = "PASS >> Approve Device option is present - correct";
				String FailStatement = "FAIL >>Approve Device option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 7) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Automatically Approve Devices";
				String PassStatement = "PASS >> Automatically Approve Devices OPtion is present - correct";
				String FailStatement = "FAIL >>Automatically Approve Devices option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 8) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Import Preapproved Devices";
				String PassStatement = "PASS >> Import Preapproved Devices option is present - correct";
				String FailStatement = "FAIL >> Import Preapproved Devices option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
		}
		clickOnDeviceManagemengtPermission.click();
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[4]/span[1]")
	private WebElement ApplicationSettingPpermission;

	public void clickApplicationSettingsPermissions() throws InterruptedException {
		ApplicationSettingPpermission.click();
		sleep(1);
	}

	public void verifyApplicationSettingsPermission() throws InterruptedException {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));

		for (int i = 4; i < 7; i++) {
			if (i == 4) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Lock Application";
				String PassStatement = "PASS >> Lock Application option is present - correct";
				String FailStatement = "FAIL >>Lock Application is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 5) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Uninstall Application";
				String PassStatement = "PASS >> Uninstall Application option is present - correct";
				String FailStatement = "FAIL >> Uninstall Application option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 6) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Application Run at Startup";
				String PassStatement = "PASS >>Application Run at Startup option is present - correct";
				String FailStatement = "FAIL >>Application Run at Startup option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 7) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Clear Application Data";
				String PassStatement = "PASS >> Clear Application Data OPtion is present - correct";
				String FailStatement = "FAIL >>Clear Application Data option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
		ApplicationSettingPpermission.click();

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[5]/span[1]")
	private WebElement jobPermission;

	public void clickonJobPermission() {
		jobPermission.click();

	}

	public void verifyJobPermessionOptions() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		for (int i = 5; i < 7; i++) {
			if (i == 5) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "New Job";
				String PassStatement = "PASS >> New Job option is present - correct";
				String FailStatement = "FAIL >> New Job option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 6) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete Job";
				String PassStatement = "PASS >>Delete Job option is present - correct";
				String FailStatement = "FAIL >>Delete Job option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 7) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Modify Job";
				String PassStatement = "PASS >> Modify Job OPtion is present - correct";
				String FailStatement = "FAIL >>Modify Job option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
		jobPermission.click();
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[6]/span[1]")
	private WebElement ReportPermission;

	public void clickOnReportPermission() {
		ReportPermission.click();
	}

	public void verifyReportPermissions() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		for (int i = 6; i < 15; i++) {
			if (i == 6) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "System Log Report";
				String PassStatement = "PASS >> System Log Report option is present - correct";
				String FailStatement = "FAIL >> System Log Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 7) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Health Report";
				String PassStatement = "PASS >>Device Health Report OPtion is present - correct";
				String FailStatement = "FAIL >>Device Health Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 8) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Device Connected Report";
				String PassStatement = "PASS >> Device Connected Report option is present - correct";
				String FailStatement = "FAIL >> Device Connected Report  option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 9) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Job Deployed Report";
				String PassStatement = "PASS >> Job Deployed Report option is present - correct";
				String FailStatement = "FAIL >> Job Deployed Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 10) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Asset Tracking Report";
				String PassStatement = "PASS >>Asset Tracking Report option is present - correct";
				String FailStatement = "FAIL >>Asset Tracking Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 11) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Call Log Tracking Report";
				String PassStatement = "PASS >> Call Log Tracking Reporte option is present - correct";
				String FailStatement = "FAIL >>Call Log Tracking Reporte option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 12) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "App Version Tracking Report";
				String PassStatement = "PASS >>App Version Tracking Report OPtion is present - correct";
				String FailStatement = "FAIL >> App Version Tracking Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 13) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Device History Report";
				String PassStatement = "PASS >> Device History Report option is present - correct";
				String FailStatement = "FAIL >> Device History Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 14) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Data Usage Report";
				String PassStatement = "PASS >> Data Usage Report option is present - correct";
				String FailStatement = "FAIL >> Data Usage Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 15) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Installed Job Report";
				String PassStatement = "PASS >> Installed Job Report option is present - correct";
				String FailStatement = "FAIL >> Installed Job Report option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
		ReportPermission.click();
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[8]/span[1]")
	private WebElement UserManagementPermissions;

	public void clickonUserManagenemtPermission() {
		UserManagementPermissions.click();
	}

	public void verifyUserManagementPermission() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));

		for (int i = 8; i < 12; i++) {
			if (i == 8) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add User";
				String PassStatement = "PASS >>Add User OPtion is present - correct";
				String FailStatement = "FAIL >>Add User option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 9) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Edit User";
				String PassStatement = "PASS >>Edit User option is present - correct";
				String FailStatement = "FAIL >>Edit User option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 10) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete User";
				String PassStatement = "PASS >> Delete User option is present - correct";
				String FailStatement = "FAIL >> Delete User option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 11) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Change Password";
				String PassStatement = "PASS >>Change Password option is present - correct";
				String FailStatement = "FAIL >>Change Passwordt option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 12) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Enable or Disable User";
				String PassStatement = "PASS >> Enable or Disable User option is present - correct";
				String FailStatement = "FAIL >>Enable or Disable User option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
		UserManagementPermissions.click();
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[9]/span[1]")
	private WebElement DashboardPermissions;

	public void clickonDashboardPermission() {
		DashboardPermissions.click();
	}

	public void verifyDashboardOptions() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));

		for (int i = 9; i < 19; i++) {
			if (i == 9) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "OS Platforms";
				String PassStatement = "PASS >> OS Platforms option is present - correct";
				String FailStatement = "FAIL >> OS Platforms  option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 10) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Available Battery";
				String PassStatement = "PASS >> Available Battery option is present - correct";
				String FailStatement = "FAIL >> Available Battery option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 11) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Available RAM";
				String PassStatement = "PASS >>Available RAM option is present - correct";
				String FailStatement = "FAIL >>Available RAM option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 12) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Available Storage";
				String PassStatement = "PASS >> Available Storage option is present - correct";
				String FailStatement = "FAIL >>Available Storage option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 13) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Last Connected";
				String PassStatement = "PASS >>Last Connected OPtion is present - correct";
				String FailStatement = "FAIL >> Last Connected option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 14) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Online/Offline";
				String PassStatement = "PASS >> Online/Offline option is present - correct";
				String FailStatement = "FAIL >> Online/Offline option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 15) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Device SIM Status";
				String PassStatement = "PASS >>Device SIM Status option is present - correct";
				String FailStatement = "FAIL >>Device SIM Status option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 16) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Roaming Status";
				String PassStatement = "PASS >>Roaming Status option is present - correct";
				String FailStatement = "FAIL >>Roaming Status Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 17) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Unread Mail";
				String PassStatement = "PASS >>Unread Mail option is present - correct";
				String FailStatement = "FAIL >>Unread Mail option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 18) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Unapproved Devices";
				String PassStatement = "PASS >>Unapproved Devices OPtion is present - correct";
				String FailStatement = "FAIL >>Unapproved Devices option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 19) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Jobs";
				String PassStatement = "PASS >>Jobs option is present - correct";
				String FailStatement = "FAIL >>Jobs option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
		}

		DashboardPermissions.click();
	}

	@FindBy(xpath = "//li[text()='File Store Permissions']/span")
	private WebElement FileStorePermission;

	public void clickonFileStorePermission() {
		FileStorePermission.click();
	}

	public void verifyFileStorePermission() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		for (int i = 11; i < 12; i++) {
			if (i == 11) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "New Folder";
				String PassStatement = "PASS >> New Folder option is present - correct";
				String FailStatement = "FAIL >>New Folder  option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 12) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Upload Files";
				String PassStatement = "PASS >>Upload Files option is present - correct";
				String FailStatement = "FAIL >>Upload Files option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 13) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete File/Folder";
				String PassStatement = "PASS >> Delete File/Folder option is present - correct";
				String FailStatement = "FAIL >>Delete File/Folder option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

		}
		FileStorePermission.click();
	}

	@FindBy(xpath = "//li[text()='Profile Permissions']/span")
	private WebElement ProfilePermissions;

	public void clickonProfilePermission() {
		ProfilePermissions.click();
	}

	public void verifyProfilePermissionOptions() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		for (int i = 12; i < 32; i++) {
			if (i == 12) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add Android Profile";
				String PassStatement = "PASS >Add Android Profile option is present - correct";
				String FailStatement = "FAIL >Add Android Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 13) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Edit Android Profile";
				String PassStatement = "PASS >> Edit Android Profile option is present - correct";
				String FailStatement = "FAIL >>Edit Android Profile is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 14) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete Android Profile";
				String PassStatement = "PASS >>Delete Android Profile OPtion is present - correct";
				String FailStatement = "FAIL >> Delete Android Profilee option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 15) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Set Default Android Profile";
				String PassStatement = "PASS >> Set Default Android Profile option is present - correct";
				String FailStatement = "FAIL >> Set Default Android Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}

			if (i == 16) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Move to Folder Android Profile";
				String PassStatement = "PASS >>Move to Folder Android Profile option is present - correct";
				String FailStatement = "FAIL >> Move to Folder Android Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 17) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Enroll AFW Android Profile";
				String PassStatement = "PASS >> Enroll AFW Android Profile option is present - correct";
				String FailStatement = "FAIL >> Enroll AFW Android Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 18) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Settings Android Profile";
				String PassStatement = "PASS >> Add iOS Profile Change option is present - correct";
				String FailStatement = "FAIL >> Add iOS Profile Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 19) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add iOS Profile";
				String PassStatement = "PASS >> Add iOS Profile Change option is present - correct";
				String FailStatement = "FAIL >> Add iOS Profile Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 20) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Edit iOS Profile";
				String PassStatement = "PASS >> Edit iOS Profile option is present - correct";
				String FailStatement = "FAIL >> Edit iOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 21) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete iOS Profile";
				String PassStatement = "PASS >>Delete iOS Profile OPtion is present - correct";
				String FailStatement = "FAIL >>Delete iOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 22) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Set Default iOS Profile";
				String PassStatement = "PASS >> Set Default iOS Profile option is present - correct";
				String FailStatement = "FAIL >> Set Default iOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}

			if (i == 23) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Move to Folder iOS Profile";
				String PassStatement = "PASS >>Add Windows Profile option is present - correct";
				String FailStatement = "FAIL >>Add Windows Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 24) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add Windows Profile";
				String PassStatement = "PASS >>Add Windows Profile option is present - correct";
				String FailStatement = "FAIL >>Add Windows Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 25) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Edit Windows Profile";
				String PassStatement = "PASS >>Edit Windows Profile option is present - correct";
				String FailStatement = "FAIL >>Edit Windows Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 26) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete Windows Profile";
				String PassStatement = "PASS >> Delete Windows Profile option is present - correct";
				String FailStatement = "FAIL >> Delete Windows Profile  option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 27) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Set Default Windows Profile";
				String PassStatement = "PASS >> Set Default Windows Profile option is present - correct";
				String FailStatement = "FAIL >> Set Default Windows Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 28) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Move to Folder Windows Profile";
				String PassStatement = "PASS >>Add macOS Profile option is present - correct";
				String FailStatement = "FAIL >>Add macOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 29) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add macOS Profile";
				String PassStatement = "PASS >>Add macOS Profile option is present - correct";
				String FailStatement = "FAIL >>Add macOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 30) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Edit macOS Profile";
				String PassStatement = "PASS >>Edit macOS Profile option is present - correct";
				String FailStatement = "FAIL >>Edit macOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 31) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Delete macOS Profile";
				String PassStatement = "PASS >>Delete macOS Profile OPtion is present - correct";
				String FailStatement = "FAIL >> Delete macOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 32) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Set Default macOS Profile";
				String PassStatement = "PASS >> Set Default macOS Profile option is present - correct";
				String FailStatement = "FAIL >> Set Default macOS Profile option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}

		}
		ProfilePermissions.click();
	}

	@FindBy(xpath = "//li[text()='App Store Permissions']/span")
	private WebElement AppStorePermission;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[14]")
	private WebElement ScrollDownToElement;

	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

	public void clickOnAppStorePermission() {
		AppStorePermission.click();
	}

	public void verifyAppStorePermission() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));
		for (int i = 13; i < 18; i++) {
			if (i == 13) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add Android";
				String PassStatement = "PASS >> Add Android option is present - correct";
				String FailStatement = "FAIL >>Add Android option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}

			if (i == 14) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Edit Android";
				String PassStatement = "PASS >>Edit Android OPtion is present - correct";
				String FailStatement = "FAIL >> Edit Android option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 15) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Remove Android";
				String PassStatement = "PASS >> Remove Android option is present - correct";
				String FailStatement = "FAIL >> Remove Android option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
			if (i == 16) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Add iOS";
				String PassStatement = "PASS >>Add iOS option is present - correct";
				String FailStatement = "FAIL >>Add iOS option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 17) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Edit iOS";
				String PassStatement = "PASS >>Edit iOS option is present - correct";
				String FailStatement = "FAIL >>Edit iOS Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 18) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Remove iOS";
				String PassStatement = "PASS >>Remove iOS option is present - correct";
				String FailStatement = "FAIL >>Remove iOS option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}

		AppStorePermission.click();
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	}

	@FindBy(xpath = "//li[text()='Other Permissions']/span")
	private WebElement OtherPermissions;
	// WebElement scrollDown = ScrollDownToElement;

	public void clickonOtherPermission() {
		// js.executeScript("arguments[0].scrollIntoView(true);",scrollDown);
		OtherPermissions.click();
		// WebElement scrollDown = ScrollDownToElement;
		// js.executeScript("arguments[0].scrollIntoView(true);",scrollDown);
		//
	}

	public void verifyOtherPermissions() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));

		for (int i = 14; i <= 15; i++) {
			if (i == 14) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Activity Log";
				String PassStatement = "PASS >>Activity Log OPtion is present - correct";
				String FailStatement = "FAIL >> Activity Log option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 15) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Security";
				String PassStatement = "PASS >> Change Password option is present - correct";
				String FailStatement = "FAIL >> Change Password option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

			}
		}
		OtherPermissions.click();
	}

	@FindBy(xpath = "//li[text()='Location Permissions']/span")
	private WebElement LocationPermission;

	public void clickLocationPermission() {
		LocationPermission.click();
	}

	public void verifyLoctaionPermission() {
		List<WebElement> optionvalues = Initialization.driver.findElements(By.xpath(".//div[@id='umTree']/ul/li"));

		for (int i = 15; i < 19; i++) {
			if (i == 15) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Clear History";
				String PassStatement = "PASS >>Clear History option is present - correct";
				String FailStatement = "FAIL >>Clear History option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 16) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Modify History";
				String PassStatement = "PASS >>Modify History option is present - correct";
				String FailStatement = "FAIL >>Modify History option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
			if (i == 17) {
				String actual = optionvalues.get(i).getText();
				Reporter.log(actual);
				String expected = "Export History";
				String PassStatement = "PASS >>Export History option is present - correct";
				String FailStatement = "FAIL >>Export History Change option is NOT present - incorrect";
				ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
			}
		}
		LocationPermission.click();

	}

	@FindBy(xpath = "//span[text()='Enter Template Name.']")
	private WebElement alertMessageWhenSavedWithoutName;
	@FindBy(xpath = ".//*[@id='SavePermissions']")
	private WebElement saveButton;

	// Error Templete is missing
	public void errorTempleteMissing() throws InterruptedException {
		saveButton.click();
		sleep(4);
		String Actualvalue = alertMessageWhenSavedWithoutName.getText();
		String Expectedvalue = "Enter Template Name.";
		String PassStatement = "PASS >>'Enter Template Name.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

	}

	@FindBy(xpath = "//span[text()='No permissions selected.']")
	private WebElement NoPermissionSelected;
	@FindBy(xpath = ".//*[@id='templateName']")
	private WebElement NameTextField;

	// Error message for no permission selected
	public void errorMessageNoPermissionSlected() throws InterruptedException {
		NameTextField.sendKeys("DemoRole1");
		saveButton.click();
		sleep(4);
		String Actualvalue = NoPermissionSelected.getText();
		String Expectedvalue = "No permissions selected.";
		String PassStatement = "PASS >>'No permissions selected.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
		NameTextField.clear();
	}

	public void SendingRoleName(String RoleName) throws InterruptedException {
		NameTextField.sendKeys(RoleName);
		sleep(3);
	}

	public void ClickOnRoleSaveButton() throws InterruptedException {
		saveButton.click();
		waitForXpathPresent("//span[text()='Role created successfully.']");
		sleep(3);
		Reporter.log("Clicked On role Save Button",true);
	}
	// create a role with ddescription

	@FindBy(xpath = ".//*[@id='TemplateDescription']")
	private WebElement descriptionTestField;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[1]/span[3]")
	private WebElement GroupPermissionCheckbox;
	@FindBy(xpath = "//span[text()='Role created successfully.']")
	private WebElement RoleCreatedSuccessfully;

	public void CreateRoleDescription() throws InterruptedException {
		NameTextField.clear();
		NameTextField.sendKeys("Automation Role");
		sleep(2);
		descriptionTestField.sendKeys("Automation Demo");
		sleep(2);
		GroupPermissionCheckbox.click();
		saveButton.click();
		sleep(3);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
	}

	@FindBy(xpath = ".//*[@id='featPerm_tabCont']/div[1]/div/div[3]/i")
	private WebElement cloneButton;
	@FindBy(xpath = ".//*[text()='Super User']")
	private WebElement superuser1;

	public void verifyOptionsOnSelectingSuperUser() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(4);
		superusertemplete.click();
		sleep(4);
		boolean displayCloneButton = cloneButton.isDisplayed();
		String PassStatement1 = "PASS >> clone button is  displayed ";
		String FailStatement1 = "FAIL >> clone button is  not been displayed";
		ALib.AssertTrueMethod(displayCloneButton, PassStatement1, FailStatement1);
		sleep(3);
		// superusertemplete.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='Automation Role']")
	private WebElement automationRole;
	@FindBy(xpath = ".//*[@id='featPerm_tabCont']/div[1]/div/div[2]/i")
	private WebElement EditOption;
	@FindBy(xpath = ".//*[@id='featPerm_tabCont']/div[1]/div/div[4]/i")
	private WebElement DeleteOption;

	public void verifyOtionsSelectingRole() throws InterruptedException {
		automationRole.click();
		sleep(4);
		boolean displayEditeButton = EditOption.isDisplayed();
		String PassStatement1 = "PASS >> Edit has been displayed ";
		String FailStatement1 = "FAIL >> Edit has not been displayed";
		ALib.AssertTrueMethod(displayEditeButton, PassStatement1, FailStatement1);
		sleep(3);
		boolean displayCloneButton = cloneButton.isDisplayed();
		String PassStatement2 = "PASS >> clone button been displayed ";
		String FailStatement2 = "FAIL >> clone button is not been displayed";
		ALib.AssertTrueMethod(displayCloneButton, PassStatement2, FailStatement2);
		sleep(4);
		boolean displayDeleteButton = DeleteOption.isDisplayed();
		String PassStatement3 = "PASS >> Delete option has been displayed ";
		String FailStatement3 = "FAIL >> Delete has not been displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
	}

	@FindBy(xpath = ".//*[@id='featPerm_tabCont']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement searchTextField;
	@FindBy(xpath = ".//*[@id='featPerm_tabCont']/div[2]/div[1]/div[1]/div[1]/button")
	private WebElement RefreshButton;

	public void searchRefreshOptions() throws InterruptedException {
		searchTextField.sendKeys("Automation Role");
		sleep(2);
		// RefreshButton.click();
		sleep(2);
		// searchTextField.clear();
		sleep(4);
		// RefreshButton.click();

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[8]/span[3]")
	private WebElement devicePermissionCheckBox;
	@FindBy(xpath = "//span[text()='Role modified successfully.']")
	private WebElement RoleEditedSuccessfully;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[2]/span[3]")
	private WebElement DevicePermissionNotExpanded;

	public void ClickandEditRole() throws InterruptedException {
		automationRole.click();
		sleep(4);
		EditOption.click();
		sleep(4);
		NameTextField.sendKeys("####");
		sleep(4);
		DevicePermissionNotExpanded.click();
		sleep(4);
		saveButton.click();
		sleep(3);
		boolean displayRoleCreated = RoleEditedSuccessfully.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		RefreshButton.click();
		sleep(4);

	}

	@FindBy(xpath = ".//*[text()='Automation Role####']")
	private WebElement EditedRoleTEXT;
	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement YesConfirmatinDialogbox;
	@FindBy(xpath = "//span[text()='User(s) assigned template(s) cannot be deleted.']")
	private WebElement AssignedUserTempleteCannotBeDeleted;

	public void ClickOnDeleteRole() throws InterruptedException {
		EditedRoleTEXT.click();
		// automationRole.click();
		sleep(5);
		DeleteOption.click();
		sleep(5);
		YesConfirmatinDialogbox.click();
		sleep(4);
		boolean displayRoleCannotBeDeleted = AssignedUserTempleteCannotBeDeleted.isDisplayed();
		String PassStatement3 = "PASS >> Templete cannot be deleted has been displayed ";
		String FailStatement3 = "FAIL >> Templete cannot be deleted not been displayed";
		ALib.AssertTrueMethod(displayRoleCannotBeDeleted, PassStatement3, FailStatement3);
		sleep(7);
	}

	@FindBy(xpath = "//span[text()='Roles cloned successfully.']")
	private WebElement RoleClonedSuccessfully;

	public void CloneOptionWorking() throws InterruptedException {
		EditedRoleTEXT.click();
		sleep(4);
		cloneButton.click();
		sleep(3);
		NameTextField.sendKeys("Automation clone");
		sleep(2);
		jobPermission.click();
		sleep(2);
		saveButton.click();
		sleep(3);
		boolean displayRoleClonedSuccessfully = RoleClonedSuccessfully.isDisplayed();
		String PassStatement3 = "PASS >> Role cloned  Successfully has been displayed ";
		String FailStatement3 = "FAIL >> Role cloned  Successfully not been displayed";
		ALib.AssertTrueMethod(displayRoleClonedSuccessfully, PassStatement3, FailStatement3);
		sleep(5);

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[3]/span[3]")
	private WebElement MAnagementPermission;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[1]/span[3]")
	private WebElement GroupPermissionCheckBox;

	public void CreateRoleForGroupPermissions() throws InterruptedException {
		// clickOnRolesOptions.click();
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Group Permissions");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER A");
		sleep(2);

		GroupPermissionCheckBox.click();
		sleep(2);

		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(4);
		User.click();

	}

	public void CreateUserWithGroupPermissions() throws InterruptedException {
		// User.click();
		sleep(5);
		AddUserButton.click();
		sleep(4);
		UserName.sendKeys("USER Abc");
		UserPasswoard.sendKeys("0000");
		ConfirmPasswoard.sendKeys("0000");
		FirstName.sendKeys("Group Permission");
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(4);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText("Group Permissions");
		sleep(2);
		OkButton.click();
		sleep(5);
	}

	@FindBy(id = "logoutButton")
	private WebElement logoutBtn;

	public void logoutAsAdmin() throws InterruptedException {
		logoutBtn.click();
		waitForXpathPresent("//a[text()='Login again']");
		sleep(3);
	}

	@FindBy(id = "uName")
	private WebElement userNAmeEdt;

	@FindBy(id = "pass")
	private WebElement passwordEdt;

	@FindBy(id = "loginBtn")
	private WebElement loginBtn;

	public void LoginAsNewUser(String un, String pw) throws InterruptedException {
		userNAmeEdt.sendKeys(un);
		sleep(2);
		passwordEdt.sendKeys(pw);
		loginBtn.click();
		waitForidPresent("commonModalSmall");
		sleep(4);

	}

	public void LoginwithDisablesUser(String Username, String Password) throws InterruptedException {
		userNAmeEdt.sendKeys(Username);
		sleep(2);
		passwordEdt.sendKeys(Password);
		loginBtn.click();
		sleep(3);
		VerifyLoginWithDisabledUser();

	}

	@FindBy(xpath = "//div[text()='User is disabled. Please contact your Admin. ']")
	private WebElement DisabledUserLoginWarningMessage;

	public void VerifyLoginWithDisabledUser() {
		try {
			DisabledUserLoginWarningMessage.isDisplayed();
			Reporter.log("PASS>>>Unable To Login With Disabled User", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Logged In As Disabled User");
		}
	}

	@FindBy(xpath = "//*[@id='licenceXpireStep1']/div/div/div[1]/a") //// *[@id="licenceXpireStep1"]/div/div/div[1]/a
	private WebElement boardingStep1;

	public void WaitForLogin() throws InterruptedException {

		sleep(10);
		boardingStep1.click();
		sleep(2);

	}

	@FindBy(xpath = "(//input[@name='btSelectAll'])[1]")
	private WebElement btSelectAll;

	@FindBy(xpath = "//*[@id='deleteUser']")
	private WebElement deleteUser;

	@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YesDelete;

	public void SelectandDeleteUser(String User) throws InterruptedException {
		SearchUser.clear();
		sleep(3);
		SearchUser.sendKeys(User);
		waitForXpathPresent("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + User + "']");
		sleep(4);
		Initialization.driver.findElement(By
				.xpath("//*[@id='userManagementGrid']//td[text()='" + User + "']//parent::tr/td[@class='bs-checkbox']"))
		.click();
		sleep(4);
		deleteUser.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		YesDelete.click();
		waitForVisibilityOf("//span[contains(text(),'User deleted successfully.')]");
		Reporter.log("'User deleted successfully.'", true);
	}

	@FindBy(id = "oldpassword")
	private WebElement OldPasswordTextField;

	@FindBy(id = "newpassword")
	private WebElement NewPasswordTextField;

	@FindBy(xpath = ".//*[@id='commonModalSmall']/div/div/div[2]/div[3]/div/input")
	private WebElement ReTypePassword;

	public void ResetPassword(String NewPassword) throws InterruptedException {
		OldPasswordTextField.sendKeys("0000");
		sleep(2);
		NewPasswordTextField.sendKeys(NewPassword);
		sleep(2);
		ReTypePassword.sendKeys(NewPassword);
		sleep(2);
		OkButton.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(2);
	}

	public void LoginAsNewUserWithoutAccept(String yy, String zz) throws InterruptedException {
		// waitForidPresent("forgot_password");
		sleep(3);
		userNAmeEdt.sendKeys(yy);
		sleep(2);
		passwordEdt.sendKeys(zz);
		loginBtn.click();

		waitForidPresent("deleteDeviceBtn");
		sleep(5);

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[11]/span[4]")
	private WebElement ChangePasswordCheckBox;

	public void EnableChangePassword() throws InterruptedException {
		UserManagementPermissions.click();
		sleep(2);
		ChangePasswordCheckBox.click();
		sleep(2);
		UserManagementPermissions.click();
		sleep(2);
	}

	// ########################################################### GROUP
	// PERMISSIONS##################################################
	@FindBy(xpath = ".//*[@id='newGroup']/i")
	// @FindBy(id="newGroup")
	private WebElement AddNewGroupPermission;
	@FindBy(xpath = "//h4[text()='Add New Group']")
	private WebElement DialogueBoxNewGroup;
	@FindBy(xpath = ".//*[@id='okbtn123']")
	private WebElement AcceptButton;
	@FindBy(xpath = ".//*[@id='groupForm']/div/input")
	private WebElement GroupNameTextField;
	@FindBy(xpath = ".//*[@id='groupokbtn']")
	private WebElement ClickOkButtonGroup;
	@FindBy(xpath = ".//*[@id='groupstree']/ul/li[text()='USER A']")
	private WebElement SelectCcreatedGroup;
	@FindBy(xpath = ".//*[@id='renameGroup']/i")
	private WebElement RenameGroup;
	@FindBy(xpath = ".//*[@id='groupForm']/div/input")
	private WebElement RenameTextField;
	@FindBy(xpath = ".//*[@id='deleteGroup']")
	private WebElement DeleteButtonGroup;
	@FindBy(xpath = ".//*[@id='deletegroupbtn']")
	private WebElement DeleteButtonPopUPYes;
	@FindBy(xpath = "//span[text()='Group deleted successfully.']")
	private WebElement GroupDeletedSuccessfully;
	@FindBy(xpath = ".//*[@id='groupProperties']")
	private WebElement GroupPropertiesUserA;
	@FindBy(xpath = ".//*[@id='groupPropertiesDialog']/div/div/div[1]/h4[text()='Group Properties']")
	private WebElement AddDeleteDefaultGroupJob;
	@FindBy(xpath = ".//*[@id='groupPropertiesDialog']/div/div/div[1]/button")
	private WebElement CloseButtonGroupProperties;
	@FindBy(xpath = ".//*[@id='properties_addBtn']")
	private WebElement AddButtonInGroupProperties;
	@FindBy(xpath = "//p[text()='Automationtestjob']")
	private WebElement selectAutomationjob;
	@FindBy(xpath = ".//*[@id='okbtn']")
	private WebElement OkButton;
	@FindBy(xpath = "//span[text()='Job added successfully.']")
	private WebElement JobAddedSuccessfully;
	@FindBy(xpath = ".//*[@id='properties_dltbtn']")
	private WebElement DeleteJob;
	@FindBy(xpath = "//span[text()='Job deleted successfully.']")
	private WebElement JobDeletedsuccessfully;
	@FindBy(xpath = ".//*[@id='selJob_Modal_close']")
	private WebElement closeButtonJobPopUp2;

	// login as UserA and check for group permissions are accessable or not.
	public void GroupPermissionsUSERAallowed()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {

		// AcceptButton.click();
		// waitForidPresent("deleteDeviceBtn");
		sleep(4);
		AddNewGroupPermission.click();
		waitForidPresent("groupDialog");
		sleep(3);
		boolean displayDeleteButton = DialogueBoxNewGroup.isDisplayed();
		String PassStatement3 = "PASS >> New Group Title  is displayed ";
		String FailStatement3 = "FAIL >> New Group is  not displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);

		GroupNameTextField.sendKeys("USER A");
		ClickOkButtonGroup.click();
		sleep(5);

		RenameGroup.click();
		sleep(2);
		RenameTextField.clear();
		RenameTextField.sendKeys("USER A Renamed");
		ClickOkButtonGroup.click();
		sleep(4);
		DeleteButtonGroup.click();
		sleep(2);
		DeleteButtonPopUPYes.click();
		sleep(4);
		/*
		 * boolean GroupDeletedslly=
		 * GroupDeletedSuccessfully.isDisplayed();//span[text()='Group deleted
		 * successfully.'] String PassStatement1 =
		 * "PASS >> New Group Title  is displayed "; String FailStatement1 =
		 * "FAIL >> New Group is  not displayed";
		 * ALib.AssertTrueMethod(GroupDeletedslly, PassStatement1, FailStatement1);
		 */
		waitForidPresent("groupsbuttonpanel");
		sleep(2);
		SearchDeviceInconsole();
		sleep(3);
		GroupPropertiesUserA.click();
		sleep(4);
		// boolean Groupproperties = AddDeleteDefaultGroupJob.isDisplayed();
		// String PassStatement4 = "PASS >> Group Property Title is displayed ";
		// String FailStatement4 = "FAIL >> group property is not displayed";
		// ALib.AssertTrueMethod(Groupproperties, PassStatement4, FailStatement4);
		// sleep(2);
		AddButtonInGroupProperties.click();
		sleep(4);
		waitForidPresent("compjob_JobGridContainer");
		sleep(2);
		closeButtonJobPopUp2.click();

		sleep(2);

		CloseButtonGroupProperties.click();
		sleep(4);
		//
		// WebElement
		// wb=Initialization.driver.findElement(By.xpath(".//*[text()='autonew']"));
		//
		//
		// Actions act=new Actions(Initialization.driver);
		// act.moveToElement(wb).contextClick(wb).perform();
		//
		//
		// Initialization.driver.findElement(By.xpath(".//*[@id='gridMenu']/li[5]")).click();

	}

	@FindBy(xpath = "//p[text()='Group Permissions']")
	private WebElement RoleWithAllGroupPermission;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[2]/span[4]")
	private WebElement NewGroupCheckBox;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[3]/span[4]")
	private WebElement RenameGroupCheckBox;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[7]/span[4]")
	private WebElement AllowHomeGroupcheckBox;

	public void InAdminEnableOnlyNewGroupRenameAllowHomeGroup() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(3);
		RoleWithAllGroupPermission.click();
		sleep(4);
		EditOption.click();
		sleep(3);
		clickonGroupPermissionUM.click();
		sleep(5);
		GroupPermissionCheckbox.click();
		sleep(3);
		NewGroupCheckBox.click();
		RenameGroupCheckBox.click();
		AllowHomeGroupcheckBox.click();
		saveButton.click();
		sleep(10);
	}

	@FindBy(xpath = "//span[text()='Access denied. Please contact your administrator.']")
	private WebElement AccessDeniedErrorMessage;
	@FindBy(xpath = ".//*[@id='groupstree']/ul/li[1]/span[2]")
	private WebElement SelectHomeGroup;

	// allowing access only to new group , rename, home group .
	public void checkAccessNewGroupRenameAllowHomeGroup() throws InterruptedException {
		AddNewGroupPermission.click();
		// waitForidPresent("groupDialog");
		sleep(3);
		boolean displayDeleteButton = DialogueBoxNewGroup.isDisplayed();
		String PassStatement3 = "PASS >> New Group Title  is displayed ";
		String FailStatement3 = "FAIL >> New Group is  not displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		sleep(3);
		GroupNameTextField.sendKeys("Denied Permission");
		sleep(2);
		ClickOkButtonGroup.click();
		sleep(5);
		// waitForidPresent("exportGridView");//.//*[@id='renameGroup']/i
		RenameGroup.click();
		sleep(2);
		RenameTextField.clear();
		sleep(2);
		RenameTextField.sendKeys("Denied Renamed");
		ClickOkButtonGroup.click();
		sleep(4);
		DeleteButtonGroup.click();
		sleep(2);
		sleep(2);
		String Actualvalue = AccessDeniedErrorMessage.getText();
		String Expectedvalue = "Access denied. Please contact your administrator.";
		String PassStatement = "PASS >>'error Message displayed'";
		String FailStatement = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(4);
		GroupPropertiesUserA.click();
		waitForidPresent("groupPropertiesDialog");
		sleep(4);
		AddButtonInGroupProperties.click();
		sleep(2);
		String Actual = AccessDeniedErrorMessage.getText();
		String Expected = "Access denied. Please contact your administrator.";
		String PassStatement1 = "PASS >>'error Message displayed'";
		String FailStatement1 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expected, Actual, PassStatement1, FailStatement1);
		sleep(4);
		CloseButtonGroupProperties.click();
	}
	// disabling all the permissions

	public void DisablilngAllPermissions() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		RoleWithAllGroupPermission.click();
		sleep(2);
		EditOption.click();
		sleep(4);

		NewGroupCheckBox.click();
		sleep(2);
		RenameGroupCheckBox.click();
		sleep(2);

		AllowHomeGroupcheckBox.click();
		sleep(4);
		devicePermissionCheckBox.click();
		sleep(2);
		saveButton.click();
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='groupstree']/ul/li[2]")
	private WebElement SelectAnyGroup;

	@FindBy(xpath = "//div[text()='No device available in this group.']")
	private WebElement NoDeviceAvilable;

	public void CaptureErrorWhileDisablingAllGroupPermissions() throws InterruptedException {
		AddNewGroupPermission.click();
		sleep(2);
		String Actualvalue = AccessDeniedErrorMessage.getText();
		String Expectedvalue = "Access denied. Please contact your administrator.";
		String PassStatement = "PASS >>'error Message displayed'";
		String FailStatement = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(2);
		SelectAnyGroup.click();
		sleep(2);

		RenameGroup.click();
		sleep(2);
		String Actualvalue1 = AccessDeniedErrorMessage.getText();
		String Expectedvalue1 = "Access denied. Please contact your administrator.";
		String PassStatement1 = "PASS >>'error Message displayed'";
		String FailStatement1 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue1, Actualvalue1, PassStatement1, FailStatement1);
		sleep(2);
		DeleteButtonGroup.click();
		sleep(2);
		String Actualvalue2 = AccessDeniedErrorMessage.getText();
		String Expectedvalue2 = "Access denied. Please contact your administrator.";
		String PassStatement2 = "PASS >>'error Message displayed'";
		String FailStatement2 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue2, Actualvalue2, PassStatement2, FailStatement2);
		sleep(4);
		GroupPropertiesUserA.click();
		sleep(4);
		boolean Groupproperties = AddDeleteDefaultGroupJob.isDisplayed();
		String PassStatement4 = "PASS >> Group Property  Title  is displayed ";
		String FailStatement4 = "FAIL >> group property is  not displayed";
		ALib.AssertTrueMethod(Groupproperties, PassStatement4, FailStatement4);
		sleep(5);
		AddButtonInGroupProperties.click();
		sleep(2);
		String Actualvalue3 = AccessDeniedErrorMessage.getText();
		String Expectedvalue3 = "Access denied. Please contact your administrator.";
		String PassStatement3 = "PASS >>'error Message displayed'";
		String FailStatement3 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue3, Actualvalue3, PassStatement3, FailStatement3);
		sleep(2);
		CloseButtonGroupProperties.click();
		sleep(2);
		SelectHomeGroup.click();
		boolean noDeviceText = NoDeviceAvilable.isDisplayed();
		String PassStatement5 = "PASS >> New Group Title  is displayed ";
		String FailStatement5 = "FAIL >> New Group is  not displayed";
		ALib.AssertTrueMethod(noDeviceText, PassStatement5, FailStatement5);
		sleep(3);

	}
	// ######################################### DEVICE ACTION
	// PERMISSIONS######################################################

	public void CreateRoleForDeviceAction() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Device Action");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER B");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		DevicePermissionNotExpanded.click();
		sleep(2);

		saveButton.click();
		sleep(5);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(4);
		User.click();
	}

	public void CreateUserForPermissions(String username, String password, String Firstname, String featuredPermission)
			throws InterruptedException {
		User.click();
		sleep(5);
		AddUserButton.click();
		sleep(2);
		waitForidPresent("permissionModalTitle");
		sleep(4);
		UserName.sendKeys(username);
		sleep(3);
		UserPasswoard.sendKeys(password);
		ConfirmPasswoard.sendKeys(password);
		FirstName.sendKeys(Firstname);
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(4);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(
				Initialization.driver.findElement(By.xpath("//Select[@id='ListFeaturePermissions']")));
		dropOption.selectByVisibleText(featuredPermission);
		sleep(4);
		ListFeaturePermissions.click();

		WebElement scrollDown = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);
		OkButton.click();
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='applyJobButton']/i")
	private WebElement ApplyButton;
	@FindBy(xpath = ".//*[@id='apply_job_header']")
	private WebElement ApplyJobHeader;
	@FindBy(xpath = ".//*[@id='applyJobDataGrid']/tbody/tr/td/p[text()='DeviceAction']")
	private WebElement SelectJobDeviceAction;
	// @FindBy(xpath="//tr/td[text()='ANDROID WORK PROFILE']")
	// private WebElement AndroidProfile;
	@FindBy(xpath = ".//*[@id='applyJob_okbtn']")
	private WebElement ApplyJobOkButton;
	@FindBy(xpath = "//span[text()='Initiated apply job on selected device.']")
	private WebElement JobAppliedMessage;
	@FindBy(xpath = ".//*[@id='JobGridContainer']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement SearchJobField;

	public void SearchJobInSearchField(String JobToBeSearched) throws InterruptedException {
		SearchJobField.sendKeys(JobToBeSearched);
		waitForXpathPresent("//p[text()='" + JobToBeSearched + "']");
		sleep(5);
		Initialization.driver.findElement(By.xpath("//p[text()='" + JobToBeSearched + "']")).click();
		sleep(3);

	}

	public void ApplyJobPermission() throws InterruptedException {
		sleep(2);
		ApplyButton.click();
		sleep(4);
		waitForidPresent("apply_job_header");
		sleep(2);
		boolean displayValidMail = ApplyJobHeader.isDisplayed();
		String PassStatement1 = "PASS >> Access to apply job is granted ";
		String FailStatement1 = "FAIL >> Access to grant job is not granted";
		ALib.AssertTrueMethod(displayValidMail, PassStatement1, FailStatement1);
		sleep(4);
		SearchJobInSearchField("DeviceAction");

		sleep(4);
		ApplyJobOkButton.click();
		sleep(2);
		boolean displayValidMai2 = JobAppliedMessage.isDisplayed();
		String PassStatement2 = "PASS >> Access to apply job is granted ";
		String FailStatement2 = "FAIL >> Access to grant job is not granted";
		ALib.AssertTrueMethod(displayValidMai2, PassStatement2, FailStatement2);
		sleep(2);
		waitForidPresent("jobQueueButton");
		sleep(3);
	}

	public void ApplyAndroidProfile() throws InterruptedException {
		ApplyButton.click();
		sleep(4);
		waitForidPresent("apply_job_header");
		boolean displayValidMail = ApplyJobHeader.isDisplayed();
		String PassStatement1 = "PASS >> Access to apply job is granted ";
		String FailStatement1 = "FAIL >> Access to grant job is not granted";
		ALib.AssertTrueMethod(displayValidMail, PassStatement1, FailStatement1);
		sleep(4);

		List<WebElement> ProfileText = Initialization.driver
				.findElements(By.xpath(".//*[@id='applyJobDataGrid']/tbody/tr/td[3][text()='Android Profile']"));
		waitForXpathPresent(".//*[@id='applyJobDataGrid']/tbody/tr/td[text()='Android Profile']");
		ProfileText.get(0).click();

		ApplyJobOkButton.click();
		sleep(2);
		boolean displayValidMai2 = JobAppliedMessage.isDisplayed();
		String PassStatement2 = "PASS >> Access to apply job is granted ";
		String FailStatement2 = "FAIL >> Access to grant job is not granted";
		ALib.AssertTrueMethod(displayValidMai2, PassStatement2, FailStatement2);
		sleep(2);
		waitForidPresent("jobQueueButton");
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='jobQueueButton']/i")
	private WebElement JobQueueButton;
	@FindBy(xpath = "//p[text()='DeviceAction']")
	private WebElement DeleteQueuedJob;
	@FindBy(xpath = ".//*[@id='jobqueueremovebtn']")
	private WebElement removeButton;
	@FindBy(xpath = ".//*[@id='commonModalLarge']/div/div/div[1]/button")
	private WebElement CloseButtonQueue;

	public void RemoveQueuedJob() throws InterruptedException {
		JobQueueButton.click();
		waitForidPresent("jobqueuetitle");
		sleep(4);
		boolean displayValidMai2 = removeButton.isDisplayed();
		String PassStatement2 = "PASS >> Queued job is granted ";
		String FailStatement2 = "FAIL >> Access to queue job is not granted";
		ALib.AssertTrueMethod(displayValidMai2, PassStatement2, FailStatement2);
		sleep(2);
		CloseButtonQueue.click();
		waitForidPresent("exportGridView");
		sleep(2);
	}

	// @FindBy(xpath=".//*[@id='locateBtn']/i")
	// private WebElement LocateButton;
	@FindBy(xpath = ".//*[@id='realtime']/div/span[text()='Enable real-time GPS tracking']")
	private WebElement History;

	public void LocateDevice() throws InterruptedException {
		sleep(7);
		LocateButton.click();
		sleep(5);

		String originalHandle = Initialization.driver.getWindowHandle();
		sleep(10);
		LocateButton.click();
		sleep(5);
		Initialization.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		sleep(5);
		Initialization.driver.switchTo().window(tabs.get(0));
		sleep(5);

		boolean isdisplayed = true;

		try {
			History.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		sleep(5);
		Assert.assertTrue(isdisplayed, "FAIL: 'Locating of device page did not appear'");
		Reporter.log("PASS>> 'Device location' page launched succesfully", true);

		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);

		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='appBtn2']/i")
	private WebElement MoreButton;
	@FindBy(xpath = ".//*[@id='messageDeviceBtn']/i")
	private WebElement MessageDeviceButton;
	@FindBy(xpath = ".//*[@id='deviceMessageSubject']")
	private WebElement DeviceMessageSubject;
	@FindBy(xpath = ".//*[@id='mess_model_body']/div[2]/div/iframe")
	private WebElement DeviceMessageBody;
	@FindBy(xpath = ".//*[@id='message_modal']/div/div/div[3]/input")
	private WebElement sendButton;
	@FindBy(xpath = "//span[text()='Successfully delivered Message to device.']")
	private WebElement DeliveryMessage;
	@FindBy(xpath = ".//*[@id='message_modal']/div/div/div[1]/button")
	private WebElement CloseMessage;

	public void SendSMStoDevice() throws InterruptedException {
		MoreButton.click();
		sleep(2);
		MessageDeviceButton.click();
		waitForidPresent("message_modal");
		sleep(2);
		/*
		 * DeviceMessageSubject.sendKeys("Test Plan"); sleep(1);
		 * DeviceMessageBody.sendKeys("meeting @ 4pm");
		 */
		CloseMessage.click();
		sleep(7);

	}

	@FindBy(xpath = ".//*[@id='device_name_input']")
	private WebElement DeviceNameInput;
	@FindBy(xpath = "//div/form/div/div[1]/div[2]/button[1]")
	private WebElement SaveButtonOwenerName;
	@FindBy(xpath = "//span[text()='Device name changed successfully.']")
	private WebElement DeviceNameChangedSuccessfully;

	public void DeviceOwnerNameChange() throws InterruptedException {
		sleep(4);
		DeviceNameInput.clear();
		sleep(2);

		// DeviceNameInput.clear();
		// sleep(2);
		DeviceNameInput.sendKeys("killed");
		sleep(4);
		SaveButtonOwenerName.click();
		sleep(5);
		// waitForidPresent("masterBody");
		boolean displayDeleteButton = DeviceNameChangedSuccessfully.isDisplayed();
		String PassStatement3 = "PASS >> Successfully delivered Message  is displayed ";
		String FailStatement3 = "FAIL >> Successfully delivered Message is  not displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		sleep(4);
		DeviceNameInput.clear();
		DeviceNameInput.sendKeys("donotdelete");
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='lockDeviceBtn']/i")
	private WebElement LockDeviceButton;
	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='No']")
	private WebElement Confirmation;
	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement NoPasswordDevice;

	public void LockDevice() throws InterruptedException {
		MoreButton.click();
		sleep(4);
		LockDeviceButton.click();
		waitForidPresent("deviceConfirmationDialog");
		sleep(5);
		Confirmation.click();
		// sleep(7);
		// NoPasswordDevice.click();
		waitForidPresent("applyJobButton");
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='dataUsage']/i")
	private WebElement DataUsageButton;
	@FindBy(xpath = "//label[text()='Period']")
	private WebElement PeriodText;
	@FindBy(xpath = ".//*[@id='datausage_modal']/div[1]/button")
	private WebElement ClosseButtonDataUsage;

	public void DataUsage() throws InterruptedException {
		sleep(2);
		MoreButton.click();
		sleep(4);
		DataUsageButton.click();
		waitForidPresent("datausage_modal");
		boolean displayDeleteButton = PeriodText.isDisplayed();
		String PassStatement3 = "PASS >> DataUsage  is displayed ";
		String FailStatement3 = "FAIL >> DataUsage is  not displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		sleep(4);
		ClosseButtonDataUsage.click();
		sleep(4);

	}

	@FindBy(xpath = ".//*[@id='callLog']/i")
	private WebElement LogButton;
	@FindBy(xpath = ".//*[@id='call_logs_refresh']/i")
	private WebElement DeviceLogTitle;
	@FindBy(xpath = ".//*[@id='commonModal']/div/div/div[1]/button")
	private WebElement CancleButtonLog;

	public void LogButton() throws InterruptedException {

		MoreButton.click();
		sleep(4);
		LogButton.click();
		waitForidPresent("call_logs_refresh");
		sleep(5);
		boolean displayDeleteButton = DeviceLogTitle.isDisplayed();
		String PassStatement3 = "PASS >> DataUsage  is displayed ";
		String FailStatement3 = "FAIL >> DataUsage is  not displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		sleep(4);
		CancleButtonLog.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='surelockSettings']/i")
	private WebElement SureLockSettings;
	@FindBy(xpath = ".//*[@id='insatall_permission']/input")
	private WebElement installButton;
	@FindBy(xpath = ".//*[@id='commonModalLargeXSLT']/div/div/div[1]/button")
	private WebElement CancleButtonSureLock;

	public void SureLockSettingsPermission() throws InterruptedException {
		SureLockSettings.click();
		// waitForidPresent("SaveAsBtn");
		sleep(4);
		// boolean displayDeleteButton = installButton.isDisplayed();
		// String PassStatement3 = "PASS >> Sure Lock Setting page has been displayed ";
		// String FailStatement3 = "FAIL >> Sure Lock Setting page has not been
		// displayed";
		// ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		// sleep(4);
		CancleButtonSureLock.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='surefoxSettings']/i")
	private WebElement SureFoxSettings;

	public void SureFoxsettingPermission() throws InterruptedException {
		SureFoxSettings.click();
		// waitForidPresent("modal_title_startup");
		sleep(4);
		boolean displayDeleteButton = installButton.isDisplayed();
		String PassStatement3 = "PASS >> Sure Lock Setting page has been displayed ";
		String FailStatement3 = "FAIL >> Sure Lock Setting page has not been displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		sleep(4);
		CancleButtonSureLock.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='surevideoSettings']/i")
	private WebElement SureVideoSettings;
	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YesConfirmationDialogueBox;

	public void SureVideoSettingsPermission() throws InterruptedException {
		MoreButton.click();
		sleep(4);
		SureVideoSettings.click();
		sleep(4);
		YesConfirmationDialogueBox.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='smsLog']/i")
	private WebElement SmsLogButton;
	@FindBy(xpath = ".//*[@id='commonModal']/div/div/div[2]/div[5]/input[1]")
	private WebElement OkButtonSmsLog;

	public void SMSlogsPermissions() throws InterruptedException {
		MoreButton.click();
		sleep(4);
		SmsLogButton.click();
		waitForidPresent("sms_logs_refresh");
		boolean displayDeleteButton = SmsLogButton.isDisplayed();
		String PassStatement3 = "PASS >> smslog page has been displayed ";
		String FailStatement3 = "FAIL >> smslog page has not been displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		sleep(4);
		OkButtonSmsLog.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='rebootDevice']/i")
	private WebElement RebootDevice;
	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement NoConfirmationRebootDevice;

	public void RebootDevicePermission() throws InterruptedException {
		MoreButton.click();
		sleep(4);
		RebootDevice.click();
		sleep(2);
		NoConfirmationRebootDevice.click();
	}

	@FindBy(xpath = ".//*[@id='remoteBuzz']/i")
	private WebElement RemoteBuzz;

	public void RemoteBuzzPermission() throws InterruptedException {
		MoreButton.click();
		sleep(2);
		RemoteBuzz.click();
		sleep(4);
		NoConfirmationRebootDevice.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='mailSection']/a")
	private WebElement InboxButton;
	@FindBy(xpath = ".//*[@id='mailReplyButton']/i")
	private WebElement ReplyButton;
	@FindBy(xpath = ".//*[@id='mess_model_body']/div[2]/div[text()='Message']")
	private WebElement MessageText;
	@FindBy(xpath = ".//*[@id='message_modal']/div[1]/button")
	private WebElement CloseButtonInbox;
	@FindBy(xpath = ".//*[@id='homeSection']/a")
	private WebElement HomesectionButton;

	public void Replypermission() throws InterruptedException {
		sleep(4);
		InboxButton.click();
		sleep(5);
		ReplyButton.click();
		waitForidPresent("message_modal");
		boolean displayDeleteButton = MessageText.isDisplayed();
		String PassStatement3 = "PASS >> Reply Permission has been displayed ";
		String FailStatement3 = "FAIL >> Reply permission  not been displayed";
		ALib.AssertTrueMethod(displayDeleteButton, PassStatement3, FailStatement3);
		sleep(4);
		CloseButtonInbox.click();
		sleep(3);
		HomesectionButton.click();
		waitForidPresent("exportGridView");
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='mailDeleteButton']/i")
	private WebElement DeleteButton;
	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement NoConfirmatioForMessageDelete;

	public void DeleteMessagePermission() throws InterruptedException {
		InboxButton.click();
		sleep(5);
		DeleteButton.click();
		sleep(2);
		NoConfirmatioForMessageDelete.click();
		sleep(4);
		HomesectionButton.click();
		waitForidPresent("exportGridView");
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='wipeDevice']/i")
	private WebElement WipeDeviceButton;
	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[1]")
	private WebElement CofirmationNoWipe;

	public void WipePermission() throws InterruptedException {
		MoreButton.click();
		sleep(2);
		WipeDeviceButton.click();
		sleep(4);
		CofirmationNoWipe.click();
		waitForidPresent("exportGridView");

	}

	@FindBy(xpath = "//p[text()='Device Action']")
	private WebElement SelectDeviceActionRole;
	@FindBy(xpath = "//span[text()='Role modified successfully.']")
	private WebElement RoleEditedDeviceAction;

	public void DisableAllDeviceActionPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(4);
		SelectDeviceActionRole.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Device Action-Edited");
		sleep(4);
		clickonGroupPermissionUM.click();
		sleep(2);
		DevicePermissionNotExpanded.click();
		sleep(4);
		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void ApplyJobDisabled() throws InterruptedException {
		ApplyButton.click();
		sleep(4);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void LocateButtonDisabled() throws InterruptedException {
		LocateButton.click();
		sleep(4);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void wipeDisabled() throws InterruptedException {
		MoreButton.click();
		sleep(2);
		WipeDeviceButton.click();
		sleep(4);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);

	}

	public void LogsDisabled() throws InterruptedException {
		// MoreButton.click();
		// sleep(4);
		LogButton.click();
		sleep(4);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void DataUsageDisabled() throws InterruptedException {
		DataUsageButton.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);

	}

	public void SMSLogDisabled() throws InterruptedException {
		SmsLogButton.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void LockDisabled() throws InterruptedException {
		LockDeviceButton.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void SMSDisabled() throws InterruptedException {
		SmsLogButton.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void RemoteBuzzDisabled() throws InterruptedException {
		RemoteBuzz.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void RebootDisabled() throws InterruptedException {
		RebootDevice.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void SureLockDisabled() throws InterruptedException {
		SureLockSettings.click();

		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void SureFoxDisabled() throws InterruptedException {
		SureFoxSettings.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void SureVideoDisable() throws InterruptedException {
		MoreButton.click();
		sleep(4);
		SureVideoSettings.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
		MoreButton.click();
	}

	@FindBy(xpath = ".//*[@id='dis-card']/div[7]/a")
	private WebElement EditOwnerName;

	public void DeviceOwnerNameChangeDisabled() throws InterruptedException {
		EditOwnerName.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='dis-card']/div[15]/div/a")
	private WebElement DeviceNotesEdit;

	public void DeviceNotesDisable() throws InterruptedException {
		DeviceNotesEdit.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Role Edited has been displayed ";
		String FailStatement1 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
	}

	@FindBy(xpath = "//p[text()='Device Action-Edited']")
	private WebElement changedRoleName;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[2]/span[1]")
	private WebElement DeviceActionDropdown;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[3]/span[4]")
	private WebElement ApplyJobCheckBoxExpanded;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[5]/span[4]")
	private WebElement RemoveQueueJobCheckBox;
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[8]/span[4]")
	private WebElement SendSMStoDseviceCheckBox;

	public void EnableFewPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(4);
		changedRoleName.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Device Action few permissions");
		sleep(4);
		clickonGroupPermissionUM.click();
		sleep(2);
		DeviceActionDropdown.click();
		// DevicePermissionNotExpanded.click();
		ApplyJobCheckBoxExpanded.click();
		RemoveQueueJobCheckBox.click();
		SendSMStoDseviceCheckBox.click();
	}

	// ################################################ DEVICE MANAGEMENT
	// ####################################################
	@FindBy(xpath = ".//*[@id='umTree']/ul/li[3]/span[3]")
	private WebElement DeviceManagementCheckBox;

	public void CreateRoleDeviceManagement() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Device Management");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER C");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		DeviceManagementCheckBox.click();
		sleep(2);

		saveButton.click();
		sleep(5);

		User.click();
	}

	@FindBy(xpath = ".//*[@id='deleteDeviceBtn']/i")
	private WebElement DeleteButtonUserC;

	public void DeleteDeviceEnabled() throws InterruptedException {
		DeleteButtonUserC.click();
		sleep(5);
		boolean displayRoleCreated = NoConfirmationRebootDevice.isDisplayed();
		String PassStatement1 = "PASS >> Delete device access granted ";
		String FailStatement1 = "FAIL >> Delete Device access not granted";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
		NoConfirmationRebootDevice.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='blacklistDeviceBtn']/i")
	private WebElement BlackListedButtonToolBar;

	public void BlackListedEnabled() throws InterruptedException {
		BlackListedButtonToolBar.click();
		sleep(2);
		boolean displayRoleCreated = Confirmation.isDisplayed();
		String PassStatement1 = "PASS >> Blacklisted permission access granted ";
		String FailStatement1 = "FAIL >> blacklistedd permision not access not granted";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(1);
		Confirmation.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='blacklistedSection']")
	private WebElement BlacklistedSection;
	@FindBy(xpath = ".//*[@id='blRefreshBtn']/i")
	private WebElement RefreshButtonBlacklisted;

	public void BlackListedSection() throws InterruptedException {
		BlacklistedSection.click();
		waitForidPresent("panelBackgroundDevice");
		boolean displayRoleCreated = RefreshButtonBlacklisted.isDisplayed();
		String PassStatement1 = "PASS >> Blacklisted permission access granted ";
		String FailStatement1 = "FAIL >> blacklistedd permision not access not granted";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(4);
		HomesectionButton.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesConfirmation;
	@FindBy(xpath = "//span[text()='Device(s) blacklisted.']")
	private WebElement blacklisteSuccessfully;
	@FindBy(xpath = ".//*[@id='blacklistGrid']/tbody/tr/td[1]")
	private WebElement SelectDevicetoWhitelist;
	@FindBy(xpath = ".//*[@id='blWhitelistBtn']/i")
	private WebElement WhiteListedButton;
	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YesConfirmationWhitelisted;
	@FindBy(xpath = "//span[text()='Device(s) whitelisted successfully']")
	private WebElement WhiteListedSuccessfully;
	@FindBy(xpath = ".//*[@id='tableContainer']/div[4]/div[1]/div[1]/button")
	private WebElement RefreshConsole;

	public void whitelistedEnabled() throws InterruptedException {
		BlackListedButtonToolBar.click();
		sleep(2);
		YesConfirmation.click();
		sleep(3);
		boolean displayRoleCreated = blacklisteSuccessfully.isDisplayed();
		String PassStatement1 = "PASS >> Blacklisted permission access granted ";
		String FailStatement1 = "FAIL >> blacklistedd permision not access not granted";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement1, FailStatement1);
		sleep(2);
		BlacklistedSection.click();
		waitForidPresent("panelBackgroundDevice");
		sleep(4);
		SelectDevicetoWhitelist.click();
		sleep(2);
		WhiteListedButton.click();
		sleep(2);
		YesConfirmationWhitelisted.click();
		sleep(5);
		boolean displayRoleCreated1 = WhiteListedSuccessfully.isDisplayed();
		String PassStatement2 = "PASS >> Blacklisted permission access granted ";
		String FailStatement2 = "FAIL >> blacklistedd permision not access not granted";
		ALib.AssertTrueMethod(displayRoleCreated1, PassStatement2, FailStatement2);
		sleep(2);
		HomesectionButton.click();
		sleep(4);
		RefreshConsole.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='unapprovedSection']/a/p")
	private WebElement ApproveButton;

	public void ApproveDevice() {
		ApproveButton.click();
	}

	// ############################################### APPLICATION SETTINGS
	// ############################################
	@FindBy(xpath = ".//*[@id='appListBtn']/i")
	private WebElement ApplicationSettingsButton;

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[4]/span[3]")
	private WebElement ApplicationSettingsCheckBox;

	public void ApplicationSettingsRole() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Application settings");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER D");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		ApplicationSettingsCheckBox.click();

		saveButton.click();
		// sleep(5);
		sleep(5);
		User.click();
	}

	@FindBy(xpath = ".//*[@id='appDetailModalTitle']")
	private WebElement ApplicationHeader;
	@FindBy(xpath = ".//*[@id='appsList_ios_modal']/div[1]/button")
	private WebElement CloseButtonApplication;

	public void ClickOnApplicationSettings() throws InterruptedException {
		sleep(4);
		ApplicationSettingsButton.click();
		sleep(4);
		waitForidPresent("apptableHeader");
		sleep(2);
		boolean displayRoleCreated1 = ApplicationHeader.isDisplayed();
		String PassStatement2 = "PASS >> Application Settings access granted ";
		String FailStatement2 = "FAIL >> Application Setting  access not granted";
		ALib.AssertTrueMethod(displayRoleCreated1, PassStatement2, FailStatement2);
		sleep(5);

		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='downlodedAppListTable']/tbody/tr[1]/td[1]/span/input")
	private WebElement FirstCheckboxEnabled;
	@FindBy(xpath = ".//*[@id='uninstallAppList']/i")
	private WebElement UninstallButton;
	@FindBy(xpath = ".//*[@id='commonModalLarger']/div/div/div[1]/button")
	private WebElement CloseButtonApplicationSettings;

	public void UninstallEnabled() throws InterruptedException {
		FirstCheckboxEnabled.click();
		sleep(4);
		UninstallButton.click();
		sleep(2);
		Confirmation.click();
		sleep(3);
		CloseButtonApplicationSettings.click();
		sleep(2);

	}
	// ################################################# JOB PERMISSIONS
	// #####################################

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[5]/span[3]")
	private WebElement jobPermisionCheckBox;

	public void JobPermissionRole() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Job Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER D");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		jobPermisionCheckBox.click();
		saveButton.click();
		sleep(5);
		User.click();
	}

	@FindBy(xpath = ".//*[@id='jobSection']/a")
	private WebElement jobToolBarButton;
	@FindBy(xpath = ".//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[1]/button")
	private WebElement RefreshButtonJobs;

	public void JobAccessable() throws InterruptedException {
		jobToolBarButton.click();
		waitForidPresent("panelBackgroundDevice");
		boolean displayRoleCreated1 = RefreshButtonJobs.isDisplayed();
		String PassStatement2 = "PASS >>Job permission access granted ";
		String FailStatement2 = "FAIL >> Job permission  access not granted";
		ALib.AssertTrueMethod(displayRoleCreated1, PassStatement2, FailStatement2);
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='job_new_job']/i")
	private WebElement NewJobIcon;

	@FindBy(xpath = ".//*[@id='osPanel_cancel_btn']")
	private WebElement canclebuttonNewJob;

	public void NewJobPermissionAccessable() throws InterruptedException {
		NewJobIcon.click();
		sleep(4);
		waitForidPresent("osPanel_cancel_btn");
		sleep(3);
		boolean displayRoleCreated1 = canclebuttonNewJob.isDisplayed();
		String PassStatement2 = "PASS >>NEWJob permission access granted ";
		String FailStatement2 = "FAIL >> NEWob permission  access not granted";
		ALib.AssertTrueMethod(displayRoleCreated1, PassStatement2, FailStatement2);
		canclebuttonNewJob.click();
		sleep(4);
	}

	@FindBy(xpath = "//p[text()='DeviceAction']")
	private WebElement SelectJobToDelete;

	@FindBy(xpath = ".//*[@id='job_delete']/i")
	private WebElement JobDeleteButton;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='No']")
	private WebElement NoDialogueBoxJobDelete;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesDialogueBoxJobDelete;

	@FindBy(xpath = "//*[@id='panelBackgroundDevice']/div[1]/div[1]/div[3]/input")
	private WebElement SelectJobSearchField;

	public void DeleteJobPermissionAccessable() throws InterruptedException {
		SelectJobSearchField.sendKeys("DeviceAction");
		waitForXpathPresent(".//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='DeviceAction']");
		sleep(3);
		SelectJobToDelete.click();
		sleep(3);
		JobDeleteButton.click();
		sleep(5);
		boolean displayRoleCreated1 = NoDialogueBoxJobDelete.isDisplayed();
		String PassStatement2 = "PASS >>Delete job permission access granted ";
		String FailStatement2 = "FAIL >> Delete job permission  access not granted";
		ALib.AssertTrueMethod(displayRoleCreated1, PassStatement2, FailStatement2);

		sleep(2);
		NoDialogueBoxJobDelete.click();
		waitForidPresent("osPanel_cancel_btn");
	}

	@FindBy(id = "job_modify")
	private WebElement JobModify;

	@FindBy(xpath = ".//*[@id='okbtn']")
	private WebElement OkButtonJobModify;

	@FindBy(xpath = ".//*[@id='txtMessage_modal']/div/div/div[1]/button")
	private WebElement CancleButtonModifymessage;

	public void ModifyJobPermission() throws InterruptedException {
		sleep(4);
		JobModify.click();
		waitForidPresent("android_only_read");
		sleep(3);
		boolean displayRoleCreated1 = OkButtonJobModify.isDisplayed();
		String PassStatement2 = "PASS >>JobModify access granted ";
		String FailStatement2 = "FAIL >>JobModify access not granted";
		ALib.AssertTrueMethod(displayRoleCreated1, PassStatement2, FailStatement2);
		sleep(3);
		CancleButtonModifymessage.click();
		sleep(3);
	}

	@FindBy(xpath = "//p[text()='Job Permission']")
	private WebElement SelectJobPermissionRole;
	@FindBy(xpath = "//span[text()='Role modified successfully.']")
	private WebElement RoleEditedJobPermis;

	public void DisableAllJobPermission() throws InterruptedException {

		clickOnRolesOptions.click();
		sleep(4);
		SelectJobPermissionRole.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Job Permission-Edited");
		sleep(4);
		clickonGroupPermissionUM.click();
		sleep(2);
		jobPermisionCheckBox.click();
		sleep(4);
		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedJobPermis.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void NewJodDisabled() throws InterruptedException {
		jobToolBarButton.click();
		waitForidPresent("panelBackgroundDevice");
		sleep(4);
		NewJobIcon.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> New Job Permisison is denied ";
		String FailStatement3 = "FAIL >> New Job Permission is accessed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void DeleteJobAccessDenied() throws InterruptedException {
		SelectJobSearchField.sendKeys("DeviceAction");
		waitForXpathPresent(".//*[@id='jobDataGrid']/tbody/tr[1]/td[2]/p[text()='DeviceAction']");
		sleep(3);
		SelectJobToDelete.click();
		sleep(3);
		JobDeleteButton.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> New Job Permisison is denied ";
		String FailStatement3 = "FAIL >> New Job Permission is accessed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);

	}

	public void ModifyJobAccessDenied() throws InterruptedException {

		sleep(3);
		JobModify.click();
		sleep(3);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> New Job Permisison is denied ";
		String FailStatement3 = "FAIL >> New Job Permission is accessed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);

	}

	// ################################################ REPORT PERMISSISONS
	// #####################################################

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[6]/span[3]")
	private WebElement ReportPermissionsCheckBox;

	public void ReportPermissionRole() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Report Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER F");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		ReportPermissionsCheckBox.click();
		sleep(2);
		saveButton.click();
		sleep(10);
		User.click();
	}

	@FindBy(xpath = ".//*[@id='reportSection']/a")
	private WebElement ReportButton;

	@FindBy(xpath = "//span[text()='System Log']")
	private WebElement SystemlogOption;

	@FindBy(xpath = ".//*[@id='generateReportBtn']")
	private WebElement RequestButton;

	@FindBy(xpath = "//a[text()='Reports']")
	private WebElement reportButton;
	
	public void ClickOnReport() throws InterruptedException {
		try {
			waitForXpathPresent("//a[text()='Reports']");
			sleep(4);
			reportButton.click();
			waitForXpathPresent("//span[text()='On Demand Reports']");
			sleep(2);
			Reporter.log("PASS>> Clicked On Reports", true);
		} catch (Exception e) {
			Initialization.driver.findElement(By.xpath("//li[@class='nav-subgrp actAsSubGrp']/p/span[text()='More']"))
					.click();
			sleep(2);
			reportButton.click();
			waitForXpathPresent("//span[text()='On Demand Reports']");
			sleep(2);
			Reporter.log("PASS >> Clicked On Reports", true);
		}
	}

	public void SystemLogOnDemand() throws InterruptedException {
		SystemlogOption.click();
		sleep(2);
		boolean SystemLlogOnDemand = RequestButton.isDisplayed();
		String PassStatement3 = "PASS >> System log on demand report is accessable";
		String FailStatement3 = "FAIL >> System log on demand report is not accessable";
		ALib.AssertTrueMethod(SystemLlogOnDemand, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Asset Tracking']")
	private WebElement AssetTracking;
	@FindBy(xpath = ".//*[@id='reportDetails']/div[1]/span[1]")
	private WebElement SelectGroup;

	public void AssetTrackingOnDemand() throws InterruptedException {
		AssetTracking.click();
		sleep(2);
		boolean AssetTrackingOnDemand = RequestButton.isDisplayed();
		String PassStatement3 = "PASS >> asset tracking on demand report is accessable";
		String FailStatement3 = "FAIL >> asset tracking on demand report is not accessable";
		ALib.AssertTrueMethod(AssetTrackingOnDemand, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Call Log Tracking']")
	private WebElement CallLogTracking;

	@FindBy(xpath = "//span[text()='Call Type']")
	private WebElement CallType;

	public void CallLogTrackingOnDemand() throws InterruptedException {
		CallLogTracking.click();
		sleep(2);
		boolean CallLogTrackingOnDemand = CallType.isDisplayed();
		String PassStatement3 = "PASS >> Call Log on demand report is accessable";
		String FailStatement3 = "FAIL >> call log tracking on demand report is not accessable";
		ALib.AssertTrueMethod(CallLogTrackingOnDemand, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Jobs Deployed']")
	private WebElement JobsDeployed;

	@FindBy(xpath = "//span[text()='Select Group']")
	private WebElement SelectGroupTextFieled;

	public void JobsDeployedOnDemand() throws InterruptedException {
		JobsDeployed.click();
		sleep(2);
		boolean JobsDeployedOnDemand = SelectGroupTextFieled.isDisplayed();
		String PassStatement3 = "PASS >>Job depoloyed  on demand report is accessable";
		String FailStatement3 = "FAIL >> Job Depoloyed tracking on demand report is not accessable";
		ALib.AssertTrueMethod(JobsDeployedOnDemand, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Installed Job Report']")
	private WebElement InstallJobReport;

	@FindBy(xpath = "//span[text()='Application Name']")
	private WebElement ApplicationName;

	public void instlledJobOnDemand() throws InterruptedException {
		InstallJobReport.click();
		sleep(2);
		boolean InstalledjobOnDemand = ApplicationName.isDisplayed();
		String PassStatement3 = "PASS >>Installed job on demand report is accessable";
		String FailStatement3 = "FAIL >> Installed job  on demand report is not accessable";
		ALib.AssertTrueMethod(InstalledjobOnDemand, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='reportsGrid']/tbody/tr[7]/td/div/span[text()='Device Health Report']")
	private WebElement DeviceHealthReport;

	@FindBy(xpath = "//span[text()='Battery Percentage <= ']")
	private WebElement BatteryPercentage;

	public void DeviceHealthOnDemand() throws InterruptedException {
		DeviceHealthReport.click();
		sleep(3);
		boolean DeviceHealthOnDemand = BatteryPercentage.isDisplayed();
		String PassStatement3 = "PASS >>Device Health on demand report is accessable";
		String FailStatement3 = "FAIL >> Device Health  on demand report is not accessable";
		ALib.AssertTrueMethod(DeviceHealthOnDemand, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Device History']")
	private WebElement DeviceHistory;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5")
	private WebElement DeviceHistoryHeader;

	public void DeviceHistoryOnDemand() throws InterruptedException {
		DeviceHistory.click();
		sleep(3);
		boolean DeviceHistoryOnDemand = DeviceHistoryHeader.isDisplayed();
		String PassStatement3 = "PASS >>Device Health on demand report is accessable";
		String FailStatement3 = "FAIL >> Device Health  on demand report is not accessable";
		ALib.AssertTrueMethod(DeviceHistoryOnDemand, PassStatement3, FailStatement3);
	}

	@FindBy(xpath = ".//*[@id='reportsGrid']/tbody/tr[9]/td/div/span[text()='Data Usage']")
	private WebElement DataUsage;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5")
	private WebElement DataUsageHeader;

	public void DataUsageOnDemand() throws InterruptedException {
		DataUsage.click();
		sleep(3);
		boolean DataUsageOnDemand = DataUsageHeader.isDisplayed();
		String PassStatement3 = "PASS >>Data usgae on demand report is accessable";
		String FailStatement3 = "FAIL >> Data usgae  on demand report is not accessable";
		ALib.AssertTrueMethod(DataUsageOnDemand, PassStatement3, FailStatement3);
	}

	@FindBy(xpath = "//span[text()='Device Connected']")
	private WebElement DeviceConnected;

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Device Connected']")
	private WebElement DeviceConnectedHeader;

	public void DeviceConnectedOnDemand() throws InterruptedException {
		DeviceConnected.click();
		sleep(3);
		boolean DeviceConnectedOnDemand = DeviceConnectedHeader.isDisplayed();
		String PassStatement3 = "PASS >>Device connected on demand report is accessable";
		String FailStatement3 = "FAIL >> Device connected  on demand report is not accessable";
		ALib.AssertTrueMethod(DeviceConnectedOnDemand, PassStatement3, FailStatement3);
	}

	@FindBy(xpath = "//span[text()='App Version']")
	private WebElement AppVersion;

	@FindBy(xpath = ".//*[@id='reportDetails']/div[2]/span[text()='App Type']")
	private WebElement AppTypeTextField;

	public void AppVersionOnDemand() throws InterruptedException {
		AppVersion.click();
		sleep(3);
		boolean AppVersionOnDemand = AppTypeTextField.isDisplayed();
		String PassStatement3 = "PASS >>App version on demand report is accessable";
		String FailStatement3 = "FAIL >> App version  on demand report is not accessable";
		ALib.AssertTrueMethod(AppVersionOnDemand, PassStatement3, FailStatement3);

	}

	@FindBy(xpath = "//span[text()='Compliance Report']")
	private WebElement ComplianceReport;

	public void ComplianceReportOnDemand() throws InterruptedException {
		ComplianceReport.click();
		waitForidPresent("ComplianceReportCont");
		sleep(2);
	}

	@FindBy(xpath = "//span[text()='Schedule Reports']")
	private WebElement ScheduleReports;

	public void ClilckOnScheduleReports() throws InterruptedException {
		ScheduleReports.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='System Log']")
	private WebElement HeaderSystemLog;

	public void SystemLogScheduleReport() throws InterruptedException {
		SystemlogOption.click();
		sleep(3);
		boolean SystemlogSchedule = HeaderSystemLog.isDisplayed();
		String PassStatement3 = "PASS >>System log Schedule Report is accessable";
		String FailStatement3 = "FAIL >> System log Schedule Report is not accessable";
		ALib.AssertTrueMethod(SystemlogSchedule, PassStatement3, FailStatement3);

	}

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Asset Tracking']")
	private WebElement AssestTrackingHeader;

	public void AssestTrackringScheduleReport() throws InterruptedException {
		AssetTracking.click();
		sleep(4);
		boolean AssetTrackingScheduleReport = AssestTrackingHeader.isDisplayed();
		String PassStatement3 = "PASS >> asset tracking on Schedule report is accessable";
		String FailStatement3 = "FAIL >> asset tracking on Schedule report is not accessable";
		ALib.AssertTrueMethod(AssetTrackingScheduleReport, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Call Log Tracking']")
	private WebElement CallLogTrackingheader;

	public void CallLogTrackingScheduleReport() throws InterruptedException {
		CallLogTracking.click();
		sleep(4);
		boolean CallLogTrackingScheduleReport = CallLogTrackingheader.isDisplayed();
		String PassStatement3 = "PASS >> Call log tracking on Schedule report is accessable";
		String FailStatement3 = "FAIL >>call log tracking on Schedule report is not accessable";
		ALib.AssertTrueMethod(CallLogTrackingScheduleReport, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Jobs Deployed']")
	private WebElement JobsDeployedHeader;

	public void JobsDeployedScheduleReport() throws InterruptedException {
		JobsDeployed.click();
		sleep(4);
		boolean JobDeployedScheduleReport = JobsDeployedHeader.isDisplayed();
		String PassStatement3 = "PASS >> Jobs deployed on Schedule report is accessable";
		String FailStatement3 = "FAIL >>Jobs deployed on Schedule report is not accessable";
		ALib.AssertTrueMethod(JobDeployedScheduleReport, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Installed Job Report']")
	private WebElement installJobHeader;

	public void InstalledJobReportScheduleReport() throws InterruptedException {
		InstallJobReport.click();
		sleep(4);
		boolean installedJobScheduleReport = installJobHeader.isDisplayed();
		String PassStatement3 = "PASS >>install job on Schedule report is accessable";
		String FailStatement3 = "FAIL >>install job on Schedule report is not accessable";
		ALib.AssertTrueMethod(installedJobScheduleReport, PassStatement3, FailStatement3);
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='reportTitCont']/h5[text()='Device Health Report']")
	private WebElement DeviceHealthHeader;

	@FindBy(xpath = ".//*[@id='reportsGrid']/tbody/tr[7]/td/div/span[text()='Device Health Report']")
	private WebElement DeviceHealthSection;

	public void DeviceHealthScheduleReport() throws InterruptedException {
		DeviceHealthSection.click();
		sleep(4);
		boolean DeviceHealthScheduleReport = DeviceHealthHeader.isDisplayed();
		String PassStatement3 = "PASS >>Device Health on Schedule report is accessable";
		String FailStatement3 = "FAIL >>Device health on Schedule report is not accessable";
		ALib.AssertTrueMethod(DeviceHealthScheduleReport, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void DeviceHistoryScheduleReport() throws InterruptedException {
		DeviceHistory.click();
		sleep(4);
		boolean DeviceHistoryScheduleReport = DeviceHistoryHeader.isDisplayed();
		String PassStatement3 = "PASS >>Device History on Schedule report is accessable";
		String FailStatement3 = "FAIL >>Device History on Schedule report is not accessable";
		ALib.AssertTrueMethod(DeviceHistoryScheduleReport, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void DataUsageScheduleReport() throws InterruptedException {
		DataUsage.click();
		sleep(3);
		boolean DataUsageOnScheduleReport = DataUsageHeader.isDisplayed();
		String PassStatement3 = "PASS >>Data Usage on Schedule report is accessable";
		String FailStatement3 = "FAIL >>Data Usage on Schedule report is not accessable";
		ALib.AssertTrueMethod(DataUsageOnScheduleReport, PassStatement3, FailStatement3);
		sleep(4);
	}

	public void DeviceConnectedScheduleReport() throws InterruptedException {
		DeviceConnected.click();
		sleep(3);
		boolean DeviceConnectedScheduleReport = DeviceConnectedHeader.isDisplayed();
		String PassStatement3 = "PASS >>Device connected on Schedule report is accessable";
		String FailStatement3 = "FAIL >>Device connected on Schedule report is not accessable";
		ALib.AssertTrueMethod(DeviceConnectedScheduleReport, PassStatement3, FailStatement3);
	}

	@FindBy(xpath = "//p[text()='Report Permission']")
	private WebElement RoleWithAllReportPermission;

	public void disablingReportPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		RoleWithAllReportPermission.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Report Permission-Edited");
		sleep(4);
		clickonGroupPermissionUM.click();
		sleep(2);
		ReportPermissionsCheckBox.click();
		sleep(4);
		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void OnDemandReportsDisabled() throws InterruptedException {
		ReportButton.click();
		waitForidPresent("report_pg_cont");
		SystemlogOption.click();
		sleep(2);
		String Actualvalue = AccessDeniedErrorMessage.getText();
		String Expectedvalue = "Access denied. Please contact your administrator.";
		String PassStatement = "PASS >>'error Message displayed'";
		String FailStatement = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(4);

		AssetTracking.click();
		sleep(2);
		String Actualvalue2 = AccessDeniedErrorMessage.getText();
		String Expectedvalue2 = "Access denied. Please contact your administrator.";
		String PassStatement2 = "PASS >>'error Message displayed'";
		String FailStatement2 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue2, Actualvalue2, PassStatement2, FailStatement2);
		sleep(4);

		CallLogTracking.click();
		sleep(2);
		String Actualvalue3 = AccessDeniedErrorMessage.getText();
		String Expectedvalue3 = "Access denied. Please contact your administrator.";
		String PassStatement3 = "PASS >>'error Message displayed'";
		String FailStatement3 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue3, Actualvalue3, PassStatement3, FailStatement3);
		sleep(4);

		JobsDeployed.click();
		sleep(2);
		String Actualvalue4 = AccessDeniedErrorMessage.getText();
		String Expectedvalue4 = "Access denied. Please contact your administrator.";
		String PassStatement4 = "PASS >>'error Message displayed'";
		String FailStatement4 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue4, Actualvalue4, PassStatement4, FailStatement4);
		sleep(4);

		InstallJobReport.click();
		sleep(2);
		String Actualvalue5 = AccessDeniedErrorMessage.getText();
		String Expectedvalue5 = "Access denied. Please contact your administrator.";
		String PassStatement5 = "PASS >>'error Message displayed'";
		String FailStatement5 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue5, Actualvalue5, PassStatement5, FailStatement5);
		sleep(4);

		DeviceHealthReport.click();
		sleep(2);
		String Actualvalue6 = AccessDeniedErrorMessage.getText();
		String Expectedvalue6 = "Access denied. Please contact your administrator.";
		String PassStatement6 = "PASS >>'error Message displayed'";
		String FailStatement6 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue6, Actualvalue6, PassStatement6, FailStatement6);
		sleep(4);

		DeviceHistory.click();
		sleep(2);
		String Actualvalue7 = AccessDeniedErrorMessage.getText();
		String Expectedvalue7 = "Access denied. Please contact your administrator.";
		String PassStatement7 = "PASS >>'error Message displayed'";
		String FailStatement7 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue7, Actualvalue7, PassStatement7, FailStatement7);
		sleep(4);

		DataUsage.click();
		sleep(2);
		String Actualvalue8 = AccessDeniedErrorMessage.getText();
		String Expectedvalue8 = "Access denied. Please contact your administrator.";
		String PassStatement8 = "PASS >>'error Message displayed'";
		String FailStatement8 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue8, Actualvalue8, PassStatement8, FailStatement8);
		sleep(4);

		DeviceConnected.click();
		sleep(2);
		String Actualvalue9 = AccessDeniedErrorMessage.getText();
		String Expectedvalue9 = "Access denied. Please contact your administrator.";
		String PassStatement9 = "PASS >>'error Message displayed'";
		String FailStatement9 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue9, Actualvalue9, PassStatement9, FailStatement9);
		sleep(4);

		AppVersion.click();
		sleep(2);
		String Actualvalue10 = AccessDeniedErrorMessage.getText();
		String Expectedvalue10 = "Access denied. Please contact your administrator.";
		String PassStatement10 = "PASS >>'error Message displayed'";
		String FailStatement10 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue10, Actualvalue10, PassStatement10, FailStatement10);
		sleep(4);

		ComplianceReport.click();
		sleep(2);
		String Actualvalue11 = AccessDeniedErrorMessage.getText();
		String Expectedvalue11 = "Access denied. Please contact your administrator.";
		String PassStatement11 = "PASS >>'error Message displayed'";
		String FailStatement11 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue11, Actualvalue11, PassStatement11, FailStatement11);
		sleep(4);

	}

	public void ScheduleReportsDisabled() throws InterruptedException {
		ScheduleReports.click();
		sleep(4);
		SystemlogOption.click();
		sleep(2);
		String Actualvalue = AccessDeniedErrorMessage.getText();
		String Expectedvalue = "Access denied. Please contact your administrator.";
		String PassStatement = "PASS >>'error Message displayed'";
		String FailStatement = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(4);

		AssetTracking.click();
		sleep(2);
		String Actualvalue2 = AccessDeniedErrorMessage.getText();
		String Expectedvalue2 = "Access denied. Please contact your administrator.";
		String PassStatement2 = "PASS >>'error Message displayed'";
		String FailStatement2 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue2, Actualvalue2, PassStatement2, FailStatement2);
		sleep(4);

		CallLogTracking.click();
		sleep(2);
		String Actualvalue3 = AccessDeniedErrorMessage.getText();
		String Expectedvalue3 = "Access denied. Please contact your administrator.";
		String PassStatement3 = "PASS >>'error Message displayed'";
		String FailStatement3 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue3, Actualvalue3, PassStatement3, FailStatement3);
		sleep(4);

		JobsDeployed.click();
		sleep(2);
		String Actualvalue4 = AccessDeniedErrorMessage.getText();
		String Expectedvalue4 = "Access denied. Please contact your administrator.";
		String PassStatement4 = "PASS >>'error Message displayed'";
		String FailStatement4 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue4, Actualvalue4, PassStatement4, FailStatement4);
		sleep(4);

		InstallJobReport.click();
		sleep(2);
		String Actualvalue5 = AccessDeniedErrorMessage.getText();
		String Expectedvalue5 = "Access denied. Please contact your administrator.";
		String PassStatement5 = "PASS >>'error Message displayed'";
		String FailStatement5 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue5, Actualvalue5, PassStatement5, FailStatement5);
		sleep(4);

		DeviceHealthReport.click();
		sleep(2);
		String Actualvalue6 = AccessDeniedErrorMessage.getText();
		String Expectedvalue6 = "Access denied. Please contact your administrator.";
		String PassStatement6 = "PASS >>'error Message displayed'";
		String FailStatement6 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue6, Actualvalue6, PassStatement6, FailStatement6);
		sleep(4);

		DeviceHistory.click();
		sleep(2);
		String Actualvalue7 = AccessDeniedErrorMessage.getText();
		String Expectedvalue7 = "Access denied. Please contact your administrator.";
		String PassStatement7 = "PASS >>'error Message displayed'";
		String FailStatement7 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue7, Actualvalue7, PassStatement7, FailStatement7);
		sleep(4);

		DataUsage.click();
		sleep(2);
		String Actualvalue8 = AccessDeniedErrorMessage.getText();
		String Expectedvalue8 = "Access denied. Please contact your administrator.";
		String PassStatement8 = "PASS >>'error Message displayed'";
		String FailStatement8 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue8, Actualvalue8, PassStatement8, FailStatement8);
		sleep(4);

		DeviceConnected.click();
		sleep(2);
		String Actualvalue9 = AccessDeniedErrorMessage.getText();
		String Expectedvalue9 = "Access denied. Please contact your administrator.";
		String PassStatement9 = "PASS >>'error Message displayed'";
		String FailStatement9 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue9, Actualvalue9, PassStatement9, FailStatement9);
		sleep(4);

		AppVersion.click();
		sleep(2);
		String Actualvalue10 = AccessDeniedErrorMessage.getText();
		String Expectedvalue10 = "Access denied. Please contact your administrator.";
		String PassStatement10 = "PASS >>'error Message displayed'";
		String FailStatement10 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue10, Actualvalue10, PassStatement10, FailStatement10);
		sleep(4);

		ComplianceReport.click();
		sleep(2);
		String Actualvalue11 = AccessDeniedErrorMessage.getText();
		String Expectedvalue11 = "Access denied. Please contact your administrator.";
		String PassStatement11 = "PASS >>'error Message displayed'";
		String FailStatement11 = "FAIL >> 'error Message not displayed'";
		ALib.AssertEqualsMethod(Expectedvalue11, Actualvalue11, PassStatement11, FailStatement11);
		sleep(4);

	}

	// ########################################### DASHBOARD PERMISSION
	// ######################################

	@FindBy(xpath = "//ul/li[text()='Dashboard Permissions']/span[3]")
	private WebElement DashboardCheckBox;

	public void DashboardPermissionRole() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Dashboard Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER F");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		DashboardCheckBox.click();
		// sleep(3);

		sleep(3);
		saveButton.click();
		sleep(4);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();

	}

	@FindBy(xpath = "//p[text()='Dashboard Permission']")
	private WebElement RoleWithAllDasahboardPermission;

	public void disablingDashboardPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		RoleWithAllDasahboardPermission.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Dashboaord Permission-Edited");
		sleep(4);
		clickonGroupPermissionUM.click();
		sleep(4);
		DashboardCheckBox.click();
		sleep(4);
		saveButton.click();
		sleep(4);
		// boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		// String PassStatement3 = "PASS >> Role Edited has been displayed ";
		// String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		// ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void errormessageDashboard() throws InterruptedException {
		sleep(2);
		boolean displayRoleCreated = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
	}

	// ###################################################### FILE STORE
	// ########################################################

	@FindBy(xpath = "//ul/li[text()='File Store Permissions']/span[3]")
	private WebElement FileStoreCheckBox;

	public void FileStorePermissionRole() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Filestore Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER pij");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		FileStoreCheckBox.click();
		saveButton.click();
		sleep(4);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();

	}

	@FindBy(xpath = ".//*[@id='efssSection']/a")
	private WebElement FileStoreToolBar;

	public void clickOnfilestore() throws InterruptedException {
		FileStoreToolBar.click();

		waitForidPresent("EFSSDisplayArea");
		sleep(7);
	}

	@FindBy(xpath = "//span[text()='New folder']")
	private WebElement Newfolder;

	public void clickOnNewFolder() {
		Newfolder.click();
	}

	@FindBy(xpath = ".//*[@id='elfinder']/div[1]/div[2]/div[6]/span[text()='Delete']")
	private WebElement DeleteButtonFileStore;

	@FindBy(xpath = ".//*[@id='elfinder']/div[7]/div[3]/div/button[2]")
	private WebElement NoButtonFileDelete;

	public void ClickOnDeleteButton() throws InterruptedException {
		sleep(5);
		DeleteButtonFileStore.click();
		waitForidPresent("elfinder");
		NoButtonFileDelete.click();
	}

	@FindBy(xpath = "//span[text()='Upload files']")
	private WebElement UploadFilesbutton;

	public boolean clickOnuploadFile() {
		boolean value = true;
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Upload files']")).isDisplayed();
		} catch (Exception e) {
			value = false;
		}
		return value;
	}

	public void verifyFileuploadbutton() throws InterruptedException {
		boolean value = clickOnuploadFile();
		sleep(5);
		String PassStatement = "PASS >>  fileupload button is presesnt";
		String FailStatement = "FAIL >> fileupload button is not present";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	@FindBy(xpath = "//p[text()='Filestore Permission']")
	private WebElement RoleWithAllFileStorePermission;

	public void DisablingAllPermissons() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		RoleWithAllFileStorePermission.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("FileStore Permission-Edited");
		sleep(4);
		clickonGroupPermissionUM.click();
		sleep(2);
		FileStoreCheckBox.click();
		sleep(4);
		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void NewFolderDisabled() throws InterruptedException {
		sleep(4);
		if (Newfolder.isDisplayed())
			Reporter.log("Fail>>NewFolder is enabled ", true);
		else
			Reporter.log("PASS>>New Folder is disabled", true);

	}

	public void deleteButtonDisabled() throws InterruptedException {
		sleep(4);
		if (DeleteButtonFileStore.isDisplayed())
			Reporter.log("Fail>>Delete button is enabled ", true);
		else
			Reporter.log("PASS>>Delete button is disabled", true);
	}

	public void FileUploadButtonDisabled() throws InterruptedException {
		sleep(4);
		if (UploadFilesbutton.isDisplayed())
			Reporter.log("Fail>>FileUpload button is enabled ", true);
		else
			Reporter.log("PASS>>FileUpload button is disabled", true);

	}
	// ############################## PROFILE PERMISSIONS
	// ##################################################

	@FindBy(xpath = "//li[text()='Profile Permissions']/span[3]")
	private WebElement ProfilePermissionCheckBox;

	public void CreateRoleProfilePermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Profile Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER pij");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		ProfilePermissionCheckBox.click();
		saveButton.click();
		sleep(4);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Roles created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();
	}

	@FindBy(xpath = ".//*[@id='byodSection']/a[text()='Profiles']")
	private WebElement ProfileButton;

	public void clickOnProfile() throws InterruptedException {
		ProfileButton.click();
		sleep(4);
		waitForidPresent("selectButtonmacOS");
	}

	@FindBy(xpath = "//td[text()='AutoAndroid']")
	private WebElement SelectAndroidProfile;

	public void SelectAndroidProfileUser() throws InterruptedException {
		sleep(4);
		SelectAndroidProfile.click();
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='addPolicyBtn']")
	private WebElement AddProfileButton;

	@FindBy(xpath = ".//*[@id='androidPolicyDetails']/p/a[text()='Android']")
	private WebElement AndroidHeader;

	@FindBy(xpath = ".//*[@id='androidPolicyDetails']/div/div[2]/div/div[1]/div/div[2]/button[1]")
	private WebElement BackButton;

	public void AddAndroidProfileEnable() throws InterruptedException {
		sleep(2);
		AddProfileButton.click();
		sleep(3);
		waitForidPresent("androidprofiledefaultab");
		boolean AddAndroid = AndroidHeader.isDisplayed();
		String PassStatement3 = "PASS >>Add Android Profile Enable ";
		String FailStatement3 = "FAIL >> Add Android Profile disabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement3, FailStatement3);
		sleep(4);
		BackButton.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='editPolicyBtn']")
	private WebElement EditButtonProfile;

	@FindBy(xpath = ".//*[@id='policyName']")
	private WebElement ProfileNameTextField;

	public void EditAndroidProfileEnabled() throws InterruptedException {
		sleep(3);
		EditButtonProfile.click();
		sleep(2);
		waitForidPresent("androidprofiledefaultab");
		boolean AddAndroid = ProfileNameTextField.isDisplayed();
		String PassStatement3 = "PASS >>Add Android Profile Enable ";
		String FailStatement3 = "FAIL >> Add Android Profile disabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement3, FailStatement3);
		sleep(4);
		BackButton.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='deletePolicyBtn']")
	private WebElement DeleteprofileButton;

	public void DeleteAndroidProfileEnable() throws InterruptedException {
		sleep(3);
		DeleteprofileButton.click();
		waitForidPresent("ConfirmationDialog");
		sleep(2);
		NoDialogueBoxJobDelete.click();
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='setDefaultPolicy']")
	private WebElement SetDefaultPolicy;

	@FindBy(xpath = "//p[text()='AutoAndroid']")
	private WebElement AutoAndroidDefault;

	@FindBy(xpath = "//span[text()='Remove Default']")
	private WebElement RemoveDefaultButton;

	public void SetDefaultPolicyAndroidProfileEnable() throws InterruptedException {
		sleep(3);
		SetDefaultPolicy.click();
		sleep(4);
		AutoAndroidDefault.click();
		sleep(5);
		RemoveDefaultButton.click();
		sleep(4);

	}

	@FindBy(xpath = ".//*[@id='afw_appsConfig_btn']")
	private WebElement EnrollAFWAndroidProfile;

	@FindBy(xpath = ".//*[@id='afw_apps_modal']/div/div/div[1]/button")
	private WebElement CloseButtonEnrollAFW;

	public void EnrollAFWAndroidProfileEnabled() throws InterruptedException {
		sleep(5);
		EnrollAFWAndroidProfile.click();
		waitForidPresent("enroll_AFW_pop");
		sleep(2);
		CloseButtonEnrollAFW.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='selectButtoniOS']")
	private WebElement IOSProfile;

	public void clickOnIOSprofile() throws InterruptedException {
		sleep(5);
		IOSProfile.click();
		waitForidPresent("profileList_tableCont");
		sleep(3);

	}

	@FindBy(xpath = "//td[text()='AutoIOS']")
	private WebElement SelectIOSprofile;

	@FindBy(xpath = ".//*[@id='setDefaultPolicy']")
	private WebElement RemoveDefaultOption;

	public void SelectIOSprofile() throws InterruptedException {
		sleep(4);
		SelectIOSprofile.click();
		sleep(3);
	}

	@FindBy(xpath = "//span[text()='Blocklist/Allowlist Apps ']")
	private WebElement BlacklistWhitelistApp;

	@FindBy(xpath = ".//*[@id='iOSPolicyDetails']/div/div[2]/div/div[1]/div/div[2]/button[text()='Back']")
	private WebElement BackButtonIOS;

	public void AddIOSEnable() throws InterruptedException {
		AddProfileButton.click();
		sleep(4);
		boolean AddAndroid = BlacklistWhitelistApp.isDisplayed();
		String PassStatement3 = "PASS >>Add Android Profile Enable ";
		String FailStatement3 = "FAIL >> Add Android Profile disabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement3, FailStatement3);
		sleep(4);
		BackButtonIOS.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='blacklistwhitelistAppsProfileRemoveTail']")
	private WebElement RemoveButton;

	public void EditIOSprofile() throws InterruptedException {
		EditButtonProfile.click();
		// waitForidPresent("blacklistwhitelistAppsProfileTail");
		sleep(3);
		boolean AddAndroid = RemoveButton.isDisplayed();
		String PassStatement3 = "PASS >>Edit IOS Profile is Enable ";
		String FailStatement3 = "FAIL >> Edit IOS Profile is disabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement3, FailStatement3);
		sleep(4);
		BackButtonIOS.click();
		sleep(2);
	}

	public void DeleteIOSprofile() throws InterruptedException {
		sleep(3);
		DeleteprofileButton.click();
		waitForidPresent("ConfirmationDialog");
		sleep(2);
		NoDialogueBoxJobDelete.click();
		sleep(3);

	}

	@FindBy(xpath = "//p[text()='AutoIOS']")
	private WebElement AutoIOSDefault;

	@FindBy(xpath = "//span[text()='Removed default profile']")
	private WebElement NotificationOnRemovingDefaultProfile;

	public void NotificationOnRemovingDefaultProfile() throws InterruptedException {
		boolean value = true;

		try {
			NotificationOnRemovingDefaultProfile.isDisplayed();

		} catch (Exception e) {
			value = false;

		}

		Assert.assertTrue(value, "FAIL >> 'Removed Default profile' Notification not displayed ");
		Reporter.log("PASS >> Notification 'Removed default profile 'is displayed", true);
		waitForidPresent("addPolicyBtn");
		sleep(4);
	}

	public void SetDefaultPolicyIOSProfileEnable() throws InterruptedException {
		sleep(3);
		SetDefaultPolicy.click();
		sleep(4);
		AutoIOSDefault.click();

		sleep(5);
		RemoveDefaultOption.click();
		sleep(7);

	}

	@FindBy(xpath = ".//*[@id='selectButtonWindows']")
	private WebElement SelectWindowsButton;

	public void clickOnWindowsProfile() throws InterruptedException {
		sleep(4);
		SelectWindowsButton.click();
		waitForidPresent("policyTable");
		sleep(4);
	}

	@FindBy(xpath = "//td[text()='AutoWindows']")
	private WebElement SelectWindowsprofile;

	public void WindowsProfileSelect() throws InterruptedException {
		SelectWindowsprofile.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='WindowsTabProfileContent']/p/a")
	private WebElement WindowsContent;

	@FindBy(xpath = ".//*[@id='WindowsTabProfileContent']/div[1]/div[2]/div/div[1]/div/div[2]/button[text()='Back']")
	private WebElement BackButtonWindows;

	public void AddWindowsProfile() throws InterruptedException {
		AddProfileButton.click();
		sleep(4);
		boolean AddAndroid = WindowsContent.isDisplayed();
		String PassStatement3 = "PASS >>Add Windows Profile is Enable ";
		String FailStatement3 = "FAIL >> Add Windows Profile is disabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement3, FailStatement3);
		sleep(4);
		BackButtonWindows.click();
		sleep(2);

	}

	public void EditWindowsProfile() throws InterruptedException {
		EditButtonProfile.click();
		sleep(4);
		BackButtonWindows.click();
		sleep(2);
		Reporter.log("Edit option for windows is enabled");
	}

	public void DeletewindowsProfile() throws InterruptedException {
		sleep(3);
		DeleteprofileButton.click();
		waitForidPresent("ConfirmationDialog");
		sleep(2);
		NoDialogueBoxJobDelete.click();
		sleep(3);
	}

	public void SetDefaultPolicyWindowsProfileEnable() throws InterruptedException {
		sleep(3);
		SetDefaultPolicy.click();
		sleep(4);
		AutoIOSDefault.click();

		sleep(5);
		RemoveDefaultOption.click();
		sleep(7);

	}

	@FindBy(xpath = ".//*[@id='selectButtonmacOS']")
	private WebElement MacOSButton;

	public void ClickOnMacOsProfile() throws InterruptedException {
		sleep(4);
		MacOSButton.click();
		waitForidPresent("policyTable");
		sleep(4);
	}

	@FindBy(xpath = "//td[text()='AutoMacOs']")
	private WebElement SelectMacOsprofile;

	public void MacOsProfileSelect() throws InterruptedException {
		SelectMacOsprofile.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='policy_item_box']/div[3]/div/ul/li[5]")
	private WebElement PasscodePolicy;

	@FindBy(xpath = ".//*[@id='macOSTabProfileContent']/div/div[2]/div/div[1]/div/div[2]/button[text()='Back']")
	private WebElement BackMacOs;

	public void AddMacOsProfileEnable() throws InterruptedException {
		AddProfileButton.click();
		sleep(4);
		waitForidPresent("policy_item_box");
		sleep(2);
		// boolean AddAndroid = PasscodePolicy.isDisplayed();
		// String PassStatement3 = "PASS >>Add MacOs Profile is Enable ";
		// String FailStatement3 = "FAIL >> Add MacOs Profile is disabled";
		// ALib.AssertTrueMethod(AddAndroid, PassStatement3, FailStatement3);
		// sleep(4);
		BackMacOs.click();
	}

	public void EditMAcOSProfile() throws InterruptedException {
		EditButtonProfile.click();
		sleep(7);
		BackMacOs.click();
		sleep(4);
		Reporter.log("Edit option for MacOs is enabled");
	}

	public void DeleteMacOSprofile() throws InterruptedException {
		sleep(3);
		DeleteprofileButton.click();
		waitForidPresent("ConfirmationDialog");
		sleep(2);
		NoDialogueBoxJobDelete.click();
		sleep(3);
	}

	@FindBy(xpath = "//p[text()='AutoMacOs']")
	private WebElement AutoMAcOsDefault;

	public void SetDefaultPolicyMacOsProfileEnable() throws InterruptedException {
		sleep(3);

		SetDefaultPolicy.click();

		sleep(5);

		// AutoMAcOsDefault.click();

		sleep(5);
		// RemoveDefaultOption.click();
		sleep(7);

	}

	@FindBy(xpath = "//p[text()='Profile Permission']")
	private WebElement RollWithAllProfilePermissions;

	public void DisablingAllProfilePermsissions() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		RollWithAllProfilePermissions.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Profile Permission-Edited");
		sleep(4);
		clickonGroupPermissionUM.click();
		sleep(2);
		ProfilePermissionCheckBox.click();
		sleep(4);
		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	@FindBy(xpath = ".//*[@id='enrollgoogleplayaccount']")
	private WebElement EnrollButton;

	@FindBy(xpath = ".//*[@id='enroll_AFW_pop']/div/div/div[1]/button")
	private WebElement CloseEnroll;

	public void DisabledAndroidProfiles() throws InterruptedException {
		sleep(2);
		AddProfileButton.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Android Profile is disabled";
		String FailStatement1 = "FAIL >> Add Android Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);

		EditButtonProfile.click();
		sleep(4);
		boolean EditAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement2 = "PASS >> Edit Android Profile is disabled";
		String FailStatement2 = "FAIL >> Edit Android Profile is Enabled";
		ALib.AssertTrueMethod(EditAndroid, PassStatement2, FailStatement2);
		sleep(4);

		DeleteprofileButton.click();
		sleep(4);
		boolean DeleteAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> Edit Android Profile is disabled";
		String FailStatement3 = "FAIL >> Edit Android Profile is Enabled";
		ALib.AssertTrueMethod(DeleteAndroid, PassStatement3, FailStatement3);
		sleep(4);

		SetDefaultPolicy.click();
		sleep(4);
		boolean SetDefaultAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement4 = "PASS >> Edit Android Profile is disabled";
		String FailStatement4 = "FAIL >> Edit Android Profile is Enabled";
		ALib.AssertTrueMethod(SetDefaultAndroid, PassStatement4, FailStatement4);
		sleep(4);

		// EnrollAFWAndroidProfile.click();
		sleep(2);
		// EnrollButton.click();
		// sleep(4);
		// boolean EnrollAFWandroid = AccessDeniedErrorMessage.isDisplayed();
		// String PassStatement5 = "PASS >> Edit Android Profile is disabled";
		// String FailStatement5 = "FAIL >> Edit Android Profile is Enabled";
		// ALib.AssertTrueMethod(EnrollAFWandroid, PassStatement5, FailStatement5);
		sleep(4);
		// CloseEnroll.click();
	}

	public void DisabledIOSProfiles() throws InterruptedException {
		sleep(2);
		AddProfileButton.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Android Profile is disabled";
		String FailStatement1 = "FAIL >> Add Android Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);

		EditButtonProfile.click();
		sleep(4);
		boolean EditAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement2 = "PASS >> Edit Android Profile is disabled";
		String FailStatement2 = "FAIL >> Edit Android Profile is Enabled";
		ALib.AssertTrueMethod(EditAndroid, PassStatement2, FailStatement2);
		sleep(4);

		DeleteprofileButton.click();
		sleep(4);
		boolean DeleteAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> Edit Android Profile is disabled";
		String FailStatement3 = "FAIL >> Edit Android Profile is Enabled";
		ALib.AssertTrueMethod(DeleteAndroid, PassStatement3, FailStatement3);
		sleep(4);

		SetDefaultPolicy.click();
		sleep(4);
		boolean SetDefaultAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement4 = "PASS >> Edit Android Profile is disabled";
		String FailStatement4 = "FAIL >> Edit Android Profile is Enabled";
		ALib.AssertTrueMethod(SetDefaultAndroid, PassStatement4, FailStatement4);
		sleep(4);

	}

	public void DisabledWindowsProfiles() throws InterruptedException {
		sleep(2);
		AddProfileButton.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);

		EditButtonProfile.click();
		sleep(4);
		boolean EditAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement2 = "PASS >> Edit Windows Profile is disabled";
		String FailStatement2 = "FAIL >> Edit Windows Profile is Enabled";
		ALib.AssertTrueMethod(EditAndroid, PassStatement2, FailStatement2);
		sleep(4);

		DeleteprofileButton.click();
		sleep(4);
		boolean DeleteAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> Edit Windows Profile is disabled";
		String FailStatement3 = "FAIL >> Edit Windows Profile is Enabled";
		ALib.AssertTrueMethod(DeleteAndroid, PassStatement3, FailStatement3);
		sleep(4);

		SetDefaultPolicy.click();
		sleep(4);
		boolean SetDefaultAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement4 = "PASS >> Edit Windows Profile is disabled";
		String FailStatement4 = "FAIL >> Edit Windows Profile is Enabled";
		ALib.AssertTrueMethod(SetDefaultAndroid, PassStatement4, FailStatement4);
		sleep(4);

	}

	public void DisabledMacOsProfiles() throws InterruptedException {
		sleep(2);
		AddProfileButton.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);

		EditButtonProfile.click();
		sleep(4);
		boolean EditAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement2 = "PASS >> Edit Windows Profile is disabled";
		String FailStatement2 = "FAIL >> Edit Windows Profile is Enabled";
		ALib.AssertTrueMethod(EditAndroid, PassStatement2, FailStatement2);
		sleep(4);

		DeleteprofileButton.click();
		sleep(4);
		boolean DeleteAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> Edit Windows Profile is disabled";
		String FailStatement3 = "FAIL >> Edit Windows Profile is Enabled";
		ALib.AssertTrueMethod(DeleteAndroid, PassStatement3, FailStatement3);
		sleep(4);

		SetDefaultPolicy.click();
		sleep(4);
		boolean SetDefaultAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement4 = "PASS >> Edit Windows Profile is disabled";
		String FailStatement4 = "FAIL >> Edit Windows Profile is Enabled";
		ALib.AssertTrueMethod(SetDefaultAndroid, PassStatement4, FailStatement4);
		sleep(4);

	}

	// ###################################### APP STORE PERMISSIONS
	// ########################################################

	@FindBy(xpath = "//ul/li[text()='App Store Permissions']/span[3]")
	private WebElement AppStoreCheckbox;

	public void CreateRoleAppStorePermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("AppStore Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER pij");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AppStoreCheckbox.click();
		saveButton.click();
		sleep(2);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Role created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();
	}

	@FindBy(xpath = ".//*[@id='mamSection']/a")
	private WebElement AppStoreButton;

	public void clickOnAppStore() throws InterruptedException {
		sleep(2);
		AppStoreButton.click();
		waitForidPresent("andriodEAM");
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='addNewAppBtn']")
	private WebElement AddAndroidButton;

	@FindBy(xpath = ".//*[@id='AddNewAppPopup']/div/div/div[1]/button")
	private WebElement closeSelectOptionTab;

	public void clickOnAddandroiod() throws InterruptedException {
		AddAndroidButton.click();
		waitForidPresent("UploadApkMam");
		sleep(4);
		closeSelectOptionTab.click();
		sleep(4);

	}

	@FindBy(xpath = "//div[p[text()='SureLock']]/following-sibling::div/div/ul/li/a[text()='Edit']")
	private WebElement EditOptionAndroidapp;// change later when running in new dns or mdm2.

	@FindBy(xpath = ".//*[@id='app_1490d042-de70-467a-b084-4232878169b6']/div[2]/div/div[2]/div/span")
	private WebElement AddAdnroidMenu;

	@FindBy(xpath = "//*[@id='editAppLinkPopup']/div/div/div[1]/button")
	private WebElement EditCloseButton;

	public void ClickOnMore(String appname) throws InterruptedException {
		Initialization.driver
		.findElement(By.xpath("//div[p[text()='" + appname + "']]/following-sibling::div/div/span")).click();
		waitForXpathPresent("//div[p[text()='" + appname + "']]/following-sibling::div/div/ul/li/a[text()='Edit']");
		sleep(4);
		Reporter.log("Clicked on More Symbol, Navigates successfully to Option field with Edit and Remove", true);
	}

	public void clickOneditandroiod() throws InterruptedException {

		sleep(2);
		EditOptionAndroidapp.click();
		waitForidPresent("editAppPopup");
		sleep(4);
		EditCloseButton.click();
		sleep(6);

	}

	@FindBy(xpath = "//div[p[text()='SureLock']]/following-sibling::div/div/ul/li/a[text()='Remove']")
	private WebElement RemoveOptionAndroidapp;// change later when running in new dns or mdm2.

	public void clickOnRemove() throws InterruptedException {

		RemoveOptionAndroidapp.click();
		waitForidPresent("ConfirmationDialog");
		sleep(4);
		NoDialogueBoxJobDelete.click();

	}

	@FindBy(xpath = ".//*[@id='iOSEAM']/a")
	private WebElement IosAppStore;

	public void clickOnIosButton() throws InterruptedException {
		sleep(4);
		IosAppStore.click();
		waitForidPresent("iOSaddNewAppBtn");
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='iOSaddNewAppBtn']")
	private WebElement AddIosAppButton;

	@FindBy(xpath = ".//*[@id='iOSAddNewAppPopupClose']")
	private WebElement IosAddClose;

	public void ClickOnAddIos() throws InterruptedException {
		sleep(20);
		AddIosAppButton.click();
		sleep(3);
		IosAddClose.click();
		sleep(2);
	}

	public void ClickOnAddIosBtn() throws InterruptedException {
		sleep(40);
		AddIosAppButton.click();

	}

	@FindBy(xpath = ".//*[@id='app_5add4e46-fef0-4d01-b2f1-9c3120ceaa7d']/div[2]/div/div[2]/div/span/i")
	private WebElement IosMenu;

	@FindBy(xpath = "(//div[p[text()='SureFox']]/following-sibling::div/div/ul/li/a[text()='Edit'])[1]")
	private WebElement EditOptionIosapp;

	public void clickOnIosMenu() throws InterruptedException {
		sleep(4);
		IosMenu.click();
		sleep(2);

	}

	@FindBy(xpath = "//*[@id='editAppPopup']/div/div/div[1]/button")
	private WebElement closeEeditIos;

	public void clickonEditIos() throws InterruptedException {
		try {
			EditOptionIosapp.click();
		} catch (Exception e) {
			Initialization.driver
			.findElement(By
					.xpath("(//div[p[text()='SureFox']]/following-sibling::div/div/ul/li/a[text()='Edit'])[2]"))
			.click();
		}
		sleep(4);
		waitForidPresent("editAppLinkPopup");
		sleep(3);
		closeEeditIos.click();
		sleep(4);

	}

	@FindBy(xpath = "(//div[p[text()='SureFox']]/following-sibling::div/div/ul/li/a[text()='Remove'])[2]")
	private WebElement RemoveOptionIosapp;

	public void ClickOnRemoveIos() throws InterruptedException {
		RemoveOptionIosapp.click();
		sleep(4);
		waitForidPresent("ConfirmationDialog");
		sleep(4);
		NoDialogueBoxJobDelete.click();
	}

	@FindBy(xpath = "//p[text()='AppStore Permission']")
	private WebElement SelectAppStoreRole;

	public void DisablingAppStorePermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(4);
		SelectAppStoreRole.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("AppStore Permission-Edited");
		clickonGroupPermissionUM.click();
		sleep(4);
		AppStoreCheckbox.click();
		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void DisabledAddAndroidAppStore() throws InterruptedException {
		AddAndroidButton.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void DisabledEditAndroidPermission() throws InterruptedException {

		EditOptionAndroidapp.click();
		waitForXpathPresent("//span[text()='Access denied. Please contact your administrator.']");
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void DisabledRemoveAndroidPermission() throws InterruptedException {

		RemoveOptionAndroidapp.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void DisabledAddIOSPermission() throws InterruptedException {
		AddIosAppButton.click();
		sleep(3);
		waitForXpathPresent("//span[text()='Access denied. Please contact your administrator.']");
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);
	}

	public void DisabledEditIOSPermission() throws InterruptedException {

		try {
			EditOptionIosapp.click();
			sleep(3);
			boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
			String PassStatement1 = "PASS >> Add Windows Profile is disabled";
			String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
			ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
			sleep(4);
		} catch (Exception e) {
			Initialization.driver
			.findElement(By
					.xpath("(//div[p[text()='SureFox']]/following-sibling::div/div/ul/li/a[text()='Edit'])[2]"))
			.click();
			sleep(3);
			boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
			String PassStatement1 = "PASS >> Add Windows Profile is disabled";
			String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
			ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
			sleep(4);
		}
	}

	public void DisabledRemovePermission() throws InterruptedException {
		RemoveOptionIosapp.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);

	}
	// ################################################# OTHER PERMISSIONS
	// ###################################################

	@FindBy(xpath = "//ul/li[text()='Other Permissions']/span[3]")
	private WebElement OtherPermissionCheckBox;

	public void RoleWithOtherPermissions() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("other Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER ninja");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);

		OtherPermissionCheckBox.click();
		saveButton.click();
		sleep(4);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Role created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();
	}

	@FindBy(xpath = ".//*[@id='logContainer']/div[2]/button")
	private WebElement ActivityLogContainer;

	public void ActivityLogEnabled() throws InterruptedException {
		sleep(3);
		boolean AddAndroid = ActivityLogContainer.isDisplayed();
		String PassStatement1 = "PASS >> Activity Log is enabled";
		String FailStatement1 = "FAIL >> Activity log is disabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);

	}

	@FindBy(xpath = ".//*[@id='changePassword']")
	private WebElement ChangePasswordButton;

	@FindBy(xpath = ".//*[@id='commonModalSmall']/div/div/div[1]/button")
	private WebElement CloseButtonChangePassword;

	public void clickOnChangePassword() throws InterruptedException {
		ChangePasswordButton.click();
		waitForidPresent("commonModalSmall");
		sleep(3);
		CloseButtonChangePassword.click();
		sleep(4);
	}

	@FindBy(xpath = "//p[text()='other Permission']")
	private WebElement OtherPermissionRole;

	public void DisablingOtherPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(4);
		OtherPermissionRole.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Other Permission-Edited");
		clickonGroupPermissionUM.click();
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		OtherPermissionCheckBox.click();
		sleep(4);

		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void DisabledActivityLog() throws InterruptedException {
		sleep(4);

	}

	public boolean IsTestElementPresent() {

		try {
			Initialization.driver.findElement(By.xpath(".//*[@id='logContainer']/div[2]/button"));
			return true;
		} catch (Exception e) {

			e.printStackTrace();

		}
		return false;
	}

	public void ChangePasswordDisabled() throws InterruptedException {
		ChangePasswordButton.click();
		sleep(3);
		boolean AddAndroid = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> Add Windows Profile is disabled";
		String FailStatement1 = "FAIL >> Add Windows Profile is Enabled";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);
	}
	// ###################################################### LOCATION PERMISSION
	// ###############################################

	@FindBy(xpath = "//ul/li[text()='Location Permissions']/span[3]")
	private WebElement LocationPermissionCheckBox;

	public void RoleWithLocationPermissions() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Location Permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER ninja");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		DeviceActionDRopDowm.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(4);
		DeviceActionDRopDowm.click();
		sleep(3);
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);

		LocationPermissionCheckBox.click();
		saveButton.click();
		sleep(4);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Roles created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();
	}

	@FindBy(xpath = ".//*[@id='locateBtn']/i")
	private WebElement LocateButton;

	@FindBy(xpath = ".//*[@id='realtimetab']/a")
	private WebElement LoctePageNewTAb;

	@FindBy(xpath = ".//*[@id='historytab']/a")
	private WebElement HistoryButton;

	@FindBy(xpath = ".//*[@id='clearBtn']")
	private WebElement ClearHistoryLocation;

	@FindBy(xpath = "//*[@id='deviceHistClear']/div/div/div[2]/button[2]")
	private WebElement ClearHistoryOkBtn;

	@FindBy(xpath = "//span[text()='Device history cleared successfully']")
	private WebElement DeviceHistoryClearedPopUP;

	@FindBy(xpath = ".//*[@id='modifybtn']")
	private WebElement modifyButtonLocation;

	@FindBy(xpath = ".//*[@id='modifyTable']/div/div/div[1]/button")
	private WebElement CloseButtonModifyPopUP;

	public void ClickOnLocteDevice() throws InterruptedException {

		String originalHandle = Initialization.driver.getWindowHandle();
		sleep(10);
		LocateButton.click();
		sleep(5);
		Initialization.driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		// waitForidPresent("realtimetab");
		Initialization.driver.switchTo().window(tabs.get(0));
		// waitForidPresent("realtimetab");
		// waitForidPresent("realtimetab");
		sleep(5);

		boolean isdisplayed = true;

		try {
			LoctePageNewTAb.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL: 'Locte Device is not displayed'");
		Reporter.log("PASS>> 'Locate' page launched succesfully", true);

		waitForidPresent("realtimetab");
		sleep(8);
		HistoryButton.click();
		sleep(5);
		ClearHistoryLocation.click();
		sleep(5);
		boolean AddAndroid = DeviceHistoryClearedPopUP.isDisplayed();
		String PassStatement1 = "PASS >>Device history cleared displayed";
		String FailStatement1 = "FAIL >> Device history cleared not displayed";
		ALib.AssertTrueMethod(AddAndroid, PassStatement1, FailStatement1);
		sleep(4);
		modifyButtonLocation.click();
		waitForidPresent("modifyTable");
		sleep(2);
		CloseButtonModifyPopUP.click();
		sleep(3);

		sleep(2);
		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}
		sleep(5);

		Initialization.driver.switchTo().window(originalHandle);

		sleep(7);

	}

	@FindBy(xpath = "//p[text()='Location Permission']")
	private WebElement LoctionPermissionrole;

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[2]/span[1]")
	private WebElement DeviceActionDRopDowm;

	public void DisablingLocationPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(4);
		LoctionPermissionrole.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Location Permission-Edited");
		clickonGroupPermissionUM.click();
		sleep(2);
		DeviceActionDRopDowm.click();
		WebElement scrollDown = ScrollDownToElement;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);
		LocationPermissionCheckBox.click();
		sleep(4);

		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void AccessDeniedLocationPermission() throws InterruptedException {
		String originalHandle = Initialization.driver.getWindowHandle();
		sleep(10);
		LocateButton.click();
		sleep(5);
		Initialization.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

		ArrayList<String> tabs = new ArrayList<String>(Initialization.driver.getWindowHandles());
		// waitForidPresent("realtimetab");
		Initialization.driver.switchTo().window(tabs.get(1));
		// waitForidPresent("realtimetab");
		// waitForidPresent("realtimetab");
		sleep(5);

		boolean isdisplayed = true;

		try {
			LoctePageNewTAb.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "FAIL: 'Locte Device is not displayed'");
		Reporter.log("PASS>> 'Locate' page launched succesfully", true);

		waitForidPresent("realtimetab");
		sleep(5);
		HistoryButton.click();
		sleep(4);
		ClearHistoryLocation.click();
		ClearHistoryOkBtn.click();
		sleep(3);
		boolean clearHistory = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement1 = "PASS >> History Button disabled";
		String FailStatement1 = "FAIL >>  History Button is Enabled";
		ALib.AssertTrueMethod(clearHistory, PassStatement1, FailStatement1);
		sleep(4);

		modifyButtonLocation.click();
		sleep(2);
		boolean modify = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement2 = "PASS >> History Button disabled";
		String FailStatement2 = "FAIL >>  History Button is Enabled";
		ALib.AssertTrueMethod(modify, PassStatement2, FailStatement2);

		sleep(2);
		for (String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);

		sleep(3);

	}
	// ######################################### USERMANAGEMENT PERMISSION
	// #################################################

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[8]/span[3]")
	private WebElement UsermanagemnetCheckBox;

	public void CreateRoleUsermanagemnetPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(2);
		clickOnAddButtonUserManagement.click();
		sleep(4);
		NameTextField.sendKeys("Usermanagement permission");
		sleep(2);

		descriptionTestField.sendKeys("FOR USER ninja");
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(2);
		AllowHomeGroupcheckBox.click();
		sleep(2);
		clickonGroupPermissionUM.click();
		sleep(4);
		UsermanagemnetCheckBox.click();
		sleep(4);
		saveButton.click();
		sleep(2);
		String Actualvalue = RoleCreatedSuccessfully.getText();
		String Expectedvalue = "Role created successfully.";
		String PassStatement = "PASS >>'Role created successfully.- displayed'";
		String FailStatement = "FAIL >> Warning message is wrong- in Roles window or Failed to save in Roles window";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

		User.click();
	}

	@FindBy(xpath = "//span[text()='Device Grid Column Set']")
	private WebElement scrollDownCancle;

	@FindBy(xpath = ".//*[@id='cancelbtn']")
	private WebElement cancleButtonUser;

	public void UsermanagementAccess() throws InterruptedException {
		sleep(4);
		usermanagementlink.click();
		waitForidPresent("userManagement_pg");
		sleep(2);
		// AddUserButton.click();
		// waitForidPresent("userMrg_userDet_modal");
		// sleep(2);
		// WebElement scrollDown1 = scrollDownCancle;
		// js.executeScript("arguments[0].scrollIntoView(true);",scrollDown1);
		// sleep(3);
		// cancleButtonUser.click();
		// sleep(4);
	}

	@FindBy(xpath = "//td[text()='bently20']")
	private WebElement selectUserToEdit;

	@FindBy(xpath = "//*[@id='userManagementGrid']/tbody/tr[1]")
	private WebElement SelectFirstRow;

	public void ClickToSelectFirstRow() throws InterruptedException {
		sleep(4);
		SelectFirstRow.click();
		;
	}

	@FindBy(xpath = ".//*[@id='editUser']/i")
	private WebElement EditUSerButton;

	@FindBy(xpath = "//span[text()='Jobs/Profiles Folder Set']")
	private WebElement ScrollJobProfiles;

	public void EditOptionEnabled() throws InterruptedException {
		sleep(4);
		selectUserToEdit.click();
		sleep(2);
		EditUSerButton.click();
		// waitForidPresent("userMrg_userDet_moda");
		sleep(4);
		WebElement scrollDown2 = ScrollJobProfiles;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(5);
		cancleButtonUser.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='deleteUser']/i")
	private WebElement DeleteUserButton;

	public void DeleteEnable() throws InterruptedException {
		DeleteUserButton.click();
		sleep(3);
		NoDialogueBoxJobDelete.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='changePasswordUserManagement']/i")
	private WebElement ResetPasswordButton;

	@FindBy(xpath = ".//*[@id='usermg_resetPassword_popup']/div/div/div[1]/button")
	private WebElement ChangePasswordCancle;

	public void ChangePasswordUserDisabled() throws InterruptedException {
		ResetPasswordButton.click();
		sleep(2);
		ChangePasswordCancle.click();
		sleep(3);
	}

	public void ChangingPasswordForSubUser(String newPassword) throws InterruptedException {
		ResetPasswordButton.click();
		waitForXpathPresent("//h4[text()='Reset Password']");
		sleep(3);
		newpassword_forreset.sendKeys(newPassword);
		sleep(2);
		retypePasswordreset.sendKeys(newPassword);
		sleep(2);
		OKButton_resetpasswordusermanagement.click();
		waitForXpathPresent("//span[text()='New password saved successfully.']");
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='disableUser']/i")
	private WebElement EnableUser;

	@FindBy(xpath = ".//*[@id='ConfirmationDialog']/div/div/div[2]/button[text()='Yes']")
	private WebElement YesButton;

	public void EnabaleDisableUser() throws InterruptedException {
		EnableUser.click();
		sleep(2);
		YesButton.click();
		sleep(7);
	}

	public void UserEnable() throws InterruptedException {
		sleep(2);
		selectUserToEdit.click();
		sleep(2);
		EnableUser.click();
		sleep(7);
		YesButton.click();
	}

	@FindBy(xpath = "//p[text()='Usermanagement permission']")
	private WebElement UsermanagementRoleToEdit;

	public void DisablingUserManagemnetPermission() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(4);
		UsermanagementRoleToEdit.click();
		sleep(2);
		EditOption.click();
		sleep(4);
		NameTextField.clear();
		NameTextField.sendKeys("Usermanagement Permission-Edited");
		clickonGroupPermissionUM.click();
		sleep(2);
		// DeviceActionDRopDowm.click();

		sleep(4);
		UsermanagemnetCheckBox.click();
		sleep(4);

		saveButton.click();
		sleep(4);
		boolean displayRoleCreated = RoleEditedDeviceAction.isDisplayed();
		String PassStatement3 = "PASS >> Role Edited has been displayed ";
		String FailStatement3 = "FAIL >> Role Editted has not been displayed";
		ALib.AssertTrueMethod(displayRoleCreated, PassStatement3, FailStatement3);
		sleep(4);
		User.click();
	}

	public void ErrorMessageUserManagement() throws InterruptedException {
		usermanagementlink.click();
		sleep(4);
		selectUserToEdit.click();
		sleep(2);
		EditUSerButton.click();
		boolean modify = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement2 = "PASS >> History Button disabled";
		String FailStatement2 = "FAIL >>  History Button is Enabled";
		ALib.AssertTrueMethod(modify, PassStatement2, FailStatement2);
		sleep(5);
		DeleteUserButton.click();
		sleep(3);
		boolean modify2 = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement3 = "PASS >> History Button disabled";
		String FailStatement3 = "FAIL >>  History Button is Enabled";
		ALib.AssertTrueMethod(modify2, PassStatement3, FailStatement3);
		sleep(4);
		ResetPasswordButton.click();
		boolean modify3 = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement4 = "PASS >> History Button disabled";
		String FailStatement4 = "FAIL >>  History Button is Enabled";
		ALib.AssertTrueMethod(modify3, PassStatement4, FailStatement4);
		sleep(4);
		EnableUser.click();
		boolean modify4 = AccessDeniedErrorMessage.isDisplayed();
		String PassStatement5 = "PASS >> History Button disabled";
		String FailStatement5 = "FAIL >>  History Button is Enabled";
		ALib.AssertTrueMethod(modify4, PassStatement5, FailStatement5);
		sleep(4);
	}

	// ################################### USER SECTION
	// ##############################################################################

	@FindBy(xpath = ".//*[@id='TabUserInfomation']/a")
	private WebElement User;
	@FindBy(xpath = ".//*[@id='userid']")
	private WebElement UserName;
	@FindBy(xpath = ".//*[@id='password']")
	private WebElement UserPasswoard;
	@FindBy(xpath = ".//*[@id='confirmpassword']")
	private WebElement ConfirmPasswoard;
	@FindBy(xpath = ".//*[@id='firstname']")
	private WebElement FirstName;
	@FindBy(xpath = ".//*[@id='email']")
	private WebElement Email;
	@FindBy(xpath = "//Select[@id='ListFeaturePermissions']")
	private WebElement ListFeaturePermissions;
	// @FindBy(xpath=".//*[@id='okbtn']")
	// private WebElement OkButton;
	@FindBy(xpath = ".//*[@id='addUser']/i")
	private WebElement AddUserButton;
	// Select dropOption =new
	// Select(Initialization.driver.findElement(By.xpath(".//*[@id='ListFeaturePermissions']")));

	public void ApplyRoleToUser() throws InterruptedException {
		User.click();
		sleep(5);
		AddUserButton.click();
		// waitForidPresent("userid");

		sleep(7);
		UserName.sendKeys("jackjill");
		UserPasswoard.sendKeys("42gears");
		ConfirmPasswoard.sendKeys("42gears");
		FirstName.sendKeys("dontDelete");
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(4);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText("Automation Role");
		sleep(2);
		OkButton.click();
		sleep(5);
	}

	public void ClickOnUsertabInUM() throws InterruptedException {
		User.click();
		waitForidPresent("addUser");
		sleep(3);
	}

	@FindBy(xpath = "//span[text()='Please choose a different user name, as this one already exists.']")
	private WebElement userIDalreadyExists;

	public void ClickOnAddUserButton() throws InterruptedException {
		AddUserButton.click();
		waitForidPresent("userid");
		sleep(3);
	}

	public void SendingUserDetails(String Username, String Password, String Firstname, String MailID)
			throws InterruptedException {
		UserName.sendKeys(Username);
		UserPasswoard.sendKeys(Password);
		ConfirmPasswoard.sendKeys(Password);
		FirstName.sendKeys(Firstname);
		Email.sendKeys(MailID);
		sleep(3);
	}

	public void AssigningRoleToUser(String RoleName) throws InterruptedException {
		Select sel = new Select(ListFeaturePermissions);
		sel.selectByVisibleText(RoleName);
		sleep(3);
	}

	public void ClickOnCreateUserButton() throws InterruptedException {
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
		OkButton.click();
		waitForXpathPresent("//span[text()='New user created successfully.']");
		Reporter.log("User Created Successfully",true);
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='usrerr']/ul/li")
	private WebElement EnterValiduserName;

	@FindBy(xpath = ".//*[@id='passworderr']/ul/li")
	private WebElement PasswordLengthIsTooShort;
	@FindBy(xpath = ".//*[@id='err']")
	private WebElement EnterValidEmailAddress;
	@FindBy(xpath = ".//*[@id='userMrg_userDet_modal']/div[1]/div[2]/div[4]/span/ul/li")
	private WebElement EnterValidFirstName;
	@FindBy(xpath = ".//*[@id='error']/ul/li")
	private WebElement PasswordDonotMatch;

	public void blankFieldInUser() throws InterruptedException {
		AddUserButton.click();
		sleep(3);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
		OkButton.click();
		sleep(2);

		boolean displayValidMail = EnterValidEmailAddress.isDisplayed();
		String PassStatement1 = "PASS >> Warning message enter valid Email ID has been displayed ";
		String FailStatement1 = "FAIL >> Warning message  enter valid Email ID has not been displayed";
		ALib.AssertTrueMethod(displayValidMail, PassStatement1, FailStatement1);

		boolean displayValidFirstNmae = EnterValidFirstName.isDisplayed();
		String PassStatement2 = "PASS >> Warning message enter valid First Name has been displayed ";
		String FailStatement2 = "FAIL >> Warning message  enter valid First Name has not been displayed";
		ALib.AssertTrueMethod(displayValidFirstNmae, PassStatement2, FailStatement2);

		WebElement scrollup = UserName;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollup);
		sleep(2);

		boolean displayed = EnterValiduserName.isDisplayed();
		sleep(2);
		String PassStatement = "PASS >> Warning message enter valid user name has been displayed ";
		String FailStatement = "FAIL >> Warning message  enter valid user name has not been displayed";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(3);

		boolean displayedPassword = PasswordLengthIsTooShort.isDisplayed();
		String PassStatement3 = "PASS >> Warning message Password length is too short has been displayed ";
		String FailStatement3 = "FAIL >> Warning message  Password length is too short name has not been displayed";
		ALib.AssertTrueMethod(displayedPassword, PassStatement3, FailStatement3);

		boolean displayedPasswordDonotMatch = PasswordDonotMatch.isDisplayed();
		String PassStatement4 = "PASS >> Warning message enter valid user name has been displayed ";
		String FailStatement4 = "FAIL >> Warning message  enter valid user name has not been displayed";
		ALib.AssertTrueMethod(displayedPasswordDonotMatch, PassStatement4, FailStatement4);

	}

	@FindBy(xpath = ".//*[@id='errMsg']")
	private WebElement InvalidNumber;

	@FindBy(xpath = "//span[text()='New user created successfully.']")
	private WebElement UserSuccessfullyCreatedPopUp;

	public void Refresh() throws InterruptedException {
		clickOnRolesOptions.click();
		sleep(3);
		User.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='phone']")
	private WebElement PhoneNumberTextField;

	public void CreateNewUser() throws InterruptedException {
		// AddUserButton.click();
		UserName.sendKeys("pickachoo");
		UserPasswoard.sendKeys("000");

		boolean displayedPassword = PasswordLengthIsTooShort.isDisplayed();
		String PassStatement3 = "PASS >> Warning message Password length is too short has been displayed ";
		String FailStatement3 = "FAIL >> Warning message  Password length is too short name has not been displayed";
		ALib.AssertTrueMethod(displayedPassword, PassStatement3, FailStatement3);
		UserPasswoard.clear();

		UserPasswoard.sendKeys("0000");
		sleep(5);

		ConfirmPasswoard.sendKeys("42gears");
		sleep(3);
		boolean displayedPasswordDonotMatch = PasswordDonotMatch.isDisplayed();
		String PassStatement4 = "PASS >> Warning message enter valid user name has been displayed ";
		String FailStatement4 = "FAIL >> Warning message  enter valid user name has not been displayed";
		ALib.AssertTrueMethod(displayedPasswordDonotMatch, PassStatement4, FailStatement4);
		ConfirmPasswoard.clear();
		ConfirmPasswoard.sendKeys("0000");

		FirstName.sendKeys("picka");
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		Email.sendKeys("pickachoo@dispostable");
		sleep(2);
		PhoneNumberTextField.sendKeys("9872345671");
		boolean displayValidMail = EnterValidEmailAddress.isDisplayed();
		String PassStatement1 = "PASS >> Warning message enter valid Email ID has been displayed ";
		String FailStatement1 = "FAIL >> Warning message  enter valid Email ID has not been displayed";
		ALib.AssertTrueMethod(displayValidMail, PassStatement1, FailStatement1);

		Email.clear();
		Email.sendKeys("pickachoo@dispostable.com");
		PhoneNumberTextField.clear();
		sleep(3);
		// WebElement scrollDown2 = OkButton;
		// js.executeScript("arguments[0].scrollIntoView(true);",scrollDown2);

		PhoneNumberTextField.sendKeys("9876");
		sleep(4);
		Email.click();
		boolean Number = InvalidNumber.isDisplayed();
		String PassStatement5 = "PASS >> Warning message enter valid Email ID has been displayed ";
		String FailStatement5 = "FAIL >> Warning message  enter valid Email ID has not been displayed";
		ALib.AssertTrueMethod(Number, PassStatement5, FailStatement5);
		PhoneNumberTextField.clear();
		sleep(2);
		PhoneNumberTextField.sendKeys("9876543211");

		// ListFeaturePermissions.click();
		// Select dropOption =new
		// Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		// dropOption.selectByVisibleText("Automation Role");

		OkButton.click();
		sleep(4);
		String Actualvalue = UserSuccessfullyCreatedPopUp.getText();
		String Expectedvalue = "User created successfully.";
		String PassStatement = "PASS >>'User created successfully.- displayed'";
		String FailStatement = "FAIL >>  User created successfully.not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

	}

	@FindBy(xpath = "//td[text()='Demouser']")
	private WebElement SelectUser;

	public void SelectUser() throws InterruptedException {
		SelectUser.click();
		sleep(2);
	}

	public void SelectUserAndCheckEnabledButtons() throws InterruptedException {
		SelectUser.click();
		sleep(2);

		boolean isPresent = EditUSerButton.isEnabled();
		String pass = "PASS>> Edit option is Enabled";
		String fail = "FAIL>> Edit option is Disabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);
		boolean isPresent1 = DeleteUserButton.isEnabled();
		String pass1 = "PASS>> Delete option is Enabled";
		String fail1 = "FAIL>>Delete option is Disabled";
		ALib.AssertTrueMethod(isPresent1, pass1, fail1);
		sleep(3);
		boolean isPresent2 = ResetPasswordButton.isEnabled();
		String pass2 = "PASS>> ResetPassword option is Enabled";
		String fail2 = "FAIL>> ResetPassword option is Disabled";
		ALib.AssertTrueMethod(isPresent2, pass2, fail2);
		sleep(3);
		boolean isPresent3 = EnableUser.isEnabled();
		String pass3 = "PASS>> ResetPassword option is Enabled";
		String fail3 = "FAIL>> ResetPassword option is Disabled";
		ALib.AssertTrueMethod(isPresent3, pass3, fail3);
		sleep(3);
		SelectUser.click();
		sleep(3);

	}

	public void ClickOnChangePassword() throws InterruptedException {
		SelectUser.click();
		sleep(2);
		ResetPasswordButton.click();
		sleep(2);
		ChangePasswordCancle.click();
		sleep(3);
		SelectUser.click();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='uneditablemessage']")
	private WebElement CannotEditUserNameMessage;

	public void UserNameCannotBeEdited() throws InterruptedException {
		SelectUser.click();
		sleep(2);
		EditUSerButton.click();
		waitForidPresent("userMrg_userDet_modal");
		sleep(2);
		boolean isPresent3 = CannotEditUserNameMessage.isDisplayed();
		String pass3 = "PASS>> cannot edit username  option is displayed";
		String fail3 = "FAIL>> cannot edit username is not displayed";
		ALib.AssertTrueMethod(isPresent3, pass3, fail3);
		sleep(3);
		cancleButtonUser.click();
	}

	public void DisableExistingUser() throws InterruptedException {
		SelectUser.click();
		sleep(2);
		EnableUser.click();
		sleep(3);
		YesButton.click();
		sleep(7);
	}

	@FindBy(xpath = ".//*[@id='warningArea']")
	private WebElement ContactAdministratorMessage;

	public void ContactAdministratorMessasge() throws InterruptedException {
		boolean isPresent3 = ContactAdministratorMessage.isDisplayed();
		String pass3 = "PASS>>Contact administrator is displayed";
		String fail3 = "FAIL>>contact administrator is not displayed";
		ALib.AssertTrueMethod(isPresent3, pass3, fail3);
		sleep(3);
	}

	public void ErrorMessagesUserIdExists() throws InterruptedException {
		User.click();
		AddUserButton.click();
		sleep(3);
		UserName.sendKeys("Demouser");
		UserPasswoard.sendKeys("42Gears@123");
		ConfirmPasswoard.sendKeys("42Gears@123");
		FirstName.sendKeys("dontDelete");
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(3);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(4);
		OkButton.click();
		sleep(2);
		String Actualvalue = userIDalreadyExists.getText();
		String Expectedvalue = "Please choose a different user name, as this one already exists.";
		String PassStatement = "PASS >>'User creation failed. User ID already exists.- displayed'";
		String FailStatement = "FAIL >> error message is not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		cancleButtonUser.click();
	}

	@FindBy(xpath = ".//*[@id='hide_parent_if_child_not_allowed']")
	private WebElement HideParentGroupWhenNoAccessToChildGroups;

	public void CreateUserParentChildCheckbox(String username, String password, String Firstname,
			String featuredPermission) throws InterruptedException {
		User.click();
		sleep(5);
		AddUserButton.click();
		sleep(2);
		waitForidPresent("permissionModalTitle");
		sleep(4);
		UserName.sendKeys(username);
		sleep(3);
		UserPasswoard.sendKeys(password);
		ConfirmPasswoard.sendKeys(password);
		FirstName.sendKeys(Firstname);
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(4);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(featuredPermission);
		sleep(2);
		HideParentGroupWhenNoAccessToChildGroups.click();
		sleep(2);
		OkButton.click();
		sleep(5);
	}

	public void checkChildparentGroupavilabe() throws InterruptedException {
		// sleep(3);
		// boolean result = jobToolBarButton.isDisplayed();
		// if (result==false)
		// System.out.println("Pass>> parent is hidden");
		// else
		// System.out.println("FAIL>> parent is not hidden");

		boolean isdisplayed = false;
		try {
			jobToolBarButton.isDisplayed();

		} catch (Exception e) {
			isdisplayed = true;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Job Folder Set modified successfully.\" is  not displayed ");
		Reporter.log("PASS >>\"Job Folder Set modified successfully.\" is displayed", true);
		sleep(3);
	}

	@FindBy(xpath = "//td[text()='Crooking']")
	private WebElement CrookingUser;

	@FindBy(xpath = "//span[text()='User deleted successfully.']")
	private WebElement UserDeletedSuccessfullyMessage;

	public void DeleteExistingUser() throws InterruptedException {
		CrookingUser.click();
		sleep(3);
		DeleteUserButton.click();
		waitForidPresent("ConfirmationDialog");
		sleep(2);
		YesButton.click();
		sleep(3);
		boolean isPresent = UserDeletedSuccessfullyMessage.isDisplayed();
		String pass = "PASS>> user deleted successfully is dispslyed ";
		String fail = "FAIL>> user deleted successfully is not dispslyed";
		ALib.AssertTrueMethod(isPresent, pass, fail);

	}

	// #####################EditUSerButton#################################################################################################
	// ############################# DEVICE GROUP SET
	// #########################################

	@FindBy(xpath = ".//*[@id='userManagement_pg']//a[text()='Device Group Set']")
	private WebElement DeviceGroupSetSection;

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[1]/div/div[1]/span[text()='Add']")
	private WebElement AddButtonDeviceGroupSet;

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[text()='00001Donotdelete']")
	private WebElement GroupPresentInHome;

	@FindBy(xpath = "//p[text()='Super User']")
	private WebElement SuperUserDeviceGroupSet;

	public void ClickOnDeviceGroupSet() throws InterruptedException {
		DeviceGroupSetSection.click();
		waitForidPresent("templateName");
		sleep(4);
	}

	public void DeletingExistingDeviceGroupSet(String DeviceGroupSet)
	{
		SearchTextFieldDeviceGroupSet.sendKeys(DeviceGroupSet);
		try {
			Initialization.driver
			.findElement(
					By.xpath("//*[@id='GroupsPermissionGrid']//p[text()='" + DeviceGroupSet + "']"))
			.isDisplayed();
			Reporter.log("Role Have been Created Already", true);
			Initialization.driver
			.findElement(
					By.xpath("//*[@id='GroupsPermissionGrid']//p[text()='" + DeviceGroupSet + "']"))
			.click();
			DeletingDeviceGroupSet();
		}
		catch (Exception e) 
		{
			Reporter.log("No Device Group Set Is Found With Searched Name", true);
		}
	}

	@FindBy(xpath = "//*[@id='selectAllPermissions']")
	private WebElement selectAllRolesPermissions;

	public void DeselectingAllGroupsInDeviceGroup() throws InterruptedException
	{
		selectAllRolesPermissions.click();
		sleep(2);
	}

	public void SelectingGroupNameInDeviceGroup(String GroupName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='umTree']//li[text()='"+GroupName+"']//span[@class='icon check-icon sc-unchked fa fa-square-o']")).click();
		sleep(3);
	}

	public void ClickOnDevieGroupSaveButton(String Action) throws InterruptedException
	{
		saveButton.click();
		waitForXpathPresent("//span[text()='Device Group Set "+Action+" successfully.']");
		sleep(3);	
	}





	public void VerifySuperUserDevuceGroupSet() {
		boolean isPresent = SuperUserDeviceGroupSet.isDisplayed();
		String pass = "PASS>> Super user template is displayed in Device Group set";
		String fail = "FAIL>> Super user template is NOT displayed in Device Group set";
		ALib.AssertTrueMethod(isPresent, pass, fail);
	}

	public void DeviceGroupSetCreatedSuccessfulMessage() throws InterruptedException {
		boolean message = DeviceGroupSetSucessfulMessage.isDisplayed();
		String PassStatement1 = "PASS >> Device Group set created successfully ";
		String FailStatement1 = "FAIL >> Device Group set not created successfully d";
		ALib.AssertTrueMethod(message, PassStatement1, FailStatement1);
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Device Group Set created successfully.']")
	private WebElement DeviceGroupSetSucessfulMessage;

	public void EnterDeviceGroupsetName() throws InterruptedException {
		NameTextField.sendKeys("Device Group Set All Groups");
		sleep(1);
		DeviceGroupSetDescription.sendKeys("Automation Script");
		saveButton.click();
		sleep(5);
	}

	public void InputRolesDescriptionDeviceGroupSet(String RoleName, String DescriptionName)
			throws InterruptedException {
		NameTextField.sendKeys(RoleName);
		sleep(2);
		descriptionTestField.sendKeys(DescriptionName);
		sleep(2);
	}

	public void DeviceGroupSet_SaveAllEnabledPermission() throws InterruptedException {

		saveButton.click();
		sleep(4);
		boolean isdisplayed = true;

		try {
			DeviceGroupSetCreatedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;
		}
		Assert.assertTrue(isdisplayed, "Fail:Warning message Device group set is created sucessfully not displayed ");
		Reporter.log("PASS >>Warning message Device group set is created sucessfully is displayed", true);
		sleep(5);
	}

	@FindBy(xpath = "//span[contains(text(),'The user was modified successfully.')]")
	private WebElement UpdatedUserModifiedSuccessMessage;

	public void SelectUser(String User) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + User + "']"))
		.click();
		sleep(2);
	}

	public void VerifyUserModificationSuccessMessage() throws InterruptedException {
		boolean isdisplayed = true;
		try {
			UpdatedUserModifiedSuccessMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed,
				"Fail:Warning message Device Group Set USER modified successfully not displayed ");
		Reporter.log("PASS >>Warning message Device Group Set USER modified successfully is displayed", true);
		sleep(5);
	}

	public void EditUserPage(String featuredPermission, String DeviceGroupSet) throws InterruptedException {

		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(featuredPermission);
		sleep(2);
		ListFeaturePermissions.click();
		sleep(4);
		DeviceGroupSetCreateUser.click();
		Select dropOption2 = new Select(Initialization.driver.findElement(By.id("ListGroupPermissions")));
		dropOption2.selectByVisibleText(DeviceGroupSet);
		sleep(2);
		OkButton.click();
		sleep(5);
	}

	@FindBy(xpath = "//*[@id='umTree']/ul/li[text()='@SourceDonotTouch']/span[4]")
	private WebElement ScrollToSourceGroup;

	public void DisableDeviceGroupSet_SourceDonotTouch() throws InterruptedException {

		WebElement scrollDown = ScrollToSourceGroup;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);

		ScrollToSourceGroup.click();
		sleep(2);
		saveButton.click();
		sleep(4);
		boolean isdisplayed = true;

		try {
			DeviceGroupSetCreatedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Warning message Device group set is created sucessfully not displayed ");
		Reporter.log("PASS >>Warning message Device group set is created sucessfully is displayed", true);
		sleep(5);
	}

	public void ClickAddDeviceGroupSet() throws InterruptedException {

		AddButtonDeviceGroupSet.click();
		waitForidPresent("SavePermissions");
		sleep(3);
	}

	public void HomeGroupPeresntinDeviceGroupSet() throws InterruptedException {
		boolean isdisplayed = true;

		try {
			GroupPresentInHome.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Grouop not present ");
		Reporter.log("PASS >>Group Present", true);
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[1]/span[3]")
	private WebElement HomeDeviceGroupSetCheckBox;

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[2]/span[4]")
	private WebElement SelectGroupFromOptions;

	@FindBy(xpath = "//span[text()='Device Group Set created successfully.']")
	private WebElement DeviceGroupSetCreatedSuccessfully;

	public void clickOnHomeCheckBox() throws InterruptedException {
		sleep(4);
		NameTextField.sendKeys("Automatic Enable");
		sleep(5);
		HomeDeviceGroupSetCheckBox.click();
		sleep(2);
		SelectGroupFromOptions.click();
		sleep(3);
		saveButton.click();
		sleep(4);
		boolean isdisplayed = true;

		try {
			DeviceGroupSetCreatedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Warning message Device group set is created sucessfully not displayed ");
		Reporter.log("PASS >>Warning message Device group set is created sucessfully is displayed", true);
		sleep(5);

	}

	@FindBy(xpath = "//span[text()='No Group selected.']")
	private WebElement ErrorMessageNoGroupSelected;

	@FindBy(xpath = ".//*[@id='permissionSelection_popup']/div/div/div[1]/button")
	private WebElement CloseDeviceGroupSet;

	public void ErrorMessageWithoutSelectingGroup() throws InterruptedException

	{
		NameTextField.sendKeys("Device group");
		sleep(1);
		HomeDeviceGroupSetCheckBox.click();
		sleep(2);
		saveButton.click();
		sleep(4);
		boolean isdisplayed = true;

		try {
			ErrorMessageNoGroupSelected.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Warning message not displayed ");
		Reporter.log("PASS >>Warning message is displayed", true);
		sleep(5);
		CloseDeviceGroupSet.click();
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Enter Template Name.']")
	private WebElement EnterTEmplateNameErrorMessage;

	public void EnterTemplateNameErrorMessage() throws InterruptedException {
		saveButton.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			EnterTEmplateNameErrorMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Warning message NO TEMPLATE NAME not displayed ");
		Reporter.log("PASS >>Warning message NO TEMPLATE NAME is displayed", true);
		sleep(5);
		CloseDeviceGroupSet.click();
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='TemplateDescription']")
	private WebElement DeviceGroupSetDescription;

	@FindBy(xpath = "//p[text()='Automation Script']")
	private WebElement descriptionText;

	public void DeviceGroupsetWithDescription() throws InterruptedException {
		NameTextField.sendKeys("Device set Description");
		sleep(1);
		DeviceGroupSetDescription.sendKeys("Automation Script");
		saveButton.click();
		sleep(2);
		boolean displayCloneButton = descriptionText.isDisplayed();
		String PassStatement1 = "PASS >> Device Group set with description is displayed  ";
		String FailStatement1 = "FAIL >> Device Group set with description is not displayed";
		ALib.AssertTrueMethod(displayCloneButton, PassStatement1, FailStatement1);
		sleep(4);

	}

	@FindBy(xpath = "//p[text()='Device set Description']")
	private WebElement SelectExistingGroup;

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[1]/div/div[2]/span[text()='Edit']")
	private WebElement EditButtonDeviceGroupSet;

	@FindBy(xpath = "//span[text()='Device Group Set modified successfully.']")
	private WebElement DeviceGroupSetmodifiedSuccessfully;

	@FindBy(xpath = ".//*[@id='allow_new_group_job_check']")
	private WebElement AutomaticallyEnableCheckbox;

	public void SelectDeviceGroupSetToBeEdited() throws InterruptedException {
		SelectExistingGroup.click();
		sleep(1);
		EditButtonDeviceGroupSet.click();
		waitForidPresent("SavePermissions");
		sleep(2);
		NameTextField.clear();
		NameTextField.sendKeys("Device Group Edited");
		sleep(4);
		HomeDeviceGroupSetCheckBox.click();
		sleep(3);
		SelectGroupFromOptions.click();
		// AutomaticallyEnableCheckbox.click();
		sleep(4);
		// YesButton.click();
		sleep(4);
		saveButton.click();
		sleep(4);
		boolean isdisplayed = true;
		try {
			DeviceGroupSetmodifiedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Warning message Device Group Set modified successfully not displayed ");
		Reporter.log("PASS >>Warning message Device Group Set modified successfully is displayed", true);
		sleep(5);

		sleep(3);
	}

	@FindBy(xpath = "//p[text()='Device Group Edited']")
	private WebElement SelectDeviceGroupEdited;

	@FindBy(xpath = "//p[text()='Automatic Enable']")
	private WebElement SelectDeviceAutomaticEnable;

	public void EditOptionEnabledDeviceGroupSet() throws InterruptedException {
		SelectDeviceGroupEdited.click();

		sleep(2);

		boolean isPresent = EditButtonDeviceGroupSet.isEnabled();
		String pass = "PASS>> Edit option is Enabled";
		String fail = "FAIL>> Edit option is Disabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);
	}

	public void EditOptionDisabledDeviceGroupSet() throws InterruptedException {
		SelectDeviceAutomaticEnable.click();
		sleep(2);

		boolean isPresent = EditButtonDeviceGroupSet.isEnabled();
		String pass = "PASS>> Edit option is Disabled";
		String fail = "FAIL>> Edit option is Enabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(4);
		SelectDeviceGroupEdited.click();
		SelectDeviceAutomaticEnable.click();
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='Super User']")
	private WebElement SelectSuperUserDeviceGroupSet;

	public void SelectSuperUserDeviceGroupSet() throws InterruptedException {
		SelectSuperUserDeviceGroupSet.click();
		sleep(4);
	}

	public void EditGreyedOut() throws InterruptedException {
		boolean isPresent = EditButtonDeviceGroupSet.isEnabled();
		String pass = "PASS>> Edit option is Disabled";
		String fail = "FAIL>> Edit option is Enabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(7);
		SelectSuperUserDeviceGroupSet.click();
		sleep(5);

	}

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[1]/div/div[3]/span[text()='Clone']")
	private WebElement CloneButtonDeviceGroupSet;

	public void CloneOptionEnabledDeviceGroupSet() throws InterruptedException {
		SelectDeviceGroupEdited.click();

		sleep(2);

		boolean isPresent = CloneButtonDeviceGroupSet.isEnabled();
		String pass = "PASS>> clone option is Enabled";
		String fail = "FAIL>> clone option is Disabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);
	}

	public void CloneOptionDisabledDeviceGroupSet() throws InterruptedException {
		SelectDeviceAutomaticEnable.click();
		sleep(2);

		boolean isPresent = CloneButtonDeviceGroupSet.isEnabled();
		String pass = "PASS>> clone option is Disabled";
		String fail = "FAIL>> clone option is Enabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(4);
		SelectDeviceGroupEdited.click();
		SelectDeviceAutomaticEnable.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[1]/div/div[4]/span[text()='Delete']")
	private WebElement DeleteButtonDeviceGroupSet;

	public void DeletingDeviceGroupSet() throws InterruptedException
	{
		DeleteButtonDeviceGroupSet.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		YesDelete.click();
		waitForXpathPresent("//span[contains(text(),'Role(s) deleted successfully.')]");
		sleep(2);
		Reporter.log("Device Group Set Deleted Successfully",true);
	}

	public void DeleteButtonDeviceGroupSetGreyedOut() throws InterruptedException {
		boolean isPresent = DeleteButtonDeviceGroupSet.isEnabled();
		String pass = "PASS>> Delete option is Disabled";
		String fail = "FAIL>> Delete option is Enabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);

	}

	public void CloneDeviceGroupSet() throws InterruptedException {
		SelectDeviceAutomaticEnable.click();
		sleep(4);
		CloneButtonDeviceGroupSet.click();
		sleep(4);
		// boolean isPresent = SelectGroupFromOptions.isEnabled();
		// String pass = "PASS>> Group is Selected";
		// String fail = "FAIL>> Group isnot selected";
		// ALib.AssertTrueMethod(isPresent, pass, fail);
		// sleep(4);
		NameTextField.sendKeys("cloned Set");
		sleep(2);
		saveButton.click();
		sleep(4);
	}

	@FindBy(xpath = "//span[text()='Device Group Set cloned successfully.']")
	private WebElement MessageDeviceGroupClonedSuccessfully;

	public void CaptureClonedSuccessfullyMessage() throws InterruptedException {
		boolean isdisplayed = true;
		try {
			MessageDeviceGroupClonedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Device Group Set cloned successfully not displayed ");
		Reporter.log("PASS >>Device Group Set cloned successfully is displayed", true);
		sleep(5);

		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement SearchTextFieldDeviceGroupSet;

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[2]/div[1]/div[1]/div[1]/button")
	private WebElement RefreshButtonDeviceGroupSet;


	public void SearchDeviceGroupSet() throws InterruptedException {
		SearchTextFieldDeviceGroupSet.sendKeys("Automatic Enable");
		sleep(2);
		boolean isdisplayed = true;
		try {
			SelectDeviceAutomaticEnable.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:Created device group is not shortlisted ");
		Reporter.log("PASS >>Device Group Set is shortlisted", true);
		sleep(5);
		SearchTextFieldDeviceGroupSet.clear();
		sleep(10);
		RefreshButtonDeviceGroupSet.click();
		sleep(2);

	}

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[2]/div[3]/div/div")
	private WebElement NoDeviceGroupSetAvilable;

	public void NoDeviceGroupSetValidation() throws InterruptedException {
		SearchTextFieldDeviceGroupSet.sendKeys("#!kimtan");
		sleep(2);
		String actual = NoDeviceGroupSetAvilable.getText();

		System.out.println("Message:" + actual + ":Message");
		String expected = "No Device Group Set Available.";
		String PassStatement = "PASS >>No Device GroupSet avilabel is displayed";
		String FailStatement = "FAIL >> No Device GroupSet avilabel is not displayed";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);

		SearchTextFieldDeviceGroupSet.clear();
		sleep(7);

	}

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[2]/div[1]/div[2]/div[1]/table/thead/tr/th[1]/div[1]/span/input")
	private WebElement SelectAllCheckBoxDeviceGroupSet;

	public void ClickOnSelectAllCheckBoxdeviceGroupSet() throws InterruptedException {
		SelectAllCheckBoxDeviceGroupSet.click();
		sleep(2);
		boolean isPresent = SelectAllCheckBoxDeviceGroupSet.isEnabled();
		String pass = "PASS>> Select all checkbox clicked";
		String fail = "FAIL>> Select all checkbox is disabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);
		SelectAllCheckBoxDeviceGroupSet.click();
		sleep(4);

	}

	@FindBy(xpath = ".//*[@id='ListGroupPermissions']")
	private WebElement DeviceGroupSetCreateUser;

	public void AssigningGroupSetToUser(String GroupName) throws InterruptedException
	{
		Select sel=new Select(DeviceGroupSetCreateUser);
		sel.selectByVisibleText(GroupName);
		sleep(2);
	}

	public void CreateUserForPermissionsAndDeviceGroupSet(String username, String password, String Firstname,
			String featuredPermission, String DeviceGroupSet) throws InterruptedException {
		User.click();
		sleep(5);
		AddUserButton.click();
		sleep(7);
		waitForidPresent("permissionModalTitle");
		sleep(5);
		UserName.sendKeys(username);
		sleep(5);
		UserPasswoard.sendKeys(password);
		ConfirmPasswoard.sendKeys(password);
		FirstName.sendKeys(Firstname);
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(5);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(featuredPermission);
		sleep(2);
		ListFeaturePermissions.click();
		sleep(4);
		DeviceGroupSetCreateUser.click();
		Select dropOption2 = new Select(Initialization.driver.findElement(By.id("ListGroupPermissions")));
		dropOption2.selectByVisibleText(DeviceGroupSet);
		sleep(2);
		OkButton.click();
		sleep(5);
	}

	@FindBy(xpath = "//table[@id='dataGrid']/tbody/tr[1]")
	private WebElement HomeGroupFirstRow;

	public void AddGroupInConsole(String groupname) throws InterruptedException {
		HomesectionButton.click();
		sleep(3);
		HomeGroupFirstRow.click();
		sleep(2);
		AddNewGroupPermission.click();
		sleep(4);
		GroupNameTextField.sendKeys(groupname);
		ClickOkButtonGroup.click();
		waitForidPresent("groupstree");
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[text()='00000papion']")
	private WebElement NewGroup000000papion;

	public void NewAddedGroupPresentInDeviceGroupSet() throws InterruptedException {
		boolean isdisplayed = true;
		try {
			NewGroup000000papion.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:NEw created group is not present");
		Reporter.log("PASS >>New created group is present", true);
		sleep(5);
		CloseDeviceGroupSet.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[text()='#000discard']")
	private WebElement NewGroup000discard;

	public void NewAddedGroupIsNotPresentInDeviceGroupSet() throws InterruptedException {

		boolean displayed;

		try {
			sleep(5);
			NewGroup000discard.isDisplayed();
			sleep(5);
			displayed = false;
			sleep(5);
		} catch (Throwable T) {
			displayed = true;

		}
		String PassStatement = "PASS >> Device group is not present";
		String FailStatement = "FAIL >> Device group is present";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(6);

		CloseDeviceGroupSet.click();
		sleep(6);
	}

	@FindBy(xpath = ".//*[@id='devGrp_tabCont']/div[1]/div/div[4]/span[text()='Delete']")
	private WebElement DeleteDeviceGroupSet;

	public void SelectDeviceGroupAssignedToUser() throws InterruptedException {
		SelectDeviceGroupEdited.click();
		sleep(2);
		DeleteDeviceGroupSet.click();
		sleep(1);
		YesButton.click();
		sleep(3);

		boolean isdisplayed = true;
		try {
			AssignedUserTempleteCannotBeDeleted.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"User assigned templete cannot be deleted\" is  not displayed ");
		Reporter.log("PASS >>\"User assigned templete cannot be deleted\" is  not displayed is displayed", true);
		sleep(5);
	}

	public void CreateDeviceGroupSet1() throws InterruptedException {
		NameTextField.sendKeys("kingmaker");
		sleep(2);
		saveButton.click();
		sleep(2);
	}

	public void ClosePremissionPopup() throws InterruptedException {
		CloseDeviceGroupSet.click();
		waitForidPresent("userProfileButton");
		sleep(2);
	}

	@FindBy(xpath = "//p[text()='kingmaker']")
	private WebElement SlectDeviceGroupSetKingMaker;

	@FindBy(xpath = "//span[text()='Device Group Set deleted successfully.']")
	private WebElement DdeviceGroupSetDeletedSuccessfully;

	public void DeleteDeviceGroupSet() throws InterruptedException {
		SlectDeviceGroupSetKingMaker.click();
		DeleteDeviceGroupSet.click();
		sleep(1);
		YesButton.click();
		sleep(4);
		boolean isdisplayed = true;
		try {
			DdeviceGroupSetDeletedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Device Group set deleted successfully\" is  not displayed ");
		Reporter.log("PASS >>\"Device Group set deleted successfully\" is  not displayed is displayed", true);
		sleep(5);

	}
	// ############################################ JOB FOLDER SET
	// ################################################
	// ###############################################################################################################

	@FindBy(xpath = ".//*[@id='userManagement_pg']/section/div[1]/ul/li/a[text()='Jobs/Profiles Folder Set']")
	private WebElement JobFolderSetButton;

	public void ClickOnJobFolderSet() throws InterruptedException {
		JobFolderSetButton.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[1]/div/div[1]/span[text()='Add']")
	private WebElement AddButtonJobFolderSet;

	public void clickOnAddJoobFolderSet() throws InterruptedException {
		AddButtonJobFolderSet.click();
		waitForidPresent("permissionModalTitle");
		sleep(3);
	}

	@FindBy(xpath = "//span[text()='Jobs Folders']")
	private WebElement JobsFolderOption;

	public void JobFolderOptionIsPresent() throws InterruptedException {
		boolean isPresent = JobsFolderOption.isDisplayed();
		String pass = "PASS>> Jobs FOlder option is present";
		String fail = "FAIL>> Jobs Folder option is not present";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(7);

	}

	@FindBy(xpath = ".//*[@id='folderTree_tabs']/li[2]/a/span[text()='Profiles Folders']")
	private WebElement ProfilesFolderOption;

	public void ProfileFolderOptionIsPresent() throws InterruptedException {
		boolean isPresent = ProfilesFolderOption.isDisplayed();
		String pass = "PASS>> Jobs FOlder option is present";
		String fail = "FAIL>> Jobs Folder option is not present";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='templateName']")
	private WebElement NameTextFieldJobProfileSet;

	@FindBy(xpath = "//span[text()='Jobs/Profiles folder set created successfully.']")
	private WebElement JobsProfileFolderSetCreatedSuccessfullymessage;

	@FindBy(xpath = "//p[text()='0Automatically enable']")
	private WebElement AutomaticallyEnableJobProfileSet;

	public void JobProfileSavedSuccessfully() throws InterruptedException {
		NameTextFieldJobProfileSet.sendKeys("0Automatically enable");
		sleep(2);
		saveButton.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			JobsProfileFolderSetCreatedSuccessfullymessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Jobs/Profiles folder set created successfully.\" is  not displayed ");
		Reporter.log("PASS >>\"Jobs/Profiles folder set created successfully.\" is  not displayed is displayed", true);
		sleep(5);

		boolean isPresent = AutomaticallyEnableJobProfileSet.isDisplayed();
		String pass = "PASS>> created profile is displayed";
		String fail = "FAIL>> created profile is not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);
	}

	public void EnterTemplateNameKjobFolderSet() throws InterruptedException {

		saveButton.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			EnterTEmplateNameErrorMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Enter template name.\" is  not displayed ");
		Reporter.log("PASS >>\"Enter template name.\" is displayed", true);
		sleep(3);
	}

	@FindBy(xpath = "//p[text()='disabled']")
	private WebElement DescriptionJobFolderset;

	public void JobProfileFolderWithdescription() throws InterruptedException {
		AutomaticallyEnableCheckbox.click();
		sleep(3);

		YesButton.click();
		sleep(3);
		NameTextFieldJobProfileSet.sendKeys("0Automatically disabled");
		sleep(3);

		descriptionTestField.sendKeys("disabled");
		sleep(3);
		saveButton.click();
		sleep(5);

		boolean isPresent = DescriptionJobFolderset.isDisplayed();
		String pass = "PASS>> description is displayed";
		String fail = "FAIL>> description is not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);
	}

	@FindBy(xpath = "//p[text()='0Automatically enable']")
	private WebElement AutomaticallyEnableJobProfileGroupSet;

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[1]/div/div[3]/span[text()='Clone']")
	private WebElement CloneButtonJobProfileSet;

	@FindBy(xpath = "//span[text()='Job Folder Set cloned successfully.']")
	private WebElement JobFoldersetClonedSuccessfully;

	public void CloneJobProfileFolderSet() throws InterruptedException {
		AutomaticallyEnableJobProfileGroupSet.click();
		sleep(4);
		CloneButtonJobProfileSet.click();
		sleep(3);
		NameTextFieldJobProfileSet.sendKeys("deletelater");
		sleep(2);
		saveButton.click();
		sleep(4);
		boolean isdisplayed = true;
		try {
			JobFoldersetClonedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Cloned successfully\" is  not displayed ");
		Reporter.log("PASS >>\"Cloned successfullye.\" is displayed", true);
		sleep(3);
	}

	@FindBy(xpath = "//p[text()='deletelater']")
	private WebElement deletelaterJobProfileGroupSet;

	public void Select2JobProfileSet() throws InterruptedException {
		AutomaticallyEnableJobProfileGroupSet.click();
		sleep(2);
		deletelaterJobProfileGroupSet.click();
		sleep(2);
		boolean isPresent = CloneButtonJobProfileSet.isEnabled();
		String pass = "PASS>> clone option is Disabled";
		String fail = "FAIL>> clone option is Enabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);
		AutomaticallyEnableJobProfileGroupSet.click();
		deletelaterJobProfileGroupSet.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[2]/div[1]/div[1]/div[2]/input")
	private WebElement SearchTextfieldJobProfileFolderSet;

	public void SearchJobProfileFolderSet() throws InterruptedException {
		SearchTextfieldJobProfileFolderSet.sendKeys("deletelater");
		sleep(2);
		boolean isPresent = deletelaterJobProfileGroupSet.isDisplayed();
		String pass = "PASS>> Job/Profile is displayed";
		String fail = "FAIL>> Job/Profile is not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(3);
		SearchTextfieldJobProfileFolderSet.clear();
		sleep(3);
	}

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[2]/div[3]/div/div")
	private WebElement NoJobFolderSetAvilable;

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[2]/div[1]/div[1]/div[1]/button")
	private WebElement refreshButtonJobprofileFolderSet;

	public void ValidationNoJobFoldersetIsAvilable() throws InterruptedException, AWTException {
		SearchTextfieldJobProfileFolderSet.sendKeys("$*jkll");
		sleep(2);

		String actual = NoJobFolderSetAvilable.getText();

		System.out.println("Message:" + actual + ":Message");
		String expected = "No Job Folder Set Available.";
		String PassStatement = "PASS >>No Device GroupSet avilabel is displayed";
		String FailStatement = "FAIL >> No Device GroupSet avilabel is not displayed";
		ALib.AssertEqualsMethod(expected, actual, PassStatement, FailStatement);
		SearchTextfieldJobProfileFolderSet.clear();
		SearchTextfieldJobProfileFolderSet.sendKeys(Keys.ENTER);
		sleep(4);
		refreshButtonJobprofileFolderSet.click();
		sleep(2);

		// Robot robot=new Robot();
		// robot.keyPress(KeyEvent.VK_ENTER);
		// sleep(4);

	}

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[1]/div/div[2]/span[text()='Edit']")
	private WebElement EditButtonJobProfileSet;

	@FindBy(xpath = "//span[text()='Job Folder Set modified successfully.']")
	private WebElement JobFolderSetModifiedSuccessfully;

	public void JobProfileFolderSetModifiedSuccessfully() throws AWTException, InterruptedException {
		deletelaterJobProfileGroupSet.click();
		sleep(3);
		EditButtonJobProfileSet.click();
		waitForidPresent("SavePermissions");
		sleep(2);
		NameTextFieldJobProfileSet.clear();
		NameTextFieldJobProfileSet.sendKeys("Modified JobFolder");
		saveButton.click();
		sleep(4);
		boolean isdisplayed = true;
		try {
			JobFolderSetModifiedSuccessfully.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Job Folder Set modified successfully.\" is  not displayed ");
		Reporter.log("PASS >>\"Job Folder Set modified successfully.\" is displayed", true);
		sleep(3);
	}

	@FindBy(xpath = "//p[text()='Modified JobFolder']")
	private WebElement MdifierJobFolder;

	public void EditOptionGreyedOutForMoreTHanoneUSer() throws InterruptedException {
		AutomaticallyEnableJobProfileGroupSet.click();
		sleep(2);
		MdifierJobFolder.click();
		sleep(2);
		boolean isPresent = EditButtonJobProfileSet.isEnabled();
		String pass = "PASS>> Edit option is Disabled";
		String fail = "FAIL>> Edit option is Enabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);
		AutomaticallyEnableJobProfileGroupSet.click();
		MdifierJobFolder.click();
		sleep(4);
	}

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[2]/div[1]/div[2]/div[1]/table/thead/tr/th[1]/div[1]/span/input")
	private WebElement SelectAllCheckBoxJobProfileSet;

	public void SelectAllmainCheckBoxJobProfileFolderSet() throws InterruptedException {
		SelectAllCheckBoxJobProfileSet.click();
		sleep(2);
		boolean isPresent = SelectAllCheckBoxJobProfileSet.isEnabled();
		String pass = "PASS>> Select all option is Enabled";
		String fail = "FAIL>> Select all Option is disabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);
		SelectAllCheckBoxJobProfileSet.click();
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='jobFold_tabCont']/div[1]/div/div[4]/span[text()='Delete']")
	private WebElement DeleteButtonJobProfileFolder;

	public void EditCloneDeleteGreayedOut() throws InterruptedException {
		boolean isPresent = EditButtonJobProfileSet.isEnabled();
		String pass = "PASS>> Edit option is Disabled";
		String fail = "FAIL>> Edit option is Enabled";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);
		boolean isPresent2 = CloneButtonJobProfileSet.isEnabled();
		String pass2 = "PASS>> clone option is Disabled";
		String fail2 = "FAIL>> clone option is Enabled";
		ALib.AssertTrueMethod(isPresent2, pass2, fail2);
		sleep(2);
		boolean isPresent3 = DeleteButtonJobProfileFolder.isEnabled();
		String pass3 = "PASS>> Delete option is Disabled";
		String fail3 = "FAIL>> Delete option is Enabled";
		ALib.AssertTrueMethod(isPresent3, pass3, fail3);
		sleep(2);
	}

	public void clickonModifiedJobSet() throws InterruptedException {
		MdifierJobFolder.click();
		sleep(3);

	}

	@FindBy(xpath = ".//*[@id='ListJobsPermissions']")
	private WebElement JobProfileFolderSetCreateUser;

	public void CreateUserForPermissionsAndJobProfileFolderSet(String username, String password, String Firstname,
			String featuredPermission, String JobProfileFoldeeSet) throws InterruptedException {
		User.click();
		sleep(5);
		AddUserButton.click();
		sleep(7);
		waitForidPresent("permissionModalTitle");
		sleep(5);
		UserName.sendKeys(username);
		sleep(5);
		UserPasswoard.sendKeys(password);
		ConfirmPasswoard.sendKeys(password);
		FirstName.sendKeys(Firstname);
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(5);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(featuredPermission);
		sleep(2);
		ListFeaturePermissions.click();
		sleep(4);
		JobProfileFolderSetCreateUser.click();
		Select dropOption2 = new Select(Initialization.driver.findElement(By.id("ListJobsPermissions")));
		dropOption2.selectByVisibleText(JobProfileFoldeeSet);
		sleep(2);
		OkButton.click();
		sleep(10);

		// String Actualvalue = UserSuccessfullyCreatedPopUp.getText();
		// String Expectedvalue = "User created successfully.";
		// String PassStatement = "PASS >>'User created successfully.- displayed'";
		// String FailStatement = "FAIL >> User created successfully.not displayed";
		// ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement,
		// FailStatement);
		// sleep(5);
	}

	@FindBy(xpath = ".//*[@id='jobSection']/a")
	private WebElement JobSection;

	@FindBy(xpath = ".//*[@id='job_new_folder']/i")

	private WebElement JobFolder;

	@FindBy(xpath = ".//*[@id='jobFolderName']")
	private WebElement FolderNameTextfield;

	@FindBy(xpath = ".//*[@id='foldername_ok']")
	private WebElement OkButtonFolderName;

	@FindBy(xpath = "//span[text()='New folder created.']")
	private WebElement NewFolderCreatedMessage;

	public void ClickOnJobAndCreateJobFolder(String JobFolderName) throws InterruptedException {

		JobSection.click();
		waitForidPresent("job_new_job");
		sleep(2);
		JobFolder.click();
		waitForidPresent("commonModalSmall");
		sleep(2);
		FolderNameTextfield.sendKeys(JobFolderName);
		OkButtonFolderName.click();
		sleep(3);
		// boolean isPresent = NewFolderCreatedMessage.isDisplayed();
		// String pass = "PASS>> message Displayed";
		// String fail = "FAIL>> Message not displayed";
		// ALib.AssertTrueMethod(isPresent, pass, fail);
		// sleep(2);
	}

	@FindBy(xpath = ".//*[@id='addPolicyFolderBtn']")
	private WebElement ProfileFolder;

	public void clickOnProfileAndCreateProfileFolder(String ProfileFolderename) throws InterruptedException {
		// ProfileButton.click();
		// sleep(4);
		// waitForidPresent("selectButtonmacOS");
		clickOnProfile();
		sleep(2);
		ProfileFolder.click();
		waitForidPresent("commonModalSmall");
		sleep(2);
		FolderNameTextfield.sendKeys(ProfileFolderename);
		OkButtonFolderName.click();
		sleep(3);
		boolean isPresent = NewFolderCreatedMessage.isDisplayed();
		String pass = "PASS>> message Displayed";
		String fail = "FAIL>> Message not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[text()='" + Config.JobFolderNameEnableAutomatic + "']")
	private WebElement CreatedJobFolder;

	public void JobFolderDisplayedInJobFolderSet() throws InterruptedException {

		sleep(3);
		boolean isPresent = CreatedJobFolder.isDisplayed();
		String pass = "PASS>> Job Folder is diplayed";
		String fail = "FAIL>> Job folder is not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='profileFolders_tree']/ul/li[text()='" + Config.ProfileFolderNameEnableAutomatic + "']")
	private WebElement CreatedProfileFolder;

	public void ProfileFolderDisplayedInJobFolderSet() throws InterruptedException {
		ProfilesFolderOption.click();

		// ProfileFolderOptionIsPresent();
		sleep(4);
		boolean isPresent = CreatedProfileFolder.isDisplayed();
		String pass = "PASS>> Profile folder is displayed";
		String fail = "FAIL>> Proflie is not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(4);
		CloseDeviceGroupSet.click();
		sleep(4);

	}

	@FindBy(xpath = ".//*[@id='umTree']/ul/li[text()='" + Config.JobFolderNameDisabledAutomatic + "']")
	private WebElement CreatedJobFolder2;

	public void JobFolderNotDisplayedInJobFolderSet() throws InterruptedException {
		sleep(4);
		// boolean isPresent = CreatedJobFolder2.isDisplayed();
		// String pass = "FAIL>> Job folder is not displayed";
		// String fail = "PASS>> Job Folder is diplayed";
		// ALib.AssertTrueMethod(isPresent, pass, fail);
		// sleep(2);

		boolean displayed;

		try {
			sleep(5);
			CreatedJobFolder2.isDisplayed();
			sleep(5);
			displayed = false;
			sleep(5);
		} catch (Throwable T) {
			displayed = true;

		}
		String PassStatement = "PASS >> Device group is not present";
		String FailStatement = "FAIL >> Device group is present";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(6);
		//
		// CloseDeviceGroupSet.click();
		// sleep(6);

	}

	@FindBy(xpath = ".//*[@id='profileFolders_tree']/ul/li[text()='" + Config.ProfileFolderNameDisabledAutomatic + "']")
	private WebElement CreatedProfileFolder2;

	public void ProfileFolderNotDisplayedInJobFolderSet() throws InterruptedException {
		ProfilesFolderOption.click();

		// ProfileFolderOptionIsPresent();
		// sleep(4);
		// boolean isPresent = CreatedProfileFolder2.isDisplayed();;
		// String pass = "FAIL>> Proflie is not displayed";
		// String fail = "PASS>> Profile folder is displayed";
		// ALib.AssertTrueMethod(isPresent, pass, fail);
		// sleep(4);
		// CloseDeviceGroupSet.click();
		sleep(4);

		boolean displayed;

		try {
			sleep(5);
			CreatedProfileFolder2.isDisplayed();
			sleep(5);
			displayed = false;
			sleep(5);
		} catch (Throwable T) {
			displayed = true;

		}
		String PassStatement = "PASS >> Device group is not present";
		String FailStatement = "FAIL >> Device group is present";
		ALib.AssertTrueMethod(displayed, PassStatement, FailStatement);
		sleep(6);

		CloseDeviceGroupSet.click();
		sleep(6);
	}

	@FindBy(xpath = "//span[text()='Job Folder Set deleted successfully.']")
	private WebElement JobFolderDeletedSuccessfullymessage;

	public void deleteJobProfileFolderSet() throws InterruptedException {
		MdifierJobFolder.click();
		sleep(3);
		DeleteButtonJobProfileFolder.click();
		waitForidPresent("ConfirmationDialog");
		sleep(3);
		YesButton.click();
		sleep(2);

		boolean isPresent = JobFolderDeletedSuccessfullymessage.isDisplayed();
		String pass = "FAIL>> Job folder is not displayed";
		String fail = "PASS>> Job Folder is diplayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);
	}

	public void DeletUserAssignedTempleteJobProfileFolderSet() throws InterruptedException {
		AutomaticallyEnableJobProfileSet.click();
		DeleteButtonJobProfileFolder.click();
		waitForidPresent("ConfirmationDialog");
		sleep(3);
		YesButton.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			AssignedUserTempleteCannotBeDeleted.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"User assigned templete cannot be deleted\" is  not displayed ");
		Reporter.log("PASS >>\"User assigned templete cannot be deleted\" is  not displayed is displayed", true);
		sleep(5);
	}

	// ################################################ DEVICE GRID COLOUM SET
	// #############################################

	public void CheckCountOfColoumnsDeviceGrid() {
		List<WebElement> ColumnCount = Initialization.driver
				.findElements(By.xpath(".//*[@id='tableHeader']/tr/th/div[1]"));

		int ActualCount = ColumnCount.size();

		int ExpectedCount = 110;
		String pass = "PASS >> Column count is: " + ColumnCount + "";
		String fail = "FAIL >> Column count is wrong";
		ALib.AssertEqualsMethodInt(ExpectedCount, ActualCount, pass, fail);
	}

	@FindBy(xpath = ".//*[@id='VisibleGridColumnDataSetLIst']")
	private WebElement columnGridDataSetlist;

	public void CreateUserForDeviceColumnGrid(String username, String password, String Firstname,
			String featuredPermission, String DeviceGridColumn) throws InterruptedException {
		User.click();
		sleep(5);
		AddUserButton.click();
		sleep(7);
		waitForidPresent("permissionModalTitle");
		sleep(5);
		UserName.sendKeys(username);
		sleep(5);
		UserPasswoard.sendKeys(password);
		ConfirmPasswoard.sendKeys(password);
		FirstName.sendKeys(Firstname);
		Email.sendKeys("bhakti.rajput@42gears.com");
		sleep(5);
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);

		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(
				Initialization.driver.findElement(By.xpath("//*[@id='ListFeaturePermissions']")));
		dropOption.selectByVisibleText(featuredPermission);
		sleep(2);
		ListFeaturePermissions.click();
		sleep(4);
		columnGridDataSetlist.click();
		Select dropOption2 = new Select(Initialization.driver.findElement(By.id("VisibleGridColumnDataSetLIst")));
		dropOption2.selectByVisibleText(DeviceGridColumn);
		sleep(2);
		OkButton.click();
		sleep(3);

		String Actualvalue = UserSuccessfullyCreatedPopUp.getText(); // span[text()='User created successfully.']
		String Expectedvalue = "New user created successfully.";
		String PassStatement = "PASS >>'User created successfully.- displayed'";
		String FailStatement = "FAIL >>  User created successfully.not displayed";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(5);
	}

	@FindBy(xpath = "//article[@id='userManagement_pg']//a[text()='Device Grid Column Set']")
	private WebElement DeviceGridColumnSection;

	public void clickOnDeviceGridColumn() throws InterruptedException {
		DeviceGridColumnSection.click();
		
		sleep(4);
	}

	@FindBy(xpath = "//div[@id='grid_column_tab_tabCont']//span[text()='Add']")
	private WebElement AddButtonDeviceGridColumn;

	@FindBy(xpath = ".//*[@id='templateNameOptionList']")
	private WebElement DeviceColumnGridTextField;

	@FindBy(xpath = ".//*[@id='selectAllGridcoulmnList']")
	private WebElement SelectAllCheckBox;

	@FindBy(xpath = ".//*[@id='Savedevicegridcolumnoption']")
	private WebElement SaveDeviceColumnGridSet;
	@FindBy(xpath = "//span[text()='Grid Column Set created successfully.']")
	private WebElement DeviceGridSavedMessage;

	public void ClickOnDevicgridColumnSetAddButton() throws InterruptedException
	{
		AddButtonDeviceGridColumn.click();
		waitForidPresent("templateNameOptionList");
		sleep(2);
	}
	public void CreateNewDeviceGridColumn() throws InterruptedException {
		AddButtonDeviceGridColumn.click();
		waitForidPresent("DevicegridoptionModalTitle");
		sleep(4);
		DeviceColumnGridTextField.sendKeys("AllowAllColumn");
		sleep(2);
		SelectAllCheckBox.click();
		sleep(2);
		SaveDeviceColumnGridSet.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			DeviceGridSavedMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Grid Column Set created successfully.\" is  not displayed ");
		Reporter.log("PASS >>\"Grid Column Set created successfully.\" is  not displayed is displayed", true);
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[text()='Serial Number']")
	private WebElement ScrollToSerialNumber;

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[16]/span[3]")
	private WebElement ImeiCheckBox;

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[text()='USB']")
	private WebElement ScrollToUsb;

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[33]/span[3]")
	private WebElement BluetoothCheckBox;

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[29]/span[3]")
	private WebElement NetworkTypeCheckBox;

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[34]/span[3]")
	private WebElement UsbColumn;

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[text()='Threats Count']")
	private WebElement ScrolltoThreatsCount;

	@FindBy(xpath = ".//*[@id='idgridlist']/ul/li[40]/span[3]")
	private WebElement BluetoothNameCheckBox;

	public void CreateNewDeviceGridColumnAllowOnlyFew() throws InterruptedException {
		AddButtonDeviceGridColumn.click();
		waitForidPresent("DevicegridoptionModalTitle");
		sleep(4);
		DeviceColumnGridTextField.sendKeys("AllowFewColumn");
		sleep(2);
		WebElement scrollDown = ScrollToSerialNumber;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(2);
		ImeiCheckBox.click();

		WebElement scrollDown2 = ScrollToUsb;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(2);
		BluetoothCheckBox.click();
		UsbColumn.click();
		NetworkTypeCheckBox.click();
		WebElement scrollDown3 = ScrolltoThreatsCount;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown3);
		sleep(2);
		BluetoothNameCheckBox.click();
		sleep(1);
		SaveDeviceColumnGridSet.click();
		sleep(2);
		boolean isdisplayed = true;
		try {
			DeviceGridSavedMessage.isDisplayed();

		} catch (Exception e) {
			isdisplayed = false;

		}
		Assert.assertTrue(isdisplayed, "Fail:\"Grid Column Set created successfully.\" is  not displayed ");
		Reporter.log("PASS >>\"Grid Column Set created successfully.\" is  not displayed is displayed", true);
		sleep(5);
	}

	@FindBy(xpath = ".//*[@id='userManagementGrid']/tbody/tr/td[text()='AutoDeviceGridColumnSet']")
	private WebElement SelectExistingUser;

	@FindBy(xpath = ".//*[@id='VisibleColumnSetting']/span[text()='Visible Column In Grid']")
	private WebElement VisibleColumnInGridButton;

	@FindBy(xpath = ".//*[@id='grid_column_assign']/tbody/tr/td/p[text()='AllowFewColumn']")
	private WebElement SelectAllowFewColulmn;

	@FindBy(xpath = ".//*[@id='AssignGridCoulmnVisibility']")
	private WebElement OkButtonAssignColulmnVisibility;

	@FindBy(xpath = "//span[text()='Device Grid Column Set applied successfully.']")
	private WebElement DeviceGridAllpiedSuccessfullMessage;

	@FindBy(xpath = "//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='IMEI']")
	private WebElement IMEIColumn;

	@FindBy(xpath = "//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Device']")
	private WebElement DeviceColumn;

	@FindBy(xpath = "//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Bluetooth']")
	private WebElement BluetoothColumn;

	@FindBy(xpath = "//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Network Type']")
	private WebElement NetworkTypeColumn;

	@FindBy(xpath = "//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Bluetooth Name']")
	private WebElement BluetoothNameColumn;

	@FindBy(xpath = "//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='USB']")
	private WebElement UsbColumnName;

	public void CheckallowedVisibleColoumn() throws InterruptedException {
		sleep(4);
		boolean isPresent = IMEIColumn.isDisplayed();
		String pass = "PASS>> IMEI column is displayed";
		String fail = "FAIL>> IMEI column is not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(4);
		// WebElement scroll =
		// driver.findElement(By.xpath("//div[@id='gvLocationHorizontalRail']"));
		// JavascriptExecutor js = (JavascriptExecutor)driver;
		// js.executeScript("window.scrollBy(250,0)", "");
		boolean isPresent1 = NetworkTypeColumn.isDisplayed();
		String pass1 = "PASS>> Network Type column is displayed";
		String fail1 = "FAIL>> Network type column is not displayed";
		ALib.AssertTrueMethod(isPresent1, pass1, fail1);
		sleep(4);
		// boolean isPresent2 = BluetoothNameColumn.isDisplayed();
		// String pass2 = "PASS>> Bluetooth Name column is displayed";
		// String fail2 = "FAIL>> Bluetooth Name column is not displayed";
		// ALib.AssertTrueMethod(isPresent2, pass2, fail2);
		// sleep(4);
	}

	public void HorizontalScroll() throws InterruptedException {
		WebElement scroll = Initialization.driver.findElement(
				By.xpath("//div[@class='fixed-table-header']/table/thead/tr/th/div[text()='Bluetooth Name']"));
		JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
		js.executeScript("arguments[0].scrollIntoView()", scroll);

		sleep(3);
		boolean isPresent = BluetoothColumn.isDisplayed();
		String pass = "PASS>> Bluetooth column is displayed";
		String fail = "FAIL>> Bluetooth column is not displayed";
		ALib.AssertTrueMethod(isPresent, pass, fail);
		sleep(2);
		// js.executeScript("window.scrollBy(250,0)", "");
		boolean isPresent1 = NetworkTypeColumn.isDisplayed();
		String pass1 = "PASS>> Network Type column is displayed";
		String fail1 = "FAIL>> Network type column is not displayed";
		ALib.AssertTrueMethod(isPresent1, pass1, fail1);
		sleep(2);
		boolean isPresent2 = BluetoothNameColumn.isDisplayed();
		String pass2 = "PASS>> Bluetooth Name column is displayed";
		String fail2 = "FAIL>> Bluetooth Name column is not displayed";
		ALib.AssertTrueMethod(isPresent2, pass2, fail2);
		sleep(2);
		boolean isPresent3 = UsbColumnName.isDisplayed();
		String pass3 = "PASS>> Usb column is displayed";
		String fail3 = "FAIL>> Usb column is not displayed";
		ALib.AssertTrueMethod(isPresent3, pass3, fail3);
		sleep(4);
	}

	public void RefreshCurrentPage() throws InterruptedException {
		Initialization.driver.navigate().refresh();
		waitForidPresent("deleteDeviceBtn");
		sleep(4);
	}

	// new Methods

	// DELETE Existing Users
	public void SearchUserUM(String InputUser) throws InterruptedException {
		SearchUser.clear();
		sleep(2);
		SearchUser.sendKeys(InputUser);
		sleep(3);
	}

	public void DeleteExistingUsersInUM() throws InterruptedException {

		deleteUser.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		YesDelete.click();
		waitForVisibilityOf("//span[contains(text(),'User deleted successfully.')]");
		Reporter.log("'User deleted successfully.'", true);
		sleep(2);

	}

	public void DeletingExistingUser(String InputUser) throws InterruptedException {
		SearchUserUM(InputUser);
		try {
			Initialization.driver
			.findElement(By.xpath("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + InputUser + "']"))
			.isDisplayed();
			Reporter.log("User Have been Created Already", true);
			Initialization.driver
			.findElement(By.xpath("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + InputUser + "']"))
			.click();
			DeleteExistingUsersInUM();
		}

		catch (Exception e) {
			Reporter.log("No User Is Found With Searched Name", true);
			SearchUser.clear();
			sleep(2);

		}
	}

	// DELETE Existing Roles
	@FindBy(xpath = "//*[@id='featPerm_tabCont']/div[1]/div/div[4]/span[text()='Delete']")
	private WebElement DeleteRole;

	public void DeleteExistingRolesInUM() throws InterruptedException {

		DeleteRole.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		YesDelete.click();
		waitForXpathPresent("//span[contains(text(),'Role(s) deleted successfully.')]");
		Reporter.log("'Role(s) deleted successfully.'", true);
		sleep(3);

	}

	// DELETE Existing Roles
	public void DeletingExistingRole(String InputRole) throws InterruptedException {
		SearchRole(InputRole);
		try{
			Initialization.driver
			.findElement(
					By.xpath("//*[@id='PermissionsRolesGrid']/tbody/tr/td[2]/p[text()='" + InputRole + "']"))
			.isDisplayed();
			Reporter.log("Role Have been Created Already", true);
			Initialization.driver
			.findElement(
					By.xpath("//*[@id='PermissionsRolesGrid']//p[text()='"+InputRole+"']//ancestor::tr/td[@class='bs-checkbox']"))
			.click();

			DeleteExistingRolesInUM();
		}
		

		catch (Exception e) {
			Reporter.log("No Role Is Found With Searched Name", true);
		}
	}

	public void VerifyOfOptionsInAddUserUI() throws InterruptedException {
		User.click();
		AddUserButton.click();
		sleep(3);

		WebElement[] eles = { UserName, UserPasswoard, ConfirmPasswoard, FirstName, LastName, Email, PhoneNumber,
				ListFeaturePermissions, OkButton };
		String[] Names = { "User Name", "Password ", "Confirm Password", "First Name", "Last Name", "Email",
				"Phone Number", "Roles", "Create" };
		for (int i = 0; i < eles.length; i++) {
			boolean val = eles[i].isDisplayed();
			String Pass = "PASS>>>" + " " + Names[i] + "Is Displayed Successfully";
			String Fail = "FAIL>>>" + " " + Names[i] + "Is Not Displayed";
			ALib.AssertTrueMethod(val, Pass, Fail);
		}
		Initialization.driver.findElement(By.id("cancelbtn")).click();
		Reporter.log("Clicked On Cancel Button", true);
	}

	public void clickOnAddUserBtn() throws InterruptedException {
		AddUserButton.click();
		Reporter.log("Cliced on Add User", true);
		sleep(2);
	}

	public void EnterUserName(String username) throws InterruptedException {
		UserName.sendKeys(username);
		Reporter.log("Entered Username", true);
		sleep(3);

	}

	public void EnterUserPasswordAndConfirm(String psw, String confirmpwd) throws InterruptedException {
		UserPasswoard.sendKeys(psw);
		Reporter.log("Entered Password", true);
		sleep(3);
		ConfirmPasswoard.sendKeys(confirmpwd);
		Reporter.log("Entered Confirm Password", true);
	}

	public void EnterFirstName(String Firstname) throws InterruptedException {
		FirstName.clear();
		sleep(2);
		FirstName.sendKeys(Firstname);
		Reporter.log("Entered First Name", true);
		sleep(2);
	}

	@FindBy(xpath = ".//*[@id='lastname']")
	private WebElement LastName;

	@FindBy(xpath = ".//*[@id='phone']")
	private WebElement PhoneNumber;

	public void EnterLastName(String lastname) throws InterruptedException {
		LastName.clear();
		sleep(2);
		LastName.sendKeys(lastname);
		Reporter.log("Entered Last Name", true);
	}

	public void EnterUserEmail(String email) throws InterruptedException {
		Email.clear();
		sleep(2);
		Email.sendKeys(email);
		Reporter.log("Entered email", true);
	}

	public void EnterPhoneNo(String phone) throws InterruptedException {
		PhoneNumber.clear();
		sleep(2);
		PhoneNumberTextField.sendKeys(phone);
		sleep(2);
		Reporter.log("Entered Phone number", true);
	}

	public void selectRoleForUser(String SelectRoles) throws InterruptedException {
		ListFeaturePermissions.click();
		sleep(3);
		Select dropOption = new Select(Initialization.driver.findElement(By.id("ListFeaturePermissions")));
		dropOption.selectByVisibleText(SelectRoles);
	}

	public void clicOnCreatBtn() throws InterruptedException {
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(2);
		OkButton.click();
		// waitForXpathPresent("//span[text()='New user created successfully.']");
		Reporter.log("Clicked on Create Button", true);
	}

	public void selectUserInUsersList(String InputUser) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//*[@id='userManagementGrid']/tbody/tr/td[text()='" + InputUser
				+ "']//parent::tr/td[@class='bs-checkbox']")).click();
		sleep(4);

	}

	public void VerifyOfUserModified() throws InterruptedException {
		WebElement scrollDown2 = OkButton;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);
		sleep(2);
		OkButton.click();
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='The user was modified successfully.']"))
			.isDisplayed();
			Reporter.log("PASS >> User modified successfully.", true);
		} catch (Exception e) {
			ALib.AssertFailMethod("User didnt modified successfully.");
		}
	}

	@FindBy(id = "changePassword")
	private WebElement SecurityBtn;

	public void clickOnSecurity() throws InterruptedException {
		SecurityBtn.click();
		Reporter.log("Clicked on Security", true);
		sleep(3);
		waitForidPresent("changePwdLiTab");
	}

	public void EnterOldPwd(String oldpwd, String newpwd, String retypenewpwd) throws InterruptedException {
		Initialization.driver.findElement(By.id("oldpassword")).sendKeys(oldpwd);
		Reporter.log("Entered Old Password", true);
		sleep(3);
		Initialization.driver.findElement(By.id("newpassword")).sendKeys(newpwd);
		Reporter.log("Entered new Password", true);
		sleep(3);
		Initialization.driver.findElement(By.xpath("//input[@data-match='#newpassword']")).sendKeys(retypenewpwd);
		Reporter.log("Re-Entered new Password", true);
		sleep(2);
	}

	public void NotificationOnResettingPwd() {
		try {
			Initialization.driver.findElement(By.xpath("//span[text()='Password reset successful.']")).isDisplayed();
			Reporter.log("PASS >> Password reset successful. is displayed", true);
		} catch (Exception e) {
			Reporter.log("FAIL >> Password reset successful. isn't displayed");
		}
	}

	public void clickOnOkBtnInresetPwd() throws InterruptedException {
		okbtn.click();
		waitForXpathPresent("//span[text()='Password reset successful.']");
		System.out.println("Password reset successful.");
		sleep(2);
	}

	public void OpenPermissionsDropDown(String Permission) throws InterruptedException {
		WebElement ele = Initialization.driver
				.findElement(By.xpath("//div[@id='umTree']/ul/li[text()='" + Permission + "']/span[1]"));
		js.executeScript("arguments[0].scrollIntoView(true);", ele);
		ele.click();
		sleep(3);
	}

	public void SelectingAllPermissions(String Permission) throws InterruptedException
	{
		WebElement ele = Initialization.driver
				.findElement(By.xpath("//div[@id='umTree']/ul/li[text()='"+Permission+"']/span[1]/following-sibling::span[2]"));
		js.executeScript("arguments[0].scrollIntoView(true);", ele);
		ele.click();
		sleep(3);
		Reporter.log("Selected "+Permission+" Permissions",true);
	}

	public void SelectingPermission(String Permission) throws InterruptedException {
		WebElement ele = Initialization.driver
				.findElement(By.xpath("//li[text()='" + Permission + "']//child::span[4]"));
		js.executeScript("arguments[0].scrollIntoView(true);", ele);
		ele.click();
		sleep(3);
	}

	public void SelectingMultiplePermissons(String[] permissions) throws InterruptedException
	{
		for(int i=0;i<permissions.length;i++)
		{
			WebElement ele = Initialization.driver
					.findElement(By.xpath("//li[text()='" + permissions[i] + "']//child::span[4]"));
			js.executeScript("arguments[0].scrollIntoView(true);", ele);
			ele.click();
			sleep(3);
		}
	}

	public void ClickOnEdituserButton() throws InterruptedException {
		EditUserButton.click();
		waitForidPresent("userid");
		sleep(3);
	}

	public void VerifyRoleFieldIsNotEmptyWhenLoggedInasSubUser(String Role) {
		Select sel = new Select(ListFeaturePermissions);
		List<WebElement> aa = sel.getAllSelectedOptions();
		for (WebElement ae : aa) {
			if (ae.getText().equals(Role))
				Reporter.log("PASS>>> Role Name Is Displayed Correctly", true);
			else
				ALib.AssertFailMethod("FAIL>>>  Role Name Is Not Displayed Correctly");
		}
	}

	public void VerifyAddingTagWhenNotPermitted(String Tagname) throws InterruptedException, AWTException {
		Initialization.Tags.EnterCustomTagName(Tagname);
		Initialization.Tags.AddButtonCustomTag();
		try {
			waitForXpathPresent("//span[text()='Access denied. Please contact your administrator.']");
			Reporter.log("PASS>>> Access Denied Message Is Displayed When Sub User Tried To Add Tag", true);
		}

		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Access Denied Message Is Not Displayed When Sub User Tried To Add Tag");
		}
		sleep(3);
	}

	public String ReadingReportNameGeneratedBySubUser()
	{
		String LatestReportname=Initialization.driver.findElement(By.xpath("//table[@id='OfflineReportGrid']/tbody//td[1]")).getText();
		return LatestReportname;

	}

	public void VerifyReportGeneratedBytSubUser(String ReportName)
	{
		if(ReadingReportNameGeneratedBySubUser().equals(ReportName))
			Reporter.log("Report Generated By Sub User Successfully",true);
		else
			ALib.AssertFailMethod("Sub User Can't generate report");
	}

	public void VerifySubUserUnableToSeeReportGeneratedByotherSubUser(String ReportName)
	{
		if(ReadingReportNameGeneratedBySubUser().equals(ReportName))
			ALib.AssertFailMethod("Sub User Can View Report generate by another sub user");

		else
			Reporter.log("Sub User Can't View Report generate by another sub user",true);
	}

	public void VerifyInboxPermissionUI(String[] permissions)
	{
		for(String per:permissions)
		{
			boolean inboxPermissons = Initialization.driver.findElement(By.xpath("//li[text()='"+per+"']")).isDisplayed();
			if(inboxPermissons)
				Reporter.log("The permissions are displayed as per options displayed in Inbox,",true);
			else
				ALib.AssertFailMethod("The permissions are not displayed as per options displayed in Inbox,");
		}
	}

	@FindBy(xpath="//a[text()='Mobile Email Management']")
	private WebElement MEMOption;

	public void VerifyMEMIsNotDisplayedForSubUserWhenNotPermitted()
	{
		try
		{
			MEMOption.click();
			ALib.AssertFailMethod("FAIL>>> MEM option is displayed for subuser when not permitted");
		}
		catch (Exception e) {
			Reporter.log("PASS>>> MEM option is Not displayed for subuser when not permitted ",true);
		}
	}

	@FindBy(xpath="//ul[@id='navbarHeaderSection']//span[text()='More']")
	private WebElement MoreOption_NavigationBar;
	
	@FindBy(id="listAppsPlugins")
	private WebElement AppsPluginPage;

	public void verifySubUserIsableToaccessPlugin() throws InterruptedException
	{
		Actions act=new Actions(Initialization.driver);
		act.moveToElement(MoreOption_NavigationBar).build().perform();
		sleep(2);
		try
		{
			Initialization.driver.findElement(By.xpath("//a[text()='Apps Plugin']")).click();
			sleep(2);
			waitForidPresent("listAppsPlugins");
			Reporter.log("PASS>> Sub user is able to access app plugin",true);
			
		}
		catch (Exception e) {
              ALib.AssertFailMethod("FAIL>>> Subuser cannot access app plugin");
		}
	}
	
	@FindBy(xpath="//div[@id='devGrp_tabCont']//span[text()='Edit']")
	private WebElement DeviceGropupSetEditButton;
	public void SelectingDeviceGroupSet() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("(//table[@id='GroupsPermissionGrid']//td[@class='bs-checkbox'])[1]")).click();
		sleep(2);
	}
	
	public void ClickOnDeviceGridEditButton() throws InterruptedException
	{
		DeviceGropupSetEditButton.click();
		waitForidPresent("templateName");
		sleep(2);
	}
	
	@FindBy(xpath="//div[@id='umTree']//li[3]")
	private WebElement GroupName_DeviceGroups;
	
	@FindBy(xpath="//div[@id='gridcoulmnoptiondiv']//button[@aria-label='Close']")
	private WebElement DeviceGridColumnCloseButton;
	
	@FindBy(xpath="//ul[@id='gridMenu']//span[text()='Move to Group']")
	private WebElement MoveToGroup;
	
	@FindBy(id="tagsDeviceSaveBtn")
	private WebElement TagSaveButton;
	
	@FindBy(id="advanceSettings")
	public WebElement AccountSettingsOption;
	
	@FindBy(xpath="//*[@id='umTree']/ul/li[2]/span[3]")//trail
	private WebElement DeviceActionPermissions ;
	
	@FindBy(id="office365Management")
	public WebElement Office365Option;
	
	@FindBy(id="licenseManagement")
	public WebElement LicensedManagementOption;
	
	boolean statusBeforeModification;
	boolean statusAfterModification;
	
	public boolean GettingCheckBoxStatusBeforeModification()
	{
		statusBeforeModification=GroupName_DeviceGroups.getAttribute("class").contains("checked");
        return statusBeforeModification;
	}
	
	public boolean GettingCheckBoxStatusAfterModification()
	{
		statusAfterModification=GroupName_DeviceGroups.getAttribute("class").contains("checked");
	   return statusAfterModification;
	}
	
	
	public void Enabling_DisablingCheckBox() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='umTree']//li[3]//span[4]")).click();
		sleep(2);
	}
	
	public void VerifyDeviceGroupSetModification()
	{
		GettingCheckBoxStatusAfterModification();
		if(statusBeforeModification==statusAfterModification)
	    ALib.AssertFailMethod("FAIL>>> Modification Is Not Saved");
		else
			Reporter.log("PASS>>> Modification Is Saved Successfully",true);
	}
	
	public void VerifyUMMessage(String ExpectedMessage)
	{
		String ActaulMessage = Initialization.driver.findElement(By.xpath("//article[@id='userManagement_pg']//div[@class='TextAreaHelpBlockmessage']//p")).getText();
	    System.out.println(ExpectedMessage);
	    System.out.println(ActaulMessage);
		String Pass="PASS>>> Message Is Shown As Expected";
	    String Fail="FAIL>>> Message Is Not Shown As Expected";
	    ALib.AssertEqualsMethod(ExpectedMessage, ActaulMessage, Pass, Fail);
	}
	
	public void VerifyPermissionUI(String per)
	{
		
			boolean inboxPermissons = Initialization.driver.findElement(By.xpath("//li[text()='"+per+"']")).isDisplayed();
			if(inboxPermissons)
				Reporter.log("The permissions are displayed successfully",true);
			else
				ALib.AssertFailMethod("The permissions are not displayed");
	}
	
	public void ClickOnGridColumnCloseButton() throws InterruptedException
	{
		DeviceGridColumnCloseButton.click();
		waitForidPresent("userProfileButton");
		sleep(2);
	}
	
	public void VerifyAccessDenied()
	{
		try
		{
		AccessDeniedErrorMessage.isDisplayed();
		Reporter.log("PASS>>> Access Denied Message Is Displayed Successfully",true);
		sleep(3);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> Access Denied Message Is not Displayed");
		}
	}
	
	public void ClikcOnMovToGroup() throws InterruptedException
	{
		MoveToGroup.click();
		sleep(2);
	}
	
	public void ClikcOnRemoveTagSaveButton() throws InterruptedException
	{
		TagSaveButton.click();
		sleep(2);
	}
	
	public void VerifyOptionForsubuser(WebElement ele,String Option)
	{
		try
		{
		ele.isDisplayed();
		Reporter.log("PASS>>> "+Option+" Option Is Dispalyed For Sub User",true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL>>> "+Option+" Option Is not Displayed For Sub user");
		}
	}
	
	@FindBy(xpath="//span[text()='Profile deleted successfully.']")
	private WebElement ProfiledeletedMessage;
	public void VerifyProfileDeltedSuccessfully()
	{
		try
		{
			ProfiledeletedMessage.isDisplayed();
			Reporter.log("PASS>>> Profile Deleted Succesfully",true);
			
		}
		catch(Exception e )
		{
			ALib.AssertFailMethod("FAIL>>> Profile Not deleted");
		}
	}
	
	public void EnableHideParentGroupCheckBox() throws InterruptedException
	{
		HideParentGroupWhenNoAccessToChildGroups.click();
		sleep(2);
	}
	
	public void VerifyWebHooksIsNotDisplayed()
	{
		try
		{
			Initialization.driver.findElement(By.id("webhooksdiv")).click();
			Initialization.driver.findElement(By.xpath("//div[@id='notification_modal']//button[@class='close']")).click();
			ALib.AssertFailMethod("FAIL>>> Sub User Is able to Use webhookoption");
		}
		catch (Exception e) 
		{
			Initialization.driver.findElement(By.xpath("//div[@id='notification_modal']//button[@class='close']")).click();
			Reporter.log("PASS>>> Sub User Is Not Able to Use webhook option",true);
		}
	}
	
	
	public void ModifyRole(String RoleName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//td//p[text()='"+RoleName+"']")).click();
		sleep(2);
		ClickOnEdit();
	}
	
	public void SelectingRole(String RoleName)
	{
		Initialization.driver
		.findElement(
				By.xpath("//*[@id='PermissionsRolesGrid']//p[text()='" + RoleName + "']//ancestor::tr/td[@class='bs-checkbox']"))
		.click();
	}
	
	public void ClickOnRoleSaveButton_Modify() throws InterruptedException {
		saveButton.click();
		waitForXpathPresent("//span[text()='Role modified successfully.']");
		sleep(3);
		Reporter.log("Clicked On role Save Button",true);
	}
	
	public void VerifyPermissionStatusAfterDisabling(String Permission) throws InterruptedException
	{
      String CheckBoxStatus= Initialization.driver.findElement(By.xpath("//li[text()='" + Permission + "']//child::span[4]")).getAttribute("class");
	  if(CheckBoxStatus.contains("unchked"))
		  Reporter.log("PASS>>> Permission Checkbox Is Disabled",true);
	  else
	  {
		  ClickOnRoleSaveButton_Modify();
		  ALib.AssertFailMethod("FAIL>>> Permission Checkbox Is Not Disabled");
	  }
	}
	
	public void DeleteExistingRolesWhenAssignedToUser() throws InterruptedException {

		DeleteRole.click();
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		YesDelete.click();
	     waitForXpathPresent("//span[text()='User(s) assigned template(s) cannot be deleted.']");
	     Reporter.log("User(s) assigned template(s) cannot be deleted. message is displayed when tried to delete Assigened role",true);
	}
	@FindBy(id="passworderr")
	private WebElement Pwdcomplexerrormessage;
	
	String pwdcomplexerrormsg="Passwords must have at least 8 characters and contain following: uppercase letters, lowercase letters, numbers and symbols.";
 	public void VerifyPwdComplextiyErrorMessage()
	{
 		String Actualmessage=Pwdcomplexerrormessage.getText();
 		String Pass="Complex password error Message displayed successfully";
 		String Fail="Complex password error Message Is not displayed";
		ALib.AssertEqualsMethod(pwdcomplexerrormsg, Actualmessage, Pass, Fail);
	}
 	
 	public void RefreshConsole() throws InterruptedException
 	{
 		Initialization.driver.navigate().refresh();
 		waitForidPresent("deleteDeviceBtn");
 		sleep(3);
 	}
	
	

}
