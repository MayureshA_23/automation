package PageObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.Helper;
import Library.WebDriverCommonLib;
import io.appium.java_client.MobileBy;

public class AccountSettings_POM extends WebDriverCommonLib{

	AssertLib ALib=new AssertLib();
	ExcelLib ELib=new ExcelLib();
	String XpathforPageToLoadAccountSettings="//span[text()='Account Settings']";
	JavascriptExecutor Jc = (JavascriptExecutor) Initialization.driver;
	@FindBy(id="advanceSettings")
	private WebElement AccountSettings;

	@FindBy(xpath="//span[text()='Account Settings']")
	private WebElement PageOfAccountSettings;

	@FindBy(id="branding_info_panel")
	private WebElement BrandingInfoSection;

	@FindBy(id="enrollement_rules_panel")
	private WebElement DeviceEnrollmentSection;

	@FindBy(id="Miscellaneous_Settings")
	private WebElement MiscellaneousSettingsSection;

	@FindBy(id="sso_settings")
	private WebElement SSOSection;

	@FindBy(id="templatealertload")
	private WebElement AlertTemplateSection;

	@FindBy(xpath="//a[text()='Customize Toolbar']")
	private WebElement CustomizeToolbarSection;

	@FindBy(xpath="//a[text()='iOS Enrolment Settings']")
	private WebElement iOSEnrolmentSettingsSection;

	@FindBy(id="use_Logo")
	private WebElement UseLogo;

	@FindBy(id="enable_Logo")
	private WebElement EnableLogoPath;

	@FindBy(id="enable_title")
	private WebElement EnableTitleOption;

	@FindBy(xpath="//span[text()='Message Footer']")
	private WebElement MessageFooterOption;

	@FindBy(id="advance_title")
	private WebElement Title;

	@FindBy(id="advance_subtitle")
	private WebElement SubTitle;

	@FindBy(id="advance_footer")
	private WebElement MessageFooter;

	@FindBy(id="branding_apply")
	private WebElement BrandingInfoApplyBtn;

	@FindBy(id="browselogoFile")
	private WebElement BrowseLogoFile;

	@FindBy(xpath="//span[text()='Please provide a title.']")
	private WebElement ErrorMessageTitle;

	@FindBy(xpath="//span[text()='Please provide a subtitle.']")
	private WebElement ErrorMessageSubTitle;

	@FindBy(xpath="//span[text()='Select image for logo.']")
	private WebElement ErrorMessageLogoPathFile;

	@FindBy(xpath="//span[text()='Logo image size should not be greater than 1MB.']")
	private WebElement ErrorMessageLogoPathSize;

	@FindBy(xpath="//span[text()='Settings updated successfully.']")
	private WebElement ConfirmationMessage;

	@FindBy(id="title")
	private WebElement HomePageTitle;

	@FindBy(id="subTitle")
	private WebElement HomePageSubTitle;

	@FindBy(id="title_logo_div")
	private WebElement HomePageLogoPath;

	@FindBy(id="title_info_div")
	private WebElement HomePageLogoInfo;

	@FindBy(xpath="//p[text()='Device Naming']")
	private WebElement DeviceEnrolmentDeviceNamingSection;

	@FindBy(xpath="//span[text()='Prefix']")
	private WebElement DeviceEnrolmentPrefixField;

	@FindBy(xpath="//span[text()='Suffix']")
	private WebElement DeviceEnrolmentSuffixField;

	@FindBy(xpath="//span[text()='Start Count']")
	private WebElement DeviceEnrolmentCountField;

	@FindBy(xpath="//span[text()='Count Length']")
	private WebElement DeviceEnrolmentCountLengthField;

	@FindBy(xpath="//p[text()='Enrollment Authentication']")
	private WebElement DeviceEnrolmentEnrollmentAuthenticationSection;

	@FindBy(xpath="//span[text()='Device Authentication Type']")
	private WebElement DeviceEnrolmentDeviceAuthenticationTypeField;

	@FindBy(id="name_prefix")
	private WebElement DeviceEnrolmentPrefix;

	@FindBy(id="name_suffix")
	private WebElement DeviceEnrolmentSuffix;

	@FindBy(id="name_Count")
	private WebElement DeviceEnrolmentCount;

	@FindBy(id="nameCount_length")
	private WebElement DeviceEnrolmentCountLength;

	@FindBy(id="device_eg")
	private WebElement DeviceEnrolmentCountLengthExample;

	@FindBy(id="device_enrollement_apply")
	private WebElement DeviceEnrolmentApplyBtn;

	@FindBy(id="device_Authenticate")
	private WebElement DeviceAuthenticationType;

	@FindBy(id="show_password")
	private WebElement ShowPasswordCheckbox;

	@FindBy(id="device_password")
	private WebElement EnterPasswordTextbox;

	@FindBy(xpath="//span[text()='Password cannot be less than 8 characters.']")
	private WebElement ErrorMessageForPassword;

	@FindBy(id="ad_auth_endpoint")
	private WebElement AuthEndpointTextbox;

	@FindBy(id="ad_token_endpoint")
	private WebElement TokenEndpointTextbox;

	@FindBy(xpath="//span[text()='Authentication endpoint cannot be empty.']")
	private WebElement ErrorMessageAuthEndPoint;

	@FindBy(xpath="//span[text()='Token endpoint cannot be empty.']")
	private WebElement ErrorMessageTokenEndPoint;

	@FindBy(id="ad_server_path")
	private WebElement ActiveDirectoryServePathTextbox;

	@FindBy(id="ad_domain_filter")
	private WebElement DomainFilterTextbox;

	@FindBy(id="oauth_client_id")
	private WebElement OathFilterTextbox;

	@FindBy(id="ad_username")
	private WebElement AdminUsernameTextbox;

	@FindBy(id="ad_password")
	private WebElement AdminPasswordTextbox;

	@FindBy(xpath="//span[text()='Active directory server path cannot be empty.']")
	private WebElement ErrorMessageActiveDirectoryServePath;

	@FindBy(xpath="//span[text()='Domain filter cannot be empty.']")
	private WebElement ErrorMessageDomainFilter;

	@FindBy(xpath="//span[text()='Admin username cannot be empty.']")
	private WebElement ErrorMessageAdminUsername;

	@FindBy(xpath="//span[text()='Admin password cannot be empty.']")
	private WebElement ErrorMessageAdminPassword;

	@FindBy(xpath="//span[text()='Use GCM']")
	private WebElement MiscellaneousUseGCMField;

	@FindBy(xpath="//span[text()='Default connectivity option for Install/File Transfer jobs']")
	private WebElement MiscellaneousDefaultConnectivityField;

	@FindBy(xpath="//span[text()='Provide Remote Access to 42Gears Support Team']")
	private WebElement MiscellaneousRemoteAccessField;

	@FindBy(xpath="//span[text()='Use old remote support']")
	private WebElement MiscellaneousUseOldRemoteSupportField;

	@FindBy(xpath="//span[contains(text(),'Pause Screen Capture')]")
	private WebElement MiscellaneousPauseScreenCaptureField;

	@FindBy(xpath="//p[text()='Remote Support']")
	private WebElement MiscellaneousRemoteSupportSection;

	@FindBy(xpath="//span[text()='Zip all downloads']")
	private WebElement MiscellaneousZipDownloadsField;

	@FindBy(id="useGCM")
	private WebElement UseGCMCheckbox;

	@FindBy(id="download_via_adv")
	private WebElement DefaultconnectivityoptionCheckbox;

	@FindBy(id="provide_remote_access_btn")
	private WebElement RemoteAccessBtn;

	@FindBy(id="use_oldRemote")
	private WebElement UseOldRemoteSupportCheckbox;

	@FindBy(id="DoNotPauseRemoteScreen")
	private WebElement DontPauseScreenCaptureCheckbox;

	@FindBy(id="IsZipAllDownloadsRemote")
	private WebElement ZipAllDownloadsCheckbox;

	@FindBy(id="miscellaneous_apply")
	private WebElement MiscellaneousApplyBtn;

	@FindBy(xpath="//span[text()='Successfully granted remote access to 42Gears Support.']")
	private WebElement RemoteAccessGrantConfirmMessage;

	@FindBy(xpath="//span[text()='Successfully revoked remote access from 42Gears Support.']")
	private WebElement RemoteAccessRevokeConfirmMessage;

	@FindBy(xpath="//span[text()='Enable Single Sign-On']")
	private WebElement EnableSingleSignOnText;

	@FindBy(id="enable_sso")
	private WebElement EnableSingleSignOnCheckbox;

	@FindBy(xpath="//span[text()='SSO Type']")
	private WebElement SSOTypeText;

	@FindBy(id="sso_service_selEle")
	private WebElement SSOTypeDropDown;

	@FindBy(xpath="//span[text()='Service Identifier']")
	private WebElement ServiceIdentifierText;

	@FindBy(id="sso_fsi")
	private WebElement ServiceIdentifierTextBox;

	@FindBy(xpath="//div[@id='sso_serviceId_row']/span/span")
	private WebElement ServiceIdentifierEg;

	@FindBy(xpath="//span[text()='Please provide Service Identifier.']")
	private WebElement ServiceIdentifierErrorMessage;

	@FindBy(xpath="//span[text()='Sign On Service Url']")
	private WebElement SignOnServiceUrlText;

	@FindBy(id="sso_url")
	private WebElement SignOnServiceUrlTextBox;

	@FindBy(xpath="//div[@id='sso_signonservice_row']/span/span")
	private WebElement SignOnServiceUrlEg;

	@FindBy(xpath="//span[text()='Please provide Service URL.']")
	private WebElement SignOnServiceUrlErrorMessage;

	@FindBy(xpath="//span[text()='Logout Service Url']")
	private WebElement LogoutServiceUrlText;

	@FindBy(id="sso_logout_url")
	private WebElement LogoutServiceUrlBox;

	@FindBy(xpath="//div[@id='sso_logoutservice_row']/span/span")
	private WebElement LogoutServiceUrlEg;

	@FindBy(xpath="//button[text()='Default User Permission']")
	private WebElement DefaultPermissionBtn;

	@FindBy(id="GenerateCertficate")
	private WebElement GenerateCertficateBtn;

	@FindBy(xpath="//span[text()='Certificate generated successfully.']")
	private WebElement GenerateCertficateConfirmMessage;

	@FindBy(id="DeleteCertificate")
	private WebElement DeleteCertificateBtn;

	@FindBy(xpath="//span[text()='Certificate deleted successfully.']")
	private WebElement DeleteCertficateConfirmMessage;

	@FindBy(id="DownloadCertificate")
	private WebElement DownloadCertificateBtn;

	@FindBy(id="UploadCertficate")
	private WebElement UploadCertficateBtn;

	@FindBy(xpath="//h4[text()='Certificate']")
	private WebElement UploadCertificateHeader;

	@FindBy(xpath="//div[@id='ssoCertificateAddPopup']/div/div/div[3]/button")
	private WebElement UploadCertificateOkBtn;

	@FindBy(id="sso_upload_file")
	private WebElement UploadFileSSO;

	@FindBy(id="cert_pswd_input")
	private WebElement PasswordSSO;

	@FindBy(xpath="//span[text()='Please select certificate to upload.']")
	private WebElement UploadCertificateErrorMessage;

	@FindBy(xpath="//div[@id='ssoCertificateAddPopup']/div/div/div[1]/button")
	private WebElement UploadCertificateCloseBtn;

	@FindBy(xpath="//div[a[@id='ssoLinkUrl']]")
	private WebElement LoginURLText;

	@FindBy(id="ssoLinkUrl")
	private WebElement SSOLoginURLBtn;

	@FindBy(id="sso_setng_done")
	private WebElement SSODoneBtn;

	@FindBy(xpath="//p[text()='Battery Policy']")
	private WebElement AlertTemplatetBatteyPolicy;

	@FindBy(xpath="//span[text()='Template Title']")
	private WebElement AlertTemplateTitle;

	@FindBy(xpath="//span[text()='Template']")
	private WebElement AlertTemplate_Template;

	@FindBy(xpath="//span[text()='Connection Policy']")
	private WebElement AlertTemplate_ConnectionPolicy;

	@FindBy(xpath=".//*[@id='EnableBatteryPolicy']")
	private WebElement TemplateTitleTextBox;

	@FindBy(xpath=".//*[@id='EnableBatteryPolicyTemp']")
	private WebElement TemplateTextBox;

	@FindBy(xpath="//p[text()='Connection Policy']")
	private WebElement AlertTemplatetConnectionPolicy;

	@FindBy(xpath=".//*[@id='EnableConnectionPolicy']")
	private WebElement ConnectionPolicyTemplateTitleTextBox;

	@FindBy(xpath=".//*[@id='EnableConnectionPolicyTemp']")
	private WebElement ConnectionPolicyTemplateTextBox;

	@FindBy(xpath="//p[text()='Data Usage Policy']")
	private WebElement AlertTemplatetDataUsagePolicy;

	@FindBy(xpath=".//*[@id='EnableDataUsagePolicy']")
	private WebElement DataUsagePolicyTemplateTitleTextBox;

	@FindBy(xpath=".//*[@id='EnableDataUsagePolicyTemp']")
	private WebElement DataUsagePolicyTemplateTextBox;

	@FindBy(xpath="//p[text()='Notify when device comes online']")
	private WebElement AlertTemplatetNotifyWhenDeviceComeOnline;

	@FindBy(xpath=".//*[@id='Notifywhendevicecomesonline']")
	private WebElement NotifyWhenDeviceComesOnlineTemplateTitleTextBox;

	@FindBy(xpath=".//*[@id='Notifywhendevicecomesonlinetemp']")
	private WebElement NotifyWhenDeviceComesOnlineTemplateTextBox;	

	@FindBy(xpath="//p[text()='Notify when SIM is changed']")
	private WebElement AlertTemplatetNotifyWhenSIMIsChanged;

	@FindBy(xpath=".//*[@id='NotifywhenSIMischanged']")
	private WebElement NotifyWhenSIMIsChangedTemplateTitleTextBox;

	@FindBy(xpath=".//*[@id='NotifywhenSIMischangedtemp']")
	private WebElement NotifyWhenSIMIsChangedTemplateTextBox;

	@FindBy(xpath="//p[text()='Notify when device is rooted or Nix has been granted with root permission']")
	private WebElement AlertTemplatetNotifyRootedNixGranted;

	@FindBy(xpath=".//*[@id='Notifywhendeviceisrooted']")
	private WebElement NotifyNixRootedGrantedTemplateTitleTextBox;

	@FindBy(xpath=".//*[@id='Notifywhendeviceisrootedtemp']")
	private WebElement NotifyNixRootedGrantedTemplateTextBox;

	@FindBy(xpath="//p[text()='Push Certificate']")
	private WebElement AlertTemplatet_iOSEnrollmentSettings;

	@FindBy(xpath="//span[text()='Download CSR']")
	private WebElement DownloadCSRText;

	@FindBy(xpath=".//*[@id='advancedSettings_pg']/section/div[1]/ul/li[8]/a")
	private WebElement iOSEnrollmentSettingsSection;

	@FindBy(xpath=".//*[@id='devEnrollSett_tab']/div/div[2]/div[2]/div/div/span[1]/span")
	private WebElement DownloadCSRSummaryText;

	@FindBy(xpath=".//*[@id='devEnrollSett_tab']/div/div[2]/div[2]/div/div/span[1]/span/a")
	private WebElement DownloadCSRLink;

	@FindBy(id="csrdowload_iOS_advnstng")
	private WebElement ClickOnDownloadButton_DownloadCSR;

	@FindBy(xpath="//span[text()='Certificate']")
	private WebElement Certificate;

	@FindBy(xpath=" //span[text()='Upload Push certificate (.pem) provided by apple.']")
	private WebElement CertificateSummaryText;

	@FindBy(xpath=".//*[@id='certificate_pem_input']")
	private WebElement CertificateAttributeText;

	@FindBy(xpath="//p[text()='Device Enrolment Program']")
	private WebElement iOSEnrollmentSettings_DeviceEnrolmentProgramHeader;

	@FindBy(xpath="//span[text()='PEM Certificate']")
	private WebElement PEMCertificateHeader;

	@FindBy(xpath=".//*[@id='devEnrollSett_tab']/div/div[2]/div[2]/div/div/span[1]/span")
	private WebElement PEMCertificateSummaryText;

	@FindBy(id="download_dep_pem_advnstng")
	private WebElement ClickOnDownloadButton_PEMCertificate;

	@FindBy(xpath=".//*[@id='devEnrollSett_tab']/div/div[2]/div[2]/div/div/span[1]/span/a")
	private WebElement AppleDeploymentProgramsLink;

	@FindBy(xpath="//span[text()='Server Token']")
	private WebElement ServerTokenHeader;

	@FindBy(xpath="//span[text()='DEP Profile']")
	private WebElement DEPProfileHeader;

	@FindBy(xpath=".//span[text()='Download the SMIME server token from deploy.apple.com and upload here.']")
	private WebElement ServerTokenSummaryText;

	@FindBy(xpath=".//span[text()='Configure DEP profile to be pushed to all devices under DEP.']")
	private WebElement DEPProfileSummary;

	@FindBy(id="dep_profile_edit_advncstng")
	private WebElement DEPProfileConfigureButton;

	@FindBy(xpath="//h4[text()='DEP Profile']")
	private WebElement DEPProfileConfigureWindowHeader;

	@FindBy(xpath="//span[text()='Profile Name']")
	private WebElement ProfileName;

	@FindBy(xpath=".//*[@id='dep_profile_popup']/div/div/div[2]/div[2]/div[1]/span[1]")
	private WebElement SuperviseDevice;

	@FindBy(xpath=".//*[@id='dep_profile_popup']/div/div/div[2]/div[2]/div[2]/span[1]")
	private WebElement IsMandatory;

	@FindBy(xpath=".//*[@id='dep_profile_popup']/div/div/div[2]/div[2]/div[3]/span[1]")
	private WebElement IsMDMProfileRemovable;

	@FindBy(xpath="//span[text()='Support Phone Number']")
	private WebElement SupportPhoneNumber;

	@FindBy(xpath="//span[text()='Support Email Address']")
	private WebElement SupportEmailAddress;

	@FindBy(xpath="//span[text()='Skip Passcode']")
	private WebElement SkipPasscode;

	@FindBy(xpath="//span[text()='Skip Restore']")
	private WebElement SkipRestore;

	@FindBy(xpath="//span[text()='Skip TOS']")
	private WebElement SkipTOS;

	@FindBy(xpath="//span[text()='Skip Payment']")
	private WebElement SkipPayment;

	@FindBy(xpath="//span[text()='Skip Location']")
	private WebElement SkipLocation;

	@FindBy(xpath="//span[text()='Skip Apple ID']")
	private WebElement SkipAppleID;

	@FindBy(xpath="//span[text()='Skip Biometric']")
	private WebElement SkipBiometric;

	@FindBy(xpath="//span[text()='Skip Siri']")
	private WebElement SkipSiri;

	@FindBy(id="support_phone_number")
	private WebElement supportPhnNumber;

	@FindBy(id="support_email_address")
	private WebElement supportEmailAddress;

	@FindBy(id="skip_Passcode")
	private WebElement skipPasscodeCheckBox;

	@FindBy(id="skip_Restore")
	private WebElement skipRestoreCheckBox;

	@FindBy(id="skip_Biometric")
	private WebElement skipBiometricCheckBox;

	@FindBy(xpath=".//*[@id='dep_profile_popup']/div/div/div[3]/button[1]")
	private WebElement CancelDepProfileWindow;

	@FindBy(xpath=".//*[@id='dep_profile_popup']/div/div/div[3]/button[2]")
	private WebElement SaveDepProfileWindow;

	@FindBy(xpath="//span[text()='Profile updated successfully.']")
	private WebElement WarningMessageOnSavingDEPProfile;

	@FindBy(xpath="//span[text()='Push DEP Profile']")
	private WebElement PushDEPProfileHeader;

	@FindBy(xpath=".//span[text()='Push the configured DEP profile to all devices under DEP.']")
	private WebElement PushDEPProfileSummary;

	@FindBy(id="dep_apply_Profileadvncstng")
	private WebElement Push;

	@FindBy(xpath="//span[text()='DEP Account not configured.']")
	private WebElement WarningMessageOnPush;

	
	
	

	public void ClickOnAccountSettings() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, AccountSettings);
		AccountSettings.click();
		sleep(4);
		waitForXpathPresent(XpathforPageToLoadAccountSettings);
		Reporter.log("PASS >> Click on Account Settings", true);
		sleep(4);
	}

	@FindBy(xpath="//*[@id='device_migration_rules_tab']/a")
	private WebElement GroupAssigmentRulesButton;  

	public void ClickOnGroupAssigmentRule() throws InterruptedException {
		GroupAssigmentRulesButton.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[2]/button[text()='Destination group']")
	private WebElement DestinationgroupOption;

	@FindBy(xpath="//*[@id='groupListModal']/div/div/div[1]/h4[text()='Group List']")
	private WebElement DestinationGroupListPopup;

	@FindBy(xpath="//*[@id='device_migration_rules_content']/div[1]/div[2]/button[text()='Add Rules']")
	private WebElement AddRulesButton;

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[3]/span[1]")
	private WebElement  IncludeSubgroupsOption;

	@FindBy(xpath="//span[text()='Add Row']")
	private WebElement AddRowOption;

	@FindBy(xpath="//span[text()='Enable Rule']")
	private WebElement EnableRuleButton;

	@FindBy(xpath="//span[text()='Enable Group Assignment Rule']")
	private WebElement EnableGroupAssignmentRulesOption;

	public void GroupAssignmentRules_OptionsAvailability() {

		boolean GroupAssignment= EnableGroupAssignmentRulesOption.isDisplayed();
		String pass = "PASS >> Option EnableGroupAssignmentRulesOption is present";
		String fail= "FAIL >> Option EnableGroupAssignmentRulesOption is not present";
		ALib.AssertTrueMethod(GroupAssignment, pass, fail);


		boolean AddRules= AddRulesButton.isDisplayed();
		String pass1 = "PASS >> Option AddRulesButton  is present";
		String fail1= "FAIL >> Option AddRulesButton is not present";
		ALib.AssertTrueMethod(AddRules, pass1, fail1);


		boolean EnableRule= EnableRuleButton.isDisplayed();
		String pass2 = "PASS >> Option EnableRuleButton  is present";
		String fail2= "FAIL >> Option EnableRuleButton is not present";
		ALib.AssertTrueMethod(EnableRule, pass2, fail2);



		boolean IncludeSubgroups= IncludeSubgroupsOption.isDisplayed();
		String pass3 = "PASS >> Option IncludeSubgroupsOption  is present";
		String fail3= "FAIL >> Option IncludeSubgroupsOption is not present";
		ALib.AssertTrueMethod(IncludeSubgroups, pass3, fail3);


		boolean Sourcegroup= SourcegroupOption.isDisplayed();
		String pass4 = "PASS >> Option SourcegroupOption  is present";
		String fail4= "FAIL >> Option SourcegroupOption is not present";
		ALib.AssertTrueMethod(Sourcegroup, pass4, fail4);


		boolean AddRow= AddRowOption.isDisplayed();
		String pass5 = "PASS >> Option AddRowOption  is present";
		String fail5= "FAIL >> Option AddRowOption is not present";
		ALib.AssertTrueMethod(AddRow, pass5, fail5);


		boolean Destinationgroup= DestinationgroupOption.isDisplayed();
		String pass6 = "PASS >> Option DestinationgroupOption  is present";
		String fail6= "FAIL >> Option DestinationgroupOption is not present";
		ALib.AssertTrueMethod(Destinationgroup, pass6, fail6);

	}

	@FindBy(xpath="//*[@id='enable_migration']")
	private WebElement EnableGrpAssignmentRules;

	@FindBy(xpath="//*[@id='enable_migration']")
	private WebElement DisableGroupAssignment;

	public void VerifyDisablingGroupAssignmentRuleCheckBox() throws InterruptedException
	{
		DisableGroupAssignment.click();
		sleep(2);
	}

	@FindBy(xpath="//span[text()='Rule(s) updated successfully.']")
	private WebElement SuccessfulMessage;

	public void RulesUpdatedMessage() throws InterruptedException
	{
		String Successful = SuccessfulMessage.getText();
		String Expected = "Rule(s) updated successfully.";
		String pass = "PASS >> Successful Message is displayed: " +SuccessfulMessage;
		String fail = "FAIL >> Successful Message is not displayed";
		ALib.AssertEqualsMethod(Expected, Successful, pass, fail);
		sleep(600); // 10 Min Wait Time for migration of the device
	}

	@FindBy(xpath="//*[@id='tableContainer']/div[7]/div/div")
	private WebElement GridMessageDispalyedWhenNoDevice;

	public void VerifyDevicesshouldnotMigrate()
	{
		String ErrorMessage = GridMessageDispalyedWhenNoDevice.getText();
		String Expected = "No device available in this group.";
		String pass = "PASS >> Grid Message is displayed: " +ErrorMessage;
		String fail = "FAIL >> Grid Message is not displayed";
		ALib.AssertEqualsMethod(Expected, ErrorMessage, pass, fail);

	}
	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div/div[2]/input")
	private WebElement RuleNameOptionTextBox;
	public void RulesNameTextBox_Enter() throws InterruptedException {
		RuleNameOptionTextBox.clear();
		sleep(2);
		RuleNameOptionTextBox.sendKeys(Config.RuleName);
		sleep(2);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[3]/button[text()='Source group']")
	private WebElement SourcegroupOption;

	public void Click_SourceGroup() throws InterruptedException {
		SourcegroupOption.click();
		sleep(2);
	}

	@FindBy(xpath="//span[text()='Only PNG image file is allowed.']")
	private WebElement WarningMessageOneInvalidAppLauncherIcon;
	public void WarningMessageWhenJPGfileSelectedAsAppLauncherIcon() throws InterruptedException
	{

		boolean isdisplayed=true;

		try{
			WarningMessageOneInvalidAppLauncherIcon.isDisplayed();

		}catch(Exception e)
		{
			isdisplayed=false;

		}
		Assert.assertTrue(isdisplayed,"FAIL >> : Receiving of notification failed");
		Reporter.log("PASS >> Alert Message : 'Only PNG image file is allowed.' is displayed",true );		
		sleep(2);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div/select[1]")
	private WebElement DropDownSelect;
	public void SelectGlobalIPDropDown() {
		Select  DropIPAddress= new Select(DropDownSelect);
		DropIPAddress.selectByIndex(1);
		Reporter.log("Global Ip Address is displayed",true);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div/input[1]")
	private WebElement EnterLocalIPAddressRangeValue1;
	public void EnterGlobalIPAddress_RangeValue1() throws InterruptedException {
		EnterLocalIPAddressRangeValue1.clear();
		sleep(2);
		EnterLocalIPAddressRangeValue1.sendKeys(Config.GlobalIp1);
		sleep(2);
	}

	public void ClearRulesNameTextBox() throws InterruptedException {
		RuleNameOptionTextBox.clear();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='ConfirmationDialogue']/div/div/div[2]/button[2][text()='OK']")
	private WebElement OKBtn;

	@FindBy(xpath="//span[text()='DEP Profile deleted successfully.']")
	private WebElement DeleteSuccessMessage;

	@FindBy(xpath="//*[text()='Are you sure you want to delete this Assignment Rule?']")
	private WebElement DeleteMessage;

	public void ClickDestinationGroup() throws InterruptedException {
		DestinationgroupOption.click();
		sleep(5);
	}

	@FindBy(xpath="//*[@id='groupList-searchCont']/input")
	private WebElement SearchDestinationGroup;
	public void SearchDestinationGroup() throws InterruptedException
	{
		SearchDestinationGroup.sendKeys(Config.DestinationGroupName);
		sleep(5);
	}

	@FindBy(xpath="//*[@id='groupList']/ul/li[text()='"+Config.DestinationGroupName+"']")
	private WebElement DestinationGrpSelect;

	public void selectDestinationGroup() throws InterruptedException
	{
		DestinationGrpSelect.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div[2]/select[3]")
	private WebElement RangeDropDownList;

	public void SelectRangeDropDownList() {
		Select  DropIPAddress= new Select(RangeDropDownList);
		DropIPAddress.selectByIndex(2);
		Reporter.log("Range is displayed",true);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div[2]/input[2]")
	private WebElement InputRule2;

	public void EnterLocalAddressValue1() throws InterruptedException {
		InputRule1.clear();
		sleep(2);
		InputRule1.sendKeys(Config.LocalIp1);
		sleep(2);
	}

	@FindBy(xpath="//*[@id='customMenu']/li[4]")
	private WebElement RightClickMoveToGroup;

	@FindBy(xpath="//*[@id='groupList-searchCont']/input")
	private WebElement SearchByGroupName;

	@FindBy(xpath=".//*[@id='groupList']/ul/li[text()='@SourceDonotTouch']")
	private WebElement  SourceGroupInDeviceGrid;

	@FindBy(xpath=".//*[@id='groupList']/ul/li[text()='@SubGroupDonotTouch']")
	private WebElement  SubGroupInDeviceGrid;

//	@FindBy(xpath="//*[@id='groupListModal']/div/div/div[3]/button")
	@FindBy(xpath="(//button[text()='Move'])[1]")

	private WebElement MoveButtonInsideMoveToGroup;

	public void MoveToGroupMultiple() throws InterruptedException {
		RightClickMoveToGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(4);
		SearchByGroupName.sendKeys(Config.SourceGroupName);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='@SourceDonotTouch']");
		sleep(4);
		SourceGroupInDeviceGrid.click();
		sleep(5);
		MoveButtonInsideMoveToGroup.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(10);

	}

	public void RightClickOndeviceInsideGroups(String name) throws InterruptedException {
		Actions action=new Actions(Initialization.driver);
		JavascriptExecutor js1=(JavascriptExecutor)Initialization.driver;
		action.contextClick(Initialization.driver.findElement(By.xpath("//p[text()='"+Config.DeviceName+"']"))).perform();
		sleep(2);
	}

	public void MoveToGroupMultipleSubGroup() throws InterruptedException {
		MovetoGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(4);
		SearchByGroupName.sendKeys(Config.SubGroupDevice);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='"+Config.SubGroupDevice+"']");		
		sleep(4);
		SubGroupInDeviceGrid.click();
		sleep(5);
		MoveButtonInsideMoveToGroup.click();
	    waitForXpathPresent("//p[contains(text(),'Are you sure you want to move the device(s) to Group "+Config.SubGroupDevice+" ?')]");
	    YesBtnMoveTogroup.click();
	    waitForXpathPresent("//span[text()='Device moved successfully to Group "+Config.SubGroupDevice+"']");
         sleep(2);
		waitForidPresent("deleteDeviceBtn");
		sleep(10);
	}

	@FindBy(xpath="//*[@id='gridMenu']/li[5]/span[text()='Move to Group']")
	private WebElement MovetoGroup;
	@FindBy(xpath="//*[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement YesBtnMoveTogroup;
	public void MoveToGroupMultiple1() throws InterruptedException {
		MovetoGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(4);
		SearchByGroupName.sendKeys(Config.SourceGroupName);
	   waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='"+Config.SourceGroupName+"']");
		sleep(4);
		SourceGroupInDeviceGrid.click();
	    sleep(5);
	    MoveButtonInsideMoveToGroup.click();
	    waitForXpathPresent("//p[contains(text(),'Are you sure you want to move the device(s) to Group "+Config.SourceGroupName+" ?')]");
	    YesBtnMoveTogroup.click();
	    waitForXpathPresent("//span[text()='Device moved successfully to Group "+Config.SourceGroupName+"']");
        sleep(2);
        waitForidPresent("deleteDeviceBtn");
		sleep(10);
	}
/*	public void MoveToGroupMultiple1() throws InterruptedException {
		MovetoGroup.click();
		waitForXpathPresent("(//h4[.='Group List'])[2]");
		sleep(4);
		SearchByGroupName.sendKeys(Config.SourceGroupName);
		waitForXpathPresent(".//*[@id='groupList']/ul/li[text()='@SourceDonotTouch']");
		sleep(4);
		SourceGroupInDeviceGrid.click();
		sleep(5);
		MoveButtonInsideMoveToGroup.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(10);
	}
*/
	
	public void SelectMultipleDevice() throws AWTException {
		Robot rb = new Robot();
		rb.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
		rb.keyPress(java.awt.event.KeyEvent.VK_A);
	}

	@FindBy(xpath="//*[@id='groupJobQ_modal']/div[1]/button")
	private WebElement CloseBtnCompletedJobSectionRtClick;

	public void RightClickOndeviceInsideGroups1(String name) throws InterruptedException {
		Actions action=new Actions(Initialization.driver);
		JavascriptExecutor js1=(JavascriptExecutor)Initialization.driver;
		action.contextClick(Initialization.driver.findElement(By.xpath("//*[@id='groupstree']/ul/li[contains(text(),'"+Config.SourceGroupName+"')]"))).perform();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='groupCheckList']/ul/li[text()='"+Config.SourceGroupName+"']")
	private WebElement ChooseSourceGroup;


	@FindBy(xpath="//*[@id='groupCheckListModal']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButton;
	public void SelectTheGroup() throws InterruptedException
	{
		ChooseSourceGroup.click();
		sleep(2);
	}

	public void ClickAddButton_OfGroupList() throws InterruptedException {
		AddButton.click();
		sleep(2);
	}

	@FindBy(xpath="//p[text()='"+Config.Device1+"']")
	private WebElement Device1;
	public void VerifyDeviceMigratedFromSource_Destination() throws InterruptedException 
	{
		String DeviceName1=Device1.getText();
		String Expected = Config.Device1;
		String pasS= "PASS >> Device is migrated: "+DeviceName1;
		String faiL= "PASS >> Device is not migrated";
		ALib.AssertEqualsMethod(Expected, DeviceName1, pasS, faiL);

		String DeviceName2=Device2.getText();
		String Expected2 = Config.Device2;
		String pasS1= "PASS >> Device is migrated: "+DeviceName2;
		String faiL1= "PASS >> Device is not migrated";
		ALib.AssertEqualsMethod(Expected2, DeviceName2, pasS1, faiL1);

	}

	public void SelectGlobalIPAddressDropDown() {
		Select  DropIPAddress= new Select(IPDropDownList);
		DropIPAddress.selectByIndex(1);
		Reporter.log("GlobalIP is displayed",true);
	}

	public void EnterGlobalIPAddressValue1() throws InterruptedException {
		InputRule1.clear();
		sleep(2);
		InputRule1.sendKeys(Config.GlobalIp1);
		sleep(2);
	}

	public void EnterGlobalIPAddressValue2() throws InterruptedException {
		InputRule2.clear();
		sleep(2);
		InputRule2.sendKeys(Config.GlobalIp2);
		sleep(2);
	}

	public void SelectDeviceModelDropDown() {
		Select  DropIPAddress= new Select(DropDownSelect);
		DropIPAddress.selectByIndex(2);
		Reporter.log("DeviceModel is displayed",true);
	}

	public void EnterDeviceModelValue() throws InterruptedException {
		EnterLocalIPAddressRangeValue1.clear();
		sleep(2);
		EnterLocalIPAddressRangeValue1.sendKeys(Config.DeviceModel1);
		sleep(4);
	}

	public void SearchSubGroups() throws InterruptedException {
		SearchGroup.sendKeys(Config.SourceSubGroupName);
		sleep(2);
	}

	@FindBy(xpath="//*[@id='groupCheckList']/ul/li[7]")
	private WebElement SubGroup;

	public void SelectSourceGroupsAndSubGroups() throws InterruptedException {
		SubGroup.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='tableContainer']/div[4]/div[1]/div[1]/button")
	private WebElement RefreshButtonDeviceGrid;

	public void GridRefresh() throws InterruptedException {
		RefreshButtonDeviceGrid.click();
		sleep(4);
	}

	@FindBy(xpath="//*[@id='homeSection']/a")
	private WebElement HomeSection;

	public void HomeSectionClick() throws InterruptedException {
		HomeSection.click();
		waitForXpathPresent("//*[@id='tableContainer']/div[4]/div[1]/div[3]/input");//SearchTextBox xpath
		sleep(5);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[3]/span[2]/input")
	private WebElement EnableIncludeSubGroups;
	public void VerifyEnablingSubgroups() throws InterruptedException
	{
		EnableIncludeSubGroups.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div/select[2]")
	private WebElement RangeDropDown;
	public void SelectDropdown() {
		Select  DropIPAddress= new Select(RangeDropDown);
		DropIPAddress.selectByIndex(1);
		Reporter.log("In(value1,value2) is displayed",true);
	}

	public void SelectoperatorDropDownOR() {
		Select  DropIPAddress= new Select(AndDropDown);
		DropIPAddress.selectByIndex(1);
		Reporter.log("OR Operator is displayed",true);
	}

	public void EnterLocalIPAddress_RangeValue2() throws InterruptedException {
		EnterLocalIPAddressRangeValue2.clear();
		sleep(2);
		EnterLocalIPAddressRangeValue2.sendKeys(Config.LocalIp2);
		sleep(2);
	}

	public void SelectLocalIPDropDown() {
		Select  DropIPAddress= new Select(DropDownSelect);
		DropIPAddress.selectByIndex(0);
		Reporter.log("Local Ip Address is displayed",true);
	}

	public void EnterLocalIPAddress_RangeValue1() throws InterruptedException {
		EnterLocalIPAddressRangeValue1.clear();
		sleep(2);
		EnterLocalIPAddressRangeValue1.sendKeys(Config.LocalIp1);
		sleep(2);
	}

	public void SelectRangeDropdown() {
		Select  DropIPAddress= new Select(RangeDropDown);
		DropIPAddress.selectByIndex(2);
		Reporter.log("Range is displayed",true);
	}

	@FindBy(xpath="//*[@id='groupCheckListModal']/div/div/div[1]/h4[text()='Group List']")
	private WebElement SourceGroupListPopupWindow;

	public void  verifyListofGroups_Opened() {
		boolean GroupListPopup=SourceGroupListPopupWindow.isDisplayed();
		String pass="PASS >> GroupList Popup is Displayed";
		String fail="FAIL >> GroupList Popup is not Displayed";
		ALib.AssertTrueMethod(GroupListPopup, pass, fail);
	}

	@FindBy(xpath="//li[@class='list-group-item node-groupCheckList node-selected']")
	private WebElement DeselectSourceGroups;

	@FindBy(xpath="//*[@id='groupCheckList-searchCont']/input")
	private WebElement SearchGroup;
	public void Deselect_SelectedSourceGroups() throws InterruptedException
	{
		try
		{
			if (DeselectSourceGroups.isDisplayed())

			{

				DeselectSourceGroups.click();
			}

		}
		catch(Exception e)
		{
			SearchGroup.sendKeys(Config.SourceGroupName);
		}
	}
	@FindBy(xpath="//p[text()='"+Config.Device2+"']")
	private WebElement Device2;
	public void RightClickOndevice(String name) throws InterruptedException {
		Actions action=new Actions(Initialization.driver);
		JavascriptExecutor js=(JavascriptExecutor)Initialization.driver;
		action.contextClick(Initialization.driver.findElement(By.xpath("//p[text()='"+Config.Device2+"']"))).perform();
		sleep(2);
	}
	public void  verifyListofDestinationGroups() {
		boolean GroupListPopup1=DestinationGroupListPopup.isDisplayed();
		String pass1="PASS >> GroupList Popup for Destination Group is Displayed";
		String fail1="FAIL >> GroupList Popup for Destination Group is not Displayed";
		ALib.AssertTrueMethod(GroupListPopup1, pass1, fail1);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div[2]/input[1]")
	private WebElement InputRule1;

	public void EnterLocalAddressValue2() throws InterruptedException {
		InputRule2.clear();
		sleep(2);
		InputRule2.sendKeys(Config.LocalIp2);
		sleep(2);
	}
	@FindBy(xpath="//*[@id='groupListModal']/div/div/div[3]/button[text()='Add']")
	private WebElement AddButtonDestinationGroup;

	public void AddButtonDestinationGroup() throws InterruptedException {
		AddButtonDestinationGroup.click();
		sleep(4);
	}
	public void ClickOkBtn() throws InterruptedException
	{
		OKBtn.click();
		waitForXpathPresent("//span[text()='Rule deleted successfully.']");
		sleep(4);
	}

	@FindBy(xpath="//*[@id='ConfirmationDialogue']/div/div/div[2]/button[1][text()='Cancel']")
	private WebElement CancelBtn;
	public void VerifyDeletingAddedRule() {
		String MessagePrompt=DeleteMessage.getText();
		Reporter.log("Are you sure you want to delete this Assignment Rule?",true);
		boolean value5= DeleteMessage.isDisplayed();
		String pass5= "PASS >> 'Are you sure you want to delete this Assignment Rule?'is displayed";
		String fail5= "FAIL >> 'Are you sure you want to delete this Assignment Rule?'is not displayed";
		ALib.AssertTrueMethod(value5, pass5, fail5);



		String CancelButton=CancelBtn.getText();
		Reporter.log("Cancel",true);
		boolean value6= CancelBtn.isDisplayed();
		String pass6= "PASS >> 'CancelBtn'is displayed";
		String fail6= "FAIL >> 'CancelBtn'is not displayed";
		ALib.AssertTrueMethod(value6, pass6, fail6);


		String OkBtn=OKBtn.getText();
		Reporter.log("OK",true);
		boolean value7= OKBtn.isDisplayed();
		String pass7= "PASS >> 'OK'is displayed";
		String fail7= "FAIL >> 'OK'is not displayed";
		ALib.AssertTrueMethod(value7, pass7, fail7);

	}
	@FindBy(xpath="//*[@id='appen_migrationRules']/div[2]/div[1]/div[1]/span/i")
	private WebElement DeleteRule;

	public void DeleteRuleClick() throws InterruptedException
	{
		DeleteRule.click();
		waitForXpathPresent("//*[text()='Are you sure you want to delete this Assignment Rule?']");
		sleep(2);
	}
	@FindBy(xpath="//span[text()='Rule name cannot be empty.']")
	private WebElement warningMessage;

	public void verifyErrorMessageDisplayed()
	{
		String ErrorMessage = warningMessage.getText();
		String Expected = "Rule name cannot be empty.";
		String pass = "PASS >> Warning is displayed: " +ErrorMessage;
		String fail = "FAIL >> Warning is not displayed";
		ALib.AssertEqualsMethod(Expected, ErrorMessage, pass, fail);
	}
	@FindBy(xpath="//*[@id='device_migration_rules_content']/div[1]/div[2]/button[text()='Add Rules']")
	private WebElement AddRules;

	public void ClickOnAddRules() throws InterruptedException {
		AddRules.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div/input[2]")
	private WebElement EnterLocalIPAddressRangeValue2;
	public void EnterGlobalIPAddress_RangeValue2() throws InterruptedException {
		EnterLocalIPAddressRangeValue2.clear();
		sleep(2);
		EnterLocalIPAddressRangeValue2.sendKeys(Config.GlobalIp2);
		sleep(2);
	}

	@FindBy(xpath="//span[text()='Add Row']")
	private WebElement AddRows;

	public void ClickOnAddRows() throws InterruptedException
	{
		AddRows.click();
		sleep(2);
	}

	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div[2]/select[1]")
	private WebElement AndDropDown;

	public void SelectoperatorDropDownAND() {
		Select  DropIPAddress= new Select(AndDropDown);
		DropIPAddress.selectByIndex(0);
		Reporter.log("and Operator is displayed",true);


	}
	@FindBy(xpath="//*[@id='appen_migrationRules']/div/div[1]/div[4]/div[2]/select[2]")
	private WebElement IPDropDownList;

	public void SelectIPAddressDropDown() {
		Select  DropIPAddress= new Select(IPDropDownList);
		DropIPAddress.selectByIndex(0);
		Reporter.log("LocalIP is displayed",true);
	}

	@FindBy(xpath="//*[@id='groupstree']/ul/li[text()='"+Config.DestinationGroupName+"']")
	private WebElement DestinationGroup;
	public void ClickOnDestinationGroup() throws InterruptedException {
		DestinationGroup.click();
		waitForidPresent("deleteDeviceBtn");
		sleep(8);
	}
	@FindBy(xpath="//*[@id='device_migrationrules_apply'][text()='Apply']")
	private WebElement ApplyButton;

	public void ClickOnApplyButton() throws InterruptedException {
		ApplyButton.click();
		waitForXpathPresent("//span[text()='Rule(s) updated successfully.']");
		sleep(2);
	}
	public void EnableGroupAssignmentRules() throws InterruptedException {
		boolean flag= EnableGrpAssignmentRules.isSelected();
		System.out.println(flag);
		if(flag==false) 
		{
			EnableGrpAssignmentRules.click();
		}

	}
	public void VerifyOfAccountSettingsPage(){
		String ActualValue=PageOfAccountSettings.getText();
		String ExpectedValue="Account Settings";
		String PassStatement="PASS >> Navigated to 'Account Settings' page";
		String FailStatement="FAIL >> Navigation to 'Account Settings' failed";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

	}

	public void BrandingInfoSectionVisible(){
		boolean value=BrandingInfoSection.isDisplayed();
		String PassStatement="PASS >> Branding Info section is displayed when Account Settings is clicked";
		String FailStatement="FAIL >> Branding Info section is not displayed when Account Settings is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void DeviceEnrollmentSectionVisible(){
		boolean value=DeviceEnrollmentSection.isDisplayed();
		String PassStatement="PASS >> Device Enrollment rules section is displayed when Account Settings is clicked";
		String FailStatement="FAIL >> Branding Info section is not displayed when Account Settings is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void MiscellaneousSettingsSectionVisible(){
		boolean value=MiscellaneousSettingsSection.isDisplayed();
		String PassStatement="PASS >> Miscellaneous Settings section is displayed when Account Settings is clicked";
		String FailStatement="FAIL >> Miscellaneous Settings section is not displayed when Account Settings is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void SSOSectionVisible(){
		boolean value=SSOSection.isDisplayed();
		String PassStatement="PASS >> SSO section is displayed when Account Settings is clicked";
		String FailStatement="FAIL >> SSO section is not displayed when Account Settings is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void AlertTemplateSectionVisible(){
		boolean value=AlertTemplateSection.isDisplayed();
		String PassStatement="PASS >> Alert Template section is displayed when Account Settings is clicked";
		String FailStatement="FAIL >> Alert Template section is not displayed when Account Settings is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void CustomizeToolbarSectionVisible(){
		boolean value=CustomizeToolbarSection.isDisplayed();
		String PassStatement="PASS >> Customize Toolbar section is displayed when Account Settings is clicked";
		String FailStatement="FAIL >> Customize Toolbar section is not displayed when Account Settings is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void iOSEnrolmentSettingsSectionVisible(){
		boolean value=iOSEnrolmentSettingsSection.isDisplayed();
		String PassStatement="PASS >> iOS Enrolment Settings section is displayed when Account Settings is clicked";
		String FailStatement="FAIL >> iOS Enrolment Settings section is not displayed when Account Settings is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void UseLogoIsSelected(boolean expectedvalue) throws InterruptedException{
		boolean value=UseLogo.isSelected();
		if(value==expectedvalue)
		{
			UseLogo.click();
			sleep(1);
		}
	}

	public void UseLogoCheckedOptionVisible(){
		String Actualvalue=EnableLogoPath.getAttribute("style");
		boolean value=Actualvalue.contains("none");
		String PassStatement="PASS >> Title and SubTitle option is displayed when UseLogo is Unchecked";
		String FailStatement="FAIL >> Title and SubTitle option is not displayed when UseLogo is Unchecked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void UseLogoMessageFooterOptionVisible(){
		boolean value=MessageFooterOption.isDisplayed();
		String PassStatement="PASS >> Message Footer option is displayed when UseLogo is Checked/Unchecked";
		String FailStatement="FAIL >> Message Footer option is not displayed when UseLogo is Checked/Unchecked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}


	public void UseLogoUnCheckedOptionVisible(){
		String Actualvalue=EnableTitleOption.getAttribute("style");
		boolean value=Actualvalue.contains("none");
		String PassStatement="PASS >> Logo Path option is displayed when UseLogo is Checked";
		String FailStatement="FAIL >> Logo Path option is not displayed when UseLogo is Checked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}


	public void EnterTitle() throws EncryptedDocumentException, InvalidFormatException, IOException{
		Title.sendKeys("SureMDM");
	}

	public void ClearTitle(){
		Title.clear();
	}

	public void VerifyHomePageTitle() throws EncryptedDocumentException, InvalidFormatException, IOException{
		String Actualvalue=HomePageTitle.getText();
		String Expectedvalue="SureMDM";
		String PassStatement="PASS >> Entered Title is displayed On Home Page";
		String FailStatement="FAIL >> Entered Title is not displayed On Home Page";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	}


	public void EnterSubTitle() throws EncryptedDocumentException, InvalidFormatException, IOException{
		SubTitle.sendKeys("SureMDM");
	}

	public void ClearSubTitle(){
		SubTitle.clear();
	}

	public void VerifyHomePageSubTitle() throws EncryptedDocumentException, InvalidFormatException, IOException{
		String Actualvalue=HomePageSubTitle.getText();
		String Expectedvalue="SureMDM";
		String PassStatement="PASS >> Entered SubTitle is displayed On Home Page";
		String FailStatement="FAIL >> Entered SubTitle is not displayed On Home Page";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	}

	public void EnterMessageFooter() throws EncryptedDocumentException, InvalidFormatException, IOException{
		MessageFooter.sendKeys("SureMDM");
	}

	public void ClearMessageFooter(){
		MessageFooter.clear();
	}

	public void ClickOnBrandingInfoApplyBtn(){
		BrandingInfoApplyBtn.click();
	}

	public void VerifyErrorMessageSubTitle() throws InterruptedException{
		sleep(4);
		boolean a = ErrorMessageSubTitle.isDisplayed();
		String PassStatement="PASS >> 'Sub-Title cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Sub-Title cannot be empty.' Message is not displayed";
		ALib.AssertTrueMethod(a, PassStatement, FailStatement);

	}
	public void VerifyErrorMessageTitle(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Title cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Title cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageTitle, value, PassStatement, FailStatement,3);
	}

	public void ConfirmationMessageVerify(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Settings updated successfully.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Settings updated successfully.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ConfirmationMessage, value, PassStatement, FailStatement,3);
	}

	public void BrowseLogoPath(String filePath) throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException{
		BrowseLogoFile.click();
		Runtime.getRuntime().exec(filePath);
		sleep(3);
	}

	public void VerifyErrorMessageSubTitle(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Sub-Title cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Sub-Title cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageSubTitle, value, PassStatement, FailStatement,3);
	}

	public void VerifyErrorMessageLogoPathFile(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Select image for logo.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Select image for logo.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageLogoPathFile, value, PassStatement, FailStatement,3);
	}

	public void VerifyErrorMessageLogoPathSize(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Logo image size should not be greater than 1MB.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Logo image size should not be greater than 1MB.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageLogoPathSize, value, PassStatement, FailStatement,3);
	}

	public void BrowseLogoPath(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException{
		BrowseLogoFile.click();
		sleep(3);
		Runtime.getRuntime().exec(ELib.getDatafromExcel("Sheet10", row, col));
		sleep(1);

	}

	public void VerifyOfLogoDisplyedOnHomePage(){
		String Actualvalue1=HomePageLogoPath.getAttribute("style");
		String Actualvalue2=HomePageLogoInfo.getAttribute("style");
		boolean value=Actualvalue1.contains("block") || Actualvalue2.contains("none");
		String PassStatement="PASS >> Logo Image is displayed On Home Page";
		String FailStatement="FAIL >> Logo Image is not displayed On Home Page";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void TestFailureBrandingInfoMthd1() throws InterruptedException {
		try {
			BrandingInfoSection.click();
		} catch (Exception e) {
			refesh();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();
			ClickOnAccountSettings();
		}
	}

	public void TestFailureBrandingInfoMthd2() throws InterruptedException {
		try {
			Initialization.commonmethdpage.ClickOnSettings();
		} catch (Exception e) {
			refesh();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();

		}
	}

	public void ClickOnDeviceEnrolment() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, DeviceEnrollmentSection);
		DeviceEnrollmentSection.click();
		waitForidPresent("name_prefix");
		sleep(2);
	}

	public void VerifyOfDeviceEnrollmentPage()
	{
		boolean value = DeviceEnrolmentDeviceNamingSection.isDisplayed();
		String PassStatement="PASS >> Device Naming Section is displayed when Device Enrollment Rules is clicked";
		String FailStatement="FAIL >> Device Naming Section is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = DeviceEnrolmentPrefixField.isDisplayed();
		PassStatement="PASS >> Prefix Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Prefix Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = DeviceEnrolmentSuffixField.isDisplayed();
		PassStatement="PASS >> Suffix Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Suffix Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = DeviceEnrolmentCountField.isDisplayed();
		PassStatement="PASS >> Count Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Count Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = DeviceEnrolmentCountLengthField.isDisplayed();
		PassStatement="PASS >> Count Length Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Count Length Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = DeviceEnrolmentEnrollmentAuthenticationSection.isDisplayed();
		PassStatement="PASS >> Enrollment Authentication Section is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Enrollmen tAuthentication Section is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = DeviceEnrolmentDeviceAuthenticationTypeField.isDisplayed();
		PassStatement="PASS >> Device Authentication Type Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Device Authentication Type Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void EnterPrefix() throws EncryptedDocumentException, InvalidFormatException, IOException{
		DeviceEnrolmentPrefix.sendKeys(ELib.getDatafromExcel("Sheet10",4, 1));
	}

	public void ClearPrefix(){
		DeviceEnrolmentPrefix.clear();
	}

	public void EnterSuffix() throws EncryptedDocumentException, InvalidFormatException, IOException{
		DeviceEnrolmentSuffix.sendKeys(ELib.getDatafromExcel("Sheet10",5, 1));
	}

	public void ClearSuffix(){
		DeviceEnrolmentSuffix.clear();
	}

	public void EnterCount() throws EncryptedDocumentException, InvalidFormatException, IOException{
		DeviceEnrolmentCount.sendKeys(ELib.getDatafromExcel("Sheet10",6, 1));
	}

	public void ClearCount(){
		DeviceEnrolmentCount.clear();
	}

	public void EnterCountLength() throws EncryptedDocumentException, InvalidFormatException, IOException{
		DeviceEnrolmentCountLength.sendKeys(ELib.getDatafromExcel("Sheet10",7, 1));
	}

	public void ClearCountLength(){
		DeviceEnrolmentCountLength.clear();
	}

	public void ClickDeviceEnrolmentAppyBtn() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, DeviceEnrolmentApplyBtn);
		DeviceEnrolmentApplyBtn.click();
		sleep(2);
	}

	public void VerifyOfEgforDeviceNaming() throws EncryptedDocumentException, InvalidFormatException, IOException{
		String ActualValue=DeviceEnrolmentCountLengthExample.getText();
		String ExpectedValue = ELib.getDatafromExcel("Sheet10", 4, 1);
		int count = Integer.parseInt(ELib.getDatafromExcel("Sheet10", 7, 1));
		for (int i = 0; i < count; i++) {
			if(i==count-1)
				ExpectedValue = ExpectedValue + ELib.getDatafromExcel("Sheet10", 6, 1);
			else
				ExpectedValue = ExpectedValue + "0";
		}
		String PassStatement ="PASS >> Example for Device Naming is displayed successfully as"+ActualValue;
		String FailStatement ="FAIL >> Example for Device Naming is not displayed";
		ExpectedValue = ExpectedValue +" "+ ELib.getDatafromExcel("Sheet10", 5, 1);
		boolean value=ExpectedValue.equals(ActualValue);
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void SelectDeviceAuthentication_NoAuthentication()
	{

		Select sel=new Select(DeviceAuthenticationType);
		sel.selectByValue("0");
	}

	public void SelectDeviceAuthentication_RequirePassword()
	{

		Select sel=new Select(DeviceAuthenticationType);
		sel.selectByValue("1");
	}

	public void SelectDeviceAuthentication_ADFS()
	{

		Select sel=new Select(DeviceAuthenticationType);
		sel.selectByValue("2");
	}

	public void SelectDeviceAuthentication_ADFSAdminAccount()
	{

		Select sel=new Select(DeviceAuthenticationType);
		sel.selectByValue("3");
	}

	public void VerifyErrorMessageForPassword(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Password cannot be less than 8 characters.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Password cannot be less than 8 characters.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageForPassword, value, PassStatement, FailStatement,3);
	}


	public void EnterPassword(int row,int col) throws EncryptedDocumentException, InvalidFormatException, IOException{
		EnterPasswordTextbox.sendKeys(ELib.getDatafromExcel("Sheet10",row,col));
	}

	public void ClearPassword(){
		DeviceEnrolmentCountLength.clear();
	}

	public void EnterOath(){
		OathFilterTextbox.sendKeys("sandjjd");
	}

	public void SelectShowPassword(){
		boolean value=ShowPasswordCheckbox.isSelected();
		if(value==false)
		{
			ShowPasswordCheckbox.click();
		}
		String ActualValue=EnterPasswordTextbox.getAttribute("type");
		String ExpectedValue="text";
		String PassStatement="PASS >> The password entered is successfully displayed when Show Password is selected";
		String FailStatement="FAIL >> The password entered is not displayed when Show Password is selected";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void UnSelectShowPassword(){
		boolean value=ShowPasswordCheckbox.isSelected();
		if(value==true)
		{
			ShowPasswordCheckbox.click();
		}
		String ActualValue=EnterPasswordTextbox.getAttribute("type");
		String ExpectedValue="password";
		String PassStatement="PASS >> The password entered is successfully hidden when Show Password is UnChecked";
		String FailStatement="FAIL >> The password entered is not Hidden when Show Password is UnChecked";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void TestFailureDeviceEnrollmentMthd() throws InterruptedException {
		try {
			DeviceEnrollmentSection.click();
		} catch (Exception e) {
			refesh();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();
			ClickOnAccountSettings();
			DeviceEnrollmentSection.click();
		}
	}

	public void VerifyErrorMessageAuthEndPoint(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Authentication endpoint cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Authentication endpoint cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageAuthEndPoint, value, PassStatement, FailStatement,3);
	}

	public void VerifyErrorMessageTokenEndPoint(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Token endpoint cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Token endpoint cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageTokenEndPoint, value, PassStatement, FailStatement,3);
	}

	public void VerifyErrorMessageActiveDirectoryServePath(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Active directory server path cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Active directory server path cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageActiveDirectoryServePath, value, PassStatement, FailStatement,3);
	}

	public void VerifyErrorMessageDomainFilter(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Domain filter cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Domain filter cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageDomainFilter, value, PassStatement, FailStatement,3);
	}

	public void VerifyErrorMessagedminUsername(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Admin username cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Admin username cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageAdminUsername, value, PassStatement, FailStatement,3);
	}

	public void VerifyErrorMessageAdminPassword(boolean value) throws InterruptedException{
		String PassStatement="PASS >> 'Admin password cannot be empty.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Admin password cannot be empty.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(ErrorMessageAdminPassword, value, PassStatement, FailStatement,3);
	}

	public void EnterAuthEndPoint() throws EncryptedDocumentException, InvalidFormatException, IOException{
		AuthEndpointTextbox.sendKeys(ELib.getDatafromExcel("Sheet10",1, 5));
	}

	public void ClearAuthEndPoint(){
		AuthEndpointTextbox.clear();
	}

	public void EnterTokenEndPoint() throws EncryptedDocumentException, InvalidFormatException, IOException{
		TokenEndpointTextbox.sendKeys(ELib.getDatafromExcel("Sheet10",2, 5));
	}

	public void ClearTokenEndPoint(){
		TokenEndpointTextbox.clear();
	}

	public void EnterActiveDirectoryServePath() throws EncryptedDocumentException, InvalidFormatException, IOException{
		ActiveDirectoryServePathTextbox.sendKeys(ELib.getDatafromExcel("Sheet10",5, 5));
	}

	public void ClearActiveDirectoryServePath(){
		ActiveDirectoryServePathTextbox.clear();
	}

	public void EnterDomainFilter() throws EncryptedDocumentException, InvalidFormatException, IOException{
		DomainFilterTextbox.sendKeys(ELib.getDatafromExcel("Sheet10",6, 5));
	}

	public void ClearDomainFilter(){
		DomainFilterTextbox.clear();
	}

	public void EnterAdminUsername() throws EncryptedDocumentException, InvalidFormatException, IOException{
		AdminUsernameTextbox.sendKeys(ELib.getDatafromExcel("Sheet10",7, 5));
	}

	public void ClearAdminUsername(){
		AdminUsernameTextbox.clear();
	}

	public void EnterAdminPassword() throws EncryptedDocumentException, InvalidFormatException, IOException{
		AdminPasswordTextbox.sendKeys(ELib.getDatafromExcel("Sheet10",8, 5));
	}

	public void ClearAdminPassword(){
		AdminPasswordTextbox.clear();
	}

	public void ClickOnMiscellaneousSettings() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, MiscellaneousSettingsSection);
		MiscellaneousSettingsSection.click();
		waitForidPresent("use_oldRemote");
		sleep(2);
	}

	public void VerifyOfMiscellaneousSettingsPage()
	{
		boolean value = MiscellaneousUseGCMField.isDisplayed();
		String PassStatement="PASS >> Use GCM Field is displayed when Device Enrollment Rules is clicked";
		String FailStatement="FAIL >> Use GCM Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = MiscellaneousDefaultConnectivityField.isDisplayed();
		PassStatement="PASS >> Default Connectivity Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Default Connectivity Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = MiscellaneousRemoteAccessField.isDisplayed();
		PassStatement="PASS >> Remote Access Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Remote Access Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = MiscellaneousRemoteSupportSection.isDisplayed();
		PassStatement="PASS >> Remote Support Section is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Remote Support Section is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = MiscellaneousUseOldRemoteSupportField.isDisplayed();
		PassStatement="PASS >> UseOldRemoteSupport Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> UseOldRemoteSupport Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = MiscellaneousPauseScreenCaptureField.isDisplayed();
		PassStatement="PASS >> Dont Pause Screen Capture Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Dont Pause Screen Capture Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		value = MiscellaneousZipDownloadsField.isDisplayed();
		PassStatement="PASS >> Zip All Downloads Field is displayed when Device Enrollment Rules is clicked";
		FailStatement="FAIL >> Zip All Downloads Field is not displayed when Device Enrollment Rules is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnMiscellaneousApplyBtn(){
		MiscellaneousApplyBtn.click();
	}

	public void SelectUseGCM(){
		boolean value=UseGCMCheckbox.isSelected();
		if(value==false)
		{
			UseGCMCheckbox.click();
		}
	}

	public void UnSelectUseGCM(){
		boolean value=UseGCMCheckbox.isSelected();
		if(value==true)
		{
			UseGCMCheckbox.click();
		}
	}

	public void VerifyUseGCMSelected(){
		boolean value=UseGCMCheckbox.isSelected();
		String PassStatement = "PASS >> Use GCM (checked) Settings is applied successfully";
		String FailStatement = "FAIL >> Use GCM (checked) Settings is not applied";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyUseGCMUnSelected(){
		boolean value=UseGCMCheckbox.isSelected();
		String PassStatement = "PASS >> Use GCM (Unchecked) Settings is applied successfully";
		String FailStatement = "FAIL >> Use GCM (Unchecked) Settings is not applied";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void SelectUseOldRemoteSupport(){
		boolean value=UseOldRemoteSupportCheckbox.isSelected();
		if(value==false)
		{
			UseOldRemoteSupportCheckbox.click();
		}
	}

	public void UnSelectUseOldRemoteSupport(){
		boolean value=UseOldRemoteSupportCheckbox.isSelected();
		if(value==true)
		{
			UseOldRemoteSupportCheckbox.click();
		}
	}

	public void DefaultConnectivity() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException{
			Select sel = new Select(DefaultconnectivityoptionCheckbox);
			sel.selectByVisibleText("Wi-Fi Only");
			ClickOnMiscellaneousApplyBtn();
			ConfirmationMessageVerify(true);
			Initialization.commonmethdpage.ClickOnHomePage();
			Initialization.commonmethdpage.ClickOnSettings();
			ClickOnAccountSettings();
			ClickOnMiscellaneousSettings();
			String ActualValue = DefaultconnectivityoptionCheckbox.getAttribute("value");
			String ExpectedValue = "0";
			String PassStatement = "PASS >>Default Connectivity Settings " + ExpectedValue+ " is applied successfully";
			String FailStatement = "FAIL >>Default Connectivity Settings " + ExpectedValue+ " is not applied";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
			sleep(2);
	}

	public void ClickOnGrantForRemoteAccess(){
		String result=RemoteAccessBtn.getAttribute("value");
		if(result.equals("Grant"))
		{
			RemoteAccessBtn.click();
		}
	}

	public void VerifyGrantForRemoteAccess() throws InterruptedException{
		sleep(5);
		String ActualResult=RemoteAccessBtn.getAttribute("value");
		String ExpectedResult="Revoke";
		String PassStatement="Provide Remote Access to 42Gears Support Team is Granted for the User";
		String FailStatement="Provide Remote Access to 42Gears Support Team is not Granted for the User";
		ALib.AssertEqualsMethod(ExpectedResult, ActualResult, PassStatement, FailStatement);
	}

	public void ClickOnRevokeForRemoteAccess() throws InterruptedException{
		String result=RemoteAccessBtn.getAttribute("value");
		if(result.equals("Revoke"))
		{
			sleep(5);
			RemoteAccessBtn.click();
			sleep(5);
		}
	}

	public void VerifyRevokeForRemoteAccess() throws InterruptedException{
		sleep(5);
		String ActualResult=RemoteAccessBtn.getAttribute("value");
		String ExpectedResult="Grant";
		String PassStatement="Provide Remote Access to 42Gears Support Team is Revoked for the User";
		String FailStatement="Provide Remote Access to 42Gears Support Team is not Revoked for the User";
		ALib.AssertEqualsMethod(ExpectedResult, ActualResult, PassStatement, FailStatement);
	}

	public void RemoteAccessGrantConfirmationMessage(boolean value) throws InterruptedException
	{
		String PassStatement="PASS >> 'Successfully granted remote access to 42Gears Support.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Successfully granted remote access to 42Gears Support.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(RemoteAccessGrantConfirmMessage, value, PassStatement, FailStatement,2);
	}

	public void RemoteAccessRevokeConfirmationMessage(boolean value) throws InterruptedException
	{
		String PassStatement="PASS >> 'Successfully revoked remote access from 42Gears Support.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Successfully revoked remote access from 42Gears Support.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(RemoteAccessRevokeConfirmMessage, value, PassStatement, FailStatement,2);
	}

	public void SelectPauseScreenCapture(){
		boolean value=DontPauseScreenCaptureCheckbox.isSelected();
		if(value==false)
		{
			DontPauseScreenCaptureCheckbox.click();
		}
	}

	public void UnSelectPauseScreenCapture(){
		boolean value=DontPauseScreenCaptureCheckbox.isSelected();
		if(value==true)
		{
			DontPauseScreenCaptureCheckbox.click();
		}
	}

	public void VerifyPauseScreenCaptureSelected(){
		boolean value=DontPauseScreenCaptureCheckbox.isSelected();
		String PassStatement = "PASS >> Don't Pause Screen Capture (checked) Settings is applied successfully";
		String FailStatement = "FAIL >> Don't Pause Screen Capture (checked) Settings is not applied";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyPauseScreenCaptureUnSelected(){
		boolean value=DontPauseScreenCaptureCheckbox.isSelected();
		String PassStatement = "PASS >> Don't Pause Screen Capture (Unchecked) Settings is applied successfully";
		String FailStatement = "FAIL >> Don't Pause Screen Capture (Unchecked) Settings is not applied";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void SelectZipAllDownloads(){
		boolean value=ZipAllDownloadsCheckbox.isSelected();
		if(value==false)
		{
			ZipAllDownloadsCheckbox.click();
		}
	}

	public void UnSelectZipAllDownloads(){
		boolean value=ZipAllDownloadsCheckbox.isSelected();
		if(value==true)
		{
			ZipAllDownloadsCheckbox.click();
		}
	}

	public void VerifyZipAllDownloadsSelected(){
		boolean value=ZipAllDownloadsCheckbox.isSelected();
		String PassStatement = "PASS >> Zip All Downloads (checked) Settings is applied successfully";
		String FailStatement = "FAIL >> Zip All Downloads (checked) Settings is not applied";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyZipAllDownloadsUnSelected(){
		boolean value=ZipAllDownloadsCheckbox.isSelected();
		String PassStatement = "PASS >> Zip All Downloads (Unchecked) Settings is applied successfully";
		String FailStatement = "FAIL >> Zip All Downloads (Unchecked) Settings is not applied";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void TestFailureMiscellaneousMthd() throws InterruptedException {
		try {
			ClickOnMiscellaneousSettings();
		} catch (Exception e) {
			refesh();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();
			ClickOnAccountSettings();
			ClickOnMiscellaneousSettings();
		}
	}

	public void CheckDownloadsZip() throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
	{
		String downloadPath = ELib.getDatafromExcel("Sheet1", 19, 1);
		String fileName = ELib.getDatafromExcel("Sheet1", 18, 1);
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		Initialization.remotesupportpage.DownLoadFile(fileName);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=result>initial;
		String PassStatement="PASS >> File is Downloaded successfully in zip Format";
		String FailStatement="Fail >> File is not Downloaded in zip Format";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void CheckDownloads() throws EncryptedDocumentException, InvalidFormatException, IOException, AWTException, InterruptedException
	{
		String downloadPath = ELib.getDatafromExcel("Sheet1", 19, 1);
		String fileName = ELib.getDatafromExcel("Sheet1", 18, 1);
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		Initialization.remotesupportpage.DownLoadFile(fileName);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=result<initial;
		String PassStatement="PASS >> File is Downloaded successfully";
		String FailStatement="Fail >> File is Downloaded in zip Format";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnSSOSettings() throws InterruptedException
	{
		SSOSection.click();
		waitForidPresent("ssoLinkUrl");
		sleep(2);
		Reporter.log("Clicking On SSO Settings , Navigates to SSO Settings Page",true);

	}

	public void VerifyOfEnableSSO()
	{
		boolean value=EnableSingleSignOnText.isDisplayed();
		String PassStatement="PASS >> 'Enable Single Sign-On' Checkbox is Displayed succesfully when Clicked on SSO Settings";
		String FailStatement="FAIL >> 'Enable Single Sign-On' Checkbox is not Displayed when Clicked on SSO Settings";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void EnableSSO()
	{
		boolean value = EnableSingleSignOnCheckbox.isSelected();
		if (value == false) {
			EnableSingleSignOnCheckbox.click();
		}
		Reporter.log("Enable Single Sign-On Checkbox is enabled", true);
	}

	public void DisableSSO() 
	{
		boolean value = EnableSingleSignOnCheckbox.isSelected();
		if (value == true) {
			EnableSingleSignOnCheckbox.click();
		}
		Reporter.log("Enable Single Sign-On Checkbox is disabled", true);
	}

	public void VerifyOfSSOType() {
		boolean value = SSOTypeText.isDisplayed();
		String PassStatement = "PASS >> 'SSO Type' Text is Displayed succesfully when Clicked on SSO Settings";
		String FailStatement = "FAIL >> 'SSO Type' Text is not Displayed when Clicked on SSO Settings";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Select sel = new Select(SSOTypeDropDown);
		String[] text = { "ADFS", "Azure AD", "Okta", "OneLogin", "PingOne" };
		List<WebElement> lst = sel.getOptions();
		for (int i = 0; i < lst.size(); i++) {
			String ActualValue = lst.get(i).getText();
			String ExpectedValue = text[i];
			PassStatement = "PASS >> " + ActualValue + " is displayed successfully in SSO Type Dropdown";
			FailStatement = "FAIL >> " + ActualValue + " is not successfully in SSO Type Dropdown";
			ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		}
	}

	public void VerifyOfServiceIdentifier() {
		boolean value = ServiceIdentifierText.isDisplayed();
		String PassStatement = "PASS >> 'Service Identifier' Text is Displayed succesfully when Clicked on SSO Settings";
		String FailStatement = "FAIL >> 'Service Identifier' Text is not Displayed when Clicked on SSO Settings";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String ActualValue=ServiceIdentifierText.getAttribute("class");
		value=ActualValue.contains("mandatory");
		PassStatement = "PASS >> Mandatory Sign is displayed successfully for 'Service Identifier'";
		FailStatement = "FAIL >> Mandatory Sign is not displayed for 'Service Identifier'";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfSignOnServiceUrl() {
		boolean value = SignOnServiceUrlText.isDisplayed();
		String PassStatement = "PASS >> 'Sign On Service Url' Text is Displayed succesfully when Clicked on SSO Settings";
		String FailStatement = "FAIL >> 'Sign On Service Url' Text is not Displayed when Clicked on SSO Settings";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String ActualValue=SignOnServiceUrlText.getAttribute("class");
		value=ActualValue.contains("mandatory");
		PassStatement = "PASS >> Mandatory Sign is displayed successfully for 'Sign On Service Url'";
		FailStatement = "FAIL >> Mandatory Sign is not displayed for 'Sign On Service Url'";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfLogoutServiceUrl() {
		boolean value = LogoutServiceUrlText.isDisplayed();
		String PassStatement = "PASS >> 'Logout Service Url' Text is Displayed succesfully when Clicked on SSO Settings";
		String FailStatement = "FAIL >> 'Logout Service Url' Text is not Displayed when Clicked on SSO Settings";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		String ActualValue=LogoutServiceUrlText.getAttribute("class");
		value=ActualValue.contains("mandatory");
		PassStatement = "PASS >> Mandatory Sign is displayed successfully for 'Logout Service Url'";
		FailStatement = "FAIL >> Mandatory Sign is not displayed for 'Logout Service Url'";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void SelectSSOType(String SSOType)
	{
		Select sel = new Select(SSOTypeDropDown);
		sel.selectByValue(SSOType);
	}

	public void VerifyOfEgForADFS() {
		String ActualValue=ServiceIdentifierEg.getText();
		String ExpectedValue="( e.g.: http://adfs.42gears.com/adfs/services/trust )";
		String PassStatement = "PASS >> 'Service Identifier Example' is Displayed succesfully when ADFS SSO Type";
		String FailStatement = "FAIL >> 'Service Identifier Example' is not Displayed when ADFS SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=SignOnServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://adfs.42gears.com/adfs/ls/ )";
		PassStatement = "PASS >> 'Sign On Service Url Example' is Displayed succesfully when ADFS SSO Type";
		FailStatement = "FAIL >> 'Sign On Service Url Example' is not Displayed when ADFS SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=LogoutServiceUrlEg.getText();
		ExpectedValue="( Generally same as Sign On Service Url )";
		PassStatement = "PASS >> 'Logout Service Url Example' is Displayed succesfully when ADFS SSO Type";
		FailStatement = "FAIL >> 'Logout Service Url Example' is not Displayed when ADFS SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

	}

	public void VerifyOfEgForAzureAD() {
		String ActualValue=ServiceIdentifierEg.getText();
		String ExpectedValue="( e.g.: https://sts.windows.net/f2f933ec-d7c9-433f-8926-d3a0732a7dcf/ )";
		String PassStatement = "PASS >> 'Service Identifier Example' is Displayed succesfully when AzureAD SSO Type";
		String FailStatement = "FAIL >> 'Service Identifier Example' is not Displayed when AzureAD SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=SignOnServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://login.microsoftonline.com/f2f933ec-d7c9-433f-8926-d3a0732a7dcf/saml2 )";
		PassStatement = "PASS >> 'Sign On Service Url Example' is Displayed succesfully when AzureAD SSO Type";
		FailStatement = "FAIL >> 'Sign On Service Url Example' is not Displayed when AzureAD SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=LogoutServiceUrlEg.getText();
		ExpectedValue="( Generally same as Sign On Service Url )";
		PassStatement = "PASS >> 'Logout Service Url Example' is Displayed succesfully when AzureAD SSO Type";
		FailStatement = "FAIL >> 'Logout Service Url Example' is not Displayed when AzureAD SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

	}

	public void VerifyOfEgForOkta() {
		String ActualValue=ServiceIdentifierEg.getText();
		String ExpectedValue="( e.g.: http://www.okta.com/exk89rwwiahjnDQiv0h7 )";
		String PassStatement = "PASS >> 'Service Identifier Example' is Displayed succesfully when Okta SSO Type";
		String FailStatement = "FAIL >> 'Service Identifier Example' is not Displayed when Okta SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=SignOnServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://componentspace.oktapreview.com/app/componentspacedev527539_exampleserviceprovider_3/exk89rwwiahjnDQiv0h7/sso/saml )";
		PassStatement = "PASS >> 'Sign On Service Url Example' is Displayed succesfully when Okta SSO Type";
		FailStatement = "FAIL >> 'Sign On Service Url Example' is not Displayed when Okta SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=LogoutServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://componentspace.oktapreview.com/app/componentspacedev527539_exampleserviceprovider_3/exk89rwwiahjnDQiv0h7/slo/saml )";
		PassStatement = "PASS >> 'Logout Service Url Example' is Displayed succesfully when Okta SSO Type";
		FailStatement = "FAIL >> 'Logout Service Url Example' is not Displayed when Okta SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

	}

	public void VerifyOfEgForOneLogin() {
		String ActualValue=ServiceIdentifierEg.getText();
		String ExpectedValue="( e.g.: https://app.onelogin.com/saml/metadata/651423 )";
		String PassStatement = "PASS >> 'Service Identifier Example' is Displayed succesfully when OneLogin SSO Type";
		String FailStatement = "FAIL >> 'Service Identifier Example' is not Displayed when OneLogin SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=SignOnServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://42g.onelogin.com/trust/saml2/http-redirect/sso/651423 )";
		PassStatement = "PASS >> 'Sign On Service Url Example' is Displayed succesfully when OneLogin SSO Type";
		FailStatement = "FAIL >> 'Sign On Service Url Example' is not Displayed when OneLogin SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=LogoutServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://42g.onelogin.com/trust/saml2/http-redirect/slo/651423 )";
		PassStatement = "PASS >> 'Logout Service Url Example' is Displayed succesfully when OneLogin SSO Type";
		FailStatement = "FAIL >> 'Logout Service Url Example' is not Displayed when OneLogin SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);

	}

	public void VerifyOfEgForPingOne() {
		String ActualValue=ServiceIdentifierEg.getText();
		String ExpectedValue="( e.g.: https://pingone.com/idp/componentspace )";
		String PassStatement = "PASS >> 'Service Identifier Example' is Displayed succesfully when PingOne SSO Type";
		String FailStatement = "FAIL >> 'Service Identifier Example' is not Displayed when PingOne SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=SignOnServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://sso.connect.pingidentity.com/sso/idp/SSO.saml2?idpid=f0f2b9e9-967a-4c79-bb00-15fe88401e13 )";
		PassStatement = "PASS >> 'Sign On Service Url Example' is Displayed succesfully when PingOne SSO Type";
		FailStatement = "FAIL >> 'Sign On Service Url Example' is not Displayed when PingOne SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		ActualValue=LogoutServiceUrlEg.getText();
		ExpectedValue="( e.g.: https://sso.connect.pingidentity.com/sso/SLO.saml2 )";
		PassStatement = "PASS >> 'Logout Service Url Example' is Displayed succesfully when PingOne SSO Type";
		FailStatement = "FAIL >> 'Logout Service Url Example' is not Displayed when PingOne SSO Type";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
	}

	public void ClickOnSSODoneBtn()
	{
		SSODoneBtn.click();
	}

	public void ClearServiceIdentifier()
	{
		ServiceIdentifierTextBox.clear();
	}

	public void EnterServiceIdentifier(String ServiceIdentifier)
	{
		ServiceIdentifierTextBox.sendKeys(ServiceIdentifier);;
	}

	public void ClearSignOnServiceUrl()
	{
		SignOnServiceUrlTextBox.clear();
	}

	public void EnterSignOnServiceUrl(String SignOnServiceUrl)
	{
		SignOnServiceUrlTextBox.sendKeys(SignOnServiceUrl);;
	}

	public void ClearLogoutServiceUrl()
	{
		LogoutServiceUrlBox.clear();
	}

	public void EnterLogoutServiceUrl(String LogoutServiceUrl)
	{
		LogoutServiceUrlBox.sendKeys(LogoutServiceUrl);;
	}

	public void VerifyOfErrorMessageOfServiceIdentifier() {
		String Actualvalue=ServiceIdentifierErrorMessage.getText();
		String Expectedvalue="Please provide Service Identifier.";
		String PassStatement="PASS >> "+Expectedvalue+ " is mandatory field. Alert Message 'Please provide Service Identifier.'is displayed succesfully";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Please provide Service Identifier.";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	}

	public void VerifyOfErrorMessageOfSignOnServiceUrl() {
		String Actualvalue=SignOnServiceUrlErrorMessage.getText();
		String Expectedvalue="Please provide Service URL.";
		String PassStatement="PASS >> "+Expectedvalue+ " is mandatory field. Alert Message 'Please provide Service URL.'is displayed succesfully";
		String FailStatement="FAIL >> Mandatory field is not highlighted : Please provide Service URL.";
		ALib.AssertEqualsMethod(Actualvalue, Expectedvalue, PassStatement, FailStatement);
	}

	public void ClickOnDefaultPermission() throws InterruptedException{
		sleep(2);
		Actions act = new Actions(Driver.driver);
		act.moveToElement(DefaultPermissionBtn).click().perform();
		waitForidPresent("permissiontype");
		sleep(2);
	}

	public void TestFailureSSOSettingsUserPermission() throws InterruptedException {
		try {
			ClickOnDefaultPermission();
		} catch (Exception e) {
			refesh();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();
			ClickOnAccountSettings();
			ClickOnSSOSettings();
			ClickOnDefaultPermission();
		}
	}

	public void TestFailureSSOSettingsCertificate(String SSOType) throws InterruptedException {
		try {
			ClickOnSSODoneBtn();
			ConfirmationMessageVerify(true);
		} catch (Exception e) {
			refesh();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();
			ClickOnAccountSettings();
			ClickOnSSOSettings();
			EnableSSO();
			SelectSSOType(SSOType);
			ClickOnDefaultPermission();
			Initialization.usermanagementpage.ClickOnOkBtnSSO();
		}
	}

	public void ClickOnGenerateCertificate() throws InterruptedException
	{
		boolean value=DeleteCertificateBtn.isDisplayed();
		System.out.println(value);
		GenerateCertficateBtn.click();
		String PassStatement="PASS >> 'Certificate generated successfully.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Certificate generated successfully.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(GenerateCertficateConfirmMessage, true, PassStatement, FailStatement,4);
		value=DeleteCertificateBtn.isDisplayed();
		System.out.println(value);
	}

	public void ClickOnDownloadCertificate() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		String downloadPath = ELib.getDatafromExcel("Sheet1", 19, 1);
		String fileName = "adfs_"+Config.AccountId+".cer";
		String dd = fileName.replace(".", "  ");
		String[] text = dd.split("  ");
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();
		int initial = 0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().startsWith(text[0]) && dir_contents[i].getName().endsWith(text[1])) {

				initial++;
			}
		}

		DownloadCertificateBtn.click();
		sleep(10);

		dir = new File(downloadPath);
		dir_contents = dir.listFiles();
		int result=0;
		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().contains(text[0]) && dir_contents[i].getName().contains(text[1])) {
				result++;
			}
		}
		boolean value=result>initial;
		String PassStatement="PASS >> Certificate Downloaded successfully";
		String FailStatement="Fail >> Certificate is not Downloaded";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnDeleteCertificate() throws InterruptedException
	{
		DeleteCertificateBtn.click();
		String PassStatement="PASS >> 'Certificate deleted successfully.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Certificate deleted successfully.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(DeleteCertficateConfirmMessage, true, PassStatement, FailStatement,3);
	}

	public void ClickOnUploadCertificate() throws InterruptedException{
		UploadCertficateBtn.click();
		waitForidPresent("cert_pswd_input");
		sleep(2);
	}

	public void VerifyUplaodCertificateHeader()
	{
		boolean value = UploadCertificateHeader.isDisplayed();
		String PassStatement = "PASS >> 'User Permissions' Header is Displayed succesfully when Clicked on Default User Permission";
		String FailStatement = "FAIL >> 'User Permissions' Header is not Displayed when Clicked on Default User Permission";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

	}

	public void ClickOnUploadOkBtnSSO() throws InterruptedException{
		UploadCertificateOkBtn.click();
	}

	public void UploadValidCertificate() throws InterruptedException, IOException{
		UploadFileSSO.click();
		sleep(1);
		Runtime.getRuntime().exec("C:\\Users\\sandhyas1792\\Desktop\\UplaodFiles\\SSO\\File1.exe");
		sleep(1);
		PasswordSSO.sendKeys("0000");

	}

	public void UploadCertificateErrorMesssage() throws InterruptedException
	{
		String PassStatement="PASS >> 'Please select certificate to upload.' Message is displayed successfully";
		String FailStatement="FAIL >> 'Please select certificate to upload.' Message is not displayed";
		Initialization.commonmethdpage.ConfirmationMessageVerify(UploadCertificateErrorMessage, true, PassStatement, FailStatement,3);
		sleep(2);
	}


	public void ClickOnUploadCertificateCloseBtn() throws InterruptedException
	{
		UploadCertificateCloseBtn.click();
		Reporter.log("Clickec On Upload Certificate Close Button",true);
		sleep(3);
	}

	public void VerifyOfSSO(String SSOType,String ServiceIdentifier,String SignOnServiceUrl,String LogoutServiceUrl) throws InterruptedException {

		String ActualValue=ServiceIdentifierTextBox.getAttribute("value");
		String PassStatement = "PASS >> 'Service Identifier' is saved succesfully when SSO Type is "+SSOType;
		String FailStatement = "FAIL >> 'Service Identifier ' is not saved when SSO Type is "+SSOType;
		ALib.AssertEqualsMethod(ActualValue, ServiceIdentifier, PassStatement, FailStatement);
		ActualValue=SignOnServiceUrlTextBox.getAttribute("value");
		PassStatement = "PASS >> 'Sign On Service Url ' is saved succesfully when SSO Type is "+SSOType;
		FailStatement = "FAIL >> 'Sign On Service Url' is not saved when SSO Type is "+SSOType;
		ALib.AssertEqualsMethod(ActualValue, SignOnServiceUrl, PassStatement, FailStatement);
		ActualValue=LogoutServiceUrlBox.getAttribute("value");
		PassStatement = "PASS >> 'Logout Service Url ' is saved succesfully when SSO Type is "+SSOType;
		FailStatement = "FAIL >> 'Logout Service Url ' is not saved when SSO Type is "+SSOType;
		ALib.AssertEqualsMethod(ActualValue, LogoutServiceUrl, PassStatement, FailStatement);

	}

	public void VerifyOfSSOURL(){
		boolean value=LoginURLText.isDisplayed();
		String PassStatement = "PASS >> 'Login URL for Single Sign-On users:' Header is Displayed succesfully when enabled SSO";
		String FailStatement = "FAIL >> 'Login URL for Single Sign-On users: ' Header is not Displayed when enabled SSO";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
	}

	public void ClickOnSSOURL() throws InterruptedException{
		String ActualValue=SSOLoginURLBtn.getText();
		String ExpectedValue = Config.url+"/ssologin/"+Config.AccountId;
		String PassStatement = "PASS >> 'Login URL for Single Sign-On users' is Displayed succesfully when enabled SSO";
		String FailStatement = "FAIL >> 'Login URL for Single Sign-On users'is not Displayed when enabled SSO";
		ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
		SSOLoginURLBtn.click();
		Set<String> set=Initialization.driver.getWindowHandles();
		Iterator<String> id=set.iterator();
		String parentwinID = id.next();;
		String childwinID = id.next();
		Initialization.driver.switchTo().window(childwinID);
		String URL=Initialization.driver.getCurrentUrl();
		PassStatement = "PASS >> Successfully Navigated to the Login URL for Single Sign-On users when clicked on SSO URL";
		FailStatement = "FAIL >> Navigation to Login URL for Single Sign-On users when clicked on SSO URL failed";
		ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
		Initialization.driver.close();
		Initialization.driver.switchTo().window(parentwinID);
		sleep(5);

	}

	public void VerifyOfDisableSSO(){
		boolean value=EnableSingleSignOnCheckbox.isSelected();
		String PassStatement = "PASS >> Disabling of SSO is saved successfully";
		String FailStatement = "FAIL >> Disabling of SSO is not saved";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}

	public void VerifyOfDisableSSOURL(){
		boolean value=SSOLoginURLBtn.isDisplayed();
		String PassStatement = "PASS >> 'Login URL for Single Sign-On users:' Header is not visible when SSO Disabled";
		String FailStatement = "FAIL >> 'Login URL for Single Sign-On users: ' Header is Displayed even though SSO Disabled";
		ALib.AssertFalseMethod(value, PassStatement, FailStatement);
	}


	//*******Alert Template by Prachujya

	public void ClickOnAlertTemplate() throws InterruptedException{
		AlertTemplateSection.click();
		waitForidPresent("EnableBatteryPolicy");
		sleep(2);
	}

	public void VerifyOfAlertTemplatePage() throws InterruptedException
	{
		//**********Battery Policy************
		String HeaderBatteryPolicyText=AlertTemplatetBatteyPolicy.getText();
		Reporter.log(HeaderBatteryPolicyText);
		boolean value = AlertTemplatetBatteyPolicy.isDisplayed();
		String PassStatement="PASS >> 'Battery Policy' sections is displayed when Alert Template section is clicked";
		String FailStatement="FAIL >> 'Battery Policy' sections is NOT displayed when Alert Template section is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = AlertTemplateTitle.isDisplayed();
		PassStatement="PASS >> 'Template Title' field is displayed when Alert Template is clicked";
		FailStatement="FAIL >> 'Template Title' field is NOT displayed when Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = AlertTemplate_Template.isDisplayed();
		PassStatement="PASS >> 'Template' field is displayed when  Alert Template is clicked";
		FailStatement="FAIL >> 'Template' field is NOT displayed when  Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("");

		//verifying default Template Title of Battery Policy
		Reporter.log("======Verify default Title of 'Template Title' of Battery Policy=====");
		String actualValue = TemplateTitleTextBox.getAttribute("value");
		Reporter.log("Template Title of Battery Policy is: " +actualValue);
		String expectedValue = "SureMDM Battery Alert for device  %DeviceName% ";
		String PassStatement1 = "PASS >> Default 'Template title' "+expectedValue+" is correct";
		String FailStatement1 = "FAIL >> Default 'Template title' "+expectedValue+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, PassStatement1, FailStatement1);
		sleep(3);
		Reporter.log("");

		//verifying default Template of Battery Policy
		Reporter.log("======Verify default Template of Battery Policy=====");
		String ActualValue1 = TemplateTextBox.getAttribute("value");
		Reporter.log("Template of Battery Policy is: " +ActualValue1);
		String ExpectedValue1 ="Device Name:  %DeviceName% Device Notes:  %DeviceNotes% Current battery level:  %CurrentBatteryLevel% %Specified threshold:  %SpecifiedThreshold% % .Current charging status :  %CurrentChargingStatus% .";
		String PassStatement2 = "PASS >> Default 'Template' "+ExpectedValue1+" is correct";
		String FailStatement2 = "FAIL >> Default 'Template' "+expectedValue+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, PassStatement2, FailStatement2);
		Reporter.log("");


		//**********Connection Policy************
		String HeaderConnectionPolicyText=AlertTemplatetConnectionPolicy.getText();
		Reporter.log(HeaderConnectionPolicyText);
		boolean value1 = AlertTemplatetConnectionPolicy.isDisplayed();
		String ConnectionPass="PASS >> 'Connection Policy' sections is displayed when Alert Template section is clicked";
		String ConnectionFail="FAIL >> 'Connection Policy' sections is NOT displayed when Alert Template section is clicked";
		ALib.AssertTrueMethod(value1, ConnectionPass, ConnectionFail);

		value = AlertTemplateTitle.isDisplayed();
		PassStatement="PASS >> 'Template Title' field of 'Connection Policy' is displayed when Alert Template is clicked";
		FailStatement="FAIL >> 'Template Title' field of 'Connection Policy' is NOT displayed when Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = AlertTemplate_Template.isDisplayed();
		PassStatement="PASS >> 'Template' field of 'Connection Policy' is displayed when  Alert Template is clicked";
		FailStatement="FAIL >> 'Template' field of 'Connection Policy' is NOT displayed when  Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("");

		//verifying default Template Title of Connection Policy
		Reporter.log("======Verify default Title of 'Template Title' of Connection Policy=====");
		String actualValue1 = ConnectionPolicyTemplateTitleTextBox.getAttribute("value");
		Reporter.log("Template Title of Connection Policy is: " +actualValue1);
		String expectedValue1 = "SureMDM Connection Alert for device  %DeviceName% ";
		String ConnectionTextPass = "PASS >> Default 'Template title' "+expectedValue1+" is correct";
		String ConnectionTextFail = "FAIL >> Default 'Template title' "+expectedValue1+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, ConnectionTextPass, ConnectionTextFail);
		Reporter.log("");

		//verifying default Template of Connection Policy
		Reporter.log("======Verify default Template of Connection Policy=====");
		String ConnectionTemplateActualTextBox= ConnectionPolicyTemplateTextBox.getAttribute("value");
		Reporter.log("Template of Connection Policy is: " +ConnectionTemplateActualTextBox);
		String ConnectionTemplateExpectedTextBox ="Device Name:  %DeviceName% was offline for %OfflineTime% , since %OfflineDateTime% (UTC) + Device Notes:  %DeviceNotes% .";
		String PassStatement3 = "PASS >> Default 'Template' "+ConnectionTemplateExpectedTextBox+" is correct";
		String FailStatement3 = "FAIL >> Default 'Template' "+ConnectionTemplateExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, PassStatement3, FailStatement3);
		Reporter.log("");								


		//**********Data Usage Policy************

		String HeaderDataUsagePolicyText=AlertTemplatetDataUsagePolicy.getText();
		Reporter.log(HeaderDataUsagePolicyText);
		boolean value2 = AlertTemplatetDataUsagePolicy.isDisplayed();
		String DataUsagePass="PASS >> 'Data Usage Policy' section is displayed when Alert Template section is clicked";
		String DataUsageFail="FAIL >> 'Data Usage Policy' section is NOT displayed when Alert Template section is clicked";
		ALib.AssertTrueMethod(value2, DataUsagePass, DataUsageFail);	

		value = AlertTemplateTitle.isDisplayed();
		PassStatement="PASS >> 'Template Title' field of 'Data Usage Policy' is displayed when Alert Template is clicked";
		FailStatement="FAIL >> 'Template Title' field of 'Connection Policy' is NOT displayed when Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = AlertTemplate_Template.isDisplayed();
		PassStatement="PASS >> 'Template' field of 'Data Usage Policy' is displayed when  Alert Template is clicked";
		FailStatement="FAIL >> 'Template' field of 'Data Usage Policy' is NOT displayed when  Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("");

		//verifying default Template Title of Data Usage Policy
		Reporter.log("======Verify default Title of 'Template Title' of Data Usage Policy=====");
		String actualValue2 = DataUsagePolicyTemplateTitleTextBox.getAttribute("value");
		Reporter.log("Template Title of Data Usage Policy is: " +actualValue2);
		String expectedValue2 = "SureMDM Connection Alert for device  %DeviceName% ";
		String DataUsageTextPass = "PASS >> Default 'Template title' "+expectedValue2+" is correct";
		String DataUsageTextFail = "FAIL >> Default 'Template title' "+expectedValue2+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, DataUsageTextPass, DataUsageTextFail);
		Reporter.log("");

		//verifying default Template of Data Usage Policy
		Reporter.log("======Verify default Template of Data Usage Policy=====");
		String DataUsageTemplateActualTextBox= DataUsagePolicyTemplateTextBox.getAttribute("value");
		Reporter.log("Template of Data Usage Policy is: " +DataUsageTemplateActualTextBox);
		String DataUsageTemplateExpectedTextBox ="Device Name:  %DeviceName% Device Notes:  %DeviceNotes% Current Data Usage:  %CurrentDataUsage% Specified threshold: %SpecifiedThresholdData% .";
		String PassStatement4 = "PASS >> Default 'Template' "+DataUsageTemplateExpectedTextBox+" is correct";
		String FailStatement4 = "FAIL >> Default 'Template' "+DataUsageTemplateExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, PassStatement4, FailStatement4);
		Reporter.log("");		


		//**********Notify when device comes online************

		String HeaderNotifyWhenDeviceComesOnlineText=AlertTemplatetNotifyWhenDeviceComeOnline.getText();
		Reporter.log(HeaderNotifyWhenDeviceComesOnlineText);
		boolean value3 = AlertTemplatetNotifyWhenDeviceComeOnline.isDisplayed();
		String NotifyPass="PASS >> ' Notify when device comes online Policy' section is displayed when Alert Template section is clicked";
		String NotifyFail="FAIL >> 'Notify when device comes online Policy' section is NOT displayed when Alert Template section is clicked";
		ALib.AssertTrueMethod(value3, NotifyPass, NotifyFail);


		value = AlertTemplateTitle.isDisplayed();
		PassStatement="PASS >> 'Template Title' field of 'Notify when device comes online' is displayed when Alert Template is clicked";
		FailStatement="FAIL >> 'Template Title' field of 'Notify when device comes online' is NOT displayed when Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = AlertTemplate_Template.isDisplayed();
		PassStatement="PASS >> 'Template' field of Notify when device comes online' is displayed when  Alert Template is clicked";
		FailStatement="FAIL >> 'Template' field of 'Notify when device comes online' is NOT displayed when  Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("");

		//verifying default Template Title of Notify when device comes online
		Reporter.log("======Verify default Template title of Notify when device comes online=====");
		String NotifyTemplateTitleActualTextBox= NotifyWhenDeviceComesOnlineTemplateTitleTextBox.getAttribute("value");
		Reporter.log("Template Title of Notify online Policy is: " +NotifyTemplateTitleActualTextBox);
		String NotifyTemplateExpectedTextBox = "SureMDM Connection Alert for device  %DeviceName% ";
		String NotifyOnlineTextPass = "PASS >> Default 'Template title' "+NotifyTemplateExpectedTextBox+" is correct";
		String NotifyOnlineTextFail = "FAIL >> Default 'Template title' "+NotifyTemplateExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, NotifyOnlineTextPass, NotifyOnlineTextFail);
		Reporter.log("");

		//verifying default Template of Notify when device comes online
		Reporter.log("======Verify default Template of Notify when device comes online=====");
		String NotifyTemplateActualTextBox= NotifyWhenDeviceComesOnlineTemplateTextBox.getAttribute("value");
		Reporter.log("Template of Notify Online Policy is: " +NotifyTemplateActualTextBox);
		String NotifyOnlineTemplateExpectedTextBox ="Device Name:  %DeviceName% came online at %OnlineDateTime% (UTC) Device Notes:  %DeviceNotes%";
		String PassStatement5 = "PASS >> Default 'Template' "+NotifyOnlineTemplateExpectedTextBox+" is correct";
		String FailStatement5 = "FAIL >> Default 'Template' "+NotifyOnlineTemplateExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, PassStatement5, FailStatement5);
		Reporter.log("");		


		//**********Notify when SIM is changed************

		String HeaderNotifyWhenSIMIsChangedText=AlertTemplatetNotifyWhenDeviceComeOnline.getText();
		Reporter.log(HeaderNotifyWhenSIMIsChangedText);
		boolean value4 = AlertTemplatetNotifyWhenSIMIsChanged.isDisplayed();
		String NotifySIMChangePass="PASS >> 'Notify when SIM is Changed Policy' section is displayed when Alert Template section is clicked";
		String NotifySIMChangeFail="FAIL >> 'Notify when SIM is Changed Policy' section is NOT displayed when Alert Template section is clicked";
		ALib.AssertTrueMethod(value4, NotifySIMChangePass, NotifySIMChangeFail);


		value = AlertTemplateTitle.isDisplayed();
		PassStatement="PASS >> 'Template Title' field of 'Notify when SIM is Changed Policy' is displayed when Alert Template is clicked";
		FailStatement="FAIL >> 'Template Title' field of 'Notify when SIM is Changed Policy' is NOT displayed when Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = AlertTemplate_Template.isDisplayed();
		PassStatement="PASS >> 'Template' field of 'Notify when SIM is Changed Policy' is displayed when  Alert Template is clicked";
		FailStatement="FAIL >> 'Template' field of 'Notify when SIM is Changed Policy' is NOT displayed when  Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("");


		//verifying default Template Title of Notify when SIM is Changed Policy
		Reporter.log("======Verify default Template title of Notify when SIM is Changed Policy=====");
		String NotifySIMChangeTemplateTitleActualTextBox= NotifyWhenSIMIsChangedTemplateTitleTextBox.getAttribute("value");
		Reporter.log("Template Title of Notify SIM Change Policy is: " +NotifySIMChangeTemplateTitleActualTextBox);
		String NotifySIMChangeTemplateTitleExpectedTextBox = "SureMDM Connection Alert for device  %DeviceName% ";
		String NotifySIMChangeTextPass = "PASS >> Default 'Template title' "+NotifySIMChangeTemplateTitleExpectedTextBox+" is correct";
		String NotifySIMChangeTextFail = "FAIL >> Default 'Template title' "+NotifySIMChangeTemplateTitleExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, NotifySIMChangeTextPass, NotifySIMChangeTextFail);
		Reporter.log("");

		//verifying default Template of Notify when SIM is Changed Policy
		Reporter.log("======Verify default Template of Notify when device comes online=====");
		String NotifySIMChangeTemplateActualTextBox= NotifyWhenSIMIsChangedTemplateTextBox.getAttribute("value");
		Reporter.log("Template of Notify When SIM Changed Policy is: " +NotifySIMChangeTemplateActualTextBox);
		String NotifySIMChangeTemplateExpectedTextBox ="Device Name: %DeviceName% New IMSI: %NewIMSI% + Old IMSI: %OldIMSI%";
		String PassStatement7 = "PASS >> Default 'Template' "+NotifySIMChangeTemplateExpectedTextBox+" is correct";
		String FailStatement7 = "FAIL >> Default 'Template' "+NotifySIMChangeTemplateExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, PassStatement7, FailStatement7);
		Reporter.log("");		

		//**********Notify when device is rooted or Nix has been granted with root permission************

		String HeaderNotifyRootedNixGrantedText=AlertTemplatetNotifyRootedNixGranted.getText();
		Reporter.log(HeaderNotifyRootedNixGrantedText);
		boolean value5 = AlertTemplatetNotifyRootedNixGranted.isDisplayed();
		String NotifyRootedGrantedPass="PASS >> 'Notify when device is rooted or Nix has been granted with root permission' section is displayed when Alert Template section is clicked";
		String NotifyRootedGrantedFail="FAIL >> 'Notify when device is rooted or Nix has been granted with root permission' section is NOT displayed when Alert Template section is clicked";
		ALib.AssertTrueMethod(value5, NotifyRootedGrantedPass, NotifyRootedGrantedFail);


		value = AlertTemplateTitle.isDisplayed();
		PassStatement="PASS >> 'Template Title' field of 'Notify when device is rooted or Nix has been granted with root permission' is displayed when Alert Template is clicked";
		FailStatement="FAIL >> 'Template Title' field of 'Notify when device is rooted or Nix has been granted with root permission' is NOT displayed when Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);

		value = AlertTemplate_Template.isDisplayed();
		PassStatement="PASS >> 'Template' field of 'Notify when device is rooted or Nix has been granted with root permission' is displayed when  Alert Template is clicked";
		FailStatement="FAIL >> 'Template' field of 'Notify when device is rooted or Nix has been granted with root permission' is NOT displayed when  Alert Template is clicked";
		ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		Reporter.log("");

		//verifying default Template Title of Notify when device is rooted or Nix has been granted with root permission
		Reporter.log("======Verify default Template title of Notify when device is rooted or Nix has been granted with root permission=====");
		String NotifyNixGrantedRootedTemplateTitleActualTextBox= NotifyNixRootedGrantedTemplateTitleTextBox.getAttribute("value");
		Reporter.log("Template Title of Notify Rooted Granted Policy is: " +NotifyNixGrantedRootedTemplateTitleActualTextBox);
		String NotifyNixRootedGrantTemplateTitleExpectedTextBox = "SureMDM Nix Agent Root Permission Alert for device %DeviceName%";
		String NotifyNixRootedGrantTextPass = "PASS >> Default 'Template title' "+NotifyNixRootedGrantTemplateTitleExpectedTextBox+" is correct";
		String NotifyNixRootedGrantTextFail = "FAIL >> Default 'Template title' "+NotifyNixRootedGrantTemplateTitleExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, NotifyNixRootedGrantTextPass, NotifyNixRootedGrantTextFail);
		Reporter.log("");

		//verifying default Template of Notify when device is rooted or Nix has been granted with root permission
		Reporter.log("======Verify default Template of Notify when device is rooted or Nix has been granted with root permission=====");
		String NotifyNixGrantRootedTemplateActualTextBox= NotifyNixRootedGrantedTemplateTextBox.getAttribute("value");
		Reporter.log("Template of Notify NIX Rooted Granted Policy is: " +NotifyNixGrantRootedTemplateActualTextBox);
		String NotifyNixRootedGrantedExpectedTextBox ="Device Name:  %DeviceName% %RootPermission%";
		String PassStatement8 = "PASS >> Default 'Template' "+NotifyNixRootedGrantedExpectedTextBox+" is correct";
		String FailStatement8 = "FAIL >> Default 'Template' "+NotifyNixRootedGrantedExpectedTextBox+" is NOT correct";
		ALib.AssertEqualsMethod(expectedValue, actualValue, PassStatement8, FailStatement8);
		Reporter.log("");		



	}
	//************iOS Enrollment Settings***************************

	//verifying clicking on iOS Enrollment Settings
	public void ClickOniOSEnrollmentSettings() throws InterruptedException{
		iOSEnrollmentSettingsSection.click();
		waitForXpathPresent("//p[text()='Push Certificate']");
		sleep(3);
	}


	//verifying header of Push Certificate
	public void	VerifyiOSEnrollmentSettings_PushCertificate() throws InterruptedException{
		String HeaderPushCertificateText=AlertTemplatet_iOSEnrollmentSettings.getText();
		Reporter.log(HeaderPushCertificateText);
		boolean value = AlertTemplatet_iOSEnrollmentSettings.isDisplayed();
		String Pass="PASS >> 'Push Certificate' section is displayed when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Push Certificate' section is NOT displayed when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);	

		//verifying Download CSR header
		value = DownloadCSRText.isDisplayed();
		Pass="PASS >> 'DownloadCSRText' is displayed when iOS Enrollment Settings is clicked";
		Fail="FAIL >> 'DownloadCSRText' is NOT displayed when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);

		String Actualvalue =DownloadCSRSummaryText.getText();
		String Expectedvalue = "Download vendor-signed CSR to generate push certificate from Apple Push Certificate Portal";
		String PassStatement = "PASS >> summary of 'Push Certificate' "+Expectedvalue+"  displayed successfully";
		String FailStatement = "FAIL >> summary of 'Push Certificate' is NOT  displayed successfully";
		ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);
		sleep(3);

	}
	//verifying Clicking On Download CSR link 

	public void ClickOnDownloadCSRLink() throws InterruptedException{ 
		DownloadCSRLink.click();
		sleep(4);
		String originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		String ActualTitle = Initialization.driver.getTitle();
		String ExpectedTitle ="Apple Push Certificates Portal";
		String PassStatement1 = "PASS>> Clicked on the link and "+ExpectedTitle+" is displayed as title of apple push certificate";
		String FailStatement1 = "FAIL >>Wrong title of  apple push  or unable to open the link" ;
		ALib.AssertEqualsMethod(ExpectedTitle, ActualTitle, PassStatement1, FailStatement1);

		//going back to previous tab.
		for(String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);   

	}

	//verifying downloading of CSR
	public void VerifyDownloadCSR() throws InterruptedException{
		ClickOnDownloadButton_DownloadCSR.click();
		sleep(4);
		Reporter.log("CSR downloaded successfully");
	}


	//verifying Certificate header
	public void VerifyCertificateSection(){
		Boolean value = Certificate.isDisplayed();
		String Pass="PASS >> 'Certificate header' is displayed when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Certificate header' is NOT displayed when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verifying Certificate summary	
	public void VerifyCertificateSummarySection(){
		Boolean value =CertificateSummaryText.isDisplayed();
		String Pass="PASS >> 'Certificate summary' is displayed when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Certificate summary' is NOT displayed or Wrong when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verifying Certificate attribute
	public void VerifyCertificateAttributeText()
	{
		String Actualvalue= CertificateAttributeText.getAttribute("value");
		System.out.println(Actualvalue);
		//String Expectedvalue = "installed job";
		//String PassStatement = "PASS >> Job is selected and "+Expectedvalue+" is  displayed in the Select Job Text box";
		//String FailStatement = "FAIL >> "+Expectedvalue+ " is not displayed in the Select Job Text box" ;
		//ALib.AssertEqualsMethod(Expectedvalue, Actualvalue, PassStatement, FailStatement);

	}

	//verifying header of Device Enrolment Program
	public void	VerifyiOSEnrollmentSettings_DeviceEnrolmentProgramHeader() throws InterruptedException
	{
		String HeaderDeviceEnrolmentProgramText=iOSEnrollmentSettings_DeviceEnrolmentProgramHeader.getText();
		Reporter.log(HeaderDeviceEnrolmentProgramText);
		boolean value = iOSEnrollmentSettings_DeviceEnrolmentProgramHeader.isDisplayed();
		String Pass="PASS >> 'Device Enrolment Program' section is displayed when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Device Enrolment Program' section is NOT displayed when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);	
		sleep(4);

	}
	//verifying PEM Certificate header
	public void VerifyPEMCertificateSection(){
		Boolean value = PEMCertificateHeader.isDisplayed();
		String Pass="PASS >> 'PEM Certificate header' is displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'PEM Certificate header' is NOT displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verifying PEM Certificate summary	
	public void VerifyPEMCertificateSummarySection() throws InterruptedException{
		Boolean value =PEMCertificateSummaryText.isDisplayed();
		String Pass="PASS >> 'PEM Certificate summary' is displayed when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'PEM Certificate summary' is NOT displayed or Wrong when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
		sleep(4);

		//verifying Clicking on the apple deployment link
		AppleDeploymentProgramsLink.click();
		sleep(4);
		String originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		String ActualTitle = Initialization.driver.getTitle();
		String ExpectedTitle ="Apple Push Certificates Portal";
		String PassStatement1 = "PASS>> Clicked on the link and "+ExpectedTitle+" is displayed as title of Apple Deployment Programs";
		String FailStatement1 = "FAIL >>Wrong title of  Apple Deployment Programs or unable to open the link" ;
		ALib.AssertEqualsMethod(ExpectedTitle, ActualTitle, PassStatement1, FailStatement1);

		//going back to previous tab.
		for(String handle : Initialization.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				Initialization.driver.switchTo().window(handle);
				Initialization.driver.close();
			}
		}

		Initialization.driver.switchTo().window(originalHandle);   


	}

	//verifying downloading of CSR
	public void VerifyDownloadingPEMCertificate() throws InterruptedException{
		ClickOnDownloadButton_PEMCertificate.click();
		sleep(4);
		Reporter.log("PEM Certificate downloaded successfully");
	}

	//verifying Server Token header
	public void VerifyServerTokenHeader(){
		Boolean value = ServerTokenHeader.isDisplayed();
		String Pass="PASS >> 'Server Token header' is displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Server Token header' is NOT displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verifying Server Token summary	
	public void VerifyServerTokenSummarySection() throws InterruptedException{
		Boolean value =ServerTokenSummaryText.isDisplayed();
		String Pass="PASS >> 'Server Token summary' is displayed when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Server Token summary' is NOT displayed or Wrong when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
		sleep(4);
	}

	//verifying DEP Profile header
	public void VerifyDEPProfileHeader(){
		Boolean value = DEPProfileHeader.isDisplayed();
		String Pass="PASS >> 'DEP Profile header' is displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'DEP Profile header' is NOT displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verifying DEP Profile summary
	public void VerifyDEPProfileSummary(){
		Boolean value = DEPProfileSummary.isDisplayed();
		String Pass="PASS >> 'DEP Profile summary' is displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'DEP Profile summary' is NOT displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verifying DEP Profile Configure
	public void VerifyDEPProfileConfigure() throws InterruptedException{
		try {
			DEPProfileConfigureButton.click();
		} catch (Exception e) {

		}
		waitForidPresent("skip_Passcode");
		sleep(5);

		Boolean value = DEPProfileConfigureWindowHeader.isDisplayed();
		String Pass="PASS >> 'DEP Profile Configure window' is displayed while clicking on the Configure Button";
		String Fail="FAIL >> 'DEP Profile Configure window' is NOT displayed while clicking on the Configure Button";
		ALib.AssertTrueMethod(value, Pass, Fail);
		sleep(4);
	}

	//Verify Cancel of DEP profile window
	public void CancelDEPProfileWindow() throws InterruptedException{
		CancelDepProfileWindow.click();
		sleep(4);
		Boolean value = ClickOnDownloadButton_DownloadCSR.isDisplayed();
		String Pass="PASS >> 'DownloadButton' is displayed - Cancelled DEP profile window successfully";
		String Fail="FAIL >> 'DownloadButton' is NOT displayed - Unable to Cancel DEP Profile Window";
		ALib.AssertTrueMethod(value, Pass, Fail);
		sleep(3);
	}

	public void SaveDEPProfileWindow() throws InterruptedException{
		SaveDepProfileWindow.click();
		sleep(5);
		boolean b=true;

		try{
			WarningMessageOnSavingDEPProfile.isDisplayed();

		}catch(Exception e)
		{
			b=false;

		}

		Assert.assertTrue(b,"FAIL >> Receiving of notitication failed");
		Reporter.log("PASS >> Notification 'Profile updated successfully.'is displayed - DEP Profile saved successfully",true );	
		sleep(3);

	}


	//verifying DEP Profile Window's options/elements
	public void VerifyAllTheOptionsOfDEPProfileWindow() throws InterruptedException{

		Boolean value = ProfileName.isDisplayed();
		String Pass="PASS >> 'Profile Name' option is displayed on the DEP Profile Window";
		String Fail="FAIL >> 'Profile Name' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value, Pass, Fail);    		    	

		Boolean value1 = SuperviseDevice.isDisplayed();
		String Pass1="PASS >> 'Supervise Device' option is displayed on the DEP Profile Window";
		String Fail1="FAIL >> 'Supervise Device' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value1, Pass1, Fail1);  

		Boolean value2 = IsMandatory.isDisplayed();
		String Pass2="PASS >> 'Is Mandatory' option is displayed on the DEP Profile Window";
		String Fail2="FAIL >> 'Is Mandatory' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value2, Pass2, Fail2);

		Boolean value3 = IsMDMProfileRemovable.isDisplayed();
		String Pass3="PASS >> 'Is MDMProfile Removable' option is displayed on the DEP Profile Window";
		String Fail3="FAIL >> 'Is MDMP rofile Removable' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value3, Pass3, Fail3); 

		Boolean value4 = SupportPhoneNumber.isDisplayed();
		String Pass4="PASS >> 'Support Phone Number' option is displayed on the DEP Profile Window";
		String Fail4="FAIL >> 'Support Phone Number' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value4, Pass4, Fail4);

		Boolean value5 = SupportEmailAddress.isDisplayed();
		String Pass5="PASS >> 'Support Email Address' option is displayed on the DEP Profile Window";
		String Fail5="FAIL >> 'Support Email Address' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value5, Pass5, Fail5);

		Boolean value6 = SkipPasscode.isDisplayed();
		String Pass6="PASS >> 'Skip Passcode' option is displayed on the DEP Profile Window";
		String Fail6="FAIL >> 'Skip Passcode' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value6, Pass6, Fail6);

		Boolean value7 = SkipRestore.isDisplayed();
		String Pass7="PASS >> 'Skip Restore' option is displayed on the DEP Profile Window";
		String Fail7="FAIL >> 'Skip Restore' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value7, Pass7, Fail7);

		Boolean value8 = SkipTOS.isDisplayed();
		String Pass8="PASS >> 'Skip TOS' option is displayed on the DEP Profile Window";
		String Fail8="FAIL >> 'Skip TOS' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value8, Pass8, Fail8);

		Boolean value9 = SkipPayment.isDisplayed();
		String Pass9="PASS >> 'Skip Payment' option is displayed on the DEP Profile Window";
		String Fail9="FAIL >> 'Skip Payment' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(value9, Pass9, Fail9);

		Boolean valu10 = SkipLocation.isDisplayed();
		String Pass10="PASS >> 'Skip Location' option is displayed on the DEP Profile Window";
		String Fail10="FAIL >> 'Skip Location' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(valu10, Pass10, Fail10);

		Boolean valu11 = SkipAppleID.isDisplayed();
		String Pass11="PASS >> 'Skip Apple ID' option is displayed on the DEP Profile Window";
		String Fail11="FAIL >> 'Skip Apple ID' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(valu11, Pass11, Fail11);

		Boolean valu12 = SkipBiometric.isDisplayed();
		String Pass12="PASS >> 'Skip Biometric' option is displayed on the DEP Profile Window";
		String Fail12="FAIL >> 'Skip Biometric' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(valu12, Pass12, Fail12);

		Boolean valu13 = SkipSiri.isDisplayed();
		String Pass13="PASS >> 'Skip Siri' option is displayed on the DEP Profile Window";
		String Fail13="FAIL >> 'Skip Siri' option is NOT displayed on the DEP Profile Window";
		ALib.AssertTrueMethod(valu13, Pass13, Fail13);
		Reporter.log("");
	}
	//Support phone Number and Email
	public void EnterPhoneNumberAndEmailAddress(){
		supportPhnNumber.sendKeys(Config.PhoneNumber);
		supportEmailAddress.sendKeys(Config.Email);
	}

	//*****SkipPasscode Enable check box*****
	public void VerifyEnable_SkipPasscode_CheckBox() throws InterruptedException{
		Reporter.log("=======Verify Enable Skip Passcode Checkboxes==============");
		boolean selected = skipPasscodeCheckBox.isSelected();
		if(!selected){
			skipPasscodeCheckBox.click();
			sleep(3);
		}
		//verify if the check box is selected
		boolean Isselected = skipPasscodeCheckBox.isSelected();
		if(Isselected==true){
			Reporter.log("Checbox has been selected");
		}
	}
	//*****SkipPasscode Disable check box*****
	public void Verify_DisableSkipPasscode_CheckBox() throws InterruptedException{
		Reporter.log("=========Verify Disable Skip Passcode Checkboxes=========");
		boolean selected = skipPasscodeCheckBox.isSelected();
		if(selected =true){
			skipPasscodeCheckBox.click();
			sleep(3);
		}
		//verify if the check box is selected
		boolean Isselected = skipPasscodeCheckBox.isSelected();
		if(!Isselected){
			Reporter.log("Checkbox has been unchecked");
		}
	}
	//*****Skip Restore Enable check box*****
	public void VerifyEnable_SkipRestore_CheckBox() throws InterruptedException{
		Reporter.log("=======Verify Enable Skip Restore Checkboxes==============");
		boolean selected = skipRestoreCheckBox.isSelected();
		if(!selected){
			skipRestoreCheckBox.click();
			sleep(3);
		}
		//verify if the check box is selected
		boolean Isselected = skipRestoreCheckBox.isSelected();
		if(Isselected==true){
			Reporter.log("Checkbox has been selected");
		}
	}
	//*****Skip Restore Disable check box*****
	public void Verify_DisableSkipRestore_CheckBox() throws InterruptedException{
		Reporter.log("=========Verify Disable Skip Passcode Checkboxes=========");
		boolean selected = skipRestoreCheckBox.isSelected();
		if(selected =true){
			skipRestoreCheckBox.click();
			sleep(3);
		}
		//verify if the check box has been unchecked
		boolean Isselected = skipRestoreCheckBox.isSelected();
		if(!Isselected){
			Reporter.log("Checkbox has been unchecked");
		}
	}

	//*****Skip Biometric Enable check box*****
	public void VerifyEnable_SkipBiometric_CheckBox() throws InterruptedException{
		Reporter.log("=======Verify Enable Skip Biometric Checkboxes==============");
		boolean selected = skipBiometricCheckBox.isSelected();
		if(!selected){
			skipBiometricCheckBox.click();
			sleep(3);
		}
		//verify if the check box is selected
		boolean Isselected = skipBiometricCheckBox.isSelected();
		if(Isselected==true){
			Reporter.log("Checkbox has been selected");
			sleep(3);

		}
	}

	//verifying Push DEP Profile header
	public void VerifyPushDEPProfileHeader(){
		Boolean value = PushDEPProfileHeader.isDisplayed();
		String Pass="PASS >> 'Push DEP Profile header' is displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Push DEP Profile header' is NOT displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verifying Push DEP Profile summary
	public void VerifyPushDEPProfileSummary(){
		Boolean value = PushDEPProfileSummary.isDisplayed();
		String Pass="PASS >> 'Push DEP Profile summary' is displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		String Fail="FAIL >> 'Push DEP Profile summary' is NOT displayed under Device 'Enrolment Program' when iOS Enrollment Settings is clicked";
		ALib.AssertTrueMethod(value, Pass, Fail);
	}

	//verify Push 
	public void Push() throws InterruptedException{
		Push.click();
		sleep(5);
		boolean b=true;

		try{
			WarningMessageOnPush.isDisplayed();

		}catch(Exception e)
		{
			b=false;

		}

		Assert.assertTrue(b,"FAIL >> Receiving Push Warning message of notitication failed");
		Reporter.log("PASS >> Notification 'DEP Account not configured.'is displayed - while clicking on PUSH",true );	

	}


	public void VerifyAccountSettingsPage()
	{
		boolean check = PageOfAccountSettings.isDisplayed();
		String pass = "PASS >> Account setting page is displayed";
		String fail = "FAIL >> Account settings page did not display";
		ALib.AssertTrueMethod(check, pass, fail);
	}





	//Data Analytics For SureLock

	@FindBy(xpath="//a[text()='Data Analytics']")
	private WebElement DataAnalyticsTab;

	@FindBy(xpath="//input[@id='EnableDataAnalytics']")
	private WebElement DataAnalyticsCheckBox;

	@FindBy(xpath="//span[text()='SureLock Analytics']/following-sibling::span/input")
	private WebElement SureLockAnalyticsCheckBox;
	
	@FindBy(xpath="//span[text()='MTD App Scan']/following-sibling::span/input")
	private WebElement MTDAppScanAnalyticsCheckBox;

	@FindBy(id="analytics_apply")
	private WebElement DataAnalyticsApplyButton;

	@FindBy(xpath="//span[text()='Settings updated successfully.']")
	private WebElement SettingsUpdateMessage;

	@FindBy(xpath="//p[contains(text(),'enable SureLock Analytics for all devices?')]")
	private WebElement SureLockAnalyticsEnablePopUp;
	
	@FindBy(xpath="//p[contains(text(),'Do you want to enable MTD App Scan for all devices?')]")
	private WebElement MTDAppScanAnalyticsEnablePopUp;

	@FindBy(xpath="//div[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
	private WebElement SureLockAnalyticsEnablePopUpYesButton;

	@FindBy(xpath="//span[text()='SureLock Analytics']/parent::div/parent::div/following-sibling::div/div/div/span[contains(text(),'Show')]")
	private WebElement SureLockAnalyticsShowButton;

	@FindBy(xpath="//span[text()='SureLock Analytics']/parent::div/parent::div/following-sibling::div/div/div/span/span")
	private WebElement SureLockAnalyticsSecretkey;

	@FindBy(xpath="//div[@id='runscriptmodal']/div/div/div[2]/div/div/div/input")
	private WebElement ReunscriptSearchField;

	@FindBy(xpath=".//*[@id='job_name_input']/parent::div/parent::div/following-sibling::div[1]/div/textarea")
	private WebElement RunScriptTextArea;

	@FindBy(xpath="//li[text()='SureLock Analytics']")
	private WebElement SureLockAnalyticsTableList;

	@FindBy(xpath="//div[@id='addSelectedNodes']")
	private WebElement AddSelectedTableButton;

	@FindBy(id="custom_reports_save_btn")
	private WebElement CustomReportSaveButton;

	@FindBy(xpath="//span[text()='On Demand Reports']")
	private WebElement OnDemandReportButton;

	@FindBy(xpath="//section[@id='report_genrateSect']/div/div/div/div/input")
	private WebElement OnDemandReportSearchField;

	@FindBy(xpath="//div[@id='reportDetails']/div/span[2]")
	private WebElement SeletDeviceButton;

	@FindBy(xpath="//span[text()='View Reports']")
	private WebElement ViewReportsButton;

	@FindBy(xpath=".//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[text()='View']")
	private WebElement viewReport;

	@FindBy(xpath="//table[@id='devicetable']/tbody/tr/td[2]")
	private WebElement PackageColumn;

	@FindBy(xpath="//table[@id='devicetable']/tbody/tr/td[3]")
	private WebElement ApplicationNameColumn;

	@FindBy(xpath="//table[@id='devicetable']/tbody/tr/td[4]")
	private WebElement ApplicationEndTimeColumn;

	@FindBy(xpath="//table[@id='devicetable']/tbody/tr/td[5]")
	private WebElement TotalTimeColumn;

	@FindBy(xpath="//div[@id='newanalytics']")
	private WebElement AddAnalyticsButton;


	public void ClickOnDataAnalyticsTab() throws InterruptedException
	{
		DataAnalyticsTab.click();
		waitForXpathPresent("//span[text()='Enable Data Analytics']");
		System.out.println("PASS >> Clicked on Data Analytics");
		sleep(3);
	}

	public void EnableDataAnalyticsCheckBox()
	{
		boolean flag = DataAnalyticsCheckBox.isSelected();
		if(flag)
		{
			Reporter.log("Data Analytics CheckBoxIs Already Enabled",true);
		}
		else
		{
			DataAnalyticsCheckBox.click();
			Reporter.log("Enabled Data Analytics CheckBox",true);
		}
	}

	public void DisableSureLockAnalyticsCheckBox()
	{
		boolean flag=SureLockAnalyticsCheckBox.isSelected();
		if(flag)
		{
			Reporter.log("SureLock Analytics CheckBoxIs Already Enabled Hence Disabling SureFox Analytics",true);
			SureLockAnalyticsCheckBox.click();
		}
		else
		{
			Reporter.log("SureLOck Analytics CheckBoxIs Already Disabled",true);
		}
	}
	public void DisableMTDAnalyticsCheckBox()
	{
		boolean flag=MTDAppScanAnalyticsCheckBox.isSelected();
		if(flag)
		{
			Reporter.log("MTDAppScan CheckBoxIs Already Enabled Hence Disabling MTDAppScan Analytics",true);
			MTDAppScanAnalyticsCheckBox.click();
		}
		else
		{
			Reporter.log("MTDAppScan Analytics CheckBoxIs Already Disabled",true);
		}
	}


	public void ClickOnDataAnalyticsApplyButton() throws InterruptedException
	{
		boolean flag;
		DataAnalyticsApplyButton.click();
		sleep(3);
		waitForXpathPresent("//span[text()='Settings updated successfully.']");
		try
		{
			SettingsUpdateMessage.isDisplayed();
			flag=true;
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="PASS >>>Settings Updated Message Displayed Successfully";
		String Fail="FAIL >>>Settings Updated Message Isn't Displayed Successfully";
		ALib.AssertTrueMethod(flag, Pass, Fail);
		sleep(3);
	}

	public void VerifySureLockAnalyticsEnablePopUp()
	{
		boolean flag = SureLockAnalyticsEnablePopUp.isDisplayed();
		String Pass="PASS >>>SureLock Analytics Enable PopUp Displayed Successfully";
		String Fail="FAIL >>>SureLock Analytics Enable PopUp Isn't Displayed Successfully";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}
	public void VerifyMTDAppScanAnalyticsEnablePopUp()
	{
		boolean flag = MTDAppScanAnalyticsEnablePopUp.isDisplayed();
		String Pass="PASS >>>MTDAppScan Analytics Enable PopUp Displayed Successfully";
		String Fail="FAIL >>>MTDAppScan Analytics Enable PopUp Isn't Displayed Successfully";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}
	public void EnablingSureLockAnalyticsChckBox() throws InterruptedException
	{
		JavascriptExecutor Jc=(JavascriptExecutor)Initialization.driver;
		Jc.executeScript("arguments[0].scrollIntoView()",SureLockAnalyticsCheckBox);
		sleep(3);
		SureLockAnalyticsCheckBox.click();
	}
	public void EnablingMTDAppScanAnalyticsChckBox() throws InterruptedException
	{
		JavascriptExecutor Jc=(JavascriptExecutor)Initialization.driver;
		Jc.executeScript("arguments[0].scrollIntoView()",MTDAppScanAnalyticsCheckBox);
		sleep(3);
		MTDAppScanAnalyticsCheckBox.click();
	}
	public void ClcikOnSureLockAnalyticsEnablePopUpYesButton() throws InterruptedException
	{
		SureLockAnalyticsEnablePopUpYesButton.click();
		sleep(2);
		waitForXpathPresent("//span[text()='SureLock Analytics']/following-sibling::span/input");
		Reporter.log("Clicked On Analytics PopUp Yes Button",true);
	}
	
	public void waitForLogXpathPresent(String wbXpath)
	{
		WebDriverWait wait = new WebDriverWait(Driver.driver,270);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(wbXpath)));		
	}

	public void ClickOnSureLockSettings() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureLock Settings']").click();
		sleep(3);
		Reporter.log("Clicked On SureLock Settings Button",true);
	}

	CommonMethods_POM common=new CommonMethods_POM();
	public void ClickOnSureLockAnalytics(String Text) throws InterruptedException
	{
		common.commonScrollMethod(Text);
		sleep(2);
		common.ClickOnAppiumText(Text);
		Reporter.log("Clicked On SureLock Analytics Button",true);
	}



	public void VerifyEnableAppAnalyticsCheckBixEnabled()
	{
		boolean flag = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Enable Schedule Export']").isEnabled();
		String Pass="PASS >>>Enable SureLock Analytics Is Applied On Device Successfully";
		String Fail="FAIL >>>Enable SureLock Analytics Isn't Applied On Device Successfully";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void ClickOnEnableAppAnalyticsPopUpOkButton() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK' and @index='0']").click();
		sleep(2);
		Reporter.log("Clicked On Enable AppAnalytics PopUp Ok Button",true);
	}	      
	public void ClickOnSureLockSecretKeyShowButton() throws InterruptedException
	{
		SureLockAnalyticsShowButton.click();
		waitForidPresent("analytics_apply");
		sleep(3);
		Reporter.log("Clicked On SureLock Secret Key Show Button",true);
	}

	@FindBy(xpath="//input[@id='saveEfota']")
	private WebElement SaveFirmware;
	public void ClickOnSaveFirmware() throws InterruptedException {
		SaveFirmware.click();
		Reporter.log("PASS >> Clicked on Save button", true);
		sleep(2);
	}

	@FindBy(xpath="//span[text()='Client Secret ID']")
	private WebElement ClientSecretIDText;

	@FindBy(xpath="//li[@id='firmware_updates']")
	private WebElement FirmWareUpdtesTab;

	@FindBy(xpath="//span[text()='Client ID']")
	private WebElement ClientIDText;

	@FindBy(xpath="//span[text()='Customer ID']")
	private WebElement CustomerIDText;

	@FindBy(xpath="//span[text()='License']")
	private WebElement LicenseText;

	public void ClearAllFieldsOfFirmware() throws InterruptedException {
		samsungLicenseId.clear();
		Reporter.log("Cleared samsung LicenseId", true);
		sleep(2);
		samsungCustomerId.clear();
		Reporter.log("Cleared samsung CustomerId", true);
		sleep(2);
		samsungClientId.clear();
		Reporter.log("Cleared samsung ClientId", true);
		sleep(2);
		samsungSecretId.clear();
		Reporter.log("Cleared samsung SecretId", true);
		sleep(2);
	}
	public void EnterAllFieldsWithoutLicenseID() throws InterruptedException {
		samsungCustomerId.sendKeys(Config.CustomerID_Efota);
		samsungClientId.sendKeys(Config.ClientID_Efota);
		samsungSecretId.sendKeys(Config.ClientSecretID_Efota);
		Reporter.log("Entered All fields except License ID field", true);
		sleep(2);
	}

	@FindBy(xpath="//textarea[@id='eulaDisclaimerPloicyText']")
	private WebElement EulatextArea;
	public void ClearDisclaimerPolicyText() throws InterruptedException {
		EulatextArea.clear();
		sleep(1);
	}

	@FindBy(xpath="//input[@id='chkbox_eulaDisclaimer']")
	private WebElement selectEulaDisclaimer;
	public void selectUseEulaDisclaimer() throws InterruptedException {
		boolean flag=selectEulaDisclaimer.isSelected();
		if(flag==false) {
			selectEulaDisclaimer.click();
		}
		Reporter.log("selected Eula Disclaimer policy",true);
		sleep(2);
	}
	@FindBy(xpath="//input[@id='eula_policy']")
	private WebElement EulaSaveButton;
	public void VerifySaveButton() {
		boolean flag=EulaSaveButton.isDisplayed();
		String PassStatement="PASS >> Eula Disclaimer Save button is displaying";
		String FailStatement="FAIL >> Eula Disclaimer Save button is not displaying";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	@FindBy(xpath="//span[text()='Please enter a license ID.']")
	private WebElement AllFieldsBlank;
	public void VerifyConfirmationMsgWhenAllFieldsBlank() throws InterruptedException {
		boolean flag = AllFieldsBlank.isDisplayed();
		String PassStatement="PASS >> License ID cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >> License ID cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}
	public void VerifyAllFeaturesOption() {
		boolean flag = samsungLicenseId.isDisplayed();
		String PassStatement="PASS >> Samsung "+LicenseText.getText()+" is displayed.";
		String FailStatement="FAIL >> Samsung "+LicenseText.getText()+" is not displayed.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

		boolean flag1 = samsungCustomerId.isDisplayed();
		String PassStatement1="PASS >> Samsung "+CustomerIDText.getText()+" is displayed.";
		String FailStatement1="FAIL >> Samsung "+CustomerIDText.getText()+" is not displayed.";
		ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);

		boolean flag2 = samsungClientId.isDisplayed();
		String PassStatement2="PASS >> Samsung "+ClientIDText.getText()+" is displayed.";
		String FailStatement2="FAIL >> Samsung "+ClientIDText.getText()+" is not displayed.";
		ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);

		boolean flag3 = samsungSecretId.isDisplayed();
		String PassStatement3="PASS >> Samsung "+ClientSecretIDText.getText()+" is displayed.";
		String FailStatement3="FAIL >> Samsung "+ClientSecretIDText.getText()+" is not displayed.";
		ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);

		boolean flag4 = SaveFirmware.isDisplayed();
		String PassStatement4="PASS >> Save button is displayed.";
		String FailStatement4="FAIL >> Save button is not displayed.";
		ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);

	}

	public void ClickOnFirmwareUpdates() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, FirmWareUpdtesTab);
		FirmWareUpdtesTab.click();
		waitForXpathPresent("//input[@id='saveEfota']");
		sleep(2);
	}
	@FindBy(xpath="//input[@id='samsungLicenseId']")
	private WebElement samsungLicenseId;
	public void EnterAllFieldsWithoutCustomerID() throws InterruptedException {
		samsungLicenseId.sendKeys(Config.license_Efota);
		samsungCustomerId.clear();
		Reporter.log("PASS >> Entered All fields except Customer ID field", true);
		sleep(2);
	}
	@FindBy(xpath="//span[text()='Please enter a customer ID.']")
	private WebElement WithoutCustomerID;
	public void VerifyConfirmationMsgWhenCustomerIDFieldBlank() {
		boolean flag = WithoutCustomerID.isDisplayed();
		String PassStatement="PASS >> Customer ID cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >> Customer ID cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	@FindBy(xpath="//input[@id='samsungSecretId']")
	private WebElement samsungSecretId;

	@FindBy(xpath="//input[@id='samsungClientId']")
	private WebElement samsungClientId;

	@FindBy(xpath="//span[text()='Please enter a client ID.']")
	private WebElement WithoutClientID;

	@FindBy(xpath="//input[@id='samsungCustomerId']")
	private WebElement samsungCustomerId;
	public void EnterAllFieldsWithoutClientID() throws InterruptedException {
		samsungCustomerId.sendKeys(Config.CustomerID_Efota);
		samsungClientId.clear();
		Reporter.log("PASS >> Entered All fields except Client ID field", true);
		sleep(2);
	}

	public void VerifyConfirmationMsgWhenCleintIDFieldBlank() {
		boolean flag = WithoutClientID.isDisplayed();
		String PassStatement="PASS >> Client ID cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >> Client ID cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	@FindBy(xpath="//span[text()='Please enter a client secret ID.']")
	private WebElement WithoutSecretClientID;
	public void VerifyConfirmationMsgWhenSecretCleintIDFieldBlank() {
		boolean flag = WithoutSecretClientID.isDisplayed();
		String PassStatement="PASS >> Secret Client ID cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >> Secret Client ID cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	public void EnterAllFieldsWithoutSecretClientID() throws InterruptedException {
		samsungClientId.sendKeys(Config.ClientID_Efota);
		samsungSecretId.clear();
		Reporter.log("PASS >> Entered All fields except Secret client ID field", true);
		sleep(2);
	}

	public String SureLockSecretKey;
	public void ReadingSureLockSecretKeyFromConsole()
	{
		boolean flag;
		SureLockSecretKey = SureLockAnalyticsSecretkey.getText();
		System.out.println(SureLockSecretKey);
		if(SureLockSecretKey.isEmpty() || SureLockSecretKey==null)
		{
			flag=false;
		}
		else
		{
			flag=true;
		}
		String Pass="PASS >>>SureLock Secret Key Is Not Empty And Key Is Displayed";
		String Fail="FAIL >>>SureLock Secret Key Is Empty";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void VerifySureLockSecretKeyForDifferentAcc()
	{
		System.out.println(SureLockSecretKey);
		boolean flag = Config.Feb8AccSureLockSecretKey.equals(SureLockSecretKey);
		String Pass="SureLock Secret Key Is Different For Different Accounts";
		String Fail="SureLock Secret Key Is Same For Different Accounts";
		ALib.AssertFalseMethod(flag, Pass, Fail);
	}

	public void SearchingRunScripts(String RunScript) throws InterruptedException
	{
		ReunscriptSearchField.sendKeys(RunScript);
		waitForXpathPresent("//span[contains(text(),'analytics of SureLock')]");
		sleep(2);
	}

	public void ClickOnSearchedRunScript() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//span[contains(text(),'analytics of SureLock')]")).click();
		waitForXpathPresent("//button[@id='ValidateBtn']");
		sleep(2);
	}

	public void ClickOnEnableScheduleExportInSureLock() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Enable Schedule Export']").click();
		sleep(3);
	}

	public void ReadingSecretKeyFromDevice()
	{
		boolean flag;
		String DeviceSureLOckSecretKey = Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='4']").getText();
		System.out.println(DeviceSureLOckSecretKey);
		if(DeviceSureLOckSecretKey.equals(SureLockSecretKey))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="Sure Lock Key Is Same In SureLock And Console";
		String Fail="Sure Lock Key Is Different In SureLock And Console";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void WaitForElementInAppium(String MobileElement)
	{
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,45);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(MobileElement)));
	}

	public void clickonEnableScheduleReportPpopUpOkButton() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK' and @index='0']").click();
		sleep(2);
	}

	public void addingApplicationToSureLockHomeScreen(String PackageName) throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec("adb shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e password 0000 -e command add_application -e package_name " + PackageName + " com.gears42.surelock");
		sleep(3);
	}

	public void LaunchingShromeInsideSureLock() throws IOException, InterruptedException
	{
		Reporter.log("Launching Chrome Inside SureLock",true);
		//	  WaitForElementInAppium("//android.widget.TextView[@text='Chrome']");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Chrome']").click();
		sleep(10);
		Runtime.getRuntime().exec("adb shell input keyevent 3");
		sleep(3);    	  
	}

	public void LaunchingPlaystoreInsideSureLock() throws InterruptedException, IOException
	{
		Reporter.log("Launching PlayStore Inside SureLock",true);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Play Store']").click();
		sleep(10);
		Runtime.getRuntime().exec("adb shell input keyevent 3");	    	  
		sleep(3);
	}

	public void launchingNixInsideSureLock() throws IOException, InterruptedException
	{
		Reporter.log("Launching Nix Inside SureLock",true);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='SureMDM Nix']").click();
		sleep(7);
		Runtime.getRuntime().exec("adb shell input keyevent 3");	    	  
		sleep(3);
	}

	public void sendingRunScript(String RunScript) throws InterruptedException
	{
		RunScriptTextArea.sendKeys(RunScript);
		sleep(3);
	}

	public void checkingSureLockAnalyticsTableInReports() throws InterruptedException
	{
		JavascriptExecutor jc=(JavascriptExecutor)Initialization.driver;
		jc.executeScript("arguments[0].scrollIntoView()", SureLockAnalyticsTableList);
		sleep(4);
		SureLockAnalyticsTableList.click();
		waitForXpathPresent("//div[@id='addSelectedNodes']");
		sleep(2);
	}

	public void ClickOnAddSelectedTable() throws InterruptedException
	{
		AddSelectedTableButton.click();
		waitForXpathPresent("//div[@id='addSelectedNodes']");
		sleep(3);
	}

	public void ClickOnCustomReportSaveButton() throws InterruptedException
	{
		CustomReportSaveButton.click();
		waitForXpathPresent("//span[text()='Custom report saved.']");
		sleep(2);
		Reporter.log("Custom Report Created Successfully");
	}

	public void ClickOnOnDemandReport()
	{
		OnDemandReportButton.click();
	}

	public void SearchingForReportInOnDemandReport(String ReportName) throws InterruptedException
	{
		OnDemandReportSearchField.clear();
		sleep(2);
		OnDemandReportSearchField.sendKeys(ReportName);
		waitForXpathPresent("(//span[text()='"+ReportName+"'])");
		sleep(2);
		Initialization.driver.findElement(By.xpath("(//table[@id='reportsGrid']/tbody/tr)[last()]")).click();
		waitForidPresent("generateReportBtn");
		sleep(7);
	}

	public void ClickOnSelectDeviceButtonInReports() throws InterruptedException
	{
		SeletDeviceButton.click();
		waitForXpathPresent("//h4[text()='Add Device Or Group']");
		sleep(3);
	}

	public void SearchingForDeviceInOnDemandReport(String DeviceName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='dGridContainer']/div[1]/div[1]/div[2]/input")).sendKeys(DeviceName);
		waitForXpathPresent("//p[text()='"+DeviceName+"']");
		sleep(15);
		Initialization.driver.findElement(By.xpath("//div[@id='dGridSection']//p[text()='"+DeviceName+"']")).click();
	}

	public void ClickOnAddDeviceButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='deviceGroupTable']/div/div/div[3]/button")).click();
		waitForidPresent("generateReportBtn");
		sleep(3);
	}

	public void ClickONRequestReportButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.id("generateReportBtn")).click();
		waitForXpathPresent("//span[contains(text(),'The request to generate this report has been added to the queue. To view the status of the request, please click on View Reports.')]");
		sleep(5);//The request to generate this report has been added to the queue. To view the status of the request, please click on View Reports.
	}

	public void ClickOnViewReportButton() throws InterruptedException
	{

		ViewReportsButton.click();
		waitForXpathPresent("//div[@id='deleteReport']");
		sleep(7);
		Reporter.log("Clicked On View Reports",true);
	}

	@FindBy(xpath="//section[@id='report_viewSect']/div/div[2]/div/div/button")
	private WebElement ReportRefreshButton;

	public void ClickOnRefreshInViewReport() throws InterruptedException
	{
		ReportRefreshButton.click();
		waitForXpathPresent("//*[@id='OfflineReportGrid']/tbody/tr[1]/td[5]/a[2]");
		sleep(4);

	}


	public void ClickOnView(String ReportName) throws InterruptedException
	{
		Reporter.log("Waiting For Report To Generate",true);
		waitForElementPresent("//table[@id='OfflineReportGrid']/tbody/tr[1]/td/p[text()='"+ReportName+"']/parent::td/following-sibling::td[4]/a[text()='View']");
		sleep(3);
		viewReport.click();
		sleep(4);
	}

	String originalHandle;
	public void WindowHandle() throws InterruptedException
	{
		originalHandle = Initialization.driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		sleep(10);
	}

	public void DeviceNameColumnVerificationDeviceHealthReport(String DeviceName)
	{
		List<WebElement>ls = Initialization.driver.findElements(By.xpath("//*[@id='devicetable']/tbody/tr/td[1]/p"));
		if(ls.size()==0) {
			ALib.AssertFailMethod("FAIL >> Device is not present with the expected name");
		}
		boolean value;
		for (int i = 0; i < ls.size(); i++) 
		{
			String ActualValue = ls.get(i).getText();
			value=ActualValue.equals(DeviceName);
			try
			{
				if(value==true) {
				 Reporter.log("PASS >> Device Name Is Shown Correctly In Report",true); }
			}catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> Device Name Is Not Shown Correctly");
			}
		}
	}

	public void SearchUsedApplicationORWebSiteInReport(String AppName) throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='reportList_tableCont']/section/div[2]/div/div[2]/input")).clear();
		sleep(2);
		try {
			Initialization.driver.findElement(By.xpath("//div[@id='reportList_tableCont']/section/div[2]/div/div[2]/input")).sendKeys(AppName);
			waitForXpathPresent("//p[text()='"+AppName+"']");
			sleep(2);
		}
		catch(Exception e)
		{
			SwitchBackWindow();
			ALib.AssertFailMethod("Data Is Not Generated In The Report");
		}
	}

	public void VerifyPackageNameInSureLockAnalytics(String PackageName)
	{
		String ReportPackageName = PackageColumn.getText();
		boolean Val = ReportPackageName.equals(PackageName);
		String Pass="Application Package Name Is Showing Correctly";
		String Fail="Application Package Name Isn't Showing Correctly";
		ALib.AssertTrueMethod(Val, Pass, Fail);
	}

	public void VerifyApplicationName(String ApplicationName)
	{
		String ReportAppName = ApplicationNameColumn.getText();
		boolean Val=ReportAppName.equals(ApplicationName);
		String Pass="Application Name Is Showing Correctly";
		String Fail="Application Name Isn't Showing Correctly";
		ALib.AssertTrueMethod(Val, Pass, Fail);
	}

	public void VerifyApplicationEndTime()
	{
		String ReportEndTime = ApplicationEndTimeColumn.getText();
		boolean Val=ReportEndTime.contains("N/A");
		String Pass="Application EndTime Is Showing Correctly";
		String Fail="Application EndTime Isn't Showing Correctly";
		ALib.AssertFalseMethod(Val, Pass, Fail);
	}

	public void VerifyApplicationTotalTime()
	{
		boolean Val;
		String AppTotalTime=TotalTimeColumn.getText();
		System.out.println(AppTotalTime);
		if(AppTotalTime.equals("N/A")||AppTotalTime.equals(""))
		{
			Val=false;
		}
		else
		{
			Val=true;
		}
		String Pass="Application Total Time Is Showing Correctly";
		String Fail="Application Total Time Isn't Showing Correctly";
		ALib.AssertTrueMethod(Val, Pass, Fail);
	}

	public void SwitchBackWindow() throws InterruptedException
	{

		Initialization.driver.close();
		Initialization.driver.switchTo().window(originalHandle);
		sleep(10); 
	}
	
	public void SwitchBackWindow1() throws InterruptedException
	{

		//Initialization.driver.close();
		Initialization.driver.switchTo().window(originalHandle);
		sleep(10); 
	}

	public void ReadingConsoleMessageForJobDeployment(String JobName,String DeviceName) throws InterruptedException
	{
	//	waitForLogXpathPresent("//p[contains(text(),'The job named "+"\""+JobName+"\" has been applied to the device known as "+"\""+DeviceName+"\"')]");
		waitForLogXpathPresent("//p[contains(text(),'The job named "+"\""+JobName+"\" was successfully deployed on the device named "+"\""+DeviceName+"\"')]");
		Reporter.log("Job applied Message Is Displayed In The Console",true);
		sleep(5);
	}
	public void ReadingConsoleMessageForSuccessfulJobDeployment(String JobName,String DeviceName)
	{
		waitForLogXpathPresent("//p[contains(text(),'The job named "+"\""+JobName+"\" was successfully deployed on the device named "+"\""+DeviceName+"\"')]");
		Reporter.log("Job Deployed Message Is Displayed In The Console",true);
	}
	public void ReadingConsoleMessageForJobDeploymentOnGroup(String JobName,String DeviceName)
	{
		waitForLogXpathPresent("//p[contains(text(),'The job named "+"\""+JobName+"\" was successfully deployed on the device named "+"\""+DeviceName+"\"')]");
		Reporter.log("Job Deployed Message Is Displayed In The Console",true);
	}
	public void VerifyEnableScheduleExportIsDisabled()
	{
		boolean flag;
		try
		{
			Initialization.driver.findElement(By.xpath("//android.widget.TextView[@text='Enable Schedule Export']")).isDisplayed();
			flag=false;
		}
		catch(Exception e)
		{
			flag=true;
		}
		String Pass="PASS >>>Verify Enable Schedule Report Is Disabled As Disble SureLock Analytics Runscript Applied On Device";
		String Fail="FAIL >>>Disble SureLock Analytics Runscript Is Not Applied On Device";
		ALib.AssertTrueMethod(flag, Pass, Fail);

	}

	// 		Data Analytics For SureFox


	@FindBy(xpath="//span[text()='SureFox Analytics']/following-sibling::span/input")
	private WebElement SureFoxAnalyticsCheckBox;

	@FindBy(xpath="//p[contains(text(),'enable SureFox Analytics for all devices?')]")
	private WebElement SureFoxEnablePopup;

	@FindBy(xpath="//span[text()='SureFox Analytics']/parent::div/parent::div/following-sibling::div/div/div/span[contains(text(),'Show')]")
	private WebElement SureFoxSecretKeyShowButton;

	@FindBy(xpath="//span[text()='SureFox Analytics']/parent::div/parent::div/following-sibling::div/div/div/span/span")
	private WebElement SureFoxAnalyticsSecretKey;

	@FindBy(xpath="//li[text()='SureFox Analytics']")
	private WebElement SureFoxAnalyticsTableList;

	@FindBy(xpath="//table[@id='devicetable']/tbody/tr/td[5]")
	private WebElement SFAnalyticsClosedTimeColumn;

	@FindBy(xpath="//table[@id='devicetable']/tbody/tr/td[6]")
	private WebElement SFAnalyticsBrowsedTimeColumn;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[2]/div/input")
	private WebElement NewlyCreatedAnalyticsNameField;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[3]/div/input")
	private WebElement NewlyCreatedAnalyticsTagNameField;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[4]/div/div[2]/div")
	private WebElement AddFiledsButton;

	@FindBy(xpath="//div[@id='analytics_popup']/div/div/div[2]/div/div/input")
	private WebElement FieldName;

	@FindBy(xpath="//select[@id='field_type_value']")
	private WebElement FieldTypeDropDown;

	@FindBy(xpath="//div[@id='analytics_popup']/div/div/div/button[text()='Add']")
	private WebElement AddFiledsAddButton;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[1]/span[2]/a")
	private WebElement NewlyCreatedAnalyticsSaveButton;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[1]/span[1]")
	private WebElement NewlyCreatedAnalyticsRemoveButton;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[5]/div[1]/span[2]/input")
	private WebElement NewlyCreatedAnalyticsEnableCheckBox;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[5]/div[2]/div/span[1]")
	private WebElement NewlyCreatedAnalyticsSecretKeyShowButton;

	@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[5]/div[2]/div/span[2]/span")
	private WebElement NewlyCreatedAnalyticsSecretKey;

	@FindBy(xpath="//li[text()='Content Blocking Analytics']")
	private WebElement ContentBlockingTableList;

	public void DisablingSureFoxAnalytics() throws InterruptedException
	{
		boolean flag=SureFoxAnalyticsCheckBox.isSelected();
		if(flag)
		{
			Reporter.log("SureFox Analytics CheckBoxIs Already Enabled Hence Disabling SureFox Analytics",true);
			SureFoxAnalyticsCheckBox.click();
			sleep(3);
			ClickOnDataAnalyticsApplyButton();

		}
		else
		{
			Reporter.log("SureFox Analytics CheckBoxIs Already Disabled",true);
		}
	}

	public void EnablingSureFoxAnalytics() throws InterruptedException 
	{
		JavascriptExecutor Jc=(JavascriptExecutor)Initialization.driver;
		Jc.executeScript("arguments[0].scrollIntoView()",SureFoxAnalyticsCheckBox);
		sleep(3);
		SureFoxAnalyticsCheckBox.click();
	}

	public void VerifySureFoxAnalyticsEnablePopup()
	{
		boolean flag = SureFoxEnablePopup.isDisplayed();
		String Pass="PASS >>>SureFox Analytics Enable PopUp Displayed Successfully";
		String Fail="FAIL >>>SureFox Analytics Enable PopUp Isn't Displayed Successfully";
		ALib.AssertTrueMethod(flag, Pass, Fail);

	}

	public void SearchingInsideSureFox(String SearchContent) throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.ImageView[@index='2']").click();
		try {
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='0']").sendKeys(SearchContent);
		}catch(Exception e) {
			Initialization.driverAppium.findElementById("android:id/search_src_text").sendKeys(SearchContent);
		}
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+SearchContent+"']").click();
		sleep(4);
	}

	public void VerifyScheduleExportIsEnabledAfterEnablingSFDataAnalytics() throws InterruptedException
	{
		boolean val = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Schedule Export' and @index='0']").isEnabled();
		String Pass="PASS >>>Enable SureFox Analytics Is Applied On Device Successfully";
		String Fail="FAIL >>>Enable SureFox Analytics Isn't Applied On Device Successfully";
		ALib.AssertTrueMethod(val, Pass, Fail);
		sleep(3);
	}

	public void ExitFromSureLock() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Exit SureFox']").click();
		sleep(3);
		Reporter.log("Clicked On Exit SureLock Button",true);
	}

	public boolean VerifySureFoxAnalyticsCheckBox()
	{
		boolean flag;
		String Val = Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox[@index='0']").getAttribute("checked");
		if(Val.equals("true"))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		String Pass="PASS >>>SureFox Analytics CheckBox Is Enabled On Applying SureFox Enable Analytics Runscript";
		String Fail="FAIL >>>SureFox Analytics CheckBox Isn't Enabled On Applying SureFox Enable Analytics Runscript";
		ALib.AssertTrueMethod(flag, Pass, Fail);
		return flag;
	}

	public void ClickOnSureFoxSecretKeyShowButton() throws InterruptedException
	{
		SureFoxSecretKeyShowButton.click();
		waitForidPresent("analytics_apply");
		sleep(3);
		Reporter.log("Clicked On SureFox Secret Key Show Button",true);
	}

	public String SureFoxSecretKey;
	public void ReadingSureFoxSecretKeyFromConsole()
	{
		boolean flag;
		SureFoxSecretKey = SureFoxAnalyticsSecretKey.getText();
		System.out.println(SureFoxSecretKey);
		if(SureFoxSecretKey.isEmpty() || SureFoxSecretKey==null)
		{
			flag=false;
		}
		else
		{
			flag=true;
		}
		String Pass="PASS >>>SureFox Secret Key Is Not Empty And Key Is Displayed";
		String Fail="FAIL >>>SureFox Secret Key Is Empty";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void ClickOnAllowedWebsites()
	{
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,30);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Allowed Websites']").click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Add URL']")));
		Reporter.log("Clicked On Allowed Website",true);
	}

	public void ClicOnAddURLButton() throws InterruptedException
	{
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,30);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Add URL']").click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.EditText[@text='www.' and @index='1']")));
		sleep(3);
		Reporter.log("Clicked On Add URL Button",true);
	}

	public void SendingWebSites(String WebSite) throws InterruptedException, IOException
	{
		Reporter.log("Sending Websites To Be Added",true);
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@text='www.' and @index='1']").sendKeys(WebSite);
		sleep(3);
		Runtime.getRuntime().exec("adb shell input keyevent 4");
		sleep(3);
	}

	public void ClickOnDoneButtonOnAddingURL(String Text) throws InterruptedException
	{
		common.commonScrollMethod(Text);
		common.ClickOnAppiumButtons(Text);
		Reporter.log("Clicked On Done Button After Adding URL",true);
	}

	public void ClickOnDoneButtonOnFinishingAddURL() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Done' and @index='1']").click();
		sleep(3);
	}

	public void OpeningWebSitesInSureFox(String Website) throws InterruptedException, IOException
	{
		Initialization.driverAppium.findElementByXPath("//android.view.View[@text='"+Website+"']").click();
		sleep(30);
		Runtime.getRuntime().exec("adb shell input keyevent 4");
		sleep(4);
	}

	public void checkingSureFoxAnalyticsTableInReports() throws InterruptedException
	{
		JavascriptExecutor jc=(JavascriptExecutor)Initialization.driver;
		jc.executeScript("arguments[0].scrollIntoView()", SureFoxAnalyticsTableList);
		sleep(4);
		SureFoxAnalyticsTableList.click();
		waitForXpathPresent("//div[@id='addSelectedNodes']");
		sleep(2);
	}

	public void VerifyURLInSFAnalyticsReport(String URL) throws InterruptedException
	{

		try
		{

			String ConsoleURL = ApplicationNameColumn.getText();
			boolean Val = ConsoleURL.contains(URL);
			String Pass="URL  Is Showing Correctly";
			String Fail="URL Isn't Showing Correctly";
			ALib.AssertTrueMethod(Val, Pass, Fail);
		}
		catch (Exception e) 
		{
			SwitchBackWindow();
			ALib.AssertFailMethod("Data Not Generated In Report");
		}
	}

	public void VerifyWebSiteOpenedTimeInSFAnalyticsReport() throws InterruptedException
	{
		try
		{
			boolean flag;
			String ConsoleWebsiteopenedTime = ApplicationEndTimeColumn.getText();
			if(ConsoleWebsiteopenedTime.equals("N/A") || ConsoleWebsiteopenedTime.equals(""))
			{
				flag=false;
			}
			else
			{
				flag=true;
			}
			String Pass="WebSite Opened Time Is Showing";
			String Fail="WebSite Opened Time Isn't Showing Correctly";
			ALib.AssertTrueMethod(flag, Pass, Fail);
		}
		catch(Exception e)
		{
			SwitchBackWindow();
			ALib.AssertFailMethod("Data Not Generated In Report");
		}

	}

	public void VerifyWebsiteCloseTime() throws InterruptedException
	{
		try
		{
			boolean flag;
			String ConsoleWebsiteClosedTime = SFAnalyticsClosedTimeColumn.getText();
			if(ConsoleWebsiteClosedTime.equals("N/A")||ConsoleWebsiteClosedTime.equals(""))
			{
				flag=false;
			}
			else
			{
				flag=true;
			}
			String Pass="WebSite Closed Time Is Showing";
			String Fail="WebSite Closed Time Isn't Showing ";
			ALib.AssertTrueMethod(flag, Pass, Fail);
		}
		catch (Exception e) 
		{
			SwitchBackWindow();
			ALib.AssertFailMethod("Data Not Generated In Report");
		}
	}

	public void VeirfyWebsiteBrowsedTime() throws InterruptedException
	{
		try
		{
			boolean flag;
			String ConsoleBrowsedTime = SFAnalyticsBrowsedTimeColumn.getText();
			if(ConsoleBrowsedTime.equals("N/A")||ConsoleBrowsedTime.equals("")||ConsoleBrowsedTime.equals("0"))
			{
				flag=false;
			}
			else
			{
				flag=true;
			}
			String Pass="Browsed Time Is Showing";
			String Fail="Browsed Time Isn't Showing ";
			ALib.AssertTrueMethod(flag, Pass, Fail);
		}
		catch (Exception e) 
		{

			SwitchBackWindow();
			ALib.AssertFailMethod("Data Not Generated In Report");
		}
	}

	public void VerifySureFoxAnalyticsCheckBoxAfterApplyingDisableSFAnalyticsRunScript()
	{
		boolean flag;
		String Val = Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox[@index='0']").getAttribute("checked");
		if(Val.equals("true"))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}

		String Pass="PASS >>>SureFox Analytics CheckBox Is Disabled On Applying SureFox Disable Analytics Runscript";
		String Fail="FAIL >>>SureFox Analytics CheckBox Isn't Disabled On Applying SureFox Disable Analytics Runscript";
		ALib.AssertFalseMethod(flag, Pass, Fail);

	}

	public void UninstallingSurefox(String Text,String Package) throws IOException, InterruptedException
	{
		System.out.println("Uninstalling Application");
		Runtime.getRuntime().exec("adb shell am start -S \"com.android.settings/.Settings\\$DeviceAdminSettingsActivity\"");
		sleep(3);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Text+"']").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Deactivate']").click();
		sleep(2);
		Initialization.driverAppium.removeApp(Package);
		sleep(3);
		Runtime.getRuntime().exec("adb shell input keyevent 3");
	}

	public void ClickOnAddAnalyticsButton()
	{
		AddAnalyticsButton.click();
	}

	public void SendingDataAnalyticsName(String DataAnalyticsName) throws InterruptedException
	{
		NewlyCreatedAnalyticsNameField.sendKeys(DataAnalyticsName);
		sleep(2);
	}

	public void SendingTagName(String TagName) throws InterruptedException
	{
		NewlyCreatedAnalyticsTagNameField.sendKeys(TagName);
		sleep(2);
	}

	public void ClickOnAddFiledButton() throws InterruptedException
	{
		AddFiledsButton.click();
		waitForXpathPresent("//h4[text()='Add Field']");
		sleep(2);
	}

	public void SendingFieldName(String ContentBlockingFieldName) throws InterruptedException
	{
		FieldName.sendKeys(ContentBlockingFieldName);
		sleep(3);
	}

	public void SelectingFieldType(String Value) throws InterruptedException
	{
		Select sel=new Select(FieldTypeDropDown);
		sel.selectByValue(Value);
		sleep(3);
	}

	public void ClickingonAddFieldAddButton() throws InterruptedException
	{

		AddFiledsAddButton.click();
		waitForXpathPresent("//div[@id='newanalytics']");
		sleep(3);
	}

	public void ClickonAnalyticsSaveButton()
	{
		NewlyCreatedAnalyticsSaveButton.click();
		try
		{
			waitForLogXpathPresent("//span[text()='Analytics Settings saved successfully.']");
			Reporter.log("Analytics Settings saved successfully Message Displayed",true);
			waitForXpathPresent("//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[5]/div[2]/div/span[1]");
			sleep(5);
		}
		catch(Exception e)
		{
			String FailMessage="Analytics Settings saved successfully Message Isn't Displayed";
			ALib.AssertFailMethod(FailMessage);
		}
	}

	public void ClickOnExitSureFoxkButton(String Text) throws InterruptedException
	{
		common.commonScrollMethod(Text);
		common.ClickOnAppiumText(Text);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Exit SureFox']").click();
		sleep(3);
	} 

	public void ClickOnExitPopUpButtonInSureLock(String Text) throws InterruptedException
	{
		common.ClickOnAppiumButtons(Text);
	}
	public void EnteringIntoSureLock() throws IOException, InterruptedException
	{

		Runtime.getRuntime().exec("adb shell am broadcast -a com.gears42.surelock.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surelock");
		sleep(8);	
	}
	public void ClickOnExitSureLockButton(String Text) throws InterruptedException
	{
		common.commonScrollMethod(Text);
		common.ClickOnAppiumText(Text);
	}

	public void EnteringIntoSureFox() throws IOException, InterruptedException
	{
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium,25);
		Runtime.getRuntime().exec("adb shell am broadcast -a com.gears42.surefox.COMMUNICATOR -e \"command\" \"open_admin_settings\" -e \"password\" \"0000\" com.gears42.surefox");
		sleep(5);	
	}


	public String NewlyAddedContentBlockingAnalyticsSecretKey;
	public void VerifyNewlyAddedAnalyticsSecretKey() throws InterruptedException
	{
		boolean flag;
		NewlyCreatedAnalyticsSecretKeyShowButton.click();
		sleep(3);
		NewlyAddedContentBlockingAnalyticsSecretKey=NewlyCreatedAnalyticsSecretKey.getText();
		if(NewlyAddedContentBlockingAnalyticsSecretKey.isEmpty() ||NewlyAddedContentBlockingAnalyticsSecretKey==null)
		{
			flag=false;
		}
		else
		{
			flag=true;
		}
		String Pass="PASS >>> Analytics Secret Key Is Not Empty And Key Is Displayed";
		String Fail="FAIL >>> Analytics Secret Key Is Empty";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	public void EnablingContentBlockingCheckBoxInSureFox() throws InterruptedException
	{
		WebElement ContentBlockingCheckBox = Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox[@index='0']");
		String CheckBoxStatus = ContentBlockingCheckBox.getAttribute("checked");
		if(CheckBoxStatus.equals("true"))
		{
			Reporter.log("Enable Content Blocking Check Box Is Already Enabled",true);
		}
		else
		{
			ContentBlockingCheckBox.click();
			sleep(3);
			Reporter.log("Enable Content Blocking Check Box Is Enabled",true);
		}

	}

	public void ClickingOnAddingKeywordToBlockAccess() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Add keywords to block access']").click();
		sleep(4);
	}

	public void SendingKeywordsToBlockAccess(String Keywords) throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='0']").clear();
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='0']").sendKeys(Keywords);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='OK' and @index='1']").click();
	}


	public void SendingEnableContentBlockingRunScripts() throws InterruptedException
	{
		RunScriptTextArea.sendKeys("am broadcast -a com.gears42.surefox.COMMUNICATOR -e password 0000 -e command enable_content_blocking_analytics -e secret_key "+NewlyAddedContentBlockingAnalyticsSecretKey+" -n com.gears42.surefox/com.gears42.surefox.service.SurefoxCommunicator");
		sleep(3);
	}

	public void VerifyingContentBlockingAnalytics()
	{
		String ContentBlockingAnalyticsCBStatus = Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox[@index='0']").getAttribute("checked");
		if(ContentBlockingAnalyticsCBStatus.equals("true"))
		{
			Reporter.log("PASS >>>Content Blocking Analytics CheckBox Is Enabled On Applying Runscript",true);
		}
		else
		{
			String FailMessage="FAIL >>>Content Blocking Analytics CheckBox Isn't Enabled On Applying Runscript";
			ALib.AssertFailMethod(FailMessage);
		}
	}

	public void OpeningWebSitesToSearchBlockedContent(String Website) throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.view.View[@text='"+Website+"']").click();
		sleep(10);
	}

	public void SearchingForBlockedContentInGoogle(String BlockedContent) throws InterruptedException, IOException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[@index='1']").sendKeys(BlockedContent);
		sleep(1);
		Runtime.getRuntime().exec("adb shell input keyevent 66");
		sleep(4);
	}

	public void checkingContentBlockingAnalyticsTableInReports() throws InterruptedException
	{
		JavascriptExecutor jc=(JavascriptExecutor)Initialization.driver;
		jc.executeScript("arguments[0].scrollIntoView()", ContentBlockingTableList);
		sleep(4);
		ContentBlockingTableList.click();
		waitForXpathPresent("//div[@id='addSelectedNodes']");
		sleep(2);
	}

	public void VerifyTypeColumnInContentBlockAnalyticsReport() throws InterruptedException
	{
		try
		{
			String URLType = TotalTimeColumn.getText();
			boolean Value = URLType.contains("BLOCKED");
			String Pass="Blocked URL Is Displayed In The Report";
			String Fail="Blocked URL Is Displayed In The Report";
			ALib.AssertTrueMethod(Value, Pass, Fail);
		}
		catch (Exception e) 
		{
			SwitchBackWindow();
			ALib.AssertFailMethod("Data Not Generated In Report");
		}
	}

	public void VerifyingContentBlockingAnalyticsAfterApplyingDisableRunscript()
	{
		String ContentBlockingAnalyticsCBStatus = Initialization.driverAppium.findElementByXPath("//android.widget.CheckBox[@index='0']").getAttribute("checked");
		if(ContentBlockingAnalyticsCBStatus.equals("true"))
		{
			String FailMessage="FAIL >>>Content Blocking Analytics CheckBox Isn't Disabled On Applying Runscript";
			ALib.AssertFailMethod(FailMessage);

		}
		else
		{
			Reporter.log("PASS >>>Content Blocking Analytics CheckBox Is Disabled On Applying Runscript",true);
		}
	}		

	public void VerifyRemoveButtonForNewlyAddedAnalytics()
	{
		try
		{
			NewlyCreatedAnalyticsRemoveButton.isDisplayed();
			Reporter.log("Remove Button iS Displayed For Newly Created Analytics",true);
		}
		catch(Exception e)
		{
			ALib.AssertFailMethod("Remove Button Is Not Displayed For Newly Created Analytics");
		}
	}

	public void EnablingNewlyCreatedAnalytics()
	{
		boolean Val = NewlyCreatedAnalyticsEnableCheckBox.isSelected();
		System.out.println(Val);
		if(Val)
		{
			Reporter.log("Analytics Is Already Enabled",true);
		}
		else
		{
			NewlyCreatedAnalyticsEnableCheckBox.click();
			Reporter.log("Enabled Analytics",true);
		}
	}

	public void DisablingNewlyCreatedAnalytics()
	{
		boolean Val = NewlyCreatedAnalyticsEnableCheckBox.isSelected();
		System.out.println(Val);
		if(Val)
		{
			NewlyCreatedAnalyticsEnableCheckBox.click();
			Reporter.log("Disabled Analytics",true);

		}
		else
		{
			Reporter.log("Analytics Is Already Disabled",true);
		}
	}

	@FindBy(xpath="//li[text()='"+Config.BatteryAnalyticsName+"']")
	private WebElement BatteryAnalyticsTableList;

	@FindBy(css="button#custom_reports_cancel_btn")
	private WebElement CustomReportCancelButton;

	public void CheckingForBatteryAnalyticsReport() throws InterruptedException
	{
		try
		{
			JavascriptExecutor jc=(JavascriptExecutor)Initialization.driver;
			jc.executeScript("arguments[0].scrollIntoView()", BatteryAnalyticsTableList);
			Reporter.log("Battery Analytics Report is Present Inside Reports",true);
			sleep(3);
			CustomReportCancelButton.click();		    	 }
		catch (Exception e) 
		{
			CustomReportCancelButton.click();
			ALib.AssertFailMethod("Battery Analytics Table Is NOt Present Inside Report");
		}
	}

	//Device Enrllment Rules

	@FindBy(xpath="//input[@id='name_prefix']")
	private WebElement PrefixTextBox;

	@FindBy(xpath="//input[@id='name_suffix']")
	private WebElement SuffixTextBox;

	@FindBy(xpath="//input[@id='name_Count']")
	private WebElement StartCountTextBox;

	@FindBy(xpath="//input[@id='nameCount_length']")
	private WebElement NumberOFDigitsTextBox;

	@FindBy(xpath="//input[@id='device_enrollement_apply']")
	private WebElement DeviceEnrollmentRulesAppliyButton;

	@FindBy(xpath="//input[@id='deviceregauth']")
	private WebElement AdvancedDeviceAuthenticationCheckBox;

	@FindBy(xpath="//input[@id='ad_auth_endpoint']")
	private WebElement AuthEndpointTextField_NativeAppTab;

	@FindBy(xpath="//input[@id='ad_token_endpoint']")
	private WebElement TokenEndpointTextField_NativeAppTab;

	@FindBy(xpath="//input[@id='oauth_client_id']")
	private WebElement ClientIDTextField_NativeAppTab;


	public void VerifyDeviceEnrollmentRulesInAccSettings() throws InterruptedException
	{
		String[] DeviceEnrollmentsTextFields= {"Prefix Text Field","Suffix Text Field","Start Count Text Field","Number Of Digits Text Field","Device Enrollment Rules Apply Button"};
		WebElement[] DeviceEnrollmentsElements= {PrefixTextBox,SuffixTextBox,StartCountTextBox,NumberOFDigitsTextBox,DeviceEnrollmentRulesAppliyButton};
		for(int i=0;i<DeviceEnrollmentsElements.length;i++)
		{
			boolean val=DeviceEnrollmentsElements[i].isDisplayed();
			String Pass=DeviceEnrollmentsTextFields[i]+" "+"Is Displayed Successfully";
			String Fail=DeviceEnrollmentsTextFields[i]+" "+"Is Not Displayed";
			ALib.AssertTrueMethod(val, Pass, Fail);
			sleep(1);
		}

		String[] DeviceEnrollmentText= {"Prefix","Suffix","Start Count","Number Of Digits","Advanced Device Authentication","Device Authentication Type"};
		for(int i=0;i<DeviceEnrollmentText.length;i++)
		{
			boolean val1=Initialization.driver.findElement(By.xpath("//div[@id='device_enrollement_panel']//span[text()='"+DeviceEnrollmentText[i]+"']")).isDisplayed();
			String Pass=DeviceEnrollmentText[i]+" "+"Is Displayed Successfully";
			String Fail=DeviceEnrollmentText[i]+" "+"Is Not Displayed";
			ALib.AssertTrueMethod(val1, Pass, Fail);
			sleep(1);
		}
	}
	//"Require Password ",
	//"OAuth Authentication","SAML Authentication","Active Directory Authentication using Admin Account"
	public void VerifyAuthTypesInDropDown()
	{
		String[] AuthTypes= {"No Authentication"};
		Select sel=new Select(DeviceAuthenticationType);
		List<WebElement> AuthOptions = sel.getOptions();
		for(int i=0;i<AuthOptions.size();i++)
		{
			if(AuthOptions.get(i).getText().equals(AuthTypes[i]))
			{
				Reporter.log(AuthOptions.get(i).getText()+" "+"Option Is Present Inside Enrollment Authentication DropDown",true);
			}
			else
			{
				ALib.AssertFailMethod(AuthTypes[i]+" "+"Option Is Not Present Inside Enrollment Authentication DropDown");
			}
		}

	}

	public String PasswordLengthErrorMessage="Password cannot be less than 8 characters.";
	public String OAuthEmptyErrorMessage="Authentication Endpoint cannot be empty.";

	@FindBy(xpath="//input[@id='sfb_tenant_id']")
	private WebElement TenantIDTextBox;

	public void ClickOnDeviceEnollmentRulespplyButton_ErrorMessage(String ErrorMessage)
	{
		DeviceEnrollmentRulesAppliyButton.click();
		waitForXpathPresent("//span[text()='"+ErrorMessage+"']");
		try
		{
			Initialization.driver.findElement(By.xpath("//span[text()='"+ErrorMessage+"']")).isDisplayed();
			Reporter.log("PASS>>> "+ErrorMessage+" Is Displayed",true);
		}
		catch (Exception e) 
		{
			ALib.AssertFailMethod("FAIL>>> "+ErrorMessage+" Is Not Displayed");
		}
	}

	public void ClearingTextFields() throws InterruptedException
	{
		AuthEndpointTextField_NativeAppTab.clear();
		TokenEndpointTextField_NativeAppTab.clear();
		ClientIDTextField_NativeAppTab.clear();
	}

	public void EnteringDataInTenantID1() throws InterruptedException
	{
		TenantIDTextBox.sendKeys("InValid Tenant ID");
		Initialization.driver.findElement(By.xpath("(//button[@id='Validate'])[1]")).click();
		sleep(4);
	}

	public void VerifyErrorMessageOnEnteringInvalidTenantID()
	{
		boolean val=Initialization.driver.findElement(By.xpath("//span[text()='Invalid Tenant ID.']")).isDisplayed();
		String Pass="Invalid Token Message Is Displayed Successfully";
		String Fail="Invalid Token Message Is Not Displayed Successfully";
		ALib.AssertTrueMethod(val, Pass, Fail);
	}

	@FindBy(id="certificatemanagement")
	private WebElement certificatemanagementTab;

	@FindBy(xpath="//input[@id='ca_server_address']")
	private WebElement CAServerAddress;

	@FindBy(xpath="//input[@id='certificate_template']")
	private WebElement certificatetemplate;

	@FindBy(xpath="//input[@id='certificate_renewal_val']")
	private WebElement certificaterenewalval;

	@FindBy(xpath="(//select[@id='certificate_renewal_unit'])[1]")
	private WebElement certificaterenewaleriod;

	@FindBy(xpath="//select[@id='wildcardscepid']")
	private WebElement CommonNameWildcard;

	@FindBy(xpath="//select[@id='sanwildcard']")
	private WebElement sanwildcard;

	@FindBy(xpath="//input[@id='sanwildcardcustom']")
	private WebElement sanwildcardEmail;

	@FindBy(xpath="//input[@id='enforceSCEPpassword']")
	private WebElement EnableOTP;

	@FindBy(xpath="//div[@id='iOSCertificateMng']/div[2]/input[1]") //   //input[@id='enforceSCEPpassword']
	private WebElement SaveButton;

	@FindBy(xpath="//div[@id='iOSCertificateMng']/div[2]/input[2]")
	private WebElement GetManagedCertificates;

	@FindBy(xpath="//input[@id='scepusername']")
	private WebElement scepUsername;

	@FindBy(xpath="//input[@id='sceppassword']")
	private WebElement scepPassword;

	@FindBy(xpath="//span[contains(text(), 'Please provide a CA server address.')]")
	private WebElement WithoutCAServerAddress;

	@FindBy(xpath="//span[contains(text(), 'Certificate Template cannot be empty.')]")
	private WebElement OnlyCAServerAddress;

	@FindBy(xpath="//span[contains(text(),'Username and Password fields needs to be filled.')]")
	private WebElement WithoutScepUsernamePassword;

	@FindBy(xpath="//div[@id='RevokeSelectedCertificates']")
	private WebElement RevokeOption;

	@FindBy(xpath="//div[@id='RenewSelectedCertificates']")
	private WebElement RenewOption;

	@FindBy(xpath="//table[@id='certificatedetails']/tbody/tr/td[2]")
	private List<WebElement> certificatedetails;

	@FindBy(xpath="//div[@id='getManagedCertificatepopup']/div[1]/div[1]/div[1]/button")
	private WebElement CloseGetmanagedcertificate;

	@FindBy(xpath="//input[@id='includescep']")
	private WebElement ClickONScep;

	@FindBy(xpath="//select[@id='CredentialType']")
	private WebElement CertificateUsage;

	@FindBy(xpath="//input[@id='certificate_name_profile']")
	private WebElement CertificateName;

	@FindBy(xpath="//td[contains(text(),'"+Config.DeviceName+"')]/following-sibling::td[1]")
	private WebElement CertificateRenew;

	@FindBy(xpath="//span[contains(text(), 'Renewed successfully.')]")
	private WebElement ConfirmationmessageRenewcertificate;

	@FindBy(xpath="//div[@id='busyIndicator']/div[1]/div[1]/div[1]")
	private WebElement CertifiateLoading;

	@FindBy(xpath="//span[contains(text(), 'The certificate has been successfully revoked.')]")
	private WebElement ConfirmationmessageRevokecertificate;

	@FindBy(id="addPolicyBtn")
	private WebElement AddProfile;


	public void ClickOnCertificateManagement() throws InterruptedException{
		//SurveyBoxIsDisplayed();
		Helper.highLightElement(Initialization.driver, certificatemanagementTab);
		certificatemanagementTab.click();
		waitForXpathPresent("//input[@id='ca_server_address']");
		sleep(6);
		Reporter.log("PASS >> Clicked on Certificate Management", true);
	}

	public void VerifyCertificateMAnagementPageOptions() {
		boolean flag=CAServerAddress.isDisplayed();
		String PassStatement="PASS >>> CA Server Address is displaying successfully";
		String FailStatement="FAIL >>> CA Server Address is not  displaying";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

		boolean flag1=certificatetemplate.isDisplayed();
		String PassStatement1="PASS >>> certificate template is displaying successfully";
		String FailStatement1="FAIL >>> certificate template is not  displaying";
		ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1); 

		boolean flag2=certificaterenewalval.isDisplayed();
		String PassStatement2="PASS >>> certificate renewalval is displaying successfully";
		String FailStatement2="FAIL >>> certificate renewalval is not  displaying";
		ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);

		boolean flag4=certificaterenewaleriod.isDisplayed();
		String PassStatement4="PASS >>> certificate renewal period is displaying successfully";
		String FailStatement4="FAIL >>> certificate renewal period is not  displaying";
		ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);

		boolean flag5=CommonNameWildcard.isDisplayed();
		String PassStatement5="PASS >>> Common Name Wildcard is displaying successfully";
		String FailStatement5="FAIL >>> Common Name Wildcard is not  displaying";
		ALib.AssertTrueMethod(flag5, PassStatement5, FailStatement5);

		boolean flag6=sanwildcard.isDisplayed();
		String PassStatement6="PASS >>> san Wildcard is displaying successfully";
		String FailStatement6="FAIL >>> san Wildcard is not  displaying";
		ALib.AssertTrueMethod(flag6, PassStatement6, FailStatement6);

		boolean flag9=EnableOTP.isEnabled();
		String PassStatement9="PASS >>> Enable OTP is Enabled successfully";
		String FailStatement9="FAIL >>> Enable OTP is not Enabled";
		ALib.AssertTrueMethod(flag9, PassStatement9, FailStatement9);

		boolean flag7=SaveButton.isEnabled();
		String PassStatement7="PASS >>> Save Button is Enabled successfully";
		String FailStatement7="FAIL >>> Save Button is not Enabled";
		ALib.AssertTrueMethod(flag7, PassStatement7, FailStatement7);

		boolean flag8=GetManagedCertificates.isEnabled();
		String PassStatement8="PASS >>> Get Managed Certificates is Enabled successfully";
		String FailStatement8="FAIL >>> Get Managed Certificates is not Enabled";
		ALib.AssertTrueMethod(flag8, PassStatement8, FailStatement8);
	}

	public void EnterAllTheFieldsOfCertiManagement(String duration, String cn, String san) throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException{
		CAServerAddress.clear();
		CAServerAddress.sendKeys(Config.CAServerAddress);
		Reporter.log("Entered CA Server Address", true);

		certificatetemplate.clear();
		certificatetemplate.sendKeys(Config.CertificateTemplate);
		Reporter.log("Entered Certificate Template", true);

		certificaterenewalval.clear();
		certificaterenewalval.sendKeys(Config.CertificateRenewalPeriodNumber);
		Reporter.log("Entered Certificate Renewal Period Number", true);

		Select period = new Select(certificaterenewaleriod);
		period.selectByVisibleText(duration);
		Reporter.log("Selected Certificate Renewal Period", true);

		Select cnWildcard = new Select(CommonNameWildcard);
		cnWildcard.selectByVisibleText(cn);
		Reporter.log("Selected Common Name Wildcard", true);

		Select sanwildcad = new Select(sanwildcard);
		sanwildcad.selectByVisibleText(san);
		Reporter.log("Selected san wildcard", true);

		sanwildcardEmail.clear();
		sanwildcardEmail.sendKeys(Config.SANWildcardEmail);
		Reporter.log("Entered SAN Wildcard Email", true);
	}

	public void SelectEnableOTp() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//span[contains(text(),'Enable OTP')]")).click();
		boolean flag=EnableOTP.isSelected();
		if(flag==false) {
			EnableOTP.click();
		}
		Reporter.log("Selected Enable OTP checkbox", true);
		sleep(1);
	}

	public void EnterScepUserPassword() throws InterruptedException
	{
		scepUsername.clear();
		scepUsername.sendKeys(Config.ScepUserNAme);
		scepPassword.clear();
		scepPassword.sendKeys(Config.ScepPassword);
		Reporter.log("Entered user name and  password");
	}

	public void SaveCertificateMangement() throws InterruptedException {
		SaveButton.click();
		Reporter.log("Clicked on Save button", true);
		sleep(2);
	}
	@FindBy(xpath="//span[text()='Settings updated successfully.']")
	private WebElement ConfirmationMessageWithoutSettings;
	String CertifiRenew;


	public void VerifyConfirmationMessage() {
		boolean flag = ConfirmationMessageWithoutSettings.isDisplayed();
		String PassStatement="PASS >> Updated Sucsessfully. Message is displayed";
		String FailStatement="FAIL >> Updated Sucsessfully. Message is displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}

	public void EnterAllTheFieldsExceptCAServerAddressCertiManagement(String duration, String cn, String san) throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException{
		CAServerAddress.clear();
		Reporter.log("Cleared CA Server Address", true);

		certificatetemplate.clear();
		certificatetemplate.sendKeys(Config.CertificateTemplate);
		Reporter.log("Entered Certificate Template", true);

		certificaterenewalval.clear();
		certificaterenewalval.sendKeys(Config.CertificateRenewalPeriodNumber);
		Reporter.log("Entered Certificate Renewal Period Number", true);

		Select period = new Select(certificaterenewaleriod);
		period.selectByVisibleText(duration);
		Reporter.log("Selected Certificate Renewal Period", true);

		Select cnWildcard = new Select(CommonNameWildcard);
		cnWildcard.selectByVisibleText(cn);
		Reporter.log("Selected Common Name Wildcard", true);

		Select sanwildcad = new Select(sanwildcard);
		sanwildcad.selectByVisibleText(san);
		Reporter.log("Selected san wildcard", true);

		sanwildcardEmail.clear();
		sanwildcardEmail.sendKeys(Config.SANWildcardEmail);
		Reporter.log("Entered SAN Wildcard Email", true);
	}

	public void ErrorMessageWithoutCAServerAddress() {
		boolean flag = WithoutCAServerAddress.isDisplayed();
		String PassStatement="PASS >> CA Server Address cannot be empty. Message is displayed";
		String FailStatement="FAIL >> CA Server Address cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	public void EnterOnlyCAServerAddress() {
		CAServerAddress.clear();
		CAServerAddress.sendKeys(Config.CAServerAddress);
		Reporter.log("Entered CA Server Address", true);

		certificatetemplate.clear();
		Reporter.log("Cleared Certificate Template", true);

		certificaterenewalval.clear();
		Reporter.log("Cleared Certificate Renewal Period Number", true);

		sanwildcardEmail.clear();
		Reporter.log("Cleared SAN Wildcard Email", true);
	}

	public void UnselectEnableOTp() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//span[contains(text(),'Enable OTP')]")).click();
		boolean flag=EnableOTP.isSelected();
		if(flag==true) {
			EnableOTP.click();
		}
		Reporter.log("Unselected Enable OTP checkbox", true);
		sleep(1);
	}

	public void ErrorMessageWhenOnlyCAServerAddress() {
		boolean flag = OnlyCAServerAddress.isDisplayed();
		String PassStatement="PASS >> Certificate Template cannot be empty. Message is displayed";
		String FailStatement="FAIL >> Certificate Template cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	public void clearScepUsernamePassword() throws InterruptedException
	{
		scepUsername.clear();
		scepPassword.clear();
		Reporter.log("Cleared user name and  password");
	}

	public void ErrorMessageWhenWithoutScepUsernamePassword() {
		boolean flag = WithoutScepUsernamePassword.isDisplayed();
		String PassStatement="PASS >> Certificate Template cannot be empty. Message is displayed";
		String FailStatement="FAIL >> Certificate Template cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	public void ClickOnGetManagedCertificates() throws InterruptedException 
	{
		Actions action=new Actions(Initialization.driver);
		action.click(GetManagedCertificates).build().perform();
		Reporter.log("PASS >> Clicked on GetManagedCertificates", true);
		sleep(10);
	}

	public void VerifyGetManagedCertificates() throws InterruptedException {
		boolean flag = RevokeOption.isDisplayed();
		String PassStatement="PASS >> Revoke Option is displayed";
		String FailStatement="FAIL >> Revoke Option is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

		boolean flag1 = RenewOption.isDisplayed();
		String PassStatement1="PASS >> Renew Option is displayed";
		String FailStatement1="FAIL >> Renew Option is not displayed";
		ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
	}

	public void VerifyCertificatedDetails() {
		if(certificatedetails.size()==0)
		{
			Reporter.log("No Certified devices in this account", true);
		}
		else {
			for(int i=0;i<certificatedetails.size();i++)
			{
				Reporter.log("PASS >> "+certificatedetails.get(i).getText()+" Certified device is displayed", true);
			}
		}
	}  

	public void CloseGetManagedCertificates() throws InterruptedException {
		//			try {
		sleep(2);
		CloseGetmanagedcertificate.click();
		sleep(2);		
		Reporter.log("PASS >> Clicked on Close button of GetManagedCertificate",true);
		
//		sleep(4);
//		Actions action=new Actions(Initialization.driver);
//		action.click(CloseGetmanagedcertificate).build().perform();
//		Reporter.log("PASS >> Clicked on Close button of GetManagedCertificate",true);
//		sleep(2);
		
		//			}
		//			catch(Exception e) {
		//				System.out.println("catched");
		//				Actions action=new Actions(Initialization.driver);
		//				action.click(CloseGetmanagedcertificate).build().perform();
		//				System.out.println("catched");
		//			}
	}



	public void ClickOnScepCheckBox() {
		boolean flag=ClickONScep.isSelected();
		if(flag==false) {
			ClickONScep.click();
		}
		Reporter.log("Selected Create Certificate using Scep", true);
	}

	public void CertifcateUsageWifi() {
		Select sel=new Select(CertificateUsage);
		sel.selectByVisibleText("Wi-Fi");
		Reporter.log("Selected Wifi", true);
	}

	public void EnterCertificateName(String value){
		CertificateName.sendKeys(value);
	}


	public void VerifyRevokedDeviceInGetManagedCertificate(String value) throws InterruptedException {
		sleep(4);
		boolean flag; 
		try 
		{
			if(CertifiRenew.contains(value)) 
			{
				flag=true;
			}
			else 
			{
				flag=false;
			}
		}
		catch(Exception e)
		{
			System.out.println("catched");
			flag=false;
		}
		String PassStatement="PASS >> Revoked Certificate is Not Displayed in Get Managed Certificate";
		String FailStatement="FAIL >> Revoked Certificate is Displayed in Get Managed Certificate";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
	}


	public void ConfirmationMessageRevokeCerticate() throws InterruptedException {
		while(CertifiateLoading.isDisplayed()) 
		{
		}
		boolean flag = ConfirmationmessageRevokecertificate.isDisplayed();
		String PassStatement="PASS >> Revoked successfully. Message is displayed successfully";
		String FailStatement="FAIL >> Revoked successfully. Message is displayed successfully";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(5);
	}

	public void ClickOnRevokeButton() throws InterruptedException {
		RevokeOption.click();
		Reporter.log("Clicked On Revoke Button", true);
		sleep(2);
	}

	public void ClickOnAddButtonOfProfile() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, AddProfile);
		AddProfile.click();
		waitForidPresent("passwordpolicyAndroidProfileConfig");
		sleep(2);
	}

	public boolean commonScrollMethod(String NameText) throws InterruptedException {
		boolean Element=Initialization.driverAppium.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + NameText + "\").instance(0))")).isDisplayed();
		String pass = NameText+"PASS >> is displayed";
		String fail = NameText+" is not displayed";
		ALib.AssertTrueMethod(Element, pass, fail);
		return Element;
	}

	public void VerifyCertificateNameFromDevice(String value) throws InterruptedException, IOException {
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 40);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'title') and @text='Network & Internet']").click();
		Reporter.log("Clicked on Network & Internet", true);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Lab-42G']").click();
		Reporter.log("Clicked on Wifi", true);
		sleep(2);
		commonScrollMethod("Add network");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Add network']").click();
		Reporter.log("Clicked on Add network", true);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Spinner[contains(@resource-id, 'security')]").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[contains(@resource-id, 'text1') and @index='3']").click();
		Reporter.log("802.1x EAP", true);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Spinner[contains(@resource-id, 'method')]")));
		Initialization.driverAppium.findElementByXPath("//android.widget.Spinner[contains(@resource-id, 'method')]").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[@text='TLS']").click();
		Reporter.log("Selected TLS", true);
		sleep(2);
		Runtime.getRuntime().exec("adb shell input keyevent 4");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Spinner[contains(@resource-id, 'user_cert')]")));
		Initialization.driverAppium.findElementByXPath("//android.widget.Spinner[contains(@resource-id, 'user_cert')]").click();
		sleep(2);   //android.widget.CheckedTextView[contains(@text, '"+Config.CertificateName+"')]

		boolean flag = Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[contains(@resource-id, 'text1') and @text='"+value+"']").isDisplayed();
		String PassStatement="PASS >> Certicate is displayed in Device Settings side";
		String FailStatement="FAIL >> Certicate is displayed in Device Settings side";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

	}


	public void VerifyCertificateNameWhenDisableOTP() throws InterruptedException, IOException {
		WebDriverWait wait=new WebDriverWait(Initialization.driverAppium, 40);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[contains(@resource-id, 'title') and @text='Network & Internet']").click();
		Reporter.log("Clicked on Network & Internet", true);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Lab-42G']").click();
		Reporter.log("Clicked on Wifi", true);
		sleep(2);
		commonScrollMethod("Add network");
		Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Add network']").click();
		Reporter.log("Clicked on Add network", true);
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.Spinner[contains(@resource-id, 'security')]").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[contains(@resource-id, 'text1') and @index='3']").click();
		Reporter.log("802.1x EAP", true);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Spinner[contains(@resource-id, 'method')]")));
		Initialization.driverAppium.findElementByXPath("//android.widget.Spinner[contains(@resource-id, 'method')]").click();
		sleep(2);
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[@text='TLS']").click();
		Reporter.log("Selected TLS", true);
		sleep(2);
		//			 Initialization.driverAppium.hideKeyboard();
		Runtime.getRuntime().exec("adb shell input keyevent 4");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Spinner[contains(@resource-id, 'user_cert')]")));
		Initialization.driverAppium.findElementByXPath("//android.widget.Spinner[contains(@resource-id, 'user_cert')]").click();
		sleep(2);   //android.widget.CheckedTextView[contains(@text, '"+Config.CertificateName+"')]

		boolean flag = Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[contains(@resource-id, 'text1') and @text='"+Config.CertificateName2+"']").isDisplayed();
		String PassStatement="PASS >> SCEP Certicate is not displayed in Device Settings side";
		String FailStatement="FAIL >> SCEP Certicate is displayed in Device Settings side";
		ALib.AssertFalseMethod(flag, PassStatement, FailStatement);

	}

	@FindBy(xpath="//span[contains(text(),'Please enter a user name.')]")
	private WebElement office365UsernameEmpty;

	@FindBy(xpath="//span[contains(text(),'Please enter a password.')]")
	private WebElement office365PasswordEmpty;

	@FindBy(xpath="//li[@id='office365Management']")
	private WebElement office365;
	public void ClickOnOffice365Settings() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, office365);
		office365.click();
		waitForXpathPresent("//input[@id='allowAccess_office365']");
		sleep(6);
		Reporter.log("PASS >> Clicked on Office 365 Settings ", true);
	}

	public void EnterInvalidTenantIDByClickingOnValidate() throws InterruptedException {
		TenantID.clear();
		TenantID.sendKeys(Config.InvalidTenantId);
		office365_ValidateButton.click();
		Reporter.log("PASS >> Clicked on Validate button", true);
		sleep(2);
	}

	public void UnselectEnableOffice365CheckBox() throws InterruptedException
	{	
		boolean flag=EnableOffice365.isSelected();
		if(flag==true)
		{
			EnableOffice365.click();
		}
		Reporter.log("Unselected Enable office 365", true);
		sleep(1);
	}

	public void ErrorMessageByGivingEmptyTenantID() {
		boolean flag = TenantIDEmpty.isDisplayed();
		String PassStatement="PASS >>> Tenant ID cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >>> Tenant ID cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}

	public void EnterTenantID() throws InterruptedException {
		TenantID.sendKeys(Config.TenantId);
		Reporter.log("PASS >> Entered TenantID", true);
	}

	public void ErrorMessageByGivingEmptyUsername() {
		boolean flag = office365UsernameEmpty.isDisplayed();
		String PassStatement="PASS >>> Username cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >>> Username cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}

	public void EnterOffice365Username() throws InterruptedException {
		office365_useranme.sendKeys(Config.office365username);
		Reporter.log("PASS >> Entered Username", true);
	}

	public void ErrorMessageByGivingEmptyPassword() {
		boolean flag = office365PasswordEmpty.isDisplayed();
		String PassStatement="PASS >>> Password cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >>> Password cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}

	@FindBy(xpath="//span[contains(text(),'Please provide a tenant ID.')]")
	private WebElement TenantIDEmpty;

	@FindBy(xpath="//span[contains(text(),'Please provide an application ID.')]")
	private WebElement ApplicationIDEmpty;

	@FindBy(xpath="//span[contains(text(),'Please enter the necessary encryption keys.')]")
	private WebElement ApplicationSecretEmpty;

	@FindBy(xpath="//span[text()='Please provide an application ID.']")
	private WebElement ApplicationIDEmptyMessgae ;
		
	public void ErrorMessageByGivingEmptyApplicationID() {
		boolean flag = ApplicationIDEmpty.isDisplayed();
		String PassStatement="PASS >>> Application ID cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >>> Application ID cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}

	public void ErrorMessageforEmptyApplicationID() {
		boolean flag = ApplicationIDEmptyMessgae.isDisplayed();
		String PassStatement="PASS >>> Please provide an application ID. Message is displayed successfully";
		String FailStatement="FAIL >>> Please provide an application ID. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}
	
	
	@FindBy(xpath="//input[@id='device_enrollement_apply']")
	private WebElement ClickonApplyButton ;
		
	public void ClickonApplyButton() throws InterruptedException {
		sleep(2);
		ClickonApplyButton.click();
		Reporter.log("PASS >> Clicked on Save button", true);
		sleep(2);
	}
	
	
	
	
	
	
	public void EnterApplicationID() throws InterruptedException {
		ApplicationID.sendKeys(Config.ApplicationId);
		Reporter.log("PASS >> Entered ApplicationID", true);

	}

	public void ErrorMessageByGivingEmptyApplicationSecret() {
		boolean flag = ApplicationSecretEmpty.isDisplayed();
		String PassStatement="PASS >>> Application Secret cannot be empty. Message is displayed successfully";
		String FailStatement="FAIL >>> Application Secret cannot be empty. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}

	
	@FindBy(xpath="//input[@id='sfb_client_secret']")
	private WebElement ClearApplicationSecreteKey ;
		
	public void ClearApplicationSecreteKey() throws InterruptedException{
		sleep(1);
		ClearApplicationSecreteKey.clear(); 
		sleep(1);
	}
	
	@FindBy(xpath="//span[text()='Please enter the necessary encryption keys.']")
	private WebElement ErrorMessageForEmptyApplicationSecret ;
	
	
	public void ErrorMessageForEmptyApplicationSecret() {
		boolean flag = ErrorMessageForEmptyApplicationSecret.isDisplayed();
		String PassStatement="PASS >>> Please enter the necessary encryption keys. Message is displayed successfully";
		String FailStatement="FAIL >>> Please enter the necessary encryption keys. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}
	
	
	
	
	
	
	public void EnterApplicationSecret() throws InterruptedException {
		ApplicationScecret.sendKeys(Config.ApplicationSecret);
		Reporter.log("PASS >> Entered ApplicationID", true);
	}

	@FindBy(xpath="//span[contains(text(),'Office 365 details saved successfully.')]")
	private WebElement ConfirmationmessageOffice365;
	public void ConfirmationMessageOffice365() {
		boolean flag = ConfirmationmessageOffice365.isDisplayed();
		String PassStatement="PASS >> Office365 details saved successfully. Message is displayed successfully";
		String FailStatement="FAIL >> Office365 details saved successfully. Message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	public void ClickOnSaveButton() throws InterruptedException {
		office365_saveButton.click();
		Reporter.log("PASS >> Clicked on Save button", true);
		sleep(1);
	}

	public void ClickOnSave() throws InterruptedException {
		EulaSaveButton.click();
		Reporter.log("PASS >> Clicked on Save button",true);
		sleep(3);
	}

	public void VerifytheDisclaimerPolicyText() {
		boolean flag=EulatextArea.isDisplayed();
		String PassStatement="PASS >> Eula Disclaimer textarea is displaying";
		String FailStatement="FAIL >> Eula Disclaimer textarea  is not displaying";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	public void UnselectUseEulaDisclaimer() throws InterruptedException {
		boolean flag=selectEulaDisclaimer.isSelected();
		if(flag==true) {
			selectEulaDisclaimer.click();
		}
		Reporter.log("Unselected Eula Disclaimer policy",true);
		sleep(2);
	}

	@FindBy(xpath="//span[text()='Settings updated successfully.']")
	private WebElement successfullMessage;
	public void SuccessMessageAfterClickOnSaveButton() {
		boolean flag=successfullMessage.isDisplayed();
		String PassStatement="PASS >> Updated successfully. Success message is displayed";
		String FailStatement="FAIL >> Updated successfully. Success message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	public void ModifyDisclaimerPolicyText() {
		EulatextArea.sendKeys(Config.ModifyEulaDisclaierPolicy_Text);
		Reporter.log("Modify the Disclaimer policy text",true);
	}

	public void entertheDisclaimerPolicyText() {
		EulatextArea.sendKeys(Config.EulaDisclaierPolicy_Text);
		Reporter.log("Entered the Disclaimer policy text",true);
	}
	@FindBy(xpath="//span[text()='Display Acceptable Use Policy']")
	private WebElement selectEulaDisclaimerabsolute;
	public void verifyUseEulaDisclaimer() {
		boolean flag=selectEulaDisclaimerabsolute.isDisplayed();
		String PassStatement="PASS >> Use Eula Disclaimer checkbox  is displaying";
		String FailStatement="FAIL >> Use Eula Disclaimer checkbox  is not displaying";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	@FindBy(id="eulaDisclaimer")
	private WebElement EulaDisclaimerTab;
	public void ClickOnEulaDisclaimer() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, EulaDisclaimerTab);
		EulaDisclaimerTab.click();
		waitForXpathPresent("//input[@id='chkbox_eulaDisclaimer']");
		sleep(6);
		Reporter.log("PASS >> Clicked on EULA Disclaimer Policy", true);
	}

	@FindBy(xpath="//span[text()='Acceptable Use Policy Text can not be empty.']")
	private WebElement errormessagewithoutText;
	public void verifyTheErrormessgewitoutEulaDisclaimerText() {
		boolean flag=errormessagewithoutText.isDisplayed();
		String PassStatement="PASS >> Acceptable Use Policy Text can not be empty. Error message is displayed";
		String FailStatement="FAIL >> Acceptable Use Policy Text can not be empty. Error message is not displayed";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	@FindBy(xpath="(//button[@id='Validate'])[2]")
	private WebElement office365_ValidateButton;
                                           
	@FindBy(xpath="//span[contains(text(),'Invalid Tenant ID.')]")
	private WebElement ErrorMessageWhenInvalidTanentID;
	public void ErrorMessageWhenInvalidTenantIDByClickingOnValidate() {
		boolean flag = ErrorMessageWhenInvalidTanentID.isDisplayed();
		String PassStatement="PASS >>> Invalid Tenant ID. Message is displayed successfully";
		String FailStatement="FAIL >>> Invalid Tenant ID. Message is displayed successfully";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement); 
	}
	@FindBy(xpath="//input[@id='allowAccess_office365']")
	private WebElement EnableOffice365;
	public void VerifyOffice365settingsFields() 
	{
		boolean flag=EnableOffice365.isEnabled();
		String PassStatement="PASS >>> Enable Office365 checkbox is displaying successfully";
		String FailStatement="FAIL >>> Enable Office365 checkbox is not  displaying";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

		boolean flag1=TenantID.isDisplayed();
		String PassStatement1="PASS >>> TenantID is displaying successfully";
		String FailStatement1="FAIL >>> TenantID is not  displaying";
		ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);

		boolean flag2=ApplicationID.isDisplayed();
		String PassStatement2="PASS >>> ApplicationID is displaying successfully";
		String FailStatement2="FAIL >>> ApplicationID is not  displaying";
		ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);

		boolean flag3=ApplicationScecret.isDisplayed();
		String PassStatement3="PASS >>> ApplicationScecret is displaying successfully";
		String FailStatement3="FAIL >>> ApplicationScecret is not  displaying";
		ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);

		boolean flag4=office365_useranme.isDisplayed();
		String PassStatement4="PASS >>> office365 username is displayed successfully";
		String FailStatement4="FAIL >>> office365 username is not  displayed";
		ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);

		boolean flag5=office365_password.isDisplayed();
		String PassStatement5="PASS >>> office365 password is displaying successfully";
		String FailStatement5="FAIL >>> office365 password is not  displaying";
		ALib.AssertTrueMethod(flag5, PassStatement5, FailStatement5);

		boolean flag6=office365_saveButton.isDisplayed();
		String PassStatement6="PASS >>> office365 Save Button is displaying successfully";
		String FailStatement6="FAIL >>> office365 Save Button is not  displaying";
		ALib.AssertTrueMethod(flag6, PassStatement6, FailStatement6);
	}

	@FindBy(xpath="//input[@id='office365_username']")
	private WebElement office365_useranme;

	@FindBy(xpath="//input[@id='office365_password']")
	private WebElement office365_password;

	@FindBy(xpath="//input[@id='office365_Save']")
	private WebElement office365_saveButton;
	public void SelectEnableOffice365CheckBox() throws InterruptedException
	{	
		boolean flag=EnableOffice365.isSelected();
		if(flag==false)
		{
			EnableOffice365.click();
		}
		Reporter.log("PASS >> Selected Enable office 365", true);
		sleep(1);
	}

	@FindBy(xpath="//input[@id='office365_tenant_id']")
	private WebElement TenantID;

	@FindBy(xpath="//input[@id='office365_applicationId']")
	private WebElement ApplicationID;

	@FindBy(xpath="//input[@id='office365_applicationSecret']")
	private WebElement ApplicationScecret;
	public void EnterAllTheFieldsOffice365Settings() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException{
		TenantID.clear();
		sleep(2);
		TenantID.sendKeys(Config.TenantId);
		Reporter.log("PASS >> Entered TenantID", true);

		ApplicationID.clear();
		sleep(2);
		ApplicationID.sendKeys(Config.ApplicationId);
		Reporter.log("PASS >> Entered Application ID", true);

		ApplicationScecret.clear();
		sleep(2);
		ApplicationScecret.sendKeys(Config.ApplicationSecret);
		Reporter.log("PASS >> Entered Application Scecret", true);

		office365_useranme.clear();
		sleep(2);
		office365_useranme.sendKeys(Config.office365username);
		Reporter.log("PASS >> Entered office365 username", true);

		office365_password.clear();
		sleep(2);
		office365_password.sendKeys(Config.office365password);
		Reporter.log("PASS >> Entered office365 password", true);

	}
	public void InstallCertificateToDevice() throws InterruptedException, IOException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Install Ceritifcate']").click();
		Reporter.log("Clicked on Install Certificate");
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.EditText[contains(@resource-id, 'credential_password')]").sendKeys(Config.ExtractCertificatePassword);
		Reporter.log("Entered Password to extract Certificate");
		sleep(1);
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[contains(@resource-id, 'button1') and @text='OK']").click();
		Reporter.log("Clicked on Ok button");
		sleep(1);

		Initialization.driverAppium.findElementByXPath("//android.widget.Spinner[contains(@resource-id, 'credential_usage')]").click();
		Initialization.driverAppium.findElementByXPath("//android.widget.CheckedTextView[@text='Wi-Fi']").click();
		Reporter.log("Selected Wi-Fi");
		sleep(1);

		Initialization.driverAppium.findElementByXPath("//android.widget.Button[contains(@resource-id, 'button1') and @text='OK']").click();
		Reporter.log("Clicked on Ok button");
		sleep(2);
		//			 Runtime.getRuntime().exec("adb shell input keyevent 4");
	}


	public void VerifyInstallCertificatePromptDuringRevoke() throws InterruptedException, IOException
	{
		boolean flag;
		WebDriverWait wait=new WebDriverWait(Initialization.driver,60);
		try
		{
			System.out.println("Waiting for Certificate popup");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Install Ceritifcate']")));
			flag=true;
		}
		catch (Exception e) 
		{
			flag=false;
		}
		String Pass="PASS >>> Install Certificate Prompt message(SCEP Certificate) is not displayed";
		String Fail="FAIL >>> Install Certificate Prompt message(SCEP Certificate) is displayed";
		ALib.AssertFalseMethod(flag, Pass, Fail);
		sleep(2);
	}


	public void VerifyRevokedDeviceInGetManagedCertificate() throws InterruptedException
	{sleep(10);
		boolean var;
		try {
			Initialization.driver.findElement(By.xpath("//*[@id='certificatedetails']/tbody/tr/td[text()='"+Config.CertificateName1+"']")).isDisplayed();
			var = false;
		} catch (Exception e) {
			var = true;

		}
		String PassStatement = "PASS >> Certificate is not available- Revoked worked";
		String FailStatement = "FAIL >> Certificate is available- Revoked did not worked";
		ALib.AssertTrueMethod(var, PassStatement, FailStatement);
	}

	public void SelectCertificate()
	{
		Initialization.driver.findElement(By.xpath("//*[@id='certificatedetails']/tbody/tr/td[text()='"+Config.CertificateName1+"']")).click();

	}

	public void GoToMiscSettings() throws InterruptedException {
		try {
			ClickOnMiscellaneousSettings();
		} catch (Exception e) {
			refesh();
			waitForidPresent("deleteDeviceBtn");
			sleep(5);
			Initialization.loginPage.TrailMessageClick();
			Initialization.commonmethdpage.ClickOnSettings();
			ClickOnAccountSettings();
			ClickOnMiscellaneousSettings();
		}
	}

	@FindBy(id="addNew_userDefineJob")
	private WebElement userdefinedAddButton;
	public void clickonAddButton() throws InterruptedException {
		userdefinedAddButton.click();
		waitForXpathPresent("//input[@id='customjobname']");
		sleep(2);
		Reporter.log("Clicked on Add button", true);
	}

	@FindBy(xpath="//input[@id='customjobname']")
	private WebElement userdefinedjobname;
	public void enterTheJobName(String jobname) {
		userdefinedjobname.sendKeys(jobname);
		Reporter.log("Enter the name", true);
	}

	@FindBy(xpath="//button[@id='browseIcn_btn']")
	private WebElement browseicon;
	public void clickOnBrowseIcon() throws InterruptedException {
		browseicon.click();
		waitForXpathPresent("//h4[text()='Select icon']");
		sleep(2);
		Reporter.log("Clicked on Browse icon", true);
	}

	@FindBy(xpath="//i[@class='fa fa-envelope-o']")
	private WebElement iconsymbol;
	public void SelectAnyicon() throws InterruptedException {
		iconsymbol.click();
		sleep(2);
		Reporter.log("clicked on any symbol", true);
	}

	@FindBy(xpath="//button[@class='btn smdm_btns smdm_grn_clr addbutton awesomeIcn_selDone']")
	private WebElement userdefinedDonebutton;
	public void ClickOnDoneButton() throws InterruptedException {
		userdefinedDonebutton.click();
		waitForXpathPresent("//div[@id='userDefine_jobIcon']");
		sleep(2);
		Reporter.log("Clicked on Done button[icon uploaded]", true);
	}

	@FindBy(xpath="//div[@onclick='forcustomShowJobs()']")
	private WebElement briefCase;
	public void clickOnBriefcaseicon() throws InterruptedException {
		briefCase.click();
		waitForXpathPresent("//h4[text()='Select Jobs To Add']");
		sleep(2);
		Reporter.log("Clicked Briefcase icon", true);
		sleep(6);
	}

	@FindBy(xpath="//input[@id='addrestrictedjob']")
	private WebElement SaveCustomizeTool;
	public void saveCustomTool() throws InterruptedException {
		SaveCustomizeTool.click();
		waitForXpathPresent("//span[text()='Settings updated successfully.']");
		sleep(4);
	}

	public void clickOnAnyOS() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement iOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and5Icon")));
		iOS.click();
		Reporter.log("Clicked on AnyOS Job", true);
		sleep(4);
	}

	public void clickOniOS() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement iOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and6Icon")));
		iOS.click();
		Reporter.log("Clicked on iOS Job", true);
		sleep(4);
	}

	@FindBy(xpath="//div[@id='deviceReboot_modal_id']//button[@id='okbtn']")
	private WebElement OkRebootButton;

	@FindBy(id="job_reset_name_input")
	private WebElement EnterResetJobName;

	public void ClickOnCustomizeToolbar() throws InterruptedException{
		Helper.highLightElement(Initialization.driver, ClickOnCustomizeToolbarSection);
		ClickOnCustomizeToolbarSection.click();
		waitForXpathPresent("//p[text()='Predefined Jobs']");
		sleep(2);
		Reporter.log("Clicked on customize Toolbar");
	}

	public  void verifyEnabledOfPredefinedjobs() 
	{
		boolean flag;
		String[] Text={"Refresh","Remote","Apps","SureLock","SureFox","Call Logs","SMS Logs","Reboot Device","Reset Password","Lock","Message","SureVideo Settings","Wipe Device","Data Usage","Shut Down","Remote Buzz","OS Updates","System Scan","Intel� AMT","Recovery Key"};
		for(int i=0;i<predefinedjobsList.size();i++)
		{
			String PredefinedJobsClass=predefinedjobsList.get(i).getAttribute("Class");
			if(PredefinedJobsClass.equals("sc-prede-app sc-appBox itemBox selectedOne"))
			{
				flag=true;
			}
			else
			{
				flag=false;
			}
			String PassStatement = "PASS >> "+Text[i]+" Predefied job is enabled in Customize tool bar Job";
			String FailStatement = "Fail >> "+Text[i]+" Predefied job is not enabled in Customize tool bar Job"; 
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}	
	}

	public void verifyUncheckPredefinedjobsinConsoleAndroid() throws InterruptedException {
		boolean flag;
		try 
		{
			flag=Initialization.driver.findElement(By.xpath("//div[@id='remoteButton']")).isDisplayed();
			System.out.println(flag);
		}
		catch(Exception e)
		{
			System.out.println("catch 1");
			flag=true;

		}
		String PassStatement="PASS >>  Predefied job is not displaying on console";
		String FailStatement="FAIL >>  Predefied job is displaying on console";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
		try 
		{
			flag=Initialization.driver.findElement(By.xpath("//div[@id='appListBtn']")).isDisplayed();
			System.out.println(flag);
		}
		catch(Exception e)
		{
			System.out.println("catch2");
			flag=true;

		}
		String PassStatement1="PASS >>  Predefied job is not displaying on console";
		String FailStatement1="FAIL >>  Predefied job is displaying on console";
		ALib.AssertTrueMethod(flag, PassStatement1, FailStatement1);
	}

	@FindBy(xpath="//div[@id='recovery_key']")
	private WebElement recoveryKey;
	public void verifyPredefinedJobsinConsoleWindows() throws InterruptedException {
		String recovery=recoveryKey.getText();
		System.out.println(recovery);
		boolean flag=recovery.contains("Recovery Key");
		System.out.println(flag);
		String PassStatement1="PASS >> "+recovery+" Predefied job is displaying on console in Windows";
		String FailStatement1="FAIL >>  "+recovery+" Predefied job is not displaying on console in Windows";
		ALib.AssertTrueMethod(flag, PassStatement1, FailStatement1);
	}

	@FindBy(xpath="//div[@id='list_Of_OS_updates']")
	private WebElement Osupdate;

	@FindBy(xpath="//div[@id='shut_down']")
	private WebElement shutdown;

	public void clickOnDisabledjobsToEnable() throws InterruptedException {
		RemoteCustomize.click();
		sleep(2);
		AppsCustomize.click();
		sleep(2);
	}

	public void scrolldowntoSavebuttonIncustomizetoolbar() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
		js.executeScript("arguments[0].scrollIntoView()",SaveCustomizeTool);
		sleep(2);
	}

	public void verifyDeletedjobIncustomizejob() {
		boolean flag;
		String PassStatement="PASS >> Deleted is not displayed in user defined job section";
		String FailStatement="FAIL >> Deleted is displayed in user defined job section";
		try {
			flag=customizeJobNmae.isDisplayed();
			System.out.println(flag);
			ALib.AssertFalseMethod(flag, PassStatement, FailStatement);
		}
		catch(Exception e) {
			flag=true;
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
	}

	public void verifyDeletedjobInConsole() throws InterruptedException {
		boolean flag;
		try {
			flag=customizeJobNmae.isDisplayed();
			System.out.println(flag);
		}
		catch(Exception e) {
			System.out.println("clicked");
			flag=true;
		}
		String PassStatement="PASS >> Deleted job is not displayed in console";
		String FailStatement="FAIL >> Deleted job is displayed in console";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(3);
	}

	@FindBy(xpath="//li[@title='Edit "+Config.Customize_jobname+"']")
	private WebElement EdituserDefindjob;

	public void ClickonEditUserDefinedjob() throws InterruptedException {
		EdituserDefindjob.click();
		waitForXpathPresent("//input[@id='customjobname']");
		sleep(2);
	}

	public void UpdateJobInUserDefined() {
		userdefinedjobname.sendKeys(Config.Customize_ModifiedJobName);
	}

	public void ClickOnUserDefinedJobs() throws InterruptedException {
		customizeJobNmaeAndroid.click();
		String SomeEnabledPredefinedJobsclass=customizeJobNmaeAndroid.getAttribute("Class");
		System.out.println(SomeEnabledPredefinedJobsclass);
		Reporter.log("Checked job1",true);
		sleep(2);
		ModifycustomizeJobNmae.click();
		sleep(2);
		String SomeEnabledPredefinedJobsclass2=customizeJobNmaeAndroid.getAttribute("Class");
		System.out.println(SomeEnabledPredefinedJobsclass2);
		Reporter.log("Checked job2",true);
		sleep(2);
	}

	@FindBy(xpath="//li[contains(@title,'"+Config.Customize_NewModifiedJobName+"')]")
	private WebElement clickOnuserdefinedJob2;

	@FindBy(xpath="//li[contains(@title,'"+Config.Customize_jobnameAndroid+"')]")
	private WebElement clickOnuserdefinedJob1;

	@FindBy(xpath = "//li[@id='jobSection']")
	private WebElement jobtab;

	public void clickonjobtab() throws InterruptedException {
		jobtab.click();
		waitForidPresent("job_new_job");
		sleep(2);
		Reporter.log("Clickig on Job section", true);
	}

	public void verifySomeOfEnabledUserdefinedjobsAndDisable() throws InterruptedException 
	{
		boolean flag = false;
		String[] Text={Config.Customize_jobnameAndroid,Config.Customize_NewModifiedJobName};
		WebElement[] element= {clickOnuserdefinedJob1,clickOnuserdefinedJob2};
		for(int i=0;i<element.length;i++)
		{
			String SomeEnabledPredefinedJobsclass=element[i].getAttribute("Class");
			if(SomeEnabledPredefinedJobsclass.equals("sc-usede-app sc-appBox1 itemBox selectedOne"))
			{
				Reporter.log(Text[i] + " is Enabled",true);
				element[i].click();
				sleep(2);
				String SomeDisabledPredefinedJobsclass=element[i].getAttribute("Class");
				if(!SomeDisabledPredefinedJobsclass.equals("sc-usede-app sc-appBox1 itemBox selectedOne"))
				{
					flag=true;
				}
				else
				{
					flag=false;
				}
			}
			else if(!SomeEnabledPredefinedJobsclass.equals("sc-usede-app sc-appBox1 itemBox selectedOne"))
			{

				flag=true;
			}
			String PassStatement = "PASS >> "+Text[i]+" Userdefied job is disabled in Customize tool bar Job";
			String FailStatement = "Fail >> "+Text[i]+" Userdefied job is not disabled in Customize tool bar Job"; 
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
			sleep(2);
		}
	}

	public void verifyUncheckUserdefinedjobsinConsole() throws InterruptedException {
		boolean flag;
		try 
		{
			flag=Initialization.driver.findElement(By.xpath("//span[text()='"+Config.Customize_jobnameAndroid+"' and @class='txt actionbuttontext']")).isDisplayed();
			System.out.println(flag);
		}
		catch(Exception e)
		{
			System.out.println("User defined job1");
			flag=true;

		}
		String PassStatement="PASS >> Userdefied job is not displaying on console";
		String FailStatement="FAIL >>  UserPredefied job is displaying on console";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);

		try 
		{
			flag=Initialization.driver.findElement(By.xpath("//span[text()='"+Config.Customize_NewModifiedJobName+"' and @class='txt actionbuttontext']")).isDisplayed();
			System.out.println(flag);
		}
		catch(Exception e)
		{
			System.out.println("User defined job2");
			flag=true;

		}
		String PassStatement1="PASS >>  Userdefied job is not displaying on console";
		String FailStatement1="FAIL >>  Userdefied job is displaying on console";
		ALib.AssertTrueMethod(flag, PassStatement1, FailStatement1);
	}

	@FindBy(xpath="//span[text()='"+Config.Customize_NewModifiedJobName+"' and @class='txt actionbuttontext']")
	private WebElement ModifyuserDeginedjobInConsloe;
	public void verifyModifiedCustomizeJobInConsole() {
		boolean flag = ModifyuserDeginedjobInConsloe.isDisplayed();
		String PassStatement="PASS >> UserDefined Job is displayig in dynamic job section";
		String FailStatement="FAIL >> UserDefined Job is not displayig in dynamic job section";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}

	@FindBy(xpath="//span[text()='"+Config.Customize_NewModifiedJobName+"']")
	private WebElement ModifycustomizeJobNmae;
	public void verifyModifiedCustomizeJob() {
		boolean flag = ModifycustomizeJobNmae.isDisplayed();
		String PassStatement="PASS >> Uploaded file is displayig in UserDefined Job section";
		String FailStatement="FAIL >> Uploaded file is displayig in UserDefined Job section";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	@FindBy(xpath="//i[@class='fa fa-picture-o']")
	private WebElement updateiconsymbol;
	public void UdateSelectAnyicon() throws InterruptedException {
		updateiconsymbol.click(); 
		sleep(2);
		Reporter.log("clicked on any symbol", true);
	}
	@FindBy(xpath="//li[@title='Delete "+Config.Customize_jobname+"']")
	private WebElement deleteuserDefindjobWithConfirmation;

	@FindBy(xpath="//p[text()='Are you sure you want to delete this Job?']")
	private WebElement deleteConfirmationpopup;
	public void deleteUserDefinedjobWithConfirmation() throws InterruptedException {
		deleteuserDefindjobWithConfirmation.click();
		waitForXpathPresent("//p[text()='Are you sure you want to delete this Job?']");
		sleep(2);
		boolean flag=deleteConfirmationpopup.isDisplayed();
		String PassStatement="PASS >> Are you sure you want to delete this Job? Message is displayed.";
		String FailStatement="FAIL >> Are you sure you want to delete this Job? Message is not displayed.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		YesConfirmationbutton.click();
		sleep(2);
		waitForidPresent("addrestrictedjob");
	}

	@FindBy(xpath="//div[@id='ConfirmationDialog']/div[1]/div[1]/div[2]/button[2]")
	private WebElement YesConfirmationbutton;
	public void deleteUserDefinedjob(String value) throws InterruptedException {
		Initialization.driver.findElement(By.xpath("//li[contains(@title, 'Delete "+value+"')]")).click();
		waitForXpathPresent("//p[text()='Are you sure you want to delete this Job?']");
		sleep(2);
		YesConfirmationbutton.click();
		waitForidPresent("addrestrictedjob");
		sleep(4);
	}

	@FindBy(xpath="//span[text()='"+Config.Customize_jobname+"' and @class='txt actionbuttontext']")
	private WebElement userDeginedjobInConsloe;
	public void verifySavedCustomizeJobInConsole() {
		boolean flag = userDeginedjobInConsloe.isDisplayed();
		String PassStatement="PASS >> UserDefined Job is displayig in dynamic job section";
		String FailStatement="FAIL >> UserDefined Job is not displayig in dynamic job section";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	}
	@FindBy(xpath="//span[text()='"+Config.Customize_jobname+"']")
	private WebElement customizeJobNmae;
	public void verifySavedCustomizeJob() throws InterruptedException {
		boolean flag = customizeJobNmae.isDisplayed();
		String PassStatement="PASS >> Uploaded file is displayig in UserDefined Job section";
		String FailStatement="FAIL >> Uploaded file is displayig in UserDefined Job section";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(3);
	}
	@FindBy(xpath="//span[text()='"+Config.Customize_jobnameAndroid+"' and @class='txt actionbuttontext']")
	private WebElement userDeginedjobInConsloeAndroid;
	public void verifySavedCustomizeJobInConsoleAndroid() throws InterruptedException {
		boolean flag = userDeginedjobInConsloeAndroid.isDisplayed();
		String PassStatement="PASS >> UserDefined Job is displayig in dynamic job section";
		String FailStatement="FAIL >> UserDefined Job is not displayig in dynamic job section";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}
	@FindBy(xpath="//ul[@class='predefined_jobsList jobsList clearfix']/li[2]")
	private WebElement RemoteCustomize;

	@FindBy(xpath="//ul[@class='predefined_jobsList jobsList clearfix']/li[3]")
	private WebElement AppsCustomize;

	public  void verifySomeOfEnabledPredefinedjobsAndDisable() 
	{
		boolean flag = false;
		String[] Text={Config.Customize_DisabledPredefinedJobs1,Config.Customize_DisabledPredefinedJobs2};
		WebElement[] element= {RemoteCustomize,AppsCustomize};
		for(int i=0;i<element.length;i++)
		{
			String SomeEnabledPredefinedJobsclass=element[i].getAttribute("Class");
			if(SomeEnabledPredefinedJobsclass.equals("sc-prede-app sc-appBox itemBox selectedOne"))
			{
				Reporter.log(Text[i] + "Is Enabled",true);
				element[i].click();
				String SomeDisabledPredefinedJobsclass=element[i].getAttribute("Class");
				if(!SomeDisabledPredefinedJobsclass.equals("sc-prede-app sc-appBox itemBox selectedOne"))
				{
					flag=true;
				}
				else
				{
					flag=false;
				}
			}
			else if(!SomeEnabledPredefinedJobsclass.equals("sc-prede-app sc-appBox itemBox selectedOne"))
			{

				flag=true;
			}
			String PassStatement = "PASS >> "+Text[i]+" Predefied job is disabled in Customize tool bar Job";
			String FailStatement = "Fail >> "+Text[i]+" Predefied job is not disabled in Customize tool bar Job"; 
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
	}
	public void clickonMorebuttonInConsole() throws InterruptedException {
		morebutton.click();
		waitForXpathPresent("//div[@id='button_menu_holder']/div[1]");
		sleep(2);
		Reporter.log("Clicked on more button of dynamic job", true);
		sleep(2);
	}
	public void verifyPredefinedJobsinConsoleMac_Os() throws InterruptedException {
		String shutdownoption=shutdown.getText();
		boolean flag=shutdownoption.contains("Shut Down");
		String PassStatement1="PASS >> "+shutdownoption+" Predefied job is displaying on console in ios";
		String FailStatement1="FAIL >> "+shutdownoption+" Predefied job is not displaying on console in ios";
		ALib.AssertTrueMethod(flag, PassStatement1, FailStatement1);

		String Osupdateoption=Osupdate.getText();
		boolean flag1=Osupdateoption.contains("OS Updates");
		String PassStatement2="PASS >> "+Osupdateoption+" Predefied job is displaying on console in ios";
		String FailStatement2="FAIL >> "+Osupdateoption+" Predefied job is not displaying on console in ios";
		ALib.AssertTrueMethod(flag1, PassStatement2, FailStatement2);
	}
	@FindBy(id="appBtn2")
	private WebElement morebutton;
	@FindBy(xpath="//div[@class='actionbuttonheader']/div")
	private List<WebElement> dynamictoolheader;
	@FindBy(xpath="//div[@id='button_menu_holder']/div")
	private List<WebElement> dynamictoolbarmenu;
	public void verifyPredefinedJobsinConsoleAndroid() throws InterruptedException {
		String[] Text={"Refresh","Remote","Apps","SureLock","SureFox"};
		for(int i=0;i<dynamictoolheader.size()-1;i++) {
			String ActualValue = dynamictoolheader.get(i).getText();
			boolean flag=ActualValue.equals(Text[i]);
			String PassStatement="PASS >> "+ActualValue+" Predefied job is displaying on console";
			String FailStatement="FAIL >> "+ActualValue+" Predefied job is not displaying on console";
			ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		}
		morebutton.click();
		waitForXpathPresent("//div[@id='button_menu_holder']/div[1]");
		sleep(2);
		Reporter.log("Clicked on more button of dynamic job", true);

		String[] Text1={"Call Logs","SMS Logs","Reboot","Reset","Lock","Message","SureVideo","Wipe","Data Usage", "Remote Buzz","System Scan"};
		for(int j=0;j<11;j++) {
			String ActualValue1 = dynamictoolbarmenu.get(j).getText();
			boolean flag1=ActualValue1.equals(Text1[j]);
			String PassStatement1="PASS >> "+ActualValue1+" Predefied job is displaying on console";
			String FailStatement1="FAIL >> "+ActualValue1+" Predefied job is not displaying on console";
			ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
		}
	}
	//"Remote Buzz","OS Updates","System Scan","Intel� AMT","Recovery Key"
	@FindBy(xpath="//ul[@class='predefined_jobsList jobsList clearfix']/li")
	private List<WebElement> predefinedjobsList;
	public void verifyPredefinedJobs() {
		String[] Text={"Refresh","Remote","Apps","SureLock","SureFox","Call Logs","SMS Logs","Reboot Device","Reset Password","Lock Device","Send Message","SureVideo Settings","Wipe Device","Data Usage","Shut Down"};
		for(int i=0;i<predefinedjobsList.size();i++)
		{
			String ActualValue = predefinedjobsList.get(i).getText();
			boolean value=ActualValue.equals(Text[i]);
			String PassStatement = "PASS >> "+ActualValue+" Predefied job is displayed when Clicked on Customize tool bar Job";
			String FailStatement = "Fail >> "+ActualValue+" Predefied job is not displayed when Clicked on Customize tool bar Job"; 
			ALib.AssertTrueMethod(value, PassStatement, FailStatement);
		}
	}

	@FindBy(xpath="//li[@id='mailSection']")
	private WebElement InboxTab;
	public void ClickOnInbox() throws InterruptedException {
		InboxTab.click();
		waitForidPresent("settPopupBtn");
		sleep(20);
	}
	@FindBy(id="customToolbarTab")
	private WebElement ClickOnCustomizeToolbarSection;

	public void clickOnLinuxOS() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement LinuxOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and8Icon")));
		LinuxOS.click();
		Reporter.log("Clicked on LinuxOS Job", true);
		sleep(4);
	}
	public void clickOnWindowsOS() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement WindowsOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and2Icon")));
		WindowsOS.click();
		Reporter.log("Clicked on Windows Job", true);
		sleep(4);
	}
	@FindBy(id="reset_device")
	private WebElement RebootDevice;
	public void ClickOnRebootJob(String value) throws InterruptedException {
		RebootDevice.click();
		Reporter.log("Clicked on Reboot job", true);
		waitForidPresent("job_reset_name_input");
		sleep(2);
		EnterResetJobName.sendKeys(value);
		sleep(2);
	}
	public void clickOnMacOS() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(Initialization.driver, Config.IMP_WAIT);
		WebElement MacOS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("and11Icon")));
		MacOS.click();
		Reporter.log("Clicked on MacOS Job", true);
		sleep(4);
	}

	public void userdefinedAddButton() throws InterruptedException {
		boolean flag = userdefinedAddButton.isDisplayed();
		String PassStatement="PASS >> ADD button is displayed in user defined section";
		String FailStatement="FAIL >> ADD button is displayed in user defined section";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}
	@FindBy(xpath = "//*[@id=\"conmptableContainer\"]/div[1]/div[1]/div[2]/input")
	private WebElement SearchTextBoxSelectJobToAdd;
	public void SearchJobToAddInCustomizeToolbar(String name) throws InterruptedException {
		SearchTextBoxSelectJobToAdd.sendKeys(name);
		sleep(8);
		Initialization.driver.findElement(By.xpath("//*[@id=\"conmptableContainer\"]/div[1]/div[1]/div[1]/button"))
		.click();
		waitForXpathPresent("//p[text()='" + name + "']");
		System.out.println("Job is searched");
		sleep(15);
	}
	@FindBy(xpath="//button[@id='savecustomdynamicjob']")
	private WebElement saveCustomdyamivJob;
	public void SaveCustomdyamicJob() throws InterruptedException {
		saveCustomdyamivJob.click();
		sleep(6);
		Reporter.log("Saved CustomDynamic job", true);
	}

	@FindBy(xpath="//span[text()='"+Config.Customize_jobnameAndroid+"']")
	private WebElement customizeJobNmaeAndroid;
	public void verifySavedCustomizeJobAndroid() throws InterruptedException {
		boolean flag = customizeJobNmaeAndroid.isDisplayed();
		String PassStatement="PASS >> Uploaded file is displayig in UserDefined Job section";
		String FailStatement="FAIL >> Uploaded file is displayig in UserDefined Job section";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(5);
	}
	@FindBy(xpath="//div[@id='mail_main_pg']/div[2]/div[1]/div[1]/div[1]/button")
	private WebElement RefreshInbox;
	@FindBy(xpath="//table[@id='mailGrid']/tbody/tr/td[3]")
	private List<WebElement> InboxSubConsoleData;
	@FindBy(xpath="//table[@id='mailGrid']/tbody/tr/td[2]")
	private List<WebElement> InboxFromConsoleData;
	@FindBy(xpath="//textarea[@id='messg_textarea']")
	private WebElement InboxBodyConsole;
	public void VerifyTextMessageFromConsoleForDataUsage(String devicename) throws InterruptedException {
		RefreshInbox.click();
		sleep(4);
		boolean flag;
		for(int i=0;i<=1;i++) 
		{
//			for(int j=1;j<=1;j++) 
//			{
				String InboxdeviceSubject = InboxSubConsoleData.get(i).getText();
				String InboxFromConsoleDevice = InboxFromConsoleData.get(i).getText();
				Reporter.log("Inbox Subject: "+InboxdeviceSubject, true);
				InboxSubConsoleData.get(i).click();
				sleep(2);
				waitForXpathPresent("//textarea[@id='messg_textarea']");
				String[] mailDetails= {"Limit2", "Limit1"};
				if(InboxdeviceSubject.contains(mailDetails[i]) && InboxFromConsoleDevice.equals(devicename))
				{
					flag=true;
				}
				else
				{
					flag=false;
				}
				sleep(2);
				String PassStatement="PASS >> TextArea: "+InboxBodyConsole.getText()+" << Message is Recieved from the console";
				String FailStatement="FAIL >> TextArea: "+InboxBodyConsole.getText()+" << Message is not Recieved from the console";
				ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
//			}
		}
	}
	@FindBy(xpath="//table[@class='table table-striped jambo_table']/tbody/tr/td[4]")
	private List<WebElement> message;
	
	public void VerifyTextMessageFromEmailDataUsage(String devicename) throws InterruptedException, AWTException {
		JavascriptExecutor jse = (JavascriptExecutor)Initialization.driver;
		jse.executeScript("window.open()");
        String Mainwindow = Initialization.driver.getWindowHandle();
		Initialization.driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		Initialization.driver.get(Config.NotifiedUrl_Misc);
		sleep(5);
		Initialization.driver.findElement(By.xpath("//input[@id='addOverlay']")).sendKeys(Config.NotifiedEmail_Misc);
		sleep(3);

		Actions action = new Actions(Initialization.driver);
		action.sendKeys(Keys.ENTER).build().perform();

		waitForXpathPresent("//table[@class='table table-striped jambo_table']/thead/tr/th[3]");
		System.out.println(4);
		System.out.println(Initialization.driver.findElement(By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[3]")).getText());
			
//		List<WebElement> message = Initialization.driver.findElements(By.xpath(""));
		
		boolean flag;
		for(int i=0;i<=1;i++) 
		{
				String msg= message.get(i).getText();
				
				String[] mailDetails= {"Limit2", "Limit1"};
				if(msg.contains(mailDetails[i]) && msg.contains(devicename))
				{
					flag=true;
				}
				else
				{
					flag=false;
				}
				String PassStatement="PASS >> "+msg+" << Message is Recieved in the registered email";
				String FailStatement="FAIL >> "+msg+" << Message is not Recieved in the registered email";
				ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
				sleep(3);
		}
			Initialization.driver.close();
		    Initialization.driver.switchTo().window(Mainwindow);
	}
	@FindBy(xpath = ".//*[@id='jobSection']/a")
	private WebElement jobsTab;

	public void clickOnJobs() throws InterruptedException {
		jobsTab.click();
		Reporter.log("Clicked on Jobs", true);
		waitForidPresent("job_new_job");
		sleep(5);
	}

	@FindBy(xpath="//span[text()='Alert Message Analytics']/following-sibling::span/input")
	private WebElement AlertMessageAnalytics;
	
	@FindBy(id="WebHooks")
	private WebElement WebhooksTab;
	
	@FindBy(id="EnableWebhook")
	private WebElement EnablewebhookCheckbox;
	
	@FindBy(id="ApplyWebhookSettings")
	private WebElement webhookSettingsApplybutton;
	
	public void ClickOnWebhookstab() throws InterruptedException
	{
		WebhooksTab.click();
		waitForXpathPresent("//span[text()='Enable Webhooks']");
		sleep(1);
	}
	
	public void DisableWebhooks() throws InterruptedException
	{
		if(EnablewebhookCheckbox.isSelected())
			EnablewebhookCheckbox.click();	
		ClickOnWebhooksApplyButton();
	}
	
	public void ClickOnWebhooksApplyButton() throws InterruptedException
	{
		webhookSettingsApplybutton.click();
		waitForXpathPresent("//span[text()='Settings updated successfully.']");
		sleep(2);
	}
	public void EnableAlertMessageAnalyticsCheckBox() throws InterruptedException
	{
		boolean flag = AlertMessageAnalytics.isSelected();
		if(flag)
		{
			Reporter.log("AlertMessageAnalytics CheckBoxIs Already Enabled",true);
			ClickOnDataAnalyticsApplyButton();
			clickOnJobs();
			
		}
		else
		{
			AlertMessageAnalytics.click();
			Reporter.log("Enabled AlertMessageAnalytics CheckBox",true);

			ClickOnDataAnalyticsApplyButton();
			clickOnJobs();


		}
	}
	public void DisableAlertMessageAnalyticsCheckBox()
	{
		boolean flag=AlertMessageAnalytics.isSelected();
		if(flag)
		{
			Reporter.log("AlertMessageAnalytics CheckBoxIs Already Enabled Hence Disabling AlertMessageAnalytics",true);
			AlertMessageAnalytics.click();
		}
		else
		{
			Reporter.log("AlertMessageAnalytics Is Already Disabled",true);
		}
	}

public void EnableAlertAnalytics() throws InterruptedException {
	
	JavascriptExecutor Jc=(JavascriptExecutor)Initialization.driver;
	Jc.executeScript("arguments[0].scrollIntoView()",AlertMessageAnalytics);
	sleep(3);
	AlertMessageAnalytics.click();

}
public void ClcikOnAlertAnalyticsEnablePopUpYesButton() throws InterruptedException
{
	SureLockAnalyticsEnablePopUpYesButton.click();
	sleep(2);
	Reporter.log("Clicked On Analytics PopUp Yes Button",true);
}

//=========================================Mithilesh (May-2021)============================================

@FindBy(xpath="//*[@id='iOSmacOSSettings']/a")   
private WebElement ClickOniosSettings ;            

@FindBy(id="uploadpemcertificateModal")
private WebElement UploadpemCertificate ;

@FindBy(xpath="//a[text()='SAML Single Sign-On']")
private WebElement ClickOnSAMLSingleSignOn ;

public void ClickOniosSettings() throws InterruptedException{                 
	Helper.highLightElement(Initialization.driver, ClickOniosSettings);       
	ClickOniosSettings.click();                                                
	waitForXpathPresent(XpathforPageToLoadAccountSettings);
	sleep(4);                                                                  
}

public void UploadpemCertificate() throws InterruptedException{                 
	Helper.highLightElement(Initialization.driver, UploadpemCertificate);       
	UploadpemCertificate.click();                                                
	waitForXpathPresent(XpathforPageToLoadAccountSettings);
	sleep(4);                                                                  
}

@FindBy(xpath="//a[text()='SAML Single Sign-On']")
private WebElement ClickonSAMLsinglebutton;

public void ClickonSAMLsinglebutton() throws InterruptedException
{
	ClickonSAMLsinglebutton.click();
	waitForXpathPresent("//*[@id='sso_signonservice_row']/span[1]/span");
	sleep(3);
}

@FindBy(xpath="//input[@id='enable_sso']")
private WebElement EnableSSOCheckbox ;

public void EnableSSOCheckbox() throws InterruptedException
{
	EnableSSOCheckbox.click();
	sleep(3);
}

public void selectEnableSSOcheckbox() throws InterruptedException {
	
	boolean flag =EnableSSOCheckbox.isSelected();
	if(flag==false) {
		
		EnableSSOCheckbox.click();
		sleep(3);
	}
	Reporter.log("PASS >> Enable SSO Check Box",true);
}

@FindBy(xpath="//input[@id='enable_sso']")
private WebElement EnableSingleSignOnCheckboxVisible;

public void EnableSingleSignOnCheckboxVisible() throws InterruptedException {
	boolean flag = EnableSingleSignOnCheckboxVisible.isEnabled();
	String PassStatement="PASS >> Single Sign-On check-box is Enabled";
	String FailStatement="FAIL >> Single Sign-On check-box is disabled";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='sso_tab']/div[1]/div[2]/div/span")
private WebElement SingleSignOnTypeOptionVisible;

public void SingleSignOnTypeOptionVisible() throws InterruptedException {
	boolean flag = SingleSignOnTypeOptionVisible.isDisplayed();
	String PassStatement="PASS >> Single Sign On Type Option is Displayed";
	String FailStatement="FAIL >> Single Sign On Type Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='sso_serviceId_row']/div/span")
private WebElement ServiceIdentifierOptionVisible;

public void ServiceIdentifierOptionVisible() throws InterruptedException {
	boolean flag = ServiceIdentifierOptionVisible.isDisplayed();
	String PassStatement="PASS >> Service Identifier Option is Displayed";
	String FailStatement="FAIL >> Service Identifier Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='sso_signonservice_row']/div/span")
private WebElement SignOnServiceUrlOptionVisible;

public void SignOnServiceUrlOptionVisible() throws InterruptedException {
	boolean flag = SignOnServiceUrlOptionVisible.isDisplayed();
	String PassStatement="PASS >> Sign On Service Url Option is Displayed";
	String FailStatement="FAIL >> Sign On Service Url Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='sso_logoutservice_row']/div/span")
private WebElement LogoutServiceUrlOptionVisible;

public void LogoutServiceUrlOptionVisible() throws InterruptedException {
	boolean flag = LogoutServiceUrlOptionVisible.isDisplayed();
	String PassStatement="PASS >> Logout Service Url Option is Displayed";
	String FailStatement="FAIL >> Logout Service Url Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='sso_tab']/div[1]/div[6]/div/span")
private WebElement RolesOptionVisible;

public void RolesOptionVisible() throws InterruptedException {
	boolean flag = RolesOptionVisible.isDisplayed();
	String PassStatement="PASS >> Roles Option is Displayed";
	String FailStatement="FAIL >> Roles Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='sso_tab']/div[1]/div[7]/div/span")
private WebElement DeviceGroupSetOptionVisible;

public void DeviceGroupSetOptionVisible() throws InterruptedException {
	boolean flag = DeviceGroupSetOptionVisible.isDisplayed();
	String PassStatement="PASS >> Device Group Set Option is Displayed";
	String FailStatement="FAIL >> Device Group Set Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='sso_tab']/div[1]/div[8]/div/span")
private WebElement JobsProfilesFolderSetOptionVisible;

public void JobsProfilesFolderSetOptionVisible() throws InterruptedException {
	boolean flag = JobsProfilesFolderSetOptionVisible.isDisplayed();
	String PassStatement="PASS >> Jobs Profiles Folder Set Option is Displayed";
	String FailStatement="FAIL >> Jobs Profiles Folder Set Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(4);
}

@FindBy(xpath="//button[@id='DeleteCertificate']")
private WebElement DeleteCertificateButtonVisible;

public void DeleteCertificateButtonVisible() throws InterruptedException {
	boolean flag = DeleteCertificateButtonVisible.isDisplayed();
	String PassStatement="PASS >> Delete Certificate Button is Displayed";
	String FailStatement="FAIL >> Delete Certificate Button is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(4);
}

@FindBy(xpath="//button[@id='DownloadCertificate']")
private WebElement DownloadCertificateButtonVisible;

public void DownloadCertificateButtonVisible() throws InterruptedException {
	boolean flag = DownloadCertificateButtonVisible.isDisplayed();
	String PassStatement="PASS >> Download Certificate Button is Displayed";
	String FailStatement="FAIL >> Download Certificate Button is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(4);
}


@FindBy(xpath="//input[@id='sso_setng_done']")
private WebElement DoneButtonVisible;

public void DoneButtonVisible() throws InterruptedException {
	boolean flag = DoneButtonVisible.isDisplayed();
	String PassStatement="PASS >> Done Button is Displayed";
	String FailStatement="FAIL >> Done Button is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

public void SingleSignOnTypeDropDownValue() {
	
	Select s = new Select(Initialization.driver.findElement(By.xpath("//select[@id='sso_service_selEle']")));
    List <WebElement> op = s.getOptions();
    int size = op.size();
    for(int i =0; i<size ; i++){
       String options = op.get(i).getText();
       
       
       if(options.contains("ADFS")||options.contains("Azure AD")||options.contains("Okta")
    		   ||options.contains("OneLogin")||options.contains("PingOne")||options.contains("Google G Suite")
    		   ||options.contains("Generic")){
    	    	   System.out.println("PASS >> "+options+ " Option Dispalyed in Dropdown" );
    	       }
       else {
    	   System.out.println("FAIL >> "+options+ " Option not Dispalyed in Dropdown");
       }
    }		
}

//upload certificate saml

@FindBy(xpath="//button[@id='DeleteCertificate']")
private WebElement DeleteVisible ;

public void DeleteVisible()throws InterruptedException{
	
	boolean flag=DeleteVisible.isDisplayed();
	if(flag)
	{	   
		sleep(5);
		DeleteVisible.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Delete Certificate",true);
	}
	else
	{	
		Reporter.log("FAIL >> Not Clicked on Delete Certificate",true);
	}
}

@FindBy(xpath="//button[@id='UploadCertficate']")
private WebElement UploadCertficateVisible ;

@FindBy(xpath="//*[@id='ssoCertificateAddPopup']/div/div/div[2]/div[1]/div/div/span")
private WebElement ClickedonBrowserFile ;

public void UploadCertficateVisible()throws InterruptedException{
	
	boolean flag=UploadCertficateVisible.isDisplayed();
	if(flag)
	{	   
		sleep(5);
		UploadCertficateVisible.click();
		sleep(5);
		//ClickedonBrowserFile.click();
		//sleep(5);
		Reporter.log("PASS >> Clicked on Upload Certficate",true);
	}
	else
	{	
		Reporter.log("FAIL >> Not Clicked on Upload Certficate",true);
	}
}

@FindBy(xpath="//div[@class='modal-body install-app-pop']//span[@class='input-group-addon labelno-bg mandatory ct-model-label'][normalize-space()='Certificate']")
private WebElement CertficateOptionVisible ;

@FindBy(xpath="//span[@class='input-group-addon labelno-bg ct-model-label'][normalize-space()='Password']")
private WebElement PasswordOptionVisible ;

@FindBy(xpath="//*[@id='ssoCertificateAddPopup']/div/div/div[2]/div[1]/div/div/span")
private WebElement BrowserOptionVisible ;

@FindBy(xpath="//*[@id='ssoCertificateAddPopup']/div/div/div[1]/button")
private WebElement CloseCertificateWindow ;

public void CertficateOptionVisible()throws InterruptedException{
	
	boolean flag=CertficateOptionVisible.isDisplayed();
	if(flag)
	{	   
		sleep(5);		
		Reporter.log("PASS >> Certficate Option is Displayed on Certificate Window",true);
	}
	else
	{	
		ALib.AssertFailMethod("FAIL >> Certficate Option is Not Displayed on Certificate Window");
		Reporter.log("FAIL >> Certficate Option is Not Displayed on Certificate Window",true);
	}
}

public void PasswordOptionVisible()throws InterruptedException{
	
	boolean flag=PasswordOptionVisible.isDisplayed();
	if(flag)
	{	   
		sleep(5);		
		Reporter.log("PASS >> Password Option is Displayed on Certificate Window",true);
	}
	else
	{	
		ALib.AssertFailMethod("FAIL >> Password Option is Not Displayed on Certificate Window");
		Reporter.log("FAIL >> Password Option is Not Displayed on Certificate Window",true);
	}
}

public void BrowserOptionVisible()throws InterruptedException{
	
	boolean flag=BrowserOptionVisible.isDisplayed();
	if(flag)
	{	   
		sleep(5);		
		Reporter.log("PASS >> Browser Option is Displayed on Certificate Window",true);
	}
	else
	{	
		ALib.AssertFailMethod("FAIL >> Browser Option is Not Displayed on Certificate Window");
		Reporter.log("FAIL >> Browser Option is Not Displayed on Certificate Window",true);
	}
}

public void CloseCertificateWindow()throws InterruptedException{
	
	boolean flag=CloseCertificateWindow.isDisplayed();
	if(flag)
	{	   		
		sleep(2);	
		CloseCertificateWindow.click();
		Reporter.log("PASS >> Certificate Tab is Closed",true);
	}
	else
	{	
		ALib.AssertFailMethod("FAIL >> Certificate Tab is Not Closed");
		Reporter.log("FAIL >> Certificate Tab is Not Closed",true);
	}
}

public void ClickedonBrowserFile()throws InterruptedException{
	
	boolean flag=ClickedonBrowserFile.isDisplayed();
	if(flag)
	{	   
		sleep(5);
		ClickedonBrowserFile.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Browser Button",true);
	}
	else
	{	
		Reporter.log("FAIL >> Not Clicked on Browser Button",true);
	}
}

@FindBy(xpath="//div[@id='ssoCertificateAddPopup']//button[@type='button'][normalize-space()='OK']")
private WebElement ClickSAMLOkbutton ;

public void browseFileSAMLSingleSignOn(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);
	sleep(5);
	ClickSAMLOkbutton.click();
	Reporter.log("PASS >> SAML Certificate Uploaded Successfully", true);
	sleep(5);
	
}

public void AdfsSelected() throws InterruptedException {
	
	   Select se = new Select(Initialization.driver.findElement(By.xpath("//select[@id='sso_service_selEle']")));
	   se.selectByVisibleText("ADFS");
	   Reporter.log("PASS >> ADFS option Selected From Drop Down", true);
	   sleep(5);
	   	   
	}

@FindBy(xpath="//input[@id='sso_fsi']")
private WebElement ServiceIdentiferADFS ;

public void ServiceIdentiferADFS()throws InterruptedException{
	
	boolean flag=ServiceIdentiferADFS.isDisplayed();
	if(flag)
	{
		ServiceIdentiferADFS.clear();
		ServiceIdentiferADFS.sendKeys("http://adfs.42gears.com/adfs/services/trust");
		sleep(5);
		Reporter.log("PASS >> Service Identifer Url Entered Successfully",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Service Identifer Url Not Entered Successfully");
		Reporter.log("FAIL >> Service Identifer Url Not Entered Successfully",true);
	}
}

@FindBy(xpath="//input[@id='sso_url']")
private WebElement SignOnServiceUrlADFS ;

public void SignOnServiceUrlADFS()throws InterruptedException{
	
	boolean flag=SignOnServiceUrlADFS.isDisplayed();
	if(flag)
	{
		SignOnServiceUrlADFS.clear();
		SignOnServiceUrlADFS.sendKeys("https://ad.42gears.com/ls/");
		sleep(5);
		Reporter.log("PASS >> Sign On Service Url Entered Successfully",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Sign On Service Url Not Entered Successfully");
		Reporter.log("FAIL >> Sign On Service Url Not Entered Successfully",true);
	}
}

@FindBy(xpath="//input[@id='sso_logout_url']")
private WebElement LogoutServiceUrlADFS ;

@FindBy(xpath="//input[@id='sso_setng_done']")
private WebElement DoneADFS ;

public void LogoutServiceUrlADFS()throws InterruptedException{
	
	boolean flag=LogoutServiceUrlADFS.isDisplayed();
	if(flag)
	{
		LogoutServiceUrlADFS.clear();
		LogoutServiceUrlADFS.sendKeys("https://ad.42gears.com/ls/");
		sleep(5);
		Reporter.log("PASS >> Logout Service Url Entered Successfully",true);	
		
		DoneADFS.click();
		waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");
		String Msg=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();		
		Reporter.log("PASS >> Success Message Displayed is : "+Msg,true);
		sleep(5);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Logout Service Url Not Entered Successfully");
		Reporter.log("FAIL >> Logout Service Url Not Entered Successfully",true);
	}
}

@FindBy(xpath="//*[@id='ssoLinkUrl']")
private WebElement LoginUrl ;

public void LoginURLforSSOUser()throws InterruptedException{

     LoginUrl.click();
     sleep(5);
     Set<String> set=Initialization.driver.getWindowHandles();
     Iterator<String> id=set.iterator();
     String parentwinID = id.next();
     String childwinID = id.next();
     Initialization.driver.switchTo().window(childwinID);
     sleep(15);
     waitForXpathPresent("//a[normalize-space()='Home']");
     sleep(5);
     String URL=Initialization.driver.getCurrentUrl();
     sleep(5);
     //String ExpectedValue ="https://stagemdm.42gears.com/console/ConsolePage/Master.html?7.10.4#suremdm";    	 
     String ExpectedValue = URL ;
     //String ExpectedValue = "https://licensedindia.in.suremdm.io/console/ConsolePage/Master.html?79.1#suremdm";
     String PassStatement = "PASS >> Current url for SSO Login is: "+URL;
     String FailStatement = "FAIL >> Current url for SSO Login is Not Active";
     ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
     Initialization.driver.close();
     Initialization.driver.switchTo().window(parentwinID);
     sleep(5);
}

@FindBy(xpath="//a[@id='userProfileButton']")
private WebElement ClickedonSSOSettings ;

@FindBy(xpath="//button[normalize-space()='Logout']")
private WebElement LogoutSSOUser ;

@FindBy(xpath="//a[normalize-space()='Login again']")
private WebElement ClickonLoginAgain ;

public void LoginLogoutforSSOUser()throws InterruptedException{

    LoginUrl.click();
    sleep(5);
    Set<String> set=Initialization.driver.getWindowHandles();
    Iterator<String> id=set.iterator();
    String parentwinID = id.next();
    String childwinID = id.next();
    Initialization.driver.switchTo().window(childwinID);
    sleep(15);
    waitForXpathPresent("//a[normalize-space()='Home']");
  
    String URL=Initialization.driver.getCurrentUrl();
    System.out.println(URL);
    
    String ExpectedValue = URL ;
    //String ExpectedValue = "https://licensedindia.in.suremdm.io/console/ConsolePage/Master.html?79.1#suremdm";
    String PassStatement = "PASS >> Current url for SSO Login is: "+URL;
    String FailStatement = "FAIL >> Current url for SSO Login is Not Active";
    ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
    sleep(5);
    ClickedonSSOSettings.click();
    sleep(5);
    
    boolean flag=LogoutSSOUser.isDisplayed();
	if(flag)
	{
		sleep(10);
		LogoutSSOUser.click();
		sleep(5);
		Reporter.log("PASS >> Logout from Single Sign on SureMDM successfully",true);  				
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Logout from Single Sign on SureMDM unsuccessfully");
		Reporter.log("FAIL >> Logout from Single Sign on SureMDM unsuccessfully",true);
	}
	
    waitForXpathPresent("//a[normalize-space()='Login again']");
    ClickonLoginAgain.click();
    sleep(5);
    userNAmeEdt.sendKeys(Config.userID);
	passwordEdt.sendKeys(Config.password);
	loginBtn.click();
	sleep(5);
	waitForXpathPresent("//a[normalize-space()='Home']");
	sleep(5);
	Reporter.log("PASS >> Logged into SureMDM successfully",true);     
    Initialization.driver.close();
    Initialization.driver.switchTo().window(parentwinID);
    sleep(5);
}



@FindBy(xpath="//td[normalize-space()='Mithilesh_Superuser']")
private WebElement ClickonSuperuser ;

@FindBy(xpath="//div[@id='editUser']//i[@class='icn fa fa-pencil-square-o']")
private WebElement ClickonEDITSuperuser ;

@FindBy(xpath="//button[@id='okbtn']")
private WebElement ClickonOK ;

public void ClickonSuperuser()throws InterruptedException{
	
	boolean flag=ClickonSuperuser.isDisplayed();
	if(flag)
	{
		sleep(5);
		JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
		WebElement scrollDown = ClickonSuperuser;
		Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);
		
		ClickonSuperuser.click();
		sleep(5);
		ClickonEDITSuperuser.click();
		sleep(5);
				
		Select se = new Select(Initialization.driver.findElement(By.xpath("//select[@id='ListFeaturePermissions']")));
		se.selectByVisibleText("Super User");
		sleep(5);
		Reporter.log("PASS >> Super user is Edited",true);  
		ClickonOK.click();				
		waitForXpathPresent("//span[contains(text(),'The user was modified successfully.')]");
		String Msg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'The user was modified successfully.')]")).getText();		
		System.out.println("PASS >> Message Displayed is : "+Msg);
		sleep(2);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Super User Not Modified Successfully");
		Reporter.log("FAIL >> Super User Not Modified Successfully",true);
	}
}

@FindBy(xpath="//button[normalize-space()='User Management']")
private WebElement UserManagementVisible ;


public void ValidatingSuperUser()throws InterruptedException{

	LoginUrl.click();
    sleep(5);
    Set<String> set=Initialization.driver.getWindowHandles();
    Iterator<String> id=set.iterator();
    String parentwinID = id.next();
    String childwinID = id.next();
    Initialization.driver.switchTo().window(childwinID);
    sleep(5);
    waitForXpathPresent("//a[normalize-space()='Home']");
    sleep(5);
    String URL=Initialization.driver.getCurrentUrl();
    sleep(5);
    String ExpectedValue = URL ;
   // String ExpectedValue = "https://licensedindia.in.suremdm.io/console/ConsolePage/Master.html?79.1#suremdm";
    String PassStatement = "PASS >> Current url for SSO Login is: "+URL;
    String FailStatement = "FAIL >> Current url for SSO Login is Not Active";
    ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
    sleep(5);
    ClickedonSSOSettings.click();
    sleep(5);

    boolean flag=UserManagementVisible.isDisplayed();
	if(flag)
	{
		sleep(10);
		UserManagementVisible.click();
		sleep(5);
		Reporter.log("PASS >> SSO User Clicked on User Management",true);  				
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> SSO User Not Clicked on User Management");
		Reporter.log("FAIL >> SSO User Not Clicked on User Management",true);
	}  
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonSuperuser;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	Reporter.log("PASS >> Super user is Visible in SSO Login",true); 
	ClickonSuperuser.click();
	sleep(5);
	ClickonEDITSuperuser.click();
	sleep(5);
			
	Select se = new Select(Initialization.driver.findElement(By.xpath("//select[@id='ListFeaturePermissions']")));
	se.selectByVisibleText("Super User");
	sleep(5);
	Reporter.log("PASS >> Super user is Edited",true);  
	ClickonOK.click();				
	waitForXpathPresent("//span[contains(text(),'The user was modified successfully.')]");
	String Msg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'The user was modified successfully.')]")).getText();		
	System.out.println("PASS >> Message Displayed is : "+Msg);
	sleep(2);		
	
	Initialization.driver.close();
    Initialization.driver.switchTo().window(parentwinID);
    sleep(5);
    
}


@FindBy(xpath="//*[@id='userManagement_pg']/section/div[1]/ul/li[2]/a")
private WebElement ClickedonRoles ;

@FindBy(xpath="//p[normalize-space()='SSOuser']")
private WebElement SSOUser ;

@FindBy(xpath="//div[@class='sc-editTemplate-btn actionbutton ct-menuBtn']//i[@class='icn fa fa-pencil-square-o']")
private WebElement ClickonRolesEDIT ;

@FindBy(xpath="//*[@id='umTree']/ul/li[1]/span[3]")
public WebElement GroupTagFilterPermissions ;

@FindBy(xpath="//*[@id='umTree']/ul/li[2]/span[3]")
public WebElement DeviceActionPermissions ;

@FindBy(xpath="//*[@id='umTree']/ul/li[3]/span[3]")
public WebElement DeviceManagementPermissions ;

@FindBy(xpath="//*[@id='umTree']/ul/li[4]/span[3]")
public WebElement ApplicationSettingsPermissions ;

@FindBy(xpath="//button[@id='SavePermissions']")
private WebElement SaveSSOROlesSettings ;

public void ModifiedRoles()throws InterruptedException{
	    
	    boolean flag=ClickedonRoles.isDisplayed();
		if(flag)
		{			
			ClickedonRoles.click();
			sleep(5);
			Reporter.log("PASS >> Clicked on Roles",true);				
		}
		else
		{		
			ALib.AssertFailMethod("FAIL >> SSO User Not Clicked on User Management");
			Reporter.log("FAIL >> SSO User Not Clicked on User Management",true);
		}  
  
	    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
		WebElement scrollDown = SSOUser;
		Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		sleep(4);
	
		SSOUser.click();
		sleep(4);
		Reporter.log("PASS >> SSO User is Selected ",true); 
		
		ClickonRolesEDIT.click();
		sleep(4);
		
		
}

JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;

public void SelectingAllPermissions(String Permission) throws InterruptedException
{
	WebElement ele = Initialization.driver
			.findElement(By.xpath("//div[@id='umTree']/ul/li[text()='"+Permission+"']/span[1]/following-sibling::span[2]"));
	js.executeScript("arguments[0].scrollIntoView(true);", ele);
	ele.click();
	sleep(3);
	Reporter.log("PASS >> Selected "+Permission+" Permissions",true);
}


public void VerifyOptionForSSOuser(WebElement ele,String Option)
{
	try
	{
	ele.isDisplayed();
	Reporter.log("PASS >> "+Option+" Option Is Selected for SSO User",true);
	}
	catch (Exception e) {
		ALib.AssertFailMethod("FAIL >> "+Option+" Option Is not Selected for SSO User");
	}
}


@FindBy(xpath="//button[@id='SavePermissions']")
private WebElement  ClickedonRolesSave;

public void ClickedonRolesSave()throws InterruptedException{
	 sleep(5);
	 ClickedonRolesSave.click();
	 sleep(5);
    
}

public void Return()throws InterruptedException{
	
	
	Initialization.driver.close();
	Initialization.driver.switchTo().window(parentwinID);
	sleep(5);

   
}



String parentwinID;

public void LoginLogoutforSSOuserVerifyRoles()throws InterruptedException{

    LoginUrl.click();
    sleep(5);
    Set<String> set=Initialization.driver.getWindowHandles();
    Iterator<String> id=set.iterator();
    parentwinID = id.next();
    String childwinID = id.next();
    Initialization.driver.switchTo().window(childwinID);
    sleep(15);
    waitForXpathPresent("//a[normalize-space()='Home']");
  
    String URL=Initialization.driver.getCurrentUrl();
    String ExpectedValue = URL ;
    //String ExpectedValue = "https://licensedindia.in.suremdm.io/console/ConsolePage/Master.html?79.1#suremdm";
    String PassStatement = "PASS >> Current url for SSO Login is: "+URL;
    String FailStatement = "FAIL >> Current url for SSO Login is Not Active";
    ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
    
}

@FindBy(xpath="//input[@id='sso_setng_done']")
private WebElement  DisabledDone ;


public void SelectDisbaleSSOcheckbox() throws InterruptedException {
	
	boolean flag =EnableSSOCheckbox.isSelected();
	if(flag==true) {
		
		EnableSSOCheckbox.click();
		sleep(3);
		
	}
	Reporter.log("PASS >> Disabled SSO Check Box",true);
}

public void SaveDisbaleSSOcheckbox() throws InterruptedException {
	
	sleep(3);
	DisabledDone.click();
	sleep(3);
	Reporter.log("PASS >> Clicked on Save",true);
	waitForXpathPresent("//span[contains(text(),'Settings updated successfully.')]");
	String Msg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'Settings updated successfully.')]")).getText();		
	System.out.println("PASS >> Message Displayed is : "+Msg);
	sleep(4);
}

//Account Management 

@FindBy(xpath="//a[text()='Account Management']")
private WebElement ClickonAccountManagement;

public void ClickonAccountManagement() throws InterruptedException
{
	ClickonAccountManagement.click();
	waitForXpathPresent("//button[text()='Clear Now']");
	sleep(3);
}

@FindBy(xpath="//button[text()='Delete Account']")
private WebElement ClickOnDeleteAccount;

@FindBy(xpath="//button[@id='account_management_onClose']")
private WebElement ClickOnCancelDeleteRequest ;

public void ClickOnDeleteAccounts() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickOnDeleteAccount;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	ClickOnDeleteAccount.click();
	Reporter.log("PASS >> Clicked on Delete Accounts",true);
	sleep(2);		
}

public void ClickOnCancelDeleteRequest()throws InterruptedException{
	
	boolean flag=ClickOnCancelDeleteRequest.isDisplayed();
	if(flag)
	{
		ClickOnCancelDeleteRequest.click();
		sleep(2);
		Reporter.log("PASS >> Account Deletion request cancelled successfully",true);		
	}
	else
	{
		Reporter.log("FAIL >>  Account Deletion request Not cancelled successfully",true);
	}
}

@FindBy(xpath="//button[text()='Delete Account']")
private WebElement DeleteAccountButtonVisible;

public void DeleteAccountButtonVisible() throws InterruptedException {
	boolean flag = DeleteAccountButtonVisible.isDisplayed();
	String PassStatement="PASS >> Delete Account Button is Displayed";
	String FailStatement="FAIL >> Delete Account is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//*[@id='account_management_content']/div/div[6]/div[2]/div/span")
private WebElement IntercomAccessForUser ;

public void IntercomAccessForUser() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = IntercomAccessForUser;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);		
}

@FindBy(xpath="//span[text()='Intercom access for User']")
private WebElement IntercomAccessForUserOptionVisible;

public void IntercomAccessForUserOptionVisible() throws InterruptedException {
	boolean flag = IntercomAccessForUserOptionVisible.isDisplayed();
	String PassStatement="PASS >> Intercom Access For User is Displayed";
	String FailStatement="FAIL >> Intercom Access For User Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

//@FindBy(xpath="//option[@value='Mithilesh']")
//private WebElement SuperUserEmailVisble;

@FindBy(xpath="//option[@value='stage@vomoto.com']")
private WebElement SuperUserEmailVisble;

public void SuperUserEmailVisble() throws InterruptedException {
	
	Select se = new Select(Initialization.driver.findElement(By.xpath("//select[@id='intercom_user']")));
	se.selectByVisibleText("stage@vomoto.com");
	//se.selectByVisibleText("licensedindia@dispostable.com");
	boolean flag = SuperUserEmailVisble.isDisplayed();
	String PassStatement="PASS >> Super User Email is Displayed";
	String FailStatement="FAIL >> Super User Email is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

public void SelectSubUser() {
	
   Select se = new Select(Initialization.driver.findElement(By.xpath("//select[@id='intercom_user']")));
   se.selectByVisibleText("Mithilesh_Superuser");
}

@FindBy(xpath="//button[@id='userintercom-add-btn']")
private WebElement ClickOnSaveIntercomButton ;

public void ClickOnSaveIntercomButton()throws InterruptedException{
	
	boolean flag=ClickOnSaveIntercomButton.isDisplayed();
	if(flag)
	{
		ClickOnSaveIntercomButton.click();
		sleep(2);
		Reporter.log("PASS >> Intercom access given to user ",true);		
	}
	else
	{
		Reporter.log("FAIL >>  Intercom access Not given to user ",true);
	}
}

public void SingleSelectedDropDown()
{	
	Select se = new Select(Initialization.driver.findElement(By.xpath("//select[@id='intercom_user']")));
	boolean result =se.isMultiple();
	System.out.println(result);
	
	if(result==false) 
	{	
		Reporter.log("PASS >> Intercom Access For User is Single Selectable ",true);
		
	}else {
		
		Reporter.log("FAIL >> Intercom Access For User is Multiple Selectable  ",true);
	}	
}

@FindBy(xpath="//*[@id='ConfirmationDialog_DeleteAccount']/div/div/div[1]/p[1]")
private WebElement PopUpMessagevisible ;

public void PopUpMessagevisible() throws InterruptedException {

       String Message = PopUpMessagevisible.getText();
       System.out.println("PASS >> Pop-up Message displayed is: " + Message +" account deletion?");

}

//API KEY

@FindBy(xpath="//span[normalize-space()='API Key']")
private WebElement APIKeyEncryptionFormat ;

public void APIKeyEncryptionFormat() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = APIKeyEncryptionFormat;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	String Format=Initialization.driver.findElement(By.xpath("//input[@id='apiKey_input']")).getAttribute("Value");
		
	if(Format.isEmpty())
	{	
		Reporter.log("PASS >> Encrypted API Key Password is Present",true);
		
	}else {
		
		Reporter.log("FAIL >> Encrypted API Key Password is Not Present ",true);
	}	
}

@FindBy(xpath="//div[@id='copytoClipboard_apiKey']")
private WebElement  CopytoClipBoardButton ;

public void CopytoClipBoardButton()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = APIKeyEncryptionFormat;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
//	Initialization.driver.findElement(By.xpath("//span[@id='check_isPwdVisible']")).click();
//	sleep(4);
		
	boolean flag=CopytoClipBoardButton.isSelected();
	if(flag)
	{
		sleep(2);
		Reporter.log("FAIL >> Copy to Clip Board Option is Not grayed Out",true);		
	}
	else
	{  
		Reporter.log("PASS >> Copy to Clip Board Option is grayed Out",true);
	}
}

@FindBy(xpath="//div[@id='copytoClipboard_apiKey']")
private WebElement  APIKeyCannotbeCopied ;

public void APIKeyCannotbeCopied()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = APIKeyEncryptionFormat;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=APIKeyCannotbeCopied.isSelected();
	if(flag)
	{
		APIKeyCannotbeCopied.click();
		sleep(2);
		Reporter.log("FAIL >> API Key Can be Copied",true);		
	}
	else
	{  
		Reporter.log("PASS >> API Key Cannot be Copied",true);
	}
}

@FindBy(xpath="//div[@id='copytoClipboard_apiKey']")
private WebElement  ClipBoardCopyButtonVisible ;

public void ClipBoardCopyButtonVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = APIKeyEncryptionFormat;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	Initialization.driver.findElement(By.xpath("//span[@id='check_isPwdVisible']")).click();
	Reporter.log("PASS >> Clicked on Eye Password Button",true);
	sleep(4);
		
	boolean flag=ClipBoardCopyButtonVisible.isDisplayed();
	if(flag)
	{
		sleep(2);
		Reporter.log("PASS >> Clip Board Copy Button is Displayed",true);		
	}
	else
	{  
		Reporter.log("FAIL >> Clip Board Copy Button is Not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='API Key']")
private WebElement APIKeyCopiedtoClipBoard ;

public void APIKeyCopiedtoClipBoard() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = APIKeyEncryptionFormat;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	Initialization.driver.findElement(By.xpath("//span[@id='check_isPwdVisible']")).click();
	Reporter.log("PASS >> Clicked on Eye Password Button",true);
			
	String Value=Initialization.driver.findElement(By.xpath("//input[@id='apiKey_input']")).getAttribute("value");
		
	if(Value.isEmpty())
	{		
		Reporter.log("FAIL >> API Key Not Copied from Clip Board",true);
		
	}else {
		
		Reporter.log("PASS >> API Key Copied from Clip Board is: "+Value,true);
	}	
}

@FindBy(xpath="//span[normalize-space()='API Key']")
private WebElement SubUserAPIKeyCopiedtoClipBoard ;

public void SubUserAPIKeyCopiedtoClipBoard() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = APIKeyEncryptionFormat;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	Initialization.driver.findElement(By.xpath("//span[@id='check_isPwdVisible']")).click();
	Reporter.log("PASS >> Clicked on Eye Password Button",true);
			
	String Value=Initialization.driver.findElement(By.xpath("//input[@id='apiKey_input']")).getAttribute("value");
		
	if(Value.isEmpty())
	{		
		Reporter.log("FAIL >> Sub-User API Key Not Copied from Clip Board",true);
		
	}else {
		
		Reporter.log("PASS >> Sub-User API Key Copied from Clip Board is: "+Value,true);
	}	
}

@FindBy(xpath="//span[normalize-space()='API Key']")
private WebElement DiffrentUserAPIKeyCopiedtoClipBoard ;

public void DiffrentUserAPIKeyCopiedtoClipBoard() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = APIKeyEncryptionFormat;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	Initialization.driver.findElement(By.xpath("//span[@id='check_isPwdVisible']")).click();
	Reporter.log("PASS >> Clicked on Eye Password Button");
			
	String Value=Initialization.driver.findElement(By.xpath("//input[@id='apiKey_input']")).getAttribute("value");
		
	if(Value.isEmpty())
	{		
		Reporter.log("FAIL >> Different User API Key Not Copied from Clip Board",true);
		
	}else {
		
		Reporter.log("PASS >> Different User API Key Copied from Clip Board is: "+Value,true);
	}	
}

//Generic Example Url Visible SAML

public void SelectGenericOption() {
	
    Select se = new Select(Initialization.driver.findElement(By.xpath("//*[@id='sso_service_selEle']")));
    se.selectByVisibleText("Generic");

}

@FindBy(xpath="//*[@id='sso_serviceId_row']/span[1]/span")
private WebElement ServiceIdentifierExampleUrlVisible ;

@FindBy(xpath="//*[@id='sso_signonservice_row']/span[1]/span")
private WebElement SignOnServiceExampleUrlVisible ;

@FindBy(xpath="//*[@id='sso_logoutservice_row']/span[1]/span")
private WebElement LogoutServiceUrlExampleUrlVisible ;

@FindBy(xpath="//*[@id='ssoLinkUrl']")
private WebElement LoginURLforSingleSignOnusersVisible ;

public void GenericExampleUrlVisible() throws InterruptedException
{
	String ExampleURL1 = ServiceIdentifierExampleUrlVisible.getText();
	String Expected1 = ExampleURL1 ;
	String pass1 = "PASS >> Service Identifier Example Url is : " +ExampleURL1;
	String fail1 = "FAIL >> Service Identifier Example Url is not displayed";
	ALib.AssertEqualsMethod(Expected1, ExampleURL1, pass1, fail1);	
	sleep(2); 
	
	String ExampleURL2 = SignOnServiceExampleUrlVisible.getText();
	String Expected2 = ExampleURL2 ;
	String pass2 = "PASS >> Sign On Service Example Url is : " +ExampleURL2;
	String fail2 = "FAIL >> Sign On Service Example Url is not displayed";
	ALib.AssertEqualsMethod(Expected2, ExampleURL2, pass2, fail2);
	sleep(2); 
		
	String ExampleURL3 = LogoutServiceUrlExampleUrlVisible.getText();
	String Expected3 = ExampleURL3 ;
	String pass3 = "PASS >> Logout Service Example Url is : " +ExampleURL3;
	String fail3 = "FAIL >> Logout Service Example Url is not displayed";
	ALib.AssertEqualsMethod(Expected3, ExampleURL3, pass3, fail3);
	sleep(2);
	
	String ExampleURL4 = LoginURLforSingleSignOnusersVisible.getText();
    String Expected4 = ExampleURL4 ;
	String pass4 = "PASS >> Login URL for Single Sign On users : " +ExampleURL4;
	String fail4 = "FAIL >> Login URL for Single Sign On users is Not displayed";
	ALib.AssertEqualsMethod(Expected4, ExampleURL4, pass4, fail4);
	sleep(2); 	
}

//Select Job Status

@FindBy(xpath="//input[@value='Deployed']")
private WebElement DeployedOptionVisible ;

public void DeployedOptionVisible() throws InterruptedException {
	boolean flag = DeployedOptionVisible.isEnabled();
	String PassStatement="PASS >> Deployed Option in Select Job Status is Displayed";
	String FailStatement="FAIL >> Deployed Option in Select Job Status is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//input[@value='Inprogress']")
private WebElement InProgressOptionVisible ;

public void InProgressOptionVisible() throws InterruptedException {
	boolean flag = InProgressOptionVisible.isEnabled();
	String PassStatement="PASS >> In Progress Option in Select Job Status is Displayed";
	String FailStatement="FAIL >> In Progress Option in Select Job Status is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//input[@value='Pending']")
private WebElement PendingOptionVisible ;

public void PendingOptionVisible() throws InterruptedException {
	boolean flag = PendingOptionVisible.isEnabled();
	String PassStatement="PASS >> Pending Option in Select Job Status is Displayed";
	String FailStatement="FAIL >> Pending Option in Select Job Status is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//input[@value='Error']")
private WebElement ErrorOptionVisible ;

public void ErrorOptionVisible() throws InterruptedException {
	boolean flag = ErrorOptionVisible.isEnabled();
	String PassStatement="PASS >> Error Option in Select Job Status is Displayed";
	String FailStatement="FAIL >> Error Option in Select Job Status is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//option[@value='Generic']")
private WebElement GenericOptionVisible ;

public void GenericOptionVisible() throws InterruptedException {
	boolean flag = GenericOptionVisible.isDisplayed();
	String PassStatement="PASS >> In SSO Type Generic option is Displayed.";
	String FailStatement="FAIL >> In SSO Type Generic option is Not Displayed.";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

//deepthought

@FindBy(xpath="//span[text()='More']")
private WebElement ClickonMoreOption;

public void ClickonMoreOption() throws InterruptedException{
	sleep(5);
	Actions act = new Actions(Driver.driver);
	act.moveToElement(ClickonMoreOption).click().perform();
	waitForXpathPresent("//*[@id='chatBot']/a");
	Reporter.log("PASS >> Clicked on More Option",true);
	sleep(2);
}

@FindBy(xpath="//*[@id='chatBot']/a")
private WebElement ClickonDeepThought;

public void ClickonDeepThought() throws InterruptedException
{
	sleep(5);
	Actions act = new Actions(Driver.driver);
	act.moveToElement(ClickonDeepThought).click().perform();
	waitForXpathPresent("//*[@id='chatBotMainBlk_div_id']/div[1]/div[1]/div/ul/li[1]");
	Reporter.log("PASS >> Clicked on Deep Thought",true);
	sleep(2);
}
	
@FindBy(xpath="//*[@id='chatBotMainBlk_div_id']/div[1]/div[1]/div/ul/li[1]")
private WebElement ClickonShowmeMyAccountDetails;

public void ClickonShowmeMyAccountDetails() throws InterruptedException
{
//	ClickonShowmeMyAccountDetails.click();
//	waitForXpathPresent("//*[@id='chatBotMainBlk_div_id']/div[1]/div[1]/div/ul/li[2]");
//	sleep(3);
	
	boolean flag=ClickonShowmeMyAccountDetails.isDisplayed();
	if(flag)
	{
		ClickonShowmeMyAccountDetails.click();
		waitForXpathPresent("//*[@id='chatBotMainBlk_div_id']/div[1]/div[1]/div/ul/li[2]");
		sleep(3);
		Reporter.log("PASS >> show me my account details is Displayed",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> show me my account details is Not Displayed");
		Reporter.log("FAIL >> show me my account details is Not Displayed",true);
	}	
}

@FindBy(xpath="//*[@id='chatBotMainBlk_div_id']/div[1]/div[1]/div/ul/li[2]")
private WebElement ClickonShowmeAllOnlineDevices;

public void ClickonShowmeAllOnlineDevices() throws InterruptedException
{
//	ClickonShowmeAllOnlineDevices.click();
//	waitForXpathPresent("//li[normalize-space()='lock it']");
//	sleep(3);
	boolean flag=ClickonShowmeAllOnlineDevices.isDisplayed();
	if(flag)
	{
		ClickonShowmeAllOnlineDevices.click();
		waitForXpathPresent("//li[normalize-space()='lock it']");
		sleep(3);
		Reporter.log("PASS >> show me all online devices is Displayed",true);		
	}
	else
	{
		ALib.AssertFailMethod("FAIL >> show me all online devices is Not Displayed");
		Reporter.log("FAIL >> show me all online devices is Not Displayed",true);		
	}		
}

@FindBy(xpath="//li[normalize-space()='lock it']")
private WebElement lockitOptionVisible ;

public void lockitOptionVisible() throws InterruptedException {
	boolean flag = lockitOptionVisible.isDisplayed();
	String PassStatement="PASS >> Lock it Option is Displayed";
	String FailStatement="FAIL >> Lock it Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//li[normalize-space()='wipe it']")
private WebElement wipeitOptionVisible ;

public void wipeitOptionVisible() throws InterruptedException {
	boolean flag = wipeitOptionVisible.isDisplayed();
	String PassStatement="PASS >> Wipe it Option is Displayed";
	String FailStatement="FAIL >> Wipe it Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//li[normalize-space()='reboot it']")
private WebElement rebootitOptionVisible ;

public void rebootitOptionVisible() throws InterruptedException {
	boolean flag = rebootitOptionVisible.isDisplayed();
	String PassStatement="PASS >> Reboot it Option is Displayed";
	String FailStatement="FAIL >> Reboot it Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//li[normalize-space()='refresh it']")
private WebElement refreshitOptionVisible ;

public void refreshitOptionVisible() throws InterruptedException {
	boolean flag = refreshitOptionVisible.isDisplayed();
	String PassStatement="PASS >> Refresh it Option is Displayed";
	String FailStatement="FAIL >> Refresh it Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//li[normalize-space()='send message']")
private WebElement sendmessageOptionVisible ;

public void sendmessageOptionVisible() throws InterruptedException {
	boolean flag = sendmessageOptionVisible.isDisplayed();
	String PassStatement="PASS >> Send message Option is Displayed";
	String FailStatement="FAIL >> Send message Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
}

// deep thought settings 

@FindBy(xpath="//button[@id='chatbotenableforapps']")
private WebElement ClickonDeepThoughtSettings;

public void ClickonDeepThoughtSettings() throws InterruptedException
{
	ClickonDeepThoughtSettings.click();
	waitForXpathPresent("//h4[text()='DeepThought Settings']");
	sleep(3);
}


@FindBy(xpath="//*[@id='deep_thought_popup']/div[4]/div/span[3]/span/label")
private WebElement EnableDeepThoughtforBYODUsers;

public void EnableDeepThoughtforBYODUsers() throws InterruptedException
{
	EnableDeepThoughtforBYODUsers.click();
	waitForXpathPresent("//span[normalize-space()='DeepThought BYOD settings updated successfully.']");
	//sleep(3);
}


@FindBy(xpath="//span[normalize-space()='DeepThought BYOD settings updated successfully.']")
private WebElement EnableDeepThoughtSettingsSuccessfully ;

public void EnableDeepThoughtSettingsSuccessfully() throws InterruptedException {
	boolean flag = EnableDeepThoughtSettingsSuccessfully.isDisplayed();
	String PassStatement="PASS >> Deep Thought BOYD Settings Updated Successfully";
	String FailStatement="FAIL >> Deep Thought BOYD Settings Not Updated Successfully";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

//Things Licenses Used


@FindBy(xpath="//article[@id='boardingStep1']//a[@title='Close']")
private WebElement CloseWindow ;

public void CloseWindow() throws InterruptedException {
	
	waitForXpathPresent("//article[@id='boardingStep1']//a[@title='Close']");	
	CloseWindow.click();
	

}

@FindBy(xpath="//span[text()='Things Licenses Used']")
private WebElement ThingsLicensesUsedOptionVisible ;

public void ThingsLicensesUsedOptionVisible() throws InterruptedException {
	
	boolean flag = ThingsLicensesUsedOptionVisible.isDisplayed();
	String PassStatement="PASS >> Things Licenses Used Option is Displayed";
	String FailStatement="FAIL >> Things Licenses Used Option is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//span[@id='thingsdevicesused']")
private WebElement ThingsLicensesCount ;

public void ThingsLicensesCount() throws InterruptedException {

       String Count = ThingsLicensesCount.getText();
       Reporter.log("PASS >> Things Licenses Used Count is " + Count,true);
}


//Require Two-Factor Authentication

@FindBy(xpath="//*[@id='Requires2FASU']/div/div[2]/div[1]/p[1]")
private WebElement RequireTwoFactorAuthenticationOption  ;

public void RequireTwoFactorAuthenticationOption() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = RequireTwoFactorAuthenticationOption;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
}

@FindBy(xpath="//*[@id='Requires2FASU']/div/div[2]/div[1]/p[1]")
private WebElement RequireTwoFactorAuthenticationOptionVisible  ;

public void RequireTwoFactorAuthenticationOptionVisible() throws InterruptedException {
	boolean flag = RequireTwoFactorAuthenticationOptionVisible.isDisplayed();
	String PassStatement="PASS >> Require Two Factor Authentication is Displayed";
	String FailStatement="FAIL >> Require Two Factor Authentication is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//span[@class='toogle_btn disabledclass']")
private WebElement Enableyourown2FAButton  ;

public void Enableyourown2FAButton() throws InterruptedException {
	boolean flag = Enableyourown2FAButton.isEnabled();
	String PassStatement="PASS >> Enable your own 2-FA Button is Disabled";
	String FailStatement="FAIL >> Enable your own 2-FA Button is Enabled";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//p[text()='Delete Account']")
private WebElement Scrolldown  ;


public void TwoFactorAuthenticationVisible()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = Scrolldown;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	boolean flag=RequireTwoFactorAuthenticationOptionVisible.isDisplayed();
	if(flag)
	{
		Reporter.log("FAIL >> Require Two Factor Authentication Option is Displayed",true);		
	}
	else
	{
		ALib.AssertFailMethod("FAIL >> Require Two Factor Authentication Option is Not Displayed");
		Reporter.log("PASS >> Require Two Factor Authentication Option is Not Displayed",true);
	}
}

//Plugin 

@FindBy(xpath="//a[text()='Plugins']")
private WebElement ClickOnPluginsButton;

public void ClickOnPluginsButton() throws InterruptedException
{
	ClickOnPluginsButton.click();
	sleep(3);
	waitForXpathPresent("//a[text()='Plugin Store']");	
}

@FindBy(xpath="//*[@id='PluginStoreLiTab']/a")
private WebElement PluginStoreVisible  ;

public void PluginStoreVisible()throws InterruptedException{
	
	boolean flag=PluginStoreVisible.isDisplayed();
	if(flag)
	{   
		sleep(4);
		PluginStoreVisible.click();
		sleep(4);
		Reporter.log("PASS >> Plugins Store under Plugin Option is Displayed",true);		
	}
	else
	{
		ALib.AssertFailMethod("FAIL >> Plugins Store under Plugin Option is Not Displayed");
		Reporter.log("FAIL >> Plugins Store under Plugin Option is Not Displayed",true);
	}
}

@FindBy(xpath="//a[text()='Plugin Store']")
private WebElement ClickOnPluginsStoreButton;

public void ClickOnPluginsStoreButton() throws InterruptedException
{
	ClickOnPluginsStoreButton.click();
	sleep(10);
	waitForXpathPresent("//p[normalize-space()='RenameD']");
	sleep(3);
}

@FindBy(xpath="//p[normalize-space()='RenameD']")
private WebElement PluginRenameDVisible  ;

@FindBy(xpath="(//p[text()=' Administrator '])[3]")
private WebElement DevelopedByAdministrator ;

@FindBy(xpath="(//div[@class='PluginStoreDesc'])[3]")
private WebElement  Descriptionofplugin;

@FindBy(xpath="(//button[text()='Installed'])[2]")
private WebElement installNowButtonVisible ;

public void PluginRenameD() throws InterruptedException
{
	boolean flag1 = PluginRenameDVisible.isDisplayed();
	String PassStatement1="PASS >> Renamed Plugin Name is Displayed";
	String FailStatement1="FAIL >> Renamed Plugin Name is Not Displayed";
	ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
	sleep(2);	
	
	boolean flag2 = DevelopedByAdministrator.isDisplayed();
	String PassStatement2="PASS >> Developed by Administrator is Displayed";
	String FailStatement2="FAIL >> Developed by Administrator is Not Displayed";
	ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);
	sleep(2);	 
	
	boolean flag3 = Descriptionofplugin.isDisplayed();
	String PassStatement3="PASS >> Description of RenameD plugin is Displayed";
	String FailStatement3="FAIL >> Description of RenameD plugin is Not Displayed";
	ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);
	sleep(2);
	
	boolean flag4 = installNowButtonVisible.isDisplayed();
	String PassStatement4="PASS >> Install Now Button for RenameD Plugin is Displayed";
	String FailStatement4="FAIL >> Install Now Button for RenameD Plugin is Not Displayed";
	ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);
	sleep(2);	
}

@FindBy(xpath="(//p[normalize-space()='Group Creator'])[1]")
private WebElement PluginGroupCreatorVisible  ;

@FindBy(xpath="(//p[text()=' Administrator '])[5]")
private WebElement DevelopedByAdministratorGroupCreator ;

@FindBy(xpath="(//div[@class='PluginStoreDesc'])[5]")
private WebElement  DescriptionofpluginGroupCreator;

@FindBy(xpath="(//button[text()='Install Now'])[3]")
private WebElement GroupCreatorinstallNowButtonVisible ;

public void PluginGroupCreator() throws InterruptedException
{
	boolean flag1 = PluginGroupCreatorVisible.isDisplayed();
	String PassStatement1="PASS >> Group Creator Plugin Name is Displayed";
	String FailStatement1="FAIL >> Group Creator Plugin Name is Not Displayed";
	ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
	sleep(2);	
	
	boolean flag2 = DevelopedByAdministratorGroupCreator.isDisplayed();
	String PassStatement2="PASS >> Developed by Administrator is Displayed";
	String FailStatement2="FAIL >> Developed by Administrator is Not Displayed";
	ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);
	sleep(2);	 
	
	boolean flag3 = DescriptionofpluginGroupCreator.isDisplayed();
	String PassStatement3="PASS >> Description of Group Creator plugin is Displayed";
	String FailStatement3="FAIL >> Description of Group Creator plugin is Not Displayed";
	ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);
	sleep(2);
	
	boolean flag4 = GroupCreatorinstallNowButtonVisible.isDisplayed();
	String PassStatement4="PASS >> Install Now Button for Group Creator Plugin is Displayed";
	String FailStatement4="FAIL >> Install Now Button for Group Creator Plugin is Not Displayed";
	ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);
	sleep(2);
		
}

@FindBy(xpath="//p[normalize-space()='Group Assignment Wizard']")
private WebElement PluginGroupAssignmentWizardVisible  ;

@FindBy(xpath="(//p[text()=' Administrator '])[7]")
private WebElement DevelopedByAdministratorGroupAssignmentWizard;

@FindBy(xpath="(//div[@class='PluginStoreDesc'])[7]")
private WebElement  DescriptionofpluginGroupAssignmentWizard;

@FindBy(xpath="(//button[text()='Install Now'])[5]")
private WebElement GroupAssignmentWizardinstallNowButtonVisible ;

public void PluginGroupAssignmentWizard() throws InterruptedException
{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = PluginGroupAssignmentWizardVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	
	boolean flag1 = PluginGroupAssignmentWizardVisible.isDisplayed();
	String PassStatement1="PASS >> Group Creator Plugin Name is Displayed";
	String FailStatement1="FAIL >> Group Creator Plugin Name is Not Displayed";
	ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
	sleep(2);	
	
	boolean flag2 = DevelopedByAdministratorGroupAssignmentWizard.isDisplayed();
	String PassStatement2="PASS >> Developed by Administrator is Displayed";
	String FailStatement2="FAIL >> Developed by Administrator is Not Displayed";
	ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);
	sleep(2);	 
	
	boolean flag3 = DescriptionofpluginGroupAssignmentWizard.isDisplayed();
	String PassStatement3="PASS >> Description of Group Assignment Wizard plugin is Displayed";
	String FailStatement3="FAIL >> Description of Group Assignment Wizard plugin is Not Displayed";
	ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);
	sleep(2);
	
	boolean flag4 = GroupAssignmentWizardinstallNowButtonVisible.isDisplayed();
	String PassStatement4="PASS >> Install Now Button for Group Assignment Wizard Plugin is Displayed";
	String FailStatement4="FAIL >> Install Now Button for Group Assignment Wizard Plugin is Displayed";
	ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);
	sleep(2);		
}

@FindBy(xpath="//p[normalize-space()='FixTheName']")
private WebElement PluginFixTheNameVisible  ;

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[4]/div/div[2]/div[2]/p")
private WebElement DevelopedByAdministratorFixTheName;

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[4]/div/div[4]/div[1]")
private WebElement  DescriptionofpluginFixTheName;

@FindBy(xpath="//button[text()='Install Now']")
private WebElement FixTheNameinstallNowButtonVisible ;

public void PluginFixTheNameVisible() throws InterruptedException
{
	boolean flag1 = PluginFixTheNameVisible.isDisplayed();
	String PassStatement1="PASS >> FixTheName Plugin Name is Displayed";
	String FailStatement1="FAIL >> FixTheName Plugin Name is Not Displayed";
	ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
	sleep(2);	
	
	boolean flag2 = DevelopedByAdministratorFixTheName.isDisplayed();
	String PassStatement2="PASS >> Developed by Administrator is Displayed";
	String FailStatement2="FAIL >> Developed by Administrator is Not Displayed";
	ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);
	sleep(2);	 
	
	boolean flag3 = DescriptionofpluginFixTheName.isDisplayed();
	String PassStatement3="PASS >> Description of FixTheName plugin is Displayed";
	String FailStatement3="FAIL >> Description of FixTheName plugin is Not Displayed";
	ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);
	sleep(2);
	
	boolean flag4 = FixTheNameinstallNowButtonVisible.isDisplayed();
	String PassStatement4="PASS >> Install Now Button for FixTheName Plugin is Displayed";
	String FailStatement4="FAIL >> Install Now Button for FixTheName Plugin is Not Displayed";
	ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);
	sleep(2);		
}

@FindBy(xpath="//p[normalize-space()='ExpoJob']")
private WebElement PluginExpoJobVisible  ;

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[5]/div/div[2]/div[2]/p")
private WebElement DevelopedByAdministratorExpoJob;

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[5]/div/div[4]/div[1]")
private WebElement  DescriptionofpluginExpoJob;

@FindBy(xpath="//button[text()='Install Now']")
private WebElement ExpoJobinstallNowButtonVisible ;

public void PluginExpoJobVisible() throws InterruptedException
{
	boolean flag1 = PluginExpoJobVisible.isDisplayed();
	String PassStatement1="PASS >> Expo Job Plugin Name is Displayed";
	String FailStatement1="FAIL >> Expo Job Plugin Name is Not Displayed";
	ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
	sleep(2);	
	
	boolean flag2 = DevelopedByAdministratorExpoJob.isDisplayed();
	String PassStatement2="PASS >> Developed by Administrator is Displayed";
	String FailStatement2="FAIL >> Developed by Administrator is Not Displayed";
	ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);
	sleep(2);	 
	
	boolean flag3 = DescriptionofpluginExpoJob.isDisplayed();
	String PassStatement3="PASS >> Description of Expo Job plugin is Displayed";
	String FailStatement3="FAIL >> Description of Expo Job plugin is Displayed";
	ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);
	sleep(2);
	
	boolean flag4 = ExpoJobinstallNowButtonVisible.isDisplayed();
	String PassStatement4="PASS >> Install Now Button for Expo Job Plugin is Displayed";
	String FailStatement4="FAIL >> Install Now Button for Expo Job Plugin is Displayed";
	ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);
	sleep(2);		
}

@FindBy(xpath="//p[normalize-space()='Columnify']")
private WebElement PluginColumnifyVisible  ;

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[6]/div/div[2]/div[2]/p")
private WebElement DevelopedByAdministratorColumnify;

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[6]/div/div[4]/div[1]")
private WebElement  DescriptionofpluginColumnify;

@FindBy(xpath="//button[text()='Install Now']")
private WebElement ColumnifyinstallNowButtonVisible ;

public void PluginColumnifyVisible() throws InterruptedException
{
	boolean flag1 = PluginColumnifyVisible.isDisplayed();
	String PassStatement1="PASS >> Columnify Plugin Name is Displayed";
	String FailStatement1="FAIL >> Columnify Plugin Name is Not Displayed";
	ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
	sleep(2);	
	
	boolean flag2 = DevelopedByAdministratorColumnify.isDisplayed();
	String PassStatement2="PASS >> Developed by Administrator is Displayed";
	String FailStatement2="FAIL >> Developed by Administrator is Not Displayed";
	ALib.AssertTrueMethod(flag2, PassStatement2, FailStatement2);
	sleep(2);	 
	
	boolean flag3 = DescriptionofpluginColumnify.isDisplayed();
	String PassStatement3="PASS >> Description of Columnify plugin is Displayed";
	String FailStatement3="FAIL >> Description of Columnify plugin is Displayed";
	ALib.AssertTrueMethod(flag3, PassStatement3, FailStatement3);
	sleep(2);
	
	boolean flag4 = ColumnifyinstallNowButtonVisible.isDisplayed();
	String PassStatement4="PASS >> Install Now Button for Columnify Plugin is Displayed";
	String FailStatement4="FAIL >> Install Now Button for Columnify Plugin is Displayed";
	ALib.AssertTrueMethod(flag4, PassStatement4, FailStatement4);
	sleep(2);		
}

@FindBy(xpath="//*[@id='searchPluginStore']")
private WebElement PluginStoreSearchBarVisible  ;

public void PluginStoreSearchBarVisible()throws InterruptedException{
	
	boolean flag=PluginStoreSearchBarVisible.isDisplayed();
	if(flag)
	{		
		Reporter.log("PASS >> In Plugin Store Search Bar is Displayed",true);		
	}
	else
	{
		ALib.AssertFailMethod("FAIL >> In Plugin Store Search Bar is Not Displayed");
		Reporter.log("FAIL >> In Plugin Store Search Bar is Not Displayed",true);
	}
}

@FindBy(xpath="//*[@id='searchPluginStore']")
private WebElement PluginStoreSearchBar  ;

public void PluginStoreSearchBar()throws InterruptedException{
	
	boolean flag=PluginStoreSearchBar.isDisplayed();
	if(flag)
	{
		PluginStoreSearchBar.sendKeys("Columnify");
		Reporter.log("PASS >> Columnify Plugin Name Entered in Search Bar",true);		
	}
	else
	{
		Reporter.log("FAIL >> Columnify Plugin Name Not Entered in Search Bar",true);
	}
}

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[6]/div/div[2]/div[1]/p")
private WebElement PluginSearchNameMatch  ;

public void PluginSearchNameMatch() throws InterruptedException {
	boolean flag = PluginSearchNameMatch.isDisplayed();
	String PassStatement="PASS >> Plugin Search Name is Matched";
	String FailStatement="FAIL >> Plugin Search Name is Not Matched";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="(//div[@class='pluginMoreDetails'])[3]")
private WebElement ClickonMoreDetails ;

public void ClickonMoreDetails() throws InterruptedException
{
	ClickonMoreDetails.click();
	sleep(5);
	waitForXpathPresent("//h4[text()='Plugin Details']");
	sleep(3);
}

@FindBy(xpath="//h4[text()='Plugin Details']")
private WebElement PluginDetailsPopupVisible  ;

@FindBy(xpath="//div[@id='appDet_modal']//button[@aria-label='Close']")
private WebElement CloseMoreDetails  ;

public void PluginDetailsPopupVisible() throws InterruptedException {
	boolean flag = PluginDetailsPopupVisible.isDisplayed();
	String PassStatement="PASS >> Plugin Details Pop-up is Displayed";
	String FailStatement="FAIL >> Plugin Details Pop-up is Not Displayed";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);	
	CloseMoreDetails.click();	
}

@FindBy(xpath="//button[text()='Install Now']")
private WebElement InstallNowButtonVisible  ;

public void InstallNowButtonVisible()throws InterruptedException{
	
	boolean flag=InstallNowButtonVisible.isDisplayed();
	if(flag)
	{		
		Reporter.log("PASS >> Install Now Button is Displayed",true);		
	}
	else
	{
		Reporter.log("FAIL >> Install Now Button is Not Displayed",true);
	}
}

@FindBy(xpath="//div/div/div[1]/div[3]/div[1]/div[1]/div/div[5]/button")
private WebElement InstalledButtonVisible  ;

public void InstalledButtonVisible()throws InterruptedException{
	
	boolean flag=InstalledButtonVisible.isDisplayed();
	if(flag)
	{		
		Reporter.log("PASS >> Installed Button is Displayed",true);
		
	}
	else
	{
		Reporter.log("FAIL >> Installed Button is Not Displayed",true);
	}
}

// EULA Disclaimer Policy

@FindBy(xpath="//input[@id='eula_policy']")
private WebElement ClickonEULASave ;

public void ClickonEULASave() throws InterruptedException
{
	ClickonEULASave.click();
	System.out.println("PASS >> Clicked on Save Button");
	sleep(5);
	waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");
	sleep(3);
}

@FindBy(xpath="//input[@id='chkbox_eulaDisclaimer']")
private WebElement DisabledEULA;

public void DisabledEULA()throws InterruptedException{
	
	boolean flag=DisabledEULA.isSelected();
	if(flag)
	{
		DisabledEULA.click();		
		Reporter.log("PASS >> EULA is Disabled",true);		
	}
	else
	{	
		Reporter.log("PASS >> EULA is Disabled",true);
	}
}

public void EULADisabledActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'EULA Disclaimer Policy settings modified by')]");
	String EULALogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'EULA Disclaimer Policy settings modified by')]")).getText();		
	System.out.println("PASS >> Activity Log Message Displayed is : "+EULALogMsg);
	sleep(2);
}

@FindBy(xpath="//input[@id='chkbox_eulaDisclaimer']")
private WebElement EnabledEULA;

public void EnabledEULA()throws InterruptedException{
	
	boolean flag=EnabledEULA.isSelected();
	if(flag)
	{			
		Reporter.log("PASS >> EULA is Enabled",true);		
	}
	else
	{	
		EnabledEULA.click();	
		Reporter.log("PASS >> EULA is Enabled",true);
	}
}

public void EULAEnabledActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'EULA Disclaimer Policy settings modified by')]");
	String EULALogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'EULA Disclaimer Policy settings modified by')]")).getText();		
	System.out.println("PASS >> Activity Log Message Displayed is : "+EULALogMsg);
	sleep(2);
}

//Intel� AMT Device Management 

@FindBy(xpath="//a[normalize-space()='Intel� AMT device management']")
private WebElement IntelAMTdevicemanagementVisible ;

public void IntelAMTdevicemanagementVisible()throws InterruptedException{
	
	boolean flag=IntelAMTdevicemanagementVisible.isDisplayed();
	if(flag)
	{			
		IntelAMTdevicemanagementVisible.click();
		waitForXpathPresent("//span[@class='input-group-addon labelno-bg ct-model-label ct-label-w60']");
		Reporter.log("PASS >> Intel� AMT Device Management is Displayed",true);		
	}
	else
	{		
		Reporter.log("PASS >> Intel� AMT Device Management is Not Displayed",true);
	}
}

//language

@FindBy(xpath="//button[normalize-space()='Change Language']")
private WebElement ClickonChangeLanguage ;

public void ClickonChangeLanguage() throws InterruptedException
{

	ClickonChangeLanguage.click();
	sleep(5);
	waitForXpathPresent("//h4[normalize-space()='Choose your language']");
	sleep(3);
}

@FindBy(xpath="//button[@id='okbtn']")
private WebElement ClickonChangeLanguageOkButton ;

public void ClickonChangeLanguageOkButton() throws InterruptedException
{
	ClickonChangeLanguageOkButton.click();
	waitForXpathPresent("//span[normalize-space()='Please select different language.']");
	
}

@FindBy(xpath="//span[normalize-space()='Please select different language.']")
private WebElement ExpectedMsg ;

public void VerifyErrormsg() throws InterruptedException
{    	
	String Errormsg = ExpectedMsg.getText();
    System.out.println("PASS >> Error Message Displayed is :" +Errormsg);
    sleep(5);
}

//Password Age

@FindBy(xpath="//input[@id='enforce_password_history']")
private WebElement EnforcePasswordHistoryVisible ;

public void EnforcePasswordHistoryVisible()throws InterruptedException{
	
	boolean flag=EnforcePasswordHistoryVisible.isDisplayed();
	if(flag)
	{
		EnforcePasswordHistoryVisible.clear();
		EnforcePasswordHistoryVisible.sendKeys("5");
		sleep(10);
		Reporter.log("PASS >> Value Entered in Enforce Password History",true);		
	}
	else
	{		
		Reporter.log("PASS >> Value Not Entered in Enforce Password History",true);
	}
}

@FindBy(xpath="//input[@id='max_password_age']")
private WebElement MaximumpasswordageVisible ;

public void MaximumpasswordageVisible()throws InterruptedException{
	
	boolean flag=MaximumpasswordageVisible.isDisplayed();
	if(flag)
	{
		MaximumpasswordageVisible.clear();
		MaximumpasswordageVisible.sendKeys("10");
		sleep(10);
		Reporter.log("PASS >> Value Entered in Maximum Password Age",true);		
	}
	else
	{		
		Reporter.log("PASS >> Value Entered in Maximum Password Age",true);
	}
}

@FindBy(xpath="//button[@id='Update_pwd_poli_btn']")
private WebElement ClickonSaveButton ;

public void ClickonSaveButton() throws InterruptedException
{
	sleep(4);
	ClickonSaveButton.click();
	sleep(4);
}

@FindBy(xpath="//span[normalize-space()='Password policy updated successfully.']")
private WebElement Passwordpolicyupdatedsuccessfully ;

public void Passwordpolicyupdatedsuccessfully()throws InterruptedException{
	
	boolean flag=Passwordpolicyupdatedsuccessfully.isDisplayed();
	if(flag)
	{				
		Reporter.log("PASS >> Password policy updated successfully.",true);		
	}
	else
	{
		Reporter.log("FAIL >> Password policy Not updated successfully.",true);
	}
}

@FindBy(xpath="//input[@id='max_password_age']")
private WebElement ResetMaximumpasswordage ;

public void ResetMaximumpasswordage()throws InterruptedException{
	
	boolean flag=ResetMaximumpasswordage.isDisplayed();
	if(flag)
	{
		ResetMaximumpasswordage.clear();
		ResetMaximumpasswordage.sendKeys("0");
		sleep(5);
		Reporter.log("PASS >> Reset Value Entered in Maximum Password Age",true);		
	}
	else
	{		
		Reporter.log("PASS >> Reset Value Entered in Maximum Password Age",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Password policy updated successfully.']")
private WebElement ResetPasswordpolicyupdatedsuccessfully ;

public void ResetPasswordpolicyupdatedsuccessfully() throws InterruptedException
{
	waitForXpathPresent("//span[normalize-space()='Password policy updated successfully.']");
	String updatedmsg = ResetPasswordpolicyupdatedsuccessfully.getText();
    System.out.println("PASS >> Update Message Displayed is :" +updatedmsg);
    sleep(5);
	
	
	
//	String updatemsg = ResetPasswordpolicyupdatedsuccessfully.getText();
//	String Expected = "Password policy updated successfully.";
//	String pass = "PASS >> Update Message Displayed is : " +updatemsg;
//	String fail = "FAIL >> Update Message Displayed is not displayed";
//	ALib.AssertEqualsMethod(Expected, updatemsg, pass, fail);	
//	sleep(2); 
}

@FindBy(xpath="//span[normalize-space()='Password policy updated successfully.']")
private WebElement Passwordpolicyupdatedsuccessfullymsg ;

public void Passwordpolicyupdatedsuccessfullymsg() throws InterruptedException
{
	waitForXpathPresent("//span[normalize-space()='Password policy updated successfully.']");
	String updatedmsg = Passwordpolicyupdatedsuccessfullymsg.getText();
    System.out.println("PASS >> Update Message Displayed is :" +updatedmsg);
    sleep(5);
	
	
	
	
//	sleep(2); 
//	String updatemsg = Passwordpolicyupdatedsuccessfully.getText();
//	String Expected = "Password policy updated successfully.";
//	String pass = "PASS >> Update Message Displayed is : " +updatemsg;
//	String fail = "FAIL >> Update Message Displayed is not displayed";
//	ALib.AssertEqualsMethod(Expected, updatemsg, pass, fail);	
//	sleep(5); 
}

@FindBy(xpath="//input[@id='enforce_password_history']")
private WebElement EnforcePasswordHistoryOptionVisible ;

public void EnforcePasswordHistoryOptionVisible()throws InterruptedException{
	
	boolean flag=EnforcePasswordHistoryOptionVisible.isDisplayed();
	if(flag)
	{	
		Reporter.log("PASS >> Enforce Password History Option is Displayed",true);		
	}
	else
	{		
		Reporter.log("FAIL >> Enforce Password History Option is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='max_password_age']")
private WebElement MaximumpasswordageOptionVisible ;

public void MaximumpasswordageOptionVisible()throws InterruptedException{
	
	boolean flag=MaximumpasswordageOptionVisible.isDisplayed();
	if(flag)
	{	
		Reporter.log("PASS >> Maximum Password Age Option is Displayed",true);		
	}
	else
	{
		Reporter.log("FAIL >> Maximum Password Age Option is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='enforce_password_history']")
private WebElement EnforcePasswordHistoryDefaultValue ;

public void EnforcePasswordHistoryDefaultValue()throws InterruptedException{
	
     String ActualValue=EnforcePasswordHistoryDefaultValue.getAttribute("min");
     String ExpectedValue="0";
     String PassStatement="PASS >> Enforce Password History Default Value is "+ExpectedValue;
     String FailStatement="FAIL >> Enforce Password History Default Value is Not Displayed";
     ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
}

@FindBy(xpath="//input[@id='max_password_age']")
private WebElement MaximumPasswordAgeDefaultValue ;

public void MaximumPasswordAgeDefaultValue()throws InterruptedException{
	
     String ActualValue=MaximumPasswordAgeDefaultValue.getAttribute("min");
     String ExpectedValue="0";
     String PassStatement="PASS >> Maximum Password Age Default Value is "+ExpectedValue;
     String FailStatement="FAIL >> Maximum Password Age Default Value is Not Displayed";
     ALib.AssertEqualsMethod(ActualValue, ExpectedValue, PassStatement, FailStatement);
}

@FindBy(xpath="//input[@id='enforce_password_history']")
private WebElement EnforcePasswordHistoryMinMax ;

public void EnforcePasswordHistoryMinMax()throws InterruptedException{
	
     String ActualValue1=EnforcePasswordHistoryMinMax.getAttribute("min");
     String ExpectedValue1="0";
     String PassStatement1="PASS >> Enforce Password History Minimum Value is "+ExpectedValue1;
     String FailStatement1="FAIL >> Enforce Password History Minimum Value is Not Displayed";
     ALib.AssertEqualsMethod(ActualValue1, ExpectedValue1, PassStatement1, FailStatement1);
         
     String ActualValue2=EnforcePasswordHistoryMinMax.getAttribute("max");
     String ExpectedValue2="24";
     String PassStatement2="PASS >> Enforce Password History Maximum Value is "+ExpectedValue2;
     String FailStatement2="FAIL >> Enforce Password History Maximum Value is Not Displayed";
     ALib.AssertEqualsMethod(ActualValue2, ExpectedValue2, PassStatement2, FailStatement2);       
}

@FindBy(xpath="//input[@id='max_password_age']")
private WebElement MaximumPasswordAgeMinMax ;

public void MaximumPasswordAgeMinMax()throws InterruptedException{
	
     String ActualValue1=MaximumPasswordAgeMinMax.getAttribute("min");
     String ExpectedValue1="0";
     String PassStatement1="PASS >> Maximum Password Age Minimum Value is "+ExpectedValue1;
     String FailStatement1="FAIL >> Maximum Password Age Minimum Value is Not Displayed";
     ALib.AssertEqualsMethod(ActualValue1, ExpectedValue1, PassStatement1, FailStatement1);
     
     String ActualValue2=MaximumPasswordAgeMinMax.getAttribute("max");
     String ExpectedValue2="999";
     String PassStatement2="PASS >> Maximum Password Age Maximum Value is "+ExpectedValue2;
     String FailStatement2="FAIL >> Maximum Password Age Maximum Value is Not Displayed";
     ALib.AssertEqualsMethod(ActualValue2, ExpectedValue2, PassStatement2, FailStatement2);
}

@FindBy(xpath="//input[@id='enforce_password_history']")
private WebElement EnforcePasswordHistoryValueEntered ;

public void EnforcePasswordHistoryValueEntered()throws InterruptedException{
	
	boolean flag=EnforcePasswordHistoryValueEntered.isDisplayed();
	if(flag)
	{
		EnforcePasswordHistoryValueEntered.clear();
		sleep(2);
		EnforcePasswordHistoryValueEntered.sendKeys("5");
		sleep(10);
		Reporter.log("PASS >> Value Entered in Enforce Password History",true);
		
	}
	else
	{		
		Reporter.log("FAIL >> Value Not Entered in Enforce Password History",true);
	}
}

@FindBy(xpath="//input[@id='max_password_age']")
private WebElement MaximumpasswordageValueEntered ;

public void MaximumpasswordageValueEntered()throws InterruptedException{
	
	boolean flag=MaximumpasswordageValueEntered.isDisplayed();
	if(flag)
	{
		MaximumpasswordageValueEntered.clear();
		sleep(2);
		MaximumpasswordageValueEntered.sendKeys("60");
		sleep(10);
		Reporter.log("PASS >> Value Entered in Maximum Password Age",true);		
	}
	else
	{		
		Reporter.log("FAIL >> Value Entered in Maximum Password Age",true);
	}
}

@FindBy(xpath="//button[@id='changePassword']")
private WebElement ClickOnSecurityButton;

public void ClickOnSecurityButton() throws InterruptedException
{
	ClickOnSecurityButton.click();
	sleep(5);
	waitForXpathPresent("//h4[text()='Security']");
	sleep(3);
}

@FindBy(xpath="//input[@id='oldpassword']")
private WebElement EnterOldPassword ;

@FindBy(xpath="//input[@id='newpassword']")
private WebElement EnterNewPassword ;

@FindBy(xpath="//*[@id='changePasswordTab']/div[3]/div/input")
private WebElement EnterRetypeNewPassword ;

public void EnterAllPasswordField()throws InterruptedException{
	
	boolean flag=EnterOldPassword.isDisplayed();
	if(flag)
	{	
		EnterOldPassword.sendKeys("42Gears@123");
		Reporter.log("PASS >> Old Password is Entered Successfully",true);
		sleep(2);
		EnterNewPassword.sendKeys("42Gears@123");
		Reporter.log("PASS >> New Password is Entered Successfully",true);
		sleep(2);
		EnterRetypeNewPassword.sendKeys("42Gears@123");
		sleep(2);
		Reporter.log("PASS >> Re-Type New Password is Entered Successfully",true);		
	}
	else
	{		
		Reporter.log("FAIL >> Password Not Entered Successfully",true);
	}
}

@FindBy(xpath="//button[@id='okbtn']")
private WebElement ClickonOKButton ;

public void ClickonOKButton() throws InterruptedException
{
	ClickonOKButton.click();	
}

@FindBy(xpath="//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-error']/span")
private WebElement ErrorMsgDispayed ;

public void ErrorMsgDispayed() throws InterruptedException
{
	
	waitForXpathPresent("//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-error']/span");
	String Errormsg = ErrorMsgDispayed.getText();
    System.out.println("PASS >> Error Message Displayed is :" +Errormsg);
    sleep(5);	
	
//	sleep(2); 
//	String updatemsg = ErrorMsgDispayed.getText();
//	String Expected = "Your password cannot be similar to last 5 passwords.";
//	String pass = "PASS >> Update Message Displayed is : " +updatemsg;
//	String fail = "FAIL >> Update Message Displayed is not displayed";
//	ALib.AssertEqualsMethod(Expected, updatemsg, pass, fail);	
//	sleep(5); 
}

@FindBy(xpath="//div[@id='changePassword_header']//button[@aria-label='Close']")
private WebElement CloseSecurityTab ;

public void CloseSecurityTab() throws InterruptedException
{
	sleep(2); 
	CloseSecurityTab.click();
	Reporter.log("PASS >> Security Tab Closed",true);
}




//Help

@FindBy(xpath="//button[@id='helpsection_cont']")
private WebElement ClickonHelpButton ;

public void ClickonHelpButton()throws InterruptedException{
	
	boolean flag=ClickonHelpButton.isDisplayed();
	if(flag)
	{
		ClickonHelpButton.click();
		sleep(5);
		Reporter.log("PASS >> Help Button is Displayed",true);		
	}
	else
	{		
		Reporter.log("PASS >> Help Button is Not Displayed",true);
	}
}

@FindBy(xpath="//button[normalize-space()='Community']")
private WebElement Community ;

@FindBy(xpath="//button[normalize-space()='Documentation']")
private WebElement Documentation ;

@FindBy(xpath="//button[normalize-space()='Release Notes']")
private WebElement ReleaseNotes ;

@FindBy(xpath="//button[normalize-space()='Training']")
private WebElement Training ;

@FindBy(xpath="//button[normalize-space()='Help Videos']")
private WebElement HelpVideos ;

public void HelpOptionsVisible() throws InterruptedException
{
	String Option1 = Community.getText();
	String Expected1 = "Community";
	String pass1 = "PASS >> Option in Help Section is : " +Option1;
	String fail1 = "FAIL >> Option in Help Section is not Displayed";
	ALib.AssertEqualsMethod(Expected1, Option1, pass1, fail1);	
	sleep(2); 
	
	String Option2 = Documentation.getText();
	String Expected2 = "Documentation";
	String pass2 = "PASS >> Option in Help Section is : " +Option2;
	String fail2 = "FAIL >> Option in Help Section is not displayed";
	ALib.AssertEqualsMethod(Expected2, Option2, pass2, fail2);	
	sleep(2); 
	
	String Option3 = ReleaseNotes.getText();
	String Expected3 = "Release Notes";
	String pass3 = "PASS >> Option in Help Section is : " +Option3;
	String fail3 = "FAIL >> Option in Help Section is not Displayed";
	ALib.AssertEqualsMethod(Expected3, Option3, pass3, fail3);	
	sleep(2); 
	
	String Option4 = Training.getText();
	String Expected4 = "Training";
	String pass4 = "PASS >> Option in Help Section is : " +Option4;
	String fail4 = "FAIL >> Option in Help Section is not Displayed";
	ALib.AssertEqualsMethod(Expected4, Option4, pass4, fail4);	
	sleep(2); 
	
	String Option5 = HelpVideos.getText();
	String Expected5 = "Help Videos";
	String pass5 = "PASS >> Option in Help Section is : " +Option5;
	String fail5 = "FAIL >> Option in Help Section is not Displayed";
	ALib.AssertEqualsMethod(Expected5, Option5, pass5, fail5);	
	sleep(2); 	
}

@FindBy(xpath="//div[@id='helplist']//span[@aria-hidden='true'][normalize-space()='�']")
private WebElement CloseHelpTab ;

public void CloseHelpTab() throws InterruptedException{
	sleep(5); 
	CloseHelpTab.click();
	Reporter.log("PASS >> Help Tab Closed",true);
	sleep(5);
}


@FindBy(xpath="//button[normalize-space()='Community']")
private WebElement ClickonCommunityLink ;

public void ClickonCommunityLink() throws InterruptedException{
	ClickonCommunityLink.click();
	sleep(5);
	Set<String> set=Initialization.driver.getWindowHandles();
	Iterator<String> id=set.iterator();
	String parentwinID = id.next();
	String childwinID = id.next();
	Initialization.driver.switchTo().window(childwinID);
	String URL=Initialization.driver.getCurrentUrl();
	String ExpectedValue = "https://community.42gears.com/";
	String PassStatement = "PASS >> Current url for Community is: "+URL;
	String FailStatement = "FAIL >> Current url for Community is Not Active";
	ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
	Initialization.driver.close();
	Initialization.driver.switchTo().window(parentwinID);
	sleep(5);
}

@FindBy(xpath="//button[normalize-space()='Documentation']")
private WebElement ClickonDocumentationLink ;

public void ClickonDocumentationLink() throws InterruptedException{
	ClickonDocumentationLink.click();
	sleep(5);
	Set<String> set=Initialization.driver.getWindowHandles();
	Iterator<String> id=set.iterator();
	String parentwinID = id.next();
	String childwinID = id.next();
	Initialization.driver.switchTo().window(childwinID);
	String URL=Initialization.driver.getCurrentUrl();
	String ExpectedValue = "https://docs.42gears.com/suremdm/docs/SureMDM/Enrollments.html";
	String PassStatement = "PASS >> Current url for Documentation is: "+URL;
	String FailStatement = "FAIL >> Current url for Documentation is Not Active";
	ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
	Initialization.driver.close();
	Initialization.driver.switchTo().window(parentwinID);
	sleep(5);
}

@FindBy(xpath="//button[normalize-space()='Release Notes']")
private WebElement ClickonReleaseNotesLink ;

public void ClickonReleaseNotesLink() throws InterruptedException{
	ClickonReleaseNotesLink.click();
	sleep(5);
	Set<String> set=Initialization.driver.getWindowHandles();
	Iterator<String> id=set.iterator();
	String parentwinID = id.next();
	String childwinID = id.next();
	Initialization.driver.switchTo().window(childwinID);
	String URL=Initialization.driver.getCurrentUrl();
	String ExpectedValue = "https://community.42gears.com/c/Release-Notes/5";
	String PassStatement = "PASS >> Current url for ReleaseNotes is: "+URL;
	String FailStatement = "FAIL >> Current url for ReleaseNotes is Not Active";
	ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
	Initialization.driver.close();
	Initialization.driver.switchTo().window(parentwinID);
	sleep(5);
}

@FindBy(xpath="//button[normalize-space()='Training']")
private WebElement ClickonTrainingLink ;

public void ClickonTrainingLink() throws InterruptedException{
	ClickonTrainingLink.click();
	sleep(5);
	Set<String> set=Initialization.driver.getWindowHandles();
	Iterator<String> id=set.iterator();
	String parentwinID = id.next();
	String childwinID = id.next();
	Initialization.driver.switchTo().window(childwinID);
	String URL=Initialization.driver.getCurrentUrl();
	String ExpectedValue = "https://www.42gears.com/services/training/";
	String PassStatement = "PASS >> Current url for Training is: "+URL;
	String FailStatement = "FAIL >> Current url for Training is Not Active";
	ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
	Initialization.driver.close();
	Initialization.driver.switchTo().window(parentwinID);
	sleep(5);
}

// Webhooks

@FindBy(xpath="//a[text()='Webhooks']")
private WebElement ClickonWebhooksButton ;

public void ClickonWebhooksButton() throws InterruptedException
{
	ClickonWebhooksButton.click();
	sleep(5);
	waitForXpathPresent("//input[@id='EnableWebhook']");
	Reporter.log("PASS >> Click on Webhook Option", true);
	sleep(3);
}

@FindBy(xpath="//input[@id='EnableWebhook']")
private WebElement WebhookEnablebyDefault;

public void WebhookEnablebyDefault() throws InterruptedException {
	boolean flag = WebhookEnablebyDefault.isEnabled();
	String PassStatement="PASS >> Webhook Option is Disabled by Default";
	String FailStatement="FAIL >> Webhook Option is Enabled by Default";
	ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
	sleep(2);
}

@FindBy(xpath="//input[@id='EnableWebhook']")
private WebElement EnableWebhookCheckbox ;

public void EnableWebhookCheckbox() throws InterruptedException
{
	EnableWebhookCheckbox.click();
	sleep(5);
	waitForXpathPresent("//span[normalize-space()='Add New Endpoint']");
	sleep(3);
}

@FindBy(xpath="//span[normalize-space()='Add New Endpoint']")
private WebElement ClickonAddNewEndpoint ;

public void ClickonAddNewEndpoint() throws InterruptedException
{
	ClickonAddNewEndpoint.click();
	sleep(5);
	waitForXpathPresent("//div[@id='dynamicEndpointID_1']//span[@class='actionbuttontext'][normalize-space()='Test Connection']");
	sleep(3);
}

@FindBy(xpath="//*[@id='dynamicEndpointID_0']/div[1]/h3")
private WebElement WebhookName1 ;

@FindBy(xpath="//div[31]/div[1]/div[3]/div[1]/div[2]/div[1]/input[1]")
private WebElement valueinfirstbox ;

public void WebhookName1()throws InterruptedException{
	
	boolean flag=WebhookName1.isDisplayed();
	if(flag)
	{
		valueinfirstbox.clear();
		sleep(5);
		valueinfirstbox.sendKeys("WebhookName");
		sleep(5);
		Reporter.log("PASS >> Webhook Name Entered ",true);
		
	}
	else
	{		
		Reporter.log("PASS >> Webhook Name Not Entered ",true);
	}
}

@FindBy(xpath="//*[@id='dynamicEndpointID_0']/div[1]/h3")
private WebElement WebhookEndpoint1 ;

@FindBy(xpath="//div[2]/div/div[31]/div/div[3]/div[1]/div[3]/div/input")
private WebElement Valueinsecondbox ;
public void WebhookEndpoint1()throws InterruptedException{
	
	boolean flag=WebhookEndpoint1.isDisplayed();
	if(flag)
	{
		Valueinsecondbox.clear();
		sleep(5);
		Valueinsecondbox.sendKeys("https://webhook.site/65d0e-7-41");
		sleep(5);
		Reporter.log("PASS >> Webhook Endpoint Entered",true);		
	}
	else
	{		
		Reporter.log("PASS >> Webhook Endpoint Entered",true);
	}
}

@FindBy(xpath="//div/div[31]/div/div[3]/div[1]/div[3]/div/div/span")
private WebElement ClickonTestConnection ;

@FindBy(xpath="//span[normalize-space()='Test connection successful']")
private WebElement  Testconnectionsuccessful;

public void Testconnectionsuccessful()throws InterruptedException{

boolean flag=ClickonTestConnection.isDisplayed();
if(flag)
{
	ClickonTestConnection.click();
	Reporter.log("PASS >> Test Connection Successful",true);	
}
else
{	
	Reporter.log("FAIL >> Test Connection in Not Successful",true);
}
}

//License Management

@FindBy(xpath="//*[@id='licenseManagement']")
private WebElement ClickonlicenseManagement ;

public void ClickonlicenseManagement()throws InterruptedException{
	
	boolean flag=ClickonlicenseManagement.isDisplayed();
	if(flag)
	{
		ClickonlicenseManagement.click();
		sleep(6);
		Reporter.log("PASS >> license Management is Displayed",true);		
	}
	else
	{		
		Reporter.log("FAIL >> license Management is Not Displayed",true);
	}
}

@FindBy(xpath="//span[@id='DeviceCount']")
private WebElement SureMDMDeviceCount ;

@FindBy(xpath="//span[@id='ThingsCount']")
private WebElement SureMDMThingsCount ;

@FindBy(xpath="//span[@id='CloudStorage']")
private WebElement SureMDMStorageSize ;

@FindBy(xpath="//span[@id='VRCount']")
private WebElement SureMDMVRCount ;

public void licenseManagementDetailsCount() throws InterruptedException {
	
	   sleep(5);
       String Count1 = SureMDMDeviceCount.getText();      
       Reporter.log("PASS >> SureMDM Device Count is: "+Count1,true);
       sleep(5);
       String Count2 = SureMDMThingsCount.getText();     
       Reporter.log("PASS >> SureMDM Things Device Count is :"+Count2,true);
       sleep(5);
       String Count3 = SureMDMStorageSize.getText(); 
       Reporter.log("PASS >> SureMDM Device Storage Size is :"+Count3,true);
       sleep(5);
       String Count4 = SureMDMVRCount.getText();
       Reporter.log("PASS >>  SureMDM Device VR Count is: "+Count4,true);
}

@FindBy(xpath="//span[@id='SureLockCode']")
private WebElement SureLockActivationCode ;

@FindBy(xpath="//span[@id='SureVideoCode']")
private WebElement SureVideoActivationCode ;

@FindBy(xpath="//span[@id='SureFoxCode']")
private WebElement SureFoxActivationCode ;

public void licenseManagementSureLockSureFoxSureVideoCode() throws InterruptedException {
	   sleep(5);
       String Code1 = SureLockActivationCode.getText();
       Reporter.log("PASS >>  SureLock Activation Code is: "+Code1,true);
       sleep(5);
       String Code2 = SureVideoActivationCode.getText();      
       Reporter.log("PASS >> SureVideo Activation Code is: "+Code2,true);
       sleep(5);
       String Code3 = SureFoxActivationCode.getText();      
       Reporter.log("PASS >> SureFox Activation Code is: "+Code3,true);
       
}

@FindBy(xpath="//div[@id='licenseDetail_header']//button[@aria-label='Close']")
private WebElement Close ;

public void CloselicenseManagementTab() throws InterruptedException {
	  
	 Close.click();
	 sleep(5);
	 Reporter.log("PASS >> license Management Tab Close ",true);	
}

//Certificate Management 

@FindBy(xpath="//input[@id='searchbyDateRange']")
private WebElement SearchBarCertificateExpiryVisible ;

public void SearchBarCertificateExpiryVisible()throws InterruptedException{
	
	boolean flag=SearchBarCertificateExpiryVisible.isDisplayed();
	if(flag)
	{	
		Reporter.log("PASS >> Search Certificate Expiry using Date range Search Bar Displayed",true);		
	}
	else
	{		
		Reporter.log("FAIL >> Search Certificate Expiry using Date range Search Bar Not Displayed",true);
	}
}


@FindBy(xpath="//div[@id='getManagedCertificatepopup']//button[@aria-label='Close']")
private WebElement CloseCertificateManagement ;

public void CloseCertificateManagement()throws InterruptedException{
	sleep(5);
	CloseCertificateManagement.click();
	Reporter.log("PASS >> Close Certificate Mangement Tab",true);
	sleep(5);
}

@FindBy(xpath="//article[@id='boardingStep1']//a[@title='Close']")
private WebElement CloseEnrollmentWindow ;

public void CloseEnrollmentWindow()throws InterruptedException{
	sleep(5);
	CloseEnrollmentWindow.click();
	Reporter.log("PASS >> Close Close Enrollment Window",true);
	sleep(5);
}


@FindBy(xpath="//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[1]")
private WebElement ClickonTodayCertMag;

public void ClickonTodayCertMag()throws InterruptedException{
		
	Initialization.driver.findElement(By.xpath("//input[@id='searchbyDateRange']")).click();
	waitForXpathPresent("//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[1]");
	sleep(5);			
		
//	Actions action=new Actions(Initialization.driver);
//	action.click(ClickonTodayCertMag).build().perform();
//	sleep(5);	
//	Reporter.log("PASS >> Clicked on Today Button",true);
	
	
	boolean flag=ClickonTodayCertMag.isDisplayed();
	if(flag)
	{	
		ClickonTodayCertMag.click();
		sleep(5);
		System.out.println(flag);
		Reporter.log("PASS >> Today is displayed",true);
	
	}
	else
	{		
		System.out.println(flag);
		ALib.AssertFailMethod("FAIL >> today is not displayed");
		Reporter.log("FAIL >> Today is not displayed",true);
		
	}	
}

@FindBy(xpath="//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[2]")
private WebElement ClickonYesterdayCertMag;

public void ClickonYesterdayCertMag()throws InterruptedException{
	
	sleep(5);
	Initialization.driver.findElement(By.xpath("//input[@id='searchbyDateRange']")).click();
	waitForXpathPresent("//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[2]");
	sleep(5);	
		
//	Actions action=new Actions(Initialization.driver);
//	action.click(ClickonYesterdayCertMag).build().perform();
//	sleep(5);	
//	Reporter.log("PASS >> Clicked on Yesterday Button",true);
	
	boolean flag=ClickonYesterdayCertMag.isDisplayed();
	if(flag)
	{	
		
		ClickonYesterdayCertMag.click();
		sleep(5);
		System.out.println(flag);
		Reporter.log("PASS >> yesterday is displayed ",true);
	
	}
	else
	{		
		System.out.println(flag);
		ALib.AssertFailMethod("FAIL >> yesterday is not displayed");
		Reporter.log("FAIL >> yesterday is not displayed",true);
	}	
}

@FindBy(xpath="//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[3]")
private WebElement ClickonLast1WeekCertMag;

public void ClickonLast1WeekCertMag()throws InterruptedException{
	sleep(5);
	Initialization.driver.findElement(By.xpath("//input[@id='searchbyDateRange']")).click();
	waitForXpathPresent("//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[3]");
	sleep(5);		
	
//    Actions action=new Actions(Initialization.driver);
//	action.click(ClickonLast1WeekCertMag).build().perform();
//	sleep(5);	
//	Reporter.log("PASS >> Clicked on Last 1 Week Button",true);
	
	boolean flag=ClickonLast1WeekCertMag.isDisplayed();
	if(flag)
	{	
		ClickonLast1WeekCertMag.click();
		System.out.println(flag);
		sleep(5);
		Reporter.log("PASS >> Last 1 week is displayed",true);	
	}
	else
	{		
		System.out.println(flag);
		ALib.AssertFailMethod("FAIL >> Last 1 week is not displayed");
		Reporter.log("FAIL >> Last 1 week is not displayed",true);
	}
}	

@FindBy(xpath="//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[4]")
private WebElement ClickonLast30DaysCertMag;

public void ClickonLast30DaysCertMag()throws InterruptedException{
		
	Initialization.driver.findElement(By.xpath("//input[@id='searchbyDateRange']")).click();
	waitForXpathPresent("//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[4]");
	sleep(5);		
	
//	Actions action=new Actions(Initialization.driver);
//    action.click(ClickonLast30DaysCertMag).build().perform();
//	sleep(5);	
//	Reporter.log("PASS >> Clicked on Last 30 Days Button",true);
	
	boolean flag=ClickonLast30DaysCertMag.isDisplayed();
	if(flag)
	{	
		ClickonLast30DaysCertMag.click();
		sleep(5);
		System.out.println(flag);
		Reporter.log("PASS >> Last 30 days is displayed",true);	
	}
	else
	{		
		System.out.println(flag);
		sleep(5);
		ALib.AssertFailMethod("FAIL >> Last 30 days is not displayed");
		Reporter.log("FAIL >> Last 30 days is not displayed",true);
	}
}	

@FindBy(xpath="//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[5]")
private WebElement ThisMonth ;

public void ThisMonth()throws InterruptedException{
		
	Initialization.driver.findElement(By.xpath("//input[@id='searchbyDateRange']")).click();
	waitForXpathPresent("//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[5]");
	sleep(5);		
	
//	Actions action=new Actions(Initialization.driver);
//    action.click(ClickonLast30DaysCertMag).build().perform();
//	sleep(5);	
//	Reporter.log("PASS >> Clicked on Last 30 Days Button",true);
	
	boolean flag=ThisMonth.isDisplayed();
	if(flag)
	{	
		ThisMonth.click();
		System.out.println(flag);
		Reporter.log("PASS >> This Month is present ",true);	
	}
	else
	{		
		System.out.println(flag);
		ALib.AssertFailMethod("FAIL >> This Month is not displayed");
		Reporter.log("FAIL >> This Month is not displayed",true);
		
	}
}	


@FindBy(xpath="//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[6]")
private WebElement CustomRange ;

public void CustomRange()throws InterruptedException{
		
	Initialization.driver.findElement(By.xpath("//input[@id='searchbyDateRange']")).click();
	waitForXpathPresent("//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[6]");
	sleep(5);		
	
//	Actions action=new Actions(Initialization.driver);
//    action.click(ClickonLast30DaysCertMag).build().perform();
//	sleep(5);	
//	Reporter.log("PASS >> Clicked on Last 30 Days Button",true);
	
	boolean flag=CustomRange.isDisplayed();
	if(flag)
	{	
		CustomRange.click();
		System.out.println(flag);
		Reporter.log("PASS >> Custom Range is present ",true);	
	}
	else
	{		
		sleep(5);
		System.out.println(flag);
		ALib.AssertFailMethod("FAIL >> Custom Range is not displayed");
		Reporter.log("FAIL >> Custom Range is Not present ",true);
	}
}	
@FindBy(xpath="//*[contains(@class,'applyBtn btn btn-sm btn-primary')][1]")
private WebElement tryapply ;

public void tryapply() throws InterruptedException
{
	Initialization.driver.findElement(By.xpath("//input[@id='searchbyDateRange']")).click();
	waitForXpathPresent("//div[@class='daterangepicker ltr show-ranges show-calendar opensleft'][1]/div/ul/li[4]");
	sleep(5);
	tryapply.click();
	sleep(5);
	waitForXpathPresent("//*[contains(@class,'applyBtn btn btn-sm btn-primary')][1]");
	sleep(3);
}

@FindBy(xpath="//div[@class='clearDate_icn']//i[@class='icn fa fa-times-circle']")
private WebElement ClearTextBox ;

public void ClearTextBox() throws InterruptedException
{	sleep(3);
	ClearTextBox.click();
	Reporter.log("PASS >> Clicked on Clear Text Button ",true);
	sleep(5);
	waitForXpathPresent("//div[1]/div[2]/div[1]/table/thead/tr/th[2]/div[1]");
	sleep(3);
}

@FindBy(xpath="//*[@id='certificatedetails']/tbody/tr")
private WebElement DeviceInformation ;

public void DeviceInformation()throws InterruptedException{
	
	boolean flag=DeviceInformation.isDisplayed();
	if(flag)
	{	
		
		Reporter.log("FAIL >> The device which will expire the certificate on selected date is Displayed ",true);	
	}
	else
	{		
		
		Reporter.log("PASS >> The device which will expire the certificate on selected date is Not Displayed",true);
	}
}

@FindBy(xpath="//*[@id='ExportCertDetails']/span")
private WebElement ExportbuttonVisible ;

public void ExportbuttonVisible()throws InterruptedException{
	
	boolean flag=ExportbuttonVisible.isDisplayed();
	if(flag)
	{	
		Reporter.log("PASS >> Export Button is Displayed ",true);	
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Export Button is  Not Displayed");
		Reporter.log("FAIL >> Export Button is  Not Displayed",true);
	}
}

//Device Enrollment Rules

@FindBy(xpath="//a[text()='Device Enrollment Rules']")
private WebElement ClickonDeviceEnrollmentRules;

public void ClickonDeviceEnrollmentRules()throws InterruptedException{
		
	boolean flag=ClickonDeviceEnrollmentRules.isDisplayed();
	if(flag)
	{	
	    ClickonDeviceEnrollmentRules.click();
	    sleep(4);
	    waitForXpathPresent("//p[text()='Device Naming']");	    
		Reporter.log("PASS >> Clicked on Device Enrollment Rules",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>  Not Clicked on Device Enrollment Rules");
		Reporter.log("FAIL >> Not Clicked on Device Enrollment Rules",true);
	}
}

@FindBy(xpath="//span[normalize-space()='UPN']")
private WebElement UPNOption ;

public void UPNOption() throws InterruptedException {
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = UPNOption;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(5);
		
	String UPN=Initialization.driver.findElement(By.xpath("//input[@id='provisioning_id']")).getAttribute("value");
		
	boolean flag=UPNOption.isDisplayed();
	if(flag)
	{		
		Reporter.log("PASS >> UPN Name Displayed is: "+UPN,true);
			
	}else {
		ALib.AssertFailMethod("FAIL >> UPN Name is Not Displayed");
		Reporter.log("FAIL >> UPN Name is Not Displayed",true);
	}	
}

@FindBy(xpath="//span[@class='user-nme']")
private WebElement AccountNameVisible ;

public void AccountNameVisible() throws InterruptedException {
		
	String AccountName=Initialization.driver.findElement(By.xpath("//span[@class='user-nme']")).getText();
		
	boolean flag=AccountNameVisible.isDisplayed();
	if(flag)
	{		
		Reporter.log("PASS >> Account Name Displayed is: "+AccountName,true);
			
	}else {
		ALib.AssertFailMethod("FAIL >> Account Name is Not Displayed");
		Reporter.log("FAIL >> Account Name is Not Displayed",true);
	}	
}

@FindBy(xpath="//span[normalize-space()='UPN']")
private WebElement UPN ;

@FindBy(xpath="//span[normalize-space()='Auth Policy']")
private WebElement AuthPolicy ;

@FindBy(xpath="//span[normalize-space()='Discovery Service URL']")
private WebElement DiscoveryServiceURL ;

@FindBy(xpath="//span[normalize-space()='Enrollment Service URL']")
private WebElement EnrollmentServiceURL ;

@FindBy(xpath="//span[normalize-space()='Policy Service URL']")
private WebElement PolicyServiceURL ;

public void StagingProvisioningOptions() throws InterruptedException
{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = UPNOption;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(5);
	String Option1 = UPN.getText();
	String Expected1 = "UPN";
	String pass1 = "PASS >> Option in Staging and Provisioning is : " +Option1;
	String fail1 = "FAIL >> Option in Staging and Provisioning is not Displayed";
	ALib.AssertEqualsMethod(Expected1, Option1, pass1, fail1);	
	sleep(2); 
	
	String Option2 = AuthPolicy.getText();
	String Expected2 = "Auth Policy";
	String pass2 = "PASS >> Option in Staging and Provisioning is : " +Option2;
	String fail2 = "FAIL >> Option in Staging and Provisioning is not Displayed";
	ALib.AssertEqualsMethod(Expected2, Option2, pass2, fail2);	
	sleep(2); 
	
	String Option3 = DiscoveryServiceURL.getText();
	String Expected3 = "Discovery Service URL";
	String pass3 = "PASS >> Option in Staging and Provisioning is : " +Option3;
	String fail3 = "FAIL >> Option in Staging and Provisioning is not Displayed";
	ALib.AssertEqualsMethod(Expected3, Option3, pass3, fail3);	
	sleep(2); 
	
	String Option4 = EnrollmentServiceURL.getText();
	String Expected4 = "Enrollment Service URL";
	String pass4 = "PASS >> Option in Staging and Provisioning is : " +Option4;
	String fail4 = "FAIL >> Option in Staging and Provisioning is not Displayed";
	ALib.AssertEqualsMethod(Expected4, Option4, pass4, fail4);	
	sleep(2); 
	
	String Option5 = PolicyServiceURL.getText();
	String Expected5 = "Policy Service URL";
	String pass5 = "PASS >> Option in Staging and Provisioning is : " +Option5;
	String fail5 = "FAIL >> Option in Staging and Provisioning is not Displayed";
	ALib.AssertEqualsMethod(Expected5, Option5, pass5, fail5);	
	sleep(2); 	
}

//Enterprise Integrations

@FindBy(xpath="//a[normalize-space()='Enterprise Integrations']")
private WebElement ClickonEnterpriseIntegrations;

public void ClickonEnterpriseIntegrations()throws InterruptedException{
		
	boolean flag=ClickonEnterpriseIntegrations.isDisplayed();
	if(flag)
	{	
		ClickonEnterpriseIntegrations.click();
	    sleep(4);
	    waitForXpathPresent("//p[text()='Device Naming']");	    
		Reporter.log("PASS >> Clicked on Enterprise Integrations",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on Enterprise Integrations");
		Reporter.log("FAIL >> Not Clicked on Enterprise Integrations",true);
	}
}

@FindBy(xpath="//div[@id='splunkLogManagement_tab']//span[@class='helpExampleText']//span[1]")
private WebElement HostUrlExampleVisible ;

public void HostUrlExampleVisible() throws InterruptedException
{
	String ExampleURL1 = HostUrlExampleVisible.getText();
	String Expected1 = "( For example: https://<splunk host>:8088/services/collector/event )";
	String pass1 = "PASS >> Host Example Url is : " +ExampleURL1;
	String fail1 = "FAIL >> Host Example Url is not displayed";
	ALib.AssertEqualsMethod(Expected1, ExampleURL1, pass1, fail1);	
	sleep(2); 	
}

@FindBy(xpath="//a[normalize-space()='Cisco ISE Integration']")
private WebElement CiscoISEIntegrationVisible;

public void CiscoISEIntegrationVisible()throws InterruptedException{
		
	boolean flag=CiscoISEIntegrationVisible.isDisplayed();
	if(flag)
	{	
		CiscoISEIntegrationVisible.click();
	    sleep(4);
	    waitForXpathPresent("//span[normalize-space()='Enable Cisco ISE Integration']");
	    
		Reporter.log("PASS >> Cisco ISE Integration is Displayed",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cisco ISE Integration is Not Displayed");
		Reporter.log("FAIL >> Cisco ISE Integration is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='enableCiscoISE']")
private WebElement EnableCiscoISE;

public void EnableCiscoISE()throws InterruptedException{
	
	boolean flag=EnableCiscoISE.isSelected();
	if(flag)
	{
			
		Reporter.log("PASS >> Allready Enable Cisco ISE Check-box",true);		
	}
	else
	{	EnableCiscoISE.click();	
		Reporter.log("PASS >> Enable Cisco ISE Check-box",true);
	}
}

@FindBy(xpath="//input[@id='ciscoISE_username']")
private WebElement CiscoUserName ;

public void CiscoUserName()throws InterruptedException{
	
	boolean flag=CiscoUserName.isDisplayed();
	if(flag)
	{
		CiscoUserName.clear();
		sleep(5);
		CiscoUserName.sendKeys(Config.CiscoISEUserName);
		sleep(5);
		Reporter.log("PASS >> Cisco User Name Entered",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cisco User Name Not Entered");
		Reporter.log("FAIL >> Cisco User Name Not Entered",true);
	}
}

@FindBy(xpath="//input[@id='ciscoISE_password']")
private WebElement CiscoPassword ;

public void CiscoPassword()throws InterruptedException{
	
	boolean flag=CiscoPassword.isDisplayed();
	if(flag)
	{
		CiscoPassword.clear();
		sleep(5);
		CiscoPassword.sendKeys(Config.CiscoISEPassword);
		sleep(5);
		Reporter.log("PASS >> Cisco Password Entered",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cisco Password Not Entered");
		Reporter.log("FAIL >> Cisco Password Not Entered",true);
	}
}

@FindBy(xpath="//input[@id='ciscoISE_Save']")
private WebElement ClickonCiscoSaveButton ;

public void ClickonCiscoSaveButton() throws InterruptedException
{
	sleep(2); 
	ClickonCiscoSaveButton.click();
	
}

@FindBy(xpath="//span[normalize-space()='Cisco ISE Integration Saved Successfully']")
private WebElement CiscoISEIntegrationSavedmsg ;

public void CiscoISEIntegrationSavedmsg() throws InterruptedException
{
	sleep(2); 
	String updatemsg = CiscoISEIntegrationSavedmsg.getText();
	String Expected = "Cisco ISE Integration Saved Successfully";
	String pass = "PASS >> Success Message Dispalyed is : " +updatemsg;
	String fail = "FAIL >> Success Message is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected, updatemsg, pass, fail);	
	sleep(5); 
}

@FindBy(xpath="//input[@id='ciscoISE_username']")
private WebElement CiscoUserNameClear ;

public void CiscoUserNameClear()throws InterruptedException{
	
	boolean flag=CiscoUserNameClear.isDisplayed();
	if(flag)
	{
		CiscoUserNameClear.clear();
		sleep(5);		
		Reporter.log("PASS >> Cisco User Name Cleared",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cisco User Name Not Cleared");
		Reporter.log("FAIL >> Cisco User Name Not Cleared",true);
	}
}

@FindBy(xpath="//input[@id='ciscoISE_password']")
private WebElement CiscoPasswordClear ;

public void CiscoPasswordClear()throws InterruptedException{
	
	boolean flag=CiscoPasswordClear.isDisplayed();
	if(flag)
	{
		CiscoPasswordClear.clear();
		sleep(5);
		Reporter.log("PASS >> Cisco Password Cleared",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Cisco Password Not Cleared");
		Reporter.log("FAIL >> Cisco Password Not Cleared",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Username cannot be empty.']")
private WebElement CiscoISEIntegrationErrorMsg ;

public void CiscoISEIntegrationErrorMsg() throws InterruptedException
{
	sleep(2); 
	String Errormsg = CiscoISEIntegrationErrorMsg.getText();
	String Expected = "Username cannot be empty.";
	String pass = "PASS >> Error Message Dispalyed is : " +Errormsg;
	String fail = "FAIL >> Error Message is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected, Errormsg, pass, fail);	
	sleep(5); 
}

@FindBy(xpath="//div[@class='accordion_header']")
private WebElement WebhookSubscriptionoptions  ;

@FindBy(xpath="//span[normalize-space()='Device Enrollment']")
private WebElement DeviceEnrollment  ;

@FindBy(xpath="//span[normalize-space()='Device Delete']")
private WebElement DeviceDelete  ;

@FindBy(xpath="//span[normalize-space()='Device Wipe']")
private WebElement DeviceWipe  ;

public void WebhookSubscriptionoptions()throws InterruptedException{
	
	boolean flag=WebhookSubscriptionoptions.isDisplayed();
	if(flag)
	{
		WebhookSubscriptionoptions.click();		
		String option1 = DeviceEnrollment.getText();
		String Expected1 = "Device Enrollment";
		String pass1 = "PASS >> Webhook Subscription options Dispalyed is : " +option1;
		String fail1 = "FAIL >> Webhook Subscription options is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
		sleep(5); 
		
		String option2 = DeviceDelete.getText();
		String Expected2 = "Device Delete";
		String pass2 = "PASS >> Webhook Subscription options Dispalyed is : " +option2;
		String fail2 = "FAIL >> Webhook Subscription options is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected2, option2, pass2, fail2);	
		sleep(5); 
		
		String option3 = DeviceWipe.getText();
		String Expected3 = "Device Wipe";
		String pass3 = "PASS >> Webhook Subscription options Dispalyed is : " +option3;
		String fail3 = "FAIL >> Webhook Subscription options is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected3, option3, pass3, fail3);	
		sleep(5); 	
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Webhook Subscription options is Not Dispalyed");
		Reporter.log("FAIL >> Webhook Subscription options is Not Dispalyed",true);
	}
}

@FindBy(xpath="//a[normalize-space()='NAC Integration']")
private WebElement NACIntegrationVisible;

public void NACIntegrationVisible()throws InterruptedException{
		
	boolean flag=NACIntegrationVisible.isDisplayed();
	if(flag)
	{	
		NACIntegrationVisible.click();
	    sleep(4);
	    waitForXpathPresent("//span[normalize-space()='Enable NAC Integration']");	    
		Reporter.log("PASS >> NAC Integration is Displayed",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> NAC Integration is Not Displayed");
		Reporter.log("FAIL >> NAC Integration is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='allowAccess_NAC']")
private WebElement EnableNACIntegration;

public void EnableNACIntegration()throws InterruptedException{
	
	boolean flag=EnableNACIntegration.isSelected();
	if(flag)
	{				
		Reporter.log("PASS >> Allready Enable NAC Integration Check-box is Enabled",true);
	}
	else
	{	
		EnableNACIntegration.click();
		Reporter.log("PASS >> Enable NAC Integration Check-box is Enabled",true);
	}
}

//Customize Nix/SureLock

@FindBy(xpath="//a[normalize-space()='Customize SureMDM Agent/SureLock']")
private WebElement ClickonCustomizeNixSureLock;

public void ClickonCustomizeNixSureLock()throws InterruptedException{
		
	boolean flag=ClickonCustomizeNixSureLock.isDisplayed();
	if(flag)
	{	
		ClickonCustomizeNixSureLock.click();
	    sleep(2);
	    waitForXpathPresent("//a[normalize-space()='Windows']");	    
		Reporter.log("PASS >> Customize Nix Sure Lock is Displayed & Clicked",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Customize Nix Sure Lock is Not Displayed");
		Reporter.log("FAIL >> Customize Nix Sure Lock is Not Displayed",true);
	}
}

@FindBy(xpath="//a[normalize-space()='Android']")
private WebElement ClickonAndriod ;

public void ClickonAndriod()throws InterruptedException{
		
	boolean flag=ClickonAndriod.isDisplayed();
	if(flag)
	{	
		ClickonAndriod.click();
	    sleep(2);
	    waitForXpathPresent("//a[normalize-space()='Windows']");	    
		Reporter.log("PASS >> Clicked on Andriod",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on Andriod");
		Reporter.log("FAIL >> Not Clicked on Andriod",true);
	}
}

public void CustomizeNixActivityLogs() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'Customize Nix settings modified by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Customize Nix settings modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

@FindBy(xpath="//input[@id='customiseApps_tab_apply']")
private WebElement ClickonAndriodGenerateButton ;

@FindBy(xpath="//span[contains(text(),'App customization successful. Please click on down')]")
private WebElement AndriodMsg ;

public void ClickonAndriodGenerateButton()throws InterruptedException{
		
	boolean flag=ClickonAndriodGenerateButton.isDisplayed();
	if(flag)
	{	
		ClickonAndriodGenerateButton.click();
		Reporter.log("PASS >> Clicked on Generate Button",true);
	    waitForXpathPresent("//span[contains(text(),'App customization successful. Please click on down')]");	    		
		String msg = AndriodMsg.getText();		
		Reporter.log("PASS >> Success Message for Generate is: "+msg,true);
		sleep(2);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Success Message for Generate is Not Displayed");
		Reporter.log("FAIL >> Success Message for Generate is Not Displayed",true);
	}
}

@FindBy(xpath="//a[normalize-space()='Windows']")
private WebElement ClickonWindows ;

public void ClickonWindows()throws InterruptedException{
		
	boolean flag=ClickonWindows.isDisplayed();
	if(flag)
	{	sleep(2);
		ClickonWindows.click();
	    sleep(2);
	    waitForXpathPresent("//span[normalize-space()='Windows']");	    
		Reporter.log("PASS >> Windows is Displayed & Clicked",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Windows is Not Displayed");
		Reporter.log("FAIL >> Windows is Not Displayed",true);
	}
}

@FindBy(xpath="//div[@id='win_customiseApps_tab']//select[@id='app_to_customize']")
private WebElement SelectAppDropDown ;

public void SelectAppDropDown() {
	
     Select se = new Select(SelectAppDropDown);
     se.selectByVisibleText("SureMDM Agent");

     String SelectApp=Initialization.driver.findElement(By.xpath("//div[@id='win_customiseApps_tab']//select[@id='app_to_customize']")).getAttribute("value");
     Reporter.log("PASS >> Select App text Displayed is : "+SelectApp,true);

}

@FindBy(xpath="//input[@id='win_custom_app_downloadlink']")
private WebElement AppDownloadURL ;

public void AppDownloadURL() throws InterruptedException {
		
	String AppDownloadUrl=Initialization.driver.findElement(By.xpath("//input[@id='win_custom_app_downloadlink']")).getAttribute("value");
	
	boolean flag=AppDownloadURL.isDisplayed();
	if(flag)
	{		
		Reporter.log("PASS >> App Download URL Displayed is: "+AppDownloadUrl,true);
			
	}else {
		ALib.AssertFailMethod("FAIL >> App Download URL is Not Displayed");
		Reporter.log("FAIL >> App Download URL is Not Displayed",true);
	}	
}


@FindBy(xpath="//input[@id='win_customiseApps_tab_apply']")
private WebElement ClickonGenerateButton ;

@FindBy(xpath="//span[contains(text(),'App customization successful. Please click on down')]")
private WebElement Successmsg ;

public void ClickonGenerateButton()throws InterruptedException{
		
	boolean flag=ClickonGenerateButton.isDisplayed();
	if(flag)
	{	
		ClickonGenerateButton.click();
	    sleep(5);
	    waitForXpathPresent("//span[contains(text(),'App customization successful. Please click on down')]");	    
		Reporter.log("PASS >> Clicked on Generate Button",true);
		String msg = Successmsg.getText();		
		Reporter.log("PASS >> Success Message for Generate is: "+msg,true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Success Message for Generate is Not Displayed");
		Reporter.log("FAIL >> Success Message for Generate is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='win_custom_app_downloadlink']")
private WebElement ClearAppDownloadUrl ;

public void ClearAppDownloadUrl()throws InterruptedException{
		
	boolean flag=ClearAppDownloadUrl.isDisplayed();
	if(flag)
	{	
		ClearAppDownloadUrl.clear();
	    sleep(2);
	    waitForXpathPresent("//input[@id='win_customiseApps_tab_apply']");	    
		Reporter.log("PASS >> App Download Url is Cleared",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Download Url is Not Cleared");
		Reporter.log("FAIL >> App Download Url is Not Cleared",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Download Link cannot be empty.']")
private WebElement Errormsg ;

public void ClickonGenerateAfterClear()throws InterruptedException{
		
	boolean flag=ClickonGenerateButton.isDisplayed();
	if(flag)
	{	
		ClickonGenerateButton.click();
	    sleep(2);
	    waitForXpathPresent("//span[normalize-space()='Download Link cannot be empty.']");	    
		Reporter.log("PASS >> Clicked on Generate Button",true);
		String msg = Errormsg.getText();		
		Reporter.log("PASS >> Error Message for Generate is: "+msg,true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Error Message for Generate is Not Displayed");
		Reporter.log("FAIL >> Error Message for Generate is Not Displayed",true);
	}
}

@FindBy(xpath="//div[@id='win_customiseApps_tab']//button[@id='sett_edit']")
private WebElement ClickonEditbutton ;

@FindBy(xpath="//*[@id='sett_edit_textArea']")
private WebElement Cleartheappsettings ;

@FindBy(xpath="//button[@class='btn smdm_btns smdm_grn_clr addbutton sc-doneBtn'][normalize-space()='Done']")
private WebElement DoneButton ;


public void ClickonEditbutton()throws InterruptedException{
		
	boolean flag=ClickonEditbutton.isDisplayed();
	if(flag)
	{	
		ClickonEditbutton.click();
	    sleep(2);
	    waitForXpathPresent("//textarea[@id='sett_edit_textArea']");	
	    Cleartheappsettings.clear();
	    DoneButton.click();
	    sleep(4);	    
		Reporter.log("PASS >> Clicked on Edit Button",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on Edit Button");
		Reporter.log("FAIL >> Not Clicked on Edit Button",true);
	}
}

@FindBy(xpath="//span[normalize-space()='App Settings field must be filled.']")
private WebElement ErrormsgAfterClearAppSettings ;

public void ErrormsgAfterClearAppSettings()throws InterruptedException{
		
	boolean flag=ClickonGenerateButton.isDisplayed();
	if(flag)
	{	
		ClickonGenerateButton.click();
	    sleep(2);
	    waitForXpathPresent("//span[normalize-space()='App Settings field must be filled.']");	    
		Reporter.log("PASS >> Clicked on Generate Button",true);
		String msg = ErrormsgAfterClearAppSettings.getText();		
		Reporter.log("PASS >> Error Message for Generate is: "+msg,true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Error Message for Generate is Not Displayed");
		Reporter.log("FAIL >> Error Message for Generate is Not Displayed",true);
	}
}

//Miscellaneous Settings

@FindBy(xpath="//a[normalize-space()='Miscellaneous Settings']")
private WebElement ClickonMiscellaneousSetting ;

public void ClickonMiscellaneousSetting()throws InterruptedException{
		
	boolean flag=ClickonMiscellaneousSetting.isDisplayed();
	if(flag)
	{	
		ClickonMiscellaneousSetting.click();
	    sleep(2);
	    Reporter.log("PASS >> Clicked on Miscellaneous Settings",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Clicked on Miscellaneous Settings");
		Reporter.log("FAIL >> Clicked on Miscellaneous Settings",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Enable Advanced Device Management']")
private WebElement EnableAdvancedDeviceManagement ;

public void EnableAdvancedDeviceManagement()throws InterruptedException{
		
	boolean flag=EnableAdvancedDeviceManagement.isEnabled();
	if(flag)
	{	
	    Reporter.log("PASS >> Enable Advanced Device Management is Enabled",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Enable Advanced Device Management is Disabled");
		Reporter.log("FAIL >> Enable Advanced Device Management is Disabled",true);
	}
}

//Alert Template

@FindBy(xpath="//a[normalize-space()='Alert Template']")
private WebElement ClickonAlertTemplate ;

public void ClickonAlertTemplate()throws InterruptedException{
		
	boolean flag=ClickonAlertTemplate.isDisplayed();
	if(flag)
	{	
		ClickonAlertTemplate.click();
	    sleep(2);
	    waitForXpathPresent("//div[@id='messTemplates_tab']//div[@class='tab-pane-body']//div[1]//div[2]//div[1]//span[1]");	
	    Reporter.log("PASS >> Clicked on Alert Template",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Clicked on Alert Template");
		Reporter.log("FAIL >> Clicked on Alert Template",true);
	}
}

@FindBy(xpath="//b[normalize-space()='%OfflineLocalDateTime% (Android only)']")
private WebElement OfflineLocalDateTimeVisible  ;

@FindBy(xpath="//b[normalize-space()='%OnlineLocalDateTime% (Android only)']")
private WebElement OnlineLocalDateTimeVisible  ;

public void OfflineOnlineTagVisble()throws InterruptedException{
	
	boolean flag=OfflineLocalDateTimeVisible.isDisplayed();
	if(flag)
	{
		String option1 = OfflineLocalDateTimeVisible.getText();
		String Expected1 = "%OfflineLocalDateTime% (Android only)";
		String pass1 = "PASS >> Tag in Alert Template is : " +option1;
		String fail1 = "FAIL >> Tag in Alert Template is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
		sleep(2); 		 	
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Webhook Subscription options is Not Dispalyed");
		Reporter.log("FAIL >> Webhook Subscription options is Not Dispalyed",true);
	}
	
	boolean flag1=OnlineLocalDateTimeVisible.isDisplayed();
	if(flag1)
	{
		String option1 = OnlineLocalDateTimeVisible.getText();
		String Expected1 = "%OnlineLocalDateTime% (Android only)";
		String pass1 = "PASS >> Tag in Alert Template is : " +option1;
		String fail1 = "FAIL >> Tag in Alert Template is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
		sleep(2); 		 	
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>  Webhook Subscription options is Not Dispalyed");
		Reporter.log("FAIL >> Webhook Subscription options is Not Dispalyed",true);
	}
}

//Data Analytics

@FindBy(xpath="//span[normalize-space()='MTD Scan']")
private WebElement MTDAppScanVisible ;

public void MTDAppScanVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = MTDAppScanVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=MTDAppScanVisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    Reporter.log("PASS >> MTD App Scan is Displayed",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> MTD App Scan is Not Displayed");
		Reporter.log("FAIL >> MTD App Scan is Not Displayed",true);
	}
}


@FindBy(xpath="(//*[@id='secretKeyBlock']/div)[3]")
private WebElement MTDAppScanSecretKeyVisible ;

public void MTDAppScanSecretKeyVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = MTDAppScanVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=MTDAppScanSecretKeyVisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    MTDAppScanSecretKeyVisible.click();
	    Reporter.log("PASS >> Secret Key is Displayed",true);
	    String Secret = MTDAppScanSecretKeyVisible.getText();		
		Reporter.log("PASS >> Secret Key For MTD Scan is : "+Secret,true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Secret Key For MTD Scan is Not Displayed");
		Reporter.log("FAIL >> Secret Key For MTD Scan is Not Displayed",true);
	}
}


@FindBy(xpath="//span[normalize-space()='SureLock Analytics']")
private WebElement SureLockVisible ;

public void SureLockVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = SureLockVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=SureLockVisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    Reporter.log("PASS >> Sure Lock is Displayed",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Sure Lock is Not Displayed");
		Reporter.log("FAIL >> Sure Lock is Not Displayed",true);
	}
}


@FindBy(xpath="(//*[@id='secretKeyBlock']/div)[1]")
private WebElement SureLockSecretKeyVisible ;

public void SureLockSecretKeyVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = SureLockSecretKeyVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=SureLockSecretKeyVisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    SureLockSecretKeyVisible.click();
	    Reporter.log("PASS >> Secret Key is Displayed",true);
	    String Secret = SureLockSecretKeyVisible.getText();		
		Reporter.log("PASS >> Secret Key For Sure Lock is : "+Secret,true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Secret Key For Sure Lock is Not Displayed");
		Reporter.log("FAIL >> Secret Key For Sure Lock is Not Displayed",true);
	}
}


@FindBy(xpath="//span[normalize-space()='SureFox Analytics']")
private WebElement SureFoxVisible ;

public void SureFoxVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = SureFoxVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=SureFoxVisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    Reporter.log("PASS >> Sure Fox is Displayed",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Sure Fox is Not Displayed");
		Reporter.log("FAIL >> Sure Fox is Not Displayed",true);
	}
}


@FindBy(xpath="(//*[@id='secretKeyBlock']/div)[2]")
private WebElement SureFoxSecretKeyVisible ;

public void SureFoxSecretKeyVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = SureFoxSecretKeyVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=SureFoxSecretKeyVisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    SureFoxSecretKeyVisible.click();
	    sleep(2);
	    Reporter.log("PASS >> Secret Key is Displayed",true);
	    String Secret = SureFoxSecretKeyVisible.getText();		
		Reporter.log("PASS >> Secret Key For Sure Fox is : "+Secret,true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Secret Key For Sure Fox is Not Displayed");
		Reporter.log("FAIL >> Secret Key For Sure Fox is Not Displayed",true);
	}
}

@FindBy(xpath="//span[normalize-space()='Alert Message Analytics']")
private WebElement AlertMessageAnalyticsvisible ;

public void AlertMessageAnalyticsvisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = AlertMessageAnalyticsvisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=AlertMessageAnalyticsvisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    Reporter.log("PASS >> Alert Message Analytics is Displayed",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Alert Message Analytics is Not Displayed");
		Reporter.log("FAIL >> Alert Message Analytics is Not Displayed",true);
	}
}


@FindBy(xpath="(//*[@id='secretKeyBlock']/div)[4]")
private WebElement AlertMessageAnalyticsSecretKeyVisible ;

public void AlertMessageAnalyticsSecretKeyVisible()throws InterruptedException{
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = AlertMessageAnalyticsSecretKeyVisible;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);	
	boolean flag=AlertMessageAnalyticsSecretKeyVisible.isDisplayed();
	if(flag)
	{		
	    sleep(2);
	    AlertMessageAnalyticsSecretKeyVisible.click();
	    Reporter.log("PASS >> Secret Key is Displayed",true);
	    String Secret = AlertMessageAnalyticsSecretKeyVisible.getText();		
		Reporter.log("PASS >> Secret Key For Alert Message Analytics is : "+Secret,true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Secret Key For Alert Message Analytics is Not Displayed");
		Reporter.log("FAIL >> Secret Key For Alert Message Analytics is Not Displayed",true);
	}
}

	
//Group Assignment Rules

@FindBy(xpath="//input[@id='enable_migration']")
private WebElement EnableGroupAssignmentRule ;

public void EnableGroupAssignmentRule()throws InterruptedException{
	
	boolean flag=EnableGroupAssignmentRule.isSelected();
	if(flag==false)
	{	   
		sleep(2);
		EnableGroupAssignmentRule.click(); 
		Reporter.log("PASS >> Enable Group Assignment Rule CheckBox",true);
	    sleep(4);
	}
	else
	{		
		Reporter.log("PASS>> Enable Group Assignment Rule CheckBox",true);
	}
}
	


@FindBy(xpath="//div[@id='groupAssignmentAddRuleModal']//button[@aria-label='Close']")
private WebElement CloseADDRuleTab;

public void CloseADDRuleTab()throws InterruptedException{
	sleep(2);
	CloseADDRuleTab.click();
	sleep(2);
	Reporter.log("PASS >> Closed ADD Rules Tab",true);
}



@FindBy(xpath="//div[@id='groupAssignAddRule']//i[@class='icn fa fa-plus']")
private WebElement ClickonADDRulesbutton;

public void ClickonADDRulesbutton()throws InterruptedException{
	sleep(2);
	ClickonADDRulesbutton.click();
	sleep(2);
	waitForXpathPresent("//h4[normalize-space()='Add Rule']");
	Reporter.log("PASS >> Clicked on ADD Rules Button",true);
}

public void ADDRulesPageVisible()throws InterruptedException{
	
    String PageVisible=Initialization.driver.findElement(By.xpath("//h4[text()='Add Rule']")).getText();		
    Reporter.log("PASS >> Page Displayed is : "+PageVisible,true);
}

@FindBy(xpath="//select[@class='rule_column form-control ct-model-input ct-select-ele parllSelects fstEle selected_tables_names']")
private WebElement SSIDDropDownOption ;

public void SSIDDropDownOption() {
	
   Select se = new Select(SSIDDropDownOption);
   se.selectByVisibleText("SSID");

   String Value=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
   Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value,true);

}	

@FindBy(xpath="//input[@class='rule_name_input form-control ct-model-input rules-input-feild']")
private WebElement FillSSIDRule ;

@FindBy(xpath="//button[normalize-space()='Source group']")
private WebElement ClickonSourceGroup ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='SourceGroup001']")
private WebElement ClickonGroup1 ;

@FindBy(xpath="//button[@class='btn smdm_btns smdm_grn_clr addbutton buttonwidth'][normalize-space()='Add']")
private WebElement ClickonADDButtonGroup1 ;

@FindBy(xpath="//input[@class='ct-selbox-in apply_recursive']")
private WebElement CheckBoxSelected ;

@FindBy(xpath="//input[@placeholder='Rule value 1']")
private WebElement Rule1Value ;

@FindBy(xpath="//div[@class='input-group ct-w100 migrationrules addRow_rule']//input[@placeholder='Rule value 1']")
private WebElement Rule2Value ;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[3]/input[1]")
private WebElement Rule3Value ;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[4]/input[1]")
private WebElement Rule4Value ;

@FindBy(xpath="//button[normalize-space()='Destination group']")
private WebElement ClickonDestinationGroup ;

@FindBy(xpath="//li[@class='list-group-item node-groupList'][normalize-space()='DestinationGroup001']")
private WebElement ClickonGroup2 ;

@FindBy(xpath="//div[@id='groupListModal']//button[@type='button'][normalize-space()='Add']")
private WebElement ClickonADDbuttonGroup2 ;

@FindBy(xpath="//button[@id='SaveAssignmentRule']")
private WebElement ClickonSavebutton ;

@FindBy(xpath="//button[@id='device_migrationrules_apply']")
private WebElement ClickonApplybutton ;

public void FillSSIDRule()throws InterruptedException{
		
	boolean flag=FillSSIDRule.isDisplayed();
	if(flag)
	{	
		FillSSIDRule.sendKeys(Config.RuleName1);
	    sleep(2);
	    ClickonSourceGroup.click();
	    sleep(2);
	    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
		WebElement scrollDown = ClickonGroup1;
		Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
		Reporter.log("PASS >> Source Group001 Selected",true);
		sleep(5); 
	    ClickonGroup1.click();
	    sleep(2);
	    ClickonADDButtonGroup1.click();
	    sleep(2);
	    CheckBoxSelected.click();
	    sleep(2);
	    Select se = new Select(SSIDDropDownOption);
	    se.selectByVisibleText("SSID");
	    String Value=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
	    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value,true);
	    Rule1Value.sendKeys(Config.SSIDName);
	    sleep(2);
	    ClickonDestinationGroup.click();
	    sleep(5);
	    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
		WebElement scrollDown1 = ClickonGroup2;
		Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
		Reporter.log("PASS >> Destination Group001 Selected",true);
		sleep(5); 
		ClickonGroup2.click();
		sleep(2);
		ClickonADDbuttonGroup2.click();
		sleep(2);
		ClickonSavebutton.click();
		sleep(5);
		ClickonApplybutton.click();
		sleep(5);
		Reporter.log("PASS >> Rules Added Successfully",true);	 
	    Reporter.log("PASS >> Settings Updated Successfully",true);	    
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Settings Not Updated Successfully");
		Reporter.log("FAIL >> Settings Not Updated Successfully",true);
	}
}

@FindBy(xpath="//input[@class='rule_name_input form-control ct-model-input rules-input-feild']")
private WebElement MultipleSSIDRules ;

@FindBy(xpath="//div[@title='Add Row']//i[@class='icn fa fa-plus-square-o']")
private WebElement ClickonADD ;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[2]/select[2]")
private WebElement  SSIDDropDownOption1;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[3]/select[2]")
private WebElement  DeviceModelDropDownOption;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[4]/select[2]")
private WebElement  IpAddressDropDownOption;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[4]/select[3]")
private WebElement  EqualtoDropDownOption;

public void MultipleSSIDRules()throws InterruptedException{

	MultipleSSIDRules.sendKeys(Config.MultipleSSIDRule);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
    ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
	
    ClickonADD.click();
    sleep(2);
    
    Select se2 = new Select(SSIDDropDownOption1);
    se2.selectByVisibleText("SSID");
    String Value2=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value2,true);
    Rule2Value.sendKeys(Config.SSIDName2);
    sleep(2);
	
    ClickonADD.click();
    sleep(2);
    
    Select se3 = new Select(DeviceModelDropDownOption);
    se3.selectByVisibleText("Device Model");
    String Value3=Initialization.driver.findElement(By.xpath("//option[@value='devicemodel']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value3,true);
    Rule3Value.sendKeys(Config.DeviceModelName);
    sleep(2);   
    ClickonADD.click();
    sleep(2);
    
    Select se4 = new Select(IpAddressDropDownOption);
    se4.selectByVisibleText("Global IP Address");
    String Value4=Initialization.driver.findElement(By.xpath("//option[@value='ipaddress']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value4,true);
    sleep(2);
    
    Select se5 = new Select(EqualtoDropDownOption);
    se5.selectByVisibleText("Equals (=)");
    String Value5=Initialization.driver.findElement(By.xpath("//option[@value='=']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value5,true);
    Rule4Value.sendKeys(Config.GlobalIPAddress);
    sleep(2);    
    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	        
}

@FindBy(xpath="//input[@class='rule_name_input form-control ct-model-input rules-input-feild']")
private WebElement SSIDDeviceModelRule ;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[2]/select[2]")
private WebElement  DeviceModelDropDown;

public void SSIDDeviceModel()throws InterruptedException{

	SSIDDeviceModelRule.sendKeys(Config.RuleName1);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
	
    ClickonADD.click();
    sleep(2);
    
    Select se2 = new Select(DeviceModelDropDown);
    se2.selectByVisibleText("Device Model");
    String Value2=Initialization.driver.findElement(By.xpath("//option[@value='devicemodel']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value2,true);
    Rule2Value.sendKeys(Config.DeviceModelName);
    sleep(2);   
    
    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	   
   
}

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[2]/select[2]")
private WebElement  IPAddressDropDown;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[2]/select[3]")
private WebElement  EqualtoOption;

public void SSIDIPaddress()throws InterruptedException{

	SSIDDeviceModelRule.sendKeys(Config.RuleName1);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
	
    ClickonADD.click();
    sleep(2);
    
    Select se2 = new Select(IPAddressDropDown);
    se2.selectByVisibleText("Global IP Address");
    String Value2=Initialization.driver.findElement(By.xpath("//option[@value='ipaddress']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value2,true);
    sleep(2);
    
    Select se3 = new Select(EqualtoOption);
    se3.selectByVisibleText("Equals (=)");
    String Value3=Initialization.driver.findElement(By.xpath("//option[@value='=']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value3,true);
    Rule2Value.sendKeys(Config.GlobalIPAddress);
    sleep(2);
 
    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	     
}

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div[2]/select[3]")
private WebElement  ValueOption;

public void SSIDEqualSSIDValue()throws InterruptedException{

	SSIDDeviceModelRule.sendKeys(Config.RuleName1);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
	
    ClickonADD.click();
    sleep(2);
    
    Select se2 = new Select(SSIDDropDownOption1);
    se2.selectByVisibleText("SSID");
    String Value2=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value2,true);
    sleep(2);
    
    Select se3 = new Select(ValueOption);
    se3.selectByVisibleText("In (value1,value2)");
    String Value3=Initialization.driver.findElement(By.xpath("//option[@value='in']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value3,true);
    Rule2Value.sendKeys(Config.SSIDValue);
    sleep(2);

    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	     
}

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP1']")
private WebElement  GROUP1 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP2']")
private WebElement  GROUP2 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP3']")
private WebElement  GROUP3 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP4']")
private WebElement  GROUP4 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP5']")
private WebElement  GROUP5 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP6']")
private WebElement  GROUP6 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP7']")
private WebElement  GROUP7 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP8']")
private WebElement  GROUP8 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP9']")
private WebElement  GROUP9 ;

@FindBy(xpath="//li[@class='list-group-item node-groupCheckList'][normalize-space()='GROUP10']")
private WebElement  GROUP10 ;

public void MultipleSource()throws InterruptedException{
	
	SSIDDeviceModelRule.sendKeys(Config.MultipleSource);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = GROUP1 ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);	
	sleep(5);
	GROUP1.click();
	Reporter.log("PASS >> Group1 Selected",true);   
	GROUP2.click();
	Reporter.log("PASS >> Group2 Selected",true);	
	GROUP3.click();
	Reporter.log("PASS >> Group3 Selected",true);	
	GROUP4.click();
	Reporter.log("PASS >> Group4 Selected",true);	
	GROUP5.click();
	Reporter.log("PASS >> Group5 Selected",true);	
	GROUP6.click();
	Reporter.log("PASS >> Group6 Selected",true);	
	GROUP7.click();
	Reporter.log("PASS >> Group7 Selected",true);	
	GROUP8.click();
	Reporter.log("PASS >> Group8 Selected",true);	
	GROUP9.click();
	Reporter.log("PASS >> Group9 Selected",true);	
	GROUP10.click();
	Reporter.log("PASS >> Group10 Selected",true);
	
	ClickonADDButtonGroup1.click();
	Reporter.log("PASS >> Clicked on ADD Button",true);
}

@FindBy(xpath="//li[@class='list-group-item node-groupList'][normalize-space()='DESTINATIONGROUP']")
private WebElement  DESTINATIONGROUP ;

@FindBy(xpath="//*[@id='groupAssignmentAddRuleModal']/div/div/div[2]/div/div/div[1]/div[4]/div/select[2]")
private WebElement  IPEqualDrop ;


public void ADDIPAddress()throws InterruptedException{
	
	CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("Global IP Address");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ipaddress']")).getAttribute("value");
    System.out.println("PASS >> ADD Rules Drop Down text Displayed is : "+Value1);
    sleep(2);
    
	Select se2 = new Select(IPEqualDrop);
    se2.selectByVisibleText("Equals (=)");
    String Value2=Initialization.driver.findElement(By.xpath("//option[@value='=']")).getAttribute("value");
    System.out.println("PASS >> ADD Rules Drop Down text Displayed is : "+Value2);
    Rule1Value.sendKeys(Config.GlobalIPAddress);
    sleep(2); 
    
    ClickonDestinationGroup.click();
    sleep(5);
    
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = DESTINATIONGROUP ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);	
	sleep(5);
	DESTINATIONGROUP.click();
	sleep(5);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	    
	
}

// Activity log in Group Assignment Rule

@FindBy(xpath="//td[normalize-space()='Edit Rule']")
private WebElement  ClickonEditRule;

@FindBy(xpath="//div[@id='groupAssignEditRule']//i[@class='icn fa fa-pencil-square-o']")
private WebElement  ClickonEditbuttononGroup;

public void EditRule() throws InterruptedException
{
	SSIDDeviceModelRule.sendKeys(Config.EditRuleName);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup1;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
	
    ClickonDestinationGroup.click();
    sleep(5);
    
    JavascriptExecutor Js2=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown2 = DESTINATIONGROUP ;
	Js2.executeScript("arguments[0].scrollIntoView(true);", scrollDown2);	
	sleep(5);
	DESTINATIONGROUP.click();
	sleep(5);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	   
		
	ClickonEditRule.click();
	sleep(2);
	ClickonEditbuttononGroup.click();
	Reporter.log("PASS >> Clicked on Edit Button",true);
	sleep(2);
	ClickonADD.click();
	Reporter.log("PASS >> Clicked on ADD Button",true);
    sleep(2);
    ClickonSavebutton.click();
    Reporter.log("PASS >> Clicked on Save Button",true);
	sleep(5);
	ClickonApplybutton.click();
	Reporter.log("PASS >> Clicked on Apply Button",true);
	sleep(5);
	
}

public void RulesModifiedLogInfo() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'Group Assignment Rules modified by')]");
	String Modifiedmsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Group Assignment Rules modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is :"+Modifiedmsg,true);
	sleep(2);		
}

public void SSIDSingle() throws InterruptedException{
	
	SSIDDeviceModelRule.sendKeys(Config.RuleName1);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
    
    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	   	
}

public void AcitvityLogInformation() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'Group Assignment Rules modified by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Group Assignment Rules modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

public void SSIDDeviceMove() throws InterruptedException{

	SSIDDeviceModelRule.sendKeys(Config.RuleName1);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDNameDevice);
    sleep(2);
    
    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);	        
}

@FindBy(xpath="//td[normalize-space()='Edit Rule']")
private WebElement  SelectDestinationGroup ;

public void AcitvityLogInformationSSID() throws InterruptedException{
		 	
	//Reporter.log("PASS >> Activity Log Message Displayed is :  Device 'AutomationDevice_A51' moved from 'SourceGroup001' to 'DestinationGroup001' based on the group assignment rule Rule.");
		
	WebDriverWait wait = new WebDriverWait(Initialization.driver,500);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(text(),'based on the group assignment rule')]")));
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'based on the group assignment rule')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

// iOS/iPadOS/macOS Settings DEP

@FindBy(xpath="//a[normalize-space()='iOS/iPadOS/macOS Settings']")
private WebElement ClickonIOSSettings ;

public void ClickonIOSSettings()throws InterruptedException{
		
	boolean flag=ClickonIOSSettings.isDisplayed();
	if(flag)
	{	
		ClickonIOSSettings.click();
	    sleep(2);
	    waitForXpathPresent("//a[normalize-space()='DEP']");	    
		Reporter.log("PASS >> Clicked on IOS Settings",true);
	}
	else
	{	
		ALib.AssertFailMethod("FAIL >> Not Clicked on IOS Settings");
		Reporter.log("FAIL >> Not Clicked on IOS Settings",true);
	}
}

@FindBy(xpath="//a[normalize-space()='DEP']")
private WebElement ClickonDEPSettings ;

public void ClickonDEPSettings()throws InterruptedException{
	
	boolean flag=ClickonDEPSettings.isDisplayed();
	if(flag)
	{	
		ClickonDEPSettings.click();
	    sleep(2);
	    waitForXpathPresent("//a[normalize-space()='DEP Profiles']");	    
		Reporter.log("PASS >> Clicked on DEP Settings",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on DEP Settings");
		Reporter.log("FAIL >> Not Clicked on DEP Settings",true);
	}
}

@FindBy(xpath="//td[normalize-space()='Edit Profile Automation']")
private WebElement EditProfile ;

@FindBy(xpath="//div[@id='EditDepProfile']//i[@class='icn fa fa-pencil-square-o']")
private WebElement ClickonEDITbutton ;

@FindBy(xpath="//input[@id='support_phone_number']")
private WebElement SupportPhoneNumberADD ;

@FindBy(xpath="//input[@id='support_email_address']")
private WebElement SupportEmailidADD ;

@FindBy(xpath="//div[@id='dep_profile_popup']//button[@type='button'][normalize-space()='Save']")
private WebElement Savebtn ;

@FindBy(xpath="//div[@id='ios_profile_tab']//input[@placeholder='Search']")
private WebElement SearchProfileCreated ;

public void EditProfile()throws InterruptedException{
 
	
	ClickonADDProfile.click();
	Reporter.log("PASS >> Clicked on ADD Profile",true);
	sleep(2);
	ProfileNameEntered.clear();
	sleep(2);
	ProfileNameEntered.sendKeys(Config.EditProfileName);
	Reporter.log("PASS >> Profile Name Entered",true);
	sleep(2);
	Savebtn.click();
	Reporter.log("PASS >> Clicked on Save button",true);
	Reporter.log("PASS >> Profile Created Successfully",true);
	
	sleep(2);
	
	SearchProfileCreated.sendKeys(Config.EditProfileName);
		
	//Nextbtn.click();
	sleep(4);
	EditProfile.click();
	Reporter.log("PASS >> Clicked on Edit Profile Automation",true);
	sleep(2);
	ClickonEDITbutton.click();
	Reporter.log("PASS >> Clicked on Edit button",true);
	sleep(2);
	SupportPhoneNumberADD.clear();
	sleep(2);
	SupportPhoneNumberADD.sendKeys(Config.SupportPhoneNumber);
	Reporter.log("PASS >> Support Phone Number Entered",true);
	sleep(2);
	SupportEmailidADD.clear();
	sleep(2);
	SupportEmailidADD.sendKeys(Config.SupportEmailid);
	Reporter.log("PASS >> Support Email Address Entered",true);
	sleep(2);
	Savebtn.click();
	Reporter.log("PASS >> Clicked on Save button",true);
	Reporter.log("PASS >> Profile Updated Successfully",true);
}

public void DEPmodifiedActivityLogs() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'DEP profile Edit Profile Automation under DEP Profiles modified by')]");
	String DEPMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'DEP profile Edit Profile Automation under DEP Profiles modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+DEPMsg,true);
	sleep(2);
}

@FindBy(xpath="//div[@id='dep_profile_edit_advncstng']//i[@class='icn fa fa-plus']")
private WebElement ClickonADDProfile ;

@FindBy(xpath="//input[@id='profile_name']")
private WebElement ProfileNameEntered ;

@FindBy(xpath="//td[normalize-space()='Automation DEP Profile']")
private WebElement SelectCreatedProfile ;

@FindBy(xpath="//div[@id='DeleteDepProfile']//i[@class='icn fa fa-minus-circle']")
private WebElement  ClickedonDeletebutton ;

@FindBy(xpath="//div[@id='ConfirmationDialogue']//button[@type='button'][normalize-space()='OK']")
private WebElement  ClickedonOKbutton ;

@FindBy(xpath="//a[text()='2']")
private WebElement  Nextbtn ;

public void CreateDeleteProfile()throws InterruptedException{

	ClickonADDProfile.click();
	Reporter.log("PASS >> Clicked on ADD Profile",true);
	sleep(2);
	ProfileNameEntered.clear();
	sleep(2);
	ProfileNameEntered.sendKeys(Config.DEPProfileName);
	Reporter.log("PASS >> Profile Name Entered",true);
	sleep(2);
	Savebtn.click();
	Reporter.log("PASS >> Clicked on Save button",true);
	Reporter.log("PASS >> Profile Created Successfully",true);	
	sleep(2);
	
	SearchProfileCreated.sendKeys(Config.DEPProfileName);
	
	
	//Nextbtn.click();
	sleep(4);
	SelectCreatedProfile.click();
	sleep(2);
	ClickedonDeletebutton.click();
	Reporter.log("PASS >> Clicked on Delete button",true);
	sleep(2);
	ClickedonOKbutton.click();
	Reporter.log("PASS >> This DEP Profile was Deleted Successfully",true);
	sleep(2);	
}

public void  DEPProfileDeleteActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'DEP Profile under DEP Profiles deleted by')]");
	String DEPProfileDeletedmsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'DEP Profile under DEP Profiles deleted by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+DEPProfileDeletedmsg,true);
	sleep(2);
}

@FindBy(xpath="//a[normalize-space()='DEP Servers']")
private WebElement ClickonDEPServers ;

@FindBy(xpath="//div[@id='addServ-btn']//i[@class='icn fa fa-plus']")
private WebElement ClickonADDDEPServers ;

@FindBy(xpath="//input[@id='DEPServerName']")
private WebElement AutomationServerName ;

@FindBy(xpath="//select[@id='DepProfileTemplateList']")
private WebElement  DEPServerDropDown ;

@FindBy(xpath="//button[@id='SaveServerDepDetail']")
private WebElement  ClickonDEPADDButton ;

public void CreateDEPServers()throws InterruptedException{

	ClickonDEPServers.click();
	Reporter.log("PASS >> Clicked on DEP Servers",true);
	sleep(2);
	waitForXpathPresent("//div[@id='addServ-btn']//i[@class='icn fa fa-plus']");
	ClickonADDDEPServers.click();
	Reporter.log("PASS >> Clicked on ADD Button",true);
	sleep(2);
	AutomationServerName.sendKeys(Config.DEPServersName);
	sleep(2);
	Reporter.log("PASS >> DEP Server Name Entered",true);
	
	Select se1 = new Select(DEPServerDropDown);
    se1.selectByVisibleText("Default DEP Profile");
    String Value1=Initialization.driver.findElement(By.xpath("//option[text()='Default DEP Profile']")).getAttribute("text");
    Reporter.log("PASS >> DEP Server Drop Down text Displayed is : "+Value1,true);
    sleep(2);
	
    ClickonDEPADDButton.click();
    Reporter.log("PASS >> Server Details Added Successfully",true);
    
}

public void  DEPServersActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'DEP server Automation DEP Servers under DEP Servers created by')]");
	String DEPServersMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'DEP server Automation DEP Servers under DEP Servers created by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+DEPServersMsg,true);
	sleep(2);
}

@FindBy(xpath="//div[@class='tab-pane-body']//div[@class='tableHolder ct-noCont-wrapper']//a[contains(text(),'2')]")
private WebElement  ClickonNextButton ;

@FindBy(xpath="//td[normalize-space()='Modified DEP Server']")
private WebElement  ClickonModifiedDEPServer ;

@FindBy(xpath="//div[@id='DEPServerDetailEdit']//i[@class='icn fa fa-pencil-square-o']")
private WebElement  ClickonDEPServerEditbutton ;

@FindBy(xpath="//textarea[@id='DEPDescription']")
private WebElement DescriptionEntered ;

@FindBy(xpath="//button[@id='SaveServerDepDetail']")
private WebElement Savebutton ;

@FindBy(xpath="//div[@id='ios_mdmSett_tab']//a[contains(text(),'2')]")
private WebElement NextButton ;

@FindBy(xpath="//div[@id='ios_mdmSett_tab']//input[@placeholder='Search']")
private WebElement SearchDepServer ;


public void ModifiedDEPServer()throws InterruptedException{

	ClickonDEPServers.click();
	Reporter.log("PASS >> Clicked on DEP Servers",true);
	sleep(2);
	waitForXpathPresent("//div[@id='addServ-btn']//i[@class='icn fa fa-plus']");
	ClickonADDDEPServers.click();
	Reporter.log("PASS >> Clicked on ADD Button",true);
	sleep(2);
	AutomationServerName.sendKeys(Config.DEPServersModified);
	sleep(2);
	Reporter.log("PASS >> DEP Server Name Entered",true);
	
	Select se1 = new Select(DEPServerDropDown);
    se1.selectByVisibleText("Default DEP Profile");
    String Value1=Initialization.driver.findElement(By.xpath("//option[text()='Default DEP Profile']")).getAttribute("text");
    Reporter.log("PASS >> DEP Server Drop Down text Displayed is : "+Value1,true);
    sleep(2);
	
    ClickonDEPADDButton.click();
    Reporter.log("PASS >> Server Details Added Successfully",true);
    sleep(2);
    //NextButton.click();
    sleep(2);
    
    SearchDepServer.sendKeys(Config.DEPServersModified);
    
    
    sleep(4);
	ClickonModifiedDEPServer.click();
	Reporter.log("PASS >> Modified DEP Server Selected",true);
	sleep(2);
	ClickonDEPServerEditbutton.click();
	Reporter.log("PASS >> Clicked on EDIT Button",true);
	sleep(2);
	DescriptionEntered.clear();
	sleep(2);
	DescriptionEntered.sendKeys(Config.DEPServersDescription);
	sleep(2);
	Savebutton.click();
	Reporter.log("PASS >> Server Details Updated Successfully",true);
}

public void  ModifiedActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'DEP server Modified DEP Server under DEP Servers modified by')]");
	String DEPServersModifiedMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'DEP server Modified DEP Server under DEP Servers modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+DEPServersModifiedMsg,true);
	sleep(2);
}

@FindBy(xpath="//td[normalize-space()='Delete Automation DEP Server']")
private WebElement ClickonDeleteAutomationDEPServer ;

@FindBy(xpath="//div[@id='DEPServerDetailDelete']//i[@class='icn fa fa-minus-circle']")
private WebElement ClickedonDeleteButton ;

@FindBy(xpath="//div[@id='ConfirmationDialogue']//button[@type='button'][normalize-space()='OK']")
private WebElement ClickedonOKButton ;

public void CreateDeleteDEPServer()throws InterruptedException{

	ClickonDEPServers.click();
	Reporter.log("PASS >> Clicked on DEP Servers",true);
	sleep(2);
	waitForXpathPresent("//div[@id='addServ-btn']//i[@class='icn fa fa-plus']");
	ClickonADDDEPServers.click();
	Reporter.log("PASS >> Clicked on ADD Button",true);
	sleep(2);
	AutomationServerName.sendKeys(Config.DEPProfileName1);
	sleep(2);
	Reporter.log("PASS >> DEP Server Name Entered",true);
	
	Select se1 = new Select(DEPServerDropDown);
    se1.selectByVisibleText("Default DEP Profile");
    String Value1=Initialization.driver.findElement(By.xpath("//option[text()='Default DEP Profile']")).getAttribute("text");
    Reporter.log("PASS >> DEP Server Drop Down text Displayed is : "+Value1,true);
    sleep(2);
	
    ClickonDEPADDButton.click();
    sleep(2);
    Reporter.log("PASS >> Server Details Added Successfully",true);
   // NextButton.click();
	sleep(2);
	
	SearchDepServer.sendKeys(Config.DEPProfileName1);
	
	sleep(4);
    ClickonDeleteAutomationDEPServer.click();
    sleep(2);
    ClickedonDeleteButton.click();
    Reporter.log("PASS >> Clicked on Delete Button",true);
    sleep(2);
    ClickedonOKButton.click();
    Reporter.log("PASS >> DEP Server Details Deleted Successfully",true);   
}

public void  DEPServerDeletedActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'DEP server under DEP Servers deleted by')]");
	String DEPServersDeletedMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'DEP server under DEP Servers deleted by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+DEPServersDeletedMsg,true);
	sleep(2);
}

@FindBy(xpath="//input[@id='recovery_pin']")
private WebElement PinBar ;

@FindBy(xpath="//input[@id='miscellaneous_save']")
private WebElement SaveBtn ;

public void EnterSIXDigitPin()throws InterruptedException{
	
	PinBar.clear();
	sleep(2);   
    PinBar.sendKeys("123456");
    sleep(2);
    Reporter.log("PASS >> Six Digit Pin Entered Successfully",true);
    SaveBtn.click();   
}

 // VPP token 

@FindBy(xpath="//a[normalize-space()='VPP']")
private WebElement ClickonVPP  ;

@FindBy(xpath="//button[normalize-space()='Modify']")
private WebElement ClickonModify  ;

public void  VPP() throws InterruptedException{

	ClickonVPP.click();
	sleep(2);
	System.out.println("PASS >> Clicked on VPP");
}

public void ModifyVisible()throws InterruptedException{
	
	boolean flag=ClickonModify.isDisplayed();
	if(flag)
	{	   
		ClickonModify.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Modified",true);
	}
	else
	{	
		Reporter.log("FAIL >> Modified Not Present",true);
	}
}

public void BrowserVisible()throws InterruptedException{
	
	boolean flag=ClickonBrowse.isDisplayed();
	if(flag)
	{	   
		ClickonBrowse.click();
		sleep(5);
		Reporter.log("PASS >> Clicked on Browser",true);
	}
	else
	{			
		Reporter.log("FAIL >> Not Clicked on Browser",true);
	}
}

@FindBy(xpath="//*[@id='vppTokenUploadWrapper']/div/div[1]")
private WebElement ClickonBrowse ;

@FindBy(xpath="//button[@id='uploadVppSerToken']")
private WebElement ClickonUploadBtn ;

@FindBy(xpath="//button[normalize-space()='Delete VPP Token']")
private WebElement ClickonDeleteVPPToken ;

@FindBy(xpath="//div[@id='ConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")
private WebElement ClickOKButton ;

public void browseFile(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);
	sleep(5);
	ClickonUploadBtn.click();
	Reporter.log("PASS >> VPP Token Uploaded Successfully", true);
	sleep(5);
	ClickonDeleteVPPToken.click();
	sleep(5);
	Reporter.log("PASS >> VPP Token Deleted Successfully", true);
	ClickOKButton.click();	
}

public void  VPPTokenDeletedActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'VPP Server Token under VPP deleted by')]");
	String VPPTokenDeletedMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'VPP Server Token under VPP deleted by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+VPPTokenDeletedMsg,true);
	sleep(2);	
}

// Miscellaneous Setting under ios

@FindBy(xpath="//a[normalize-space()='Miscellaneous']")
private WebElement ClickonIOSMiscellaneous ;

public void  IOSMiscellaneous() throws InterruptedException{

	ClickonIOSMiscellaneous.click();
	sleep(2);
	Reporter.log("PASS >> Clicked on Miscellaneous Setting under IOS",true);	
}

@FindBy(xpath="//input[@id='enable_ewd']")
private WebElement ClickonEnableDeviceWipe ;

@FindBy(xpath="//input[@id='miscellaneous_save']")
private WebElement ClickonSave ;

public void EnableDeviceWipe()throws InterruptedException{
	
	boolean flag=ClickonEnableDeviceWipe.isSelected();
	if(flag)
	{	
		ClickonEnableDeviceWipe.click();
		sleep(2);
		ClickonSave.click();
		Reporter.log("PASS >> Clicked on Save",true);
		sleep(2);
		Reporter.log("PASS >> Clicked on Enable Device Wipe",true);
	}
	else
	{		
		ClickonEnableDeviceWipe.click();
		sleep(4);
		ClickonSave.click();
		Reporter.log("PASS >> Clicked on Save",true);
		sleep(4);
		Reporter.log("PASS >> Clicked on Enable Device Wipe",true);
	}
}

public void EnableDeviceWipeCheckbox()throws InterruptedException{
	
	boolean flag=ClickonEnableDeviceWipe.isSelected();
	if(flag==false)
	{	
		ClickonEnableDeviceWipe.click();
		sleep(2);		
	}	
	Reporter.log("PASS >> Clicked on Enable Device Wipe",true);
}

public void  MiscellaneousModifiedActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'Miscellaneous settings under iOS/macOS Settings modified by')]");
	String DEPServersDeletedMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Miscellaneous settings under iOS/macOS Settings modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+DEPServersDeletedMsg,true);
	sleep(2);	
}

@FindBy(xpath="//td[normalize-space()='Delete New Rule']")
private WebElement ClickonDeleteNewRule ;

@FindBy(xpath="//div[@id='groupAssignDelete']//i[@class='icn fa fa-minus-circle']")
private WebElement ClickonDeletebutton ;

@FindBy(xpath="//div[@id='ConfirmationDialogue']//button[@type='button'][normalize-space()='OK']")
private WebElement ClickedOKButton ;

public void DeleteNewRule() throws InterruptedException{
	
	SSIDDeviceModelRule.sendKeys(Config.DeleteNewRuleName);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
    
    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);
    ClickonDeleteNewRule.click();
    sleep(2);
    ClickonDeletebutton.click();
    sleep(2);
    ClickedOKButton.click();
    Reporter.log("PASS >> Rule Deleted New Rule Deleted Successfully",true);
    
}

public void GroupDeletedActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'Rule Delete New Rule deleted by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Rule Delete New Rule deleted by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

@FindBy(xpath="//td[normalize-space()='Disabled Group Rule']")
private WebElement ClickonDisabledGroup ;

@FindBy(xpath="//div[@id='groupAssignStatus']//i[@class='icn fa fa-ban']")
private WebElement ClickonDisabledbutton ;

public void DisabledGroupRule() throws InterruptedException{
	
	SSIDDeviceModelRule.sendKeys(Config.DisabledGroupRule);
	sleep(2);
    ClickonSourceGroup.click();
    sleep(2);   
    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ClickonGroup1;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	Reporter.log("PASS >> Source Group001 Selected",true);
	sleep(5); 
	ClickonGroup1.click();
    sleep(2);
    ClickonADDButtonGroup1.click();
    sleep(2);
    CheckBoxSelected.click();
    sleep(2);
    
    Select se1 = new Select(SSIDDropDownOption);
    se1.selectByVisibleText("SSID");
    String Value1=Initialization.driver.findElement(By.xpath("//option[@value='ssid']")).getAttribute("value");
    Reporter.log("PASS >> ADD Rules Drop Down text Displayed is : "+Value1,true);
    Rule1Value.sendKeys(Config.SSIDName1);
    sleep(2);
    
    ClickonDestinationGroup.click();
    sleep(5);
    JavascriptExecutor Js1=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown1 = ClickonGroup2;
	Js1.executeScript("arguments[0].scrollIntoView(true);", scrollDown1);
	Reporter.log("PASS >> Destination Group001 Selected",true);
	sleep(5); 
	ClickonGroup2.click();
	sleep(2);
	ClickonADDbuttonGroup2.click();
	sleep(2);
	ClickonSavebutton.click();
	sleep(5);
	ClickonApplybutton.click();
	sleep(5);
	Reporter.log("PASS >> Rules Added Successfully",true);	 
    Reporter.log("PASS >> Settings Updated Successfully",true);
    ClickonDisabledGroup.click();
    sleep(2); 
	ClickonDisabledbutton.click();
	sleep(5);
	Reporter.log("PASS >> Clicked on Disabled button",true);	
	ClickonApplybutton.click();
	sleep(5);
    Reporter.log("PASS >> Rule Disabled Group Rule Disabled Successfully",true);   
}

public void DisabledActivityLog() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'Enable Group Assignment Rule')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Enable Group Assignment Rule')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

// Customize tool bar

@FindBy(xpath="//input[@id='customjobname']")
private WebElement CustomizeJobName;

@FindBy(xpath="//button[@id='browseIcn_btn']")
private WebElement browseIcons;

@FindBy(xpath="//i[@class='fa fa-circle-thin']")
private WebElement iconsymbolCircle;

@FindBy(xpath="//button[@class='btn smdm_btns smdm_grn_clr addbutton awesomeIcn_selDone']")
private WebElement ClickedonDone ;

@FindBy(xpath="//div[@onclick='forcustomShowJobs()']")
private WebElement ClickonBriefCase ;

@FindBy(xpath="//p[text()='Trial details']")
private WebElement SelectTrialDetails ;

@FindBy(xpath="//button[@id='okbtn']")
private WebElement ClickedonDoneButton ;
	
@FindBy(xpath="//button[@id='savecustomdynamicjob']")
private WebElement SaveCutomizeJob ;

@FindBy(xpath="//div[@id='conmptableContainer']//input[@placeholder='Search']")
private WebElement SearchTrialdetails ;


public void ADDJob() throws InterruptedException {
	
	userdefinedAddButton.click();
	waitForXpathPresent("//input[@id='customjobname']");
	sleep(2);
	Reporter.log("PASS >> Clicked on ADD Button", true);
	CustomizeJobName.sendKeys(Config.CustomizeJobName);
	sleep(2);
	Reporter.log("PASS >> Customize Job Name Entered", true);
	browseIcons.click();
	sleep(2);
	iconsymbolCircle.click();
	sleep(2);
	Reporter.log("PASS >> Clicked on Symbol", true);
	ClickedonDone.click();
	sleep(2);
	ClickonBriefCase.click();
	sleep(4);
	
	SearchTrialdetails.clear();
	sleep(4);
	SearchTrialdetails.sendKeys("Trial details");
	
	waitForXpathPresent("//p[text()='Trial details']");
		
	SelectTrialDetails.click();
	sleep(4);
	ClickedonDoneButton.click();
	Reporter.log("PASS >> Trial Details Job Selected", true);
	SaveCutomizeJob.click();
	Reporter.log("PASS >> Added Successfully", true);
	Reporter.log("PASS >> Setting Updated Successfully", true);
	sleep(5);
}
 
public void CreatedLogs() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'under Customize Toolbar created by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'under Customize Toolbar created by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

@FindBy(xpath="//li[@title='Delete 42Test']//i[@class='ico fa fa-trash-o']")
private WebElement Delete42Gears ;

@FindBy(xpath="//div[@id='ConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")
private WebElement YesButton ;

public void Delete42Test() throws InterruptedException{
	sleep(5);
	waitForXpathPresent("//li[@title='Delete 42Test']//i[@class='ico fa fa-trash-o']");
	Delete42Gears.click();
	sleep(2);
	YesButton.click();
	sleep(2);	
}

@FindBy(xpath="//li[@title='Edit 42Test']//i[@class='ico fa fa-pencil-square-o']")
private WebElement ModifyJob ;

public void ModifyJob() throws InterruptedException{
	sleep(5);
	ModifyJob.click();
	sleep(2);
	ClickonBriefCase.click();
	
	SearchTrialdetails.clear();
	sleep(4);
	SearchTrialdetails.sendKeys("Trial details");
	waitForXpathPresent("//p[text()='Trial details']");
	sleep(2);
	SelectTrialDetails.click();
	sleep(2);
	
	ClickedonDoneButton.click();
	Reporter.log("PASS >> Trial Details Job Selected", true);
	sleep(4);
	SaveCutomizeJob.click();
	sleep(4);
	Reporter.log("PASS >> Added Successfully", true);
	Reporter.log("PASS >> Setting Updated Successfully", true);	
	sleep(5);
}

public void ModifyLogs() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'under Customize Toolbar modified by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'under Customize Toolbar modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

public void DeleteActivityLogs() throws InterruptedException{
	
	Reporter.log("PASS >> Deleted Successfully", true);
	waitForXpathPresent("//p[contains(text(),'under Customize Toolbar deleted by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'under Customize Toolbar deleted by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

// Data Analysis 

@FindBy(xpath="//span[normalize-space()='Add Analytics']")
private WebElement ADDAnalytics ;

@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[2]/div/input")
private WebElement AnalyticsName ;

@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[3]/div/input")
private WebElement TagName ;

@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[4]/div/div[2]/div")
private WebElement PluseButton ;

@FindBy(xpath="//div[@id='analytics_popup']/div/div/div[2]/div/div/input")
private WebElement FieldNameEnter ;

@FindBy(xpath="//select[@id='field_type_value']")
private WebElement FieldtypeDropDown ;

@FindBy(xpath="//div[@id='analytics_popup']/div/div/div/button[text()='Add']")
private WebElement ADDbutton ;

@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[1]/span[2]/a")
private WebElement SaveAnalyticsbutton ;

@FindBy(xpath="//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[1]/span[1]")
private WebElement RemoveAnalyticsButton;

@FindBy(xpath="//div[@id='ConfirmationDialog']//button[contains(@type,'button')][normalize-space()='Yes']")
private WebElement Yesbutton;

public void CreateAnalytics() throws InterruptedException{
	sleep(5);
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ADDAnalytics;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	ADDAnalytics.click();
	sleep(5);
	AnalyticsName.sendKeys(Config.AnalyticsNameAutomation);
	sleep(2);
	Reporter.log("PASS >> Analytics Name Entered", true);
	TagName.sendKeys(Config.TagNameTest);
	sleep(2);
	Reporter.log("PASS >> Tag Name Entered", true);
	PluseButton.click();
	sleep(2);
	
	FieldNameEnter.sendKeys("Automation");
	sleep(2);
	Reporter.log("PASS >> Field Name Entered", true);
	Select sel=new Select(FieldtypeDropDown);
	sel.selectByValue("string");
	sleep(3);
	Reporter.log("PASS >> String Selected From Drop Down", true);	
	ADDbutton.click();	
}

public void SaveAnalyticsbutton()
{
	SaveAnalyticsbutton.click();
	try
	{
		waitForLogXpathPresent("//span[text()='Analytics Settings saved successfully.']");
		Reporter.log("PASS >> Analytics Settings saved successfully Message Displayed",true);
		waitForXpathPresent("//div[@class='addanalyticsbtnSec']/following-sibling::div/div[1]/div[5]/div[2]/div/span[1]");
		sleep(5);
	}
	catch(Exception e)
	{
		String FailMessage="FAIL >> Analytics Settings saved successfully Message Isn't Displayed";
		ALib.AssertFailMethod(FailMessage);
	}
}

public void DataAnalysisCreatedActivityLogs() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'under Data Analytics created by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'under Data Analytics created by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg, true);
	sleep(2);
}

public void ModifyDataAnalytics() throws InterruptedException{
	sleep(5);
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ADDAnalytics;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	ADDAnalytics.click();
	sleep(5);
	AnalyticsName.sendKeys(Config.AnalyticsNameAutomation);
	sleep(2);
	Reporter.log("PASS >> Analytics Name Entered", true);
	TagName.sendKeys(Config.TagNameTest);
	sleep(2);
	Reporter.log("PASS >> Tag Name Entered", true);
	PluseButton.click();
	sleep(2);
	
	FieldNameEnter.sendKeys("Automation");
	sleep(2);
	Reporter.log("PASS >> Field Name Entered", true);
	Select sel=new Select(FieldtypeDropDown);
	sel.selectByValue("string");
	sleep(3);
	Reporter.log("PASS >> String Selected From Drop Down", true);	
	ADDbutton.click();
	
	SaveAnalyticsbutton.click();
	sleep(2);
	Reporter.log("PASS >> Save Button Clicked", true);
	
	PluseButton.click();
	sleep(2);
	
	FieldNameEnter.sendKeys("AutomationTest");
	sleep(2);
	Reporter.log("PASS >> Modify Field Name Entered", true);
	Select sel1=new Select(FieldtypeDropDown);
	sel1.selectByValue("string");
	sleep(3);
	Reporter.log("PASS >> Modify String Selected From Drop Down", true);	
	ADDbutton.click();	
}

public void DataAnalysisModifyActivityLogs() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'under Data Analytics modified by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'under Data Analytics modified by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

public void RemoveAnalyticsButton() throws InterruptedException{
	
	RemoveAnalyticsButton.click();
	sleep(3);
	Reporter.log("PASS >> Clicked Remove Button", true);
	Yesbutton.click();
	sleep(3);
	Reporter.log("PASS >> Analytics Settings Deleted successfully", true);
}

public void DataAnalysisDeleteActivityLogs() throws InterruptedException{
	
	waitForXpathPresent("//p[contains(text(),'under Data Analytics deleted by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'under Data Analytics deleted by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}
 
public void Office365ActivityLogs() throws InterruptedException{
	
    waitForXpathPresent("//p[contains(text(),'Office365 Settings enabled by')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Office365 Settings enabled by')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}

@FindBy(xpath="//span[normalize-space()='Alert Message Analytics']")
private WebElement AlertMessageAnalyticsVisible ;

public void AlertMessageAnalyticsVisible()throws InterruptedException{
	
	boolean flag=AlertMessageAnalyticsVisible.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Alert Message Analytics Option is Displayed",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Alert Message Analytics Option is Not Displayed");
		Reporter.log("FAIL >> Alert Message Analytics Option is Not Displayed",true);
	}
}

@FindBy(xpath="(//span[text()='Secret Key'])[4]")
private WebElement SecretKeyVisible ;

public void SecretKeyVisible()throws InterruptedException{
	
	boolean flag=SecretKeyVisible.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Secret Key is Displayed",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Secret Key is Not Displayed");
		Reporter.log("FAIL >> Secret Key is Not Displayed",true);
	}
}

@FindBy(xpath="//li[@class='switch_to_NewUI_console']//a[@class='menu-item']")
private WebElement ClickonTrynewconsole ;

public void ClickonTrynewconsole()throws InterruptedException{
	
	boolean flag=ClickonTrynewconsole.isDisplayed();
	if(flag)
	{	   
		ClickonTrynewconsole.click();
		sleep(5);
		waitForXpathPresent("//button[normalize-space()='Switch To Old Console']");
		Reporter.log("PASS >> Try New Console Option is Displayed",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Try New Console Option is Not Displayed");
		Reporter.log("FAIL >> Try New Console Option is Not Displayed",true);
	}
}

@FindBy(xpath="//button[normalize-space()='Switch To Old Console']")
private WebElement SwitchtoOldConsole ;

public void SwitchtoOldConsole()throws InterruptedException{
	
	boolean flag=SwitchtoOldConsole.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		SwitchtoOldConsole.click();
		waitForXpathPresent("//a[@id='userProfileButton']");
		Reporter.log("PASS >> Clicked on Switch to Old Console",true);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on Switch to Old Console");
		Reporter.log("FAIL >> Not Clicked on Switch to Old Console",true);	
	}
}

// User Management 

@FindBy(xpath="//button[normalize-space()='User Management']")
private WebElement ClickedonUserManagement ;

public void ClickedonUserManagement()throws InterruptedException{
	
	boolean flag=ClickedonUserManagement.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		ClickedonUserManagement.click();
		waitForXpathPresent("//a[normalize-space()='Roles']");
		Reporter.log("PASS >> Clicked on User Management",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on User Management");
		Reporter.log("FAIL >> Not Clicked on User Management",true);
	}
}

@FindBy(xpath="//td[normalize-space()='Mithilesh_Superuser']")
private WebElement SelectSuperUser ;

@FindBy(xpath="//div[@id='editUser']//i[@class='icn fa fa-pencil-square-o']")
private WebElement EditSuperUser ;

public void SelectSuperUser()throws InterruptedException{

    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = SelectSuperUser;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	SelectSuperUser.click();
	Reporter.log("PASS >> Super User Selected",true);
	
	EditSuperUser.click();
	sleep(4);	
	waitForXpathPresent("//span[normalize-space()='User Name']");
	Reporter.log("PASS >> Clicked on Edit User",true);
}

@FindBy(xpath="//select[@id='ListFeaturePermissions']")
private WebElement SuperuserisVisible ;

public void SuperuserisVisible()throws InterruptedException{
    
	String username=Initialization.driver.findElement(By.xpath("//*[@id='userid']")).getAttribute("value");
	Reporter.log("PASS >> User Name Displayed is : "+username,true);	
	Select se = new Select(SuperuserisVisible);
    se.selectByVisibleText("Super User");    
	String Role=Initialization.driver.findElement(By.xpath("//option[text()='Super User']")).getAttribute("text");
	Reporter.log("PASS >> Role Displayed is : "+Role,true);	
}

// change language 

@FindBy(xpath="//button[normalize-space()='Change Language']")
private WebElement ClickedonChangeLanguage ;

public void ChangeLanguageVisible()throws InterruptedException{
	
	boolean flag=ClickedonChangeLanguage.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		ClickedonChangeLanguage.click();
		Reporter.log("PASS >> Clicked on Change Language",true);
		sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >>  Not Clicked on Change Language");
		Reporter.log("FAIL >> Not Clicked on Change Language",true);
	}
}

@FindBy(xpath="//select[@id='Select_language']")
private WebElement SelectLanguageDropDown ;

public void ChooseYourLangauge()throws InterruptedException{
    
	Select se1 = new Select(SelectLanguageDropDown);
    se1.selectByVisibleText("English");    
	String Lang1=Initialization.driver.findElement(By.xpath("//option[text()='English']")).getAttribute("text");
	Reporter.log("PASS >> Language Option is : "+Lang1,true);
    
    Select se2 = new Select(SelectLanguageDropDown);
    se2.selectByVisibleText("Spanish(espa�ola) - Beta");    
    String Lang2=Initialization.driver.findElement(By.xpath("//option[text()='Spanish(espa�ola) - Beta']")).getAttribute("text");
    Reporter.log("PASS >> Language Option is : "+Lang2,true);
    
    Select se3 = new Select(SelectLanguageDropDown);
    se3.selectByVisibleText("German(Deutsch) - Beta");    
    String Lang3=Initialization.driver.findElement(By.xpath("//option[text()='German(Deutsch) - Beta']")).getAttribute("text");
    Reporter.log("PASS >> Language Option is : "+Lang3,true);
    
    Select se4 = new Select(SelectLanguageDropDown);
    se4.selectByVisibleText("French(le fran�ais) - Beta");    
    String Lang4=Initialization.driver.findElement(By.xpath("//option[text()='French(le fran�ais) - Beta']")).getAttribute("text");
    Reporter.log("PASS >> Language Option is : "+Lang4,true);
      
    Select se5 = new Select(SelectLanguageDropDown);
    se5.selectByVisibleText("Italian - Beta"); 
    String Lang5=Initialization.driver.findElement(By.xpath("//option[text()='Italian        - Beta']")).getAttribute("text");
    Reporter.log("PASS >> Language Option is : "+Lang5,true);    
}

@FindBy(xpath="//button[@id='okbtn']")
private WebElement ClickedonOK ;

public void GermanLanguageValidation()throws InterruptedException{

	Select se1 = new Select(SelectLanguageDropDown);
    se1.selectByVisibleText("German(Deutsch) - Beta");    
    String German=Initialization.driver.findElement(By.xpath("//option[text()='German(Deutsch) - Beta']")).getAttribute("text");
    Reporter.log("PASS >> Language Selected is : "+German,true);
    sleep(4);
    ClickedonOK.click();
    waitForXpathPresent("//a[@id='userProfileButton']");
}

@FindBy(xpath="//span[text()='Abonnement']")
private WebElement Subscription ;

@FindBy(xpath="//span[normalize-space()='Verwendete Ger�telizenz']")
private WebElement DeviceLicensesUsed ;

@FindBy(xpath="//span[normalize-space()='Verwendete Lizenz']")
private WebElement ThingsLicensesUsed ;

@FindBy(xpath="//span[normalize-space()='VR-Lizenz verwendet']")
private WebElement VRLicensesUsed ;

@FindBy(xpath="//span[normalize-space()='Sie verwenden']")
private WebElement Youareusing ;

@FindBy(xpath="//button[normalize-space()='Ger�teeintragung']")
private WebElement DeviceEnrollmentSettings ;

@FindBy(xpath="//button[normalize-space()='Kontoeinstellungen']")
private WebElement AccountSettingsLanguage ;

@FindBy(xpath="//button[normalize-space()='Sprache �ndern']")
private WebElement ChangeLanguageSettings ;

public void GermanLanguageVisible()throws InterruptedException{
	
	String option1 = Subscription.getText();
	String Expected1 = "Abonnement";
	String pass1 = "PASS >> Subscription Displayed in German is : " +option1;
	String fail1 = "FAIL >> Subscription Displayed in German Not Dispalyed";
	ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
	sleep(2); 
	
	String option2 = DeviceLicensesUsed.getText();
	String Expected2 = "Verwendete Ger�telizenz";
	String pass2 = "PASS >> Device Licenses Used Displayed in German is : " +option2;
	String fail2 = "FAIL >> Device Licenses Used Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected2, option2, pass2, fail2);	
	sleep(2); 
	
	String option3 = ThingsLicensesUsed.getText();
	String Expected3 = "Verwendete Lizenz";
	String pass3 = "PASS >> Things Licenses Used Displayed in German is : " +option3;
	String fail3 = "FAIL >> Things Licenses Used Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected3, option3, pass3, fail3);	
	sleep(2);
	
	String option4 = VRLicensesUsed.getText();
	String Expected4 = "VR-Lizenz verwendet";
	String pass4 = "PASS >> VRLicenses Used Displayed in German is : " +option4;
	String fail4 = "FAIL >> VRLicenses Used Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected4, option4, pass4, fail4);	
	sleep(2);
	
	String option5 = Youareusing.getText();
	String Expected5 = "Sie verwenden";
	String pass5 = "PASS >> You are using Displayed in German is : " +option5;
	String fail5 = "FAIL >> You are using Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected5, option5, pass5, fail5);	
	sleep(2);
	
	String option6 = DeviceEnrollmentSettings.getText();
	String Expected6 = "Ger�teeintragung";
	String pass6 = "PASS >> Device Enrollment Displayed in German is : " +option6;
	String fail6 = "FAIL >> Device Enrollment Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected6, option6, pass6, fail6);	
	sleep(2);
	
	String option7 = AccountSettingsLanguage.getText();
	String Expected7 = "Kontoeinstellungen";
	String pass7 = "PASS >> Account Settings Displayed in German is : " +option7;
	String fail7 = "FAIL >> Account Settings Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected7, option7, pass7, fail7);	
	sleep(2);
	
	String option8 = ChangeLanguageSettings.getText();
	String Expected8 = "Sprache �ndern";
	String pass8 = "PASS >> Change Language Displayed in German is : " +option8;
	String fail8 = "FAIL >> Change Language Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected8, option8, pass8, fail8);	
	sleep(2);
}

@FindBy(xpath="//button[normalize-space()='Sprache �ndern']")
private WebElement GermantoEnglish ;

public void GermantoEnglish()throws InterruptedException{
	
	boolean flag=GermantoEnglish.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		GermantoEnglish.click();
		Select se1 = new Select(SelectLanguageDropDown);
	    se1.selectByVisibleText("English");    
	    sleep(4);
	    ClickedonOK.click();
	    waitForXpathPresent("//a[@id='userProfileButton']");
	}
	else
	{   
		ALib.AssertFailMethod("FAIL >> Language Not Changed to English");
		Reporter.log("FAIL>> Language Not Changed to English",true);
	}
}

//French(le fran�ais) - Beta

public void FrenchLanguageValidation()throws InterruptedException{

	Select se1 = new Select(SelectLanguageDropDown);
    se1.selectByVisibleText("French(le fran�ais) - Beta");    
    String French=Initialization.driver.findElement(By.xpath("//option[text()='French(le fran�ais) - Beta']")).getAttribute("text");
    Reporter.log("PASS>> Language Selected is : "+French,true);
    sleep(4);
    ClickedonOK.click();
    waitForXpathPresent("//a[@id='userProfileButton']");

}

public void SpanishLanguageValidation()throws InterruptedException{

	Select se1 = new Select(SelectLanguageDropDown);
    se1.selectByVisibleText("Spanish(espa�ola) - Beta");    
    String Spanish=Initialization.driver.findElement(By.xpath("//option[text()='Spanish(espa�ola) - Beta']")).getAttribute("text");
    Reporter.log("PASS>> Language Selected is : "+Spanish,true);
    sleep(4);
    ClickedonOK.click();
    waitForXpathPresent("//a[@id='userProfileButton']");

}

@FindBy(xpath="//span[text()='Abonnement']")
private WebElement subscriptionFrench ;

@FindBy(xpath="//span[normalize-space()='Objets utilis�s']")
private WebElement ThingsLicensesUsedFrench ;

@FindBy(xpath="//span[normalize-space()='Licence VR utilis�e']")
private WebElement VRLicensesUsedFrench ;

@FindBy(xpath="//span[normalize-space()='Vous Utilisez']")
private WebElement YouareusingFrench ;

@FindBy(xpath="//button[normalize-space()='Inscription du dispositif']")
private WebElement DeviceEnrollmentFrench ;

@FindBy(xpath="//button[normalize-space()='R�glages du compte']")
private WebElement AccountSettingsFrench ;

@FindBy(xpath="//button[normalize-space()='Gestion des utilisateurs']")
private WebElement UserManagementFrench ;

@FindBy(xpath="//button[normalize-space()='Modifier la langue']")
private WebElement ChangeLanguageFrench ;

public void FrenchLanguageVisible()throws InterruptedException{
	
	String option1 = subscriptionFrench.getText();
	String Expected1 = "Abonnement";
	String pass1 = "PASS >> Subscription Displayed in French is : " +option1;
	String fail1 = "FAIL >> Subscription Displayed in French Not Dispalyed";
	ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
	sleep(2); 
	
	String option2 = ThingsLicensesUsedFrench.getText();
	String Expected2 = "Objets utilis�s";
	String pass2 = "PASS >> Device Licenses Used Displayed in French is : " +option2;
	String fail2 = "FAIL >> Device Licenses Used Displayed in French is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected2, option2, pass2, fail2);	
	sleep(2); 
	
	String option3 = VRLicensesUsedFrench.getText();
	String Expected3 = "Licence VR utilis�e";
	String pass3 = "PASS >> VRLicenses Used Displayed in French is : " +option3;
	String fail3 = "FAIL >> VRLicenses Used Displayed in French is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected3, option3, pass3, fail3);	
	sleep(2);
	
	String option4 = YouareusingFrench.getText();
	String Expected4 = "Vous Utilisez";
	String pass4 = "PASS >> You are using Displayed in French is : " +option4;
	String fail4 = "FAIL >> You are using Displayed in French is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected4, option4, pass4, fail4);	
	sleep(2);
	
	String option6 = AccountSettingsFrench.getText();
	String Expected6 = "R�glages du compte";
	String pass6 = "PASS >> Account Settings Displayed in French is : " +option6;
	String fail6 = "FAIL >> Account Settings Displayed in French is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected6, option6, pass6, fail6);	
	sleep(2);
	
	String option7 = UserManagementFrench.getText();
	String Expected7 = "Gestion des utilisateurs";
	String pass7 = "PASS >> User Management Displayed in French is : " +option7;
	String fail7 = "FAIL >> User Management Displayed in French is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected7, option7, pass7, fail7);	
	sleep(2);
	
	String option8 = ChangeLanguageFrench.getText();
	String Expected8 = "Modifier la langue";
	String pass8 = "PASS >> Change Language Displayed in French is : " +option8;
	String fail8 = "FAIL >> Change Language Displayed in French is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected8, option8, pass8, fail8);	
	sleep(2);
}

@FindBy(xpath="//button[normalize-space()='Modifier la langue']")
private WebElement FrenchtoEnglish ;

public void FrenchtoEnglish()throws InterruptedException{
	
	boolean flag=FrenchtoEnglish.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		FrenchtoEnglish.click();
		Select se1 = new Select(SelectLanguageDropDown);
	    se1.selectByVisibleText("English");    
	    sleep(4);
	    ClickedonOK.click();
	    waitForXpathPresent("//a[@id='userProfileButton']");
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Language Not Changed to English");
		Reporter.log("FAIL>> Language Not Changed to English",true);
	}
}

@FindBy(xpath="//button[normalize-space()='D�connexion']")
private WebElement LogoutSessionFrench ;

public void LogoutSessionFrench()throws InterruptedException{
	
	boolean flag=LogoutSessionFrench.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		LogoutSessionFrench.click(); 
		Reporter.log("PASS >> Logout Successfully",true);
	    sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Logout is Not Successfully");
		Reporter.log("FAIL>> Logout is Not Successfully",true);
	}
}

@FindBy(xpath="//button[normalize-space()='Ausloggen']")
private WebElement LogoutSessionGerman ;

public void LogoutSessionGerman()throws InterruptedException{
	
	boolean flag=LogoutSessionGerman.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		LogoutSessionGerman.click(); 
		Reporter.log("PASS >> Logout Successfully",true);
	    sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Logout is Not Successfully");
		Reporter.log("FAIL>> Logout is Not Successfully",true);
	}
}

@FindBy(id="uName")
private WebElement userNAmeEdt;

@FindBy(id="pass")
private WebElement passwordEdt;

@FindBy(id="loginBtn")
private WebElement loginBtn;

public void loginBack() throws InterruptedException{
		
	userNAmeEdt.sendKeys(Config.userID);
	passwordEdt.sendKeys(Config.password);
	loginBtn.click();
	waitForidPresent("deleteDeviceBtn");
	sleep(5);
	Reporter.log("PASS >> Logged into SureMDM successfully",true);
}

public void FrenchVisible()throws InterruptedException{

	sleep(2);
	String Home=Initialization.driver.findElement(By.xpath("//a[normalize-space()='Accueil']")).getText();
	Reporter.log("PASS >> Home Displayed in French is : "+Home,true);
    
    String Enrollment=Initialization.driver.findElement(By.xpath("//a[contains(text(),'Inscription')]")).getText();
    Reporter.log("PASS >> Enrollment Displayed in French is : "+Enrollment,true);
}

@FindBy(xpath="//a[normalize-space()='Startseite']")
private WebElement HomeGerman ;

@FindBy(xpath="//a[contains(text(),'Eintragung')]")
private WebElement EnrollmentGerman ;

public void GremanVisible()throws InterruptedException{

	sleep(2);
	String option1 = HomeGerman.getText();
	String Expected1= "Startseite";
	String pass1 = "PASS >> Home Displayed in German is : " +option1;
	String fail1= "FAIL >> Home Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
	sleep(2);
	String option2= EnrollmentGerman.getText();
	String Expected2= "Eintragung";
	String pass2 = "PASS >> Enrollment Displayed in German is : " +option2;
	String fail2= "FAIL >> Enrollment Displayed in German is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected2, option2, pass2, fail2);	
	
}

public void VRLicensesUsed()throws InterruptedException{

	sleep(5);
	String VR=Initialization.driver.findElement(By.xpath("//span[@id='vrLicenseUsed']")).getText();
    Reporter.log("PASS >> VR Licenses Used For Trail Account is : "+VR,true);
}

public void Thingsdevicesused()throws InterruptedException{

	sleep(5);
	String VR=Initialization.driver.findElement(By.xpath("//span[@id='thingsdevicesused']")).getText();
    Reporter.log("PASS >> Things Devices used For Trail Account is : "+VR,true);
}


public void Closewindow()throws InterruptedException{
	
	waitForXpathPresent("//a[@id='userProfileButton']");
	sleep(2);
	Initialization.driver.findElement(By.xpath("//*[@id='boardingStep1']/div/div/div[1]/a")).click();
	Reporter.log("PASS >> Window Closed",true);
}

@FindBy(xpath="//div[@id='changeLanguage_Popup']//button[@aria-label='Close']")
private WebElement CloseChooseyourlanguage ;

public void CloseChooseyourlanguage()throws InterruptedException{
	
	boolean flag=CloseChooseyourlanguage.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		CloseChooseyourlanguage.click();
		sleep(2);
	    waitForXpathPresent("//a[@id='userProfileButton']");
	    Reporter.log("PASS >> Choose your language window closed",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Language Not Changed to English");
		Reporter.log("FAIL >> Choose your language window not closed",true);
	}
}

//spanish 

@FindBy(xpath="//span[normalize-space()='Suscripci�n']")
private WebElement subscriptionSpanish ;

@FindBy(xpath="//span[normalize-space()='Licencia de dispositivo utilizada']")
private WebElement DeviceLicensesUsedSpanish ;

@FindBy(xpath="//span[normalize-space()='Licencia VR utilizada']")
private WebElement VRLicensesUsedSpanish ;

@FindBy(xpath="//span[normalize-space()='Est�s usando']")
private WebElement YouareusingSpanish ;

@FindBy(xpath="//button[normalize-space()='Configuraci�n de la Cuenta']")
private WebElement AccountSettingsSpanish ;

@FindBy(xpath="//button[normalize-space()='Gesti�n de usuarios']")
private WebElement UserManagementSpanish ;

@FindBy(xpath="//button[normalize-space()='Cambiar Idioma']")
private WebElement ChangeLanguageSpanish ;

public void SpanishLanguageVisible()throws InterruptedException{
	
	String option1 = subscriptionSpanish.getText();
	String Expected1 = "Suscripci�n";
	String pass1 = "PASS >> Subscription Displayed in Spanish is : " +option1;
	String fail1 = "FAIL >> Subscription Displayed in Spanish Not Dispalyed";
	ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
	sleep(2); 
	
	String option2 = DeviceLicensesUsedSpanish.getText();
	String Expected2 = "Licencia de dispositivo utilizada";
	String pass2 = "PASS >> Device Licenses Used Displayed in Spanish is : " +option2;
	String fail2 = "FAIL >> Device Licenses Used Displayed in Spanish is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected2, option2, pass2, fail2);	
	sleep(2); 
	
	String option3 = VRLicensesUsedSpanish.getText();
	String Expected3 = "Licencia VR utilizada";
	String pass3 = "PASS >> VRLicenses Used Displayed in Spanish is : " +option3;
	String fail3 = "FAIL >> VRLicenses Used Displayed in Spanish is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected3, option3, pass3, fail3);	
	sleep(2);
	
	String option4 = YouareusingSpanish.getText();
	String Expected4 = "Est�s usando";
	String pass4 = "PASS >> You are using Displayed in Spanish is : " +option4;
	String fail4 = "FAIL >> You are using Displayed in Spanish is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected4, option4, pass4, fail4);	
	sleep(2);
	
	String option6 = AccountSettingsSpanish.getText();
	String Expected6 = "Configuraci�n de la Cuenta";
	String pass6 = "PASS >> Account Settings Displayed in Spanish is : " +option6;
	String fail6 = "FAIL >> Account Settings Displayed in Spanish is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected6, option6, pass6, fail6);	
	sleep(2);
	
	String option7 = UserManagementSpanish.getText();
	String Expected7 = "Gesti�n de usuarios";
	String pass7 = "PASS >> User Management Displayed in Spanish is : " +option7;
	String fail7 = "FAIL >> User Management Displayed in Spanish is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected7, option7, pass7, fail7);	
	sleep(2);
	
	String option8 = ChangeLanguageSpanish.getText();
	String Expected8 = "Cambiar Idioma";
	String pass8 = "PASS >> Change Language Displayed in Spanish is : " +option8;
	String fail8 = "FAIL >> Change Language Displayed in Spanish is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected8, option8, pass8, fail8);	
	sleep(2);
}

@FindBy(xpath="//button[normalize-space()='Cambiar Idioma']")
private WebElement SpanishtoEnglish ;

public void SpanishtoEnglish()throws InterruptedException{
	
	boolean flag=SpanishtoEnglish.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		SpanishtoEnglish.click();
		Select se1 = new Select(SelectLanguageDropDown);
	    se1.selectByVisibleText("English");    
	    sleep(4);
	    ClickedonOK.click();
	    waitForXpathPresent("//a[@id='userProfileButton']");
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Language Not Changed to English");
		Reporter.log("FAIL>> Language Not Changed to English",true);
	}
}

// Italian 

public void ItalianLanguageValidation()throws InterruptedException{

	Select se1 = new Select(SelectLanguageDropDown);
    se1.selectByVisibleText("Italian - Beta");    
    String Italian=Initialization.driver.findElement(By.xpath("//option[text()='Italian        - Beta']")).getAttribute("text");
    Reporter.log("PASS>> Language Selected is : "+Italian,true);
    sleep(4);
    ClickedonOK.click();
    waitForXpathPresent("//a[@id='userProfileButton']");
}

@FindBy(xpath="//span[normalize-space()='Sottoscrizione']")
private WebElement subscriptionItalian ;

@FindBy(xpath="//span[normalize-space()='Licenze dispositivo utilizzate']")
private WebElement DeviceLicensesUsedItalian ;

@FindBy(xpath="//span[normalize-space()='Licenze VR utilizzate']")
private WebElement VRLicensesUsedItalian ;

@FindBy(xpath="//span[normalize-space()='Stai utilizzando']")
private WebElement YouareusingItalian ;

@FindBy(xpath="//button[normalize-space()='Disconnetti']")
private WebElement LogoutSettingsItalian ;

@FindBy(xpath="//button[normalize-space()='Aiuto']")
private WebElement HelpItalian ;

@FindBy(xpath="//button[normalize-space()='Cambia lingua']")
private WebElement ChangeLanguageItalian ;

public void ItalianLanguageVisible()throws InterruptedException{
	
	sleep(2); 
	String option1 = subscriptionItalian.getText();
	String Expected1 = "Sottoscrizione";
	String pass1 = "PASS >> Subscription Displayed in Italian is : " +option1;
	String fail1 = "FAIL >> Subscription Displayed in Italian Not Dispalyed";
	ALib.AssertEqualsMethod(Expected1, option1, pass1, fail1);	
	sleep(2); 
	
	String option2 = DeviceLicensesUsedItalian.getText();
	String Expected2 = "Licenze dispositivo utilizzate";
	String pass2 = "PASS >> Device Licenses Used Displayed in Italian is : " +option2;
	String fail2 = "FAIL >> Device Licenses Used Displayed in Italian is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected2, option2, pass2, fail2);	
	sleep(2); 
	
	String option3 = VRLicensesUsedItalian.getText();
	String Expected3 = "Licenze VR utilizzate";
	String pass3 = "PASS >> VRLicenses Used Displayed in Italian is : " +option3;
	String fail3 = "FAIL >> VRLicenses Used Displayed in Italian is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected3, option3, pass3, fail3);	
	sleep(2);
	
	String option4 = YouareusingItalian.getText();
	String Expected4 = "Stai utilizzando";
	String pass4 = "PASS >> You are using Displayed in Italian is : " +option4;
	String fail4 = "FAIL >> You are using Displayed in Italian is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected4, option4, pass4, fail4);	
	sleep(2);
	
	String option6 = LogoutSettingsItalian.getText();
	String Expected6 = "Disconnetti";
	String pass6 = "PASS >> Logout Displayed in Italian is : " +option6;
	String fail6 = "FAIL >> Logout Displayed in Italian is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected6, option6, pass6, fail6);	
	sleep(2);
	
	String option7 = HelpItalian.getText();
	String Expected7 = "Aiuto";
	String pass7 = "PASS >> Help Displayed in Italian is : " +option7;
	String fail7 = "FAIL >> Help Displayed in Italian is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected7, option7, pass7, fail7);	
	sleep(2);
	
	String option8 = ChangeLanguageItalian.getText();
	String Expected8 = "Cambia lingua";
	String pass8 = "PASS >> Change Language Displayed in Italian is : " +option8;
	String fail8 = "FAIL >> Change Language Displayed in Italian is Not Dispalyed";
	ALib.AssertEqualsMethod(Expected8, option8, pass8, fail8);	
	sleep(2);
}

@FindBy(xpath="//button[normalize-space()='Cambia lingua']")
private WebElement ItaliantoEnglish ;

public void ItaliantoEnglish()throws InterruptedException{
	
	boolean flag=ItaliantoEnglish.isDisplayed();
	if(flag)
	{	   
		sleep(2);
		ItaliantoEnglish.click();
		Select se1 = new Select(SelectLanguageDropDown);
	    se1.selectByVisibleText("English");    
	    sleep(4);
	    ClickedonOK.click();
	    waitForXpathPresent("//a[@id='userProfileButton']");
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Language Not Changed to English");
		Reporter.log("FAIL>> Language Not Changed to English",true);
	}
}

// certificate management 

@FindBy(xpath="//div[@id='RevokeSelectedCertificates']//i[@class='icn fa fa-minus-circle']")
private WebElement RevokeSelectCertificate ;

public void RevokeSelectCertificate()throws InterruptedException{
	
	boolean flag=RevokeSelectCertificate.isDisplayed();
	if(flag)
	{	   
		 sleep(2);
		 RevokeSelectCertificate.click();
		 Reporter.log("PASS >> Clicked on Revoke Button Without Selecting Device",true);
		 waitForXpathPresent("//span[normalize-space()='Please select a device from the list.']");		
		 String RevokeSelect=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Please select a device from the list.']")).getText();
		 Reporter.log("PASS >> Error Message Displyed is : "+RevokeSelect,true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Error Message Displyed is Not Displayed");
		Reporter.log("FAIL >> Error Message Displyed is Not Displayed",true);
	}
}

@FindBy(xpath="//i[@class='icn fa fa-plus-circle']")
private WebElement RenewSelectCertificate ;

public void RenewSelectCertificate()throws InterruptedException{
	
	boolean flag=RenewSelectCertificate.isDisplayed();
	if(flag)
	{	   
		 sleep(2);
		 RenewSelectCertificate.click();
		 Reporter.log("PASS >> Clicked on Renew Button Without Selecting Device",true);
		 waitForXpathPresent("//span[normalize-space()='Please select a device from the list.']");		
		 String RenewSelect=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Please select a device from the list.']")).getText();
		 Reporter.log("PASS >> Error Message Displyed is : "+RenewSelect,true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Error Message Displyed is Not Displayed");
		Reporter.log("FAIL >> Error Message Displyed is Not Displayed",true);
	}
}

// manage subcription 

@FindBy(xpath="//button[normalize-space()='Buy Subscription']")
private WebElement BuySubscription ;

public void BuySubscription()throws InterruptedException{
	
	boolean flag=BuySubscription.isDisplayed();
	if(flag)
	{	   
		 sleep(2);
		 BuySubscription.click();
		 Reporter.log("PASS >> Clicked on Buy Subscription",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on Buy Subscription");
		Reporter.log("FAIL >> Not Clicked on Buy Subscription",true);
	}
}

@FindBy(xpath="//button[@class='closeBtn sc-close-subscrPlan']//span[@aria-hidden='true'][normalize-space()='�']")
private WebElement CloseSubscriptionWindow ;

@FindBy(xpath="//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")
private WebElement YesSubscriptionWindow ;

public void CloseSubscriptionWindow()throws InterruptedException{
	sleep(4);
	CloseSubscriptionWindow.click();
	Reporter.log("PASS >> Buy Subscription Tab Closed",true);
	sleep(2);
	YesSubscriptionWindow.click();
	sleep(4);	
}

@FindBy(xpath="//*[@id='subscr_planCard_holder']/div[1]/div[2]/div/ul/li[37]/span")
private WebElement IntelActiveManagementTechnologyVisible ;

public void IntelActiveManagementTechnologyVisible()throws InterruptedException{

     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = IntelActiveManagementTechnologyVisible ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	 sleep(4);
	 String RenewSelect=Initialization.driver.findElement(By.xpath("//*[@id='subscr_planCard_holder']/div[1]/div[2]/div/ul/li[37]/span")).getText();
	 Reporter.log("PASS >> Option Displayed is : "+RenewSelect,true);
	 sleep(4);
}

@FindBy(xpath="//*[@id='card_basic']/div[2]/div/ul/li[37]/span[2]")
private WebElement IntelAMTForStandardVisible ;

public void IntelAMTForStandardVisible()throws InterruptedException{

     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = IntelActiveManagementTechnologyVisible ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	 sleep(4);
	 String Standard=Initialization.driver.findElement(By.xpath("//*[@id='card_basic']/div[2]/div/ul/li[37]/span[2]")).getText();
	 Reporter.log("PASS >> Intel Active Management Technology For Standard is : "+Standard,true);
	 sleep(4);
}

@FindBy(xpath="//*[@id='card_premium']/div[2]/div/ul/li[37]/span[2]")
private WebElement IntelAMTForPremiumVisible ;

public void IntelAMTForPremiumVisible()throws InterruptedException{

     JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	 WebElement scrollDown = IntelActiveManagementTechnologyVisible ;
	 Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	 sleep(4);
	 String Premium=Initialization.driver.findElement(By.xpath("//*[@id='card_premium']/div[2]/div/ul/li[37]/span[2]")).getText();
	 Reporter.log("PASS >> Intel Active Management Technology For Premium is : "+Premium,true);
	 sleep(4);
}

@FindBy(xpath="//*[@id='card_Enterprise']/div[2]/div/ul/li[37]/span[2]")
private WebElement IntelAMTForEnterpriseVisible ;

public void IntelAMTForEnterpriseVisible()throws InterruptedException{

    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = IntelActiveManagementTechnologyVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	String Enterprise=Initialization.driver.findElement(By.xpath("//*[@id='card_Enterprise']/div[2]/div/ul/li[37]/span[2]")).getText();
	 Reporter.log("PASS >> Intel Active Management Technology For Enterprise is : "+Enterprise,true);
	 sleep(4);
}

////Alert Templates


@FindBy(xpath="//p[normalize-space()='Battery Policy']")
private WebElement BatteryPolicyVisible ;

public void BatteryPolicyVisible()throws InterruptedException{
	
	boolean flag=BatteryPolicyVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Battery Policy is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Battery Policy is Not Displayed");
		Reporter.log("FAIL >> Battery Policy is Not Displayed",true);
	}
}

@FindBy(xpath="//p[normalize-space()='Connection Policy']")
private WebElement ConnectionPolicyVisible ;

public void ConnectionPolicyVisible()throws InterruptedException{
	
	boolean flag=ConnectionPolicyVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Connection Policy is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Connection Policy is Not Displayed");
		Reporter.log("FAIL >> Connection Policy is Not Displayed",true);
	}
}

@FindBy(xpath="//p[normalize-space()='Data Usage Policy']")
private WebElement DataUsagePolicyVisible ;

public void DataUsagePolicyVisible()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = DataUsagePolicyVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	boolean flag=DataUsagePolicyVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Data Usage Policy is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Data Usage Policy is Not Displayed");
		Reporter.log("FAIL >> Data Usage Policy is Not Displayed",true);
	}
}

@FindBy(xpath="//p[normalize-space()='Notify when device comes online']")
private WebElement NotifywhendevicecomesonlineVisible ;

public void NotifywhendevicecomesonlineVisible()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = NotifywhendevicecomesonlineVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	boolean flag=NotifywhendevicecomesonlineVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Notify when device comes on line is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Notify when device comes on line is Not Displayed");
		Reporter.log("FAIL >> Notify when device comes on line is Not Displayed",true);
	}
}

@FindBy(xpath="//p[normalize-space()='Notify when SIM is changed']")
private WebElement NotifywhenSIMischangedVisible ;

public void NotifywhenSIMischangedVisible()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = NotifywhenSIMischangedVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	boolean flag=NotifywhenSIMischangedVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Notify when SIM is changed is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Notify when SIM is changed is Not Displayed");
		Reporter.log("FAIL >> Notify when SIM is changed is Not Displayed",true);
	}
}

@FindBy(xpath="//p[contains(text(),'Notify when device is rooted or SureMDM Agent has been granted with root permission')]")
private WebElement NotifywhendeviceisrootedorNixhasbeengrantVisible ;

public void NotifywhendeviceisrootedorNixhasbeengrantVisible()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = NotifywhendeviceisrootedorNixhasbeengrantVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	boolean flag=NotifywhendeviceisrootedorNixhasbeengrantVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Notify when device is rooted or Nix has been grant is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Notify when device is rooted or Nix has been grant is Not Displayed");
		Reporter.log("FAIL >> Notify when device is rooted or Nix has been grant is Not Displayed",true);
	}
}


@FindBy(xpath="//p[normalize-space()='Invite Users.']")
private WebElement InviteUsersVisible ;

public void InviteUsersVisible()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = InviteUsersVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	boolean flag=InviteUsersVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Invite Users is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Invite Users is Not Displayed");
		Reporter.log("FAIL >> Invite Users is Not Displayed",true);
	}
}

@FindBy(xpath="//p[normalize-space()='Schedule Report']")
private WebElement ScheduleReportVisible ;

public void ScheduleReportVisible()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ScheduleReportVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
	
	boolean flag=ScheduleReportVisible.isDisplayed();
	if(flag)
	{	   
		 Reporter.log("PASS >> Schedule Report is Displayed",true);
		 sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Schedule Report is Not Displayed");
		Reporter.log("FAIL >> Schedule Report is Not Displayed",true);
	}
}

@FindBy(xpath="//textarea[@id='EnableBatteryPolicyTemp']")
private WebElement EditTextBatteryPolicy ;

@FindBy(xpath="//input[@id='alerttemplatefornotipolicy']")
private WebElement SaveAlertTemplate ;

public void EditTextBatteryPolicy()throws InterruptedException{
	
	boolean flag=EditTextBatteryPolicy.isDisplayed();
	if(flag)
	{	   
		 EditTextBatteryPolicy.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextBatteryPolicy.sendKeys(Config.BatteryPolicyText);
		 Reporter.log("PASS >> Battery Policy Template Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);
		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}


public void CustomizeTextBatteryPolicy()throws InterruptedException{
	
	boolean flag=EditTextBatteryPolicy.isDisplayed();
	if(flag)
	{	   
		 EditTextBatteryPolicy.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextBatteryPolicy.sendKeys(Config.CustomizeBatteryPolicyText);
		 Reporter.log("PASS >> Battery Policy Template Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);
		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

@FindBy(xpath="//span[@title='Notification Policy']")
private WebElement NotificationPolicy ;

public void ClickonNotificationPolicy()throws InterruptedException{
	
	boolean flag=NotificationPolicy.isDisplayed();
	if(flag)
	{	   
		sleep(4);
		NotificationPolicy.click();
		Reporter.log("PASS >> Notification Policy is Displayed",true);
		sleep(4);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Notification Policy is Not Displayed");
		Reporter.log("FAIL >> Notification Policy is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='job_notify_name_input']")
private WebElement  EnterJobName ;

@FindBy(xpath="//input[@id='job_ip1_input']")
private WebElement  ClickonEnableBatteryPolicy ;

@FindBy(xpath="//input[@id='job_ip1_input']")
private WebElement  ClickonRollerSlider ;

public void NotificationPolicyDetails()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.AlertTemplateBatteryPolicyName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	ClickonEnableBatteryPolicy.click();
	Reporter.log("PASS >> Enable Battery Policy Selected",true);
	sleep(4);
	WebElement slider = Initialization.driver.findElement(By.xpath("//div[@class='slider-handle min-slider-handle round']"));
    Actions act = new Actions(Initialization.driver);
    act.dragAndDropBy(slider, 104, 0).build().perform();
    Reporter.log("PASS >> Notify when battery is below 30 Percentage",true);
	
}


@FindBy(xpath="//input[@id='job_ip6_input']")
private WebElement  SendAlertToMDM ;

public void SendAlertToMDM()throws InterruptedException{
	
	sleep(4);
	SendAlertToMDM.click();
	Reporter.log("PASS >> Send Alert to MDM ",true);		
}

@FindBy(xpath="//input[@id='job_ip7_input']")
private WebElement  SendAlertToDevice ;

public void SendAlertToDevice()throws InterruptedException{
	
	sleep(4);
	SendAlertToDevice.click();
	Reporter.log("PASS >> Send Alert to Device ",true);		
}

@FindBy(xpath="//input[@id='job_ip8_input']")
private WebElement  SendAlertToEmail ;

@FindBy(xpath="//input[@id='job_ip9_input']")
private WebElement  EmailAddressEntered ;

@FindBy(xpath="//button[@onclick='EnterEmail()']")
private WebElement  EnterEmailOKButton ;

@FindBy(xpath="//button[@id='okbtn']")
private WebElement  NotificationPolicyOKButton ;

public void SendAlertToEmail()throws InterruptedException{
	
	 sleep(4);
	 SendAlertToEmail.click();
	 Reporter.log("PASS >> Send Alert to Email ",true);
	 sleep(4);
	 EmailAddressEntered.sendKeys("42test2017@gmail.com");
	 Reporter.log("PASS >> Email Address Entered",true);
	 sleep(4);
	 EnterEmailOKButton.click();
	 sleep(4);
	 NotificationPolicyOKButton.click();
	 waitForXpathPresent("//span[normalize-space()='Job created successfully.']");		
	 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Job created successfully.']")).getText();
	 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
	 sleep(4);
}

@FindBy(xpath="//div[@id='tableContainer']//input[@placeholder='Search']")
private WebElement  ApplyJobonDevice ;

@FindBy(xpath="//div[@id='applyJobButton']//i[@class='icn fa fa-check-square']")
private WebElement  ClickonApply ;

@FindBy(xpath="//p[normalize-space()='Alert Template Battery Policy']")
private WebElement  SelectonBatteryPolicy ;

@FindBy(xpath="//button[@id='applyJob_okbtn']")
private WebElement  ApplyJobOKButton ;

public void ApplyJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.sendKeys("AutomationDevice_Lenovo");
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);

	SelectonBatteryPolicy.click();
	sleep(4);
	Reporter.log("PASS >> Alert Template Battery Policy Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

////

@FindBy(xpath = "//*[@id='tableContainer']/div[4]/div[1]/div[3]/input")
private WebElement SearchDeviceTextField;

public void SearchDeviceInconsole(String Device)
		throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {

	SearchDeviceTextField.clear();
	sleep(3);
	SearchDeviceTextField.sendKeys(Device);
	waitForXpathPresent("//p[text()='" + Device + "']");
	sleep(5);
}

@FindBy(xpath = ".//*[@id='applyJob_okbtn']")
private WebElement OkButtonApplyJob;

@FindBy(xpath = "//*[@id='applyJob_table_cover']/div[1]/div[1]/div[2]/input")
private WebElement SearchTextBoxApplyJob;

public void SearchField(String JobName) throws InterruptedException {
	SearchTextBoxApplyJob.sendKeys(JobName);
	sleep(4);
	waitForXpathPresent("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']");
	sleep(6);
	WebElement JobSelected = Initialization.driver
			.findElement(By.xpath("//table[@id='applyJobDataGrid']/tbody/tr[1]/td[2]/p[text()='" + JobName + "']"));
	JobSelected.click();
	sleep(2);
	OkButtonApplyJob.click();
	sleep(2);
}

///////










public void JobApplyActivityLogs() throws InterruptedException{
	
    waitForXpathPresent("//p[contains(text(),'has applied a job named')]");
	String LogMsg=Initialization.driver.findElement(By.xpath("//p[contains(text(),'has applied a job named')]")).getText();		
	Reporter.log("PASS >> Activity Log Message Displayed is : "+LogMsg,true);
	sleep(2);
}


@FindBy(xpath="//div[@id='job_delete']//i[@class='icn fa fa-minus-circle']")
private WebElement  DeleteButtonClick ;

@FindBy(xpath="//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")
private WebElement  DeleteOKButtonClick ;


@FindBy(xpath="(//p[normalize-space()='Alert Template Battery Policy'])[2]")
private WebElement  SelecttoDeleteAlertJob ;
		
		
public void SelectBatteryJob()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteAlertJob.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

//customize Alert Battery Policy

public void NotificationPolicyCustomizeDetails()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.CustomizeAlertTemplateBatteryPolicyName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	ClickonEnableBatteryPolicy.click();
	Reporter.log("PASS >> Enable Battery Policy Selected",true);
	sleep(4);
	WebElement slider = Initialization.driver.findElement(By.xpath("//div[@class='slider-handle min-slider-handle round']"));
    Actions act = new Actions(Initialization.driver);
    act.dragAndDropBy(slider, 104, 0).build().perform();
    Reporter.log("PASS >> Notify when battery is below 30 Percentage",true);
	
}

@FindBy(xpath="//p[@class='st-scrollTxt-left'][normalize-space()='Customize Alert Template Battery Policy']")
private WebElement  SelectCustomizeAlertTemplateBatteryPolicy ;


public void ApplyJobonDee()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);
	ApplyJobonDevice.sendKeys(Config.BatteryPolicyDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);

	SelectCustomizeAlertTemplateBatteryPolicy.click();
	sleep(4);
	Reporter.log("PASS >> Customize Alert Template Battery Policy Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}


@FindBy(xpath="(//p[normalize-space()='Customize Alert Template Battery Policy'])[2]")
private WebElement  SelecttoDeleteCustomizeAlertJob ;
		
		
public void SelectCustomizeBatteryJob()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteCustomizeAlertJob.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

@FindBy(xpath="//p[normalize-space()='Customize Alert Template Battery Policy']")
private WebElement  SelectonCustomizeAlertBatteryJob ;

public void ApplyCustomizeJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.CustomizeDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);

	SelectonCustomizeAlertBatteryJob.click();
	sleep(4);
	Reporter.log("PASS >> Customize Alert Template Battery Policy Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

// Enable Data Usage Policy

@FindBy(xpath="//textarea[@id='EnableDataUsagePolicyTemp']")
private WebElement  EditTextDataUsagePolicy ;

public void AlertTemplatesDataUsagePolicy()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = DataUsagePolicyVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextDataUsagePolicy.isDisplayed();
	if(flag)
	{	   
		 EditTextDataUsagePolicy.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextDataUsagePolicy.sendKeys(Config.DataUsagePolicyText);
		 Reporter.log("PASS >> Data Usage Policy Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

@FindBy(xpath="//input[@id='job_ip10_input']")
private WebElement  EnableDataUsagePolicyOption ;

@FindBy(xpath="//input[@id='job_ip11_input']")
private WebElement  EnterDataUsagePolicy ;


public void NotificationPolicyDataUsageDetails()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.DataUsageJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableDataUsagePolicyOption.click();
	Reporter.log("PASS >> Enable Data Usage Policy Selected",true);
	sleep(4);
	
	EnterDataUsagePolicy.clear();
	sleep(4);
	EnterDataUsagePolicy.sendKeys("2");
	Reporter.log("PASS >> 2MB Data Usage Policy Entered",true);
	sleep(4);
	
}

@FindBy(xpath="//p[normalize-space()='Trials Data Usage Policy']")
private WebElement  SelectTrialsDataUsagePolicy ;

public void ApplyDataUsageJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.DataUsageDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsDataUsagePolicy.click();
	sleep(4);
	Reporter.log("PASS >> Trials Data Usage Policy Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}


@FindBy(xpath="(//p[normalize-space()='Trials Data Usage Policy'])[2]")
private WebElement  SelecttoDeleteTrialsDataUsagePolicy ;
		
		
public void SelectTrialsDatausageJob()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsDataUsagePolicy.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

//user defined data usage policy

public void AlertTemplatesDataUsagePolicyUserdefined()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = DataUsagePolicyVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextDataUsagePolicy.isDisplayed();
	if(flag)
	{	   
		 EditTextDataUsagePolicy.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextDataUsagePolicy.sendKeys(Config.DataUsagePolicyuserdefinedText);
		 Reporter.log("PASS >> Data Usage Policy Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

public void NotificationPolicyDataUsageUserdefined()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.DataUsageUserdefinedJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableDataUsagePolicyOption.click();
	Reporter.log("PASS >> Enable Data Usage Policy Selected",true);
	sleep(4);
	
	EnterDataUsagePolicy.clear();
	sleep(4);
	EnterDataUsagePolicy.sendKeys("2");
	Reporter.log("PASS >> 2MB Data Usage Policy Entered",true);
	sleep(4);
	
}


@FindBy(xpath="//p[normalize-space()='Trials Userdefined Data Usage Policy']")
private WebElement  SelectTrialsUserdefinedDataUsagePolicy ;


public void DataUsageJobonDeviceUserdefined()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.DataUsageDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsUserdefinedDataUsagePolicy.click();
	sleep(4);
	Reporter.log("PASS >> Trials Userdefined Data Usage Policy Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}


@FindBy(xpath="(//p[normalize-space()='Trials Userdefined Data Usage Policy'])[2]")
private WebElement  SelecttoDeleteTrialsUserdefinedDataUsagePolicy ;
		
		
public void SelectTrialsUserdefinedDatausageJob()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsUserdefinedDataUsagePolicy.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

// Connection Policy 

@FindBy(xpath="//textarea[@id='EnableConnectionPolicyTemp']")
private WebElement  EditTextConnectionPolicy;

public void AlertTemplatesConnectionPolicy()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ConnectionPolicyVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextConnectionPolicy.isDisplayed();
	if(flag)
	{	   
		 EditTextConnectionPolicy.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextConnectionPolicy.sendKeys(Config.ConnectionPolicyText);
		 Reporter.log("PASS >> Connection Policy Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

@FindBy(xpath="//input[@id='job_ip3_input']")
private WebElement  EnableConnectionPolicyOption ;

@FindBy(xpath="//input[@id='job_ip4_input']")
private WebElement  EnterConnectionPolicy ;


public void NotificationPolicyConnectionDetails()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.ConnectionPolicyJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableConnectionPolicyOption.click();
	Reporter.log("PASS >> Enable Connection Policy Selected",true);
	sleep(4);
	
	EnterConnectionPolicy.clear();
	sleep(4);
	EnterConnectionPolicy.sendKeys("5");
	Reporter.log("PASS >> Notify when device is not connected for 5 Min",true);
	sleep(4);
	
}


@FindBy(xpath="//p[normalize-space()='Trials Connection Policy']")
private WebElement  SelectTrialsConnectionPolicy ;

public void ApplyConnectionPolicyJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.ConnectionPolicyDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsConnectionPolicy.click();
	sleep(4);
	Reporter.log("PASS >> Trials Connection Policy Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Connection Policy'])[2]")
private WebElement  SelecttoDeleteTrialsConnectionPolicy ;
			
public void SelectTrialsConnectionPolicyJob()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsConnectionPolicy.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

// Connection Policy User Defined 

public void AlertTemplatesConnectionPolicyUserdefined()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = ConnectionPolicyVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextConnectionPolicy.isDisplayed();
	if(flag)
	{	   
		 EditTextConnectionPolicy.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextConnectionPolicy.sendKeys(Config.ConnectionPolicyUserdefinedText);
		 Reporter.log("PASS >> Userdefined Connection Policy Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}


public void NotificationPolicyConnectionDetailsUserdefined()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.ConnectionUserdefinedJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableConnectionPolicyOption.click();
	Reporter.log("PASS >> Enable Connection Policy Selected",true);
	sleep(4);
	
	EnterConnectionPolicy.clear();
	sleep(4);
	EnterConnectionPolicy.sendKeys("5");
	Reporter.log("PASS >> Notify when device is not connected for 5 Min",true);
	sleep(4);
	
}

@FindBy(xpath="//p[normalize-space()='Trials Userdefined Connection Policy']")
private WebElement  SelectTrialsConnectionPolicyUserdefined ;

public void ApplyConnectionPolicyUserdefinedJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.ConnectionPolicyDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsConnectionPolicyUserdefined.click();
	sleep(4);
	Reporter.log("PASS >> Trials Userdefined Connection Policy Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Userdefined Connection Policy'])[2]")
private WebElement  SelecttoDeleteTrialsConnectionPolicyuserdefined ;
			
public void SelectTrialsConnectionPolicyJobUserdefined()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsConnectionPolicyuserdefined.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

// Notify when device comes online

@FindBy(xpath="//textarea[@id='Notifywhendevicecomesonlinetemp']")
private WebElement  EditTextNotifywhendevicecomesonline ;

public void AlertTemplatesNotifywhendevicecomesonline()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = NotifywhendevicecomesonlineVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextNotifywhendevicecomesonline.isDisplayed();
	if(flag)
	{	   
		 EditTextNotifywhendevicecomesonline.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextNotifywhendevicecomesonline.sendKeys(Config.NotifywhendevicecomesonlineText);
		 Reporter.log("PASS >> Notify when device comes on line Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

@FindBy(xpath="//input[@id='DeviceOnlineNotification']")
private WebElement  EnableNotifywhendevicecomesonline ;


public void NotificationPolicyNotifywhenDeviceComesonline()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.NotifywhendevicecomesonlineJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableNotifywhendevicecomesonline.click();
	Reporter.log("PASS >> Notify when device comes on line Selected",true);
	sleep(4);
	
}

@FindBy(xpath="//p[normalize-space()='Trials Notify when device comes online']")
private WebElement  SelectTrialsNotifywhendevicecomesonline ;

public void ApplyNotifywhenDevicecomesonlineJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.NotifywhendevicecomesonlineDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsNotifywhendevicecomesonline.click();
	sleep(4);
	Reporter.log("PASS >> Trials Notify when device comes online Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Notify when device comes online'])[2]")
private WebElement  SelecttoDeleteTrialsNotifywhendevicecomesonline ;
			
public void SelectTrialsNotifywhendevicecomesonlineJob()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsNotifywhendevicecomesonline.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

// Notify when device comes online userdefined


public void AlertTemplatesNotifywhendevicecomesonlineUserdefined()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = NotifywhendevicecomesonlineVisible ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextNotifywhendevicecomesonline.isDisplayed();
	if(flag)
	{	   
		EditTextNotifywhendevicecomesonline.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextNotifywhendevicecomesonline.sendKeys(Config.NotifywhendevicecomesonlineUserdefinedText);
		 Reporter.log("PASS >> Userdefined Notify when device comes on line Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

public void NotificationPolicyNotifywhenDeviceComesonlineUserdefined()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.NotifywhendevicecomesonlineUserdefinedJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableNotifywhendevicecomesonline.click();
	Reporter.log("PASS >> Notify when device comes on line Selected",true);
	sleep(4);	
}

@FindBy(xpath="//p[normalize-space()='Trials Userdefined Notify when device comes online']")
private WebElement  SelectTrialsNotifywhendevicecomesonlineUserdefined ;

public void ApplyNotifywhendevicecomesonlineUserdefinedJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.NotifywhendevicecomesonlineuserdefinedDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsNotifywhendevicecomesonlineUserdefined.click();
	sleep(4);
	Reporter.log("PASS >> Trials Userdefined Notify when device comes online Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Userdefined Notify when device comes online'])[2]")
private WebElement  SelecttoDeleteTrialsNotifywhendevicecomesonlineuserdefined ;
			
public void SelectTrialsNotifywhendevicecomesonlineJobUserdefined()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsNotifywhendevicecomesonlineuserdefined.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

// Notify when SIM is changed

@FindBy(xpath="//textarea[@id='NotifywhenSIMischangedtemp']")
private WebElement  EditTextNotifywhenSIMischanged ;

public void AlertTemplatesNotifywhenSIMischanged()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = EditTextNotifywhenSIMischanged ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextNotifywhenSIMischanged.isDisplayed();
	if(flag)
	{	   
		EditTextNotifywhenSIMischanged.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextNotifywhenSIMischanged.sendKeys(Config.NotifywhenSIMischangedText);
		 Reporter.log("PASS >> Notify when SIM is changed Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

@FindBy(xpath="//input[@id='SIMChangeNotication']")
private WebElement  EnableNotifywhenSIMischanged ;

public void NotificationPolicyNotifywhenSIMischanged()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.NotifywhenSIMischangedJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableNotifywhenSIMischanged.click();
	Reporter.log("PASS >> Notify when SIM is changed Selected",true);
	sleep(4);	
}

@FindBy(xpath="//p[normalize-space()='Trials Notify when SIM is changed']")
private WebElement  SelectTrialsNotifywhenSIMischanged ;

public void ApplyNotifywhenSIMischangedJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.NotifywhenSIMischangedDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsNotifywhenSIMischanged.click();
	sleep(4);
	Reporter.log("PASS >> Trials Notify when SIM is changed Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Notify when SIM is changed'])[2]")
private WebElement  SelecttoDeleteTrialsNotifywhenSIMischanged ;
			
public void SelectTrialsNotifywhenSIMischanged()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsNotifywhenSIMischanged.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

//  Notify when SIM is changed userdefined

public void AlertTemplatesNotifywhenSIMischangedUserdefined()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = EditTextNotifywhenSIMischanged ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextNotifywhenSIMischanged.isDisplayed();
	if(flag)
	{	   
		 EditTextNotifywhenSIMischanged.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextNotifywhenSIMischanged.sendKeys(Config.NotifywhenSIMischangeduserdefinedText);
		 Reporter.log("PASS >> Notify when SIM is changed Userdefined Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

@FindBy(xpath="//input[@id='SIMChangeNotication']")
private WebElement  EnableNotifywhenSIMischangedUserdefined ;

public void NotificationPolicyNotifywhenSIMischangedUserdefined()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.NotifywhenSIMischangedUserdefinedJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableNotifywhenSIMischangedUserdefined.click();
	Reporter.log("PASS >> Notify when SIM is changed Selected",true);
	sleep(4);	
}

@FindBy(xpath="//p[normalize-space()='Trials Userdefined Notify when SIM is changed']")
private WebElement  SelectTrialsNotifywhenSIMischangedUserdefined ;

public void ApplyNotifywhenSIMischangedUserdefinedJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.NotifywhenSIMischangedUserdefinedDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsNotifywhenSIMischangedUserdefined.click();
	sleep(4);
	Reporter.log("PASS >> Trials Userdefined Notify when SIM is changed Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Userdefined Notify when SIM is changed'])[2]")
private WebElement  SelecttoDeleteTrialsNotifywhenSIMischangedUserdefined ;
			
public void SelectTrialsNotifywhenSIMischangedUserdefined()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsNotifywhenSIMischangedUserdefined.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);

}

// Notify when device is rooted or Nix has been granted with root permission

@FindBy(xpath="//textarea[@id='Notifywhendeviceisrootedtemp']")
private WebElement  EditTextNotifywhendeviceisrooted ;

public void AlertTemplatesNotifywhendeviceisrooted()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = EditTextNotifywhendeviceisrooted ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextNotifywhendeviceisrooted.isDisplayed();
	if(flag)
	{	   
		 EditTextNotifywhendeviceisrooted.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextNotifywhendeviceisrooted.sendKeys(Config.NotifywhendeviceisrootedText);
		 Reporter.log("PASS >> Notify when device is rooted Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

@FindBy(xpath="//input[@id='PermissionGrantedNotification']")
private WebElement  EnableNotifywhendeviceisrooted ;

public void NotificationPolicyNotifywhendeviceisrooted()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.NotifywhendeviceisrootedJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableNotifywhendeviceisrooted.click();
	Reporter.log("PASS >> Notify when device is rooted is Selected",true);
	sleep(4);	
}

@FindBy(xpath="//p[normalize-space()='Trials Notify when device is rooted']")
private WebElement  SelectTrialsNotifywhendeviceisrooted ;

public void ApplyNotifywhendeviceisrootedJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.NotifywhendeviceisrootedDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsNotifywhendeviceisrooted.click();
	sleep(4);
	Reporter.log("PASS >> Trials Notify when device is rooted Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Notify when device is rooted'])[2]")
private WebElement  SelecttoDeleteTrialsNotifywhendeviceisrooted ;
			
public void SelectTrialsNotifywhendeviceisrooted()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsNotifywhendeviceisrooted.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);
}

// Notify when device is rooted or Nix has been granted with root permission Userdefined 

public void AlertTemplatesNotifywhendeviceisrootedUserdefined()throws InterruptedException{
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollDown = EditTextNotifywhendeviceisrooted ;
	Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
	sleep(4);
		
	boolean flag=EditTextNotifywhendeviceisrooted.isDisplayed();
	if(flag)
	{	   
		 EditTextNotifywhendeviceisrooted.clear();		
		 Reporter.log("PASS >> Text Field is Clear",true);
		 sleep(4);
		 EditTextNotifywhendeviceisrooted.sendKeys(Config.NotifywhendeviceisrootedUserdefinedText);
		 Reporter.log("PASS >> Notify when device is rooted Userdefined Templates Entered Successfully",true);
		 sleep(4);
		 SaveAlertTemplate.click();
		 waitForXpathPresent("//span[normalize-space()='Settings updated successfully.']");		
		 String MGS=Initialization.driver.findElement(By.xpath("//span[normalize-space()='Settings updated successfully.']")).getText();
		 Reporter.log("PASS >> Message Displyed is : "+MGS,true);
		 sleep(4);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Text Field is Not Clear");
		Reporter.log("FAIL >> Text Field is Not Clear",true);
	}
}

public void NotificationPolicyNotifywhendeviceisrootedUserdefined()throws InterruptedException{
	
	sleep(4);
	EnterJobName.sendKeys(Config.NotifywhendeviceisrootedUserdefinedJobName);
	Reporter.log("PASS >> Notification Policy Job Name Entered",true);
	sleep(4);
	
	EnableNotifywhendeviceisrooted.click();
	Reporter.log("PASS >> Notify when device is rooted is Selected",true);
	sleep(4);	
}

@FindBy(xpath="//p[normalize-space()='Trials Userdefined Notify when device is rooted']")
private WebElement  SelectTrialsNotifywhendeviceisrootedUserdefined ;

public void ApplyNotifywhendeviceisrootedUserdefinedJobonDevice()throws InterruptedException{

	sleep(4);
	ApplyJobonDevice.clear();
	sleep(4);	
	ApplyJobonDevice.sendKeys(Config.NotifywhendeviceisrootedUserdefinedDeviceName);
	sleep(4);
	Reporter.log("PASS >> Device is Selected to Apply Job ",true);
	ClickonApply.click();
	sleep(4);
	Reporter.log("PASS >> Click on Apply ",true);
	
	SelectTrialsNotifywhendeviceisrootedUserdefined.click();
	sleep(4);
	Reporter.log("PASS >> Trials Userdefined Notify when device is rooted Job is Selected ",true);
	ApplyJobOKButton.click();
	Reporter.log("PASS >> Click on Apply ",true);
	
	waitForXpathPresent("//span[contains(text(),'has been initiated on the device named')]");
	String ToastMsg=Initialization.driver.findElement(By.xpath("//span[contains(text(),'has been initiated on the device named')]")).getText();		
	Reporter.log("PASS >> Message Displayed is : "+ToastMsg,true);
	sleep(2);
}

@FindBy(xpath="(//p[normalize-space()='Trials Userdefined Notify when device is rooted'])[2]")
private WebElement  SelecttoDeleteTrialsNotifywhendeviceisrootedUserdefined ;
			
public void SelectTrialsNotifywhendeviceisrootedUserdefined()throws InterruptedException{
	
	sleep(4);
	SelecttoDeleteTrialsNotifywhendeviceisrootedUserdefined.click();
	sleep(4);
	DeleteButtonClick.click();
	sleep(4);
	DeleteOKButtonClick.click();
	sleep(4);
}

// Customize Nix Sure Lock under windows

@FindBy(xpath="(//span[normalize-space()='Select App'])[2]")
private WebElement  SelectAppVisible ;

public void SelectAppVisible()throws InterruptedException{
		
	boolean flag=SelectAppVisible.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Select App Option is Displayed",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Select App Option is Not Displayed");
		Reporter.log("FAIL >> Select App Option is Not Displayed",true);
	}
}

@FindBy(xpath="(//span[normalize-space()='App Download URL'])[2]")
private WebElement  AppDownloadURLVisible ;

public void AppDownloadURLVisible()throws InterruptedException{
		
	boolean flag=AppDownloadURLVisible.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> App Download URL Option is Displayed",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Download URL Option is Not Displayed");
		Reporter.log("FAIL >> App Download URL Option is Not Displayed",true);
	}
}

@FindBy(xpath="(//span[normalize-space()='App Settings'])[2]")
private WebElement  AppSettingsVisible ;

public void AppSettingsVisible()throws InterruptedException{
		
	boolean flag=AppSettingsVisible.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> App Settings Option is Displayed",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Settings Option is Not Displayed");
		Reporter.log("FAIL >> App Settings Option is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='win_customiseApps_tab_apply']")
private WebElement  GenerateVisible ;

public void GenerateVisible()throws InterruptedException{
		
	boolean flag=GenerateVisible.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Generate Button is Displayed",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Generate Button is Not Displayed");
		Reporter.log("FAIL >> Generate Button is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='win_customizeApps_tab_download']")
private WebElement  DownloadVisible ;

public void DownloadVisible()throws InterruptedException{
		
	boolean flag=DownloadVisible.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Download Button is Displayed",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Download Button is Not Displayed");
		Reporter.log("FAIL >> Download Button is Not Displayed",true);
	}
}

//Customize Nix Sure Lock under Andriod


@FindBy(xpath="(//span[normalize-space()='Select App'])[1]")
private WebElement  SelectAppVisibleAndriod ;

public void SelectAppVisibleAndriod()throws InterruptedException{
		
	boolean flag=SelectAppVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Select App Option is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Select App Option is Not Displayed in Andriod");
		Reporter.log("FAIL >> Select App Option is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="(//span[normalize-space()='App Download URL'])[1]")
private WebElement  AppDownloadURLVisibleAndriod ;

public void AppDownloadURLVisibleAndriod()throws InterruptedException{
		
	boolean flag=AppDownloadURLVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> App Download URL Option is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Download URL Option is Not Displayed in Andriod");
		Reporter.log("FAIL >> App Download URL Option is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="//span[normalize-space()='App Title']")
private WebElement  AppTitleVisibleAndriod ;

public void AppTitleVisibleAndriod()throws InterruptedException{
		
	boolean flag=AppTitleVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> App Title Option is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Title Option is Not Displayed in Andriod");
		Reporter.log("FAIL >> App Title Option is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="(//span[normalize-space()='App Settings'])[1]")
private WebElement  AppSettingsVisibleAndriod ;

public void AppSettingsVisibleAndriod()throws InterruptedException{
		
	boolean flag=AppSettingsVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> App Settings Option is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Settings Option is Not Displayed in Andriod");
		Reporter.log("FAIL >> App Settings Option is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="//input[@id='customiseApps_tab_apply']")
private WebElement  GenerateVisibleAndriod ;

public void GenerateVisibleAndriod()throws InterruptedException{
		
	boolean flag=GenerateVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Generate Button is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Generate Button is Not Displayed in Andriod");
		Reporter.log("FAIL >> Generate Button is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="//input[@id='customizeApps_tab_download']")
private WebElement  DownloadVisibleAndriod ;

public void DownloadVisibleAndriod()throws InterruptedException{
		
	boolean flag=DownloadVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Download Button is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Download Button is Not Displayed in Andriod");
		Reporter.log("FAIL >> Download Button is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="(//*[@id='sett_import'])[1]")
private WebElement  ImportSettingsVisibleAndriod ;

public void ImportSettingsVisibleAndriod()throws InterruptedException{
		
	boolean flag=ImportSettingsVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Import Settings is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Import Settings is Not Displayed in Andriod");
		Reporter.log("FAIL >> Import Settings is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="//div[@id='customiseApps_tab']//button[@id='sett_edit']")
private WebElement  EditSettingsVisibleAndriod ;

public void EditSettingsVisibleAndriod()throws InterruptedException{
		
	boolean flag=EditSettingsVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> Edit Settings is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Edit Settings is Not Displayed in Andriod");
		Reporter.log("FAIL >> Edit Settings is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="//span[normalize-space()='App Launcher Icon']")
private WebElement  AppLauncherIconVisibleAndriod ;

public void AppLauncherIconVisibleAndriod()throws InterruptedException{
		
	boolean flag=AppLauncherIconVisibleAndriod.isDisplayed();
	if(flag)
	{	   
		Reporter.log("PASS >> App Launcher Icon is Displayed in Andriod",true);		 
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Launcher Icon is Not Displayed in Andriod");
		Reporter.log("FAIL >> App Launcher Icon is Not Displayed in Andriod",true);
	}
}

@FindBy(xpath="//input[@id='win_custom_app_downloadlink']")
private WebElement  AppAppDownloadURLVisible ;

public void AppAppDownloadURLVisible()throws InterruptedException{
		
	boolean flag=AppAppDownloadURLVisible.isDisplayed();
	if(flag)
	{	   
		String option = AppAppDownloadURLVisible.getAttribute("value");
		String Expected = "https://suremdm.42gears.com/nix/nix_installer_win.exe";
		String pass = "PASS >> App Download URL Displayed is : " +option;
		String fail = "FAIL >> App Download URL Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Download URL Not Displayed in windows");
		Reporter.log("FAIL >> App Download URL Not Displayed in windows",true);
	}
}

@FindBy(xpath="//div[@id='customiseApps_tab']//select[@id='app_to_customize']")
private WebElement  SelectAppDropDownVisible ;

@FindBy(xpath="//input[@id='custom_app_downloadlink']")
private WebElement  AppAppDownloadURLVisibleAndriod ;

public void AppAppDownloadURLVisibleAndriod()throws InterruptedException{
		
	Select se1 = new Select(SelectAppDropDownVisible);
	se1.selectByVisibleText("SureMDM Nix Agent");	
	boolean flag=AppAppDownloadURLVisibleAndriod.isDisplayed();
	if(flag)
	{	   				
		String option = AppAppDownloadURLVisibleAndriod.getAttribute("value");
		//String Expected = "https://mars.42gears.com/support/inout/surelockv21.03.29.apk?_ga=2.71189136.1620681701.1625718319-1253889108.1588753628";
		String Expected = "http://suremdm.42gears.com/nix/nixagent.apk";
		String pass = "PASS >> App Download URL Displayed is : " +option;
		String fail = "FAIL >> App Download URL Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2);			
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Download URL Not Displayed in windows");
		Reporter.log("FAIL >> App Download URL Not Displayed in windows",true);
	}
}

public void AppAppDownloadURLSureLockVisibleAndriod()throws InterruptedException{
	
	Select se1 = new Select(SelectAppDropDownVisible);
	se1.selectByVisibleText("SureLock");	
	boolean flag=AppAppDownloadURLVisibleAndriod.isDisplayed();
	if(flag)
	{	   				
		String option = AppAppDownloadURLVisibleAndriod.getAttribute("value");
		//String Expected = "https://mars.42gears.com/support/inout/surelock.apk?_ga=2.186693932.567116683.1605070583-1978696663.1588140234";
		String Expected = "https://mars.42gears.com/support/inout/surelock.apk";
		String pass = "PASS >> App Download URL Displayed is : " +option;
		String fail = "FAIL >> App Download URL Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2);			
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> App Download URL Not Displayed in windows");
		Reporter.log("FAIL >> App Download URL Not Displayed in windows",true);
	}
}

@FindBy(xpath="//input[@id='custom_app_label']")
private WebElement  EditAppTitleAndriod ;

public void EditAppTitleSureMDMNixAgentAndriod()throws InterruptedException{

	 Select se1 = new Select(SelectAppDropDownVisible);
	 se1.selectByVisibleText("SureMDM Nix Agent");	
	 String option=Initialization.driver.findElement(By.xpath("(//option[@value='nixagent'])[1]")).getText();		
	 Reporter.log("PASS >> Option Selected is : "+option,true);
	 sleep(2);
	 EditAppTitleAndriod.clear();
	 sleep(2);	 
	 EditAppTitleAndriod.sendKeys("42Gears");
	 sleep(2);
	 Reporter.log("PASS >> App Title of Sure MDM Nix Agent is Edited Successfully ",true);
}

public void EditAppTitleSureLockAndriod()throws InterruptedException{

	 Select se1 = new Select(SelectAppDropDownVisible);
	 se1.selectByVisibleText("SureLock");	
	 String option=Initialization.driver.findElement(By.xpath("//option[@value='surelock']")).getText();		
	 Reporter.log("PASS >> Option Selected is : "+option,true);
	 sleep(2);
	 EditAppTitleAndriod.clear();
	 sleep(2);	 
	 EditAppTitleAndriod.sendKeys("42Gears");
	 sleep(2);
	 Reporter.log("PASS >> App Title of Sure Lock is Edited Successfully ",true);
}

@FindBy(xpath="(//div[@id='sett_import'])[1]")
private WebElement  ImportSettingbutton ;

public void SureMDMNixAgentAndriod()throws InterruptedException{

	 Select se1 = new Select(SelectAppDropDownVisible);
	 se1.selectByVisibleText("SureMDM Nix Agent");	
	 String option=Initialization.driver.findElement(By.xpath("(//option[@value='nixagent'])[1]")).getText();		
	 Reporter.log("PASS >> Option Selected is : "+option,true);
	 sleep(2);
}

public void ClickedonImportButton()throws InterruptedException{
	
	boolean flag=ImportSettingbutton.isDisplayed();
	if(flag)
	{	   		
		sleep(2);
		ImportSettingbutton.click();
		sleep(2);
		Reporter.log("PASS >> Import Setting Button is Displayed",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Import Setting Button is Not Displayed");
		Reporter.log("FAIL >> Import Setting Button is Not Displayed",true);
	}
}

public void browseFileImportSettings(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);
	sleep(5);
	Reporter.log("PASS >> Imported Settings Uploaded Successfully", true);		
}

@FindBy(xpath="(//*[@id='sett_edit'])[1]")
private WebElement  EditSettingbutton ;

public void EditSettingbutton()throws InterruptedException{
	
	boolean flag=EditSettingbutton.isDisplayed();
	if(flag)
	{	   		
		sleep(2);
		EditSettingbutton.click();
		sleep(2);
		Reporter.log("PASS >> Edit Setting Button is Displayed",true);
		System.out.println(flag);
	}
	else
	{		
		System.out.println(flag);
		ALib.AssertFailMethod("FAIL >> Edit Setting Button is Not Displayed");
		Reporter.log("FAIL >> Edit Setting Button is Not Displayed",true);
	}
}

@FindBy(xpath="//*[@id='sett_edit_textArea']")
private WebElement  EditSettingClearTextbutton ;

@FindBy(xpath="(//button[normalize-space()='Done'])[3]")
private WebElement  EditSettingClearTextOKbutton ;

public void EditSettingClearTextbutton()throws InterruptedException{

	sleep(2);
	EditSettingClearTextbutton.clear();
	sleep(2);
	Reporter.log("PASS >> Edit Setting Field Cleared Successfully",true);
	EditSettingClearTextOKbutton.click();
	sleep(4);
}

@FindBy(xpath="//span[@title='Remove Icon']")
private WebElement  AppLauncherIconRemovebutton ;

public void AppLauncherIconRemovebutton()throws InterruptedException{
	
	boolean flag=AppLauncherIconRemovebutton.isDisplayed();
	if(flag)
	{	   		
		sleep(2);
		AppLauncherIconRemovebutton.click();
		sleep(2);
		Reporter.log("PASS >> App Launcher Icon Remove Button is Displayed",true);		
	}
	else
	{		
		Reporter.log("FAIL >> App Launcher Icon Remove Button is Not Displayed",true);
	}
}

@FindBy(xpath="//*[@id='custAppIcnCover']/div")
private WebElement  AppLauncherIconBrowserbutton ;

public void AppLauncherIconBrowserbutton()throws InterruptedException{
	sleep(2);
	AppLauncherIconBrowserbutton.click();
	sleep(4);
	Reporter.log("PASS >> App Launcher Icon Browser Button is Displayed",true);
}

public void browseFileAppLauncherIcon(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);
	sleep(5);
	Reporter.log("PASS >> App Launcher Icon Uploaded Successfully", true);		
}

@FindBy(xpath="//input[@id='customizeApps_tab_download']")
private WebElement  NixAndriodDownloadbutton ;

public void NixAndriodDownloadbutton()throws InterruptedException{
	
	sleep(2);
	NixAndriodDownloadbutton.click();
	sleep(4);
	Reporter.log("PASS >> Download Successfully",true);
}

public void SureLockAndriod()throws InterruptedException{

	 Select se1 = new Select(SelectAppDropDownVisible);
	 se1.selectByVisibleText("SureLock");	
	 String option=Initialization.driver.findElement(By.xpath("//option[@value='surelock']")).getText();		
	 Reporter.log("PASS >> Option Selected is : "+option,true);
	 sleep(2);
}

public void browseFileImportSettingsSureLock(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);
	sleep(5);
	Reporter.log("PASS >> Imported Settings SureLock Uploaded Successfully", true);		
}

public void SureLockAndriodDownloadbutton()throws InterruptedException{
	
	sleep(5);
	NixAndriodDownloadbutton.click();
	sleep(4);
	Reporter.log("PASS >> SureLock APK Download Successfully",true);
}

public void APPDownloadURLLinkbutton()throws InterruptedException{
	
	sleep(2);
	AppAppDownloadURLVisibleAndriod.clear();
	sleep(4);
	Reporter.log("PASS >> APP Download URL Link Empty",true);
}

@FindBy(xpath="//span[normalize-space()='Download Link cannot be empty.']")
private WebElement  MsgLinkEmpty ;

public void ClickonAndriodGenerateLinkEmpty()throws InterruptedException{
	
	boolean flag=ClickonAndriodGenerateButton.isDisplayed();
	if(flag)
	{	
		ClickonAndriodGenerateButton.click();
	    waitForXpathPresent("//span[normalize-space()='Download Link cannot be empty.']");	    
		Reporter.log("PASS >> Clicked on Generate Button",true);
		String msg = MsgLinkEmpty.getText();		
		Reporter.log("PASS >> Error Message for Generate is: "+msg,true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Error Message for Generate is Not Displayed");
		Reporter.log("FAIL >> Error Message for Generate is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='custom_app_label']")
private WebElement  APPTittleTextField ;

public void APPTittleTextField()throws InterruptedException{
	
	sleep(2);
	APPTittleTextField.clear();
	sleep(4);
	Reporter.log("PASS >> APP Tittle Text Field is Empty",true);
}

@FindBy(xpath="//span[contains(text(),'Atleast one of App Title, App Settings, App Launch')]")
private WebElement  AllEmptyMsgTextField ;

public void ClickonAndriodGenerateALLEmpty()throws InterruptedException{
	
	boolean flag=ClickonAndriodGenerateButton.isDisplayed();
	if(flag)
	{	
		ClickonAndriodGenerateButton.click();
	    waitForXpathPresent("//span[contains(text(),'Atleast one of App Title, App Settings, App Launch')]");	    
		Reporter.log("PASS >> Clicked on Generate Button",true);
		String msg = AllEmptyMsgTextField.getText();		
		Reporter.log("PASS >> Error Message for Generate is: "+msg,true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Error Message for Generate is Not Displayed");
		Reporter.log("FAIL >> Error Message for Generate is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='win_custom_app_downloadlink']")
private WebElement  APPDownloadURLEmptyWindows ;

public void APPDownloadURLEmptyWindows()throws InterruptedException{
	
	sleep(2);
	APPDownloadURLEmptyWindows.clear();
	sleep(4);
	Reporter.log("PASS >> APP Download URL is Empty",true);
}

@FindBy(xpath="//div[@id='win_customiseApps_tab']//button[@id='sett_edit']")
private WebElement  EditSettingWindowButton ;

@FindBy(xpath="//textarea[@id='sett_edit_textArea']")
private WebElement  ClearTextAreaWindow ;

@FindBy(xpath="//button[@class='btn smdm_btns smdm_grn_clr addbutton sc-doneBtn'][normalize-space()='Done']")
private WebElement  DoneWindow ;

public void EditSettingWindowButton()throws InterruptedException{
	
	sleep(2);
	EditSettingWindowButton.click();
	sleep(4);
	ClearTextAreaWindow.clear();
	sleep(4);
	DoneWindow.click();
	Reporter.log("PASS >> APP Setting Text Area is Clear",true);
	sleep(2);
}

@FindBy(xpath="//span[contains(text(),'App Settings field must be filled.')]")
private WebElement  MsgLinkEmptywindows ;

@FindBy(xpath="//input[@id='win_customiseApps_tab_apply']")
private WebElement  ClickonWindowsGenerateButton ;

public void ClickonWindowGenerateAllLinkEmpty()throws InterruptedException{
	
	boolean flag=ClickonWindowsGenerateButton.isDisplayed();
	if(flag)
	{	
		ClickonWindowsGenerateButton.click();
	    waitForXpathPresent("//span[contains(text(),'App Settings field must be filled.')]");	    
		Reporter.log("PASS >> Clicked on Generate Button",true);
		String msg = MsgLinkEmptywindows.getText();		
		Reporter.log("PASS >> Error Message for Generate for windows is: "+msg,true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Error Message for Generate is Not Displayed");
		Reporter.log("FAIL >> Error Message for Generate is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='win_customiseApps_tab_apply']")
private WebElement  ClickonWindowsGeneButton ;

@FindBy(xpath="//span[contains(text(),'App customization successful. Please click on download to download this app.')]")
private WebElement  SuccessMgsWindows ;

public void ClickonWindowsGeneButton()throws InterruptedException{
	
	boolean flag=ClickonWindowsGeneButton.isDisplayed();
	if(flag)
	{	
		ClickonWindowsGeneButton.click();
	    waitForXpathPresent("//span[contains(text(),'App customization successful. Please click on download to download this app.')]");	    
		Reporter.log("PASS >> Clicked on Generate Button",true);
		String msg = SuccessMgsWindows.getText();		
		Reporter.log("PASS >> Success Message Generate for windows is: "+msg,true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Success Message Generate for windows is Not Displayed");
		Reporter.log("FAIL >> Success Message Generate for windows is Not Displayed",true);
	}
}

@FindBy(xpath="(//div[@id='sett_import'])[2]")
private WebElement  ClickedonImportWindowButton ;

public void ClickedonImportWindowButton()throws InterruptedException{
	
	boolean flag=ClickedonImportWindowButton.isDisplayed();
	if(flag)
	{	   		
		sleep(2);
		ClickedonImportWindowButton.click();
		sleep(2);
		Reporter.log("PASS >> Import Setting Button is Displayed",true);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Import Setting Button is Not Displayed");
		Reporter.log("FAIL >> Import Setting Button is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='win_customizeApps_tab_download']")
private WebElement  ClickedonDownloadwindows ;

public void ClickedonDownloadwindows()throws InterruptedException{
	
	sleep(2);
	ClickedonDownloadwindows.click();
	Reporter.log("PASS >> Nix Zip File Download Successfully",true);
	sleep(2);
}

// Mis 

@FindBy(xpath="//select[@id='download_via_adv']")
private WebElement  DefaultConnectivityDropDown ;

public void DefaultconnectivityoptionforInstallFileTransferjobs()throws InterruptedException{
	
   Select se1 = new Select(DefaultConnectivityDropDown);
   se1.selectByVisibleText("Wi-Fi Only");	
   String option=Initialization.driver.findElement(By.xpath("//option[text()='Wi-Fi Only']")).getText();		
   Reporter.log("PASS >> Option Selected is : "+option,true);

}

@FindBy(xpath="//div[@id='tableContainer']//input[@placeholder='Search']")
private WebElement SearchAutomationDevice;

public void SearchAutomationDevice() throws InterruptedException {
	
	sleep(2);
	SearchAutomationDevice.clear();
	sleep(2);
	SearchAutomationDevice.sendKeys(Config.MiscellaneousDeviceName);
	sleep(4);
}

@FindBy(xpath="//p[normalize-space()='Trials Install Job']")
private WebElement SelectTrialsInstallJob ;

public void SelectTrialsInstallJob() throws InterruptedException {
	
	sleep(2);
	SelectTrialsInstallJob.click();
	sleep(2);
	Reporter.log("PASS >> Trials Install Job Selected",true);
}

@FindBy(xpath="//div[normalize-space()='Deploy job on']")
private WebElement  DeployjobonVisible ;

@FindBy(xpath="//select[@id='download_job_via']")
private WebElement  DeployjobonDropDown ;

public void DeployjobonVisible()throws InterruptedException{
	
	boolean flag=DeployjobonVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//option[text()='Wi-Fi Only']")).getText();
		String Expected = "Wi-Fi Only";
		String pass = "PASS >> Option By Default Displayed is : " +option;
		String fail = "FAIL >> Option By Default Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2);		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Option By Default is Not Displayed");
		Reporter.log("FAIL >> Option By Default is Not Displayed",true);
	}
}

@FindBy(xpath="//button[@id='applyJob_okbtn']")
private WebElement ClickonJobApplyButton ;

@FindBy(xpath="//span[contains(text(),'has been initiated on the device named ')]")
private WebElement SuccessMessage ;

public void ClickonJobApplyButton() throws InterruptedException {
	
	  sleep(2);
	  ClickonJobApplyButton.click();
	  waitForXpathPresent("//span[contains(text(),'has been initiated on the device named ')]");	    
	  Reporter.log("PASS >> Clicked on Apply Button",true);
	  String msg = SuccessMessage.getText();		
	  Reporter.log("PASS >> Success Message is : "+msg,true);
	  sleep(4);
}

@FindBy(xpath="(//p[text()='Trials Install Job'])[2]")
private WebElement SelectTrialsInstallJobAgain ;

@FindBy(xpath="//div[@id='job_delete']//i[@class='icn fa fa-minus-circle']")
private WebElement ClickonDeleteTrailsInstallJobs ;

@FindBy(xpath="//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")
private WebElement OKButton ;

public void SelectTrialsInstallJobAgain() throws InterruptedException {
	
	  sleep(2);
	  SelectTrialsInstallJobAgain.click();
	  sleep(2);
	  ClickonDeleteTrailsInstallJobs.click();
	  sleep(2);
	  OKButton.click();

}

@FindBy(xpath="//*[@id='jobSection']")
private WebElement SelectJobAgain ;

public void JobButtonVisible()throws InterruptedException{
	
	boolean flag=SelectJobAgain.isDisplayed();
	if(flag)
	{	   		
		sleep(2);
		SelectJobAgain.click();
		sleep(2);
		Reporter.log("PASS >> Clicked on Job ",true);
	}
	else
	{		
		Reporter.log("FAIL >> Not Clicked on Job",true);
	}
}

@FindBy(xpath="(//p[text()='Trials File Transfer Job'])[2]")
private WebElement SelectTrialsTrialsFileTransferJobAgain ;

@FindBy(xpath="//div[@id='job_delete']//i[@class='icn fa fa-minus-circle']")
private WebElement ClickonDeleteTrailsFileTransferJobs ;

@FindBy(xpath="//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")
private WebElement OKFileTransferButton ;

public void SelectTrialsFileTransferJobAgain() throws InterruptedException {
	
	  sleep(2);
	  SelectTrialsTrialsFileTransferJobAgain.click();
	  sleep(2);
	  ClickonDeleteTrailsFileTransferJobs.click();
	  sleep(2);
	  OKFileTransferButton.click();

}

@FindBy(xpath="//p[normalize-space()='Trials File Transfer Job']")
private WebElement SelectTrialsTrialsFileTransferJob ;

public void SelectTrialsTrialsFileTransferJob() throws InterruptedException {
	
	sleep(2);
	SelectTrialsTrialsFileTransferJob.click();
	sleep(2);
	Reporter.log("PASS >> Trials File Transfer Job Selected",true);
}

//

public void DefaultconnectivityMobileDataOnlyInstallFileTransferjobs()throws InterruptedException{
	
	   Select se1 = new Select(DefaultConnectivityDropDown);
	   se1.selectByVisibleText("Mobile Data Only");	
	   String option=Initialization.driver.findElement(By.xpath("//option[text()='Mobile Data Only']")).getText();		
	   Reporter.log("PASS >> Option Selected is : "+option,true);
	}

public void DeployMobileDataOnlyjobonVisible()throws InterruptedException{
	
	boolean flag=DeployjobonVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//option[text()='Mobile Data Only']")).getText();
		String Expected = "Mobile Data Only";
		String pass = "PASS >> Option By Default Displayed is : " +option;
		String fail = "FAIL >> Option By Default Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Option By Default is Not Displayed");
		Reporter.log("FAIL >> Option By Default is Not Displayed",true);
	}
}

@FindBy(xpath="//p[normalize-space()='Trials Mobile Data Only Install Job']")
private WebElement SelectTrialsMobileDataOnlyInstallJob ;

public void SelectTrialsMobileDataOnlyInstallJob() throws InterruptedException {
	
	sleep(2);
	SelectTrialsMobileDataOnlyInstallJob.click();
	sleep(2);
	Reporter.log("PASS >> Trials Mobile Data Only Install Job Selected",true);
}

@FindBy(xpath="(//p[text()='Trials Mobile Data Only Install Job'])[2]")
private WebElement SelectTrialsMobileDataOnlyInstallJobAgain ;

public void SelectTrialsMobileDataOnlyInstallJobAgain() throws InterruptedException {
	
	  sleep(2);
	  SelectTrialsMobileDataOnlyInstallJobAgain.click();
	  sleep(2);
	  ClickonDeleteTrailsInstallJobs.click();
	  sleep(2);
	  OKButton.click();
}

//

public void DefaultconnectivityMobileDataOnlyTransferfilejobs()throws InterruptedException{
	
	   Select se1 = new Select(DefaultConnectivityDropDown);
	   se1.selectByVisibleText("Mobile Data Only");	
	   String option=Initialization.driver.findElement(By.xpath("//option[text()='Mobile Data Only']")).getText();		
	   Reporter.log("PASS >> Option Selected is : "+option,true);

	}

@FindBy(xpath="//p[normalize-space()='Trials Mobile Data File Transfer Job']")
private WebElement SelectTrialsMobileDataFileTransferJob ;

public void SelectTrialsMobileDataFileTransferJob() throws InterruptedException {
	
	sleep(2);
	SelectTrialsMobileDataFileTransferJob.click();
	sleep(2);
	Reporter.log("PASS >> Trials Mobile Data Only File Transfer Job Selected",true);
}

@FindBy(xpath="(//p[text()='Trials Mobile Data File Transfer Job'])[2]")
private WebElement SelectTrialsMobileDataFileTransferJobAgain ;

public void SelectTrialsMobileDataFileTransferJobAgain() throws InterruptedException {
	
	  sleep(2);
	  SelectTrialsMobileDataFileTransferJobAgain.click();
	  sleep(2);
	  ClickonDeleteTrailsInstallJobs.click();
	  sleep(2);
	  OKButton.click();

}
//

public void DefaultconnectivityAnyNetworkInstallFileTransferjobs()throws InterruptedException{
	
	   Select se1 = new Select(DefaultConnectivityDropDown);
	   se1.selectByVisibleText("Any Network");	
	   String option=Initialization.driver.findElement(By.xpath("//option[text()='Any Network']")).getText();		
	   Reporter.log("PASS >> Option Selected is : "+option,true);
	}

@FindBy(xpath="//p[normalize-space()='Trials Any Network Install Job']")
private WebElement SelectTrialsAnyNetworkInstallJob ;

public void SelectTrialsAnyNetworkInstallJob() throws InterruptedException {
	
	sleep(2);
	SelectTrialsAnyNetworkInstallJob.click();
	sleep(2);
	Reporter.log("PASS >> Trials Any Network Install Job Selected",true);
}

@FindBy(xpath="(//p[text()='Trials Any Network Install Job'])[2]")
private WebElement SelectTrialsAnyNetworkInstallJobAgain ;

public void SelectTrialsAnyNetworkInstallJobAgain() throws InterruptedException {
	
	  sleep(2);
	  SelectTrialsAnyNetworkInstallJobAgain.click();
	  sleep(2);
	  ClickonDeleteTrailsInstallJobs.click();
	  sleep(2);
	  OKButton.click();

}

public void DeployAnyNetworkjobonVisible()throws InterruptedException{
	
	boolean flag=DeployjobonVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//option[text()='Any Network']")).getText();
		String Expected = "Any Network";
		String pass = "PASS >> Option By Default Displayed is : " +option;
		String fail = "FAIL >> Option By Default Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Option By Default is Not Displayed");
		Reporter.log("FAIL >> Option By Default is Not Displayed",true);
	}
}

//

public void DefaultconnectivityAnyNetworkTransferfilejobs()throws InterruptedException{
	
	   Select se1 = new Select(DefaultConnectivityDropDown);
	   se1.selectByVisibleText("Any Network");	
	   String option=Initialization.driver.findElement(By.xpath("//option[text()='Any Network']")).getText();		
	   Reporter.log("PASS >> Option Selected is : "+option,true);
	}

@FindBy(xpath="//p[normalize-space()='Trials Any Network File Transfer Job']")
private WebElement SelectTrialsAnyNetworkFileTransferJob ;

public void SelectTrialsAnyNetworkFileTransferJob() throws InterruptedException {
	
	sleep(2);
	SelectTrialsAnyNetworkFileTransferJob.click();
	sleep(2);
	Reporter.log("PASS >> Trials Any Network File Transfer Job Selected",true);
}

@FindBy(xpath="(//p[text()='Trials Any Network File Transfer Job'])[2]")
private WebElement SelectTrialsAnyNetworkFileTransferJobAgain ;

public void SelectTrialsAnyNetworkFileTransferJobAgain() throws InterruptedException {
	
	  sleep(2);
	  SelectTrialsAnyNetworkFileTransferJobAgain.click();
	  sleep(2);
	  ClickonDeleteTrailsInstallJobs.click();
	  sleep(2);
	  OKButton.click();
}

public void DeployAnyNetworkFileTransferjobonVisible()throws InterruptedException{
	
	boolean flag=DeployjobonVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//option[text()='Any Network']")).getText();
		String Expected = "Any Network";
		String pass = "PASS >> Option By Default Displayed is : " +option;
		String fail = "FAIL >> Option By Default Displayed is Not Dispalyed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Option By Default is Not Displayed");
		Reporter.log("FAIL >> Option By Default is Not Displayed",true);
	}
}

// MisCellenus Global Search Option

@FindBy(xpath="//input[@id='globalsearchchckbox']")
private WebElement EnableGlobalSearchCheckbox ;

public void EnableGlobalSearchCheckbox()throws InterruptedException{
	
	boolean flag=EnableGlobalSearchCheckbox.isSelected();
	if(flag==false)
	{	
		EnableGlobalSearchCheckbox.click();
		sleep(2);			
	}	
	Reporter.log("PASS >> Clicked on Enable Global Search",true);
}

@FindBy(xpath="//div[@id='tableContainer']//input[@placeholder='Search']")
private WebElement EnableGlobalSearchAutomationDevice;

public void EnableGlobalSearchAutomationDevice() throws InterruptedException {
	
	sleep(2);
	EnableGlobalSearchAutomationDevice.clear();
	sleep(2);
	EnableGlobalSearchAutomationDevice.sendKeys(Config.EnableGlobalSearchDeviceName);
	waitForXpathPresent("//p[text()='AutomationDevice_Lenovo']");
	sleep(2);
}

@FindBy(xpath="//p[text()='AutomationDevice_Lenovo']")
private WebElement AutomationDeviceVisible ;

public void EnableGlobalSearchAutomationDeviceVisible()throws InterruptedException{
	
	boolean flag=AutomationDeviceVisible.isDisplayed();
	if(flag)
	{	   				
		System.out.println(flag);
		String option = Initialization.driver.findElement(By.xpath("//p[text()='AutomationDevice_Lenovo']")).getText();
		String Expected = "AutomationDevice_Lenovo";
		String pass = "PASS >> Device Displayed is : " +option;
		String fail = "FAIL >> Device Not Displayed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{		
		System.out.println(flag);
		ALib.AssertFailMethod("FAIL >> Device is Not Displayed");
		Reporter.log("FAIL >> Device is Not Displayed",true);
	}
}

@FindBy(xpath="//div[contains(text(),'No device available in this group.')]")
private WebElement NoDeviceAvailable ;

public void DisableGlobalSearchAutomationDeviceVisible()throws InterruptedException{
	
	boolean flag=NoDeviceAvailable.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//div[contains(text(),'No device available in this group.')]")).getText();
		String Expected = "No device available in this group.";
		String pass = "PASS >> Message Displayed is : " +option;
		String fail = "FAIL >> Message Not Displayed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{				
		ALib.AssertFailMethod("FAIL >> Device is Not Displayed");
		Reporter.log("FAIL >> Device is Not Displayed",true);
	}
}

@FindBy(xpath="//input[@id='globalsearchchckbox']")
private WebElement DisableGlobalSearchCheckboxs ;

public void DisableGlobalSearchCheckboxs()throws InterruptedException{
	
	boolean flag=DisableGlobalSearchCheckboxs.isSelected();
	if(flag==true)
	{	
		EnableGlobalSearchCheckbox.click();
		sleep(2);			
	}	
	Reporter.log("PASS >> Clicked on Disable Global Search",true);
}

@FindBy(xpath="//div[@id='tableContainer']//input[@placeholder='Search']")
private WebElement DisableGlobalSearchAutomationDevice;

public void DisableGlobalSearchAutomationDevice() throws InterruptedException {
	
	sleep(2);
	EnableGlobalSearchAutomationDevice.clear();
	sleep(2);
	EnableGlobalSearchAutomationDevice.sendKeys(Config.DisableGlobalSearchDeviceName);
	sleep(8);
	waitForXpathPresent("//div[contains(text(),'No device available in this group.')]");
	sleep(2);
}

@FindBy(xpath="//input[@id='AutoSearchEnabled']")
private WebElement DisableEnableAutosearchCheckboxs ;

public void DisableEnableAutosearchCheckboxs()throws InterruptedException{
	
	boolean flag=DisableEnableAutosearchCheckboxs.isSelected();
	if(flag==true)
	{	
		DisableEnableAutosearchCheckboxs.click();
		sleep(2);			
	}	
	Reporter.log("PASS >> Clicked on Disable Auto Search",true);
}

@FindBy(xpath="//div[@id='tableContainer']//input[@placeholder='Search']")
private WebElement DisableEnableAutosearchAutomationDevice;

public void DisableEnableAutosearchAutomationDevice() throws InterruptedException {
	
	sleep(2);
	DisableEnableAutosearchAutomationDevice.clear();
	sleep(2);
	DisableEnableAutosearchAutomationDevice.sendKeys(Config.DisableEnableAutosearchDeviceName);
	sleep(5);
	DisableEnableAutosearchAutomationDevice.sendKeys(Keys.ENTER);
	Reporter.log("PASS >> Clicked on Enter Key",true);
	sleep(8);
	waitForXpathPresent("//p[text()='AutomationDevice_Lenovo']");	
}

public void DisableEnableAutosearchAutomationDeviceVisible()throws InterruptedException{
	
	boolean flag=AutomationDeviceVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//p[text()='AutomationDevice_Lenovo']")).getText();
		String Expected = "AutomationDevice_Lenovo";
		String pass = "PASS >> Device Displayed After Pressing Enter Key : " +option;
		String fail = "FAIL >> Device Not Displayed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Device is Not Displayed");
		Reporter.log("FAIL >> Device is Not Displayed",true);
	}
}

//

@FindBy(xpath="//input[@id='AutoSearchEnabled']")
private WebElement EnableAutosearchCheckboxs ;

public void EnableAutosearchCheckboxs()throws InterruptedException{
	
	boolean flag=EnableAutosearchCheckboxs.isSelected();
	if(flag==false)
	{	
		EnableAutosearchCheckboxs.click();
		sleep(2);			
	}	
	Reporter.log("PASS >> Clicked on Enable Auto Search",true);
}

@FindBy(xpath="//div[@id='tableContainer']//input[@placeholder='Search']")
private WebElement EnableAutosearchAutomationDevice;

public void EnableAutosearchAutomationDevice() throws InterruptedException {
	
	sleep(2);
	EnableAutosearchAutomationDevice.clear();
	sleep(2);
	EnableAutosearchAutomationDevice.sendKeys(Config.EnableAutosearchDeviceName);
	sleep(8);
	Reporter.log("PASS >> Not Clicked on Enter Key",true);
	waitForXpathPresent("//p[text()='AutomationDevice_Lenovo']");	
}

public void EnableAutosearchAutomationDeviceVisible()throws InterruptedException{
	
	boolean flag=AutomationDeviceVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//p[text()='AutomationDevice_Lenovo']")).getText();
		String Expected = "AutomationDevice_Lenovo";
		String pass = "PASS >> Device Displayed Without Pressing Enter Key : " +option;
		String fail = "FAIL >> Device Not Displayed";
		ALib.AssertEqualsMethod(Expected, option, pass, fail);	
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Device is Not Displayed");
		Reporter.log("FAIL >> Device is Not Displayed",true);
	}
}

//
@FindBy(xpath="//select[@id='temperature_unit']")
private WebElement DevicegridBatteryTemperatureinCelsius ;

public void DevicegridBatteryTemperatureinCelsius()throws InterruptedException{
	
	   Select se1 = new Select(DevicegridBatteryTemperatureinCelsius);
	   se1.selectByVisibleText("Celsius (�C)");	
	   String option=Initialization.driver.findElement(By.xpath("//option[text()='Celsius (�C)']")).getText();		
	   Reporter.log("PASS >> Temperature Unit Option Selected is : "+option,true);
	}

@FindBy(xpath="//div[@id='tableContainer']//input[@placeholder='Search']")
private WebElement TemperatureUnitAutomationDevice;

public void TemperatureUnitAutomationDevice() throws InterruptedException {
	
	sleep(2);
	TemperatureUnitAutomationDevice.clear();
	sleep(2);
	TemperatureUnitAutomationDevice.sendKeys(Config.TemperatureUnitDeviceName);
	sleep(5);
	waitForXpathPresent("//p[text()='AutomationDevice_Lenovo']");	
}

//@FindBy(id="temperature_356d5f7a-893c-4ca5-aef7-6942fec19652")
@FindBy(id="temperature_ce9670db-a9fb-4162-9ea1-1bfb8f3bce0a")
private WebElement MoveRight ;

public void BatteryTemperatureVisible() throws InterruptedException {
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollRight = MoveRight ;
	Js.executeScript("arguments[0].scrollIntoView();", scrollRight);
	sleep(4);
}

@FindBy(xpath="//p[contains(text(),' �C')]")
private WebElement BatteryTemperatureValidation ;

public void BatteryTemperatureValidation()throws InterruptedException{
	
	boolean flag=BatteryTemperatureValidation.isDisplayed();
	if(flag)
	{	   				
		String option=Initialization.driver.findElement(By.xpath("//p[contains(text(),' �C')]")).getText();		
		Reporter.log("PASS >> Temperature Unit Option Displayed is : "+option,true);
	}
	else
	{				
		ALib.AssertFailMethod("FAIL >> Temperature Unit Option in �C is Not Displayed");
		Reporter.log("FAIL >> Temperature Unit Option in �C is Not Displayed",true);
	}
}

//@FindBy(id="deviceName_356d5f7a-893c-4ca5-aef7-6942fec19652")
@FindBy(id="deviceName_ce9670db-a9fb-4162-9ea1-1bfb8f3bce0a")
private WebElement MoveLeft ;

public void OriginalElement() throws InterruptedException {
	
	JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
	WebElement scrollLeft = MoveLeft ;
	Js.executeScript("arguments[0].scrollIntoView();", scrollLeft);
	sleep(4);
}

//
@FindBy(xpath="//select[@id='temperature_unit']")
private WebElement DevicegridBatteryTemperatureinFahrenheit ;

public void DevicegridBatteryTemperatureinFahrenheit()throws InterruptedException{
	
	   Select se1 = new Select(DevicegridBatteryTemperatureinFahrenheit);
	   se1.selectByVisibleText("Fahrenheit (�F)");	
	   String option=Initialization.driver.findElement(By.xpath("//option[text()='Fahrenheit (�F)']")).getText();		
	   Reporter.log("PASS >> Temperature Unit Option Selected is : "+option,true);

	}

@FindBy(xpath="//p[contains(text(),' �F')]")
private WebElement BatteryTemperatureFahrenheitValidation ;

public void BatteryTemperatureFahrenheitValidation()throws InterruptedException{
	
	boolean flag=BatteryTemperatureFahrenheitValidation.isDisplayed();
	if(flag)
	{	   				
		String option=Initialization.driver.findElement(By.xpath("//p[contains(text(),' �F')]")).getText();		
		Reporter.log("PASS >> Temperature Unit Option Displayed is : "+option,true);
	}
	else
	{		
		
		ALib.AssertFailMethod("FAIL >> Temperature Unit Option in �F is Not Displayed");
		Reporter.log("FAIL >> Temperature Unit Option in �F is Not Displayed",true);
	}
}

// custome report 

@FindBy(xpath="//input[@id='custom_report_name_input']")
private WebElement CustomereportName ;

@FindBy(xpath="//input[@id='custom_report_description_input']")
private WebElement CustomereportDescription ;

public void EnteringCustomeReportDetails(String un, String pwd) throws InterruptedException {
	sleep(4);
	CustomereportName.sendKeys(un);
	Reporter.log("PASS >> Custome Report Name Entered ",true);
	sleep(4);
	CustomereportDescription.sendKeys(pwd);
	Reporter.log("PASS >> Custome Report Description Entered ",true);
	sleep(2);

}

@FindBy(xpath="//li[@class='list-group-item node-selected_tablesList_tree'][normalize-space()='Battery Temperature']")
private WebElement ClickonBatteryTemperature ;

public void ClickonBatteryTemperature()throws InterruptedException{

    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
    WebElement scrollDown = ClickonBatteryTemperature ;
    Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
    sleep(4);
    ClickonBatteryTemperature.click();
    Reporter.log("PASS >> Clicked on Battery Temperature ",true);
    sleep(5);
    
}

@FindBy(xpath="//li[@class='list-group-item node-tablesList_tree'][normalize-space()='Battery Temperature']")
private WebElement ClickonLeftSideBatteryTemperature ;

public void ClickonLeftSideBatteryTemperature()throws InterruptedException{

    JavascriptExecutor Js=(JavascriptExecutor)Initialization.driver;
    WebElement scrollDown = ClickonLeftSideBatteryTemperature ;
    Js.executeScript("arguments[0].scrollIntoView(true);", scrollDown);
    sleep(4);
    ClickonLeftSideBatteryTemperature.click();
    Reporter.log("PASS >> Clicked on Battery Temperature ",true);
    sleep(5);  
}


@FindBy(xpath="//*[@id='reportList_tableCont']/section[1]/div[2]/div[1]/div[2]/input")
private WebElement  SearchDeviceinReports ;

public void SearchDeviceinReports()throws InterruptedException{

    sleep(2);
    SearchDeviceinReports.sendKeys("Windows_Device");   
    Reporter.log("PASS >> Search for Device",true);
    sleep(5);  
}


@FindBy(xpath="//*[contains(text(),' �C ')]")
private WebElement ClickonBatteryVisible ;

public void ClickonBatteryVisible()throws InterruptedException{

	boolean flag=ClickonBatteryVisible.isDisplayed();
	if(flag)
	{	   				
		String option = Initialization.driver.findElement(By.xpath("//*[contains(text(),' �C ')]")).getText();
		Reporter.log("PASS >> Battery Temperature is : "+option,true);
		sleep(2); 		
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Battery Temperature is not Displayed");
		Reporter.log("FAIL >> Battery Temperature is not Displayed",true);
	}
}
    
@FindBy(xpath="//p[@class='rpt_nme st-scrollTxt-left']")
private WebElement TrialsBatteryDelete ;

@FindBy(xpath="//div[@id='deleteReport']//i[@class='icn fa fa-minus-circle']")
private WebElement ClickedonTrialsBatteryDelete ;

@FindBy(xpath="//div[@class='modal-dialog modal-sm']//button[@type='button'][normalize-space()='Yes']")
private WebElement ClickedonTrialsBatteryDeleteOK ;

public void TrialsBatteryDelete()throws InterruptedException{

	TrialsBatteryDelete.click();
	sleep(2); 	
	ClickedonTrialsBatteryDelete.click();
	sleep(2); 
	ClickedonTrialsBatteryDeleteOK.click();
	sleep(2); 	
}

//RND TASK 


public void RND()throws InterruptedException{

	int version = 0;
	 
    LogEntries logEntries = Initialization.driver.manage().logs().get(LogType.BROWSER);
    
    for (LogEntry entry : logEntries) {
    	
    	//System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
    	System.out.println(entry.getMessage());
    	String Strentry = entry.getMessage();
    	if(Strentry.contains("~~VERSION~~")) {
    		
    		version++;
    	}			    	
    	
    }
    if(version==0) {
		
		System.out.println("TEST CASE PASS BUILD IS STABLE");
	}else {
		
		System.out.println("TEST CASE FAIL BUILD IS UNSTABLE");
		
	}


}//rnd task

// iosipadosmac

@FindBy(xpath="//button[@id='uploadpemcertificateModal']")
private WebElement UploadpemCertificateok ;

public void UploadpemCertificatebutton()throws InterruptedException{
	
	boolean flag=UploadpemCertificateok.isDisplayed();
	if(flag)
	{	   			
		sleep(2); 	
		UploadpemCertificateok.click();	
		sleep(2); 
		Reporter.log("PASS >> Clicked on upload button  ",true);
	}
	else
	{				
		ALib.AssertFailMethod("FAIL >> Not Clicked on upload button");
		Reporter.log("FAIL >> Not Clicked on upload button",true);
	}
}

@FindBy(xpath="//*[@id='certFileInputFielddiv']/div/div")
private WebElement ClickonbrowserbuttonAPNS ;

public void ClickonbrowserbuttonAPNS()throws InterruptedException{
	
	boolean flag=ClickonbrowserbuttonAPNS.isDisplayed();
	if(flag)
	{	   			
		sleep(2); 	
		ClickonbrowserbuttonAPNS.click();	
		sleep(2); 
		Reporter.log("PASS >> Clicked on Browser button  ",true);
	}
	else
	{				
		ALib.AssertFailMethod("FAIL >> Not Clicked on Browser button");
		Reporter.log("FAIL >> Not Clicked on Browser button",true);
	}
}


public void browseFileAPNS(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);	
	Reporter.log("PASS >> APNS Certificate Uploaded Successfully", true);
	sleep(5);	
}

@FindBy(xpath="//input[@id='apple_id']")
private WebElement EnterAppleID ;

@FindBy(xpath="//button[@id='uploadpemcertificate']")
private WebElement EnterAppleIDUploadbutton ;



public void EnterAppleID() throws IOException, InterruptedException {
	sleep(2);
	EnterAppleID.sendKeys("abcd");
	sleep(2);
	EnterAppleIDUploadbutton.click();
	Reporter.log("PASS >> Apple ID Entered Successfully", true);
	waitForXpathPresent("//span[text()='Uploaded successfully.']");
	Reporter.log("PASS >> Uploaded successfully.", true);
	sleep(2); 
}

public void browseFileWrongAPNS(String FilePath) throws IOException, InterruptedException {

	sleep(5);
	Runtime.getRuntime().exec(FilePath);	
	Reporter.log("PASS >> Wrong APNS Certificate Uploaded Successfully", true);
	sleep(5);	
}
@FindBy(xpath="//div[@id='certificateUploadPopup']//div[@class='close']")
private WebElement CloseAPNSWindow ;

public void EnterAppleIDforWrongCertificate() throws IOException, InterruptedException {
	sleep(2);
	EnterAppleID.sendKeys("abcd");
	sleep(2);
	EnterAppleIDUploadbutton.click();
	Reporter.log("PASS >> Apple ID Entered Successfully", true);
	waitForXpathPresent("//span[text()='The credential you entered was invalid. Please try again.']");
	String msg = Initialization.driver.findElement(By.xpath("//span[text()='The credential you entered was invalid. Please try again.']")).getText();
	Reporter.log("PASS >> Message Displayed is : "+msg,true);
	sleep(7);
	CloseAPNSWindow.click();
	
}

public void EnterAppleIDWithoutCertificate() throws IOException, InterruptedException {
	sleep(2);
	EnterAppleID.sendKeys("abcd");
	sleep(2);
	boolean flag=EnterAppleIDUploadbutton.isSelected();
	if(flag==false)
	{	   					
		Reporter.log("PASS >> Upload button is grayed out without Pem Certificate",true);
	}
	else
	{				
		ALib.AssertFailMethod("FAIL >> Upload button is Not grayed out without Pem Certificate");
		Reporter.log("FAIL >> Upload button is Not grayed out Pem Certificate",true);
	}
}


public void CloseAPNSWindow() throws IOException, InterruptedException {
	sleep(2);
	CloseAPNSWindow.click();
	sleep(2);
	Reporter.log("PASS >> Closed APNS Certificate Window",true);
}

public void EnterCertificateWithoutAppleID() throws IOException, InterruptedException {
	
	
	boolean flag=EnterAppleIDUploadbutton.isSelected();
	if(flag==false)
	{	   					
		Reporter.log("PASS >> Upload button is grayed out without Apple ID",true);
	}
	else
	{				
		ALib.AssertFailMethod("FAIL >> Upload button is Not grayed out without Apple ID");
		Reporter.log("FAIL >> Upload button is Not grayed out without Apple ID",true);
	}
}

//Custome report 
@FindBy(xpath="//button[@id='custom_reports_save_btn']")
private WebElement SaveButtonCustomReports;

public void ClickOnSaveButtonCustomReport() throws InterruptedException
{
	 sleep(2);	 
	 SaveButtonCustomReports.click();
	 sleep(2);
}






























































 //Kavya
	@FindBy(xpath = "//button[@id='changePassword']")
	private WebElement SecurityLink;

	@FindBy(xpath = "//a[normalize-space()='Zebra LifeGuard OTA']")
	private WebElement ZebraLifeGuardOTA;

	@FindBy(xpath = "//button[normalize-space()='Copy To Clipboard']")
	private WebElement CopyToClipboard;

	@FindBy(xpath = "//span[normalize-space()='Disable NPS Survey']")
	private WebElement NPSOPtion;

	@FindBy(xpath = "//p[normalize-space()='Export Settings']")
	private WebElement ExportSettings;

	@FindBy(id = "mask_sett")
	private WebElement ExportSettingOptions;

	@FindBy(xpath = "//input[@id='webapp_ad_auth_endpoint']")
	private WebElement AuthEndpoint;

	@FindBy(xpath = "//input[@id='webapp_ad_token_endpoint']")
	private WebElement TokenEndpoint;

	@FindBy(xpath = "//input[@id='webapp_oauth_client_id']")
	private WebElement clientID;

	@FindBy(xpath = "//input[@id='ClientSecret']")
	private WebElement ClientSecret;

	@FindBy(xpath = "//input[@id='sfb_client_secret']")
	private WebElement AppSecret;

	@FindBy(xpath = "//input[@id='sfb_client_id']")
	private WebElement AppID;

	@FindBy(xpath = "//span[normalize-space()='Enter Password']")
	private WebElement EnterPwd;

	@FindBy(xpath = "//span[normalize-space()='Show Password']")
	private WebElement ShowPwd;

	@FindBy(xpath = "//span[contains(text(),'Bypass password authentication during QR enrollmen')]")
	private WebElement BypssPwd;

	@FindBy(id = "iOSmacOSSettings")
	private WebElement iOSmacOS;

	@FindBy(id = "ios_miscellaneous")
	private WebElement iosmiscellaneous;

	@FindBy(xpath = "//span[normalize-space()='Preserve Data Plan']")
	private WebElement PreserveDataPlan;

	@FindBy(xpath = "//span[normalize-space()='Disallow proximity Setup']")
	private WebElement DisallowproximitySetup;

	@FindBy(xpath = "//span[normalize-space()='Recovery PIN(6 Digit)']")
	private WebElement RecoveryPIN;

	@FindBy(xpath = "//input[@id='recovery_pin']")
	private WebElement recoverypin;

	@FindBy(xpath = "//div[@id='apns_status']")
	private WebElement CertificateInstr;

	@FindBy(xpath = "//a[normalize-space()='APNS']")
	private WebElement APNS;

	@FindBy(xpath = "//button[normalize-space()='Security']")
	private WebElement security;

	@FindBy(xpath = "//a[normalize-space()='Change Password']")
	private WebElement ChangePassword;

	@FindBy(xpath = "//a[normalize-space()='Two-Factor Authentication']")
	private WebElement TwoFactorAuthentication;

	@FindBy(xpath = "//div[@id='changePassword_header']//button[@aria-label='Close']")
	private WebElement SecuritycloseBtn;

	public void ClickOnSecurityLink() throws InterruptedException {
		Jc.executeScript("arguments[0].scrollIntoView()", SecurityLink);
		sleep(2);
		Helper.highLightElement(Initialization.driver, SecurityLink);
		SecurityLink.click();
		waitForXpathPresent("//input[@id='account_management_content']");
		sleep(2);
	}

	public void clickOnSecurity() throws InterruptedException {
		sleep(4);
		Helper.highLightElement(Initialization.driver, SecurityLink);
		SecurityLink.click();
		sleep(4);
		waitForXpathPresent("//h4[normalize-space()='Security']");
	}

	public void validateOptionsInSecurityTab() throws InterruptedException {
		boolean flag = ChangePassword.isDisplayed();
		String PassStatement = "PASS >> ChangePassword option is present.";
		String FailStatement = "FAIL >> ChangePassword option is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
		boolean flag1 = TwoFactorAuthentication.isDisplayed();
		String PassStatement1 = "PASS >> TwoFactorAuthentication option is present.";
		String FailStatement1 = "FAIL >> TwoFactorAuthentication option is not present.";
		ALib.AssertTrueMethod(flag1, PassStatement1, FailStatement1);
		sleep(2);
	}

	public void closeSecurityTab() throws InterruptedException {
		sleep(4);
		SecuritycloseBtn.click();
		sleep(4);
	}

	public void clickOnTwoFactorAuthention() throws InterruptedException {
		sleep(4);
		TwoFactorAuthentication.click();
		sleep(2);
	}
	@FindBy(xpath = "//div[@id='twoFATab']//div[@class='toogle_btn ct-input-middle pull-right']")
	private WebElement ToggleBtn;
	public void validatecheckbox() throws InterruptedException {
		boolean flag = ToggleBtn.isDisplayed();
		String PassStatement = "PASS >> Enable and Disable option is present.";
		String FailStatement = "FAIL >> Enable and Disable option is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}

	public void verifysecurityOption() throws InterruptedException {
		boolean flag = security.isDisplayed();
		String PassStatement = "PASS >> security option is present.";
		String FailStatement = "FAIL >> security option is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}

	public void clickOnZebraLifeGuardOTA() throws InterruptedException {
		ZebraLifeGuardOTA.click();
		sleep(2);
	}

	public void verifycopyToClipBoard() {
		try {
			CopyToClipboard.isDisplayed();
			Reporter.log("PASS >> 'Copy to clipboard button with correct spelling and alignment.' is displayed.", true);
		} catch (Exception e) {
			ALib.AssertFailMethod(
					"FAIL >> 'Copy to clipboard button with correct spelling and alignment.' is not displayed.");
		}
	}

	public void verifyDisableNPSOPtion() throws InterruptedException {
		boolean flag = NPSOPtion.isDisplayed();
		String PassStatement = "PASS >> �Disable NPS Survey� option is present.";
		String FailStatement = "FAIL >> �Disable NPS Survey� option is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}

	public void verifyExportsetting() throws InterruptedException {
		boolean flag = ExportSettings.isDisplayed();
		String PassStatement = "PASS >> A new option Export Setting option is present.";
		String FailStatement = "FAIL >> A new option Export Setting option is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}

	public void VerifyExportSettingOpInDropDown() throws InterruptedException {
		String[] AuthTypes = { "Do not hide/mask PII", "Hide PII", "Mask PII" };
		Select sel = new Select(ExportSettingOptions);
		List<WebElement> AuthOptions = sel.getOptions();
		for (int i = 0; i < AuthOptions.size(); i++) {
			if (AuthOptions.get(i).getText().equals(AuthTypes[i])) {
				Reporter.log(AuthOptions.get(i).getText() + " " + "Option Is Present Inside Export Setting DropDown",
						true);
			} else {
				ALib.AssertFailMethod(AuthTypes[i] + " " + "Option Is Not Present Inside Export Setting DropDown");
			}
		}

	}

	public void enterAuthEndPoint(String AuthEndpoints) throws InterruptedException {
		AuthEndpoint.clear();
		sleep(2);
		AuthEndpoint.sendKeys(AuthEndpoints);
	}

	public void enterTokenEndpoint(String val) throws InterruptedException {
		TokenEndpoint.clear();
		sleep(2);
		TokenEndpoint.sendKeys(val);
	}

	public void enterClientID(String id) throws InterruptedException {
		clientID.clear();
		sleep(2);
		clientID.sendKeys(id);
	}

	public void enterClientSecret(String key) throws InterruptedException {
		ClientSecret.clear();
		sleep(2);
		ClientSecret.sendKeys(key);
	}

	public void EnteringDataInTenantID() throws InterruptedException {
		TenantIDTextBox.sendKeys("InValid Tenant ID");
		Initialization.driver.findElement(By.xpath("(//button[@id='Validate'])[1]")).click();
		sleep(4);
	}

	public void enterTenantid(String val) throws InterruptedException {
		Jc.executeScript("arguments[0].scrollIntoView()", TenantIDTextBox);
		TenantIDTextBox.clear();
		sleep(2);
		TenantIDTextBox.sendKeys(val);
	}

	public void enterTenantidBlank(String val) throws InterruptedException {
		Jc.executeScript("arguments[0].scrollIntoView()", TenantIDTextBox);
		TenantIDTextBox.clear();
		sleep(2);
		//TenantIDTextBox.sendKeys(val);
	}
	
	public void enterAppIDClear() throws InterruptedException {
		AppID.clear();
		sleep(2);
		//AppID.sendKeys(val);
	}
	
	
	public void enterAppSecret(String val) throws InterruptedException {
		AppSecret.clear();
		sleep(2);
		AppSecret.sendKeys(val);
	}

	public void enterAppID(String val) throws InterruptedException {
		AppID.clear();
		sleep(2);
		AppID.sendKeys(val);
	}

	public void verifyEnterPassword() throws InterruptedException {
		boolean flag = EnterPwd.isDisplayed();
		String PassStatement = "PASS >> Enter Password option is present.";
		String FailStatement = "FAIL >> Enter Password option is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}

	public void verifyShowpassword() throws InterruptedException {
		try {
			ShowPwd.isDisplayed();
			ALib.AssertFailMethod("FAIL >> Show Password is present.");
			sleep(2);
		} catch (Exception e) {
			Reporter.log("PASS >> Show Password is not present as expected.", true);
		}
	}

	public void verifyBypassPwd() throws InterruptedException {
		boolean flag = BypssPwd.isDisplayed();
		String PassStatement = "PASS >> The title is present.";
		String FailStatement = "FAIL >> The title is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}

	public void clickOniOSmacTab() throws InterruptedException {
		iOSmacOS.click();
		sleep(2);
	}

	public void clickOnMiscellaneous() throws InterruptedException {
		iosmiscellaneous.click();
		sleep(3);
	}

	public void clickOnAPNS() throws InterruptedException {
		APNS.click();
		sleep(2);
	}

	public void verifyOptionsInMiscelleaneous() {
		WebElement[] ele = { PreserveDataPlan, DisallowproximitySetup, RecoveryPIN };
		String[] Options = { "Preserve Data Plan", "Disallow proximity Setup", "Recovery PIN(6 Digit)" };
		for (int i = 0; i < ele.length; i++) {
			boolean val = ele[i].isDisplayed();
			String Pass = "PASS>>>" + " " + Options[i] + "Is Displayed Successfully";
			String Fail = "FAIL>>>" + " " + Options[i] + "Is Not Displayed";
			ALib.AssertTrueMethod(val, Pass, Fail);
		}
	}

	public void enterRecoveryPin(String val) throws InterruptedException {
		recoverypin.clear();
		sleep(2);
		recoverypin.sendKeys(val);
	}

	public void verifyInstructionStmt() throws InterruptedException {
		boolean flag = CertificateInstr.isDisplayed();
		String PassStatement = "PASS >> The title is present.";
		String FailStatement = "FAIL >> The title is not present.";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		sleep(2);
	}

	@FindBy(xpath = "//span[normalize-space()='Issued by']")
	private WebElement Issuedby;

	@FindBy(xpath = "//span[normalize-space()='Issued to']")
	private WebElement Issuedto;

	@FindBy(xpath = "//span[normalize-space()='Valid from']")
	private WebElement Validfrom;

	@FindBy(xpath = "//span[normalize-space()='Valid till']")
	private WebElement Validtill;

	@FindBy(xpath = "//span[@class='input-group-addon labelno-bg ct-model-label'][normalize-space()='Apple ID']")
	private WebElement AppleID;
	
	@FindBy(xpath = "//button[normalize-space()='Change Language']")
	private WebElement ChangeLanguage;
	
	@FindBy(id = "Select_language")
	private WebElement Languages;
	
	@FindBy(xpath = "//div[@id='changeLanguage_Popup']//button[@aria-label='Close']")
	private WebElement ChangeLanguagesClose;

	public void verifyOptionsInAPNS() {
		WebElement[] ele = { Issuedto, Issuedby, Validfrom, Validtill, AppleID };
		String[] Options = { "Issued to", "Issued by", "Valid from", "Valid till", "Apple ID" };
		for (int i = 0; i < ele.length; i++) {
			boolean val = ele[i].isDisplayed();
			String Pass = "PASS>>>" + " " + Options[i] + "Is Displayed Successfully";
			String Fail = "FAIL>>>" + " " + Options[i] + "Is Not Displayed";
			ALib.AssertTrueMethod(val, Pass, Fail);
		}

	}
	
	public void clickOnChangeLanguage() throws InterruptedException
	{
		ChangeLanguage.click();
		sleep(2);
	}
	
	public void validateLanguages()
	{
		String[] Types = { "English", "Spanish(espa�ola) - Beta", "German(Deutsch) - Beta", "French(le fran�ais) - Beta","Italian - Beta"};
		Select sel = new Select(Languages);
		List<WebElement> Options = sel.getOptions();
		for (int i = 0; i < Options.size(); i++) {
			if (Options.get(i).getText().equals(Types[i])) {
				Reporter.log(Options.get(i).getText() + " " + "Languages Is Present Inside Languages DropDown",
						true);
			} else {
				ALib.AssertFailMethod(Types[i] + " " + "Languages Is Not Present Inside Languages DropDown");
			}
		}

	}
	
	public void ChangeLanguageCloseBtn() throws InterruptedException
	{
		ChangeLanguagesClose.click();
		sleep(2);
	}
	
	
//============vinimanoj========================================================
	
@FindBy(xpath = "//button[normalize-space()='Account Settings']")
private WebElement clickOnAccountSettings;
	
public void AccountSettings() throws InterruptedException {
		clickOnAccountSettings.click();
		waitForLogXpathPresent("//li[@id='enrollement_rules_panel']");
		Reporter.log("PASS >> click on account settings succesfully", true);
		sleep(2);
}
	
@FindBy(xpath = "//li[@id='account_management']")
private WebElement clickOnAccountManagement;
	
public void accountManagement() throws InterruptedException {
	boolean flag=clickOnAccountManagement.isDisplayed();
	if(flag == true)
	{	   
		 sleep(2);
		 clickOnAccountManagement.click();
		 Reporter.log("PASS >> Clicked on Account managment succesfully",true);
		 sleep(3);
	}
	else
	{		
		ALib.AssertFailMethod("FAIL >> Not Clicked on Buy Subscription");
		Reporter.log("FAIL >> Not Clicked on Account manager",true);
	}
}
	
@FindBy(xpath = "//select[@id='intercom_user']")
private WebElement IntercomaccessforUserDropDown;
	
@FindBy(xpath = "//button[@id='userintercom-add-btn']")
private WebElement clickOnIntercomaccessforUserSave;
	
public void IntercomaccessforUserDropDown() throws InterruptedException {
	JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
	js.executeScript("arguments[0].scrollIntoView()", IntercomaccessforUserDropDown);
	sleep(1);
	
	Select se1 = new Select(IntercomaccessforUserDropDown);
    se1.selectByVisibleText("Mithilesh_Superuser");
	sleep(1);
	Reporter.log("PASS >> Selected mithilesh_Superuser succesfully", true);
	sleep(1);
	clickOnIntercomaccessforUserSave.click();
	sleep(2);
}
		
@FindBy(xpath = "//span[contains(text(),'Intercom access given to the user named \"Mithilesh')]")
private WebElement checkForIntercomaccessforUserConfiemationMessage;
	
public void checkForIntercomaccessforUserConfiemationMessage() {
		try {
		checkForIntercomaccessforUserConfiemationMessage.isDisplayed();
		Reporter.log("PASS >> Intercomaccess for User selected succesfuly Confirmation message displayed", true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL >> Intercomaccess for User is not selected");
		}
}	
@FindBy(xpath = "//button[@id='logoutButton']")
private WebElement logout;
	
public void logoutFromAccount() throws InterruptedException {
		logout.click();
		waitForLogXpathPresent("//a[text()='Login again']");
		Reporter.log("PASS >> sucesfully loged out from the console", true);
		sleep(2);
}
	
@FindBy(xpath = "//a[text()='Login again']")
private WebElement loginAgain;
	
public void loginAgainLink() {
		loginAgain.click();
		waitForLogXpathPresent("//button[@id='loginBtn']");
		Reporter.log("PASS >> click on loginAgain link succesfully", true);
}
	
@FindBy(xpath = "//input[@id='uName']")
private WebElement enterUserName;
	
@FindBy(xpath = "//input[@id='pass']")
private WebElement enterPassword;
	
@FindBy(xpath = "//button[@id='loginBtn']")
private WebElement loginButton;
	
public void EnterUNToLoginToConsole(String value) throws InterruptedException {
		enterUserName.sendKeys(value);
		sleep(1);
		Reporter.log("PASS >> enter user name succesfully", true);
}
	
public void EnterPassAndClickLoginToConsole(String value) throws InterruptedException {
		enterPassword.sendKeys(value);
		sleep(1);
		loginButton.click();
		waitForLogXpathPresent("//li[@id='jobSection']");
		sleep(2);
		Reporter.log("PASS >> enter un, pass and click on login button succesully", true);
}
	
@FindBy(xpath = "//div[@class='intercom-lightweight-app-launcher intercom-launcher']")
private WebElement checkForInterCom;

public void verifyForInterCom() {
		try {
		checkForInterCom.isDisplayed();
		Reporter.log("PASS >>> intercom is displaying for SuperUser", true);
		}
		catch (Exception e) {
			ALib.AssertFailMethod("FAIL >>> intercom is not displaying for SuperUser");
		}
}
	
//====== TC_ST_481 =====
	
@FindBy(xpath = "//span[text() ='Enterprise']")
private WebElement checkForEnterprise;
	
@FindBy(xpath = "//li[@id='analyticToolbarTabs']")
private WebElement clickDataAnalytics;
	
@FindBy(xpath = "//div[@id='newanalytics']")
private WebElement AddAnalytics;
	
@FindBy(xpath = "//span[text()='Add Things Analytics']")
private WebElement AddThingsAnalytics;
	
@FindBy(xpath = "//input[@id='EnableDataAnalytics']")
private WebElement checkIfEnableDataAnalyticsCheckbox;
	
public void checkForAccountIsEnterprise() throws InterruptedException {
		if(checkForEnterprise.isDisplayed()) {
			Reporter.log("PASS >> The account is Enterprise", true);
			clickOnAccountSettings.click();
			Reporter.log("PASS >> Click on account settings succesfull", true);
			waitForLogXpathPresent("//li[@id='enrollement_rules_panel']");
			clickDataAnalytics.click();
			Reporter.log("PASS >> Click on Data Analyst succesfull", true);
			waitForLogXpathPresent("//input[@id='EnableDataAnalytics']");
			boolean value=checkIfEnableDataAnalyticsCheckbox.isSelected();
			if(value == false) {
				checkIfEnableDataAnalyticsCheckbox.click();
				sleep(2);
			}
			JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
		    js.executeScript("arguments[0].scrollIntoView()", AddAnalytics);
			sleep(2);
			try {
				AddThingsAnalytics.isDisplayed();
				Reporter.log("PASS >>> AddThingsAnalytics button is present for Enterprise account", true);
			}
			catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>> AddThingsAnalytics button is not present for Enterprise account");
			}	
		}		
		else {
			ALib.AssertFailMethod("Fail >>> The account is not Enterprise ");
		}
	}
	
//====  TC_ST_489  ====
	
@FindBy(xpath = "(//a[@class='rightBtn optBtn sc-closeBoarding'])[2]")
private WebElement closeEnroleDeviceButtonForTrailConsole;
			
@FindBy(xpath = "//li[@id='buySubscription']")
private WebElement BuySubscriptionButton;
			
@FindBy(xpath = "//span[text() = 'Things License Count']")
private WebElement ThingsLicenseCount;
			
@FindBy(xpath = "//*[@id='subscr_mainContainer']/button")
private WebElement BuySubscriptionCloseBtn;
			
@FindBy(xpath = "//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")
private WebElement BuySubscriptionCloseYESBtn;
			
public void closeEnroleDeviceButtonForTrailConsole() {
			closeEnroleDeviceButtonForTrailConsole.click();
			Reporter.log("PASS >> succesfully clised enroll device popup", true);
			waitForLogXpathPresent("//button[normalize-space()='Enroll Device']");
}
			
public void BuySubscriptionButton() throws InterruptedException {
			sleep(2);
			BuySubscriptionButton.click();
			waitForLogXpathPresent("//span[text() = 'SureMDM Subscription']");
			Reporter.log("PASS >> Click on Buy Subscription Button succesfully", true);
}
			
public void verifyThingsLicenseCount() {
			boolean value= ThingsLicenseCount.isDisplayed();
			if(value == false) {
			Reporter.log("PASS >>>> ThingsLicenseCount is not displaying in Buy Subscription", true);
						
            }else {
				ALib.AssertFailMethod("FAIL >>>> Things License Count is displaying in trail account");
			}
}
			
public void BuySubscriptionCloseBtn() throws InterruptedException {
			sleep(2);
			BuySubscriptionCloseBtn.click();
			waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]");
		    Reporter.log("PASS >>>> Click on Buy subscription cllose button succesfully", true);
}
			
public void BuySubscriptionCloseYESBtn() throws InterruptedException {
			sleep(1);
			BuySubscriptionCloseYESBtn.click();
			Reporter.log("PASS >>>> Click on Buy subscription cllose YES button succesfully", true);
			sleep(2);
}			
//====== TC_ST_514 =====
						
@FindBy(xpath = "//li[@id='firmware_updates']")
private WebElement FirmwareUpdates;
		
@FindBy(xpath = "//li[@id='firmware_zebra']")
private WebElement ZebraLifeGuardOTAButton;
			 
@FindBy(xpath = "//button[@id='zebraEnrol-token-copyBtn']")
private WebElement CopyToClipboardButton;
			
@FindBy(xpath = "//input[@id='zebraEnrollmentToken']")
private WebElement DeviceTockenInput;
			
public void FirmwareUpdates() {
			JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
			js.executeScript("arguments[0].scrollIntoView()", FirmwareUpdates);
			FirmwareUpdates.click();
			waitForLogXpathPresent("//li[@id='firmware_zebra']");
			Reporter.log("PASS >> Click on Firmware Updates succesfully", true);
}
			
public void ZebraLifeGuardOTA() throws InterruptedException {
			ZebraLifeGuardOTAButton.click();
			waitForLogXpathPresent("//input[@id='zebraEnrollmentToken']");
			Reporter.log("PASS >> Clicck on Zebra LifeGuard OTA succesfully", true);
			sleep(1);
}
			
public void CopyToClipboardButton() throws InterruptedException {
			CopyToClipboardButton.click();
			sleep(1);
			String value= Initialization.driver.findElement(By.xpath("//input[@id='zebraEnrollmentToken']")).getAttribute("value");
			if(value.equals("95f80a6a-1f53-499c-95d4-24f4fa26b480")) {
				Reporter.log("PASS >>>> Copy to clipboard button with correct spelling and alignment succesful", true);
			}
			else {
				Reporter.log("FAIL >>>> Copy to clipboard button with correct spelling and alignment is not succesful", true);	
				}
				sleep(2);
}
						
//===== TC_ST_536 =====
				
@FindBy(xpath = "//label[@for='TwoFAToggle']")
private WebElement EnableTwoFactorAuthenticationRbutton;
			
@FindBy(xpath = "//input[@id='PssdVerf_2FA']")
private WebElement enterTwoFactorAuthenticationPassword;
			
@FindBy(xpath = "//button[@id='psswd_2FA_sbmt_btn']")
private WebElement TwoFactorAuthenticationVerifyButton;
			
@FindBy(xpath = "//img[@id='GoogleAUthenticatorQRCODE']")
private WebElement TwoFactorAuthenticationQRcodeImg;
			
@FindBy(xpath = "//span[normalize-space()='Incorrect Password.']")
private WebElement TwoFactorAuthenticationIncorrectPass;
			
@FindBy(xpath = "//div[@id='changePassword_header']//button[@type='button']")
private WebElement securityPopupCloseButton;
					
public void TwoFactorAuthentication() {
			TwoFactorAuthentication.click();
			waitForLogXpathPresent("//label[@for='TwoFAToggle']");
			Reporter.log("PASS >> Click on two Two Factor Authentication succesfully", true);
}
					
public void EnableTwoFactorAuthenticationRbutton() {
			EnableTwoFactorAuthenticationRbutton.click();
			waitForLogXpathPresent("//button[@id='psswd_2FA_sbmt_btn']");
			Reporter.log("PASS >> Enable Two Factor Authentication button succesfully", true);
}
					
public void enterTwoFactorAuthenticationPassword(String value) throws InterruptedException {
			enterTwoFactorAuthenticationPassword.click();
			enterTwoFactorAuthenticationPassword.sendKeys(value);
			Reporter.log("PASS >> Entered Two Factor Authentication Password Succesfully", true);
			if(value.equals("Mithilesh@123")) {
			Reporter.log("PASS >> Enterd Two Factor Authentication Password is currect", true);
			TwoFactorAuthenticationVerifyButton.click();
			waitForLogXpathPresent("//select[@id='dropDown2FA']");
			Reporter.log("PASS >> Click on Two Factor Authentication Verify Button succesfully", true);
			try {
				TwoFactorAuthenticationQRcodeImg.isDisplayed();
				Reporter.log("PASS >>>> QR code is displaying in Two Factor Authentication", true);
				}
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>> Two Factor Authentication Password is correct but QR code is not displaying");
				}
				sleep(2);
		       }else {
					TwoFactorAuthenticationVerifyButton.click();
				    TwoFactorAuthenticationIncorrectPass.isDisplayed();
					Reporter.log("FAIL >>>> Incorect password notification message is displayed", true);
				}
}
					
public void securityPopupCloseButton() {
			securityPopupCloseButton.click();
			waitForLogXpathPresent("//li[@id='dashboardSection']");
			Reporter.log("PASS >> Click on Security Close button succesful", true);
}
					
@FindBy(xpath = "//li[@id='homeSection']")
private WebElement clickHome;
					
public void clickHome() throws InterruptedException {
			sleep(3);
			clickHome.click();
			Reporter.log("PASS >> Click on home button succesfully", true);
}
					
//===== TC_ST_722 =====
					
@FindBy(xpath = "//button[@id='account_management_content_deleteaccount']")
private WebElement accountManagementDeleteaccount;
					
@FindBy(xpath = "//span[@id='check_isPwdVisible']")
private WebElement accountManagementAPIeye;
					
@FindBy(xpath = "//div[@id='copytoClipboard_apiKey']")
private WebElement copytoClipboard_apiKey;
					
@FindBy(xpath = "//span[normalize-space()='Text copied to clipboard']")
private WebElement verifyAPIkeyValueIsCopied;
					
public void accountManagementDeleteaccount() {
			JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
		    js.executeScript("arguments[0].scrollIntoView()", accountManagementDeleteaccount);
			Reporter.log("PASS >> Scroll down till account Management Delete button Succesful", true);
}
					
public void accountManagementAPIeye() throws InterruptedException {
			accountManagementAPIeye.click();
			Reporter.log("PASS >> Click on account Management API eye button Succesful", true);
			sleep(1);
}
				
public void copytoClipboard_apiKey() throws InterruptedException {
			boolean value= copytoClipboard_apiKey.isDisplayed();
			if(value== true) {
			copytoClipboard_apiKey.click();
			Reporter.log("PASS >>>> API key Copy button is able to click After click on API Key", true);
			sleep(1);
			}else {
				ALib.AssertFailMethod("FAIL >>>> API key Copy button is NOT able to click After click on API Key");
			}
	}
					
public void verifyAPIkeyValueIsCopied() throws InterruptedException {
			try {
				verifyAPIkeyValueIsCopied.isDisplayed();
				Reporter.log("PASS >>>> API key Copied Succesfully notification message is displayed", true);
				}
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> API key Copied notification button is not displayed");						
				}
				sleep(4);
}
					
//======== TC 742 ========
@FindBy(xpath = "//div[text()=' to see instructions to renew the existing APNS Certificate.']")
private WebElement VerifyTextIsPresentIniOSiPadOSmacOSSettings;
					
@FindBy(xpath = "//a[text()='Click here']")
private WebElement ClickOnIniOSiPadOSmacOSSettingsClickHere;
					
@FindBy(xpath = "//h1[text()='How to register APNs Certificate For Apple Devices In SureMDM?']")
private WebElement SwitchToIniOSiPadOSmacOSSettingsToAnotherWindow;
					
public void VerifyTextIsPresentIniOSiPadOSmacOSSettings() throws InterruptedException {
			try {
				VerifyTextIsPresentIniOSiPadOSmacOSSettings.isDisplayed();
				Reporter.log("PASS >>>> to see instructions to renew the existing APNS Certificate. is displayed", true);
			    }
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> Proper text is not displayed");						
				}
				sleep(1);
}
					
public void ClickOnIniOSiPadOSmacOSSettingsClickHere() throws InterruptedException {
			ClickOnIniOSiPadOSmacOSSettingsClickHere.click();
			Reporter.log("PASS >> Click on Click Here link succesfully", true);
			sleep(3);
}
					
public void SwitchToIniOSiPadOSmacOSSettingsToAnotherWindow() throws InterruptedException {
		Set<String> set=Initialization.driver.getWindowHandles();
		Iterator<String> id=set.iterator();
		String parentwinID = id.next();
		String childwinID = id.next();
		Initialization.driver.switchTo().window(childwinID);
		String URL=Initialization.driver.getCurrentUrl();
		String ExpectedValue = "https://knowledgebase.42gears.com/article/registering-apns-certificate-for-apple-devices-in-suremdm/";
		String PassStatement = "PASS >> Current url for Training is: "+URL;
		String FailStatement = "FAIL >> Current url for Training is Not Active";
	    ALib.AssertEqualsMethod(URL, ExpectedValue, PassStatement, FailStatement);
						
		String Text = Initialization.driver.findElement(By.xpath("//h1[text()='How to register APNs Certificate For Apple Devices In SureMDM?']")).getText();
		Reporter.log("PASS >>>> New window is opened succesfully", true);
	
						
		if(Text.contains("How to register ")) {
		Reporter.log("PASS >>>> Currect text is displaying ie., proper window is displaying", true);
		}
		else {
		ALib.AssertFailMethod("FAIL >>>> We are not in Proper window");
		}

		sleep(2);
		Initialization.driver.close();
		Initialization.driver.switchTo().window(parentwinID);
		waitForLogXpathPresent("//li[@id='homeSection']");
		try {
			clickHome.isDisplayed();
			Reporter.log("PASS >> Switch back to parant window succesful", true);
			} 
		catch (Exception e) {
	    ALib.AssertFailMethod("FAIL >> Switch back to parant window is not succesfull");
		}
						
	}
										
//===== tc No 744 =====
									
@FindBy(xpath = "//div[text()='Certificate is not configured, please configure it for managing iOS and macOS devices. ']")
private WebElement VerifyProperTextIsPresentIniOSiPadOSmacOSSettings;
					
					
public void VerifyProperTextIsPresentIniOSiPadOSmacOSSettings() throws InterruptedException {
		try {
		VerifyProperTextIsPresentIniOSiPadOSmacOSSettings.isDisplayed();
		Reporter.log("PASS >>>> Certificate is not configured, please configure it. is displaying succesfully", true);
		}
		catch (Exception e) {
		ALib.AssertFailMethod("FAIL >>>> Certificate is not configured, please configure it. is not displaying");
		}
		sleep(2);
}
										
//===== TC No 325 =====
@FindBy(xpath = "(//*[@id='secretKeyBlock']/div)[1]")
private WebElement SureLockAnalyticSecreteKeyShowButton;
					
@FindBy(xpath = "(//span[@class='secValue'])[1]")
private WebElement SureLockAnalyticSecreteKeyText;
					
@FindBy(xpath = "//input[@id='uName']")
private WebElement SureMdmConsoleUN;
					
@FindBy(xpath = "//input[@id='pass']")
private WebElement SureMdmConsolePASS;
					
@FindBy(xpath = "//button[text()='Login']")
private WebElement SureMdmConsoleLogin;
					
String value1= "d27a9231";	
String value2;
public void SureLockAnalyticSecreteKeyShowButton() throws InterruptedException {
			SureLockAnalyticSecreteKeyShowButton.click();
			sleep(2);
			Reporter.log("PASS >> Click on SureLock Analytic SecreteKey Show Button", true);
			value1 = SureLockAnalyticSecreteKeyText.getText();
			Reporter.log("PASS >> Main account SureLock Analytic Secrete Key - "+value1, true);
	}
					
public void LoginWithAnotherAccount() throws InterruptedException{
		    JavascriptExecutor jse = (JavascriptExecutor) Initialization.driver;
			jse.executeScript("window.open()");
			Reporter.log("PASS >> New window opend);ed succesfully", true);
			Set<String> set=Initialization.driver.getWindowHandles();
			Iterator<String> id=set.iterator();
			String parentwinID= id.next();
			String childwinID= id.next();
						
			Initialization.driver.switchTo().window(childwinID);
			sleep(1);
			Initialization.driver.get("https://worldoftesting.eu.suremdm.io/console/");
		    waitForLogXpathPresent("//button[text()='Login']");
			sleep(2);
			Reporter.log("PASS >> Now the focus is on child window", true);
						
						
			SureMdmConsoleUN.sendKeys("abdulemithilesh@gmail.com");
			Reporter.log("PASS >> UN entered succesfully for new console", true);
			SureMdmConsolePASS.sendKeys("Mithilesh@123");
			Reporter.log("PASS >> PASS entered succesfully for new console", true);
			SureMdmConsoleLogin.click();
			Reporter.log("PASS >> Click on login button succesfully", true);
			waitForLogXpathPresent("//li[@id='dashboardSection']");
		    Reporter.log("PASS >> New console Home page is displaying succesfully", true);
		    sleep(2);
		
					
}
					
public void verifySecreteKeyIsDifferentForDifferentaccounts() throws InterruptedException {
			SureLockAnalyticSecreteKeyShowButton.click();
			sleep(1);
			Reporter.log("PASS >> Click on SureLock Analytic SecreteKey Show Button", true);
			value2 = SureLockAnalyticSecreteKeyText.getText();
			Reporter.log("PASS >> Trail account SureLock Analytic Secrete Key - "+value2, true);
//			System.out.println("PASS >> Trail account SureLock Analytic Secrete Key - "+value2);
			if(value1.equals(value2)) {
			Assert.fail("FAIL >>>> Main account Secrete Key value is equal to Trail account Secrete Key");
			}
			else {
			Reporter.log("PASS >>>> SureLock Analytic Secrete Key is different for different accounts", true);
			}
			Set<String> set=Initialization.driver.getWindowHandles();
			Iterator<String> id=set.iterator();
			String parentwinID= id.next();
			String childwinID= id.next();
			Initialization.driver.close();
			Initialization.driver.switchTo().window(parentwinID);
			Reporter.log("PASS >> Switched to parent window succesfully", true);
			sleep(2);
						
}
										
//====== TC NO 806 =====
					
					
@FindBy(xpath = "//a[text()='Enterprise Integrations']")
private WebElement EnterpriseIntegrations;
					
@FindBy(xpath = "//a[text()='NAC Integration']")
private WebElement NACIntegration;
					
@FindBy(xpath = "//span[text()='Download Template']")
private WebElement NACIntegrationDownloadTemplate;
					
@FindBy(xpath = "//div[@id='NAC_Import']")
private WebElement NACIntegrationImpport;
					
public void EnterpriseIntegrations() {
			EnterpriseIntegrations.click();
			waitForLogXpathPresent("//a[text()='NAC Integration']");
			Reporter.log("PASS >> Click on Enterprise Integrations is succesful", true);
}
					
public void NACIntegration() {
			NACIntegration.click();
			waitForLogXpathPresent("//span[text()='Download Template']");
			Reporter.log("PASS >> Click On NAC Integration is succesfull", true);
}
					
public void NACIntegrationDownloadTemplate() {
			NACIntegrationDownloadTemplate.click();
			Reporter.log("PASS >>>> Download Template is succesfull", true);
}
					
public void NACIntegrationImpport() {
			try {
			NACIntegrationImpport.isDisplayed();
			Reporter.log("PASS >> Inport button is present in NAC Integration", true);
			NACIntegrationImpport.click();
			sleep(3);
			Reporter.log("PASS >> Click on Inport button in NAC Integration", true);
	      }
	       catch (Exception e) {
		   ALib.AssertFailMethod("FAIL >> Import button is not present in NAC Integration");
		}
}
					
public void clickbeowser() throws IOException, InterruptedException {
           sleep(2);
           uploadVppFolderBrowserButton.click();
			sleep(2);
}
					
public void browserIntegrationImport(String File) throws IOException, InterruptedException {
			sleep(5);
			Runtime.getRuntime().exec(File);
			Reporter.log("PASS >>>> File Uploaded succesfully", true);
			sleep(5);
}
//======== TC No 821 ============
										
@FindBy(xpath = "//li[@id='customiseAppsTab']")
private WebElement customiseAppsTab;
					
@FindBy(xpath = "//li[@id='windows_custom_app_tab']")
private WebElement windows_custom_app_tab;
					
@FindBy(xpath = "//div[@id='customiseApps_tab']//select[@id='app_to_customize']")
private WebElement selectAppToCustomize;
					
@FindBy(xpath = "//input[@id='win_custom_app_downloadlink']")
private WebElement customAppDownloadlink;
					
@FindBy(xpath = "//input[@id='win_customiseApps_tab_apply']")
private WebElement customiseAppsTabApplyGenerateButton;
					
@FindBy(xpath = "//div[@id='win_advsettng_upload_loader']//div[@class='loader'][normalize-space()='Loader']")
private WebElement waitTillGenerate;
					
@FindBy(xpath = "//span[contains(text(),'App customization successful. Please click on down')]")
private WebElement generatedNotificationMessage;
					
@FindBy(xpath = "//input[@id='win_customizeApps_tab_download']")
private WebElement WindowsAppDownloadButton;
					
public void customiseAppsTab() {
			customiseAppsTab.click();
			waitForLogXpathPresent("//li[@id='windows_custom_app_tab']");
			Reporter.log("PASS >> Click on customise Apps Tab succesfully", true);
}
					
public void windows_custom_app_tab() {
			windows_custom_app_tab.click();
			waitForLogXpathPresent("//div[@id='customiseApps_tab']//select[@id='app_to_customize']");
			Reporter.log("PASS >> Click on windows custom app tab succesfully");
}
					
public void SelecGlobalIPAddressDropDown() throws InterruptedException {
			Select DropIPAddress = new Select(selectAppToCustomize);
			DropIPAddress.selectByVisibleText("SureMDM Nix Agent");
			Reporter.log("PASS >> SureMDM Nix Agen selected succesfully", true);
			sleep(2);
}

public void customAppDownloadlink() throws InterruptedException {
					 
			sleep(1);
			customAppDownloadlink.click();
			sleep(1);
			customAppDownloadlink.clear();
			//customAppDownloadlink.sendKeys(Config.AppDOwnloadURL);
			Reporter.log("PASS >> Enter custom App Download link succesfully", true);
			sleep(2);
}
					
public void customiseAppsTabApplyGenerateButton() throws InterruptedException {
			sleep(2);
			customiseAppsTabApplyGenerateButton.click();
			sleep(2);
			Reporter.log("PASS >> Click on Customise Apps Tab Apply Generate Button succesfully", true);
			waitForXpathPresent("//span[contains(text(),'App customization successful. Please click on down')]");
			Reporter.log("PASS >> App customization successful. Please click on download message is Displayed succesfully", true);
}
					
public void WindowsAppDownloadButton() throws InterruptedException {
			sleep(2);
			WindowsAppDownloadButton.click();
			sleep(2);
		    Reporter.log("PASS >>>> Nix geting downloaded in .zip format", true);
			waitForXpathPresent("//li[@id='homeSection']");
			sleep(2);
}
										
//======== TC 849 ========
										
@FindBy(xpath = "//li[@id='Miscellaneous_Settings']")
private WebElement Miscellaneous_Settings;

@FindBy(xpath = "//input[@id='miscellaneous_apply']")
private WebElement Miscellaneous_SettingsApplyButton;
					
@FindBy(xpath = "//select[@id='mask_sett']")
private WebElement secureDataEXportedDropDown;
					
@FindBy(xpath = "//span[contains(text(), 'Exported data may contain PII and 42Gears ')]")
private WebElement verifyExporteddatamaycontainDisplayed;
					
@FindBy(xpath = "//span[text()= 'The columns containing any personal information will be skipped while exporting the data.']")
private WebElement verifyExportedDoNotForHidePIITDisplayed;
					
@FindBy(xpath = "//span[text()= 'The columns containing any personal information will be masked in the exported files.']")
private WebElement verifyExportedDoNotForMaskPIITDisplayed;
					
					
public void Miscellaneous_Settings() throws InterruptedException {
			Miscellaneous_Settings.click();
			waitForXpathPresent("//input[@id='advDeviceManagement']");
			Reporter.log("PASS >> Click on Miscellaneous Settings succesfully", true);
			sleep(1);
}
					
public void scrolldowntoMiscellaneous_SettingsApplyButton() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) Initialization.driver;
			js.executeScript("arguments[0].scrollIntoView()", Miscellaneous_SettingsApplyButton);
			Reporter.log("PASS >> Scroldown to bottom succesfully", true);
			sleep(2);
}
					
public void secureDataEXportedDropDown() throws InterruptedException {
			Select DropIPAddress = new Select(secureDataEXportedDropDown);
			DropIPAddress.selectByVisibleText("Do not hide/mask PII");
			Reporter.log("PASS >> Do not hide/mask PII is selected succesfully", true);
			sleep(2);
}
					
public void verifyExportedDoNotForMaskHidePIITDisplayed() {
						
			try {
				verifyExporteddatamaycontainDisplayed.isDisplayed();
				Reporter.log("PASS >>>> Exporting data when Donot Mask Hide PIIT and 42Gears is displaying succesfully", true);
			}
			catch (Exception e) {
			ALib.AssertFailMethod("FAIL >>>> Exporting data when Donot Mask Hide PIIT is not displaying");
			}
	}

public void verifyExportedForHidePIITDisplayed() throws InterruptedException {
			Select DropIPAddress = new Select(secureDataEXportedDropDown);
			DropIPAddress.selectByVisibleText("Hide PII");
			Reporter.log("PASS >> HidePII is selected succesfully", true);
			sleep(2);
}
					
public void verifyExportedDoNotForHidePIITDisplayed() {
						
			try {
				verifyExportedDoNotForHidePIITDisplayed.isDisplayed();
				Reporter.log("PASS >>>> Exporting data when Hide PIIT and 42Gears is displaying succesfully", true);
			}
			catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> Exporting data when Hide PIIT is not displaying");
		    }
}
					
public void verifyExportedForMaskPIITDisplayed() throws InterruptedException {
			Select DropIPAddress = new Select(secureDataEXportedDropDown);
			DropIPAddress.selectByVisibleText("Mask PII");
			Reporter.log("PASS >> mask PII is selected succesfully", true);
			sleep(2);
}
					
public void verifyexportedForMaskPIITDisplayed() {
						
			try {
				verifyExportedDoNotForMaskPIITDisplayed.isDisplayed();
				Reporter.log("PASS >>>> Exporting data when Mask and 42Gears is displaying succesfully", true);
		}
			catch (Exception e) {
			ALib.AssertFailMethod("FAIL >>>> Exporting data when Mask is not displaying");
		}
}
			
// ====== TC 850 =======
					

@FindBy(xpath = "//li[@id='Dep_li_tab']")
private WebElement iOSmacTabDepTab;
					
@FindBy(xpath = "//div[@id='dep_profile_edit_advncstng']")
private WebElement DepProfileAddButton;
					
@FindBy(xpath = "//label[@for='enable_shared_ipad']")
private WebElement EnableSharediPadRbutton;
					
@FindBy(xpath = "//span[text()='Shared iPad Property ']")
private WebElement SharediPadProperty ;
					
@FindBy(xpath = "//span[text()='Value ']")
private WebElement SharediPadValue ;
					
@FindBy(xpath = "//div[@id='dep_profile_popup']//button[@aria-label='Close']")
private WebElement DEEPprofilePupupClosebutton;
					
public void iOSmacTabDepTab() throws InterruptedException {
			iOSmacTabDepTab.click();
			waitForXpathPresent("//li[@id='ios_Dep_profile_tab']");
			Reporter.log("PASS >> Click on DEP tab succesfully", true);
			sleep(1);
}
					
public void DepProfileAddButton() throws InterruptedException {
			DepProfileAddButton.click();
			waitForXpathPresent("//input[@id='profile_name']");
			Reporter.log("PASS >> Click DEP profile Add button succesfully", true);
			sleep(1);
}
					
public void EnableSharediPadRbutton() throws InterruptedException {
			EnableSharediPadRbutton.click();
			sleep(2);
		    Reporter.log("PASS >> Enable Shared iPad Radio Button succesfully", true);
}
					
public void verifyIfSharediPadValueAndValuedisplayed() throws InterruptedException {
			try {
				SharediPadProperty.isDisplayed();
				SharediPadValue.isDisplayed();
				Reporter.log("PASS >>>> Shared iPad Property and Shared iPad Value displayed succesfuly", true);
			} 
			catch (Exception e) {
			ALib.AssertFailMethod("FAIL >>>> Shared iPad Property and Shared iPad Value is not displaying");
			}
			sleep(2);
}
					
public void DEEPprofilePupupClosebutton() throws InterruptedException {
			DEEPprofilePupupClosebutton.click();
			Reporter.log("PASS >> Click on DEEP profile Pupup Close button succesful", true);
			sleep(3);
}
										
//========= TC NO 851 =========
										
@FindBy(xpath = "//label[@for='is_supervised']")
private WebElement SupervisionEnableButton;
					
@FindBy(xpath = "//label[@for='is_mdm_removable']")
private WebElement MDMProfileRemovable;
					
@FindBy(xpath = "//label[@for='enable_shared_ipad']")
private WebElement EnableSharediPadText;
					
public void SupervisionEnableButton() throws InterruptedException {
			SupervisionEnableButton.click();
			sleep(2);
		    Reporter.log("PASS >> Click on Supervision Enable Button Succesfully", true);
}
					
public void verifyMDMProfileRemovableTextAredisable() throws InterruptedException {
		    boolean value1= MDMProfileRemovable.isSelected();
			if(value1== false) {
			Reporter.log("PASS >>>> MDM Profile Removable is disabled after disableing of supervised", true);
			}
			else {
			ALib.AssertFailMethod("FAIL >>>> MDM Profile Removableis disabled after disableing of supervised");
			}
			sleep(2);						
}
										
public void verifyEnableSharediPadTextAredisable() throws InterruptedException {
			boolean value2= EnableSharediPadText.isSelected();
			if(value2== false) {
			Reporter.log("PASS >>>> Enable Shared iPad Text is disabled after disableing of supervised", true);
			}
			else {
			ALib.AssertFailMethod("FAIL >>>> Enable Shared iPad Text disabled after disableing of supervised");
			}
			sleep(2);
						
}
										
//=========== TC 866 ===========
					
@FindBy(xpath = "//li[@id='vpp_tab_account_settings']")
private WebElement IOSvppButton;
					
@FindBy(xpath = "//button[@id='vppModifyTokenBtn']")
private WebElement vppModifyTokenBtn;
					
@FindBy(xpath = "//button[@id='vppDeleteTokenBtn']")
private WebElement vppDeleteTokenBtn;
					
@FindBy(xpath = "//div[@id='ConfirmationDialog']//button[@type='button'][normalize-space()='Yes']")
private WebElement vppDeleteYesBtn;
					
@FindBy(xpath = "//span[normalize-space()='VPP token deleted successfully.']")
private WebElement vppDeleteTokenNotification;
					
@FindBy(xpath = "//button[@id='uploadVppSerToken']")
private WebElement uploadVppSerToken;
					
@FindBy(xpath = "(//div[@class='uploadBtnCont input-group-addon ct-upload-btn-cover'])[2]")
private WebElement uploadVppFolderBrowserButton;
					
@FindBy(xpath = "//span[normalize-space()='VPP token uploaded successfully.']")
private WebElement uploadVppSucessfulNotficationMessagen;
					
@FindBy(xpath = "//div[text()='VPP token expires on ']")
private WebElement VerifyExpirationdateforVPPtoken;
					
public void IOSvppButton() throws InterruptedException {
			IOSvppButton.click();
			waitForXpathPresent("//p[text()='VPP']");
			Reporter.log("PASS >> Click on VPP succesfully", true);
			sleep(1);
}
					
public void vppDeleteTokenBtn() throws InterruptedException {
			if(vppDeleteTokenBtn.isDisplayed()) {
			vppDeleteTokenBtn.click();
			waitForXpathPresent("//p[text()='Are you sure you want to delete this VPP token?']");
			Reporter.log("PASS >> Click On vpp Delete Token Btn succesfully", true);
			vppDeleteYesBtn.click();
			Reporter.log("PASS >> Click On vpp Delete Token YES Btn succesfully", true);
			sleep(3);
			try {
				vppDeleteTokenNotification.isDisplayed();
				Reporter.log("PASS >> Vpp Delete Token succesfully notification displayed", true);
				sleep(2);
				} 
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> Vpp Delete Token succesfully notification is not displayed");
				}
            }
		    else {
				sleep(2);
				}
}
					
public void clilckuploadVppSerToken() throws InterruptedException {
			sleep(2);
			uploadVppFolderBrowserButton.click();
			Reporter.log("PASS >> Click on upload Vpp Folder Browser Button succesfully", true);
}
					
public void uploadVppSerTokenBtn() throws InterruptedException {
			sleep(2);
			uploadVppSerToken.click();
			Reporter.log("PASS >> Click on VPP upload Button succesfully", true);
			sleep(1);
}
					
public void uploadVppSucessfulNotficationMessagen() {
			try {
				uploadVppSucessfulNotficationMessagen.isDisplayed();
				Reporter.log("PASS >> Vpp file upload succesfully notification displayed", true);
				} 
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> Vpp file upload succesfully notification is not displayed");
				}
}
					
public void VerifyExpirationdateforVPPtoken() throws InterruptedException {
			try {
				sleep(2);
				VerifyExpirationdateforVPPtoken.isDisplayed();
				Reporter.log("PASS >> Expiration date for VPP token is displayed succesfully", true);
				sleep(2);
		        } 
				catch (Exception e) {
			    ALib.AssertFailMethod("FAIL >>>> Expiration date for VPP token is not displayed");
				}
							
}
										
//============ 867 ===========
					
					
public void checkIsUploadButtonisPresent(String File) throws InterruptedException, Exception { 
			if(uploadVppSerToken.isDisplayed()) {
			sleep(2);
			uploadVppFolderBrowserButton.click();
			sleep(8);
			Runtime.getRuntime().exec(File);
			Reporter.log("PASS >>>> File Uploaded succesfully", true);
			sleep(7);			
			uploadVppSerToken.click();
			Reporter.log("PASS >> Click on VPP upload Button succesfully", true);
			sleep(5);
			}
}
					
public void vppDeleteTokenButton() {
		    vppDeleteTokenBtn.click();
			waitForXpathPresent("//p[text()='Are you sure you want to delete this VPP token?']");
			Reporter.log("PASS >> Click On vpp Delete Token Btn succesfully", true);
}
					
public void vppDeleteYesBtn() throws InterruptedException {
			sleep(2);
			vppDeleteYesBtn.click();
			Reporter.log("PASS >> Click On vpp Delete Token YES Btn succesfully", true);
			sleep(3);
}
					
public void vppDeleteTokenNotification() {
			try {
				vppDeleteTokenNotification.isDisplayed();
				Reporter.log("PASS >> Vpp Delete Token succesfully notification displayed", true);
				sleep(2);
				} catch (Exception e) {
				ALib.AssertFailMethod("FAIL >> Vpp Delete Token succesfully notification is not displayed");
				}
						
}
					
public void VerifyExpirationdateNotShownAfterdelete() throws InterruptedException {
			try {
				sleep(2);
				VerifyExpirationdateforVPPtoken.isDisplayed();
				ALib.AssertFailMethod("FAIL >>>> Expiration date for VPP token is not displayed");
				} 
				catch (Exception e) {
				Reporter.log("PASS >>>> Expiration date for VPP token is not displayed", true);
				sleep(2);
				}
							
}
				
//========== 875 ==========

@FindBy(xpath = "//a[text()='DEP Servers']")
private WebElement DEPServers;
					
@FindBy(xpath = "//div[@id='viewDevices-btn']")
private WebElement ViewDevices;
					
@FindBy(xpath = "//table[@id='DEPServerDetailTable']//tr[@data-index='0']")
private WebElement selectViewDevicesFirstdevice;
					
@FindBy(xpath = "//div[@id='ExportDeviceList']")
private WebElement diviceListExport;
					
@FindBy(xpath = "(//thead[@id='tableHeader']//div[text()='Device Name'])[1]")
private WebElement deviceListDeviceName;
					
@FindBy(xpath = "(//thead[@id='tableHeader']//div[text()='Serial Number'])[1]")
private WebElement deviceListSerialNumber;		
					
@FindBy(xpath = "//div[@class='modal-body']//div[@class='tab-pane-body']//input[@placeholder='Search']")
private WebElement deviceListsearch;	
					
@FindBy(xpath = "//div[@id='device_list_popup']//button[@aria-label='Close']")
private WebElement deviceListpopupCloseBtn;	
										
public void DEPServers() throws InterruptedException { 
			DEPServers.click();
			sleep(2);
			waitForXpathPresent("//div[@id='viewDevices-btn']");
			Reporter.log("PASS >> Click on DEPServers succesfuly", true);
}
					
public void selectViewDevicesFirstdevice() throws InterruptedException {
			selectViewDevicesFirstdevice.click();
			sleep(1);
			Reporter.log("PASS >> Device selected succesfully", true);
}
					
public void ViewDevices() throws InterruptedException {
	        sleep(2);
			ViewDevices.click();
			waitForXpathPresent("//h4[@id='deviceListTitle']");
			sleep(2);
			Reporter.log("PASS >> Device list popup displayed succesfully", true);
}
					
public void diviceListExport() {
			try {
				diviceListExport.isDisplayed();
				Reporter.log("PASS >>>> Export is present in Device list popup", true);
				} 
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> Export is not present in Device list popup");
				}
}
					
public void deviceListDeviceName() {
			try {
				deviceListDeviceName.isDisplayed();
				Reporter.log("PASS >>>> DeviceName is present in Device list popup", true);
				} 
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> DeviceName is not present in Device list popup");
				}
}
										
public void deviceListSerialNumber() {
			try {
				deviceListSerialNumber.isDisplayed();
				Reporter.log("PASS >>>> Serial Number is present in Device list popup", true);
				} 
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> Serial Number is not present in Device list popup");
				}
}
					
public void deviceListsearch() {
			try {
				deviceListsearch.isDisplayed();
				Reporter.log("PASS >>>> deviceList Search is present in Device list popup", true);
				} 
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> deviceList Search is not present in Device list popup");
				}
}
					
public void deviceListpopupCloseBtn() throws InterruptedException {
			sleep(4);
			deviceListpopupCloseBtn.click();
			Reporter.log("PASS >>>> Clilck on device List Popup CloseBtn succesfully", true);
}
					
					
					
//======= TC 877 ========
									
@FindBy(xpath = "//span[normalize-space()='No Data available.']")
private WebElement NoDataavailableNotificationMessage;	
					
public void NoDataavailableNotificationMessage() {
			try {
				NoDataavailableNotificationMessage.isDisplayed();
				Reporter.log("PASS >>>> No Data available erorr message displayed succesfully", true);
				} 
				catch (Exception e) {
				ALib.AssertFailMethod("FAIL >>>> No Data available erorr message is not displayed");
				}
}
										
//======= TC 878 ========
					
public void clickDiviceListExport() throws InterruptedException {
			sleep(2);
			diviceListExport.click();
			Reporter.log("PASS >>>> The data is exported in .CSV format", true);
			sleep(2);
}
////////////////////////////////////////////////////////////////////////////					
	
public void AdminCenterTrials()throws InterruptedException{

	//boolean flag=AdminCenter.isDisplayed();
	//if(flag)
	//{	   		
		sleep(5);
	 String STD=Initialization.driver.findElement(By.xpath("//h1[@class='login-style__Heading-sc-1kwv0tj-1 eXdArh']")).getText();
	System.out.println(STD);
		
		//Reporter.log("PASS >> Text is Displayed : "+option,true);
		sleep(2); 		
//	}
//	else
//	{		
//		//ALib.AssertFailMethod("FAIL >> Admin Center is not Displayed");
//		Reporter.log("FAIL >> Admin Center is not Displayed",true);
//	}
}	



























//=========================================Mithilesh (May-2021)============================================
}

