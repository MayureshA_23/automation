package PageObjectRepository;

import java.io.IOException;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.WebDriverCommonLib;

public class Nixdatausage extends WebDriverCommonLib
{
   public void ClickOnElementsInSettings(String Text) throws InterruptedException
   {
	   Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='"+Text+"']").click();
	   sleep(3);
   }
   
   float overalldatausedInMB;
   float nixdatausedInMB;
   float GooglePlayServiceDataUsedInMB;
   float overalldatausedInMBAfter30Mins;
   float nixdatausedInMBAfter30Mins;
   float GooglePlayServiceDataUsedInMBAfter30Mins;
   
   public void ReadingOverAllDataUsage() throws IOException  
   {
	   String text = Initialization.driverAppium.findElementByXPath("(//android.widget.TextView[@index='0'])[2]").getText();
       String[] data = text.split(" ");
       float DataUsed = Float.parseFloat(data[0]);
	   if(text.contains("GB"))
       {
		   overalldatausedInMB=(int) (DataUsed*(1000));
       }
	   else if(text.contains("MB"))
	   {
		   overalldatausedInMB=(int) DataUsed;
	   }
	   Reporter.log("Over All Data Usage Is :"+overalldatausedInMB+" MB",true );
	   Runtime.getRuntime().exec("adb shell input keyevent 3");
	   
   }
   
   public void ReadingDataUsageOfNixApplication() throws IOException
   {
	   String text = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Mobile data']/../android.widget.TextView[2]").getText();
	   String[] data = text.split(" ");
	   float DataUsed = Float.parseFloat(data[0]);
	   if(text.contains("GB"))
       {
		   nixdatausedInMB=(int) (DataUsed*(1000));
       }
	   else if(text.contains("MB"))
	   {
		   nixdatausedInMB= DataUsed;
	   }
	   else if(text.contains("KB"))
	   {
		   nixdatausedInMB=(DataUsed/(1000));
	   }
	   	  
	   Reporter.log("Nix Data Usage Is :"+nixdatausedInMB+" MB",true );
	   Runtime.getRuntime().exec("adb shell input keyevent 3");
   }
   public void ReadingDataUsageOfGooglePlayServiceApplication() throws IOException
   {
	   String text = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Mobile data']/../android.widget.TextView[2]").getText();
	   String[] data = text.split(" ");
	   float DataUsed = Float.parseFloat(data[0]);
	   if(text.contains("GB"))
       {
		   GooglePlayServiceDataUsedInMB=(int) (DataUsed*(1000));
       }
	   else if(text.contains("MB"))
	   {
		   GooglePlayServiceDataUsedInMB= DataUsed;
	   }
	   else if(text.contains("KB"))
	   {
		   GooglePlayServiceDataUsedInMB= (DataUsed/(1000));
	   }
	   
	   Reporter.log("Google Play Service Data Usage Is :"+GooglePlayServiceDataUsedInMB+" MB",true );
	   Runtime.getRuntime().exec("adb shell input keyevent 3");
   }
   
   public void ReadingOverAllDataUsageAfter30Mins() throws IOException
   {
	   String text = Initialization.driverAppium.findElementByXPath("(//android.widget.TextView[@index='0'])[2]").getText();
       String[] data = text.split(" ");
       float DataUsed = Float.parseFloat(data[0]);
	   if(text.contains("GB"))
       {
		   overalldatausedInMBAfter30Mins=(int) (DataUsed*(1000));
       }
	   else if(text.contains("MB"))
	   {
		   overalldatausedInMBAfter30Mins=(int) DataUsed;
	   }
	   Reporter.log("Over All Data Usage After 30 Mins Is :"+overalldatausedInMBAfter30Mins+" MB" ,true);
	   Runtime.getRuntime().exec("adb shell input keyevent 3");
   }
   
   public void ReadingDataUsageOfNixApplicationAfter30Mins() throws IOException
   {
	   String text = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Mobile data']/../android.widget.TextView[2]").getText();
	   String[] data = text.split(" ");
	   float DataUsed = Float.parseFloat(data[0]);
	   if(text.contains("GB"))
       {
		   nixdatausedInMBAfter30Mins=(int) (DataUsed*(1000));
       }
	   else if(text.contains("MB"))
	   {
		   nixdatausedInMBAfter30Mins= DataUsed;
	   }
	   else if(text.contains("KB"))
	   {
		   nixdatausedInMBAfter30Mins=(DataUsed/(1000));
	   }
	   	  
	   Reporter.log("NIx Data Usage After 30 Mins Is :"+nixdatausedInMBAfter30Mins+" MB" ,true);
	   Runtime.getRuntime().exec("adb shell input keyevent 3");
   }
   
   public void ReadingDataUsageOfGooglePlayServiceApplicationAfter30Mins() throws IOException
   {
	   String text = Initialization.driverAppium.findElementByXPath("//android.widget.TextView[@text='Mobile data']/../android.widget.TextView[2]").getText();
	   String[] data = text.split(" ");
	   float DataUsed = Float.parseFloat(data[0]);
	   if(text.contains("GB"))
       {
		   GooglePlayServiceDataUsedInMBAfter30Mins=(int) (DataUsed*(1000));
       }
	   else if(text.contains("MB"))
	   {
		   GooglePlayServiceDataUsedInMBAfter30Mins= DataUsed;
	   }
	   else if(text.contains("KB"))
	   {
		   GooglePlayServiceDataUsedInMBAfter30Mins= (DataUsed/(1000));
	   }
	   
	   Reporter.log("Google Play Service Data Usage After 30 Mins Is :"+GooglePlayServiceDataUsedInMBAfter30Mins+" MB",true );
	   Runtime.getRuntime().exec("adb shell input keyevent 3");
   }
   
   public void FindingOverAllDataDifference()
   {
	   float OverAllDataDifference = overalldatausedInMBAfter30Mins-overalldatausedInMB;
	   Reporter.log("Over All Data Usage Difference Is :"+OverAllDataDifference+" MB",true);
   }
   
   public void FindingNixDataUsageDifference()
   {
	  float NixDataDifference = nixdatausedInMBAfter30Mins-nixdatausedInMB;
	  Reporter.log("Nix Data Usage Difference Is :"+NixDataDifference+" MB",true);
   }
   
   public void FindingGooglePlayServiceDataUsageDifference()
   {
	   float GooglePlayServiceDataDifference=GooglePlayServiceDataUsedInMBAfter30Mins-GooglePlayServiceDataUsedInMB;
	   Reporter.log("Google Play Service Data Usage Difference Is :"+GooglePlayServiceDataDifference+" MB",true);
   }
   
   
   
}
