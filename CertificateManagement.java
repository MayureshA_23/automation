package Settings_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class CertificateManagement extends Initialization {

	@Test(priority = 1, description = "Verify Certificate management is present in Account settings.")
	public void VerifyCertificatemanagementispresentinAccountsettings()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.VerifyCertificateMAnagementPageOptions();
	}

	@Test(priority = 2, description = "Verify not entering CA server address and Clicking on save.")
	public void VerifynotenteringCAserveraddressandClickingonsave()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		accountsettingspage.EnterAllTheFieldsExceptCAServerAddressCertiManagement(
				Config.CertificateRenewalPeriodperiodDay, Config.CNWildcard, Config.SANWildcard);
		accountsettingspage.SelectEnableOTp();
		accountsettingspage.EnterScepUserPassword();
		accountsettingspage.SaveCertificateMangement();
		accountsettingspage.ErrorMessageWithoutCAServerAddress();
	}

	@Test(priority = 3, description = "Verify Entering only CA server address in Certificate Management.")
	public void VerifyEnteringonlyCAserveraddressinCertificateManagement()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		accountsettingspage.EnterOnlyCAServerAddress();
		accountsettingspage.UnselectEnableOTp();
		accountsettingspage.SaveCertificateMangement();
		accountsettingspage.ErrorMessageWhenOnlyCAServerAddress();
	}

	@Test(priority = 4, description = "Verify enable OTP and not entering username and password in Certificate Management.")
	public void VerifyenableOTPandnotenteringusernameandpasswordinCertificateManagement()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		accountsettingspage.EnterAllTheFieldsOfCertiManagement(Config.CertificateRenewalPeriodperiodDay,
				Config.CNWildcard, Config.SANWildcard);
		accountsettingspage.SelectEnableOTp();
		accountsettingspage.clearScepUsernamePassword();
		accountsettingspage.SaveCertificateMangement();
		accountsettingspage.ErrorMessageWhenWithoutScepUsernamePassword();
	}

	@Test(priority = 5, description = "Verify configuring the certificate management.")
	public void Verifyconfiguringthecertificatemanagement()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		accountsettingspage.EnterAllTheFieldsOfCertiManagement(Config.CertificateRenewalPeriodperiodDay,
				Config.CNWildcard, Config.SANWildcard);
		accountsettingspage.SelectEnableOTp();
		accountsettingspage.EnterScepUserPassword();
		accountsettingspage.SaveCertificateMangement();
		accountsettingspage.VerifyConfirmationMessage();
	}

	@Test(priority = 6, description = "Verify Get Managed Certificates option in Certificate management")
	public void VerifyGetManagedCertificatesoptioninCertificatemanagement()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.VerifyGetManagedCertificates();
		accountsettingspage.VerifyCertificatedDetails();
		accountsettingspage.CloseGetManagedCertificates();
	}

	@Test(priority = 7, description = "Verify Enable OTP in certificate management if OTP is configured for that ad server.")
	public void VerifyEnableOTPincertificatemanagementifOTPisconfigureforthatadserver()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		commonmethdpage.ClickOnHomePage();
		profilesAndroid.ClickOnProfiles();
		accountsettingspage.ClickOnAddButtonOfProfile();// this optional
		profilesAndroid.ClickOnCertificatePolicy();
		profilesAndroid.ClickOnCertificatePolicyConfigButton();
		accountsettingspage.ClickOnScepCheckBox();
		accountsettingspage.CertifcateUsageWifi();
		accountsettingspage.EnterCertificateName(Config.CertificateName1);
		profilesAndroid.ClickOnAddButtonOnCertWindow();
		profilesAndroid.EnterCertificateProfileName(Config.CertificateProfileName1);
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();

		commonmethdpage.ClickOnHomePage();
		// androidJOB.AppiumConfigurationforNIX();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.CertificateProfileName1);
	//	androidJOB.ClickOnApplyJobOkButton();
		androidJOB.CheckStatusOfappliedInstalledJob(Config.CertificateProfileName1, 300);
		androidJOB.ClickOnJobQueueCloseButton();
		// accountsettingspage.InstallCertificateToDevice();
		// androidJOB.AppiumConfigurationforSettings();
		// accountsettingspage.VerifyCertificateNameFromDevice(Config.CertificateName1);
	}

	@Test(priority = 8, description = "8.Verify Certificate Revoke Period option in Certificate Management.")
	public void VerifyCertificateRevokePeriodoptioninCertificateManagement()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		// androidJOB.AppiumConfigurationforNIX();
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SelectCertificate();
		accountsettingspage.ClickOnRevokeButton();
		accountsettingspage.ConfirmationMessageRevokeCerticate();
		accountsettingspage.CloseGetManagedCertificates();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.VerifyRevokedDeviceInGetManagedCertificate();
		accountsettingspage.CloseGetManagedCertificates();
		// accountsettingspage.VerifyInstallCertificatePromptDuringRevoke();
	}

	@Test(priority = 9, description = "9. Verify Disable OTP in certificate management if OTP is configured for that ad server.")
	public void VerifyDisableOTPincertificatemanagementifOTPisconfigureforthatadserver()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.EnterAllTheFieldsOfCertiManagement(Config.CertificateRenewalPeriodperiodDay,
				Config.CNWildcard, Config.SANWildcard);
		accountsettingspage.UnselectEnableOTp();
		accountsettingspage.SaveCertificateMangement();
		accountsettingspage.VerifyConfirmationMessage();
		commonmethdpage.ClickOnHomePage();
		profilesAndroid.ClickOnProfiles();
		accountsettingspage.ClickOnAddButtonOfProfile();// this optional
		profilesAndroid.ClickOnCertificatePolicy();
		profilesAndroid.ClickOnCertificatePolicyConfigButton();
		accountsettingspage.ClickOnScepCheckBox();
		accountsettingspage.CertifcateUsageWifi();
		accountsettingspage.EnterCertificateName(Config.CertificateName2);
		profilesAndroid.ClickOnAddButtonOnCertWindow();
		profilesAndroid.EnterCertificateProfileName(Config.CertificateProfileName2);
		profilesAndroid.ClickOnSaveButton();
		profilesAndroid.NotificationOnProfileCreated();
		// androidJOB.AppiumConfigurationforNIX();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.CertificateProfileName2);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.CertificateProfileName2, 300);
		androidJOB.ClickOnJobQueueCloseButton();
//		androidJOB.AppiumConfigurationforSettings();
//		accountsettingspage.VerifyCertificateNameWhenDisableOTP();
	}
	
	//mithilesh 
	@Test(priority = 10, description = "Verify Search by date for certificate expiry column in Certficate Management")
	public void VerifySearchbydateforcertificateexpirycolumninCertficateManagementTC_ST_712()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
	    Reporter.log("\n10.Verify Search by date for certificate expiry column in Certficate Management",true);
	    //accountsettingspage.CloseEnrollmentWindow();//remove this
	    commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();
		accountsettingspage.CloseGetManagedCertificates();	   					
	}
	
	
	@Test(priority = 11, description = "Verify Search by date for certificate expiry column by selecting date as Today  in Certficate Management")
	public void VerifySearchbydateforcertificateexpirycolumnbyselectingdateasTodayinCertficateManagementTC_ST_713()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n11.Verify Search by date for certificate expiry column by selecting date as Today  in Certficate Management",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();
	    accountsettingspage.ClickonTodayCertMag();
	    //accountsettingspage.ClearTextBox();//trial
	    //accountsettingspage.DeviceInformation();
	    accountsettingspage.CloseGetManagedCertificates();
	}
	
	@Test(priority = 12, description = "Verify Search by date for certificate expiry column by selecting date as Yesterday in Certficate Management")
	public void VerifySearchbydateforcertificateexpirycolumnbyselectingdateasYesterdayinCertficateManagementTC_ST_714()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n12.Verify Search by date for certificate expiry column by selecting date as Yesterday in Certficate Management",true);
		//accountsettingspage.CloseEnrollmentWindow();//remove this
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();
		accountsettingspage.ClickonYesterdayCertMag();
		//accountsettingspage.ClearTextBox();//trial
		//accountsettingspage.DeviceInformation();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	
	@Test(priority = 13, description = "Verify Search by date for certificate expiry column by selecting date as Last Week in Certficate Management")
	public void VerifySearchbydateforcertificateexpirycolumnbyselectingdateasLastWeekinCertficateManagementTC_ST_715()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n13.Verify Search by date for certificate expiry column by selecting date as Last Week in Certficate Management",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();
		accountsettingspage.ClickonLast1WeekCertMag();
		//accountsettingspage.ClearTextBox();//trial
		//accountsettingspage.DeviceInformation();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	@Test(priority = 14, description = "Verify Search by date for certificate expiry column by selecting date as Last 30 days in Certficate Management")
	public void VerifySearchbydateforcertificateexpirycolumnbyselectingdateasLast30daysinCertficateManagementTC_ST_716()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n14.Verify Search by date for certificate expiry column by selecting date as Last 30 days in Certficate Management",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();
		accountsettingspage.ClickonLast30DaysCertMag();	
		//accountsettingspage.DeviceInformation();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	@Test(priority = 15, description = "Verify Search by date for certificate expiry column by selecting date as This Month in Certficate Management")
	public void VerifySearchbydateforcertificateexpirycolumnbyselectingThisMonthinCertficateManagementTC_ST_717()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n15.Verify Search by date for certificate expiry column by selecting date as This Month in Certficate Management",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();
		accountsettingspage.ThisMonth();
		//accountsettingspage.ClickonLast30DaysCertMag();	
		//accountsettingspage.DeviceInformation();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	@Test(priority = 16, description = "Verify Search by date for certificate expiry column by selecting date as Custom Range in Certficate Management")
	public void VerifySearchbydateforcertificateexpirycolumnbyselectingCustomRangeCertficateManagementTC_ST_716()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n16.Verify Search by date for certificate expiry column by selecting date as Custom Range in Certficate Management",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();	
		accountsettingspage.CustomRange();
		//accountsettingspage.ClickonLast30DaysCertMag();	
		//accountsettingspage.DeviceInformation();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
		
	@Test(priority = 17, description = "Verify clearing the search by date in the textbox")
	public void VerifyclearingthesearchbydateinthetextboxTC_ST_718()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n17.Verify clearing the search by date in the textbox",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.SearchBarCertificateExpiryVisible();
		accountsettingspage.ClickonLast30DaysCertMag();	
		accountsettingspage.DeviceInformation();
		accountsettingspage.ClearTextBox();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	@Test(priority = 18, description = "Verify export option in certificate Management ")
	public void VerifyexportoptionincertificateManagementTC_ST_719()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n18.Verify export option in certificate Management ",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.ExportbuttonVisible();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	
	@Test(priority = 19, description = "verify errror message when no device is selected in certificate management and clicking on Revoke")
	public void verifyerrrormessagewhennodeviceisselectedincertificatemanagementandclickingonRevokeTC_ST_666()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n19.verify errror message when no device is selected in certificate management and clicking on Revoke",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.ExportbuttonVisible();
		accountsettingspage.RevokeSelectCertificate();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	@Test(priority = 20, description = "verify errror message when no device is selected in certificate management and clicking on renew")
	public void verifyerrrormessagewhennodeviceisselectedincertificatemanagementandclickingonrenewATC_ST_667()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n20.verify errror message when no device is selected in certificate management and clicking on renew",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();	
		accountsettingspage.ClickOnCertificateManagement();
		accountsettingspage.ClickOnGetManagedCertificates();
		accountsettingspage.ExportbuttonVisible();
		accountsettingspage.RenewSelectCertificate();
		accountsettingspage.CloseGetManagedCertificates();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
