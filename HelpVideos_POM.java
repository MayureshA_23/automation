package PageObjectRepository;

import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import Common.Initialization;
import Library.AssertLib;
import Library.WebDriverCommonLib;

public class HelpVideos_POM extends WebDriverCommonLib {
	AssertLib ALib=new AssertLib(); 
	
	@FindBy(id = "allDemoVideos")
	private WebElement ClickOnHelpVideos;
	
	@FindBy(id = "helpVides_btn")
	private WebElement helpVideosIcon;
	
	@FindBy(xpath=".//*[@id='helpVides_btn']/i")
	private WebElement clicking;
	
	@FindBy(xpath =".//*[@id='vidList']/li/div[2]/p[1]")
	private WebElement TextofVideos;
	
	@FindBy(xpath=".//*[@id='allDemoVideos_cont']/span/span[1]/input")
	private WebElement demoVideoCountCheckBox;
	
	@FindBy(id="vid_search")
	private WebElement videoSearch;
	
	@FindBy(xpath="//*[@id='vidList']/li[2]/div[2]/div[2]/ul/li/div[2]/p[1]")
	private WebElement Video;
	
	@FindBy(xpath=".//*[@id='videoList_cont']/div[1]/button")
	private WebElement closeHelpVideosPopUp;
	
	@FindBy(id="jobSection")
	private WebElement ClickOnJobs;
	
	@FindBy(xpath=".//*[@id='jobPageHolder']/div[2]/div/div[1]/a/span/i")
	private WebElement ClickOnPlayHelpVideoinJobs;
	
	@FindBy(xpath="//p[text()='Install Apps and Push Updates with SureMDM Install Job Feature']")
	private WebElement TextOfHelpVideosPopUpinJob;
	
	@FindBy(xpath=".//*[@id='player_uid_727133929_1']/div[4]/button")
	private WebElement PlayHelpVideo;
	
	@FindBy(xpath=".//*[@id='player_uid_442121123_1']/div[4]/div")
	private WebElement CloseVideo;
	
	@FindBy(id="byodSection")
	private WebElement profiles;
	
	@FindBy(xpath=".//*[@id='emm_container']/article/div[2]/div[1]/div/div[2]/div[2]/div/div[2]/div[1]/p")
	private WebElement ProfileText;
	
	SoftAssert s_assert = new SoftAssert();
	
	public void VerifyingHelpVideoinAdvanceSettings(){
		ClickOnHelpVideos.click();
	}
	
	
	
	public void GetTextofVideos() throws InterruptedException{
		
		
		List<WebElement> videoList = Initialization.driver.findElements(By.xpath(".//*[@id='vidList']/li/div[2]/p[1]"));
		int count = videoList.size();
		for(int i=0;i<count;i++)
		{
			String actual = videoList.get(i).getText();	
			Reporter.log(actual);
			
			if(i==0){
				String firtVideoActualTitle = videoList.get(i).getText();	
				String firtVideoExpectedTitle = "Introduction to SureMDM";
				String firtVideoPassStatement = "PASS >> Title "+firtVideoExpectedTitle+" is correct";
				String firtVideoFailStatement = "FAIL >> Title "+firtVideoExpectedTitle+ " is not correct";
				ALib.AssertEqualsMethod(firtVideoExpectedTitle, firtVideoActualTitle, firtVideoPassStatement, firtVideoFailStatement);
				Reporter.log("");
				sleep(4);
			}
			if(i==1){
					String secondVideoActualTitle = videoList.get(i).getText();	
					String secondVideoExpectedTitle = "Location Tracking iPhone and iPad";
					String secondVideoPassStatement = "PASS >> Title "+secondVideoExpectedTitle+" is correct";
					String secondVideoFailStatement = "FAIL >> Title "+secondVideoExpectedTitle+ " is not correct";
					ALib.AssertEqualsMethod(secondVideoExpectedTitle, secondVideoActualTitle, secondVideoPassStatement, secondVideoFailStatement);
					Reporter.log("");
					sleep(4);
			}
			if(i==2){
						String thirdVideoActualTitle = videoList.get(i).getText();	
						String thirdVideoExpectedTitle = "Lockdown mobile devices into Kiosk Mode with SureLock";
						String thirdVideoPassStatement = "PASS >> Title "+thirdVideoExpectedTitle+" is correct";
						String thirdVideoFailStatement = "FAIL >> Title "+thirdVideoExpectedTitle+ " is not correct";
						ALib.AssertEqualsMethod(thirdVideoExpectedTitle, thirdVideoActualTitle, thirdVideoPassStatement, thirdVideoFailStatement);
						Reporter.log("");
						sleep(4);
			}
			if(i==3){
				String fourthVideoActualTitle = videoList.get(i).getText();	
				String fourthVideoExpectedTitle = "Remote Control Android Devices with SureMDM";
				String fourthVideoPassStatement = "PASS >> Title "+fourthVideoExpectedTitle+" is correct";
				String fourthVideoFailStatement = "FAIL >> Title "+fourthVideoExpectedTitle+ " is not correct";
				ALib.AssertEqualsMethod(fourthVideoExpectedTitle, fourthVideoActualTitle, fourthVideoPassStatement, fourthVideoFailStatement);
				Reporter.log("");
				sleep(4);
				closeHelpVideosPopUp.click();
				sleep(3);
	}
	
		}
	}
	
	@FindBy(xpath="//*[@id=\"allDemoVideos\"]")
	private WebElement HelpVideosOption;
	 public void ClickOnHelpvideos() throws InterruptedException{
		
		 HelpVideosOption.click();
		 sleep(2);
		 
	}
	 
	public void VerifyEnablingShowAllVideos() throws InterruptedException{
		 List<WebElement> videoList = Initialization.driver.findElements(By.xpath(".//*[@id='vidList']/li/div[2]/p[1]"));
		 sleep(3);
		 clicking.click();
		 sleep(3);
		 demoVideoCountCheckBox.click();
		 sleep(3);
		 int actual_size = videoList.size();
	     int expected_size = 3;
	     String PassStatement = "PASS >> 4 videos are displayed and 'show all videos' is checked";
	     String FailStatement = "FAIL >> Demo Video Count "+expected_size+ " is not correct";
	     ALib.AssertEqualsMethodInt(expected_size, actual_size, PassStatement, FailStatement);
	     sleep(3);
		
	}
	
	public void VerifyDisablingShowAllVideos() throws InterruptedException{
		 demoVideoCountCheckBox.click();
		 sleep(2);
		 List<WebElement> videoList = Initialization.driver.findElements(By.xpath(".//*[@id='vidList']/li/div[2]/p[1]"));
		 sleep(3);
		 int actual_size = videoList.size();
	     int expected_size = 3;
	     String PassStatement = "PASS >> 3 videos are displayed and 'show all videos' is unchecked";
	     String FailStatement = "FAIL >> Demo Video Count "+expected_size+ " is not correct";
	     ALib.AssertEqualsMethodInt(expected_size, actual_size, PassStatement, FailStatement);
	    
	}
	
	public void VerifyingSearchOptionIHelpVideos() throws InterruptedException{
		videoSearch.sendKeys("Remote");
		String ThirdVideoActualTitle = Video.getText();
		String ThirdVideoExpectedTitle = "Remote Control Android Devices with SureMDM";
		String ThirdVideoPassStatement = "PASS >> Title "+ThirdVideoExpectedTitle+" is correct - search result is correct";
		String ThirdVideoFailStatement = "FAIL >> Title "+ThirdVideoExpectedTitle+ " is not correct- Search result is not correct";
		ALib.AssertEqualsMethod(ThirdVideoExpectedTitle, ThirdVideoActualTitle, ThirdVideoPassStatement, ThirdVideoFailStatement);
		Reporter.log("");
		sleep(4);
		closeHelpVideosPopUp.click();
			
		
	}
	
	public void VerifyingHelpVideoInJobsModule() throws InterruptedException{
		ClickOnJobs.click();
		sleep(3);
		ClickOnPlayHelpVideoinJobs.click();
		boolean b = true;
	       try{
	    	   TextOfHelpVideosPopUpinJob.isDisplayed();
				   	 
				    }catch(Exception e)
				    {
				   	 b=false;
				   	 
				    }
		 
		   Assert.assertTrue(b,"FAIL>> Either Video pop up is not shown or Video text is wrong");
		   Reporter.log("PASS>> help videos pop up is displayed with correct video title");
		   sleep(3);
		   CloseVideo.click();
		   sleep(2);
		
		
	}
	
	public void VerifyingMediaControlsfromvideos() throws InterruptedException{
		JavascriptExecutor js = (JavascriptExecutor)Initialization.driver;
		//WebElement video = Initialization.driver.findElement(By.id("player_uid_442121123_1"));
       // js.executeScript("arguments[0].play();", video);
        
        
        js.executeScript("document.getElementsByClassName('html5-video-container')[0].click();");
        
        
		//js.executeScript("document.getElementId(\"player_uid_727133929_1\").play()");
		//sleep(10);
		//js.executeScript("document.getElementId(\"player_uid_727133929_1\").pause()");
		
		
		
	}
	
	public void VerifyingHelpTextMessageinProfiles() throws InterruptedException{
		profiles.click();
		WebDriverWait wait = new WebDriverWait(Initialization.driver, 10);

		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("addPolicyBtn")));
		String actualText = ProfileText.getText();
		String expectedText = "Profiles in SureMDM allows admins to control the functions and settings of specific apps and enrolled devices. Using this feature admins can have a control the environment of enrolled devices while still letting users use their personal apps and profiles.";
		String PassStatement = "PASS >> Help Text in Profile : "+expectedText+" is correct";
	    String FailStatement = "FAIL >> Help Text in Profile : "+expectedText+ " is wrong";
	    ALib.AssertEqualsMethod(expectedText, actualText, PassStatement, FailStatement);
	    sleep(3);
	}
	
	 public void demoVideoCount() throws InterruptedException{
		 List<WebElement> videoList = Initialization.driver.findElements(By.xpath(".//*[@id='vidList']/li/div[2]/p[1]"));
		 for(int i=0;i<1;i++){
		 
		 if(i==0){
		     int actual_size = videoList.size();
		     int expected_size = 3;
		     String PassStatement = "PASS >> Demo Video Count  "+expected_size+" is correct and 'show all videos' is not checked";
		     String FailStatement = "FAIL >> Demo Video Count "+expected_size+ " is not correct";
		     ALib.AssertEqualsMethodInt(expected_size, actual_size, PassStatement, FailStatement);
		     sleep(3);
		 }
		 /*
		 if(i==1){
			 demoVideoCountCheckBox.click();
		     int actual_size = videoList.size();
		     int expected_size = 4;
		     String PassStatement = "PASS >> Demo Video Count  "+expected_size+" is correct and 'show all videos' is not checked";
		     String FailStatement = "FAIL >> Demo Video Count "+expected_size+ " is not correct";
		     ALib.AssertEqualsMethodInt(expected_size, actual_size, PassStatement, FailStatement);
		 }
		 */
	 }
					
			
			
			    
			     
				
				
				
				
				
			}
		}
		
		
		
		
		
	
	
		
	


