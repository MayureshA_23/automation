package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

	public class SystemLog_CustomReport extends Initialization {
	@Test(priority='0',description="Verify generating System Log Custom report with filters")
	public void VerifyingSystemLogReportForCurrebtDate_TC_RE_140() throws InterruptedException
	{Reporter.log("\nVerify generating System Log Custom report with filters",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SystLog Custom Report for current date","test");
		customReports.ClickOnColumnInTable(Config.SelectSystemLog);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSystemLog);
		customReports.Selectvaluefromdropdown(Config.SelectSystemLog);
		customReports.SelectValueFromColumn(Config.SelectLogTime);
		customReports.SelectTodayDateInCustomReport();
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SystLog Custom Report for current date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SystLog Custom Report for current date");
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SystLog Custom Report for current date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SystLog Custom Report for current date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.VarifyingCurrentDateInViewReport_AppliedJobDetails();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority='1',description="Verify generating System Log Custom report with filters for yesterday date")
	public void VerifyingSystemLogReportYestdaysDate_TC_RE_141() throws InterruptedException
	{Reporter.log("\nVerify generating System Log Custom report with filters for yesterday date",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSystemLog);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSystemLog);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SystemLog CustomReport for yesterday date","test");
		customReports.Selectvaluefromdropdown(Config.SelectSystemLog);
		customReports.SelectValueFromColumn(Config.SelectLogTime);
		customReports.SelectYesterdayDateInCustomReport();	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();	
		customReports.SearchCustomizedReport("SystemLog CustomReport for yesterday date");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection("SystemLog CustomReport for yesterday date");
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SystemLog CustomReport for yesterday date");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SystemLog CustomReport for yesterday date");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);
		customReports.VarifyingYesterDateInViewReport_SystemLog();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@Test(priority='2',description="Verify generating System Log Custom report with filters- The report should contain  Message field wrt  message mentioned in filters")
	public void VerifyingSystemLogReport_TC_RE_142() throws InterruptedException
	{Reporter.log("\nVerify generating System Log Custom report with filters- The report should contain  Message field wrt  message mentioned in filters==",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSystemLog);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.VerifyTableInSelectedTableList(Config.SelectSystemLog);
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SystemLog CustomReport with like operator","test");
		customReports.Selectvaluefromdropdown(Config.SelectSystemLog);
		customReports.SelectValueFromColumn("Message");
		customReports.SelectOperatorFromDropDown(Config.SelectlikeOperator);
		customReports.SendValueToTextfield("Device Started");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();		
		customReports.SearchCustomizedReport("SystemLog CustomReport with like operator");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.SelectLast30daysInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SystemLog CustomReport with like operator");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SystemLog CustomReport with like operator");
		reportsPage.WindowHandle();
		customReports.ClickOnSearchReportButtonInsidedViewedReport_SystemLog();
		customReports.VerificationOfMessagefield();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();	
	}
	@Test(priority='3',description="Verify Sort by and Group by for System Log")
	public void VerifyingSystemLogReportWithAggregateOption_TC_RE_143() throws InterruptedException
	{Reporter.log("\nVerify Sort by and Group by for System Log",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSystemLog);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SytemLog CustomReport for SortBy And GroupBy","test");
		customReports.Selectvaluefromsortbydropdown(Config.SelectLogTime);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName(Config.EnterAliasNameAsMyDevice);	
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SytemLog CustomReport for SortBy And GroupBy");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SytemLog CustomReport for SortBy And GroupBy");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SytemLog CustomReport for SortBy And GroupBy");
		reportsPage.WindowHandle();
		customReports.VerificationOfValuesInColumn();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();				
	}
	@Test(priority='4',description="Verify sorting of columns in System Log report")
	public void VerifyingSystemLogReport_TC_RE_220() throws InterruptedException
	{Reporter.log("\nVerify sorting of columns in System Log report",true);
		commonmethdpage.ClickOnHomePage();
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectSystemLog);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("SytemLog CustomReport with Sorting in Rep","test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("SytemLog CustomReport with Sorting in Rep");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("SytemLog CustomReport with Sorting in Rep");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("SytemLog CustomReport with Sorting in Rep");
		reportsPage.WindowHandle();
		customReports.ChooseDevicesPerPage();
		customReports.VerifySortingInSystemLogRep();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	
	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
