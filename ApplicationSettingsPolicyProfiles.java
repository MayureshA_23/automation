package ProfilesAndroid_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class ApplicationSettingsPolicyProfiles extends Initialization {

//	@Test(priority=1,description="App Store-Verifying added applications inside application policy.+App Store-Verifying creating application policy and applying on device..")
//	public void AddAppInApplicationPolicy() throws Throwable
//	{
//		Reporter.log("App Store-Verifying added applications inside application policy.+App Store-Verifying creating application policy and applying on device..",true);
//		appstorepage.ClickOnAppStore();
//		appstorepage.ClickOnAddNewAppAndroid();
//		appstorepage.ClickOnUploadAPKAndroid();
//		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreADDApplication.exe"); 
//		appstorepage.VerifyOfUploadApkAndroid();
//		appstorepage.EnterCategoryAndroid("SMSPopUp");
//		appstorepage.EnterDescriptionAndroid("SMSPopUp");
//		appstorepage.ClickOnUploadApkPopupAddButton();
//		commonmethdpage.ClickOnHomePage();
//		profilesAndroid.ClickOnProfiles();
//		profilesAndroid.AddProfile();
//		profilesAndroid.ClickOnApplicationSettingsPolicy();
//		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
//		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
//		profilesAndroid.ClickOnSureMDMAppStore();
//		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
//		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
//		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
//		profilesAndroid.EnterSystemSettingsProfileName("CreationApplicationPolicy");
//		profilesAndroid.ClickOnSaveApplicationPolicy();
//		commonmethdpage.ClickOnHomePage(); 
//		commonmethdpage.SearchDevice();
//		commonmethdpage.ClickOnApplyButton();
//		commonmethdpage.SearchJob("CreationApplicationPolicy");
//		commonmethdpage.Selectjob("CreationApplicationPolicy");
//		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		androidJOB.CheckStatusOfappliedInstalledJob("CreationApplicationPolicy",600);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
//		profilesAndroid.VerifyAppStore();
//		profilesAndroid.VerifyAppInstall("SMS Popup");
//		androidJOB.unInstallApplication("net.everythingandroid.smspopup");
//		profilesAndroid.ClickOnProfiles();
//		appstorepage.DeleteCreatedProfile("CreationApplicationPolicy");
//		
//		Reporter.log("PASS>>App Store-Verifying added applications inside application policy.+App Store-Verifying creating application policy and applying on device..",true); 	
//	}
//
//	@Test(priority=2,description="App Store-Application policy with multiple apk's")
//	public void AddMultiApplicationPolicy() throws Throwable
//	{
//		Reporter.log("App Store-Application policy with multiple apk's",true);
//		appstorepage.ClickOnAppStore();
//		appstorepage.ClickOnAddNewAppAndroid();
//		appstorepage.ClickOnUploadAPKAndroid();
//		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreADDApplication.exe"); 
//		appstorepage.VerifyOfUploadApkAndroid();
//		appstorepage.EnterCategoryAndroid("SMSPopUp");
//		appstorepage.EnterDescriptionAndroid("SMSPopUp");
//		appstorepage.ClickOnUploadApkPopupAddButton();
//
//		appstorepage.ClickOnAddNewAppAndroid();
//		appstorepage.ClickOnUploadAPKAndroid();
//		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreAddAppDeviceInfo.exe"); 
//		appstorepage.VerifyOfUploadApkAndroid();
//		appstorepage.EnterCategoryAndroid("Device Info");
//		appstorepage.EnterDescriptionAndroid("Device Info");
//		appstorepage.ClickOnUploadApkPopupAddButton();
//		commonmethdpage.ClickOnHomePage();
//		profilesAndroid.ClickOnProfiles();
//		profilesAndroid.AddProfile();
//		profilesAndroid.ClickOnApplicationSettingsPolicy();
//		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
//		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
//		profilesAndroid.ClickOnSureMDMAppStore();
//		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
//		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
//		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
//
//		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
//		profilesAndroid.ClickOnSureMDMAppStore();
//		profilesAndroid.SelectingTheCreatedAppDropdown("Device Info");
//		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
//		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
//
//		profilesAndroid.EnterSystemSettingsProfileName("CreationMultiApplicationPolicy");
//		profilesAndroid.ClickOnSaveApplicationPolicy();
//		commonmethdpage.ClickOnHomePage(); 
//		commonmethdpage.SearchDevice();
//		commonmethdpage.ClickOnApplyButton();
//		commonmethdpage.SearchJob("CreationMultiApplicationPolicy");
//		commonmethdpage.Selectjob("CreationMultiApplicationPolicy");
//		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
//		androidJOB.CheckStatusOfappliedInstalledJob("CreationMultiApplicationPolicy",600);
//		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
//		profilesAndroid.VerifyAppStore();
//		profilesAndroid.VerifyAppInstall("SMS Popup,Device Info");
//		androidJOB.unInstallMultipleApplication("net.everythingandroid.smspopup,com.alphabetlabs.deviceinfo");
//		profilesAndroid.ClickOnProfiles();
//		appstorepage.DeleteCreatedProfile("CreationMultiApplicationPolicy");
//		Reporter.log("PASS>>App Store-Application policy with multiple apk's",true); 	
//	}

	@Test(priority=3,description="App Store-Verifying update apk")
	public void verifyUpdateApk() throws Throwable
	{
		Reporter.log("App Store-Verifying update apk",true);
		appstorepage.ClickOnAppStore();
		appstorepage.EnterSearchAndroid("SMS");
		appstorepage.DeleteSearchedApp("SMS Popup");
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreADDApplication.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		commonmethdpage.ClickOnHomePage(); 
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("ApplicationPolicyLowerVersion");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("ApplicationPolicyLowerVersion");
		commonmethdpage.Selectjob("ApplicationPolicyLowerVersion");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("ApplicationPolicyLowerVersion",800);
		appstorepage.ClickOnAppStore();
		appstorepage.EnterSearchAndroid("SMS");
		appstorepage.ClickOnMoreSymbolLite("SMS Popup");
		appstorepage.ClickOnEditSymbol("SMS Popup");
		appstorepage.ClickOnEditUploadAPkAndroid_DifferentPackage("./Uploads/apk/\\SMSPopupHighVersion.exe");
		appstorepage.ClickOnUploadapkPopupAddButton();
		commonmethdpage.ClickOnHomePage(); 
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("Upgradpolicy");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("Upgradpolicy");
		commonmethdpage.Selectjob("Upgradpolicy");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("Upgradpolicy",800);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		profilesAndroid.ClickOnStoreTabInAppStore();
		profilesAndroid.VerifyAppStoreUpdate();
		profilesAndroid.IsCountBadgeDisplayed();
		profilesAndroid.ClickOnUpdateButton();
		androidJOB.unInstallMultipleApplication("net.everythingandroid.smspopup");
		androidJOB.ClickOnHomeButtonDeviceSide();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		appstorepage.verifynoApplicationAvailable();
		androidJOB.CheckApplicationsUnInstalledDeviceSide("net.everythingandroid.smspopup");
		profilesAndroid.ClickOnProfiles();
		appstorepage.DeleteCreatedProfile("ApplicationPolicyLowerVersion");
		appstorepage.DeleteCreatedProfile("Upgradpolicy");
		Reporter.log("Pass>> App Store-Verifying update apk+App Store-Verifying uninstall apps+App Store-Verifying update apk",true);
	}

	@Test(priority=4,description="App Store-Verifying deploying web app profile")
	public void VerifyProfileDeployingWebApp() throws Throwable
	{
		Reporter.log("App Store-Application policy with multiple apk's",true);
		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		profilesAndroid.VerifyClickingOnWebApp();
		profilesAndroid.EnterWebAppDetails("Amazon","https://www.amazon.com/");
		profilesAndroid.ClickOnWebAppAddButton();
		commonmethdpage.ClickOnHomePage(); 
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("Amazon");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("WebAppPolicy");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("WebAppPolicy");
		commonmethdpage.Selectjob("WebAppPolicy");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("WebAppPolicy",800);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppIsPresentInHometab("Amazon");
		profilesAndroid.ClickOnProfiles();
		appstorepage.DeleteCreatedProfile("WebAppPolicy");
		Reporter.log("PASS>>App Store-Verifying deploying web app profile",true); 	
	}

	@Test(priority=5,description="App Store-Verifying deploying multiple web app profile")
	public void VerifyProfileDeployingMultiWebApp() throws Throwable
	{
		Reporter.log("App Store-Application policy with multiple apk's",true);
		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		profilesAndroid.VerifyClickingOnWebApp();
		profilesAndroid.EnterWebAppDetails("Amazon","https://www.amazon.com/");
		profilesAndroid.ClickOnWebAppAddButton();
		
		appstorepage.ClickOnAddNewAppAndroid();
		profilesAndroid.VerifyClickingOnWebApp();
		profilesAndroid.EnterWebAppDetails("FaceBook","https://www.facebook.com/");
		profilesAndroid.ClickOnWebAppAddButton();

		commonmethdpage.ClickOnHomePage(); 
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("Amazon");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("FaceBook");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();

		profilesAndroid.EnterSystemSettingsProfileName("MultiWebAppPolicy");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("MultiWebAppPolicy");
		commonmethdpage.Selectjob("MultiWebAppPolicy");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("MultiWebAppPolicy",800);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppIsPresentInHometab("Amazon,FaceBook");
		profilesAndroid.ClickOnProfiles();
		appstorepage.DeleteCreatedProfile("MultiWebAppPolicy");
		Reporter.log("PASS>>App Store-Verifying deploying multiple web app profile",true); 	
	}


	@Test(priority=6,description="App Store-Verifying install apk in device")
	public void VerifyInstallApk() throws Throwable
	{
		Reporter.log("App Store-Verifying install apk in device",true);
		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreADDApplication.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		commonmethdpage.ClickOnHomePage();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();
		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("SMS Popup");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("FirstApkProfile");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FirstApkProfile");
		commonmethdpage.Selectjob("FirstApkProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("FirstApkProfile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		profilesAndroid.VerifyAppInstall("SMS Popup");
		androidJOB.unInstallApplication("net.everythingandroid.smspopup");

		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreAddAppDeviceInfo.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("Device Info");
		appstorepage.EnterDescriptionAndroid("Device Info");
		appstorepage.ClickOnUploadApkPopupAddButton();
		commonmethdpage.ClickOnHomePage();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("Device Info");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();

		profilesAndroid.EnterSystemSettingsProfileName("anotherApkProfile");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("anotherApkProfile");
		commonmethdpage.Selectjob("anotherApkProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("anotherApkProfile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		profilesAndroid.VerifyAppInstall("Device Info");
		profilesAndroid.VerifyAppNotPresentInAppStore("SMS Popup");
		androidJOB.unInstallMultipleApplication("com.alphabetlabs.deviceinfo");
		profilesAndroid.ClickOnProfiles();
		appstorepage.DeleteCreatedProfile("FirstApkProfile");
		appstorepage.DeleteCreatedProfile("anotherApkProfile");
		Reporter.log("PASS>>App Store-Verifying install apk in device",true); 	
	}

	@Test(priority=7,description="App Store-Verify replacing apk")
	public void VerifyReplacingApk() throws Throwable
	{
		Reporter.log("App Store-Verify replacing apk",true);
		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		profilesAndroid.VerifyClickingOnWebApp();
		profilesAndroid.EnterWebAppDetails("Amazon","https://www.amazon.com/");
		profilesAndroid.ClickOnWebAppAddButton();

		commonmethdpage.ClickOnHomePage(); 
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("Amazon");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();
		profilesAndroid.EnterSystemSettingsProfileName("FirstDeployWebApp");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("FirstDeployWebApp");
		commonmethdpage.Selectjob("FirstDeployWebApp");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("FirstDeployWebApp",800);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppIsPresentInHometab("Amazon");

		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreAddAppDeviceInfo.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("Device Info");
		appstorepage.EnterDescriptionAndroid("Device Info");
		appstorepage.ClickOnUploadApkPopupAddButton();
		commonmethdpage.ClickOnHomePage();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("Device Info");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();

		profilesAndroid.EnterSystemSettingsProfileName("ReplaceWithApkProfile");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("ReplaceWithApkProfile");
		commonmethdpage.Selectjob("ReplaceWithApkProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("ReplaceWithApkProfile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		profilesAndroid.VerifyAppInstall("Device Info");
		profilesAndroid.VerifyAppNotPresentInAppStore("Amazon");
		androidJOB.unInstallMultipleApplication("com.alphabetlabs.deviceinfo");
		profilesAndroid.ClickOnProfiles();
		appstorepage.DeleteCreatedProfile("FirstDeployWebApp");
		appstorepage.DeleteCreatedProfile("ReplaceWithApkProfile");
		Reporter.log("PASS>>App Store-Verify replacing apk",true); 	
	}

	@Test(priority=8,description="App Store-Verifying apply profile with upload apk, apk link, web app")
	public void VerifyApkApkLinkWebApp() throws Throwable
	{
		Reporter.log("App Store-Verify replacing apk",true);
		appstorepage.ClickOnAppStore();
		appstorepage.ClickOnAddNewAppAndroid();
		profilesAndroid.VerifyClickingOnWebApp();
		profilesAndroid.EnterWebAppDetails("Amazon","https://www.amazon.com/");
		profilesAndroid.ClickOnWebAppAddButton();

		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnAPKLinkAndroid();
		appstorepage.EnterAppLink("https://mars.42gears.com/support/inout/astrocontact/astrocontacts.apk");
		appstorepage.ClickOnApkLinkPopupAddButton();
		appstorepage.EnterEditAppTitleAndroid("AstroContactAppLink");
		appstorepage.EnterEditAppLinkCategoryAndroid("Astro Contact");
		appstorepage.EnterEditAppLinkDescriptionAndroid("Description");
		appstorepage.ClickOnUploadApkPopupAddButton();

		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreAddAppDeviceInfo.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("Device Info");
		appstorepage.EnterDescriptionAndroid("Device Info");
		appstorepage.ClickOnUploadApkPopupAddButton();

		commonmethdpage.ClickOnHomePage();
		profilesAndroid.ClickOnProfiles();
		profilesAndroid.AddProfile();
		profilesAndroid.ClickOnApplicationSettingsPolicy();
		profilesAndroid.ClickOnApplicationSettingPolicyConfigButton();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("Device Info");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("AstroContactAppLink");
		profilesAndroid.ClickOnInstallSilentlyCheck_Box();
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();

		profilesAndroid.VerifyClickingOnApplicationPolicyAddButtton();
		profilesAndroid.ClickOnSureMDMAppStore();
		profilesAndroid.SelectingTheCreatedAppDropdown("Amazon");
		profilesAndroid.ClickOnAddButton_EnterpriseAppStore();

		profilesAndroid.EnterSystemSettingsProfileName("VerifyApkWebAppApkLinkProfile");
		profilesAndroid.ClickOnSaveApplicationPolicy();
		commonmethdpage.ClickOnHomePage(); 
		commonmethdpage.SearchDevice();
		commonmethdpage.ClickOnApplyButton();
		commonmethdpage.SearchJob("VerifyApkWebAppApkLinkProfile");
		commonmethdpage.Selectjob("VerifyApkWebAppApkLinkProfile");
		commonmethdpage.ClickOnApplyButtonApplyJobWindow();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.CheckStatusOfappliedInstalledJob("VerifyApkWebAppApkLinkProfile",600);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.enterpriseppstore.splashScreen.SplashScreenActivity");
		profilesAndroid.VerifyAppStore();
		profilesAndroid.VerifyAppInstall("Device Info,Amazon,AstroContacts");
		androidJOB.unInstallMultipleApplication("com.alphabetlabs.deviceinfo,com.gears42.astrocontacts");
		profilesAndroid.ClickOnProfiles();
		appstorepage.DeleteCreatedProfile("VerifyApkWebAppApkLinkProfile");
		Reporter.log("PASS>>App Store-Verifying apply profile with upload apk, apk link, web app",true); 	
	}

	// ***********************************NEW CASES *************************************

	@Test(priority='S',description="Verify updating an older version of application to latest application")
	public void  older_version() throws Throwable {
		
		appstorepage.ClickOnAppStore();
		appstorepage.EnterSearchAndroid("SMS");
		appstorepage.DeleteSearchedApp("SMS Popup");
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreADDApplication.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		
		appstorepage.ClickOnMoreSymbolLite("SMS Popup");
		appstorepage.ClickOnEditSymbol("SMS Popup");
		appstorepage.ClickOnEditUploadAPkAndroid_DifferentPackage("./Uploads/apk/\\SMSPopupHighVersion.exe");
		appstorepage.ClickOnUploadapkPopupAdd();
        appstorepage.VerifyConformationMessageOnAddingApp();
       
	}

	@Test(priority='T',description="Verify updating an older version of application to latest  version when that latest version already present in app store")
	public void  olderVersionAndNewVersion() throws Throwable {
		Reporter.log("To verify Adding new App ",true);
		appstorepage.ClickOnAppStore();
		appstorepage.EnterSearchAndroid("SMS");
		appstorepage.DeleteSearchedApp("SMS Popup");
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\AppStoreADDApplication.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\SMSPopupHighVersion.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		
		
		appstorepage.ClickOnMoreSymbolLite("SMS Popup");
		appstorepage.ClickOnEditSymbol("SMS Popup");
		appstorepage.ClickOnEditUploadAPkAndroid_DifferentPackage("./Uploads/apk/\\SMSPopupHighVersion.exe");
		appstorepage.ClickOnUploadApkPopupAddButton();
		appstorepage.VerifyErrorMessage();
	}


	@Test(priority='V',description="Verify Adding same application same version in app store")
	public void AddingSameApplicationSameVersion() throws Throwable {

		Reporter.log("To verify Adding new App ",true);
		appstorepage.ClickOnAppStore();
		appstorepage.EnterSearchAndroid("SMS");
		appstorepage.DeleteSearchedApp("SMS Popup");
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\SMSPopupHighVersion.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		
		appstorepage.ClickOnAddNewAppAndroid();
		appstorepage.ClickOnUploadAPKAndroid();
		appstorepage.ClickOnBrowseFileAndroid("./Uploads/apk/\\SMSPopupHighVersion.exe"); 
		appstorepage.VerifyOfUploadApkAndroid();
		appstorepage.EnterCategoryAndroid("SMSPopUp");
		appstorepage.EnterDescriptionAndroid("SMSPopUp");
		appstorepage.ClickOnUploadApkPopupAddButton();
		appstorepage.VerifyErrorMessage();
		
	}


}









