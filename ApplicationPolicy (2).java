package Profiles_Windows_Testscripts;

import java.awt.AWTException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class ApplicationPolicy extends Initialization{
	
	@Test(priority='1',description="To verify clicking on Application Policy") 
    public void VerifyClickingOnApplicationPolicy() throws InterruptedException
	{
        Reporter.log("1.To verify clicking on Application Policy",true);
        profilesAndroid.ClickOnProfile();
        profilesWindows.ClickOnWindowsOption();
        profilesWindows.AddProfile();
        profilesWindows.ClickOnApplicationPolicy();
    }
	
	@Test(priority='2',description="To verify summary of Application Policy")
	public void VerifySummaryofAppLocker() throws InterruptedException
	{
		Reporter.log("\n2.To verify summary of Application Policy",true);
		profilesWindows.VerifySummaryof_ApplicationPolicy();
	}
	
	@Test(priority='3',description="To Verify warning message Saving Application profile without profile Name")
	public void VerifyWarningMessageOnSavingApplicationProfilePolicyProfileWithoutName() throws InterruptedException{
		Reporter.log("\n3.To Verify warning message Saving Application Policy profile without profile Name",true);
		profilesWindows.ClickOnConfigureButton_ApplicationPolicyProfile();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='4',description="To Verify warning message Saving App locker Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingApplicationProfileWihtoutName() throws InterruptedException{
		Reporter.log("\n4.To Verify warning message Saving a App Locker Profile without entering all the fields",true);
		profilesWindows.EnterApplicationPolicyProfileName();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	
	@Test(priority='5',description="To Verify clicking on Add button of the application policy profile")
	public void VerifyClickingOnAddButtonOfApplicationPolicyProfile() throws InterruptedException{
		Reporter.log("\n5.To Verify clicking on Add button of the application policy profile",true);
		profilesWindows.ClickOnAddButton_ApplicationPolicy();
	}
	
	
	@Test(priority='6',description="To Verify warning message while clicking Add Button of Add App Window of Application Policy Profile without entering all the fields")
	public void VerifyWarningMessageOnClickingOnAddButtonOfAddAppWindowOfApplicationPolicyWithoutAllFields() throws InterruptedException{
		Reporter.log("\n6.To Verify warning message while clicking Add Button of Add App Window of Application Policy Profile without entering all the fields",true);
		profilesWindows.AddButton_ApplicationPolicyWindow();
		profilesWindows.WarningMessageWithoutOutgoing_MailConfiguration();//common
	}
	
	@Test(priority='7',description="To Verify cancelling the Add App Window")
	public void VerifyCancellingTheAddAppWindow() throws InterruptedException{
		Reporter.log("\n7.To Verify cancelling the Add App  Window",true);
		
		profilesWindows.ClickOnCancelButton_AddAppWindow();
	}
	
	@Test(priority='8',description="To Verify warning messages without Entering 'Name' or 'Enter URL'")
	public void VerifyWarningMessageWithoutNameEnterUrl() throws InterruptedException{
		Reporter.log("\n8.To Verify warning messages without Entering 'Name' or 'Enter URL'",true);
		profilesWindows.ClickOnAddButton_ApplicationPolicy();
		profilesWindows.ClickOnAddButtonAddAppWindow();
		profilesWindows.WarningMessageWithoutOutgoing_MailConfiguration();
	}
	
	@Test(priority='9',description="To Verify adding the Application")
	public void VerifyAddingApplication() throws InterruptedException, AWTException{
		Reporter.log("\n9.To Verify adding the Application",true);
		profilesWindows.AddButton_ApplicationPolicyWindow();
		profilesWindows.ClickOnChooseApp();
		profilesWindows.EnterApplicationName();
		profilesWindows.ClickOnAddButtonInAddAppWindow();
		
	}
	
	@Test(priority='A',description="To Verify the warning dialog on Deleting the App in the Application Policy")
	public void VerifyWarningMessageOnDeletingApp_ApplicationPolicy() throws InterruptedException{
		Reporter.log("\n10.To Verify the warning dialog on Deleting the App in the Application Policy",true);
		profilesWindows.ClickOnFirstRowDataOfApplicationPolicy();
		profilesWindows.ClickOnDeleteButton_ApplicationPolicy();
		profilesiOS.WarningMessageOnApplicationDelete(); //common for this as well
		
	}
	@Test(priority='B',description="To Verify cancelling the warning dialog")
	public void VerifyCancellingTheWarningDialog() throws InterruptedException{
		Reporter.log("\n11.To Verify cancelling the warning dialog",true);
		profilesWindows.ClickOnNoButtonWarningDialog_ApplicationPolicy();//not making Not button method common because delete button used to verify is different from eacth other
		
	}
	@Test(priority='C',description="To Verify Saving Application Policy profile")
	public void VerifySavingApplicationPolicyProfile() throws InterruptedException{
		Reporter.log("\n12To Verify Saving Application Policy profile",true);
	    profilesWindows.ClickOnSaveButton();
		profilesWindows.NotificationOnProfileCreated();
	}
	
		

}
