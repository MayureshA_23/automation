package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceHistory_CustomReport extends Initialization
{
	@Test(priority='1',description="Verify Device History Column Report from custom Report")
	public void DeviceHistory_TC_RE_16() throws InterruptedException
	{
	    Reporter.log("\nVerify Device History Column Report from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.VerifyDeviceHistory();
		Reporter.log("All the options are present in Device History Column",true);
		customReports.ClickOnCancelButton();
	}
	@Test(priority='2',description="Verify Device History Report from custom Report")
	public void DeviceHistory_TC_RE_17() throws InterruptedException
	{
		Reporter.log("\nVerify Device History Report from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceHistory_CustomReportName,"Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.SearchingReportInCustomReportsList(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.VerifyingGeneratedReportInOndemandSection(Config.DeviceHistory_CustomReportName);
	}
	@Test(priority='5',description="Verify Device History Report with filter from custom Report")
	public void DeviceHistory_TC_RE_18() throws InterruptedException
	{
		Reporter.log("\nVerify Device History Report with filter from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceHistory_CustomReportName,"This is Custom report");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceDetails);
		customReports.SelectValueFromColumn(Config.DeviceNameValue);
		customReports.SendValueToTextfield(Config.DeviceDetails_ReportView);
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton(Config.DeviceHistory_CustomReportName);
		customReports.VerifyingReportInViewReportSection(Config.DeviceHistory_CustomReportName);
	}
	@Test(priority='3',description="Verify Device History Report with selected column from custom Report")
	public void DeviceHistory_TC_RE_19() throws InterruptedException
	{
		Reporter.log("\nVerify Device History Report with selected column from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();	
		customReports.ClickOnAddButtonCustomReport();
		//customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.AddingFewColInDevHistoryTable();
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceHistory_CustomReportName,"This is Custom report");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton(Config.DeviceHistory_CustomReportName);
		customReports.VerifyingReportInViewReportSection(Config.DeviceHistory_CustomReportName);
	}
	@Test(priority='4',description="Verify Device History Report with Modify from custom Report")
	public void DeviceHistory_TC_RE_20() throws InterruptedException
	{
		Reporter.log("\nVerify Device History Report with Modify from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceHistory_CustomReportName,"This is Custom report");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.SearchingReportInCustomReportsList(Config.DeviceHistory_CustomReportName);
		customReports.ModifyReport();
		customReports.ClickOnSaveButton_CustomReports();
		Reporter.log("PASS >> Device History custom Report Modified",true);
	}
	
	@Test(priority='6',description="Verify Device History Report with sort from custom Report")
	public void DeviceHistory_TC_RE_07() throws InterruptedException
	{
		Reporter.log("\nVerify Device History Report with sort from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceHistory_CustomReportName,"This is Custom report");
		customReports.Selectvaluefromdropdown(Config.SelectDeviceDetails);
		customReports.SelectValueFromColumn(Config.DeviceNameValue);
		customReports.SendValueToTextfield(Config.DeviceDetails_ReportView);
		customReports.SelectvaluefromsortbydropdownForOrder(Config.SelectAscendingOrder);
		customReports.SelectvaluefromGroupByDropDown(Config.GroupByNameAsDeviceName);
		customReports.SelectvaluefromAggregateOptionsDropDown(Config.SelectAggregateOptionAsMax);
		customReports.SendingAliasName("My Device");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.SelectYesterdayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton(Config.DeviceHistory_CustomReportName);
		customReports.VerifyingReportInViewReportSection(Config.DeviceHistory_CustomReportName);
	}
	@Test(priority='7',description="Verify Device History Report with download from custom Report")
	public void DeviceHistory_TC_RE_09() throws InterruptedException
	{
		Reporter.log("\nVerify Device History Report with download from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceHistory_CustomReportName,"This is Custom report");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.SelectTodayDateInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView(Config.DeviceHistory_CustomReportName);
		reportsPage.WindowHandle();
		customReports.VerifyViewLinkIsClicked();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		customReports.ClickOnCustomReports();
		customReports.DeletingGeneratedReport(Config.DeviceHistory_CustomReportName);
	}
	@Test(priority='8',description="Verify Device History Report from custom Report")
	public void DeviceHistory_TC_RE_03() throws InterruptedException
	{
		Reporter.log("\nVerify Device History Report from custom Report",true);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.DeviceHistory_CustomReportName,"Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickOnAddGroup();
		customReports.ClickOnOneGroup();
		customReports.ClickOnAddGroupButton();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton(Config.DeviceHistory_CustomReportName);
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView(Config.DeviceHistory_CustomReportName);
		reportsPage.WindowHandle();
		customReports.VerifyingDeviceName(Config.Device_Name);	
		customReports.VerifyOS();
		customReports.VerifyingDeviceDate();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
		customReports.ClickOnCustomReports();
		customReports.DeletingGeneratedReport(Config.DeviceHistory_CustomReportName);
	}
	@Test(priority='8',description="Verify Device History Report with download from custom Report")
	public void DeviceHistory_TC_RE_24() throws InterruptedException
	{reportsPage.ClickOnReports_Button();
		customReports.ClickOnCustomReports();
		customReports.ClickOnAddButtonCustomReport();
		customReports.ClickOnColumnInTable(Config.SelectDeviceHistoryTable);
		customReports.ClickOnAddButtonInCustomReport();
		customReports.EnterCustomReportsNameDescription_DeviceDetails("DevHistory Rep to Verify View link","Test");
		customReports.ClickOnSaveButton_CustomReports();
		customReports.ClickOnOnDemandReport();
		customReports.SearchCustomizedReport("DevHistory Rep to Verify View link");
		customReports.ClickOnCustomizedReportNameInOndemand();
		customReports.ClickRequestReportButton();
		customReports.ClickOnViewReportButton();
		customReports.ClickOnSearchReportButton("DevHistory Rep to Verify View link");
		customReports.ClickOnRefreshInViewReport();
		customReports.ClickOnView("DevHistory Rep to Verify View link");
		reportsPage.WindowHandle();
		customReports.SearchBoxInsideViewRep(Config.Device_Name);
		customReports.VerifyingDeviceName(Config.Device_Name);	
		customReports.SearchBoxInsideViewRep(Config.OS_Name);
		customReports.VerifyOS();
		reportsPage.SwitchBackWindow();
		customReports.ClearSearchedReport();
	}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}
