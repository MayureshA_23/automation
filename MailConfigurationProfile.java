package Profiles_Windows_Testscripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class MailConfigurationProfile extends Initialization{

	
	@Test(priority='1',description="To verify clicking on Mail Configuration Policy") 
    public void VerifyClickingOnMailConfigurationPolicy() throws InterruptedException {
    Reporter.log("1.To verify clicking on Mail Configuration Policy",true);
    profilesAndroid.ClickOnProfile();
    profilesWindows.ClickOnWindowsOption();
    profilesWindows.AddProfile();
    profilesWindows.ClickOnMailConfigurationPolicy();
}
	@Test(priority='2',description="1.To Verify warning message Saving Mail Configuration profile without profile Name")
	public void VerifyWarningMessageOnSavingAProfileWithoutName() throws InterruptedException{
		Reporter.log("\n2.clickOnConfigureButton_PasswordPolicye",true);
		profilesWindows.ClickOnConfigureButtonMailConfigurationProfile();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='3',description="To Verify warning message Saving a Mail Configuration Profile without entering all the fields")
	public void VerifyWarningMessageOnSavingMailConfigurationProfileWihtoutName() throws InterruptedException{
		Reporter.log("\n3.To Verify warning message Saving a Mail Configuration Profile without entering all the fields",true);
		profilesWindows.EnterMailConfigurationPolicyProfileName();
		profilesWindows.ClickOnSaveButton();
		profilesWindows.WarningMessageSavingProfileWithoutAllRequiredFields();
	}
	@Test(priority='4',description="To Verify clicking on Add button and launching the Mail configuration window")
	public void VerifyClickingOnAddButttonOfTheMailConfigurationProfile() throws InterruptedException{
		Reporter.log("\n4.To Verify clicking on Add button and launching the Mail configuration window",true);
		profilesWindows.ClickOnAddButton();
		profilesWindows.isMailConfigurationWindowDisplayed();
	}
	
	@Test(priority='5',description="To Verify All the parameters of Mail configuration window")
	public void VerifyParametersOfMailConfigurationWindow() throws InterruptedException{
		Reporter.log("\n5.To Verify All the parameters of Mail configuration window",true);
		profilesWindows.VerifyAlltheOptionsOfMailConfigurationWindow();
	}

	@Test(priority='6',description="To Verify cancelling Mail Configuration Window")
	public void VerifyCancellingMailConfigurationWindow() throws InterruptedException{
		Reporter.log("\n6.To Verify cancelling Mail Configuration Window",true);
		profilesWindows.ClickOnCancelButtonMailConfigurationWindow();
	}
	
	@Test(priority='7',description="To Verify warning while adding Incoming type  Mail Configuration without port number")
	public void VerifyWarningWhileAddingIncomingMailConfigurationWithoutPortNumber() throws InterruptedException{
		Reporter.log("\n7.To Verify warning while adding Incoming type  Mail Configuration without port number",true);
		profilesWindows.ClickOnAddButton();
		profilesWindows.EnterIncomingMailDetails();
		profilesWindows.ClickOnAddButtonMailConfigWindow();
		profilesWindows.WarningWithoutPort();
		
	}
	@Test(priority='8',description="To Verify warning while adding Mail Configuration without Outgoing Mail Configuration")
	public void VerifyWarningWhileAddingWithoutOutgoingMailConfiguration() throws InterruptedException{
		Reporter.log("\n8.To Verify warning while adding Mail Configuration without Outgoing Mail Configuration",true);
		profilesWindows.EnterIncomingPort();
		profilesWindows.ClickOnAddButtonMailConfigWindow();
		profilesWindows.WarningMessageWithoutOutgoing_MailConfiguration();
	}
 
    @Test(priority='9',description="To Verify Adding the Mail Configuration - POP3 type")
	public void VerifyAddingMailConfigurationProfilePOP3Type() throws InterruptedException{
		Reporter.log("\n9.To Verify adding  Mail Configuration",true);
		profilesWindows.ClickOnOutgoingMail();
		profilesWindows.EnterOutgoingServerAndPort();
		profilesWindows.EnterValidEmailAddress();
		profilesWindows.ClickOnAddButtonMailConfigWindow();
		profilesWindows.VerifyAccountNameAccountTypeEmailAddressAfterAdding();
		
	}
    
    
	
	
}
