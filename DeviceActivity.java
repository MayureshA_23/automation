package OnDemandReports_TestScripts;

import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class DeviceActivity extends Initialization {

	@Test(priority = 1, description = "Generate and View the Device Activity Report'")
	public void VerifyClickingOnDeviceActivityReportAndItsHeader_TC_RE_36() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("1.Generate and View the Device Activity Report", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnSelectDevice();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 2, description = "Generate and View the Device Activity Report for Home Group")
	public void VerifyClickingOnDeviceActivityReportAndItsHeader_HomeGroup() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n2.Generate and View the Device Activity Report for Home Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 3, description = "3.Generate and View the Device Activity Report for Sub Group")
	public void VerifyClickingOnDeviceActivityReportAndItsHeader_SubGroup() throws InterruptedException,
			NumberFormatException, EncryptedDocumentException, InvalidFormatException, IOException {

		Reporter.log("\n3.Generate and View the Device Activity Report for Sub Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.clickOnSelectGroupInODRep();
		reportsPage.selectGrpAndAdd(Config.GroupName_Reports);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();
		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 4, description = "4.Download and Verify the data for Device Activity Report for 'Home' Group")
	public void VerifyDownloadingDeviceActivityReport() throws InterruptedException {
		Reporter.log("\n4.Download and Verify the data for Device Activity Report for 'Home' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnSelectDevice();
		reportsPage.SearchDeviceNameInReports(Config.DeviceName);
		reportsPage.SelectSearchedDeviceInReport();
		reportsPage.ClickOnAddButtonInReportsWindow();
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@Test(priority = 5, description = "5.Generate and View the Device Activity Report for Today")
	public void VerifyDeviceActivityReport_Today() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n5.Generate and View the Device Activity Report for Today", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Today");
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();

		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForToday_DeviceActivityReport();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 6, description = "6.Generate and View the Device Activity Report for Yesterday")
	public void VerifyDeviceActivityReport_Yesterday() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n6.Generate and View the Device Activity Report for Yesterday", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Yesterday");
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();

		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForYesterdayInDeviceActivityReport();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();
	}

	@Test(priority = 7, description = "7.Generate and View the Device Activity Report for Last1week")
	public void VerifyDeviceActivityReport_Last1week() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n7.Generate and View the Device Activity Report for Last1week", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 1 Week");
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();

		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast1week_DeviceActivity();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 8, description = "8.Generate and View the Device Activity Report for Last30Days")
	public void VerifyDeviceActivityReport_Last30Days() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n8.Generate and View the Device Activity Report for Last30Days", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("Last 30 Days");
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();

		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForLast30Days_DeviceActivity();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 9, description = "9.Generate and View the Device Activity Report for ThisMonth")
	public void VerifyDeviceActivityReport_ThisMonth() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n9.Generate and View the Device Activity Report for ThisMonth", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnDate();
		reportsPage.ClickOnPeriod("This Month");
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();

		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForThisMonth_DeviceActivity();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();

	}

	@Test(priority = 10, description = "10.Generate and View the Device Activity Report for CustomRange")
	public void VerifyDeviceActivityReport_CustomRange() throws InterruptedException, NumberFormatException,
			EncryptedDocumentException, InvalidFormatException, IOException, ParseException {

		Reporter.log("\n10.Generate and View the Device Activity Report for CustomRange", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.ClickOnDate();
		reportsPage.SelectingDateRange_InstalledJob();
		accountsettingspage.ClickONRequestReportButton();

		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.ClickOnView();
		reportsPage.WindowHandle();

		reportsPage.VerifyOfReportOpenedInNextTab();
		reportsPage.verifySelectedDateForCustomRange_DeviceActivity();
		reportsPage.SearchDeviceNameInAppVersionReports(Config.DeviceName);
		reportsPage.DeviceNameColumnVerificationDeviceActivityReport(Config.DeviceName);
		reportsPage.ActivityColumnVerificationDeviceActivityReport();
		reportsPage.VerifyDeviceActivityReportColumns();
		reportsPage.SwitchBackWindow();

	}
	@Test(priority = 11, description = "11.Download and Verify the data for Device Activity Report for 'Sub' Group")
	public void VerifyDownloadingDeviceActivityReportForSubGroup() throws InterruptedException {
		Reporter.log("\n11.Download and Verify the data for Device Activity Report for 'Sub' Group", true);
		reportsPage.ClickOnReports_Button();
		reportsPage.ClickOnOnDemandReport();
		reportsPage.ClickingOnDeviceActivityReport();
		reportsPage.clickOnSelectGroupInODRep();
		reportsPage.selectGrpAndAdd(Config.GroupName_Reports);
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		reportsPage.DownloadReport();
	}

	@AfterMethod
	public void ttt(ITestResult result) throws InterruptedException {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(driver, result.getName());
			reportsPage.SwitchBackWindow();
		}
	}
}
