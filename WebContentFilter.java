package Profiles_iOS_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class WebContentFilter extends Initialization{
	


	@Test(priority='1',description="1.To verify clicking on the Web Content filter option")
	public void VerifyClickingOnWebContentFilter() throws InterruptedException{
		profilesAndroid.ClickOnProfile();
		profilesiOS.clickOniOSOption();
		profilesiOS.AddProfile();
		profilesiOS.ClickOnWebContentFilter();
		}
	
	@Test(priority='2',description="1.To verify summary web content filter")
	public void VerifySummaryofWebContentFilter() throws InterruptedException
	{
		profilesiOS.VerifySummaryof_WebContentFilter();
	}
	
	@Test(priority='3',description="1.To Verify warning message Saving a profile")
	public void VerifyWarningMessageOnSavingAProfileWithoutName() throws InterruptedException{
		Reporter.log("=======To Verify warning messages while Saving a Password Policy profile=========");
		profilesiOS.clickOnConfigureButton();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutName();
	}
	@Test(priority='4',description="1.To Verify warning message Saving a profile without entering all the fields")
	public void VerifyWarningMessageOnSavingAProfileEnteringRequiredFields() throws InterruptedException{
		Reporter.log("=======To Verify warning message Saving a profile without entering all the fields=========");
		profilesiOS.EnterWebContentFilterProfileName();
		profilesiOS.ClickOnSavebutton();
		profilesiOS.WarningMessageSavingProfileWithoutAllRequiredFields();
		
	}
	
	@Test(priority='5',description="to verify clicking on add button without adding an blacklisted URL")
	public void VerifyWarningMessageWithoutAddingBlacklistedURL() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify clicking on add button without adding an blacklisted URL");
		
		profilesiOS.ClickOnAddButtonWebContentFilter();
		profilesiOS.ClickOnAddButtonURL();
		profilesiOS.WarningMessageOnClickingAddButtonWithoutBlacklistedURL();
	}
	@Test(priority='6',description="to verify adding valid blacklisted URL")
	public void VerifyAddingValidBlackistedURL() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify adding valid blacklisted URL");
		
		profilesiOS.EnterBlacklistURL();
		profilesiOS.ClickOnAddButtonURL();
		profilesiOS.isBlacklistedURL_displayed();
	}
	@Test(priority='7',description="to verify warning message without whitelist bookmarks title")
	public void VerifyWarningWithoutBookmarkTitleWhitelistBookmarks() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify adding whitelist bookmarks");
	    profilesiOS.ClickOnWhitelistBookmarks();
	    profilesiOS.ClickOnAddButtonWhitelistURL();
	    profilesiOS.ClickOnAddButtonURL();
	    profilesiOS.WarningMessageOnClickingAddButtonWithoutWhitelistBookmark();
	    profilesiOS.EnterTitleWhitelistBookmarkTITLE();
	}
	@Test(priority='8',description="to verify warning message without valid whitelist URL")
	public void VerifyWarningWithoutWhitelistURL() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify warning message without valid whitelist URL");
	    profilesiOS.ClickOnAddButtonURL();
	    profilesiOS.WarningMessageOnClickingAddButtonWithoutBlacklistedURL();//same for URL field of whitelist bookmark
	    profilesiOS.EnterWhitelistBookmark_URL();
	}
	@Test(priority='9',description="to verify warning message without bookmark path")
	public void VerifyWarningWithoutBookmarkPath() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify warning message without bookmark path");
	    profilesiOS.ClickOnAddButtonURL();
	    profilesiOS.WarningMessageOnClickingAddButtonWithoutBookmarkPath();
	    profilesiOS.EnterWhitelistBookmark_Bookmarkpath();
	}
	@Test(priority='A',description="to verify saving whitelist bookmark")
	public void VerifySavingWhitelistBookmark() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify saving whitelist bookmark");
	    profilesiOS.ClickOnAddButtonURL();
	    profilesiOS.isWhitelistBookmark_displayed();
	    
	}
	
	@Test(priority='B',description="to verify warning message without filtered URL")
	public void VerifyWarningWithoutFilteredURL() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify warning message without filtered URL");
	    profilesiOS.ClickOnAutoFilter();	
	    profilesiOS.CheckEnableAutoFiltering();
	    profilesiOS.ClickOnAddButton_AutoFilter();
	    profilesiOS.ClickOnAddButtonURL();
	    profilesiOS.WarningMessageOnClickingAddButtonWithoutBlacklistedURL();//common for this as well
	    profilesiOS.EnterAutoFilterURL();
	 }
	
	@Test(priority='C',description="to verify saving Auto Filter URL ")
	public void VerifySavingAutoFilter() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{   Reporter.log("to verify saving Auto Filter URL");
	    profilesiOS.ClickOnAddButtonURL();
	    profilesiOS.isAutoFilterURL_displayed();
	    
	}
	@Test(priority='D',description="To verify saving the Web Content Filter Profile")
	public void Verify_SavingWebContentFilterProfile() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{
		Reporter.log("To verify saving the Web Content Filter Profile",true);
		profilesiOS.ClickOnSavebutton();
	    profilesAndroid.NotificationOnProfileCreated();
	}
	
	
	
}
