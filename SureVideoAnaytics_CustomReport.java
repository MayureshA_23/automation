package CustomReportsScripts;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class SureVideoAnaytics_CustomReport extends Initialization{
	@Test(priority='1',description="Verify Duration column in SureVideo Anaytics Report should be displayed as HH:MM:SS") 
	public void VerifySureVideoAnalytics_TC_RE_224() throws InterruptedException
		{Reporter.log("\nVerify Duration column in SureVideo Anaytics Report should be displayed as HH:MM:SS",true);
			
			commonmethdpage.ClickOnHomePage();
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureVideoAnalytics CustRep to verify DurationCol In ViewRep","test");
			customReports.ClickOnColumnInTable("SureVieo Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureVideoAnalytics CustRep to verify DurationCol In ViewRep");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureVideoAnalytics CustRep to verify DurationCol In ViewRep");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureVideoAnalytics CustRep to verify DurationCol In ViewRep");
			reportsPage.WindowHandle();
			customReports.ChooseDevicesPerPage();
			customReports.CheckingDurationColDateFormat_InSureLockAnalytics();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		}
	@Test(priority='2',description="Verify duration column filter for the SureVideo Analytics report") 
	public void VerifySureVideoAnalytics_TC_RE_228() throws InterruptedException
		{Reporter.log("\nVerify duration column filter for the SureVideo Analytics report",true);
			reportsPage.ClickOnReports_Button();
			customReports.ClickOnCustomReports();
			customReports.ClickOnAddButtonCustomReport();
			customReports.ClickOnColumnInTable("SureVieo Analytics");
			customReports.ClickOnAddButtonInCustomReport();
			customReports.EnterCustomReportsNameDescription_DeviceDetails("SureVieoAnalytics CustRep duration column filter","test");
			customReports.Selectvaluefromdropdown("SureFox Analytics");
			customReports.SelectValueFromColumn("Duration");
			customReports.SelectOperatorFromDropDown("<=");
			customReports.SendValueToTextfield(Config.DurationFormat3);
			customReports.ClickOnSaveButton_CustomReports();
			customReports.ClickOnOnDemandReport();
			customReports.SearchCustomizedReport("SureVieoAnalytics CustRep duration column filter");
			customReports.ClickOnCustomizedReportNameInOndemand();
			customReports.SelectLast30daysInOndemand();
			customReports.ClickRequestReportButton();
			customReports.ClickOnViewReportButton();
			customReports.ClickOnSearchReportButton("SureVieoAnalytics CustRep duration column filter");
			customReports.ClickOnRefreshInViewReport();
			customReports.ClickOnView("SureVieoAnalytics CustRep duration column filter");
			reportsPage.WindowHandle();
			customReports.ChooseDevicesPerPage();
			customReports.CheckingDurationColDateFormat_InSureFoxAnalytics();
			customReports.VerifyingDurationColForGivenRange_InSureVideoAnalytics();
			reportsPage.SwitchBackWindow();
			customReports.ClearSearchedReport();
		
		}
	@AfterMethod
	public void CollectDeviceLogs(ITestResult result) throws InterruptedException
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try
			{
				String FailedWindow = Initialization.driver.getWindowHandle();	
				if(FailedWindow.equals(customReports.PARENTWINDOW))
				{
					commonmethdpage.ClickOnHomePage();
				}
				else
				{
					reportsPage.SwitchBackWindow();
				}
			} 
			catch (Exception e) 
			{
				
			}
		}
	}
}


