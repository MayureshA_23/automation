package Common;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import Library.Config;
import Library.Driver;
import Library.ExcelLib;
import Library.Helper;
import Library.Utility;
import PageObjectRepository.AccountRegistrationPOM;
import PageObjectRepository.AccountSettings_POM;
import PageObjectRepository.ActivityLog_POM;
import PageObjectRepository.AndroidJobPOM;
import PageObjectRepository.AppStore_POM;
import PageObjectRepository.AvaylerPOM;
import PageObjectRepository.Blacklisted_POM;
import PageObjectRepository.BuySubscription_POM;
import PageObjectRepository.ChangePassword_POM;
import PageObjectRepository.CommonMethods_POM;
import PageObjectRepository.ContactUs_POM;
import PageObjectRepository.CustomReports_POM;
import PageObjectRepository.DashBoard_POM;
import PageObjectRepository.DeepThoughts_POM;
import PageObjectRepository.DeviceEnrollement_POM;
import PageObjectRepository.DeviceEnrollmentHonor8_ScanQR_POM;
import PageObjectRepository.DeviceGrid_POM;
import PageObjectRepository.DeviceInfoPanel_POM;
import PageObjectRepository.DynamicJobs_POM;
import PageObjectRepository.DynamicRefresh_JobQueueQuickAction_POM;
import PageObjectRepository.FileStore_POM;
import PageObjectRepository.Filters_POM;
import PageObjectRepository.GridPaginationPOM;
import PageObjectRepository.Groups_POM;
import PageObjectRepository.HelpVideos_POM;
import PageObjectRepository.Help_POM;
import PageObjectRepository.IOS_DeviceInfo_POM;
import PageObjectRepository.InboxPOM;
import PageObjectRepository.JobsOnAndroidPOM;
import PageObjectRepository.LicenseManagementPOM;
import PageObjectRepository.LocationTracking_DeviceInfo_POM;
import PageObjectRepository.LoginLogoutPOM;
import PageObjectRepository.MacOS_Deviceinfo_POM;
import PageObjectRepository.Message_POM;
import PageObjectRepository.MiscellaneousGridPerformance_POM;
import PageObjectRepository.NewUIScripts_POM;
import PageObjectRepository.NixSignUpMailValidationPOM;
import PageObjectRepository.Nixdatausage;
import PageObjectRepository.PendingDelete_POM;
import PageObjectRepository.PreApproved_POM;
import PageObjectRepository.ProfilesPOM;
import PageObjectRepository.Profiles_Android_POM;
import PageObjectRepository.Profiles_Windows_POM;
import PageObjectRepository.Profiles_iOS_POM;
import PageObjectRepository.QuickActionToolBar_POM;
import PageObjectRepository.RemoteSupport_POM;
import PageObjectRepository.ReportsPOM;
import PageObjectRepository.RightClick_POM;
import PageObjectRepository.RunScript_POM;
import PageObjectRepository.ScheduleReports_POM;
import PageObjectRepository.Tags_POM;
import PageObjectRepository.Unapproved_POM;
import PageObjectRepository.UserManagement_POM;
import PageObjectRepository.UserManagement_POMold;
import PageObjectRepository.WindowsDeviceGrid_POM;
import PageObjectRepository.buildhealth_POM;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class Initialization {

	public static WebDriver driver;
	public static AndroidDriver driverAppium;
	public static AndroidDriver driverAppium1;
	public static LoginLogoutPOM loginPage;
	public static CommonMethods_POM commonmethdpage;
	public static UserManagement_POM usermanagement;
	public static InboxPOM inboxpage;
	public static AccountSettings_POM accountsettingspage;
	public static Message_POM dynamicmessagepage;
	public static ReportsPOM reportsPage;
	public static Groups_POM groupspage;
	public static QuickActionToolBar_POM quickactiontoolbarpage;
	public static ChangePassword_POM changepwdpage;
	public static Blacklisted_POM blacklistedpage;
	public static ExcelLib eLib;
	public static DynamicJobs_POM dynamicjobspage;
	public static JobsOnAndroidPOM jobsOnAndroidpage;
	public static RemoteSupport_POM remotesupportpage;
	public static ScheduleReports_POM schedulereportpage;
	public static ContactUs_POM contactUsPage;
	public static Help_POM helpPage;
	public static DashBoard_POM dashboardpage;
	public static HelpVideos_POM helpVideos;
	public static DeviceInfoPanel_POM deviceinfopanelpage;
	public static LicenseManagementPOM licenseMgmt;
	public static DeviceGrid_POM devicegridpage;
	public static DeviceEnrollement_POM deviceEnrollment;
	public static BuySubscription_POM buySubscription;
	public static Unapproved_POM UnApproved;
	public static RemoteSupport_POM remoteSupport;
	public static UserManagement_POMold usermanagementpage;
	public static ProfilesPOM profilespage;
	public static ActivityLog_POM activitylogpage;
	public static AppStore_POM appstorepage;
	public static FileStore_POM filestorepage;
	public static Profiles_Android_POM profilesAndroid;
	public static Profiles_iOS_POM profilesiOS;
	public static Profiles_Windows_POM profilesWindows;
	public static NixSignUpMailValidationPOM nixSignUpmail;
	public static CustomReports_POM customReports;
	public static DeviceGrid_POM deviceGrid;
	public static AndroidJobPOM androidJOB;
	public static DeepThoughts_POM deepthoughts;
	public static RunScript_POM runScript;
	public static DynamicRefresh_JobQueueQuickAction_POM DynamicRefreshingDevice;
	public static MiscellaneousGridPerformance_POM MiscellaneousGridPerformance;
	public static LocationTracking_DeviceInfo_POM TrackLocation;
	public static IOS_DeviceInfo_POM IOSDeviceInfo;
	public static MacOS_Deviceinfo_POM MacOSDeviceInfo;
	public static buildhealth_POM buildhealth;
	public static DeviceEnrollmentHonor8_ScanQR_POM DeviceEnrollment_SCanQR;
	public static Nixdatausage nixdatausage;
	public static Filters_POM Filter;
	public static RightClick_POM rightclickDevicegrid;
	public static AccountRegistrationPOM accountRegistrationPage;
	public static ReportsPOM reports;
	public static WindowsDeviceGrid_POM devicegridwindows;
	public static PendingDelete_POM PendingDelete;
	public static PreApproved_POM PreApproved;
	public static Tags_POM Tags;
	public static GridPaginationPOM gridpagination;
	public static NewUIScripts_POM newUIScriptsPage;
	public static AvaylerPOM AvaylerScript ;

	@Parameters({ "device", "apppackage", "activity", "version", "appiumServer" })
	// @BeforeSuite
	@BeforeClass

	public void configBeforClass() throws InterruptedException {
		// launch browser

		driver = Driver.getBrowser();
		gridpagination = PageFactory.initElements(driver, GridPaginationPOM.class);
		rightclickDevicegrid = PageFactory.initElements(driver, RightClick_POM.class);
		Filter = PageFactory.initElements(driver, Filters_POM.class);
		DeviceEnrollment_SCanQR = PageFactory.initElements(driver, DeviceEnrollmentHonor8_ScanQR_POM.class);
		buildhealth = PageFactory.initElements(driver, buildhealth_POM.class);
		MacOSDeviceInfo = PageFactory.initElements(driver, MacOS_Deviceinfo_POM.class);
		IOSDeviceInfo = PageFactory.initElements(driver, IOS_DeviceInfo_POM.class);
		PendingDelete = PageFactory.initElements(driver, PendingDelete_POM.class);
		PreApproved = PageFactory.initElements(driver, PreApproved_POM.class);
		devicegridwindows = PageFactory.initElements(driver, WindowsDeviceGrid_POM.class);
		runScript = PageFactory.initElements(driver, RunScript_POM.class);
		deepthoughts = PageFactory.initElements(driver, DeepThoughts_POM.class);
		loginPage = PageFactory.initElements(driver, LoginLogoutPOM.class);
		usermanagement = PageFactory.initElements(driver, UserManagement_POM.class);// usermang
		inboxpage = PageFactory.initElements(driver, InboxPOM.class);
		reportsPage = PageFactory.initElements(driver, ReportsPOM.class);
		groupspage = PageFactory.initElements(driver, Groups_POM.class);
		accountsettingspage = PageFactory.initElements(driver, AccountSettings_POM.class);
		commonmethdpage = PageFactory.initElements(driver, CommonMethods_POM.class);
		dynamicmessagepage = PageFactory.initElements(driver, Message_POM.class);
		changepwdpage = PageFactory.initElements(driver, ChangePassword_POM.class);
		remotesupportpage = PageFactory.initElements(driver, RemoteSupport_POM.class);
		jobsOnAndroidpage = PageFactory.initElements(driver, JobsOnAndroidPOM.class);
		schedulereportpage = PageFactory.initElements(driver, ScheduleReports_POM.class);
		quickactiontoolbarpage = PageFactory.initElements(driver, QuickActionToolBar_POM.class);
		contactUsPage = PageFactory.initElements(driver, ContactUs_POM.class);
		blacklistedpage = PageFactory.initElements(driver, Blacklisted_POM.class);
		helpPage = PageFactory.initElements(driver, Help_POM.class);
		devicegridpage = PageFactory.initElements(driver, DeviceGrid_POM.class);
		dynamicjobspage = PageFactory.initElements(driver, DynamicJobs_POM.class);
		UnApproved = PageFactory.initElements(driver, Unapproved_POM.class);
		helpVideos = PageFactory.initElements(driver, HelpVideos_POM.class);
		licenseMgmt = PageFactory.initElements(driver, LicenseManagementPOM.class);
		deviceEnrollment = PageFactory.initElements(driver, DeviceEnrollement_POM.class);
		buySubscription = PageFactory.initElements(driver, BuySubscription_POM.class);
		remoteSupport = PageFactory.initElements(driver, RemoteSupport_POM.class);
		deviceinfopanelpage = PageFactory.initElements(driver, DeviceInfoPanel_POM.class);
		// usermanagementpage = PageFactory.initElements(driver,
		// UserManagement_POMold.class);
		activitylogpage = PageFactory.initElements(driver, ActivityLog_POM.class);
		appstorepage = PageFactory.initElements(driver, AppStore_POM.class);
		dashboardpage = PageFactory.initElements(driver, DashBoard_POM.class);
		filestorepage = PageFactory.initElements(driver, FileStore_POM.class);
		profilespage = PageFactory.initElements(driver, ProfilesPOM.class);
		profilesAndroid = PageFactory.initElements(driver, Profiles_Android_POM.class);
		profilesiOS = PageFactory.initElements(driver, Profiles_iOS_POM.class);
		profilesWindows = PageFactory.initElements(driver, Profiles_Windows_POM.class);
		nixSignUpmail = PageFactory.initElements(driver, NixSignUpMailValidationPOM.class);
		customReports = PageFactory.initElements(driver, CustomReports_POM.class);
		deviceGrid = PageFactory.initElements(driver, DeviceGrid_POM.class);
		remotesupportpage = PageFactory.initElements(driver, RemoteSupport_POM.class);
		androidJOB = PageFactory.initElements(driver, AndroidJobPOM.class);
		MiscellaneousGridPerformance = PageFactory.initElements(driver, MiscellaneousGridPerformance_POM.class);
		TrackLocation = PageFactory.initElements(driver, LocationTracking_DeviceInfo_POM.class);
		DynamicRefreshingDevice = PageFactory.initElements(driver, DynamicRefresh_JobQueueQuickAction_POM.class);
		nixdatausage = PageFactory.initElements(driver, Nixdatausage.class);
		Tags = PageFactory.initElements(driver, Tags_POM.class);
		accountRegistrationPage = PageFactory.initElements(driver, AccountRegistrationPOM.class);
		newUIScriptsPage = PageFactory.initElements(driver, NewUIScripts_POM.class);
		AvaylerScript = PageFactory.initElements(driver, AvaylerPOM.class);
		// login method
		
		//loginPage.loginToAPP(Config.userID, Config.password, Config.url);
		loginPage.loginToAPP(Config.url);
		eLib = new ExcelLib();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterMethod
	public void TakeScreenshotForFailedTestCases(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			Utility.captureScreenshot(Initialization.driver, result.getName());
		}
	}

	/*
	 * @AfterClass public void afterclass() { driver.quit();
	 * 
	 * }
	 */

	 


}
