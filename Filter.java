package Filter_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import Library.Utility;

public class Filter extends Initialization {

		@Test(priority='0',description="Counting Online Devices in grid before creating filter")
		public void GettingOnlineDevicesCount() throws InterruptedException
		{
			System.out.println(">> Counting Online Devices in grid before creating filter");
			Filter.ClickOnAllDevices();
			Filter.ClickOnDevicesPerPagebutton();
			Filter.chooseMaxDevicesPerPage();
			Filter.OnlineDevicesCountInGrid();
			Filter.OfflineDevicesCountInGrid();
			Filter.ClickOnHomeGroup();	
			System.out.println("");
		}
	
		@Test(priority='1',description="Verify Filters option next to Tags.")
		public void VerifyFilterOption() throws InterruptedException
		{
			System.out.println("Test Case 1: Verify Filters option next to Tags.");
			Filter.ClickOnFilter();
			//Filter.VerifyFilterDisplayed();
			System.out.println("");
		}
		
		@Test(priority='2',description="Verify Clicking on (+) button shows Save Filter button")
		public void VerifyPlusButtonFilter() throws InterruptedException
		{
			System.out.println("Test Case 2: Verify Clicking on (+) button shows Save Filter button");
			Filter.ClickOnPlusButton();
			Filter.VerifySaveFilterButton();
			System.out.println("");
		}
		
		@Test(priority='3',description="Verify Creating Online Status Filter")
		public void VerifyCreatingOnlineStatusFilter() throws InterruptedException, AWTException
		{
			System.out.println("Test Case 3: Verify Creating Online Status Filter");
			Filter.ClickOnStatus();
			Filter.SelectOnlineStatus();
			Filter.ClickOnSaveFilterButton();
			Filter.EnterFilterName("Online Devices");
			Filter.ClickOKButtonFilter("Online Devices");
			Filter.ClickOnCreatedFilter(Config.OnlineStatus);
			Filter.OnlineDeviceCountMatchFilterandGrid();
			System.out.println("");
		}
		
		@Test(priority='4',description="Verify Creating Offline Status Filter")
		public void VerifyCreatingOfflineStatusFilter() throws InterruptedException, AWTException
		{
			System.out.println("Test Case 4: Verify Creating Offline Status Filter");
			Filter.ClickOnPlusButton();
			Filter.VerifySaveFilterButton();
			Filter.ClickOnStatus();
			Filter.SelectOfflineStatus();
			Filter.ClickOnSaveFilterButton();
			Filter.EnterFilterName("Offline Devices");
			Filter.ClickOKButtonFilter("Offline Devices");
			Filter.ClickOnCreatedFilter(Config.OfflineStatus);
			Filter.OffineDeviceCountMatchFilterandGrid();
			System.out.println("");
		}
		
	
		@Test(priority='5',description="Verify deleting Online Filter from the filter list.")
		public void VerifyDeleteOnlineFilter() throws InterruptedException
		{
			System.out.println("Test Case 5: Verify deleting Filter from the filter list.");
			Filter.ClickOnCreatedFilter(Config.OnlineStatus);
			Filter.CheckDeviceInFilterBeforeDelete();
			Filter.ClickOnMinusButton();
			Filter.WarningMessageOnDeleteFilter("Online Devices");
			Filter.ClickOnDeleteFilter();
			Filter.MessageOnFilterDelete("Online");
			System.out.println("");
		}
		
		@Test(priority='6',description="Verify deleting the filter should not delete the device present in that filter")
		public void VerifyDeletingOnlineFilterShouldNotDeleteTheDevice() throws InterruptedException
		{
			 System.out.println("Test Case 6: Verify deleting Online filter should not delete the device present in that filter");
			 Filter.ClickOnGroups();
			 Filter.DeviceSearchAfterDeleteFilter();
			 Filter.IfDevicePresentAfterFilterDelete();
			 System.out.println("");
		}
		
		@Test(priority='7',description="Verify deleting Offline Filter from the filter list.")
		public void VerifyDeleteOfflineFilter() throws InterruptedException
		{
			System.out.println("Test Case 7: Verify deleting Offline Filter from the filter list.");
			Filter.ClickOnFilter();
			Filter.ClickOnCreatedFilter(Config.OfflineStatus);
			Filter.CheckDeviceInFilterBeforeDelete();
			Filter.ClickOnMinusButton();
			Filter.WarningMessageOnDeleteFilter("Offline Devices");
			Filter.ClickOnDeleteFilter();
			Filter.MessageOnFilterDelete("Offline");
			System.out.println("");
			//Filter.TakeLogs();
			//Filter.analyzeLog();
		}
		
		@Test(priority='8',description="Verify deleting the filter should not delete the device present in that filter")
		public void VerifyDeletingOfflineFilterShouldNotDeleteTheDevice() throws InterruptedException
		{
			 System.out.println("Test Case 8: Verify deleting offline filter should not delete the device present in that filter");
			 Filter.ClickOnGroups();
			 Filter.DeviceSearchAfterDeleteFilter();
			 Filter.IfDevicePresentAfterFilterDelete();
			 System.out.println("");
		}
		
		@Test(priority='9',description="Verify deleting the filter should not delete the device present in that filter")
		public void GoingBackto20PerPage() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
		{
			 System.out.println("Verify deleting offline filter should not delete the device present in that filter");
			 commonmethdpage.ClearSearchDeviceWithEnter();
			 Filter.ClickOnDevicesPerPagebutton();
	         Filter.chooseDevicesPerPage20();
		}

	// Madhu
		
	@Test(priority = 'A', description = "Verify creating filter with N/A value in custom column")
	public void verifyNAInCC_TC_FL_012() throws InterruptedException, AWTException {
		Reporter.log("10.Verify creating filter with N/A value in custom column", true);
		//			Filter.deleteExistingFilter(Config.customColumn);
		deviceGrid.DisablingAllCustomColumns();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumnName);
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumnName);
		deviceGrid.ClickOnCustomColumnsCloseButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumnName);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumnName);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumnName);
		Filter.ClickOnFilter();
		Filter.ClickOnPlusButton();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll(Config.CustomColumnName);
		deviceGrid.EnterValueForAdvanceSeach("N/A", "CustomColumn1");
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName(Config.customColumn);
		Filter.ClickOKButtonFilter(Config.customColumn);
		Filter.ClickOnCreatedFilter(Config.customColumn);
		Filter.verifyCustomColumnFilter("CustomColumn1");
	}

	@Test(priority = 'B', description = "Verify Creating filter with combination of Custom column and Normal columns")
	public void verifyNAInCCAndSL_TC_FL_014() throws InterruptedException, AWTException {
		Reporter.log("11.Verify Creating filter with combination of Custom column and Normal columns", true);
		deviceGrid.DisablingAllCustomColumns();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumnName);
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumnName);
		deviceGrid.ClickOnCustomColumnsCloseButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumnName);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumnName);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumnName);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("SureLock Version");
		deviceGrid.Enable_DisbleColumn("SureLock Version", "Enable");
		Filter.ClickOnFilter();
		Filter.ClickOnPlusButton();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("SureLock Version");
		deviceGrid.EnterValueForAdvanceSeach("N/A", "SureLockVersion");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll(Config.CustomColumnName);
		deviceGrid.EnterValueForAdvanceSeach("N/A", "CustomColumn1");
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName(Config.CombinationOfCol);
		Filter.ClickOKButtonFilter(Config.CombinationOfCol);
		Filter.ClickOnCreatedFilter(Config.CombinationOfCol);
		Filter.verifyCustomColumnFilter("CustomColumn1");
		Filter.verifySureLockVerColumnFilter();
	}

	@Test(priority = 'C', description = "Verify Creating Filter with multiple Custom Columns")
	public void verifyNAInMultipleCol_TC_FL_014() throws InterruptedException, AWTException {
		Reporter.log("12.Verify Creating Filter with multiple Custom Columns", true);
		deviceGrid.DisablingAllCustomColumns();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.ClickOnCustomColumnConfigbutton();
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumnName);
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumnName);
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName1);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumnName1);
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumnName1);
		deviceGrid.DeletingExistingCustomColumn(Config.CustomColumnName2);
		deviceGrid.ClickOnAddCustomColumnButton();
		deviceGrid.AddingNewCustomColumn(Config.CustomColumnName2);
		deviceGrid.VerifyCustomColumnsAddedSuccessfully(Config.CustomColumnName2);
		deviceGrid.ClickOnCustomColumnsCloseButton();
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumnName);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumnName);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumnName);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumnName1);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumnName1);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumnName1);
		deviceGrid.ClickOnColumnsButton();
		deviceGrid.ClickOncustomColumnList();
		deviceGrid.SearchingCustomColumnInColumnList(Config.CustomColumnName2);
		deviceGrid.SelectingCustomColumnCheckBoxInColumnlist(Config.CustomColumnName2);
		deviceGrid.VerifySelectedCstomColumnIsPresentInGrid(Config.CustomColumnName2);
		Filter.ClickOnFilter();
		Filter.ClickOnPlusButton();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll(Config.CustomColumnName);
		deviceGrid.EnterValueForAdvanceSeach("N/A", "CustomColumn1");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll(Config.CustomColumnName);
		deviceGrid.EnterValueForAdvanceSeach("N/A", "CustomColumn2");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll(Config.CustomColumnName);
		deviceGrid.EnterValueForAdvanceSeach("N/A", "CustomColumn3");
		Filter.ClickOnSaveFilterButton();
		Filter.EnterFilterName(Config.multipleCustCol);
		Filter.ClickOKButtonFilter(Config.multipleCustCol);
		Filter.ClickOnCreatedFilter(Config.multipleCustCol);
		Filter.verifyCustomColumnFilter("CustomColumn1");
		Filter.verifyCustomColumnFilter("CustomColumn2");
		Filter.verifyCustomColumnFilter("CustomColumn3");
	}

	
	  @AfterMethod
			public void CollectDeviceLogs(ITestResult result) throws InterruptedException
			{
				if(ITestResult.FAILURE==result.getStatus())
				{
					try {
						
						Utility.captureScreenshot(Initialization.driver, result.getName());
						Thread.sleep(60000);
						Runtime.getRuntime().exec("adb -s HKE87WXV pull /sdcard/SureMDM_Log.txt D:\\Automation\\SureMDM_POM_FrameWork\\Logs");
				
					} catch (IOException e) {
						
						e.printStackTrace();
					}
				}
			}
		}
