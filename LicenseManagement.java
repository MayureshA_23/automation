package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;

public class LicenseManagement extends Initialization {
	
	
	@Test(priority=1,description="Verify License counts/Storage size are displayed in License Management for existing accounts") 
	public void VerifyLicensecountsStoragesizearedisplayedinLicenseManagementforexistingaccountsTC_ST_677() throws Throwable {
		Reporter.log("\n1.Verify License counts/Storage size are displayed in License Management for existing accounts",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonlicenseManagement();	
		accountsettingspage.licenseManagementDetailsCount();
		accountsettingspage.CloselicenseManagementTab();		
}
	
	@Test(priority=2,description="Verify SureLock/SureFox/SureVideo Activation code is displayed in License Management for existing accounts") 
	public void VerifySureLockSureFoxSureVideoActivationcodeisdisplayedinLicenseManagementforexistingaccountsTC_ST_678() throws Throwable {
		Reporter.log("\n2.Verify SureLock/SureFox/SureVideo Activation code is displayed in License Management for existing accounts",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickonlicenseManagement();	
		accountsettingspage.licenseManagementSureLockSureFoxSureVideoCode();
		accountsettingspage.CloselicenseManagementTab();
}
	
	@Test(priority=3,description="Verify VR license used field is displaying in Settings dropdown.") 
	public void VerifyVRlicenseusedfieldisdisplayinginSettingsdropdownTC_ST_644() throws Throwable {
		Reporter.log("\n3.Verify VR license used field is displaying in Settings dropdown.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.VRLicensesUsed();		
}
	
	
	//set pre condition
	//String url = "https://worldoftesting.eu.suremdm.io/console/"; String userID ="abdulemithilesh@gmail.com"; String password = "Mithilesh@123";
//	@Test(priority=4,description="Verify VR license for the Trial Account.") 
//	public void VerifyVRlicensefortheTrialAccountTC_ST_645() throws Throwable {
//		Reporter.log("\n4.Verify VR license for the Trial Account.",true);		
//		accountsettingspage.Closewindow();
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.VRLicensesUsed();		
//}
	
	
	
	
	

}
