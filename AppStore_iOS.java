package AppStore_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class AppStore_iOS extends Initialization{


	@Test(priority='1',description="1.To Verify App Store Window's UI")
	public void AppStore_iOS_TC1() throws InterruptedException{
		Reporter.log("\n1.To Verify App Store Window's UI",true);
		appstorepage.ClickOnAppStore();
		appstorepage.VerifyOfAppStoreWindow();
		Reporter.log("PASS>> Verification of App Store Window's UI",true);
	}
	
	@Test(priority='2',description="2.To Verify Add New Pop Window's UI for iOS Platform of App Store")
	public void AppStore_iOS_TC2() throws InterruptedException{
		Reporter.log("\n2.To Verify Add New Pop Window's UI for iOS Platform of App Store",true);
		appstorepage.ClickOniOSAppStore();
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.VerifyOfAddNewAppPopupiOS();
		appstorepage.ClickOnCloseAddNewAppiOS();
		Reporter.log("PASS>> Verification of Add New Pop Window's UI for iOS Platform of App Store",true);
	}
	
	@Test(priority='3',description="3.To Verify Upload ipa Pop Window's UI in iOS Platform of App Store")
	public void AppStore_iOS_TC3() throws InterruptedException{
		Reporter.log("\n3.To Verify Upload ipa Pop Window's UI for iOS Platform of App Store",true);
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnUploadIPAiOS();
		appstorepage.VerifyOfUploadIpaPopupiOS();
		appstorepage.ClickOnCloseAddNewAppiOS();
		Reporter.log("PASS>> Verification of Upload ipa Pop Window's UI for iOS Platform of App Store",true);
	}
	
	@Test(priority='4',description="4.To Verify Upload ipa of App Store with Category and Description fields is empty")
	public void AppStore_iOS_TC4() throws Throwable{
		Reporter.log("\n4.To Verify Upload ipa of App Store with Category and Description fields is empty",true);
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnUploadIPAiOS();
		appstorepage.ClickOnBrowseFileiOS();
		appstorepage.VerifyOfUploadiPAiOS();
		appstorepage.ClickOnUploadipaPopupAddButton_Error();
		appstorepage.ErrorMessageToFillDetails();
		Reporter.log("PASS>> Verification of Upload ipa of App Store with Category and Description fields is empty",true);
	}
	
	@Test(priority='5',description="5.To Verify Upload ipa of App Store with Category field is empty")
	public void AppStore_iOS_TC5() throws Throwable{
		Reporter.log("\n5.To Verify Upload ipa of App Store with with Category field is empty",true);
		
		appstorepage.EnterCategoryiOS();
		appstorepage.ClickOnUploadipaPopupAddButton_Error();
		appstorepage.ErrorMessageToFillDetails();
		appstorepage.ClickOnCloseOfAppDetailsiOS();		
		Reporter.log("PASS>> Verification of Upload ipa of App Store with Category field is empty",true);
	}
	
	@Test(priority='6',description="6.To Verify Upload ipa of App Store when clicked on BackButton of Upload ipa ")
	public void AppStore_iOS_TC6() throws Throwable{
		Reporter.log("\n6.To Verify Upload ipa of App Store when clicked on BackButton of Upload ipa ",true);
		

		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnUploadIPAiOS();
		appstorepage.ClickOnBrowseFileiOS();
		appstorepage.VerifyOfUploadiPAiOS();
		appstorepage.EnterCategoryiOS();
		appstorepage.EnterDescriptioniOS();
		appstorepage.ClickOnUploadipaPopupBackButton();
		appstorepage.ClickOnUploadIPAcloseButton();	
		Reporter.log("PASS>> Verification of Upload ipa of App Store when clicked on BackButton of Upload ipa",true);
	}

	@Test(priority='7',description="7.To Verify Upload ipa of App Store when clicked on Add Button of Upload ipa ")
	public void AppStore_iOS_TC7() throws Throwable{
		Reporter.log("\n7.To Verify Upload ipa of App Store when clicked on BackButton of Upload ipa ",true);
	
	   
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnUploadIPAiOS();
		appstorepage.ClickOnBrowseFileiOS();
		appstorepage.VerifyOfUploadiPAiOS();
		appstorepage.EnterCategoryiOS();
		appstorepage.EnterDescriptioniOS();
		appstorepage.ClickOnUploadipaPopupAddButton();
		appstorepage.VerifyOfApplicationiOS_True();
		Reporter.log("PASS>> Verification of Upload ipa of App Store when clicked on BackButton of Upload ipa",true);
	}
	
	@Test(priority='8',description="8.To Verify App Details Popup of the Application of App Store when application is added through Upload ipa")
	public void AppStore_iOS_TC8() throws Throwable{
		Reporter.log("\n8.To Verify App Details Popup of the Application of App Store when application is added through Upload ipa",true);
		appstorepage.SearchAppIniOSAppStore();
		appstorepage.ClickOnAppDetailsiOS();
		appstorepage.VerifyOfAppDetailsHeader();
		appstorepage.VerifyOfAppInAppDetailsiOS();
		appstorepage.CloseAppDetailsiOSWindow();
		Reporter.log("PASS>> Verification of App Details Popup of the Application of App Store when application is added through Upload ipa",true);
	}
	
	
	@Test(priority='9',description="9.To Verify Edit Button of App Details Page when application is added through Upload ipa")
	public void AppStore_iOS_9() throws Throwable{
		Reporter.log("\n9.To Verify Edit Button of App Details Page when application is added through Upload ipa",true);
		
		appstorepage.ClickOnAppDetailsiOS();
		appstorepage.ClickOnEditInAppDetails();
		appstorepage.VerifyEditAppDetailsHeader();
		appstorepage.VerifyOfAppInEditAppDetailsiOS();
		appstorepage.ClickOnCloseOfEditAppDetailsiOS();
		Reporter.log("PASS>> Verification of Edit Button of App Details Page when application is added through Upload ipa",true);
	}
	
	@Test(priority='A',description="10.To Verify Edit App Details Page of Application when application is added through Upload ipa")
	public void AppStore_iOS_TCA() throws Throwable{
		Reporter.log("\n10.To Verify Edit App Details Page of Application when application is added through Upload ipa",true);
		appstorepage.ClickOnMoreSymboliOS();
		appstorepage.ClickOnEditSymboliOS();
		appstorepage.VerifyEditAppDetailsHeader();
		appstorepage.VerifyOfAppInEditAppDetailsiOS();
		appstorepage.ClickOnCloseOfEditAppDetailsiOS();
		Reporter.log("PASS>> Verification of Edit App Details Page of Application when application is added through Upload ipa",true);
	}
	
	@Test(priority='B',description="11.To Verify Cancel Button of Edit App Details when application is added through Upload ipa")
	public void AppStore_iOS_TCB() throws Throwable{
		Reporter.log("\n11.To Verify Cancel Button of Edit App Details when application is added through Upload ipa",true);
		appstorepage.ClickOnMoreSymboliOS();
		appstorepage.ClickOnEditSymboliOS();
		appstorepage.VerifyOfAppInEditAppDetailsiOS();
		appstorepage.ClearEditAppTitleiOS();
		appstorepage.EnterEditAppTitleiOS();
		appstorepage.ClearEditAppCategoryiOS();
		appstorepage.EnterEditAppCategoryiOS();
		appstorepage.ClearEditAppDescriptioniOS();
		appstorepage.EnterEditAppDescriptioniOS();
		appstorepage.ClickOnCancelButtonOfEditAppDetailsiOS();
		appstorepage.VerifyOfEditApplicationiOS_False();
		appstorepage.ClickOnMoreSymboliOS();
		appstorepage.ClickOnEditSymboliOS();
		appstorepage.VerifyOfEditedAppInEditAppDetailsiOS_Cancel();
		appstorepage.ClickOnCloseOfEditAppDetailsiOS();
		Reporter.log("PASS>> Verification of Cancel Button of Edit App Details when application is added through Upload ipa",true);
	}
	
	@Test(priority='C',description="12.To Verify Save Button of Edit App Details when application is added through Upload ipa")
	public void AppStore_iOS_TCD() throws Throwable{
		Reporter.log("\n12.To Verify Save Button of Edit App Details when application is added through Upload ipa",true);
		appstorepage.ClickOnMoreSymboliOS();
		appstorepage.ClickOnEditSymboliOS();
		appstorepage.VerifyOfAppInEditAppDetailsiOS();
		appstorepage.ClearEditAppTitleiOS();
		appstorepage.EnterEditAppTitleiOS();
		appstorepage.ClearEditAppCategoryiOS();
		appstorepage.EnterEditAppCategoryiOS();
		appstorepage.ClearEditAppDescriptioniOS();
		appstorepage.EnterEditAppDescriptioniOS();
		appstorepage.ClickOnSaveButtonOfEditAppDetailsiOS();
		appstorepage.VerifyOfEditApplicationiOS_True();
		appstorepage.ClickOnEditedAppMoreSymboliOS();
		appstorepage.ClickOnEditedAppEditSymboliOS();
		appstorepage.VerifyOfEditedAppInEditAppDetailsiOS_Save();
		appstorepage.ClickOnCloseOfEditAppDetailsiOS();
		Reporter.log("PASS>> Verification of Save Button of Edit App Details when application is added through Upload ipa",true);
	}
	
	@Test(priority='D',description="13.To Verify Edit of Apk File with same Package when application is added through Upload ipa")
	public void AppStore_iOS_TCF() throws Throwable{
		Reporter.log("\n13.To Verify Edit of Apk File with same Package when application is added through Upload ipa",true);
		
		appstorepage.ClickOnEditedAppMoreSymboliOS();
		appstorepage.ClickOnEditedAppEditSymboliOS();
		appstorepage.ClickOnCloseOfEditAppDetailsiOS();
		Reporter.log("PASS>> Verification of Edit of Apk File with same Package when application is added through Upload ipa",true);
	}
	
	@Test(priority='E',description="14.To Verify Manifest Link Popup UI in iOS Platform of App Store")
	public void AppStore_iOS_TCH() throws InterruptedException{
		Reporter.log("\n14.To Verify Manifest Link Popup UI for iOS Platform of App Store",true);
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnManifestLinkiOS();
		appstorepage.VerifyOfManifestLinkPopupiOS();
		appstorepage.ClickOnCloseOfManifestLink();
		Reporter.log("PASS>> Verification of Manifest Link Popup UI for iOS Platform of App Store",true);
	}
	
	@Test(priority='F',description="15.To Verify Manifest Link Popup when Link is not mentioned")
	public void AppStore_iOS_TCI() throws InterruptedException{
		Reporter.log("\n15.To Verify Manifest Link Popup when when Link is not mentioned",true);
		appstorepage.ClickOnAddNewAppiOS();
		appstorepage.ClickOnManifestLinkiOS();
		appstorepage.ClickOnManifestLinkPopupSearchButton_Error();
		appstorepage.ManifestLinkErrorMessage_Empty();
		appstorepage.ClickOnCloseOfManifestLink();
		Reporter.log("PASS>> Verification of Manifest Link Popup when when Link is not mentioned",true);
	}

}
