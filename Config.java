package Library;

public interface Config {

	// MDM
	String url ="https://console-sass-admin-develop.onthedrive.co.uk/"; String userID ="licensedindia@dispostable.com"; String password = "42Gears@123";
	//String url = "https://licensedindia.in.suremdm.io/console/"; String userID ="licensedindia@dispostable.com"; String password = "42Gears@123";
	 
	//live account for things license //trial account
   // String url = "https://worldoftesting.eu.suremdm.io/console/"; String userID ="abdulemithilesh@gmail.com"; String password = "Mithilesh@123";
		
	//String url = "https://showcase.clouderizer.com/auth/login"; String userID ="abdulemithilesh@gmail.com"; String password = "Mithilesh@123";
	
	
	String browser = "chrome";

	//String url = "https://licenseduk.eu.suremdm.io/console/";String userID ="licenseduk@dispostable.com"; String password = "42Gears@123";

	
	 //String url = "https://stagemdm.42gears.com/console/"; String userID ="stage@vomoto.Com"; String password = "42Gears@123";
	 
	// String url = "https://stagemdm.42gears.com/console/"; String userID = "sa";
	// String password = "42Gears@234";
	
	//String url = "https://licensedindia.in.suremdm.io/console/"; String userID ="Mithilesh_Superuser"; String password = "Mithilesh@123";
	

	// DeviceGrid

	//String DeviceName = "AutomationDevice_A51";
	 String DeviceName = "M20_Automation";	
	 // String DeviceName = "KARTHIKBS-239";	  
	String Searchdevice2 = "AutomationDevice_A21";
	String WindowsDeviceName = "MSI";

	String DeviceWithStandAloneSL = "abc";
	String KnoxDevice = "M30";
	String DeviceWithoutIntegratedNix = "A21";

	String MultipleDeviceName = "AutomationDevice_MotoG7,AutomationDevice_A51";
	String MultipleDevices1 = "AutomationDevice_A51";
	String MultipleDevices2 = "Dontouch1";

	String Device1 = "Client0001";
	String Device2 = "Client0002";

	String DeviceNameContains = "Moto";// Pass some char which are there in device name
	String DeviceNameStartsWith = "A";// Pass 1st Latter of devicename
	String DeviceNameEndsWith = "7";// Pass Last Latter of devicename

	String DeviceNameSearchOp1 = "Contains: Device name contains";
	String DeviceNameSearchOp2 = "Starts with: Device name starts with";
	String DeviceNameSearchOp3 = "Ends with: Device name ends with";

	String ContainsFilterDeviceName = "DeviceNameContainsFilter";
	String StartsWithFilterDeviceName = "DeviceNameStartsWithFilter";
	String EndsWithFilterDeviceName = "DeviceNameEndsWithFilter";

	String DeviceNameContainsFilter_Update = "auto";// Pass any one/two char from devicename
	String DeviceNameStartsFilter_Update = "S";
	String DeviceNameendsFilter_Update = "3";

	String OfflineAndroid_Device = "On5";
	String OSBuildNu_CustomReportName = "OS Build Number Device Details";
	String OSBuildNu_CustomReportDescription = "OS Build Number Device Details Description";

	String SureLockXSLT_ConsoleMessageJobName = "https://mars.42gears.com/support/inout/surelock.apk";
	String SerialNumber_Samsung = "N/A";
	String SerialNumber_Honor = "BKFDU17401001226";
	String Surelock_ActivationCode = "909686";
	String SureFox_ActivationCode = "567446";
	String SureVideo_ActivationCode = "235335";
	String SureLockLowerVersion = "14.18";
	String SureLockVersion = "21.01.07";
	String SureVideo_ConsoleMessageJobName = "https://mars.42gears.com/support/inout/surevideo.apk";

	String Offline_Android = "UK435168";
	String Offline_iOS = "iPad";
	String Offline_MacOS = "Ramyashree�s MacBook Air";
	String Offline_Windows = "DESKTOP-7850G8U";
	String MacOS_Multiple1 = "Serial Number";
	String MacOS_IPAddress = "15.1.0.86";
	String MacOS_Connection = "Secured";
	String iOS_Multiple1 = "Vishal�s iPhone";

	String DeviceLocaleColumnValue = "en-IN";

	String OSVersion = "10";
	String SearchiOSdevice = "Vishal�s iPhone";

	String SearchMacdevice = "Ramyashree�s MacBook Air";
	String InstalledAppVersion = "3.3.6.100 (186)";
	String NameOfJob = "JTouchLowVersion";
	String PackageNameInstallJobApp = "com.bs.smarttouch.gp";
	String AppName = "J Touch";
	String LowerVersion = "1.5.5-GP (55)"; // 1.7.0-GP (70)
	String UpgradedVersion = "1.7.0-GP (70)";

	String SearchInstalledAppForCustomColumn = "9Apps";
	String SearchInstalledAppForSorting = "AstroContacts";
	String SurelockPassword = "0000";
	String JobNameForverification = "CallAndSmsLogTracking";
	String SecurityPatchColumnValue = "1 Dec 2020";
	String DeviceGroupPathColumnValue = "Home";
	String GpuUsageColumnValue = "0%";
	String ExpectedNotes = "rocking";

	String Windows_DeviceName1 = "Saketh";
	String DualEnrolledWindows_DeviceName = "42GEARS";
	String EMMEnrolledWindows_DeviceName = "TEST";
	String EADevice = "LenovoTab";
	String Windows_DeviceModel = "20KNS0DD00";
	String Windows_DeviceIP = "182.76.94.114";
	String Windows_NixAgentVersion = "4.05";
	String Windows_OSVersion = "6.2.9200.0";
	String Windows_OSBuildNumber = "1909 (OS Build 18363.1198)";

	String Windows_SerialNumber = "L1HF8A307D7";
	String Windows_DeviceNotes = "TestKarthik7";
	String Windows_DeviceWifiSSID = "42Gears-Gen";
	String windows_DeviceGroupPath = "Home/AATestGroupPath";
	String windows_DeviceOperatingSystem = "Microsoft Windows 10 Pro [Windows]";
	String windows_DeviceMACAddress = "E8:6A:64:33:73:60";
	String NixDownloadLink = "https://suremdm.42gears.com/nix/nixagent.apk";
	// "34:E1:2D:B8:95:61";
	String InboxEntryFenceMessage = "Time fence entry alert for device Client0001";
	String InboxExitFenceMessage = "Time fence exit alert for device Client0001";
	String InboxExitWindwsFenceMessage = "Time fence exit alert for device Windows";
	String InboxEntryWindwsFenceMessage = "Time fence entry alert for device Windows";
	String InboxExitNetworkFence = "Network fence exit alert for device Client0001";
	String InboxentryNetworkFence = "Network fence entry alert for device Client0001";

	String DeviceModel2 = "XT1022";

	String CallLogs_CallerName = "Hello";
	String CallLogs_PhoneNumber = "+919380377695";
	String UDID = "34010b429f0b36f3";

	// SMS Logs
	String SMSLogs_SenderName = "KArthik";
	String SMSLOgs_SenderNumber = "+919380377695";
	String DeviceName1 = "Saketh_MotoG6";

	// DeviceReboot
	String NonKnoxDeviceName = "vinod";
	String KnoxDeviceName = "donotdelete1";

	// MiscellaneousDevice
	String Miscellaneous_DeviceName = "client0408";

	// Customize Toolbar
	String Android_FileTransferJob = "AutommationDontdeleteAndroid";
	String iOSDeviceName = "iPhone"; // iPad

	String iOS_TextMessageJob = "AutommationDontdeleteiOS";
	String Windows_InstallJob = "AutommationDontdeleteWindows";
	String Linux_TextMessageJob = "AutommationDontDeleteLinux";
	String MacOS_RebootJob = "AutommationDontdeletemacOS";
	String LinuxDeviceName = "LinuxDevice";
	String MacOSDeviceName = "";
	String AnyOSTextMessageJob = "AutommationDontdeleteAnyOS";
	String Customize_jobnameAndroid = "42Auto123";
	String Customize_jobname = "42Test";
	String Customize_DisabledPredefinedJobs1 = "Remote";
	String Customize_DisabledPredefinedJobs2 = "Apps";
	String Linux_OS = "Vostro 3446 (version: Not Specified)";
	String Customize_ModifiedJobName = "Auto";
	String Customize_NewModifiedJobName = "42TestAuto";
	String Customize_TemperatureUnitCelsius = "Celsius (�C)";
	String Customize_TemperatureUnitFahrenheit = "Fahrenheit (�F)";
	String Customize_Sub = "Testing";
	String Customize_Message = "Testing Purpose";
	String CustomizeJobName = "42Test";

	// FileUpload Remote Support
	String FileName = "software testing.pdf";

	// Groups
	String GroupName1 = "AutoSubGroup";
	// Android Job
	// public static final int IMP_WAIT = 400;

	String UploadFileSideInstagram = "40.130 MB";
	String initialLicenseCount = "License Used 3 of 5";
	String AfterDeletingDeviceLicenseCount = "License Used 2 of 5";
	String EnterApkUrl = "https://mars.42gears.com/support/inout/astrocontact/astrocontacts.apk";
	String EnterJobWithApkUrl = "jobWithApkUrl";
	String MultipleFileTransferURL = "https://mars.42gears.com/support/inout/surelock.apk,https://mars.42gears.com/support/inout/surevideo.apk";
	String BundleID = "com.gears42.astrocontacts";
	String GPSTestApp_BundleID = "com.chartcross.gpstest";
	String MultipleBundleID = "com.gears42.astrocontacts,com.gears42.surevideo";
	int TimeToDeployInstallJob = 80;
	String EnterJobWNameUseAppsfromAppstore = "EnableUseAppsfromAppstore";
	String EnterJobWNameFileApk = "FileApk";
	String EnterJobNameWithApkUrlCopyClick = "EnableCopyInstall";
	String EnterJobNameWithApkUrlCopyUnchecked = "ApkUrlCopyInstallUnchecked";
	String EnterJobWNameAdvanceSetting = "AdvanceSetting";
	String SearchApkName = "astrocontacts.apk";
	String BundleIDFakeGPS = "com.lexa.fakegps";
	String Andriodversion = "Marshmallow(6.0)";
	String appversion = "14.69";
	String greaterThanEquals = ">=";
	String LessThanEquals = "<=";
	String appName = "SureMDM Nix";
	String EnterMultipleApkUrl = "https://mars.42gears.com/support/inout/surelock.apk,https://mars.42gears.com/support/inout/astrocontact/astrocontacts.apk";
	String EnterTrialInstallJob = "Trials Install Job";
	String EnterTrialFileTransferJob = "Trials File Transfer Job";
	String EnterTrialMobileDataOnlyJob = "Trials Mobile Data Only Install Job";
	String EnterTrialMobileDataFileTransferJob = "Trials Mobile Data File Transfer Job";
	String EnterTrialAnyNetworkJob = "Trials Any Network Install Job";
	String EnterTrialAnyNetworkFileTransferJob = "Trials Any Network File Transfer Job";
	
	
	// SureLockSureFox:
	String FileDownloaded = "C:\\Users\\madhu.b\\Downloads";
	String DeviceImagePath = "/Internal storage/DCIM/Camera/image.jpg";
	String ActivationKey = "7802000034";

	// Device Enrollment

	String AccId = "012100052";
	String QrCodeName = "QR_codeAutomation";
	String ServerPath = "https://stagemdm.42gears.com";
	String Enrolled_DeviceName = "SamsungM30Pie";
	String Enrolled_DeviceIMEI = "SM-M305F";
	String AuthPassword = "00000000";
	String AuthEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
	String TokenEndpoint = "https://www.googleapis.com/oauth2/v4/token";
	String ClientID = "959684086677-57agtbulgu6ecipk41i9mb61k6vphqfg.apps.googleusercontent.com";
	String ClientSecret = "Pbwc_j6jC79LEX5IXK31ZvZp";
	String OAuthGmailID = "42test2018@gmail.com";
	String OAuthGmailPassword = "42Gears@234";
	String OAuthUserName = "42gears test";
	String OAuthEndpoint_Azureportal = "https://login.microsoftonline.com/vishnu42.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1_vishnu42";
	String OAuthTokenEndpoint_Azureportal = "https://login.microsoftonline.com/vishnu42.onmicrosoft.com/oauth2/v2.0/token?p=B2C_1_vishnu42";
	String ClientID_Azureportal = "26338647-0ab5-42da-9c25-f4c7befecc1b";
	String ClientSecret_Azureportal = "r7[GljQ5@v2lwB._ityozImVj2p@2GAC";
	String OAuthEmailID_Azureportal = "vishnuvardhan2637@gmail.com";
	String OAuthEmailPassword_Azureportal = "42Gears@123";
	String OAuthUsername_Azureportal = "vishnuvardhan reddy";
	String NativeAuthEndPoint_ADADisabled = "https://accounts.google.com/o/oauth2/v2/auth";
	String NativeTokenEndpoint_ADADisabled = "https://www.googleapis.com/oauth2/v4/token";
	String NativeClientID__ADADisabled = "513228862518-7dgtd9rhm09gr4m22ln4uj89c3cudu3n.apps.googleusercontent.com";
	String OAuthEndpoint_Azureportal_ADADisabled = "https://login.microsoftonline.com/42gearsaad.onmicrosoft.com/oauth2/v2.0/authorize?p=b2c_1_signupin";
	String OAuthTokenEndpoint_Azureportal_ADADisabled = "https://login.microsoftonline.com/42gearsaad.onmicrosoft.com/oauth2/v2.0/token?p=b2c_1_signupin";
	String ClientID_Azureportal_ADADisabled = "de309969-64da-4d01-857c-9d9525daf641";

	// QuickActionToolBar

	String TextMessageJobname = "0TextMessage";// "\"0TextMessage\""
	String FileTransferJobName = "0FileTransfer";
	String InstallJObName = "0InstallApplication";
	String DeleteDeviceName = "DoNotUse";
	String JobFolderName = "##Autotest";
	String MultinstallJobName = "0MultiInstall";
	String MultipleFileTransferJobName = "0MultiFileTransfer";
	String MultipleFileTransferFile1 = "dataMining.pdf";
	String MultipleFileTransferFile2 = "test.txt";
	String MultipleFileTransferFile3 = "test2.txt";
	String MultipleFileTransferFile4 = "test3.pdf";
	String CompositeJobName = "AutomationDonotDeleteCompositeJob";
	String CompositeJobNameEdited = "0DonotDeleteCompositeJobEdited";
	String CompositeJobWithErrorStatus = "CompositeJobWithErrorStatus";
	String LoctionTrackingAddress = "Aralihalli";
	String PinCode = "577233";
	String GroupName = "##Automation";
	String SubGroupName = "#AutoTestJobQueue_SubGroup";
	String GroupName_Reports = "dontouch";
	String JobName1 = "DontDeleteCompositeJob";
	int TotalTimeForJobName1ToGetDeployedOnDevice = 300;
	String NotificationJobName = "AnyNotificationJob";

	// Compliance Reports
	String Compliance_Policy = "OS Version";
	String Compliance_Status = "Non-Compliant";

	// Pending Delete
	String PendingDeleteDeviceName = "SamsungM30";
	String DeactivationJob1 = "Deactivate of SL";
	String DeactivationJob2 = "Deactivate of SF";
	String DeactivationJob3 = "Deactivate of SV";
	String PendingDeleteDeviceName1 = "SamsungOn8-7.0";
	String PendingDeleteOfflineDeviceName = "No Name";

	// Data Analytics

	String Feb8AccSureLockSecretKey = "65a78001";
	String EnableSureLockRunScript = "Run-script to enable/disable analytics of SureLock/SureVideo/SureFox";
	String RunScript = "am broadcast -a com.gears42.surelock.COMMUNICATOR -e password 0000  -e command export_analytics com.gears42.surelock";
	String AnalyticsReportName = "SureLock Analytics Report Automation";
	String AnalyticsReportDescription = "SureLock Analytics Report Description Automation";
	String RunscriptToExportSFAnalytics = "am broadcast -a com.gears42.surefox.COMMUNICATOR  -e password 0000 -e command export_analytics  -n com.gears42.surefox/com.gears42.surefox.service.SurefoxCommunicator";
	String SFAnalyticsReportName = "SureFox Analytics Report Automation";
	String SFAnalyticsReportDescription = "SureFox Analytics Report Description Automation";
	String SFAnalyticsDisableRunscript = "am broadcast -a com.gears42.surefox.COMMUNICATOR -e password 0000 -e command disable_analytics -n com.gears42.surefox/com.gears42.surefox.service.SurefoxCommunicator";
	String AnalyticsName = "Content Blocking Analytics";
	String AnalyticsTagName = "Keyword";
	String ContentBlockingExportRunscript = "am broadcast -a com.gears42.surefox.COMMUNICATOR  -e password 0000 -e command export_content_blocking_analytics  -n com.gears42.surefox/com.gears42.surefox.service.SurefoxCommunicator";
	String ContentBlockingReportName = "Content Blocking Report Automation";
	String ContentBlockingReportDescription = "Content Blocking Report Description Automation";
	String ContentBlockingAnalyticsDisableRunscript = "am broadcast -a com.gears42.surefox.COMMUNICATOR  -e password 0000 -e command disable_content_blocking_analytics -n com.gears42.surefox/com.gears42.surefox.service.SurefoxCommunicator";
	String BatteryAnalyticsName = "Battery&Charging Analytics";
	String AnalyticsNameAutomation = "Automation Analytics";
	String TagNameTest = "Testing42Gears";
	

	// ComplianceJobs
	String CompliancePackageName = "com.nix";
	String NotifiedEmail_Misc = "ComplianceJobmdm@mailinator.com";
	String NotifiedUrl_Misc = "https://www.mailinator.com";
	String OutOFComplianceJobName = "OutOfCompliance_InstallJob";
	String OutOfCompliancePhoneNumber = "+916364909265";
	String CompliancepackageName_Uninstall = "com.bhanu.torch";

	// Custom Column

	String CustomColumnName = "CustomColumn_Automation1";
	String CustomColumnName1 = "CustomColumn_Automation2";
	String CustomColumnName2 = "CustomColumn_Automation3";
	String CustomColumnModified = "CustomColumn_Modified_Automation";
	String CustomColumn_Search = "CustomColumn_Search";
	String CustomColumn_UniqueValue = "CustomColumn_UniqueValue";
	String CustomColumn_UniqueValue1 = "CustomColumn_UniqueValue1";
	String UniqueValue1 = "M30";
	String UniqueValue2 = "Hon8";
	String SameValue = "Common";
	String ThingsDeviceName = "zebra Zebra SmartBattery";
	
	// Advance Search

	String AdvanceSearch_CustomColumn = "AdvanceSearch CustomColumn";
	String AdvanceSearch_CustomColumnValue = "AdvanceSearch";

	// Nix Permission CheckList

	String NixPermission_RunScript = "!#suremdm\ngetNixPermissionStatus";
	String AccountId = "051800617";

	// OnDemand Report

	// AppVersion Report
	String AppName1 = "ES File Explorer";
	String AppVersion1 = "4.2.1.9 (10058)";
	String AppName2 = "Chrome";
	String AppVersion2 = "80.0.3987.99 (398709933)";

	// Device Health Report
	String DeviceNameColumn = "NULL";
	String BatteryPercentageColumnValue1 = "NULL";
	String BatteryPercentageColumnValue2 = "0%";
	String AvailableStorageColumnValue1 = "NULL";
	String AvailableStorageColumnValue2 = "0 MB";
	String AvailablePhyscialMemoryColumnValue1 = "NULL";
	String AvailablePhyscialMemoryColumnValue2 = "0 MB";
	String GPSTestVersion = "1.2.9 (30)";
	int TotalTimeForGPSInstallJobDeployment = 300;

	// ActivityLog
	String Job1 = "Text1";
	String Job2 = "Text2";
	String AcccountUser = "stage";
	String DonwloadPath = "C:\\Users\\kavya.ba\\Downloads";

	// Asset Tracking Report
	String SIMSerialNumber = "89910342110360838948";
	String DeviceModelVersion = "0";
	String OSType1 = "PocketPC";
	String OSType2 = "Android";
	String OSType3 = "Windows";
	String OSType4 = "macOS";
	String OSType5 = "Linux";
	String OSType6 = "iOS";
	String OSType7 = "Things";
	String OSType8 = "AndroidVR";
	String OSType9 = "AndroidWear";
	String OnlineStatus1 = "Offline";
	String OnlineStatus2 = "Online";
	String RootStatus1 = "No";
	String RootStatus2 = "Signed";
	String RootStatus3 = "Granted";
	String KnoxStatus1 = "Yes";
	String KnoxStatus2 = "N/A";
	String SureLockVersion1 = "13.26";
	String SureLockVersion2 = "13.27";
	String SureLockVersion3 = "12.98";
	String SureLockVersion4 = "7.97";
	String SureLockVersion5 = "12.99";
	String SureLockVersion6 = "14.04";
	String SureLockVersion7 = "N/A";

	String ScheduledJobsCOunt = "0";
	String DefaultLauncherApplication = "com.gears42.surelock";

	// System Log Report
	String TextMessageJobname1 = "\"0TextMessage\"";
	String DeviceName2 = "\"AutomationDevice_A21\"";
	String UserID2 = "\"stage@vomoto.com\"";
	// The job named "0TextMessage" was successfully deployed on the device named
	// "AutomationDevice_M
	String SystemLogReport_LogMessage = "The job named "+TextMessageJobname1+" was successfully deployed on the device named "+DeviceName2+"";
	// String SystemLogReport_SystemLogMessage="The job named
	// "+TextMessageJobname1+" has been applied to the device known as
	// "+DeviceName2+"";
	String SystemLogReport_SystemLogMessage = "The user "+UserID2+" has applied a job named "+TextMessageJobname1+" to the device known as "+DeviceName2+"";
	String Device_LogMessage = "Job(AutomationTextMessage) successfully deployed on donotdelete.";
	String SystemLogJob = "AutomationTextMessage";
	int JobDeploymentTime = 20;

	// Device Activity
	String DeviceActivitytext = "Device Activity";
	String DeviceActivitytext1 = "Device Info Update and Online Offline Report";
	String DeviceActivityReportcolunm1 = "Device Name";
	String DeviceActivityReportcolunm2 = "Time";
	String DeviceActivityReportcolunm3 = "Activity";
	String DeviceActivityReportdata1 = "Test2";
	String ActivityInfo1 = "Device Came Online";
	String ActivityInfo2 = "Device Updated Its Info";

	// Device History

	String BackUpBatteryPercent = "N/A";
	String AvailablePhysicalMemoryPercentColumnValue1 = "NULL";
	int AvailablePhysicalMemoryPercentColumnValue2 = 0;
	String AvailablePhysicalMemoryPercentColumnValue3 = "0 MB";
	String AvailablePhysicalMemoryPercentColumnValue4 = "N/A";
	int AvailablePhysicalMemoryPercentColumnValue5 = 100;

	String AvailableStoragePercentColumnValue1 = "NULL";
	int AvailableStoragePercentColumnValue2 = 0;
	String AvailableStoragePercentColumnValue3 = "0 MB";
	String AvailableStoragePercentColumnValue4 = "N/A";
	int AvailableStoragePercentColumnValue5 = 100;
	String NetworkOperator = "airtel";
	String DeviceRoaming = "75";
	String CPUUsage = "0";
	String GPUUsage = "-1";
	String LastSystemScan = "N/A";
	String threatsCount = "0";

	// DataUsage
	String WifiDataUsageColumnNegativeValue = "N/A";

	// DataUsageLegacy
	String WifiDataUsageLegacyColumnNegativeValue = "Not Available";

	// Device Connected
	String DeviceStatusColumnValue = "Approved";
	String DeviceStatusColumnValue1 = "Unapproved";

	// installed job report
	String JobName = "AutomationGPSInstall";
	String ApplicationName = "GPS Test";
	String UserID = "sa";
	String JobStatus = "Deployed";
	String JobStatus1 = "SCHEDULED";
	String JobStatus2 = "Error";
	String JobStatus3 = "INPROGRESS";
	String JobStatus4 = "Pending";
	String SureMDMNix = "26.02007 (3207)";
	// Filter

	String OnlineStatus = "Online Devices";
	String OfflineStatus = "Offline Devices";
	String customColumn = "CustomColumnFilterWithN/A";
	String CombinationOfCol = "CombinationOFColFilterWithN/A";
	String multipleCustCol = "MultipleCustColFilterWithN/A";
	
	// Appstore

	String Profile_Name1 = "Filestoreprofile Image";
	String Profile_Name2 = "Filestoreprofile video";
	String Profile_Name3 = "Filestoreprofile Audio";
	String Profile_Name4 = "Filestoreprofile Doc";
	String Profile_Name5 = "Filestoreprofile Folder";
	String Profile_Name6 = "Filestoreprofile APK";
	String Profile_Name7 = "Filestoreprofile MultipleFile";

	// Certificate Management
	String CAServerAddress = "https://bg42.eastus.cloudapp.azure.com/certsrv/mscep/mscep.dll";
	String CertificateTemplate = "scepTemplate1";
	String CertificateRenewalPeriodNumber = "2";
	String CertificateRenewalPeriodperiodDay = "Days";
	String CertificateRenewalPeriodperiodweek = "Weeks";
	String CNWildcard = "IMEI";
	String SANWildcard = "Constant Text";
	String SANWildcardEmail = "bala@test.42gears.com";
	String ScepUserNAme = "bkg122";
	String ScepPassword = "Dexter@12";
	String CertificateName1 = "AndroidCerti1234";
	String CertificateName2 = "AndroidCerti2132";
	String ExtractCertificatePassword = "Sc#p@uth_321";
	String CertificateProfileName1 = "AutomationCertificateProfileAuto1";
	String CertificateProfileName2 = "AutomationCertificateProfileAuto2";

	// user management
	String Queue_DeviceName = "597";
	String ImportExportUserPermission = "C:\\Users\\admin\\Documents\\UplaodFiles\\User Management\\File1.exe";
	String Uninstall_DownloadedApps = "SureVideo";
	String Uninstall_SystemApps = "Android System";
	String Uninstall_AllApps_Download = "OlaCabs";
	String Uninstall_AllApps_System = "Contacts";
	String ClearData_DownloadedApps = "File Manager";
	String ClearData_SystemApps = "Android System";
	String ClearData_AllApps_Download = "Uber";
	String ClearData_AllApps_System = "Contacts";
	String Lock_DownloadedApps = "Snapchat"; // "ES File Explorer";
	String Lock_SystemApps = "Android System";
	String Lock_SystemApp2 = "Chrome";
	String Lock_AllApps_Download = "Uber";
	String Lock_AllApps_System = "Contacts";
	String Email = "cariqa@zetmail.com";
	String MailTo = "sandhya@42gears.com,aishwarya.venkataraman@42gears.com,sameer.bokade@42gears.com,abhilash.d@42gears.com,prachujya.saikia@42gears.com";
	String PhoneNumber = "+916366231496";
	String MiniumPasswordLength = "3";
	String ReEnterMiniumPasswordLength = "4";
	String MaximumFailedAttempts = "7";
	String MaximumPasswordAge = "2";
	String PasswordHistoryLength = "11";
	String maxiumumTimeToLock = "5";
	String ReEnterMaxiumumTimeToLock = "10";
	String ReEnterPasswordHistoryLength = "8";
	String ReEnterMaximumFailedAttempts = "5";
	String ApplicationSettingsProfileName = "Automation AppSettingsProfile";
	String EnterWebAppTitle = "ESFileExplorer";
	String EnterWebLink = "https://es-file-explorer.en.uptodown.com/android/download";
	String EnterWebAppCatergory = "Application";
	String EnterWebAppDesc = "Sample app";
	String NetworkSettingsProfileName = "Automation NetworkSettingsProfile";
	String EnterproxyServer = "http://00.00.00.00";
	String EnterProxyPort = "8080";
	String EnterProxyPACURL = "http://sample.com";
	String EnterVPNPackageName = "packageNameofPulseSecure";
	String EnterMailConfigurationProfileName = "Automation MailConfigurationProfile";

	// Remote Support
	String RemoteSupportUploadFile1 = "D:\\UplaodFiles\\App Store-Android\\Remote_test.exe";
	String RemoteSupportUploadFile1Name = "test.txt";
	String RemoteSupportUploadFile2 = "D:\\UplaodFiles\\App Store-Android\\File4.exe";
	String RemoteSupportUploadFile2Name = "GPS Test.apk";
	String DownloadFile1Name = "test.txt";
	String DownloadFile2Name = "b.jpg.png";
	String DownloadFile1Name_Windows = "Nix_ServiceLog.txt";
	String DownloadFile1Name_Windows_Underscore = "user.xlsx";
	String DownloadFile2Name_Windows = "script.txt";
	String TaskManagerKillProcessAppName = "ES File Explorer";
	String TaskManagerKillProcessAppName_Windows = "notepad++.exe";
	String SendtoDeviceClipBoardText = "Send Text To Device";
	String DeviceText = "Text From Device";

	// mail configuration
	String EnterEmailAddress = "helloprachu12@gmail.com";
	String EnterHostname = "sampleHost";
	String EnterUserName = "SampeUserName";
	String EnterDeviceIdentifier = "Sample";
	String EnterLoginCertificateAlias = "ps";
	String EnterDefaultEmailSignature = "prc";
	String EnterDefaultSyncWindow = "10";

	// Android Job
	public static final int IMP_WAIT = 400;
	String UplodedFileSizeFileTransferJob = "2.611 MB";
	String UploadFileSizeInstallApplication = "18.279 MB";

	// Runscript

	String PackageName = "com.ubercab/com.ubercab.presidio.app.core.root.RootActivity";
	String ImageForLockScreen = "/storage/emulated/0/Download/crop.jpeg";
	String ClearData_PackageName = "com.alc.filemanager";

	// DynamicJobs
	String TextMsg = "\"TEXT_MESSAGE\"";
	String Pwd = "000000";
	String DownloadedApp = "Snapchat";
	String RunAtStartup_DownloadedApps = "ES File Explorer"; // "ES File Explorer";
	String RunAtStartup_DownloadedAppsPackage = "com.nix";
	String RunAtStartup_SystemApps = "Maps";
	String RunAtStartup_SystemAppsPackage = "android";
	String RunAtStartup_AllApps_Download = "ANT Radio Service";
	String RunAtStartup_AllApps_DownloadPackage = "com.dsi.ant.service.socket";
	String RunAtStartup_AllApps_System = "Contacts";
	String AnalyticsJobName = "Enable Alert Message Analytics";

	String StartDate_Monthly = "15/02/2018";
	String EndDate_Monthly = "14/03/2018";

	String StartDate_Weekly = "25/02/2018";
	String EndDate_Weekly = "03/03/2018";

	String StartDate_Custom = "27/02/2018";
	String EndDate_Custom = "28/02/2018";

	String CustomDate = "15";
	String StartDate_CustomBillingCycle = "15/11/2017";

	String SureLockSettingsXMLFile_Error = "C:\\Users\\admin\\Documents\\UplaodFiles\\SureLock Settings\\SureLock_Error.settings";
	String SureLockSettingsXMLFile = "C:\\Users\\admin\\Documents\\UplaodFiles\\SureLock Settings\\SureLock Settings.settings";
	String SLSaveAsJob = "SureLock Settings Save As Job";

	String SureFoxSettingsXMLFile_Error = "C:\\Users\\admin\\Documents\\UplaodFiles\\SureFox Settings\\SureFox_Error.settings";
	String SureFoxSettingsXMLFile = "C:\\Users\\admin\\Documents\\UplaodFiles\\SureFox Settings\\SureFox Settings.settings";
	String SFSaveAsJob = "SureFox Settings Save As Job";

	String SureVideoSettingsXMLFile = "C:\\Users\\admin\\Documents\\UplaodFiles\\SureVideo Settings\\surevideo.settings";
	String SVSaveAsJob = "SureVideo Settings Save As Job";

	// Unapproved
	String Unapprove_ApproveDeviceName = "KARTHIK-239";
	String Unapprove_DeleteDeviceName = "715";
	String Unapproved_SearchDeviceName = "KARTHIK-239";
	String Unapproved_SearchModel = "20KNS0DD00";
	String Unapproved_SearchPlatform = "Windows";
	String Unapproved_DeleteDevice = "Client0286";

	// FileStore

	String FileStore_foldername1 = "TestFolder";
	String FileStore_foldername2 = "NestedFolder";
	String FileStore_foldername = "ReTestFolder";

	String FileStore_imagefile = "./Uploads/UplaodFiles/FileStore/\\Image.exe";
	String FileStore_ImageName = "Image.jpg";
	String FileStore_ImageSize = "13 KB";
	String FileStore_imagefile1 = "./Uploads/UplaodFiles/FileStore/\\Image3.exe";
	String FileStore_ImageName1 = "Image3.jpg";
	String FileStore_audiofile = "./Uploads\\UplaodFiles\\FileStore/\\Audio.exe";
	String FileStore_AudioName = "Audio.mp3";
	String FileStore_AudioSize = "4.43 MB";
	String FileStore_videofile = "./Uploads/UplaodFiles/FileStore/\\Video.exe";
	String FileStore_VideoName = "Video.mp4";
	String FileStore_VideoSize = "2.01 MB";
	String FileStore_nestedFile = "./Uploads/UplaodFiles/FileStore/\\Image3.exe";
	String FileStore_nestedFileName = "Image3.jpg";
	String FileStore_VideoSize1 = "1 MB";
	String FileStore_docfile = "./Uploads/UplaodFiles/FileStore/\\File.exe";
	String FileStore_DocName = "File.xlsx";
	String FileStore_DocSize = "12 KB";
	String FileStore_APKfile = "./Uploads/UplaodFiles/FileStore/\\Game.exe";
	String FileStore_APKfileName = "Game.apk";
	String FileStore_ApkSize = "40.24 MB";
	String FilestoreFile = "./Uploads/UplaodFiles/FileStore/\\ATfile.exe";
	String FilestoreFilesize = "113 KB";
	String FilestoreFilename = "ATfile";

	String Filstorefile2 = "./Uploads/UplaodFiles/FileStore/\\Afile.exe";

	String SureFoxSaveasjob = "SureFox Settings Save As Job";

	// AppStore
	String EnterWifiConfigurationProfileNameHotSpot = "AutomationWifiConfigurationProfileHotSpot";
	String Appstore_Profile1 = "Appstore Profile AstroMail";
	String Appstore_Profile2 = "Appstore Profile FacebookLite";
	String Appstore_Profile3 = "Appstore Profile SIMNC";
	String Appstore_Profile4 = "Appstore Profile LiteNew";
	String Appstore_Profile5 = "Appstore Profile MultipleWebApp";
	String Appstore_Profile6 = "Appstore Profile MultipleAPK";
	String Appstore_Profile7 = "Profile1";
	String Appstore_Profile8 = "Profile2";
	String Appstore_Profile9 = "Profile3";
	String FirstProfileName = "PasswordPolicyprofile";
	String SecondProfileName = "SamplePasswordPolicyDevice";
	String SystemSettingsProfileName = "Automation SystemSettingsProfileLenovo";
	String SystemSettingsProfileName1 = "Automation SystemSettingsProfile Samsung";
	String SystemSettingsProfileName3 = "Automation SystemSettingsProfileKioskMode";
	String EnterWifiConfigurationProfileName = "Automation WifiConfigurationProfile";
	String EnterWifiConfigurationSSIDOpen = "vinod";
	String EnterWifiConfigurationPasswordOpen = "dont know";
	String EnterWifiConfigurationSSIDWAP2 = "42GearsUniFi";
	String EnterWifiConfigurationPasswordWAP2 = "phobos42gears";
	String EnterWifiConfigurationProfileName1 = "Automation WifiConfigurationProfile1";
	String EnterMailConfigurationProfileName1 = "Automation MailConfigurationProfile1";
	String EnterEmailAddress1 = "vinodpatilr777@gmail.com";
	String EnterHostname1 = "m.Google.com";
	String EnterUserName1 = "vinodpatilr777@gmail.com";
	String AstroMailEmail3 = "uma.anand@42gearstest.onmicrosoft.com";
	String PasswordAstro3 = "42Gears@123";
	String ExchangeServer3 = "outlook.office365.com";
	String Port3 = "993"; // or 587
	String UserNmaeAstro3 = "uma.anand@42gearstest.onmicrosoft.com";
	String ProfileActive3 = "AstroprofileActiveSync";
	String ProfileIMAP = "AstroprofileIMAP";
	String ProfilePOP = "AstroprofilePOP3";
	String ProfileActive = "AstroprofileActiveSync";
	String ExchangeServer1 = "map.gmail.com";
	String Port1 = "995";
	String UserNmaeAstro1 = "vinodpatilr777@gmail.com";
	String AstroMailEmail1 = "vinodpatilr777@gmail.com";
	String PasswordAstro1 = "vinodpatil123";
	String ExchangeServer = "pop.gmail.com";
	String Port = "995";
	String UserNmaeAstro = "42gearsts@gmail.com";
	String AstroMailEmail = "42gearsts@gmail.com";
	String PasswordAstro = "42gears@123";
	String AstroMailEmail2 = "vinod.patil@42gears.com";

	// Refreshing Deivce_Quick Action
	String Refrehing_SearchDeviceName = "KArthik_Test";

	// Certificate
	String EnterCertificateProfileName = "Automation CertificateProfile";

	// WiFi configuration
	String EnterWifiConfigurationSSID = "42gearsUniFi";
	String EnterWifiConfigurationPassword = "dont know";
	String EnterSSID = "42GearsUniFi";
	String EnterServer = "http://NoIdea";
	String EnterPort = "8080";

	// ########################### USERMANAGEMENT ROLES
	// ##############################

//	//  group permissions
//
//	String UserNameGroupPermission="dfg3";
//	//  Job Permissions
//	String UserNameJobPermission="pknbyuj3456";
//	//     Device action permission
//	String UserNameDeviceActionpermission="jnbghn67";
//	//   Device ManagementUser1NameJobProfileFolderSet
//	String UserNameDeviceManagement="xa34fgg";
//	//     File Store permisssion 
//	String UserNameFileStorePermission="sdf12f";
//	//     profile Permission
//	String UserNameProfilePermission="oiuhk34";
//	//     Other permisssions
//	String UserNameOtherPermission="wert4567";
//	//     Report Permission
//	String UserNameReportPermission="poijh876";
//	//     Applicataion permission
//	String UserNameApplicationPermission="sdfer345";
//	//     App Store Permission
//	String UserNameAppStorePermission="dfty345yui";
//	//     Dashboard Permission
//	String UserNameDashBoardPermission="rty456gh";
//	//     Location Permission
//	String UserNameLocationPermission="yujekr78hjm";
//	//     USerManagement Permissions
//	String UserNameUsermanagementPermission="ehuir5678ghgj";
	// ######################################## DEVICE GROUP SET
	// ##############################3

	String User1NameDeviceGroupSet = "pkjgqw";
	String User2NameDeviceGroupSet = "pirgbbn";

	String User1NameJobProfileFolderSet = "rgbniuhv";
	String User2NameJobProfileFolderSet = "rdxqsc";

	String JobFolderNameEnableAutomatic = "00EnableJob"; // start with zero
	String ProfileFolderNameEnableAutomatic = "00EnableProfile";

	String JobFolderNameDisabledAutomatic = "00DisabledJob";
	String ProfileFolderNameDisabledAutomatic = "00DisabledProfile";

	// ################################# device column set
	// ####################################
//	String User1NameDeviceGridColumnSet="tyuvb";

	// String User2NameDeviceGridColumnSet="ijhbtyui";

	// Mail Configuration iOS
	String EnterAccountDescription = "This is mail configuration settings";
	String EnterMailServerURL = "http://www.server.com";
	String EnterUsername = "prachujya";

	// Global HTTP Proxy iOS
	// WiFi configuration
	String EnterGlobalHTTPProxyProfileName = "Automation GlobalHTTPProxyProfile";
	String EnterProxyServerURL = "http://proxy.com";

	// VPN iOS
	String EnterVPNProfileName = "Automation VPNProfile";
	String EnterConnectionName = "Private";
	String EnterServerVPN = "vpn.42gears.com";
	String EnterPasswordVPN = "42gears";

	// ExchangeActiveSync iOS
	String EnterExchangeActiveSyncProfileName = "ExchangeActiveSyncProfile";
	String EnterCertificatePassword = "password";
	String EnterAccountName = "MailAccount";
	String EnterExchangeActiveSyncHost = "host";
	String EnterUser = "User";
	String EnterEmailAddressExchangeActiveSync = "helloprachu12@gmail.com";
	String EnterPasswordExchangeActiveSync = "password";
	String EnteriOSCertificateProfileName = "Automation Certificate";

	// Device Info

	String EnterNotes = "rocking";
	String EnterNotes1= "automation";
	String NotesExceeds256Characters = "Software testing is an investigation conducted to provide stakeholders with information about the quality of the software product or service under test Software testing can also provide an objective, independent view of the software to allow the business to appreciate and understand the risks of software";
	String DisablingNotesColumn = "";

	// File Sharing
	String EnterFileSharingProfileName = "FileSharingProfile";

	// Add page
	String EnterPageName = "AnyName";

	// Send Message Window

	// Inbox
	String searchFrom = "donotdelete";
	String searchSubject = "test";
	String messageBodySentFromDeviceSie = "hello how is ur day";
	String WriteMessage = "Hello hi how are doing! I am great,thank you. how are you doing how are you doing";
	int Select50 = 50;
	int Select100 = 100;

	// iOS Profile
	String BlacklistProfileName = "Automation Blacklist";
	String WhitelistProfileName = "Automation Whitelist";
	String WebContentFilterProfileName = "Automation WebContentFilter";
	String BlacklistURL = "http://www.42gears.com";
	String Title_WhitelistBookmark = "test";
	String URL_WhitelistBookmark = "http://www.qspiders.com";
	String BookmarkPath_WhitelistBookmark = "any/any";
	String AutoFilterURL = "http://www.gmail.com";
	String BrandingProfileName = "Automation Branding";
	String PasscodePolicyProfileName = "Automation Passcode Policy";
	String PasscodeAge = "2";
	String PasscodeHistory = "2";
	String SAMProfileName = "Automation SingleApplicationMode Profile";
	String LaunchPeriodTime = "5";
	String RestrictionProfileName = "Automation RestrictionProfile";
	String ApplicationPolicyProfileName = "Automation Application Policy Profile";
	String ConfigurationProfileName = "Automation Configuration Profile";
	String EnterKeyApplicationPolicy = "key";
	String EnterValueApplicationPolicy = "value";

	// Blacklisted
	String SearchDeviceName1 = "test2 Test1";
	String SearchDeviceName2 = "819";

	// SignUp UserSignUpInformation

	String EnterCompanyName = "anytimeeeeeee"; // must change
	String UserName = "TestUser";
	String TelephoneNumber = "1234567890";
	String EnterEmail = "kijemdm1234@gmail.com";// must change
	String EnterPassword = "test@123";
	String EnterDNS = "qapsssaa";
	String EnterHowManyDevices = "100";
	String EnterTheRequirement = "We need this to use in our MDM deployment for 1000 devices, can we use this please contact us at +3989839833";
	String WarningExistingEmailId = "Email Id already exists.";

	// On Demand Reports
	String date = "20/06/2018 - 20/06/2018";
	String groupName = "3D Testing";
	String Device = "karbonn";
	String job = "hillari";
	// Windows Profile

	String EnterPasswordPolicyName = "PasswordPolicy";

	// Windows Mail Configuration Profile
	String EnterMailConfigurationProfile = "Mail Configuration";
	String EnterRestrictionProfile = "Restriction Profile";
	String EnterMailAccountName = "Mail Configuration Account";
	String EnterAppLockerProfileName = "EnterMailConfigurationProfile";
	String EnterYourName = "prachujya";
	String AccoutType = "POP3";
	String Enter_Invalid_Email_Address = "inavlidEmail";
	String Enter_UserName = "ps";
	String Enter_Password = "1234";
	String Enter_MailServer = "no idea";
	String Enter_IncomingPortNumber = "8080";
	String Enter_OutgoingServer = "No idea";
	String Enter_OutgoingPort = "8080";
	String Enter_ValidEmailAddress = "prachujya.saikia@42gears.com";

	// Windows AppLocker Profile

	// Allow Type - App Locker
	String EnterPublisher = "noidea";
	String EnterPackageName = "com.nix";
	String Action = "Allow";

	// Allow Type - App Locker
	String EnterPublisherDeny = "noidea";
	String EnterPackageNameDeny = "com.surelock";
	String ActionDeny = "Deny";

	// Rename publisherName -
	String RenamePublisher = "Rename";

	// Windows WifiConfiguration Profile
	String EnterWifiConfigProfile = "WifiConfigurationProfile";

	// Windows VPNConfiguration Profile
	String EnterVPNConfigurationProfile = "VPNConfigurationProfile";

	// Windows Wifi Configuration
	String EnterSSIDWifiConfig = "42GearsUnifi";
	String EnterPasswordWifiConfig = "phobos42gears";
	String EnterSecurityType = "Open";
	String EnterEncryptionType = "None";

	// Windows Application Policy Profile
	String EnterApplicationPolicyProfile = "ApplicationPolicyProfile";
	String EnterName = "No idea";
	String EnterUrl = "http://noidea";
	String EnterNewName = "rename";

	// Windows VPN Configuration Profile
	String EnterVPNConfigProfile = "VPNConfigurationProfile";

	// windows Periodic App Launch
	String EnterPeriodicAppLaunchProfileName = "Periodic App Launch";
	String EnterPeriodicAppLaunchTimeLessThan1 = "0"; // negative scenario
	String EnterPeriodicAppLaunchTime = "2";

	// windows Exchange ActiveSync
	String EnterExchangeActiveSyncProfileNameWin = "Automation ExchangeActiveSync";
	String EnterAccountName_ExchangeActiveSync = "Test Account";
	String EnterActiveSyncHost = "No idea";
	String EnterUserNameExchangeActiveSync = "no idea";
	String EnterInvalidEmailAddressExchangeActiveSyncWin = "no idea";
	String EnterPasswordExchangeActivceSync = "no idea";
	String EnterValidEmailAddressExchangeActiveSyncWin = "prachujya.saikia@42gears.com";

	// Basic search
	String searchWithDeviceName = "Client0002";
	String searchTwoDevices = "Client0280,Client0281";
	String searchWithModel = "XT1068";
	String searchWithNotes = "hellonotes";
	String searchWithIMEI = "353326069869099";
	String searchWithIPAddress = "192.168.x.x";

	// Advanced Search
	String BasicSearchTextSummary = "Search devices using Device Name, Model, Operator, IMEI, Notes.";
	String AdvancedSearchTextSummary = "Enable per column search filter in device grid.";
	String BatteryPercentage = "<=30";
	String BatteryPercentage1 = "<=45";
	String BatteryPercentage2 = ">20";
	String BatteryPercentageMultipleValues = ">30,<80";
	// Nix Version
	String NixAgentVersion = "11.45";

	// IMEI
	String IMEINumberClient0280 = "359475071226790";

	// KNOX STATUS
	String KnoxStatus = "N/A";

	// NetworkTyoe
	String NetworkType = "Wi-Fi";
	// RootStatus
	String RootStatus = "No";
	// Notes
	String Notes = "hellonotes";
	// Roaming Status
	String RoamingStatus = "No";

	// Advance search using device models across platforms
	String searchAndroidModel = "D2105";
	String searchWindowsModel = "Satellite C50-A"; // TOSHIBA -PC
	String searchiOSModel = "iPhone 6 Plus";
	String searchLinuxModel = "B150M-D3H-CF";

	// OSVersion
	String d2105 = "4.4.2";
	// Network Operator
	String networkOperatorName = "airtel";

	String DeviceID = "49e1fedf-0dbc-4cba-a8bc-0c94a1205f8e";
	String downloadpath = "C:\\Users\\madhu.b\\Downloads";

	String AccountUserName = "8april";
	String ApplyJobName_message = "QueueMessage";
	String SelectDate_ApplyJobMonth = "February";
	String SelectDate_ApplyJobYear = "2018";

	String QueueInstallJobName1 = "QueueInstallJobWiFi";
	String QueueInstallJobSubStatus1 = "FakeGPS.apk";
	String QueueInstallJobName2 = "QueueInstallJobMobile";
	String QueueInstallJobName3 = "QueueInstallJobAny";

	String QueueFileJobName1 = "QueueFileJobWiFi";
	String QueueFileJobSubStatus1 = "a.jpg";
	String QueueFileJobName2 = "QueueFileJobMobile";
	String QueueFileJobName3 = "QueueFileJobAny";

	// NixUpgrade
	String nixLatestAPK = "https://mars.42gears.com/support/inout/nixagent.apk?_ga=2.252380361.172915845.1619409179-353089909.1615949906";

	// Account Registration signUP
	String EnterDNSURL = "https://42gears45.test.suremdm.io/";
	String signUPEnterEmailID = "test2@test.com";
	String signUPEnterPassword = "Test@123";
	String signUPEnterInvalidPassword = "12345";
	String signUPConfirmPassword = "Test@123";
	String invalidSignUPEnterEmailID = "123@.com";
	String companyName = "abcjye";
	String DomainName = "xyz";
	String EnterNamesignUP = "Test123";
	String EnterPhoneNumer = "8134214576";
	String EnterZipcode = "6454ps";
	String EnterSpecialCharZipcode = "6454ps#";

	/**********************************
	 * Device Grid
	 ***********************************/
	// Advanced Search
	String SerialNumber = "R58N65SY11L";// "09c913ab0805";
	String PhoneNumberDeviceGrid = "+916366231496";
	String ModelName = "moto g(7)";// "SM-J710FN";
	String ModelName1="SM-A510F";
	String SimSerialNumber = "89914509009559732477";
	String DeviceUserName = "sarthak";
	String DeviceUserNameDevice = "LAPTOP-DT57BD1T";
	String IMEINumber = "N/A";// "353572082746208";
	String WIFIName = "OnePlus 6T";// Skylinenet-pallavi";
	String BlueToothName = "moto g(7)";// "Galaxy On8";
	String IMEINumber2 = "N/A";
	String BluetoothSSID = "c8:d7:79:c2:4e:b7";// "60:e3:27:38:1e:ff";
	String IPAddressOfEnrolledDevice = "157.45.19.221";// "169.149.229.42";
	String LocalIPAddressOfEnrolledDevice = "100.102.209.25";// "49.207.58.85";
	String EncryptionStatus = "Yes";
	String Polling = "FCM";
	String AndroidEnterpriseGrid = "Not Enrolled";
	String DeviceAndroidID = "73845d740b3ad071";// "ad88a4b3cdacbe0e";

	String Android_OSBuildNumber = "QPUS30.52-16-2-7-7";// "NRD90M.J710FNDDS1BSG1";
	String DeviceModel = "SM-A217F";// "SM-J710FN";

	String Network = "N/A";
	String BlueToothStatus = "On";
	String GPSStatus = "Enabled";// Enabled

	// Firmware Updates
	String license_Efota = "EFOTA1-P7B337-5ZCS2O-36VM";
	String CustomerID_Efota = "BC30A833-97E3-49B9-BDD3-9E77F9007B91";
	String ClientID_Efota = "f702c7b8-38a3-47fb-a844-866a2dad13ad";
	String ClientSecretID_Efota = "31ab4531-b6a2-4796-abd7-403cbc01e3bc";
	String JobnameEfota = "";
	String RunSriptRegister = "";
	String RunSriptDeregister = "";

	// Office 365 settings
	String TenantId = "d17025d4-f081-488d-b8f8-75ac4de0431d";
	String ApplicationId = "2e95aab7-a7d2-414a-8096-2447ac81ad25";
	String ApplicationSecret = "Aw+2N+1joFbXzj@G5D8CJVGJsc=hUh/:";
	String office365username = "kv@gearsms.onmicrosoft.com";
	String office365password = "42Gears@123";
	String InvalidTenantId = "d17025d4-f081-458d-b8f8-75ac4de0431d";

	// Eula
	String EulaDisclaierPolicy_Text = "42 Gears";
	String ModifyEulaDisclaierPolicy_Text = " Hello Team!";
	String ModifiedTextOnAndroid = "42 Gears Hello Team!";

	// Runscript
	String ToDisableUninstallApp = "To disable Uninstall";
	String ToAllowUninstallApp = "To enable Uninstall";
	String ApplicationPackageToDisableEnableUnistall = "package of Ola,package of Tez";
	String RunScriptOffGPSJob = "TurnOFFGps";
	String RunScriptOnGPSJob = "TurnONGps";
	String RunScriptClearDataRS = "TurnONGps";
	String RunScriptNFCDontCare = "NFCDontCare";
	String RunScriptChangeLockWallpaper = "ChangeLockWallpaper";
	String RunScriptResetLockScreenWallPaper = "ResetLockScreenWallPaper";
	String RunScriptDisableLogger = "DisableLogger";
	String RunScriptDisableMultiWindow = "DisableMultiWindow";
	String RunScriptEnableMultiWindow = "EnableMultiWindow";
	String RunScriptDisableUninstall = "DisableUninstall";
	String RunScriptEnableUninstall = "EnableUninstall";
	String RunScriptDeactivateAdmin = "DeactivateAdmin";
	int TimeToDeployRunscriptJob = 120; // seconds
	String Launch_App_PackageNanme = "com.alc.filemanager";
	String Launch_App_AppName = "File Manager";

	// Enterprise Data Protection
	String Enter_ProfileNameForUWPApp = "EDP Profile for Skype App";
	String Enter_ProfileNameForEXEandWin32App = "EDP Profile for NotePad++ App";
	String Enter_ProfileNameForEXE = "EDP Profile for Chrome App";
	String Enter_ProfileNameforExtempApp = "EDP Profile for Chrome and NotePad++";
	String SelectStoreApps = "StoreApps";
	String SelectEXE = "EXE";
	String Enter_PublisherNameForSkypeApplication = "CN=Skype Software Sarl,O=Microsoft Corporation,L=Luxembourg,S=uxembourg,C=LU";
	String Enter_PublisherNameForNotePad = "O=NOTEPAD++, L=SAINT CLOUD, S=ILE-DE-FRANCE, C=FR";
	String Enter_PackageNameAsMicrosoftSkypeApp = "Microsoft SkypeApp";
	String Enter_ProductNameAsNotePad = "NotePad++";
	String Enter_BinaryName = "NOTEPAD++.EXE";
	String Enter_PrimaryDomain = "42gears.com";
	String ApplicationlevelForBlock = "3";
	String ApplicationlevelForOverride = "2";
	String EnterChromePath = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe\r\n";

	// GroupAssignment Rules
	String RuleName = "Test";
	String LocalIp1 = "192.168.1.100"; // "192.168.104.90";
	String LocalIp2 = "192.168.1.108";
	String GlobalIp1 = "157.49.189.4"; // "182.76.94.115"; //"182.76.94.113" ; //"14.98.21.240";
	String GlobalIp2 = "157.49.142.178"; // 49.37.199.1
	String DeviceModel1 = "PRA-AL00X,SM-M405F"; // "Lenovo PB-6505M,SM-A205F";
	String SourceGroupName = "@SourceDonotTouch";
	String SourceSubGroupName = "SourceSubGroups";
	String DestinationGroupName = "@DestinationDonotTouch";
	
	String AnyOsJobGrpName = "!!AutomationAnyOs";

	String GroupNameRightClick = "@SourceDonotTouch";
	String SubGroupDevice = "@SubGroupDonotTouch";
	String AllJobStatus = "AllJobStatusJobs"; // AllJobStatusJobs
	String DefaultJobGroups_JobName = "DefaultJobGroups";
	String DeviceNameChange = "Auto";
	String GroupDeviceDrop = "@SourceDonotTouch";

	String SLActivationCode = "8242235894";
	String SVActivationCode = "6785548131";
	String SFActivationCode = "7802000034";

	// PushFile and Application
	String Pushapk = "./Uploads\\UplaodFiles\\Right Click-App\\File1.exe";
	String apkname = "GPS Test.apk";
	String Pushapk1 = "./Uploads\\UplaodFiles\\Right Click-App\\File2.exe";
	String apkname1 = "DeviceInfo.apk";
	String apkpackage1 = "com.curvefish.apps.deviceinfo";
	String Pushapk2 = "./Uploads\\\\UplaodFiles\\\\Right Click-App\\\\File3.exe";
	String apkname2 = "FakeGPS.apk";
	String PushFile3 = "./Uploads\\UplaodFiles\\FileStore\\File.exe";
	String PushFileNme3 = "File.xlsx";
	String PushFile4 = "./Uploads\\UplaodFiles\\FileStore\\Image.exe";
	String PushFileNme4 = "Image.jpg";
	String PushApp = "https://mars.42gears.com/support/inout/surevideo.apk";
	String Pushapk3 = "./Uploads\\UplaodFiles\\Right Click-App\\File4.exe";
	String apkname3 = "astrocontacts.apk";

	// Time Fence
	String JobNameToApplyMiscSettings = "AutomationFTDontDeleteMisc";
	String InstallJob = "AutomationInstallDontDelete";
	String ConsoleTime = "Console Time";
	String DeviceTime = "Device Time";
	String JobNameToApplyTimeFenceone = "Trials WiFi Modified Job";
	String JobNameToApplyTimeFencetwo = "Trials Firewall Job";

	// UserMangement
	String UserNameUsermanagementLogin = "Demouser1";
	String InputUser = "Demouser2";
	String InputUserDemouser1 = "Demouser1";
	String InputUserDemouser10 = "Demouser10";
	String InputUserDeviceGroupSet = "DemouserDeviceGroupSetEnabled";
	String InputSuperUser = "sa";
	String NewPwdSuperuser = "42Gears@345"; // Old pwd(42Gears@123)
	String RetypeNewPwdSuperuser = "42Gears@345";
	String NewPwd = "42Gears@234"; // Old pwd(42Gears@123)
	String RetypePwd = "42Gears@234";
	String NewPwdAgain = "42Gears@567";
	String RetypePwdAgain = "42Gears@567";
	String InputUserSuperuser = "sa";
	String DemouserAccountSettings = "DemouserWebhookSettingsPermission";
	String DemoUserDisabled = "DemoUserDisable";
	String UNdemouser2 = "Demouser2";
	String UNdemouser1 = "Demouser1";
	String FieldNameDataAnalytics = "AutomationDataAnalytics";
	String FieldNameDataAnalyticsSubUser = "SubUserAutomationDataAnalytics";
	String FieldNameDataAnalyticsUsers = "UsersAutomationDataAnalytics";
	String MoveToFolderAndroid = "Move to Folder Android Profile";
	String MoveToFolderIOS = "Move to Folder iOS Profile";
	String MoveToFolderWindow = "Move to Folder Windows Profile";
	String MoveToFolderMacOs = "Move to Folder macOS Profile";
	String RoleName1 = "Group + RemoteScreen + RemoteFileExlorer + Clipboard";
	String Roles1Description = "Descp";
	String user = "Demouser";
	String user1 = "Demouser1";
	String user2 = "Demouser2";
	String pswd = "42Gears@123";
	String RoleName = "AccountSettingsPermission";
	String RolesDescription = "AccountSettingPermission Description";
	String FirstName = "donotdelete";
	String EmailID = "Nolil@42gears.com";
	String EditUserNote = "Note: User name cannot be edited.";
	String SelectCreatedRoleInboxPermission = "AllPermissionsExceptInboxPermission";
	String PwdDemouser2 = "42Gears@123";
	String urlDemouser2 = "https://stagemdm.42gears.com";
	String RoleSelect = "SelectUnapprovedAndroidEnterpriseApplications";
	String RoleSelectJobPermission = "ALLJobpermissions";
	String SelectCreatedRoleWebhookSettings = "AllRoleWebhookSettingsPermission";
	String RemotePermissionUser = "AutomationUser_RemotePermission";
	String JobPermissionUser = "AutomationUser_JobPermission";
	String Superuser = "AutomationUser_SuperUser";
	String TagsPermissionUser = "AutomationUser_TagPermisson";
	String PushFileApplication = "AutomationUser_PushFile";
	String ReportPermission = "AutomationUser_Reports";
	String AutomationUser = "AutomationUser";
	String Automationrole = "AutomationRole";
	String LinuxProfileName = "AutomationLinuxProfile";
	String MacOSProfileName = "AutomationmacOSprofile";
	String AndroidProfileName = "AutomationAndroidProfile";
	String UMMessage = "SureMDM User Management allows you to customize user permissions for both existing and new administrators using the following set of permissions.";

	// Custom Reports-Device Details Custom Report

	// Application DataUsage CustomReport

	String SelectPackageName = "Package Name";
	String SelectPackageName1 = "com.google.android.gms";
	String SortBy_AppPackage = "Application Package";
	String SelectAscendingOrder = "asc";
	String AggregateOptionCount = "count";
	String SelectAggregateOptionAsMax = "max";
	String EnterAliasNameAsSelectedPlatform = "AliasNameForPlatform";
	String EnterAliasNameAsMyDevice = "My Device";
	String GroupByNameAsDeviceName = "Device Name";
	String Enter_App1 = "Google Play services";
	String Enter_App2 = "";
	String SelectApplicationName = "Application Name";
	String SelectMobileData = "Mobile Data(In Bytes)";
	String SelectWifiData = "WiFi Data(In Bytes)";
	String SelectGreaterThanOrEqualtoOperator = ">=";
	String SelectLessThanOrEqualtoOperator = "<=";
	String SelectNotEqualtoOperator = "!=";
	String SelectAppDataUSage = "Application Data Usage";
	String SelectDateTime = "Datetime";

	// DataUsage CustomReport

	String SelectDataUsage = "Data Usage";

	// System Log CustomReport
	String SelectSystemLog = "System Log";
	String SelectCallLogTable = "Call Log";
	String SelectLogTime = "Log Time";
	String SelectNumber = "Number";
	String SelectSMSLogTable = "SMS Log";
	String SelectCallTypeInColumn = "Call Type";
	String SelectName = "Name";

	// Device History Custom Report

	String SelectDeviceHistoryTable = "Device History";
	String DeviceHistory_CustomReportName = "DeviceHistory Custom Report";

	String DeviceDetails_CustomReportName = "DevDetails CustomReport";
	String CallLog_CustomReportName = "CallLog Custom Report";
	String SmsLog_CustomReportName = "SmsLog Custom Report";
	String InstalledJob_CustomReportName = "InstJobDet CustomReport";
	String ApplicationDataUSage_CustomReportName = "Application DataUsage Report";
	String DeviceLocation_CustomReportName = "DeviceLocation Custom Report";
	String DataAnalytics_CustomReportName = "DataAnalytics Custom Report";
	String LockCheckList_CustomReportName1 = "LockCheckList Report1";
	String LockCheckList_CustomReportName2 = "LockCheckList Report2";
	String LockCheckList_CustomReportName3 = "LockCheckList Report3";
	String SystemLog_CustomReportName = "SystLog Custom Report";
	String Select_SureLockCheckList = "SureLock CheckList";

	String Select_Defualtluncher = "SureLock Default Launcher";
	String SureLockDefaultLauncher = "No";

	String DeviceLocaleCol_Val = "en-GB";
	String DurationFormat1 = "00:00:00";
	String DurationFormat2 = "00:00:01";
	String DurationFormat3 = "00:00:02";

	String NameCol = "Name";
	String Descrip = "Description";
	String CreatedDate = "Created Date";
	String DataUsage_CustomReportName = "DataUsage Report";
	String DeviceDetails_CustomReportDescription = "dadfghjkl;";
	String Version = "6.0.1";
	String SearchCustomized_Report = "Automation testing device";
	int LenthOfTheDeviceList = 79;
	int LenthOfTheDeviceList1 = 7;
	int LenthOfTheDeviceList3 = 5;
	int LenthOfTheDeviceList2 = 4;
	String Group_Name = "Home/Kavya";
	String DeviceDetails_ReportView = "Client0002";

	String EnterBatteryPercentage = "5";
	// String SendUsedMobileDataRange="100";
	// String EnterValueAs500="500";
	String SendDataUsage = "50000";
	String SelectJobName = "Job Name";
	String SelectStatus = "Status";
	String EnterOperatingSystem = "Operating System";
	String DeviceNameValue = "Device Name";
	String SelectValueAsBatteryPercent = "Battery Percent";
	int Input_Val1 = 500;
	String EnterSomeLongitudeValue = "12.9126898";
	String EnterLocationAsAMRtechPark = "Bellary - Hubli Rd, Munirabad Project Area, Karnataka 583234, India";
	// String EnterDeviceStarted="Device Started";
	String Input_Val17 = "Device Started.";
	// String SendApplicationName="MDM Nix";
	int LengthOfAppliedJob = 7;
	String EnterOSVersion = "6.0.0";
	double App_Version = 2.0;
	String EnterApplicationVersion = "2.0";
	// String EnterChromeVersion="78.0.3904.62 (390406237)";
	int Battery_Percentage = 5;
	String Device_Name = "Dontouch2";

	String Device_Name_IOS = "iPad mini 4 16 GB Gold";
	String Device_Name_Linux = "Vostro 3446 (version: Not Specified)";
	String OS_Name = "PIE";
	String Device_Platform = "ANDROID 10";
	String Device_Model = "moto g(7)";

	int LengthOf_CallLog = 8;
	String EnterJobNameAsTextMessage = "TextMessage";
	String EnterJobNameAsWifiSettingsJob = "Wifi setting job";
	String EnterJobStatusAsDeployed = "Deployed";
	String Generated_Report = "Device Report3";
	String Job_Name = "TextMessage";
	int LengthOfInstalledJob = 6;
	String Installed_App = "Calculator";
	String Installed_App1 = "Calendar";
	String SendAliasNameAsTestingdevice = "Testing device";
	String Dev_Name = "i pad ps team";
	String Browsed_Url = "www.42gears.com";
	String App_bundleid = "com.apple.mobilesafari";
	String Defualt_App3 = "The APK is potentially malware.";
	String Varify_App = "Chrome";
	String EnterPackageId = "com.nix";
	String Application_Name = "Gmail";
	int Installed_Job = 708;
	String Agent_version = "15.66";
	String SortBy_DeviceName = "Job Name";

	String Col_Val1 = "N/A";
	String Col_Val2 = " ";
	String Col_Val3 = "0";
	String GroupByPlatform = "Platform";

	String EnterSureMDMNix = "SureMDM Nix";
	String Alias_NAme_JobName = "All Jobs List";
	String Alias_ActlJobName = "Job Name";
	String Custom_Column = "abcdef";
	String SelectEqualOperator = "=";
	String SelectlikeOperator = "like";

	String SelectAppPacakage = "Application Package";
	String ApplicationNameAsGmail = "Gmail";
	String EnterCallDuration = "20";
	String EnterCallType = "Incoming";
	String EnterContactNumber = "+918792164818";
	String Profile_Name = "MTDProfile";
	String SendContactName = "N/A";

	String SelectAppType = "Application Type";
	String SelectApplicationVersion = "Application Version";
	String SendJobStatusAsInstalled = "Installed";
	String SelectDeviceDetails = "Device Details";
	String SelectInstalledAppDetails = "Installed App Details";

	String SelectAppliedJobDetails = "Applied Job Details";
	int LenthOfTheDeviceList_AfterModify = 78;
	int LenthOfTheDeviceList_AfterModifyClickOnModify = 84;

	String SelectAddress = "Address";
	int Data_usage = 100;
	String Data_usage1 = "100";
	// String Column_Val2="WiFi Data(In Bytes)";
	String SelectLongitude = "Longitude";
	String Column_Val4 = "Longitude";
	String SelectDeviceLocation = "Device Location";
	float longitudeValue = 12.9126898F;
	String ExpectedDeviceAddress = "AMR Tech Park";
	String Current_Date = "2019-09-25";

	// String SelectColumnAsLogTime="Log Time";
	String SelectMessageInColumn = "Message";
	String SelectCallDuration = "Duration (Seconds)";
	String SelectLockDefaultLauncher = "Lock Default Launcher";
	String SelectSmsType = "SMS Type";
	String Sort_CallLogCheckList = "Call Type";
	String Sort_CostumColumn = "Device Type";
	String SelectModel = "Model";
	int Actl_duration = 60;
	String subuser_name = "POC";
	String subuser_passwrd = "Demo@123";
	String Report = "Reports";
	String Delete_Device_Log = "delete";

	// DeviceDetails Columns
	String IP_Address = "223.237.173.185";
	String IPAdd_Like = "223";
	String LocalIP_Address = "192.168.43.1";
	String LocalIP_Like = "192";

	String Platform = "Android";
	String Model_Name = "moto g(7)";
	String Model = "moto";

	String MAC_Address = "0C:CB:85:6E:58:51";
	String Network_OPerator = "airtel";
	String SSID = "Siti brodband";

	String AgentVersion = "20.01";
	String IMEI = "354137101143792";
	String IMSI_Type = "404450955973247";

	String Protocal_Type = "Secured";
	String RootStatus_Type = "Signed";
	String KNOXStatus = "Yes";

	String SureLock_version = "14.18";
	String SureFox_version = "8.05";
	String SureVideo_version = "3.84";

	String DefaultLauncher = "com.sec.android.app.launcher";

	String Phone_Number = "+916366231496";
	String Roaming_Status = "No";
	double OS_Version = 9;

	String Group_Path = "Home/dontouch";// Home/Deeksha,
	String GroupName_Like = "dontouch";// send group name for like operator
	String Tag = "Android,moto g(7)";// moto g(7),Android
	String TagName_Like = "Android";// send Tag name for like operator

	String DeviceUser_Name = "Sachi Gowda";// Vishnuvardhan Reddy
	String DeviceUserName_Like = "Sachi";

	String AndroidEnterpriseStatus = "Device Owner";
	String Device_Time_Zone = "Asia/Calcutta (GMT +05:30)";
	String DeviceTimeZone_Like = "Asia";

	String AndroidID = "1eb574088994365f";
	String HashCode = "b87c018f";

	String FOTARegistration_Status = "SUCCESS";
	String Firmware_Version = "Success";
	String Value = "5";

	// AppStore
	String AppStoreEditOldVersionToLatest = "Facebook283";
	String AppStoreEdit = "Facebook1";

	// TAGS
	String TagName = "moto g(7)";
	String TagNameDeviceModelName = "moto g(7)";
	
	String TagNameDevicePlatFormName="moto g(7)";
	String TagNameDevice = "AutomationTag";
	String TagNameDevicePlatFormNameRightClick = "moto g(7)";
	String TagNameRightClick = " #Auto0001";
	String RenameTag = "RenamedTag";
	String FirstTagName = "TestTag";
	String CustomTagName = "CustomTag";
	String SecondDeviceNameFilter = "donotdelete";
	String DeviceNameTag = "Client9137";
	String DeviceModelTag = "moto g(7)";

	String InputUser1 = "Autouser01";
	String InputUser2 = "Autouser02";
	String InputUser3 = "Autouser03";
	String InputUser4 = "AutouserJobPermission";
	String InputUser5 = "Autouser5";
	String InputUser5Wrongpwd = "42gears";
	String InputDeviceGrpSetUser = "AutouserDeviceGroupSetEnabled";

	String RolesFoeWebhookSettings = "AllRoleWebhookSettingsPermission";
	String DescriptionWebhookSettings = "WebhookSettings";
	String SubUser3 = "AutomationUserDontDelete";
	String pwd3 = "42Gears@123";
	String DataAnaRoles = "AllRoleDataAnalyticsSettingsPermission";
	String InputDataAnaName = "AutomationDataAnalytics";
	String InputDataAnaTagName = "AutomationDataAnalytics";
	String InputFieldName = "AutomationDataAnalytics";
	String InputDataAnaNameSubUser = "SubUserAutomationDataAnalytics";
	String InputDataAnaTagNameSubUser = "SubUserAutomationDataAnalytics";
	String InputDataAnaFieldNameSubUser = "SubUserAutomationDataAnalytics";
	String DataAnaUser = "DemouserAccountSettingsPermissionUser";
	String InputDataAnaNameUser = "UsersAutomationDataAnalytics";
	String InputDataAnaTagNameUser = "UsersAutomationDataAnalytics";
	String InputDataAnaFieldNameUser = "UsersAutomationDataAnalytics";
	String InputProfilePermissionRoles = "SelectUnapprovedAndroidEnterpriseApplications";
	String JobNameUM = "donotdelete";
	String FolderNameUMJobPermission = "DonotdeleteFolder";
	String EnterpriseAccURL = "https://enterprise.test.suremdm.io";
	String EnterpriseAccUser = "Enterprise@Getnada.Com";
	String EnterpriseAccPwd = "42Gears@123";
	String PremiumAccURL = "https://premiumclrmailcom.test.suremdm.io";
	String PremiumAccUser = "Premium@Clrmail.Com";
	String PremiumAccPwd = "42Gears@123";
	String LegacyAccURL = "https://legacyinboxbearcom.test.suremdm.io";
	String LegacyAccUser = "Legacy@Inboxbear.Com";
	String LegacyAccPwd = "42Gears@123";
	String StandardAccURL = "https://standardvomotocom.test.suremdm.io";
	String StandardAccUser = "Standard@Vomoto.Com";
	String StandardAccPwd = "42Gears@123";
	String PermissionForAllRoles = "ALLRolespermissions1";
	String InputRoles_Desc_DeviceGroupSet = "EnableGroup_@SourceDonotTouch";
	String PermissionForAllRolesDeviceGrpSet1 = "ALLRolespermissions3";
	String InputRolesDescDeviceGrpSet_Disable = "DisableGroup_@SourceDonotTouch";
	String PermissionForAllRolesDeviceGrpSet2 = "ALLRolespermissions4";

	// GROUPS
	String DefaultJob = "donotdelete";
	String GroupsApplyJobFileTransfer = "0FileTransfer";
	String DeviceInsideGroups = "Client0001";
	String DeviceInsideSubgroups = "Client0002";
	String JobNew = "ImageFile";
	String DeviceAccountId = "1";
	String ChangedDeviceName = "Auto";
	String FailJob = "SureFoxSettings_Automation";
	String FileTransferJobTAGS = "FileJob";
	String AutoGroup = "!Automation_Test";
	String AutoGroup1 = "!Automation_Test1";
	String homeGroup = "Home";
	String GroupNote = "The default jobs from both source and destination groups, if any, will be merged and applied to the underlying devices automatically. Please review the modified Default Jobs for the destination group (selected above) under Group Properties.";
	
	// DeviceInfo Panel

	String KnoxDeviceName1 = "Reg9559S";

	// LINUX PLATFORM
	String Linux_DeviceName = "Client1873CD";
	String Linux_ModelName = "VirtualBox (version: 1.2)";
	String DeviceNameLinux = "Client1873CD";
	String Linux_AgentVersion = "1.19";
	String Linux_LastDeviceTime = "7 Sept 2020 8:30:10 AM";
	String Linux_Notes = "Add notes here";
	String Linux_CustomCol = "N/A";
	String Linux_MACaddress = "4C:BB:58:3F:97:93";
	String Linux_IPaddress = "106.193.79.113";
	String LinuxConnection = "Secured";

	// ANDROID PLATFORM
	String AndroidDeviceModelName = "SM-A217F";
	String AndroidDevice_Name = "Client0000";
	String Android_OS = "ANDROID 10 [Android]";
	String Android_AgentVersion = "26.02.07";
	String Android_LastDeviceTime = "12 Jan 2021 5:16:55 PM";
	String Android_AddNotes = "AutomationMDM";
	String Android_LocTracking = "OFF";
	String Android_GeoFence = "";
	String Android_TimeFence = "";
	String Android_Telecom = "ON";
	String Android_CustomCol = "N/A";

	// WINDOWS PLATFORM
	String WindowsDeviceModelName = "Modern 14 A10M";
	String WindowsDevice_Name = "SHRILATHA-320";
	String Windows_OS = "Microsoft Windows 10 Home Single Language [Windows]";
	String Windows_AgentVersion = "4.63";
	String Windows_LastDeviceTime = "11 Jan 2021 12:21:34 PM";
	String Windows_AddNotes = "Add notes here";
	String Windows_LocTracking = "OFF";
	String Windows_GeoFence = "";
	String Windows_TimeFence = "";
	String Windows_Telecom = "ON";
	String Windows_CustomCol = "N/A";
	String Windows_MACaddress = "60:45:CB:C1:C3:29";
	String WindowsIPaddress = "103.206.227.192";
	String WindowsLocalIPaddress = "172.25.128.1";
	String WindowsConnection = "Secured";
	String WindowsWifiSignal = "100%";
	String WindowsMobileSignal = "0%";
	String WindowsIMEI = "356131100049629";
	String WindowsSerialNumber = "N0CV1708MB0159025";

	// WINDOWS EMM PLATFORM
	String Windows_DeviceNameEMM = "";
	String WinEMM_DeviceModelName = "";
	String WinEMM_OS = "";
	String WindowsEMM_AgentVersion = "";
	String WindowsEMM_LastDeviceTime = "";
	String WindowsEMM_LocTracking = "";
	String WindowsEMM_InstalledPro = "";
	String WindowsEMM_CustomCol = "";
	String WindowsEMM_MACaddress = "00:A0:C6:00:00:00";
	String EMMIPaddress = "1.129.107.28";
	String WindowsEMM_Connection = "Secured";

	// MacOS DeviceInfo

	String Macos_DeviceName = "KArthik-239-MacOS";
	String MacOS_DeviceModel = "MacBook Air";
	String MacOS_DeviceOS = "10.13.6 [macOS]";
	String MacOS_ChangedDeviceName = "Prachujya's Macb";
	String MacOS_NixAgentVerson = "2.70";
	String MacOS_LastDeviceTime = "";
	String MacOS_AddNotes = "";
	String MacOS_LocTracking = "";
	String MacOS_InstalledPro = "";
	String MacOS_CustomCol = "";
	String MacOS_DeviceMacAddress = "14:c2:13:11:9c:c6";
	String MacOS_DeviceSerialNumber = "FVFXJSJAJ1WK";
	String MacOS_DeviceOsInDeviceGrid = "10.13.6";
	String MacOS_RightClickGroupPath = "Home/AATestGroupPath";

	// IOS Device Info
	String WifiSSID = "noni";
	String WifiSSID2 = "Siti broadband";

	String IOS_DeviceModel = "iPad";
	String iOS_OS = "14.4.2 [iOS/iPadOS]";
	String iOS_DeviceName = "iPad";
	String iOS_LastDeviceTime = "12 Jan 2021 12:39:57 PM";
	String iOS_AddNotes = "Add notes here";
	String iOS_LocTracking = "ON";
	String iOS_Telecom = "ON";
	String iOS_InstalledPro = "None";
	String iOS_Supervised = "Yes";
	String iOS_ActivatedLockCode = "DU5C2-2KFXL-KL0A-XMZX-TMT3-3NV1";
	String iOS_CustomCol = "N/A";

	// Network Panel for Android device

	String AndroidMACaddress = "E4:5D:75:CB:FE:26";
	String AndroidIPaddress = "117.199.247.84";
	String AndroidLocalIPaddress = "192.168.1.4";
	String AndroidConnection = "Unsecured";
	String AndroidWifiSignal = "100%";
	String AndroidMobileSignal = "9%";
	String AndroidIMEI = "352810080618040";
	String AndroidSerialNumber = "RZ8H812DF9T";
	String AndroidIMSI = "404450925650339";
	String AndroidWIFI_SSID = "V V R";
	String AndroidMobileNetwork = "Airtel | airtel";
	String SimSerialNumberAndroid = "89914509009256503395";

	// VR
	String DeviceNameVR = "Ankush_VR";
	String VRdeviceModel = "Pacific";
	String VRoperatingSystem = "[Android VR]";
	String VR_BluetoothName = "Oculus Go";
	String VR_AgentVersion = "1.96";
	String VR_LastDeviceTime = "9 Sept 2020 5:02:32 PM";
	String VR_DeviceNotes = "Add notes here";
	String VR_TimeFence = "OFF";
	String ColumnVR = "N/A";

	// Windows Mobile Device
	String WindowsMobileDeviceModelName = "LGE-VS750";

	// PRINTER
	String DeviceNamePrinter = "G18F78308 Joya Scanner"; // "raspberrypi Dummy printer"; //"Client0382 EPSON M205
															// Series";

	String EditingParentDeviceName = "TestDonotTouch";

	// DATA LOGIC DEVICE
	String DeviceNameDataLogic = "G18F78308 DataLogicSmart Battery";
	String ParentDeviceName = "";

	// EFOTA
	String DeviceNameFOTA = "Reg9316S";

	// DUAL ENROLLMENT
	String DualEnrollmentDeviceName = "Test42GEARS";
	String DualEnrollment_MacAddress = "42:9F:38:52:A5:3F";
	String DualEnrollment_MacAddress_LocalAreaConnection = "Local Area Connection* 9";
	String DualEnrollment_showMoremacAddress1 = "52:9F:38:52:A5:3F";
	String DualEnrollment_showMoremacAddressLocal = "Local Area Connection* 10";
	String DualEnrollment_showMoremacAddress2 = "40:9F:38:52:A5:3F";
	String DualEnrollment_showMoremacAddressWifi = "Wi-Fi";
	String DualEnrollment_showMoremacAddress3 = "40:9F:38:52:A5:3E";
	String DualEnrollment_showMoremacAddress_BluetoothNetworkConnection = "Bluetooth Network Connection";

	String TimeFenceEnterJob = "JobEnterTimeFence";
	String TimeFenceExitJob = "JobExitTimeFence";
	String EmailForAlert = "anithamdm@mailinator.com";
	int DeploymentTime = 120;

	String Things_deviceModel = "Handheld Scanner";
	String Printer_OperatingSystem = "[Things]";
	String Printer_AgentVersion = "18.91";
	String Things_LastDeviceTime = "16 Jul 2020 3:30:17 AM";
	String Things_DeviceNotes = "Add notes here";
	String ColumnThings = "N/A";
	String Windows_DeviceName = "KARTHIK-239";
	String WindowsDeviceModelNameEMM = "Precision 3551";
	String WindowsMobileOS = "[PocketPC]";
	String WindowsMobileDeviceName = "client0855";
	String AgentVersionWindowsMobile = "2.58";
	String LastDeviceTimeOfWindowsMobile = "1 Jan 2001 12:00:00 AM";
	String AddNotesofWindowsMobile = "Add notes here";
	String LocTrackingWindowsMobile = "OFF";
	String CustomColumnWindowsMobile = "N/A";
	String DeviceModelNameSacnnerAndPrinter = "Joya Scanner"; // "Dummy printer";

	String DetectederrorstatePrinter = "No error";
	String Black_tonerpercentagePrinter = "89.77";
	String DeviceStatusPrinter = "idle";
	String SystemUptimePrinter = "50min 60sec";
	String ColorsPrinter = "black";
	String TotalPrints = "123";

	String ParentDeviceNameSearch = "DonotTouch";
	String EditingChildDeviceName = "TestDonotTouch Handheld Scanner";

	String HealthDataLogicSmartBattery_ThingsInfo = "93";
	String ConnectorNameDataLogicSmartBattery_ThingsInfo = "Datalogic IoT Connector ";
	String BatteryRemainingDataLogicSmartBattery_ThingsInfo = "2015";
	String TimeRemainingDataLogicSmartBattery_ThingsInfo = " 45 ";
	String BatteryCapacityDataLogicSmartBattery_ThingsInfo = "2735";

	String FOTAstatus = "Success";
	String ColumnName = "FOTA Registration Status";

	String DualEnrollmentDeviceModel = "NUC7i5DNHE";

	// IOS DeviceInfo

	String IOS_DeviceName = "iPad";
	String IOS_DeviceOS = "12.3 [iOS]";
	String IOS_DeviceMAC = "b4:9c:df:d1:96:3e";
	String IOS_DeviceSerialNumber = "F9FTJ069HLFC";

	// TELECOM AND NETWORK FENCE

	String Limit1 = "Limit1";
	int DataLimit2 = 92;
	int DataLimit1 = 28;
	String AppBundledimo = "com.imo.android.imoim"; // "net.peakgames.amy";//"com.joytunes.simplypiano";
	String AppBundledFlipkart = "com.flipkart.android"; // "sg.bigo.live";//"com.jeevansathi.android";
	String SubTitleFlipkart = "Flipkart";
	String TelecomManagementPolicyFile = "AutomationFTDontDeleteTelecom";
	String ConfigureBillingDaily = "Daily";
	String AppBundledTrain = "com.whereismytrain.android";
	String AppBundledTikTok = "com.zhiliaoapp.musically";
	String TelecomManagementPolicyFileReset = "ResetAutomationFTDontDeleteTelecom";
	String AppBundledSureFox = "com.gears42.surefox";
	String ConfigureBillingMonth = "Monthly";
	String TelecomStatusOn = "ON";
	String SubTitleWhereIsMyTrain = "Sigmoid Labs and its Affiliates";
	String SubTitleTikTok = "TikTok Inc.";
	String Limit2 = "Limit2";
	String TelecomManagementPolicyFileDisable = "DisableAutomationFTDontDeleteTelecom";
	String ConfigureBillingCustom = "Custom ";
	String CustomNumberOfDays = "7";
	String Zip_Filename1 = "Enrolment-QR.j";
	String ConfigureBillingWeekly = "Weekly";

	// WebHook Settingd

	String WrbhookEndPoint = "https://webhook.site/eb3b1869-9625-4db6-84ea-09807fc0e09f";
	
	
	
	// SuperUser Details
	String SuperUserName = "licensedindia@dispostable.com";
	
	// Cisco ISE 
	
	String CiscoISEUserName= "licenseduk";
	String CiscoISEPassword= "42Gears@123";
	
	
	// Group Assignment Rules
	
	String RuleName1= "Rule";
	String SSIDName= "SSID_NAME";
	String MultipleSSIDRule= "Multiples Rules";
	String SSIDName1= "SSID_NAME1";
	String SSIDName2= "SSID_NAME2";
	String DeviceModelName= "Device_Model1";	
	String GlobalIPAddress= "IP_Address";
	String SSIDValue= "Value1,Value2";	
	String SSIDNameDevice= "KESHAV";
	String DeleteNewRuleName= "Delete New Rule";
	String DisabledGroupRule= "Disabled Group Rule";
	String MultipleSource="Multiple Source";
	String EditRuleName= "Edit Rule";
	
	
	
	//iOS/iPadOS/macOS Settings DEP
	
	
	String SupportPhoneNumber= "123456789";
	String SupportEmailid= "abc@gmail.com";
	String DEPProfileName= "Automation DEP Profile";
	String DEPServersName= "Automation DEP Servers";
	String DEPServersDescription= "Automation Modified Server";
	String DEPProfileName1= "Delete Automation DEP Server";
	String DEPServersModified= "Modified DEP Server";	
	String EditProfileName= "Edit Profile Automation";
	
	//Battery Policy
	String BatteryPolicyText= "&lt;b&gt;Device Name: &lt;/b&gt; %DeviceName% &lt;br/&gt;&lt;b&gt;Device Notes: &lt;/b&gt;"
			+ " %DeviceNotes% pls make a note &lt;br/&gt;&lt;b&gt;Current battery level: &lt;/b&gt;"
			+ " %CurrentBatteryLevel% %&lt;br/&gt;&lt;b&gt;Specified threshold: &lt;/b&gt;"
			+ " %SpecifiedThreshold% % .&lt;br/&gt;&lt;b&gt;";
	String CustomizeBatteryPolicyText="Battery Percentage is below the threshold,please charge the device";
	String AlertTemplateBatteryPolicyName="Alert Template Battery Policy";
	String CustomizeAlertTemplateBatteryPolicyName="Customize Alert Template Battery Policy";	
	String BatteryPolicyDeviceName="AutomationDevice_A51";
	String CustomizeDeviceName="AutomationDevice_A51";
	
	String DataUsagePolicyText="&lt;b&gt;Device Name: &lt;/b&gt; %DeviceName% &lt;br/&gt;&lt;b&gt;Device Notes: &lt;/b&gt;"
			+ " %DeviceNotes% &lt;br/&gt;&lt;b&gt;Current Data Usage: &lt;/b&gt; %CurrentDataUsage% &lt;br/&gt;SpecifiedThresholdData% .";
	
	String DataUsageJobName="Trials Data Usage Policy";
	String DataUsageDeviceName="AutomationDevice_A51";
	String DataUsagePolicyuserdefinedText="Data usage is above mentioned threshold,limit the data usage ";
	String DataUsageUserdefinedJobName="Trials Userdefined Data Usage Policy";
	
	String ConnectionPolicyText="<b>Device Name: <b/> %DeviceName% was offline for %OfflineTimein hrs% ,"
			+ " since %OfflineDateTime% (UTC) + <br/><b>Device Notes: </b> %DeviceNotes% .\n"
			+ "Device is offline \n"
			+ "";
	String ConnectionPolicyJobName="Trials Connection Policy";
	String ConnectionPolicyDeviceName="AutomationDevice_A51";
	
	String ConnectionPolicyUserdefinedText="Device is offline,please make it online";
	
	String ConnectionUserdefinedJobName="Trials Userdefined Connection Policy";
	
	String NotifywhendevicecomesonlineText="<b>Device Name: <b/> %DeviceName% came online at"
			+ " %OnlineDateTime% (UTC) <br/><b>Device Notes: </b> %DeviceNotes%\n"
			+ "Device is online";
	
	String NotifywhendevicecomesonlineJobName="Trials Notify when device comes online";
	
	String NotifywhendevicecomesonlineDeviceName="AutomationDevice_A51";
	
	String NotifywhendevicecomesonlineUserdefinedText="Device is offline,please make it online";
	
	String NotifywhendevicecomesonlineUserdefinedJobName="Trials Userdefined Notify when device comes online";
	
	String NotifywhendevicecomesonlineuserdefinedDeviceName="AutomationDevice_A51";
	
	String NotifywhenSIMischangedText="Device Name:   %DeviceName% <br/>New IMSI:   %NewIMSI% + <br/> Old IMSI:   %OldIMSI%";
	
	String NotifywhenSIMischangedJobName="Trials Notify when SIM is changed";
		
	String NotifywhenSIMischangedDeviceName="AutomationDevice_A51";
	
	String NotifywhenSIMischangeduserdefinedText="Device sim is changed";
	
	String NotifywhenSIMischangedUserdefinedJobName="Trials Userdefined Notify when SIM is changed";
	
	String NotifywhenSIMischangedUserdefinedDeviceName="AutomationDevice_A51";
	
	String NotifywhendeviceisrootedText="<b>Device Name: <b/> %DeviceName%  %RootPermission%";
	
	String NotifywhendeviceisrootedJobName="Trials Notify when device is rooted";
	
	String NotifywhendeviceisrootedDeviceName="AutomationDevice_A51";
	
	String NotifywhendeviceisrootedUserdefinedText="Device is rooted";
	
	String NotifywhendeviceisrootedUserdefinedJobName="Trials Userdefined Notify when device is rooted";
	
	String NotifywhendeviceisrootedUserdefinedDeviceName="AutomationDevice_A51";
	
	// miscellaneous Settings 
	
	String MiscellaneousDeviceName="AutomationDevice_Lenovo";
	
	String EnableGlobalSearchDeviceName="AutomationDevice_Lenovo";
		
	String DisableGlobalSearchDeviceName="AutomationDevice_MMMM";
	
	String DisableEnableAutosearchDeviceName="AutomationDevice_Lenovo";
	
	String EnableAutosearchDeviceName="AutomationDevice_Lenovo";
	
	String TemperatureUnitDeviceName="AutomationDevice_Lenovo";
	
	//
	String CustomereportName="Trails Battery Temperature";
	String CustomereportDescription="Battery Temperature in Celsius";
	
	String SearchDeviceName = "AutomationDevice_Lenovo";
	
	
	
	
	
	//windows
	
	//String WindowDeviceName = "KARTHIKBS-239";
	//String WindowDeviceName = "Client9298abc";
	//String WindowDeviceName = "TEST_DUAL";
	String WindowDeviceNameCompositejob = "TEST_DUAL";
	String ModifiedAppUninstallJobName = "Trials Modified App Uninstall Job";
	String SearchforApplicationName = "AMDs Settings";
	
	String RunScriptStartName = "Run Script Start";
	String RunScriptStartText = " !#suremdm locationtracking start <interval> ";
	
	String RunScriptRestartName = "Run Script Restart";
	String RunScriptRestartText = " !#suremdm locationtracking restart <interval> ";
	
	String RunScriptStopName = "Run Script Stop";
	String RunScriptStopText = " !#suremdm locationtracking stop ";
	
	String WindowComplianceJob = "KARTHIKBS-239";
	
	String NotificationWeebookJobName = "Trials Weehbook Job";
	
	String TrackingperiodicityDevice = "KARTHIKBS-239";
	
	String SureLockDevice = "KARTHIKBS-239";
	
	String SureLockJobName = "Trials Surelock Settings Job";
	
	String InstallApplicationDevice = "SALMAN-258RK";
	
	String SureMDMNixSettings = "SALMAN-258RK";
	
	String FirewallpolicyDevice = "SALMAN-258RK";
	
	String ProxypolicyDevice = "SALMAN-258RK";
		
	String WiFIConfigurationDevice = "SALMAN-258RK";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}