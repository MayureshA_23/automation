package RightClick;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;
import Library.Config;
// "mauriciotogneri File Manager" app must be installed on the device

public class PushFile_Scripts extends Initialization {

	@Test(priority='1',description="1.To verify Push File Uploading Single File")
	public void VerifyPushFileUploading() throws Throwable {
		Reporter.log("\n1.To verify Push File Uploading Single File",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
	    rightclickDevicegrid.ClickOnPushFile();
	    rightclickDevicegrid.UploadFile(Config.FileStore_audiofile, Config.FileStore_AudioName);
		rightclickDevicegrid.ClickOnNextBtnPushFilePopup();
		rightclickDevicegrid.VerifyingSuccessMsg();
		rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	}
	
	@Test(priority='2',description="2.To verify Push File On device")
	public void VerifyPushFileOndeviceSingle() throws Exception {
		commonmethdpage.AppiumConfigurationCommonMethod("com.mauriciotogneri.fileexplorer","com.mauriciotogneri.fileexplorer.app.MainActivity");
		Reporter.log("\n2.To verify Push File On device",true);
		rightclickDevicegrid.LaunchFileManager();
		commonmethdpage.commonScrollMethod("Audio.mp3");
		rightclickDevicegrid.ClickOnHomeButtonDevice();
		rightclickDevicegrid.CloseRecentApp();
		rightclickDevicegrid.ClickOnHomeButtonDevice();
	}
	
	@Test(priority='3',description="3.To verify Push File Uploading MulripleFile")
	public void VerifyUploadingMultipleFile() throws Exception{
		Reporter.log("\n3.To verify Push File Uploading MulripleFile",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
	    rightclickDevicegrid.ClickOnPushFile();
	    rightclickDevicegrid.UploadFile(Config.PushFile3, Config.PushFileNme3);
	    rightclickDevicegrid.UploadFile(Config.PushFile4, Config.PushFileNme4);
	    rightclickDevicegrid.ClickOnNextBtnPushFilePopup();
		rightclickDevicegrid.VerifyingSuccessMsg();
		rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	}
	
	@Test(priority='4',description="4.To verify Push File On device Multiple")
	public void VerifyPushFileOndeviceMultiple() throws Exception {
		commonmethdpage.AppiumConfigurationCommonMethod("com.mauriciotogneri.fileexplorer","com.mauriciotogneri.fileexplorer.app.MainActivity");
		Reporter.log("\n4.To verify Push File On device Multiple",true);
		rightclickDevicegrid.LaunchFileManager();
		commonmethdpage.commonScrollMethod("Audio.mp3");
		commonmethdpage.commonScrollMethod("File.xlsx");
		rightclickDevicegrid.ClickOnHomeButtonDevice();
		rightclickDevicegrid.CloseRecentApp();
		rightclickDevicegrid.ClickOnHomeButtonDevice();
	}
	
	@Test(priority='5',description="5.To verify Push File On device URL")
	public void VerifyUploadingFileByFilepath() throws InterruptedException, Exception {
		Reporter.log("\n5.To verify Push File On device URL",true);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
	    rightclickDevicegrid.ClickOnPushFile();
	    rightclickDevicegrid.PushFileUploadFilePath();
	    rightclickDevicegrid.ClickOnNextBtnPushFilePopup();
		rightclickDevicegrid.VerifyingSuccessMsg();
		rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
	}
	
	@Test(priority='6',description="6.To verify Push File On device URL")
	private void verifyPushFileOndevicebyURL() throws Exception {
		Reporter.log("\n6.To verify Push File On device URL",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mauriciotogneri.fileexplorer","com.mauriciotogneri.fileexplorer.app.MainActivity");
        rightclickDevicegrid.LaunchFileManager();
        commonmethdpage.commonScrollMethod("Image.jpg");
		rightclickDevicegrid.ClickOnHomeButtonDevice();
	}
	
	@Test(priority='7',description="7.Verify Push File by exiting the dialogue box. ")
	private void verifyPushFileOndeviceDialogbox_TC_DG_088() throws Exception {
		Reporter.log("\n7.Verify Push File by exiting the dialogue box. ",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
	    rightclickDevicegrid.ClickOnPushFile();
	    rightclickDevicegrid.UploadFile(Config.FileStore_audiofile, Config.FileStore_AudioName);
		rightclickDevicegrid.ClickOnNextBtnPushFilePopup();
		rightclickDevicegrid.VerifyingSuccessMsg();
		rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.FileStore_AudioName,Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mauriciotogneri.fileexplorer","com.mauriciotogneri.fileexplorer.app.MainActivity");
		//rightclickDevicegrid.LaunchFileManager();
		commonmethdpage.commonScrollMethod("Audio.mp3");
		rightclickDevicegrid.ClickOnHomeButtonDevice();
		rightclickDevicegrid.CloseRecentApp();
		rightclickDevicegrid.ClickOnHomeButtonDevice();
		
	}
	@Test(priority='8',description="8.Verify Push File from Jobs section")
	private void verifyPushFileFromJobsSection_TC_DG_090() throws Exception {
		Reporter.log("\n8.Verify Push File from Jobs section",true);
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		rightclickDevicegrid.RightClickOndevice(Config.DeviceName);
	    rightclickDevicegrid.ClickOnPushFile();
	    rightclickDevicegrid.UploadFile(Config.PushFile3, Config.PushFileNme3);
		rightclickDevicegrid.ClickOnNextBtnPushFilePopup();
		rightclickDevicegrid.VerifyingSuccessMsg();
		rightclickDevicegrid.ClickOnCloseBtnpushFilePopUp();
		
		androidJOB.SearchDeviceInconsole(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField("PushFile_File.xlsx"); 
		androidJOB.JobInitiatedOnDevice();
		accountsettingspage.ReadingConsoleMessageForJobDeployment("PushFile_File.xlsx",Config.DeviceName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("File.xlsx",Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.mauriciotogneri.fileexplorer","com.mauriciotogneri.fileexplorer.app.MainActivity");
		//rightclickDevicegrid.LaunchFileManager();
		commonmethdpage.commonScrollMethod("File.xlsx");
		rightclickDevicegrid.ClickOnHomeButtonDevice();
		rightclickDevicegrid.CloseRecentApp();
		rightclickDevicegrid.ClickOnHomeButtonDevice();
		
	}
	
	
	
	
	
	
}
