package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class OtherPermissions extends Initialization{

	@Test(priority='1',description="1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException{
		Reporter.log("\n1.To Verify roles option is present",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		
	}
	@Test(priority='2',description="2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException{
		Reporter.log("\n2.To Verify super user is present",true);
	
		usermanagement.toVerifySuperUsertemplete();
	}
	@Test(priority='3',description="3.To create roll for other permission")
	public void createRollForOtherPermission() throws InterruptedException{
		Reporter.log("\n3.To create a roll for Other permission",true);
	
		usermanagement.RoleWithOtherPermissions();
	}
	@Test(priority='4',description="4.To create User for Device Permissions")
	public void CreateUserForOtherPermission() throws InterruptedException{	
		Reporter.log("\n4. User with other Permissions",true);
		usermanagement.CreateUserForPermissions("AutoOtherPermission", "42Gears@123", "AutoOtherPermission", "other Permission");  // uncomment remove later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoOtherPermission", "42Gears@123");
		//usermanagement.RefreshCurrentPage();
		Thread.sleep(2000);
		}
	@Test(priority='5',description="5.Activity log enabled")
	public void ActivityLogEnabled() throws InterruptedException{
		Reporter.log("\n5.Activity log enabled",true);	
		usermanagement.ActivityLogEnabled();
	}
	@Test(priority='6',description="6. Changepassword enabled")
	public void ChangePasswordEnabled() throws InterruptedException{
		Reporter.log("\n6.Activity log enabled",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.clickOnChangePassword();
	}
	@Test(priority='7',description="7.Changepassword enabled")
	public void DisablingAllPermissions() throws InterruptedException{
		Reporter.log("\n7.Activity log enabled",true);
	
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID ,Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.DisablingOtherPermission();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoOtherPermission", "42Gears@123");
		//usermanagement.RefreshCurrentPage();
	}
	
	@Test(priority='8',description="8. Changepassword enabled")
	public void ActivityLogDisabled() throws InterruptedException{
		Reporter.log("\n8.Activity log Disabled",true);
		usermanagement.IsTestElementPresent();
		Reporter.log("\nActivity log is not visible",true);
	}
	@Test(priority='9',description="9. Changepassword enabled")
	public void PasswordDisabled() throws InterruptedException{
		Reporter.log("\n9.Change password Disabled",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ChangePasswordDisabled();
	}
}
