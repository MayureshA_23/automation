package SanityTestScripts;

import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

public class DeviceGrid extends Initialization {
	@Test(priority = '0', description = "Validation of Notes column in Device Grid.")
	public void VerifyNotesColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log("=====1.Validation of Notes column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Notes");
		deviceGrid.Enable_DisbleColumn("Notes", "Enable");
		commonmethdpage.SearchDevice();
		deviceGrid.ClickOnEditNotes();
		deviceGrid.SelectAllNotesAndEnterNotesgrid();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Notes", "AutomationMDM");
		commonmethdpage.ClearSearchDevice();
		Reporter.log("Pass >>Validation of Notes column in Device Grid.", true);
	}

	@Test(priority = '1', description = "Verify Basic and Advance search")
	public void BasicSearchInDeviceGrid()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		Reporter.log("\n=====2.Verify Basic and Advance search=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAvailable(Config.DeviceName);
		commonmethdpage.BasicSearch(Config.DeviceModel);
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.DeviceModel, "Platform / Model");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Network Operator");
		deviceGrid.Enable_DisbleColumn("Network Operator", "Enable");
		commonmethdpage.BasicSearch(Config.Network);
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.Network, "Network Operator");
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("IMEI");
		deviceGrid.Enable_DisbleColumn("IMEI", "Enable");
		commonmethdpage.BasicSearch(Config.IMEINumber);
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.IMEINumber, "IMEI");
		commonmethdpage.BasicSearch("AutomationMDM");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("AutomationMDM", "Notes");
		Reporter.log("Pass >>Verify Basic search for the following:", true);
	}

	// ****************************Advance
	// Search************************************************//
	@Test(priority = '2', description = "Validation of IP Address column in Device Grid.")
	public void VerifySortingOfIPAddressColumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n=====3.Validation of IP Address column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("IP Address");
		deviceGrid.Enable_DisbleColumn("IP Address", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("IP Address");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "IP Address");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "IP Address");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("IP Address");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IP Address");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceIPAddress", Config.IPAddressOfEnrolledDevice);
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IP Address");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.IPAddressOfEnrolledDevice, "IP Address");
		deviceGrid.clearColValue("DeviceIPAddress");
		Reporter.log("Pass >>Validation of IP Address column in Device Grid.", true);
	}

	@Test(priority = '3', description = "Validation of CPU Usage column in Device Grid.")
	public void VerifySortingOfCPUColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n=====4.Validation of Phone Number column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("CPU Usage");
		deviceGrid.Enable_DisbleColumn("CPU Usage", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("CPU Usage");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("CPU Usage");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("CPU Usage");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "CPU Usage");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "CPU Usage");
		deviceGrid.SelectSearchOption("Advanced Search");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", "<90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<", 90, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", ">80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">", 80, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", ">=80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">=", 80, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", "<=80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=<", 80, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", "=80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=", 80, "CPU Usage");
		deviceGrid.clearColValue("CpuUsage");
		Reporter.log("Pass >>Validation of CPU Usage column in Device Grid.", true);
	}

	@Test(priority = '4', description = "Validation of Device Time Zone column in Device Grid.")
	public void VerifySortingOfTimeZoneColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n=====5.Validation of Device Time Zone column in Device Grid.=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device Time Zone");
		deviceGrid.Enable_DisbleColumn("Device Time Zone", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Device Time Zone");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Device Time Zone");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Device Time Zone");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Device Time Zone");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Device Time Zone");
		deviceGrid.EnterValueForAdvanceSeach("Asia", "DeviceTimeZone");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Asia", "Device Time Zone");
		deviceGrid.clearColValue("DeviceTimeZone");
		Reporter.log("Pass >>Validation of Device Time Zone column in Device Grid.", true);
	}

	@Test(priority = '5', description = "Validation of KNOX Status column in Device Grid")
	public void VerifyKNOXStatus() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n=====6.Validation of KNOX Status column in Device Grid=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("KNOX Status");
		deviceGrid.Enable_DisbleColumn("KNOX Status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("KNOX Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "KNOX Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "KNOX Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("KNOX Status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No", "KnoxStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("No", "KNOX Status");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes", "KnoxStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Yes", "KNOX Status");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "KnoxStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "KNOX Status");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "KnoxStatus", "KNOX Status", 2);
		Reporter.log("Pass >>Validation of KNOX Status column in Device Grid", true);
	}

	@Test(priority = '6', description = "Validation of Network Type column in Device Grid.")
	public void VerifyNetworkType() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log("\n=====7.Validation of Device Time Zone column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Network Type");
		deviceGrid.Enable_DisbleColumn("Network Type", "Enable");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Network Type");
		deviceGrid.EnterValueForAdvanceSeach("Wi-Fi", "NetworkType");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Wi-Fi", "Network Type");
		deviceGrid.clearColValue("NetworkType");
		Reporter.log("Pass >>Validation of Network Type column in Device Grid.", true);
	}

	@Test(priority = '7', description = "Validation of Bluetooth SSID column in Device Grid.")
	public void VerifyBlueToothSSID() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n=====8.Validation of Bluetooth SSID column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Bluetooth SSID");
		deviceGrid.Enable_DisbleColumn("Bluetooth SSID", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Bluetooth SSID");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Bluetooth SSID");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Bluetooth SSID");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Bluetooth SSID");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth SSID");
		deviceGrid.EnterValueForAdvanceSeach(Config.BluetoothSSID, "BSSID");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.BluetoothSSID, "Bluetooth SSID");
		deviceGrid.clearColValue("BSSID");
		Reporter.log("Pass >>Validation of Bluetooth SSID column in Device Grid.", true);
	}

	@Test(priority = '8', description = "Validation of USB Debugging column in Device Grid.")
	public void VerifyUSBDebugging() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n=====9.Validation of USB Debugging column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("USB Debugging");
		deviceGrid.Enable_DisbleColumn("USB Debugging", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("USB Debugging");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "USB Debugging");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "USB Debugging");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("USB Debugging");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Enabled", "ADBEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Enabled", "USB Debugging");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Disabled", "ADBEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Disabled", "USB Debugging");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "ADBEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "USB Debugging");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "ADBEnable", "USB Debugging", 2);
		Reporter.log("Pass >>Validation of USB Debugging column in Device Grid..", true);
	}

	@Test(priority = '9', description = "Validation of Polling Mechanism column in Device Grid.")
	public void VerifyPolling() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n=====10.Validation of Polling Mechanism column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Polling Mechanism");
		deviceGrid.Enable_DisbleColumn("Polling Mechanism", "Enable");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Polling Mechanism", Config.Polling);
		commonmethdpage.ClearSearchDevice();
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Periodic Polling", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Periodic Polling", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("APNS", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("APNS", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("WNS", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("WNS", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Normal Polling", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Normal Polling", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("GCM", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("GCM", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("FCM", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("FCM", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "NixPollingType", "Polling Mechanism", 6);
		Reporter.log("Pass >>Validation of Polling Mechanism column in Device Grid.", true);
	}

	@Test(priority = 'A', description = "Validation of Android Enterprise Status column in Device Grid.")
	public void VerifyAndroidEnterprise() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException, AWTException {
		Reporter.log("\n=====11.Validation of Android Enterprise Status column in Device Grid.=====", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Android Enterprise");
		deviceGrid.Enable_DisbleColumn("Android Enterprise", "Enable");
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Android Enterprise",
				Config.AndroidEnterpriseGrid);
		commonmethdpage.ClearSearchDevice();
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Not Supported", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Not Supported", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Not Enrolled", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Not Enrolled", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Managed Device", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Managed Device", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Profile Owner", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Profile Owner", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Device Owner", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Device Owner", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "AfwProfile", "Android Enterprise", 6);
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Polling Mechanism column in Device Grid.", true);
	}
	//***************************************Blocked**************************************************************
	@Test(priority='B',description="Validation of Root Status column in Device Grid.") 
		public void VerifyRootStatus() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException, AWTException {
		Reporter.log("\n=====12.Validation of Root Status column in Device Grid.=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Root Status");
		deviceGrid.Enable_DisbleColumn("Root Status","Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Root Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2","Root Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1","Root Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Root Status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No","RootStatus",0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("No","Root Status");
		
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes","RootStatus",0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Yes","Root Status");
		
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Signed","RootStatus",0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Signed","Root Status");
		
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Granted","RootStatus",0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Granted","Root Status");
		
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A","RootStatus",0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("N/A","Root Status");
		
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "RootStatus","Root Status",5);
		Reporter.log("Validation of Root Status column in Device Grid.",true);
	}
	@Test(priority='C',description="Validation of Serial Number column in Device Grid.") 
	public void VerifySortingOfSerialNumberColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, ParseException {
		Reporter.log("\n=====13.Validation of Serial Number column in Device Grid.=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Serial Number");
		deviceGrid.Enable_DisbleColumn("Serial Number","Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Serial Number");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Serial Number");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2","Serial Number");
		deviceGrid.selectPageAndVerifySortingWith_NA("1","Serial Number");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Serial Number");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Serial Number");
		deviceGrid.EnterValueForAdvanceSeach(Config.SerialNumber,"SerialNumber");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.SerialNumber,"Serial Number");
		deviceGrid.ClearAdvanceSeach("SerialNumber");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Serial Number column in Device Grid.",true);
	}
	@Test(priority='D',description="Validation of Battery column in Device Grid.") 
	public void VerifySortingOfBatteryColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n=====14.Validation of Battery column in Device Grid.=====",true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Battery");
		deviceGrid.Enable_DisbleColumn("Battery","Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Battery");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Battery");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("Battery");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2","Battery");
		deviceGrid.selectPageAndVerifySortingWith_NA("1","Battery");
		deviceGrid.SelectSearchOption("Advanced Search");
		
		deviceGrid.EnterValueInAdvanceSearchColumn("Battery","<90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<",90,"Battery");
		
		deviceGrid.EnterValueInAdvanceSearchColumn("Battery",">90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">",90,"Battery");
		
		deviceGrid.EnterValueInAdvanceSearchColumn("Battery",">=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">=",90,"Battery");
		
		//deviceGrid.EnterValueInAdvanceSearchColumn("Battery","<=90");
		//deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=<",90,"Battery");
		
		deviceGrid.EnterValueInAdvanceSearchColumn("Battery","=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=",90,"Battery");
		deviceGrid.clearColValue("Battery");
		Reporter.log("Pass >>Validation of Battery column in Device Grid.",true);
	}
	
	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			loginPage.logoutfromApp();
			loginPage.loginToAPP();
		}
		commonmethdpage.HardWait(2);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.RefreshDeviceGrid();
		commonmethdpage.HardWait(2);
	}
}
