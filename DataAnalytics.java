package Settings_TestScripts;

import java.io.IOException;

import javax.security.auth.login.AccountNotFoundException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;


public class DataAnalytics extends Initialization
{
	//Data Ana and SureLock Ana must be disabled in account settings Data Ana and Click On Apply .Check in job qeueu there should not be any job present
	//SureLock  Should Be Licensed and kept launched
	//And SureFox Should Be Licensed
	
	/*
	 * @Test(priority=1,
	 * description="Verify Enable SureLock analytics in account settings") public
	 * void VerifyEnableSureLockAnalyticsUI() throws InterruptedException,
	 * IOException, EncryptedDocumentException, InvalidFormatException {
	 * commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox();
	 * accountsettingspage.DisableSureLockAnalyticsCheckBox();
	 * accountsettingspage.ClickOnDataAnalyticsApplyButton();
	 * commonmethdpage.ClickOnHomePage(); //
	 * commonmethdpage.SearchDevice(Config.DeviceName); //
	 * androidJOB.CheckStatusOfappliedInstalledJob("Disable SureLock Analytics",360)
	 * ;
	 * 
	 * }
	 * 
	 * @Test(priority=2,
	 * description="Verify Enable SureLock analytics in account settings") public
	 * void VerifyEnableSureLockAnalytics() throws InterruptedException,
	 * IOException, EncryptedDocumentException, InvalidFormatException {
	 * commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox(); //
	 * accountsettingspage.DisableSureLockAnalyticsCheckBox();
	 * accountsettingspage.ClickOnDataAnalyticsApplyButton();
	 * accountsettingspage.EnablingSureLockAnalyticsChckBox();
	 * accountsettingspage.ClickOnDataAnalyticsApplyButton();
	 * accountsettingspage.VerifySureLockAnalyticsEnablePopUp();
	 * accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * androidJOB.CheckStatusOfappliedInstalledJob("Enable SureLock Analytics",360);
	 * //Check analytics //
	 * accountsettingspage.ReadingConsoleMessageForJobDeployment("Check analytics"
	 * ,Config.DeviceName);
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock",
	 * "com.gears42.surelock.HomeScreen");
	 * 
	 * // commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock",
	 * "com.gears42.surelock.ClearDefaultsActivity");
	 * accountsettingspage.EnteringIntoSureLock();
	 * accountsettingspage.ClickOnSureLockSettings();
	 * accountsettingspage.ClickOnSureLockAnalytics("SureLock Analytics");
	 * accountsettingspage.VerifyEnableAppAnalyticsCheckBixEnabled();
	 * commonmethdpage.ClickOnAppiumText("SureLock Analytics");
	 * accountsettingspage.ClickOnEnableAppAnalyticsPopUpOkButton();
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * commonmethdpage.ClickOnAppiumButtons("Done"); //
	 * accountsettingspage.ClickOnExitSureLockButton("Ends Lockdown Mode"); //
	 * accountsettingspage.ClickOnExitPopUpButtonInSureLock("EXIT");
	 * 
	 * }
	 * 
	 * @Test(priority=3,description="Verify Secret key for SureLock Analytics")
	 * public void VerifySecretKeyInSureLockAnalytics() throws InterruptedException
	 * { commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox();
	 * accountsettingspage.ClickOnSureLockSecretKeyShowButton();
	 * accountsettingspage.ReadingSureLockSecretKeyFromConsole();
	 * commonmethdpage.ClickOnHomePage(); }
	 * 
	 * @Test(priority=4,description="Verify Secret key is unique for every account")
	 * public void VerifySecretKeyForDifferentAccount() {
	 * accountsettingspage.VerifySureLockSecretKeyForDifferentAcc(); }
	 * 
	 * @Test(priority=5,description="Verify Secret key on Surelock") public void
	 * VerifySecretKeyInSureLock() throws InterruptedException,
	 * EncryptedDocumentException, InvalidFormatException, IOException {
	 * commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.ClickOnSureLockSecretKeyShowButton();
	 * accountsettingspage.ReadingSureLockSecretKeyFromConsole();
	 * 
	 * 
	 * androidJOB.clickOnJobs(); androidJOB.clickNewJob();
	 * androidJOB.clickOnAndroidOS(); runScript.ClickOnRunscript();
	 * accountsettingspage.SearchingRunScripts(Config.EnableSureLockRunScript);
	 * accountsettingspage.ClickOnSearchedRunScript();
	 * runScript.sendPackageNameORpath("com.gears42.surelock,"+accountsettingspage.
	 * SureLockSecretKey+""); runScript.ClickOnValidateButton();
	 * runScript.ScriptValidatedMessage(); runScript.ClickOnInsertButton();
	 * runScript.RunScriptName("Enable Sure Lock Data Analytics");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Enable Sure Lock Data Analytics");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.JobInitiatedMessage();
	 * androidJOB.CheckStatusOfappliedInstalledJob("Enable Sure Lock Data Analytics"
	 * ,400); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Enable Sure Lock Data Analytics"
	 * ,Config.DeviceName);
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock",
	 * "com.gears42.surelock.HomeScreen");
	 * accountsettingspage.EnteringIntoSureLock();
	 * accountsettingspage.ClickOnSureLockSettings();
	 * accountsettingspage.ClickOnSureLockAnalytics("SureLock Analytics");
	 * accountsettingspage.ClickOnEnableScheduleExportInSureLock();
	 * accountsettingspage.ReadingSecretKeyFromDevice();
	 * accountsettingspage.clickonEnableScheduleReportPpopUpOkButton();
	 * accountsettingspage.addingApplicationToSureLockHomeScreen("com.nix");
	 * accountsettingspage.addingApplicationToSureLockHomeScreen(
	 * "com.android.chrome");
	 * accountsettingspage.addingApplicationToSureLockHomeScreen(
	 * "com.android.vending"); commonmethdpage.ClickOnAppiumButtons("Done");
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * accountsettingspage.LaunchingShromeInsideSureLock();
	 * accountsettingspage.LaunchingPlaystoreInsideSureLock();
	 * accountsettingspage.launchingNixInsideSureLock(); androidJOB.clickOnJobs();
	 * androidJOB.clickNewJob(); androidJOB.clickOnAndroidOS();
	 * runScript.ClickOnRunscript();
	 * accountsettingspage.sendingRunScript(Config.RunScript);
	 * runScript.RunScriptName("Export Sure Lock Data Analytics Automation");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Export Sure Lock Data Analytics Automation");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.JobInitiatedMessage();
	 * androidJOB.
	 * CheckStatusOfappliedInstalledJob("Export Sure Lock Data Analytics Automation"
	 * ,400); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Export Sure Lock Data Analytics Automation"
	 * ,Config.DeviceName); reportsPage.ClickOnReports_Button();
	 * customReports.ClickOnCustomReports();
	 * customReports.ClickOnAddButtonCustomReport();
	 * customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.
	 * AnalyticsReportName,Config.AnalyticsReportDescription);
	 * accountsettingspage.checkingSureLockAnalyticsTableInReports();
	 * accountsettingspage.ClickOnAddSelectedTable();
	 * accountsettingspage.ClickOnCustomReportSaveButton();
	 * accountsettingspage.ClickOnOnDemandReport();
	 * accountsettingspage.SearchingForReportInOnDemandReport(Config.
	 * AnalyticsReportName);
	 * accountsettingspage.ClickOnSelectDeviceButtonInReports();
	 * accountsettingspage.SearchingForDeviceInOnDemandReport(Config.DeviceName);
	 * accountsettingspage.ClickOnAddDeviceButton();
	 * accountsettingspage.ClickONRequestReportButton();
	 * accountsettingspage.ClickOnViewReportButton();
	 * accountsettingspage.ClickOnRefreshInViewReport();
	 * accountsettingspage.ClickOnView(Config.AnalyticsReportName);
	 * accountsettingspage.WindowHandle();
	 * accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.
	 * DeviceName);
	 * accountsettingspage.SearchUsedApplicationORWebSiteInReport("SureMDM Nix");
	 * accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.nix");
	 * accountsettingspage.VerifyApplicationName("SureMDM Nix");
	 * accountsettingspage.VerifyApplicationEndTime();
	 * accountsettingspage.VerifyApplicationTotalTime();
	 * accountsettingspage.SearchUsedApplicationORWebSiteInReport("Play Store");
	 * accountsettingspage.VerifyPackageNameInSureLockAnalytics(
	 * "com.android.vending");
	 * accountsettingspage.VerifyApplicationName("Play Store");
	 * accountsettingspage.VerifyApplicationEndTime();
	 * accountsettingspage.VerifyApplicationTotalTime();
	 * accountsettingspage.SearchUsedApplicationORWebSiteInReport("Chrome");
	 * accountsettingspage.VerifyPackageNameInSureLockAnalytics("com.android.chrome"
	 * ); accountsettingspage.VerifyApplicationName("Chrome");
	 * accountsettingspage.VerifyApplicationEndTime();
	 * accountsettingspage.VerifyApplicationTotalTime();
	 * accountsettingspage.SwitchBackWindow(); commonmethdpage.ClickOnHomePage(); }
	 * 
	 * @Test(priority=6,
	 * description="Verify Disable SureLock analytics in account settings") public
	 * void VerifyDisableSurelockAnalytics() throws InterruptedException,
	 * IOException, EncryptedDocumentException, InvalidFormatException {
	 * commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.DisableSureLockAnalyticsCheckBox();
	 * accountsettingspage.ClickOnDataAnalyticsApplyButton();
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * 
	 * androidJOB.CheckStatusOfappliedInstalledJob("Disable SureLock Analytics",800)
	 * ; //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment(" Disable SureLock Analytics",Config.
	 * DeviceName);
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surelock",
	 * "com.gears42.surelock.HomeScreen");
	 * accountsettingspage.EnteringIntoSureLock();
	 * accountsettingspage.ClickOnSureLockSettings();
	 * accountsettingspage.ClickOnSureLockAnalytics("SureLock Analytics");
	 * accountsettingspage.VerifyEnableScheduleExportIsDisabled();
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * accountsettingspage.ClickOnExitSureLockButton("Ends Lockdown Mode");
	 * accountsettingspage.ClickOnExitPopUpButtonInSureLock("EXIT"); }
	 * 
	 * 
	 * //SureFox Data Analytics
	 * 
	 * @Test(priority=7,description="Verify Enabling of Data Analytics") public void
	 * VerifyEnablingDataAnalyticsForSureFox() throws InterruptedException,
	 * EncryptedDocumentException, InvalidFormatException, IOException {
	 * androidJOB.LaunchSureFox();
	 * 
	 * commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox();
	 * accountsettingspage.DisablingSureFoxAnalytics();
	 * accountsettingspage.EnablingSureFoxAnalytics();
	 * accountsettingspage.ClickOnDataAnalyticsApplyButton();
	 * accountsettingspage.VerifySureFoxAnalyticsEnablePopup();
	 * accountsettingspage.ClcikOnSureLockAnalyticsEnablePopUpYesButton();
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * androidJOB.CheckStatusOfappliedInstalledJob("Enable SureFox Analytics",800);
	 * //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Enable SureFox Analytics",Config.
	 * DeviceName);
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * accountsettingspage.EnteringIntoSureFox();
	 * accountsettingspage.SearchingInsideSureFox("SureFox Analytics");
	 * accountsettingspage.VerifyScheduleExportIsEnabledAfterEnablingSFDataAnalytics
	 * (); commonmethdpage.ClickOnAppiumButtons("Done");
	 * commonmethdpage.commonScrollMethod("Exit SureFox");
	 * accountsettingspage.ExitFromSureLock();
	 * accountsettingspage.ClickOnExitPopUpButtonInSureLock("EXIT");
	 * //PendingDelete.ClickOnExitPopUpButtonInSureLock(); }
	 * 
	 * @Test(priority=8,
	 * description="Verify Enabling of SureFox analytics by applying runscript.")
	 * public void VerifyEnablingSFAnalyticsByRunScript() throws
	 * InterruptedException, EncryptedDocumentException, InvalidFormatException,
	 * IOException { androidJOB.LaunchSureFox();
	 * 
	 * commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox();
	 * accountsettingspage.ClickOnSureFoxSecretKeyShowButton();
	 * accountsettingspage.ReadingSureFoxSecretKeyFromConsole();
	 * commonmethdpage.ClickOnHomePage(); androidJOB.clickOnJobs();
	 * androidJOB.clickNewJob(); androidJOB.clickOnAndroidOS();
	 * runScript.ClickOnRunscript();
	 * accountsettingspage.SearchingRunScripts(Config.EnableSureLockRunScript);
	 * accountsettingspage.ClickOnSearchedRunScript();
	 * runScript.sendPackageNameORpath("com.gears42.surefox,"+accountsettingspage.
	 * SureFoxSecretKey+""); runScript.ClickOnValidateButton();
	 * runScript.ScriptValidatedMessage(); runScript.ClickOnInsertButton();
	 * runScript.RunScriptName("Enable Sure Fox Data Analytics");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Enable Sure Fox Data Analytics");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.JobInitiatedMessage();
	 * androidJOB.CheckStatusOfappliedInstalledJob("Enable Sure Fox Data Analytics"
	 * ,800); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Enable Sure Fox Data Analytics",Config
	 * .DeviceName); //PendingDelete.AppiumConfigurationForSureFox(); //
	 * PendingDelete.EnteringIntoSureFox();
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * accountsettingspage.EnteringIntoSureFox();
	 * accountsettingspage.SearchingInsideSureFox("SureFox Analytics");
	 * accountsettingspage.VerifySureFoxAnalyticsCheckBox();
	 * commonmethdpage.ClickOnAppiumButtons("Done"); }
	 * 
	 * @Test(priority=9,
	 * description="Run script-Apply script to export analytics and verify custom report for SureFox Data analytics"
	 * ) public void VerifyCustomReportforSFAnalytics() throws InterruptedException,
	 * EncryptedDocumentException, InvalidFormatException, IOException {
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * 
	 * accountsettingspage.EnteringIntoSureFox();
	 * accountsettingspage.ClickOnAllowedWebsites();
	 * accountsettingspage.ClicOnAddURLButton();
	 * accountsettingspage.SendingWebSites("www.Google.com");
	 * commonmethdpage.commonScrollMethod("Done");
	 * accountsettingspage.ClickOnDoneButtonOnAddingURL("Done");
	 * accountsettingspage.ClicOnAddURLButton();
	 * accountsettingspage.SendingWebSites("www.Flipkart.com");
	 * commonmethdpage.commonScrollMethod("Done");
	 * accountsettingspage.ClickOnDoneButtonOnAddingURL("Done");
	 * accountsettingspage.ClickOnDoneButtonOnFinishingAddURL();
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * accountsettingspage.OpeningWebSitesInSureFox("Google");
	 * accountsettingspage.OpeningWebSitesInSureFox("Flipkart");
	 * androidJOB.clickOnJobs(); androidJOB.clickNewJob();
	 * androidJOB.clickOnAndroidOS(); runScript.ClickOnRunscript();
	 * accountsettingspage.sendingRunScript(Config.RunscriptToExportSFAnalytics);
	 * runScript.RunScriptName("Export Sure Fox Data Analytics Automation");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Export Sure Fox Data Analytics Automation");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.
	 * CheckStatusOfappliedInstalledJob("Export Sure Fox Data Analytics Automation"
	 * ,400); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Export Sure Fox Data Analytics Automation"
	 * ,Config.DeviceName); reportsPage.ClickOnReports_Button();
	 * customReports.ClickOnCustomReports();
	 * customReports.ClickOnAddButtonCustomReport();
	 * customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.
	 * SFAnalyticsReportName,Config.SFAnalyticsReportDescription);
	 * accountsettingspage.checkingSureFoxAnalyticsTableInReports();
	 * accountsettingspage.ClickOnAddSelectedTable();
	 * accountsettingspage.ClickOnCustomReportSaveButton();
	 * accountsettingspage.ClickOnOnDemandReport();
	 * accountsettingspage.SearchingForReportInOnDemandReport(Config.
	 * SFAnalyticsReportName);
	 * accountsettingspage.ClickOnSelectDeviceButtonInReports();
	 * accountsettingspage.SearchingForDeviceInOnDemandReport(Config.DeviceName);
	 * accountsettingspage.ClickOnAddDeviceButton();
	 * accountsettingspage.ClickONRequestReportButton();
	 * accountsettingspage.ClickOnViewReportButton();
	 * accountsettingspage.ClickOnRefreshInViewReport();
	 * accountsettingspage.ClickOnView(Config.SFAnalyticsReportName);
	 * accountsettingspage.WindowHandle();
	 * accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.
	 * DeviceName); //
	 * accountsettingspage.SearchUsedApplicationORWebSiteInReport("Flipkart"); //
	 * accountsettingspage.VerifyURLInSFAnalyticsReport("https://www.flipkart.com/")
	 * ; // accountsettingspage.VerifyWebSiteOpenedTimeInSFAnalyticsReport(); //
	 * accountsettingspage.VerifyWebsiteCloseTime(); //
	 * accountsettingspage.VeirfyWebsiteBrowsedTime();
	 * accountsettingspage.SearchUsedApplicationORWebSiteInReport("Google");
	 * accountsettingspage.VerifyURLInSFAnalyticsReport(
	 * "https://www.google.com/?gws_rd=ssl");
	 * accountsettingspage.VerifyWebSiteOpenedTimeInSFAnalyticsReport();
	 * accountsettingspage.VerifyWebsiteCloseTime();
	 * accountsettingspage.VeirfyWebsiteBrowsedTime();
	 * accountsettingspage.SwitchBackWindow(); commonmethdpage.ClickOnHomePage(); }
	 * 
	 * @Test(priority='A',description="Run script-Disable Surefox Analytics") public
	 * void VerifyDisableSFAnalyticsRunScript() throws InterruptedException,
	 * EncryptedDocumentException, InvalidFormatException, IOException {
	 * androidJOB.clickOnJobs(); androidJOB.clickNewJob();
	 * androidJOB.clickOnAndroidOS(); runScript.ClickOnRunscript();
	 * accountsettingspage.sendingRunScript(Config.SFAnalyticsDisableRunscript);
	 * runScript.RunScriptName("Disable Sure Fox Data Analytics Automation");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Disable Sure Fox Data Analytics Automation");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.JobInitiatedMessage();
	 * androidJOB.
	 * CheckStatusOfappliedInstalledJob("Disable Sure Fox Data Analytics Automation"
	 * ,400); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Disable Sure Fox Data Analytics Automation"
	 * ,Config.DeviceName);
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * accountsettingspage.EnteringIntoSureFox();
	 * accountsettingspage.SearchingInsideSureFox("SureFox Analytics");
	 * accountsettingspage.
	 * VerifySureFoxAnalyticsCheckBoxAfterApplyingDisableSFAnalyticsRunScript();
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * commonmethdpage.commonScrollMethod("Exit SureFox");
	 * accountsettingspage.ExitFromSureLock();
	 * accountsettingspage.ClickOnExitPopUpButtonInSureLock("EXIT");
	 * 
	 * }
	 * 
	 * // Content Blocking
	 * 
	 * @Test(priority=10,description="verify creation of content blocking") public
	 * void VerifyCreationOfContentBlocking() throws InterruptedException {
	 * commonmethdpage.ClickOnSettings(); commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox();
	 * accountsettingspage.ClickOnAddAnalyticsButton();
	 * accountsettingspage.SendingDataAnalyticsName(Config.AnalyticsName);
	 * accountsettingspage.SendingTagName(Config.AnalyticsTagName);
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("URL");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("Blocked Time");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("Type");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickonAnalyticsSaveButton();
	 * accountsettingspage.VerifyNewlyAddedAnalyticsSecretKey();
	 * accountsettingspage.ClickOnDataAnalyticsApplyButton();
	 * commonmethdpage.ClickOnHomePage();
	 * 
	 * }
	 * 
	 * @Test(priority=11,description="Run Script:Enable Content Blocking") public
	 * void VerifyRunscriptToEnableContentBlocking() throws InterruptedException,
	 * IOException, EncryptedDocumentException, InvalidFormatException {
	 * androidJOB.LaunchSureFox();
	 * 
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * accountsettingspage.EnteringIntoSureFox();
	 * accountsettingspage.SearchingInsideSureFox("Enable Content Blocking");
	 * accountsettingspage.EnablingContentBlockingCheckBoxInSureFox();
	 * accountsettingspage.ClickingOnAddingKeywordToBlockAccess();
	 * accountsettingspage.SendingKeywordsToBlockAccess("Cricket,football");
	 * commonmethdpage.ClickOnAppiumButtons("Done"); androidJOB.clickOnJobs();
	 * androidJOB.clickNewJob(); androidJOB.clickOnAndroidOS();
	 * runScript.ClickOnRunscript();
	 * accountsettingspage.SendingEnableContentBlockingRunScripts();
	 * runScript.RunScriptName("Enable Content Blocking Analytics Automation");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Enable Content Blocking Analytics Automation");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.JobInitiatedMessage();
	 * androidJOB.
	 * CheckStatusOfappliedInstalledJob("Enable Content Blocking Analytics Automation"
	 * ,400); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Enable Content Blocking Analytics Automation"
	 * ,Config.DeviceName);
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * accountsettingspage.EnteringIntoSureFox();
	 * accountsettingspage.SearchingInsideSureFox("Content Blocking Analytics");
	 * accountsettingspage.VerifyingContentBlockingAnalytics(); }
	 * 
	 * @Test(priority='D',
	 * description="Run script-Apply script to export content blocking anlyticsand verify custom report for Data analytics"
	 * ) public void VerifyExportContentBlocking() throws InterruptedException,
	 * IOException, EncryptedDocumentException, InvalidFormatException {
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * accountsettingspage.EnteringIntoSureFox();
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * accountsettingspage.OpeningWebSitesToSearchBlockedContent("Google");
	 * accountsettingspage.SearchingForBlockedContentInGoogle("cricket");
	 * accountsettingspage.OpeningWebSitesToSearchBlockedContent("Google");
	 * accountsettingspage.SearchingForBlockedContentInGoogle("football");
	 * androidJOB.clickOnJobs(); androidJOB.clickNewJob();
	 * androidJOB.clickOnAndroidOS(); runScript.ClickOnRunscript();
	 * accountsettingspage.sendingRunScript(Config.ContentBlockingExportRunscript);
	 * runScript.RunScriptName("Export Content Blocking Analytics Automation");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Export Content Blocking Analytics Automation");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.JobInitiatedMessage();
	 * androidJOB.
	 * CheckStatusOfappliedInstalledJob("Export Content Blocking Analytics Automation"
	 * ,400); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Export Content Blocking Analytics Automation"
	 * ,Config.DeviceName); reportsPage.ClickOnReports_Button();
	 * customReports.ClickOnCustomReports();
	 * customReports.ClickOnAddButtonCustomReport();
	 * customReports.EnterCustomReportsNameDescription_DeviceDetails(Config.
	 * ContentBlockingReportName,Config.ContentBlockingReportDescription);
	 * accountsettingspage.checkingContentBlockingAnalyticsTableInReports();
	 * accountsettingspage.ClickOnAddSelectedTable();
	 * accountsettingspage.ClickOnCustomReportSaveButton();
	 * accountsettingspage.ClickOnOnDemandReport();
	 * accountsettingspage.SearchingForReportInOnDemandReport(Config.
	 * ContentBlockingReportName);
	 * accountsettingspage.ClickOnSelectDeviceButtonInReports();
	 * accountsettingspage.SearchingForDeviceInOnDemandReport(Config.DeviceName);
	 * accountsettingspage.ClickOnAddDeviceButton();
	 * accountsettingspage.ClickONRequestReportButton();
	 * accountsettingspage.ClickOnViewReportButton();
	 * accountsettingspage.ClickOnRefreshInViewReport();
	 * accountsettingspage.ClickOnView(Config.ContentBlockingReportName);
	 * accountsettingspage.WindowHandle();
	 * accountsettingspage.DeviceNameColumnVerificationDeviceHealthReport(Config.
	 * DeviceName);
	 * accountsettingspage.SearchUsedApplicationORWebSiteInReport("cricket");
	 * accountsettingspage.VerifyURLInSFAnalyticsReport("https://www.google.com");
	 * accountsettingspage.VerifyWebSiteOpenedTimeInSFAnalyticsReport();
	 * accountsettingspage.VerifyTypeColumnInContentBlockAnalyticsReport();
	 * accountsettingspage.SearchUsedApplicationORWebSiteInReport("football");
	 * accountsettingspage.VerifyURLInSFAnalyticsReport("https://www.google.com");
	 * accountsettingspage.VerifyWebSiteOpenedTimeInSFAnalyticsReport();
	 * accountsettingspage.VerifyTypeColumnInContentBlockAnalyticsReport();
	 * accountsettingspage.SwitchBackWindow(); commonmethdpage.ClickOnHomePage(); }
	 * 
	 * @Test(priority=12,description="Run Script:Disable Content Blocking") public
	 * void VerifyRunScriptToDisableToContentBlocking() throws InterruptedException,
	 * EncryptedDocumentException, InvalidFormatException, IOException {
	 * androidJOB.clickOnJobs(); androidJOB.clickNewJob();
	 * androidJOB.clickOnAndroidOS(); runScript.ClickOnRunscript();
	 * accountsettingspage.sendingRunScript(Config.
	 * ContentBlockingAnalyticsDisableRunscript);
	 * runScript.RunScriptName("Disable Content Blocking Analytics Automation");
	 * commonmethdpage.ClickOnHomePage();
	 * commonmethdpage.SearchDevice(Config.DeviceName);
	 * commonmethdpage.ClickOnApplyButton();
	 * androidJOB.SearchField("Disable Content Blocking Analytics Automation");
	 * //androidJOB.ClickOnApplyJobOkButton(); androidJOB.
	 * CheckStatusOfappliedInstalledJob("Disable Content Blocking Analytics Automation"
	 * ,800); //accountsettingspage.
	 * ReadingConsoleMessageForJobDeployment("Disable Content Blocking Analytics Automation"
	 * ,Config.DeviceName);
	 * commonmethdpage.AppiumConfigurationCommonMethod("com.gears42.surefox",
	 * "com.gears42.surefox.SurefoxBrowserScreen");
	 * accountsettingspage.EnteringIntoSureFox();
	 * accountsettingspage.SearchingInsideSureFox("Content Blocking Analytics");
	 * accountsettingspage.
	 * VerifyingContentBlockingAnalyticsAfterApplyingDisableRunscript();
	 * commonmethdpage.ClickOnAppiumButtons("Done");
	 * commonmethdpage.commonScrollMethod("Exit SureFox");
	 * accountsettingspage.ExitFromSureLock();
	 * accountsettingspage.ClickOnExitPopUpButtonInSureLock("EXIT"); }
	 * 
	 * @Test(priority=13,
	 * description="Verify adding 3rd party application to the analytics window")
	 * public void VerifyAdding3rdPartyApplicationAnalytics() throws
	 * InterruptedException { commonmethdpage.ClickOnSettings();
	 * commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox();
	 * accountsettingspage.ClickOnAddAnalyticsButton();
	 * accountsettingspage.SendingDataAnalyticsName(Config.BatteryAnalyticsName);
	 * accountsettingspage.SendingTagName("Application Analytics");
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("Cycle Type");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("Start Time");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("Start Level");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("End Time");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("End Level");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("Battery Level Change");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickOnAddFiledButton();
	 * accountsettingspage.SendingFieldName("Change Per Minute");
	 * accountsettingspage.SelectingFieldType("string");
	 * accountsettingspage.ClickingonAddFieldAddButton();
	 * accountsettingspage.ClickonAnalyticsSaveButton();
	 * accountsettingspage.VerifyRemoveButtonForNewlyAddedAnalytics();
	 * accountsettingspage.VerifyNewlyAddedAnalyticsSecretKey(); }
	 * 
	 * @Test(priority='G',description="Enable 3rd party application analytics"
	 * ,dependsOnMethods= {"VerifyAdding3rdPartyApplicationAnalytics"}) public void
	 * VerifyEnabling3rdPartyApplicationAnalytics() throws InterruptedException {
	 * accountsettingspage.EnablingNewlyCreatedAnalytics();
	 * accountsettingspage.ClickOnDataAnalyticsApplyButton();
	 * commonmethdpage.ClickOnHomePage(); reportsPage.ClickOnReports_Button();
	 * customReports.ClickOnCustomReports();
	 * customReports.ClickOnAddButtonCustomReport();
	 * accountsettingspage.CheckingForBatteryAnalyticsReport();
	 * accountsettingspage.ClickOnOnDemandReport();
	 * commonmethdpage.ClickOnHomePage();
	 * 
	 * }
	 * 
	 * @Test(priority=14,description="Disable 3rd party application analytics")
	 * public void VerifyDisabling3rdPartyApplicationAnalytics() throws
	 * InterruptedException { commonmethdpage.ClickOnSettings();
	 * commonmethdpage.ClickonAccsettings();
	 * accountsettingspage.ClickOnDataAnalyticsTab();
	 * accountsettingspage.EnableDataAnalyticsCheckBox();
	 * accountsettingspage.DisablingNewlyCreatedAnalytics(); }
	 */
	 

	@Test(priority=15,description="Verify MTD analytics option under Data Analytics") 
	public void VerifyMTDanalyticsoptionunderDataAnalyticsTC_ST_894() throws Throwable {
		Reporter.log("\n15.Verify MTD analytics option under Data Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();		
		accountsettingspage.MTDAppScanVisible();
		
}
	
	@Test(priority=16,description="Verify Logs when User Adds any Data Analytics") 
	public void VerifyLogswhenUserAddsanyDataAnalyticsATC_ST_621() throws Throwable {
		Reporter.log("\n16.Verify Logs when User Adds any Data Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();
		accountsettingspage.CreateAnalytics();	
		accountsettingspage.SaveAnalyticsbutton();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DataAnalysisCreatedActivityLogs();
}
	
	@Test(priority=17,description="Verify Logs when User Modify any Data Analytics") 
	public void VerifyLogswhenUserModifyanyDataAnalyticsBTC_ST_621() throws Throwable {
		Reporter.log("\n17.Verify Logs when User Modify any Data Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();
		accountsettingspage.ModifyDataAnalytics();
		accountsettingspage.SaveAnalyticsbutton();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DataAnalysisModifyActivityLogs();		
}
	
	@Test(priority=18,description="Verify Logs when User Modify any Data Analytics") 
	public void VerifyLogswhenUserModifyanyDataAnalyticsCTC_ST_621() throws Throwable {
		Reporter.log("\n18.Verify Logs when User Modify any Data Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();
		accountsettingspage.CreateAnalytics();	
		accountsettingspage.SaveAnalyticsbutton();
		accountsettingspage.RemoveAnalyticsButton();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.DataAnalysisDeleteActivityLogs();		
}
	
	
	@Test(priority=19,description="Verify UI of Alert Message Analytics in Data Analytics") 
	public void VerifyUIofAlertMessageAnalyticsinDataAnalyticsTC_ST_772() throws Throwable {
		Reporter.log("\n19.Verify UI of Alert Message Analytics in Data Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();
		accountsettingspage.AlertMessageAnalyticsVisible();	
		accountsettingspage.SecretKeyVisible();
}
	
	
	@Test(priority=20,description="Verify MTD analytics option under Data Analytics with Secret key") 
	public void VerifyMTDanalyticsoptionunderDataAnalyticswithSecretkey() throws Throwable {
		Reporter.log("\n20.Verify MTD analytics option under Data Analytics with Secret key",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();		
		accountsettingspage.MTDAppScanVisible();
		accountsettingspage.MTDAppScanSecretKeyVisible();		
}
	
	@Test(priority=21,description="Verify Secret key for SureLock Analytics") 
	public void VerifySecretkeyforSureLockAnalytics() throws Throwable {
		Reporter.log("\n21.Verify Secret key for SureLock Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();		
		accountsettingspage.SureLockVisible();
		accountsettingspage.SureLockSecretKeyVisible();		
}
	
	@Test(priority=22,description="Verify Secret key for SureFox Analytics") 
	public void VerifySecretkeyforSureFoxAnalytics() throws Throwable {
		Reporter.log("\n22.Verify Secret key for SureFox Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();		
		accountsettingspage.SureFoxVisible();
		accountsettingspage.SureFoxSecretKeyVisible();		
}
	
	@Test(priority=23,description="Verify Secret key for Alert Message Analytics") 
	public void VerifySecretkeyforAlertMessageAnalytics() throws Throwable {
		Reporter.log("\n23.Verify Secret key for Alert Message Analytics",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnDataAnalyticsTab();		
		accountsettingspage.AlertMessageAnalyticsvisible();
		accountsettingspage.AlertMessageAnalyticsSecretKeyVisible();		
}
	
	//vinimanoj 
	
	@Test(priority = 24, description = "Verify UIthingan Alytics in Dataan alytics TC_ST_481")
	public void VerifyUIthinganAlyticsinDataanalyticsTC_ST_481() throws InterruptedException {
		Reporter.log("\n24.Verify alert template feature in Account Settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.checkForAccountIsEnterprise();
		accountsettingspage.clickHome();
	}
	
	
	
	
	
	
	
	
	
	
}
