package PageObjectRepository;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Common.Initialization;
import Library.AssertLib;
import Library.Config;
import Library.Driver;
import Library.WebDriverCommonLib;

public class PendingDelete_POM extends WebDriverCommonLib 
{
	AssertLib ALib=new AssertLib();
	JavascriptExecutor js=(JavascriptExecutor) Initialization.driver;


	@FindBy(id="deletedSection")
	private WebElement PendingDeleteButton;

	@FindBy(id="ddRefreshBtn")
	private WebElement pendingDeleteRefreshButton;

	@FindBy(id="ddDeleteBtn")
	private WebElement PendingForceDeleteButton;

	@FindBy(xpath="//*[@id=\"tableContainer\"]/div[4]/div[1]/div[3]/input")
	private WebElement SearchDeviceinHomeSection;

	@FindBy(xpath="//span[text()='No matching result found.']")
	private WebElement NotificationOnNoDeviceFound;

	@FindBy(xpath=".//*[@id='logContent']/ul/li[1]/p")
	private WebElement FirstLog;

	@FindBy(xpath="//div[@id='ddRefreshBtn']")
	private WebElement PendingDeleteRefreshButton;

	@FindBy(xpath="//input[@id='sfslsvderegistration']")
	private WebElement FreeUpLicenseCheckBox;

	@FindBy(xpath="//p[contains(text(),'Job("+Config.DeactivationJob1+") successfully deployed on "+Config.PendingDeleteDeviceName1+"')]")
	private WebElement SureLockLog;

	@FindBy(xpath="//p[contains(text(),'Job("+Config.DeactivationJob2+") successfully deployed on "+Config.PendingDeleteDeviceName1+"')]")
	private WebElement SureFoxLog;

	@FindBy(xpath="//p[contains(text(),'Job("+Config.DeactivationJob3+") successfully deployed on "+Config.PendingDeleteDeviceName1+"')]")
	private WebElement SureVideoLog;    

	@FindBy(xpath="//span[@id='accountdetail_refreshbtn']")
	private WebElement LicenseRefreshButton;

	public void verifyPendingDeleteButton()
	{

		boolean mark = PendingDeleteButton.isDisplayed();
		String PassStatement="PASS >> PendingDelete is displayed successfully when logged into SureMDM Console";
		String FailStatement="FAIL >> PendingDelete is not displayed successfully when logged into SureMDM Console";
		ALib.AssertTrueMethod(mark, PassStatement, FailStatement);
	}
	public void verifyFreeupSureLockfromDevicewhenItIsDeleted() throws InterruptedException
	{

		boolean flag;


		if(FreeUpLicenseCheckBox.isSelected())
		{
			flag=true;
		}
		else
		{
			FreeUpLicenseCheckBox.click();
			flag=true;
		}
		String PassStatement="PASS >> FreeUp License CheckBox is Selected";
		String FailStatement="FAIL >> FreeUp License CheckBox isn't Selected ";
		ALib.AssertTrueMethod(flag, PassStatement, FailStatement);
		System.out.println("-------------------------------------------------------");
	}

	public void clickOnDeleteDevice() throws InterruptedException
	{
		Initialization.driver.findElement(By.id("deleteDeviceBtn")).click();
		Reporter.log(">>>>>>>Clicked On Delete Device Button",true);
		sleep(2);
		waitForXpathPresent("//*[@id='deviceConfirmationDialog']/div/div/div[1]");
		sleep(5);
	}

	public void ClickingOnDeleteDeviceConfirmationYesButton() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//div[@id='deviceConfirmationDialog']/div/div/div[2]/button[2]")).click();
		Reporter.log(">>>>>>>Clicked on YES",true);
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[3]")).isDisplayed())
		{}
		sleep(7);
	}
	public void ClickOnPendingdelete() throws InterruptedException
	{

		PendingDeleteButton.click();

		waitForVisibilityOf("//div[@id='ddRefreshBtn']");
		sleep(2);
		if(Initialization.driver.findElement(By.xpath("//div[@id='ddRefreshBtn']")).isDisplayed())
		{
			Reporter.log(">>>>>>>Clicked on pendingDelete Button",true);
		}
		else
		{
			Reporter.log("Didn't Clicked on pendingDelete Button",true);
		}
	}
	public void SearchingDeviceInsidePendingDelete(String DeviceName)
	{
		boolean mark;
		Initialization.driver.findElement(By.xpath("//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")).sendKeys(DeviceName);
		try
		{
			mark=Initialization.driver.findElement(By.xpath("//td[text()='"+DeviceName+"']")).isDisplayed();
			Initialization.driver.findElement(By.xpath("//td[text()='"+DeviceName+"']")).click();
			sleep(3);

		}
		catch(Exception e)
		{
			mark=true;
			Reporter.log("Device Not Present Inside Pending Delete");
		}
		String PassStatement="PASS >> Deleted Device is present Inside PendingDelete Section";
		String FailStatement="FAIL >> Deleted Device isn't present Inside PendingDelete Section";
		ALib.AssertTrueMethod(mark, PassStatement, FailStatement);
	}

	public void DeletingDeviceFromPendingDelete() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//td[text()='"+Config.DeleteDeviceName+"']")).click();
		Initialization.driver.findElement(By.xpath("//div[@id='ddDeleteBtn']")).click();
		Reporter.log(">>>>>>>Clicked On Force Delete Button",true);
		waitForXpathPresent("//*[@id='ConfirmationDialog']/div/div/div[1]/p");
		sleep(2); 
		Initialization.driver.findElement(By.xpath("//*[@id='ConfirmationDialog']/div/div/div[2]/button[2]")).click();
		Reporter.log(">>>>>>>Clicked On Yes",true);
		while (Initialization.driver.findElement(By.xpath("(//div[@class='loader'])[1]")).isDisplayed())
		{}
	}


	public void SearchingDeletedDeviceInsideHome() throws InterruptedException 
	{
		SearchDeviceinHomeSection.clear();
		sleep(3);
		SearchDeviceinHomeSection.sendKeys(Config.DeleteDeviceName);
		boolean value;
		sleep(3);
		waitForXpathPresent("//span[text()='No matching result found.']");
		try
		{
			NotificationOnNoDeviceFound.isDisplayed();
			value=true;
		}
		catch(Exception e)
		{
			value=false;
		}
		String passStatement="PASS >> Deleted Device is not present Inside Home group";
		String failStatement="FAIL >> Deleted Device is present Inside Home group";
		ALib.AssertTrueMethod(value, passStatement, failStatement);         
	}    

	public int LicenseCountBeforeDeletingDevice;
	public void ReadingLicenseCount() throws InterruptedException
	{

		String DeviceLicenseText = Initialization.driver.findElement(By.xpath("//span[@id='devicesused']")).getText();
		String[] DeviceLicenseTextElements  = DeviceLicenseText.split(" ");
		String element=DeviceLicenseTextElements[0];
		LicenseCountBeforeDeletingDevice = Integer.parseInt(element);
		System.out.println("License Count Is "+" : "+LicenseCountBeforeDeletingDevice);

	}    

	public void waitForLogXpathPresent(String wbXpath){
		WebDriverWait wait = new WebDriverWait(Driver.driver,300);
		wait.until(ExpectedConditions.
				presenceOfAllElementsLocatedBy(By.xpath(wbXpath)));		
	}
	public boolean LogVariable;

	public void ReadingLogsInConsole(String DeviceName) throws InterruptedException
	{
		try
		{
			try
			{
				waitForLogXpathPresent("//p[contains(text(),'Job("+Config.DeactivationJob1+") successfully deployed on "+DeviceName+"')]");
				String SurelOckLog=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Job("+Config.DeactivationJob1+") successfully deployed on "+DeviceName+"')]")).getText();
				boolean DeactivationOfSureLock = Initialization.driver.findElement(By.xpath("//p[contains(text(),'Job("+Config.DeactivationJob1+") successfully deployed on "+DeviceName+"')]")).isDisplayed();
				if(DeactivationOfSureLock)
				{
					Reporter.log(SurelOckLog,true);
					Reporter.log("Deactivation Of SureLock License Is Displayed",true);
					LogVariable=true;
				}
			}
			catch(Exception e)
			{
				LogVariable=false;
			}
			try
			{
				waitForLogXpathPresent("//p[contains(text(),'Job("+Config.DeactivationJob2+") successfully deployed on "+DeviceName+"')]");
				String SureFoxLog=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Job("+Config.DeactivationJob2+") successfully deployed on "+DeviceName+"')]")).getText();
				boolean DeactivationOfSureFox=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Job("+Config.DeactivationJob2+") successfully deployed on "+DeviceName+"')]")).isDisplayed();
				if(DeactivationOfSureFox)
				{
					Reporter.log(SureFoxLog,true);
					Reporter.log("Deactivation Of SureFox License Is Displayed",true);
					LogVariable=true;
				}

			}
			catch(Exception e)
			{
				LogVariable=false;
			}

			try
			{
				waitForLogXpathPresent("//p[contains(text(),'Job("+Config.DeactivationJob3+") successfully deployed on "+DeviceName+"')]");
				String SureFoxLog=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Job("+Config.DeactivationJob2+") successfully deployed on "+DeviceName+"')]")).getText();
				boolean DeactivationOfSureFox=Initialization.driver.findElement(By.xpath("//p[contains(text(),'Job("+Config.DeactivationJob3+") successfully deployed on "+DeviceName+"')]")).isDisplayed();
				if(DeactivationOfSureFox)
				{
					Reporter.log(SureFoxLog,true);
					Reporter.log("Deactivation Of SureVideo License Is Displayed",true);
					LogVariable=true;
				}
			}
			catch(Exception e)
			{
				LogVariable=false;
			}
		}

		catch(Exception e)
		{
			Reporter.log("Deactivation Messages are Not Displayed On Deleting Online Device",true);
			LogVariable=false;
		}
		String Pass="Deavtivation Messages Are Displayed successfully";
		String Fail="Deactivation Messages Are Not Diplayed";
		ALib.AssertTrueMethod(LogVariable, Pass,Fail);
	}

	public void VerifyingLicenseCountAfterDeletingOnlineDevice() throws InterruptedException
	{

		boolean flag;
		LicenseRefreshButton.click();
		sleep(4);
		String DeviceLicenseText = Initialization.driver.findElement(By.xpath("//span[@id='devicesused']")).getText();
		String[] DeviceLicenseTextElements  = DeviceLicenseText.split(" ");
		for(int i=0;i<1;i++)
		{
			String element = DeviceLicenseTextElements[i];
			int LicenseCountAfterDeleting = Integer.parseInt(element);
			System.out.println("License Count Is "+" : "+LicenseCountAfterDeleting);
			if(LicenseCountAfterDeleting==LicenseCountBeforeDeletingDevice-1)
			{
				flag=true;
			}
			else
			{
				flag=false;
			}
			String Pass="PASS>>> License Count Is Reduced After Deleting Device";
			String Fail="FAIL>>> License Count Is not Reduced After Deleting Device";
			ALib.AssertTrueMethod(flag, Pass,Fail);
		}
	}

	public void WaitingForDeviceDeletionFromPendingDelete(String DeviceName) throws InterruptedException
	{
		PendingDeleteRefreshButton.click();
		Initialization.driver.findElement(By.xpath("//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")).sendKeys(DeviceName);
		PendingDeleteRefreshButton.click();
		waitForXpathPresent("//div[contains(text(),'No Device available')]");

		/*try
    	{
    		Initialization.driver.findElement(By.xpath("//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")).clear();
    		Initialization.driver.findElement(By.xpath("//*[@id='panelBackgroundDevice']/div[2]/div[1]/div/input")).sendKeys(DeviceName);
    		sleep(3);
    	while(Initialization.driver.findElement(By.xpath("//td[text()='"+DeviceName+"']")).isDisplayed())
    	{
    		System.out.println("While");
           Reporter.log("Device Is Still Present Inside Pending Delete",true);
           PendingDeleteRefreshButton.click();
           sleep(4);
    	}
    	}
    	catch(Exception e)
    	{
    		Reporter.log("Device Is Not Present Inside Pending Delete",true);
    	}*/
	}

	public void UncheckingFreeUpSLSVSFLicenseCheckBox()
	{  	 
		if(FreeUpLicenseCheckBox.isSelected())
		{
			FreeUpLicenseCheckBox.click();
			Reporter.log("Free Up Check Box Is Unchecked",true);
		}
		else
		{
			Reporter.log("Free Up Check Box Is Already Unchecked",true);
		}

	}

	public void VerifyingPendingDeleteButtonAferUncheckingFreeUpLicenseCheckBox()
	{
		boolean flag;
		try
		{
			flag=PendingDeleteButton.isDisplayed();

		}
		catch(Exception e)
		{
			flag=true;
		}

		String Pass="Pending Delete Button Is Not Displayed After UNchecking Free Up License Check Box";
		String Fail="Pending Delete Button Is Displayed Even After UnChecking Free Up License Check Box";
		ALib.AssertFalseMethod(flag, Pass, Fail);
	}

	public void WaitingForLogUpdates() throws InterruptedException
	{
		Reporter.log("Waiting For Log Updates",true);
		sleep(140);
	}

	public void VerifyLogsUpdateAfterUncheckingFreeUpLicesnseCheckBox()
	{
		boolean flag;
		try
		{
			if(SureLockLog.isDisplayed()||SureFoxLog.isDisplayed()||SureVideoLog.isDisplayed())
			{
				flag=false;
			}
			else
			{
				flag=true;
			}
		}
		catch (Exception e) 
		{
			flag=true;
		}

		String Pass="PASS >>>SL,SV,SF Logs Are Not Updated When FreeUp License Is Disabled";
		String Fail="FAIL >>>SL,SV,SF Logs Are  Updated When FreeUp License Is Disabled";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}

	

	

	public void VerifyingSLSFLicenseIsDeletedInDeviceSide()
	{
		boolean flag;
		try
		{
			flag=Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Skip']").isDisplayed();
		}
		catch(Exception e)
		{
			flag=false;
		}
		String Pass="Sure Lock/SureFox Licesne Is Deleted In Device After Deleting The Device From Console";
		String Fail="Sure Lock/SureFox License Is Not Deleter In Device";
		ALib.AssertTrueMethod(flag, Pass, Fail);
	}
	public void ClickOnSkipButtonInSure() throws InterruptedException
	{
		Initialization.driverAppium.findElementByXPath("//android.widget.Button[@text='Skip']").click();
		sleep(2);
	}

	

	public void ClickOnHomeButtonInDeviceSide() throws IOException
	{
		Runtime.getRuntime().exec("adb shell input keyevent 3");
	}
	
	String Mainwindow;
	public void OpeningGmail() throws InterruptedException, AWTException
    {
		JavascriptExecutor jse = (JavascriptExecutor)Initialization.driver;
		jse.executeScript("window.open()");
         Mainwindow = Initialization.driver.getWindowHandle();
		Initialization.driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		ArrayList<String> tabs = new ArrayList<String> (Initialization.driver.getWindowHandles());
		Initialization.driver.switchTo().window(tabs.get(1));
		Initialization.driver.get("https://mail.google.com");
		sleep(5);
    }
	
	public void SendingMailID_Password() throws InterruptedException
	{
		Initialization.driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys("dvvr901@gmail.com");
		sleep(2);
		Initialization.driver.findElement(By.xpath("//span[text()='Next']")).click();
		waitForXpathPresent("//span[text()='Next']");
		sleep(3);
		Initialization.driver.findElement(By.xpath("//input[@name='password']")).sendKeys("vishnu123");
		sleep(2);
		Initialization.driver.findElement(By.xpath("//span[text()='Next']")).click();
	}
	
	public void VerifyEmailAlertForAppBlackList(String DeviceName) throws InterruptedException, AWTException
	{
		OpeningGmail();
		SendingMailID_Password();
		waitForXpathPresent("//div[text()='Compose']");
		sleep(2);
		WebElement ele = Initialization.driver.findElement(By.xpath("(//table[@cellpadding='0']/tbody/tr[1]/td[6]//span[1])[2]"));
		String MailSubject = ele.getText();
    	if(MailSubject.contains("Device force deleted from SureMDM"))
    	{
    		ele.click();
    		Reporter.log("PASS>>> Received Mail Notification Successfully",true);
    		try
    		{
    			sleep(4);
    			Initialization.driver.findElement(By.xpath("//td[text()='"+DeviceName+"']")).isDisplayed();
    			Reporter.log("PASS>>> On Force Deleting The Device Mail Has Been Received Successfully",true);
    		}
    		
    		catch (Exception e) 
    		{
    			Initialization.driver.close();
    		    Initialization.driver.switchTo().window(Mainwindow);
			   ALib.AssertFailMethod("FAIL>>> No Mail Has Been Received On Force Delting The Device");
			}
    		Initialization.driver.close();
		    Initialization.driver.switchTo().window(Mainwindow);
    	}
    	
    	else
    	{
    		Initialization.driver.close();
		    Initialization.driver.switchTo().window(Mainwindow);
    		ALib.AssertFailMethod("FAIL>>> No Mail Notification Is Received");
    	}
    }


}
