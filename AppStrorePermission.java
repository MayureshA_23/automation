package UserManagement_TestScripts;

import org.testng.Reporter;

import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

// precondition
// have an android apk named SureLock
// ios app SureFox

public class AppStrorePermission extends Initialization{

   @Test(priority='1',description="1.To Verify roles option is present")
	public void VerifyUserManagement() throws InterruptedException{
		Reporter.log("\n1.To Verify roles option is present",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		
	}
	@Test(priority='2',description="2.To Verify default super user")
	public void verifysuperuser() throws InterruptedException{
		Reporter.log("\n2.To Verify super user is present",true);
		usermanagement.toVerifySuperUsertemplete();
	}
	@Test(priority='3',description="3.To Verify default super user")
	public void createRollForAppStorePermission() throws InterruptedException{
		Reporter.log("\n3.To create a roll for AppPermission",true);
		usermanagement.CreateRoleAppStorePermission();
	}
	
	@Test(priority='4',description="4.To create User for Device Permissions")
	public void CreateUserForAppStorePermission() throws InterruptedException{
		Reporter.log("\n4. User with AppStore Permissions",true);
		usermanagement.CreateUserForPermissions("AutoAppStorePermission", "42Gears@123", "AutoAppStorePermission", "AppStore Permission");  // uncomment remove later
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUser("AutoAppStorePermission", "42Gears@123");
		Thread.sleep(2000);
		}
	
	@Test(priority='5',description="5.click on app store")
	public void clickOnappStore() throws InterruptedException{
		Reporter.log("\n5.click on app store",true);
		usermanagement.clickOnAppStore();
		Thread.sleep(4000);
	}
	@Test(priority='6',description="6. Add Android is enabled")
	public void AddAndroidAppEnable() throws InterruptedException{
		Reporter.log("\n6.To create a roll for AppPermission",true);
		usermanagement.clickOnAddandroiod();
		Thread.sleep(2000);
	}
	@Test(priority='7',description="7.Edit android is enabled")
	public void EditAndroidAppEnable() throws InterruptedException{
		Reporter.log("\n7.To create a roll for AppPermission",true);
		usermanagement.clickOnAppStore();// remove
		usermanagement.ClickOnMore("SureLock");
		usermanagement.clickOneditandroiod();
	}
	@Test(priority='8',description="8.remove is enabled")
	public void RemoveAndroidAppEnable() throws InterruptedException{
		Reporter.log("\n8.To create a roll for AppPermission",true);
		usermanagement.ClickOnMore("SureLock");
		usermanagement.clickOnRemove();
	}
	@Test(priority='9',description="9.Add IOS Enabled")
	public void AddIosAppEnable() throws InterruptedException{
		Reporter.log("\n9.To create a roll for AppPermission",true);
		usermanagement.clickOnIosButton();
		Thread.sleep(10000);
		usermanagement.ClickOnAddIos();

	}
	@Test(priority='A',description="10.Edit IOs enabled")
	public void EditIosAppEnable() throws InterruptedException{
		Reporter.log("\n10.To create a roll for AppPermission",true);
		usermanagement.ClickOnMoreIOS("SureFox");
		usermanagement.clickonEditIos();
	}
	@Test(priority='B',description="11.remove IOS Enabled")
	public void RemoveIosAppEnable() throws InterruptedException{
		Reporter.log("\n11.To create a roll for AppPermission",true);
		usermanagement.ClickOnMoreIOS("SureFox");
		usermanagement.ClickOnRemoveIos();
		
	}
	
	@Test(priority='C',description="12.Disabling all the permissions")
	public void DisablingAppStorePermission() throws InterruptedException{
		Reporter.log("\n12.To create a roll for AppPermission",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept(Config.userID ,Config.password);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.DisablingAppStorePermission();
		commonmethdpage.ClickOnSettings();
		usermanagement.logoutAsAdmin();
		usermanagement.ClickOnLoginAgainLink();
		usermanagement.LoginAsNewUserWithoutAccept("AutoAppStorePermission", "42Gears@123");
		
	}
	@Test(priority='D',description="13.Add android disabled")
	public void DisabledAddAndroidApp() throws InterruptedException{
		Reporter.log("\n13.Add android disabled",true);
		usermanagement.clickOnAppStore();
		usermanagement.DisabledAddAndroidAppStore();
	}
	@Test(priority='E',description="14.Edit android app disabled")
	public void DisabledEditAndroidApp() throws InterruptedException{
		Reporter.log("\n14.Edit android app disabled",true);
		usermanagement.ClickOnMore("SureLock");
		usermanagement.DisabledEditAndroidPermission();
	}
	
	@Test(priority='F',description="15.remove android app disabled")
	public void DisabledRemoveAndroidApp() throws InterruptedException{
		Reporter.log("\n15.To create a roll for AppPermission",true);
		usermanagement.ClickOnMore("SureLock");
		usermanagement.DisabledRemoveAndroidPermission();
	}
	@Test(priority='G',description="16. Disabled Add Ios app")
	public void DisabledAddIOSApp() throws InterruptedException{
		Reporter.log("\n16.To create a roll for AppPermission",true);
		usermanagement.clickOnIosButton();
		usermanagement.ClickOnAddIosBtn();
		usermanagement.DisabledAddIOSPermission();
	}
	@Test(priority='H',description="17. Disabled edit Ios app")
	public void DisabledEditIOSApp() throws InterruptedException{
		Reporter.log("\n17.To create a roll for AppPermission",true);
		usermanagement.ClickOnMoreIOS("SureFox");
		usermanagement.DisabledEditIOSPermission();
	}
	@Test(priority='I',description="18.Disabled remove Ios app")
	public void DisabledRemoveIOSApp() throws InterruptedException{
		Reporter.log("\n18.To create a roll for AppPermission",true);
		usermanagement.ClickOnMoreIOS("SureFox");
        usermanagement.DisabledRemovePermission();		
	}
}
