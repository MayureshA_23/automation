package UserManagement_TestScripts;

import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;

//Demouser needs to be deleted

public class User  extends Initialization {
	
	
	
	@Test(priority='1',description="1.To Verify creating a User")
	public void VerifyErrormessageUserIdPresrent() throws InterruptedException{
		Reporter.log("\n1.To Verify user option is present",true);
		commonmethdpage.ClickOnSettings();
		usermanagement.ClickOnUserManagementOption();
		usermanagement.VerifyUserManagementIsDisplayed();
		usermanagement.ClickOnAddUserButton();
		usermanagement.SendingUserDetails("Demouser","42Gears@123","dontDelete","abc@gmail.com");
		usermanagement.ClickOnCreateUserButton();

    }
	@Test(priority='2',description="2.To Verify Warning Messages in User without data")
	public void UserNameBlank() throws InterruptedException{
		Reporter.log("\n2.To Verify Warning Messages in User without data",true);
		usermanagement.WarningMessagesblankFieldInUser();
		usermanagement.ClickOnCancelButton();
	}
	
    @Test(priority='3',description="4.Edit user, delete user, reset password, disable/enable user options should be enabled")
	public void VerifyEditDeleteUserResetPasswordDisableEnableUserAreEnabledWhenOneOfUserIsSelected() throws InterruptedException{
		Reporter.log("\n3.Edit user, delete user, reset password, disable/enable user options should be enabled",true);

		usermanagement. SelectUserAndCheckEnabledButtons();
	}
	
	@Test(priority='4',description="4 \"Note: User Name cannot be edited.\" help text message should be present and user should not be able to edit user name")
	public void VerifyThatUserShouldNotBeAbleToEditUserNameOption() throws InterruptedException{
		Reporter.log("\n4.\"Note: User Name cannot be edited.\" help text message should be present and user should not be able to edit user name",true);
		usermanagement.SelectDemoUser();
		usermanagement.ClickOnEditUser();
		usermanagement.VerifyEditUserNote();
		usermanagement.ClickOnCancelButton();
	}
	
	@Test(priority='5',description="5 \"User ID already exists.\" error message should be displayed")
	public void VerifyThatUserWillGetErrorMessageWhenWeTrytoCreateUserWithAlreadyExistingCredentials() throws InterruptedException{
		Reporter.log("\n5.\"User ID already exists.\" error message should be displayed",true);
        usermanagement. ErrorMessagesUserIdExists();
	}
	
	
}
	
	
	
	