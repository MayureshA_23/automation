package Settings_TestScripts;
import org.testng.Reporter;
import org.testng.annotations.Test;
import Common.Initialization;

public class AccountManagement extends Initialization {
	
	@Test(priority=1,description="Verify Cancel Delete Request option in Account Settings") 
	public void VerifyCancelDeleteRequestoptioninAccountSettingsTC_ST_364() throws Throwable {
		Reporter.log("\n1.Verify Cancel Delete Request option in Account Settings",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.ClickOnDeleteAccounts();
		accountsettingspage.ClickOnCancelDeleteRequest();				
}
    @Test(priority=2,description="Verify Account Management Delete Account No option in Account Settings") 
    public void VerifyAccountManagementDeleteAccountNooptioninAccountSettingsTC_ST_365()throws Throwable {
        Reporter.log("\n2.Verify Account Management Delete Account No option in Account Settings",true); 
        commonmethdpage.ClickOnSettings();
        accountsettingspage.ClickOnAccountSettings();
        accountsettingspage.ClickonAccountManagement();
        accountsettingspage.ClickOnDeleteAccounts();
        accountsettingspage.ClickOnCancelDeleteRequest();
        accountsettingspage.DeleteAccountButtonVisible();       
}
    
    @Test(priority=3,description="Verify Intercom access for User field in the Account Management") 
    public void VerifyIntercomaccessforUserfieldintheAccountManagementTC_ST_477()throws Throwable {
        Reporter.log("\n3.Verify Intercom access for User field in the Account Management",true); 
        commonmethdpage.ClickOnSettings();
        accountsettingspage.ClickOnAccountSettings();
        accountsettingspage.ClickonAccountManagement();
        accountsettingspage.IntercomAccessForUser();
        accountsettingspage.IntercomAccessForUserOptionVisible();
        accountsettingspage.SuperUserEmailVisble();
      
    }
    
    @Test(priority=4,description="Verify assigning intercom for the Sub User.") 
	  public void VerifyssigningintercomfortheSubUserTC_ST_478() throws Throwable {
		Reporter.log("\n4.Verify assigning intercom for the Sub User.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.IntercomAccessForUser();
		accountsettingspage.SelectSubUser();
		accountsettingspage.ClickOnSaveIntercomButton();
	 }  
    
    
    @Test(priority=5,description="Verify assigning intercom for the multiple Sub Users.") 
	  public void VerifyassigningintercomforthemultipleSubUsersTC_ST_479() throws Throwable {
		Reporter.log("\n5.Verify assigning intercom for the multiple Sub Users.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.IntercomAccessForUser();
		accountsettingspage.SelectSubUser();
		accountsettingspage.SingleSelectedDropDown();
		
	 }  
    
    
	@Test(priority=6,description="Verify Clear Job Queue in Account Management") 
	  public void VerifyClearJobQueueinAccountManagementTC_ST_412() throws Throwable {
		Reporter.log("\n6.Verify Clear Job Queue in Account Management.",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.DeployedOptionVisible();
		accountsettingspage.InProgressOptionVisible();
		accountsettingspage.PendingOptionVisible();
		accountsettingspage.ErrorOptionVisible();
			
	 }  
    
	@Test(priority=7,description="Verify pop-up displayed when delete account button is clicked.") 
	public void VerifypopupdisplayedwhendeleteaccountbuttonisclickedTC_ST_498() throws Throwable {
		Reporter.log("\n7.Verify pop-up displayed when delete account button is clicked.",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.ClickOnDeleteAccounts();
		accountsettingspage.PopUpMessagevisible();
		accountsettingspage.ClickOnCancelDeleteRequest();				
}
	
    
	@Test(priority=8,description="Require Two-Factor Authentication for Super user Option Displayed ") 
	public void RequireTwoFactorAuthenticationforsuperuseroptiondisplayedTC_ST_545() throws Throwable {
		Reporter.log("\n8.Require Two-Factor Authentication for Super user Option Displayed",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.RequireTwoFactorAuthenticationOption();
		accountsettingspage.RequireTwoFactorAuthenticationOptionVisible();
}
    
	
	//set pre condition as 
	//String url = "https://licensedindia.in.suremdm.io/console/"; String userID ="Mithilesh_Superuser"; String password = "Mithilesh@123";
//	@Test(priority=9,description="Require Two-Factor Authentication for sub user") 
//	public void RequireTwoFactorAuthenticationforsubuserTC_ST_546() throws Throwable {
//		Reporter.log("\n9.Require Two-Factor Authentication for Super user Option Displayed",true);
//		
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonAccountManagement();
//		accountsettingspage.TwoFactorAuthenticationVisible();		
//}
	

	
	@Test(priority=10,description="Enable your own 2-FA Option is Disabled") 
	public void Enableyourown2FAOptionDisabledTC_ST_547() throws Throwable {
		Reporter.log("\n10.Enable your own 2-FA Option is Disabled",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.RequireTwoFactorAuthenticationOption();
		accountsettingspage.Enableyourown2FAButton();		
}
    
	@Test(priority=11,description="Verify password Age set to '0' in password policy in Account Management") 
	public void VerifypasswordAgesetto0inpasswordpolicyinAccountManagementTC_ST_667() throws Throwable {
		Reporter.log("\n11.Verify password Age set to '0' in password policy in Account Management",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.EnforcePasswordHistoryVisible();
		accountsettingspage.MaximumpasswordageVisible();
		accountsettingspage.ClickonSaveButton();
		accountsettingspage.Passwordpolicyupdatedsuccessfully();
		accountsettingspage.ResetMaximumpasswordage();
		accountsettingspage.ClickonSaveButton();
		accountsettingspage.ResetPasswordpolicyupdatedsuccessfully();
}
		
	@Test(priority=12,description="Verify Password Policy in the Account Management.") 
	public void VerifyPasswordPolicyintheAccountManagementTC_ST_683() throws Throwable {
		Reporter.log("\n12.Verify Password Policy in the Account Management.",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.EnforcePasswordHistoryOptionVisible();
		accountsettingspage.MaximumpasswordageOptionVisible();		
}
	
	@Test(priority=13,description="Verify default values for Enforce password history and Maximum password age") 
	public void VerifydefaultvaluesforEnforcepasswordhistoryandMaximumpasswordageTC_ST_684() throws Throwable {
		Reporter.log("\n13.Verify default values for Enforce password history and Maximum password age",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.EnforcePasswordHistoryDefaultValue();
		accountsettingspage.MaximumPasswordAgeDefaultValue();				
}
	
	@Test(priority=14,description="Verify minimum and maximum values for Enforce password history and Maximum password age.") 
	public void VerifyminimumandmaximumvaluesforEnforcepasswordhistoryandMaximumpasswordageATC_ST_686() throws Throwable {
		Reporter.log("\n14.Verify minimum and maximum values for Enforce password history and Maximum password age.",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.EnforcePasswordHistoryMinMax();
		accountsettingspage.MaximumPasswordAgeMinMax();		
}
	
	@Test(priority=15,description="Verify Configuring the Enforce password history and maximum password age in Account Management") 
	public void VerifyConfiguringtheEnforcepasswordhistoryandmaximumpasswordageinAccountManagementTC_ST_687() throws Throwable {
		Reporter.log("\n15.Verify Configuring the Enforce password history and maximum password age in Account Management",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.EnforcePasswordHistoryValueEntered();
		accountsettingspage.MaximumpasswordageValueEntered();
		accountsettingspage.ClickonSaveButton();
		accountsettingspage.ResetPasswordpolicyupdatedsuccessfully();
}
	
		
	@Test(priority=16,description="Verify Enforce Password history while changing the password from Security") 
	public void VerifyEnforcePasswordhistorywhilechangingthepasswordfromSecurityTC_ST_688() throws Throwable {
		Reporter.log("\n16.Verify Enforce Password history while changing the password from Security",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.EnforcePasswordHistoryValueEntered();
		accountsettingspage.MaximumpasswordageValueEntered();
		accountsettingspage.ClickonSaveButton();
		accountsettingspage.Passwordpolicyupdatedsuccessfullymsg();
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnSecurityButton();
		accountsettingspage.EnterAllPasswordField();
		accountsettingspage.ClickonOKButton();
		accountsettingspage.ErrorMsgDispayed();	
		accountsettingspage.CloseSecurityTab();
		
}
	
	@Test(priority=17,description="Initially Key must be in dot format ") 
	public void InitiallyKeymustbeindotformatTC_ST_723() throws Throwable {
		Reporter.log("\n17.Initially Key must be in dot format ",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.APIKeyEncryptionFormat();			
}
	
	@Test(priority=18,description="Copy key must be grayed out") 
	public void CopykeymustbegrayedoutTC_ST_724() throws Throwable {
		Reporter.log("\n18.Copy key must be grayed out",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.CopytoClipBoardButton();		
}
	
	@Test(priority=19,description="Try to copy the key when the key is in star format") 
	public void TrytocopythekeywhenthekeyisinstarformatTC_ST_725() throws Throwable {
		Reporter.log("\n19.Try to copy the key when the key is in star format",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.APIKeyCannotbeCopied();	
}
	
	@Test(priority=20,description="Copy to clipboard button must not be grayed out when key is visible") 
	public void CopytoclipboardbuttonmustnotbegrayedoutwhenkeyisvisibleTC_ST_726() throws Throwable {
		Reporter.log("\n20.Copy to clipboard button must not be grayed out when key is visible",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.ClipBoardCopyButtonVisible();
}
	
	
	@Test(priority=21,description="Text copied to clipboard message should be shown ") 
	public void TextcopiedtoclipboardmessageshouldbeshownTC_ST_727() throws Throwable {
		Reporter.log("\n21.Text copied to clipboard message should be shown ",true);		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.APIKeyCopiedtoClipBoard();
}
	
	// set the pre-condition as it should be sub-user account
//	@Test(priority=22,description="Same key for sub user should be shown") 
//	public void SamekeyforsubusershouldbeshownTC_ST_728() throws Throwable {
//		Reporter.log("\n22.Same key for sub user should be shown",true);		
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonAccountManagement();
//		accountsettingspage.SubUserAPIKeyCopiedtoClipBoard();
//}
	

	//set the pre-condition as some different account
//	@Test(priority=23,description="Different key for different account should be shown") 
//	public void DifferentkeyfordifferentaccountshouldbeshownTC_ST_729() throws Throwable {
//		Reporter.log("\n23.Different key for different account should be shown",true);		
//		commonmethdpage.ClickOnSettings();
//		accountsettingspage.ClickOnAccountSettings();
//		accountsettingspage.ClickonAccountManagement();
//		accountsettingspage.DiffrentUserAPIKeyCopiedtoClipBoard();		
//}
	
	@Test(priority=24,description="Verify minimum and maximum values for Enforce password history and Maximum password age.") 
	public void VerifyminimumandmaximumvaluesforEnforcepasswordhistoryandMaximumpasswordageBTC_ST_686() throws Throwable {
		Reporter.log("\n24.Verify minimum and maximum values for Enforce password history and Maximum password age.",true);
		
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickonAccountManagement();
		accountsettingspage.EnforcePasswordHistoryMinMax();
		accountsettingspage.MaximumPasswordAgeMinMax();		
}
	
	//vinimaomj
	
	@Test(priority = 25, description = "Verify API key in Account management TC_ST_722")
	public void VerifyAPIkeyinAccountManagementTC_ST_722() throws InterruptedException {
	Reporter.log("\n25.Verify API key in Account management TC_ST_722",true);
	commonmethdpage.ClickOnSettings();
	accountsettingspage.AccountSettings();
	accountsettingspage.accountManagement();
	accountsettingspage.accountManagementDeleteaccount();
	accountsettingspage.accountManagementAPIeye();
	accountsettingspage.copytoClipboard_apiKey();
	accountsettingspage.verifyAPIkeyValueIsCopied();
	accountsettingspage.clickHome();
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    
}