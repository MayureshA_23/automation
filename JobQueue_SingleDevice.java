package QuickActionToolBar_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import PageObjectRepository.DeviceEnrollmentHonor8_ScanQR_POM;
import PageObjectRepository.QuickActionToolBar_POM;

public class JobQueue_SingleDevice extends Initialization
{
	
	//SureFox should not be installed In the device.
	
	@Test(priority=0,description="Verify job count for offline devices.")
	public void VerifyJobCountForOfflineDevice() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("1.Verify job count for offline devices.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.GettingJobCountsForAnOnlineDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.GettingAllJobCounts();
		quickactiontoolbarpage.VerifyingJobCountForOnlineAndOfflineDevice();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
	}
	
	@Test(priority=1,description="verify job history prompt.")
	public void VerifyJobHistoryPompt() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n2.Verify job count for offline devices.",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName); 
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.VerifyOfQueuePage();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();	 
	}

	@Test(priority=2,description="verify Inprogress job count in job history")
	public void VerifyInprogressJobCountInHistoryTab() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n3.verify Inprogress job count in job history",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("InProgress",2);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnInstallApplication();
		androidJOB.enterJobName(Config.InstallJObName);   
		androidJOB.clickOnAddInstallApplication();
		androidJOB.browseFileInstallApplication("./Uploads/UplaodFiles/Job On Android/InstallJobSingle/\\File1.exe");
		androidJOB.clickOkBtnOnBrowseFileWindow();
		androidJOB.clickOkBtnOnAndroidJob();
		androidJOB.UploadcompleteStatus();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		androidJOB.JobInitiatedMessage();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideIncompleteJobsTab(Config.InstallJObName);
		quickactiontoolbarpage.VerifyJobsCountInsideJobsQueue_AfterApplyingJob("InProgress",2);
		accountsettingspage.ReadingConsoleMessageForJobDeploymentOnGroup("surevideo.apk",Config.DeviceName);
	}

	@Test(priority=3,description="Verify Failed jobs in Job history.")
	public void VerifyFailedJobsInJobHistory() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n4.Verify Failed jobs in Job history.",true);
		commonmethdpage.ClickOnAllDevicesButton();
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Failed JobsTab",3);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		quickactiontoolbarpage.CreatingSureFoxSettingsJob(quickactiontoolbarpage.ErrorjobName);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		androidJOB.JobInitiatedMessage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickOnFailedJobsTab();
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideFailedJobsTab(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.VerifyJobsCountInsideJobsQueue_AfterApplyingJob("Failed JobsTab",3);
	}

	@Test(priority=4,description="Verify Completed jobs in Job history")
	public void VerifyCompletedJobsInJobHistory() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n5.Verify Completed jobs in Job history",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Completed JobTab",4);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.clickOnTextMessageJob();
		androidJOB.textMessageJobWhenNoFieldIsEmpty(Config.TextMessageJobname, "0text","Normal Text");
		androidJOB.ClickOnOkButton();
		androidJOB.JobCreatedMessage();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TextMessageJobname);
		accountsettingspage.ReadingConsoleMessageForJobDeployment(Config.TextMessageJobname,Config.DeviceName);
		androidJOB.CheckStatusOfappliedInstalledJob(Config.TextMessageJobname, 120);
		androidJOB.clickonJobQueue();
		androidJOB.clickOnJobhistoryTab();
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideCopletedJobTab(Config.TextMessageJobname);
		quickactiontoolbarpage.VerifyJobsCountInsideJobsQueue_AfterApplyingJob("Completed JobTab",4);
	}

	@Test(priority=5,description="Verify On clicking on pending job status in device grid navigating to job history Incomplete jobs tab.")
	public void VerifyOnClickingPendingJob_NavigationtoIncompleteJObsTab() throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n6.Verify On clicking on pending job status in device grid navigating to job history Incomplete jobs tab.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobPendingStateSymbol);
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideIncompleteJobsTab(Config.InstallJObName); 
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}

	@Test(priority=6,description="Verify On clicking on Inprogress job status in device grid navigating to job history Incomplete jobs tab.",dependsOnMethods="VerifyOnClickingPendingJob_NavigationtoIncompleteJObsTab")
	public void VerifyOnClickingInprogressJob_NavigationtoIncompleteJobsTab() throws InterruptedException, IOException
	{
		Reporter.log("\n7.Verify On clicking on Inprogress job status in device grid navigating to job history Incomplete jobs tab.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobInprogressStateSymbol);
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideIncompleteJobsTab(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}

	@Test(priority=7,description="Verify On clicking on Failed  job status in device grid navigating to job history failed jobs tab")
	public void VerifyOnClickingFailedJob_NavigationtoFailedJobsTab() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n8.Verify On clicking on Failed  job status in device grid navigating to job history failed jobs tab",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobErrorStateSymbol);
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideFailedJobsTab(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}

	/*@Test(priority=8,description="Verify On clicking on Success job status in device grid navigating to job history Complete jobs tab")
	public void VerifyOnClickingSuccessJob_NavigationtoCompletedJosTab() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n9.Verify On clicking on Success job status in device grid navigating to job history Complete jobs tab",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		accountsettingspage.ReadingConsoleMessageForJobDeployment("surevideo.apk",Config.DeviceName);
		quickactiontoolbarpage.ClickOnJobStatus(quickactiontoolbarpage.JobSuccessStateSymbol);
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideCopletedJobTab(Config.InstallJObName);
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}

	@Test(priority=9,description="Verify status is changing from Pending to Inprogress state once job when to In Progress.")
	public void VerifyJobStatusChangeFrompendingToInprogress() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n10.Verify status is changing from Pending to Inprogress state once job when to In Progress.",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		accountsettingspage.ReadingConsoleMessageForJobDeployment(Config.InstallJObName,Config.DeviceName);
		quickactiontoolbarpage.VerifyJobStatusChangeFromPendingToInprogressInDevieGrid();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
	}*/
	@Test(priority='A',description="Verify status is changing from Pending to Inprogress and error if jobs to go to error state.")
	public void VerifyJobStatusChangeFromPendingToInprogressAndError() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n11.Verify status is changing from Pending to Inprogress and error if jobs to go to error state.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.VerifyJObStatusChangeFromPendingToinprogressAndError();
		
	}

	/*@Test(priority='B',description="Verify On Mouse hover on the pending Job status in device Grid displaying pending Job count.")
	public void VerifyMouseHoverOnPendingJobStatusInDeviceGridDisplayingJobCount() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n12.Verify On Mouse hover on the pending Job status in device Grid displaying pending Job count.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobPendingStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Pending JobsTab",1);
	}*/

	/*@Test(priority='C',description="Verify On Mouse hover on the Inprogress Job status in device Grid displaying Inprogress Job count.",dependsOnMethods="VerifyMouseHoverOnPendingJobStatusInDeviceGridDisplayingJobCount")
	public void VerifyMouseHoverOnInprogressJobStatusInDeviceGridDisplayingInprogressJobCount() throws InterruptedException, IOException
	{
		Reporter.log("\n13.Verify On Mouse hover on the Inprogress Job status in device Grid displaying Inprogress Job count.",true);
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobInprogressStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Inprogress JobsTab",2);	   
	}*/


	/*@Test(priority='D',description="Verify On Mouse hover on the Error Job status in device Grid displaying Error Job count.")
	public void VerifyMouseHoverOnErrorJobStatusInDeviceGriddisplayingErrorJobCount() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n14.Verify On Mouse hover on the Error Job status in device Grid displaying Error Job count.",true);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobErrorStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Error JobsTab",3);
	}*/

	/*@Test(priority='E',description="Verify On Mouse hover on the Success Job status in device Grid displaying Success Job count.")
	public void VerifyMouseHoveroNSuccessjobStatusInDeviceGridDisplsyingSuccessJobCount() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n15.Verify On Mouse hover on the Success Job status in device Grid displaying Success Job count.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.ReadingToolTipMessageForJobStatusInDeviceGrid(quickactiontoolbarpage.JobSuccessStateSymbol);
		quickactiontoolbarpage.VerifyJobCountInDeviceGridAndInsideJobHistoryAreSame("Success jobsTab",4);
	}*/

	@Test(priority='F',description="Verify On deleting the pending jobs in job history, Pending job count is decreasing or not.")
	public void VerifyDeletingPendingJobsInHistoryAndVerifyingPendingJobsCount() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n16.Verify On deleting the pending jobs in job history, Pending job count is decreasing or not.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		quickactiontoolbarpage.clickOnJobHistoryButton();
		quickactiontoolbarpage.ReadingJobsCountBeforeDeletingJobsInJobHistory("Pending JobsTab",1);
		quickactiontoolbarpage.VerifyJobsCountafterDeletingjobsInJobHistory("Pending JobsTab",1,quickactiontoolbarpage.LatestPendingJobName,quickactiontoolbarpage.IncompleteJobRemoveButton,"Pending");
	}

	@Test(priority='G',description="Verify On deleting the InProgress jobs in job history, InProgress job count is decreasing or not.")
	public void VerifyDeletingInProgressJobsInHistoryAndVerifyingInprogressJobsCount() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n17.Verify On deleting the InProgress jobs in job history, InProgress job count is decreasing or not.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		quickactiontoolbarpage.clickOnJobHistoryButton();
		quickactiontoolbarpage.ReadingJobsCountBeforeDeletingJobsInJobHistory("Inprogess",2);
		quickactiontoolbarpage.VerifyJobsCountafterDeletingjobsInJobHistory("Inprogess",2,quickactiontoolbarpage.LatestPendingJobName,quickactiontoolbarpage.IncompleteJobRemoveButton,"Inprogress");

	}

	@Test(priority='H',description="Verify On deleting the Failed jobs in job history, failed job count is decreasing or not.")
	public void VerifyDeletingFailedJobsInHistoryAndVerifyingFailedjobsCount() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("\n18.Verify On deleting the Failed jobs in job history, failed job count is decreasing or not.",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		//androidJOB.ClickOnApplyJobOkButton();
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		quickactiontoolbarpage.clickOnJobHistoryButton();
		quickactiontoolbarpage.DeletingMultipleJobsInJobHistory();
		quickactiontoolbarpage.VerifyJobsCountafterDeletingFailedjobsInJobHistory("Error JobsTab",3);
	}

	@Test(priority='I',description="Verify Scheduled Jobs count in the Asset Tracking report")
	public void VerifyScheduledjobsCountInAssetTrackinhReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n19.Verify Scheduled Jobs count in the Asset Tracking report",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Pending",1);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Scheduled Jobs Column",62);
		accountsettingspage.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority='J',description="Verify InProgress Jobs count column in the Asset Tracking report",dependsOnMethods="VerifyScheduledjobsCountInAssetTrackinhReport")
	public void VerifyInprogressjobsCountInAssetTrackinhReport() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n20.Verify InProgress Jobs count column in the Asset Tracking report",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Inprogress",2);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Inprogress Jobs Coulumn",63);
		accountsettingspage.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority='K',description="Verify Failed Jobs count column in the Asset Tracking report")
	public void VerifyFailedjobsCountInAssetTrackinhReport() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n21.Verify Failed Jobs count column in the Asset Tracking report",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Error",3);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Failed Jobs Column",64);
		accountsettingspage.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority='L',description="Verify Deployed Jobs count column in the Asset Tracking report")
	public void VerifyDeployedjobsCountInAssetTrackinhReport() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n22.Verify Deployed Jobs count column in the Asset Tracking report",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Completed Jobs",4);
		reportsPage.ClickOnReports_Button();
		customReports.ClickOnOnDemandReport();
		accountsettingspage.SearchingForReportInOnDemandReport("Asset Tracking");
		accountsettingspage.ClickONRequestReportButton();
		accountsettingspage.ClickOnViewReportButton();
		accountsettingspage.ClickOnRefreshInViewReport();
		accountsettingspage.ClickOnView("Asset Tracking");
		accountsettingspage.WindowHandle();	
		quickactiontoolbarpage.SearchingInsideReport(Config.DeviceName);
		quickactiontoolbarpage.VerifyScheduledjobsCountInsideReport("Deployed Jobs Coulumn",65);
		accountsettingspage.SwitchBackWindow();
		commonmethdpage.ClickOnHomePage();
	}

	@Test(priority='M',description="Verify Retry for the pending jobs in Job history")
	public void VerifyRetryForPendingJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		Reporter.log("\n23.Verify Retry for the pending jobs in Job history",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.GettingTimeBeforeRetryingPendingJob();
		quickactiontoolbarpage.RetryingJobPresentInIncompleteJobsTab();
		quickactiontoolbarpage.GettingTimeAfterRetryingPendingJob();
		quickactiontoolbarpage.VerifyRetryingPendingJob();
		
	}
	@Test(priority='N',description="Verify Retry for the Inprogress jobs in Job history",dependsOnMethods="VerifyRetryForPendingJob")
	public void VerifyRetryForInprogressJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n24.Verify Retry for the Inprogress jobs in Job history",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Inprogress",2);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.RetryingJobPresentInIncompleteJobsTab();
		quickactiontoolbarpage.ReadingJobsCountAfterRetryingJobsInJobHistory("Inprogress",2);
		quickactiontoolbarpage.VerifyJobCountAndJobStatusAfterRetryingInprogressJob();
	}

	@Test(priority='O',description="Verify Retry for the Failed Jobs.")
	public void VerifyRetryForFailedJob() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n25.Verify Retry for the Failed Jobs.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(quickactiontoolbarpage.ErrorjobName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		quickactiontoolbarpage.GettingInitialCountOfJobsInJobQueue("Error",3);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickOnFailedJobsTab();
		quickactiontoolbarpage.RetryingFailedJob();
		quickactiontoolbarpage.ReadingJobsCountAfterRetryingJobsInJobHistory("Error",3);
		quickactiontoolbarpage.VerifyJobCountAndJobStatusAfterRetryingFailedJob();	   
	}
	
	@Test(priority='Q',description="Verify deleting deployed dynamic jobs from the quick action tab.")
	public void VerifyDeleteDeployedJobFromQueue() throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException
	{
		Reporter.log("\n26.Verify deleting deployed dynamic jobs from the quick action tab.",true);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		remotesupportpage.ClickOnRemoteSupport();
		remotesupportpage.windowhandles();
		remotesupportpage.SwitchToRemoteWindow();
		remotesupportpage.SwitchToMainWindow();
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickOnQuickActionTab();
		quickactiontoolbarpage.VerifyDeletingDeployedDynamicJob("Remote support");
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
		
	}

	@Test(priority='R',description="Verify Sub job Queue  in job Queue prompt.")
	public void VerifySubJobqueue() throws Throwable
	{
		Reporter.log("\n27.Verify Sub job Queue  in job Queue prompt.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickingOnSubJobQueue();
		quickactiontoolbarpage.VerifysubJobqueuePromptIsDisplyed();
		quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}
	
	@Test(priority='S',description="Verify Sub job Queue  in job Queue prompt should be displayed for multi install,multi file transfer and composite jobs")
    public void SubJobqueuPromptFormultipleFiletransfer() throws Throwable
    {
		Reporter.log("\n28.Verify Sub job Queue  in job Queue prompt should be displayed for multi install,multi file transfer and composite jobs",true);
		androidJOB.CreatingMultiFileTransferJob(Config.MultipleFileTransferJobName);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.MultipleFileTransferJobName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickingOnSubJobQueue();
		quickactiontoolbarpage.VerifyingMultipleFileTransferFileNamesInSubJobQueue();
		quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
		androidJOB.CreatingMultiInstallJob(Config.MultinstallJobName);
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.MultinstallJobName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickingOnSubJobQueue();
		quickactiontoolbarpage.VerifyMultipleJobsSubJobQueue("LED Flashlight Small size_v1.3_apkpure.com.apk");
		quickactiontoolbarpage.VerifyMultipleJobsSubJobQueue("surevideo.apk");
		quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnCompositeJob();
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.MultinstallJobName);
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.MultipleFileTransferJobName);
		androidJOB.CompositeJobName(Config.CompositeJobName);
		androidJOB.ClickOnOnOkButtonAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		quickactiontoolbarpage.DisablingNixService();
		androidJOB.clickOnDoneBtnInSettings();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.CompositeJobName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Pending",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickingOnSubJobQueue();
		quickactiontoolbarpage.VerifyMultipleJobsSubJobQueue("0MultiInstall");
		quickactiontoolbarpage.VerifyMultipleJobsSubJobQueue("0MultiFileTransfer");
		quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		androidJOB.ClikOnNixSettings();
		androidJOB.EnableNix();
		androidJOB.clickOnDoneBtnInSettings();
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
    }
	
	@Test(priority='T',description="Verify that job should deploy on the device if disable and enable the internet.")
	public void VerifyJobStatusOnDisablingEnablingInternet() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
	{
		Reporter.log("\n29.Verify that job should deploy on the device if disable and enable the internet.",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		quickactiontoolbarpage.SwitchingWifiStatus("OFF");
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.TextMessageJobname);
		quickactiontoolbarpage.VerifyJobStaysInPendingStatewhenWifiIsOFF("Pending",Config.DeviceName);
		quickactiontoolbarpage.SwitchingWifiStatus("ON");
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,Config.DeviceName);
		Reporter.log("Job Deployed Successfully On Turning On Wifi",true);
	}
	
	@Test(priority='U',description="Verify if any sub job shows \"Error\" job status then overall status of the job should be \"Error\".")
	public void VerifyOverallJobStatusWhenSubJobIsInerrorState() throws Throwable
	{
		Reporter.log("\n30.Verify if any sub job shows \"Error\" job status then overall status of the job should be \"Error\".",true);
		androidJOB.clickOnJobs();
		androidJOB.clickNewJob();
		androidJOB.clickOnAndroidOS();
		androidJOB.ClickOnCompositeJob();
		androidJOB.SelectingJobToAddOutOfComplianceAction(Config.TextMessageJobname);
		androidJOB.SelectingJobToAddOutOfComplianceAction(quickactiontoolbarpage.ErrorjobName);
		androidJOB.CompositeJobName(Config.CompositeJobWithErrorStatus);
		androidJOB.ClickOnOnOkButtonAndroidJob();
		commonmethdpage.ClickOnHomePage();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.CompositeJobWithErrorStatus);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Error",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickOnFailedJobsTab();
		quickactiontoolbarpage.VerifyAppliedJobIsPresentInsideFailedJobsTab(Config.CompositeJobWithErrorStatus);
		quickactiontoolbarpage.ClickingOnSubJobQueue();
		quickactiontoolbarpage.VerifySubJobststus("Error");
		Reporter.log("OverAll Job Status Is Error As Sub job is in error state",true);
		quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}
	
	
	@Test(priority='V',description="Verify after applying Composite job/Multi install/Multi File transfer all the sub-job status change to \"In progress\".")
	public void VerifyAllSubJobststusChangeToInprogress() throws Throwable
	{
		Reporter.log("\n31.Verify after applying Composite job/Multi install/Multi File transfer all the sub-job status change to \"In progress\".",true);
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.MultipleFileTransferJobName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.ClickingOnSubJobQueue();
		quickactiontoolbarpage.VerifyMultipleSubJobStatus();
		quickactiontoolbarpage.ClickOnSubStatusJobCloseButton();
		quickactiontoolbarpage.ClickOnJobQueueCloseBtn();
	}
	
	@Test(priority='W',description="Verify 'Scheduled' status in Job Queue prompt.")
	public void VerifyScheduledStatuInJobQueue() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException
	{
		Reporter.log("\n32.Verify 'Scheduled' status in Job Queue prompt.",true);
		quickactiontoolbarpage.ClearingConsoleLogs();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJobsInApplyJob(Config.TextMessageJobname);
		quickactiontoolbarpage.SearchFieldApplyJobAndScheduleTime("ScheduledtextJob","ScheduleDayAndTime");
		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.VerifyJobStatusScheduled();
		quickactiontoolbarpage.WaitingForUpdate(90);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchJobsInApplyJob(Config.TextMessageJobname);
		quickactiontoolbarpage.SearchFieldApplyJobAndScheduleTime("ScheduledtextJob","Select Date And Time");
		androidJOB.ClickOnApplyJobOkButton();
		androidJOB.clickonJobQueue();
		quickactiontoolbarpage.VerifyJobStatusScheduled();
		quickactiontoolbarpage.WaitingForUpdate(90);
		commonmethdpage.AppiumConfigurationCommonMethod("com.nix","com.nix.MainFrm");
		accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment(Config.TextMessageJobname,Config.DeviceName);
	}
	
	@Test(priority='X',description="Verify that after reboot the device all the pending/In Progress job deployed on the device successfully.")
	public void VerifyJobStatusAfterReboot() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{
		Reporter.log("\n33.Verify that after reboot the device all the pending/In Progress job deployed on the device successfully.",true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		commonmethdpage.ClickOnApplyButton();
		androidJOB.SearchField(Config.InstallJObName);
		quickactiontoolbarpage.WaitingForJobStatusUpdationInConsole("Inprogress",Config.DeviceName);
		dynamicjobspage.ClickOnMoreOption();
	    dynamicjobspage.ClickOnRebootDevice();
	    dynamicjobspage.ClickOnRebootDeviceYesBtn();
	 	dynamicjobspage.VerifyOfRebootActivityLogs();
	 	accountsettingspage.ReadingConsoleMessageForSuccessfulJobDeployment("surevideo.apk",Config.DeviceName);
	 	Reporter.log("PASS>>> Job Deployed Successfully On The Device After Reboot",true);
	}
}
