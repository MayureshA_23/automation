package DeviceGrid_TestScripts;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;

/*Enroll EA device to verify Hash Code column
 *Declare Varible in config  IPAddressOfEnrolledDevice,Give value equal to IP address of Enrolled Device 
 *IMEI Number of Enrolled device
 *IMEINumber2 of Enrolled device
 *Enable GPS of Enrolled Device
 *Enable Bluetooth of Enrolled Device
 *DeviceUserName
 *DeviceUserNameDevice
 *BlueToothName
 **/

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import Common.Initialization;
import Library.Config;
import bsh.ParseException;

public class DeviceGridSorting extends Initialization {
	/*
	 * Varibles has to be updated in Config IPAddressOfEnrolledDevice
	 * MultipleDeviceName SerialNumber BluetoothSSID PhoneNumber Install sure lock
	 * pass data "test" in sureLock Identifier DeviceAndroidID Enable location in
	 * device Location in device should be On AndroidEnterpriseGrid Network Polling
	 * IMEI OS build Number
	 */
	@Test(priority = 1, description = "Validation of Device column in Device Grid.")
	public void VerifySortingOfDeviceColumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("1.Validation of Device column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.VerifyColumnSorting("Device");
		deviceGrid.NavigateToOtherPages("Dashboard", "Home");
		deviceGrid.VerifySortingIsNotChanged("Device");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChanged("Device");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPage("2", "Device");
		deviceGrid.selectPage("1", "Device");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceName", Config.DeviceName);
		deviceGrid.verifyEnrolledDeviceAvailable();
		deviceGrid.SearchMultipleDevice(Config.KnoxDevice, "DeviceName");
		deviceGrid.clearColValue("DeviceName");
		Reporter.log("Pass >>Validation of Device column in Device Grid.", true);
	}

	@Test(priority = 2, description = "Validation of Battery column in Device Grid.", enabled = false)
	public void VerifySortingOfBatteryColumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n2.Validation of Battery column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Battery");
		deviceGrid.Enable_DisbleColumn("Battery", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Battery");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Battery");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("Battery");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Battery");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Battery");
		deviceGrid.SelectSearchOption("Advanced Search");

		deviceGrid.EnterValueInAdvanceSearchColumn("Battery", "<90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<", 90, "Battery");

		deviceGrid.EnterValueInAdvanceSearchColumn("Battery", ">90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">", 90, "Battery");

		deviceGrid.EnterValueInAdvanceSearchColumn("Battery", ">=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">=", 90, "Battery");

		deviceGrid.EnterValueInAdvanceSearchColumn("Battery", "<=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<=", 90, "Battery");

		deviceGrid.EnterValueInAdvanceSearchColumn("Battery", "=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=", 90, "Battery");
		deviceGrid.clearColValue("Battery");
		Reporter.log("Pass >>Validation of Battery column in Device Grid.", true);
	}

	@Test(priority = 3, description = "Validation of Signal Strength column in Device Grid.",enabled=false)
	public void VerifySortingOfSignalStrengthColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n3.Validation of Signal Strength column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Cell Signal Strength");
		deviceGrid.Enable_DisbleColumn("Cell Signal Strength", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Cell Signal Strength");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Cell Signal Strength");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Cell Signal Strength");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Cell Signal Strength");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Cell Signal Strength");

		deviceGrid.EnterValueInAdvanceSearchColumn("PhoneSignal", "<90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<", 90, "Cell Signal Strength");

		deviceGrid.EnterValueInAdvanceSearchColumn("PhoneSignal", ">90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">", 90, "Cell Signal Strength");

		deviceGrid.EnterValueInAdvanceSearchColumn("PhoneSignal", ">=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">=", 90, "Cell Signal Strength");

		deviceGrid.EnterValueInAdvanceSearchColumn("PhoneSignal", "<=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<=", 90, "Cell Signal Strength");

		deviceGrid.EnterValueInAdvanceSearchColumn("PhoneSignal", "=90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=", 90, "Cell Signal Strength");

		deviceGrid.clearColValue("PhoneSignal");
		Reporter.log("Pass >>Validation of Signal Strength column in Device Grid.", true);
	}

	// Agent version have chaned to new pattern change logic from double
	@Test(priority = 4, description = "Validation of Agent Version column in Device Grid.", enabled = false)
	public void VerifySortingOfAgenVersionColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n4.Validation of Agent Version column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Agent Version");
		deviceGrid.Enable_DisbleColumn("Agent Version", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Agent Version");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Agent Version");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Agent Version");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Agent Version");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Agent Version");

		deviceGrid.EnterValueInAdvanceSearchColumn("AgentVersion", "<16");
		deviceGrid.verifyDeviceAdvanceSearchWithDoubleValues("<", 16, "Agent Version");

		deviceGrid.EnterValueInAdvanceSearchColumn("AgentVersion", ">16");
		deviceGrid.verifyDeviceAdvanceSearchWithDoubleValues(">", 16, "Agent Version");

		deviceGrid.EnterValueInAdvanceSearchColumn("AgentVersion", ">=16");
		deviceGrid.verifyDeviceAdvanceSearchWithDoubleValues(">=", 16, "Agent Version");

		deviceGrid.EnterValueInAdvanceSearchColumn("AgentVersion", "<=16");
		deviceGrid.verifyDeviceAdvanceSearchWithDoubleValues("<=", 16, "Agent Version");

		deviceGrid.EnterValueInAdvanceSearchColumn("AgentVersion", "=16");
		deviceGrid.verifyDeviceAdvanceSearchWithDoubleValues("=", 16, "Agent Version");
		deviceGrid.clearColValue("AgentVersion");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Agent Version column in Device Grid.", true);
	}

	@Test(priority = 5, description = "Validation of Battery Temperature column in Device Grid")
	public void VerifySortingOfBatteryTempColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n5.Validation of Phone Number column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Battery Temperature");
		deviceGrid.Enable_DisbleColumn("Battery Temperature", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Battery Temperature");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Battery Temperature");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("Battery Temperature");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Battery Temperature");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Battery Temperature");
		Reporter.log("Pass >>Validation of Battery Temperature column in Device Grid", true);
	}

	@Test(priority = 6, description = "Validation of IP Address column in Device Grid.")
	public void VerifySortingOfIPAddressColumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n6.Validation of IP Address column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("IP Address");
		deviceGrid.Enable_DisbleColumn("IP Address", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("IP Address");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "IP Address");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "IP Address");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("IP Address");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IP Address");
		deviceGrid.EnterValueInAdvanceSearchColumn("DeviceIPAddress", Config.IPAddressOfEnrolledDevice);
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IP Address");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.IPAddressOfEnrolledDevice, "IP Address");
		deviceGrid.clearColValue("DeviceIPAddress");
		Reporter.log("Pass >>Validation of IP Address column in Device Grid.", true);
	}

	@Test(priority = 7, description = "Validation of CPU Usage column in Device Grid.")
	public void VerifySortingOfCPUColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n7.Validation of CPU Usage column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("CPU Usage");
		deviceGrid.Enable_DisbleColumn("CPU Usage", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("CPU Usage");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("CPU Usage");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("CPU Usage");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "CPU Usage");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "CPU Usage");
		deviceGrid.SelectSearchOption("Advanced Search");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", "<90");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<", 90, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", ">80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">", 80, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", ">=80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">=", 80, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", "<=80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<=", 80, "CPU Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("CpuUsage", "=80");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=", 80, "CPU Usage");
		deviceGrid.clearColValue("CpuUsage");
		Reporter.log("Pass >>Validation of CPU Usage column in Device Grid.", true);
	}

	@Test(priority = 8, description = "Validation of Device Time Zone column in Device Grid.")
	public void VerifySortingOfTimeZoneColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n8.Validation of Device Time Zone column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device Time Zone");
		deviceGrid.Enable_DisbleColumn("Device Time Zone", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Device Time Zone");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Device Time Zone");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Device Time Zone");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Device Time Zone");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Device Time Zone");
		deviceGrid.EnterValueForAdvanceSeach("Asia", "DeviceTimeZone");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Asia", "Device Time Zone");
		deviceGrid.clearColValue("DeviceTimeZone");
		Reporter.log("Pass >>Validation of Device Time Zone column in Device Grid.", true);
	}

	@Test(priority = 9, description = "Validation of KNOX Status column in Device Grid")
	public void VerifyKNOXStatus() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n9.Validation of KNOX Status column in Device Grid", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("KNOX Status");
		deviceGrid.Enable_DisbleColumn("KNOX Status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("KNOX Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "KNOX Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "KNOX Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("KNOX Status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No", "KnoxStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("No", "KNOX Status");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes", "KnoxStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Yes", "KNOX Status");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "KnoxStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "KNOX Status");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("KNOX Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("All", "KnoxStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("All", "KNOX Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "KnoxStatus", "KNOX Status", 2);
		Reporter.log("Pass >>Validation of KNOX Status column in Device Grid", true);
	}

	@Test(priority = 'A', description = "Validation of Network Type column in Device Grid.")
	public void VerifyNetworkType() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log("\n10.Validation of Device Time Zone column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Network Type");
		deviceGrid.Enable_DisbleColumn("Network Type", "Enable");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Network Type");
		deviceGrid.EnterValueForAdvanceSeach("Wi-Fi", "NetworkType");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Wi-Fi", "Network Type");
		deviceGrid.clearColValue("NetworkType");
		Reporter.log("Pass >>Validation of Network Type column in Device Grid.", true);
	}

	@Test(priority = 'B', description = "Validation of Bluetooth SSID column in Device Grid.")
	public void VerifyBlueToothSSID() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n11.Validation of Bluetooth SSID column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Bluetooth SSID");
		deviceGrid.Enable_DisbleColumn("Bluetooth SSID", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Bluetooth SSID");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Bluetooth SSID");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Bluetooth SSID");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Bluetooth SSID");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth SSID");
		deviceGrid.EnterValueForAdvanceSeach(Config.BluetoothSSID, "BSSID");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.BluetoothSSID, "Bluetooth SSID");
		deviceGrid.clearColValue("BSSID");
		Reporter.log("Pass >>Validation of Bluetooth SSID column in Device Grid.", true);
	}

	@Test(priority = 'C', description = "Validation of USB Debugging column in Device Grid.", enabled = false)
	public void VerifyUSBDebugging() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n12.Validation of USB Debugging column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("USB Debugging");
		deviceGrid.Enable_DisbleColumn("USB Debugging", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("USB Debugging");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "USB Debugging");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "USB Debugging");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("USB Debugging");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Enabled", "ADBEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Enabled", "USB Debugging");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Disabled", "ADBEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Disabled", "USB Debugging");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "ADBEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "USB Debugging");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Debugging");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "ADBEnable", "USB Debugging", 2);
		Reporter.log("Pass >>Validation of USB Debugging column in Device Grid..", true);
	}

	// **********************************Blocked******************************************************
	// **********************************Blocked******************************************************
	@Test(priority = 'D', description = "Validation of Unknown Source Apps column in Device Grid.", enabled = false)
	public void VerifyUnkownSource() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n13.Validation of Unknown Source Apps column in Device Grid.", true);
		commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Unknown Source Apps");
		deviceGrid.Enable_DisbleColumn("Unknown Source Apps", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Unknown Source Apps");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Unknown Source Apps");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Unknown Source Apps");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Unknown Source Apps");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Unknown Source Apps");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Allowed", "AllowUnknownSource", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Allowed", "Unknown Source Apps");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Unknown Source Apps");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Blocked", "AllowUnknownSource", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Blocked", "Unknown Source Apps");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Unknown Source Apps");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "AllowUnknownSource", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("N/A", "Unknown Source Apps");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Unknown Source Apps");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "AllowUnknownSource", "Unknown Source Apps", 2);
		Reporter.log("Pass >>Validation of Unknown Source Apps column in Device Grid.", true);
	}

	@Test(priority = 'E', description = "Validation of Threat Protection column in Device Grid.")
	public void VerifyThreatProtection() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException, AWTException {
		Reporter.log("\n14.Validation of Threat Protection column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Threat Protection");
		deviceGrid.Enable_DisbleColumn("Threat Protection", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Threat Protection");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Threat Protection");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Threat Protection");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Threat Protection");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Threat Protection");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Enabled", "VerifyAppEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Enabled", "Threat Protection");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Threat Protection");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Disabled", "VerifyAppEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Disabled", "Threat Protection");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Threat Protection");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "VerifyAppEnable", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Threat Protection");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Threat Protection");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "VerifyAppEnable", "Threat Protection", 2);
		Reporter.log("Pass >>Validation of Threat Protection column in Device Grid.", true);
	}

	@Test(priority = 'F', description = "Validation of Platform Integrity column in Device Grid.")
	public void VerifyPlatformIntegrity() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException, AWTException {
		Reporter.log("\n15.Validation of Platform Integrity column in Device Grid.", true);
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Platform Integrity");
		deviceGrid.Enable_DisbleColumn("Platform Integrity", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Platform Integrity");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Platform Integrity");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Platform Integrity");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Platform Integrity");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Platform Integrity");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Pass", "BasicIntegrity", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Pass", "Platform Integrity");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Platform Integrity");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Fail", "BasicIntegrity", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Fail", "Platform Integrity");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Platform Integrity");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "BasicIntegrity", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Platform Integrity");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Threat Protection");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "BasicIntegrity", "Platform Integrity", 2);
		Reporter.log("Pass >>Validation of Platform Integrity column in Device Grid.", true);
	}

	@Test(priority = 'G', description = "Validation of CTS Verified column in Device Grid.")
	public void VerifyCTS() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n16.Validation of CTS Verified column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("CTS Verified");
		deviceGrid.Enable_DisbleColumn("CTS Verified", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("CTS Verified");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "CTS Verified");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "CTS Verified");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("CTS Verified");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("CTS Verified");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes", "CtsProfileMatch", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Yes", "CTS Verified");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("CTS Verified");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No", "CtsProfileMatch", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("No", "CTS Verified");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("CTS Verified");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "CtsProfileMatch", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "CTS Verified");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("CTS Verified");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "CtsProfileMatch", "CTS Verified", 2);
		Reporter.log("Pass >>Validation of CTS Verified column in Device Grid.", true);
	}

	@Test(priority = 'H', description = "Validation of Threats Count column in Device Grid.")
	public void VerifyThreatsCount() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n17.Validation of Threats Count column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Threats Count");
		deviceGrid.Enable_DisbleColumn("Threats Count", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Threats Count");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Threats Count");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("Threats Count");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Threats Count");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Threats Count");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("MTPSystemScanThreatCount", "<1");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<", 1, "Threats Count");

		deviceGrid.EnterValueInAdvanceSearchColumn("MTPSystemScanThreatCount", ">1");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">", 1, "Threats Count");

		deviceGrid.EnterValueInAdvanceSearchColumn("MTPSystemScanThreatCount", ">=1");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues(">=", 1, "Threats Count");

		deviceGrid.EnterValueInAdvanceSearchColumn("MTPSystemScanThreatCount", "<=1");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("<=", 1, "Threats Count");

		deviceGrid.EnterValueInAdvanceSearchColumn("MTPSystemScanThreatCount", "=1");
		deviceGrid.verifyDeviceAdvanceSearchWithIntValues("=", 1, "Threats Count");
		deviceGrid.clearColValue("MTPSystemScanThreatCount");
		Reporter.log("Pass >>Validation of Threats Count column in Device Grid.", true);
	}

	@Test(priority = 'I', description = "Validation of Device Roaming column in Device Grid.")
	public void VerifySortingOfDeviceRoamingColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n18.Validation of Device Roaming column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device Roaming");
		deviceGrid.Enable_DisbleColumn("Device Roaming", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Device Roaming");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Device Roaming");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Device Roaming");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Device Roaming");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Device Roaming");
		deviceGrid.EnterValueForAdvanceSeach("No", "PhoneRoaming");
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("No", "Device Roaming");
		deviceGrid.clearColValue("PhoneRoaming");
		deviceGrid.EnterValueForAdvanceSeach("Yes", "PhoneRoaming");
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Yes", "Device Roaming");
		deviceGrid.clearColValue("PhoneRoaming");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Device Roaming column in Device Grid.", true);
	}

	@Test(priority = 'J', description = "Validation of Phone Number column in Device Grid.")
	public void VerifySortingOfPhoneNumberColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n19.Validation of Phone Number column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Phone Number");
		deviceGrid.Enable_DisbleColumn("Phone Number", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Phone Number");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Phone Number");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Phone Number");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Phone Number");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Phone Number");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Phone Number");
		deviceGrid.EnterValueForAdvanceSeach(Config.PhoneNumber, "PhoneNumber");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.PhoneNumber, "Phone Number");
		deviceGrid.ClearAdvanceSeach("PhoneNumber");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Phone Number column in Device Grid.", true);
	}

	@Test(priority = 'K', description = "Validation of Serial Number column in Device Grid.")
	public void VerifySortingOfSerialNumberColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n20.Validation of Serial Number column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Serial Number");
		deviceGrid.Enable_DisbleColumn("Serial Number", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Serial Number");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Serial Number");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Serial Number");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Serial Number");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Serial Number");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Serial Number");
		deviceGrid.EnterValueForAdvanceSeach(Config.SerialNumber, "SerialNumber");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.SerialNumber, "Serial Number");
		deviceGrid.ClearAdvanceSeach("SerialNumber");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Serial Number column in Device Grid.", true);
	}

	@Test(priority = 'L', description = "Validation of Root Status column in Device Grid.")
	public void VerifyRootStatus() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n21.Validation of Root Status column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Root Status");
		deviceGrid.Enable_DisbleColumn("Root Status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Root Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Root Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Root Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Root Status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No", "RootStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("No", "Root Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes", "RootStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Yes", "Root Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Signed", "RootStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Signed", "Root Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Granted", "RootStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Granted", "Root Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "RootStatus", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("N/A", "Root Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Root Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "RootStatus", "Root Status", 5);
		Reporter.log("Validation of Root Status column in Device Grid.", true);
	}

	@Test(priority = 'M', description = "Validation of Supervised (IOS) column in Device Grid")
	public void VerifySupervisedIOS() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n22.Validation of Platform Integrity column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Supervised(iOS/iPadOS)");
		deviceGrid.Enable_DisbleColumn("Supervised(iOS/iPadOS)", "Enable");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Supervised(iOS/iPadOS)");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No", "IsSupervised", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("No", "Supervised(iOS/iPadOS)");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Supervised(iOS/iPadOS)");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes", "IsSupervised", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Yes", "Supervised(iOS/iPadOS)");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Supervised(iOS/iPadOS)");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "IsSupervised", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Supervised(iOS/iPadOS)");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Supervised(iOS/iPadOS)");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "IsSupervised", "Supervised(iOS/iPadOS)", 2);
		Reporter.log("Pass >>Validation of Platform Integrity column in Device Grid.", true);
	}

	@Test(priority = 'N', description = "Validation of Enrolled column in Device Grid.")
	public void VerifyEnrolledDevice() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n23.Validation of Enrolled column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Enrolled");
		deviceGrid.Enable_DisbleColumn("Enrolled", "Enable");
		deviceGrid.SelectSearchOption("Basic Search");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Yes", "Enrolled");
		commonmethdpage.ClearSearchDevice();

		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Enrolled");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No", "Isenrolled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("No", "Enrolled");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Enrolled");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes", "Isenrolled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Yes", "Enrolled");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Enrolled");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "Isenrolled", "Enrolled", 1);
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Enrolled column in Device Grid.", true);
	}

	@Test(priority = 'O', description = "Validation of Sim Serial Number column in Device Grid")
	public void VerifySimSerialNumber() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n24.Validation of Sim Serial Number column in Device Grid", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Sim Serial Number (ICCID)");
		deviceGrid.Enable_DisbleColumn("Sim Serial Number (ICCID)", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Sim Serial Number (ICCID)");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Sim Serial Number (ICCID)");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Sim Serial Number (ICCID)");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Sim Serial Number (ICCID)");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Sim Serial Number (ICCID)");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Sim Serial Number (ICCID)");
		deviceGrid.EnterValueForAdvanceSeach(Config.SimSerialNumber, "SimSerialNumber");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.SimSerialNumber, "Sim Serial Number (ICCID)");
		deviceGrid.ClearAdvanceSeach("SimSerialNumber");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Sim Serial Number column in Device Grid", true);
	}

	@Test(priority = 'P', description = "Validation of GPS Status column in Device Grid.")
	public void VerifyGPSStatus() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n25.Validation of GPS Status column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("GPS Status");
		deviceGrid.Enable_DisbleColumn("GPS Status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("GPS Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "GPS Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "GPS Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("GPS Status");
		deviceGrid.MoveDevicesToGroup("Home");

		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("GPS Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Enabled", "GPSEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Enabled", "GPS Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("GPS Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Disabled", "GPSEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Disabled", "GPS Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("GPS Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "GPSEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "GPS Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("GPS Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "GPSEnabled", "GPS Status", 2);
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of GPS Status column in Device Grid.", true);
	}

	@Test(priority = 'Q', description = "Validation of Bluetooth Status column in Device Grid.")
	public void VerifyBlueToothStatus() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n26.Validation of Bluetooth Status column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Bluetooth status");
		deviceGrid.Enable_DisbleColumn("Bluetooth status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Bluetooth status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Bluetooth status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Bluetooth status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Bluetooth status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("On", "BluetoothEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("On", "Bluetooth status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Off", "BluetoothEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Off", "Bluetooth status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "BluetoothEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Bluetooth status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "BluetoothEnabled", "Bluetooth status", 2);
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Bluetooth Status column in Device Grid.", true);
	}

	// *****************************Blocked***************************************************
	@Test(priority = 'R', description = "Validation of USB Status column in Device Grid.", enabled = false)
	public void VerifyUSBStatus() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n27.Validation of USB Status column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("USB Status");
		deviceGrid.Enable_DisbleColumn("USB Status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("USB Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "USB Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "USB Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("USB Status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Basic Search");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Plugged In", "USB Status");
		commonmethdpage.ClearSearchDevice();

		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Plugged In", "USBPluggedIn", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Plugged In", "USB Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Plugged Out", "USBPluggedIn", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Plugged Out", "USB Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "USBPluggedIn", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("N/A", "USB Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("USB Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "USBPluggedIn", "USB Status", 2);
		Reporter.log("Pass >>Validation of USB Status column in Device Grid.", true);
	}

	@Test(priority = 'S', description = "Validation of Wi-Fi Hotspot Status column in Device Grid.")
	public void VerifyWiFiHotSpotStatus() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException, AWTException {
		Reporter.log("\n28.Validation of Wi-Fi Hotspot Status column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Wi-Fi Hotspot Status");
		deviceGrid.Enable_DisbleColumn("Wi-Fi Hotspot Status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Wi-Fi Hotspot Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Wi-Fi Hotspot Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Wi-Fi Hotspot Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Wi-Fi Hotspot Status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Wi-Fi Hotspot Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Off", "IsMobileHotSpotEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Off", "Wi-Fi Hotspot Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Wi-Fi Hotspot Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("On", "IsMobileHotSpotEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("On", "Wi-Fi Hotspot Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Wi-Fi Hotspot Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "IsMobileHotSpotEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Wi-Fi Hotspot Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Wi-Fi Hotspot Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "IsMobileHotSpotEnabled", "Wi-Fi Hotspot Status", 2);
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Wi-Fi Hotspot Status column in Device Grid.", true);
	}

	@Test(priority = 'T', description = "Validation of  Device Local IP Address column in Device Grid.")
	public void VerifyLocalIPAddress() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n29.Validation of  Device Local IP Address column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device Local IP Address");
		deviceGrid.Enable_DisbleColumn("Device Local IP Address", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Device Local IP Address");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Device Local IP Address");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Device Local IP Address");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Device Local IP Address");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Device Local IP Address");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Device Local IP Address");
		deviceGrid.EnterValueForAdvanceSeach(Config.LocalIPAddressOfEnrolledDevice, "DeviceLocalIPAddress");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.LocalIPAddressOfEnrolledDevice,
				"Device Local IP Address");
		deviceGrid.ClearAdvanceSeach("DeviceLocalIPAddress");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of  Device Local IP Address column in Device Grid.", true);
	}

	@Test(priority = 'U', description = "Validation of Encryption Status column in Device Grid.")
	public void VerifyEncryptionStatus() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException, AWTException {
		Reporter.log("\n30.Validation of Encryption Status column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Encryption Status");
		deviceGrid.Enable_DisbleColumn("Encryption Status", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Encryption Status");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Encryption Status");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Encryption Status");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Encryption Status");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Encryption Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Yes", "IsEncryptionEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Yes", "Encryption Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Encryption Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("No", "IsEncryptionEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("No", "Encryption Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Encryption Status");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "IsEncryptionEnabled", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Encryption Status");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Encryption Status");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "IsEncryptionEnabled", "Encryption Status", 2);
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Encryption Status column in Device Grid.", true);
	}

	@Test(priority = 'V', description = "Validation of Free Storage Memory column in Device Grid.")
	public void VerifySortingOfFreeStorageMemoryColumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n31.Validation of Free Storage Memory column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Free Storage Memory");
		deviceGrid.Enable_DisbleColumn("Free Storage Memory", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Free Storage Memory");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Free Storage Memory");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("Free Storage Memory");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Free Storage Memory");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Free Storage Memory");
		deviceGrid.SelectSearchOption("Basic Search");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceFreeStorage("Free Storage Memory");
		commonmethdpage.ClearSearchDevice();
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Free Storage Memory column in Device Grid.", true);
	}

	@Test(priority = 'W', description = "Validation of Free Program Memory column in Device Grid.")
	public void VerifySortingOfFreeProgramMemoryColumn()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n32.Validation of Free Program Memory column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Free Program Memory");
		deviceGrid.Enable_DisbleColumn("Free Program Memory", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Free Program Memory");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Free Program Memory");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.VerifySortingIsNotChangedWithNA("Free Program Memory");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Free Program Memory");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Free Program Memory");
		deviceGrid.SelectSearchOption("Basic Search");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceFreeStorage("Free Program Memory");
		commonmethdpage.ClearSearchDevice();
		Reporter.log("Pass >>Validation of Free Program Memory column in Device Grid.", true);
	}

	// Blocked-It is displaying both N/A and
	@Test(priority = 'X', description = "Validation of IMEI column in Device Grid.", enabled = false)
	public void VerifySortingOfIMEIColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n33.Validation of IMEI column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("IMEI");
		deviceGrid.Enable_DisbleColumn("IMEI", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IMEI");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IMEI");
		deviceGrid.EnterValueForAdvanceSeach(Config.IMEINumber, "IMEI");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.IMEINumber, "IMEI");
		deviceGrid.ClearAdvanceSeach("IMEI");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of IMEI column in Device Grid.", true);
	}

	@Test(priority = 'Y', description = "Validation of IMEI2 column in Device Grid.", enabled = false)
	public void VerifySortingOfIMEI2Column() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n34.Validation of IMEI2 column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("IMEI2");
		deviceGrid.Enable_DisbleColumn("IMEI2", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IMEI2");
		deviceGrid.VerifyColumnHaving_NA_Sorting("IMEI2");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "IMEI2");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "IMEI2");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("IMEI2");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("IMEI2");
		deviceGrid.EnterValueForAdvanceSeach(Config.IMEINumber2, "IMEI2");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.IMEINumber2, "IMEI2");
		deviceGrid.ClearAdvanceSeach("IMEI2");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of IMEI2 column in Device Grid.", true);
	}

	// *****************************Blocked***************************************************
	@Test(priority = 'Z', description = "Validation of Device User Name column in Device Grid", enabled = false)
	public void VerifyDeviceUserColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n35.Validation of Device User Name column in Device Grid", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Device User Name");
		deviceGrid.Enable_DisbleColumn("Device User Name", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Device User Name");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Device User Name");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Device User Name");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Device User Name");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Device User Name");
		deviceGrid.EnterValueForAdvanceSeach(Config.DeviceUserName, "DeviceUserName");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.DeviceUserName, "Device User Name");
		deviceGrid.verifyDeviceAvailable(Config.DeviceUserNameDevice);
		deviceGrid.ClearAdvanceSeach("DeviceUserName");
		Reporter.log("Pass >>Validation of Device User Name column in Device Grid", true);
	}

	@Test(priority = 'a', description = "Validation of  Wifi SSID column in Device Grid.")
	public void VerifySortingOfWIFISSIDColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n36.Validation of  Wifi SSID column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Wi-Fi SSID");
		deviceGrid.Enable_DisbleColumn("Wi-Fi SSID", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Wi-Fi SSID");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Wi-Fi SSID");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Wi-Fi SSID");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Wi-Fi SSID");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Wi-Fi SSID");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Wi-Fi SSID");
		deviceGrid.EnterValueForAdvanceSeach(Config.WIFIName, "WifiSSID");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.WIFIName, "Wi-Fi SSID");
		deviceGrid.ClearAdvanceSeach("WifiSSID");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of  Wifi SSID column in Device Grid.", true);
	}

	@Test(priority = 'b', description = "Validation of Bluetooth Name column in Device Grid.")
	public void VerifySortingOfBluetoothNameColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n37.Validation of Bluetooth Name column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Bluetooth Name");
		deviceGrid.Enable_DisbleColumn("Bluetooth Name", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth Name");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Bluetooth Name");
		deviceGrid.VerifySortingInPagination("10");
		deviceGrid.selectPageAndVerifySortingWith_NA("2", "Bluetooth Name");
		deviceGrid.selectPageAndVerifySortingWith_NA("1", "Bluetooth Name");
		deviceGrid.MoveDevicesToGroup("dontouch");
		deviceGrid.VerifySortingIsNotChangedWithNA("Bluetooth Name");
		deviceGrid.MoveDevicesToGroup("Home");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Bluetooth Name");
		deviceGrid.EnterValueForAdvanceSeach(Config.BlueToothName, "RealDeviceName");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.BlueToothName, "Bluetooth Name");
		deviceGrid.ClearAdvanceSeach("RealDeviceName");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Bluetooth Name column in Device Grid.", true);
	}

	@Test(priority = 'c', description = "Validation of Polling Mechanism column in Device Grid.")
	public void VerifyPolling() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException, AWTException {
		Reporter.log("\n38.Validation of Polling Mechanism column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Polling Mechanism");
		deviceGrid.Enable_DisbleColumn("Polling Mechanism", "Enable");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Polling Mechanism", Config.Polling);
		commonmethdpage.ClearSearchDevice();
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Periodic Polling", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Periodic Polling", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("APNS", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("APNS", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("WNS", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("WNS", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Normal Polling", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Normal Polling", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("GCM", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("GCM", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterValueAdvanceSeach("FCM", "NixPollingType", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("FCM", "Polling Mechanism");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Polling Mechanism");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "NixPollingType", "Polling Mechanism", 6);
		Reporter.log("Pass >>Validation of Polling Mechanism column in Device Grid.", true);
	}

	@Test(priority = 'd', description = "Validation of Android Enterprise Status column in Device Grid.")
	public void VerifyAndroidEnterprise() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException, AWTException {
		Reporter.log("\n39.Validation of Android Enterprise Status column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Android Enterprise");
		deviceGrid.Enable_DisbleColumn("Android Enterprise", "Enable");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Android Enterprise",
				Config.AndroidEnterpriseGrid);
		commonmethdpage.ClearSearchDevice();
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Not Supported", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Not Supported", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Not Enrolled", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Not Enrolled", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Managed Device", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Managed Device", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Profile Owner", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Profile Owner", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("Device Owner", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("Device Owner", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterValueAdvanceSeach("N/A", "AfwProfile", 0);
		deviceGrid.verifyDeviceAdvanceSearchForValuesSelectedfromFilter("N/A", "Android Enterprise");

		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android Enterprise");
		deviceGrid.ClickAndEnterResetAdvanceSeach("All", "AfwProfile", "Android Enterprise", 6);
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Polling Mechanism column in Device Grid.", true);
	}

	@Test(priority = 'e', description = "Validation of SureLock Settings Identifier column in Device Grid.")
	public void VerifySortingOfSureLockSettingsColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n40.Validation of SureLock Settings Identifier column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("SureLock Settings Identifier");
		deviceGrid.Enable_DisbleColumn("SureLock Settings Identifier", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("SureLock Settings Identifier");
		deviceGrid.VerifyColumnHaving_NA_Sorting("SureLock Settings Identifier");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("SureLock Settings Identifier");
		deviceGrid.EnterValueForAdvanceSeach("test", "SureLockSettingsVersionCode");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("test", "SureLock Settings Identifier");
		deviceGrid.ClearAdvanceSeach("SureLockSettingsVersionCode");
		deviceGrid.SelectSearchOption("Basic Search");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "SureLock Settings Identifier", "N/A");
		commonmethdpage.ClearSearchDevice();
		Reporter.log("Pass >>Validation of SureLock Settings Identifier column in Device Grid.", true);
	}

	@Test(priority = 'f', description = "Validation of Notes column in Device Grid.")
	public void VerifyNotesColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log("\n41.Validation of Notes column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Notes");
		deviceGrid.Enable_DisbleColumn("Notes", "Enable");
		commonmethdpage.SearchDevice();
		deviceGrid.ClickOnEditNotes();
		deviceGrid.SelectAllNotesAndEnterNotesgrid();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Notes", "AutomationMDM");
		commonmethdpage.ClearSearchDevice();
		Reporter.log("Pass >>Validation of Notes column in Device Grid.", true);
	}

	@Test(priority = 'g', description = "Validation of Data Usage column in Device Grid.")
	public void VerifyDataUsageColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log("\n42.Validation of Data Usage column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Data Usage");
		deviceGrid.Enable_DisbleColumn("Data Usage", "Enable");
		deviceGrid.VerifySortingOfDataUsage("Data Usage");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.EnterValueInAdvanceSearchColumn("DataUsage", "<20000000");
		deviceGrid.VerifyAdvanceSearchDataUsage("<", 20000000, "Data Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("DataUsage", ">20000000");
		deviceGrid.VerifyAdvanceSearchDataUsage(">", 20000000, "Data Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("DataUsage", ">=20000000");
		deviceGrid.VerifyAdvanceSearchDataUsage(">=", 20000000, "Data Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("DataUsage", "<=20000000");
		deviceGrid.VerifyAdvanceSearchDataUsage("<=", 20000000, "Data Usage");

		deviceGrid.EnterValueInAdvanceSearchColumn("DataUsage", "=20000000");
		deviceGrid.VerifyAdvanceSearchDataUsage("=", 20000000, "Data Usage");
		deviceGrid.clearColValue("DataUsage");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Data Usage column in Device Grid.", true);
	}

	@Test(priority = 'h', description = "Verify device HashCode grid column with Enable from device grid column view", enabled = false)
	public void VerifyHashCodeIsPresent()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n43.Verify device HashCode grid column with Enable from device grid column view", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Hash Code");
		deviceGrid.Enable_DisbleColumn("Hash Code", "Enable");
		loginPage.logoutfromApp();
		commonmethdpage.loginAgain();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Hash Code");
		Reporter.log("Pass >>Verify device HashCode grid column with Enable from device grid column view", true);
	}

	@Test(priority = 'i', description = "Verify AndroidID Grid Column by Enable from Device grid column View+Verify AndroidID Column in Device grid column View", enabled = false)
	public void VerifyAndroidIdEnable()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log(
				"\n44.Verify AndroidID Grid Column by Enable from Device grid column View+Verify AndroidID Column in Device grid column View",
				true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Android ID");
		deviceGrid.Enable_DisbleColumn("Android ID", "Enable");
		loginPage.logoutfromApp();
		commonmethdpage.loginAgain();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android ID");
		Reporter.log(
				"Pass >>Verify AndroidID Grid Column by Enable from Device grid column View+Verify AndroidID Column in Device grid column View",
				true);
	}

	@Test(priority = 'j', description = "Verify AndroidID Grid Column value by Sorting the AndroidID value+Verify AndroidID Grid Column value by Advance search the AndroidID value+Verify AndroidID Grid Column value by enroll the device")
	public void VerifySortingColumn() throws InterruptedException, EncryptedDocumentException, InvalidFormatException,
			IOException, ParseException {
		Reporter.log("\n45.Validation of Group Path column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Android ID");
		deviceGrid.Enable_DisbleColumn("Android ID", "Enable");
		deviceGrid.ClickOnAllDevices();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android ID");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Android ID");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Android ID", Config.DeviceAndroidID);
		commonmethdpage.ClearSearchDevice();
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Android ID");
		deviceGrid.EnterValueForAdvanceSeach(Config.DeviceAndroidID, "AndroidID");
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.DeviceAndroidID, "Android ID");
		deviceGrid.ClearAdvanceSeach("AndroidID");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log(
				"Pass >>Verify AndroidID Grid Column value by Sorting the AndroidID value+Verify AndroidID Grid Column value by Advance search the AndroidID value+Verify AndroidID Grid Column value by enroll the device",
				true);
	}

	@Test(priority = 'k', description = "Verify AndroidID Grid Column value For Sub Group with enroll the device")
	public void VerifyAndroidIDForSubGroup() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n46.Verify AndroidID Grid Column value For Sub Group with enroll the device", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.SelectSearchOption("Basic Search");
		commonmethdpage.SearchDevice();
		deviceGrid.MoveDeviceToGroup("dontouch");
		commonmethdpage.ClearSearchDevice();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "Android ID", Config.DeviceAndroidID);
		commonmethdpage.SearchDevice();
		deviceGrid.MoveDeviceToGroup("Home");
		commonmethdpage.ClearSearchDevice();
		Reporter.log(
				"Pass >>Verify AndroidID Grid Column value by Sorting the AndroidID value+Verify AndroidID Grid Column value by Advance search the AndroidID value+Verify AndroidID Grid Column value by enroll the device",
				true);
	}

	@Test(priority = 'l', description = "Validation of OS Build Number column in Device Grid.")
	public void VerifySortingOfOSVersionColumn() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n47.Validation of OS Build Number column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("OS Build Number");
		deviceGrid.Enable_DisbleColumn("OS Build Number", "Enable");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("OS Build Number");
		deviceGrid.VerifyColumnHaving_NA_Sorting("OS Build Number");
		commonmethdpage.SearchDevice();
		deviceGrid.verifyDeviceAndRelatedProperty(Config.DeviceName, "OS Build Number", Config.Android_OSBuildNumber);
		commonmethdpage.ClearSearchDevice();
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of OS Build Number column in Device Grid.", true);
	}

	@Test(priority = 'm', description = "Verify Basic search for the following:")
	public void ValidationforBasicSearch() throws InterruptedException, EncryptedDocumentException,
			InvalidFormatException, IOException, ParseException {
		Reporter.log("\n48.Verify Basic search for the following:", true);
		commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.SearchDevice(Config.DeviceName);
		deviceGrid.verifyDeviceAvailable(Config.DeviceName);
		commonmethdpage.BasicSearch(Config.DeviceModel);
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.DeviceModel, "Platform / Model");
		commonmethdpage.BasicSearch(Config.Network);
		deviceGrid.verifyDeviceAdvanceSearchStringValues(Config.Network, "Network Operator");
		commonmethdpage.BasicSearch(Config.IMEINumber);
		deviceGrid.verifyDeviceAdvanceSearchStringValues("N/A", "IMEI");
		commonmethdpage.BasicSearch("AutomationMDM");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("AutomationMDM", "Notes");
		Reporter.log("Pass >>Verify Basic search for the following:", true);
	}

	@Test(priority = 'n', description = "Verify basic search for invalid data")
	public void VerifySearchingAddingInstalledAppVersion()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n49.Verify basic search for invalid data", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.VarificationInvalidSearch();
		commonmethdpage.ClearSearchDevice();
		Reporter.log("Pass >>Verify basic search for invalid data", true);
	}

	@Test(priority = 'o', description = "Verify searching and adding Installed App Version")
	public void VerifyBasicSearchWithInvalidData()
			throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		Reporter.log("\n50.Verify searching and adding Installed App Version", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.ClickOnInstalledApplicationVersionList();
		deviceGrid.DisableInstallApplicationversionColumns();
		deviceGrid.SearchingForApplicationInstalledAppVersionList("com.android.chrome");
		deviceGrid.enableInstallAppCheckBox("com.android.chrome");
		deviceGrid.closeColumnGrid();
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Chrome");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Chrome");
		Reporter.log("Pass >>Verify searching and adding Installed App Version", true);
	}

	@Test(priority = 'p', description = "Validation of Network Operator column in Device Grid.")
	public void ValidationOfModelCol()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n51.Validation of Network Operator column in Device Grid.", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Network Operator");
		deviceGrid.Enable_DisbleColumn("Network Operator", "Enable");
		deviceGrid.VerifyColumnHaving_NA_Sorting("Network Operator");
		deviceGrid.SelectSearchOption("Advanced Search");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Network Operator");
		deviceGrid.EnterValueForAdvanceSeach("Jio 4G", "Operator");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("Jio 4G", "Network Operator");
		deviceGrid.ClearAdvanceSeach("Operator");
		deviceGrid.VerifyColumnIsPresentOnGridAndScroll("Network Operator");
		deviceGrid.EnterValueForAdvanceSeach("N/A", "Operator");
		deviceGrid.verifyDeviceAdvanceSearchStringValues("N/A", "Network Operator");
		deviceGrid.ClearAdvanceSeach("Operator");
		deviceGrid.SelectSearchOption("Basic Search");
		Reporter.log("Pass >>Validation of Network Operator column in Device Grid.", true);
	}

	@Test(priority = 'q', description = "Verify device HashCode grid column with Disable from device grid column view", enabled = false)
	public void VerifyAndroidIDPresentAndEnabled()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n52.Verify device HashCode grid column with Enable from device grid column view", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Hash Code");
		deviceGrid.Enable_DisbleColumn("Hash Code", "Disable");
		loginPage.logoutfromApp();
		commonmethdpage.loginAgain();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.VerifyColumnIsNotPresentOnGrid("Hash Code");
		Reporter.log("Pass >>Verify device HashCode grid column with Disable from device grid column view", true);
	}

	@Test(priority = 'r', description = "Verify AndroidID Grid Column by Disable from Device grid column View", enabled = false)
	public void VerifyAndroidIDIsNotPresent()
			throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Reporter.log("\n53.Verify AndroidID Grid Column by Disable from Device grid column View", true);
		//commonmethdpage.ClickOnAllDevicesButton();
		deviceGrid.clickOnGridColumn();
		deviceGrid.EnterColumnName("Android ID");
		deviceGrid.Enable_DisbleColumn("Android ID", "Disable");
		loginPage.logoutfromApp();
		commonmethdpage.loginAgain();
		loginPage.login(Config.userID, Config.password);
		deviceGrid.VerifyColumnIsNotPresentOnGrid("Hash Code");
		Reporter.log("Pass >>Verify AndroidID Grid Column by Disable from Device grid column View", true);
	}

	@AfterMethod
	public void RefreshDeviceGrid(ITestResult result) throws InterruptedException, IOException {
		deviceGrid.RefreshGrid();
			if(ITestResult.FAILURE==result.getStatus())
			{
				deviceGrid.RefreshGrid();
			}
		}
		/*commonmethdpage.ClickOnAllDevicesButton();
		commonmethdpage.HardWait(2);
		deviceGrid.RefreshDeviceGrid();
		deviceGrid.RefreshGrid();
		commonmethdpage.HardWait(2);*/
		
}
