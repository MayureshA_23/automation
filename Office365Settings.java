package Settings_TestScripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import Common.Initialization;

public class Office365Settings extends Initialization{	
	
	@Test(priority=1,description="Verify Certificate management is present in Account settings.")
	public void VerifyCertificatemanagementispresentinAccountsettings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		commonmethdpage.ClickOnSettings();
		commonmethdpage.ClickonAccsettings();
		accountsettingspage.ClickOnOffice365Settings();
		accountsettingspage.VerifyOffice365settingsFields();
	}
	
	@Test(priority=2,description="Office 365 Settings -Verify entering Wrong tenant ID in Office 365 Settings.")
	public void VerifyenteringWrongtenantIDinOffice365Settings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		accountsettingspage.SelectEnableOffice365CheckBox();
		accountsettingspage.EnterInvalidTenantIDByClickingOnValidate();
		accountsettingspage.ErrorMessageWhenInvalidTenantIDByClickingOnValidate();
	}
	
	@Test(priority=3,description="Office 365 Settings - Verify not entering the Tenant ID in Office 365 Settings")
	public void VerifynotenteringtheTenantIDinOffice365Settings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		accountsettingspage.UnselectEnableOffice365CheckBox();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ConfirmationMessageOffice365();
		accountsettingspage.SelectEnableOffice365CheckBox();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ErrorMessageByGivingEmptyTenantID();
	}
	
	@Test(priority=4,description="Office 365 Settings - Verify not entering the Application ID in Office 365 Settings")
	public void VerifynotenteringtheApplicationIDinOffice365Settings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		accountsettingspage.EnterTenantID();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ErrorMessageByGivingEmptyApplicationID();
	}
	
	@Test(priority=5,description="Office 365 Settings - Verify not entering the Application Secret in Office 365 Settings")
	public void VerifynotenteringtheApplicationSecretinOffice365Settings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		accountsettingspage.EnterApplicationID();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ErrorMessageByGivingEmptyApplicationSecret();
	}
	
	@Test(priority=6,description="Office 365 Settings - Verify not entering the Username in Office 365 Settings")
	public void VerifynotenteringtheUsernameinOffice365Settings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		accountsettingspage.EnterApplicationSecret();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ErrorMessageByGivingEmptyUsername();
	}
	
	@Test(priority=7,description="Office 365 Settings - Verify not entering the Password in Office 365 Settings")
	public void VerifynotenteringthePasswordinOffice365Settings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		accountsettingspage.EnterOffice365Username();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ErrorMessageByGivingEmptyPassword();
	}
	
	@Test(priority=8,description="Verify Configuring the Office 365 Settings.")
	public void VerifyConfiguringtheOffice365Settings() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException
	{	
		accountsettingspage.SelectEnableOffice365CheckBox();
		accountsettingspage.EnterAllTheFieldsOffice365Settings();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ConfirmationMessageOffice365();
	}

	@Test(priority=9,description="Verify Logs when User Enables/Disables Office 365")
	public void VerifyLogswhenUserEnablesDisablesOffice365TC_ST_624() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException{	
		Reporter.log("\n9.Verify Logs when User Enables/Disables Office 365",true);
		commonmethdpage.ClickOnSettings();
		accountsettingspage.ClickOnAccountSettings();
		accountsettingspage.ClickOnOffice365Settings();
		accountsettingspage.SelectEnableOffice365CheckBox();
		accountsettingspage.EnterAllTheFieldsOffice365Settings();
		accountsettingspage.ClickOnSaveButton();
		accountsettingspage.ConfirmationMessageOffice365();
		commonmethdpage.ClickOnHomePage();
		accountsettingspage.Office365ActivityLogs();
		
	}
	
	
	
	
	
	
}
